package com.android.server;

import android.content.Context;
import android.location.Country;
import android.location.CountryListener;
import android.location.ICountryDetector.Stub;
import android.location.ICountryListener;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Looper;
import android.os.Process;
import android.os.RemoteException;
import android.util.Slog;
import com.android.server.location.ComprehensiveCountryDetector;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

public class CountryDetectorService extends ICountryDetector.Stub
    implements Runnable
{
    private static final boolean DEBUG = false;
    private static final String TAG = "CountryDetector";
    private final Context mContext;
    private ComprehensiveCountryDetector mCountryDetector;
    private Handler mHandler;
    private CountryListener mLocationBasedDetectorListener;
    private final HashMap<IBinder, Receiver> mReceivers = new HashMap();
    private boolean mSystemReady;

    public CountryDetectorService(Context paramContext)
    {
        this.mContext = paramContext;
    }

    private void addListener(ICountryListener paramICountryListener)
    {
        synchronized (this.mReceivers)
        {
            Receiver localReceiver = new Receiver(paramICountryListener);
            try
            {
                paramICountryListener.asBinder().linkToDeath(localReceiver, 0);
                this.mReceivers.put(paramICountryListener.asBinder(), localReceiver);
                if (this.mReceivers.size() == 1)
                {
                    Slog.d("CountryDetector", "The first listener is added");
                    setCountryListener(this.mLocationBasedDetectorListener);
                }
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Slog.e("CountryDetector", "linkToDeath failed:", localRemoteException);
            }
        }
    }

    private void initialize()
    {
        this.mCountryDetector = new ComprehensiveCountryDetector(this.mContext);
        this.mLocationBasedDetectorListener = new CountryListener()
        {
            public void onCountryDetected(final Country paramAnonymousCountry)
            {
                CountryDetectorService.this.mHandler.post(new Runnable()
                {
                    public void run()
                    {
                        CountryDetectorService.this.notifyReceivers(paramAnonymousCountry);
                    }
                });
            }
        };
    }

    private void removeListener(IBinder paramIBinder)
    {
        synchronized (this.mReceivers)
        {
            this.mReceivers.remove(paramIBinder);
            if (this.mReceivers.isEmpty())
            {
                setCountryListener(null);
                Slog.d("CountryDetector", "No listener is left");
            }
            return;
        }
    }

    public void addCountryListener(ICountryListener paramICountryListener)
        throws RemoteException
    {
        if (!this.mSystemReady)
            throw new RemoteException();
        addListener(paramICountryListener);
    }

    public Country detectCountry()
        throws RemoteException
    {
        if (!this.mSystemReady)
            throw new RemoteException();
        return this.mCountryDetector.detectCountry();
    }

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.DUMP", "CountryDetector");
    }

    boolean isSystemReady()
    {
        return this.mSystemReady;
    }

    protected void notifyReceivers(Country paramCountry)
    {
        synchronized (this.mReceivers)
        {
            Iterator localIterator = this.mReceivers.values().iterator();
            while (true)
                if (localIterator.hasNext())
                {
                    Receiver localReceiver = (Receiver)localIterator.next();
                    try
                    {
                        localReceiver.getListener().onCountryDetected(paramCountry);
                    }
                    catch (RemoteException localRemoteException)
                    {
                        Slog.e("CountryDetector", "notifyReceivers failed:", localRemoteException);
                    }
                }
        }
    }

    public void removeCountryListener(ICountryListener paramICountryListener)
        throws RemoteException
    {
        if (!this.mSystemReady)
            throw new RemoteException();
        removeListener(paramICountryListener.asBinder());
    }

    public void run()
    {
        Process.setThreadPriority(10);
        Looper.prepare();
        this.mHandler = new Handler();
        initialize();
        this.mSystemReady = true;
        Looper.loop();
    }

    protected void setCountryListener(final CountryListener paramCountryListener)
    {
        this.mHandler.post(new Runnable()
        {
            public void run()
            {
                CountryDetectorService.this.mCountryDetector.setCountryListener(paramCountryListener);
            }
        });
    }

    void systemReady()
    {
        new Thread(this, "CountryDetectorService").start();
    }

    private final class Receiver
        implements IBinder.DeathRecipient
    {
        private final IBinder mKey;
        private final ICountryListener mListener;

        public Receiver(ICountryListener arg2)
        {
            Object localObject;
            this.mListener = localObject;
            this.mKey = localObject.asBinder();
        }

        public void binderDied()
        {
            CountryDetectorService.this.removeListener(this.mKey);
        }

        public boolean equals(Object paramObject)
        {
            if ((paramObject instanceof Receiver));
            for (boolean bool = this.mKey.equals(((Receiver)paramObject).mKey); ; bool = false)
                return bool;
        }

        public ICountryListener getListener()
        {
            return this.mListener;
        }

        public int hashCode()
        {
            return this.mKey.hashCode();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.CountryDetectorService
 * JD-Core Version:        0.6.2
 */