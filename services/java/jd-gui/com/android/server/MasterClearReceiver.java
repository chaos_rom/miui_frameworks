package com.android.server;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.RecoverySystem;
import android.util.Log;
import android.util.Slog;
import java.io.IOException;

public class MasterClearReceiver extends BroadcastReceiver
{
    private static final String TAG = "MasterClear";

    public void onReceive(final Context paramContext, Intent paramIntent)
    {
        if ((paramIntent.getAction().equals("com.google.android.c2dm.intent.RECEIVE")) && (!"google.com".equals(paramIntent.getStringExtra("from"))))
            Slog.w("MasterClear", "Ignoring master clear request -- not from trusted server.");
        while (true)
        {
            return;
            Slog.w("MasterClear", "!!! FACTORY RESET !!!");
            new Thread("Reboot")
            {
                public void run()
                {
                    try
                    {
                        RecoverySystem.rebootWipeUserData(paramContext);
                        Log.wtf("MasterClear", "Still running after master clear?!");
                        return;
                    }
                    catch (IOException localIOException)
                    {
                        while (true)
                            Slog.e("MasterClear", "Can't perform master clear/factory reset", localIOException);
                    }
                }
            }
            .start();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.MasterClearReceiver
 * JD-Core Version:        0.6.2
 */