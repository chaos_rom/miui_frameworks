package com.android.server;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.ActivityManagerNative;
import android.app.IActivityManager;
import android.app.INotificationManager.Stub;
import android.app.ITransientNotification;
import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.media.IAudioService;
import android.media.IAudioService.Stub;
import android.net.Uri;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.UserId;
import android.os.Vibrator;
import android.provider.Settings.Secure;
import android.provider.Settings.System;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.EventLog;
import android.util.Log;
import android.util.Slog;
import android.util.Xml;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import com.android.internal.os.AtomicFile;
import com.android.internal.util.FastXmlSerializer;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import libcore.io.IoUtils;
import miui.app.ExtraNotification;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class NotificationManagerService extends INotificationManager.Stub
{
    private static final String ATTR_NAME = "name";
    private static final String ATTR_VERSION = "version";
    private static final boolean DBG = false;
    private static final int DB_VERSION = 1;
    private static final int DEFAULT_STREAM_TYPE = 5;
    private static final long[] DEFAULT_VIBRATE_PATTERN = arrayOfLong;
    private static final boolean ENABLE_BLOCKED_NOTIFICATIONS = true;
    private static final boolean ENABLE_BLOCKED_TOASTS = true;
    private static final int JUNK_SCORE = -1000;
    private static final int LONG_DELAY = 3500;
    private static final int MAX_PACKAGE_NOTIFICATIONS = 50;
    private static final int MESSAGE_TIMEOUT = 2;
    private static final int NOTIFICATION_PRIORITY_MULTIPLIER = 10;
    private static final int SCORE_DISPLAY_THRESHOLD = -20;
    private static final boolean SCORE_ONGOING_HIGHER = false;
    private static final int SHORT_DELAY = 2000;
    private static final String TAG = "NotificationService";
    private static final String TAG_BLOCKED_PKGS = "blocked-packages";
    private static final String TAG_BODY = "notification-policy";
    private static final String TAG_PACKAGE = "package";
    final IActivityManager mAm;
    private LightsService.Light mAttentionLight;
    private IAudioService mAudioService;
    private HashSet<String> mBlockedPackages = new HashSet();
    final Context mContext;
    private int mDefaultNotificationColor;
    private int mDefaultNotificationLedOff;
    private int mDefaultNotificationLedOn;
    private int mDisabledNotifications;
    final IBinder mForegroundToken = new Binder();
    private WorkerHandler mHandler;
    private boolean mInCall = false;
    private BroadcastReceiver mIntentReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            String str1 = paramAnonymousIntent.getAction();
            boolean bool1 = false;
            boolean bool2 = false;
            if ((!str1.equals("android.intent.action.PACKAGE_REMOVED")) && (!str1.equals("android.intent.action.PACKAGE_RESTARTED")))
            {
                bool2 = str1.equals("android.intent.action.PACKAGE_CHANGED");
                if (!bool2)
                {
                    bool1 = str1.equals("android.intent.action.QUERY_PACKAGE_RESTART");
                    if ((!bool1) && (!str1.equals("android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE")))
                        break label244;
                }
            }
            String[] arrayOfString1;
            if (str1.equals("android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"))
                arrayOfString1 = paramAnonymousIntent.getStringArrayExtra("android.intent.extra.changed_package_list");
            boolean bool3;
            label132: Uri localUri;
            while ((arrayOfString1 != null) && (arrayOfString1.length > 0))
            {
                String[] arrayOfString2 = arrayOfString1;
                int i = arrayOfString2.length;
                int j = 0;
                while (true)
                    if (j < i)
                    {
                        String str3 = arrayOfString2[j];
                        NotificationManagerService localNotificationManagerService = NotificationManagerService.this;
                        if (bool1)
                            break label238;
                        bool3 = true;
                        localNotificationManagerService.cancelAllNotificationsInt(str3, 0, 0, bool3);
                        j++;
                        continue;
                        if (bool1)
                        {
                            arrayOfString1 = paramAnonymousIntent.getStringArrayExtra("android.intent.extra.PACKAGES");
                            break;
                        }
                        localUri = paramAnonymousIntent.getData();
                        if (localUri != null)
                            break label178;
                    }
            }
            while (true)
            {
                return;
                label178: String str2 = localUri.getSchemeSpecificPart();
                if (str2 != null)
                    if (bool2)
                    {
                        int k = NotificationManagerService.this.mContext.getPackageManager().getApplicationEnabledSetting(str2);
                        if ((k == 1) || (k == 0));
                    }
                    else
                    {
                        arrayOfString1 = new String[1];
                        arrayOfString1[0] = str2;
                        break;
                        label238: bool3 = false;
                        break label132;
                        label244: if (str1.equals("android.intent.action.SCREEN_ON"))
                        {
                            NotificationManagerService.access$1102(NotificationManagerService.this, true);
                        }
                        else if (str1.equals("android.intent.action.SCREEN_OFF"))
                        {
                            NotificationManagerService.access$1102(NotificationManagerService.this, false);
                        }
                        else if (str1.equals("android.intent.action.PHONE_STATE"))
                        {
                            NotificationManagerService.access$1202(NotificationManagerService.this, paramAnonymousIntent.getStringExtra("state").equals(TelephonyManager.EXTRA_STATE_OFFHOOK));
                            NotificationManagerService.this.updateNotificationPulse();
                        }
                        else if (str1.equals("android.intent.action.USER_PRESENT"))
                        {
                            NotificationManagerService.this.mNotificationLight.turnOff();
                        }
                    }
            }
        }
    };
    private NotificationRecord mLedNotification;
    private ArrayList<NotificationRecord> mLights = new ArrayList();
    private StatusBarManagerService.NotificationCallbacks mNotificationCallbacks = new StatusBarManagerService.NotificationCallbacks()
    {
        public void onClearAll()
        {
            NotificationManagerService.this.cancelAll();
        }

        public void onNotificationClear(String paramAnonymousString1, String paramAnonymousString2, int paramAnonymousInt)
        {
            NotificationManagerService.this.cancelNotification(paramAnonymousString1, paramAnonymousString2, paramAnonymousInt, 0, 66, true);
        }

        public void onNotificationClick(String paramAnonymousString1, String paramAnonymousString2, int paramAnonymousInt)
        {
            NotificationManagerService.this.cancelNotification(paramAnonymousString1, paramAnonymousString2, paramAnonymousInt, 16, 64, false);
        }

        public void onNotificationError(String paramAnonymousString1, String paramAnonymousString2, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3, String paramAnonymousString3)
        {
            Slog.d("NotificationService", "onNotification error pkg=" + paramAnonymousString1 + " tag=" + paramAnonymousString2 + " id=" + paramAnonymousInt1 + "; will crashApplication(uid=" + paramAnonymousInt2 + ", pid=" + paramAnonymousInt3 + ")");
            NotificationManagerService.this.cancelNotification(paramAnonymousString1, paramAnonymousString2, paramAnonymousInt1, 0, 0, false);
            long l = Binder.clearCallingIdentity();
            try
            {
                ActivityManagerNative.getDefault().crashApplication(paramAnonymousInt2, paramAnonymousInt3, paramAnonymousString1, "Bad notification posted from package " + paramAnonymousString1 + ": " + paramAnonymousString3);
                label128: Binder.restoreCallingIdentity(l);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                break label128;
            }
        }

        // ERROR //
        public void onPanelRevealed()
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 14	com/android/server/NotificationManagerService$1:this$0	Lcom/android/server/NotificationManagerService;
            //     4: invokestatic 97	com/android/server/NotificationManagerService:access$100	(Lcom/android/server/NotificationManagerService;)Ljava/util/ArrayList;
            //     7: astore_1
            //     8: aload_1
            //     9: monitorenter
            //     10: aload_0
            //     11: getfield 14	com/android/server/NotificationManagerService$1:this$0	Lcom/android/server/NotificationManagerService;
            //     14: aconst_null
            //     15: invokestatic 101	com/android/server/NotificationManagerService:access$602	(Lcom/android/server/NotificationManagerService;Lcom/android/server/NotificationManagerService$NotificationRecord;)Lcom/android/server/NotificationManagerService$NotificationRecord;
            //     18: pop
            //     19: invokestatic 72	android/os/Binder:clearCallingIdentity	()J
            //     22: lstore 4
            //     24: aload_0
            //     25: getfield 14	com/android/server/NotificationManagerService$1:this$0	Lcom/android/server/NotificationManagerService;
            //     28: invokestatic 105	com/android/server/NotificationManagerService:access$300	(Lcom/android/server/NotificationManagerService;)Landroid/media/IAudioService;
            //     31: invokeinterface 111 1 0
            //     36: astore 13
            //     38: aload 13
            //     40: ifnull +10 -> 50
            //     43: aload 13
            //     45: invokeinterface 116 1 0
            //     50: lload 4
            //     52: invokestatic 92	android/os/Binder:restoreCallingIdentity	(J)V
            //     55: aload_0
            //     56: getfield 14	com/android/server/NotificationManagerService$1:this$0	Lcom/android/server/NotificationManagerService;
            //     59: aconst_null
            //     60: invokestatic 119	com/android/server/NotificationManagerService:access$702	(Lcom/android/server/NotificationManagerService;Lcom/android/server/NotificationManagerService$NotificationRecord;)Lcom/android/server/NotificationManagerService$NotificationRecord;
            //     63: pop
            //     64: invokestatic 72	android/os/Binder:clearCallingIdentity	()J
            //     67: lstore 8
            //     69: aload_0
            //     70: getfield 14	com/android/server/NotificationManagerService$1:this$0	Lcom/android/server/NotificationManagerService;
            //     73: invokestatic 123	com/android/server/NotificationManagerService:access$400	(Lcom/android/server/NotificationManagerService;)Landroid/os/Vibrator;
            //     76: invokevirtual 128	android/os/Vibrator:cancel	()V
            //     79: lload 8
            //     81: invokestatic 92	android/os/Binder:restoreCallingIdentity	(J)V
            //     84: aload_0
            //     85: getfield 14	com/android/server/NotificationManagerService$1:this$0	Lcom/android/server/NotificationManagerService;
            //     88: invokestatic 131	com/android/server/NotificationManagerService:access$800	(Lcom/android/server/NotificationManagerService;)Ljava/util/ArrayList;
            //     91: invokevirtual 136	java/util/ArrayList:clear	()V
            //     94: aload_0
            //     95: getfield 14	com/android/server/NotificationManagerService$1:this$0	Lcom/android/server/NotificationManagerService;
            //     98: aconst_null
            //     99: invokestatic 139	com/android/server/NotificationManagerService:access$902	(Lcom/android/server/NotificationManagerService;Lcom/android/server/NotificationManagerService$NotificationRecord;)Lcom/android/server/NotificationManagerService$NotificationRecord;
            //     102: pop
            //     103: aload_0
            //     104: getfield 14	com/android/server/NotificationManagerService$1:this$0	Lcom/android/server/NotificationManagerService;
            //     107: invokestatic 142	com/android/server/NotificationManagerService:access$1000	(Lcom/android/server/NotificationManagerService;)V
            //     110: aload_1
            //     111: monitorexit
            //     112: return
            //     113: astore 12
            //     115: lload 4
            //     117: invokestatic 92	android/os/Binder:restoreCallingIdentity	(J)V
            //     120: aload 12
            //     122: athrow
            //     123: astore_2
            //     124: aload_1
            //     125: monitorexit
            //     126: aload_2
            //     127: athrow
            //     128: astore 10
            //     130: lload 8
            //     132: invokestatic 92	android/os/Binder:restoreCallingIdentity	(J)V
            //     135: aload 10
            //     137: athrow
            //     138: astore 6
            //     140: lload 4
            //     142: invokestatic 92	android/os/Binder:restoreCallingIdentity	(J)V
            //     145: goto -90 -> 55
            //
            // Exception table:
            //     from	to	target	type
            //     24	50	113	finally
            //     10	24	123	finally
            //     50	69	123	finally
            //     79	126	123	finally
            //     130	145	123	finally
            //     69	79	128	finally
            //     24	50	138	android/os/RemoteException
        }

        // ERROR //
        public void onSetDisabled(int paramAnonymousInt)
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 14	com/android/server/NotificationManagerService$1:this$0	Lcom/android/server/NotificationManagerService;
            //     4: invokestatic 97	com/android/server/NotificationManagerService:access$100	(Lcom/android/server/NotificationManagerService;)Ljava/util/ArrayList;
            //     7: astore_2
            //     8: aload_2
            //     9: monitorenter
            //     10: aload_0
            //     11: getfield 14	com/android/server/NotificationManagerService$1:this$0	Lcom/android/server/NotificationManagerService;
            //     14: iload_1
            //     15: invokestatic 148	com/android/server/NotificationManagerService:access$202	(Lcom/android/server/NotificationManagerService;I)I
            //     18: pop
            //     19: ldc 149
            //     21: aload_0
            //     22: getfield 14	com/android/server/NotificationManagerService$1:this$0	Lcom/android/server/NotificationManagerService;
            //     25: invokestatic 153	com/android/server/NotificationManagerService:access$200	(Lcom/android/server/NotificationManagerService;)I
            //     28: iand
            //     29: ifeq +59 -> 88
            //     32: invokestatic 72	android/os/Binder:clearCallingIdentity	()J
            //     35: lstore 5
            //     37: aload_0
            //     38: getfield 14	com/android/server/NotificationManagerService$1:this$0	Lcom/android/server/NotificationManagerService;
            //     41: invokestatic 105	com/android/server/NotificationManagerService:access$300	(Lcom/android/server/NotificationManagerService;)Landroid/media/IAudioService;
            //     44: invokeinterface 111 1 0
            //     49: astore 12
            //     51: aload 12
            //     53: ifnull +10 -> 63
            //     56: aload 12
            //     58: invokeinterface 116 1 0
            //     63: lload 5
            //     65: invokestatic 92	android/os/Binder:restoreCallingIdentity	(J)V
            //     68: invokestatic 72	android/os/Binder:clearCallingIdentity	()J
            //     71: lstore 8
            //     73: aload_0
            //     74: getfield 14	com/android/server/NotificationManagerService$1:this$0	Lcom/android/server/NotificationManagerService;
            //     77: invokestatic 123	com/android/server/NotificationManagerService:access$400	(Lcom/android/server/NotificationManagerService;)Landroid/os/Vibrator;
            //     80: invokevirtual 128	android/os/Vibrator:cancel	()V
            //     83: lload 8
            //     85: invokestatic 92	android/os/Binder:restoreCallingIdentity	(J)V
            //     88: aload_2
            //     89: monitorexit
            //     90: return
            //     91: astore 11
            //     93: lload 5
            //     95: invokestatic 92	android/os/Binder:restoreCallingIdentity	(J)V
            //     98: aload 11
            //     100: athrow
            //     101: astore_3
            //     102: aload_2
            //     103: monitorexit
            //     104: aload_3
            //     105: athrow
            //     106: astore 10
            //     108: lload 8
            //     110: invokestatic 92	android/os/Binder:restoreCallingIdentity	(J)V
            //     113: aload 10
            //     115: athrow
            //     116: astore 7
            //     118: lload 5
            //     120: invokestatic 92	android/os/Binder:restoreCallingIdentity	(J)V
            //     123: goto -55 -> 68
            //
            // Exception table:
            //     from	to	target	type
            //     37	63	91	finally
            //     10	37	101	finally
            //     63	73	101	finally
            //     83	104	101	finally
            //     108	123	101	finally
            //     73	83	106	finally
            //     37	63	116	android/os/RemoteException
        }
    };
    private LightsService.Light mNotificationLight;
    private final ArrayList<NotificationRecord> mNotificationList = new ArrayList();
    private boolean mNotificationPulseEnabled;
    private AtomicFile mPolicyFile;
    private boolean mScreenOn = true;
    private NotificationRecord mSoundNotification;
    private StatusBarManagerService mStatusBar;
    private boolean mSystemReady;
    private ArrayList<ToastRecord> mToastQueue;
    private NotificationRecord mVibrateNotification;
    private Vibrator mVibrator;

    static
    {
        long[] arrayOfLong = new long[4];
        arrayOfLong[0] = 0L;
        arrayOfLong[1] = 250L;
        arrayOfLong[2] = 250L;
        arrayOfLong[3] = 250L;
    }

    NotificationManagerService(Context paramContext, StatusBarManagerService paramStatusBarManagerService, LightsService paramLightsService)
    {
        this.mContext = paramContext;
        this.mVibrator = ((Vibrator)paramContext.getSystemService("vibrator"));
        this.mAm = ActivityManagerNative.getDefault();
        this.mToastQueue = new ArrayList();
        this.mHandler = new WorkerHandler(null);
        loadBlockDb();
        this.mStatusBar = paramStatusBarManagerService;
        paramStatusBarManagerService.setNotificationCallbacks(this.mNotificationCallbacks);
        this.mNotificationLight = paramLightsService.getLight(4);
        this.mAttentionLight = paramLightsService.getLight(5);
        Resources localResources = this.mContext.getResources();
        this.mDefaultNotificationColor = localResources.getColor(17170531);
        this.mDefaultNotificationLedOn = localResources.getInteger(17694746);
        this.mDefaultNotificationLedOff = localResources.getInteger(17694747);
        if (Settings.Secure.getInt(this.mContext.getContentResolver(), "device_provisioned", 0) == 0)
            this.mDisabledNotifications = 262144;
        IntentFilter localIntentFilter1 = new IntentFilter();
        localIntentFilter1.addAction("android.intent.action.SCREEN_ON");
        localIntentFilter1.addAction("android.intent.action.SCREEN_OFF");
        localIntentFilter1.addAction("android.intent.action.PHONE_STATE");
        localIntentFilter1.addAction("android.intent.action.USER_PRESENT");
        this.mContext.registerReceiver(this.mIntentReceiver, localIntentFilter1);
        IntentFilter localIntentFilter2 = new IntentFilter();
        localIntentFilter2.addAction("android.intent.action.PACKAGE_REMOVED");
        localIntentFilter2.addAction("android.intent.action.PACKAGE_CHANGED");
        localIntentFilter2.addAction("android.intent.action.PACKAGE_RESTARTED");
        localIntentFilter2.addAction("android.intent.action.QUERY_PACKAGE_RESTART");
        localIntentFilter2.addDataScheme("package");
        this.mContext.registerReceiver(this.mIntentReceiver, localIntentFilter2);
        IntentFilter localIntentFilter3 = new IntentFilter("android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE");
        this.mContext.registerReceiver(this.mIntentReceiver, localIntentFilter3);
        new SettingsObserver(this.mHandler).observe();
    }

    private boolean areNotificationsEnabledForPackageInt(String paramString)
    {
        if (!this.mBlockedPackages.contains(paramString));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    // ERROR //
    private void cancelNotification(String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
    {
        // Byte code:
        //     0: iconst_5
        //     1: anewarray 345	java/lang/Object
        //     4: astore 7
        //     6: aload 7
        //     8: iconst_0
        //     9: aload_1
        //     10: aastore
        //     11: aload 7
        //     13: iconst_1
        //     14: iload_3
        //     15: invokestatic 351	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     18: aastore
        //     19: aload 7
        //     21: iconst_2
        //     22: aload_2
        //     23: aastore
        //     24: aload 7
        //     26: iconst_3
        //     27: iload 4
        //     29: invokestatic 351	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     32: aastore
        //     33: aload 7
        //     35: iconst_4
        //     36: iload 5
        //     38: invokestatic 351	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     41: aastore
        //     42: sipush 2751
        //     45: aload 7
        //     47: invokestatic 357	android/util/EventLog:writeEvent	(I[Ljava/lang/Object;)I
        //     50: pop
        //     51: aload_0
        //     52: getfield 137	com/android/server/NotificationManagerService:mNotificationList	Ljava/util/ArrayList;
        //     55: astore 9
        //     57: aload 9
        //     59: monitorenter
        //     60: aload_0
        //     61: aload_1
        //     62: aload_2
        //     63: iload_3
        //     64: invokespecial 361	com/android/server/NotificationManagerService:indexOfNotificationLocked	(Ljava/lang/String;Ljava/lang/String;I)I
        //     67: istore 11
        //     69: iload 11
        //     71: iflt +89 -> 160
        //     74: aload_0
        //     75: getfield 137	com/android/server/NotificationManagerService:mNotificationList	Ljava/util/ArrayList;
        //     78: iload 11
        //     80: invokevirtual 365	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     83: checkcast 19	com/android/server/NotificationManagerService$NotificationRecord
        //     86: astore 12
        //     88: iload 4
        //     90: aload 12
        //     92: getfield 369	com/android/server/NotificationManagerService$NotificationRecord:notification	Landroid/app/Notification;
        //     95: getfield 374	android/app/Notification:flags	I
        //     98: iand
        //     99: iload 4
        //     101: if_icmpeq +9 -> 110
        //     104: aload 9
        //     106: monitorexit
        //     107: goto +56 -> 163
        //     110: iload 5
        //     112: aload 12
        //     114: getfield 369	com/android/server/NotificationManagerService$NotificationRecord:notification	Landroid/app/Notification;
        //     117: getfield 374	android/app/Notification:flags	I
        //     120: iand
        //     121: ifeq +17 -> 138
        //     124: aload 9
        //     126: monitorexit
        //     127: goto +36 -> 163
        //     130: astore 10
        //     132: aload 9
        //     134: monitorexit
        //     135: aload 10
        //     137: athrow
        //     138: aload_0
        //     139: getfield 137	com/android/server/NotificationManagerService:mNotificationList	Ljava/util/ArrayList;
        //     142: iload 11
        //     144: invokevirtual 377	java/util/ArrayList:remove	(I)Ljava/lang/Object;
        //     147: pop
        //     148: aload_0
        //     149: aload 12
        //     151: iload 6
        //     153: invokespecial 381	com/android/server/NotificationManagerService:cancelNotificationLocked	(Lcom/android/server/NotificationManagerService$NotificationRecord;Z)V
        //     156: aload_0
        //     157: invokespecial 290	com/android/server/NotificationManagerService:updateLightsLocked	()V
        //     160: aload 9
        //     162: monitorexit
        //     163: return
        //
        // Exception table:
        //     from	to	target	type
        //     60	135	130	finally
        //     138	163	130	finally
    }

    // ERROR //
    private void cancelNotificationLocked(NotificationRecord paramNotificationRecord, boolean paramBoolean)
    {
        // Byte code:
        //     0: iload_2
        //     1: ifeq +23 -> 24
        //     4: aload_1
        //     5: getfield 369	com/android/server/NotificationManagerService$NotificationRecord:notification	Landroid/app/Notification;
        //     8: getfield 389	android/app/Notification:deleteIntent	Landroid/app/PendingIntent;
        //     11: ifnull +13 -> 24
        //     14: aload_1
        //     15: getfield 369	com/android/server/NotificationManagerService$NotificationRecord:notification	Landroid/app/Notification;
        //     18: getfield 389	android/app/Notification:deleteIntent	Landroid/app/PendingIntent;
        //     21: invokevirtual 394	android/app/PendingIntent:send	()V
        //     24: aload_1
        //     25: getfield 369	com/android/server/NotificationManagerService$NotificationRecord:notification	Landroid/app/Notification;
        //     28: getfield 397	android/app/Notification:icon	I
        //     31: ifeq +29 -> 60
        //     34: invokestatic 401	android/os/Binder:clearCallingIdentity	()J
        //     37: lstore 12
        //     39: aload_0
        //     40: getfield 186	com/android/server/NotificationManagerService:mStatusBar	Lcom/android/server/StatusBarManagerService;
        //     43: aload_1
        //     44: getfield 404	com/android/server/NotificationManagerService$NotificationRecord:statusBarKey	Landroid/os/IBinder;
        //     47: invokevirtual 408	com/android/server/StatusBarManagerService:removeNotification	(Landroid/os/IBinder;)V
        //     50: lload 12
        //     52: invokestatic 412	android/os/Binder:restoreCallingIdentity	(J)V
        //     55: aload_1
        //     56: aconst_null
        //     57: putfield 404	com/android/server/NotificationManagerService$NotificationRecord:statusBarKey	Landroid/os/IBinder;
        //     60: aload_0
        //     61: getfield 330	com/android/server/NotificationManagerService:mSoundNotification	Lcom/android/server/NotificationManagerService$NotificationRecord;
        //     64: aload_1
        //     65: if_acmpne +41 -> 106
        //     68: aload_0
        //     69: aconst_null
        //     70: putfield 330	com/android/server/NotificationManagerService:mSoundNotification	Lcom/android/server/NotificationManagerService$NotificationRecord;
        //     73: invokestatic 401	android/os/Binder:clearCallingIdentity	()J
        //     76: lstore 7
        //     78: aload_0
        //     79: getfield 318	com/android/server/NotificationManagerService:mAudioService	Landroid/media/IAudioService;
        //     82: invokeinterface 418 1 0
        //     87: astore 11
        //     89: aload 11
        //     91: ifnull +10 -> 101
        //     94: aload 11
        //     96: invokeinterface 423 1 0
        //     101: lload 7
        //     103: invokestatic 412	android/os/Binder:restoreCallingIdentity	(J)V
        //     106: aload_0
        //     107: getfield 333	com/android/server/NotificationManagerService:mVibrateNotification	Lcom/android/server/NotificationManagerService$NotificationRecord;
        //     110: aload_1
        //     111: if_acmpne +25 -> 136
        //     114: aload_0
        //     115: aconst_null
        //     116: putfield 333	com/android/server/NotificationManagerService:mVibrateNotification	Lcom/android/server/NotificationManagerService$NotificationRecord;
        //     119: invokestatic 401	android/os/Binder:clearCallingIdentity	()J
        //     122: lstore 4
        //     124: aload_0
        //     125: getfield 166	com/android/server/NotificationManagerService:mVibrator	Landroid/os/Vibrator;
        //     128: invokevirtual 426	android/os/Vibrator:cancel	()V
        //     131: lload 4
        //     133: invokestatic 412	android/os/Binder:restoreCallingIdentity	(J)V
        //     136: aload_0
        //     137: getfield 139	com/android/server/NotificationManagerService:mLights	Ljava/util/ArrayList;
        //     140: aload_1
        //     141: invokevirtual 428	java/util/ArrayList:remove	(Ljava/lang/Object;)Z
        //     144: pop
        //     145: aload_0
        //     146: getfield 337	com/android/server/NotificationManagerService:mLedNotification	Lcom/android/server/NotificationManagerService$NotificationRecord;
        //     149: aload_1
        //     150: if_acmpne +8 -> 158
        //     153: aload_0
        //     154: aconst_null
        //     155: putfield 337	com/android/server/NotificationManagerService:mLedNotification	Lcom/android/server/NotificationManagerService$NotificationRecord;
        //     158: return
        //     159: astore 15
        //     161: ldc 60
        //     163: new 430	java/lang/StringBuilder
        //     166: dup
        //     167: invokespecial 431	java/lang/StringBuilder:<init>	()V
        //     170: ldc_w 433
        //     173: invokevirtual 437	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     176: aload_1
        //     177: getfield 440	com/android/server/NotificationManagerService$NotificationRecord:pkg	Ljava/lang/String;
        //     180: invokevirtual 437	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     183: invokevirtual 444	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     186: aload 15
        //     188: invokestatic 450	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     191: pop
        //     192: goto -168 -> 24
        //     195: astore 14
        //     197: lload 12
        //     199: invokestatic 412	android/os/Binder:restoreCallingIdentity	(J)V
        //     202: aload 14
        //     204: athrow
        //     205: astore 10
        //     207: lload 7
        //     209: invokestatic 412	android/os/Binder:restoreCallingIdentity	(J)V
        //     212: aload 10
        //     214: athrow
        //     215: astore 6
        //     217: lload 4
        //     219: invokestatic 412	android/os/Binder:restoreCallingIdentity	(J)V
        //     222: aload 6
        //     224: athrow
        //     225: astore 9
        //     227: goto -126 -> 101
        //
        // Exception table:
        //     from	to	target	type
        //     14	24	159	android/app/PendingIntent$CanceledException
        //     39	50	195	finally
        //     78	101	205	finally
        //     124	131	215	finally
        //     78	101	225	android/os/RemoteException
    }

    private void cancelToastLocked(int paramInt)
    {
        ToastRecord localToastRecord = (ToastRecord)this.mToastQueue.get(paramInt);
        try
        {
            localToastRecord.callback.hide();
            this.mToastQueue.remove(paramInt);
            keepProcessAliveLocked(localToastRecord.pid);
            if (this.mToastQueue.size() > 0)
                showNextToastLocked();
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Slog.w("NotificationService", "Object died trying to hide notification " + localToastRecord.callback + " in package " + localToastRecord.pkg);
        }
    }

    private static final int clamp(int paramInt1, int paramInt2, int paramInt3)
    {
        if (paramInt1 < paramInt2);
        while (true)
        {
            return paramInt2;
            if (paramInt1 > paramInt3)
                paramInt2 = paramInt3;
            else
                paramInt2 = paramInt1;
        }
    }

    private void handleTimeout(ToastRecord paramToastRecord)
    {
        synchronized (this.mToastQueue)
        {
            int i = indexOfToastLocked(paramToastRecord.pkg, paramToastRecord.callback);
            if (i >= 0)
                cancelToastLocked(i);
            return;
        }
    }

    // ERROR //
    private static String idDebugString(Context paramContext, String paramString, int paramInt)
    {
        // Byte code:
        //     0: aload_1
        //     1: ifnull +42 -> 43
        //     4: aload_0
        //     5: aload_1
        //     6: iconst_0
        //     7: invokevirtual 501	android/content/Context:createPackageContext	(Ljava/lang/String;I)Landroid/content/Context;
        //     10: astore 9
        //     12: aload 9
        //     14: astore_3
        //     15: aload_3
        //     16: invokevirtual 206	android/content/Context:getResources	()Landroid/content/res/Resources;
        //     19: astore 4
        //     21: aload 4
        //     23: iload_2
        //     24: invokevirtual 505	android/content/res/Resources:getResourceName	(I)Ljava/lang/String;
        //     27: astore 7
        //     29: aload 7
        //     31: astore 6
        //     33: aload 6
        //     35: areturn
        //     36: astore 8
        //     38: aload_0
        //     39: astore_3
        //     40: goto -25 -> 15
        //     43: aload_0
        //     44: astore_3
        //     45: goto -30 -> 15
        //     48: astore 5
        //     50: ldc_w 507
        //     53: astore 6
        //     55: goto -22 -> 33
        //
        // Exception table:
        //     from	to	target	type
        //     4	12	36	android/content/pm/PackageManager$NameNotFoundException
        //     21	29	48	android/content/res/Resources$NotFoundException
    }

    private int indexOfNotificationLocked(String paramString1, String paramString2, int paramInt)
    {
        ArrayList localArrayList = this.mNotificationList;
        int i = localArrayList.size();
        int j = 0;
        if (j < i)
        {
            NotificationRecord localNotificationRecord = (NotificationRecord)localArrayList.get(j);
            if (paramString2 == null)
                if (localNotificationRecord.tag == null)
                    break label65;
            label65: 
            while ((localNotificationRecord.id != paramInt) || (!localNotificationRecord.pkg.equals(paramString1)))
                do
                {
                    j++;
                    break;
                }
                while (!paramString2.equals(localNotificationRecord.tag));
        }
        while (true)
        {
            return j;
            j = -1;
        }
    }

    private int indexOfToastLocked(String paramString, ITransientNotification paramITransientNotification)
    {
        IBinder localIBinder = paramITransientNotification.asBinder();
        ArrayList localArrayList = this.mToastQueue;
        int i = localArrayList.size();
        int j = 0;
        if (j < i)
        {
            ToastRecord localToastRecord = (ToastRecord)localArrayList.get(j);
            if ((!localToastRecord.pkg.equals(paramString)) || (localToastRecord.callback.asBinder() != localIBinder));
        }
        while (true)
        {
            return j;
            j++;
            break;
            j = -1;
        }
    }

    private void keepProcessAliveLocked(int paramInt)
    {
        int i = 0;
        ArrayList localArrayList = this.mToastQueue;
        int j = localArrayList.size();
        for (int k = 0; k < j; k++)
            if (((ToastRecord)localArrayList.get(k)).pid == paramInt)
                i++;
        try
        {
            IActivityManager localIActivityManager = this.mAm;
            IBinder localIBinder = this.mForegroundToken;
            if (i > 0);
            for (boolean bool = true; ; bool = false)
            {
                localIActivityManager.setProcessForeground(localIBinder, paramInt, bool);
                label79: return;
            }
        }
        catch (RemoteException localRemoteException)
        {
            break label79;
        }
    }

    private void loadBlockDb()
    {
        FileInputStream localFileInputStream;
        synchronized (this.mBlockedPackages)
        {
            if (this.mPolicyFile == null)
            {
                this.mPolicyFile = new AtomicFile(new File(new File("/data/system"), "notification_policy.xml"));
                this.mBlockedPackages.clear();
                localFileInputStream = null;
            }
        }
        while (true)
        {
            try
            {
                localFileInputStream = this.mPolicyFile.openRead();
                localXmlPullParser = Xml.newPullParser();
                localXmlPullParser.setInput(localFileInputStream, null);
                i = localXmlPullParser.next();
                if (i != 1)
                {
                    String str1 = localXmlPullParser.getName();
                    if (i != 2)
                        continue;
                    if ("notification-policy".equals(str1))
                    {
                        Integer.parseInt(localXmlPullParser.getAttributeValue(null, "version"));
                        continue;
                    }
                    if (!"blocked-packages".equals(str1))
                        continue;
                    j = localXmlPullParser.next();
                    if (j == 1)
                        continue;
                    str2 = localXmlPullParser.getName();
                    if ("package".equals(str2))
                    {
                        this.mBlockedPackages.add(localXmlPullParser.getAttributeValue(null, "name"));
                        continue;
                    }
                }
            }
            catch (FileNotFoundException localFileNotFoundException)
            {
                XmlPullParser localXmlPullParser;
                int i;
                int j;
                String str2;
                IoUtils.closeQuietly(localFileInputStream);
                return;
                boolean bool = "blocked-packages".equals(str2);
                if ((!bool) || (j != 3))
                    continue;
                continue;
            }
            catch (IOException localIOException)
            {
                Log.wtf("NotificationService", "Unable to read blocked notifications database", localIOException);
                IoUtils.closeQuietly(localFileInputStream);
                continue;
                localObject1 = finally;
                throw localObject1;
            }
            catch (NumberFormatException localNumberFormatException)
            {
                Log.wtf("NotificationService", "Unable to parse blocked notifications database", localNumberFormatException);
                IoUtils.closeQuietly(localFileInputStream);
                continue;
            }
            catch (XmlPullParserException localXmlPullParserException)
            {
                Log.wtf("NotificationService", "Unable to parse blocked notifications database", localXmlPullParserException);
                IoUtils.closeQuietly(localFileInputStream);
                continue;
            }
            finally
            {
                IoUtils.closeQuietly(localFileInputStream);
            }
            IoUtils.closeQuietly(localFileInputStream);
        }
    }

    private void scheduleTimeoutLocked(ToastRecord paramToastRecord, boolean paramBoolean)
    {
        Message localMessage = Message.obtain(this.mHandler, 2, paramToastRecord);
        long l;
        if (paramBoolean)
        {
            l = 0L;
            this.mHandler.removeCallbacksAndMessages(paramToastRecord);
            this.mHandler.sendMessageDelayed(localMessage, l);
            return;
        }
        if (paramToastRecord.duration == 1);
        for (int i = 3500; ; i = 2000)
        {
            l = i;
            break;
        }
    }

    private void sendAccessibilityEvent(Notification paramNotification, CharSequence paramCharSequence)
    {
        AccessibilityManager localAccessibilityManager = AccessibilityManager.getInstance(this.mContext);
        if (!localAccessibilityManager.isEnabled());
        while (true)
        {
            return;
            AccessibilityEvent localAccessibilityEvent = AccessibilityEvent.obtain(64);
            localAccessibilityEvent.setPackageName(paramCharSequence);
            localAccessibilityEvent.setClassName(Notification.class.getName());
            localAccessibilityEvent.setParcelableData(paramNotification);
            CharSequence localCharSequence = paramNotification.tickerText;
            if (!TextUtils.isEmpty(localCharSequence))
                localAccessibilityEvent.getText().add(localCharSequence);
            localAccessibilityManager.sendAccessibilityEvent(localAccessibilityEvent);
        }
    }

    private void showNextToastLocked()
    {
        ToastRecord localToastRecord = (ToastRecord)this.mToastQueue.get(0);
        while (true)
        {
            if (localToastRecord != null);
            try
            {
                localToastRecord.callback.show();
                scheduleTimeoutLocked(localToastRecord, false);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                Slog.w("NotificationService", "Object died trying to show notification " + localToastRecord.callback + " in package " + localToastRecord.pkg);
                int i = this.mToastQueue.indexOf(localToastRecord);
                if (i >= 0)
                    this.mToastQueue.remove(i);
                keepProcessAliveLocked(localToastRecord.pid);
                if (this.mToastQueue.size() > 0)
                    localToastRecord = (ToastRecord)this.mToastQueue.get(0);
                else
                    localToastRecord = null;
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private void updateLightsLocked()
    {
        if (this.mLedNotification == null)
        {
            int m = this.mLights.size();
            if (m > 0)
                this.mLedNotification = ((NotificationRecord)this.mLights.get(m - 1));
        }
        if ((this.mLedNotification == null) || (this.mInCall) || (this.mScreenOn))
            this.mNotificationLight.turnOff();
        while (true)
        {
            return;
            int i = this.mLedNotification.notification.ledARGB;
            int j = this.mLedNotification.notification.ledOnMS;
            int k = this.mLedNotification.notification.ledOffMS;
            if ((0x4 & this.mLedNotification.notification.defaults) != 0)
            {
                Injector.updateNotificationLight(this);
                i = this.mLedNotification.notification.ledARGB;
                j = this.mLedNotification.notification.ledOnMS;
                k = this.mLedNotification.notification.ledOffMS;
            }
            if (this.mNotificationPulseEnabled)
                this.mNotificationLight.setFlashing(i, 1, j, k);
        }
    }

    private void updateNotificationPulse()
    {
        synchronized (this.mNotificationList)
        {
            updateLightsLocked();
            return;
        }
    }

    private void writeBlockDb()
    {
        HashSet localHashSet = this.mBlockedPackages;
        FileOutputStream localFileOutputStream = null;
        try
        {
            localFileOutputStream = this.mPolicyFile.startWrite();
            localFastXmlSerializer = new FastXmlSerializer();
            localFastXmlSerializer.setOutput(localFileOutputStream, "utf-8");
            localFastXmlSerializer.startDocument(null, Boolean.valueOf(true));
            localFastXmlSerializer.startTag(null, "notification-policy");
            localFastXmlSerializer.attribute(null, "version", String.valueOf(1));
            localFastXmlSerializer.startTag(null, "blocked-packages");
            Iterator localIterator = this.mBlockedPackages.iterator();
            while (localIterator.hasNext())
            {
                String str = (String)localIterator.next();
                localFastXmlSerializer.startTag(null, "package");
                localFastXmlSerializer.attribute(null, "name", str);
                localFastXmlSerializer.endTag(null, "package");
            }
        }
        catch (IOException localIOException)
        {
            FastXmlSerializer localFastXmlSerializer;
            if (localFileOutputStream != null)
                this.mPolicyFile.failWrite(localFileOutputStream);
            while (true)
            {
                return;
                localFastXmlSerializer.endTag(null, "blocked-packages");
                localFastXmlSerializer.endTag(null, "notification-policy");
                localFastXmlSerializer.endDocument();
                this.mPolicyFile.finishWrite(localFileOutputStream);
            }
        }
        finally
        {
        }
    }

    public boolean areNotificationsEnabledForPackage(String paramString)
    {
        checkCallerIsSystem();
        return areNotificationsEnabledForPackageInt(paramString);
    }

    void cancelAll()
    {
        while (true)
        {
            int i;
            synchronized (this.mNotificationList)
            {
                i = -1 + this.mNotificationList.size();
                if (i >= 0)
                {
                    NotificationRecord localNotificationRecord = (NotificationRecord)this.mNotificationList.get(i);
                    if ((0x22 & localNotificationRecord.notification.flags) == 0)
                    {
                        this.mNotificationList.remove(i);
                        cancelNotificationLocked(localNotificationRecord, true);
                    }
                }
                else
                {
                    updateLightsLocked();
                    return;
                }
            }
            i--;
        }
    }

    public void cancelAllNotifications(String paramString)
    {
        checkCallerIsSystemOrSameApp(paramString);
        cancelAllNotificationsInt(paramString, 0, 64, true);
    }

    boolean cancelAllNotificationsInt(String paramString, int paramInt1, int paramInt2, boolean paramBoolean)
    {
        boolean bool1 = true;
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = paramString;
        arrayOfObject[bool1] = Integer.valueOf(paramInt1);
        arrayOfObject[2] = Integer.valueOf(paramInt2);
        EventLog.writeEvent(2752, arrayOfObject);
        while (true)
        {
            boolean bool2;
            int j;
            synchronized (this.mNotificationList)
            {
                int i = this.mNotificationList.size();
                bool2 = false;
                j = i - 1;
                if (j >= 0)
                {
                    NotificationRecord localNotificationRecord = (NotificationRecord)this.mNotificationList.get(j);
                    if (((paramInt1 & localNotificationRecord.notification.flags) != paramInt1) || ((paramInt2 & localNotificationRecord.notification.flags) != 0) || (!localNotificationRecord.pkg.equals(paramString)))
                        continue;
                    bool2 = true;
                    if (!paramBoolean)
                        break;
                    this.mNotificationList.remove(j);
                    cancelNotificationLocked(localNotificationRecord, false);
                }
            }
        }
        return bool1;
    }

    @Deprecated
    public void cancelNotification(String paramString, int paramInt)
    {
        cancelNotificationWithTag(paramString, null, paramInt);
    }

    public void cancelNotificationWithTag(String paramString1, String paramString2, int paramInt)
    {
        checkCallerIsSystemOrSameApp(paramString1);
        if (Binder.getCallingUid() == 1000);
        for (int i = 0; ; i = 64)
        {
            cancelNotification(paramString1, paramString2, paramInt, 0, i, false);
            return;
        }
    }

    public void cancelToast(String paramString, ITransientNotification paramITransientNotification)
    {
        Slog.i("NotificationService", "cancelToast pkg=" + paramString + " callback=" + paramITransientNotification);
        if ((paramString == null) || (paramITransientNotification == null))
            Slog.e("NotificationService", "Not cancelling notification. pkg=" + paramString + " callback=" + paramITransientNotification);
        while (true)
        {
            return;
            long l;
            synchronized (this.mToastQueue)
            {
                l = Binder.clearCallingIdentity();
            }
            try
            {
                int i = indexOfToastLocked(paramString, paramITransientNotification);
                if (i >= 0)
                    cancelToastLocked(i);
                while (true)
                {
                    Binder.restoreCallingIdentity(l);
                    break;
                    localObject1 = finally;
                    throw localObject1;
                    Slog.w("NotificationService", "Toast already cancelled. pkg=" + paramString + " callback=" + paramITransientNotification);
                }
            }
            finally
            {
                Binder.restoreCallingIdentity(l);
            }
        }
    }

    void checkCallerIsSystem()
    {
        int i = Binder.getCallingUid();
        if ((i == 1000) || (i == 0))
            return;
        throw new SecurityException("Disallowed call for uid " + i);
    }

    void checkCallerIsSystemOrSameApp(String paramString)
    {
        int i = Binder.getCallingUid();
        if ((i == 1000) || (i == 0));
        while (true)
        {
            return;
            try
            {
                ApplicationInfo localApplicationInfo = this.mContext.getPackageManager().getApplicationInfo(paramString, 0);
                if (!UserId.isSameApp(localApplicationInfo.uid, i))
                    throw new SecurityException("Calling uid " + i + " gave package" + paramString + " which is owned by uid " + localApplicationInfo.uid);
            }
            catch (PackageManager.NameNotFoundException localNameNotFoundException)
            {
            }
        }
        throw new SecurityException("Unknown package " + paramString);
    }

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.DUMP") != 0)
            paramPrintWriter.println("Permission Denial: can't dump NotificationManager from from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid());
        while (true)
        {
            return;
            paramPrintWriter.println("Current Notification Manager state:");
            synchronized (this.mToastQueue)
            {
                int i = this.mToastQueue.size();
                if (i > 0)
                {
                    paramPrintWriter.println("    Toast Queue:");
                    for (int i1 = 0; i1 < i; i1++)
                        ((ToastRecord)this.mToastQueue.get(i1)).dump(paramPrintWriter, "        ");
                    paramPrintWriter.println("    ");
                }
            }
            synchronized (this.mNotificationList)
            {
                int j = this.mNotificationList.size();
                if (j > 0)
                {
                    paramPrintWriter.println("    Notification List:");
                    int n = 0;
                    while (n < j)
                    {
                        ((NotificationRecord)this.mNotificationList.get(n)).dump(paramPrintWriter, "        ", this.mContext);
                        n++;
                        continue;
                        localObject1 = finally;
                        throw localObject1;
                    }
                    paramPrintWriter.println("    ");
                }
                int k = this.mLights.size();
                if (k > 0)
                {
                    paramPrintWriter.println("    Lights List:");
                    for (int m = 0; m < k; m++)
                        ((NotificationRecord)this.mLights.get(m)).dump(paramPrintWriter, "        ", this.mContext);
                    paramPrintWriter.println("    ");
                }
                paramPrintWriter.println("    mSoundNotification=" + this.mSoundNotification);
                paramPrintWriter.println("    mVibrateNotification=" + this.mVibrateNotification);
                paramPrintWriter.println("    mDisabledNotifications=0x" + Integer.toHexString(this.mDisabledNotifications));
                paramPrintWriter.println("    mSystemReady=" + this.mSystemReady);
            }
        }
    }

    @Deprecated
    public void enqueueNotification(String paramString, int paramInt, Notification paramNotification, int[] paramArrayOfInt)
    {
        enqueueNotificationWithTag(paramString, null, paramInt, paramNotification, paramArrayOfInt);
    }

    // ERROR //
    public void enqueueNotificationInternal(String paramString1, int paramInt1, int paramInt2, String paramString2, int paramInt3, Notification paramNotification, int[] paramArrayOfInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: aload_1
        //     2: invokevirtual 784	com/android/server/NotificationManagerService:checkCallerIsSystemOrSameApp	(Ljava/lang/String;)V
        //     5: ldc_w 913
        //     8: aload_1
        //     9: invokevirtual 515	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     12: istore 8
        //     14: iload 8
        //     16: ifne +112 -> 128
        //     19: aload_0
        //     20: getfield 137	com/android/server/NotificationManagerService:mNotificationList	Ljava/util/ArrayList;
        //     23: astore 46
        //     25: aload 46
        //     27: monitorenter
        //     28: iconst_0
        //     29: istore 47
        //     31: aload_0
        //     32: getfield 137	com/android/server/NotificationManagerService:mNotificationList	Ljava/util/ArrayList;
        //     35: invokevirtual 471	java/util/ArrayList:size	()I
        //     38: istore 49
        //     40: iconst_0
        //     41: istore 50
        //     43: iload 50
        //     45: iload 49
        //     47: if_icmpge +78 -> 125
        //     50: aload_0
        //     51: getfield 137	com/android/server/NotificationManagerService:mNotificationList	Ljava/util/ArrayList;
        //     54: iload 50
        //     56: invokevirtual 365	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     59: checkcast 19	com/android/server/NotificationManagerService$NotificationRecord
        //     62: getfield 440	com/android/server/NotificationManagerService$NotificationRecord:pkg	Ljava/lang/String;
        //     65: aload_1
        //     66: invokevirtual 515	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     69: ifeq +1114 -> 1183
        //     72: iinc 47 1
        //     75: iload 47
        //     77: bipush 50
        //     79: if_icmplt +1104 -> 1183
        //     82: ldc 60
        //     84: new 430	java/lang/StringBuilder
        //     87: dup
        //     88: invokespecial 431	java/lang/StringBuilder:<init>	()V
        //     91: ldc_w 915
        //     94: invokevirtual 437	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     97: iload 47
        //     99: invokevirtual 820	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     102: ldc_w 917
        //     105: invokevirtual 437	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     108: aload_1
        //     109: invokevirtual 437	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     112: invokevirtual 444	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     115: invokestatic 811	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     118: pop
        //     119: aload 46
        //     121: monitorexit
        //     122: goto +1060 -> 1182
        //     125: aload 46
        //     127: monitorexit
        //     128: aload_1
        //     129: ldc_w 919
        //     132: invokevirtual 515	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     135: ifeq +13 -> 148
        //     138: ldc_w 921
        //     141: iconst_2
        //     142: invokestatic 925	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
        //     145: ifeq +47 -> 192
        //     148: iconst_4
        //     149: anewarray 345	java/lang/Object
        //     152: astore 9
        //     154: aload 9
        //     156: iconst_0
        //     157: aload_1
        //     158: aastore
        //     159: aload 9
        //     161: iconst_1
        //     162: iload 5
        //     164: invokestatic 351	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     167: aastore
        //     168: aload 9
        //     170: iconst_2
        //     171: aload 4
        //     173: aastore
        //     174: aload 9
        //     176: iconst_3
        //     177: aload 6
        //     179: invokevirtual 926	android/app/Notification:toString	()Ljava/lang/String;
        //     182: aastore
        //     183: sipush 2750
        //     186: aload 9
        //     188: invokestatic 357	android/util/EventLog:writeEvent	(I[Ljava/lang/Object;)I
        //     191: pop
        //     192: aload_1
        //     193: ifnull +8 -> 201
        //     196: aload 6
        //     198: ifnonnull +61 -> 259
        //     201: new 928	java/lang/IllegalArgumentException
        //     204: dup
        //     205: new 430	java/lang/StringBuilder
        //     208: dup
        //     209: invokespecial 431	java/lang/StringBuilder:<init>	()V
        //     212: ldc_w 930
        //     215: invokevirtual 437	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     218: aload_1
        //     219: invokevirtual 437	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     222: ldc_w 932
        //     225: invokevirtual 437	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     228: iload 5
        //     230: invokevirtual 820	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     233: ldc_w 934
        //     236: invokevirtual 437	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     239: aload 6
        //     241: invokevirtual 479	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     244: invokevirtual 444	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     247: invokespecial 935	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     250: athrow
        //     251: astore 48
        //     253: aload 46
        //     255: monitorexit
        //     256: aload 48
        //     258: athrow
        //     259: aload 6
        //     261: getfield 397	android/app/Notification:icon	I
        //     264: ifeq +61 -> 325
        //     267: aload 6
        //     269: getfield 939	android/app/Notification:contentView	Landroid/widget/RemoteViews;
        //     272: ifnonnull +53 -> 325
        //     275: new 928	java/lang/IllegalArgumentException
        //     278: dup
        //     279: new 430	java/lang/StringBuilder
        //     282: dup
        //     283: invokespecial 431	java/lang/StringBuilder:<init>	()V
        //     286: ldc_w 941
        //     289: invokevirtual 437	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     292: aload_1
        //     293: invokevirtual 437	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     296: ldc_w 932
        //     299: invokevirtual 437	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     302: iload 5
        //     304: invokevirtual 820	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     307: ldc_w 934
        //     310: invokevirtual 437	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     313: aload 6
        //     315: invokevirtual 479	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     318: invokevirtual 444	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     321: invokespecial 935	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     324: athrow
        //     325: aload 6
        //     327: aload 6
        //     329: getfield 944	android/app/Notification:priority	I
        //     332: bipush 254
        //     334: iconst_2
        //     335: invokestatic 946	com/android/server/NotificationManagerService:clamp	(III)I
        //     338: putfield 944	android/app/Notification:priority	I
        //     341: sipush 128
        //     344: aload 6
        //     346: getfield 374	android/app/Notification:flags	I
        //     349: iand
        //     350: ifeq +18 -> 368
        //     353: aload 6
        //     355: getfield 944	android/app/Notification:priority	I
        //     358: iconst_2
        //     359: if_icmpge +9 -> 368
        //     362: aload 6
        //     364: iconst_2
        //     365: putfield 944	android/app/Notification:priority	I
        //     368: bipush 10
        //     370: aload 6
        //     372: getfield 944	android/app/Notification:priority	I
        //     375: imul
        //     376: istore 11
        //     378: iload 8
        //     380: ifne +48 -> 428
        //     383: aload_0
        //     384: aload_1
        //     385: invokespecial 779	com/android/server/NotificationManagerService:areNotificationsEnabledForPackageInt	(Ljava/lang/String;)Z
        //     388: ifne +40 -> 428
        //     391: sipush -1000
        //     394: istore 11
        //     396: ldc 60
        //     398: new 430	java/lang/StringBuilder
        //     401: dup
        //     402: invokespecial 431	java/lang/StringBuilder:<init>	()V
        //     405: ldc_w 948
        //     408: invokevirtual 437	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     411: aload_1
        //     412: invokevirtual 437	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     415: ldc_w 950
        //     418: invokevirtual 437	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     421: invokevirtual 444	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     424: invokestatic 811	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     427: pop
        //     428: iload 11
        //     430: bipush 236
        //     432: if_icmplt +750 -> 1182
        //     435: aload_0
        //     436: getfield 137	com/android/server/NotificationManagerService:mNotificationList	Ljava/util/ArrayList;
        //     439: astore 12
        //     441: aload 12
        //     443: monitorenter
        //     444: new 19	com/android/server/NotificationManagerService$NotificationRecord
        //     447: dup
        //     448: aload_1
        //     449: aload 4
        //     451: iload 5
        //     453: iload_2
        //     454: iload_3
        //     455: iload 11
        //     457: aload 6
        //     459: invokespecial 953	com/android/server/NotificationManagerService$NotificationRecord:<init>	(Ljava/lang/String;Ljava/lang/String;IIIILandroid/app/Notification;)V
        //     462: astore 13
        //     464: aconst_null
        //     465: astore 14
        //     467: aload_0
        //     468: aload_1
        //     469: aload 4
        //     471: iload 5
        //     473: invokespecial 361	com/android/server/NotificationManagerService:indexOfNotificationLocked	(Ljava/lang/String;Ljava/lang/String;I)I
        //     476: istore 16
        //     478: iload 16
        //     480: ifge +432 -> 912
        //     483: aload_0
        //     484: getfield 137	com/android/server/NotificationManagerService:mNotificationList	Ljava/util/ArrayList;
        //     487: aload 13
        //     489: invokevirtual 954	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     492: pop
        //     493: bipush 64
        //     495: aload 6
        //     497: getfield 374	android/app/Notification:flags	I
        //     500: iand
        //     501: ifeq +16 -> 517
        //     504: aload 6
        //     506: bipush 34
        //     508: aload 6
        //     510: getfield 374	android/app/Notification:flags	I
        //     513: ior
        //     514: putfield 374	android/app/Notification:flags	I
        //     517: aload 6
        //     519: getfield 397	android/app/Notification:icon	I
        //     522: ifeq +520 -> 1042
        //     525: new 956	com/android/internal/statusbar/StatusBarNotification
        //     528: dup
        //     529: aload_1
        //     530: iload 5
        //     532: aload 4
        //     534: aload 13
        //     536: getfield 957	com/android/server/NotificationManagerService$NotificationRecord:uid	I
        //     539: aload 13
        //     541: getfield 960	com/android/server/NotificationManagerService$NotificationRecord:initialPid	I
        //     544: iload 11
        //     546: aload 6
        //     548: invokespecial 963	com/android/internal/statusbar/StatusBarNotification:<init>	(Ljava/lang/String;ILjava/lang/String;IIILandroid/app/Notification;)V
        //     551: astore 17
        //     553: aload 14
        //     555: ifnull +430 -> 985
        //     558: aload 14
        //     560: getfield 404	com/android/server/NotificationManagerService$NotificationRecord:statusBarKey	Landroid/os/IBinder;
        //     563: ifnull +422 -> 985
        //     566: aload 13
        //     568: aload 14
        //     570: getfield 404	com/android/server/NotificationManagerService$NotificationRecord:statusBarKey	Landroid/os/IBinder;
        //     573: putfield 404	com/android/server/NotificationManagerService$NotificationRecord:statusBarKey	Landroid/os/IBinder;
        //     576: invokestatic 401	android/os/Binder:clearCallingIdentity	()J
        //     579: lstore 37
        //     581: aload_0
        //     582: getfield 186	com/android/server/NotificationManagerService:mStatusBar	Lcom/android/server/StatusBarManagerService;
        //     585: aload 13
        //     587: getfield 404	com/android/server/NotificationManagerService$NotificationRecord:statusBarKey	Landroid/os/IBinder;
        //     590: aload 17
        //     592: invokevirtual 967	com/android/server/StatusBarManagerService:updateNotification	(Landroid/os/IBinder;Lcom/android/internal/statusbar/StatusBarNotification;)V
        //     595: lload 37
        //     597: invokestatic 412	android/os/Binder:restoreCallingIdentity	(J)V
        //     600: aload_0
        //     601: aload 6
        //     603: aload_1
        //     604: invokespecial 969	com/android/server/NotificationManagerService:sendAccessibilityEvent	(Landroid/app/Notification;Ljava/lang/CharSequence;)V
        //     607: ldc 237
        //     609: aload_0
        //     610: getfield 239	com/android/server/NotificationManagerService:mDisabledNotifications	I
        //     613: iand
        //     614: ifne +238 -> 852
        //     617: aload 14
        //     619: ifnull +14 -> 633
        //     622: bipush 8
        //     624: aload 6
        //     626: getfield 374	android/app/Notification:flags	I
        //     629: iand
        //     630: ifne +222 -> 852
        //     633: aload_0
        //     634: getfield 900	com/android/server/NotificationManagerService:mSystemReady	Z
        //     637: ifeq +215 -> 852
        //     640: aload_0
        //     641: getfield 154	com/android/server/NotificationManagerService:mContext	Landroid/content/Context;
        //     644: ldc_w 971
        //     647: invokevirtual 162	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
        //     650: checkcast 973	android/media/AudioManager
        //     653: astore 23
        //     655: iconst_1
        //     656: aload 6
        //     658: getfield 706	android/app/Notification:defaults	I
        //     661: iand
        //     662: ifeq +527 -> 1189
        //     665: iconst_1
        //     666: istore 24
        //     668: iload 24
        //     670: ifne +11 -> 681
        //     673: aload 6
        //     675: getfield 977	android/app/Notification:sound	Landroid/net/Uri;
        //     678: ifnull +96 -> 774
        //     681: iload 24
        //     683: ifeq +434 -> 1117
        //     686: getstatic 982	android/provider/Settings$System:DEFAULT_NOTIFICATION_URI	Landroid/net/Uri;
        //     689: astore 25
        //     691: iconst_4
        //     692: aload 6
        //     694: getfield 374	android/app/Notification:flags	I
        //     697: iand
        //     698: ifeq +497 -> 1195
        //     701: iconst_1
        //     702: istore 26
        //     704: aload 6
        //     706: getfield 985	android/app/Notification:audioStreamType	I
        //     709: iflt +492 -> 1201
        //     712: aload 6
        //     714: getfield 985	android/app/Notification:audioStreamType	I
        //     717: istore 27
        //     719: aload_0
        //     720: aload 13
        //     722: putfield 330	com/android/server/NotificationManagerService:mSoundNotification	Lcom/android/server/NotificationManagerService$NotificationRecord;
        //     725: aload 23
        //     727: iload 27
        //     729: invokevirtual 988	android/media/AudioManager:getStreamVolume	(I)I
        //     732: ifeq +42 -> 774
        //     735: invokestatic 401	android/os/Binder:clearCallingIdentity	()J
        //     738: lstore 32
        //     740: aload_0
        //     741: getfield 318	com/android/server/NotificationManagerService:mAudioService	Landroid/media/IAudioService;
        //     744: invokeinterface 418 1 0
        //     749: astore 36
        //     751: aload 36
        //     753: ifnull +16 -> 769
        //     756: aload 36
        //     758: aload 25
        //     760: iload 26
        //     762: iload 27
        //     764: invokeinterface 992 4 0
        //     769: lload 32
        //     771: invokestatic 412	android/os/Binder:restoreCallingIdentity	(J)V
        //     774: iconst_2
        //     775: aload 6
        //     777: getfield 706	android/app/Notification:defaults	I
        //     780: iand
        //     781: ifeq +426 -> 1207
        //     784: iconst_1
        //     785: istore 28
        //     787: iload 28
        //     789: ifne +11 -> 800
        //     792: aload 6
        //     794: getfield 995	android/app/Notification:vibrate	[J
        //     797: ifnull +55 -> 852
        //     800: aload 23
        //     802: invokevirtual 998	android/media/AudioManager:getRingerMode	()I
        //     805: ifeq +47 -> 852
        //     808: aload_0
        //     809: aload 13
        //     811: putfield 333	com/android/server/NotificationManagerService:mVibrateNotification	Lcom/android/server/NotificationManagerService$NotificationRecord;
        //     814: aload_0
        //     815: getfield 166	com/android/server/NotificationManagerService:mVibrator	Landroid/os/Vibrator;
        //     818: astore 29
        //     820: iload 28
        //     822: ifeq +315 -> 1137
        //     825: getstatic 119	com/android/server/NotificationManagerService:DEFAULT_VIBRATE_PATTERN	[J
        //     828: astore 30
        //     830: iconst_4
        //     831: aload 6
        //     833: getfield 374	android/app/Notification:flags	I
        //     836: iand
        //     837: ifeq +376 -> 1213
        //     840: iconst_0
        //     841: istore 31
        //     843: aload 29
        //     845: aload 30
        //     847: iload 31
        //     849: invokevirtual 1001	android/os/Vibrator:vibrate	([JI)V
        //     852: aload_0
        //     853: getfield 139	com/android/server/NotificationManagerService:mLights	Ljava/util/ArrayList;
        //     856: aload 14
        //     858: invokevirtual 428	java/util/ArrayList:remove	(Ljava/lang/Object;)Z
        //     861: pop
        //     862: aload_0
        //     863: getfield 337	com/android/server/NotificationManagerService:mLedNotification	Lcom/android/server/NotificationManagerService$NotificationRecord;
        //     866: aload 14
        //     868: if_acmpne +8 -> 876
        //     871: aload_0
        //     872: aconst_null
        //     873: putfield 337	com/android/server/NotificationManagerService:mLedNotification	Lcom/android/server/NotificationManagerService$NotificationRecord;
        //     876: iconst_1
        //     877: aload 6
        //     879: getfield 374	android/app/Notification:flags	I
        //     882: iand
        //     883: ifeq +264 -> 1147
        //     886: aload_0
        //     887: getfield 139	com/android/server/NotificationManagerService:mLights	Ljava/util/ArrayList;
        //     890: aload 13
        //     892: invokevirtual 954	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     895: pop
        //     896: aload_0
        //     897: invokespecial 290	com/android/server/NotificationManagerService:updateLightsLocked	()V
        //     900: aload 12
        //     902: monitorexit
        //     903: aload 7
        //     905: iconst_0
        //     906: iload 5
        //     908: iastore
        //     909: goto +273 -> 1182
        //     912: aload_0
        //     913: getfield 137	com/android/server/NotificationManagerService:mNotificationList	Ljava/util/ArrayList;
        //     916: iload 16
        //     918: invokevirtual 377	java/util/ArrayList:remove	(I)Ljava/lang/Object;
        //     921: checkcast 19	com/android/server/NotificationManagerService$NotificationRecord
        //     924: astore 14
        //     926: aload_0
        //     927: getfield 137	com/android/server/NotificationManagerService:mNotificationList	Ljava/util/ArrayList;
        //     930: iload 16
        //     932: aload 13
        //     934: invokevirtual 1004	java/util/ArrayList:add	(ILjava/lang/Object;)V
        //     937: aload 14
        //     939: ifnull -446 -> 493
        //     942: aload 6
        //     944: aload 6
        //     946: getfield 374	android/app/Notification:flags	I
        //     949: bipush 64
        //     951: aload 14
        //     953: getfield 369	com/android/server/NotificationManagerService$NotificationRecord:notification	Landroid/app/Notification;
        //     956: getfield 374	android/app/Notification:flags	I
        //     959: iand
        //     960: ior
        //     961: putfield 374	android/app/Notification:flags	I
        //     964: goto -471 -> 493
        //     967: astore 15
        //     969: aload 12
        //     971: monitorexit
        //     972: aload 15
        //     974: athrow
        //     975: astore 39
        //     977: lload 37
        //     979: invokestatic 412	android/os/Binder:restoreCallingIdentity	(J)V
        //     982: aload 39
        //     984: athrow
        //     985: invokestatic 401	android/os/Binder:clearCallingIdentity	()J
        //     988: lstore 18
        //     990: aload 13
        //     992: aload_0
        //     993: getfield 186	com/android/server/NotificationManagerService:mStatusBar	Lcom/android/server/StatusBarManagerService;
        //     996: aload 17
        //     998: invokevirtual 1008	com/android/server/StatusBarManagerService:addNotification	(Lcom/android/internal/statusbar/StatusBarNotification;)Landroid/os/IBinder;
        //     1001: putfield 404	com/android/server/NotificationManagerService$NotificationRecord:statusBarKey	Landroid/os/IBinder;
        //     1004: iconst_1
        //     1005: aload 17
        //     1007: getfield 1009	com/android/internal/statusbar/StatusBarNotification:notification	Landroid/app/Notification;
        //     1010: getfield 374	android/app/Notification:flags	I
        //     1013: iand
        //     1014: ifeq +10 -> 1024
        //     1017: aload_0
        //     1018: getfield 202	com/android/server/NotificationManagerService:mAttentionLight	Lcom/android/server/LightsService$Light;
        //     1021: invokevirtual 1012	com/android/server/LightsService$Light:pulse	()V
        //     1024: lload 18
        //     1026: invokestatic 412	android/os/Binder:restoreCallingIdentity	(J)V
        //     1029: goto -429 -> 600
        //     1032: astore 20
        //     1034: lload 18
        //     1036: invokestatic 412	android/os/Binder:restoreCallingIdentity	(J)V
        //     1039: aload 20
        //     1041: athrow
        //     1042: ldc 60
        //     1044: new 430	java/lang/StringBuilder
        //     1047: dup
        //     1048: invokespecial 431	java/lang/StringBuilder:<init>	()V
        //     1051: ldc_w 1014
        //     1054: invokevirtual 437	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1057: aload 6
        //     1059: invokevirtual 479	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     1062: invokevirtual 444	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1065: invokestatic 811	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     1068: pop
        //     1069: aload 14
        //     1071: ifnull -464 -> 607
        //     1074: aload 14
        //     1076: getfield 404	com/android/server/NotificationManagerService$NotificationRecord:statusBarKey	Landroid/os/IBinder;
        //     1079: ifnull -472 -> 607
        //     1082: invokestatic 401	android/os/Binder:clearCallingIdentity	()J
        //     1085: lstore 41
        //     1087: aload_0
        //     1088: getfield 186	com/android/server/NotificationManagerService:mStatusBar	Lcom/android/server/StatusBarManagerService;
        //     1091: aload 14
        //     1093: getfield 404	com/android/server/NotificationManagerService$NotificationRecord:statusBarKey	Landroid/os/IBinder;
        //     1096: invokevirtual 408	com/android/server/StatusBarManagerService:removeNotification	(Landroid/os/IBinder;)V
        //     1099: lload 41
        //     1101: invokestatic 412	android/os/Binder:restoreCallingIdentity	(J)V
        //     1104: goto -497 -> 607
        //     1107: astore 43
        //     1109: lload 41
        //     1111: invokestatic 412	android/os/Binder:restoreCallingIdentity	(J)V
        //     1114: aload 43
        //     1116: athrow
        //     1117: aload 6
        //     1119: getfield 977	android/app/Notification:sound	Landroid/net/Uri;
        //     1122: astore 25
        //     1124: goto -433 -> 691
        //     1127: astore 35
        //     1129: lload 32
        //     1131: invokestatic 412	android/os/Binder:restoreCallingIdentity	(J)V
        //     1134: aload 35
        //     1136: athrow
        //     1137: aload 6
        //     1139: getfield 995	android/app/Notification:vibrate	[J
        //     1142: astore 30
        //     1144: goto -314 -> 830
        //     1147: aload 14
        //     1149: ifnull -249 -> 900
        //     1152: iconst_1
        //     1153: aload 14
        //     1155: getfield 369	com/android/server/NotificationManagerService$NotificationRecord:notification	Landroid/app/Notification;
        //     1158: getfield 374	android/app/Notification:flags	I
        //     1161: iand
        //     1162: ifeq -262 -> 900
        //     1165: aload_0
        //     1166: invokespecial 290	com/android/server/NotificationManagerService:updateLightsLocked	()V
        //     1169: goto -269 -> 900
        //     1172: astore 34
        //     1174: lload 32
        //     1176: invokestatic 412	android/os/Binder:restoreCallingIdentity	(J)V
        //     1179: goto -405 -> 774
        //     1182: return
        //     1183: iinc 50 1
        //     1186: goto -1143 -> 43
        //     1189: iconst_0
        //     1190: istore 24
        //     1192: goto -524 -> 668
        //     1195: iconst_0
        //     1196: istore 26
        //     1198: goto -494 -> 704
        //     1201: iconst_5
        //     1202: istore 27
        //     1204: goto -485 -> 719
        //     1207: iconst_0
        //     1208: istore 28
        //     1210: goto -423 -> 787
        //     1213: bipush 255
        //     1215: istore 31
        //     1217: goto -374 -> 843
        //
        // Exception table:
        //     from	to	target	type
        //     31	128	251	finally
        //     253	256	251	finally
        //     444	581	967	finally
        //     595	740	967	finally
        //     769	903	967	finally
        //     912	972	967	finally
        //     977	990	967	finally
        //     1024	1087	967	finally
        //     1099	1179	967	finally
        //     581	595	975	finally
        //     990	1024	1032	finally
        //     1087	1099	1107	finally
        //     740	769	1127	finally
        //     740	769	1172	android/os/RemoteException
    }

    public void enqueueNotificationWithTag(String paramString1, String paramString2, int paramInt, Notification paramNotification, int[] paramArrayOfInt)
    {
        enqueueNotificationInternal(paramString1, Binder.getCallingUid(), Binder.getCallingPid(), paramString2, paramInt, paramNotification, paramArrayOfInt);
    }

    public void enqueueToast(String paramString, ITransientNotification paramITransientNotification, int paramInt)
    {
        if ((paramString == null) || (paramITransientNotification == null))
            Slog.e("NotificationService", "Not doing toast. pkg=" + paramString + " callback=" + paramITransientNotification);
        while (true)
        {
            return;
            boolean bool = "android".equals(paramString);
            if ((!bool) && (!areNotificationsEnabledForPackageInt(paramString)))
            {
                Slog.e("NotificationService", "Suppressing toast from package " + paramString + " by user request.");
                continue;
            }
            int i;
            long l;
            synchronized (this.mToastQueue)
            {
                i = Binder.getCallingPid();
                l = Binder.clearCallingIdentity();
            }
            try
            {
                int j = indexOfToastLocked(paramString, paramITransientNotification);
                if (j >= 0)
                    ((ToastRecord)this.mToastQueue.get(j)).update(paramInt);
                while (true)
                {
                    if (j == 0)
                        showNextToastLocked();
                    Binder.restoreCallingIdentity(l);
                    break;
                    localObject1 = finally;
                    throw localObject1;
                    if (!bool)
                    {
                        int k = 0;
                        int m = this.mToastQueue.size();
                        for (int n = 0; ; n++)
                        {
                            if (n >= m)
                                break label291;
                            if (((ToastRecord)this.mToastQueue.get(n)).pkg.equals(paramString))
                            {
                                k++;
                                if (k >= 50)
                                {
                                    Slog.e("NotificationService", "Package has already posted " + k + " toasts. Not showing more. Package=" + paramString);
                                    Binder.restoreCallingIdentity(l);
                                    break;
                                }
                            }
                        }
                    }
                    label291: ToastRecord localToastRecord = new ToastRecord(i, paramString, paramITransientNotification, paramInt);
                    this.mToastQueue.add(localToastRecord);
                    j = -1 + this.mToastQueue.size();
                    keepProcessAliveLocked(i);
                }
            }
            finally
            {
                Binder.restoreCallingIdentity(l);
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    int getDefaultNotificationColor()
    {
        return this.mDefaultNotificationColor;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    NotificationRecord getLedNotification()
    {
        return this.mLedNotification;
    }

    public void setNotificationsEnabledForPackage(String paramString, boolean paramBoolean)
    {
        checkCallerIsSystem();
        if (paramBoolean)
        {
            this.mBlockedPackages.remove(paramString);
            writeBlockDb();
            return;
        }
        this.mBlockedPackages.add(paramString);
        while (true)
        {
            int j;
            synchronized (this.mNotificationList)
            {
                int i = this.mNotificationList.size();
                j = 0;
                if (j < i)
                {
                    NotificationRecord localNotificationRecord = (NotificationRecord)this.mNotificationList.get(j);
                    if (!localNotificationRecord.pkg.equals(paramString))
                        break label109;
                    cancelNotificationLocked(localNotificationRecord, false);
                }
            }
            label109: j++;
        }
    }

    void systemReady()
    {
        this.mAudioService = IAudioService.Stub.asInterface(ServiceManager.getService("audio"));
        this.mSystemReady = true;
    }

    private final class WorkerHandler extends Handler
    {
        private WorkerHandler()
        {
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
            case 2:
            }
            while (true)
            {
                return;
                NotificationManagerService.this.handleTimeout((NotificationManagerService.ToastRecord)paramMessage.obj);
            }
        }
    }

    class SettingsObserver extends ContentObserver
    {
        SettingsObserver(Handler arg2)
        {
            super();
        }

        void observe()
        {
            NotificationManagerService.this.mContext.getContentResolver().registerContentObserver(Settings.System.getUriFor("notification_light_pulse"), false, this);
            update();
        }

        public void onChange(boolean paramBoolean)
        {
            update();
        }

        public void update()
        {
            boolean bool = false;
            if (Settings.System.getInt(NotificationManagerService.this.mContext.getContentResolver(), "notification_light_pulse", 0) != 0)
                bool = true;
            if (NotificationManagerService.this.mNotificationPulseEnabled != bool)
            {
                NotificationManagerService.access$1502(NotificationManagerService.this, bool);
                NotificationManagerService.this.updateNotificationPulse();
            }
        }
    }

    private static final class ToastRecord
    {
        final ITransientNotification callback;
        int duration;
        final int pid;
        final String pkg;

        ToastRecord(int paramInt1, String paramString, ITransientNotification paramITransientNotification, int paramInt2)
        {
            this.pid = paramInt1;
            this.pkg = paramString;
            this.callback = paramITransientNotification;
            this.duration = paramInt2;
        }

        void dump(PrintWriter paramPrintWriter, String paramString)
        {
            paramPrintWriter.println(paramString + this);
        }

        public final String toString()
        {
            return "ToastRecord{" + Integer.toHexString(System.identityHashCode(this)) + " pkg=" + this.pkg + " callback=" + this.callback + " duration=" + this.duration;
        }

        void update(int paramInt)
        {
            this.duration = paramInt;
        }
    }

    private static final class NotificationRecord
    {
        final int id;
        final int initialPid;
        final Notification notification;
        final String pkg;
        final int score;
        IBinder statusBarKey;
        final String tag;
        final int uid;

        NotificationRecord(String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, Notification paramNotification)
        {
            this.pkg = paramString1;
            this.tag = paramString2;
            this.id = paramInt1;
            this.uid = paramInt2;
            this.initialPid = paramInt3;
            this.score = paramInt4;
            this.notification = paramNotification;
        }

        void dump(PrintWriter paramPrintWriter, String paramString, Context paramContext)
        {
            paramPrintWriter.println(paramString + this);
            paramPrintWriter.println(paramString + "    icon=0x" + Integer.toHexString(this.notification.icon) + " / " + NotificationManagerService.idDebugString(paramContext, this.pkg, this.notification.icon));
            paramPrintWriter.println(paramString + "    pri=" + this.notification.priority);
            paramPrintWriter.println(paramString + "    score=" + this.score);
            paramPrintWriter.println(paramString + "    contentIntent=" + this.notification.contentIntent);
            paramPrintWriter.println(paramString + "    deleteIntent=" + this.notification.deleteIntent);
            paramPrintWriter.println(paramString + "    tickerText=" + this.notification.tickerText);
            paramPrintWriter.println(paramString + "    contentView=" + this.notification.contentView);
            paramPrintWriter.println(paramString + "    uid=" + this.uid);
            paramPrintWriter.println(paramString + "    defaults=0x" + Integer.toHexString(this.notification.defaults));
            paramPrintWriter.println(paramString + "    flags=0x" + Integer.toHexString(this.notification.flags));
            paramPrintWriter.println(paramString + "    sound=" + this.notification.sound);
            paramPrintWriter.println(paramString + "    vibrate=" + Arrays.toString(this.notification.vibrate));
            paramPrintWriter.println(paramString + "    ledARGB=0x" + Integer.toHexString(this.notification.ledARGB) + " ledOnMS=" + this.notification.ledOnMS + " ledOffMS=" + this.notification.ledOffMS);
        }

        public final String toString()
        {
            return "NotificationRecord{" + Integer.toHexString(System.identityHashCode(this)) + " pkg=" + this.pkg + " id=" + Integer.toHexString(this.id) + " tag=" + this.tag + " score=" + this.score + "}";
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static void updateNotificationLight(NotificationManagerService paramNotificationManagerService)
        {
            Context localContext = paramNotificationManagerService.mContext;
            int i = paramNotificationManagerService.getDefaultNotificationColor();
            NotificationManagerService.NotificationRecord localNotificationRecord = paramNotificationManagerService.getLedNotification();
            int j = Settings.System.getInt(localContext.getContentResolver(), "breathing_light_color", i);
            int k = localContext.getResources().getInteger(101187594);
            int[] arrayOfInt = ExtraNotification.getLedPwmOffOn(Settings.System.getInt(localContext.getContentResolver(), "breathing_light_freq", k));
            localNotificationRecord.notification.ledARGB = j;
            localNotificationRecord.notification.ledOnMS = arrayOfInt[1];
            localNotificationRecord.notification.ledOffMS = arrayOfInt[0];
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.NotificationManagerService
 * JD-Core Version:        0.6.2
 */