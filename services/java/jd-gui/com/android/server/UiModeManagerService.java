package com.android.server;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.ActivityManagerNative;
import android.app.AlarmManager;
import android.app.IActivityManager;
import android.app.IUiModeManager.Stub;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.StatusBarManager;
import android.app.UiModeManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.provider.Settings.Secure;
import android.provider.Settings.System;
import android.text.format.Time;
import android.util.Slog;
import com.android.internal.app.DisableCarModeActivity;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

class UiModeManagerService extends IUiModeManager.Stub
{
    private static final String ACTION_UPDATE_NIGHT_MODE = "com.android.server.action.UPDATE_NIGHT_MODE";
    private static final boolean ENABLE_LAUNCH_CAR_DOCK_APP = true;
    private static final boolean ENABLE_LAUNCH_DESK_DOCK_APP = true;
    private static final double FACTOR_GMT_OFFSET_LONGITUDE = 0.004166666666666667D;
    private static final String KEY_LAST_UPDATE_INTERVAL = "LAST_UPDATE_INTERVAL";
    private static final float LOCATION_UPDATE_DISTANCE_METER = 20000.0F;
    private static final long LOCATION_UPDATE_ENABLE_INTERVAL_MAX = 900000L;
    private static final long LOCATION_UPDATE_ENABLE_INTERVAL_MIN = 5000L;
    private static final long LOCATION_UPDATE_MS = 86400000L;
    private static final boolean LOG = false;
    private static final long MIN_LOCATION_UPDATE_MS = 1800000L;
    private static final int MSG_ENABLE_LOCATION_UPDATES = 1;
    private static final int MSG_GET_NEW_LOCATION_UPDATE = 2;
    private static final int MSG_UPDATE_TWILIGHT;
    private static final String TAG = UiModeManager.class.getSimpleName();
    private AlarmManager mAlarmManager;
    private final BroadcastReceiver mBatteryReceiver;
    private boolean mCarModeEnabled;
    private final boolean mCarModeKeepsScreenOn;
    private boolean mCharging;
    private boolean mComputedNightMode;
    private Configuration mConfiguration;
    private final Context mContext;
    private int mCurUiMode;
    private final int mDefaultUiModeType;
    private final boolean mDeskModeKeepsScreenOn;
    private final BroadcastReceiver mDockModeReceiver;
    private int mDockState;
    private final LocationListener mEmptyLocationListener;
    private final Handler mHandler;
    private boolean mHoldingConfiguration;
    private int mLastBroadcastState;
    private Location mLocation;
    private final LocationListener mLocationListener;
    private LocationManager mLocationManager;
    final Object mLock;
    private int mNightMode;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    int mNormalType;
    private NotificationManager mNotificationManager;
    private final BroadcastReceiver mResultReceiver;
    private int mSetUiMode;
    private StatusBarManager mStatusBarManager;
    private boolean mSystemReady;
    private final boolean mTelevision;
    private final BroadcastReceiver mTwilightUpdateReceiver;
    private final BroadcastReceiver mUpdateLocationReceiver;
    private final PowerManager.WakeLock mWakeLock;

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public UiModeManagerService(Context paramContext)
    {
        this.mNormalType = i;
        this.mLock = new Object();
        this.mDockState = 0;
        this.mLastBroadcastState = 0;
        this.mNightMode = i;
        this.mCarModeEnabled = false;
        this.mCharging = false;
        this.mCurUiMode = 0;
        this.mSetUiMode = 0;
        this.mHoldingConfiguration = false;
        this.mConfiguration = new Configuration();
        this.mResultReceiver = new BroadcastReceiver()
        {
            // ERROR //
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                // Byte code:
                //     0: aload_0
                //     1: invokevirtual 23	com/android/server/UiModeManagerService$1:getResultCode	()I
                //     4: bipush 255
                //     6: if_icmpeq +4 -> 10
                //     9: return
                //     10: aload_2
                //     11: ldc 25
                //     13: iconst_0
                //     14: invokevirtual 31	android/content/Intent:getIntExtra	(Ljava/lang/String;I)I
                //     17: istore_3
                //     18: aload_2
                //     19: ldc 33
                //     21: iconst_0
                //     22: invokevirtual 31	android/content/Intent:getIntExtra	(Ljava/lang/String;I)I
                //     25: istore 4
                //     27: aload_0
                //     28: getfield 12	com/android/server/UiModeManagerService$1:this$0	Lcom/android/server/UiModeManagerService;
                //     31: getfield 37	com/android/server/UiModeManagerService:mLock	Ljava/lang/Object;
                //     34: astore 5
                //     36: aload 5
                //     38: monitorenter
                //     39: aconst_null
                //     40: astore 6
                //     42: getstatic 43	android/app/UiModeManager:ACTION_ENTER_CAR_MODE	Ljava/lang/String;
                //     45: aload_2
                //     46: invokevirtual 47	android/content/Intent:getAction	()Ljava/lang/String;
                //     49: invokevirtual 53	java/lang/String:equals	(Ljava/lang/Object;)Z
                //     52: ifeq +138 -> 190
                //     55: iload_3
                //     56: iconst_1
                //     57: iand
                //     58: ifeq +7 -> 65
                //     61: ldc 55
                //     63: astore 6
                //     65: aload 6
                //     67: ifnull +82 -> 149
                //     70: aload 6
                //     72: invokestatic 59	com/android/server/UiModeManagerService:buildHomeIntent	(Ljava/lang/String;)Landroid/content/Intent;
                //     75: astore 9
                //     77: aconst_null
                //     78: astore 10
                //     80: aload_0
                //     81: getfield 12	com/android/server/UiModeManagerService$1:this$0	Lcom/android/server/UiModeManagerService;
                //     84: invokestatic 63	com/android/server/UiModeManagerService:access$000	(Lcom/android/server/UiModeManagerService;)Z
                //     87: ifeq +33 -> 120
                //     90: aload_0
                //     91: getfield 12	com/android/server/UiModeManagerService$1:this$0	Lcom/android/server/UiModeManagerService;
                //     94: iconst_0
                //     95: invokestatic 67	com/android/server/UiModeManagerService:access$002	(Lcom/android/server/UiModeManagerService;Z)Z
                //     98: pop
                //     99: aload_0
                //     100: getfield 12	com/android/server/UiModeManagerService$1:this$0	Lcom/android/server/UiModeManagerService;
                //     103: iconst_0
                //     104: invokevirtual 71	com/android/server/UiModeManagerService:updateConfigurationLocked	(Z)V
                //     107: aload_0
                //     108: getfield 12	com/android/server/UiModeManagerService$1:this$0	Lcom/android/server/UiModeManagerService;
                //     111: invokestatic 75	com/android/server/UiModeManagerService:access$100	(Lcom/android/server/UiModeManagerService;)Landroid/content/res/Configuration;
                //     114: astore 16
                //     116: aload 16
                //     118: astore 10
                //     120: invokestatic 81	android/app/ActivityManagerNative:getDefault	()Landroid/app/IActivityManager;
                //     123: aconst_null
                //     124: aload 9
                //     126: aconst_null
                //     127: aconst_null
                //     128: aconst_null
                //     129: iconst_0
                //     130: iconst_0
                //     131: aload 10
                //     133: aconst_null
                //     134: invokeinterface 87 10 0
                //     139: pop
                //     140: aload_0
                //     141: getfield 12	com/android/server/UiModeManagerService$1:this$0	Lcom/android/server/UiModeManagerService;
                //     144: iconst_0
                //     145: invokestatic 67	com/android/server/UiModeManagerService:access$002	(Lcom/android/server/UiModeManagerService;Z)Z
                //     148: pop
                //     149: aload_0
                //     150: getfield 12	com/android/server/UiModeManagerService$1:this$0	Lcom/android/server/UiModeManagerService;
                //     153: invokestatic 63	com/android/server/UiModeManagerService:access$000	(Lcom/android/server/UiModeManagerService;)Z
                //     156: ifeq +20 -> 176
                //     159: aload_0
                //     160: getfield 12	com/android/server/UiModeManagerService$1:this$0	Lcom/android/server/UiModeManagerService;
                //     163: iconst_0
                //     164: invokestatic 67	com/android/server/UiModeManagerService:access$002	(Lcom/android/server/UiModeManagerService;Z)Z
                //     167: pop
                //     168: aload_0
                //     169: getfield 12	com/android/server/UiModeManagerService$1:this$0	Lcom/android/server/UiModeManagerService;
                //     172: iconst_1
                //     173: invokevirtual 71	com/android/server/UiModeManagerService:updateConfigurationLocked	(Z)V
                //     176: aload 5
                //     178: monitorexit
                //     179: goto -170 -> 9
                //     182: astore 7
                //     184: aload 5
                //     186: monitorexit
                //     187: aload 7
                //     189: athrow
                //     190: getstatic 90	android/app/UiModeManager:ACTION_ENTER_DESK_MODE	Ljava/lang/String;
                //     193: aload_2
                //     194: invokevirtual 47	android/content/Intent:getAction	()Ljava/lang/String;
                //     197: invokevirtual 53	java/lang/String:equals	(Ljava/lang/Object;)Z
                //     200: ifeq +33 -> 233
                //     203: iload_3
                //     204: iconst_1
                //     205: iand
                //     206: ifeq -141 -> 65
                //     209: ldc 92
                //     211: astore 6
                //     213: goto -148 -> 65
                //     216: astore 11
                //     218: invokestatic 95	com/android/server/UiModeManagerService:access$200	()Ljava/lang/String;
                //     221: aload 11
                //     223: invokevirtual 99	android/os/RemoteException:getCause	()Ljava/lang/Throwable;
                //     226: invokestatic 105	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/Throwable;)I
                //     229: pop
                //     230: goto -81 -> 149
                //     233: iload 4
                //     235: iconst_1
                //     236: iand
                //     237: ifeq -172 -> 65
                //     240: ldc 107
                //     242: astore 6
                //     244: goto -179 -> 65
                //
                // Exception table:
                //     from	to	target	type
                //     42	116	182	finally
                //     120	149	182	finally
                //     149	187	182	finally
                //     190	230	182	finally
                //     120	149	216	android/os/RemoteException
            }
        };
        this.mTwilightUpdateReceiver = new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                if ((UiModeManagerService.this.isDoingNightMode()) && (UiModeManagerService.this.mNightMode == 0))
                    UiModeManagerService.this.mHandler.sendEmptyMessage(0);
            }
        };
        this.mDockModeReceiver = new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                int i = paramAnonymousIntent.getIntExtra("android.intent.extra.DOCK_STATE", 0);
                UiModeManagerService.this.updateDockState(i);
            }
        };
        this.mBatteryReceiver = new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                boolean bool = false;
                UiModeManagerService localUiModeManagerService = UiModeManagerService.this;
                if (paramAnonymousIntent.getIntExtra("plugged", 0) != 0)
                    bool = true;
                UiModeManagerService.access$502(localUiModeManagerService, bool);
                synchronized (UiModeManagerService.this.mLock)
                {
                    if (UiModeManagerService.this.mSystemReady)
                        UiModeManagerService.this.updateLocked(0, 0);
                    return;
                }
            }
        };
        this.mUpdateLocationReceiver = new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                if ("android.intent.action.AIRPLANE_MODE".equals(paramAnonymousIntent.getAction()))
                    if (!paramAnonymousIntent.getBooleanExtra("state", false))
                        UiModeManagerService.this.mHandler.sendEmptyMessage(2);
                while (true)
                {
                    return;
                    UiModeManagerService.this.mHandler.sendEmptyMessage(2);
                }
            }
        };
        this.mEmptyLocationListener = new LocationListener()
        {
            public void onLocationChanged(Location paramAnonymousLocation)
            {
            }

            public void onProviderDisabled(String paramAnonymousString)
            {
            }

            public void onProviderEnabled(String paramAnonymousString)
            {
            }

            public void onStatusChanged(String paramAnonymousString, int paramAnonymousInt, Bundle paramAnonymousBundle)
            {
            }
        };
        this.mLocationListener = new LocationListener()
        {
            private boolean hasMoved(Location paramAnonymousLocation)
            {
                boolean bool1 = true;
                boolean bool2 = false;
                if (paramAnonymousLocation == null);
                do
                    while (true)
                    {
                        return bool2;
                        if (UiModeManagerService.this.mLocation != null)
                            break;
                        bool2 = bool1;
                    }
                while (paramAnonymousLocation.getTime() < UiModeManagerService.this.mLocation.getTime());
                if (UiModeManagerService.this.mLocation.distanceTo(paramAnonymousLocation) >= UiModeManagerService.this.mLocation.getAccuracy() + paramAnonymousLocation.getAccuracy());
                while (true)
                {
                    bool2 = bool1;
                    break;
                    bool1 = false;
                }
            }

            public void onLocationChanged(Location paramAnonymousLocation)
            {
                int i = 0;
                boolean bool = hasMoved(paramAnonymousLocation);
                if ((UiModeManagerService.this.mLocation == null) || (paramAnonymousLocation.getAccuracy() < UiModeManagerService.this.mLocation.getAccuracy()))
                    i = 1;
                if ((bool) || (i != 0))
                    synchronized (UiModeManagerService.this.mLock)
                    {
                        UiModeManagerService.access$702(UiModeManagerService.this, paramAnonymousLocation);
                        if ((bool) && (UiModeManagerService.this.isDoingNightMode()) && (UiModeManagerService.this.mNightMode == 0))
                            UiModeManagerService.this.mHandler.sendEmptyMessage(0);
                    }
            }

            public void onProviderDisabled(String paramAnonymousString)
            {
            }

            public void onProviderEnabled(String paramAnonymousString)
            {
            }

            public void onStatusChanged(String paramAnonymousString, int paramAnonymousInt, Bundle paramAnonymousBundle)
            {
            }
        };
        this.mHandler = new Handler()
        {
            boolean mDidFirstInit;
            long mLastNetworkRegisterTime = -1800000L;
            boolean mNetworkListenerEnabled;
            boolean mPassiveListenerEnabled;

            private void retrieveLocation()
            {
                Object localObject1 = null;
                Iterator localIterator = UiModeManagerService.this.mLocationManager.getProviders(new Criteria(), true).iterator();
                while (localIterator.hasNext())
                {
                    Location localLocation = UiModeManagerService.this.mLocationManager.getLastKnownLocation((String)localIterator.next());
                    if ((localObject1 == null) || ((localLocation != null) && (((Location)localObject1).getTime() < localLocation.getTime())))
                        localObject1 = localLocation;
                }
                long l;
                if (localObject1 == null)
                {
                    Time localTime = new Time();
                    localTime.set(System.currentTimeMillis());
                    l = localTime.gmtoff;
                    if (localTime.isDst <= 0)
                        break label201;
                }
                label201: for (int i = 3600; ; i = 0)
                {
                    double d = 0.004166666666666667D * (l - i);
                    localObject1 = new Location("fake");
                    ((Location)localObject1).setLongitude(d);
                    ((Location)localObject1).setLatitude(0.0D);
                    ((Location)localObject1).setAccuracy(417000.0F);
                    ((Location)localObject1).setTime(System.currentTimeMillis());
                    synchronized (UiModeManagerService.this.mLock)
                    {
                        UiModeManagerService.access$702(UiModeManagerService.this, (Location)localObject1);
                        return;
                    }
                }
            }

            // ERROR //
            public void handleMessage(android.os.Message paramAnonymousMessage)
            {
                // Byte code:
                //     0: aload_1
                //     1: getfield 125	android/os/Message:what	I
                //     4: tableswitch	default:+28 -> 32, 0:+29->33, 1:+145->149, 2:+101->105
                //     33: aload_0
                //     34: getfield 18	com/android/server/UiModeManagerService$8:this$0	Lcom/android/server/UiModeManagerService;
                //     37: getfield 112	com/android/server/UiModeManagerService:mLock	Ljava/lang/Object;
                //     40: astore 15
                //     42: aload 15
                //     44: monitorenter
                //     45: aload_0
                //     46: getfield 18	com/android/server/UiModeManagerService$8:this$0	Lcom/android/server/UiModeManagerService;
                //     49: invokevirtual 128	com/android/server/UiModeManagerService:isDoingNightMode	()Z
                //     52: ifeq +39 -> 91
                //     55: aload_0
                //     56: getfield 18	com/android/server/UiModeManagerService$8:this$0	Lcom/android/server/UiModeManagerService;
                //     59: invokestatic 132	com/android/server/UiModeManagerService:access$700	(Lcom/android/server/UiModeManagerService;)Landroid/location/Location;
                //     62: ifnull +29 -> 91
                //     65: aload_0
                //     66: getfield 18	com/android/server/UiModeManagerService$8:this$0	Lcom/android/server/UiModeManagerService;
                //     69: invokestatic 136	com/android/server/UiModeManagerService:access$300	(Lcom/android/server/UiModeManagerService;)I
                //     72: ifne +19 -> 91
                //     75: aload_0
                //     76: getfield 18	com/android/server/UiModeManagerService$8:this$0	Lcom/android/server/UiModeManagerService;
                //     79: invokevirtual 139	com/android/server/UiModeManagerService:updateTwilightLocked	()V
                //     82: aload_0
                //     83: getfield 18	com/android/server/UiModeManagerService$8:this$0	Lcom/android/server/UiModeManagerService;
                //     86: iconst_0
                //     87: iconst_0
                //     88: invokevirtual 143	com/android/server/UiModeManagerService:updateLocked	(II)V
                //     91: aload 15
                //     93: monitorexit
                //     94: goto -62 -> 32
                //     97: astore 16
                //     99: aload 15
                //     101: monitorexit
                //     102: aload 16
                //     104: athrow
                //     105: aload_0
                //     106: getfield 145	com/android/server/UiModeManagerService$8:mNetworkListenerEnabled	Z
                //     109: ifeq -77 -> 32
                //     112: ldc2_w 146
                //     115: aload_0
                //     116: getfield 25	com/android/server/UiModeManagerService$8:mLastNetworkRegisterTime	J
                //     119: ladd
                //     120: invokestatic 152	android/os/SystemClock:elapsedRealtime	()J
                //     123: lcmp
                //     124: ifge -92 -> 32
                //     127: aload_0
                //     128: iconst_0
                //     129: putfield 145	com/android/server/UiModeManagerService$8:mNetworkListenerEnabled	Z
                //     132: aload_0
                //     133: getfield 18	com/android/server/UiModeManagerService$8:this$0	Lcom/android/server/UiModeManagerService;
                //     136: invokestatic 30	com/android/server/UiModeManagerService:access$900	(Lcom/android/server/UiModeManagerService;)Landroid/location/LocationManager;
                //     139: aload_0
                //     140: getfield 18	com/android/server/UiModeManagerService$8:this$0	Lcom/android/server/UiModeManagerService;
                //     143: invokestatic 156	com/android/server/UiModeManagerService:access$800	(Lcom/android/server/UiModeManagerService;)Landroid/location/LocationListener;
                //     146: invokevirtual 160	android/location/LocationManager:removeUpdates	(Landroid/location/LocationListener;)V
                //     149: aload_0
                //     150: getfield 18	com/android/server/UiModeManagerService$8:this$0	Lcom/android/server/UiModeManagerService;
                //     153: invokestatic 30	com/android/server/UiModeManagerService:access$900	(Lcom/android/server/UiModeManagerService;)Landroid/location/LocationManager;
                //     156: ldc 162
                //     158: invokevirtual 166	android/location/LocationManager:isProviderEnabled	(Ljava/lang/String;)Z
                //     161: istore 14
                //     163: iload 14
                //     165: istore_3
                //     166: aload_0
                //     167: getfield 145	com/android/server/UiModeManagerService$8:mNetworkListenerEnabled	Z
                //     170: ifne +129 -> 299
                //     173: iload_3
                //     174: ifeq +125 -> 299
                //     177: aload_0
                //     178: iconst_1
                //     179: putfield 145	com/android/server/UiModeManagerService$8:mNetworkListenerEnabled	Z
                //     182: aload_0
                //     183: invokestatic 152	android/os/SystemClock:elapsedRealtime	()J
                //     186: putfield 25	com/android/server/UiModeManagerService$8:mLastNetworkRegisterTime	J
                //     189: aload_0
                //     190: getfield 18	com/android/server/UiModeManagerService$8:this$0	Lcom/android/server/UiModeManagerService;
                //     193: invokestatic 30	com/android/server/UiModeManagerService:access$900	(Lcom/android/server/UiModeManagerService;)Landroid/location/LocationManager;
                //     196: ldc 162
                //     198: ldc2_w 167
                //     201: fconst_0
                //     202: aload_0
                //     203: getfield 18	com/android/server/UiModeManagerService$8:this$0	Lcom/android/server/UiModeManagerService;
                //     206: invokestatic 156	com/android/server/UiModeManagerService:access$800	(Lcom/android/server/UiModeManagerService;)Landroid/location/LocationListener;
                //     209: invokevirtual 172	android/location/LocationManager:requestLocationUpdates	(Ljava/lang/String;JFLandroid/location/LocationListener;)V
                //     212: aload_0
                //     213: getfield 174	com/android/server/UiModeManagerService$8:mDidFirstInit	Z
                //     216: ifne +83 -> 299
                //     219: aload_0
                //     220: iconst_1
                //     221: putfield 174	com/android/server/UiModeManagerService$8:mDidFirstInit	Z
                //     224: aload_0
                //     225: getfield 18	com/android/server/UiModeManagerService$8:this$0	Lcom/android/server/UiModeManagerService;
                //     228: invokestatic 132	com/android/server/UiModeManagerService:access$700	(Lcom/android/server/UiModeManagerService;)Landroid/location/Location;
                //     231: ifnonnull +7 -> 238
                //     234: aload_0
                //     235: invokespecial 176	com/android/server/UiModeManagerService$8:retrieveLocation	()V
                //     238: aload_0
                //     239: getfield 18	com/android/server/UiModeManagerService$8:this$0	Lcom/android/server/UiModeManagerService;
                //     242: getfield 112	com/android/server/UiModeManagerService:mLock	Ljava/lang/Object;
                //     245: astore 12
                //     247: aload 12
                //     249: monitorenter
                //     250: aload_0
                //     251: getfield 18	com/android/server/UiModeManagerService$8:this$0	Lcom/android/server/UiModeManagerService;
                //     254: invokevirtual 128	com/android/server/UiModeManagerService:isDoingNightMode	()Z
                //     257: ifeq +39 -> 296
                //     260: aload_0
                //     261: getfield 18	com/android/server/UiModeManagerService$8:this$0	Lcom/android/server/UiModeManagerService;
                //     264: invokestatic 132	com/android/server/UiModeManagerService:access$700	(Lcom/android/server/UiModeManagerService;)Landroid/location/Location;
                //     267: ifnull +29 -> 296
                //     270: aload_0
                //     271: getfield 18	com/android/server/UiModeManagerService$8:this$0	Lcom/android/server/UiModeManagerService;
                //     274: invokestatic 136	com/android/server/UiModeManagerService:access$300	(Lcom/android/server/UiModeManagerService;)I
                //     277: ifne +19 -> 296
                //     280: aload_0
                //     281: getfield 18	com/android/server/UiModeManagerService$8:this$0	Lcom/android/server/UiModeManagerService;
                //     284: invokevirtual 139	com/android/server/UiModeManagerService:updateTwilightLocked	()V
                //     287: aload_0
                //     288: getfield 18	com/android/server/UiModeManagerService$8:this$0	Lcom/android/server/UiModeManagerService;
                //     291: iconst_0
                //     292: iconst_0
                //     293: invokevirtual 143	com/android/server/UiModeManagerService:updateLocked	(II)V
                //     296: aload 12
                //     298: monitorexit
                //     299: aload_0
                //     300: getfield 18	com/android/server/UiModeManagerService$8:this$0	Lcom/android/server/UiModeManagerService;
                //     303: invokestatic 30	com/android/server/UiModeManagerService:access$900	(Lcom/android/server/UiModeManagerService;)Landroid/location/LocationManager;
                //     306: ldc 178
                //     308: invokevirtual 166	android/location/LocationManager:isProviderEnabled	(Ljava/lang/String;)Z
                //     311: istore 11
                //     313: iload 11
                //     315: istore 5
                //     317: aload_0
                //     318: getfield 180	com/android/server/UiModeManagerService$8:mPassiveListenerEnabled	Z
                //     321: ifne +35 -> 356
                //     324: iload 5
                //     326: ifeq +30 -> 356
                //     329: aload_0
                //     330: iconst_1
                //     331: putfield 180	com/android/server/UiModeManagerService$8:mPassiveListenerEnabled	Z
                //     334: aload_0
                //     335: getfield 18	com/android/server/UiModeManagerService$8:this$0	Lcom/android/server/UiModeManagerService;
                //     338: invokestatic 30	com/android/server/UiModeManagerService:access$900	(Lcom/android/server/UiModeManagerService;)Landroid/location/LocationManager;
                //     341: ldc 178
                //     343: lconst_0
                //     344: ldc 181
                //     346: aload_0
                //     347: getfield 18	com/android/server/UiModeManagerService$8:this$0	Lcom/android/server/UiModeManagerService;
                //     350: invokestatic 184	com/android/server/UiModeManagerService:access$1000	(Lcom/android/server/UiModeManagerService;)Landroid/location/LocationListener;
                //     353: invokevirtual 172	android/location/LocationManager:requestLocationUpdates	(Ljava/lang/String;JFLandroid/location/LocationListener;)V
                //     356: aload_0
                //     357: getfield 145	com/android/server/UiModeManagerService$8:mNetworkListenerEnabled	Z
                //     360: ifeq +10 -> 370
                //     363: aload_0
                //     364: getfield 180	com/android/server/UiModeManagerService$8:mPassiveListenerEnabled	Z
                //     367: ifne -335 -> 32
                //     370: ldc2_w 185
                //     373: aload_1
                //     374: invokevirtual 190	android/os/Message:getData	()Landroid/os/Bundle;
                //     377: ldc 192
                //     379: invokevirtual 198	android/os/Bundle:getLong	(Ljava/lang/String;)J
                //     382: l2d
                //     383: dmul
                //     384: d2l
                //     385: lstore 6
                //     387: lload 6
                //     389: lconst_0
                //     390: lcmp
                //     391: ifne +86 -> 477
                //     394: ldc2_w 199
                //     397: lstore 6
                //     399: new 194	android/os/Bundle
                //     402: dup
                //     403: invokespecial 201	android/os/Bundle:<init>	()V
                //     406: astore 8
                //     408: aload 8
                //     410: ldc 192
                //     412: lload 6
                //     414: invokevirtual 205	android/os/Bundle:putLong	(Ljava/lang/String;J)V
                //     417: aload_0
                //     418: getfield 18	com/android/server/UiModeManagerService$8:this$0	Lcom/android/server/UiModeManagerService;
                //     421: invokestatic 209	com/android/server/UiModeManagerService:access$400	(Lcom/android/server/UiModeManagerService;)Landroid/os/Handler;
                //     424: iconst_1
                //     425: invokevirtual 213	android/os/Handler:obtainMessage	(I)Landroid/os/Message;
                //     428: astore 9
                //     430: aload 9
                //     432: aload 8
                //     434: invokevirtual 217	android/os/Message:setData	(Landroid/os/Bundle;)V
                //     437: aload_0
                //     438: getfield 18	com/android/server/UiModeManagerService$8:this$0	Lcom/android/server/UiModeManagerService;
                //     441: invokestatic 209	com/android/server/UiModeManagerService:access$400	(Lcom/android/server/UiModeManagerService;)Landroid/os/Handler;
                //     444: aload 9
                //     446: lload 6
                //     448: invokevirtual 221	android/os/Handler:sendMessageDelayed	(Landroid/os/Message;J)Z
                //     451: pop
                //     452: goto -420 -> 32
                //     455: astore_2
                //     456: iconst_0
                //     457: istore_3
                //     458: goto -292 -> 166
                //     461: astore 13
                //     463: aload 12
                //     465: monitorexit
                //     466: aload 13
                //     468: athrow
                //     469: astore 4
                //     471: iconst_0
                //     472: istore 5
                //     474: goto -157 -> 317
                //     477: lload 6
                //     479: ldc2_w 222
                //     482: lcmp
                //     483: ifle -84 -> 399
                //     486: ldc2_w 222
                //     489: lstore 6
                //     491: goto -92 -> 399
                //
                // Exception table:
                //     from	to	target	type
                //     45	102	97	finally
                //     149	163	455	java/lang/Exception
                //     250	299	461	finally
                //     463	466	461	finally
                //     299	313	469	java/lang/Exception
            }
        };
        this.mContext = paramContext;
        ServiceManager.addService("uimode", this);
        this.mAlarmManager = ((AlarmManager)this.mContext.getSystemService("alarm"));
        this.mLocationManager = ((LocationManager)this.mContext.getSystemService("location"));
        this.mContext.registerReceiver(this.mTwilightUpdateReceiver, new IntentFilter("com.android.server.action.UPDATE_NIGHT_MODE"));
        this.mContext.registerReceiver(this.mDockModeReceiver, new IntentFilter("android.intent.action.DOCK_EVENT"));
        this.mContext.registerReceiver(this.mBatteryReceiver, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        IntentFilter localIntentFilter = new IntentFilter("android.intent.action.AIRPLANE_MODE");
        localIntentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
        this.mContext.registerReceiver(this.mUpdateLocationReceiver, localIntentFilter);
        Injector.registerUIModeScaleChangedOjbserver(this, this.mContext);
        this.mWakeLock = ((PowerManager)paramContext.getSystemService("power")).newWakeLock(26, TAG);
        this.mConfiguration.setToDefaults();
        this.mDefaultUiModeType = paramContext.getResources().getInteger(17694737);
        int j;
        if (paramContext.getResources().getInteger(17694739) == i)
        {
            j = i;
            this.mCarModeKeepsScreenOn = j;
            if (paramContext.getResources().getInteger(17694738) != i)
                break label437;
        }
        while (true)
        {
            this.mDeskModeKeepsScreenOn = i;
            this.mTelevision = paramContext.getPackageManager().hasSystemFeature("android.hardware.type.television");
            this.mNightMode = Settings.Secure.getInt(this.mContext.getContentResolver(), "ui_night_mode", 0);
            return;
            j = 0;
            break;
            label437: i = 0;
        }
    }

    private void adjustStatusBarCarModeLocked()
    {
        if (this.mStatusBarManager == null)
            this.mStatusBarManager = ((StatusBarManager)this.mContext.getSystemService("statusbar"));
        int i;
        if (this.mStatusBarManager != null)
        {
            StatusBarManager localStatusBarManager = this.mStatusBarManager;
            if (this.mCarModeEnabled)
            {
                i = 524288;
                localStatusBarManager.disable(i);
            }
        }
        else
        {
            if (this.mNotificationManager == null)
                this.mNotificationManager = ((NotificationManager)this.mContext.getSystemService("notification"));
            if (this.mNotificationManager != null)
            {
                if (!this.mCarModeEnabled)
                    break label191;
                Intent localIntent = new Intent(this.mContext, DisableCarModeActivity.class);
                Notification localNotification = new Notification();
                localNotification.icon = 17302799;
                localNotification.defaults = 4;
                localNotification.flags = 2;
                localNotification.when = 0L;
                localNotification.setLatestEventInfo(this.mContext, this.mContext.getString(17040515), this.mContext.getString(17040516), PendingIntent.getActivity(this.mContext, 0, localIntent, 0));
                this.mNotificationManager.notify(0, localNotification);
            }
        }
        while (true)
        {
            return;
            i = 0;
            break;
            label191: this.mNotificationManager.cancel(0);
        }
    }

    static Intent buildHomeIntent(String paramString)
    {
        Intent localIntent = new Intent("android.intent.action.MAIN");
        localIntent.addCategory(paramString);
        localIntent.setFlags(270532608);
        return localIntent;
    }

    static final boolean isDeskDockState(int paramInt)
    {
        switch (paramInt)
        {
        case 2:
        default:
        case 1:
        case 3:
        case 4:
        }
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    public void disableCarMode(int paramInt)
    {
        synchronized (this.mLock)
        {
            setCarModeLocked(false);
            if (this.mSystemReady)
                updateLocked(0, paramInt);
            return;
        }
    }

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.DUMP") != 0)
            paramPrintWriter.println("Permission Denial: can't dump uimode service from from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid());
        while (true)
        {
            return;
            synchronized (this.mLock)
            {
                paramPrintWriter.println("Current UI Mode Service state:");
                paramPrintWriter.print("    mDockState=");
                paramPrintWriter.print(this.mDockState);
                paramPrintWriter.print(" mLastBroadcastState=");
                paramPrintWriter.println(this.mLastBroadcastState);
                paramPrintWriter.print("    mNightMode=");
                paramPrintWriter.print(this.mNightMode);
                paramPrintWriter.print(" mCarModeEnabled=");
                paramPrintWriter.print(this.mCarModeEnabled);
                paramPrintWriter.print(" mComputedNightMode=");
                paramPrintWriter.println(this.mComputedNightMode);
                paramPrintWriter.print("    mCurUiMode=0x");
                paramPrintWriter.print(Integer.toHexString(this.mCurUiMode));
                paramPrintWriter.print(" mSetUiMode=0x");
                paramPrintWriter.println(Integer.toHexString(this.mSetUiMode));
                paramPrintWriter.print("    mHoldingConfiguration=");
                paramPrintWriter.print(this.mHoldingConfiguration);
                paramPrintWriter.print(" mSystemReady=");
                paramPrintWriter.println(this.mSystemReady);
                if (this.mLocation != null)
                {
                    paramPrintWriter.print("    mLocation=");
                    paramPrintWriter.println(this.mLocation);
                }
            }
        }
    }

    public void enableCarMode(int paramInt)
    {
        synchronized (this.mLock)
        {
            setCarModeLocked(true);
            if (this.mSystemReady)
                updateLocked(paramInt, 0);
            return;
        }
    }

    public int getCurrentModeType()
    {
        synchronized (this.mLock)
        {
            int i = 0xF & this.mCurUiMode;
            return i;
        }
    }

    public int getNightMode()
        throws RemoteException
    {
        return this.mNightMode;
    }

    boolean isDoingNightMode()
    {
        if ((this.mCarModeEnabled) || (this.mDockState != 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    void setCarModeLocked(boolean paramBoolean)
    {
        if (this.mCarModeEnabled != paramBoolean)
            this.mCarModeEnabled = paramBoolean;
    }

    // ERROR //
    public void setNightMode(int paramInt)
        throws RemoteException
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 134	com/android/server/UiModeManagerService:mLock	Ljava/lang/Object;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: iload_1
        //     8: tableswitch	default:+28 -> 36, 0:+61->69, 1:+61->69, 2:+61->69
        //     37: aconst_null
        //     38: <illegal opcode>
        //     39: dup
        //     40: new 415	java/lang/StringBuilder
        //     43: dup
        //     44: invokespecial 416	java/lang/StringBuilder:<init>	()V
        //     47: ldc_w 499
        //     50: invokevirtual 422	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     53: iload_1
        //     54: invokevirtual 431	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     57: invokevirtual 439	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     60: invokespecial 500	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     63: athrow
        //     64: astore_3
        //     65: aload_2
        //     66: monitorexit
        //     67: aload_3
        //     68: athrow
        //     69: aload_0
        //     70: invokevirtual 502	com/android/server/UiModeManagerService:isDoingNightMode	()Z
        //     73: ifne +8 -> 81
        //     76: aload_2
        //     77: monitorexit
        //     78: goto +49 -> 127
        //     81: aload_0
        //     82: getfield 140	com/android/server/UiModeManagerService:mNightMode	I
        //     85: iload_1
        //     86: if_icmpeq +39 -> 125
        //     89: invokestatic 506	android/os/Binder:clearCallingIdentity	()J
        //     92: lstore 4
        //     94: aload_0
        //     95: getfield 183	com/android/server/UiModeManagerService:mContext	Landroid/content/Context;
        //     98: invokevirtual 283	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
        //     101: ldc_w 285
        //     104: iload_1
        //     105: invokestatic 510	android/provider/Settings$Secure:putInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
        //     108: pop
        //     109: lload 4
        //     111: invokestatic 514	android/os/Binder:restoreCallingIdentity	(J)V
        //     114: aload_0
        //     115: iload_1
        //     116: putfield 140	com/android/server/UiModeManagerService:mNightMode	I
        //     119: aload_0
        //     120: iconst_0
        //     121: iconst_0
        //     122: invokevirtual 405	com/android/server/UiModeManagerService:updateLocked	(II)V
        //     125: aload_2
        //     126: monitorexit
        //     127: return
        //
        // Exception table:
        //     from	to	target	type
        //     36	67	64	finally
        //     69	127	64	finally
    }

    void systemReady()
    {
        for (boolean bool = true; ; bool = false)
            synchronized (this.mLock)
            {
                this.mSystemReady = true;
                if (this.mDockState == 2)
                {
                    this.mCarModeEnabled = bool;
                    updateLocked(0, 0);
                    this.mHandler.sendEmptyMessage(1);
                    return;
                }
            }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    final void updateConfigurationLocked(boolean paramBoolean)
    {
        int i;
        if (this.mTelevision)
            i = 4;
        while (true)
        {
            label18: int k;
            label47: int j;
            if (this.mCarModeEnabled)
            {
                i = 3;
                if (!this.mCarModeEnabled)
                    break label144;
                if (this.mNightMode != 0)
                    break label132;
                updateTwilightLocked();
                if (!this.mComputedNightMode)
                    break label125;
                k = 32;
                j = i | k;
                label52: this.mCurUiMode = j;
                if ((!this.mHoldingConfiguration) && (j != this.mSetUiMode))
                {
                    this.mSetUiMode = j;
                    this.mConfiguration.uiMode = j;
                    if (!paramBoolean);
                }
            }
            try
            {
                ActivityManagerNative.getDefault().updateConfiguration(this.mConfiguration);
                return;
                i = this.mNormalType;
                continue;
                if (!isDeskDockState(this.mDockState))
                    break label18;
                i = 2;
                break label18;
                label125: k = 16;
                break label47;
                label132: j = i | this.mNightMode << 4;
                break label52;
                label144: j = 0x10 | i & 0xFFFFFFCF;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Slog.w(TAG, "Failure communicating with activity manager", localRemoteException);
            }
        }
    }

    void updateDockState(int paramInt)
    {
        for (boolean bool = true; ; bool = false)
            synchronized (this.mLock)
            {
                if (paramInt != this.mDockState)
                {
                    this.mDockState = paramInt;
                    if (this.mDockState != 2)
                        continue;
                    setCarModeLocked(bool);
                    if (this.mSystemReady)
                        updateLocked(1, 0);
                }
                return;
            }
    }

    final void updateLocked(int paramInt1, int paramInt2)
    {
        long l = Binder.clearCallingIdentity();
        Object localObject1 = null;
        String str = null;
        while (true)
        {
            try
            {
                if (this.mLastBroadcastState == 2)
                {
                    adjustStatusBarCarModeLocked();
                    str = UiModeManager.ACTION_EXIT_CAR_MODE;
                    if (this.mCarModeEnabled)
                    {
                        if (this.mLastBroadcastState != 2)
                        {
                            adjustStatusBarCarModeLocked();
                            if (str != null)
                                this.mContext.sendBroadcast(new Intent(str));
                            this.mLastBroadcastState = 2;
                            localObject1 = UiModeManager.ACTION_ENTER_CAR_MODE;
                        }
                        if (localObject1 == null)
                            continue;
                        Intent localIntent1 = new Intent((String)localObject1);
                        localIntent1.putExtra("enableFlags", paramInt1);
                        localIntent1.putExtra("disableFlags", paramInt2);
                        this.mContext.sendOrderedBroadcast(localIntent1, null, this.mResultReceiver, null, -1, null, null);
                        this.mHoldingConfiguration = true;
                        updateConfigurationLocked(true);
                        if (!this.mCharging)
                            break label416;
                        if ((this.mCarModeEnabled) && (this.mCarModeKeepsScreenOn))
                            break label410;
                        if ((this.mCurUiMode != 2) || (!this.mDeskModeKeepsScreenOn))
                            break label416;
                        break label410;
                        if (i != this.mWakeLock.isHeld())
                        {
                            if (i == 0)
                                continue;
                            this.mWakeLock.acquire();
                        }
                    }
                }
                else
                {
                    if (!isDeskDockState(this.mLastBroadcastState))
                        continue;
                    str = UiModeManager.ACTION_EXIT_DESK_MODE;
                    continue;
                }
                if (isDeskDockState(this.mDockState))
                {
                    if (isDeskDockState(this.mLastBroadcastState))
                        continue;
                    if (str != null)
                        this.mContext.sendBroadcast(new Intent(str));
                    this.mLastBroadcastState = this.mDockState;
                    localObject1 = UiModeManager.ACTION_ENTER_DESK_MODE;
                    continue;
                }
                this.mLastBroadcastState = 0;
                localObject1 = str;
                continue;
                Object localObject3 = null;
                if (this.mCarModeEnabled)
                {
                    if ((paramInt1 & 0x1) != 0)
                    {
                        Intent localIntent2 = buildHomeIntent("android.intent.category.CAR_DOCK");
                        localObject3 = localIntent2;
                    }
                    if (localObject3 == null)
                        continue;
                    try
                    {
                        this.mContext.startActivity((Intent)localObject3);
                    }
                    catch (ActivityNotFoundException localActivityNotFoundException)
                    {
                    }
                    continue;
                }
                if (isDeskDockState(this.mDockState))
                {
                    if ((paramInt1 & 0x1) == 0)
                        continue;
                    localObject3 = buildHomeIntent("android.intent.category.DESK_DOCK");
                    continue;
                }
                if ((paramInt2 & 0x1) == 0)
                    continue;
                localObject3 = buildHomeIntent("android.intent.category.HOME");
                continue;
                continue;
                this.mWakeLock.release();
                continue;
            }
            finally
            {
                Binder.restoreCallingIdentity(l);
            }
            label410: int i = 1;
            continue;
            label416: i = 0;
        }
    }

    void updateTwilightLocked()
    {
        if (this.mLocation == null)
            return;
        long l1 = System.currentTimeMillis();
        TwilightCalculator localTwilightCalculator = new TwilightCalculator();
        localTwilightCalculator.calculateTwilight(l1, this.mLocation.getLatitude(), this.mLocation.getLongitude());
        boolean bool;
        label49: long l2;
        if (localTwilightCalculator.mState == 0)
        {
            bool = false;
            if ((localTwilightCalculator.mSunrise != -1L) && (localTwilightCalculator.mSunset != -1L))
                break label138;
            l2 = l1 + 43200000L;
        }
        while (true)
        {
            Intent localIntent = new Intent("com.android.server.action.UPDATE_NIGHT_MODE");
            PendingIntent localPendingIntent = PendingIntent.getBroadcast(this.mContext, 0, localIntent, 0);
            this.mAlarmManager.cancel(localPendingIntent);
            this.mAlarmManager.set(0, l2, localPendingIntent);
            this.mComputedNightMode = bool;
            break;
            bool = true;
            break label49;
            label138: int i = localTwilightCalculator.mState;
            long l3 = 0L + 60000L;
            if (l1 > localTwilightCalculator.mSunset)
                localTwilightCalculator.calculateTwilight(l1 + 86400000L, this.mLocation.getLatitude(), this.mLocation.getLongitude());
            if (i == 1)
                l2 = l3 + localTwilightCalculator.mSunrise;
            else
                l2 = l3 + localTwilightCalculator.mSunset;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static void registerUIModeScaleChangedOjbserver(final UiModeManagerService paramUiModeManagerService, final Context paramContext)
        {
            ContentObserver local1 = new ContentObserver(new Handler())
            {
                public void onChange(boolean paramAnonymousBoolean)
                {
                    synchronized (paramUiModeManagerService.mLock)
                    {
                        paramUiModeManagerService.mNormalType = Settings.System.getInt(paramContext.getContentResolver(), "ui_mode_scale", 1);
                        return;
                    }
                }
            };
            paramContext.getContentResolver().registerContentObserver(Settings.System.getUriFor("ui_mode_scale"), false, local1);
            local1.onChange(false);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.UiModeManagerService
 * JD-Core Version:        0.6.2
 */