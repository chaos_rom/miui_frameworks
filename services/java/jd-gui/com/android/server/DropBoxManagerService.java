package com.android.server;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.DropBoxManager.Entry;
import android.os.Handler;
import android.os.Message;
import android.os.StatFs;
import android.os.SystemClock;
import android.provider.Settings.Secure;
import android.util.Slog;
import com.android.internal.os.IDropBoxManagerService.Stub;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

public final class DropBoxManagerService extends IDropBoxManagerService.Stub
{
    private static final int DEFAULT_AGE_SECONDS = 259200;
    private static final int DEFAULT_MAX_FILES = 1000;
    private static final int DEFAULT_QUOTA_KB = 5120;
    private static final int DEFAULT_QUOTA_PERCENT = 10;
    private static final int DEFAULT_RESERVE_PERCENT = 10;
    private static final int MSG_SEND_BROADCAST = 1;
    private static final boolean PROFILE_DUMP = false;
    private static final int QUOTA_RESCAN_MILLIS = 5000;
    private static final String TAG = "DropBoxManagerService";
    private FileList mAllFiles = null;
    private int mBlockSize = 0;
    private volatile boolean mBooted = false;
    private int mCachedQuotaBlocks = 0;
    private long mCachedQuotaUptimeMillis = 0L;
    private final ContentResolver mContentResolver;
    private final Context mContext;
    private final File mDropBoxDir;
    private HashMap<String, FileList> mFilesByTag = null;
    private final Handler mHandler;
    private final BroadcastReceiver mReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            if ((paramAnonymousIntent != null) && ("android.intent.action.BOOT_COMPLETED".equals(paramAnonymousIntent.getAction())))
                DropBoxManagerService.access$002(DropBoxManagerService.this, true);
            while (true)
            {
                return;
                DropBoxManagerService.access$102(DropBoxManagerService.this, 0L);
                new Thread()
                {
                    public void run()
                    {
                        try
                        {
                            DropBoxManagerService.this.init();
                            DropBoxManagerService.this.trimToFit();
                            return;
                        }
                        catch (IOException localIOException)
                        {
                            while (true)
                                Slog.e("DropBoxManagerService", "Can't init", localIOException);
                        }
                    }
                }
                .start();
            }
        }
    };
    private StatFs mStatFs = null;

    public DropBoxManagerService(final Context paramContext, File paramFile)
    {
        this.mDropBoxDir = paramFile;
        this.mContext = paramContext;
        this.mContentResolver = paramContext.getContentResolver();
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("android.intent.action.DEVICE_STORAGE_LOW");
        localIntentFilter.addAction("android.intent.action.BOOT_COMPLETED");
        paramContext.registerReceiver(this.mReceiver, localIntentFilter);
        this.mContentResolver.registerContentObserver(Settings.Secure.CONTENT_URI, true, new ContentObserver(new Handler())
        {
            public void onChange(boolean paramAnonymousBoolean)
            {
                DropBoxManagerService.this.mReceiver.onReceive(paramContext, (Intent)null);
            }
        });
        this.mHandler = new Handler()
        {
            public void handleMessage(Message paramAnonymousMessage)
            {
                if (paramAnonymousMessage.what == 1)
                    DropBoxManagerService.this.mContext.sendBroadcast((Intent)paramAnonymousMessage.obj, "android.permission.READ_LOGS");
            }
        };
    }

    /** @deprecated */
    private long createEntry(File paramFile, String paramString, int paramInt)
        throws IOException
    {
        while (true)
        {
            int j;
            try
            {
                long l1 = System.currentTimeMillis();
                SortedSet localSortedSet = this.mAllFiles.contents.tailSet(new EntryFile(10000L + l1));
                EntryFile[] arrayOfEntryFile1 = null;
                if (!localSortedSet.isEmpty())
                {
                    arrayOfEntryFile1 = (EntryFile[])localSortedSet.toArray(new EntryFile[localSortedSet.size()]);
                    localSortedSet.clear();
                }
                if (!this.mAllFiles.contents.isEmpty())
                    l1 = Math.max(l1, 1L + ((EntryFile)this.mAllFiles.contents.last()).timestampMillis);
                if (arrayOfEntryFile1 != null)
                {
                    EntryFile[] arrayOfEntryFile2 = arrayOfEntryFile1;
                    int i = arrayOfEntryFile2.length;
                    j = 0;
                    if (j < i)
                    {
                        EntryFile localEntryFile = arrayOfEntryFile2[j];
                        FileList localFileList1 = this.mAllFiles;
                        localFileList1.blocks -= localEntryFile.blocks;
                        FileList localFileList2 = (FileList)this.mFilesByTag.get(localEntryFile.tag);
                        if ((localFileList2 != null) && (localFileList2.contents.remove(localEntryFile)))
                            localFileList2.blocks -= localEntryFile.blocks;
                        if ((0x1 & localEntryFile.flags) == 0)
                        {
                            File localFile2 = localEntryFile.file;
                            File localFile3 = this.mDropBoxDir;
                            String str2 = localEntryFile.tag;
                            long l3 = l1 + 1L;
                            enrollEntry(new EntryFile(localFile2, localFile3, str2, l1, localEntryFile.flags, this.mBlockSize));
                            l1 = l3;
                            break label391;
                        }
                        File localFile1 = this.mDropBoxDir;
                        String str1 = localEntryFile.tag;
                        long l2 = l1 + 1L;
                        enrollEntry(new EntryFile(localFile1, str1, l1));
                        l1 = l2;
                        break label391;
                    }
                }
                if (paramFile == null)
                {
                    enrollEntry(new EntryFile(this.mDropBoxDir, paramString, l1));
                    return l1;
                }
                enrollEntry(new EntryFile(paramFile, this.mDropBoxDir, paramString, l1, paramInt, this.mBlockSize));
                continue;
            }
            finally
            {
            }
            label391: j++;
        }
    }

    /** @deprecated */
    private void enrollEntry(EntryFile paramEntryFile)
    {
        try
        {
            this.mAllFiles.contents.add(paramEntryFile);
            FileList localFileList1 = this.mAllFiles;
            localFileList1.blocks += paramEntryFile.blocks;
            if ((paramEntryFile.tag != null) && (paramEntryFile.file != null) && (paramEntryFile.blocks > 0))
            {
                FileList localFileList2 = (FileList)this.mFilesByTag.get(paramEntryFile.tag);
                if (localFileList2 == null)
                {
                    localFileList2 = new FileList(null);
                    this.mFilesByTag.put(paramEntryFile.tag, localFileList2);
                }
                localFileList2.contents.add(paramEntryFile);
                localFileList2.blocks += paramEntryFile.blocks;
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    private void init()
        throws IOException
    {
        try
        {
            if (this.mStatFs == null)
                if ((!this.mDropBoxDir.isDirectory()) && (!this.mDropBoxDir.mkdirs()))
                    throw new IOException("Can't mkdir: " + this.mDropBoxDir);
        }
        finally
        {
        }
        File[] arrayOfFile;
        try
        {
            this.mStatFs = new StatFs(this.mDropBoxDir.getPath());
            this.mBlockSize = this.mStatFs.getBlockSize();
            if (this.mAllFiles != null)
                break label376;
            arrayOfFile = this.mDropBoxDir.listFiles();
            if (arrayOfFile == null)
                throw new IOException("Can't list files: " + this.mDropBoxDir);
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            throw new IOException("Can't statfs: " + this.mDropBoxDir);
        }
        this.mAllFiles = new FileList(null);
        this.mFilesByTag = new HashMap();
        int i = arrayOfFile.length;
        for (int j = 0; ; j++)
            if (j < i)
            {
                File localFile = arrayOfFile[j];
                if (localFile.getName().endsWith(".tmp"))
                {
                    Slog.i("DropBoxManagerService", "Cleaning temp file: " + localFile);
                    localFile.delete();
                }
                else
                {
                    EntryFile localEntryFile = new EntryFile(localFile, this.mBlockSize);
                    if (localEntryFile.tag == null)
                    {
                        Slog.w("DropBoxManagerService", "Unrecognized file: " + localFile);
                    }
                    else if (localEntryFile.timestampMillis == 0L)
                    {
                        Slog.w("DropBoxManagerService", "Invalid filename: " + localFile);
                        localFile.delete();
                    }
                    else
                    {
                        enrollEntry(localEntryFile);
                    }
                }
            }
            else
            {
                label376: return;
            }
    }

    /** @deprecated */
    private long trimToFit()
    {
        while (true)
        {
            int n;
            int i1;
            FileList localFileList3;
            int i2;
            FileList localFileList1;
            try
            {
                int i = Settings.Secure.getInt(this.mContentResolver, "dropbox_age_seconds", 259200);
                int j = Settings.Secure.getInt(this.mContentResolver, "dropbox_max_files", 1000);
                long l1 = System.currentTimeMillis() - i * 1000;
                EntryFile localEntryFile2;
                if (!this.mAllFiles.contents.isEmpty())
                {
                    localEntryFile2 = (EntryFile)this.mAllFiles.contents.first();
                    if ((localEntryFile2.timestampMillis <= l1) || (this.mAllFiles.contents.size() >= j));
                }
                else
                {
                    long l2 = SystemClock.uptimeMillis();
                    if (l2 > 5000L + this.mCachedQuotaUptimeMillis)
                    {
                        int i3 = Settings.Secure.getInt(this.mContentResolver, "dropbox_quota_percent", 10);
                        int i4 = Settings.Secure.getInt(this.mContentResolver, "dropbox_reserve_percent", 10);
                        int i5 = Settings.Secure.getInt(this.mContentResolver, "dropbox_quota_kb", 5120);
                        this.mStatFs.restat(this.mDropBoxDir.getPath());
                        int i6 = this.mStatFs.getAvailableBlocks() - i4 * this.mStatFs.getBlockCount() / 100;
                        this.mCachedQuotaBlocks = Math.min(i5 * 1024 / this.mBlockSize, Math.max(0, i6 * i3 / 100));
                        this.mCachedQuotaUptimeMillis = l2;
                    }
                    if (this.mAllFiles.blocks > this.mCachedQuotaBlocks)
                    {
                        n = this.mAllFiles.blocks;
                        i1 = 0;
                        TreeSet localTreeSet = new TreeSet(this.mFilesByTag.values());
                        Iterator localIterator1 = localTreeSet.iterator();
                        if (localIterator1.hasNext())
                        {
                            localFileList3 = (FileList)localIterator1.next();
                            if ((i1 <= 0) || (localFileList3.blocks > (this.mCachedQuotaBlocks - n) / i1))
                                break label516;
                        }
                        i2 = (this.mCachedQuotaBlocks - n) / i1;
                        Iterator localIterator2 = localTreeSet.iterator();
                        if (localIterator2.hasNext())
                        {
                            localFileList1 = (FileList)localIterator2.next();
                            if (this.mAllFiles.blocks >= this.mCachedQuotaBlocks)
                                break label532;
                        }
                    }
                    int k = this.mCachedQuotaBlocks;
                    int m = this.mBlockSize;
                    long l3 = k * m;
                    return l3;
                }
                FileList localFileList4 = (FileList)this.mFilesByTag.get(localEntryFile2.tag);
                if ((localFileList4 != null) && (localFileList4.contents.remove(localEntryFile2)))
                    localFileList4.blocks -= localEntryFile2.blocks;
                if (this.mAllFiles.contents.remove(localEntryFile2))
                {
                    FileList localFileList5 = this.mAllFiles;
                    localFileList5.blocks -= localEntryFile2.blocks;
                }
                if (localEntryFile2.file == null)
                    continue;
                localEntryFile2.file.delete();
                continue;
            }
            finally
            {
            }
            label516: n -= localFileList3.blocks;
            i1++;
            continue;
            label532: 
            while ((localFileList1.blocks > i2) && (!localFileList1.contents.isEmpty()))
            {
                EntryFile localEntryFile1 = (EntryFile)localFileList1.contents.first();
                if (localFileList1.contents.remove(localEntryFile1))
                    localFileList1.blocks -= localEntryFile1.blocks;
                if (this.mAllFiles.contents.remove(localEntryFile1))
                {
                    FileList localFileList2 = this.mAllFiles;
                    localFileList2.blocks -= localEntryFile1.blocks;
                }
                try
                {
                    if (localEntryFile1.file != null)
                        localEntryFile1.file.delete();
                    enrollEntry(new EntryFile(this.mDropBoxDir, localEntryFile1.tag, localEntryFile1.timestampMillis));
                }
                catch (IOException localIOException)
                {
                    Slog.e("DropBoxManagerService", "Can't write tombstone file", localIOException);
                }
            }
        }
    }

    // ERROR //
    public void add(DropBoxManager.Entry paramEntry)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: aconst_null
        //     3: astore_3
        //     4: aload_1
        //     5: invokevirtual 398	android/os/DropBoxManager$Entry:getTag	()Ljava/lang/String;
        //     8: astore 4
        //     10: aload_1
        //     11: invokevirtual 401	android/os/DropBoxManager$Entry:getFlags	()I
        //     14: istore 12
        //     16: iload 12
        //     18: iconst_1
        //     19: iand
        //     20: ifeq +64 -> 84
        //     23: new 251	java/lang/IllegalArgumentException
        //     26: dup
        //     27: invokespecial 402	java/lang/IllegalArgumentException:<init>	()V
        //     30: athrow
        //     31: astore 8
        //     33: ldc 37
        //     35: new 261	java/lang/StringBuilder
        //     38: dup
        //     39: invokespecial 262	java/lang/StringBuilder:<init>	()V
        //     42: ldc_w 404
        //     45: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     48: aload 4
        //     50: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     53: invokevirtual 275	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     56: aload 8
        //     58: invokestatic 392	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     61: pop
        //     62: aload_3
        //     63: ifnull +7 -> 70
        //     66: aload_3
        //     67: invokevirtual 409	java/io/OutputStream:close	()V
        //     70: aload_1
        //     71: invokevirtual 410	android/os/DropBoxManager$Entry:close	()V
        //     74: aload_2
        //     75: ifnull +8 -> 83
        //     78: aload_2
        //     79: invokevirtual 317	java/io/File:delete	()Z
        //     82: pop
        //     83: return
        //     84: aload_0
        //     85: invokespecial 141	com/android/server/DropBoxManagerService:init	()V
        //     88: aload_0
        //     89: aload 4
        //     91: invokevirtual 413	com/android/server/DropBoxManagerService:isTagEnabled	(Ljava/lang/String;)Z
        //     94: istore 13
        //     96: iload 13
        //     98: ifne +20 -> 118
        //     101: iconst_0
        //     102: ifeq +5 -> 107
        //     105: aconst_null
        //     106: athrow
        //     107: aload_1
        //     108: invokevirtual 410	android/os/DropBoxManager$Entry:close	()V
        //     111: iconst_0
        //     112: ifeq -29 -> 83
        //     115: goto -37 -> 78
        //     118: aload_0
        //     119: invokespecial 147	com/android/server/DropBoxManagerService:trimToFit	()J
        //     122: lstore 14
        //     124: invokestatic 158	java/lang/System:currentTimeMillis	()J
        //     127: lstore 16
        //     129: aload_0
        //     130: getfield 72	com/android/server/DropBoxManagerService:mBlockSize	I
        //     133: newarray byte
        //     135: astore 18
        //     137: aload_1
        //     138: invokevirtual 417	android/os/DropBoxManager$Entry:getInputStream	()Ljava/io/InputStream;
        //     141: astore 19
        //     143: iconst_0
        //     144: istore 20
        //     146: aload 18
        //     148: arraylength
        //     149: istore 21
        //     151: iload 20
        //     153: iload 21
        //     155: if_icmpge +29 -> 184
        //     158: aload 18
        //     160: arraylength
        //     161: iload 20
        //     163: isub
        //     164: istore 40
        //     166: aload 19
        //     168: aload 18
        //     170: iload 20
        //     172: iload 40
        //     174: invokevirtual 423	java/io/InputStream:read	([BII)I
        //     177: istore 41
        //     179: iload 41
        //     181: ifgt +367 -> 548
        //     184: new 253	java/io/File
        //     187: dup
        //     188: aload_0
        //     189: getfield 85	com/android/server/DropBoxManagerService:mDropBoxDir	Ljava/io/File;
        //     192: new 261	java/lang/StringBuilder
        //     195: dup
        //     196: invokespecial 262	java/lang/StringBuilder:<init>	()V
        //     199: ldc_w 425
        //     202: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     205: invokestatic 431	java/lang/Thread:currentThread	()Ljava/lang/Thread;
        //     208: invokevirtual 434	java/lang/Thread:getId	()J
        //     211: invokevirtual 437	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     214: ldc_w 300
        //     217: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     220: invokevirtual 275	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     223: invokespecial 440	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     226: astore 22
        //     228: aload_0
        //     229: getfield 72	com/android/server/DropBoxManagerService:mBlockSize	I
        //     232: istore 23
        //     234: iload 23
        //     236: sipush 4096
        //     239: if_icmple +427 -> 666
        //     242: sipush 4096
        //     245: istore 23
        //     247: goto +419 -> 666
        //     250: new 442	java/io/FileOutputStream
        //     253: dup
        //     254: aload 22
        //     256: invokespecial 445	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
        //     259: astore 24
        //     261: new 447	java/io/BufferedOutputStream
        //     264: dup
        //     265: aload 24
        //     267: iload 23
        //     269: invokespecial 450	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;I)V
        //     272: astore 25
        //     274: aload 18
        //     276: arraylength
        //     277: istore 26
        //     279: iload 20
        //     281: iload 26
        //     283: if_icmpne +377 -> 660
        //     286: iload 12
        //     288: iconst_4
        //     289: iand
        //     290: ifne +370 -> 660
        //     293: new 452	java/util/zip/GZIPOutputStream
        //     296: dup
        //     297: aload 25
        //     299: invokespecial 455	java/util/zip/GZIPOutputStream:<init>	(Ljava/io/OutputStream;)V
        //     302: astore_3
        //     303: iload 12
        //     305: iconst_4
        //     306: ior
        //     307: istore 12
        //     309: aload_3
        //     310: aload 18
        //     312: iconst_0
        //     313: iload 20
        //     315: invokevirtual 459	java/io/OutputStream:write	([BII)V
        //     318: invokestatic 158	java/lang/System:currentTimeMillis	()J
        //     321: lstore 27
        //     323: lload 27
        //     325: lload 16
        //     327: lsub
        //     328: ldc2_w 460
        //     331: lcmp
        //     332: ifle +13 -> 345
        //     335: aload_0
        //     336: invokespecial 147	com/android/server/DropBoxManagerService:trimToFit	()J
        //     339: lstore 14
        //     341: lload 27
        //     343: lstore 16
        //     345: aload 19
        //     347: aload 18
        //     349: invokevirtual 464	java/io/InputStream:read	([B)I
        //     352: istore 20
        //     354: iload 20
        //     356: ifgt +202 -> 558
        //     359: aload 24
        //     361: invokestatic 470	android/os/FileUtils:sync	(Ljava/io/FileOutputStream;)Z
        //     364: pop
        //     365: aload_3
        //     366: invokevirtual 409	java/io/OutputStream:close	()V
        //     369: aconst_null
        //     370: astore_3
        //     371: aload 22
        //     373: invokevirtual 473	java/io/File:length	()J
        //     376: lload 14
        //     378: lcmp
        //     379: ifle +194 -> 573
        //     382: ldc 37
        //     384: new 261	java/lang/StringBuilder
        //     387: dup
        //     388: invokespecial 262	java/lang/StringBuilder:<init>	()V
        //     391: ldc_w 475
        //     394: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     397: aload 4
        //     399: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     402: ldc_w 477
        //     405: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     408: aload 22
        //     410: invokevirtual 473	java/io/File:length	()J
        //     413: invokevirtual 437	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     416: ldc_w 479
        //     419: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     422: lload 14
        //     424: invokevirtual 437	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     427: ldc_w 481
        //     430: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     433: invokevirtual 275	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     436: invokestatic 325	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     439: pop
        //     440: aload 22
        //     442: invokevirtual 317	java/io/File:delete	()Z
        //     445: pop
        //     446: aconst_null
        //     447: astore_2
        //     448: aload_0
        //     449: aload_2
        //     450: aload 4
        //     452: iload 12
        //     454: invokespecial 483	com/android/server/DropBoxManagerService:createEntry	(Ljava/io/File;Ljava/lang/String;I)J
        //     457: lstore 29
        //     459: aconst_null
        //     460: astore_2
        //     461: new 485	android/content/Intent
        //     464: dup
        //     465: ldc_w 487
        //     468: invokespecial 488	android/content/Intent:<init>	(Ljava/lang/String;)V
        //     471: astore 31
        //     473: aload 31
        //     475: ldc_w 489
        //     478: aload 4
        //     480: invokevirtual 493	android/content/Intent:putExtra	(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
        //     483: pop
        //     484: aload 31
        //     486: ldc_w 495
        //     489: lload 29
        //     491: invokevirtual 498	android/content/Intent:putExtra	(Ljava/lang/String;J)Landroid/content/Intent;
        //     494: pop
        //     495: aload_0
        //     496: getfield 78	com/android/server/DropBoxManagerService:mBooted	Z
        //     499: ifne +12 -> 511
        //     502: aload 31
        //     504: ldc_w 499
        //     507: invokevirtual 503	android/content/Intent:addFlags	(I)Landroid/content/Intent;
        //     510: pop
        //     511: aload_0
        //     512: getfield 131	com/android/server/DropBoxManagerService:mHandler	Landroid/os/Handler;
        //     515: aload_0
        //     516: getfield 131	com/android/server/DropBoxManagerService:mHandler	Landroid/os/Handler;
        //     519: iconst_1
        //     520: aload 31
        //     522: invokevirtual 507	android/os/Handler:obtainMessage	(ILjava/lang/Object;)Landroid/os/Message;
        //     525: invokevirtual 511	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
        //     528: pop
        //     529: aload_3
        //     530: ifnull +7 -> 537
        //     533: aload_3
        //     534: invokevirtual 409	java/io/OutputStream:close	()V
        //     537: aload_1
        //     538: invokevirtual 410	android/os/DropBoxManager$Entry:close	()V
        //     541: iconst_0
        //     542: ifeq -459 -> 83
        //     545: goto -467 -> 78
        //     548: iload 20
        //     550: iload 41
        //     552: iadd
        //     553: istore 20
        //     555: goto -409 -> 146
        //     558: aload_3
        //     559: invokevirtual 514	java/io/OutputStream:flush	()V
        //     562: goto -191 -> 371
        //     565: astore 8
        //     567: aload 22
        //     569: astore_2
        //     570: goto -537 -> 33
        //     573: iload 20
        //     575: ifgt -266 -> 309
        //     578: aload 22
        //     580: astore_2
        //     581: goto -133 -> 448
        //     584: astore 5
        //     586: aload_3
        //     587: ifnull +7 -> 594
        //     590: aload_3
        //     591: invokevirtual 409	java/io/OutputStream:close	()V
        //     594: aload_1
        //     595: invokevirtual 410	android/os/DropBoxManager$Entry:close	()V
        //     598: aload_2
        //     599: ifnull +8 -> 607
        //     602: aload_2
        //     603: invokevirtual 317	java/io/File:delete	()Z
        //     606: pop
        //     607: aload 5
        //     609: athrow
        //     610: astore 7
        //     612: goto -18 -> 594
        //     615: astore 11
        //     617: goto -547 -> 70
        //     620: astore 42
        //     622: goto -515 -> 107
        //     625: astore 35
        //     627: goto -90 -> 537
        //     630: astore 5
        //     632: aload 22
        //     634: astore_2
        //     635: goto -49 -> 586
        //     638: astore 5
        //     640: aload 25
        //     642: astore_3
        //     643: aload 22
        //     645: astore_2
        //     646: goto -60 -> 586
        //     649: astore 8
        //     651: aload 25
        //     653: astore_3
        //     654: aload 22
        //     656: astore_2
        //     657: goto -624 -> 33
        //     660: aload 25
        //     662: astore_3
        //     663: goto -354 -> 309
        //     666: iload 23
        //     668: sipush 512
        //     671: if_icmpge -421 -> 250
        //     674: sipush 512
        //     677: istore 23
        //     679: goto -429 -> 250
        //
        // Exception table:
        //     from	to	target	type
        //     10	31	31	java/io/IOException
        //     84	96	31	java/io/IOException
        //     118	228	31	java/io/IOException
        //     448	529	31	java/io/IOException
        //     228	274	565	java/io/IOException
        //     309	446	565	java/io/IOException
        //     558	562	565	java/io/IOException
        //     10	31	584	finally
        //     33	62	584	finally
        //     84	96	584	finally
        //     118	228	584	finally
        //     448	529	584	finally
        //     590	594	610	java/io/IOException
        //     66	70	615	java/io/IOException
        //     105	107	620	java/io/IOException
        //     533	537	625	java/io/IOException
        //     228	274	630	finally
        //     309	446	630	finally
        //     558	562	630	finally
        //     274	303	638	finally
        //     274	303	649	java/io/IOException
    }

    /** @deprecated */
    // ERROR //
    public void dump(java.io.FileDescriptor paramFileDescriptor, java.io.PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 87	com/android/server/DropBoxManagerService:mContext	Landroid/content/Context;
        //     6: ldc_w 518
        //     9: invokevirtual 522	android/content/Context:checkCallingOrSelfPermission	(Ljava/lang/String;)I
        //     12: ifeq +13 -> 25
        //     15: aload_2
        //     16: ldc_w 524
        //     19: invokevirtual 529	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     22: aload_0
        //     23: monitorexit
        //     24: return
        //     25: aload_0
        //     26: invokespecial 141	com/android/server/DropBoxManagerService:init	()V
        //     29: new 261	java/lang/StringBuilder
        //     32: dup
        //     33: invokespecial 262	java/lang/StringBuilder:<init>	()V
        //     36: astore 7
        //     38: iconst_0
        //     39: istore 8
        //     41: iconst_0
        //     42: istore 9
        //     44: new 531	java/util/ArrayList
        //     47: dup
        //     48: invokespecial 532	java/util/ArrayList:<init>	()V
        //     51: astore 10
        //     53: iconst_0
        //     54: istore 11
        //     56: aload_3
        //     57: ifnull +171 -> 228
        //     60: aload_3
        //     61: arraylength
        //     62: istore 65
        //     64: iload 11
        //     66: iload 65
        //     68: if_icmpge +160 -> 228
        //     71: aload_3
        //     72: iload 11
        //     74: aaload
        //     75: ldc_w 534
        //     78: invokevirtual 537	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     81: ifne +1134 -> 1215
        //     84: aload_3
        //     85: iload 11
        //     87: aaload
        //     88: ldc_w 539
        //     91: invokevirtual 537	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     94: ifeq +54 -> 148
        //     97: goto +1118 -> 1215
        //     100: astore 5
        //     102: aload_2
        //     103: new 261	java/lang/StringBuilder
        //     106: dup
        //     107: invokespecial 262	java/lang/StringBuilder:<init>	()V
        //     110: ldc_w 541
        //     113: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     116: aload 5
        //     118: invokevirtual 271	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     121: invokevirtual 275	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     124: invokevirtual 529	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     127: ldc 37
        //     129: ldc_w 543
        //     132: aload 5
        //     134: invokestatic 392	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     137: pop
        //     138: goto -116 -> 22
        //     141: astore 4
        //     143: aload_0
        //     144: monitorexit
        //     145: aload 4
        //     147: athrow
        //     148: aload_3
        //     149: iload 11
        //     151: aaload
        //     152: ldc_w 545
        //     155: invokevirtual 537	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     158: ifne +1066 -> 1224
        //     161: aload_3
        //     162: iload 11
        //     164: aaload
        //     165: ldc_w 547
        //     168: invokevirtual 537	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     171: ifeq +6 -> 177
        //     174: goto +1050 -> 1224
        //     177: aload_3
        //     178: iload 11
        //     180: aaload
        //     181: ldc_w 549
        //     184: invokevirtual 552	java/lang/String:startsWith	(Ljava/lang/String;)Z
        //     187: ifeq +28 -> 215
        //     190: aload 7
        //     192: ldc_w 554
        //     195: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     198: aload_3
        //     199: iload 11
        //     201: aaload
        //     202: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     205: ldc_w 556
        //     208: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     211: pop
        //     212: goto +1006 -> 1218
        //     215: aload 10
        //     217: aload_3
        //     218: iload 11
        //     220: aaload
        //     221: invokevirtual 557	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     224: pop
        //     225: goto +993 -> 1218
        //     228: aload 7
        //     230: ldc_w 559
        //     233: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     236: aload_0
        //     237: getfield 66	com/android/server/DropBoxManagerService:mAllFiles	Lcom/android/server/DropBoxManagerService$FileList;
        //     240: getfield 162	com/android/server/DropBoxManagerService$FileList:contents	Ljava/util/TreeSet;
        //     243: invokevirtual 339	java/util/TreeSet:size	()I
        //     246: invokevirtual 562	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     249: ldc_w 564
        //     252: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     255: pop
        //     256: aload 10
        //     258: invokevirtual 565	java/util/ArrayList:isEmpty	()Z
        //     261: ifne +67 -> 328
        //     264: aload 7
        //     266: ldc_w 567
        //     269: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     272: pop
        //     273: aload 10
        //     275: invokevirtual 568	java/util/ArrayList:iterator	()Ljava/util/Iterator;
        //     278: astore 61
        //     280: aload 61
        //     282: invokeinterface 383 1 0
        //     287: ifeq +32 -> 319
        //     290: aload 61
        //     292: invokeinterface 386 1 0
        //     297: checkcast 302	java/lang/String
        //     300: astore 63
        //     302: aload 7
        //     304: ldc_w 570
        //     307: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     310: aload 63
        //     312: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     315: pop
        //     316: goto -36 -> 280
        //     319: aload 7
        //     321: ldc_w 556
        //     324: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     327: pop
        //     328: iconst_0
        //     329: istore 13
        //     331: aload 10
        //     333: invokevirtual 571	java/util/ArrayList:size	()I
        //     336: istore 14
        //     338: new 573	android/text/format/Time
        //     341: dup
        //     342: invokespecial 574	android/text/format/Time:<init>	()V
        //     345: astore 15
        //     347: aload 7
        //     349: ldc_w 556
        //     352: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     355: pop
        //     356: aload_0
        //     357: getfield 66	com/android/server/DropBoxManagerService:mAllFiles	Lcom/android/server/DropBoxManagerService$FileList;
        //     360: getfield 162	com/android/server/DropBoxManagerService$FileList:contents	Ljava/util/TreeSet;
        //     363: invokevirtual 378	java/util/TreeSet:iterator	()Ljava/util/Iterator;
        //     366: astore 17
        //     368: aload 17
        //     370: invokeinterface 383 1 0
        //     375: ifeq +755 -> 1130
        //     378: aload 17
        //     380: invokeinterface 386 1 0
        //     385: checkcast 12	com/android/server/DropBoxManagerService$EntryFile
        //     388: astore 21
        //     390: aload 15
        //     392: aload 21
        //     394: getfield 200	com/android/server/DropBoxManagerService$EntryFile:timestampMillis	J
        //     397: invokevirtual 577	android/text/format/Time:set	(J)V
        //     400: aload 15
        //     402: ldc_w 579
        //     405: invokevirtual 583	android/text/format/Time:format	(Ljava/lang/String;)Ljava/lang/String;
        //     408: astore 22
        //     410: iconst_1
        //     411: istore 23
        //     413: iconst_0
        //     414: istore 24
        //     416: iload 24
        //     418: iload 14
        //     420: if_icmpge +46 -> 466
        //     423: iload 23
        //     425: ifeq +41 -> 466
        //     428: aload 10
        //     430: iload 24
        //     432: invokevirtual 586	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     435: checkcast 302	java/lang/String
        //     438: astore 59
        //     440: aload 22
        //     442: aload 59
        //     444: invokevirtual 590	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
        //     447: ifne +783 -> 1230
        //     450: aload 59
        //     452: aload 21
        //     454: getfield 213	com/android/server/DropBoxManagerService$EntryFile:tag	Ljava/lang/String;
        //     457: invokevirtual 537	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     460: ifeq +779 -> 1239
        //     463: goto +767 -> 1230
        //     466: iload 23
        //     468: ifeq -100 -> 368
        //     471: iinc 13 1
        //     474: iload 8
        //     476: ifeq +12 -> 488
        //     479: aload 7
        //     481: ldc_w 592
        //     484: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     487: pop
        //     488: aload 7
        //     490: aload 22
        //     492: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     495: ldc_w 570
        //     498: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     501: astore 25
        //     503: aload 21
        //     505: getfield 213	com/android/server/DropBoxManagerService$EntryFile:tag	Ljava/lang/String;
        //     508: ifnonnull +36 -> 544
        //     511: ldc_w 594
        //     514: astore 26
        //     516: aload 25
        //     518: aload 26
        //     520: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     523: pop
        //     524: aload 21
        //     526: getfield 229	com/android/server/DropBoxManagerService$EntryFile:file	Ljava/io/File;
        //     529: ifnonnull +25 -> 554
        //     532: aload 7
        //     534: ldc_w 596
        //     537: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     540: pop
        //     541: goto -173 -> 368
        //     544: aload 21
        //     546: getfield 213	com/android/server/DropBoxManagerService$EntryFile:tag	Ljava/lang/String;
        //     549: astore 26
        //     551: goto -35 -> 516
        //     554: iconst_1
        //     555: aload 21
        //     557: getfield 226	com/android/server/DropBoxManagerService$EntryFile:flags	I
        //     560: iand
        //     561: ifeq +15 -> 576
        //     564: aload 7
        //     566: ldc_w 598
        //     569: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     572: pop
        //     573: goto -205 -> 368
        //     576: aload 7
        //     578: ldc_w 477
        //     581: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     584: pop
        //     585: iconst_4
        //     586: aload 21
        //     588: getfield 226	com/android/server/DropBoxManagerService$EntryFile:flags	I
        //     591: iand
        //     592: ifeq +12 -> 604
        //     595: aload 7
        //     597: ldc_w 600
        //     600: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     603: pop
        //     604: iconst_2
        //     605: aload 21
        //     607: getfield 226	com/android/server/DropBoxManagerService$EntryFile:flags	I
        //     610: iand
        //     611: ifeq +250 -> 861
        //     614: ldc_w 602
        //     617: astore 29
        //     619: aload 7
        //     621: aload 29
        //     623: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     626: pop
        //     627: aload 7
        //     629: ldc_w 604
        //     632: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     635: aload 21
        //     637: getfield 229	com/android/server/DropBoxManagerService$EntryFile:file	Ljava/io/File;
        //     640: invokevirtual 473	java/io/File:length	()J
        //     643: invokevirtual 437	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     646: ldc_w 606
        //     649: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     652: pop
        //     653: iload 9
        //     655: ifne +18 -> 673
        //     658: iload 8
        //     660: ifeq +47 -> 707
        //     663: iconst_2
        //     664: aload 21
        //     666: getfield 226	com/android/server/DropBoxManagerService$EntryFile:flags	I
        //     669: iand
        //     670: ifne +37 -> 707
        //     673: iload 8
        //     675: ifne +12 -> 687
        //     678: aload 7
        //     680: ldc_w 608
        //     683: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     686: pop
        //     687: aload 7
        //     689: aload 21
        //     691: getfield 229	com/android/server/DropBoxManagerService$EntryFile:file	Ljava/io/File;
        //     694: invokevirtual 282	java/io/File:getPath	()Ljava/lang/String;
        //     697: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     700: ldc_w 556
        //     703: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     706: pop
        //     707: aload 21
        //     709: getfield 226	com/android/server/DropBoxManagerService$EntryFile:flags	I
        //     712: istore 33
        //     714: iload 33
        //     716: iconst_2
        //     717: iand
        //     718: ifeq +126 -> 844
        //     721: iload 8
        //     723: ifne +8 -> 731
        //     726: iload 9
        //     728: ifne +116 -> 844
        //     731: aconst_null
        //     732: astore 35
        //     734: new 395	android/os/DropBoxManager$Entry
        //     737: dup
        //     738: aload 21
        //     740: getfield 213	com/android/server/DropBoxManagerService$EntryFile:tag	Ljava/lang/String;
        //     743: aload 21
        //     745: getfield 200	com/android/server/DropBoxManagerService$EntryFile:timestampMillis	J
        //     748: aload 21
        //     750: getfield 229	com/android/server/DropBoxManagerService$EntryFile:file	Ljava/io/File;
        //     753: aload 21
        //     755: getfield 226	com/android/server/DropBoxManagerService$EntryFile:flags	I
        //     758: invokespecial 611	android/os/DropBoxManager$Entry:<init>	(Ljava/lang/String;JLjava/io/File;I)V
        //     761: astore 36
        //     763: iload 8
        //     765: ifeq +254 -> 1019
        //     768: new 613	java/io/InputStreamReader
        //     771: dup
        //     772: aload 36
        //     774: invokevirtual 417	android/os/DropBoxManager$Entry:getInputStream	()Ljava/io/InputStream;
        //     777: invokespecial 616	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
        //     780: astore 48
        //     782: sipush 4096
        //     785: newarray char
        //     787: astore 49
        //     789: iconst_0
        //     790: istore 50
        //     792: aload 48
        //     794: aload 49
        //     796: invokevirtual 619	java/io/InputStreamReader:read	([C)I
        //     799: istore 51
        //     801: iload 51
        //     803: ifgt +66 -> 869
        //     806: iload 50
        //     808: ifne +12 -> 820
        //     811: aload 7
        //     813: ldc_w 556
        //     816: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     819: pop
        //     820: aload 48
        //     822: astore 35
        //     824: aload 36
        //     826: ifnull +8 -> 834
        //     829: aload 36
        //     831: invokevirtual 410	android/os/DropBoxManager$Entry:close	()V
        //     834: aload 35
        //     836: ifnull +8 -> 844
        //     839: aload 35
        //     841: invokevirtual 620	java/io/InputStreamReader:close	()V
        //     844: iload 8
        //     846: ifeq -478 -> 368
        //     849: aload 7
        //     851: ldc_w 556
        //     854: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     857: pop
        //     858: goto -490 -> 368
        //     861: ldc_w 622
        //     864: astore 29
        //     866: goto -247 -> 619
        //     869: aload 7
        //     871: aload 49
        //     873: iconst_0
        //     874: iload 51
        //     876: invokevirtual 625	java/lang/StringBuilder:append	([CII)Ljava/lang/StringBuilder;
        //     879: pop
        //     880: aload 49
        //     882: iload 51
        //     884: iconst_1
        //     885: isub
        //     886: caload
        //     887: bipush 10
        //     889: if_icmpne +124 -> 1013
        //     892: iconst_1
        //     893: istore 50
        //     895: aload 7
        //     897: invokevirtual 627	java/lang/StringBuilder:length	()I
        //     900: ldc_w 628
        //     903: if_icmple -111 -> 792
        //     906: aload_2
        //     907: aload 7
        //     909: invokevirtual 275	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     912: invokevirtual 630	java/io/PrintWriter:write	(Ljava/lang/String;)V
        //     915: aload 7
        //     917: iconst_0
        //     918: invokevirtual 634	java/lang/StringBuilder:setLength	(I)V
        //     921: goto -129 -> 792
        //     924: astore 39
        //     926: aload 48
        //     928: astore 35
        //     930: aload 7
        //     932: ldc_w 636
        //     935: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     938: aload 39
        //     940: invokevirtual 637	java/io/IOException:toString	()Ljava/lang/String;
        //     943: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     946: ldc_w 556
        //     949: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     952: pop
        //     953: ldc 37
        //     955: new 261	java/lang/StringBuilder
        //     958: dup
        //     959: invokespecial 262	java/lang/StringBuilder:<init>	()V
        //     962: ldc_w 639
        //     965: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     968: aload 21
        //     970: getfield 229	com/android/server/DropBoxManagerService$EntryFile:file	Ljava/io/File;
        //     973: invokevirtual 271	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     976: invokevirtual 275	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     979: aload 39
        //     981: invokestatic 392	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     984: pop
        //     985: aload 36
        //     987: ifnull +8 -> 995
        //     990: aload 36
        //     992: invokevirtual 410	android/os/DropBoxManager$Entry:close	()V
        //     995: aload 35
        //     997: ifnull -153 -> 844
        //     1000: aload 35
        //     1002: invokevirtual 620	java/io/InputStreamReader:close	()V
        //     1005: goto -161 -> 844
        //     1008: astore 42
        //     1010: goto -166 -> 844
        //     1013: iconst_0
        //     1014: istore 50
        //     1016: goto -121 -> 895
        //     1019: aload 36
        //     1021: bipush 70
        //     1023: invokevirtual 643	android/os/DropBoxManager$Entry:getText	(I)Ljava/lang/String;
        //     1026: astore 43
        //     1028: aload 43
        //     1030: invokevirtual 644	java/lang/String:length	()I
        //     1033: bipush 70
        //     1035: if_icmpne +61 -> 1096
        //     1038: iconst_1
        //     1039: istore 44
        //     1041: aload 7
        //     1043: ldc_w 608
        //     1046: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1049: aload 43
        //     1051: invokevirtual 647	java/lang/String:trim	()Ljava/lang/String;
        //     1054: bipush 10
        //     1056: bipush 47
        //     1058: invokevirtual 651	java/lang/String:replace	(CC)Ljava/lang/String;
        //     1061: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1064: pop
        //     1065: iload 44
        //     1067: ifeq +12 -> 1079
        //     1070: aload 7
        //     1072: ldc_w 653
        //     1075: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1078: pop
        //     1079: aload 7
        //     1081: ldc_w 556
        //     1084: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1087: pop
        //     1088: goto -264 -> 824
        //     1091: astore 39
        //     1093: goto -163 -> 930
        //     1096: iconst_0
        //     1097: istore 44
        //     1099: goto -58 -> 1041
        //     1102: astore 37
        //     1104: aconst_null
        //     1105: astore 36
        //     1107: aload 36
        //     1109: ifnull +8 -> 1117
        //     1112: aload 36
        //     1114: invokevirtual 410	android/os/DropBoxManager$Entry:close	()V
        //     1117: aload 35
        //     1119: ifnull +8 -> 1127
        //     1122: aload 35
        //     1124: invokevirtual 620	java/io/InputStreamReader:close	()V
        //     1127: aload 37
        //     1129: athrow
        //     1130: iload 13
        //     1132: ifne +12 -> 1144
        //     1135: aload 7
        //     1137: ldc_w 655
        //     1140: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1143: pop
        //     1144: aload_3
        //     1145: ifnull +8 -> 1153
        //     1148: aload_3
        //     1149: arraylength
        //     1150: ifne +26 -> 1176
        //     1153: iload 8
        //     1155: ifne +12 -> 1167
        //     1158: aload 7
        //     1160: ldc_w 556
        //     1163: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1166: pop
        //     1167: aload 7
        //     1169: ldc_w 657
        //     1172: invokevirtual 268	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1175: pop
        //     1176: aload_2
        //     1177: aload 7
        //     1179: invokevirtual 275	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1182: invokevirtual 630	java/io/PrintWriter:write	(Ljava/lang/String;)V
        //     1185: goto -1163 -> 22
        //     1188: astore 38
        //     1190: goto -63 -> 1127
        //     1193: astore 37
        //     1195: goto -88 -> 1107
        //     1198: astore 37
        //     1200: aload 48
        //     1202: astore 35
        //     1204: goto -97 -> 1107
        //     1207: astore 39
        //     1209: aconst_null
        //     1210: astore 36
        //     1212: goto -282 -> 930
        //     1215: iconst_1
        //     1216: istore 8
        //     1218: iinc 11 1
        //     1221: goto -1165 -> 56
        //     1224: iconst_1
        //     1225: istore 9
        //     1227: goto -9 -> 1218
        //     1230: iconst_1
        //     1231: istore 23
        //     1233: iinc 24 1
        //     1236: goto -820 -> 416
        //     1239: iconst_0
        //     1240: istore 23
        //     1242: goto -9 -> 1233
        //
        // Exception table:
        //     from	to	target	type
        //     25	29	100	java/io/IOException
        //     2	22	141	finally
        //     25	29	141	finally
        //     29	138	141	finally
        //     148	714	141	finally
        //     829	834	141	finally
        //     839	844	141	finally
        //     849	866	141	finally
        //     990	995	141	finally
        //     1000	1005	141	finally
        //     1112	1117	141	finally
        //     1122	1127	141	finally
        //     1127	1185	141	finally
        //     782	820	924	java/io/IOException
        //     869	921	924	java/io/IOException
        //     839	844	1008	java/io/IOException
        //     1000	1005	1008	java/io/IOException
        //     768	782	1091	java/io/IOException
        //     1019	1088	1091	java/io/IOException
        //     734	763	1102	finally
        //     1122	1127	1188	java/io/IOException
        //     768	782	1193	finally
        //     930	985	1193	finally
        //     1019	1088	1193	finally
        //     782	820	1198	finally
        //     869	921	1198	finally
        //     734	763	1207	java/io/IOException
    }

    /** @deprecated */
    public DropBoxManager.Entry getNextEntry(String paramString, long paramLong)
    {
        try
        {
            if (this.mContext.checkCallingOrSelfPermission("android.permission.READ_LOGS") != 0)
                throw new SecurityException("READ_LOGS permission required");
        }
        finally
        {
        }
        try
        {
            init();
            if (paramString == null)
            {
                localFileList = this.mAllFiles;
                if (localFileList != null)
                    break label95;
                localEntry = null;
                return localEntry;
            }
        }
        catch (IOException localIOException1)
        {
            while (true)
            {
                Slog.e("DropBoxManagerService", "Can't init", localIOException1);
                DropBoxManager.Entry localEntry = null;
                continue;
                FileList localFileList = (FileList)this.mFilesByTag.get(paramString);
                continue;
                label95: Iterator localIterator = localFileList.contents.tailSet(new EntryFile(1L + paramLong)).iterator();
                while (true)
                    while (true)
                    {
                        if (!localIterator.hasNext())
                            break label251;
                        EntryFile localEntryFile = (EntryFile)localIterator.next();
                        if (localEntryFile.tag != null)
                        {
                            if ((0x1 & localEntryFile.flags) != 0)
                            {
                                localEntry = new DropBoxManager.Entry(localEntryFile.tag, localEntryFile.timestampMillis);
                                break;
                            }
                            try
                            {
                                localEntry = new DropBoxManager.Entry(localEntryFile.tag, localEntryFile.timestampMillis, localEntryFile.file, localEntryFile.flags);
                            }
                            catch (IOException localIOException2)
                            {
                                Slog.e("DropBoxManagerService", "Can't read: " + localEntryFile.file, localIOException2);
                            }
                        }
                    }
                label251: localEntry = null;
            }
        }
    }

    public boolean isTagEnabled(String paramString)
    {
        if (!"disabled".equals(Settings.Secure.getString(this.mContentResolver, "dropbox:" + paramString)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void stop()
    {
        this.mContext.unregisterReceiver(this.mReceiver);
    }

    private static final class EntryFile
        implements Comparable<EntryFile>
    {
        public final int blocks;
        public final File file;
        public final int flags;
        public final String tag;
        public final long timestampMillis;

        public EntryFile(long paramLong)
        {
            this.tag = null;
            this.timestampMillis = paramLong;
            this.flags = 1;
            this.file = null;
            this.blocks = 0;
        }

        public EntryFile(File paramFile, int paramInt)
        {
            this.file = paramFile;
            this.blocks = ((int)((this.file.length() + paramInt - 1L) / paramInt));
            String str1 = paramFile.getName();
            int i = str1.lastIndexOf('@');
            if (i < 0)
            {
                this.tag = null;
                this.timestampMillis = 0L;
                this.flags = 1;
            }
            while (true)
            {
                return;
                int j = 0;
                this.tag = Uri.decode(str1.substring(0, i));
                if (str1.endsWith(".gz"))
                {
                    j = 0x0 | 0x4;
                    str1 = str1.substring(0, -3 + str1.length());
                }
                String str2;
                if (str1.endsWith(".lost"))
                {
                    j |= 1;
                    str2 = str1.substring(i + 1, -5 + str1.length());
                    label139: this.flags = j;
                }
                try
                {
                    long l2 = Long.valueOf(str2).longValue();
                    l1 = l2;
                    this.timestampMillis = l1;
                    continue;
                    if (str1.endsWith(".txt"))
                    {
                        j |= 2;
                        str2 = str1.substring(i + 1, -4 + str1.length());
                        break label139;
                    }
                    if (str1.endsWith(".dat"))
                    {
                        str2 = str1.substring(i + 1, -4 + str1.length());
                        break label139;
                    }
                    this.flags = 1;
                    this.timestampMillis = 0L;
                }
                catch (NumberFormatException localNumberFormatException)
                {
                    while (true)
                        long l1 = 0L;
                }
            }
        }

        public EntryFile(File paramFile1, File paramFile2, String paramString, long paramLong, int paramInt1, int paramInt2)
            throws IOException
        {
            if ((paramInt1 & 0x1) != 0)
                throw new IllegalArgumentException();
            this.tag = paramString;
            this.timestampMillis = paramLong;
            this.flags = paramInt1;
            StringBuilder localStringBuilder1 = new StringBuilder().append(Uri.encode(paramString)).append("@").append(paramLong);
            String str1;
            StringBuilder localStringBuilder2;
            if ((paramInt1 & 0x2) != 0)
            {
                str1 = ".txt";
                localStringBuilder2 = localStringBuilder1.append(str1);
                if ((paramInt1 & 0x4) == 0)
                    break label172;
            }
            label172: for (String str2 = ".gz"; ; str2 = "")
            {
                this.file = new File(paramFile2, str2);
                if (paramFile1.renameTo(this.file))
                    break label179;
                throw new IOException("Can't rename " + paramFile1 + " to " + this.file);
                str1 = ".dat";
                break;
            }
            label179: this.blocks = ((int)((this.file.length() + paramInt2 - 1L) / paramInt2));
        }

        public EntryFile(File paramFile, String paramString, long paramLong)
            throws IOException
        {
            this.tag = paramString;
            this.timestampMillis = paramLong;
            this.flags = 1;
            this.file = new File(paramFile, Uri.encode(paramString) + "@" + paramLong + ".lost");
            this.blocks = 0;
            new FileOutputStream(this.file).close();
        }

        public final int compareTo(EntryFile paramEntryFile)
        {
            int i = -1;
            if (this.timestampMillis < paramEntryFile.timestampMillis);
            while (true)
            {
                return i;
                if (this.timestampMillis > paramEntryFile.timestampMillis)
                    i = 1;
                else if ((this.file != null) && (paramEntryFile.file != null))
                    i = this.file.compareTo(paramEntryFile.file);
                else if (paramEntryFile.file == null)
                    if (this.file != null)
                        i = 1;
                    else if (this == paramEntryFile)
                        i = 0;
                    else if (hashCode() >= paramEntryFile.hashCode())
                        if (hashCode() > paramEntryFile.hashCode())
                            i = 1;
                        else
                            i = 0;
            }
        }
    }

    private static final class FileList
        implements Comparable<FileList>
    {
        public int blocks = 0;
        public final TreeSet<DropBoxManagerService.EntryFile> contents = new TreeSet();

        public final int compareTo(FileList paramFileList)
        {
            int i = 0;
            if (this.blocks != paramFileList.blocks)
                i = paramFileList.blocks - this.blocks;
            while (true)
            {
                return i;
                if (this != paramFileList)
                    if (hashCode() < paramFileList.hashCode())
                        i = -1;
                    else if (hashCode() > paramFileList.hashCode())
                        i = 1;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.DropBoxManagerService
 * JD-Core Version:        0.6.2
 */