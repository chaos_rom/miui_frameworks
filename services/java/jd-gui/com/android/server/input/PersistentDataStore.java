package com.android.server.input;

import android.util.Slog;
import android.util.Xml;
import com.android.internal.os.AtomicFile;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.XmlUtils;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import libcore.io.IoUtils;
import libcore.util.Objects;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

final class PersistentDataStore
{
    static final String TAG = "InputManager";
    private final AtomicFile mAtomicFile = new AtomicFile(new File("/data/system/input-manager-state.xml"));
    private boolean mDirty;
    private final HashMap<String, InputDeviceState> mInputDevices = new HashMap();
    private boolean mLoaded;

    private void clearState()
    {
        this.mInputDevices.clear();
    }

    private InputDeviceState getInputDeviceState(String paramString, boolean paramBoolean)
    {
        loadIfNeeded();
        InputDeviceState localInputDeviceState = (InputDeviceState)this.mInputDevices.get(paramString);
        if ((localInputDeviceState == null) && (paramBoolean))
        {
            localInputDeviceState = new InputDeviceState(null);
            this.mInputDevices.put(paramString, localInputDeviceState);
            setDirty();
        }
        return localInputDeviceState;
    }

    private void load()
    {
        clearState();
        try
        {
            localFileInputStream = this.mAtomicFile.openRead();
        }
        catch (FileNotFoundException localFileNotFoundException)
        {
            try
            {
                XmlPullParser localXmlPullParser = Xml.newPullParser();
                localXmlPullParser.setInput(new BufferedInputStream(localFileInputStream), null);
                loadFromXml(localXmlPullParser);
                while (true)
                {
                    return;
                    localFileNotFoundException = localFileNotFoundException;
                }
            }
            catch (IOException localIOException)
            {
                while (true)
                {
                    Slog.w("InputManager", "Failed to load input manager persistent store data.", localIOException);
                    clearState();
                    IoUtils.closeQuietly(localFileInputStream);
                }
            }
            catch (XmlPullParserException localXmlPullParserException)
            {
                while (true)
                {
                    Slog.w("InputManager", "Failed to load input manager persistent store data.", localXmlPullParserException);
                    clearState();
                    IoUtils.closeQuietly(localFileInputStream);
                }
            }
            finally
            {
                FileInputStream localFileInputStream;
                IoUtils.closeQuietly(localFileInputStream);
            }
        }
    }

    private void loadFromXml(XmlPullParser paramXmlPullParser)
        throws IOException, XmlPullParserException
    {
        XmlUtils.beginDocument(paramXmlPullParser, "input-manager-state");
        int i = paramXmlPullParser.getDepth();
        while (XmlUtils.nextElementWithin(paramXmlPullParser, i))
            if (paramXmlPullParser.getName().equals("input-devices"))
                loadInputDevicesFromXml(paramXmlPullParser);
    }

    private void loadIfNeeded()
    {
        if (!this.mLoaded)
        {
            load();
            this.mLoaded = true;
        }
    }

    private void loadInputDevicesFromXml(XmlPullParser paramXmlPullParser)
        throws IOException, XmlPullParserException
    {
        int i = paramXmlPullParser.getDepth();
        while (XmlUtils.nextElementWithin(paramXmlPullParser, i))
            if (paramXmlPullParser.getName().equals("input-device"))
            {
                String str = paramXmlPullParser.getAttributeValue(null, "descriptor");
                if (str == null)
                    throw new XmlPullParserException("Missing descriptor attribute on input-device.");
                if (this.mInputDevices.containsKey(str))
                    throw new XmlPullParserException("Found duplicate input device.");
                InputDeviceState localInputDeviceState = new InputDeviceState(null);
                localInputDeviceState.loadFromXml(paramXmlPullParser);
                this.mInputDevices.put(str, localInputDeviceState);
            }
    }

    // ERROR //
    private void save()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 44	com/android/server/input/PersistentDataStore:mAtomicFile	Lcom/android/internal/os/AtomicFile;
        //     4: invokevirtual 172	com/android/internal/os/AtomicFile:startWrite	()Ljava/io/FileOutputStream;
        //     7: astore_3
        //     8: new 174	com/android/internal/util/FastXmlSerializer
        //     11: dup
        //     12: invokespecial 175	com/android/internal/util/FastXmlSerializer:<init>	()V
        //     15: astore 4
        //     17: aload 4
        //     19: new 177	java/io/BufferedOutputStream
        //     22: dup
        //     23: aload_3
        //     24: invokespecial 180	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;)V
        //     27: ldc 182
        //     29: invokeinterface 188 3 0
        //     34: aload_0
        //     35: aload 4
        //     37: invokespecial 192	com/android/server/input/PersistentDataStore:saveToXml	(Lorg/xmlpull/v1/XmlSerializer;)V
        //     40: aload 4
        //     42: invokeinterface 195 1 0
        //     47: iconst_1
        //     48: ifeq +14 -> 62
        //     51: aload_0
        //     52: getfield 44	com/android/server/input/PersistentDataStore:mAtomicFile	Lcom/android/internal/os/AtomicFile;
        //     55: aload_3
        //     56: invokevirtual 199	com/android/internal/os/AtomicFile:finishWrite	(Ljava/io/FileOutputStream;)V
        //     59: goto +55 -> 114
        //     62: aload_0
        //     63: getfield 44	com/android/server/input/PersistentDataStore:mAtomicFile	Lcom/android/internal/os/AtomicFile;
        //     66: aload_3
        //     67: invokevirtual 202	com/android/internal/os/AtomicFile:failWrite	(Ljava/io/FileOutputStream;)V
        //     70: goto +44 -> 114
        //     73: astore_1
        //     74: ldc 13
        //     76: ldc 204
        //     78: aload_1
        //     79: invokestatic 115	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     82: pop
        //     83: goto +31 -> 114
        //     86: astore 5
        //     88: iconst_0
        //     89: ifeq +14 -> 103
        //     92: aload_0
        //     93: getfield 44	com/android/server/input/PersistentDataStore:mAtomicFile	Lcom/android/internal/os/AtomicFile;
        //     96: aload_3
        //     97: invokevirtual 199	com/android/internal/os/AtomicFile:finishWrite	(Ljava/io/FileOutputStream;)V
        //     100: aload 5
        //     102: athrow
        //     103: aload_0
        //     104: getfield 44	com/android/server/input/PersistentDataStore:mAtomicFile	Lcom/android/internal/os/AtomicFile;
        //     107: aload_3
        //     108: invokevirtual 202	com/android/internal/os/AtomicFile:failWrite	(Ljava/io/FileOutputStream;)V
        //     111: goto -11 -> 100
        //     114: return
        //
        // Exception table:
        //     from	to	target	type
        //     0	8	73	java/io/IOException
        //     51	70	73	java/io/IOException
        //     92	111	73	java/io/IOException
        //     8	47	86	finally
    }

    private void saveToXml(XmlSerializer paramXmlSerializer)
        throws IOException
    {
        paramXmlSerializer.startDocument(null, Boolean.valueOf(true));
        paramXmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
        paramXmlSerializer.startTag(null, "input-manager-state");
        paramXmlSerializer.startTag(null, "input-devices");
        Iterator localIterator = this.mInputDevices.entrySet().iterator();
        while (localIterator.hasNext())
        {
            Map.Entry localEntry = (Map.Entry)localIterator.next();
            String str = (String)localEntry.getKey();
            InputDeviceState localInputDeviceState = (InputDeviceState)localEntry.getValue();
            paramXmlSerializer.startTag(null, "input-device");
            paramXmlSerializer.attribute(null, "descriptor", str);
            localInputDeviceState.saveToXml(paramXmlSerializer);
            paramXmlSerializer.endTag(null, "input-device");
        }
        paramXmlSerializer.endTag(null, "input-devices");
        paramXmlSerializer.endTag(null, "input-manager-state");
        paramXmlSerializer.endDocument();
    }

    private void setDirty()
    {
        this.mDirty = true;
    }

    public boolean addKeyboardLayout(String paramString1, String paramString2)
    {
        boolean bool = true;
        if (getInputDeviceState(paramString1, bool).addKeyboardLayout(paramString2))
            setDirty();
        while (true)
        {
            return bool;
            bool = false;
        }
    }

    public String getCurrentKeyboardLayout(String paramString)
    {
        InputDeviceState localInputDeviceState = getInputDeviceState(paramString, false);
        if (localInputDeviceState != null);
        for (String str = localInputDeviceState.getCurrentKeyboardLayout(); ; str = null)
            return str;
    }

    public String[] getKeyboardLayouts(String paramString)
    {
        InputDeviceState localInputDeviceState = getInputDeviceState(paramString, false);
        if (localInputDeviceState == null);
        for (String[] arrayOfString = (String[])ArrayUtils.emptyArray(String.class); ; arrayOfString = localInputDeviceState.getKeyboardLayouts())
            return arrayOfString;
    }

    public boolean removeKeyboardLayout(String paramString1, String paramString2)
    {
        boolean bool = true;
        if (getInputDeviceState(paramString1, bool).removeKeyboardLayout(paramString2))
            setDirty();
        while (true)
        {
            return bool;
            bool = false;
        }
    }

    public boolean removeUninstalledKeyboardLayouts(Set<String> paramSet)
    {
        int i = 0;
        Iterator localIterator = this.mInputDevices.values().iterator();
        while (localIterator.hasNext())
            if (((InputDeviceState)localIterator.next()).removeUninstalledKeyboardLayouts(paramSet))
                i = 1;
        if (i != 0)
            setDirty();
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void saveIfNeeded()
    {
        if (this.mDirty)
        {
            save();
            this.mDirty = false;
        }
    }

    public boolean setCurrentKeyboardLayout(String paramString1, String paramString2)
    {
        boolean bool = true;
        if (getInputDeviceState(paramString1, bool).setCurrentKeyboardLayout(paramString2))
            setDirty();
        while (true)
        {
            return bool;
            bool = false;
        }
    }

    public boolean switchKeyboardLayout(String paramString, int paramInt)
    {
        boolean bool = false;
        InputDeviceState localInputDeviceState = getInputDeviceState(paramString, false);
        if ((localInputDeviceState != null) && (localInputDeviceState.switchKeyboardLayout(paramInt)))
        {
            setDirty();
            bool = true;
        }
        return bool;
    }

    private static final class InputDeviceState
    {
        private String mCurrentKeyboardLayout;
        private ArrayList<String> mKeyboardLayouts = new ArrayList();

        static
        {
            if (!PersistentDataStore.class.desiredAssertionStatus());
            for (boolean bool = true; ; bool = false)
            {
                $assertionsDisabled = bool;
                return;
            }
        }

        private void updateCurrentKeyboardLayoutIfRemoved(String paramString, int paramInt)
        {
            int i;
            if (Objects.equal(this.mCurrentKeyboardLayout, paramString))
            {
                if (this.mKeyboardLayouts.isEmpty())
                    break label52;
                i = paramInt;
                if (i == this.mKeyboardLayouts.size())
                    i = 0;
            }
            label52: for (this.mCurrentKeyboardLayout = ((String)this.mKeyboardLayouts.get(i)); ; this.mCurrentKeyboardLayout = null)
                return;
        }

        public boolean addKeyboardLayout(String paramString)
        {
            int i = Collections.binarySearch(this.mKeyboardLayouts, paramString);
            if (i >= 0);
            for (boolean bool = false; ; bool = true)
            {
                return bool;
                this.mKeyboardLayouts.add(-1 + -i, paramString);
                if (this.mCurrentKeyboardLayout == null)
                    this.mCurrentKeyboardLayout = paramString;
            }
        }

        public String getCurrentKeyboardLayout()
        {
            return this.mCurrentKeyboardLayout;
        }

        public String[] getKeyboardLayouts()
        {
            if (this.mKeyboardLayouts.isEmpty());
            for (String[] arrayOfString = (String[])ArrayUtils.emptyArray(String.class); ; arrayOfString = (String[])this.mKeyboardLayouts.toArray(new String[this.mKeyboardLayouts.size()]))
                return arrayOfString;
        }

        public void loadFromXml(XmlPullParser paramXmlPullParser)
            throws IOException, XmlPullParserException
        {
            int i = paramXmlPullParser.getDepth();
            while (XmlUtils.nextElementWithin(paramXmlPullParser, i))
                if (paramXmlPullParser.getName().equals("keyboard-layout"))
                {
                    String str1 = paramXmlPullParser.getAttributeValue(null, "descriptor");
                    if (str1 == null)
                        throw new XmlPullParserException("Missing descriptor attribute on keyboard-layout.");
                    String str2 = paramXmlPullParser.getAttributeValue(null, "current");
                    if (this.mKeyboardLayouts.contains(str1))
                        throw new XmlPullParserException("Found duplicate keyboard layout.");
                    this.mKeyboardLayouts.add(str1);
                    if ((str2 != null) && (str2.equals("true")))
                    {
                        if (this.mCurrentKeyboardLayout != null)
                            throw new XmlPullParserException("Found multiple current keyboard layouts.");
                        this.mCurrentKeyboardLayout = str1;
                    }
                }
            Collections.sort(this.mKeyboardLayouts);
            if ((this.mCurrentKeyboardLayout == null) && (!this.mKeyboardLayouts.isEmpty()))
                this.mCurrentKeyboardLayout = ((String)this.mKeyboardLayouts.get(0));
        }

        public boolean removeKeyboardLayout(String paramString)
        {
            int i = Collections.binarySearch(this.mKeyboardLayouts, paramString);
            if (i < 0);
            for (boolean bool = false; ; bool = true)
            {
                return bool;
                this.mKeyboardLayouts.remove(i);
                updateCurrentKeyboardLayoutIfRemoved(paramString, i);
            }
        }

        public boolean removeUninstalledKeyboardLayouts(Set<String> paramSet)
        {
            boolean bool = false;
            int j;
            for (int i = this.mKeyboardLayouts.size(); ; i = j)
            {
                j = i - 1;
                if (i <= 0)
                    break;
                String str = (String)this.mKeyboardLayouts.get(j);
                if (!paramSet.contains(str))
                {
                    Slog.i("InputManager", "Removing uninstalled keyboard layout " + str);
                    this.mKeyboardLayouts.remove(j);
                    updateCurrentKeyboardLayoutIfRemoved(str, j);
                    bool = true;
                }
            }
            return bool;
        }

        public void saveToXml(XmlSerializer paramXmlSerializer)
            throws IOException
        {
            Iterator localIterator = this.mKeyboardLayouts.iterator();
            while (localIterator.hasNext())
            {
                String str = (String)localIterator.next();
                paramXmlSerializer.startTag(null, "keyboard-layout");
                paramXmlSerializer.attribute(null, "descriptor", str);
                if (str.equals(this.mCurrentKeyboardLayout))
                    paramXmlSerializer.attribute(null, "current", "true");
                paramXmlSerializer.endTag(null, "keyboard-layout");
            }
        }

        public boolean setCurrentKeyboardLayout(String paramString)
        {
            if (Objects.equal(this.mCurrentKeyboardLayout, paramString));
            for (boolean bool = false; ; bool = true)
            {
                return bool;
                addKeyboardLayout(paramString);
                this.mCurrentKeyboardLayout = paramString;
            }
        }

        public boolean switchKeyboardLayout(int paramInt)
        {
            int i = this.mKeyboardLayouts.size();
            boolean bool;
            if (i < 2)
            {
                bool = false;
                return bool;
            }
            int j = Collections.binarySearch(this.mKeyboardLayouts, this.mCurrentKeyboardLayout);
            assert (j >= 0);
            if (paramInt > 0);
            for (int k = (j + 1) % i; ; k = (-1 + (j + i)) % i)
            {
                this.mCurrentKeyboardLayout = ((String)this.mKeyboardLayouts.get(k));
                bool = true;
                break;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.input.PersistentDataStore
 * JD-Core Version:        0.6.2
 */