package com.android.server.input;

public final class InputApplicationHandle
{
    public final Object appWindowToken;
    public long dispatchingTimeoutNanos;
    public String name;
    private int ptr;

    public InputApplicationHandle(Object paramObject)
    {
        this.appWindowToken = paramObject;
    }

    private native void nativeDispose();

    protected void finalize()
        throws Throwable
    {
        try
        {
            nativeDispose();
            return;
        }
        finally
        {
            super.finalize();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.input.InputApplicationHandle
 * JD-Core Version:        0.6.2
 */