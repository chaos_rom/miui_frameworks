package com.android.server.input;

import android.graphics.Region;
import android.view.InputChannel;

public final class InputWindowHandle
{
    public boolean canReceiveKeys;
    public long dispatchingTimeoutNanos;
    public int frameBottom;
    public int frameLeft;
    public int frameRight;
    public int frameTop;
    public boolean hasFocus;
    public boolean hasWallpaper;
    public final InputApplicationHandle inputApplicationHandle;
    public InputChannel inputChannel;
    public int inputFeatures;
    public int layer;
    public int layoutParamsFlags;
    public int layoutParamsType;
    public String name;
    public int ownerPid;
    public int ownerUid;
    public boolean paused;
    private int ptr;
    public float scaleFactor;
    public final Region touchableRegion = new Region();
    public boolean visible;
    public final Object windowState;

    public InputWindowHandle(InputApplicationHandle paramInputApplicationHandle, Object paramObject)
    {
        this.inputApplicationHandle = paramInputApplicationHandle;
        this.windowState = paramObject;
    }

    private native void nativeDispose();

    protected void finalize()
        throws Throwable
    {
        try
        {
            nativeDispose();
            return;
        }
        finally
        {
            super.finalize();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.input.InputWindowHandle
 * JD-Core Version:        0.6.2
 */