package com.android.server.input;

import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.database.ContentObserver;
import android.hardware.input.IInputDevicesChangedListener;
import android.hardware.input.IInputManager.Stub;
import android.hardware.input.KeyboardLayout;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Looper;
import android.os.Message;
import android.os.MessageQueue;
import android.os.Process;
import android.os.RemoteException;
import android.provider.Settings.SettingNotFoundException;
import android.provider.Settings.System;
import android.server.BluetoothService;
import android.util.Log;
import android.util.Slog;
import android.util.SparseArray;
import android.view.InputChannel;
import android.view.InputDevice;
import android.view.InputEvent;
import android.view.KeyEvent;
import android.view.PointerIcon;
import android.view.ViewConfiguration;
import android.widget.Toast;
import com.android.server.Watchdog;
import com.android.server.Watchdog.Monitor;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import libcore.io.Streams;

public class InputManagerService extends IInputManager.Stub
    implements Watchdog.Monitor
{
    public static final int BTN_MOUSE = 272;
    static final boolean DEBUG = false;
    private static final String EXCLUDED_DEVICES_PATH = "etc/excluded-input-devices.xml";
    private static final int INJECTION_TIMEOUT_MILLIS = 30000;
    private static final int INPUT_EVENT_INJECTION_FAILED = 2;
    private static final int INPUT_EVENT_INJECTION_PERMISSION_DENIED = 1;
    private static final int INPUT_EVENT_INJECTION_SUCCEEDED = 0;
    private static final int INPUT_EVENT_INJECTION_TIMED_OUT = 3;
    public static final int KEY_STATE_DOWN = 1;
    public static final int KEY_STATE_UNKNOWN = -1;
    public static final int KEY_STATE_UP = 0;
    public static final int KEY_STATE_VIRTUAL = 2;
    private static final int MSG_DELIVER_INPUT_DEVICES_CHANGED = 1;
    private static final int MSG_RELOAD_DEVICE_ALIASES = 5;
    private static final int MSG_RELOAD_KEYBOARD_LAYOUTS = 3;
    private static final int MSG_SWITCH_KEYBOARD_LAYOUT = 2;
    private static final int MSG_UPDATE_KEYBOARD_LAYOUTS = 4;
    public static final int SW_KEYPAD_SLIDE = 10;
    public static final int SW_LID = 0;
    static final String TAG = "InputManager";
    private BluetoothService mBluetoothService;
    private final Callbacks mCallbacks;
    private final Context mContext;
    private final PersistentDataStore mDataStore = new PersistentDataStore();
    private final InputManagerHandler mHandler;
    private InputDevice[] mInputDevices = new InputDevice[0];
    private final SparseArray<InputDevicesChangedListenerRecord> mInputDevicesChangedListeners = new SparseArray();
    private boolean mInputDevicesChangedPending;
    private Object mInputDevicesLock = new Object();
    InputFilter mInputFilter;
    InputFilterHost mInputFilterHost;
    final Object mInputFilterLock = new Object();
    private PendingIntent mKeyboardLayoutIntent;
    private boolean mKeyboardLayoutNotificationShown;
    private int mNextVibratorTokenValue;
    private NotificationManager mNotificationManager;
    private final int mPtr;
    private Toast mSwitchedKeyboardLayoutToast;
    private boolean mSystemReady;
    private final ArrayList<InputDevice> mTempFullKeyboards = new ArrayList();
    private final ArrayList<InputDevicesChangedListenerRecord> mTempInputDevicesChangedListenersToNotify = new ArrayList();
    private Object mVibratorLock = new Object();
    private HashMap<IBinder, VibratorToken> mVibratorTokens = new HashMap();

    public InputManagerService(Context paramContext, Callbacks paramCallbacks)
    {
        this.mContext = paramContext;
        this.mCallbacks = paramCallbacks;
        this.mHandler = new InputManagerHandler(null);
        Slog.i("InputManager", "Initializing input manager");
        this.mPtr = nativeInit(this, this.mContext, this.mHandler.getLooper().getQueue());
    }

    private void cancelVibrateIfNeeded(VibratorToken paramVibratorToken)
    {
        try
        {
            if (paramVibratorToken.mVibrating)
            {
                nativeCancelVibrate(this.mPtr, paramVibratorToken.mDeviceId, paramVibratorToken.mTokenValue);
                paramVibratorToken.mVibrating = false;
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private boolean checkCallingPermission(String paramString1, String paramString2)
    {
        boolean bool = true;
        if (Binder.getCallingPid() == Process.myPid());
        while (true)
        {
            return bool;
            if (this.mContext.checkCallingPermission(paramString1) != 0)
            {
                Slog.w("InputManager", "Permission Denial: " + paramString2 + " from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid() + " requires " + paramString1);
                bool = false;
            }
        }
    }

    private boolean checkInjectEventsPermission(int paramInt1, int paramInt2)
    {
        if (this.mContext.checkPermission("android.permission.INJECT_EVENTS", paramInt1, paramInt2) == 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean containsInputDeviceWithDescriptor(InputDevice[] paramArrayOfInputDevice, String paramString)
    {
        int i = paramArrayOfInputDevice.length;
        int j = 0;
        if (j < i)
            if (!paramArrayOfInputDevice[j].getDescriptor().equals(paramString));
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            j++;
            break;
        }
    }

    private void deliverInputDevicesChanged(InputDevice[] paramArrayOfInputDevice)
    {
        this.mTempInputDevicesChangedListenersToNotify.clear();
        this.mTempFullKeyboards.clear();
        int i;
        int[] arrayOfInt;
        int m;
        int n;
        synchronized (this.mInputDevicesLock)
        {
            if (this.mInputDevicesChangedPending)
            {
                this.mInputDevicesChangedPending = false;
                i = this.mInputDevicesChangedListeners.size();
                for (int j = 0; j < i; j++)
                    this.mTempInputDevicesChangedListenersToNotify.add(this.mInputDevicesChangedListeners.valueAt(j));
                int k = this.mInputDevices.length;
                arrayOfInt = new int[k * 2];
                m = 0;
                n = 0;
                if (m >= k);
            }
        }
        while (true)
        {
            int i5;
            try
            {
                InputDevice localInputDevice2 = this.mInputDevices[m];
                arrayOfInt[(m * 2)] = localInputDevice2.getId();
                arrayOfInt[(1 + m * 2)] = localInputDevice2.getGeneration();
                if ((localInputDevice2.isVirtual()) || (!localInputDevice2.isFullKeyboard()))
                    break label408;
                if (!containsInputDeviceWithDescriptor(paramArrayOfInputDevice, localInputDevice2.getDescriptor()))
                {
                    ArrayList localArrayList = this.mTempFullKeyboards;
                    i6 = n + 1;
                    localArrayList.add(n, localInputDevice2);
                    m++;
                    n = i6;
                    break;
                }
                this.mTempFullKeyboards.add(localInputDevice2);
                break label408;
                int i1 = 0;
                if (i1 < i)
                {
                    ((InputDevicesChangedListenerRecord)this.mTempInputDevicesChangedListenersToNotify.get(i1)).notifyInputDevicesChanged(arrayOfInt);
                    i1++;
                    continue;
                    localObject2 = finally;
                    throw localObject2;
                }
                this.mTempInputDevicesChangedListenersToNotify.clear();
                int i3;
                int i4;
                PersistentDataStore localPersistentDataStore;
                if (this.mNotificationManager != null)
                {
                    int i2 = this.mTempFullKeyboards.size();
                    i3 = 0;
                    i4 = 0;
                    localPersistentDataStore = this.mDataStore;
                    i5 = 0;
                    if (i5 >= i2);
                }
                try
                {
                    InputDevice localInputDevice1 = (InputDevice)this.mTempFullKeyboards.get(i5);
                    if (this.mDataStore.getCurrentKeyboardLayout(localInputDevice1.getDescriptor()) != null)
                        break label415;
                    i3 = 1;
                    if (i5 >= n)
                        break label415;
                    i4 = 1;
                    break label415;
                    if (i3 != 0)
                    {
                        if (i4 != 0)
                            showMissingKeyboardLayoutNotification();
                        this.mTempFullKeyboards.clear();
                    }
                }
                finally
                {
                }
                if (!this.mKeyboardLayoutNotificationShown)
                    continue;
                hideMissingKeyboardLayoutNotification();
            }
            finally
            {
                continue;
            }
            return;
            label408: int i6 = n;
            continue;
            label415: i5++;
        }
    }

    private KeyEvent dispatchUnhandledKey(InputWindowHandle paramInputWindowHandle, KeyEvent paramKeyEvent, int paramInt)
    {
        return this.mCallbacks.dispatchUnhandledKey(paramInputWindowHandle, paramKeyEvent, paramInt);
    }

    private String getDeviceAlias(String paramString)
    {
        if ((this.mBluetoothService != null) && (BluetoothAdapter.checkBluetoothAddress(paramString)));
        for (String str = this.mBluetoothService.getRemoteAlias(paramString); ; str = null)
            return str;
    }

    private int getDoubleTapTimeout()
    {
        return ViewConfiguration.getDoubleTapTimeout();
    }

    // ERROR //
    private String[] getExcludedDeviceNames()
    {
        // Byte code:
        //     0: new 148	java/util/ArrayList
        //     3: dup
        //     4: invokespecial 149	java/util/ArrayList:<init>	()V
        //     7: astore_1
        //     8: new 401	java/io/File
        //     11: dup
        //     12: invokestatic 407	android/os/Environment:getRootDirectory	()Ljava/io/File;
        //     15: ldc 53
        //     17: invokespecial 410	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     20: astore_2
        //     21: aconst_null
        //     22: astore_3
        //     23: new 412	java/io/FileReader
        //     26: dup
        //     27: aload_2
        //     28: invokespecial 415	java/io/FileReader:<init>	(Ljava/io/File;)V
        //     31: astore 4
        //     33: invokestatic 421	android/util/Xml:newPullParser	()Lorg/xmlpull/v1/XmlPullParser;
        //     36: astore 12
        //     38: aload 12
        //     40: aload 4
        //     42: invokeinterface 427 2 0
        //     47: aload 12
        //     49: ldc_w 429
        //     52: invokestatic 435	com/android/internal/util/XmlUtils:beginDocument	(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V
        //     55: aload 12
        //     57: invokestatic 439	com/android/internal/util/XmlUtils:nextElement	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     60: ldc_w 441
        //     63: aload 12
        //     65: invokeinterface 444 1 0
        //     70: invokevirtual 313	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     73: istore 13
        //     75: iload 13
        //     77: ifne +28 -> 105
        //     80: aload 4
        //     82: ifnull +8 -> 90
        //     85: aload 4
        //     87: invokevirtual 447	java/io/FileReader:close	()V
        //     90: aload_1
        //     91: aload_1
        //     92: invokevirtual 356	java/util/ArrayList:size	()I
        //     95: anewarray 309	java/lang/String
        //     98: invokevirtual 451	java/util/ArrayList:toArray	([Ljava/lang/Object;)[Ljava/lang/Object;
        //     101: checkcast 453	[Ljava/lang/String;
        //     104: areturn
        //     105: aload 12
        //     107: aconst_null
        //     108: ldc_w 455
        //     111: invokeinterface 459 3 0
        //     116: astore 14
        //     118: aload 14
        //     120: ifnull -65 -> 55
        //     123: aload_1
        //     124: aload 14
        //     126: invokevirtual 328	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     129: pop
        //     130: goto -75 -> 55
        //     133: astore 10
        //     135: aload 4
        //     137: astore_3
        //     138: aload_3
        //     139: ifnull -49 -> 90
        //     142: aload_3
        //     143: invokevirtual 447	java/io/FileReader:close	()V
        //     146: goto -56 -> 90
        //     149: astore 11
        //     151: goto -61 -> 90
        //     154: astore 16
        //     156: goto -66 -> 90
        //     159: astore 5
        //     161: ldc 80
        //     163: new 268	java/lang/StringBuilder
        //     166: dup
        //     167: invokespecial 269	java/lang/StringBuilder:<init>	()V
        //     170: ldc_w 461
        //     173: invokevirtual 275	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     176: aload_2
        //     177: invokevirtual 464	java/io/File:getAbsolutePath	()Ljava/lang/String;
        //     180: invokevirtual 275	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     183: ldc_w 466
        //     186: invokevirtual 275	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     189: invokevirtual 291	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     192: aload 5
        //     194: invokestatic 470	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     197: pop
        //     198: aload_3
        //     199: ifnull -109 -> 90
        //     202: aload_3
        //     203: invokevirtual 447	java/io/FileReader:close	()V
        //     206: goto -116 -> 90
        //     209: astore 9
        //     211: goto -121 -> 90
        //     214: astore 6
        //     216: aload_3
        //     217: ifnull +7 -> 224
        //     220: aload_3
        //     221: invokevirtual 447	java/io/FileReader:close	()V
        //     224: aload 6
        //     226: athrow
        //     227: astore 7
        //     229: goto -5 -> 224
        //     232: astore 6
        //     234: aload 4
        //     236: astore_3
        //     237: goto -21 -> 216
        //     240: astore 5
        //     242: aload 4
        //     244: astore_3
        //     245: goto -84 -> 161
        //     248: astore 17
        //     250: goto -112 -> 138
        //
        // Exception table:
        //     from	to	target	type
        //     33	75	133	java/io/FileNotFoundException
        //     105	130	133	java/io/FileNotFoundException
        //     142	146	149	java/io/IOException
        //     85	90	154	java/io/IOException
        //     23	33	159	java/lang/Exception
        //     202	206	209	java/io/IOException
        //     23	33	214	finally
        //     161	198	214	finally
        //     220	224	227	java/io/IOException
        //     33	75	232	finally
        //     105	130	232	finally
        //     33	75	240	java/lang/Exception
        //     105	130	240	java/lang/Exception
        //     23	33	248	java/io/FileNotFoundException
    }

    private int getHoverTapSlop()
    {
        return ViewConfiguration.getHoverTapSlop();
    }

    private int getHoverTapTimeout()
    {
        return ViewConfiguration.getHoverTapTimeout();
    }

    private int getKeyRepeatDelay()
    {
        return ViewConfiguration.getKeyRepeatDelay();
    }

    private int getKeyRepeatTimeout()
    {
        return ViewConfiguration.getKeyRepeatTimeout();
    }

    private String[] getKeyboardLayoutOverlay(String paramString)
    {
        final String[] arrayOfString;
        if (!this.mSystemReady)
            arrayOfString = null;
        while (true)
        {
            return arrayOfString;
            String str = getCurrentKeyboardLayoutForInputDevice(paramString);
            if (str == null)
            {
                arrayOfString = null;
            }
            else
            {
                arrayOfString = new String[2];
                visitKeyboardLayout(str, new KeyboardLayoutVisitor()
                {
                    public void visitKeyboardLayout(Resources paramAnonymousResources, String paramAnonymousString1, String paramAnonymousString2, String paramAnonymousString3, int paramAnonymousInt)
                    {
                        try
                        {
                            arrayOfString[0] = paramAnonymousString1;
                            arrayOfString[1] = Streams.readFully(new InputStreamReader(paramAnonymousResources.openRawResource(paramAnonymousInt)));
                            label29: return;
                        }
                        catch (Resources.NotFoundException localNotFoundException)
                        {
                            break label29;
                        }
                        catch (IOException localIOException)
                        {
                            break label29;
                        }
                    }
                });
                if (arrayOfString[0] == null)
                {
                    Log.w("InputManager", "Could not get keyboard layout with descriptor '" + str + "'.");
                    arrayOfString = null;
                }
            }
        }
    }

    private int getLongPressTimeout()
    {
        return ViewConfiguration.getLongPressTimeout();
    }

    private PointerIcon getPointerIcon()
    {
        return PointerIcon.getDefaultIcon(this.mContext);
    }

    private int getPointerLayer()
    {
        return this.mCallbacks.getPointerLayer();
    }

    private int getPointerSpeedSetting()
    {
        int i = 0;
        try
        {
            int j = Settings.System.getInt(this.mContext.getContentResolver(), "pointer_speed");
            i = j;
            label18: return i;
        }
        catch (Settings.SettingNotFoundException localSettingNotFoundException)
        {
            break label18;
        }
    }

    private int getShowTouchesSetting(int paramInt)
    {
        int i = paramInt;
        try
        {
            int j = Settings.System.getInt(this.mContext.getContentResolver(), "show_touches");
            i = j;
            label20: return i;
        }
        catch (Settings.SettingNotFoundException localSettingNotFoundException)
        {
            break label20;
        }
    }

    private int getVirtualKeyQuietTimeMillis()
    {
        return this.mContext.getResources().getInteger(17694756);
    }

    // ERROR //
    private void handleSwitchKeyboardLayout(int paramInt1, int paramInt2)
    {
        // Byte code:
        //     0: aload_0
        //     1: iload_1
        //     2: invokevirtual 551	com/android/server/input/InputManagerService:getInputDevice	(I)Landroid/view/InputDevice;
        //     5: astore_3
        //     6: aload_3
        //     7: invokevirtual 307	android/view/InputDevice:getDescriptor	()Ljava/lang/String;
        //     10: astore 4
        //     12: aload_3
        //     13: ifnull +115 -> 128
        //     16: aload_0
        //     17: getfield 132	com/android/server/input/InputManagerService:mDataStore	Lcom/android/server/input/PersistentDataStore;
        //     20: astore 5
        //     22: aload 5
        //     24: monitorenter
        //     25: aload_0
        //     26: getfield 132	com/android/server/input/InputManagerService:mDataStore	Lcom/android/server/input/PersistentDataStore;
        //     29: aload 4
        //     31: iload_2
        //     32: invokevirtual 555	com/android/server/input/PersistentDataStore:switchKeyboardLayout	(Ljava/lang/String;I)Z
        //     35: istore 8
        //     37: aload_0
        //     38: getfield 132	com/android/server/input/InputManagerService:mDataStore	Lcom/android/server/input/PersistentDataStore;
        //     41: aload 4
        //     43: invokevirtual 360	com/android/server/input/PersistentDataStore:getCurrentKeyboardLayout	(Ljava/lang/String;)Ljava/lang/String;
        //     46: astore 9
        //     48: aload_0
        //     49: getfield 132	com/android/server/input/InputManagerService:mDataStore	Lcom/android/server/input/PersistentDataStore;
        //     52: invokevirtual 558	com/android/server/input/PersistentDataStore:saveIfNeeded	()V
        //     55: aload 5
        //     57: monitorexit
        //     58: iload 8
        //     60: ifeq +68 -> 128
        //     63: aload_0
        //     64: getfield 560	com/android/server/input/InputManagerService:mSwitchedKeyboardLayoutToast	Landroid/widget/Toast;
        //     67: ifnull +15 -> 82
        //     70: aload_0
        //     71: getfield 560	com/android/server/input/InputManagerService:mSwitchedKeyboardLayoutToast	Landroid/widget/Toast;
        //     74: invokevirtual 565	android/widget/Toast:cancel	()V
        //     77: aload_0
        //     78: aconst_null
        //     79: putfield 560	com/android/server/input/InputManagerService:mSwitchedKeyboardLayoutToast	Landroid/widget/Toast;
        //     82: aload 9
        //     84: ifnull +40 -> 124
        //     87: aload_0
        //     88: aload 9
        //     90: invokevirtual 569	com/android/server/input/InputManagerService:getKeyboardLayout	(Ljava/lang/String;)Landroid/hardware/input/KeyboardLayout;
        //     93: astore 10
        //     95: aload 10
        //     97: ifnull +27 -> 124
        //     100: aload_0
        //     101: aload_0
        //     102: getfield 164	com/android/server/input/InputManagerService:mContext	Landroid/content/Context;
        //     105: aload 10
        //     107: invokevirtual 574	android/hardware/input/KeyboardLayout:getLabel	()Ljava/lang/String;
        //     110: iconst_0
        //     111: invokestatic 578	android/widget/Toast:makeText	(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
        //     114: putfield 560	com/android/server/input/InputManagerService:mSwitchedKeyboardLayoutToast	Landroid/widget/Toast;
        //     117: aload_0
        //     118: getfield 560	com/android/server/input/InputManagerService:mSwitchedKeyboardLayoutToast	Landroid/widget/Toast;
        //     121: invokevirtual 581	android/widget/Toast:show	()V
        //     124: aload_0
        //     125: invokespecial 220	com/android/server/input/InputManagerService:reloadKeyboardLayouts	()V
        //     128: return
        //     129: astore 6
        //     131: aload_0
        //     132: getfield 132	com/android/server/input/InputManagerService:mDataStore	Lcom/android/server/input/PersistentDataStore;
        //     135: invokevirtual 558	com/android/server/input/PersistentDataStore:saveIfNeeded	()V
        //     138: aload 6
        //     140: athrow
        //     141: astore 7
        //     143: aload 5
        //     145: monitorexit
        //     146: aload 7
        //     148: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     25	48	129	finally
        //     48	58	141	finally
        //     131	146	141	finally
    }

    private void hideMissingKeyboardLayoutNotification()
    {
        if (this.mKeyboardLayoutNotificationShown)
        {
            this.mKeyboardLayoutNotificationShown = false;
            this.mNotificationManager.cancel(17040459);
        }
    }

    private long interceptKeyBeforeDispatching(InputWindowHandle paramInputWindowHandle, KeyEvent paramKeyEvent, int paramInt)
    {
        return this.mCallbacks.interceptKeyBeforeDispatching(paramInputWindowHandle, paramKeyEvent, paramInt);
    }

    private int interceptKeyBeforeQueueing(KeyEvent paramKeyEvent, int paramInt, boolean paramBoolean)
    {
        return this.mCallbacks.interceptKeyBeforeQueueing(paramKeyEvent, paramInt, paramBoolean);
    }

    private int interceptMotionBeforeQueueingWhenScreenOff(int paramInt)
    {
        return this.mCallbacks.interceptMotionBeforeQueueingWhenScreenOff(paramInt);
    }

    private static native void nativeCancelVibrate(int paramInt1, int paramInt2, int paramInt3);

    private static native String nativeDump(int paramInt);

    private static native int nativeGetKeyCodeState(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    private static native int nativeGetScanCodeState(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    private static native int nativeGetSwitchState(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    private static native boolean nativeHasKeys(int paramInt1, int paramInt2, int paramInt3, int[] paramArrayOfInt, boolean[] paramArrayOfBoolean);

    private static native int nativeInit(InputManagerService paramInputManagerService, Context paramContext, MessageQueue paramMessageQueue);

    private static native int nativeInjectInputEvent(int paramInt1, InputEvent paramInputEvent, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6);

    private static native void nativeMonitor(int paramInt);

    private static native void nativeRegisterInputChannel(int paramInt, InputChannel paramInputChannel, InputWindowHandle paramInputWindowHandle, boolean paramBoolean);

    private static native void nativeReloadDeviceAliases(int paramInt);

    private static native void nativeReloadKeyboardLayouts(int paramInt);

    private static native void nativeSetDisplayOrientation(int paramInt1, int paramInt2, int paramInt3, int paramInt4);

    private static native void nativeSetDisplaySize(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6);

    private static native void nativeSetFocusedApplication(int paramInt, InputApplicationHandle paramInputApplicationHandle);

    private static native void nativeSetInputDispatchMode(int paramInt, boolean paramBoolean1, boolean paramBoolean2);

    private static native void nativeSetInputFilterEnabled(int paramInt, boolean paramBoolean);

    private static native void nativeSetInputWindows(int paramInt, InputWindowHandle[] paramArrayOfInputWindowHandle);

    private static native void nativeSetPointerSpeed(int paramInt1, int paramInt2);

    private static native void nativeSetShowTouches(int paramInt, boolean paramBoolean);

    private static native void nativeSetSystemUiVisibility(int paramInt1, int paramInt2);

    private static native void nativeStart(int paramInt);

    private static native boolean nativeTransferTouchFocus(int paramInt, InputChannel paramInputChannel1, InputChannel paramInputChannel2);

    private static native void nativeUnregisterInputChannel(int paramInt, InputChannel paramInputChannel);

    private static native void nativeVibrate(int paramInt1, int paramInt2, long[] paramArrayOfLong, int paramInt3, int paramInt4);

    private long notifyANR(InputApplicationHandle paramInputApplicationHandle, InputWindowHandle paramInputWindowHandle)
    {
        return this.mCallbacks.notifyANR(paramInputApplicationHandle, paramInputWindowHandle);
    }

    private void notifyConfigurationChanged(long paramLong)
    {
        this.mCallbacks.notifyConfigurationChanged();
    }

    private void notifyInputChannelBroken(InputWindowHandle paramInputWindowHandle)
    {
        this.mCallbacks.notifyInputChannelBroken(paramInputWindowHandle);
    }

    private void notifyInputDevicesChanged(InputDevice[] paramArrayOfInputDevice)
    {
        synchronized (this.mInputDevicesLock)
        {
            if (!this.mInputDevicesChangedPending)
            {
                this.mInputDevicesChangedPending = true;
                this.mHandler.obtainMessage(1, this.mInputDevices).sendToTarget();
            }
            this.mInputDevices = paramArrayOfInputDevice;
            return;
        }
    }

    private void notifyLidSwitchChanged(long paramLong, boolean paramBoolean)
    {
        this.mCallbacks.notifyLidSwitchChanged(paramLong, paramBoolean);
    }

    private void onInputDevicesChangedListenerDied(int paramInt)
    {
        synchronized (this.mInputDevicesLock)
        {
            this.mInputDevicesChangedListeners.remove(paramInt);
            return;
        }
    }

    private void registerPointerSpeedSettingObserver()
    {
        this.mContext.getContentResolver().registerContentObserver(Settings.System.getUriFor("pointer_speed"), true, new ContentObserver(this.mHandler)
        {
            public void onChange(boolean paramAnonymousBoolean)
            {
                InputManagerService.this.updatePointerSpeedFromSettings();
            }
        });
    }

    private void registerShowTouchesSettingObserver()
    {
        this.mContext.getContentResolver().registerContentObserver(Settings.System.getUriFor("show_touches"), true, new ContentObserver(this.mHandler)
        {
            public void onChange(boolean paramAnonymousBoolean)
            {
                InputManagerService.this.updateShowTouchesFromSettings();
            }
        });
    }

    private void reloadDeviceAliases()
    {
        nativeReloadDeviceAliases(this.mPtr);
    }

    private void reloadKeyboardLayouts()
    {
        nativeReloadKeyboardLayouts(this.mPtr);
    }

    private void setPointerSpeedUnchecked(int paramInt)
    {
        int i = Math.min(Math.max(paramInt, -7), 7);
        nativeSetPointerSpeed(this.mPtr, i);
    }

    private void showMissingKeyboardLayoutNotification()
    {
        if (!this.mKeyboardLayoutNotificationShown)
        {
            if (this.mKeyboardLayoutIntent == null)
            {
                Intent localIntent = new Intent("android.settings.INPUT_METHOD_SETTINGS");
                localIntent.setFlags(337641472);
                this.mKeyboardLayoutIntent = PendingIntent.getActivity(this.mContext, 0, localIntent, 0);
            }
            Resources localResources = this.mContext.getResources();
            Notification localNotification = new Notification.Builder(this.mContext).setContentTitle(localResources.getString(17040459)).setContentText(localResources.getString(17040460)).setContentIntent(this.mKeyboardLayoutIntent).setSmallIcon(17302360).setPriority(-1).build();
            this.mNotificationManager.notify(17040459, localNotification);
            this.mKeyboardLayoutNotificationShown = true;
        }
    }

    // ERROR //
    private void updateKeyboardLayouts()
    {
        // Byte code:
        //     0: new 750	java/util/HashSet
        //     3: dup
        //     4: invokespecial 751	java/util/HashSet:<init>	()V
        //     7: astore_1
        //     8: aload_0
        //     9: new 12	com/android/server/input/InputManagerService$3
        //     12: dup
        //     13: aload_0
        //     14: aload_1
        //     15: invokespecial 754	com/android/server/input/InputManagerService$3:<init>	(Lcom/android/server/input/InputManagerService;Ljava/util/HashSet;)V
        //     18: invokespecial 758	com/android/server/input/InputManagerService:visitAllKeyboardLayouts	(Lcom/android/server/input/InputManagerService$KeyboardLayoutVisitor;)V
        //     21: aload_0
        //     22: getfield 132	com/android/server/input/InputManagerService:mDataStore	Lcom/android/server/input/PersistentDataStore;
        //     25: astore_2
        //     26: aload_2
        //     27: monitorenter
        //     28: aload_0
        //     29: getfield 132	com/android/server/input/InputManagerService:mDataStore	Lcom/android/server/input/PersistentDataStore;
        //     32: aload_1
        //     33: invokevirtual 762	com/android/server/input/PersistentDataStore:removeUninstalledKeyboardLayouts	(Ljava/util/Set;)Z
        //     36: pop
        //     37: aload_0
        //     38: getfield 132	com/android/server/input/InputManagerService:mDataStore	Lcom/android/server/input/PersistentDataStore;
        //     41: invokevirtual 558	com/android/server/input/PersistentDataStore:saveIfNeeded	()V
        //     44: aload_2
        //     45: monitorexit
        //     46: aload_0
        //     47: invokespecial 220	com/android/server/input/InputManagerService:reloadKeyboardLayouts	()V
        //     50: return
        //     51: astore_3
        //     52: aload_0
        //     53: getfield 132	com/android/server/input/InputManagerService:mDataStore	Lcom/android/server/input/PersistentDataStore;
        //     56: invokevirtual 558	com/android/server/input/PersistentDataStore:saveIfNeeded	()V
        //     59: aload_3
        //     60: athrow
        //     61: astore 4
        //     63: aload_2
        //     64: monitorexit
        //     65: aload 4
        //     67: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     28	37	51	finally
        //     37	46	61	finally
        //     52	65	61	finally
    }

    private void visitAllKeyboardLayouts(KeyboardLayoutVisitor paramKeyboardLayoutVisitor)
    {
        PackageManager localPackageManager = this.mContext.getPackageManager();
        Iterator localIterator = localPackageManager.queryBroadcastReceivers(new Intent("android.hardware.input.action.QUERY_KEYBOARD_LAYOUTS"), 128).iterator();
        while (localIterator.hasNext())
            visitKeyboardLayoutsInPackage(localPackageManager, ((ResolveInfo)localIterator.next()).activityInfo, null, paramKeyboardLayoutVisitor);
    }

    private void visitKeyboardLayout(String paramString, KeyboardLayoutVisitor paramKeyboardLayoutVisitor)
    {
        KeyboardLayoutDescriptor localKeyboardLayoutDescriptor = KeyboardLayoutDescriptor.parse(paramString);
        PackageManager localPackageManager;
        if (localKeyboardLayoutDescriptor != null)
            localPackageManager = this.mContext.getPackageManager();
        try
        {
            visitKeyboardLayoutsInPackage(localPackageManager, localPackageManager.getReceiverInfo(new ComponentName(localKeyboardLayoutDescriptor.packageName, localKeyboardLayoutDescriptor.receiverName), 128), localKeyboardLayoutDescriptor.keyboardLayoutName, paramKeyboardLayoutVisitor);
            label52: return;
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            break label52;
        }
    }

    // ERROR //
    private void visitKeyboardLayoutsInPackage(PackageManager paramPackageManager, android.content.pm.ActivityInfo paramActivityInfo, String paramString, KeyboardLayoutVisitor paramKeyboardLayoutVisitor)
    {
        // Byte code:
        //     0: aload_2
        //     1: getfield 829	android/content/pm/ActivityInfo:metaData	Landroid/os/Bundle;
        //     4: astore 5
        //     6: aload 5
        //     8: ifnonnull +4 -> 12
        //     11: return
        //     12: aload 5
        //     14: ldc_w 831
        //     17: invokevirtual 835	android/os/Bundle:getInt	(Ljava/lang/String;)I
        //     20: istore 6
        //     22: iload 6
        //     24: ifne +48 -> 72
        //     27: ldc 80
        //     29: new 268	java/lang/StringBuilder
        //     32: dup
        //     33: invokespecial 269	java/lang/StringBuilder:<init>	()V
        //     36: ldc_w 837
        //     39: invokevirtual 275	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     42: aload_2
        //     43: getfield 840	android/content/pm/ComponentInfo:packageName	Ljava/lang/String;
        //     46: invokevirtual 275	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     49: ldc_w 842
        //     52: invokevirtual 275	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     55: aload_2
        //     56: getfield 844	android/content/pm/ComponentInfo:name	Ljava/lang/String;
        //     59: invokevirtual 275	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     62: invokevirtual 291	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     65: invokestatic 503	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     68: pop
        //     69: goto -58 -> 11
        //     72: aload_2
        //     73: aload_1
        //     74: invokevirtual 848	android/content/pm/ActivityInfo:loadLabel	(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
        //     77: astore 7
        //     79: aload 7
        //     81: ifnull +115 -> 196
        //     84: aload 7
        //     86: invokevirtual 849	java/lang/Object:toString	()Ljava/lang/String;
        //     89: astore 8
        //     91: aload_1
        //     92: aload_2
        //     93: getfield 853	android/content/pm/ActivityInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     96: invokevirtual 857	android/content/pm/PackageManager:getResourcesForApplication	(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;
        //     99: astore 11
        //     101: aload 11
        //     103: iload 6
        //     105: invokevirtual 861	android/content/res/Resources:getXml	(I)Landroid/content/res/XmlResourceParser;
        //     108: astore 12
        //     110: aload 12
        //     112: ldc_w 863
        //     115: invokestatic 435	com/android/internal/util/XmlUtils:beginDocument	(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V
        //     118: aload 12
        //     120: invokestatic 439	com/android/internal/util/XmlUtils:nextElement	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     123: aload 12
        //     125: invokeinterface 866 1 0
        //     130: astore 14
        //     132: aload 14
        //     134: ifnonnull +70 -> 204
        //     137: aload 12
        //     139: invokeinterface 867 1 0
        //     144: goto -133 -> 11
        //     147: astore 9
        //     149: ldc 80
        //     151: new 268	java/lang/StringBuilder
        //     154: dup
        //     155: invokespecial 269	java/lang/StringBuilder:<init>	()V
        //     158: ldc_w 869
        //     161: invokevirtual 275	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     164: aload_2
        //     165: getfield 840	android/content/pm/ComponentInfo:packageName	Ljava/lang/String;
        //     168: invokevirtual 275	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     171: ldc_w 842
        //     174: invokevirtual 275	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     177: aload_2
        //     178: getfield 844	android/content/pm/ComponentInfo:name	Ljava/lang/String;
        //     181: invokevirtual 275	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     184: invokevirtual 291	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     187: aload 9
        //     189: invokestatic 871	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     192: pop
        //     193: goto -182 -> 11
        //     196: ldc_w 873
        //     199: astore 8
        //     201: goto -110 -> 91
        //     204: aload 14
        //     206: ldc_w 875
        //     209: invokevirtual 313	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     212: ifeq +175 -> 387
        //     215: aload 11
        //     217: aload 12
        //     219: getstatic 881	com/android/internal/R$styleable:KeyboardLayout	[I
        //     222: invokevirtual 885	android/content/res/Resources:obtainAttributes	(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
        //     225: astore 16
        //     227: aload 16
        //     229: iconst_1
        //     230: invokevirtual 888	android/content/res/TypedArray:getString	(I)Ljava/lang/String;
        //     233: astore 18
        //     235: aload 16
        //     237: iconst_0
        //     238: invokevirtual 888	android/content/res/TypedArray:getString	(I)Ljava/lang/String;
        //     241: astore 19
        //     243: aload 16
        //     245: iconst_2
        //     246: iconst_0
        //     247: invokevirtual 891	android/content/res/TypedArray:getResourceId	(II)I
        //     250: istore 20
        //     252: aload 18
        //     254: ifnull +13 -> 267
        //     257: aload 19
        //     259: ifnull +8 -> 267
        //     262: iload 20
        //     264: ifne +65 -> 329
        //     267: ldc 80
        //     269: new 268	java/lang/StringBuilder
        //     272: dup
        //     273: invokespecial 269	java/lang/StringBuilder:<init>	()V
        //     276: ldc_w 893
        //     279: invokevirtual 275	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     282: aload_2
        //     283: getfield 840	android/content/pm/ComponentInfo:packageName	Ljava/lang/String;
        //     286: invokevirtual 275	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     289: ldc_w 842
        //     292: invokevirtual 275	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     295: aload_2
        //     296: getfield 844	android/content/pm/ComponentInfo:name	Ljava/lang/String;
        //     299: invokevirtual 275	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     302: invokevirtual 291	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     305: invokestatic 503	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     308: pop
        //     309: aload 16
        //     311: invokevirtual 896	android/content/res/TypedArray:recycle	()V
        //     314: goto -196 -> 118
        //     317: astore 13
        //     319: aload 12
        //     321: invokeinterface 867 1 0
        //     326: aload 13
        //     328: athrow
        //     329: aload_2
        //     330: getfield 840	android/content/pm/ComponentInfo:packageName	Ljava/lang/String;
        //     333: aload_2
        //     334: getfield 844	android/content/pm/ComponentInfo:name	Ljava/lang/String;
        //     337: aload 18
        //     339: invokestatic 900	com/android/server/input/InputManagerService$KeyboardLayoutDescriptor:format	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //     342: astore 22
        //     344: aload_3
        //     345: ifnull +12 -> 357
        //     348: aload 18
        //     350: aload_3
        //     351: invokevirtual 313	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     354: ifeq -45 -> 309
        //     357: aload 4
        //     359: aload 11
        //     361: aload 22
        //     363: aload 19
        //     365: aload 8
        //     367: iload 20
        //     369: invokeinterface 903 6 0
        //     374: goto -65 -> 309
        //     377: astore 17
        //     379: aload 16
        //     381: invokevirtual 896	android/content/res/TypedArray:recycle	()V
        //     384: aload 17
        //     386: athrow
        //     387: ldc 80
        //     389: new 268	java/lang/StringBuilder
        //     392: dup
        //     393: invokespecial 269	java/lang/StringBuilder:<init>	()V
        //     396: ldc_w 905
        //     399: invokevirtual 275	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     402: aload 14
        //     404: invokevirtual 275	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     407: ldc_w 907
        //     410: invokevirtual 275	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     413: aload_2
        //     414: getfield 840	android/content/pm/ComponentInfo:packageName	Ljava/lang/String;
        //     417: invokevirtual 275	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     420: ldc_w 842
        //     423: invokevirtual 275	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     426: aload_2
        //     427: getfield 844	android/content/pm/ComponentInfo:name	Ljava/lang/String;
        //     430: invokevirtual 275	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     433: invokevirtual 291	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     436: invokestatic 503	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     439: pop
        //     440: goto -322 -> 118
        //
        // Exception table:
        //     from	to	target	type
        //     91	110	147	java/lang/Exception
        //     137	144	147	java/lang/Exception
        //     319	329	147	java/lang/Exception
        //     110	132	317	finally
        //     204	227	317	finally
        //     309	314	317	finally
        //     379	440	317	finally
        //     227	309	377	finally
        //     329	374	377	finally
    }

    // ERROR //
    public void addKeyboardLayoutForInputDevice(String paramString1, String paramString2)
    {
        // Byte code:
        //     0: aload_0
        //     1: ldc_w 910
        //     4: ldc_w 912
        //     7: invokespecial 914	com/android/server/input/InputManagerService:checkCallingPermission	(Ljava/lang/String;Ljava/lang/String;)Z
        //     10: ifne +14 -> 24
        //     13: new 916	java/lang/SecurityException
        //     16: dup
        //     17: ldc_w 918
        //     20: invokespecial 919	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     23: athrow
        //     24: aload_1
        //     25: ifnonnull +14 -> 39
        //     28: new 921	java/lang/IllegalArgumentException
        //     31: dup
        //     32: ldc_w 923
        //     35: invokespecial 924	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     38: athrow
        //     39: aload_2
        //     40: ifnonnull +14 -> 54
        //     43: new 921	java/lang/IllegalArgumentException
        //     46: dup
        //     47: ldc_w 926
        //     50: invokespecial 924	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     53: athrow
        //     54: aload_0
        //     55: getfield 132	com/android/server/input/InputManagerService:mDataStore	Lcom/android/server/input/PersistentDataStore;
        //     58: astore_3
        //     59: aload_3
        //     60: monitorenter
        //     61: aload_0
        //     62: getfield 132	com/android/server/input/InputManagerService:mDataStore	Lcom/android/server/input/PersistentDataStore;
        //     65: aload_1
        //     66: invokevirtual 360	com/android/server/input/PersistentDataStore:getCurrentKeyboardLayout	(Ljava/lang/String;)Ljava/lang/String;
        //     69: astore 6
        //     71: aload_0
        //     72: getfield 132	com/android/server/input/InputManagerService:mDataStore	Lcom/android/server/input/PersistentDataStore;
        //     75: aload_1
        //     76: aload_2
        //     77: invokevirtual 929	com/android/server/input/PersistentDataStore:addKeyboardLayout	(Ljava/lang/String;Ljava/lang/String;)Z
        //     80: ifeq +28 -> 108
        //     83: aload 6
        //     85: aload_0
        //     86: getfield 132	com/android/server/input/InputManagerService:mDataStore	Lcom/android/server/input/PersistentDataStore;
        //     89: aload_1
        //     90: invokevirtual 360	com/android/server/input/PersistentDataStore:getCurrentKeyboardLayout	(Ljava/lang/String;)Ljava/lang/String;
        //     93: invokestatic 935	libcore/util/Objects:equal	(Ljava/lang/Object;Ljava/lang/Object;)Z
        //     96: ifne +12 -> 108
        //     99: aload_0
        //     100: getfield 171	com/android/server/input/InputManagerService:mHandler	Lcom/android/server/input/InputManagerService$InputManagerHandler;
        //     103: iconst_3
        //     104: invokevirtual 939	com/android/server/input/InputManagerService$InputManagerHandler:sendEmptyMessage	(I)Z
        //     107: pop
        //     108: aload_0
        //     109: getfield 132	com/android/server/input/InputManagerService:mDataStore	Lcom/android/server/input/PersistentDataStore;
        //     112: invokevirtual 558	com/android/server/input/PersistentDataStore:saveIfNeeded	()V
        //     115: aload_3
        //     116: monitorexit
        //     117: return
        //     118: astore 4
        //     120: aload_0
        //     121: getfield 132	com/android/server/input/InputManagerService:mDataStore	Lcom/android/server/input/PersistentDataStore;
        //     124: invokevirtual 558	com/android/server/input/PersistentDataStore:saveIfNeeded	()V
        //     127: aload 4
        //     129: athrow
        //     130: astore 5
        //     132: aload_3
        //     133: monitorexit
        //     134: aload 5
        //     136: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     61	108	118	finally
        //     108	134	130	finally
    }

    public void cancelVibrate(int paramInt, IBinder paramIBinder)
    {
        synchronized (this.mVibratorLock)
        {
            VibratorToken localVibratorToken = (VibratorToken)this.mVibratorTokens.get(paramIBinder);
            if ((localVibratorToken == null) || (localVibratorToken.mDeviceId == paramInt))
                cancelVibrateIfNeeded(localVibratorToken);
        }
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.DUMP") != 0)
            paramPrintWriter.println("Permission Denial: can't dump InputManager from from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid());
        while (true)
        {
            return;
            paramPrintWriter.println("INPUT MANAGER (dumpsys input)\n");
            String str = nativeDump(this.mPtr);
            if (str != null)
                paramPrintWriter.println(str);
        }
    }

    final boolean filterInputEvent(InputEvent paramInputEvent, int paramInt)
    {
        boolean bool;
        synchronized (this.mInputFilterLock)
        {
            if (this.mInputFilter != null)
            {
                this.mInputFilter.filterInputEvent(paramInputEvent, paramInt);
                bool = false;
            }
            else
            {
                paramInputEvent.recycle();
                bool = true;
            }
        }
        return bool;
    }

    public String getCurrentKeyboardLayoutForInputDevice(String paramString)
    {
        if (paramString == null)
            throw new IllegalArgumentException("inputDeviceDescriptor must not be null");
        synchronized (this.mDataStore)
        {
            String str = this.mDataStore.getCurrentKeyboardLayout(paramString);
            return str;
        }
    }

    public InputDevice getInputDevice(int paramInt)
    {
        while (true)
        {
            int j;
            InputDevice localInputDevice;
            synchronized (this.mInputDevicesLock)
            {
                int i = this.mInputDevices.length;
                j = 0;
                if (j < i)
                {
                    localInputDevice = this.mInputDevices[j];
                    if (localInputDevice.getId() != paramInt)
                        break label63;
                }
                else
                {
                    localInputDevice = null;
                }
            }
            label63: j++;
        }
    }

    public int[] getInputDeviceIds()
    {
        synchronized (this.mInputDevicesLock)
        {
            int i = this.mInputDevices.length;
            int[] arrayOfInt = new int[i];
            for (int j = 0; j < i; j++)
                arrayOfInt[j] = this.mInputDevices[j].getId();
            return arrayOfInt;
        }
    }

    public InputDevice[] getInputDevices()
    {
        synchronized (this.mInputDevicesLock)
        {
            InputDevice[] arrayOfInputDevice = this.mInputDevices;
            return arrayOfInputDevice;
        }
    }

    public int getKeyCodeState(int paramInt1, int paramInt2, int paramInt3)
    {
        return nativeGetKeyCodeState(this.mPtr, paramInt1, paramInt2, paramInt3);
    }

    public KeyboardLayout getKeyboardLayout(String paramString)
    {
        if (paramString == null)
            throw new IllegalArgumentException("keyboardLayoutDescriptor must not be null");
        final KeyboardLayout[] arrayOfKeyboardLayout = new KeyboardLayout[1];
        visitKeyboardLayout(paramString, new KeyboardLayoutVisitor()
        {
            public void visitKeyboardLayout(Resources paramAnonymousResources, String paramAnonymousString1, String paramAnonymousString2, String paramAnonymousString3, int paramAnonymousInt)
            {
                arrayOfKeyboardLayout[0] = new KeyboardLayout(paramAnonymousString1, paramAnonymousString2, paramAnonymousString3);
            }
        });
        if (arrayOfKeyboardLayout[0] == null)
            Log.w("InputManager", "Could not get keyboard layout with descriptor '" + paramString + "'.");
        return arrayOfKeyboardLayout[0];
    }

    public KeyboardLayout[] getKeyboardLayouts()
    {
        final ArrayList localArrayList = new ArrayList();
        visitAllKeyboardLayouts(new KeyboardLayoutVisitor()
        {
            public void visitKeyboardLayout(Resources paramAnonymousResources, String paramAnonymousString1, String paramAnonymousString2, String paramAnonymousString3, int paramAnonymousInt)
            {
                localArrayList.add(new KeyboardLayout(paramAnonymousString1, paramAnonymousString2, paramAnonymousString3));
            }
        });
        return (KeyboardLayout[])localArrayList.toArray(new KeyboardLayout[localArrayList.size()]);
    }

    public String[] getKeyboardLayoutsForInputDevice(String paramString)
    {
        if (paramString == null)
            throw new IllegalArgumentException("inputDeviceDescriptor must not be null");
        synchronized (this.mDataStore)
        {
            String[] arrayOfString = this.mDataStore.getKeyboardLayouts(paramString);
            return arrayOfString;
        }
    }

    public int getScanCodeState(int paramInt1, int paramInt2, int paramInt3)
    {
        return nativeGetScanCodeState(this.mPtr, paramInt1, paramInt2, paramInt3);
    }

    public int getSwitchState(int paramInt1, int paramInt2, int paramInt3)
    {
        return nativeGetSwitchState(this.mPtr, paramInt1, paramInt2, paramInt3);
    }

    public boolean hasKeys(int paramInt1, int paramInt2, int[] paramArrayOfInt, boolean[] paramArrayOfBoolean)
    {
        if (paramArrayOfInt == null)
            throw new IllegalArgumentException("keyCodes must not be null.");
        if ((paramArrayOfBoolean == null) || (paramArrayOfBoolean.length < paramArrayOfInt.length))
            throw new IllegalArgumentException("keyExists must not be null and must be at least as large as keyCodes.");
        return nativeHasKeys(this.mPtr, paramInt1, paramInt2, paramArrayOfInt, paramArrayOfBoolean);
    }

    public boolean injectInputEvent(InputEvent paramInputEvent, int paramInt)
    {
        if (paramInputEvent == null)
            throw new IllegalArgumentException("event must not be null");
        if ((paramInt != 0) && (paramInt != 2) && (paramInt != 1))
            throw new IllegalArgumentException("mode is invalid");
        int i = Binder.getCallingPid();
        int j = Binder.getCallingUid();
        long l = Binder.clearCallingIdentity();
        while (true)
        {
            try
            {
                int k = nativeInjectInputEvent(this.mPtr, paramInputEvent, i, j, paramInt, 30000, 134217728);
                Binder.restoreCallingIdentity(l);
                switch (k)
                {
                case 2:
                default:
                    Slog.w("InputManager", "Input event injection from pid " + i + " failed.");
                    bool = false;
                    return bool;
                case 1:
                case 0:
                case 3:
                }
            }
            finally
            {
                Binder.restoreCallingIdentity(l);
            }
            Slog.w("InputManager", "Input event injection from pid " + i + " permission denied.");
            throw new SecurityException("Injecting to another application requires INJECT_EVENTS permission");
            boolean bool = true;
            continue;
            Slog.w("InputManager", "Input event injection from pid " + i + " timed out.");
            bool = false;
        }
    }

    public void monitor()
    {
        synchronized (this.mInputFilterLock)
        {
            nativeMonitor(this.mPtr);
        }
        throw localObject2;
    }

    public InputChannel monitorInput(String paramString)
    {
        if (paramString == null)
            throw new IllegalArgumentException("inputChannelName must not be null.");
        InputChannel[] arrayOfInputChannel = InputChannel.openInputChannelPair(paramString);
        nativeRegisterInputChannel(this.mPtr, arrayOfInputChannel[0], null, true);
        arrayOfInputChannel[0].dispose();
        return arrayOfInputChannel[1];
    }

    void onVibratorTokenDied(VibratorToken paramVibratorToken)
    {
        synchronized (this.mVibratorLock)
        {
            this.mVibratorTokens.remove(paramVibratorToken.mToken);
            cancelVibrateIfNeeded(paramVibratorToken);
            return;
        }
    }

    public void registerInputChannel(InputChannel paramInputChannel, InputWindowHandle paramInputWindowHandle)
    {
        if (paramInputChannel == null)
            throw new IllegalArgumentException("inputChannel must not be null.");
        nativeRegisterInputChannel(this.mPtr, paramInputChannel, paramInputWindowHandle, false);
    }

    // ERROR //
    public void registerInputDevicesChangedListener(IInputDevicesChangedListener paramIInputDevicesChangedListener)
    {
        // Byte code:
        //     0: aload_1
        //     1: ifnonnull +14 -> 15
        //     4: new 921	java/lang/IllegalArgumentException
        //     7: dup
        //     8: ldc_w 1069
        //     11: invokespecial 924	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     14: athrow
        //     15: aload_0
        //     16: getfield 137	com/android/server/input/InputManagerService:mInputDevicesLock	Ljava/lang/Object;
        //     19: astore_2
        //     20: aload_2
        //     21: monitorenter
        //     22: invokestatic 256	android/os/Binder:getCallingPid	()I
        //     25: istore 4
        //     27: aload_0
        //     28: getfield 146	com/android/server/input/InputManagerService:mInputDevicesChangedListeners	Landroid/util/SparseArray;
        //     31: iload 4
        //     33: invokevirtual 1070	android/util/SparseArray:get	(I)Ljava/lang/Object;
        //     36: ifnull +19 -> 55
        //     39: new 916	java/lang/SecurityException
        //     42: dup
        //     43: ldc_w 1072
        //     46: invokespecial 919	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     49: athrow
        //     50: astore_3
        //     51: aload_2
        //     52: monitorexit
        //     53: aload_3
        //     54: athrow
        //     55: new 27	com/android/server/input/InputManagerService$InputDevicesChangedListenerRecord
        //     58: dup
        //     59: aload_0
        //     60: iload 4
        //     62: aload_1
        //     63: invokespecial 1075	com/android/server/input/InputManagerService$InputDevicesChangedListenerRecord:<init>	(Lcom/android/server/input/InputManagerService;ILandroid/hardware/input/IInputDevicesChangedListener;)V
        //     66: astore 5
        //     68: aload_1
        //     69: invokeinterface 1081 1 0
        //     74: aload 5
        //     76: iconst_0
        //     77: invokeinterface 1087 3 0
        //     82: aload_0
        //     83: getfield 146	com/android/server/input/InputManagerService:mInputDevicesChangedListeners	Landroid/util/SparseArray;
        //     86: iload 4
        //     88: aload 5
        //     90: invokevirtual 1090	android/util/SparseArray:put	(ILjava/lang/Object;)V
        //     93: aload_2
        //     94: monitorexit
        //     95: return
        //     96: astore 6
        //     98: new 1092	java/lang/RuntimeException
        //     101: dup
        //     102: aload 6
        //     104: invokespecial 1095	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
        //     107: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     22	53	50	finally
        //     55	68	50	finally
        //     68	82	50	finally
        //     82	108	50	finally
        //     68	82	96	android/os/RemoteException
    }

    // ERROR //
    public void removeKeyboardLayoutForInputDevice(String paramString1, String paramString2)
    {
        // Byte code:
        //     0: aload_0
        //     1: ldc_w 910
        //     4: ldc_w 1098
        //     7: invokespecial 914	com/android/server/input/InputManagerService:checkCallingPermission	(Ljava/lang/String;Ljava/lang/String;)Z
        //     10: ifne +14 -> 24
        //     13: new 916	java/lang/SecurityException
        //     16: dup
        //     17: ldc_w 918
        //     20: invokespecial 919	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     23: athrow
        //     24: aload_1
        //     25: ifnonnull +14 -> 39
        //     28: new 921	java/lang/IllegalArgumentException
        //     31: dup
        //     32: ldc_w 923
        //     35: invokespecial 924	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     38: athrow
        //     39: aload_2
        //     40: ifnonnull +14 -> 54
        //     43: new 921	java/lang/IllegalArgumentException
        //     46: dup
        //     47: ldc_w 926
        //     50: invokespecial 924	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     53: athrow
        //     54: aload_0
        //     55: getfield 132	com/android/server/input/InputManagerService:mDataStore	Lcom/android/server/input/PersistentDataStore;
        //     58: astore_3
        //     59: aload_3
        //     60: monitorenter
        //     61: aload_0
        //     62: getfield 132	com/android/server/input/InputManagerService:mDataStore	Lcom/android/server/input/PersistentDataStore;
        //     65: aload_1
        //     66: invokevirtual 360	com/android/server/input/PersistentDataStore:getCurrentKeyboardLayout	(Ljava/lang/String;)Ljava/lang/String;
        //     69: astore 6
        //     71: aload_0
        //     72: getfield 132	com/android/server/input/InputManagerService:mDataStore	Lcom/android/server/input/PersistentDataStore;
        //     75: aload_1
        //     76: aload_2
        //     77: invokevirtual 1101	com/android/server/input/PersistentDataStore:removeKeyboardLayout	(Ljava/lang/String;Ljava/lang/String;)Z
        //     80: ifeq +28 -> 108
        //     83: aload 6
        //     85: aload_0
        //     86: getfield 132	com/android/server/input/InputManagerService:mDataStore	Lcom/android/server/input/PersistentDataStore;
        //     89: aload_1
        //     90: invokevirtual 360	com/android/server/input/PersistentDataStore:getCurrentKeyboardLayout	(Ljava/lang/String;)Ljava/lang/String;
        //     93: invokestatic 935	libcore/util/Objects:equal	(Ljava/lang/Object;Ljava/lang/Object;)Z
        //     96: ifne +12 -> 108
        //     99: aload_0
        //     100: getfield 171	com/android/server/input/InputManagerService:mHandler	Lcom/android/server/input/InputManagerService$InputManagerHandler;
        //     103: iconst_3
        //     104: invokevirtual 939	com/android/server/input/InputManagerService$InputManagerHandler:sendEmptyMessage	(I)Z
        //     107: pop
        //     108: aload_0
        //     109: getfield 132	com/android/server/input/InputManagerService:mDataStore	Lcom/android/server/input/PersistentDataStore;
        //     112: invokevirtual 558	com/android/server/input/PersistentDataStore:saveIfNeeded	()V
        //     115: aload_3
        //     116: monitorexit
        //     117: return
        //     118: astore 4
        //     120: aload_0
        //     121: getfield 132	com/android/server/input/InputManagerService:mDataStore	Lcom/android/server/input/PersistentDataStore;
        //     124: invokevirtual 558	com/android/server/input/PersistentDataStore:saveIfNeeded	()V
        //     127: aload 4
        //     129: athrow
        //     130: astore 5
        //     132: aload_3
        //     133: monitorexit
        //     134: aload 5
        //     136: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     61	108	118	finally
        //     108	134	130	finally
    }

    // ERROR //
    public void setCurrentKeyboardLayoutForInputDevice(String paramString1, String paramString2)
    {
        // Byte code:
        //     0: aload_0
        //     1: ldc_w 910
        //     4: ldc_w 1104
        //     7: invokespecial 914	com/android/server/input/InputManagerService:checkCallingPermission	(Ljava/lang/String;Ljava/lang/String;)Z
        //     10: ifne +14 -> 24
        //     13: new 916	java/lang/SecurityException
        //     16: dup
        //     17: ldc_w 918
        //     20: invokespecial 919	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     23: athrow
        //     24: aload_1
        //     25: ifnonnull +14 -> 39
        //     28: new 921	java/lang/IllegalArgumentException
        //     31: dup
        //     32: ldc_w 923
        //     35: invokespecial 924	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     38: athrow
        //     39: aload_2
        //     40: ifnonnull +14 -> 54
        //     43: new 921	java/lang/IllegalArgumentException
        //     46: dup
        //     47: ldc_w 926
        //     50: invokespecial 924	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     53: athrow
        //     54: aload_0
        //     55: getfield 132	com/android/server/input/InputManagerService:mDataStore	Lcom/android/server/input/PersistentDataStore;
        //     58: astore_3
        //     59: aload_3
        //     60: monitorenter
        //     61: aload_0
        //     62: getfield 132	com/android/server/input/InputManagerService:mDataStore	Lcom/android/server/input/PersistentDataStore;
        //     65: aload_1
        //     66: aload_2
        //     67: invokevirtual 1107	com/android/server/input/PersistentDataStore:setCurrentKeyboardLayout	(Ljava/lang/String;Ljava/lang/String;)Z
        //     70: ifeq +12 -> 82
        //     73: aload_0
        //     74: getfield 171	com/android/server/input/InputManagerService:mHandler	Lcom/android/server/input/InputManagerService$InputManagerHandler;
        //     77: iconst_3
        //     78: invokevirtual 939	com/android/server/input/InputManagerService$InputManagerHandler:sendEmptyMessage	(I)Z
        //     81: pop
        //     82: aload_0
        //     83: getfield 132	com/android/server/input/InputManagerService:mDataStore	Lcom/android/server/input/PersistentDataStore;
        //     86: invokevirtual 558	com/android/server/input/PersistentDataStore:saveIfNeeded	()V
        //     89: aload_3
        //     90: monitorexit
        //     91: return
        //     92: astore 4
        //     94: aload_0
        //     95: getfield 132	com/android/server/input/InputManagerService:mDataStore	Lcom/android/server/input/PersistentDataStore;
        //     98: invokevirtual 558	com/android/server/input/PersistentDataStore:saveIfNeeded	()V
        //     101: aload 4
        //     103: athrow
        //     104: astore 5
        //     106: aload_3
        //     107: monitorexit
        //     108: aload 5
        //     110: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     61	82	92	finally
        //     82	108	104	finally
    }

    public void setDisplayOrientation(int paramInt1, int paramInt2, int paramInt3)
    {
        if ((paramInt2 < 0) || (paramInt2 > 3))
            throw new IllegalArgumentException("Invalid rotation.");
        nativeSetDisplayOrientation(this.mPtr, paramInt1, paramInt2, paramInt3);
    }

    public void setDisplaySize(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        if ((paramInt2 <= 0) || (paramInt3 <= 0) || (paramInt4 <= 0) || (paramInt5 <= 0))
            throw new IllegalArgumentException("Invalid display id or dimensions.");
        nativeSetDisplaySize(this.mPtr, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
    }

    public void setFocusedApplication(InputApplicationHandle paramInputApplicationHandle)
    {
        nativeSetFocusedApplication(this.mPtr, paramInputApplicationHandle);
    }

    public void setInputDispatchMode(boolean paramBoolean1, boolean paramBoolean2)
    {
        nativeSetInputDispatchMode(this.mPtr, paramBoolean1, paramBoolean2);
    }

    public void setInputFilter(InputFilter paramInputFilter)
    {
        while (true)
        {
            synchronized (this.mInputFilterLock)
            {
                InputFilter localInputFilter = this.mInputFilter;
                if (localInputFilter == paramInputFilter)
                    break;
                if (localInputFilter != null)
                {
                    this.mInputFilter = null;
                    this.mInputFilterHost.disconnectLocked();
                    this.mInputFilterHost = null;
                    localInputFilter.uninstall();
                }
                if (paramInputFilter != null)
                {
                    this.mInputFilter = paramInputFilter;
                    this.mInputFilterHost = new InputFilterHost(null);
                    paramInputFilter.install(this.mInputFilterHost);
                }
                int i = this.mPtr;
                if (paramInputFilter != null)
                {
                    bool = true;
                    nativeSetInputFilterEnabled(i, bool);
                }
            }
            boolean bool = false;
        }
    }

    public void setInputWindows(InputWindowHandle[] paramArrayOfInputWindowHandle)
    {
        nativeSetInputWindows(this.mPtr, paramArrayOfInputWindowHandle);
    }

    public void setSystemUiVisibility(int paramInt)
    {
        nativeSetSystemUiVisibility(this.mPtr, paramInt);
    }

    public void start()
    {
        Slog.i("InputManager", "Starting input manager");
        nativeStart(this.mPtr);
        Watchdog.getInstance().addMonitor(this);
        registerPointerSpeedSettingObserver();
        registerShowTouchesSettingObserver();
        updatePointerSpeedFromSettings();
        updateShowTouchesFromSettings();
    }

    public void switchKeyboardLayout(int paramInt1, int paramInt2)
    {
        this.mHandler.obtainMessage(2, paramInt1, paramInt2).sendToTarget();
    }

    public void systemReady(BluetoothService paramBluetoothService)
    {
        this.mBluetoothService = paramBluetoothService;
        this.mNotificationManager = ((NotificationManager)this.mContext.getSystemService("notification"));
        this.mSystemReady = true;
        IntentFilter localIntentFilter1 = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        localIntentFilter1.addAction("android.intent.action.PACKAGE_REMOVED");
        localIntentFilter1.addAction("android.intent.action.PACKAGE_CHANGED");
        localIntentFilter1.addDataScheme("package");
        this.mContext.registerReceiver(new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                InputManagerService.this.updateKeyboardLayouts();
            }
        }
        , localIntentFilter1, null, this.mHandler);
        IntentFilter localIntentFilter2 = new IntentFilter("android.bluetooth.device.action.ALIAS_CHANGED");
        this.mContext.registerReceiver(new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                InputManagerService.this.reloadDeviceAliases();
            }
        }
        , localIntentFilter2, null, this.mHandler);
        this.mHandler.sendEmptyMessage(5);
        this.mHandler.sendEmptyMessage(4);
    }

    public boolean transferTouchFocus(InputChannel paramInputChannel1, InputChannel paramInputChannel2)
    {
        if (paramInputChannel1 == null)
            throw new IllegalArgumentException("fromChannel must not be null.");
        if (paramInputChannel2 == null)
            throw new IllegalArgumentException("toChannel must not be null.");
        return nativeTransferTouchFocus(this.mPtr, paramInputChannel1, paramInputChannel2);
    }

    public void tryPointerSpeed(int paramInt)
    {
        if (!checkCallingPermission("android.permission.SET_POINTER_SPEED", "tryPointerSpeed()"))
            throw new SecurityException("Requires SET_POINTER_SPEED permission");
        if ((paramInt < -7) || (paramInt > 7))
            throw new IllegalArgumentException("speed out of range");
        setPointerSpeedUnchecked(paramInt);
    }

    public void unregisterInputChannel(InputChannel paramInputChannel)
    {
        if (paramInputChannel == null)
            throw new IllegalArgumentException("inputChannel must not be null.");
        nativeUnregisterInputChannel(this.mPtr, paramInputChannel);
    }

    public void updatePointerSpeedFromSettings()
    {
        setPointerSpeedUnchecked(getPointerSpeedSetting());
    }

    public void updateShowTouchesFromSettings()
    {
        boolean bool = false;
        int i = getShowTouchesSetting(0);
        int j = this.mPtr;
        if (i != 0)
            bool = true;
        nativeSetShowTouches(j, bool);
    }

    // ERROR //
    public void vibrate(int paramInt1, long[] paramArrayOfLong, int paramInt2, IBinder paramIBinder)
    {
        // Byte code:
        //     0: iload_3
        //     1: aload_2
        //     2: arraylength
        //     3: if_icmplt +11 -> 14
        //     6: new 1245	java/lang/ArrayIndexOutOfBoundsException
        //     9: dup
        //     10: invokespecial 1246	java/lang/ArrayIndexOutOfBoundsException:<init>	()V
        //     13: athrow
        //     14: aload_0
        //     15: getfield 155	com/android/server/input/InputManagerService:mVibratorLock	Ljava/lang/Object;
        //     18: astore 5
        //     20: aload 5
        //     22: monitorenter
        //     23: aload_0
        //     24: getfield 160	com/android/server/input/InputManagerService:mVibratorTokens	Ljava/util/HashMap;
        //     27: aload 4
        //     29: invokevirtual 944	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     32: checkcast 24	com/android/server/input/InputManagerService$VibratorToken
        //     35: astore 7
        //     37: aload 7
        //     39: ifnonnull +54 -> 93
        //     42: aload_0
        //     43: getfield 1248	com/android/server/input/InputManagerService:mNextVibratorTokenValue	I
        //     46: istore 9
        //     48: aload_0
        //     49: iload 9
        //     51: iconst_1
        //     52: iadd
        //     53: putfield 1248	com/android/server/input/InputManagerService:mNextVibratorTokenValue	I
        //     56: new 24	com/android/server/input/InputManagerService$VibratorToken
        //     59: dup
        //     60: aload_0
        //     61: iload_1
        //     62: aload 4
        //     64: iload 9
        //     66: invokespecial 1251	com/android/server/input/InputManagerService$VibratorToken:<init>	(Lcom/android/server/input/InputManagerService;ILandroid/os/IBinder;I)V
        //     69: astore 7
        //     71: aload 4
        //     73: aload 7
        //     75: iconst_0
        //     76: invokeinterface 1087 3 0
        //     81: aload_0
        //     82: getfield 160	com/android/server/input/InputManagerService:mVibratorTokens	Ljava/util/HashMap;
        //     85: aload 4
        //     87: aload 7
        //     89: invokevirtual 1254	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     92: pop
        //     93: aload 5
        //     95: monitorexit
        //     96: aload 7
        //     98: monitorenter
        //     99: aload 7
        //     101: iconst_1
        //     102: putfield 238	com/android/server/input/InputManagerService$VibratorToken:mVibrating	Z
        //     105: aload_0
        //     106: getfield 195	com/android/server/input/InputManagerService:mPtr	I
        //     109: iload_1
        //     110: aload_2
        //     111: iload_3
        //     112: aload 7
        //     114: getfield 244	com/android/server/input/InputManagerService$VibratorToken:mTokenValue	I
        //     117: invokestatic 1256	com/android/server/input/InputManagerService:nativeVibrate	(II[JII)V
        //     120: aload 7
        //     122: monitorexit
        //     123: return
        //     124: astore 10
        //     126: new 1092	java/lang/RuntimeException
        //     129: dup
        //     130: aload 10
        //     132: invokespecial 1095	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
        //     135: athrow
        //     136: astore 6
        //     138: aload 5
        //     140: monitorexit
        //     141: aload 6
        //     143: athrow
        //     144: astore 8
        //     146: aload 7
        //     148: monitorexit
        //     149: aload 8
        //     151: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     71	81	124	android/os/RemoteException
        //     23	71	136	finally
        //     71	81	136	finally
        //     81	96	136	finally
        //     126	141	136	finally
        //     99	123	144	finally
        //     146	149	144	finally
    }

    private final class VibratorToken
        implements IBinder.DeathRecipient
    {
        public final int mDeviceId;
        public final IBinder mToken;
        public final int mTokenValue;
        public boolean mVibrating;

        public VibratorToken(int paramIBinder, IBinder paramInt1, int arg4)
        {
            this.mDeviceId = paramIBinder;
            this.mToken = paramInt1;
            int i;
            this.mTokenValue = i;
        }

        public void binderDied()
        {
            InputManagerService.this.onVibratorTokenDied(this);
        }
    }

    private final class InputDevicesChangedListenerRecord
        implements IBinder.DeathRecipient
    {
        private final IInputDevicesChangedListener mListener;
        private final int mPid;

        public InputDevicesChangedListenerRecord(int paramIInputDevicesChangedListener, IInputDevicesChangedListener arg3)
        {
            this.mPid = paramIInputDevicesChangedListener;
            Object localObject;
            this.mListener = localObject;
        }

        public void binderDied()
        {
            InputManagerService.this.onInputDevicesChangedListenerDied(this.mPid);
        }

        public void notifyInputDevicesChanged(int[] paramArrayOfInt)
        {
            try
            {
                this.mListener.onInputDevicesChanged(paramArrayOfInt);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                {
                    Slog.w("InputManager", "Failed to notify process " + this.mPid + " that input devices changed, assuming it died.", localRemoteException);
                    binderDied();
                }
            }
        }
    }

    private static abstract interface KeyboardLayoutVisitor
    {
        public abstract void visitKeyboardLayout(Resources paramResources, String paramString1, String paramString2, String paramString3, int paramInt);
    }

    private static final class KeyboardLayoutDescriptor
    {
        public String keyboardLayoutName;
        public String packageName;
        public String receiverName;

        public static String format(String paramString1, String paramString2, String paramString3)
        {
            return paramString1 + "/" + paramString2 + "/" + paramString3;
        }

        public static KeyboardLayoutDescriptor parse(String paramString)
        {
            KeyboardLayoutDescriptor localKeyboardLayoutDescriptor = null;
            int i = paramString.indexOf('/');
            if ((i < 0) || (i + 1 == paramString.length()));
            while (true)
            {
                return localKeyboardLayoutDescriptor;
                int j = paramString.indexOf('/', i + 1);
                if ((j >= i + 2) && (j + 1 != paramString.length()))
                {
                    localKeyboardLayoutDescriptor = new KeyboardLayoutDescriptor();
                    localKeyboardLayoutDescriptor.packageName = paramString.substring(0, i);
                    localKeyboardLayoutDescriptor.receiverName = paramString.substring(i + 1, j);
                    localKeyboardLayoutDescriptor.keyboardLayoutName = paramString.substring(j + 1);
                }
            }
        }
    }

    private final class InputFilterHost
        implements InputFilter.Host
    {
        private boolean mDisconnected;

        private InputFilterHost()
        {
        }

        public void disconnectLocked()
        {
            this.mDisconnected = true;
        }

        public void sendInputEvent(InputEvent paramInputEvent, int paramInt)
        {
            if (paramInputEvent == null)
                throw new IllegalArgumentException("event must not be null");
            synchronized (InputManagerService.this.mInputFilterLock)
            {
                if (!this.mDisconnected)
                    InputManagerService.nativeInjectInputEvent(InputManagerService.this.mPtr, paramInputEvent, 0, 0, 0, 0, paramInt | 0x4000000);
                return;
            }
        }
    }

    private final class InputManagerHandler extends Handler
    {
        private InputManagerHandler()
        {
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            }
            while (true)
            {
                return;
                InputManagerService.this.deliverInputDevicesChanged((InputDevice[])paramMessage.obj);
                continue;
                InputManagerService.this.handleSwitchKeyboardLayout(paramMessage.arg1, paramMessage.arg2);
                continue;
                InputManagerService.this.reloadKeyboardLayouts();
                continue;
                InputManagerService.this.updateKeyboardLayouts();
                continue;
                InputManagerService.this.reloadDeviceAliases();
            }
        }
    }

    public static abstract interface Callbacks
    {
        public abstract KeyEvent dispatchUnhandledKey(InputWindowHandle paramInputWindowHandle, KeyEvent paramKeyEvent, int paramInt);

        public abstract int getPointerLayer();

        public abstract long interceptKeyBeforeDispatching(InputWindowHandle paramInputWindowHandle, KeyEvent paramKeyEvent, int paramInt);

        public abstract int interceptKeyBeforeQueueing(KeyEvent paramKeyEvent, int paramInt, boolean paramBoolean);

        public abstract int interceptMotionBeforeQueueingWhenScreenOff(int paramInt);

        public abstract long notifyANR(InputApplicationHandle paramInputApplicationHandle, InputWindowHandle paramInputWindowHandle);

        public abstract void notifyConfigurationChanged();

        public abstract void notifyInputChannelBroken(InputWindowHandle paramInputWindowHandle);

        public abstract void notifyLidSwitchChanged(long paramLong, boolean paramBoolean);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.input.InputManagerService
 * JD-Core Version:        0.6.2
 */