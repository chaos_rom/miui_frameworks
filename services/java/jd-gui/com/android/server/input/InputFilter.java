package com.android.server.input;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.InputEvent;
import android.view.InputEventConsistencyVerifier;

public abstract class InputFilter
{
    private static final int MSG_INPUT_EVENT = 3;
    private static final int MSG_INSTALL = 1;
    private static final int MSG_UNINSTALL = 2;
    private final H mH;
    private Host mHost;
    private final InputEventConsistencyVerifier mInboundInputEventConsistencyVerifier;
    private final InputEventConsistencyVerifier mOutboundInputEventConsistencyVerifier;

    public InputFilter(Looper paramLooper)
    {
        if (InputEventConsistencyVerifier.isInstrumentationEnabled());
        for (InputEventConsistencyVerifier localInputEventConsistencyVerifier2 = new InputEventConsistencyVerifier(this, 1, "InputFilter#InboundInputEventConsistencyVerifier"); ; localInputEventConsistencyVerifier2 = null)
        {
            this.mInboundInputEventConsistencyVerifier = localInputEventConsistencyVerifier2;
            if (InputEventConsistencyVerifier.isInstrumentationEnabled())
                localInputEventConsistencyVerifier1 = new InputEventConsistencyVerifier(this, 1, "InputFilter#OutboundInputEventConsistencyVerifier");
            this.mOutboundInputEventConsistencyVerifier = localInputEventConsistencyVerifier1;
            this.mH = new H(paramLooper);
            return;
        }
    }

    final void filterInputEvent(InputEvent paramInputEvent, int paramInt)
    {
        this.mH.obtainMessage(3, paramInt, 0, paramInputEvent).sendToTarget();
    }

    final void install(Host paramHost)
    {
        this.mH.obtainMessage(1, paramHost).sendToTarget();
    }

    public void onInputEvent(InputEvent paramInputEvent, int paramInt)
    {
        sendInputEvent(paramInputEvent, paramInt);
    }

    public void onInstalled()
    {
    }

    public void onUninstalled()
    {
    }

    public void sendInputEvent(InputEvent paramInputEvent, int paramInt)
    {
        if (paramInputEvent == null)
            throw new IllegalArgumentException("event must not be null");
        if (this.mHost == null)
            throw new IllegalStateException("Cannot send input event because the input filter is not installed.");
        if (this.mOutboundInputEventConsistencyVerifier != null)
            this.mOutboundInputEventConsistencyVerifier.onInputEvent(paramInputEvent, 0);
        this.mHost.sendInputEvent(paramInputEvent, paramInt);
    }

    final void uninstall()
    {
        this.mH.obtainMessage(2).sendToTarget();
    }

    static abstract interface Host
    {
        public abstract void sendInputEvent(InputEvent paramInputEvent, int paramInt);
    }

    private final class H extends Handler
    {
        public H(Looper arg2)
        {
            super();
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
            case 1:
            case 2:
            case 3:
            }
            while (true)
            {
                return;
                InputFilter.access$002(InputFilter.this, (InputFilter.Host)paramMessage.obj);
                if (InputFilter.this.mInboundInputEventConsistencyVerifier != null)
                    InputFilter.this.mInboundInputEventConsistencyVerifier.reset();
                if (InputFilter.this.mOutboundInputEventConsistencyVerifier != null)
                    InputFilter.this.mOutboundInputEventConsistencyVerifier.reset();
                InputFilter.this.onInstalled();
                continue;
                InputEvent localInputEvent;
                try
                {
                    InputFilter.this.onUninstalled();
                    InputFilter.access$002(InputFilter.this, null);
                }
                finally
                {
                    InputFilter.access$002(InputFilter.this, null);
                }
                try
                {
                    if (InputFilter.this.mInboundInputEventConsistencyVerifier != null)
                        InputFilter.this.mInboundInputEventConsistencyVerifier.onInputEvent(localInputEvent, 0);
                    InputFilter.this.onInputEvent(localInputEvent, paramMessage.arg1);
                    localInputEvent.recycle();
                }
                finally
                {
                    localInputEvent.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.input.InputFilter
 * JD-Core Version:        0.6.2
 */