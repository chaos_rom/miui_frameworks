package com.android.server;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.IPackageDataObserver.Stub;
import android.content.pm.IPackageManager;
import android.content.pm.IPackageManager.Stub;
import android.os.Binder;
import android.os.Environment;
import android.os.FileObserver;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.StatFs;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.provider.Settings.Secure;
import android.util.EventLog;
import android.util.Slog;
import java.io.File;

public class DeviceStorageMonitorService extends Binder
{
    private static final String CACHE_PATH = "/cache";
    private static final String DATA_PATH = "/data";
    private static final boolean DEBUG = false;
    private static final long DEFAULT_CHECK_INTERVAL = 60000L;
    private static final long DEFAULT_DISK_FREE_CHANGE_REPORTING_THRESHOLD = 2097152L;
    private static final int DEFAULT_FREE_STORAGE_LOG_INTERVAL_IN_MINUTES = 720;
    private static final int DEFAULT_FULL_THRESHOLD_BYTES = 1048576;
    private static final int DEFAULT_THRESHOLD_MAX_BYTES = 524288000;
    private static final int DEFAULT_THRESHOLD_PERCENTAGE = 10;
    private static final int DEVICE_MEMORY_WHAT = 1;
    private static final int LOW_MEMORY_NOTIFICATION_ID = 1;
    private static final int MONITOR_INTERVAL = 1;
    public static final String SERVICE = "devicestoragemonitor";
    private static final String SYSTEM_PATH = "/system";
    private static final String TAG = "DeviceStorageMonitorService";
    private static final int _FALSE = 0;
    private static final int _TRUE = 1;
    private static final boolean localLOGV;
    private final CacheFileDeletedObserver mCacheFileDeletedObserver;
    private StatFs mCacheFileStats;
    private CachePackageDataObserver mClearCacheObserver;
    private boolean mClearSucceeded = false;
    private boolean mClearingCache;
    private ContentResolver mContentResolver;
    private Context mContext;
    private StatFs mDataFileStats;
    private long mFreeMem;
    Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            int i = 1;
            if (paramAnonymousMessage.what != i)
            {
                Slog.e("DeviceStorageMonitorService", "Will not process invalid message");
                return;
            }
            DeviceStorageMonitorService localDeviceStorageMonitorService = DeviceStorageMonitorService.this;
            if (paramAnonymousMessage.arg1 == i);
            while (true)
            {
                localDeviceStorageMonitorService.checkMemory(i);
                break;
                int j = 0;
            }
        }
    };
    private long mLastReportedFreeMem;
    private long mLastReportedFreeMemTime = 0L;
    private boolean mLowMemFlag = false;
    private boolean mMemFullFlag = false;
    private int mMemFullThreshold;
    private long mMemLowThreshold;
    private Intent mStorageFullIntent;
    private Intent mStorageLowIntent;
    private Intent mStorageNotFullIntent;
    private Intent mStorageOkIntent;
    private StatFs mSystemFileStats;
    private long mThreadStartTime = -1L;
    private long mTotalMemory;

    public DeviceStorageMonitorService(Context paramContext)
    {
        this.mContext = paramContext;
        this.mContentResolver = this.mContext.getContentResolver();
        this.mDataFileStats = new StatFs("/data");
        this.mSystemFileStats = new StatFs("/system");
        this.mCacheFileStats = new StatFs("/cache");
        this.mTotalMemory = (this.mDataFileStats.getBlockCount() * this.mDataFileStats.getBlockSize() / 100L);
        this.mStorageLowIntent = new Intent("android.intent.action.DEVICE_STORAGE_LOW");
        this.mStorageLowIntent.addFlags(134217728);
        this.mStorageOkIntent = new Intent("android.intent.action.DEVICE_STORAGE_OK");
        this.mStorageOkIntent.addFlags(134217728);
        this.mStorageFullIntent = new Intent("android.intent.action.DEVICE_STORAGE_FULL");
        this.mStorageFullIntent.addFlags(134217728);
        this.mStorageNotFullIntent = new Intent("android.intent.action.DEVICE_STORAGE_NOT_FULL");
        this.mStorageNotFullIntent.addFlags(134217728);
        this.mMemLowThreshold = getMemThreshold();
        this.mMemFullThreshold = getMemFullThreshold();
        checkMemory(true);
        this.mCacheFileDeletedObserver = new CacheFileDeletedObserver();
        this.mCacheFileDeletedObserver.startWatching();
    }

    private final void cancelFullNotification()
    {
        this.mContext.removeStickyBroadcast(this.mStorageFullIntent);
        this.mContext.sendBroadcast(this.mStorageNotFullIntent);
    }

    private final void cancelNotification()
    {
        ((NotificationManager)this.mContext.getSystemService("notification")).cancel(1);
        this.mContext.removeStickyBroadcast(this.mStorageLowIntent);
        this.mContext.sendBroadcast(this.mStorageOkIntent);
    }

    private final void checkMemory(boolean paramBoolean)
    {
        if (this.mClearingCache)
            if (System.currentTimeMillis() - this.mThreadStartTime > 600000L)
                Slog.w("DeviceStorageMonitorService", "Thread that clears cache file seems to run for ever");
        while (true)
        {
            postCheckMemoryMsg(true, 60000L);
            return;
            restatDataDir();
            if (this.mFreeMem < this.mMemLowThreshold)
                if (!this.mLowMemFlag)
                {
                    if (!paramBoolean)
                        break label114;
                    this.mThreadStartTime = System.currentTimeMillis();
                    this.mClearSucceeded = false;
                    clearCache();
                }
            while (true)
            {
                if (this.mFreeMem >= this.mMemFullThreshold)
                    break label161;
                if (this.mMemFullFlag)
                    break;
                sendFullNotification();
                this.mMemFullFlag = true;
                break;
                label114: Slog.i("DeviceStorageMonitorService", "Running low on memory. Sending notification");
                sendNotification();
                this.mLowMemFlag = true;
                continue;
                if (this.mLowMemFlag)
                {
                    Slog.i("DeviceStorageMonitorService", "Memory available. Cancelling notification");
                    cancelNotification();
                    this.mLowMemFlag = false;
                }
            }
            label161: if (this.mMemFullFlag)
            {
                cancelFullNotification();
                this.mMemFullFlag = false;
            }
        }
    }

    private final void clearCache()
    {
        if (this.mClearCacheObserver == null)
            this.mClearCacheObserver = new CachePackageDataObserver();
        this.mClearingCache = true;
        try
        {
            IPackageManager.Stub.asInterface(ServiceManager.getService("package")).freeStorageAndNotify(this.mMemLowThreshold, this.mClearCacheObserver);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Slog.w("DeviceStorageMonitorService", "Failed to get handle for PackageManger Exception: " + localRemoteException);
                this.mClearingCache = false;
                this.mClearSucceeded = false;
            }
        }
    }

    private int getMemFullThreshold()
    {
        return Settings.Secure.getInt(this.mContentResolver, "sys_storage_full_threshold_bytes", 1048576);
    }

    private long getMemThreshold()
    {
        long l1 = Settings.Secure.getInt(this.mContentResolver, "sys_storage_threshold_percentage", 10) * this.mTotalMemory;
        long l2 = Settings.Secure.getInt(this.mContentResolver, "sys_storage_threshold_max_bytes", 524288000);
        if (l1 < l2);
        while (true)
        {
            return l1;
            l1 = l2;
        }
    }

    private void postCheckMemoryMsg(boolean paramBoolean, long paramLong)
    {
        this.mHandler.removeMessages(1);
        Handler localHandler1 = this.mHandler;
        Handler localHandler2 = this.mHandler;
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localHandler1.sendMessageDelayed(localHandler2.obtainMessage(1, i, 0), paramLong);
            return;
        }
    }

    private final void restatDataDir()
    {
        try
        {
            this.mDataFileStats.restat("/data");
            this.mFreeMem = (this.mDataFileStats.getAvailableBlocks() * this.mDataFileStats.getBlockSize());
            label30: String str = SystemProperties.get("debug.freemem");
            if (!"".equals(str))
                this.mFreeMem = Long.parseLong(str);
            long l1 = 1000L * (60L * Settings.Secure.getLong(this.mContentResolver, "sys_free_storage_log_interval", 720L));
            long l2 = SystemClock.elapsedRealtime();
            long l3;
            long l4;
            if ((this.mLastReportedFreeMemTime == 0L) || (l2 - this.mLastReportedFreeMemTime >= l1))
            {
                this.mLastReportedFreeMemTime = l2;
                l3 = -1L;
                l4 = -1L;
            }
            try
            {
                this.mSystemFileStats.restat("/system");
                long l8 = this.mSystemFileStats.getAvailableBlocks();
                int j = this.mSystemFileStats.getBlockSize();
                l3 = l8 * j;
                try
                {
                    label155: this.mCacheFileStats.restat("/cache");
                    long l7 = this.mCacheFileStats.getAvailableBlocks();
                    int i = this.mCacheFileStats.getBlockSize();
                    l4 = l7 * i;
                    label191: Object[] arrayOfObject = new Object[3];
                    arrayOfObject[0] = Long.valueOf(this.mFreeMem);
                    arrayOfObject[1] = Long.valueOf(l3);
                    arrayOfObject[2] = Long.valueOf(l4);
                    EventLog.writeEvent(2746, arrayOfObject);
                    long l5 = Settings.Secure.getLong(this.mContentResolver, "disk_free_change_reporting_threshold", 2097152L);
                    long l6 = this.mFreeMem - this.mLastReportedFreeMem;
                    if ((l6 > l5) || (l6 < -l5))
                    {
                        this.mLastReportedFreeMem = this.mFreeMem;
                        EventLog.writeEvent(2744, this.mFreeMem);
                    }
                    return;
                }
                catch (IllegalArgumentException localIllegalArgumentException3)
                {
                    break label191;
                }
            }
            catch (IllegalArgumentException localIllegalArgumentException2)
            {
                break label155;
            }
        }
        catch (IllegalArgumentException localIllegalArgumentException1)
        {
            break label30;
        }
    }

    private final void sendFullNotification()
    {
        this.mContext.sendStickyBroadcast(this.mStorageFullIntent);
    }

    private final void sendNotification()
    {
        EventLog.writeEvent(2745, this.mFreeMem);
        if (Environment.isExternalStorageEmulated());
        for (String str = "android.settings.INTERNAL_STORAGE_SETTINGS"; ; str = "android.intent.action.MANAGE_PACKAGE_STORAGE")
        {
            Intent localIntent = new Intent(str);
            localIntent.putExtra("memory", this.mFreeMem);
            localIntent.addFlags(268435456);
            NotificationManager localNotificationManager = (NotificationManager)this.mContext.getSystemService("notification");
            CharSequence localCharSequence1 = this.mContext.getText(17040324);
            CharSequence localCharSequence2 = this.mContext.getText(17040325);
            PendingIntent localPendingIntent = PendingIntent.getActivity(this.mContext, 0, localIntent, 0);
            Notification localNotification = new Notification();
            localNotification.icon = 17302801;
            localNotification.tickerText = localCharSequence1;
            localNotification.flags = (0x20 | localNotification.flags);
            localNotification.setLatestEventInfo(this.mContext, localCharSequence1, localCharSequence2, localPendingIntent);
            localNotificationManager.notify(1, localNotification);
            this.mContext.sendStickyBroadcast(this.mStorageLowIntent);
            return;
        }
    }

    public long getMemoryLowThreshold()
    {
        return this.mMemLowThreshold;
    }

    public boolean isMemoryLow()
    {
        return this.mLowMemFlag;
    }

    public void updateMemory()
    {
        if (getCallingUid() != 1000);
        while (true)
        {
            return;
            postCheckMemoryMsg(true, 0L);
        }
    }

    public static class CacheFileDeletedObserver extends FileObserver
    {
        public CacheFileDeletedObserver()
        {
            super(512);
        }

        public void onEvent(int paramInt, String paramString)
        {
            EventLogTags.writeCacheFileDeleted(paramString);
        }
    }

    class CachePackageDataObserver extends IPackageDataObserver.Stub
    {
        CachePackageDataObserver()
        {
        }

        public void onRemoveCompleted(String paramString, boolean paramBoolean)
        {
            DeviceStorageMonitorService.access$102(DeviceStorageMonitorService.this, paramBoolean);
            DeviceStorageMonitorService.access$202(DeviceStorageMonitorService.this, false);
            DeviceStorageMonitorService.this.postCheckMemoryMsg(false, 0L);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.DeviceStorageMonitorService
 * JD-Core Version:        0.6.2
 */