package com.android.server;

import android.util.SparseArray;
import java.util.HashMap;

public class ProcessMap<E>
{
    final HashMap<String, SparseArray<E>> mMap = new HashMap();

    public E get(String paramString, int paramInt)
    {
        SparseArray localSparseArray = (SparseArray)this.mMap.get(paramString);
        if (localSparseArray == null);
        for (Object localObject = null; ; localObject = localSparseArray.get(paramInt))
            return localObject;
    }

    public HashMap<String, SparseArray<E>> getMap()
    {
        return this.mMap;
    }

    public E put(String paramString, int paramInt, E paramE)
    {
        SparseArray localSparseArray = (SparseArray)this.mMap.get(paramString);
        if (localSparseArray == null)
        {
            localSparseArray = new SparseArray(2);
            this.mMap.put(paramString, localSparseArray);
        }
        localSparseArray.put(paramInt, paramE);
        return paramE;
    }

    public void remove(String paramString, int paramInt)
    {
        SparseArray localSparseArray = (SparseArray)this.mMap.get(paramString);
        if (localSparseArray != null)
        {
            localSparseArray.remove(paramInt);
            if (localSparseArray.size() == 0)
                this.mMap.remove(paramString);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.ProcessMap
 * JD-Core Version:        0.6.2
 */