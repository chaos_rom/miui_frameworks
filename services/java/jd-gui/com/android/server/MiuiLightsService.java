package com.android.server;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings.Secure;
import android.provider.Settings.System;

public class MiuiLightsService extends LightsService
{
    private Context mContext;
    private ContentResolver mResolver;

    MiuiLightsService(Context paramContext)
    {
        super(paramContext);
        this.mContext = paramContext;
        this.mResolver = paramContext.getContentResolver();
        setLight(2, new Light(2, null));
        setLight(4, new Light(4, null));
    }

    public class Light extends LightsService.Light
    {
        private static final int LIGHT_ON_MS = 500;
        private static final int STOP_FLASH_MSG = 1;
        private int mBrightnessMode;
        private int mColor;
        private boolean mDisabled;
        private Handler mHandler = new Handler()
        {
            public void handleMessage(Message paramAnonymousMessage)
            {
                switch (paramAnonymousMessage.what)
                {
                default:
                case 1:
                }
                while (true)
                {
                    return;
                    MiuiLightsService.Light.this.turnOff();
                }
            }
        };
        private int mMode;
        private int mOffMS;
        private int mOnMS;
        private boolean mTurnOn = true;

        private Light(int arg2)
        {
            super(i, 0);
            if (2 == i)
            {
                Settings.Secure.putInt(MiuiLightsService.this.mResolver, "screen_buttons_state", 0);
                this.mTurnOn = isTurnOn();
                MiuiLightsService.this.mResolver.registerContentObserver(Settings.Secure.getUriFor("screen_buttons_state"), true, new ContentObserver(new Handler())
                {
                    public void onChange(boolean paramAnonymousBoolean)
                    {
                        MiuiLightsService.Light localLight = MiuiLightsService.Light.this;
                        boolean bool;
                        if (Settings.Secure.getInt(MiuiLightsService.this.mResolver, "screen_buttons_state", 0) != 0)
                        {
                            bool = true;
                            MiuiLightsService.Light.access$202(localLight, bool);
                            if (!MiuiLightsService.Light.this.mDisabled)
                                break label60;
                            MiuiLightsService.Light.this.setLightLocked(0, 0, 0, 0, 0);
                        }
                        while (true)
                        {
                            return;
                            bool = false;
                            break;
                            label60: if (MiuiLightsService.Light.this.mTurnOn)
                                MiuiLightsService.Light.this.setLightLocked(MiuiLightsService.Light.this.mColor, MiuiLightsService.Light.this.mMode, MiuiLightsService.Light.this.mOnMS, MiuiLightsService.Light.this.mOffMS, MiuiLightsService.Light.this.mBrightnessMode);
                        }
                    }
                });
                MiuiLightsService.this.mResolver.registerContentObserver(Settings.Secure.getUriFor("screen_buttons_turn_on"), true, new ContentObserver(new Handler())
                {
                    public void onChange(boolean paramAnonymousBoolean)
                    {
                        if (MiuiLightsService.Light.this.mDisabled);
                        while (true)
                        {
                            return;
                            MiuiLightsService.Light.access$402(MiuiLightsService.Light.this, MiuiLightsService.Light.this.isTurnOn());
                            if (MiuiLightsService.Light.this.mTurnOn)
                                MiuiLightsService.Light.this.setLightLocked(MiuiLightsService.Light.this.mColor, MiuiLightsService.Light.this.mMode, MiuiLightsService.Light.this.mOnMS, MiuiLightsService.Light.this.mOffMS, MiuiLightsService.Light.this.mBrightnessMode);
                            else
                                MiuiLightsService.Light.this.setLightLocked(0, MiuiLightsService.Light.this.mMode, 0, 0, MiuiLightsService.Light.this.mBrightnessMode);
                        }
                    }
                });
            }
            while (true)
            {
                return;
                if (4 == i)
                {
                    MiuiLightsService.this.mResolver.registerContentObserver(Settings.System.getUriFor("breathing_light_color"), true, new ContentObserver(new Handler())
                    {
                        public void onChange(boolean paramAnonymousBoolean)
                        {
                            MiuiLightsService.Light.this.setFlashing("breathing_light_color", "breathing_light_freq");
                        }
                    });
                    MiuiLightsService.this.mResolver.registerContentObserver(Settings.System.getUriFor("call_breathing_light_color"), true, new ContentObserver(new Handler())
                    {
                        public void onChange(boolean paramAnonymousBoolean)
                        {
                            MiuiLightsService.Light.this.setFlashing("call_breathing_light_color", "call_breathing_light_freq");
                        }
                    });
                    MiuiLightsService.this.mResolver.registerContentObserver(Settings.System.getUriFor("mms_breathing_light_color"), true, new ContentObserver(new Handler())
                    {
                        public void onChange(boolean paramAnonymousBoolean)
                        {
                            MiuiLightsService.Light.this.setFlashing("mms_breathing_light_color", "mms_breathing_light_freq");
                        }
                    });
                }
            }
        }

        private boolean isTurnOn()
        {
            int i = 1;
            if (Settings.Secure.getInt(MiuiLightsService.this.mResolver, "screen_buttons_turn_on", i) == i);
            while (true)
            {
                return i;
                int j = 0;
            }
        }

        void setFlashing(String paramString1, String paramString2)
        {
            int i = MiuiLightsService.this.mContext.getResources().getColor(101122057);
            setFlashing(Settings.System.getInt(MiuiLightsService.this.mResolver, paramString1, i), 1, 500, 0);
            this.mHandler.removeMessages(1);
            this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(1), 500L);
        }

        void setLightLocked(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
        {
            this.mColor = paramInt1;
            this.mMode = paramInt2;
            this.mOnMS = paramInt3;
            this.mOffMS = paramInt4;
            this.mBrightnessMode = paramInt5;
            if ((!this.mDisabled) && (this.mTurnOn))
                super.setLightLocked(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.MiuiLightsService
 * JD-Core Version:        0.6.2
 */