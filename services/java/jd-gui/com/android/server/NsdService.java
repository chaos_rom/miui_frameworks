package com.android.server;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.net.nsd.DnsSdTxtRecord;
import android.net.nsd.INsdManager.Stub;
import android.net.nsd.NsdServiceInfo;
import android.os.Binder;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.provider.Settings.Secure;
import android.util.Slog;
import android.util.SparseArray;
import com.android.internal.util.AsyncChannel;
import com.android.internal.util.State;
import com.android.internal.util.StateMachine;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.CountDownLatch;

public class NsdService extends INsdManager.Stub
{
    private static final int BASE = 393216;
    private static final int CMD_TO_STRING_COUNT = 19;
    private static final boolean DBG = true;
    private static final String MDNS_TAG = "mDnsConnector";
    private static final String TAG = "NsdService";
    private static String[] sCmdToString = new String[19];
    private int INVALID_ID = 0;
    private HashMap<Messenger, ClientInfo> mClients = new HashMap();
    private ContentResolver mContentResolver;
    private Context mContext;
    private SparseArray<ClientInfo> mIdToClientInfoMap = new SparseArray();
    private NativeDaemonConnector mNativeConnector;
    private final CountDownLatch mNativeDaemonConnected = new CountDownLatch(1);
    private NsdStateMachine mNsdStateMachine;
    private AsyncChannel mReplyChannel = new AsyncChannel();
    private int mUniqueId = 1;

    static
    {
        sCmdToString[1] = "DISCOVER";
        sCmdToString[6] = "STOP-DISCOVER";
        sCmdToString[9] = "REGISTER";
        sCmdToString[12] = "UNREGISTER";
        sCmdToString[18] = "RESOLVE";
    }

    private NsdService(Context paramContext)
    {
        this.mContext = paramContext;
        this.mContentResolver = paramContext.getContentResolver();
        this.mNativeConnector = new NativeDaemonConnector(new NativeCallbackReceiver(), "mdns", 10, "mDnsConnector", 25);
        this.mNsdStateMachine = new NsdStateMachine("NsdService");
        this.mNsdStateMachine.start();
        new Thread(this.mNativeConnector, "mDnsConnector").start();
    }

    private static String cmdToString(int paramInt)
    {
        int i = paramInt - 393216;
        if ((i >= 0) && (i < sCmdToString.length));
        for (String str = sCmdToString[i]; ; str = null)
            return str;
    }

    public static NsdService create(Context paramContext)
        throws InterruptedException
    {
        NsdService localNsdService = new NsdService(paramContext);
        localNsdService.mNativeDaemonConnected.await();
        return localNsdService;
    }

    private boolean discoverServices(int paramInt, String paramString)
    {
        boolean bool = true;
        Slog.d("NsdService", "discoverServices: " + paramInt + " " + paramString);
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mNativeConnector;
            Object[] arrayOfObject = new Object[3];
            arrayOfObject[0] = "discover";
            arrayOfObject[1] = Integer.valueOf(paramInt);
            arrayOfObject[2] = paramString;
            localNativeDaemonConnector.execute("mdnssd", arrayOfObject);
            return bool;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            while (true)
            {
                Slog.e("NsdService", "Failed to discoverServices " + localNativeDaemonConnectorException);
                bool = false;
            }
        }
    }

    private boolean getAddrInfo(int paramInt, String paramString)
    {
        boolean bool = true;
        Slog.d("NsdService", "getAdddrInfo: " + paramInt);
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mNativeConnector;
            Object[] arrayOfObject = new Object[3];
            arrayOfObject[0] = "getaddrinfo";
            arrayOfObject[1] = Integer.valueOf(paramInt);
            arrayOfObject[2] = paramString;
            localNativeDaemonConnector.execute("mdnssd", arrayOfObject);
            return bool;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            while (true)
            {
                Slog.e("NsdService", "Failed to getAddrInfo " + localNativeDaemonConnectorException);
                bool = false;
            }
        }
    }

    private int getUniqueId()
    {
        int i = 1 + this.mUniqueId;
        this.mUniqueId = i;
        int j;
        if (i == this.INVALID_ID)
        {
            j = 1 + this.mUniqueId;
            this.mUniqueId = j;
        }
        while (true)
        {
            return j;
            j = this.mUniqueId;
        }
    }

    private void handleNativeEvent(int paramInt, String paramString, String[] paramArrayOfString)
    {
        int i = Integer.parseInt(paramArrayOfString[1]);
        ClientInfo localClientInfo = (ClientInfo)this.mIdToClientInfoMap.get(i);
        if (localClientInfo == null)
            Slog.e("NsdService", "Unique id with no client mapping: " + i);
        while (true)
        {
            return;
            int j = -1;
            int k = localClientInfo.mClientIds.indexOfValue(Integer.valueOf(i));
            if (k != -1)
                j = localClientInfo.mClientIds.keyAt(k);
            switch (paramInt)
            {
            case 609:
            case 610:
            default:
                break;
            case 602:
                Slog.d("NsdService", "SERVICE_DISC_FAILED Raw: " + paramString);
                localClientInfo.mChannel.sendMessage(393219, 0, j);
                break;
            case 603:
                Slog.d("NsdService", "SERVICE_FOUND Raw: " + paramString);
                NsdServiceInfo localNsdServiceInfo3 = new NsdServiceInfo(paramArrayOfString[2], paramArrayOfString[3], null);
                localClientInfo.mChannel.sendMessage(393220, 0, j, localNsdServiceInfo3);
                break;
            case 604:
                Slog.d("NsdService", "SERVICE_LOST Raw: " + paramString);
                NsdServiceInfo localNsdServiceInfo2 = new NsdServiceInfo(paramArrayOfString[2], paramArrayOfString[3], null);
                localClientInfo.mChannel.sendMessage(393221, 0, j, localNsdServiceInfo2);
                break;
            case 606:
                Slog.d("NsdService", "SERVICE_REGISTERED Raw: " + paramString);
                NsdServiceInfo localNsdServiceInfo1 = new NsdServiceInfo(paramArrayOfString[2], null, null);
                localClientInfo.mChannel.sendMessage(393227, i, j, localNsdServiceInfo1);
                break;
            case 605:
                Slog.d("NsdService", "SERVICE_REGISTER_FAILED Raw: " + paramString);
                localClientInfo.mChannel.sendMessage(393226, 0, j);
                break;
            case 608:
                Slog.d("NsdService", "SERVICE_RESOLVED Raw: " + paramString);
                int m = paramArrayOfString[2].indexOf(".");
                if (m == -1)
                {
                    Slog.e("NsdService", "Invalid service found " + paramString);
                    continue;
                }
                String str1 = paramArrayOfString[2].substring(0, m);
                String str2 = paramArrayOfString[2].substring(m).replace(".local.", "");
                localClientInfo.mResolvedService.setServiceName(str1);
                localClientInfo.mResolvedService.setServiceType(str2);
                localClientInfo.mResolvedService.setPort(Integer.parseInt(paramArrayOfString[4]));
                stopResolveService(i);
                if (getAddrInfo(i, paramArrayOfString[3]))
                    continue;
                localClientInfo.mChannel.sendMessage(393235, 0, j);
                this.mIdToClientInfoMap.remove(i);
                ClientInfo.access$2302(localClientInfo, null);
                break;
            case 607:
                Slog.d("NsdService", "SERVICE_RESOLVE_FAILED Raw: " + paramString);
                stopResolveService(i);
                this.mIdToClientInfoMap.remove(i);
                ClientInfo.access$2302(localClientInfo, null);
                localClientInfo.mChannel.sendMessage(393235, 0, j);
                break;
            case 611:
                stopGetAddrInfo(i);
                this.mIdToClientInfoMap.remove(i);
                ClientInfo.access$2302(localClientInfo, null);
                Slog.d("NsdService", "SERVICE_RESOLVE_FAILED Raw: " + paramString);
                localClientInfo.mChannel.sendMessage(393235, 0, j);
                break;
            case 612:
                Slog.d("NsdService", "SERVICE_GET_ADDR_SUCCESS Raw: " + paramString);
                try
                {
                    localClientInfo.mResolvedService.setHost(InetAddress.getByName(paramArrayOfString[4]));
                    localClientInfo.mChannel.sendMessage(393236, 0, j, localClientInfo.mResolvedService);
                    stopGetAddrInfo(i);
                    this.mIdToClientInfoMap.remove(i);
                    ClientInfo.access$2302(localClientInfo, null);
                }
                catch (UnknownHostException localUnknownHostException)
                {
                    while (true)
                        localClientInfo.mChannel.sendMessage(393235, 0, j);
                }
            }
        }
    }

    private boolean isNsdEnabled()
    {
        int i = 1;
        if (Settings.Secure.getInt(this.mContentResolver, "nsd_on", i) == i);
        while (true)
        {
            Slog.d("NsdService", "Network service discovery enabled " + i);
            return i;
            int j = 0;
        }
    }

    private Message obtainMessage(Message paramMessage)
    {
        Message localMessage = Message.obtain();
        localMessage.arg2 = paramMessage.arg2;
        return localMessage;
    }

    private boolean registerService(int paramInt, NsdServiceInfo paramNsdServiceInfo)
    {
        boolean bool = true;
        Slog.d("NsdService", "registerService: " + paramInt + " " + paramNsdServiceInfo);
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mNativeConnector;
            Object[] arrayOfObject = new Object[5];
            arrayOfObject[0] = "register";
            arrayOfObject[1] = Integer.valueOf(paramInt);
            arrayOfObject[2] = paramNsdServiceInfo.getServiceName();
            arrayOfObject[3] = paramNsdServiceInfo.getServiceType();
            arrayOfObject[4] = Integer.valueOf(paramNsdServiceInfo.getPort());
            localNativeDaemonConnector.execute("mdnssd", arrayOfObject);
            return bool;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            while (true)
            {
                Slog.e("NsdService", "Failed to execute registerService " + localNativeDaemonConnectorException);
                bool = false;
            }
        }
    }

    private void replyToMessage(Message paramMessage, int paramInt)
    {
        if (paramMessage.replyTo == null);
        while (true)
        {
            return;
            Message localMessage = obtainMessage(paramMessage);
            localMessage.what = paramInt;
            this.mReplyChannel.replyToMessage(paramMessage, localMessage);
        }
    }

    private void replyToMessage(Message paramMessage, int paramInt1, int paramInt2)
    {
        if (paramMessage.replyTo == null);
        while (true)
        {
            return;
            Message localMessage = obtainMessage(paramMessage);
            localMessage.what = paramInt1;
            localMessage.arg1 = paramInt2;
            this.mReplyChannel.replyToMessage(paramMessage, localMessage);
        }
    }

    private void replyToMessage(Message paramMessage, int paramInt, Object paramObject)
    {
        if (paramMessage.replyTo == null);
        while (true)
        {
            return;
            Message localMessage = obtainMessage(paramMessage);
            localMessage.what = paramInt;
            localMessage.obj = paramObject;
            this.mReplyChannel.replyToMessage(paramMessage, localMessage);
        }
    }

    private boolean resolveService(int paramInt, NsdServiceInfo paramNsdServiceInfo)
    {
        boolean bool = true;
        Slog.d("NsdService", "resolveService: " + paramInt + " " + paramNsdServiceInfo);
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mNativeConnector;
            Object[] arrayOfObject = new Object[5];
            arrayOfObject[0] = "resolve";
            arrayOfObject[1] = Integer.valueOf(paramInt);
            arrayOfObject[2] = paramNsdServiceInfo.getServiceName();
            arrayOfObject[3] = paramNsdServiceInfo.getServiceType();
            arrayOfObject[4] = "local.";
            localNativeDaemonConnector.execute("mdnssd", arrayOfObject);
            return bool;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            while (true)
            {
                Slog.e("NsdService", "Failed to resolveService " + localNativeDaemonConnectorException);
                bool = false;
            }
        }
    }

    private void sendNsdStateChangeBroadcast(boolean paramBoolean)
    {
        Intent localIntent = new Intent("android.net.nsd.STATE_CHANGED");
        localIntent.addFlags(134217728);
        if (paramBoolean)
            localIntent.putExtra("nsd_state", 2);
        while (true)
        {
            this.mContext.sendStickyBroadcast(localIntent);
            return;
            localIntent.putExtra("nsd_state", 1);
        }
    }

    private boolean startMDnsDaemon()
    {
        boolean bool = true;
        Slog.d("NsdService", "startMDnsDaemon");
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mNativeConnector;
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = "start-service";
            localNativeDaemonConnector.execute("mdnssd", arrayOfObject);
            return bool;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            while (true)
            {
                Slog.e("NsdService", "Failed to start daemon" + localNativeDaemonConnectorException);
                bool = false;
            }
        }
    }

    private boolean stopGetAddrInfo(int paramInt)
    {
        boolean bool = true;
        Slog.d("NsdService", "stopGetAdddrInfo: " + paramInt);
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mNativeConnector;
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = "stop-getaddrinfo";
            arrayOfObject[1] = Integer.valueOf(paramInt);
            localNativeDaemonConnector.execute("mdnssd", arrayOfObject);
            return bool;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            while (true)
            {
                Slog.e("NsdService", "Failed to stopGetAddrInfo " + localNativeDaemonConnectorException);
                bool = false;
            }
        }
    }

    private boolean stopMDnsDaemon()
    {
        boolean bool = true;
        Slog.d("NsdService", "stopMDnsDaemon");
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mNativeConnector;
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = "stop-service";
            localNativeDaemonConnector.execute("mdnssd", arrayOfObject);
            return bool;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            while (true)
            {
                Slog.e("NsdService", "Failed to start daemon" + localNativeDaemonConnectorException);
                bool = false;
            }
        }
    }

    private boolean stopResolveService(int paramInt)
    {
        boolean bool = true;
        Slog.d("NsdService", "stopResolveService: " + paramInt);
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mNativeConnector;
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = "stop-resolve";
            arrayOfObject[1] = Integer.valueOf(paramInt);
            localNativeDaemonConnector.execute("mdnssd", arrayOfObject);
            return bool;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            while (true)
            {
                Slog.e("NsdService", "Failed to stop resolve " + localNativeDaemonConnectorException);
                bool = false;
            }
        }
    }

    private boolean stopServiceDiscovery(int paramInt)
    {
        boolean bool = true;
        Slog.d("NsdService", "stopServiceDiscovery: " + paramInt);
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mNativeConnector;
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = "stop-discover";
            arrayOfObject[1] = Integer.valueOf(paramInt);
            localNativeDaemonConnector.execute("mdnssd", arrayOfObject);
            return bool;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            while (true)
            {
                Slog.e("NsdService", "Failed to stopServiceDiscovery " + localNativeDaemonConnectorException);
                bool = false;
            }
        }
    }

    private boolean unregisterService(int paramInt)
    {
        boolean bool = true;
        Slog.d("NsdService", "unregisterService: " + paramInt);
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mNativeConnector;
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = "stop-register";
            arrayOfObject[1] = Integer.valueOf(paramInt);
            localNativeDaemonConnector.execute("mdnssd", arrayOfObject);
            return bool;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            while (true)
            {
                Slog.e("NsdService", "Failed to execute unregisterService " + localNativeDaemonConnectorException);
                bool = false;
            }
        }
    }

    private boolean updateService(int paramInt, DnsSdTxtRecord paramDnsSdTxtRecord)
    {
        boolean bool = false;
        Slog.d("NsdService", "updateService: " + paramInt + " " + paramDnsSdTxtRecord);
        if (paramDnsSdTxtRecord == null);
        while (true)
        {
            return bool;
            try
            {
                NativeDaemonConnector localNativeDaemonConnector = this.mNativeConnector;
                Object[] arrayOfObject = new Object[4];
                arrayOfObject[0] = "update";
                arrayOfObject[1] = Integer.valueOf(paramInt);
                arrayOfObject[2] = Integer.valueOf(paramDnsSdTxtRecord.size());
                arrayOfObject[3] = paramDnsSdTxtRecord.getRawData();
                localNativeDaemonConnector.execute("mdnssd", arrayOfObject);
                bool = true;
            }
            catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
            {
                Slog.e("NsdService", "Failed to updateServices " + localNativeDaemonConnectorException);
            }
        }
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.DUMP") != 0)
            paramPrintWriter.println("Permission Denial: can't dump ServiceDiscoverService from from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid());
        while (true)
        {
            return;
            Iterator localIterator = this.mClients.values().iterator();
            while (localIterator.hasNext())
            {
                ClientInfo localClientInfo = (ClientInfo)localIterator.next();
                paramPrintWriter.println("Client Info");
                paramPrintWriter.println(localClientInfo);
            }
            this.mNsdStateMachine.dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
        }
    }

    public Messenger getMessenger()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.INTERNET", "NsdService");
        return new Messenger(this.mNsdStateMachine.getHandler());
    }

    public void setEnabled(boolean paramBoolean)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NsdService");
        ContentResolver localContentResolver = this.mContentResolver;
        int i;
        if (paramBoolean)
        {
            i = 1;
            Settings.Secure.putInt(localContentResolver, "nsd_on", i);
            if (!paramBoolean)
                break label52;
            this.mNsdStateMachine.sendMessage(393240);
        }
        while (true)
        {
            return;
            i = 0;
            break;
            label52: this.mNsdStateMachine.sendMessage(393241);
        }
    }

    private class ClientInfo
    {
        private static final int MAX_LIMIT = 10;
        private AsyncChannel mChannel;
        private SparseArray<Integer> mClientIds = new SparseArray();
        private Messenger mMessenger;
        private NsdServiceInfo mResolvedService;

        private ClientInfo(AsyncChannel paramMessenger, Messenger arg3)
        {
            this.mChannel = paramMessenger;
            Object localObject;
            this.mMessenger = localObject;
            Slog.d("NsdService", "New client, channel: " + paramMessenger + " messenger: " + localObject);
        }

        public String toString()
        {
            StringBuffer localStringBuffer = new StringBuffer();
            localStringBuffer.append("mChannel ").append(this.mChannel).append("\n");
            localStringBuffer.append("mMessenger ").append(this.mMessenger).append("\n");
            localStringBuffer.append("mResolvedService ").append(this.mResolvedService).append("\n");
            for (int i = 0; i < this.mClientIds.size(); i++)
            {
                localStringBuffer.append("clientId ").append(this.mClientIds.keyAt(i));
                localStringBuffer.append(" mDnsId ").append(this.mClientIds.valueAt(i)).append("\n");
            }
            return localStringBuffer.toString();
        }
    }

    class NativeCallbackReceiver
        implements INativeDaemonConnectorCallbacks
    {
        NativeCallbackReceiver()
        {
        }

        public void onDaemonConnected()
        {
            NsdService.this.mNativeDaemonConnected.countDown();
        }

        public boolean onEvent(int paramInt, String paramString, String[] paramArrayOfString)
        {
            NsdService.NativeEvent localNativeEvent = new NsdService.NativeEvent(NsdService.this, paramInt, paramString);
            NsdService.this.mNsdStateMachine.sendMessage(393242, localNativeEvent);
            return true;
        }
    }

    private class NativeEvent
    {
        int code;
        String raw;

        NativeEvent(int paramString, String arg3)
        {
            this.code = paramString;
            Object localObject;
            this.raw = localObject;
        }
    }

    class NativeResponseCode
    {
        public static final int SERVICE_DISCOVERY_FAILED = 602;
        public static final int SERVICE_FOUND = 603;
        public static final int SERVICE_GET_ADDR_FAILED = 611;
        public static final int SERVICE_GET_ADDR_SUCCESS = 612;
        public static final int SERVICE_LOST = 604;
        public static final int SERVICE_REGISTERED = 606;
        public static final int SERVICE_REGISTRATION_FAILED = 605;
        public static final int SERVICE_RESOLUTION_FAILED = 607;
        public static final int SERVICE_RESOLVED = 608;
        public static final int SERVICE_UPDATED = 609;
        public static final int SERVICE_UPDATE_FAILED = 610;

        NativeResponseCode()
        {
        }
    }

    private class NsdStateMachine extends StateMachine
    {
        private final DefaultState mDefaultState = new DefaultState();
        private final DisabledState mDisabledState = new DisabledState();
        private final EnabledState mEnabledState = new EnabledState();

        NsdStateMachine(String arg2)
        {
            super();
            addState(this.mDefaultState);
            addState(this.mDisabledState, this.mDefaultState);
            addState(this.mEnabledState, this.mDefaultState);
            if (NsdService.this.isNsdEnabled())
                setInitialState(this.mEnabledState);
            while (true)
            {
                setProcessedMessagesSize(25);
                registerForNsdSetting();
                return;
                setInitialState(this.mDisabledState);
            }
        }

        private void registerForNsdSetting()
        {
            ContentObserver local1 = new ContentObserver(getHandler())
            {
                public void onChange(boolean paramAnonymousBoolean)
                {
                    if (NsdService.this.isNsdEnabled())
                        NsdService.this.mNsdStateMachine.sendMessage(393240);
                    while (true)
                    {
                        return;
                        NsdService.this.mNsdStateMachine.sendMessage(393241);
                    }
                }
            };
            NsdService.this.mContext.getContentResolver().registerContentObserver(Settings.Secure.getUriFor("nsd_on"), false, local1);
        }

        protected String getMessageInfo(Message paramMessage)
        {
            return NsdService.cmdToString(paramMessage.what);
        }

        class EnabledState extends State
        {
            EnabledState()
            {
            }

            private void removeRequestMap(int paramInt1, int paramInt2, NsdService.ClientInfo paramClientInfo)
            {
                paramClientInfo.mClientIds.remove(paramInt1);
                NsdService.this.mIdToClientInfoMap.remove(paramInt2);
            }

            private boolean requestLimitReached(NsdService.ClientInfo paramClientInfo)
            {
                if (paramClientInfo.mClientIds.size() >= 10)
                    Slog.d("NsdService", "Exceeded max outstanding requests " + paramClientInfo);
                for (boolean bool = true; ; bool = false)
                    return bool;
            }

            private void storeRequestMap(int paramInt1, int paramInt2, NsdService.ClientInfo paramClientInfo)
            {
                paramClientInfo.mClientIds.put(paramInt1, Integer.valueOf(paramInt2));
                NsdService.this.mIdToClientInfoMap.put(paramInt2, paramClientInfo);
            }

            public void enter()
            {
                NsdService.this.sendNsdStateChangeBroadcast(true);
                if (NsdService.this.mClients.size() > 0)
                    NsdService.this.startMDnsDaemon();
            }

            public void exit()
            {
                if (NsdService.this.mClients.size() > 0)
                    NsdService.this.stopMDnsDaemon();
            }

            public boolean processMessage(Message paramMessage)
            {
                boolean bool = true;
                switch (paramMessage.what)
                {
                default:
                    bool = false;
                case 69632:
                case 69636:
                case 393241:
                case 393217:
                case 393222:
                case 393225:
                case 393228:
                case 393234:
                case 393242:
                }
                while (true)
                {
                    return bool;
                    if ((paramMessage.arg1 == 0) && (NsdService.this.mClients.size() == 0))
                        NsdService.this.startMDnsDaemon();
                    bool = false;
                    continue;
                    if (NsdService.this.mClients.size() == 1)
                        NsdService.this.stopMDnsDaemon();
                    bool = false;
                    continue;
                    NsdService.NsdStateMachine.this.transitionTo(NsdService.NsdStateMachine.this.mDisabledState);
                    continue;
                    Slog.d("NsdService", "Discover services");
                    NsdServiceInfo localNsdServiceInfo2 = (NsdServiceInfo)paramMessage.obj;
                    NsdService.ClientInfo localClientInfo5 = (NsdService.ClientInfo)NsdService.this.mClients.get(paramMessage.replyTo);
                    if (requestLimitReached(localClientInfo5))
                    {
                        NsdService.this.replyToMessage(paramMessage, 393219, 4);
                    }
                    else
                    {
                        int n = NsdService.this.getUniqueId();
                        if (NsdService.this.discoverServices(n, localNsdServiceInfo2.getServiceType()))
                        {
                            Slog.d("NsdService", "Discover " + paramMessage.arg2 + " " + n + localNsdServiceInfo2.getServiceType());
                            storeRequestMap(paramMessage.arg2, n, localClientInfo5);
                            NsdService.this.replyToMessage(paramMessage, 393218, localNsdServiceInfo2);
                        }
                        else
                        {
                            NsdService.this.stopServiceDiscovery(n);
                            NsdService.this.replyToMessage(paramMessage, 393219, 0);
                            continue;
                            Slog.d("NsdService", "Stop service discovery");
                            NsdService.ClientInfo localClientInfo4 = (NsdService.ClientInfo)NsdService.this.mClients.get(paramMessage.replyTo);
                            try
                            {
                                int m = ((Integer)localClientInfo4.mClientIds.get(paramMessage.arg2)).intValue();
                                removeRequestMap(paramMessage.arg2, m, localClientInfo4);
                                if (!NsdService.this.stopServiceDiscovery(m))
                                    break label496;
                                NsdService.this.replyToMessage(paramMessage, 393224);
                            }
                            catch (NullPointerException localNullPointerException2)
                            {
                                NsdService.this.replyToMessage(paramMessage, 393223, 0);
                            }
                            continue;
                            label496: NsdService.this.replyToMessage(paramMessage, 393223, 0);
                            continue;
                            Slog.d("NsdService", "Register service");
                            NsdService.ClientInfo localClientInfo3 = (NsdService.ClientInfo)NsdService.this.mClients.get(paramMessage.replyTo);
                            if (requestLimitReached(localClientInfo3))
                            {
                                NsdService.this.replyToMessage(paramMessage, 393226, 4);
                            }
                            else
                            {
                                int k = NsdService.this.getUniqueId();
                                if (NsdService.this.registerService(k, (NsdServiceInfo)paramMessage.obj))
                                {
                                    Slog.d("NsdService", "Register " + paramMessage.arg2 + " " + k);
                                    storeRequestMap(paramMessage.arg2, k, localClientInfo3);
                                }
                                else
                                {
                                    NsdService.this.unregisterService(k);
                                    NsdService.this.replyToMessage(paramMessage, 393226, 0);
                                    continue;
                                    Slog.d("NsdService", "unregister service");
                                    NsdService.ClientInfo localClientInfo2 = (NsdService.ClientInfo)NsdService.this.mClients.get(paramMessage.replyTo);
                                    try
                                    {
                                        int j = ((Integer)localClientInfo2.mClientIds.get(paramMessage.arg2)).intValue();
                                        removeRequestMap(paramMessage.arg2, j, localClientInfo2);
                                        if (!NsdService.this.unregisterService(j))
                                            break label798;
                                        NsdService.this.replyToMessage(paramMessage, 393230);
                                    }
                                    catch (NullPointerException localNullPointerException1)
                                    {
                                        NsdService.this.replyToMessage(paramMessage, 393229, 0);
                                    }
                                    continue;
                                    label798: NsdService.this.replyToMessage(paramMessage, 393229, 0);
                                    continue;
                                    Slog.d("NsdService", "Resolve service");
                                    NsdServiceInfo localNsdServiceInfo1 = (NsdServiceInfo)paramMessage.obj;
                                    NsdService.ClientInfo localClientInfo1 = (NsdService.ClientInfo)NsdService.this.mClients.get(paramMessage.replyTo);
                                    if (localClientInfo1.mResolvedService != null)
                                    {
                                        NsdService.this.replyToMessage(paramMessage, 393235, 3);
                                    }
                                    else
                                    {
                                        int i = NsdService.this.getUniqueId();
                                        if (NsdService.this.resolveService(i, localNsdServiceInfo1))
                                        {
                                            NsdService.ClientInfo.access$2302(localClientInfo1, new NsdServiceInfo());
                                            storeRequestMap(paramMessage.arg2, i, localClientInfo1);
                                        }
                                        else
                                        {
                                            NsdService.this.replyToMessage(paramMessage, 393235, 0);
                                            continue;
                                            NsdService.NativeEvent localNativeEvent = (NsdService.NativeEvent)paramMessage.obj;
                                            NsdService.this.handleNativeEvent(localNativeEvent.code, localNativeEvent.raw, NativeDaemonEvent.unescapeArgs(localNativeEvent.raw));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        class DisabledState extends State
        {
            DisabledState()
            {
            }

            public void enter()
            {
                NsdService.this.sendNsdStateChangeBroadcast(false);
            }

            public boolean processMessage(Message paramMessage)
            {
                switch (paramMessage.what)
                {
                default:
                case 393240:
                }
                for (boolean bool = false; ; bool = true)
                {
                    return bool;
                    NsdService.NsdStateMachine.this.transitionTo(NsdService.NsdStateMachine.this.mEnabledState);
                }
            }
        }

        class DefaultState extends State
        {
            DefaultState()
            {
            }

            public boolean processMessage(Message paramMessage)
            {
                boolean bool = false;
                switch (paramMessage.what)
                {
                default:
                    Slog.e("NsdService", "Unhandled " + paramMessage);
                    return bool;
                case 69632:
                    if (paramMessage.arg1 == 0)
                    {
                        AsyncChannel localAsyncChannel = (AsyncChannel)paramMessage.obj;
                        Slog.d("NsdService", "New client listening to asynchronous messages");
                        localAsyncChannel.sendMessage(69634);
                        NsdService.ClientInfo localClientInfo = new NsdService.ClientInfo(NsdService.this, localAsyncChannel, paramMessage.replyTo, null);
                        NsdService.this.mClients.put(paramMessage.replyTo, localClientInfo);
                    }
                    break;
                case 69636:
                case 69633:
                case 393217:
                case 393222:
                case 393225:
                case 393228:
                case 393234:
                }
                while (true)
                {
                    bool = true;
                    break;
                    Slog.e("NsdService", "Client connection failure, error=" + paramMessage.arg1);
                    continue;
                    if (paramMessage.arg1 == 2)
                        Slog.e("NsdService", "Send failed, client connection lost");
                    while (true)
                    {
                        NsdService.this.mClients.remove(paramMessage.replyTo);
                        break;
                        Slog.d("NsdService", "Client connection lost with reason: " + paramMessage.arg1);
                    }
                    new AsyncChannel().connect(NsdService.this.mContext, NsdService.NsdStateMachine.this.getHandler(), paramMessage.replyTo);
                    continue;
                    NsdService.this.replyToMessage(paramMessage, 393219, 0);
                    continue;
                    NsdService.this.replyToMessage(paramMessage, 393223, 0);
                    continue;
                    NsdService.this.replyToMessage(paramMessage, 393226, 0);
                    continue;
                    NsdService.this.replyToMessage(paramMessage, 393229, 0);
                    continue;
                    NsdService.this.replyToMessage(paramMessage, 393235, 0);
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.NsdService
 * JD-Core Version:        0.6.2
 */