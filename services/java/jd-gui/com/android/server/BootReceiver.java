package com.android.server;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.DropBoxManager;
import android.os.FileObserver;
import android.os.FileUtils;
import android.os.RecoverySystem;
import android.os.SystemProperties;
import android.provider.Downloads;
import android.util.Slog;
import java.io.File;
import java.io.IOException;

public class BootReceiver extends BroadcastReceiver
{
    private static final int LOG_SIZE = 0;
    private static final String OLD_UPDATER_CLASS = "com.google.android.systemupdater.SystemUpdateReceiver";
    private static final String OLD_UPDATER_PACKAGE = "com.google.android.systemupdater";
    private static final String TAG = "BootReceiver";
    private static final File TOMBSTONE_DIR;
    private static FileObserver sTombstoneObserver;

    static
    {
        if (SystemProperties.getInt("ro.debuggable", 0) == 1);
        for (int i = 98304; ; i = 65536)
        {
            LOG_SIZE = i;
            TOMBSTONE_DIR = new File("/data/tombstones");
            sTombstoneObserver = null;
            return;
        }
    }

    private static void addFileToDropBox(DropBoxManager paramDropBoxManager, SharedPreferences paramSharedPreferences, String paramString1, String paramString2, int paramInt, String paramString3)
        throws IOException
    {
        if ((paramDropBoxManager == null) || (!paramDropBoxManager.isTagEnabled(paramString3)));
        while (true)
        {
            return;
            File localFile = new File(paramString2);
            long l = localFile.lastModified();
            if (l > 0L)
                if (paramSharedPreferences != null)
                {
                    if (paramSharedPreferences.getLong(paramString2, 0L) != l)
                        paramSharedPreferences.edit().putLong(paramString2, l).apply();
                }
                else
                {
                    Slog.i("BootReceiver", "Copying " + paramString2 + " to DropBox (" + paramString3 + ")");
                    paramDropBoxManager.addText(paramString3, paramString1 + FileUtils.readTextFile(localFile, paramInt, "[[TRUNCATED]]\n"));
                }
        }
    }

    private void logBootEvents(Context paramContext)
        throws IOException
    {
        final DropBoxManager localDropBoxManager = (DropBoxManager)paramContext.getSystemService("dropbox");
        final SharedPreferences localSharedPreferences = paramContext.getSharedPreferences("log_files", 0);
        final String str1 = 512 + "Build: " + Build.FINGERPRINT + "\n" + "Hardware: " + Build.BOARD + "\n" + "Bootloader: " + Build.BOOTLOADER + "\n" + "Radio: " + Build.RADIO + "\n" + "Kernel: " + FileUtils.readTextFile(new File("/proc/version"), 1024, "...\n") + "\n";
        String str2 = RecoverySystem.handleAftermath();
        if ((str2 != null) && (localDropBoxManager != null))
            localDropBoxManager.addText("SYSTEM_RECOVERY_LOG", str1 + str2);
        if (SystemProperties.getLong("ro.runtime.firstboot", 0L) == 0L)
        {
            SystemProperties.set("ro.runtime.firstboot", Long.toString(System.currentTimeMillis()));
            if (localDropBoxManager != null)
                localDropBoxManager.addText("SYSTEM_BOOT", str1);
            addFileToDropBox(localDropBoxManager, localSharedPreferences, str1, "/proc/last_kmsg", -LOG_SIZE, "SYSTEM_LAST_KMSG");
            addFileToDropBox(localDropBoxManager, localSharedPreferences, str1, "/cache/recovery/log", -LOG_SIZE, "SYSTEM_RECOVERY_LOG");
            addFileToDropBox(localDropBoxManager, localSharedPreferences, str1, "/data/dontpanic/apanic_console", -LOG_SIZE, "APANIC_CONSOLE");
            addFileToDropBox(localDropBoxManager, localSharedPreferences, str1, "/data/dontpanic/apanic_threads", -LOG_SIZE, "APANIC_THREADS");
        }
        while (true)
        {
            File[] arrayOfFile = TOMBSTONE_DIR.listFiles();
            for (int i = 0; (arrayOfFile != null) && (i < arrayOfFile.length); i++)
                addFileToDropBox(localDropBoxManager, localSharedPreferences, str1, arrayOfFile[i].getPath(), LOG_SIZE, "SYSTEM_TOMBSTONE");
            if (localDropBoxManager != null)
                localDropBoxManager.addText("SYSTEM_RESTART", str1);
        }
        sTombstoneObserver = new FileObserver(TOMBSTONE_DIR.getPath(), 8)
        {
            public void onEvent(int paramAnonymousInt, String paramAnonymousString)
            {
                try
                {
                    String str = new File(BootReceiver.TOMBSTONE_DIR, paramAnonymousString).getPath();
                    BootReceiver.addFileToDropBox(localDropBoxManager, localSharedPreferences, str1, str, BootReceiver.LOG_SIZE, "SYSTEM_TOMBSTONE");
                    return;
                }
                catch (IOException localIOException)
                {
                    while (true)
                        Slog.e("BootReceiver", "Can't log tombstone", localIOException);
                }
            }
        };
        sTombstoneObserver.startWatching();
    }

    private void removeOldUpdatePackages(Context paramContext)
    {
        Downloads.removeAllDownloadsByPackage(paramContext, "com.google.android.systemupdater", "com.google.android.systemupdater.SystemUpdateReceiver");
    }

    public void onReceive(final Context paramContext, Intent paramIntent)
    {
        new Thread()
        {
            public void run()
            {
                try
                {
                    BootReceiver.this.logBootEvents(paramContext);
                }
                catch (Exception localException1)
                {
                    try
                    {
                        while (true)
                        {
                            BootReceiver.this.removeOldUpdatePackages(paramContext);
                            return;
                            localException1 = localException1;
                            Slog.e("BootReceiver", "Can't log boot events", localException1);
                        }
                    }
                    catch (Exception localException2)
                    {
                        while (true)
                            Slog.e("BootReceiver", "Can't remove old update packages", localException2);
                    }
                }
            }
        }
        .start();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.BootReceiver
 * JD-Core Version:        0.6.2
 */