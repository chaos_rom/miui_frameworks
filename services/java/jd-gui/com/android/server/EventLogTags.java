package com.android.server;

import android.util.EventLog;

public class EventLogTags
{
    public static final int BACKUP_AGENT_FAILURE = 2823;
    public static final int BACKUP_DATA_CHANGED = 2820;
    public static final int BACKUP_INITIALIZE = 2827;
    public static final int BACKUP_PACKAGE = 2824;
    public static final int BACKUP_RESET = 2826;
    public static final int BACKUP_START = 2821;
    public static final int BACKUP_SUCCESS = 2825;
    public static final int BACKUP_TRANSPORT_FAILURE = 2822;
    public static final int BATTERY_DISCHARGE = 2730;
    public static final int BATTERY_LEVEL = 2722;
    public static final int BATTERY_STATUS = 2723;
    public static final int BOOT_PROGRESS_PMS_DATA_SCAN_START = 3080;
    public static final int BOOT_PROGRESS_PMS_READY = 3100;
    public static final int BOOT_PROGRESS_PMS_SCAN_END = 3090;
    public static final int BOOT_PROGRESS_PMS_START = 3060;
    public static final int BOOT_PROGRESS_PMS_SYSTEM_SCAN_START = 3070;
    public static final int BOOT_PROGRESS_SYSTEM_RUN = 3010;
    public static final int CACHE_FILE_DELETED = 2748;
    public static final int CONNECTIVITY_STATE_CHANGED = 50020;
    public static final int FREE_STORAGE_CHANGED = 2744;
    public static final int FREE_STORAGE_LEFT = 2746;
    public static final int IMF_FORCE_RECONNECT_IME = 32000;
    public static final int LOW_STORAGE = 2745;
    public static final int NETSTATS_MOBILE_SAMPLE = 51100;
    public static final int NETSTATS_WIFI_SAMPLE = 51101;
    public static final int NOTIFICATION_CANCEL = 2751;
    public static final int NOTIFICATION_CANCEL_ALL = 2752;
    public static final int NOTIFICATION_ENQUEUE = 2750;
    public static final int POWER_PARTIAL_WAKE_STATE = 2729;
    public static final int POWER_SCREEN_BROADCAST_DONE = 2726;
    public static final int POWER_SCREEN_BROADCAST_SEND = 2725;
    public static final int POWER_SCREEN_BROADCAST_STOP = 2727;
    public static final int POWER_SCREEN_STATE = 2728;
    public static final int POWER_SLEEP_REQUESTED = 2724;
    public static final int RESTORE_AGENT_FAILURE = 2832;
    public static final int RESTORE_PACKAGE = 2833;
    public static final int RESTORE_START = 2830;
    public static final int RESTORE_SUCCESS = 2834;
    public static final int RESTORE_TRANSPORT_FAILURE = 2831;
    public static final int WATCHDOG = 2802;
    public static final int WATCHDOG_HARD_RESET = 2805;
    public static final int WATCHDOG_MEMINFO = 2809;
    public static final int WATCHDOG_PROC_PSS = 2803;
    public static final int WATCHDOG_PROC_STATS = 2807;
    public static final int WATCHDOG_PSS_STATS = 2806;
    public static final int WATCHDOG_REQUESTED_REBOOT = 2811;
    public static final int WATCHDOG_SCHEDULED_REBOOT = 2808;
    public static final int WATCHDOG_SOFT_RESET = 2804;
    public static final int WATCHDOG_VMSTAT = 2810;
    public static final int WM_NO_SURFACE_MEMORY = 31000;

    public static void writeBackupAgentFailure(String paramString1, String paramString2)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = paramString1;
        arrayOfObject[1] = paramString2;
        EventLog.writeEvent(2823, arrayOfObject);
    }

    public static void writeBackupDataChanged(String paramString)
    {
        EventLog.writeEvent(2820, paramString);
    }

    public static void writeBackupInitialize()
    {
        EventLog.writeEvent(2827, new Object[0]);
    }

    public static void writeBackupPackage(String paramString, int paramInt)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = paramString;
        arrayOfObject[1] = Integer.valueOf(paramInt);
        EventLog.writeEvent(2824, arrayOfObject);
    }

    public static void writeBackupReset(String paramString)
    {
        EventLog.writeEvent(2826, paramString);
    }

    public static void writeBackupStart(String paramString)
    {
        EventLog.writeEvent(2821, paramString);
    }

    public static void writeBackupSuccess(int paramInt1, int paramInt2)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        EventLog.writeEvent(2825, arrayOfObject);
    }

    public static void writeBackupTransportFailure(String paramString)
    {
        EventLog.writeEvent(2822, paramString);
    }

    public static void writeBatteryDischarge(long paramLong, int paramInt1, int paramInt2)
    {
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = Long.valueOf(paramLong);
        arrayOfObject[1] = Integer.valueOf(paramInt1);
        arrayOfObject[2] = Integer.valueOf(paramInt2);
        EventLog.writeEvent(2730, arrayOfObject);
    }

    public static void writeBatteryLevel(int paramInt1, int paramInt2, int paramInt3)
    {
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = Integer.valueOf(paramInt3);
        EventLog.writeEvent(2722, arrayOfObject);
    }

    public static void writeBatteryStatus(int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString)
    {
        Object[] arrayOfObject = new Object[5];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = Integer.valueOf(paramInt3);
        arrayOfObject[3] = Integer.valueOf(paramInt4);
        arrayOfObject[4] = paramString;
        EventLog.writeEvent(2723, arrayOfObject);
    }

    public static void writeBootProgressPmsDataScanStart(long paramLong)
    {
        EventLog.writeEvent(3080, paramLong);
    }

    public static void writeBootProgressPmsReady(long paramLong)
    {
        EventLog.writeEvent(3100, paramLong);
    }

    public static void writeBootProgressPmsScanEnd(long paramLong)
    {
        EventLog.writeEvent(3090, paramLong);
    }

    public static void writeBootProgressPmsStart(long paramLong)
    {
        EventLog.writeEvent(3060, paramLong);
    }

    public static void writeBootProgressPmsSystemScanStart(long paramLong)
    {
        EventLog.writeEvent(3070, paramLong);
    }

    public static void writeBootProgressSystemRun(long paramLong)
    {
        EventLog.writeEvent(3010, paramLong);
    }

    public static void writeCacheFileDeleted(String paramString)
    {
        EventLog.writeEvent(2748, paramString);
    }

    public static void writeConnectivityStateChanged(int paramInt)
    {
        EventLog.writeEvent(50020, paramInt);
    }

    public static void writeFreeStorageChanged(long paramLong)
    {
        EventLog.writeEvent(2744, paramLong);
    }

    public static void writeFreeStorageLeft(long paramLong1, long paramLong2, long paramLong3)
    {
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = Long.valueOf(paramLong1);
        arrayOfObject[1] = Long.valueOf(paramLong2);
        arrayOfObject[2] = Long.valueOf(paramLong3);
        EventLog.writeEvent(2746, arrayOfObject);
    }

    public static void writeImfForceReconnectIme(Object[] paramArrayOfObject, long paramLong, int paramInt)
    {
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = paramArrayOfObject;
        arrayOfObject[1] = Long.valueOf(paramLong);
        arrayOfObject[2] = Integer.valueOf(paramInt);
        EventLog.writeEvent(32000, arrayOfObject);
    }

    public static void writeLowStorage(long paramLong)
    {
        EventLog.writeEvent(2745, paramLong);
    }

    public static void writeNetstatsMobileSample(long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5, long paramLong6, long paramLong7, long paramLong8, long paramLong9, long paramLong10, long paramLong11, long paramLong12, long paramLong13)
    {
        Object[] arrayOfObject = new Object[13];
        arrayOfObject[0] = Long.valueOf(paramLong1);
        arrayOfObject[1] = Long.valueOf(paramLong2);
        arrayOfObject[2] = Long.valueOf(paramLong3);
        arrayOfObject[3] = Long.valueOf(paramLong4);
        arrayOfObject[4] = Long.valueOf(paramLong5);
        arrayOfObject[5] = Long.valueOf(paramLong6);
        arrayOfObject[6] = Long.valueOf(paramLong7);
        arrayOfObject[7] = Long.valueOf(paramLong8);
        arrayOfObject[8] = Long.valueOf(paramLong9);
        arrayOfObject[9] = Long.valueOf(paramLong10);
        arrayOfObject[10] = Long.valueOf(paramLong11);
        arrayOfObject[11] = Long.valueOf(paramLong12);
        arrayOfObject[12] = Long.valueOf(paramLong13);
        EventLog.writeEvent(51100, arrayOfObject);
    }

    public static void writeNetstatsWifiSample(long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5, long paramLong6, long paramLong7, long paramLong8, long paramLong9, long paramLong10, long paramLong11, long paramLong12, long paramLong13)
    {
        Object[] arrayOfObject = new Object[13];
        arrayOfObject[0] = Long.valueOf(paramLong1);
        arrayOfObject[1] = Long.valueOf(paramLong2);
        arrayOfObject[2] = Long.valueOf(paramLong3);
        arrayOfObject[3] = Long.valueOf(paramLong4);
        arrayOfObject[4] = Long.valueOf(paramLong5);
        arrayOfObject[5] = Long.valueOf(paramLong6);
        arrayOfObject[6] = Long.valueOf(paramLong7);
        arrayOfObject[7] = Long.valueOf(paramLong8);
        arrayOfObject[8] = Long.valueOf(paramLong9);
        arrayOfObject[9] = Long.valueOf(paramLong10);
        arrayOfObject[10] = Long.valueOf(paramLong11);
        arrayOfObject[11] = Long.valueOf(paramLong12);
        arrayOfObject[12] = Long.valueOf(paramLong13);
        EventLog.writeEvent(51101, arrayOfObject);
    }

    public static void writeNotificationCancel(String paramString1, int paramInt1, String paramString2, int paramInt2, int paramInt3)
    {
        Object[] arrayOfObject = new Object[5];
        arrayOfObject[0] = paramString1;
        arrayOfObject[1] = Integer.valueOf(paramInt1);
        arrayOfObject[2] = paramString2;
        arrayOfObject[3] = Integer.valueOf(paramInt2);
        arrayOfObject[4] = Integer.valueOf(paramInt3);
        EventLog.writeEvent(2751, arrayOfObject);
    }

    public static void writeNotificationCancelAll(String paramString, int paramInt1, int paramInt2)
    {
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = paramString;
        arrayOfObject[1] = Integer.valueOf(paramInt1);
        arrayOfObject[2] = Integer.valueOf(paramInt2);
        EventLog.writeEvent(2752, arrayOfObject);
    }

    public static void writeNotificationEnqueue(String paramString1, int paramInt, String paramString2, String paramString3)
    {
        Object[] arrayOfObject = new Object[4];
        arrayOfObject[0] = paramString1;
        arrayOfObject[1] = Integer.valueOf(paramInt);
        arrayOfObject[2] = paramString2;
        arrayOfObject[3] = paramString3;
        EventLog.writeEvent(2750, arrayOfObject);
    }

    public static void writePowerPartialWakeState(int paramInt, String paramString)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = Integer.valueOf(paramInt);
        arrayOfObject[1] = paramString;
        EventLog.writeEvent(2729, arrayOfObject);
    }

    public static void writePowerScreenBroadcastDone(int paramInt1, long paramLong, int paramInt2)
    {
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Long.valueOf(paramLong);
        arrayOfObject[2] = Integer.valueOf(paramInt2);
        EventLog.writeEvent(2726, arrayOfObject);
    }

    public static void writePowerScreenBroadcastSend(int paramInt)
    {
        EventLog.writeEvent(2725, paramInt);
    }

    public static void writePowerScreenBroadcastStop(int paramInt1, int paramInt2)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        EventLog.writeEvent(2727, arrayOfObject);
    }

    public static void writePowerScreenState(int paramInt1, int paramInt2, long paramLong, int paramInt3)
    {
        Object[] arrayOfObject = new Object[4];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = Long.valueOf(paramLong);
        arrayOfObject[3] = Integer.valueOf(paramInt3);
        EventLog.writeEvent(2728, arrayOfObject);
    }

    public static void writePowerSleepRequested(int paramInt)
    {
        EventLog.writeEvent(2724, paramInt);
    }

    public static void writeRestoreAgentFailure(String paramString1, String paramString2)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = paramString1;
        arrayOfObject[1] = paramString2;
        EventLog.writeEvent(2832, arrayOfObject);
    }

    public static void writeRestorePackage(String paramString, int paramInt)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = paramString;
        arrayOfObject[1] = Integer.valueOf(paramInt);
        EventLog.writeEvent(2833, arrayOfObject);
    }

    public static void writeRestoreStart(String paramString, long paramLong)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = paramString;
        arrayOfObject[1] = Long.valueOf(paramLong);
        EventLog.writeEvent(2830, arrayOfObject);
    }

    public static void writeRestoreSuccess(int paramInt1, int paramInt2)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        EventLog.writeEvent(2834, arrayOfObject);
    }

    public static void writeRestoreTransportFailure()
    {
        EventLog.writeEvent(2831, new Object[0]);
    }

    public static void writeWatchdog(String paramString)
    {
        EventLog.writeEvent(2802, paramString);
    }

    public static void writeWatchdogHardReset(String paramString, int paramInt1, int paramInt2, int paramInt3)
    {
        Object[] arrayOfObject = new Object[4];
        arrayOfObject[0] = paramString;
        arrayOfObject[1] = Integer.valueOf(paramInt1);
        arrayOfObject[2] = Integer.valueOf(paramInt2);
        arrayOfObject[3] = Integer.valueOf(paramInt3);
        EventLog.writeEvent(2805, arrayOfObject);
    }

    public static void writeWatchdogMeminfo(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10, int paramInt11)
    {
        Object[] arrayOfObject = new Object[11];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = Integer.valueOf(paramInt3);
        arrayOfObject[3] = Integer.valueOf(paramInt4);
        arrayOfObject[4] = Integer.valueOf(paramInt5);
        arrayOfObject[5] = Integer.valueOf(paramInt6);
        arrayOfObject[6] = Integer.valueOf(paramInt7);
        arrayOfObject[7] = Integer.valueOf(paramInt8);
        arrayOfObject[8] = Integer.valueOf(paramInt9);
        arrayOfObject[9] = Integer.valueOf(paramInt10);
        arrayOfObject[10] = Integer.valueOf(paramInt11);
        EventLog.writeEvent(2809, arrayOfObject);
    }

    public static void writeWatchdogProcPss(String paramString, int paramInt1, int paramInt2)
    {
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = paramString;
        arrayOfObject[1] = Integer.valueOf(paramInt1);
        arrayOfObject[2] = Integer.valueOf(paramInt2);
        EventLog.writeEvent(2803, arrayOfObject);
    }

    public static void writeWatchdogProcStats(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        Object[] arrayOfObject = new Object[5];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = Integer.valueOf(paramInt3);
        arrayOfObject[3] = Integer.valueOf(paramInt4);
        arrayOfObject[4] = Integer.valueOf(paramInt5);
        EventLog.writeEvent(2807, arrayOfObject);
    }

    public static void writeWatchdogPssStats(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10, int paramInt11)
    {
        Object[] arrayOfObject = new Object[11];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = Integer.valueOf(paramInt3);
        arrayOfObject[3] = Integer.valueOf(paramInt4);
        arrayOfObject[4] = Integer.valueOf(paramInt5);
        arrayOfObject[5] = Integer.valueOf(paramInt6);
        arrayOfObject[6] = Integer.valueOf(paramInt7);
        arrayOfObject[7] = Integer.valueOf(paramInt8);
        arrayOfObject[8] = Integer.valueOf(paramInt9);
        arrayOfObject[9] = Integer.valueOf(paramInt10);
        arrayOfObject[10] = Integer.valueOf(paramInt11);
        EventLog.writeEvent(2806, arrayOfObject);
    }

    public static void writeWatchdogRequestedReboot(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7)
    {
        Object[] arrayOfObject = new Object[7];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = Integer.valueOf(paramInt3);
        arrayOfObject[3] = Integer.valueOf(paramInt4);
        arrayOfObject[4] = Integer.valueOf(paramInt5);
        arrayOfObject[5] = Integer.valueOf(paramInt6);
        arrayOfObject[6] = Integer.valueOf(paramInt7);
        EventLog.writeEvent(2811, arrayOfObject);
    }

    public static void writeWatchdogScheduledReboot(long paramLong, int paramInt1, int paramInt2, int paramInt3, String paramString)
    {
        Object[] arrayOfObject = new Object[5];
        arrayOfObject[0] = Long.valueOf(paramLong);
        arrayOfObject[1] = Integer.valueOf(paramInt1);
        arrayOfObject[2] = Integer.valueOf(paramInt2);
        arrayOfObject[3] = Integer.valueOf(paramInt3);
        arrayOfObject[4] = paramString;
        EventLog.writeEvent(2808, arrayOfObject);
    }

    public static void writeWatchdogSoftReset(String paramString1, int paramInt1, int paramInt2, int paramInt3, String paramString2)
    {
        Object[] arrayOfObject = new Object[5];
        arrayOfObject[0] = paramString1;
        arrayOfObject[1] = Integer.valueOf(paramInt1);
        arrayOfObject[2] = Integer.valueOf(paramInt2);
        arrayOfObject[3] = Integer.valueOf(paramInt3);
        arrayOfObject[4] = paramString2;
        EventLog.writeEvent(2804, arrayOfObject);
    }

    public static void writeWatchdogVmstat(long paramLong, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
    {
        Object[] arrayOfObject = new Object[6];
        arrayOfObject[0] = Long.valueOf(paramLong);
        arrayOfObject[1] = Integer.valueOf(paramInt1);
        arrayOfObject[2] = Integer.valueOf(paramInt2);
        arrayOfObject[3] = Integer.valueOf(paramInt3);
        arrayOfObject[4] = Integer.valueOf(paramInt4);
        arrayOfObject[5] = Integer.valueOf(paramInt5);
        EventLog.writeEvent(2810, arrayOfObject);
    }

    public static void writeWmNoSurfaceMemory(String paramString1, int paramInt, String paramString2)
    {
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = paramString1;
        arrayOfObject[1] = Integer.valueOf(paramInt);
        arrayOfObject[2] = paramString2;
        EventLog.writeEvent(31000, arrayOfObject);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.EventLogTags
 * JD-Core Version:        0.6.2
 */