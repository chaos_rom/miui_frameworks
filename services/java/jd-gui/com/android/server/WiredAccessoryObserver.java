package com.android.server;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.UEventObserver;
import android.os.UEventObserver.UEvent;
import android.util.Log;
import android.util.Slog;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

class WiredAccessoryObserver extends UEventObserver
{
    private static final int BIT_HDMI_AUDIO = 16;
    private static final int BIT_HEADSET = 1;
    private static final int BIT_HEADSET_NO_MIC = 2;
    private static final int BIT_USB_HEADSET_ANLG = 4;
    private static final int BIT_USB_HEADSET_DGTL = 8;
    private static final int HEADSETS_WITH_MIC = 1;
    private static final boolean LOG = true;
    private static final int SUPPORTED_HEADSETS = 31;
    private static final String TAG = WiredAccessoryObserver.class.getSimpleName();
    private static List<UEventInfo> uEventInfo = makeObservedUEventList();
    private final AudioManager mAudioManager;
    private final Context mContext;
    private final Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            WiredAccessoryObserver.this.setDevicesState(paramAnonymousMessage.arg1, paramAnonymousMessage.arg2, (String)paramAnonymousMessage.obj);
            WiredAccessoryObserver.this.mWakeLock.release();
        }
    };
    private String mHeadsetName;
    private int mHeadsetState;
    private int mPrevHeadsetState;
    private final PowerManager.WakeLock mWakeLock;

    public WiredAccessoryObserver(Context paramContext)
    {
        this.mContext = paramContext;
        this.mWakeLock = ((PowerManager)paramContext.getSystemService("power")).newWakeLock(1, "WiredAccessoryObserver");
        this.mWakeLock.setReferenceCounted(false);
        this.mAudioManager = ((AudioManager)paramContext.getSystemService("audio"));
        paramContext.registerReceiver(new BootCompletedReceiver(null), new IntentFilter("android.intent.action.BOOT_COMPLETED"), null, null);
    }

    /** @deprecated */
    // ERROR //
    private final void init()
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: sipush 1024
        //     5: newarray char
        //     7: astore_2
        //     8: aload_0
        //     9: aload_0
        //     10: getfield 134	com/android/server/WiredAccessoryObserver:mHeadsetState	I
        //     13: putfield 136	com/android/server/WiredAccessoryObserver:mPrevHeadsetState	I
        //     16: getstatic 54	com/android/server/WiredAccessoryObserver:TAG	Ljava/lang/String;
        //     19: ldc 138
        //     21: invokestatic 144	android/util/Slog:v	(Ljava/lang/String;Ljava/lang/String;)I
        //     24: pop
        //     25: iconst_0
        //     26: istore 4
        //     28: iload 4
        //     30: getstatic 60	com/android/server/WiredAccessoryObserver:uEventInfo	Ljava/util/List;
        //     33: invokeinterface 150 1 0
        //     38: if_icmpge +154 -> 192
        //     41: getstatic 60	com/android/server/WiredAccessoryObserver:uEventInfo	Ljava/util/List;
        //     44: iload 4
        //     46: invokeinterface 154 2 0
        //     51: checkcast 11	com/android/server/WiredAccessoryObserver$UEventInfo
        //     54: astore 5
        //     56: new 156	java/io/FileReader
        //     59: dup
        //     60: aload 5
        //     62: invokevirtual 159	com/android/server/WiredAccessoryObserver$UEventInfo:getSwitchStatePath	()Ljava/lang/String;
        //     65: invokespecial 160	java/io/FileReader:<init>	(Ljava/lang/String;)V
        //     68: astore 6
        //     70: aload 6
        //     72: aload_2
        //     73: iconst_0
        //     74: sipush 1024
        //     77: invokevirtual 164	java/io/FileReader:read	([CII)I
        //     80: istore 11
        //     82: aload 6
        //     84: invokevirtual 167	java/io/FileReader:close	()V
        //     87: new 169	java/lang/String
        //     90: dup
        //     91: aload_2
        //     92: iconst_0
        //     93: iload 11
        //     95: invokespecial 172	java/lang/String:<init>	([CII)V
        //     98: invokevirtual 175	java/lang/String:trim	()Ljava/lang/String;
        //     101: invokestatic 181	java/lang/Integer:valueOf	(Ljava/lang/String;)Ljava/lang/Integer;
        //     104: invokevirtual 184	java/lang/Integer:intValue	()I
        //     107: istore 12
        //     109: iload 12
        //     111: ifle +19 -> 130
        //     114: aload_0
        //     115: aload 5
        //     117: invokevirtual 187	com/android/server/WiredAccessoryObserver$UEventInfo:getDevPath	()Ljava/lang/String;
        //     120: aload 5
        //     122: invokevirtual 190	com/android/server/WiredAccessoryObserver$UEventInfo:getDevName	()Ljava/lang/String;
        //     125: iload 12
        //     127: invokespecial 194	com/android/server/WiredAccessoryObserver:updateState	(Ljava/lang/String;Ljava/lang/String;I)V
        //     130: iinc 4 1
        //     133: goto -105 -> 28
        //     136: astore 9
        //     138: getstatic 54	com/android/server/WiredAccessoryObserver:TAG	Ljava/lang/String;
        //     141: new 196	java/lang/StringBuilder
        //     144: dup
        //     145: invokespecial 197	java/lang/StringBuilder:<init>	()V
        //     148: aload 5
        //     150: invokevirtual 159	com/android/server/WiredAccessoryObserver$UEventInfo:getSwitchStatePath	()Ljava/lang/String;
        //     153: invokevirtual 201	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     156: ldc 203
        //     158: invokevirtual 201	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     161: invokevirtual 206	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     164: invokestatic 209	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     167: pop
        //     168: goto -38 -> 130
        //     171: astore_1
        //     172: aload_0
        //     173: monitorexit
        //     174: aload_1
        //     175: athrow
        //     176: astore 7
        //     178: getstatic 54	com/android/server/WiredAccessoryObserver:TAG	Ljava/lang/String;
        //     181: ldc 211
        //     183: aload 7
        //     185: invokestatic 215	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     188: pop
        //     189: goto -59 -> 130
        //     192: aload_0
        //     193: monitorexit
        //     194: return
        //
        // Exception table:
        //     from	to	target	type
        //     56	130	136	java/io/FileNotFoundException
        //     2	56	171	finally
        //     56	130	171	finally
        //     138	168	171	finally
        //     178	189	171	finally
        //     56	130	176	java/lang/Exception
    }

    private static List<UEventInfo> makeObservedUEventList()
    {
        ArrayList localArrayList = new ArrayList();
        UEventInfo localUEventInfo1 = new UEventInfo("h2w", 1, 2);
        if (localUEventInfo1.checkSwitchExists())
        {
            localArrayList.add(localUEventInfo1);
            UEventInfo localUEventInfo2 = new UEventInfo("usb_audio", 4, 8);
            if (!localUEventInfo2.checkSwitchExists())
                break label108;
            localArrayList.add(localUEventInfo2);
            label63: UEventInfo localUEventInfo3 = new UEventInfo("hdmi_audio", 16, 0);
            if (!localUEventInfo3.checkSwitchExists())
                break label120;
            localArrayList.add(localUEventInfo3);
        }
        while (true)
        {
            return localArrayList;
            Slog.w(TAG, "This kernel does not have wired headset support");
            break;
            label108: Slog.w(TAG, "This kernel does not have usb audio support");
            break label63;
            label120: UEventInfo localUEventInfo4 = new UEventInfo("hdmi", 16, 0);
            if (localUEventInfo4.checkSwitchExists())
                localArrayList.add(localUEventInfo4);
            else
                Slog.w(TAG, "This kernel does not have HDMI audio support");
        }
    }

    private final void setDeviceState(int paramInt1, int paramInt2, int paramInt3, String paramString)
    {
        int i;
        int j;
        label26: String str1;
        StringBuilder localStringBuilder;
        if ((paramInt2 & paramInt1) != (paramInt3 & paramInt1))
        {
            if ((paramInt2 & paramInt1) == 0)
                break label90;
            i = 1;
            if (paramInt1 != 1)
                break label96;
            j = 4;
            str1 = TAG;
            localStringBuilder = new StringBuilder().append("device ").append(paramString);
            if (i != 1)
                break label178;
        }
        label178: for (String str2 = " connected"; ; str2 = " disconnected")
        {
            Slog.v(str1, str2);
            this.mAudioManager.setWiredDeviceConnectionState(j, i, paramString);
            while (true)
            {
                return;
                label90: i = 0;
                break;
                label96: if (paramInt1 == 2)
                {
                    j = 8;
                    break label26;
                }
                if (paramInt1 == 4)
                {
                    j = 2048;
                    break label26;
                }
                if (paramInt1 == 8)
                {
                    j = 4096;
                    break label26;
                }
                if (paramInt1 == 16)
                {
                    j = 1024;
                    break label26;
                }
                Slog.e(TAG, "setDeviceState() invalid headset type: " + paramInt1);
            }
        }
    }

    /** @deprecated */
    private final void setDevicesState(int paramInt1, int paramInt2, String paramString)
    {
        int i = 31;
        int j = 1;
        while (true)
        {
            if ((i == 0) || ((j & i) != 0));
            try
            {
                setDeviceState(j, paramInt1, paramInt2, paramString);
                i &= (j ^ 0xFFFFFFFF);
                j <<= 1;
                continue;
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }
    }

    /** @deprecated */
    private final void update(String paramString, int paramInt)
    {
        int i = paramInt & 0x1F;
        try
        {
            (i | this.mHeadsetState);
            int j = i & 0x4;
            int k = i & 0x8;
            int m = i & 0x3;
            int n = 1;
            int i1 = 1;
            Slog.v(TAG, "newState = " + paramInt + ", headsetState = " + i + "," + "mHeadsetState = " + this.mHeadsetState);
            if ((this.mHeadsetState == i) || ((m & m - 1) != 0))
            {
                Log.e(TAG, "unsetting h2w flag");
                n = 0;
            }
            if ((j >> 2 == 1) && (k >> 3 == 1))
            {
                Log.e(TAG, "unsetting usb flag");
                i1 = 0;
            }
            if ((n == 0) && (i1 == 0))
                Log.e(TAG, "invalid transition, returning ...");
            while (true)
            {
                return;
                this.mHeadsetName = paramString;
                this.mPrevHeadsetState = this.mHeadsetState;
                this.mHeadsetState = i;
                this.mWakeLock.acquire();
                this.mHandler.sendMessage(this.mHandler.obtainMessage(0, this.mHeadsetState, this.mPrevHeadsetState, this.mHeadsetName));
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    private final void updateState(String paramString1, String paramString2, int paramInt)
    {
        int i = 0;
        try
        {
            if (i < uEventInfo.size())
            {
                UEventInfo localUEventInfo = (UEventInfo)uEventInfo.get(i);
                if (paramString1.equals(localUEventInfo.getDevPath()))
                    update(paramString2, localUEventInfo.computeNewHeadsetState(this.mHeadsetState, paramInt));
            }
            else
            {
                return;
            }
            i++;
        }
        finally
        {
        }
    }

    public void onUEvent(UEventObserver.UEvent paramUEvent)
    {
        Slog.v(TAG, "Headset UEVENT: " + paramUEvent.toString());
        try
        {
            updateState(paramUEvent.get("DEVPATH"), paramUEvent.get("SWITCH_NAME"), Integer.parseInt(paramUEvent.get("SWITCH_STATE")));
            return;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            while (true)
                Slog.e(TAG, "Could not parse switch state from event " + paramUEvent);
        }
    }

    private final class BootCompletedReceiver extends BroadcastReceiver
    {
        private BootCompletedReceiver()
        {
        }

        public void onReceive(Context paramContext, Intent paramIntent)
        {
            WiredAccessoryObserver.this.init();
            for (int i = 0; i < WiredAccessoryObserver.uEventInfo.size(); i++)
            {
                WiredAccessoryObserver.UEventInfo localUEventInfo = (WiredAccessoryObserver.UEventInfo)WiredAccessoryObserver.uEventInfo.get(i);
                WiredAccessoryObserver.this.startObserving("DEVPATH=" + localUEventInfo.getDevPath());
            }
        }
    }

    private static class UEventInfo
    {
        private final String mDevName;
        private final int mState1Bits;
        private final int mState2Bits;

        public UEventInfo(String paramString, int paramInt1, int paramInt2)
        {
            this.mDevName = paramString;
            this.mState1Bits = paramInt1;
            this.mState2Bits = paramInt2;
        }

        public boolean checkSwitchExists()
        {
            File localFile = new File(getSwitchStatePath());
            if ((localFile != null) && (localFile.exists()));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public int computeNewHeadsetState(int paramInt1, int paramInt2)
        {
            int i = 0xFFFFFFFF ^ (this.mState1Bits | this.mState2Bits);
            int j;
            if (paramInt2 == 1)
                j = this.mState1Bits;
            while (true)
            {
                return j | paramInt1 & i;
                if (paramInt2 == 2)
                    j = this.mState2Bits;
                else
                    j = 0;
            }
        }

        public String getDevName()
        {
            return this.mDevName;
        }

        public String getDevPath()
        {
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = this.mDevName;
            return String.format("/devices/virtual/switch/%s", arrayOfObject);
        }

        public String getSwitchStatePath()
        {
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = this.mDevName;
            return String.format("/sys/class/switch/%s/state", arrayOfObject);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.WiredAccessoryObserver
 * JD-Core Version:        0.6.2
 */