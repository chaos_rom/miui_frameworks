package com.android.server.usb;

import android.content.Context;
import android.content.res.Resources;
import android.hardware.usb.UsbDevice;
import android.os.ParcelFileDescriptor;
import java.util.HashMap;

public class UsbHostManager
{
    private static final boolean LOG;
    private static final String TAG = UsbHostManager.class.getSimpleName();
    private final Context mContext;
    private final HashMap<String, UsbDevice> mDevices = new HashMap();
    private final String[] mHostBlacklist;
    private final Object mLock = new Object();
    private final UsbSettingsManager mSettingsManager;

    public UsbHostManager(Context paramContext, UsbSettingsManager paramUsbSettingsManager)
    {
        this.mContext = paramContext;
        this.mSettingsManager = paramUsbSettingsManager;
        this.mHostBlacklist = paramContext.getResources().getStringArray(17235999);
    }

    private boolean isBlackListed(int paramInt1, int paramInt2, int paramInt3)
    {
        int i = 1;
        if (paramInt1 == 9);
        while (true)
        {
            return i;
            if ((paramInt1 != 3) || (paramInt2 != i))
                i = 0;
        }
    }

    private boolean isBlackListed(String paramString)
    {
        int i = this.mHostBlacklist.length;
        int j = 0;
        if (j < i)
            if (!paramString.startsWith(this.mHostBlacklist[j]));
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            j++;
            break;
        }
    }

    private native void monitorUsbHostBus();

    private native ParcelFileDescriptor nativeOpenDevice(String paramString);

    // ERROR //
    private void usbDeviceAdded(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int[] paramArrayOfInt1, int[] paramArrayOfInt2)
    {
        // Byte code:
        //     0: aload_0
        //     1: aload_1
        //     2: invokespecial 82	com/android/server/usb/UsbHostManager:isBlackListed	(Ljava/lang/String;)Z
        //     5: ifne +16 -> 21
        //     8: aload_0
        //     9: iload 4
        //     11: iload 5
        //     13: iload 6
        //     15: invokespecial 84	com/android/server/usb/UsbHostManager:isBlackListed	(III)Z
        //     18: ifeq +4 -> 22
        //     21: return
        //     22: aload_0
        //     23: getfield 42	com/android/server/usb/UsbHostManager:mLock	Ljava/lang/Object;
        //     26: astore 9
        //     28: aload 9
        //     30: monitorenter
        //     31: aload_0
        //     32: getfield 40	com/android/server/usb/UsbHostManager:mDevices	Ljava/util/HashMap;
        //     35: aload_1
        //     36: invokevirtual 88	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     39: ifnull +43 -> 82
        //     42: getstatic 31	com/android/server/usb/UsbHostManager:TAG	Ljava/lang/String;
        //     45: new 90	java/lang/StringBuilder
        //     48: dup
        //     49: invokespecial 91	java/lang/StringBuilder:<init>	()V
        //     52: ldc 93
        //     54: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     57: aload_1
        //     58: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     61: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     64: invokestatic 106	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     67: pop
        //     68: aload 9
        //     70: monitorexit
        //     71: goto -50 -> 21
        //     74: astore 10
        //     76: aload 9
        //     78: monitorexit
        //     79: aload 10
        //     81: athrow
        //     82: aload 7
        //     84: arraylength
        //     85: iconst_5
        //     86: idiv
        //     87: istore 11
        //     89: iload 11
        //     91: anewarray 108	android/hardware/usb/UsbInterface
        //     94: astore 12
        //     96: iconst_0
        //     97: istore 13
        //     99: iconst_0
        //     100: istore 14
        //     102: iconst_0
        //     103: istore 15
        //     105: iload 14
        //     107: iload 11
        //     109: if_icmpge +241 -> 350
        //     112: iload 15
        //     114: iconst_1
        //     115: iadd
        //     116: istore 18
        //     118: aload 7
        //     120: iload 15
        //     122: iaload
        //     123: istore 21
        //     125: iload 18
        //     127: iconst_1
        //     128: iadd
        //     129: istore 22
        //     131: aload 7
        //     133: iload 18
        //     135: iaload
        //     136: istore 24
        //     138: iload 22
        //     140: iconst_1
        //     141: iadd
        //     142: istore 25
        //     144: aload 7
        //     146: iload 22
        //     148: iaload
        //     149: istore 26
        //     151: iload 25
        //     153: iconst_1
        //     154: iadd
        //     155: istore 22
        //     157: aload 7
        //     159: iload 25
        //     161: iaload
        //     162: istore 27
        //     164: iload 22
        //     166: iconst_1
        //     167: iadd
        //     168: istore 28
        //     170: aload 7
        //     172: iload 22
        //     174: iaload
        //     175: istore 29
        //     177: iload 29
        //     179: anewarray 110	android/hardware/usb/UsbEndpoint
        //     182: astore 30
        //     184: iconst_0
        //     185: istore 31
        //     187: iload 13
        //     189: istore 32
        //     191: iload 31
        //     193: iload 29
        //     195: if_icmpge +77 -> 272
        //     198: iload 32
        //     200: iconst_1
        //     201: iadd
        //     202: istore 35
        //     204: aload 8
        //     206: iload 32
        //     208: iaload
        //     209: istore 36
        //     211: iload 35
        //     213: iconst_1
        //     214: iadd
        //     215: istore 32
        //     217: aload 8
        //     219: iload 35
        //     221: iaload
        //     222: istore 37
        //     224: iload 32
        //     226: iconst_1
        //     227: iadd
        //     228: istore 38
        //     230: aload 8
        //     232: iload 32
        //     234: iaload
        //     235: istore 39
        //     237: iload 38
        //     239: iconst_1
        //     240: iadd
        //     241: istore 32
        //     243: aload 30
        //     245: iload 31
        //     247: new 110	android/hardware/usb/UsbEndpoint
        //     250: dup
        //     251: iload 36
        //     253: iload 37
        //     255: iload 39
        //     257: aload 8
        //     259: iload 38
        //     261: iaload
        //     262: invokespecial 113	android/hardware/usb/UsbEndpoint:<init>	(IIII)V
        //     265: aastore
        //     266: iinc 31 1
        //     269: goto -78 -> 191
        //     272: aload_0
        //     273: iload 24
        //     275: iload 26
        //     277: iload 27
        //     279: invokespecial 84	com/android/server/usb/UsbHostManager:isBlackListed	(III)Z
        //     282: istore 34
        //     284: iload 34
        //     286: ifeq +9 -> 295
        //     289: aload 9
        //     291: monitorexit
        //     292: goto -271 -> 21
        //     295: aload 12
        //     297: iload 14
        //     299: new 108	android/hardware/usb/UsbInterface
        //     302: dup
        //     303: iload 21
        //     305: iload 24
        //     307: iload 26
        //     309: iload 27
        //     311: aload 30
        //     313: invokespecial 116	android/hardware/usb/UsbInterface:<init>	(IIII[Landroid/os/Parcelable;)V
        //     316: aastore
        //     317: iinc 14 1
        //     320: iload 32
        //     322: istore 13
        //     324: iload 28
        //     326: istore 15
        //     328: goto -223 -> 105
        //     331: astore 19
        //     333: getstatic 31	com/android/server/usb/UsbHostManager:TAG	Ljava/lang/String;
        //     336: ldc 118
        //     338: aload 19
        //     340: invokestatic 122	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     343: pop
        //     344: aload 9
        //     346: monitorexit
        //     347: goto -326 -> 21
        //     350: new 124	android/hardware/usb/UsbDevice
        //     353: dup
        //     354: aload_1
        //     355: iload_2
        //     356: iload_3
        //     357: iload 4
        //     359: iload 5
        //     361: iload 6
        //     363: aload 12
        //     365: invokespecial 127	android/hardware/usb/UsbDevice:<init>	(Ljava/lang/String;IIIII[Landroid/os/Parcelable;)V
        //     368: astore 16
        //     370: aload_0
        //     371: getfield 40	com/android/server/usb/UsbHostManager:mDevices	Ljava/util/HashMap;
        //     374: aload_1
        //     375: aload 16
        //     377: invokevirtual 131	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     380: pop
        //     381: aload_0
        //     382: getfield 46	com/android/server/usb/UsbHostManager:mSettingsManager	Lcom/android/server/usb/UsbSettingsManager;
        //     385: aload 16
        //     387: invokevirtual 137	com/android/server/usb/UsbSettingsManager:deviceAttached	(Landroid/hardware/usb/UsbDevice;)V
        //     390: aload 9
        //     392: monitorexit
        //     393: goto -372 -> 21
        //     396: astore 19
        //     398: iload 22
        //     400: pop
        //     401: goto -68 -> 333
        //     404: astore 19
        //     406: iload 32
        //     408: pop
        //     409: goto -76 -> 333
        //
        // Exception table:
        //     from	to	target	type
        //     31	79	74	finally
        //     82	96	74	finally
        //     118	125	74	finally
        //     131	138	74	finally
        //     144	151	74	finally
        //     157	164	74	finally
        //     170	211	74	finally
        //     217	224	74	finally
        //     230	237	74	finally
        //     243	284	74	finally
        //     289	292	74	finally
        //     295	317	74	finally
        //     333	393	74	finally
        //     118	125	331	java/lang/Exception
        //     144	151	331	java/lang/Exception
        //     170	211	331	java/lang/Exception
        //     230	237	331	java/lang/Exception
        //     131	138	396	java/lang/Exception
        //     157	164	396	java/lang/Exception
        //     217	224	404	java/lang/Exception
        //     243	284	404	java/lang/Exception
        //     295	317	404	java/lang/Exception
    }

    private void usbDeviceRemoved(String paramString)
    {
        synchronized (this.mLock)
        {
            UsbDevice localUsbDevice = (UsbDevice)this.mDevices.remove(paramString);
            if (localUsbDevice != null)
                this.mSettingsManager.deviceDetached(localUsbDevice);
            return;
        }
    }

    // ERROR //
    public void dump(java.io.FileDescriptor paramFileDescriptor, java.io.PrintWriter paramPrintWriter)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 42	com/android/server/usb/UsbHostManager:mLock	Ljava/lang/Object;
        //     4: astore_3
        //     5: aload_3
        //     6: monitorenter
        //     7: aload_2
        //     8: ldc 149
        //     10: invokevirtual 154	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     13: aload_0
        //     14: getfield 40	com/android/server/usb/UsbHostManager:mDevices	Ljava/util/HashMap;
        //     17: invokevirtual 158	java/util/HashMap:keySet	()Ljava/util/Set;
        //     20: invokeinterface 164 1 0
        //     25: astore 5
        //     27: aload 5
        //     29: invokeinterface 170 1 0
        //     34: ifeq +66 -> 100
        //     37: aload 5
        //     39: invokeinterface 174 1 0
        //     44: checkcast 71	java/lang/String
        //     47: astore 6
        //     49: aload_2
        //     50: new 90	java/lang/StringBuilder
        //     53: dup
        //     54: invokespecial 91	java/lang/StringBuilder:<init>	()V
        //     57: ldc 176
        //     59: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     62: aload 6
        //     64: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     67: ldc 178
        //     69: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     72: aload_0
        //     73: getfield 40	com/android/server/usb/UsbHostManager:mDevices	Ljava/util/HashMap;
        //     76: aload 6
        //     78: invokevirtual 88	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     81: invokevirtual 181	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     84: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     87: invokevirtual 154	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     90: goto -63 -> 27
        //     93: astore 4
        //     95: aload_3
        //     96: monitorexit
        //     97: aload 4
        //     99: athrow
        //     100: aload_3
        //     101: monitorexit
        //     102: return
        //
        // Exception table:
        //     from	to	target	type
        //     7	97	93	finally
        //     100	102	93	finally
    }

    // ERROR //
    public void getDeviceList(android.os.Bundle paramBundle)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 42	com/android/server/usb/UsbHostManager:mLock	Ljava/lang/Object;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 40	com/android/server/usb/UsbHostManager:mDevices	Ljava/util/HashMap;
        //     11: invokevirtual 158	java/util/HashMap:keySet	()Ljava/util/Set;
        //     14: invokeinterface 164 1 0
        //     19: astore 4
        //     21: aload 4
        //     23: invokeinterface 170 1 0
        //     28: ifeq +41 -> 69
        //     31: aload 4
        //     33: invokeinterface 174 1 0
        //     38: checkcast 71	java/lang/String
        //     41: astore 5
        //     43: aload_1
        //     44: aload 5
        //     46: aload_0
        //     47: getfield 40	com/android/server/usb/UsbHostManager:mDevices	Ljava/util/HashMap;
        //     50: aload 5
        //     52: invokevirtual 88	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     55: checkcast 185	android/os/Parcelable
        //     58: invokevirtual 191	android/os/Bundle:putParcelable	(Ljava/lang/String;Landroid/os/Parcelable;)V
        //     61: goto -40 -> 21
        //     64: astore_3
        //     65: aload_2
        //     66: monitorexit
        //     67: aload_3
        //     68: athrow
        //     69: aload_2
        //     70: monitorexit
        //     71: return
        //
        // Exception table:
        //     from	to	target	type
        //     7	67	64	finally
        //     69	71	64	finally
    }

    // ERROR //
    public ParcelFileDescriptor openDevice(String paramString)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 42	com/android/server/usb/UsbHostManager:mLock	Ljava/lang/Object;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_0
        //     8: aload_1
        //     9: invokespecial 82	com/android/server/usb/UsbHostManager:isBlackListed	(Ljava/lang/String;)Z
        //     12: ifeq +18 -> 30
        //     15: new 194	java/lang/SecurityException
        //     18: dup
        //     19: ldc 196
        //     21: invokespecial 198	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     24: athrow
        //     25: astore_3
        //     26: aload_2
        //     27: monitorexit
        //     28: aload_3
        //     29: athrow
        //     30: aload_0
        //     31: getfield 40	com/android/server/usb/UsbHostManager:mDevices	Ljava/util/HashMap;
        //     34: aload_1
        //     35: invokevirtual 88	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     38: checkcast 124	android/hardware/usb/UsbDevice
        //     41: astore 4
        //     43: aload 4
        //     45: ifnonnull +35 -> 80
        //     48: new 200	java/lang/IllegalArgumentException
        //     51: dup
        //     52: new 90	java/lang/StringBuilder
        //     55: dup
        //     56: invokespecial 91	java/lang/StringBuilder:<init>	()V
        //     59: ldc 202
        //     61: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     64: aload_1
        //     65: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     68: ldc 204
        //     70: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     73: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     76: invokespecial 205	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     79: athrow
        //     80: aload_0
        //     81: getfield 46	com/android/server/usb/UsbHostManager:mSettingsManager	Lcom/android/server/usb/UsbSettingsManager;
        //     84: aload 4
        //     86: invokevirtual 208	com/android/server/usb/UsbSettingsManager:checkPermission	(Landroid/hardware/usb/UsbDevice;)V
        //     89: aload_0
        //     90: aload_1
        //     91: invokespecial 210	com/android/server/usb/UsbHostManager:nativeOpenDevice	(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
        //     94: astore 5
        //     96: aload_2
        //     97: monitorexit
        //     98: aload 5
        //     100: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     7	28	25	finally
        //     30	98	25	finally
    }

    public void systemReady()
    {
        synchronized (this.mLock)
        {
            new Thread(null, new Runnable()
            {
                public void run()
                {
                    UsbHostManager.this.monitorUsbHostBus();
                }
            }
            , "UsbService host thread").start();
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.usb.UsbHostManager
 * JD-Core Version:        0.6.2
 */