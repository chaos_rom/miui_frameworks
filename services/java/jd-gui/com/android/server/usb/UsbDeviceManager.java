package com.android.server.usb;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.hardware.usb.UsbAccessory;
import android.os.FileUtils;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.UEventObserver;
import android.os.UEventObserver.UEvent;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.provider.Settings.Secure;
import android.util.Pair;
import android.util.Slog;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class UsbDeviceManager
{
    private static final String ACCESSORY_START_MATCH = "DEVPATH=/devices/virtual/misc/usb_accessory";
    private static final int AUDIO_MODE_NONE = 0;
    private static final int AUDIO_MODE_SOURCE = 1;
    private static final String AUDIO_SOURCE_PCM_PATH = "/sys/class/android_usb/android0/f_audio_source/pcm";
    private static final String BOOT_MODE_PROPERTY = "ro.bootmode";
    private static final boolean DEBUG = false;
    private static final String FUNCTIONS_PATH = "/sys/class/android_usb/android0/functions";
    private static final String MASS_STORAGE_FILE_PATH = "/sys/class/android_usb/android0/f_mass_storage/lun/file";
    private static final int MSG_BOOT_COMPLETED = 4;
    private static final int MSG_ENABLE_ADB = 1;
    private static final int MSG_SET_CURRENT_FUNCTIONS = 2;
    private static final int MSG_SYSTEM_READY = 3;
    private static final int MSG_UPDATE_STATE = 0;
    private static final String RNDIS_ETH_ADDR_PATH = "/sys/class/android_usb/android0/f_rndis/ethaddr";
    private static final String STATE_PATH = "/sys/class/android_usb/android0/state";
    private static final String TAG = UsbDeviceManager.class.getSimpleName();
    private static final int UPDATE_DELAY = 1000;
    private static final String USB_STATE_MATCH = "DEVPATH=/devices/virtual/android_usb/android0";
    private String[] mAccessoryStrings;
    private boolean mAdbEnabled;
    private boolean mAudioSourceEnabled;
    private boolean mBootCompleted;
    private final ContentResolver mContentResolver;
    private final Context mContext;
    private UsbHandler mHandler;
    private final boolean mHasUsbAccessory;
    private NotificationManager mNotificationManager;
    private Map<String, List<Pair<String, String>>> mOemModeMap;
    private final UsbSettingsManager mSettingsManager;
    private final UEventObserver mUEventObserver = new UEventObserver()
    {
        public void onUEvent(UEventObserver.UEvent paramAnonymousUEvent)
        {
            String str1 = paramAnonymousUEvent.get("USB_STATE");
            String str2 = paramAnonymousUEvent.get("ACCESSORY");
            if (str1 != null)
                UsbDeviceManager.this.mHandler.updateState(str1);
            while (true)
            {
                return;
                if ("START".equals(str2))
                    UsbDeviceManager.this.startAccessoryMode();
            }
        }
    };
    private boolean mUseUsbNotification;

    public UsbDeviceManager(Context paramContext, UsbSettingsManager paramUsbSettingsManager)
    {
        this.mContext = paramContext;
        this.mContentResolver = paramContext.getContentResolver();
        this.mSettingsManager = paramUsbSettingsManager;
        this.mHasUsbAccessory = this.mContext.getPackageManager().hasSystemFeature("android.hardware.usb.accessory");
        initRndisAddress();
        readOemUsbOverrideConfig();
        HandlerThread localHandlerThread = new HandlerThread("UsbDeviceManager", 10);
        localHandlerThread.start();
        this.mHandler = new UsbHandler(localHandlerThread.getLooper());
        if (nativeIsStartRequested())
            startAccessoryMode();
    }

    private static String addFunction(String paramString1, String paramString2)
    {
        if ("none".equals(paramString1));
        while (true)
        {
            return paramString2;
            if (!containsFunction(paramString1, paramString2))
            {
                if (paramString1.length() > 0)
                    paramString1 = paramString1 + ",";
                paramString1 = paramString1 + paramString2;
            }
            paramString2 = paramString1;
        }
    }

    private static boolean containsFunction(String paramString1, String paramString2)
    {
        boolean bool = false;
        int i = paramString1.indexOf(paramString2);
        if (i < 0);
        while (true)
        {
            return bool;
            if ((i <= 0) || (paramString1.charAt(i - 1) == ','))
            {
                int j = i + paramString2.length();
                if ((j >= paramString1.length()) || (paramString1.charAt(j) == ','))
                    bool = true;
            }
        }
    }

    private static void initRndisAddress()
    {
        int[] arrayOfInt = new int[6];
        arrayOfInt[0] = 2;
        String str1 = SystemProperties.get("ro.serialno", "1234567890ABCDEF");
        int i = str1.length();
        for (int j = 0; j < i; j++)
        {
            int k = 1 + j % 5;
            arrayOfInt[k] ^= str1.charAt(j);
        }
        Object[] arrayOfObject = new Object[6];
        arrayOfObject[0] = Integer.valueOf(arrayOfInt[0]);
        arrayOfObject[1] = Integer.valueOf(arrayOfInt[1]);
        arrayOfObject[2] = Integer.valueOf(arrayOfInt[2]);
        arrayOfObject[3] = Integer.valueOf(arrayOfInt[3]);
        arrayOfObject[4] = Integer.valueOf(arrayOfInt[4]);
        arrayOfObject[5] = Integer.valueOf(arrayOfInt[5]);
        String str2 = String.format("%02X:%02X:%02X:%02X:%02X:%02X", arrayOfObject);
        try
        {
            FileUtils.stringToFile("/sys/class/android_usb/android0/f_rndis/ethaddr", str2);
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
                Slog.e(TAG, "failed to write to /sys/class/android_usb/android0/f_rndis/ethaddr");
        }
    }

    private native String[] nativeGetAccessoryStrings();

    private native int nativeGetAudioMode();

    private native boolean nativeIsStartRequested();

    private native ParcelFileDescriptor nativeOpenAccessory();

    private boolean needsOemUsbOverride()
    {
        boolean bool = false;
        if (this.mOemModeMap == null);
        while (true)
        {
            return bool;
            String str = SystemProperties.get("ro.bootmode", "unknown");
            if (this.mOemModeMap.get(str) != null)
                bool = true;
        }
    }

    private String processOemUsbOverride(String paramString)
    {
        if ((paramString == null) || (this.mOemModeMap == null))
            break label48;
        while (true)
        {
            return paramString;
            String str = SystemProperties.get("ro.bootmode", "unknown");
            List localList = (List)this.mOemModeMap.get(str);
            if (localList != null)
            {
                Iterator localIterator = localList.iterator();
                label48: if (localIterator.hasNext())
                {
                    Pair localPair = (Pair)localIterator.next();
                    if (!((String)localPair.first).equals(paramString))
                        break;
                    Slog.d(TAG, "OEM USB override: " + (String)localPair.first + " ==> " + (String)localPair.second);
                    paramString = (String)localPair.second;
                }
            }
        }
    }

    private void readOemUsbOverrideConfig()
    {
        String[] arrayOfString1 = this.mContext.getResources().getStringArray(17236014);
        if (arrayOfString1 != null)
        {
            int i = arrayOfString1.length;
            for (int j = 0; j < i; j++)
            {
                String[] arrayOfString2 = arrayOfString1[j].split(":");
                if (arrayOfString2.length == 3)
                {
                    if (this.mOemModeMap == null)
                        this.mOemModeMap = new HashMap();
                    Object localObject = (List)this.mOemModeMap.get(arrayOfString2[0]);
                    if (localObject == null)
                    {
                        localObject = new LinkedList();
                        this.mOemModeMap.put(arrayOfString2[0], localObject);
                    }
                    ((List)localObject).add(new Pair(arrayOfString2[1], arrayOfString2[2]));
                }
            }
        }
    }

    private static String removeFunction(String paramString1, String paramString2)
    {
        String[] arrayOfString = paramString1.split(",");
        for (int i = 0; i < arrayOfString.length; i++)
            if (paramString2.equals(arrayOfString[i]))
                arrayOfString[i] = null;
        if ((arrayOfString.length == 1) && (arrayOfString[0] == null));
        StringBuilder localStringBuilder;
        for (String str1 = "none"; ; str1 = localStringBuilder.toString())
        {
            return str1;
            localStringBuilder = new StringBuilder();
            for (int j = 0; j < arrayOfString.length; j++)
            {
                String str2 = arrayOfString[j];
                if (str2 != null)
                {
                    if (localStringBuilder.length() > 0)
                        localStringBuilder.append(",");
                    localStringBuilder.append(str2);
                }
            }
        }
    }

    private void startAccessoryMode()
    {
        this.mAccessoryStrings = nativeGetAccessoryStrings();
        int i;
        int j;
        label45: String str;
        if (nativeGetAudioMode() == 1)
        {
            i = 1;
            if ((this.mAccessoryStrings == null) || (this.mAccessoryStrings[0] == null) || (this.mAccessoryStrings[1] == null))
                break label75;
            j = 1;
            str = null;
            if ((j == 0) || (i == 0))
                break label80;
            str = "accessory,audio_source";
        }
        while (true)
        {
            if (str != null)
                setCurrentFunctions(str, false);
            return;
            i = 0;
            break;
            label75: j = 0;
            break label45;
            label80: if (j != 0)
                str = "accessory";
            else if (i != 0)
                str = "audio_source";
        }
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter)
    {
        if (this.mHandler != null)
            this.mHandler.dump(paramFileDescriptor, paramPrintWriter);
    }

    public UsbAccessory getCurrentAccessory()
    {
        return this.mHandler.getCurrentAccessory();
    }

    public ParcelFileDescriptor openAccessory(UsbAccessory paramUsbAccessory)
    {
        UsbAccessory localUsbAccessory = this.mHandler.getCurrentAccessory();
        if (localUsbAccessory == null)
            throw new IllegalArgumentException("no accessory attached");
        if (!localUsbAccessory.equals(paramUsbAccessory))
            throw new IllegalArgumentException(paramUsbAccessory.toString() + " does not match current accessory " + localUsbAccessory);
        this.mSettingsManager.checkPermission(paramUsbAccessory);
        return nativeOpenAccessory();
    }

    public void setCurrentFunctions(String paramString, boolean paramBoolean)
    {
        this.mHandler.sendMessage(2, paramString, paramBoolean);
    }

    public void setMassStorageBackingFile(String paramString)
    {
        if (paramString == null)
            paramString = "";
        try
        {
            FileUtils.stringToFile("/sys/class/android_usb/android0/f_mass_storage/lun/file", paramString);
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
                Slog.e(TAG, "failed to write to /sys/class/android_usb/android0/f_mass_storage/lun/file");
        }
    }

    public void systemReady()
    {
        int i = 1;
        this.mNotificationManager = ((NotificationManager)this.mContext.getSystemService("notification"));
        boolean bool = false;
        StorageVolume[] arrayOfStorageVolume = ((StorageManager)this.mContext.getSystemService("storage")).getVolumeList();
        if (arrayOfStorageVolume.length > 0)
            bool = arrayOfStorageVolume[0].allowMassStorage();
        int j;
        ContentResolver localContentResolver;
        if (!bool)
        {
            j = i;
            this.mUseUsbNotification = j;
            localContentResolver = this.mContentResolver;
            if (!this.mAdbEnabled)
                break label102;
        }
        while (true)
        {
            Settings.Secure.putInt(localContentResolver, "adb_enabled", i);
            this.mHandler.sendEmptyMessage(3);
            return;
            j = 0;
            break;
            label102: i = 0;
        }
    }

    private final class UsbHandler extends Handler
    {
        private boolean mAdbNotificationShown;
        private final BroadcastReceiver mBootCompletedReceiver = new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                UsbDeviceManager.this.mHandler.sendEmptyMessage(4);
            }
        };
        private boolean mConfigured;
        private boolean mConnected;
        private UsbAccessory mCurrentAccessory;
        private String mCurrentFunctions;
        private String mDefaultFunctions;
        private int mUsbNotificationId;

        public UsbHandler(Looper arg2)
        {
            super();
            try
            {
                this.mDefaultFunctions = SystemProperties.get("persist.sys.usb.config", "adb");
                this.mDefaultFunctions = UsbDeviceManager.this.processOemUsbOverride(this.mDefaultFunctions);
                if (!SystemProperties.get("sys.usb.config", "none").equals(this.mDefaultFunctions))
                {
                    Slog.w(UsbDeviceManager.TAG, "resetting config to persistent property: " + this.mDefaultFunctions);
                    SystemProperties.set("sys.usb.config", this.mDefaultFunctions);
                }
                this.mCurrentFunctions = this.mDefaultFunctions;
                updateState(FileUtils.readTextFile(new File("/sys/class/android_usb/android0/state"), 0, null).trim());
                UsbDeviceManager.access$502(UsbDeviceManager.this, UsbDeviceManager.containsFunction(this.mCurrentFunctions, "adb"));
                String str = SystemProperties.get("persist.service.adb.enable", "");
                int i;
                if (str.length() > 0)
                {
                    i = str.charAt(0);
                    if (i != 49)
                        break label250;
                    setAdbEnabled(true);
                }
                while (true)
                {
                    SystemProperties.set("persist.service.adb.enable", "");
                    UsbDeviceManager.this.mContentResolver.registerContentObserver(Settings.Secure.getUriFor("adb_enabled"), false, new UsbDeviceManager.AdbSettingsObserver(UsbDeviceManager.this));
                    UsbDeviceManager.this.mUEventObserver.startObserving("DEVPATH=/devices/virtual/android_usb/android0");
                    UsbDeviceManager.this.mUEventObserver.startObserving("DEVPATH=/devices/virtual/misc/usb_accessory");
                    UsbDeviceManager.this.mContext.registerReceiver(this.mBootCompletedReceiver, new IntentFilter("android.intent.action.BOOT_COMPLETED"));
                    break;
                    label250: if (i == 48)
                        setAdbEnabled(false);
                }
            }
            catch (Exception localException)
            {
                Slog.e(UsbDeviceManager.TAG, "Error initializing UsbHandler", localException);
            }
        }

        private void setAdbEnabled(boolean paramBoolean)
        {
            if (paramBoolean != UsbDeviceManager.this.mAdbEnabled)
            {
                UsbDeviceManager.access$502(UsbDeviceManager.this, paramBoolean);
                setEnabledFunctions(this.mDefaultFunctions, true);
                updateAdbNotification();
            }
        }

        private void setEnabledFunctions(String paramString, boolean paramBoolean)
        {
            String str3;
            if ((paramString != null) && (paramBoolean) && (!UsbDeviceManager.this.needsOemUsbOverride()))
                if (UsbDeviceManager.this.mAdbEnabled)
                {
                    str3 = UsbDeviceManager.addFunction(paramString, "adb");
                    if (!this.mDefaultFunctions.equals(str3))
                    {
                        if (setUsbConfig("none"))
                            break label87;
                        Slog.e(UsbDeviceManager.TAG, "Failed to disable USB");
                        setUsbConfig(this.mCurrentFunctions);
                    }
                }
            while (true)
            {
                return;
                str3 = UsbDeviceManager.removeFunction(paramString, "adb");
                break;
                label87: SystemProperties.set("persist.sys.usb.config", str3);
                if (waitForState(str3))
                {
                    this.mCurrentFunctions = str3;
                    this.mDefaultFunctions = str3;
                }
                else
                {
                    Slog.e(UsbDeviceManager.TAG, "Failed to switch persistent USB config to " + str3);
                    SystemProperties.set("persist.sys.usb.config", this.mDefaultFunctions);
                    continue;
                    if (paramString == null)
                        paramString = this.mDefaultFunctions;
                    String str1 = UsbDeviceManager.this.processOemUsbOverride(paramString);
                    if (UsbDeviceManager.this.mAdbEnabled);
                    for (String str2 = UsbDeviceManager.addFunction(str1, "adb"); ; str2 = UsbDeviceManager.removeFunction(str1, "adb"))
                    {
                        if (this.mCurrentFunctions.equals(str2))
                            break label244;
                        if (setUsbConfig("none"))
                            break label246;
                        Slog.e(UsbDeviceManager.TAG, "Failed to disable USB");
                        setUsbConfig(this.mCurrentFunctions);
                        break;
                    }
                    label244: continue;
                    label246: if (setUsbConfig(str2))
                    {
                        this.mCurrentFunctions = str2;
                    }
                    else
                    {
                        Slog.e(UsbDeviceManager.TAG, "Failed to switch USB config to " + str2);
                        setUsbConfig(this.mCurrentFunctions);
                    }
                }
            }
        }

        private boolean setUsbConfig(String paramString)
        {
            SystemProperties.set("sys.usb.config", paramString);
            return waitForState(paramString);
        }

        private void updateAdbNotification()
        {
            if (UsbDeviceManager.this.mNotificationManager == null);
            while (true)
            {
                return;
                if ((UsbDeviceManager.this.mAdbEnabled) && (this.mConnected))
                {
                    if ((!"0".equals(SystemProperties.get("persist.adb.notify"))) && (!this.mAdbNotificationShown))
                    {
                        Resources localResources = UsbDeviceManager.this.mContext.getResources();
                        CharSequence localCharSequence1 = localResources.getText(17040452);
                        CharSequence localCharSequence2 = localResources.getText(17040453);
                        Notification localNotification = new Notification();
                        localNotification.icon = 17302808;
                        localNotification.when = 0L;
                        localNotification.flags = 2;
                        localNotification.tickerText = localCharSequence1;
                        localNotification.defaults = 0;
                        localNotification.sound = null;
                        localNotification.vibrate = null;
                        localNotification.priority = -1;
                        Intent localIntent = Intent.makeRestartActivityTask(new ComponentName("com.android.settings", "com.android.settings.DevelopmentSettings"));
                        PendingIntent localPendingIntent = PendingIntent.getActivity(UsbDeviceManager.this.mContext, 0, localIntent, 0);
                        localNotification.setLatestEventInfo(UsbDeviceManager.this.mContext, localCharSequence1, localCharSequence2, localPendingIntent);
                        this.mAdbNotificationShown = true;
                        UsbDeviceManager.this.mNotificationManager.notify(17040452, localNotification);
                    }
                }
                else if (this.mAdbNotificationShown)
                {
                    this.mAdbNotificationShown = false;
                    UsbDeviceManager.this.mNotificationManager.cancel(17040452);
                }
            }
        }

        private void updateAudioSourceFunction()
        {
            boolean bool = UsbDeviceManager.containsFunction(this.mCurrentFunctions, "audio_source");
            Intent localIntent;
            int i;
            if (bool != UsbDeviceManager.this.mAudioSourceEnabled)
            {
                localIntent = new Intent("android.intent.action.USB_AUDIO_ACCESSORY_PLUG");
                localIntent.addFlags(536870912);
                localIntent.addFlags(1073741824);
                if (!bool)
                    break label144;
                i = 1;
            }
            while (true)
            {
                localIntent.putExtra("state", i);
                if (bool);
                try
                {
                    Scanner localScanner = new Scanner(new File("/sys/class/android_usb/android0/f_audio_source/pcm"));
                    int j = localScanner.nextInt();
                    int k = localScanner.nextInt();
                    localIntent.putExtra("card", j);
                    localIntent.putExtra("device", k);
                    UsbDeviceManager.this.mContext.sendStickyBroadcast(localIntent);
                    UsbDeviceManager.access$1602(UsbDeviceManager.this, bool);
                    return;
                    label144: i = 0;
                }
                catch (FileNotFoundException localFileNotFoundException)
                {
                    while (true)
                        Slog.e(UsbDeviceManager.TAG, "could not open audio source PCM file", localFileNotFoundException);
                }
            }
        }

        private void updateCurrentAccessory()
        {
            if (!UsbDeviceManager.this.mHasUsbAccessory);
            while (true)
            {
                return;
                if (this.mConfigured)
                {
                    if (UsbDeviceManager.this.mAccessoryStrings != null)
                    {
                        this.mCurrentAccessory = new UsbAccessory(UsbDeviceManager.this.mAccessoryStrings);
                        Slog.d(UsbDeviceManager.TAG, "entering USB accessory mode: " + this.mCurrentAccessory);
                        if (UsbDeviceManager.this.mBootCompleted)
                            UsbDeviceManager.this.mSettingsManager.accessoryAttached(this.mCurrentAccessory);
                    }
                    else
                    {
                        Slog.e(UsbDeviceManager.TAG, "nativeGetAccessoryStrings failed");
                    }
                }
                else if (!this.mConnected)
                {
                    Slog.d(UsbDeviceManager.TAG, "exited USB accessory mode");
                    setEnabledFunctions(this.mDefaultFunctions, false);
                    if (this.mCurrentAccessory != null)
                    {
                        if (UsbDeviceManager.this.mBootCompleted)
                            UsbDeviceManager.this.mSettingsManager.accessoryDetached(this.mCurrentAccessory);
                        this.mCurrentAccessory = null;
                        UsbDeviceManager.access$1302(UsbDeviceManager.this, null);
                    }
                }
            }
        }

        private void updateUsbNotification()
        {
            if ((UsbDeviceManager.this.mNotificationManager == null) || (!UsbDeviceManager.this.mUseUsbNotification));
            label328: 
            while (true)
            {
                return;
                int i = 0;
                Resources localResources = UsbDeviceManager.this.mContext.getResources();
                if (this.mConnected)
                {
                    if (!UsbDeviceManager.containsFunction(this.mCurrentFunctions, "mtp"))
                        break label244;
                    i = 17040444;
                }
                while (true)
                {
                    if (i == this.mUsbNotificationId)
                        break label328;
                    if (this.mUsbNotificationId != 0)
                    {
                        UsbDeviceManager.this.mNotificationManager.cancel(this.mUsbNotificationId);
                        this.mUsbNotificationId = 0;
                    }
                    if (i == 0)
                        break;
                    CharSequence localCharSequence1 = localResources.getText(17040448);
                    CharSequence localCharSequence2 = localResources.getText(i);
                    Notification localNotification = new Notification();
                    localNotification.icon = 17302838;
                    localNotification.when = 0L;
                    localNotification.flags = 2;
                    localNotification.tickerText = localCharSequence2;
                    localNotification.defaults = 0;
                    localNotification.sound = null;
                    localNotification.vibrate = null;
                    localNotification.priority = -2;
                    Intent localIntent = Intent.makeRestartActivityTask(new ComponentName("com.android.settings", "com.android.settings.UsbSettings"));
                    PendingIntent localPendingIntent = PendingIntent.getActivity(UsbDeviceManager.this.mContext, 0, localIntent, 0);
                    localNotification.setLatestEventInfo(UsbDeviceManager.this.mContext, localCharSequence2, localCharSequence1, localPendingIntent);
                    UsbDeviceManager.this.mNotificationManager.notify(i, localNotification);
                    this.mUsbNotificationId = i;
                    break;
                    label244: if (UsbDeviceManager.containsFunction(this.mCurrentFunctions, "ptp"))
                        i = 17040445;
                    else if (UsbDeviceManager.containsFunction(this.mCurrentFunctions, "mass_storage"))
                        i = 17040446;
                    else if (UsbDeviceManager.containsFunction(this.mCurrentFunctions, "accessory"))
                        i = 17040447;
                    else if (!UsbDeviceManager.containsFunction(this.mCurrentFunctions, "rndis"))
                        Slog.e(UsbDeviceManager.TAG, "No known USB function in updateUsbNotification");
                }
            }
        }

        private void updateUsbState()
        {
            Intent localIntent = new Intent("android.hardware.usb.action.USB_STATE");
            localIntent.addFlags(536870912);
            localIntent.putExtra("connected", this.mConnected);
            localIntent.putExtra("configured", this.mConfigured);
            if (this.mCurrentFunctions != null)
            {
                String[] arrayOfString = this.mCurrentFunctions.split(",");
                for (int i = 0; i < arrayOfString.length; i++)
                    localIntent.putExtra(arrayOfString[i], true);
            }
            UsbDeviceManager.this.mContext.sendStickyBroadcast(localIntent);
        }

        private boolean waitForState(String paramString)
        {
            int i = 0;
            if (i < 20)
                if (!paramString.equals(SystemProperties.get("sys.usb.state")));
            for (boolean bool = true; ; bool = false)
            {
                return bool;
                SystemClock.sleep(50L);
                i++;
                break;
                Slog.e(UsbDeviceManager.TAG, "waitForState(" + paramString + ") FAILED");
            }
        }

        public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter)
        {
            paramPrintWriter.println("    USB Device State:");
            paramPrintWriter.println("        Current Functions: " + this.mCurrentFunctions);
            paramPrintWriter.println("        Default Functions: " + this.mDefaultFunctions);
            paramPrintWriter.println("        mConnected: " + this.mConnected);
            paramPrintWriter.println("        mConfigured: " + this.mConfigured);
            paramPrintWriter.println("        mCurrentAccessory: " + this.mCurrentAccessory);
            try
            {
                paramPrintWriter.println("        Kernel state: " + FileUtils.readTextFile(new File("/sys/class/android_usb/android0/state"), 0, null).trim());
                paramPrintWriter.println("        Kernel function list: " + FileUtils.readTextFile(new File("/sys/class/android_usb/android0/functions"), 0, null).trim());
                paramPrintWriter.println("        Mass storage backing file: " + FileUtils.readTextFile(new File("/sys/class/android_usb/android0/f_mass_storage/lun/file"), 0, null).trim());
                return;
            }
            catch (IOException localIOException)
            {
                while (true)
                    paramPrintWriter.println("IOException: " + localIOException);
            }
        }

        public UsbAccessory getCurrentAccessory()
        {
            return this.mCurrentAccessory;
        }

        public void handleMessage(Message paramMessage)
        {
            int i = 1;
            switch (paramMessage.what)
            {
            default:
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            }
            while (true)
            {
                return;
                if (paramMessage.arg1 == i)
                {
                    int n = i;
                    label52: this.mConnected = n;
                    if (paramMessage.arg2 != i)
                        break label139;
                }
                while (true)
                {
                    this.mConfigured = i;
                    updateUsbNotification();
                    updateAdbNotification();
                    if (UsbDeviceManager.containsFunction(this.mCurrentFunctions, "accessory"))
                        updateCurrentAccessory();
                    if (!this.mConnected)
                        setEnabledFunctions(this.mDefaultFunctions, false);
                    if (!UsbDeviceManager.this.mBootCompleted)
                        break;
                    updateUsbState();
                    updateAudioSourceFunction();
                    break;
                    int i1 = 0;
                    break label52;
                    label139: i = 0;
                }
                if (paramMessage.arg1 == i);
                int j;
                while (true)
                {
                    setAdbEnabled(i);
                    break;
                    j = 0;
                }
                String str = (String)paramMessage.obj;
                if (paramMessage.arg1 == j);
                int m;
                for (int k = j; ; m = 0)
                {
                    setEnabledFunctions(str, k);
                    break;
                }
                updateUsbNotification();
                updateAdbNotification();
                updateUsbState();
                updateAudioSourceFunction();
                continue;
                UsbDeviceManager.access$1402(UsbDeviceManager.this, j);
                if (this.mCurrentAccessory != null)
                    UsbDeviceManager.this.mSettingsManager.accessoryAttached(this.mCurrentAccessory);
            }
        }

        public void sendMessage(int paramInt, Object paramObject)
        {
            removeMessages(paramInt);
            Message localMessage = Message.obtain(this, paramInt);
            localMessage.obj = paramObject;
            sendMessage(localMessage);
        }

        public void sendMessage(int paramInt, Object paramObject, boolean paramBoolean)
        {
            removeMessages(paramInt);
            Message localMessage = Message.obtain(this, paramInt);
            localMessage.obj = paramObject;
            if (paramBoolean);
            for (int i = 1; ; i = 0)
            {
                localMessage.arg1 = i;
                sendMessage(localMessage);
                return;
            }
        }

        public void sendMessage(int paramInt, boolean paramBoolean)
        {
            removeMessages(paramInt);
            Message localMessage = Message.obtain(this, paramInt);
            if (paramBoolean);
            for (int i = 1; ; i = 0)
            {
                localMessage.arg1 = i;
                sendMessage(localMessage);
                return;
            }
        }

        public void updateState(String paramString)
        {
            int i;
            int j;
            Message localMessage;
            if ("DISCONNECTED".equals(paramString))
            {
                i = 0;
                j = 0;
                removeMessages(0);
                localMessage = Message.obtain(this, 0);
                localMessage.arg1 = i;
                localMessage.arg2 = j;
                if (i != 0)
                    break label125;
            }
            label125: for (long l = 1000L; ; l = 0L)
            {
                sendMessageDelayed(localMessage, l);
                while (true)
                {
                    return;
                    if ("CONNECTED".equals(paramString))
                    {
                        i = 1;
                        j = 0;
                        break;
                    }
                    if ("CONFIGURED".equals(paramString))
                    {
                        i = 1;
                        j = 1;
                        break;
                    }
                    Slog.e(UsbDeviceManager.TAG, "unknown state " + paramString);
                }
            }
        }
    }

    private class AdbSettingsObserver extends ContentObserver
    {
        public AdbSettingsObserver()
        {
            super();
        }

        public void onChange(boolean paramBoolean)
        {
            boolean bool = false;
            if (Settings.Secure.getInt(UsbDeviceManager.this.mContentResolver, "adb_enabled", 0) > 0)
                bool = true;
            UsbDeviceManager.this.mHandler.sendMessage(1, bool);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.usb.UsbDeviceManager
 * JD-Core Version:        0.6.2
 */