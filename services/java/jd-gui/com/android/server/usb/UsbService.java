package com.android.server.usb;

import android.app.PendingIntent;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.usb.IUsbManager.Stub;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbDevice;
import android.os.Binder;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import java.io.File;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class UsbService extends IUsbManager.Stub
{
    private final Context mContext;
    private UsbDeviceManager mDeviceManager;
    private UsbHostManager mHostManager;
    private final UsbSettingsManager mSettingsManager;

    public UsbService(Context paramContext)
    {
        this.mContext = paramContext;
        this.mSettingsManager = new UsbSettingsManager(paramContext);
        if (this.mContext.getPackageManager().hasSystemFeature("android.hardware.usb.host"))
            this.mHostManager = new UsbHostManager(paramContext, this.mSettingsManager);
        if (new File("/sys/class/android_usb").exists())
            this.mDeviceManager = new UsbDeviceManager(paramContext, this.mSettingsManager);
    }

    public void clearDefaults(String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.MANAGE_USB", null);
        this.mSettingsManager.clearDefaults(paramString);
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.DUMP") != 0)
            paramPrintWriter.println("Permission Denial: can't dump UsbManager from from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid());
        while (true)
        {
            return;
            paramPrintWriter.println("USB Manager State:");
            if (this.mDeviceManager != null)
                this.mDeviceManager.dump(paramFileDescriptor, paramPrintWriter);
            if (this.mHostManager != null)
                this.mHostManager.dump(paramFileDescriptor, paramPrintWriter);
            this.mSettingsManager.dump(paramFileDescriptor, paramPrintWriter);
        }
    }

    public UsbAccessory getCurrentAccessory()
    {
        if (this.mDeviceManager != null);
        for (UsbAccessory localUsbAccessory = this.mDeviceManager.getCurrentAccessory(); ; localUsbAccessory = null)
            return localUsbAccessory;
    }

    public void getDeviceList(Bundle paramBundle)
    {
        if (this.mHostManager != null)
            this.mHostManager.getDeviceList(paramBundle);
    }

    public void grantAccessoryPermission(UsbAccessory paramUsbAccessory, int paramInt)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.MANAGE_USB", null);
        this.mSettingsManager.grantAccessoryPermission(paramUsbAccessory, paramInt);
    }

    public void grantDevicePermission(UsbDevice paramUsbDevice, int paramInt)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.MANAGE_USB", null);
        this.mSettingsManager.grantDevicePermission(paramUsbDevice, paramInt);
    }

    public boolean hasAccessoryPermission(UsbAccessory paramUsbAccessory)
    {
        return this.mSettingsManager.hasPermission(paramUsbAccessory);
    }

    public boolean hasDefaults(String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.MANAGE_USB", null);
        return this.mSettingsManager.hasDefaults(paramString);
    }

    public boolean hasDevicePermission(UsbDevice paramUsbDevice)
    {
        return this.mSettingsManager.hasPermission(paramUsbDevice);
    }

    public ParcelFileDescriptor openAccessory(UsbAccessory paramUsbAccessory)
    {
        if (this.mDeviceManager != null);
        for (ParcelFileDescriptor localParcelFileDescriptor = this.mDeviceManager.openAccessory(paramUsbAccessory); ; localParcelFileDescriptor = null)
            return localParcelFileDescriptor;
    }

    public ParcelFileDescriptor openDevice(String paramString)
    {
        if (this.mHostManager != null);
        for (ParcelFileDescriptor localParcelFileDescriptor = this.mHostManager.openDevice(paramString); ; localParcelFileDescriptor = null)
            return localParcelFileDescriptor;
    }

    public void requestAccessoryPermission(UsbAccessory paramUsbAccessory, String paramString, PendingIntent paramPendingIntent)
    {
        this.mSettingsManager.requestPermission(paramUsbAccessory, paramString, paramPendingIntent);
    }

    public void requestDevicePermission(UsbDevice paramUsbDevice, String paramString, PendingIntent paramPendingIntent)
    {
        this.mSettingsManager.requestPermission(paramUsbDevice, paramString, paramPendingIntent);
    }

    public void setAccessoryPackage(UsbAccessory paramUsbAccessory, String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.MANAGE_USB", null);
        this.mSettingsManager.setAccessoryPackage(paramUsbAccessory, paramString);
    }

    public void setCurrentFunction(String paramString, boolean paramBoolean)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.MANAGE_USB", null);
        if (this.mDeviceManager != null)
        {
            this.mDeviceManager.setCurrentFunctions(paramString, paramBoolean);
            return;
        }
        throw new IllegalStateException("USB device mode not supported");
    }

    public void setDevicePackage(UsbDevice paramUsbDevice, String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.MANAGE_USB", null);
        this.mSettingsManager.setDevicePackage(paramUsbDevice, paramString);
    }

    public void setMassStorageBackingFile(String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.MANAGE_USB", null);
        if (this.mDeviceManager != null)
        {
            this.mDeviceManager.setMassStorageBackingFile(paramString);
            return;
        }
        throw new IllegalStateException("USB device mode not supported");
    }

    public void systemReady()
    {
        if (this.mDeviceManager != null)
            this.mDeviceManager.systemReady();
        if (this.mHostManager != null)
            this.mHostManager.systemReady();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.usb.UsbService
 * JD-Core Version:        0.6.2
 */