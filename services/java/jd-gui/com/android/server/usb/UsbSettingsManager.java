package com.android.server.usb;

import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.XmlResourceParser;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbInterface;
import android.os.Binder;
import android.os.FileUtils;
import android.os.Parcelable;
import android.util.Slog;
import android.util.SparseBooleanArray;
import com.android.internal.content.PackageMonitor;
import com.android.internal.util.FastXmlSerializer;
import com.android.internal.util.XmlUtils;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

class UsbSettingsManager
{
    private static final boolean DEBUG = false;
    private static final String TAG = "UsbSettingsManager";
    private static final File sSettingsFile = new File("/data/system/usb_device_manager.xml");
    private final HashMap<UsbAccessory, SparseBooleanArray> mAccessoryPermissionMap = new HashMap();
    private final HashMap<AccessoryFilter, String> mAccessoryPreferenceMap = new HashMap();
    private final Context mContext;
    private final HashMap<String, SparseBooleanArray> mDevicePermissionMap = new HashMap();
    private final HashMap<DeviceFilter, String> mDevicePreferenceMap = new HashMap();
    private final Object mLock = new Object();
    private final PackageManager mPackageManager;
    MyPackageMonitor mPackageMonitor = new MyPackageMonitor(null);

    public UsbSettingsManager(Context paramContext)
    {
        this.mContext = paramContext;
        this.mPackageManager = paramContext.getPackageManager();
        synchronized (this.mLock)
        {
            readSettingsLocked();
            this.mPackageMonitor.register(paramContext, null, true);
            return;
        }
    }

    private boolean clearCompatibleMatchesLocked(String paramString, AccessoryFilter paramAccessoryFilter)
    {
        boolean bool = false;
        Iterator localIterator = this.mAccessoryPreferenceMap.keySet().iterator();
        while (localIterator.hasNext())
        {
            AccessoryFilter localAccessoryFilter = (AccessoryFilter)localIterator.next();
            if (paramAccessoryFilter.matches(localAccessoryFilter))
            {
                this.mAccessoryPreferenceMap.remove(localAccessoryFilter);
                bool = true;
            }
        }
        return bool;
    }

    private boolean clearCompatibleMatchesLocked(String paramString, DeviceFilter paramDeviceFilter)
    {
        boolean bool = false;
        Iterator localIterator = this.mDevicePreferenceMap.keySet().iterator();
        while (localIterator.hasNext())
        {
            DeviceFilter localDeviceFilter = (DeviceFilter)localIterator.next();
            if (paramDeviceFilter.matches(localDeviceFilter))
            {
                this.mDevicePreferenceMap.remove(localDeviceFilter);
                bool = true;
            }
        }
        return bool;
    }

    private boolean clearPackageDefaultsLocked(String paramString)
    {
        boolean bool = false;
        while (true)
        {
            int j;
            int i;
            synchronized (this.mLock)
            {
                if (this.mDevicePreferenceMap.containsValue(paramString))
                {
                    Object[] arrayOfObject2 = this.mDevicePreferenceMap.keySet().toArray();
                    j = 0;
                    if (j < arrayOfObject2.length)
                    {
                        Object localObject4 = arrayOfObject2[j];
                        if (!paramString.equals(this.mDevicePreferenceMap.get(localObject4)))
                            break label168;
                        this.mDevicePreferenceMap.remove(localObject4);
                        bool = true;
                        break label168;
                    }
                }
                if (this.mAccessoryPreferenceMap.containsValue(paramString))
                {
                    Object[] arrayOfObject1 = this.mAccessoryPreferenceMap.keySet().toArray();
                    i = 0;
                    if (i < arrayOfObject1.length)
                    {
                        Object localObject3 = arrayOfObject1[i];
                        if (!paramString.equals(this.mAccessoryPreferenceMap.get(localObject3)))
                            break label174;
                        this.mAccessoryPreferenceMap.remove(localObject3);
                        bool = true;
                        break label174;
                    }
                }
                return bool;
            }
            label168: j++;
            continue;
            label174: i++;
        }
    }

    private final ArrayList<ResolveInfo> getAccessoryMatchesLocked(UsbAccessory paramUsbAccessory, Intent paramIntent)
    {
        ArrayList localArrayList = new ArrayList();
        List localList = this.mPackageManager.queryIntentActivities(paramIntent, 128);
        int i = localList.size();
        for (int j = 0; j < i; j++)
        {
            ResolveInfo localResolveInfo = (ResolveInfo)localList.get(j);
            if (packageMatchesLocked(localResolveInfo, paramIntent.getAction(), null, paramUsbAccessory))
                localArrayList.add(localResolveInfo);
        }
        return localArrayList;
    }

    private final ArrayList<ResolveInfo> getDeviceMatchesLocked(UsbDevice paramUsbDevice, Intent paramIntent)
    {
        ArrayList localArrayList = new ArrayList();
        List localList = this.mPackageManager.queryIntentActivities(paramIntent, 128);
        int i = localList.size();
        for (int j = 0; j < i; j++)
        {
            ResolveInfo localResolveInfo = (ResolveInfo)localList.get(j);
            if (packageMatchesLocked(localResolveInfo, paramIntent.getAction(), paramUsbDevice, null))
                localArrayList.add(localResolveInfo);
        }
        return localArrayList;
    }

    private void handlePackageUpdate(String paramString)
    {
        Object localObject1 = this.mLock;
        int i = 0;
        ActivityInfo[] arrayOfActivityInfo;
        try
        {
            PackageInfo localPackageInfo = this.mPackageManager.getPackageInfo(paramString, 129);
            arrayOfActivityInfo = localPackageInfo.activities;
            if (arrayOfActivityInfo != null);
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            Slog.e("UsbSettingsManager", "handlePackageUpdate could not find package " + paramString, localNameNotFoundException);
        }
        finally
        {
        }
        for (int j = 0; ; j++)
            if (j < arrayOfActivityInfo.length)
            {
                if (handlePackageUpdateLocked(paramString, arrayOfActivityInfo[j], "android.hardware.usb.action.USB_DEVICE_ATTACHED"))
                    i = 1;
                if (handlePackageUpdateLocked(paramString, arrayOfActivityInfo[j], "android.hardware.usb.action.USB_ACCESSORY_ATTACHED"))
                    i = 1;
            }
            else
            {
                if (i != 0)
                    writeSettingsLocked();
                return;
            }
    }

    private boolean handlePackageUpdateLocked(String paramString1, ActivityInfo paramActivityInfo, String paramString2)
    {
        Object localObject1 = null;
        boolean bool1 = false;
        while (true)
        {
            try
            {
                XmlResourceParser localXmlResourceParser = paramActivityInfo.loadXmlMetaData(this.mPackageManager, paramString2);
                localObject1 = localXmlResourceParser;
                if (localObject1 == null)
                {
                    bool2 = false;
                    return bool2;
                }
                XmlUtils.nextElement(localObject1);
                if (localObject1.getEventType() != 1)
                {
                    str = localObject1.getName();
                    if ("usb-device".equals(str))
                    {
                        if (clearCompatibleMatchesLocked(paramString1, DeviceFilter.read(localObject1)))
                            bool1 = true;
                        XmlUtils.nextElement(localObject1);
                        continue;
                    }
                }
            }
            catch (Exception localException)
            {
                String str;
                Slog.w("UsbSettingsManager", "Unable to load component info " + paramActivityInfo.toString(), localException);
                if (localObject1 != null)
                    localObject1.close();
                boolean bool2 = bool1;
                continue;
                if (!"usb-accessory".equals(str))
                    continue;
                boolean bool3 = clearCompatibleMatchesLocked(paramString1, AccessoryFilter.read(localObject1));
                if (!bool3)
                    continue;
                bool1 = true;
                continue;
            }
            finally
            {
                if (localObject1 != null)
                    localObject1.close();
            }
            if (localObject1 == null);
        }
    }

    private boolean packageMatchesLocked(ResolveInfo paramResolveInfo, String paramString, UsbDevice paramUsbDevice, UsbAccessory paramUsbAccessory)
    {
        boolean bool1 = false;
        ActivityInfo localActivityInfo = paramResolveInfo.activityInfo;
        XmlResourceParser localXmlResourceParser = null;
        while (true)
        {
            try
            {
                localXmlResourceParser = localActivityInfo.loadXmlMetaData(this.mPackageManager, paramString);
                if (localXmlResourceParser == null)
                {
                    Slog.w("UsbSettingsManager", "no meta-data for " + paramResolveInfo);
                    return bool1;
                }
                XmlUtils.nextElement(localXmlResourceParser);
                if (localXmlResourceParser.getEventType() != 1)
                {
                    String str = localXmlResourceParser.getName();
                    if ((paramUsbDevice != null) && ("usb-device".equals(str)))
                    {
                        boolean bool3 = DeviceFilter.read(localXmlResourceParser).matches(paramUsbDevice);
                        if (!bool3)
                            continue;
                        if (localXmlResourceParser != null)
                            localXmlResourceParser.close();
                        bool1 = true;
                        continue;
                    }
                    if ((paramUsbAccessory != null) && ("usb-accessory".equals(str)))
                    {
                        boolean bool2 = AccessoryFilter.read(localXmlResourceParser).matches(paramUsbAccessory);
                        if (bool2)
                        {
                            if (localXmlResourceParser != null)
                                localXmlResourceParser.close();
                            bool1 = true;
                            continue;
                        }
                    }
                    XmlUtils.nextElement(localXmlResourceParser);
                    continue;
                }
            }
            catch (Exception localException)
            {
                Slog.w("UsbSettingsManager", "Unable to load component info " + paramResolveInfo.toString(), localException);
                if (localXmlResourceParser == null)
                    continue;
                continue;
            }
            finally
            {
                if (localXmlResourceParser != null)
                    localXmlResourceParser.close();
            }
            if (localXmlResourceParser == null);
        }
    }

    private void readPreference(XmlPullParser paramXmlPullParser)
        throws XmlPullParserException, IOException
    {
        String str = null;
        int i = paramXmlPullParser.getAttributeCount();
        int j = 0;
        if (j < i)
        {
            if ("package".equals(paramXmlPullParser.getAttributeName(j)))
                str = paramXmlPullParser.getAttributeValue(j);
        }
        else
        {
            XmlUtils.nextElement(paramXmlPullParser);
            if (!"usb-device".equals(paramXmlPullParser.getName()))
                break label90;
            DeviceFilter localDeviceFilter = DeviceFilter.read(paramXmlPullParser);
            this.mDevicePreferenceMap.put(localDeviceFilter, str);
        }
        while (true)
        {
            XmlUtils.nextElement(paramXmlPullParser);
            return;
            j++;
            break;
            label90: if ("usb-accessory".equals(paramXmlPullParser.getName()))
            {
                AccessoryFilter localAccessoryFilter = AccessoryFilter.read(paramXmlPullParser);
                this.mAccessoryPreferenceMap.put(localAccessoryFilter, str);
            }
        }
    }

    // ERROR //
    private void readSettingsLocked()
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_1
        //     2: new 319	java/io/FileInputStream
        //     5: dup
        //     6: getstatic 53	com/android/server/usb/UsbSettingsManager:sSettingsFile	Ljava/io/File;
        //     9: invokespecial 322	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //     12: astore_2
        //     13: invokestatic 328	android/util/Xml:newPullParser	()Lorg/xmlpull/v1/XmlPullParser;
        //     16: astore 10
        //     18: aload 10
        //     20: aload_2
        //     21: aconst_null
        //     22: invokeinterface 332 3 0
        //     27: aload 10
        //     29: invokestatic 245	com/android/internal/util/XmlUtils:nextElement	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     32: aload 10
        //     34: invokeinterface 333 1 0
        //     39: iconst_1
        //     40: if_icmpeq +87 -> 127
        //     43: ldc_w 335
        //     46: aload 10
        //     48: invokeinterface 311 1 0
        //     53: invokevirtual 148	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     56: ifeq +25 -> 81
        //     59: aload_0
        //     60: aload 10
        //     62: invokespecial 337	com/android/server/usb/UsbSettingsManager:readPreference	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     65: goto -33 -> 32
        //     68: astore 9
        //     70: aload_2
        //     71: astore_1
        //     72: aload_1
        //     73: ifnull +7 -> 80
        //     76: aload_1
        //     77: invokevirtual 338	java/io/FileInputStream:close	()V
        //     80: return
        //     81: aload 10
        //     83: invokestatic 245	com/android/internal/util/XmlUtils:nextElement	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     86: goto -54 -> 32
        //     89: astore 5
        //     91: aload_2
        //     92: astore_1
        //     93: ldc 22
        //     95: ldc_w 340
        //     98: aload 5
        //     100: invokestatic 215	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     103: pop
        //     104: getstatic 53	com/android/server/usb/UsbSettingsManager:sSettingsFile	Ljava/io/File;
        //     107: invokevirtual 343	java/io/File:delete	()Z
        //     110: pop
        //     111: aload_1
        //     112: ifnull -32 -> 80
        //     115: aload_1
        //     116: invokevirtual 338	java/io/FileInputStream:close	()V
        //     119: goto -39 -> 80
        //     122: astore 8
        //     124: goto -44 -> 80
        //     127: aload_2
        //     128: ifnull +7 -> 135
        //     131: aload_2
        //     132: invokevirtual 338	java/io/FileInputStream:close	()V
        //     135: goto -55 -> 80
        //     138: astore_3
        //     139: aload_1
        //     140: ifnull +7 -> 147
        //     143: aload_1
        //     144: invokevirtual 338	java/io/FileInputStream:close	()V
        //     147: aload_3
        //     148: athrow
        //     149: astore 4
        //     151: goto -4 -> 147
        //     154: astore 11
        //     156: goto -21 -> 135
        //     159: astore_3
        //     160: aload_2
        //     161: astore_1
        //     162: goto -23 -> 139
        //     165: astore 5
        //     167: goto -74 -> 93
        //     170: astore 12
        //     172: goto -100 -> 72
        //
        // Exception table:
        //     from	to	target	type
        //     13	65	68	java/io/FileNotFoundException
        //     81	86	68	java/io/FileNotFoundException
        //     13	65	89	java/lang/Exception
        //     81	86	89	java/lang/Exception
        //     76	80	122	java/io/IOException
        //     115	119	122	java/io/IOException
        //     2	13	138	finally
        //     93	111	138	finally
        //     143	147	149	java/io/IOException
        //     131	135	154	java/io/IOException
        //     13	65	159	finally
        //     81	86	159	finally
        //     2	13	165	java/lang/Exception
        //     2	13	170	java/io/FileNotFoundException
    }

    private void requestPermissionDialog(Intent paramIntent, String paramString, PendingIntent paramPendingIntent)
    {
        int i = Binder.getCallingUid();
        try
        {
            if (this.mPackageManager.getApplicationInfo(paramString, 0).uid != i)
                throw new IllegalArgumentException("package " + paramString + " does not match caller's uid " + i);
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            throw new IllegalArgumentException("package " + paramString + " not found");
        }
        long l = Binder.clearCallingIdentity();
        paramIntent.setClassName("com.android.systemui", "com.android.systemui.usb.UsbPermissionActivity");
        paramIntent.addFlags(268435456);
        paramIntent.putExtra("android.intent.extra.INTENT", paramPendingIntent);
        paramIntent.putExtra("package", paramString);
        paramIntent.putExtra("uid", i);
        try
        {
            this.mContext.startActivity(paramIntent);
            return;
        }
        catch (ActivityNotFoundException localActivityNotFoundException)
        {
            while (true)
                Slog.e("UsbSettingsManager", "unable to start UsbPermissionActivity");
        }
        finally
        {
            Binder.restoreCallingIdentity(l);
        }
    }

    private void resolveActivity(Intent paramIntent, ArrayList<ResolveInfo> paramArrayList, String paramString, UsbDevice paramUsbDevice, UsbAccessory paramUsbAccessory)
    {
        int i = paramArrayList.size();
        if (i == 0)
        {
            Intent localIntent2;
            if (paramUsbAccessory != null)
            {
                String str = paramUsbAccessory.getUri();
                if ((str != null) && (str.length() > 0))
                {
                    localIntent2 = new Intent();
                    localIntent2.setClassName("com.android.systemui", "com.android.systemui.usb.UsbAccessoryUriActivity");
                    localIntent2.addFlags(268435456);
                    localIntent2.putExtra("accessory", paramUsbAccessory);
                    localIntent2.putExtra("uri", str);
                }
            }
            try
            {
                this.mContext.startActivity(localIntent2);
                return;
            }
            catch (ActivityNotFoundException localActivityNotFoundException3)
            {
                while (true)
                    Slog.e("UsbSettingsManager", "unable to start UsbAccessoryUriActivity");
            }
        }
        Object localObject = null;
        if ((i == 1) && (paramString == null))
        {
            ResolveInfo localResolveInfo2 = (ResolveInfo)paramArrayList.get(0);
            if ((localResolveInfo2.activityInfo != null) && (localResolveInfo2.activityInfo.applicationInfo != null) && ((0x1 & localResolveInfo2.activityInfo.applicationInfo.flags) != 0))
                localObject = localResolveInfo2;
        }
        int j;
        if ((localObject == null) && (paramString != null))
        {
            j = 0;
            label186: if (j < i)
            {
                ResolveInfo localResolveInfo1 = (ResolveInfo)paramArrayList.get(j);
                if ((localResolveInfo1.activityInfo == null) || (!paramString.equals(localResolveInfo1.activityInfo.packageName)))
                    break label313;
                localObject = localResolveInfo1;
            }
        }
        if (localObject != null)
        {
            if (paramUsbDevice != null)
                grantDevicePermission(paramUsbDevice, localObject.activityInfo.applicationInfo.uid);
            while (true)
            {
                try
                {
                    paramIntent.setComponent(new ComponentName(localObject.activityInfo.packageName, localObject.activityInfo.name));
                    this.mContext.startActivity(paramIntent);
                }
                catch (ActivityNotFoundException localActivityNotFoundException2)
                {
                    Slog.e("UsbSettingsManager", "startActivity failed", localActivityNotFoundException2);
                }
                break;
                label313: j++;
                break label186;
                if (paramUsbAccessory != null)
                    grantAccessoryPermission(paramUsbAccessory, localObject.activityInfo.applicationInfo.uid);
            }
        }
        Intent localIntent1 = new Intent();
        localIntent1.addFlags(268435456);
        if (i == 1)
        {
            localIntent1.setClassName("com.android.systemui", "com.android.systemui.usb.UsbConfirmActivity");
            localIntent1.putExtra("rinfo", (Parcelable)paramArrayList.get(0));
            if (paramUsbDevice != null)
                localIntent1.putExtra("device", paramUsbDevice);
        }
        while (true)
        {
            try
            {
                this.mContext.startActivity(localIntent1);
            }
            catch (ActivityNotFoundException localActivityNotFoundException1)
            {
                Slog.e("UsbSettingsManager", "unable to start activity " + localIntent1);
            }
            break;
            localIntent1.putExtra("accessory", paramUsbAccessory);
            continue;
            localIntent1.setClassName("com.android.systemui", "com.android.systemui.usb.UsbResolverActivity");
            localIntent1.putParcelableArrayListExtra("rlist", paramArrayList);
            localIntent1.putExtra("android.intent.extra.INTENT", paramIntent);
        }
    }

    private void writeSettingsLocked()
    {
        FileOutputStream localFileOutputStream;
        BufferedOutputStream localBufferedOutputStream;
        FastXmlSerializer localFastXmlSerializer;
        try
        {
            localFileOutputStream = new FileOutputStream(sSettingsFile);
            localBufferedOutputStream = new BufferedOutputStream(localFileOutputStream);
            localFastXmlSerializer = new FastXmlSerializer();
            localFastXmlSerializer.setOutput(localBufferedOutputStream, "utf-8");
            localFastXmlSerializer.startDocument(null, Boolean.valueOf(true));
            localFastXmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
            localFastXmlSerializer.startTag(null, "settings");
            Iterator localIterator1 = this.mDevicePreferenceMap.keySet().iterator();
            while (localIterator1.hasNext())
            {
                DeviceFilter localDeviceFilter = (DeviceFilter)localIterator1.next();
                localFastXmlSerializer.startTag(null, "preference");
                localFastXmlSerializer.attribute(null, "package", (String)this.mDevicePreferenceMap.get(localDeviceFilter));
                localDeviceFilter.write(localFastXmlSerializer);
                localFastXmlSerializer.endTag(null, "preference");
            }
        }
        catch (Exception localException)
        {
            Slog.e("UsbSettingsManager", "error writing settings file, deleting to start fresh", localException);
            sSettingsFile.delete();
        }
        while (true)
        {
            return;
            Iterator localIterator2 = this.mAccessoryPreferenceMap.keySet().iterator();
            while (localIterator2.hasNext())
            {
                AccessoryFilter localAccessoryFilter = (AccessoryFilter)localIterator2.next();
                localFastXmlSerializer.startTag(null, "preference");
                localFastXmlSerializer.attribute(null, "package", (String)this.mAccessoryPreferenceMap.get(localAccessoryFilter));
                localAccessoryFilter.write(localFastXmlSerializer);
                localFastXmlSerializer.endTag(null, "preference");
            }
            localFastXmlSerializer.endTag(null, "settings");
            localFastXmlSerializer.endDocument();
            localBufferedOutputStream.flush();
            FileUtils.sync(localFileOutputStream);
            localBufferedOutputStream.close();
        }
    }

    public void accessoryAttached(UsbAccessory paramUsbAccessory)
    {
        Intent localIntent = new Intent("android.hardware.usb.action.USB_ACCESSORY_ATTACHED");
        localIntent.putExtra("accessory", paramUsbAccessory);
        localIntent.addFlags(268435456);
        synchronized (this.mLock)
        {
            ArrayList localArrayList = getAccessoryMatchesLocked(paramUsbAccessory, localIntent);
            String str = (String)this.mAccessoryPreferenceMap.get(new AccessoryFilter(paramUsbAccessory));
            resolveActivity(localIntent, localArrayList, str, null, paramUsbAccessory);
            return;
        }
    }

    public void accessoryDetached(UsbAccessory paramUsbAccessory)
    {
        this.mAccessoryPermissionMap.remove(paramUsbAccessory);
        Intent localIntent = new Intent("android.hardware.usb.action.USB_ACCESSORY_DETACHED");
        localIntent.putExtra("accessory", paramUsbAccessory);
        this.mContext.sendBroadcast(localIntent);
    }

    public void checkPermission(UsbAccessory paramUsbAccessory)
    {
        if (!hasPermission(paramUsbAccessory))
            throw new SecurityException("User has not given permission to accessory " + paramUsbAccessory);
    }

    public void checkPermission(UsbDevice paramUsbDevice)
    {
        if (!hasPermission(paramUsbDevice))
            throw new SecurityException("User has not given permission to device " + paramUsbDevice);
    }

    public void clearDefaults(String paramString)
    {
        synchronized (this.mLock)
        {
            if (clearPackageDefaultsLocked(paramString))
                writeSettingsLocked();
            return;
        }
    }

    public void deviceAttached(UsbDevice paramUsbDevice)
    {
        Intent localIntent = new Intent("android.hardware.usb.action.USB_DEVICE_ATTACHED");
        localIntent.putExtra("device", paramUsbDevice);
        localIntent.addFlags(268435456);
        synchronized (this.mLock)
        {
            ArrayList localArrayList = getDeviceMatchesLocked(paramUsbDevice, localIntent);
            String str = (String)this.mDevicePreferenceMap.get(new DeviceFilter(paramUsbDevice));
            resolveActivity(localIntent, localArrayList, str, paramUsbDevice, null);
            return;
        }
    }

    public void deviceDetached(UsbDevice paramUsbDevice)
    {
        this.mDevicePermissionMap.remove(paramUsbDevice.getDeviceName());
        Intent localIntent = new Intent("android.hardware.usb.action.USB_DEVICE_DETACHED");
        localIntent.putExtra("device", paramUsbDevice);
        this.mContext.sendBroadcast(localIntent);
    }

    // ERROR //
    public void dump(java.io.FileDescriptor paramFileDescriptor, java.io.PrintWriter paramPrintWriter)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 69	com/android/server/usb/UsbSettingsManager:mLock	Ljava/lang/Object;
        //     4: astore_3
        //     5: aload_3
        //     6: monitorenter
        //     7: aload_2
        //     8: ldc_w 603
        //     11: invokevirtual 608	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     14: aload_0
        //     15: getfield 61	com/android/server/usb/UsbSettingsManager:mDevicePermissionMap	Ljava/util/HashMap;
        //     18: invokevirtual 102	java/util/HashMap:keySet	()Ljava/util/Set;
        //     21: invokeinterface 108 1 0
        //     26: astore 5
        //     28: aload 5
        //     30: invokeinterface 114 1 0
        //     35: ifeq +133 -> 168
        //     38: aload 5
        //     40: invokeinterface 118 1 0
        //     45: checkcast 145	java/lang/String
        //     48: astore 15
        //     50: aload_2
        //     51: new 199	java/lang/StringBuilder
        //     54: dup
        //     55: invokespecial 200	java/lang/StringBuilder:<init>	()V
        //     58: ldc_w 610
        //     61: invokevirtual 206	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     64: aload 15
        //     66: invokevirtual 206	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     69: ldc_w 612
        //     72: invokevirtual 206	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     75: invokevirtual 209	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     78: invokevirtual 615	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     81: aload_0
        //     82: getfield 61	com/android/server/usb/UsbSettingsManager:mDevicePermissionMap	Ljava/util/HashMap;
        //     85: aload 15
        //     87: invokevirtual 143	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     90: checkcast 617	android/util/SparseBooleanArray
        //     93: astore 16
        //     95: aload 16
        //     97: invokevirtual 618	android/util/SparseBooleanArray:size	()I
        //     100: istore 17
        //     102: iconst_0
        //     103: istore 18
        //     105: iload 18
        //     107: iload 17
        //     109: if_icmpge +42 -> 151
        //     112: aload_2
        //     113: new 199	java/lang/StringBuilder
        //     116: dup
        //     117: invokespecial 200	java/lang/StringBuilder:<init>	()V
        //     120: aload 16
        //     122: iload 18
        //     124: invokevirtual 622	android/util/SparseBooleanArray:keyAt	(I)I
        //     127: invokestatic 626	java/lang/Integer:toString	(I)Ljava/lang/String;
        //     130: invokevirtual 206	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     133: ldc_w 628
        //     136: invokevirtual 206	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     139: invokevirtual 209	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     142: invokevirtual 615	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     145: iinc 18 1
        //     148: goto -43 -> 105
        //     151: aload_2
        //     152: ldc_w 630
        //     155: invokevirtual 608	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     158: goto -130 -> 28
        //     161: astore 4
        //     163: aload_3
        //     164: monitorexit
        //     165: aload 4
        //     167: athrow
        //     168: aload_2
        //     169: ldc_w 632
        //     172: invokevirtual 608	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     175: aload_0
        //     176: getfield 63	com/android/server/usb/UsbSettingsManager:mAccessoryPermissionMap	Ljava/util/HashMap;
        //     179: invokevirtual 102	java/util/HashMap:keySet	()Ljava/util/Set;
        //     182: invokeinterface 108 1 0
        //     187: astore 6
        //     189: aload 6
        //     191: invokeinterface 114 1 0
        //     196: ifeq +126 -> 322
        //     199: aload 6
        //     201: invokeinterface 118 1 0
        //     206: checkcast 420	android/hardware/usb/UsbAccessory
        //     209: astore 11
        //     211: aload_2
        //     212: new 199	java/lang/StringBuilder
        //     215: dup
        //     216: invokespecial 200	java/lang/StringBuilder:<init>	()V
        //     219: ldc_w 610
        //     222: invokevirtual 206	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     225: aload 11
        //     227: invokevirtual 281	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     230: ldc_w 612
        //     233: invokevirtual 206	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     236: invokevirtual 209	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     239: invokevirtual 615	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     242: aload_0
        //     243: getfield 63	com/android/server/usb/UsbSettingsManager:mAccessoryPermissionMap	Ljava/util/HashMap;
        //     246: aload 11
        //     248: invokevirtual 143	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     251: checkcast 617	android/util/SparseBooleanArray
        //     254: astore 12
        //     256: aload 12
        //     258: invokevirtual 618	android/util/SparseBooleanArray:size	()I
        //     261: istore 13
        //     263: iconst_0
        //     264: istore 14
        //     266: iload 14
        //     268: iload 13
        //     270: if_icmpge +42 -> 312
        //     273: aload_2
        //     274: new 199	java/lang/StringBuilder
        //     277: dup
        //     278: invokespecial 200	java/lang/StringBuilder:<init>	()V
        //     281: aload 12
        //     283: iload 14
        //     285: invokevirtual 622	android/util/SparseBooleanArray:keyAt	(I)I
        //     288: invokestatic 626	java/lang/Integer:toString	(I)Ljava/lang/String;
        //     291: invokevirtual 206	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     294: ldc_w 628
        //     297: invokevirtual 206	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     300: invokevirtual 209	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     303: invokevirtual 615	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     306: iinc 14 1
        //     309: goto -43 -> 266
        //     312: aload_2
        //     313: ldc_w 630
        //     316: invokevirtual 608	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     319: goto -130 -> 189
        //     322: aload_2
        //     323: ldc_w 634
        //     326: invokevirtual 608	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     329: aload_0
        //     330: getfield 65	com/android/server/usb/UsbSettingsManager:mDevicePreferenceMap	Ljava/util/HashMap;
        //     333: invokevirtual 102	java/util/HashMap:keySet	()Ljava/util/Set;
        //     336: invokeinterface 108 1 0
        //     341: astore 7
        //     343: aload 7
        //     345: invokeinterface 114 1 0
        //     350: ifeq +64 -> 414
        //     353: aload 7
        //     355: invokeinterface 118 1 0
        //     360: checkcast 14	com/android/server/usb/UsbSettingsManager$DeviceFilter
        //     363: astore 10
        //     365: aload_2
        //     366: new 199	java/lang/StringBuilder
        //     369: dup
        //     370: invokespecial 200	java/lang/StringBuilder:<init>	()V
        //     373: ldc_w 610
        //     376: invokevirtual 206	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     379: aload 10
        //     381: invokevirtual 281	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     384: ldc_w 612
        //     387: invokevirtual 206	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     390: aload_0
        //     391: getfield 65	com/android/server/usb/UsbSettingsManager:mDevicePreferenceMap	Ljava/util/HashMap;
        //     394: aload 10
        //     396: invokevirtual 143	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     399: checkcast 145	java/lang/String
        //     402: invokevirtual 206	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     405: invokevirtual 209	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     408: invokevirtual 608	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     411: goto -68 -> 343
        //     414: aload_2
        //     415: ldc_w 636
        //     418: invokevirtual 608	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     421: aload_0
        //     422: getfield 67	com/android/server/usb/UsbSettingsManager:mAccessoryPreferenceMap	Ljava/util/HashMap;
        //     425: invokevirtual 102	java/util/HashMap:keySet	()Ljava/util/Set;
        //     428: invokeinterface 108 1 0
        //     433: astore 8
        //     435: aload 8
        //     437: invokeinterface 114 1 0
        //     442: ifeq +64 -> 506
        //     445: aload 8
        //     447: invokeinterface 118 1 0
        //     452: checkcast 11	com/android/server/usb/UsbSettingsManager$AccessoryFilter
        //     455: astore 9
        //     457: aload_2
        //     458: new 199	java/lang/StringBuilder
        //     461: dup
        //     462: invokespecial 200	java/lang/StringBuilder:<init>	()V
        //     465: ldc_w 610
        //     468: invokevirtual 206	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     471: aload 9
        //     473: invokevirtual 281	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     476: ldc_w 612
        //     479: invokevirtual 206	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     482: aload_0
        //     483: getfield 67	com/android/server/usb/UsbSettingsManager:mAccessoryPreferenceMap	Ljava/util/HashMap;
        //     486: aload 9
        //     488: invokevirtual 143	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     491: checkcast 145	java/lang/String
        //     494: invokevirtual 206	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     497: invokevirtual 209	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     500: invokevirtual 608	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     503: goto -68 -> 435
        //     506: aload_3
        //     507: monitorexit
        //     508: return
        //
        // Exception table:
        //     from	to	target	type
        //     7	165	161	finally
        //     168	508	161	finally
    }

    public void grantAccessoryPermission(UsbAccessory paramUsbAccessory, int paramInt)
    {
        synchronized (this.mLock)
        {
            SparseBooleanArray localSparseBooleanArray = (SparseBooleanArray)this.mAccessoryPermissionMap.get(paramUsbAccessory);
            if (localSparseBooleanArray == null)
            {
                localSparseBooleanArray = new SparseBooleanArray(1);
                this.mAccessoryPermissionMap.put(paramUsbAccessory, localSparseBooleanArray);
            }
            localSparseBooleanArray.put(paramInt, true);
            return;
        }
    }

    public void grantDevicePermission(UsbDevice paramUsbDevice, int paramInt)
    {
        synchronized (this.mLock)
        {
            String str = paramUsbDevice.getDeviceName();
            SparseBooleanArray localSparseBooleanArray = (SparseBooleanArray)this.mDevicePermissionMap.get(str);
            if (localSparseBooleanArray == null)
            {
                localSparseBooleanArray = new SparseBooleanArray(1);
                this.mDevicePermissionMap.put(str, localSparseBooleanArray);
            }
            localSparseBooleanArray.put(paramInt, true);
            return;
        }
    }

    // ERROR //
    public boolean hasDefaults(String paramString)
    {
        // Byte code:
        //     0: iconst_1
        //     1: istore_2
        //     2: aload_0
        //     3: getfield 69	com/android/server/usb/UsbSettingsManager:mLock	Ljava/lang/Object;
        //     6: astore_3
        //     7: aload_3
        //     8: monitorenter
        //     9: aload_0
        //     10: getfield 65	com/android/server/usb/UsbSettingsManager:mDevicePreferenceMap	Ljava/util/HashMap;
        //     13: invokevirtual 647	java/util/HashMap:values	()Ljava/util/Collection;
        //     16: aload_1
        //     17: invokeinterface 652 2 0
        //     22: ifeq +8 -> 30
        //     25: aload_3
        //     26: monitorexit
        //     27: goto +35 -> 62
        //     30: aload_0
        //     31: getfield 67	com/android/server/usb/UsbSettingsManager:mAccessoryPreferenceMap	Ljava/util/HashMap;
        //     34: invokevirtual 647	java/util/HashMap:values	()Ljava/util/Collection;
        //     37: aload_1
        //     38: invokeinterface 652 2 0
        //     43: ifeq +15 -> 58
        //     46: aload_3
        //     47: monitorexit
        //     48: goto +14 -> 62
        //     51: astore 4
        //     53: aload_3
        //     54: monitorexit
        //     55: aload 4
        //     57: athrow
        //     58: iconst_0
        //     59: istore_2
        //     60: aload_3
        //     61: monitorexit
        //     62: iload_2
        //     63: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     9	55	51	finally
        //     60	62	51	finally
    }

    public boolean hasPermission(UsbAccessory paramUsbAccessory)
    {
        boolean bool;
        synchronized (this.mLock)
        {
            SparseBooleanArray localSparseBooleanArray = (SparseBooleanArray)this.mAccessoryPermissionMap.get(paramUsbAccessory);
            if (localSparseBooleanArray == null)
                bool = false;
            else
                bool = localSparseBooleanArray.get(Binder.getCallingUid());
        }
        return bool;
    }

    public boolean hasPermission(UsbDevice paramUsbDevice)
    {
        boolean bool;
        synchronized (this.mLock)
        {
            SparseBooleanArray localSparseBooleanArray = (SparseBooleanArray)this.mDevicePermissionMap.get(paramUsbDevice.getDeviceName());
            if (localSparseBooleanArray == null)
                bool = false;
            else
                bool = localSparseBooleanArray.get(Binder.getCallingUid());
        }
        return bool;
    }

    public void requestPermission(UsbAccessory paramUsbAccessory, String paramString, PendingIntent paramPendingIntent)
    {
        Intent localIntent = new Intent();
        if (hasPermission(paramUsbAccessory))
        {
            localIntent.putExtra("accessory", paramUsbAccessory);
            localIntent.putExtra("permission", true);
        }
        try
        {
            paramPendingIntent.send(this.mContext, 0, localIntent);
            while (true)
            {
                label48: return;
                localIntent.putExtra("accessory", paramUsbAccessory);
                requestPermissionDialog(localIntent, paramString, paramPendingIntent);
            }
        }
        catch (PendingIntent.CanceledException localCanceledException)
        {
            break label48;
        }
    }

    public void requestPermission(UsbDevice paramUsbDevice, String paramString, PendingIntent paramPendingIntent)
    {
        Intent localIntent = new Intent();
        if (hasPermission(paramUsbDevice))
        {
            localIntent.putExtra("device", paramUsbDevice);
            localIntent.putExtra("permission", true);
        }
        try
        {
            paramPendingIntent.send(this.mContext, 0, localIntent);
            while (true)
            {
                label48: return;
                localIntent.putExtra("device", paramUsbDevice);
                requestPermissionDialog(localIntent, paramString, paramPendingIntent);
            }
        }
        catch (PendingIntent.CanceledException localCanceledException)
        {
            break label48;
        }
    }

    public void setAccessoryPackage(UsbAccessory paramUsbAccessory, String paramString)
    {
        AccessoryFilter localAccessoryFilter = new AccessoryFilter(paramUsbAccessory);
        Object localObject1 = this.mLock;
        if (paramString == null);
        while (true)
        {
            try
            {
                if (this.mAccessoryPreferenceMap.remove(localAccessoryFilter) == null)
                    break label99;
                i = 1;
                if (i != 0)
                    writeSettingsLocked();
                return;
                if (!paramString.equals(this.mAccessoryPreferenceMap.get(localAccessoryFilter)))
                {
                    i = 1;
                    if (i == 0)
                        continue;
                    this.mAccessoryPreferenceMap.put(localAccessoryFilter, paramString);
                }
            }
            finally
            {
            }
            continue;
            label99: int i = 0;
        }
    }

    public void setDevicePackage(UsbDevice paramUsbDevice, String paramString)
    {
        DeviceFilter localDeviceFilter = new DeviceFilter(paramUsbDevice);
        Object localObject1 = this.mLock;
        if (paramString == null);
        while (true)
        {
            try
            {
                if (this.mDevicePreferenceMap.remove(localDeviceFilter) == null)
                    break label99;
                i = 1;
                if (i != 0)
                    writeSettingsLocked();
                return;
                if (!paramString.equals(this.mDevicePreferenceMap.get(localDeviceFilter)))
                {
                    i = 1;
                    if (i == 0)
                        continue;
                    this.mDevicePreferenceMap.put(localDeviceFilter, paramString);
                }
            }
            finally
            {
            }
            continue;
            label99: int i = 0;
        }
    }

    private class MyPackageMonitor extends PackageMonitor
    {
        private MyPackageMonitor()
        {
        }

        public void onPackageAdded(String paramString, int paramInt)
        {
            UsbSettingsManager.this.handlePackageUpdate(paramString);
        }

        public void onPackageChanged(String paramString, int paramInt, String[] paramArrayOfString)
        {
            UsbSettingsManager.this.handlePackageUpdate(paramString);
        }

        public void onPackageRemoved(String paramString, int paramInt)
        {
            UsbSettingsManager.this.clearDefaults(paramString);
        }
    }

    private static class AccessoryFilter
    {
        public final String mManufacturer;
        public final String mModel;
        public final String mVersion;

        public AccessoryFilter(UsbAccessory paramUsbAccessory)
        {
            this.mManufacturer = paramUsbAccessory.getManufacturer();
            this.mModel = paramUsbAccessory.getModel();
            this.mVersion = paramUsbAccessory.getVersion();
        }

        public AccessoryFilter(String paramString1, String paramString2, String paramString3)
        {
            this.mManufacturer = paramString1;
            this.mModel = paramString2;
            this.mVersion = paramString3;
        }

        public static AccessoryFilter read(XmlPullParser paramXmlPullParser)
            throws XmlPullParserException, IOException
        {
            Object localObject1 = null;
            Object localObject2 = null;
            Object localObject3 = null;
            int i = paramXmlPullParser.getAttributeCount();
            int j = 0;
            if (j < i)
            {
                String str1 = paramXmlPullParser.getAttributeName(j);
                String str2 = paramXmlPullParser.getAttributeValue(j);
                if ("manufacturer".equals(str1))
                    localObject1 = str2;
                while (true)
                {
                    j++;
                    break;
                    if ("model".equals(str1))
                        localObject2 = str2;
                    else if ("version".equals(str1))
                        localObject3 = str2;
                }
            }
            return new AccessoryFilter(localObject1, localObject2, localObject3);
        }

        public boolean equals(Object paramObject)
        {
            boolean bool = true;
            if ((this.mManufacturer == null) || (this.mModel == null) || (this.mVersion == null))
                bool = false;
            while (true)
            {
                return bool;
                if ((paramObject instanceof AccessoryFilter))
                {
                    AccessoryFilter localAccessoryFilter = (AccessoryFilter)paramObject;
                    if ((!this.mManufacturer.equals(localAccessoryFilter.mManufacturer)) || (!this.mModel.equals(localAccessoryFilter.mModel)) || (!this.mVersion.equals(localAccessoryFilter.mVersion)))
                        bool = false;
                }
                else if ((paramObject instanceof UsbAccessory))
                {
                    UsbAccessory localUsbAccessory = (UsbAccessory)paramObject;
                    if ((!this.mManufacturer.equals(localUsbAccessory.getManufacturer())) || (!this.mModel.equals(localUsbAccessory.getModel())) || (!this.mVersion.equals(localUsbAccessory.getVersion())))
                        bool = false;
                }
                else
                {
                    bool = false;
                }
            }
        }

        public int hashCode()
        {
            int i = 0;
            int j;
            int k;
            label20: int m;
            if (this.mManufacturer == null)
            {
                j = 0;
                if (this.mModel != null)
                    break label48;
                k = 0;
                m = j ^ k;
                if (this.mVersion != null)
                    break label59;
            }
            while (true)
            {
                return m ^ i;
                j = this.mManufacturer.hashCode();
                break;
                label48: k = this.mModel.hashCode();
                break label20;
                label59: i = this.mVersion.hashCode();
            }
        }

        public boolean matches(UsbAccessory paramUsbAccessory)
        {
            boolean bool = false;
            if ((this.mManufacturer != null) && (!paramUsbAccessory.getManufacturer().equals(this.mManufacturer)));
            while (true)
            {
                return bool;
                if (((this.mModel == null) || (paramUsbAccessory.getModel().equals(this.mModel))) && ((this.mVersion == null) || (paramUsbAccessory.getVersion().equals(this.mVersion))))
                    bool = true;
            }
        }

        public boolean matches(AccessoryFilter paramAccessoryFilter)
        {
            boolean bool = false;
            if ((this.mManufacturer != null) && (!paramAccessoryFilter.mManufacturer.equals(this.mManufacturer)));
            while (true)
            {
                return bool;
                if (((this.mModel == null) || (paramAccessoryFilter.mModel.equals(this.mModel))) && ((this.mVersion == null) || (paramAccessoryFilter.mVersion.equals(this.mVersion))))
                    bool = true;
            }
        }

        public String toString()
        {
            return "AccessoryFilter[mManufacturer=\"" + this.mManufacturer + "\", mModel=\"" + this.mModel + "\", mVersion=\"" + this.mVersion + "\"]";
        }

        public void write(XmlSerializer paramXmlSerializer)
            throws IOException
        {
            paramXmlSerializer.startTag(null, "usb-accessory");
            if (this.mManufacturer != null)
                paramXmlSerializer.attribute(null, "manufacturer", this.mManufacturer);
            if (this.mModel != null)
                paramXmlSerializer.attribute(null, "model", this.mModel);
            if (this.mVersion != null)
                paramXmlSerializer.attribute(null, "version", this.mVersion);
            paramXmlSerializer.endTag(null, "usb-accessory");
        }
    }

    private static class DeviceFilter
    {
        public final int mClass;
        public final int mProductId;
        public final int mProtocol;
        public final int mSubclass;
        public final int mVendorId;

        public DeviceFilter(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
        {
            this.mVendorId = paramInt1;
            this.mProductId = paramInt2;
            this.mClass = paramInt3;
            this.mSubclass = paramInt4;
            this.mProtocol = paramInt5;
        }

        public DeviceFilter(UsbDevice paramUsbDevice)
        {
            this.mVendorId = paramUsbDevice.getVendorId();
            this.mProductId = paramUsbDevice.getProductId();
            this.mClass = paramUsbDevice.getDeviceClass();
            this.mSubclass = paramUsbDevice.getDeviceSubclass();
            this.mProtocol = paramUsbDevice.getDeviceProtocol();
        }

        private boolean matches(int paramInt1, int paramInt2, int paramInt3)
        {
            if (((this.mClass == -1) || (paramInt1 == this.mClass)) && ((this.mSubclass == -1) || (paramInt2 == this.mSubclass)) && ((this.mProtocol == -1) || (paramInt3 == this.mProtocol)));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public static DeviceFilter read(XmlPullParser paramXmlPullParser)
            throws XmlPullParserException, IOException
        {
            int i = -1;
            int j = -1;
            int k = -1;
            int m = -1;
            int n = -1;
            int i1 = paramXmlPullParser.getAttributeCount();
            int i2 = 0;
            if (i2 < i1)
            {
                String str = paramXmlPullParser.getAttributeName(i2);
                int i3 = Integer.parseInt(paramXmlPullParser.getAttributeValue(i2));
                if ("vendor-id".equals(str))
                    i = i3;
                while (true)
                {
                    i2++;
                    break;
                    if ("product-id".equals(str))
                        j = i3;
                    else if ("class".equals(str))
                        k = i3;
                    else if ("subclass".equals(str))
                        m = i3;
                    else if ("protocol".equals(str))
                        n = i3;
                }
            }
            return new DeviceFilter(i, j, k, m, n);
        }

        public boolean equals(Object paramObject)
        {
            boolean bool = true;
            if ((this.mVendorId == -1) || (this.mProductId == -1) || (this.mClass == -1) || (this.mSubclass == -1) || (this.mProtocol == -1))
                bool = false;
            while (true)
            {
                return bool;
                if ((paramObject instanceof DeviceFilter))
                {
                    DeviceFilter localDeviceFilter = (DeviceFilter)paramObject;
                    if ((localDeviceFilter.mVendorId != this.mVendorId) || (localDeviceFilter.mProductId != this.mProductId) || (localDeviceFilter.mClass != this.mClass) || (localDeviceFilter.mSubclass != this.mSubclass) || (localDeviceFilter.mProtocol != this.mProtocol))
                        bool = false;
                }
                else if ((paramObject instanceof UsbDevice))
                {
                    UsbDevice localUsbDevice = (UsbDevice)paramObject;
                    if ((localUsbDevice.getVendorId() != this.mVendorId) || (localUsbDevice.getProductId() != this.mProductId) || (localUsbDevice.getDeviceClass() != this.mClass) || (localUsbDevice.getDeviceSubclass() != this.mSubclass) || (localUsbDevice.getDeviceProtocol() != this.mProtocol))
                        bool = false;
                }
                else
                {
                    bool = false;
                }
            }
        }

        public int hashCode()
        {
            return (this.mVendorId << 16 | this.mProductId) ^ (this.mClass << 16 | this.mSubclass << 8 | this.mProtocol);
        }

        public boolean matches(UsbDevice paramUsbDevice)
        {
            boolean bool = false;
            if ((this.mVendorId != -1) && (paramUsbDevice.getVendorId() != this.mVendorId));
            label121: 
            while (true)
            {
                return bool;
                if ((this.mProductId == -1) || (paramUsbDevice.getProductId() == this.mProductId))
                    if (matches(paramUsbDevice.getDeviceClass(), paramUsbDevice.getDeviceSubclass(), paramUsbDevice.getDeviceProtocol()))
                    {
                        bool = true;
                    }
                    else
                    {
                        int i = paramUsbDevice.getInterfaceCount();
                        for (int j = 0; ; j++)
                        {
                            if (j >= i)
                                break label121;
                            UsbInterface localUsbInterface = paramUsbDevice.getInterface(j);
                            if (matches(localUsbInterface.getInterfaceClass(), localUsbInterface.getInterfaceSubclass(), localUsbInterface.getInterfaceProtocol()))
                            {
                                bool = true;
                                break;
                            }
                        }
                    }
            }
        }

        public boolean matches(DeviceFilter paramDeviceFilter)
        {
            boolean bool = false;
            if ((this.mVendorId != -1) && (paramDeviceFilter.mVendorId != this.mVendorId));
            while (true)
            {
                return bool;
                if ((this.mProductId == -1) || (paramDeviceFilter.mProductId == this.mProductId))
                    bool = matches(paramDeviceFilter.mClass, paramDeviceFilter.mSubclass, paramDeviceFilter.mProtocol);
            }
        }

        public String toString()
        {
            return "DeviceFilter[mVendorId=" + this.mVendorId + ",mProductId=" + this.mProductId + ",mClass=" + this.mClass + ",mSubclass=" + this.mSubclass + ",mProtocol=" + this.mProtocol + "]";
        }

        public void write(XmlSerializer paramXmlSerializer)
            throws IOException
        {
            paramXmlSerializer.startTag(null, "usb-device");
            if (this.mVendorId != -1)
                paramXmlSerializer.attribute(null, "vendor-id", Integer.toString(this.mVendorId));
            if (this.mProductId != -1)
                paramXmlSerializer.attribute(null, "product-id", Integer.toString(this.mProductId));
            if (this.mClass != -1)
                paramXmlSerializer.attribute(null, "class", Integer.toString(this.mClass));
            if (this.mSubclass != -1)
                paramXmlSerializer.attribute(null, "subclass", Integer.toString(this.mSubclass));
            if (this.mProtocol != -1)
                paramXmlSerializer.attribute(null, "protocol", Integer.toString(this.mProtocol));
            paramXmlSerializer.endTag(null, "usb-device");
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.usb.UsbSettingsManager
 * JD-Core Version:        0.6.2
 */