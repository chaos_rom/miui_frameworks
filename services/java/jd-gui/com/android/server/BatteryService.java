package com.android.server;

import android.app.ActivityManagerNative;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Binder;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.UEventObserver;
import android.os.UEventObserver.UEvent;
import android.provider.Settings.Secure;
import android.util.EventLog;
import android.util.Slog;
import com.android.internal.app.IBatteryStats;
import com.android.server.am.BatteryStatsService;
import java.io.File;
import java.io.FileDescriptor;
import java.io.PrintWriter;

class BatteryService extends Binder
{
    private static final int BATTERY_PLUGGED_NONE = 0;
    static final int BATTERY_SCALE = 100;
    private static final String BATTERY_STATS_SERVICE_NAME = "batteryinfo";
    private static final String[] DUMPSYS_ARGS = arrayOfString;
    private static final String DUMPSYS_DATA_PATH = "/data/system/";
    private static final int DUMP_MAX_LENGTH = 24576;
    private static final boolean LOCAL_LOGV;
    private static final String TAG = BatteryService.class.getSimpleName();
    private boolean mAcOnline;
    private int mBatteryHealth;
    private int mBatteryLevel;
    private boolean mBatteryLevelCritical;
    private boolean mBatteryPresent;
    private final IBatteryStats mBatteryStats;
    private int mBatteryStatus;
    private String mBatteryTechnology;
    private int mBatteryTemperature;
    private int mBatteryVoltage;
    private final Context mContext;
    private int mCriticalBatteryLevel;
    private int mDischargeStartLevel;
    private long mDischargeStartTime;
    private int mInvalidCharger;
    private UEventObserver mInvalidChargerObserver = new UEventObserver()
    {
        public void onUEvent(UEventObserver.UEvent paramAnonymousUEvent)
        {
            if ("1".equals(paramAnonymousUEvent.get("SWITCH_STATE")));
            for (int i = 1; ; i = 0)
            {
                if (BatteryService.this.mInvalidCharger != i)
                {
                    BatteryService.access$102(BatteryService.this, i);
                    BatteryService.this.update();
                }
                return;
            }
        }
    };
    private int mLastBatteryHealth;
    private int mLastBatteryLevel;
    private boolean mLastBatteryLevelCritical;
    private boolean mLastBatteryPresent;
    private int mLastBatteryStatus;
    private int mLastBatteryTemperature;
    private int mLastBatteryVoltage;
    private int mLastInvalidCharger;
    private int mLastPlugType = -1;
    private Led mLed;
    private int mLowBatteryCloseWarningLevel;
    private int mLowBatteryWarningLevel;
    private int mPlugType;
    private UEventObserver mPowerSupplyObserver = new UEventObserver()
    {
        public void onUEvent(UEventObserver.UEvent paramAnonymousUEvent)
        {
            BatteryService.this.update();
        }
    };
    private boolean mSentLowBatteryBroadcast = false;
    private boolean mUsbOnline;

    static
    {
        String[] arrayOfString = new String[2];
        arrayOfString[0] = "--checkin";
        arrayOfString[1] = "-u";
    }

    public BatteryService(Context paramContext, LightsService paramLightsService)
    {
        this.mContext = paramContext;
        this.mLed = new Led(paramContext, paramLightsService);
        this.mBatteryStats = BatteryStatsService.getService();
        this.mCriticalBatteryLevel = this.mContext.getResources().getInteger(17694743);
        this.mLowBatteryWarningLevel = this.mContext.getResources().getInteger(17694744);
        this.mLowBatteryCloseWarningLevel = this.mContext.getResources().getInteger(17694745);
        this.mPowerSupplyObserver.startObserving("SUBSYSTEM=power_supply");
        if (new File("/sys/devices/virtual/switch/invalid_charger/state").exists())
            this.mInvalidChargerObserver.startObserving("DEVPATH=/devices/virtual/switch/invalid_charger");
        update();
    }

    private final int getIcon(int paramInt)
    {
        int i = 17302823;
        if (this.mBatteryStatus == 2);
        while (true)
        {
            return i;
            if (this.mBatteryStatus == 3)
                i = 17302809;
            else if ((this.mBatteryStatus == 4) || (this.mBatteryStatus == 5))
            {
                if ((!isPowered()) || (this.mBatteryLevel < 100))
                    i = 17302809;
            }
            else
                i = 17302837;
        }
    }

    // ERROR //
    private final void logBatteryStats()
    {
        // Byte code:
        //     0: ldc 20
        //     2: invokestatic 193	android/os/ServiceManager:getService	(Ljava/lang/String;)Landroid/os/IBinder;
        //     5: astore_1
        //     6: aload_1
        //     7: ifnonnull +4 -> 11
        //     10: return
        //     11: aload_0
        //     12: getfield 103	com/android/server/BatteryService:mContext	Landroid/content/Context;
        //     15: ldc 195
        //     17: invokevirtual 199	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
        //     20: checkcast 201	android/os/DropBoxManager
        //     23: astore_2
        //     24: aload_2
        //     25: ifnull -15 -> 10
        //     28: aload_2
        //     29: ldc 203
        //     31: invokevirtual 207	android/os/DropBoxManager:isTagEnabled	(Ljava/lang/String;)Z
        //     34: ifeq -24 -> 10
        //     37: aconst_null
        //     38: astore_3
        //     39: aconst_null
        //     40: astore 4
        //     42: new 147	java/io/File
        //     45: dup
        //     46: ldc 209
        //     48: invokespecial 151	java/io/File:<init>	(Ljava/lang/String;)V
        //     51: astore 5
        //     53: new 211	java/io/FileOutputStream
        //     56: dup
        //     57: aload 5
        //     59: invokespecial 214	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
        //     62: astore 6
        //     64: aload_1
        //     65: aload 6
        //     67: invokevirtual 218	java/io/FileOutputStream:getFD	()Ljava/io/FileDescriptor;
        //     70: getstatic 85	com/android/server/BatteryService:DUMPSYS_ARGS	[Ljava/lang/String;
        //     73: invokeinterface 224 3 0
        //     78: aload 6
        //     80: invokestatic 230	android/os/FileUtils:sync	(Ljava/io/FileOutputStream;)Z
        //     83: pop
        //     84: aload_2
        //     85: ldc 203
        //     87: aload 5
        //     89: iconst_2
        //     90: invokevirtual 234	android/os/DropBoxManager:addFile	(Ljava/lang/String;Ljava/io/File;I)V
        //     93: aload 6
        //     95: ifnull +8 -> 103
        //     98: aload 6
        //     100: invokevirtual 237	java/io/FileOutputStream:close	()V
        //     103: aload 5
        //     105: ifnull +353 -> 458
        //     108: aload 5
        //     110: invokevirtual 240	java/io/File:delete	()Z
        //     113: ifne +345 -> 458
        //     116: getstatic 77	com/android/server/BatteryService:TAG	Ljava/lang/String;
        //     119: new 242	java/lang/StringBuilder
        //     122: dup
        //     123: invokespecial 243	java/lang/StringBuilder:<init>	()V
        //     126: ldc 245
        //     128: invokevirtual 249	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     131: aload 5
        //     133: invokevirtual 252	java/io/File:getAbsolutePath	()Ljava/lang/String;
        //     136: invokevirtual 249	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     139: invokevirtual 255	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     142: invokestatic 261	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     145: pop
        //     146: goto -136 -> 10
        //     149: astore 23
        //     151: getstatic 77	com/android/server/BatteryService:TAG	Ljava/lang/String;
        //     154: ldc_w 263
        //     157: invokestatic 261	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     160: pop
        //     161: goto -58 -> 103
        //     164: astore 7
        //     166: getstatic 77	com/android/server/BatteryService:TAG	Ljava/lang/String;
        //     169: ldc_w 265
        //     172: aload 7
        //     174: invokestatic 268	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     177: pop
        //     178: aload 4
        //     180: ifnull +8 -> 188
        //     183: aload 4
        //     185: invokevirtual 237	java/io/FileOutputStream:close	()V
        //     188: aload_3
        //     189: ifnull -179 -> 10
        //     192: aload_3
        //     193: invokevirtual 240	java/io/File:delete	()Z
        //     196: ifne -186 -> 10
        //     199: getstatic 77	com/android/server/BatteryService:TAG	Ljava/lang/String;
        //     202: new 242	java/lang/StringBuilder
        //     205: dup
        //     206: invokespecial 243	java/lang/StringBuilder:<init>	()V
        //     209: ldc 245
        //     211: invokevirtual 249	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     214: aload_3
        //     215: invokevirtual 252	java/io/File:getAbsolutePath	()Ljava/lang/String;
        //     218: invokevirtual 249	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     221: invokevirtual 255	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     224: invokestatic 261	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     227: pop
        //     228: goto -218 -> 10
        //     231: astore 14
        //     233: getstatic 77	com/android/server/BatteryService:TAG	Ljava/lang/String;
        //     236: ldc_w 263
        //     239: invokestatic 261	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     242: pop
        //     243: goto -55 -> 188
        //     246: astore 16
        //     248: getstatic 77	com/android/server/BatteryService:TAG	Ljava/lang/String;
        //     251: ldc_w 270
        //     254: aload 16
        //     256: invokestatic 268	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     259: pop
        //     260: aload 4
        //     262: ifnull +8 -> 270
        //     265: aload 4
        //     267: invokevirtual 237	java/io/FileOutputStream:close	()V
        //     270: aload_3
        //     271: ifnull -261 -> 10
        //     274: aload_3
        //     275: invokevirtual 240	java/io/File:delete	()Z
        //     278: ifne -268 -> 10
        //     281: getstatic 77	com/android/server/BatteryService:TAG	Ljava/lang/String;
        //     284: new 242	java/lang/StringBuilder
        //     287: dup
        //     288: invokespecial 243	java/lang/StringBuilder:<init>	()V
        //     291: ldc 245
        //     293: invokevirtual 249	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     296: aload_3
        //     297: invokevirtual 252	java/io/File:getAbsolutePath	()Ljava/lang/String;
        //     300: invokevirtual 249	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     303: invokevirtual 255	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     306: invokestatic 261	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     309: pop
        //     310: goto -300 -> 10
        //     313: astore 19
        //     315: getstatic 77	com/android/server/BatteryService:TAG	Ljava/lang/String;
        //     318: ldc_w 263
        //     321: invokestatic 261	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     324: pop
        //     325: goto -55 -> 270
        //     328: astore 8
        //     330: aload 4
        //     332: ifnull +8 -> 340
        //     335: aload 4
        //     337: invokevirtual 237	java/io/FileOutputStream:close	()V
        //     340: aload_3
        //     341: ifnull +39 -> 380
        //     344: aload_3
        //     345: invokevirtual 240	java/io/File:delete	()Z
        //     348: ifne +32 -> 380
        //     351: getstatic 77	com/android/server/BatteryService:TAG	Ljava/lang/String;
        //     354: new 242	java/lang/StringBuilder
        //     357: dup
        //     358: invokespecial 243	java/lang/StringBuilder:<init>	()V
        //     361: ldc 245
        //     363: invokevirtual 249	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     366: aload_3
        //     367: invokevirtual 252	java/io/File:getAbsolutePath	()Ljava/lang/String;
        //     370: invokevirtual 249	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     373: invokevirtual 255	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     376: invokestatic 261	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     379: pop
        //     380: aload 8
        //     382: athrow
        //     383: astore 10
        //     385: getstatic 77	com/android/server/BatteryService:TAG	Ljava/lang/String;
        //     388: ldc_w 263
        //     391: invokestatic 261	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     394: pop
        //     395: goto -55 -> 340
        //     398: astore 8
        //     400: aload 5
        //     402: astore_3
        //     403: goto -73 -> 330
        //     406: astore 8
        //     408: aload 6
        //     410: astore 4
        //     412: aload 5
        //     414: astore_3
        //     415: goto -85 -> 330
        //     418: astore 16
        //     420: aload 5
        //     422: astore_3
        //     423: goto -175 -> 248
        //     426: astore 16
        //     428: aload 6
        //     430: astore 4
        //     432: aload 5
        //     434: astore_3
        //     435: goto -187 -> 248
        //     438: astore 7
        //     440: aload 5
        //     442: astore_3
        //     443: goto -277 -> 166
        //     446: astore 7
        //     448: aload 6
        //     450: astore 4
        //     452: aload 5
        //     454: astore_3
        //     455: goto -289 -> 166
        //     458: goto -448 -> 10
        //
        // Exception table:
        //     from	to	target	type
        //     98	103	149	java/io/IOException
        //     42	53	164	android/os/RemoteException
        //     183	188	231	java/io/IOException
        //     42	53	246	java/io/IOException
        //     265	270	313	java/io/IOException
        //     42	53	328	finally
        //     166	178	328	finally
        //     248	260	328	finally
        //     335	340	383	java/io/IOException
        //     53	64	398	finally
        //     64	93	406	finally
        //     53	64	418	java/io/IOException
        //     64	93	426	java/io/IOException
        //     53	64	438	android/os/RemoteException
        //     64	93	446	android/os/RemoteException
    }

    private final void logOutlier(long paramLong)
    {
        ContentResolver localContentResolver = this.mContext.getContentResolver();
        String str1 = Settings.Secure.getString(localContentResolver, "battery_discharge_threshold");
        String str2 = Settings.Secure.getString(localContentResolver, "battery_discharge_duration_threshold");
        if ((str1 != null) && (str2 != null));
        try
        {
            long l = Long.parseLong(str2);
            int i = Integer.parseInt(str1);
            if ((paramLong <= l) && (this.mDischargeStartLevel - this.mBatteryLevel >= i))
                logBatteryStats();
            return;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            while (true)
                Slog.e(TAG, "Invalid DischargeThresholds GService string: " + str2 + " or " + str1);
        }
    }

    private native void native_update();

    private void processValues()
    {
        int i = 0;
        long l = 0L;
        boolean bool;
        if (this.mBatteryLevel <= this.mCriticalBatteryLevel)
            bool = true;
        while (true)
        {
            this.mBatteryLevelCritical = bool;
            if (this.mAcOnline)
                this.mPlugType = 1;
            try
            {
                label36: this.mBatteryStats.setBatteryState(this.mBatteryStatus, this.mBatteryHealth, this.mPlugType, this.mBatteryLevel, this.mBatteryTemperature, this.mBatteryVoltage);
                label69: shutdownIfNoPower();
                shutdownIfOverTemp();
                label264: int j;
                label346: int k;
                label506: int m;
                label516: int n;
                label559: Intent localIntent;
                if ((this.mBatteryStatus != this.mLastBatteryStatus) || (this.mBatteryHealth != this.mLastBatteryHealth) || (this.mBatteryPresent != this.mLastBatteryPresent) || (this.mBatteryLevel != this.mLastBatteryLevel) || (this.mPlugType != this.mLastPlugType) || (this.mBatteryVoltage != this.mLastBatteryVoltage) || (this.mBatteryTemperature != this.mLastBatteryTemperature) || (this.mInvalidCharger != this.mLastInvalidCharger))
                {
                    if (this.mPlugType != this.mLastPlugType)
                    {
                        if (this.mLastPlugType != 0)
                            break label765;
                        if ((this.mDischargeStartTime != 0L) && (this.mDischargeStartLevel != this.mBatteryLevel))
                        {
                            l = SystemClock.elapsedRealtime() - this.mDischargeStartTime;
                            i = 1;
                            Object[] arrayOfObject3 = new Object[3];
                            arrayOfObject3[0] = Long.valueOf(l);
                            arrayOfObject3[1] = Integer.valueOf(this.mDischargeStartLevel);
                            arrayOfObject3[2] = Integer.valueOf(this.mBatteryLevel);
                            EventLog.writeEvent(2730, arrayOfObject3);
                            this.mDischargeStartTime = 0L;
                        }
                    }
                    if ((this.mBatteryStatus != this.mLastBatteryStatus) || (this.mBatteryHealth != this.mLastBatteryHealth) || (this.mBatteryPresent != this.mLastBatteryPresent) || (this.mPlugType != this.mLastPlugType))
                    {
                        Object[] arrayOfObject1 = new Object[5];
                        arrayOfObject1[0] = Integer.valueOf(this.mBatteryStatus);
                        arrayOfObject1[1] = Integer.valueOf(this.mBatteryHealth);
                        if (!this.mBatteryPresent)
                            break label790;
                        j = 1;
                        arrayOfObject1[2] = Integer.valueOf(j);
                        arrayOfObject1[3] = Integer.valueOf(this.mPlugType);
                        arrayOfObject1[4] = this.mBatteryTechnology;
                        EventLog.writeEvent(2723, arrayOfObject1);
                    }
                    if ((this.mBatteryLevel != this.mLastBatteryLevel) || (this.mBatteryVoltage != this.mLastBatteryVoltage) || (this.mBatteryTemperature != this.mLastBatteryTemperature))
                    {
                        Object[] arrayOfObject2 = new Object[3];
                        arrayOfObject2[0] = Integer.valueOf(this.mBatteryLevel);
                        arrayOfObject2[1] = Integer.valueOf(this.mBatteryVoltage);
                        arrayOfObject2[2] = Integer.valueOf(this.mBatteryTemperature);
                        EventLog.writeEvent(2722, arrayOfObject2);
                    }
                    if ((this.mBatteryLevelCritical) && (!this.mLastBatteryLevelCritical) && (this.mPlugType == 0))
                    {
                        l = SystemClock.elapsedRealtime() - this.mDischargeStartTime;
                        i = 1;
                    }
                    if (this.mPlugType == 0)
                        break label796;
                    k = 1;
                    if (this.mLastPlugType == 0)
                        break label802;
                    m = 1;
                    if ((k != 0) || (this.mBatteryStatus == 1) || (this.mBatteryLevel > this.mLowBatteryWarningLevel) || ((m == 0) && (this.mLastBatteryLevel <= this.mLowBatteryWarningLevel)))
                        break label808;
                    n = 1;
                    sendIntent();
                    localIntent = new Intent();
                    localIntent.setFlags(134217728);
                    if ((this.mPlugType == 0) || (this.mLastPlugType != 0))
                        break label814;
                    localIntent.setAction("android.intent.action.ACTION_POWER_CONNECTED");
                    this.mContext.sendBroadcast(localIntent);
                    label613: if (n == 0)
                        break label849;
                    this.mSentLowBatteryBroadcast = true;
                    localIntent.setAction("android.intent.action.BATTERY_LOW");
                    this.mContext.sendBroadcast(localIntent);
                }
                while (true)
                {
                    this.mLed.updateLightsLocked();
                    if ((i != 0) && (l != 0L))
                        logOutlier(l);
                    this.mLastBatteryStatus = this.mBatteryStatus;
                    this.mLastBatteryHealth = this.mBatteryHealth;
                    this.mLastBatteryPresent = this.mBatteryPresent;
                    this.mLastBatteryLevel = this.mBatteryLevel;
                    this.mLastPlugType = this.mPlugType;
                    this.mLastBatteryVoltage = this.mBatteryVoltage;
                    this.mLastBatteryTemperature = this.mBatteryTemperature;
                    this.mLastBatteryLevelCritical = this.mBatteryLevelCritical;
                    this.mLastInvalidCharger = this.mInvalidCharger;
                    return;
                    bool = false;
                    break;
                    if (this.mUsbOnline)
                    {
                        this.mPlugType = 2;
                        break label36;
                    }
                    this.mPlugType = 0;
                    break label36;
                    label765: if (this.mPlugType != 0)
                        break label264;
                    this.mDischargeStartTime = SystemClock.elapsedRealtime();
                    this.mDischargeStartLevel = this.mBatteryLevel;
                    break label264;
                    label790: j = 0;
                    break label346;
                    label796: k = 0;
                    break label506;
                    label802: m = 0;
                    break label516;
                    label808: n = 0;
                    break label559;
                    label814: if ((this.mPlugType != 0) || (this.mLastPlugType == 0))
                        break label613;
                    localIntent.setAction("android.intent.action.ACTION_POWER_DISCONNECTED");
                    this.mContext.sendBroadcast(localIntent);
                    break label613;
                    label849: if ((this.mSentLowBatteryBroadcast) && (this.mLastBatteryLevel >= this.mLowBatteryCloseWarningLevel))
                    {
                        this.mSentLowBatteryBroadcast = false;
                        localIntent.setAction("android.intent.action.BATTERY_OKAY");
                        this.mContext.sendBroadcast(localIntent);
                    }
                }
            }
            catch (RemoteException localRemoteException)
            {
                break label69;
            }
        }
    }

    private final void sendIntent()
    {
        Intent localIntent = new Intent("android.intent.action.BATTERY_CHANGED");
        localIntent.addFlags(1610612736);
        int i = getIcon(this.mBatteryLevel);
        localIntent.putExtra("status", this.mBatteryStatus);
        localIntent.putExtra("health", this.mBatteryHealth);
        localIntent.putExtra("present", this.mBatteryPresent);
        localIntent.putExtra("level", this.mBatteryLevel);
        localIntent.putExtra("scale", 100);
        localIntent.putExtra("icon-small", i);
        localIntent.putExtra("plugged", this.mPlugType);
        localIntent.putExtra("voltage", this.mBatteryVoltage);
        localIntent.putExtra("temperature", this.mBatteryTemperature);
        localIntent.putExtra("technology", this.mBatteryTechnology);
        localIntent.putExtra("invalid_charger", this.mInvalidCharger);
        ActivityManagerNative.broadcastStickyIntent(localIntent, null);
    }

    private final void shutdownIfNoPower()
    {
        if ((this.mBatteryLevel == 0) && (!isPowered()) && (ActivityManagerNative.isSystemReady()))
        {
            Intent localIntent = new Intent("android.intent.action.ACTION_REQUEST_SHUTDOWN");
            localIntent.putExtra("android.intent.extra.KEY_CONFIRM", false);
            localIntent.setFlags(268435456);
            this.mContext.startActivity(localIntent);
        }
    }

    private final void shutdownIfOverTemp()
    {
        if ((this.mBatteryTemperature > 680) && (ActivityManagerNative.isSystemReady()))
        {
            Intent localIntent = new Intent("android.intent.action.ACTION_REQUEST_SHUTDOWN");
            localIntent.putExtra("android.intent.extra.KEY_CONFIRM", false);
            localIntent.setFlags(268435456);
            this.mContext.startActivity(localIntent);
        }
    }

    /** @deprecated */
    private final void update()
    {
        try
        {
            native_update();
            processValues();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.DUMP") != 0)
            paramPrintWriter.println("Permission Denial: can't dump Battery service from from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid());
        while (true)
        {
            return;
            if ((paramArrayOfString != null) && (paramArrayOfString.length != 0) && (!"-a".equals(paramArrayOfString[0])))
                continue;
            try
            {
                paramPrintWriter.println("Current Battery Service state:");
                paramPrintWriter.println("    AC powered: " + this.mAcOnline);
                paramPrintWriter.println("    USB powered: " + this.mUsbOnline);
                paramPrintWriter.println("    status: " + this.mBatteryStatus);
                paramPrintWriter.println("    health: " + this.mBatteryHealth);
                paramPrintWriter.println("    present: " + this.mBatteryPresent);
                paramPrintWriter.println("    level: " + this.mBatteryLevel);
                paramPrintWriter.println("    scale: 100");
                paramPrintWriter.println("    voltage:" + this.mBatteryVoltage);
                paramPrintWriter.println("    temperature: " + this.mBatteryTemperature);
                paramPrintWriter.println("    technology: " + this.mBatteryTechnology);
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }
    }

    final int getBatteryLevel()
    {
        return this.mBatteryLevel;
    }

    final int getPlugType()
    {
        return this.mPlugType;
    }

    final boolean isPowered()
    {
        int i = 1;
        if ((this.mAcOnline) || (this.mUsbOnline) || (this.mBatteryStatus == i));
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    final boolean isPowered(int paramInt)
    {
        int i = 1;
        if (this.mBatteryStatus == i);
        while (true)
        {
            return i;
            int j;
            if (paramInt == 0)
            {
                j = 0;
            }
            else
            {
                int k = 0;
                if (this.mAcOnline)
                    k = 0x0 | 0x1;
                if (this.mUsbOnline)
                    k |= 2;
                if ((paramInt & k) == 0)
                    j = 0;
            }
        }
    }

    void systemReady()
    {
        shutdownIfNoPower();
        shutdownIfOverTemp();
    }

    class Led
    {
        private boolean mBatteryCharging;
        private boolean mBatteryFull;
        private int mBatteryFullARGB;
        private int mBatteryLedOff;
        private int mBatteryLedOn;
        private LightsService.Light mBatteryLight;
        private boolean mBatteryLow;
        private int mBatteryLowARGB;
        private int mBatteryMediumARGB;
        private LightsService mLightsService;

        Led(Context paramLightsService, LightsService arg3)
        {
            Object localObject;
            this.mLightsService = localObject;
            this.mBatteryLight = localObject.getLight(3);
            this.mBatteryLowARGB = BatteryService.this.mContext.getResources().getInteger(17694748);
            this.mBatteryMediumARGB = BatteryService.this.mContext.getResources().getInteger(17694749);
            this.mBatteryFullARGB = BatteryService.this.mContext.getResources().getInteger(17694750);
            this.mBatteryLedOn = BatteryService.this.mContext.getResources().getInteger(17694751);
            this.mBatteryLedOff = BatteryService.this.mContext.getResources().getInteger(17694752);
        }

        void updateLightsLocked()
        {
            int i = BatteryService.this.mBatteryLevel;
            int j = BatteryService.this.mBatteryStatus;
            if (i < BatteryService.this.mLowBatteryWarningLevel)
                if (j == 2)
                    this.mBatteryLight.setColor(this.mBatteryLowARGB);
            while (true)
            {
                return;
                this.mBatteryLight.setFlashing(this.mBatteryLowARGB, 1, this.mBatteryLedOn, this.mBatteryLedOff);
                continue;
                if ((j == 2) || (j == 5))
                {
                    if ((j == 5) || (i >= 90))
                        this.mBatteryLight.setColor(this.mBatteryFullARGB);
                    else
                        this.mBatteryLight.setColor(this.mBatteryMediumARGB);
                }
                else
                    this.mBatteryLight.turnOff();
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.BatteryService
 * JD-Core Version:        0.6.2
 */