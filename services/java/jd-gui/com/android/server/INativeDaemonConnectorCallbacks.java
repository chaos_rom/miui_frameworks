package com.android.server;

abstract interface INativeDaemonConnectorCallbacks
{
    public abstract void onDaemonConnected();

    public abstract boolean onEvent(int paramInt, String paramString, String[] paramArrayOfString);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.INativeDaemonConnectorCallbacks
 * JD-Core Version:        0.6.2
 */