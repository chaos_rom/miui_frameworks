package com.android.server;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Debug;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.provider.Settings.Secure;
import android.util.EventLog;
import android.util.Log;
import android.util.Slog;
import com.android.server.am.ActivityManagerService;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

public class Watchdog extends Thread
{
    static final boolean DB = false;
    static final int MEMCHECK_DEFAULT_MIN_ALARM = 180;
    static final int MEMCHECK_DEFAULT_MIN_SCREEN_OFF = 300;
    static final int MEMCHECK_DEFAULT_RECHECK_INTERVAL = 300;
    static final int MONITOR = 2718;
    static final String[] NATIVE_STACKS_OF_INTEREST = arrayOfString;
    static final String REBOOT_ACTION = "com.android.service.Watchdog.REBOOT";
    static final int REBOOT_DEFAULT_INTERVAL = 0;
    static final int REBOOT_DEFAULT_START_TIME = 10800;
    static final int REBOOT_DEFAULT_WINDOW = 3600;
    static final boolean RECORD_KERNEL_THREADS = true;
    static final String TAG = "Watchdog";
    static final int TIME_TO_RESTART = 60000;
    static final int TIME_TO_WAIT = 30000;
    static final boolean localLOGV;
    static Watchdog sWatchdog;
    ActivityManagerService mActivity;
    AlarmManagerService mAlarm;
    BatteryService mBattery;
    long mBootTime;
    final Calendar mCalendar = Calendar.getInstance();
    PendingIntent mCheckupIntent;
    boolean mCompleted;
    Monitor mCurrentMonitor;
    boolean mForceKillSystem;
    final Handler mHandler = new HeartbeatHandler();
    int mMinAlarm = 180;
    int mMinScreenOff = 300;
    final ArrayList<Monitor> mMonitors = new ArrayList();
    boolean mNeedScheduledCheck;
    int mPhonePid;
    PowerManagerService mPower;
    PendingIntent mRebootIntent;
    int mRebootInterval;
    int mReqMinNextAlarm = -1;
    int mReqMinScreenOff = -1;
    int mReqRebootInterval = -1;
    boolean mReqRebootNoWait;
    int mReqRebootStartTime = -1;
    int mReqRebootWindow = -1;
    int mReqRecheckInterval = -1;
    ContentResolver mResolver;

    static
    {
        String[] arrayOfString = new String[3];
        arrayOfString[0] = "/system/bin/mediaserver";
        arrayOfString[1] = "/system/bin/sdcard";
        arrayOfString[2] = "/system/bin/surfaceflinger";
    }

    private Watchdog()
    {
        super("watchdog");
    }

    static long computeCalendarTime(Calendar paramCalendar, long paramLong1, long paramLong2)
    {
        paramCalendar.setTimeInMillis(paramLong1);
        int i = (int)paramLong2 / 3600;
        paramCalendar.set(11, i);
        long l1 = paramLong2 - i * 3600;
        int j = (int)l1 / 60;
        paramCalendar.set(12, j);
        paramCalendar.set(13, (int)l1 - j * 60);
        paramCalendar.set(14, 0);
        long l2 = paramCalendar.getTimeInMillis();
        if (l2 < paramLong1)
        {
            paramCalendar.add(5, 1);
            l2 = paramCalendar.getTimeInMillis();
        }
        return l2;
    }

    private File dumpKernelStackTraces()
    {
        File localFile = null;
        String str = SystemProperties.get("dalvik.vm.stack-trace-file", null);
        if ((str == null) || (str.length() == 0));
        while (true)
        {
            return localFile;
            native_dumpKernelStacks(str);
            localFile = new File(str);
        }
    }

    public static Watchdog getInstance()
    {
        if (sWatchdog == null)
            sWatchdog = new Watchdog();
        return sWatchdog;
    }

    private native void native_dumpKernelStacks(String paramString);

    // ERROR //
    public void addMonitor(Monitor paramMonitor)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: invokevirtual 190	com/android/server/Watchdog:isAlive	()Z
        //     6: ifeq +18 -> 24
        //     9: new 192	java/lang/RuntimeException
        //     12: dup
        //     13: ldc 194
        //     15: invokespecial 195	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
        //     18: athrow
        //     19: astore_2
        //     20: aload_0
        //     21: monitorexit
        //     22: aload_2
        //     23: athrow
        //     24: aload_0
        //     25: getfield 114	com/android/server/Watchdog:mMonitors	Ljava/util/ArrayList;
        //     28: aload_1
        //     29: invokevirtual 198	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     32: pop
        //     33: aload_0
        //     34: monitorexit
        //     35: return
        //
        // Exception table:
        //     from	to	target	type
        //     2	22	19	finally
        //     24	35	19	finally
    }

    // ERROR //
    void checkReboot(boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 128	com/android/server/Watchdog:mReqRebootInterval	I
        //     4: iflt +29 -> 33
        //     7: aload_0
        //     8: getfield 128	com/android/server/Watchdog:mReqRebootInterval	I
        //     11: istore_2
        //     12: aload_0
        //     13: iload_2
        //     14: putfield 202	com/android/server/Watchdog:mRebootInterval	I
        //     17: iload_2
        //     18: ifgt +29 -> 47
        //     21: aload_0
        //     22: getfield 204	com/android/server/Watchdog:mAlarm	Lcom/android/server/AlarmManagerService;
        //     25: aload_0
        //     26: getfield 206	com/android/server/Watchdog:mRebootIntent	Landroid/app/PendingIntent;
        //     29: invokevirtual 212	com/android/server/AlarmManagerService:remove	(Landroid/app/PendingIntent;)V
        //     32: return
        //     33: aload_0
        //     34: getfield 214	com/android/server/Watchdog:mResolver	Landroid/content/ContentResolver;
        //     37: ldc 216
        //     39: iconst_0
        //     40: invokestatic 222	android/provider/Settings$Secure:getInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)I
        //     43: istore_2
        //     44: goto -32 -> 12
        //     47: aload_0
        //     48: getfield 130	com/android/server/Watchdog:mReqRebootStartTime	I
        //     51: iflt +209 -> 260
        //     54: aload_0
        //     55: getfield 130	com/android/server/Watchdog:mReqRebootStartTime	I
        //     58: i2l
        //     59: lstore_3
        //     60: aload_0
        //     61: getfield 132	com/android/server/Watchdog:mReqRebootWindow	I
        //     64: iflt +213 -> 277
        //     67: aload_0
        //     68: getfield 132	com/android/server/Watchdog:mReqRebootWindow	I
        //     71: i2l
        //     72: lstore 5
        //     74: lload 5
        //     76: ldc2_w 223
        //     79: lmul
        //     80: lstore 7
        //     82: aload_0
        //     83: getfield 138	com/android/server/Watchdog:mReqRecheckInterval	I
        //     86: iflt +209 -> 295
        //     89: aload_0
        //     90: getfield 138	com/android/server/Watchdog:mReqRecheckInterval	I
        //     93: i2l
        //     94: lstore 9
        //     96: lload 9
        //     98: ldc2_w 223
        //     101: lmul
        //     102: lstore 11
        //     104: aload_0
        //     105: invokevirtual 227	com/android/server/Watchdog:retrieveBrutalityAmount	()V
        //     108: aload_0
        //     109: monitorenter
        //     110: invokestatic 232	java/lang/System:currentTimeMillis	()J
        //     113: lstore 14
        //     115: aload_0
        //     116: getfield 122	com/android/server/Watchdog:mCalendar	Ljava/util/Calendar;
        //     119: lload 14
        //     121: lload_3
        //     122: invokestatic 234	com/android/server/Watchdog:computeCalendarTime	(Ljava/util/Calendar;JJ)J
        //     125: lstore 16
        //     127: sipush 1000
        //     130: bipush 60
        //     132: bipush 60
        //     134: iload_2
        //     135: bipush 24
        //     137: imul
        //     138: imul
        //     139: imul
        //     140: imul
        //     141: i2l
        //     142: lstore 18
        //     144: aload_0
        //     145: getfield 236	com/android/server/Watchdog:mReqRebootNoWait	Z
        //     148: ifne +19 -> 167
        //     151: lload 14
        //     153: aload_0
        //     154: getfield 238	com/android/server/Watchdog:mBootTime	J
        //     157: lsub
        //     158: lload 18
        //     160: lload 7
        //     162: lsub
        //     163: lcmp
        //     164: iflt +169 -> 333
        //     167: iload_1
        //     168: ifeq +145 -> 313
        //     171: lload 7
        //     173: lconst_0
        //     174: lcmp
        //     175: ifgt +138 -> 313
        //     178: iconst_5
        //     179: anewarray 240	java/lang/Object
        //     182: astore 26
        //     184: aload 26
        //     186: iconst_0
        //     187: lload 14
        //     189: invokestatic 246	java/lang/Long:valueOf	(J)Ljava/lang/Long;
        //     192: aastore
        //     193: aload 26
        //     195: iconst_1
        //     196: lload 18
        //     198: l2i
        //     199: invokestatic 251	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     202: aastore
        //     203: aload 26
        //     205: iconst_2
        //     206: sipush 1000
        //     209: lload_3
        //     210: l2i
        //     211: imul
        //     212: invokestatic 251	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     215: aastore
        //     216: aload 26
        //     218: iconst_3
        //     219: lload 7
        //     221: l2i
        //     222: invokestatic 251	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     225: aastore
        //     226: aload 26
        //     228: iconst_4
        //     229: ldc 253
        //     231: aastore
        //     232: sipush 2808
        //     235: aload 26
        //     237: invokestatic 259	android/util/EventLog:writeEvent	(I[Ljava/lang/Object;)I
        //     240: pop
        //     241: aload_0
        //     242: ldc_w 261
        //     245: invokevirtual 264	com/android/server/Watchdog:rebootSystem	(Ljava/lang/String;)V
        //     248: aload_0
        //     249: monitorexit
        //     250: goto -218 -> 32
        //     253: astore 13
        //     255: aload_0
        //     256: monitorexit
        //     257: aload 13
        //     259: athrow
        //     260: aload_0
        //     261: getfield 214	com/android/server/Watchdog:mResolver	Landroid/content/ContentResolver;
        //     264: ldc_w 266
        //     267: ldc2_w 267
        //     270: invokestatic 272	android/provider/Settings$Secure:getLong	(Landroid/content/ContentResolver;Ljava/lang/String;J)J
        //     273: lstore_3
        //     274: goto -214 -> 60
        //     277: aload_0
        //     278: getfield 214	com/android/server/Watchdog:mResolver	Landroid/content/ContentResolver;
        //     281: ldc_w 274
        //     284: ldc2_w 275
        //     287: invokestatic 272	android/provider/Settings$Secure:getLong	(Landroid/content/ContentResolver;Ljava/lang/String;J)J
        //     290: lstore 5
        //     292: goto -218 -> 74
        //     295: aload_0
        //     296: getfield 214	com/android/server/Watchdog:mResolver	Landroid/content/ContentResolver;
        //     299: ldc_w 278
        //     302: ldc2_w 279
        //     305: invokestatic 272	android/provider/Settings$Secure:getLong	(Landroid/content/ContentResolver;Ljava/lang/String;J)J
        //     308: lstore 9
        //     310: goto -214 -> 96
        //     313: lload 14
        //     315: lload 16
        //     317: lcmp
        //     318: ifge +45 -> 363
        //     321: aload_0
        //     322: getfield 122	com/android/server/Watchdog:mCalendar	Ljava/util/Calendar;
        //     325: lload 14
        //     327: lload_3
        //     328: invokestatic 234	com/android/server/Watchdog:computeCalendarTime	(Ljava/util/Calendar;JJ)J
        //     331: lstore 16
        //     333: aload_0
        //     334: monitorexit
        //     335: aload_0
        //     336: getfield 204	com/android/server/Watchdog:mAlarm	Lcom/android/server/AlarmManagerService;
        //     339: aload_0
        //     340: getfield 206	com/android/server/Watchdog:mRebootIntent	Landroid/app/PendingIntent;
        //     343: invokevirtual 212	com/android/server/AlarmManagerService:remove	(Landroid/app/PendingIntent;)V
        //     346: aload_0
        //     347: getfield 204	com/android/server/Watchdog:mAlarm	Lcom/android/server/AlarmManagerService;
        //     350: iconst_0
        //     351: lload 16
        //     353: aload_0
        //     354: getfield 206	com/android/server/Watchdog:mRebootIntent	Landroid/app/PendingIntent;
        //     357: invokevirtual 283	com/android/server/AlarmManagerService:set	(IJLandroid/app/PendingIntent;)V
        //     360: goto -328 -> 32
        //     363: lload 14
        //     365: lload 16
        //     367: lload 7
        //     369: ladd
        //     370: lcmp
        //     371: ifge +130 -> 501
        //     374: aload_0
        //     375: lload 14
        //     377: invokevirtual 287	com/android/server/Watchdog:shouldWeBeBrutalLocked	(J)Ljava/lang/String;
        //     380: astore 22
        //     382: iconst_5
        //     383: anewarray 240	java/lang/Object
        //     386: astore 23
        //     388: aload 23
        //     390: iconst_0
        //     391: lload 14
        //     393: invokestatic 246	java/lang/Long:valueOf	(J)Ljava/lang/Long;
        //     396: aastore
        //     397: aload 23
        //     399: iconst_1
        //     400: iload_2
        //     401: invokestatic 251	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     404: aastore
        //     405: aload 23
        //     407: iconst_2
        //     408: sipush 1000
        //     411: lload_3
        //     412: l2i
        //     413: imul
        //     414: invokestatic 251	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     417: aastore
        //     418: aload 23
        //     420: iconst_3
        //     421: lload 7
        //     423: l2i
        //     424: invokestatic 251	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     427: aastore
        //     428: aload 22
        //     430: ifnull +93 -> 523
        //     433: aload 22
        //     435: astore 24
        //     437: aload 23
        //     439: iconst_4
        //     440: aload 24
        //     442: aastore
        //     443: sipush 2808
        //     446: aload 23
        //     448: invokestatic 259	android/util/EventLog:writeEvent	(I[Ljava/lang/Object;)I
        //     451: pop
        //     452: aload 22
        //     454: ifnonnull +15 -> 469
        //     457: aload_0
        //     458: ldc_w 289
        //     461: invokevirtual 264	com/android/server/Watchdog:rebootSystem	(Ljava/lang/String;)V
        //     464: aload_0
        //     465: monitorexit
        //     466: goto -434 -> 32
        //     469: lload 14
        //     471: lload 11
        //     473: ladd
        //     474: lload 16
        //     476: lload 7
        //     478: ladd
        //     479: lcmp
        //     480: iflt +50 -> 530
        //     483: aload_0
        //     484: getfield 122	com/android/server/Watchdog:mCalendar	Ljava/util/Calendar;
        //     487: lload 14
        //     489: lload 18
        //     491: ladd
        //     492: lload_3
        //     493: invokestatic 234	com/android/server/Watchdog:computeCalendarTime	(Ljava/util/Calendar;JJ)J
        //     496: lstore 16
        //     498: goto -165 -> 333
        //     501: aload_0
        //     502: getfield 122	com/android/server/Watchdog:mCalendar	Ljava/util/Calendar;
        //     505: lload 14
        //     507: lload 18
        //     509: ladd
        //     510: lload_3
        //     511: invokestatic 234	com/android/server/Watchdog:computeCalendarTime	(Ljava/util/Calendar;JJ)J
        //     514: lstore 20
        //     516: lload 20
        //     518: lstore 16
        //     520: goto -187 -> 333
        //     523: ldc 253
        //     525: astore 24
        //     527: goto -90 -> 437
        //     530: lload 14
        //     532: lload 11
        //     534: ladd
        //     535: lstore 16
        //     537: goto -204 -> 333
        //
        // Exception table:
        //     from	to	target	type
        //     110	257	253	finally
        //     321	335	253	finally
        //     374	516	253	finally
    }

    public void init(Context paramContext, BatteryService paramBatteryService, PowerManagerService paramPowerManagerService, AlarmManagerService paramAlarmManagerService, ActivityManagerService paramActivityManagerService)
    {
        this.mResolver = paramContext.getContentResolver();
        this.mBattery = paramBatteryService;
        this.mPower = paramPowerManagerService;
        this.mAlarm = paramAlarmManagerService;
        this.mActivity = paramActivityManagerService;
        paramContext.registerReceiver(new RebootReceiver(), new IntentFilter("com.android.service.Watchdog.REBOOT"));
        this.mRebootIntent = PendingIntent.getBroadcast(paramContext, 0, new Intent("com.android.service.Watchdog.REBOOT"), 0);
        paramContext.registerReceiver(new RebootRequestReceiver(), new IntentFilter("android.intent.action.REBOOT"), "android.permission.REBOOT", null);
        this.mBootTime = System.currentTimeMillis();
    }

    public void processStarted(String paramString, int paramInt)
    {
        try
        {
            if ("com.android.phone".equals(paramString))
                this.mPhonePid = paramInt;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    void rebootSystem(String paramString)
    {
        Slog.i("Watchdog", "Rebooting system because: " + paramString);
        ((PowerManagerService)ServiceManager.getService("power")).reboot(paramString);
    }

    void retrieveBrutalityAmount()
    {
        int i;
        if (this.mReqMinScreenOff >= 0)
        {
            i = this.mReqMinScreenOff;
            this.mMinScreenOff = (i * 1000);
            if (this.mReqMinNextAlarm < 0)
                break label60;
        }
        label60: for (int j = this.mReqMinNextAlarm; ; j = Settings.Secure.getInt(this.mResolver, "memcheck_min_alarm", 180))
        {
            this.mMinAlarm = (j * 1000);
            return;
            i = Settings.Secure.getInt(this.mResolver, "memcheck_min_screen_off", 300);
            break;
        }
    }

    public void run()
    {
        int i = 0;
        while (true)
        {
            this.mCompleted = false;
            this.mHandler.sendEmptyMessage(2718);
            long l1 = 30000L;
            try
            {
                long l2 = SystemClock.uptimeMillis();
                while (true)
                    if (l1 > 0L)
                    {
                        boolean bool2 = this.mForceKillSystem;
                        if (!bool2)
                            try
                            {
                                wait(l1);
                                l1 = 30000L - (SystemClock.uptimeMillis() - l2);
                            }
                            catch (InterruptedException localInterruptedException2)
                            {
                                while (true)
                                    Log.wtf("Watchdog", localInterruptedException2);
                            }
                    }
            }
            finally
            {
            }
            if ((this.mCompleted) && (!this.mForceKillSystem))
            {
                i = 0;
                continue;
            }
            if (i == 0)
            {
                ArrayList localArrayList1 = new ArrayList();
                localArrayList1.add(Integer.valueOf(Process.myPid()));
                ActivityManagerService.dumpStackTraces(true, localArrayList1, null, null, NATIVE_STACKS_OF_INTEREST);
                i = 1;
                continue;
            }
            final String str;
            label171: boolean bool1;
            label228: Thread local1;
            if (this.mCurrentMonitor != null)
            {
                str = this.mCurrentMonitor.getClass().getName();
                EventLog.writeEvent(2802, str);
                ArrayList localArrayList2 = new ArrayList();
                localArrayList2.add(Integer.valueOf(Process.myPid()));
                if (this.mPhonePid > 0)
                    localArrayList2.add(Integer.valueOf(this.mPhonePid));
                if (i != 0)
                    break label340;
                bool1 = true;
                final File localFile = ActivityManagerService.dumpStackTraces(bool1, localArrayList2, null, null, NATIVE_STACKS_OF_INTEREST);
                SystemClock.sleep(2000L);
                dumpKernelStackTraces();
                local1 = new Thread("watchdogWriteToDropbox")
                {
                    public void run()
                    {
                        Watchdog.this.mActivity.addErrorToDropBox("watchdog", null, "system_server", null, null, str, null, localFile, null);
                    }
                };
                local1.start();
            }
            try
            {
                local1.join(2000L);
                label283: if (!Debug.isDebuggerConnected())
                {
                    Slog.w("Watchdog", "*** WATCHDOG KILLING SYSTEM PROCESS: " + str);
                    Process.killProcess(Process.myPid());
                    System.exit(10);
                }
                while (true)
                {
                    i = 0;
                    break;
                    str = "null";
                    break label171;
                    label340: bool1 = false;
                    break label228;
                    Slog.w("Watchdog", "Debugger connected: Watchdog is *not* killing the system process");
                }
            }
            catch (InterruptedException localInterruptedException1)
            {
                break label283;
            }
        }
    }

    String shouldWeBeBrutalLocked(long paramLong)
    {
        String str;
        if ((this.mBattery == null) || (!this.mBattery.isPowered()))
            str = "battery";
        while (true)
        {
            return str;
            if ((this.mMinScreenOff >= 0) && ((this.mPower == null) || (this.mPower.timeSinceScreenOn() < this.mMinScreenOff)))
                str = "screen";
            else if ((this.mMinAlarm >= 0) && ((this.mAlarm == null) || (this.mAlarm.timeToNextAlarm() < this.mMinAlarm)))
                str = "alarm";
            else
                str = null;
        }
    }

    public static abstract interface Monitor
    {
        public abstract void monitor();
    }

    final class RebootRequestReceiver extends BroadcastReceiver
    {
        RebootRequestReceiver()
        {
        }

        public void onReceive(Context paramContext, Intent paramIntent)
        {
            Watchdog localWatchdog = Watchdog.this;
            boolean bool;
            Object[] arrayOfObject;
            if (paramIntent.getIntExtra("nowait", 0) != 0)
            {
                bool = true;
                localWatchdog.mReqRebootNoWait = bool;
                Watchdog.this.mReqRebootInterval = paramIntent.getIntExtra("interval", -1);
                Watchdog.this.mReqRebootStartTime = paramIntent.getIntExtra("startTime", -1);
                Watchdog.this.mReqRebootWindow = paramIntent.getIntExtra("window", -1);
                Watchdog.this.mReqMinScreenOff = paramIntent.getIntExtra("minScreenOff", -1);
                Watchdog.this.mReqMinNextAlarm = paramIntent.getIntExtra("minNextAlarm", -1);
                Watchdog.this.mReqRecheckInterval = paramIntent.getIntExtra("recheckInterval", -1);
                arrayOfObject = new Object[7];
                if (!Watchdog.this.mReqRebootNoWait)
                    break label252;
            }
            label252: for (int i = 1; ; i = 0)
            {
                arrayOfObject[0] = Integer.valueOf(i);
                arrayOfObject[1] = Integer.valueOf(Watchdog.this.mReqRebootInterval);
                arrayOfObject[2] = Integer.valueOf(Watchdog.this.mReqRecheckInterval);
                arrayOfObject[3] = Integer.valueOf(Watchdog.this.mReqRebootStartTime);
                arrayOfObject[4] = Integer.valueOf(Watchdog.this.mReqRebootWindow);
                arrayOfObject[5] = Integer.valueOf(Watchdog.this.mReqMinScreenOff);
                arrayOfObject[6] = Integer.valueOf(Watchdog.this.mReqMinNextAlarm);
                EventLog.writeEvent(2811, arrayOfObject);
                Watchdog.this.checkReboot(true);
                return;
                bool = false;
                break;
            }
        }
    }

    final class RebootReceiver extends BroadcastReceiver
    {
        RebootReceiver()
        {
        }

        public void onReceive(Context paramContext, Intent paramIntent)
        {
            Watchdog.this.checkReboot(true);
        }
    }

    final class HeartbeatHandler extends Handler
    {
        HeartbeatHandler()
        {
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
            case 2718:
            }
            while (true)
            {
                return;
                if (Watchdog.this.mReqRebootInterval >= 0);
                for (int i = Watchdog.this.mReqRebootInterval; ; i = Settings.Secure.getInt(Watchdog.this.mResolver, "reboot_interval", 0))
                {
                    if (Watchdog.this.mRebootInterval != i)
                    {
                        Watchdog.this.mRebootInterval = i;
                        Watchdog.this.checkReboot(false);
                    }
                    int j = Watchdog.this.mMonitors.size();
                    for (int k = 0; k < j; k++)
                    {
                        Watchdog.this.mCurrentMonitor = ((Watchdog.Monitor)Watchdog.this.mMonitors.get(k));
                        Watchdog.this.mCurrentMonitor.monitor();
                    }
                }
                synchronized (Watchdog.this)
                {
                    Watchdog.this.mCompleted = true;
                    Watchdog.this.mCurrentMonitor = null;
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.Watchdog
 * JD-Core Version:        0.6.2
 */