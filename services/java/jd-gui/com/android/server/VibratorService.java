package com.android.server;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.hardware.input.InputManager;
import android.hardware.input.InputManager.InputDeviceListener;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.IVibratorService.Stub;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Process;
import android.os.SystemClock;
import android.os.Vibrator;
import android.os.WorkSource;
import android.provider.Settings.SettingNotFoundException;
import android.provider.Settings.System;
import android.view.InputDevice;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;

public class VibratorService extends IVibratorService.Stub
    implements InputManager.InputDeviceListener
{
    private static final String TAG = "VibratorService";
    private final Context mContext;
    private Vibration mCurrentVibration;
    private final Handler mH = new Handler();
    private InputManager mIm;
    private boolean mInputDeviceListenerRegistered;
    private final ArrayList<Vibrator> mInputDeviceVibrators = new ArrayList();
    BroadcastReceiver mIntentReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            if (paramAnonymousIntent.getAction().equals("android.intent.action.SCREEN_OFF"))
                synchronized (VibratorService.this.mVibrations)
                {
                    VibratorService.this.doCancelVibrateLocked();
                    int i = VibratorService.this.mVibrations.size();
                    for (int j = 0; j < i; j++)
                        VibratorService.this.unlinkVibration((VibratorService.Vibration)VibratorService.this.mVibrations.get(j));
                    VibratorService.this.mVibrations.clear();
                }
        }
    };
    volatile VibrateThread mThread;
    private final WorkSource mTmpWorkSource = new WorkSource();
    private boolean mVibrateInputDevicesSetting;
    private final Runnable mVibrationRunnable = new Runnable()
    {
        public void run()
        {
            synchronized (VibratorService.this.mVibrations)
            {
                VibratorService.this.doCancelVibrateLocked();
                VibratorService.this.startNextVibrationLocked();
                return;
            }
        }
    };
    private final LinkedList<Vibration> mVibrations;
    private final PowerManager.WakeLock mWakeLock;

    VibratorService(Context paramContext)
    {
        vibratorOff();
        this.mContext = paramContext;
        this.mWakeLock = ((PowerManager)paramContext.getSystemService("power")).newWakeLock(1, "*vibrator*");
        this.mWakeLock.setReferenceCounted(true);
        this.mVibrations = new LinkedList();
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("android.intent.action.SCREEN_OFF");
        paramContext.registerReceiver(this.mIntentReceiver, localIntentFilter);
    }

    private void doCancelVibrateLocked()
    {
        if (this.mThread != null);
        synchronized (this.mThread)
        {
            this.mThread.mDone = true;
            this.mThread.notify();
            this.mThread = null;
            doVibratorOff();
            this.mH.removeCallbacks(this.mVibrationRunnable);
            return;
        }
    }

    private boolean doVibratorExists()
    {
        return vibratorExists();
    }

    private void doVibratorOff()
    {
        synchronized (this.mInputDeviceVibrators)
        {
            int i = this.mInputDeviceVibrators.size();
            if (i != 0)
                for (int j = 0; j < i; j++)
                    ((Vibrator)this.mInputDeviceVibrators.get(j)).cancel();
            vibratorOff();
            return;
        }
    }

    private void doVibratorOn(long paramLong)
    {
        synchronized (this.mInputDeviceVibrators)
        {
            int i = this.mInputDeviceVibrators.size();
            if (i != 0)
                for (int j = 0; j < i; j++)
                    ((Vibrator)this.mInputDeviceVibrators.get(j)).vibrate(paramLong);
            vibratorOn(paramLong);
            return;
        }
    }

    private boolean isAll0(long[] paramArrayOfLong)
    {
        int i = paramArrayOfLong.length;
        int j = 0;
        if (j < i)
            if (paramArrayOfLong[j] == 0L);
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            j++;
            break;
        }
    }

    private Vibration removeVibrationLocked(IBinder paramIBinder)
    {
        ListIterator localListIterator = this.mVibrations.listIterator(0);
        Vibration localVibration;
        while (localListIterator.hasNext())
        {
            localVibration = (Vibration)localListIterator.next();
            if (localVibration.mToken == paramIBinder)
            {
                localListIterator.remove();
                unlinkVibration(localVibration);
            }
        }
        while (true)
        {
            return localVibration;
            if ((this.mCurrentVibration != null) && (this.mCurrentVibration.mToken == paramIBinder))
            {
                unlinkVibration(this.mCurrentVibration);
                localVibration = this.mCurrentVibration;
            }
            else
            {
                localVibration = null;
            }
        }
    }

    private void startNextVibrationLocked()
    {
        if (this.mVibrations.size() <= 0)
            this.mCurrentVibration = null;
        while (true)
        {
            return;
            this.mCurrentVibration = ((Vibration)this.mVibrations.getFirst());
            startVibrationLocked(this.mCurrentVibration);
        }
    }

    private void startVibrationLocked(Vibration paramVibration)
    {
        if (paramVibration.mTimeout != 0L)
        {
            doVibratorOn(paramVibration.mTimeout);
            this.mH.postDelayed(this.mVibrationRunnable, paramVibration.mTimeout);
        }
        while (true)
        {
            return;
            this.mThread = new VibrateThread(paramVibration);
            this.mThread.start();
        }
    }

    private void unlinkVibration(Vibration paramVibration)
    {
        if (paramVibration.mPattern != null)
            paramVibration.mToken.unlinkToDeath(paramVibration, 0);
    }

    private void updateInputDeviceVibrators()
    {
        boolean bool = true;
        synchronized (this.mVibrations)
        {
            doCancelVibrateLocked();
            synchronized (this.mInputDeviceVibrators)
            {
                this.mVibrateInputDevicesSetting = false;
            }
        }
        try
        {
            if (Settings.System.getInt(this.mContext.getContentResolver(), "vibrate_input_devices") > 0)
            {
                this.mVibrateInputDevicesSetting = bool;
                if (this.mVibrateInputDevicesSetting)
                    if (!this.mInputDeviceListenerRegistered)
                    {
                        this.mInputDeviceListenerRegistered = true;
                        this.mIm.registerInputDeviceListener(this, this.mH);
                    }
                while (true)
                {
                    this.mInputDeviceVibrators.clear();
                    if (!this.mVibrateInputDevicesSetting)
                        break;
                    int[] arrayOfInt = this.mIm.getInputDeviceIds();
                    i = 0;
                    if (i >= arrayOfInt.length)
                        break;
                    Vibrator localVibrator = this.mIm.getInputDevice(arrayOfInt[i]).getVibrator();
                    if (!localVibrator.hasVibrator())
                        break label202;
                    this.mInputDeviceVibrators.add(localVibrator);
                    break label202;
                    if (this.mInputDeviceListenerRegistered)
                    {
                        this.mInputDeviceListenerRegistered = false;
                        this.mIm.unregisterInputDeviceListener(this);
                    }
                }
                localObject2 = finally;
                throw localObject2;
                localObject1 = finally;
                throw localObject1;
                startNextVibrationLocked();
                return;
            }
        }
        catch (Settings.SettingNotFoundException localSettingNotFoundException)
        {
            while (true)
            {
                int i;
                continue;
                label202: i++;
                continue;
                bool = false;
            }
        }
    }

    static native boolean vibratorExists();

    static native void vibratorOff();

    static native void vibratorOn(long paramLong);

    public void cancelVibrate(IBinder paramIBinder)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.VIBRATE", "cancelVibrate");
        long l = Binder.clearCallingIdentity();
        try
        {
            synchronized (this.mVibrations)
            {
                if (removeVibrationLocked(paramIBinder) == this.mCurrentVibration)
                {
                    doCancelVibrateLocked();
                    startNextVibrationLocked();
                }
                return;
            }
        }
        finally
        {
            Binder.restoreCallingIdentity(l);
        }
    }

    public boolean hasVibrator()
    {
        return doVibratorExists();
    }

    public void onInputDeviceAdded(int paramInt)
    {
        updateInputDeviceVibrators();
    }

    public void onInputDeviceChanged(int paramInt)
    {
        updateInputDeviceVibrators();
    }

    public void onInputDeviceRemoved(int paramInt)
    {
        updateInputDeviceVibrators();
    }

    public void systemReady()
    {
        this.mIm = ((InputManager)this.mContext.getSystemService("input"));
        this.mContext.getContentResolver().registerContentObserver(Settings.System.getUriFor("vibrate_input_devices"), true, new ContentObserver(this.mH)
        {
            public void onChange(boolean paramAnonymousBoolean)
            {
                VibratorService.this.updateInputDeviceVibrators();
            }
        });
        updateInputDeviceVibrators();
    }

    public void vibrate(long paramLong, IBinder paramIBinder)
    {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.VIBRATE") != 0)
            throw new SecurityException("Requires VIBRATE permission");
        int i = Binder.getCallingUid();
        if ((paramLong <= 0L) || ((this.mCurrentVibration != null) && (this.mCurrentVibration.hasLongerTimeout(paramLong))));
        while (true)
        {
            return;
            Vibration localVibration = new Vibration(paramIBinder, paramLong, i);
            synchronized (this.mVibrations)
            {
                removeVibrationLocked(paramIBinder);
                doCancelVibrateLocked();
                this.mCurrentVibration = localVibration;
                startVibrationLocked(localVibration);
            }
        }
    }

    // ERROR //
    public void vibratePattern(long[] paramArrayOfLong, int paramInt, IBinder paramIBinder)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 82	com/android/server/VibratorService:mContext	Landroid/content/Context;
        //     4: ldc_w 311
        //     7: invokevirtual 354	android/content/Context:checkCallingOrSelfPermission	(Ljava/lang/String;)I
        //     10: ifeq +14 -> 24
        //     13: new 356	java/lang/SecurityException
        //     16: dup
        //     17: ldc_w 358
        //     20: invokespecial 360	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     23: athrow
        //     24: invokestatic 363	android/os/Binder:getCallingUid	()I
        //     27: istore 4
        //     29: invokestatic 322	android/os/Binder:clearCallingIdentity	()J
        //     32: lstore 5
        //     34: aload_1
        //     35: ifnull +30 -> 65
        //     38: aload_1
        //     39: arraylength
        //     40: ifeq +25 -> 65
        //     43: aload_0
        //     44: aload_1
        //     45: invokespecial 376	com/android/server/VibratorService:isAll0	([J)Z
        //     48: ifne +17 -> 65
        //     51: aload_1
        //     52: arraylength
        //     53: istore 8
        //     55: iload_2
        //     56: iload 8
        //     58: if_icmpge +7 -> 65
        //     61: aload_3
        //     62: ifnonnull +9 -> 71
        //     65: lload 5
        //     67: invokestatic 327	android/os/Binder:restoreCallingIdentity	(J)V
        //     70: return
        //     71: new 17	com/android/server/VibratorService$Vibration
        //     74: dup
        //     75: aload_0
        //     76: aload_3
        //     77: aload_1
        //     78: iload_2
        //     79: iload 4
        //     81: invokespecial 379	com/android/server/VibratorService$Vibration:<init>	(Lcom/android/server/VibratorService;Landroid/os/IBinder;[JII)V
        //     84: astore 9
        //     86: aload_3
        //     87: aload 9
        //     89: iconst_0
        //     90: invokeinterface 383 3 0
        //     95: aload_0
        //     96: getfield 111	com/android/server/VibratorService:mVibrations	Ljava/util/LinkedList;
        //     99: astore 11
        //     101: aload 11
        //     103: monitorenter
        //     104: aload_0
        //     105: aload_3
        //     106: invokespecial 324	com/android/server/VibratorService:removeVibrationLocked	(Landroid/os/IBinder;)Lcom/android/server/VibratorService$Vibration;
        //     109: pop
        //     110: aload_0
        //     111: invokespecial 148	com/android/server/VibratorService:doCancelVibrateLocked	()V
        //     114: iload_2
        //     115: iflt +45 -> 160
        //     118: aload_0
        //     119: getfield 111	com/android/server/VibratorService:mVibrations	Ljava/util/LinkedList;
        //     122: aload 9
        //     124: invokevirtual 387	java/util/LinkedList:addFirst	(Ljava/lang/Object;)V
        //     127: aload_0
        //     128: invokespecial 152	com/android/server/VibratorService:startNextVibrationLocked	()V
        //     131: aload 11
        //     133: monitorexit
        //     134: goto -69 -> 65
        //     137: astore 12
        //     139: aload 11
        //     141: monitorexit
        //     142: aload 12
        //     144: athrow
        //     145: astore 7
        //     147: lload 5
        //     149: invokestatic 327	android/os/Binder:restoreCallingIdentity	(J)V
        //     152: aload 7
        //     154: athrow
        //     155: astore 10
        //     157: goto -92 -> 65
        //     160: aload_0
        //     161: aload 9
        //     163: putfield 130	com/android/server/VibratorService:mCurrentVibration	Lcom/android/server/VibratorService$Vibration;
        //     166: aload_0
        //     167: aload 9
        //     169: invokespecial 230	com/android/server/VibratorService:startVibrationLocked	(Lcom/android/server/VibratorService$Vibration;)V
        //     172: goto -41 -> 131
        //
        // Exception table:
        //     from	to	target	type
        //     104	142	137	finally
        //     160	172	137	finally
        //     38	55	145	finally
        //     71	86	145	finally
        //     86	95	145	finally
        //     95	104	145	finally
        //     142	145	145	finally
        //     86	95	155	android/os/RemoteException
    }

    private class VibrateThread extends Thread
    {
        boolean mDone;
        final VibratorService.Vibration mVibration;

        VibrateThread(VibratorService.Vibration arg2)
        {
            VibratorService.Vibration localVibration;
            this.mVibration = localVibration;
            VibratorService.this.mTmpWorkSource.set(VibratorService.Vibration.access$800(localVibration));
            VibratorService.this.mWakeLock.setWorkSource(VibratorService.this.mTmpWorkSource);
            VibratorService.this.mWakeLock.acquire();
        }

        private void delay(long paramLong)
        {
            long l;
            if (paramLong > 0L)
                l = SystemClock.uptimeMillis();
            try
            {
                wait(paramLong);
                label15: if (this.mDone);
                while (true)
                {
                    return;
                    paramLong = paramLong - SystemClock.uptimeMillis() - l;
                    if (paramLong > 0L)
                        break;
                }
            }
            catch (InterruptedException localInterruptedException)
            {
                break label15;
            }
        }

        public void run()
        {
            Process.setThreadPriority(-8);
            try
            {
                long[] arrayOfLong = VibratorService.Vibration.access$700(this.mVibration);
                int i = arrayOfLong.length;
                int j = VibratorService.Vibration.access$1100(this.mVibration);
                long l1 = 0L;
                int k = 0;
                label221: label247: 
                do
                    while (true)
                        try
                        {
                            boolean bool1 = this.mDone;
                            if (bool1)
                                break label247;
                            if (k < i)
                            {
                                int n = k + 1;
                                long l2 = arrayOfLong[k];
                                l1 += l2;
                                k = n;
                            }
                            delay(l1);
                            boolean bool2 = this.mDone;
                            if (bool2)
                                VibratorService.this.mWakeLock.release();
                            synchronized (VibratorService.this.mVibrations)
                            {
                                if (VibratorService.this.mThread == this)
                                    VibratorService.this.mThread = null;
                                if (!this.mDone)
                                {
                                    VibratorService.this.mVibrations.remove(this.mVibration);
                                    VibratorService.this.unlinkVibration(this.mVibration);
                                    VibratorService.this.startNextVibrationLocked();
                                }
                                return;
                                if (k >= i)
                                    break;
                                m = k + 1;
                                l1 = arrayOfLong[k];
                                if (l1 > 0L)
                                {
                                    VibratorService.this.doVibratorOn(l1);
                                    k = m;
                                    continue;
                                    Object localObject1;
                                    throw localObject1;
                                }
                            }
                        }
                        finally
                        {
                            int m;
                            continue;
                            k = m;
                        }
                while (j < 0);
                l1 = 0L;
                k = j;
            }
            finally
            {
                break label221;
            }
        }
    }

    private class Vibration
        implements IBinder.DeathRecipient
    {
        private final long[] mPattern;
        private final int mRepeat;
        private final long mStartTime;
        private final long mTimeout;
        private final IBinder mToken;
        private final int mUid;

        Vibration(IBinder paramLong, long arg3, int arg5)
        {
            this(paramLong, ???, null, 0, i);
        }

        private Vibration(IBinder paramLong, long arg3, long[] paramInt1, int paramInt2, int arg7)
        {
            this.mToken = paramLong;
            this.mTimeout = ???;
            this.mStartTime = SystemClock.uptimeMillis();
            this.mPattern = paramInt1;
            this.mRepeat = paramInt2;
            int i;
            this.mUid = i;
        }

        Vibration(IBinder paramArrayOfLong, long[] paramInt1, int paramInt2, int arg5)
        {
            this(paramArrayOfLong, 0L, paramInt1, paramInt2, i);
        }

        public void binderDied()
        {
            synchronized (VibratorService.this.mVibrations)
            {
                VibratorService.this.mVibrations.remove(this);
                if (this == VibratorService.this.mCurrentVibration)
                {
                    VibratorService.this.doCancelVibrateLocked();
                    VibratorService.this.startNextVibrationLocked();
                }
                return;
            }
        }

        public boolean hasLongerTimeout(long paramLong)
        {
            boolean bool = false;
            if (this.mTimeout == 0L);
            while (true)
            {
                return bool;
                if (this.mStartTime + this.mTimeout >= paramLong + SystemClock.uptimeMillis())
                    bool = true;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.VibratorService
 * JD-Core Version:        0.6.2
 */