package com.android.server;

import android.content.Context;
import android.content.res.Resources;
import android.hardware.ISerialManager.Stub;
import android.os.ParcelFileDescriptor;
import java.io.File;
import java.util.ArrayList;

public class SerialService extends ISerialManager.Stub
{
    private final Context mContext;
    private final String[] mSerialPorts;

    public SerialService(Context paramContext)
    {
        this.mContext = paramContext;
        this.mSerialPorts = paramContext.getResources().getStringArray(17236000);
    }

    private native ParcelFileDescriptor native_open(String paramString);

    public String[] getSerialPorts()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.SERIAL_PORT", null);
        ArrayList localArrayList = new ArrayList();
        for (int i = 0; i < this.mSerialPorts.length; i++)
        {
            String str = this.mSerialPorts[i];
            if (new File(str).exists())
                localArrayList.add(str);
        }
        String[] arrayOfString = new String[localArrayList.size()];
        localArrayList.toArray(arrayOfString);
        return arrayOfString;
    }

    public ParcelFileDescriptor openSerialPort(String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.SERIAL_PORT", null);
        return native_open(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.SerialService
 * JD-Core Version:        0.6.2
 */