package com.android.server;

import android.content.ComponentName;
import android.content.pm.ActivityInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.ResolveInfo;
import android.util.Slog;
import com.android.internal.util.XmlUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class PreferredComponent
{
    private final Callbacks mCallbacks;
    public final ComponentName mComponent;
    public final int mMatch;
    private String mParseError;
    private final String[] mSetClasses;
    private final String[] mSetComponents;
    private final String[] mSetPackages;
    private final String mShortComponent;

    public PreferredComponent(Callbacks paramCallbacks, int paramInt, ComponentName[] paramArrayOfComponentName, ComponentName paramComponentName)
    {
        this.mCallbacks = paramCallbacks;
        this.mMatch = (0xFFF0000 & paramInt);
        this.mComponent = paramComponentName;
        this.mShortComponent = paramComponentName.flattenToShortString();
        this.mParseError = null;
        String[] arrayOfString1;
        String[] arrayOfString2;
        String[] arrayOfString3;
        int j;
        ComponentName localComponentName;
        if (paramArrayOfComponentName != null)
        {
            int i = paramArrayOfComponentName.length;
            arrayOfString1 = new String[i];
            arrayOfString2 = new String[i];
            arrayOfString3 = new String[i];
            j = 0;
            if (j < i)
            {
                localComponentName = paramArrayOfComponentName[j];
                if (localComponentName == null)
                {
                    this.mSetPackages = null;
                    this.mSetClasses = null;
                    this.mSetComponents = null;
                }
            }
        }
        while (true)
        {
            return;
            arrayOfString1[j] = localComponentName.getPackageName().intern();
            arrayOfString2[j] = localComponentName.getClassName().intern();
            arrayOfString3[j] = localComponentName.flattenToShortString().intern();
            j++;
            break;
            this.mSetPackages = arrayOfString1;
            this.mSetClasses = arrayOfString2;
            this.mSetComponents = arrayOfString3;
            continue;
            this.mSetPackages = null;
            this.mSetClasses = null;
            this.mSetComponents = null;
        }
    }

    public PreferredComponent(Callbacks paramCallbacks, XmlPullParser paramXmlPullParser)
        throws XmlPullParserException, IOException
    {
        this.mCallbacks = paramCallbacks;
        this.mShortComponent = paramXmlPullParser.getAttributeValue(null, "name");
        this.mComponent = ComponentName.unflattenFromString(this.mShortComponent);
        if (this.mComponent == null)
            this.mParseError = ("Bad activity name " + this.mShortComponent);
        String str1 = paramXmlPullParser.getAttributeValue(null, "match");
        int i;
        int j;
        label117: String[] arrayOfString1;
        label129: String[] arrayOfString2;
        label141: String[] arrayOfString3;
        label153: int k;
        int m;
        if (str1 != null)
        {
            i = Integer.parseInt(str1, 16);
            this.mMatch = i;
            String str2 = paramXmlPullParser.getAttributeValue(null, "set");
            if (str2 == null)
                break label287;
            j = Integer.parseInt(str2);
            if (j <= 0)
                break label293;
            arrayOfString1 = new String[j];
            if (j <= 0)
                break label299;
            arrayOfString2 = new String[j];
            if (j <= 0)
                break label305;
            arrayOfString3 = new String[j];
            k = 0;
            m = paramXmlPullParser.getDepth();
        }
        while (true)
        {
            label164: int n = paramXmlPullParser.next();
            if ((n == 1) || ((n == 3) && (paramXmlPullParser.getDepth() <= m)))
                break label497;
            if ((n != 3) && (n != 4))
            {
                String str3 = paramXmlPullParser.getName();
                if (str3.equals("set"))
                {
                    String str4 = paramXmlPullParser.getAttributeValue(null, "name");
                    if (str4 == null)
                        if (this.mParseError == null)
                            this.mParseError = ("No name in set tag in preferred activity " + this.mShortComponent);
                    while (true)
                    {
                        XmlUtils.skipCurrentTag(paramXmlPullParser);
                        break label164;
                        i = 0;
                        break;
                        label287: j = 0;
                        break label117;
                        label293: arrayOfString1 = null;
                        break label129;
                        label299: arrayOfString2 = null;
                        break label141;
                        label305: arrayOfString3 = null;
                        break label153;
                        if (k >= j)
                        {
                            if (this.mParseError == null)
                                this.mParseError = ("Too many set tags in preferred activity " + this.mShortComponent);
                        }
                        else
                        {
                            ComponentName localComponentName = ComponentName.unflattenFromString(str4);
                            if (localComponentName == null)
                            {
                                if (this.mParseError == null)
                                    this.mParseError = ("Bad set name " + str4 + " in preferred activity " + this.mShortComponent);
                            }
                            else
                            {
                                arrayOfString1[k] = localComponentName.getPackageName();
                                arrayOfString2[k] = localComponentName.getClassName();
                                arrayOfString3[k] = str4;
                                k++;
                            }
                        }
                    }
                }
                if (!this.mCallbacks.onReadTag(str3, paramXmlPullParser))
                {
                    Slog.w("PreferredComponent", "Unknown element: " + paramXmlPullParser.getName());
                    XmlUtils.skipCurrentTag(paramXmlPullParser);
                }
            }
        }
        label497: if ((k != j) && (this.mParseError == null))
            this.mParseError = ("Not enough set tags (expected " + j + " but found " + k + ") in " + this.mShortComponent);
        this.mSetPackages = arrayOfString1;
        this.mSetClasses = arrayOfString2;
        this.mSetComponents = arrayOfString3;
    }

    public void dump(PrintWriter paramPrintWriter, String paramString, Object paramObject)
    {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print(Integer.toHexString(System.identityHashCode(paramObject)));
        paramPrintWriter.print(' ');
        paramPrintWriter.print(this.mComponent.flattenToShortString());
        paramPrintWriter.print(" match=0x");
        paramPrintWriter.println(Integer.toHexString(this.mMatch));
        if (this.mSetComponents != null)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.println("    Selected from:");
            for (int i = 0; i < this.mSetComponents.length; i++)
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("        ");
                paramPrintWriter.println(this.mSetComponents[i]);
            }
        }
    }

    public String getParseError()
    {
        return this.mParseError;
    }

    public boolean sameSet(List<ResolveInfo> paramList, int paramInt)
    {
        boolean bool = false;
        if (this.mSetPackages == null);
        while (true)
        {
            return bool;
            int i = paramList.size();
            int j = this.mSetPackages.length;
            int k = 0;
            int m = 0;
            while (m < i)
            {
                ResolveInfo localResolveInfo = (ResolveInfo)paramList.get(m);
                if (localResolveInfo.priority != paramInt)
                {
                    label61: m++;
                }
                else
                {
                    ActivityInfo localActivityInfo = localResolveInfo.activityInfo;
                    int n = 0;
                    for (int i1 = 0; ; i1++)
                        if (i1 < j)
                        {
                            if ((this.mSetPackages[i1].equals(localActivityInfo.packageName)) && (this.mSetClasses[i1].equals(localActivityInfo.name)))
                            {
                                k++;
                                n = 1;
                            }
                        }
                        else
                        {
                            if (n != 0)
                                break label61;
                            break;
                        }
                }
            }
            if (k == j)
                bool = true;
        }
    }

    public void writeToXml(XmlSerializer paramXmlSerializer)
        throws IOException
    {
        if (this.mSetClasses != null);
        for (int i = this.mSetClasses.length; ; i = 0)
        {
            paramXmlSerializer.attribute(null, "name", this.mShortComponent);
            if (this.mMatch != 0)
                paramXmlSerializer.attribute(null, "match", Integer.toHexString(this.mMatch));
            paramXmlSerializer.attribute(null, "set", Integer.toString(i));
            for (int j = 0; j < i; j++)
            {
                paramXmlSerializer.startTag(null, "set");
                paramXmlSerializer.attribute(null, "name", this.mSetComponents[j]);
                paramXmlSerializer.endTag(null, "set");
            }
        }
    }

    public static abstract interface Callbacks
    {
        public abstract boolean onReadTag(String paramString, XmlPullParser paramXmlPullParser)
            throws XmlPullParserException, IOException;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.PreferredComponent
 * JD-Core Version:        0.6.2
 */