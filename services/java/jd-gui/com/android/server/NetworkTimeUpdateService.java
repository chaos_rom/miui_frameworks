package com.android.server;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.provider.Settings.System;
import android.util.NtpTrustedTime;
import android.util.TrustedTime;

public class NetworkTimeUpdateService
{
    private static final String ACTION_POLL = "com.android.server.NetworkTimeUpdateService.action.POLL";
    private static final boolean DBG = false;
    private static final int EVENT_AUTO_TIME_CHANGED = 1;
    private static final int EVENT_NETWORK_CONNECTED = 3;
    private static final int EVENT_POLL_NETWORK_TIME = 2;
    private static final long NOT_SET = -1L;
    private static final long POLLING_INTERVAL_MS = 86400000L;
    private static final long POLLING_INTERVAL_SHORTER_MS = 60000L;
    private static int POLL_REQUEST = 0;
    private static final String TAG = "NetworkTimeUpdateService";
    private static final int TIME_ERROR_THRESHOLD_MS = 5000;
    private static final int TRY_AGAIN_TIMES_MAX = 3;
    private AlarmManager mAlarmManager;
    private BroadcastReceiver mConnectivityReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            if ("android.net.conn.CONNECTIVITY_CHANGE".equals(paramAnonymousIntent.getAction()))
            {
                NetworkInfo localNetworkInfo = ((ConnectivityManager)paramAnonymousContext.getSystemService("connectivity")).getActiveNetworkInfo();
                if ((localNetworkInfo != null) && (localNetworkInfo.getState() == NetworkInfo.State.CONNECTED) && ((localNetworkInfo.getType() == 1) || (localNetworkInfo.getType() == 9)))
                    NetworkTimeUpdateService.this.mHandler.obtainMessage(3).sendToTarget();
            }
        }
    };
    private Context mContext;
    private Handler mHandler;
    private long mLastNtpFetchTime = -1L;
    private BroadcastReceiver mNitzReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            String str = paramAnonymousIntent.getAction();
            if ("android.intent.action.NETWORK_SET_TIME".equals(str))
                NetworkTimeUpdateService.access$102(NetworkTimeUpdateService.this, SystemClock.elapsedRealtime());
            while (true)
            {
                return;
                if ("android.intent.action.NETWORK_SET_TIMEZONE".equals(str))
                    NetworkTimeUpdateService.access$202(NetworkTimeUpdateService.this, SystemClock.elapsedRealtime());
            }
        }
    };
    private long mNitzTimeSetTime = -1L;
    private long mNitzZoneSetTime = -1L;
    private PendingIntent mPendingPollIntent;
    private SettingsObserver mSettingsObserver;
    private HandlerThread mThread;
    private TrustedTime mTime;
    private int mTryAgainCounter;

    public NetworkTimeUpdateService(Context paramContext)
    {
        this.mContext = paramContext;
        this.mTime = NtpTrustedTime.getInstance(paramContext);
        this.mAlarmManager = ((AlarmManager)this.mContext.getSystemService("alarm"));
        Intent localIntent = new Intent("com.android.server.NetworkTimeUpdateService.action.POLL", null);
        this.mPendingPollIntent = PendingIntent.getBroadcast(this.mContext, POLL_REQUEST, localIntent, 0);
    }

    private boolean isAutomaticTimeRequested()
    {
        boolean bool = false;
        if (Settings.System.getInt(this.mContext.getContentResolver(), "auto_time", 0) != 0)
            bool = true;
        return bool;
    }

    private void onPollNetworkTime(int paramInt)
    {
        if (!isAutomaticTimeRequested());
        while (true)
        {
            return;
            long l1 = SystemClock.elapsedRealtime();
            if ((this.mNitzTimeSetTime != -1L) && (l1 - this.mNitzTimeSetTime < 86400000L))
            {
                resetAlarm(86400000L);
            }
            else
            {
                long l2 = System.currentTimeMillis();
                if ((this.mLastNtpFetchTime == -1L) || (l1 >= 86400000L + this.mLastNtpFetchTime) || (paramInt == 1))
                {
                    if (this.mTime.getCacheAge() >= 86400000L)
                        this.mTime.forceRefresh();
                    if (this.mTime.getCacheAge() < 86400000L)
                    {
                        long l3 = this.mTime.currentTimeMillis();
                        this.mTryAgainCounter = 0;
                        if (((Math.abs(l3 - l2) > 5000L) || (this.mLastNtpFetchTime == -1L)) && (l3 / 1000L < 2147483647L))
                            SystemClock.setCurrentTimeMillis(l3);
                        this.mLastNtpFetchTime = SystemClock.elapsedRealtime();
                    }
                }
                else
                {
                    resetAlarm(86400000L);
                    continue;
                }
                this.mTryAgainCounter = (1 + this.mTryAgainCounter);
                if (this.mTryAgainCounter <= 3)
                {
                    resetAlarm(60000L);
                }
                else
                {
                    this.mTryAgainCounter = 0;
                    resetAlarm(86400000L);
                }
            }
        }
    }

    private void registerForAlarms()
    {
        this.mContext.registerReceiver(new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                NetworkTimeUpdateService.this.mHandler.obtainMessage(2).sendToTarget();
            }
        }
        , new IntentFilter("com.android.server.NetworkTimeUpdateService.action.POLL"));
    }

    private void registerForConnectivityIntents()
    {
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        this.mContext.registerReceiver(this.mConnectivityReceiver, localIntentFilter);
    }

    private void registerForTelephonyIntents()
    {
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("android.intent.action.NETWORK_SET_TIME");
        localIntentFilter.addAction("android.intent.action.NETWORK_SET_TIMEZONE");
        this.mContext.registerReceiver(this.mNitzReceiver, localIntentFilter);
    }

    private void resetAlarm(long paramLong)
    {
        this.mAlarmManager.cancel(this.mPendingPollIntent);
        long l = paramLong + SystemClock.elapsedRealtime();
        this.mAlarmManager.set(3, l, this.mPendingPollIntent);
    }

    public void systemReady()
    {
        registerForTelephonyIntents();
        registerForAlarms();
        registerForConnectivityIntents();
        this.mThread = new HandlerThread("NetworkTimeUpdateService");
        this.mThread.start();
        this.mHandler = new MyHandler(this.mThread.getLooper());
        this.mHandler.obtainMessage(2).sendToTarget();
        this.mSettingsObserver = new SettingsObserver(this.mHandler, 1);
        this.mSettingsObserver.observe(this.mContext);
    }

    private static class SettingsObserver extends ContentObserver
    {
        private Handler mHandler;
        private int mMsg;

        SettingsObserver(Handler paramHandler, int paramInt)
        {
            super();
            this.mHandler = paramHandler;
            this.mMsg = paramInt;
        }

        void observe(Context paramContext)
        {
            paramContext.getContentResolver().registerContentObserver(Settings.System.getUriFor("auto_time"), false, this);
        }

        public void onChange(boolean paramBoolean)
        {
            this.mHandler.obtainMessage(this.mMsg).sendToTarget();
        }
    }

    private class MyHandler extends Handler
    {
        public MyHandler(Looper arg2)
        {
            super();
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
            case 1:
            case 2:
            case 3:
            }
            while (true)
            {
                return;
                NetworkTimeUpdateService.this.onPollNetworkTime(paramMessage.what);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.NetworkTimeUpdateService
 * JD-Core Version:        0.6.2
 */