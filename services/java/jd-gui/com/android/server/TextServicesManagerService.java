package com.android.server;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.RemoteException;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.util.Slog;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;
import android.view.textservice.SpellCheckerInfo;
import android.view.textservice.SpellCheckerSubtype;
import com.android.internal.content.PackageMonitor;
import com.android.internal.textservice.ISpellCheckerService;
import com.android.internal.textservice.ISpellCheckerService.Stub;
import com.android.internal.textservice.ISpellCheckerSession;
import com.android.internal.textservice.ISpellCheckerSessionListener;
import com.android.internal.textservice.ITextServicesManager.Stub;
import com.android.internal.textservice.ITextServicesSessionListener;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import org.xmlpull.v1.XmlPullParserException;

public class TextServicesManagerService extends ITextServicesManager.Stub
{
    private static final boolean DBG;
    private static final String TAG = TextServicesManagerService.class.getSimpleName();
    private final Context mContext;
    private final TextServicesMonitor mMonitor;
    private final HashMap<String, SpellCheckerBindGroup> mSpellCheckerBindGroups = new HashMap();
    private final ArrayList<SpellCheckerInfo> mSpellCheckerList = new ArrayList();
    private final HashMap<String, SpellCheckerInfo> mSpellCheckerMap = new HashMap();
    private boolean mSystemReady = false;

    public TextServicesManagerService(Context paramContext)
    {
        this.mContext = paramContext;
        this.mMonitor = new TextServicesMonitor(null);
        this.mMonitor.register(paramContext, null, true);
        synchronized (this.mSpellCheckerMap)
        {
            buildSpellCheckerMapLocked(paramContext, this.mSpellCheckerList, this.mSpellCheckerMap);
            if (getCurrentSpellChecker(null) == null)
            {
                SpellCheckerInfo localSpellCheckerInfo = findAvailSpellCheckerLocked(null, null);
                if (localSpellCheckerInfo != null)
                    setCurrentSpellCheckerLocked(localSpellCheckerInfo.getId());
            }
            return;
        }
    }

    private static void buildSpellCheckerMapLocked(Context paramContext, ArrayList<SpellCheckerInfo> paramArrayList, HashMap<String, SpellCheckerInfo> paramHashMap)
    {
        paramArrayList.clear();
        paramHashMap.clear();
        List localList = paramContext.getPackageManager().queryIntentServices(new Intent("android.service.textservice.SpellCheckerService"), 128);
        int i = localList.size();
        int j = 0;
        if (j < i)
        {
            ResolveInfo localResolveInfo = (ResolveInfo)localList.get(j);
            ServiceInfo localServiceInfo = localResolveInfo.serviceInfo;
            ComponentName localComponentName = new ComponentName(localServiceInfo.packageName, localServiceInfo.name);
            if (!"android.permission.BIND_TEXT_SERVICE".equals(localServiceInfo.permission))
                Slog.w(TAG, "Skipping text service " + localComponentName + ": it does not require the permission " + "android.permission.BIND_TEXT_SERVICE");
            while (true)
            {
                j++;
                break;
                try
                {
                    localSpellCheckerInfo = new SpellCheckerInfo(paramContext, localResolveInfo);
                    if (localSpellCheckerInfo.getSubtypeCount() <= 0)
                        Slog.w(TAG, "Skipping text service " + localComponentName + ": it does not contain subtypes.");
                }
                catch (XmlPullParserException localXmlPullParserException)
                {
                    SpellCheckerInfo localSpellCheckerInfo;
                    Slog.w(TAG, "Unable to load the spell checker " + localComponentName, localXmlPullParserException);
                    continue;
                    paramArrayList.add(localSpellCheckerInfo);
                    paramHashMap.put(localSpellCheckerInfo.getId(), localSpellCheckerInfo);
                }
                catch (IOException localIOException)
                {
                    Slog.w(TAG, "Unable to load the spell checker " + localComponentName, localIOException);
                }
            }
        }
    }

    private SpellCheckerInfo findAvailSpellCheckerLocked(String paramString1, String paramString2)
    {
        int i = this.mSpellCheckerList.size();
        SpellCheckerInfo localSpellCheckerInfo;
        if (i == 0)
        {
            Slog.w(TAG, "no available spell checker services found");
            localSpellCheckerInfo = null;
        }
        while (true)
        {
            return localSpellCheckerInfo;
            if (paramString2 != null)
                for (int j = 0; ; j++)
                {
                    if (j >= i)
                        break label72;
                    localSpellCheckerInfo = (SpellCheckerInfo)this.mSpellCheckerList.get(j);
                    if (paramString2.equals(localSpellCheckerInfo.getPackageName()))
                        break;
                }
            label72: if (i > 1)
                Slog.w(TAG, "more than one spell checker service found, picking first");
            localSpellCheckerInfo = (SpellCheckerInfo)this.mSpellCheckerList.get(0);
        }
    }

    private boolean isSpellCheckerEnabledLocked()
    {
        int i = 1;
        long l = Binder.clearCallingIdentity();
        try
        {
            int j = Settings.Secure.getInt(this.mContext.getContentResolver(), "spell_checker_enabled", 1);
            if (j == i)
                return i;
            i = 0;
        }
        finally
        {
            Binder.restoreCallingIdentity(l);
        }
    }

    private void setCurrentSpellCheckerLocked(String paramString)
    {
        if ((TextUtils.isEmpty(paramString)) || (!this.mSpellCheckerMap.containsKey(paramString)));
        while (true)
        {
            return;
            SpellCheckerInfo localSpellCheckerInfo = getCurrentSpellChecker(null);
            if ((localSpellCheckerInfo != null) && (localSpellCheckerInfo.getId().equals(paramString)))
                continue;
            long l = Binder.clearCallingIdentity();
            try
            {
                Settings.Secure.putString(this.mContext.getContentResolver(), "selected_spell_checker", paramString);
                setCurrentSpellCheckerSubtypeLocked(0);
                Binder.restoreCallingIdentity(l);
            }
            finally
            {
                Binder.restoreCallingIdentity(l);
            }
        }
    }

    private void setCurrentSpellCheckerSubtypeLocked(int paramInt)
    {
        SpellCheckerInfo localSpellCheckerInfo = getCurrentSpellChecker(null);
        int i = 0;
        int j = 0;
        while (true)
        {
            long l;
            if ((localSpellCheckerInfo != null) && (j < localSpellCheckerInfo.getSubtypeCount()))
            {
                if (localSpellCheckerInfo.getSubtypeAt(j).hashCode() == paramInt)
                    i = paramInt;
            }
            else
                l = Binder.clearCallingIdentity();
            try
            {
                Settings.Secure.putString(this.mContext.getContentResolver(), "selected_spell_checker_subtype", String.valueOf(i));
                return;
                j++;
            }
            finally
            {
                Binder.restoreCallingIdentity(l);
            }
        }
    }

    private void setSpellCheckerEnabledLocked(boolean paramBoolean)
    {
        long l = Binder.clearCallingIdentity();
        try
        {
            ContentResolver localContentResolver = this.mContext.getContentResolver();
            if (paramBoolean)
            {
                i = 1;
                Settings.Secure.putInt(localContentResolver, "spell_checker_enabled", i);
                return;
            }
            int i = 0;
        }
        finally
        {
            Binder.restoreCallingIdentity(l);
        }
    }

    private void startSpellCheckerServiceInnerLocked(SpellCheckerInfo paramSpellCheckerInfo, String paramString, ITextServicesSessionListener paramITextServicesSessionListener, ISpellCheckerSessionListener paramISpellCheckerSessionListener, int paramInt, Bundle paramBundle)
    {
        String str = paramSpellCheckerInfo.getId();
        InternalServiceConnection localInternalServiceConnection = new InternalServiceConnection(str, paramString, paramBundle);
        Intent localIntent = new Intent("android.service.textservice.SpellCheckerService");
        localIntent.setComponent(paramSpellCheckerInfo.getComponent());
        if (!this.mContext.bindService(localIntent, localInternalServiceConnection, 1))
            Slog.e(TAG, "Failed to get a spell checker service.");
        while (true)
        {
            return;
            SpellCheckerBindGroup localSpellCheckerBindGroup = new SpellCheckerBindGroup(localInternalServiceConnection, paramITextServicesSessionListener, paramString, paramISpellCheckerSessionListener, paramInt, paramBundle);
            this.mSpellCheckerBindGroups.put(str, localSpellCheckerBindGroup);
        }
    }

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.DUMP") != 0)
            paramPrintWriter.println("Permission Denial: can't dump TextServicesManagerService from from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid());
        while (true)
        {
            return;
            synchronized (this.mSpellCheckerMap)
            {
                paramPrintWriter.println("Current Text Services Manager state:");
                paramPrintWriter.println("    Spell Checker Map:");
                Iterator localIterator1 = this.mSpellCheckerMap.entrySet().iterator();
                while (localIterator1.hasNext())
                {
                    Map.Entry localEntry2 = (Map.Entry)localIterator1.next();
                    paramPrintWriter.print("        ");
                    paramPrintWriter.print((String)localEntry2.getKey());
                    paramPrintWriter.println(":");
                    SpellCheckerInfo localSpellCheckerInfo = (SpellCheckerInfo)localEntry2.getValue();
                    paramPrintWriter.print("            ");
                    paramPrintWriter.print("id=");
                    paramPrintWriter.println(localSpellCheckerInfo.getId());
                    paramPrintWriter.print("            ");
                    paramPrintWriter.print("comp=");
                    paramPrintWriter.println(localSpellCheckerInfo.getComponent().toShortString());
                    int k = localSpellCheckerInfo.getSubtypeCount();
                    for (int m = 0; m < k; m++)
                    {
                        SpellCheckerSubtype localSpellCheckerSubtype = localSpellCheckerInfo.getSubtypeAt(m);
                        paramPrintWriter.print("            ");
                        paramPrintWriter.print("Subtype #");
                        paramPrintWriter.print(m);
                        paramPrintWriter.println(":");
                        paramPrintWriter.print("                ");
                        paramPrintWriter.print("locale=");
                        paramPrintWriter.println(localSpellCheckerSubtype.getLocale());
                        paramPrintWriter.print("                ");
                        paramPrintWriter.print("extraValue=");
                        paramPrintWriter.println(localSpellCheckerSubtype.getExtraValue());
                    }
                }
                paramPrintWriter.println("");
                paramPrintWriter.println("    Spell Checker Bind Groups:");
                Iterator localIterator2 = this.mSpellCheckerBindGroups.entrySet().iterator();
                while (localIterator2.hasNext())
                {
                    Map.Entry localEntry1 = (Map.Entry)localIterator2.next();
                    SpellCheckerBindGroup localSpellCheckerBindGroup = (SpellCheckerBindGroup)localEntry1.getValue();
                    paramPrintWriter.print("        ");
                    paramPrintWriter.print((String)localEntry1.getKey());
                    paramPrintWriter.print(" ");
                    paramPrintWriter.print(localSpellCheckerBindGroup);
                    paramPrintWriter.println(":");
                    paramPrintWriter.print("            ");
                    paramPrintWriter.print("mInternalConnection=");
                    paramPrintWriter.println(localSpellCheckerBindGroup.mInternalConnection);
                    paramPrintWriter.print("            ");
                    paramPrintWriter.print("mSpellChecker=");
                    paramPrintWriter.println(localSpellCheckerBindGroup.mSpellChecker);
                    paramPrintWriter.print("            ");
                    paramPrintWriter.print("mBound=");
                    paramPrintWriter.print(localSpellCheckerBindGroup.mBound);
                    paramPrintWriter.print(" mConnected=");
                    paramPrintWriter.println(localSpellCheckerBindGroup.mConnected);
                    int i = localSpellCheckerBindGroup.mListeners.size();
                    for (int j = 0; j < i; j++)
                    {
                        InternalDeathRecipient localInternalDeathRecipient = (InternalDeathRecipient)localSpellCheckerBindGroup.mListeners.get(j);
                        paramPrintWriter.print("            ");
                        paramPrintWriter.print("Listener #");
                        paramPrintWriter.print(j);
                        paramPrintWriter.println(":");
                        paramPrintWriter.print("                ");
                        paramPrintWriter.print("mTsListener=");
                        paramPrintWriter.println(localInternalDeathRecipient.mTsListener);
                        paramPrintWriter.print("                ");
                        paramPrintWriter.print("mScListener=");
                        paramPrintWriter.println(localInternalDeathRecipient.mScListener);
                        paramPrintWriter.print("                ");
                        paramPrintWriter.print("mGroup=");
                        paramPrintWriter.println(localInternalDeathRecipient.mGroup);
                        paramPrintWriter.print("                ");
                        paramPrintWriter.print("mScLocale=");
                        paramPrintWriter.print(localInternalDeathRecipient.mScLocale);
                        paramPrintWriter.print(" mUid=");
                        paramPrintWriter.println(localInternalDeathRecipient.mUid);
                    }
                }
            }
        }
    }

    // ERROR //
    public void finishSpellCheckerService(ISpellCheckerSessionListener paramISpellCheckerSessionListener)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 54	com/android/server/TextServicesManagerService:mSpellCheckerMap	Ljava/util/HashMap;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: new 56	java/util/ArrayList
        //     10: dup
        //     11: invokespecial 57	java/util/ArrayList:<init>	()V
        //     14: astore_3
        //     15: aload_0
        //     16: getfield 61	com/android/server/TextServicesManagerService:mSpellCheckerBindGroups	Ljava/util/HashMap;
        //     19: invokevirtual 489	java/util/HashMap:values	()Ljava/util/Collection;
        //     22: invokeinterface 492 1 0
        //     27: astore 5
        //     29: aload 5
        //     31: invokeinterface 361 1 0
        //     36: ifeq +37 -> 73
        //     39: aload 5
        //     41: invokeinterface 365 1 0
        //     46: checkcast 14	com/android/server/TextServicesManagerService$SpellCheckerBindGroup
        //     49: astore 8
        //     51: aload 8
        //     53: ifnull -24 -> 29
        //     56: aload_3
        //     57: aload 8
        //     59: invokevirtual 214	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     62: pop
        //     63: goto -34 -> 29
        //     66: astore 4
        //     68: aload_2
        //     69: monitorexit
        //     70: aload 4
        //     72: athrow
        //     73: aload_3
        //     74: invokevirtual 219	java/util/ArrayList:size	()I
        //     77: istore 6
        //     79: iconst_0
        //     80: istore 7
        //     82: iload 7
        //     84: iload 6
        //     86: if_icmpge +22 -> 108
        //     89: aload_3
        //     90: iload 7
        //     92: invokevirtual 222	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     95: checkcast 14	com/android/server/TextServicesManagerService$SpellCheckerBindGroup
        //     98: aload_1
        //     99: invokevirtual 495	com/android/server/TextServicesManagerService$SpellCheckerBindGroup:removeListener	(Lcom/android/internal/textservice/ISpellCheckerSessionListener;)V
        //     102: iinc 7 1
        //     105: goto -23 -> 82
        //     108: aload_2
        //     109: monitorexit
        //     110: return
        //
        // Exception table:
        //     from	to	target	type
        //     7	70	66	finally
        //     73	110	66	finally
    }

    public SpellCheckerInfo getCurrentSpellChecker(String paramString)
    {
        SpellCheckerInfo localSpellCheckerInfo;
        synchronized (this.mSpellCheckerMap)
        {
            String str = Settings.Secure.getString(this.mContext.getContentResolver(), "selected_spell_checker");
            if (TextUtils.isEmpty(str))
                localSpellCheckerInfo = null;
            else
                localSpellCheckerInfo = (SpellCheckerInfo)this.mSpellCheckerMap.get(str);
        }
        return localSpellCheckerInfo;
    }

    public SpellCheckerSubtype getCurrentSpellCheckerSubtype(String paramString, boolean paramBoolean)
    {
        while (true)
        {
            SpellCheckerInfo localSpellCheckerInfo;
            Object localObject2;
            int i;
            Object localObject3;
            InputMethodManager localInputMethodManager;
            InputMethodSubtype localInputMethodSubtype;
            String str3;
            String str2;
            synchronized (this.mSpellCheckerMap)
            {
                String str1 = Settings.Secure.getString(this.mContext.getContentResolver(), "selected_spell_checker_subtype");
                localSpellCheckerInfo = getCurrentSpellChecker(null);
                if ((localSpellCheckerInfo == null) || (localSpellCheckerInfo.getSubtypeCount() == 0))
                {
                    localObject2 = null;
                }
                else if (!TextUtils.isEmpty(str1))
                {
                    i = Integer.valueOf(str1).intValue();
                    if ((i != 0) || (paramBoolean))
                        continue;
                    localObject2 = null;
                }
            }
            Object localObject4 = null;
            int j = 0;
            continue;
            j++;
        }
    }

    public SpellCheckerInfo[] getEnabledSpellCheckers()
    {
        return (SpellCheckerInfo[])this.mSpellCheckerList.toArray(new SpellCheckerInfo[this.mSpellCheckerList.size()]);
    }

    // ERROR //
    public void getSpellCheckerService(String paramString1, String paramString2, ITextServicesSessionListener paramITextServicesSessionListener, ISpellCheckerSessionListener paramISpellCheckerSessionListener, Bundle paramBundle)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 63	com/android/server/TextServicesManagerService:mSystemReady	Z
        //     4: ifne +4 -> 8
        //     7: return
        //     8: aload_1
        //     9: invokestatic 257	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
        //     12: ifne +12 -> 24
        //     15: aload_3
        //     16: ifnull +8 -> 24
        //     19: aload 4
        //     21: ifnonnull +16 -> 37
        //     24: getstatic 45	com/android/server/TextServicesManagerService:TAG	Ljava/lang/String;
        //     27: ldc_w 567
        //     30: invokestatic 313	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     33: pop
        //     34: goto -27 -> 7
        //     37: aload_0
        //     38: getfield 54	com/android/server/TextServicesManagerService:mSpellCheckerMap	Ljava/util/HashMap;
        //     41: astore 7
        //     43: aload 7
        //     45: monitorenter
        //     46: aload_0
        //     47: getfield 54	com/android/server/TextServicesManagerService:mSpellCheckerMap	Ljava/util/HashMap;
        //     50: aload_1
        //     51: invokevirtual 260	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
        //     54: ifne +17 -> 71
        //     57: aload 7
        //     59: monitorexit
        //     60: goto -53 -> 7
        //     63: astore 8
        //     65: aload 7
        //     67: monitorexit
        //     68: aload 8
        //     70: athrow
        //     71: aload_0
        //     72: getfield 54	com/android/server/TextServicesManagerService:mSpellCheckerMap	Ljava/util/HashMap;
        //     75: aload_1
        //     76: invokevirtual 502	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     79: checkcast 88	android/view/textservice/SpellCheckerInfo
        //     82: astore 9
        //     84: invokestatic 337	android/os/Binder:getCallingUid	()I
        //     87: istore 10
        //     89: aload_0
        //     90: getfield 61	com/android/server/TextServicesManagerService:mSpellCheckerBindGroups	Ljava/util/HashMap;
        //     93: aload_1
        //     94: invokevirtual 260	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
        //     97: ifeq +93 -> 190
        //     100: aload_0
        //     101: getfield 61	com/android/server/TextServicesManagerService:mSpellCheckerBindGroups	Ljava/util/HashMap;
        //     104: aload_1
        //     105: invokevirtual 502	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     108: checkcast 14	com/android/server/TextServicesManagerService$SpellCheckerBindGroup
        //     111: astore 14
        //     113: aload 14
        //     115: ifnull +75 -> 190
        //     118: aload_0
        //     119: getfield 61	com/android/server/TextServicesManagerService:mSpellCheckerBindGroups	Ljava/util/HashMap;
        //     122: aload_1
        //     123: invokevirtual 502	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     126: checkcast 14	com/android/server/TextServicesManagerService$SpellCheckerBindGroup
        //     129: aload_3
        //     130: aload_2
        //     131: aload 4
        //     133: iload 10
        //     135: aload 5
        //     137: invokevirtual 571	com/android/server/TextServicesManagerService$SpellCheckerBindGroup:addListener	(Lcom/android/internal/textservice/ITextServicesSessionListener;Ljava/lang/String;Lcom/android/internal/textservice/ISpellCheckerSessionListener;ILandroid/os/Bundle;)Lcom/android/server/TextServicesManagerService$InternalDeathRecipient;
        //     140: astore 15
        //     142: aload 15
        //     144: ifnonnull +9 -> 153
        //     147: aload 7
        //     149: monitorexit
        //     150: goto -143 -> 7
        //     153: aload 14
        //     155: getfield 428	com/android/server/TextServicesManagerService$SpellCheckerBindGroup:mSpellChecker	Lcom/android/internal/textservice/ISpellCheckerService;
        //     158: ifnonnull +173 -> 331
        //     161: iconst_1
        //     162: istore 16
        //     164: iload 16
        //     166: aload 14
        //     168: getfield 440	com/android/server/TextServicesManagerService$SpellCheckerBindGroup:mConnected	Z
        //     171: iand
        //     172: ifeq +48 -> 220
        //     175: getstatic 45	com/android/server/TextServicesManagerService:TAG	Ljava/lang/String;
        //     178: ldc_w 573
        //     181: invokestatic 313	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     184: pop
        //     185: aload 14
        //     187: invokevirtual 576	com/android/server/TextServicesManagerService$SpellCheckerBindGroup:removeAll	()V
        //     190: invokestatic 235	android/os/Binder:clearCallingIdentity	()J
        //     193: lstore 11
        //     195: aload_0
        //     196: aload 9
        //     198: aload_2
        //     199: aload_3
        //     200: aload 4
        //     202: iload 10
        //     204: aload 5
        //     206: invokespecial 578	com/android/server/TextServicesManagerService:startSpellCheckerServiceInnerLocked	(Landroid/view/textservice/SpellCheckerInfo;Ljava/lang/String;Lcom/android/internal/textservice/ITextServicesSessionListener;Lcom/android/internal/textservice/ISpellCheckerSessionListener;ILandroid/os/Bundle;)V
        //     209: lload 11
        //     211: invokestatic 251	android/os/Binder:restoreCallingIdentity	(J)V
        //     214: aload 7
        //     216: monitorexit
        //     217: goto -210 -> 7
        //     220: aload 14
        //     222: getfield 428	com/android/server/TextServicesManagerService$SpellCheckerBindGroup:mSpellChecker	Lcom/android/internal/textservice/ISpellCheckerService;
        //     225: astore 17
        //     227: aload 17
        //     229: ifnull -39 -> 190
        //     232: aload 14
        //     234: getfield 428	com/android/server/TextServicesManagerService$SpellCheckerBindGroup:mSpellChecker	Lcom/android/internal/textservice/ISpellCheckerService;
        //     237: aload 15
        //     239: getfield 475	com/android/server/TextServicesManagerService$InternalDeathRecipient:mScLocale	Ljava/lang/String;
        //     242: aload 15
        //     244: getfield 464	com/android/server/TextServicesManagerService$InternalDeathRecipient:mScListener	Lcom/android/internal/textservice/ISpellCheckerSessionListener;
        //     247: aload 5
        //     249: invokeinterface 584 4 0
        //     254: astore 20
        //     256: aload 20
        //     258: ifnull +17 -> 275
        //     261: aload_3
        //     262: aload 20
        //     264: invokeinterface 590 2 0
        //     269: aload 7
        //     271: monitorexit
        //     272: goto -265 -> 7
        //     275: aload 14
        //     277: invokevirtual 576	com/android/server/TextServicesManagerService$SpellCheckerBindGroup:removeAll	()V
        //     280: goto -90 -> 190
        //     283: astore 18
        //     285: getstatic 45	com/android/server/TextServicesManagerService:TAG	Ljava/lang/String;
        //     288: new 177	java/lang/StringBuilder
        //     291: dup
        //     292: invokespecial 178	java/lang/StringBuilder:<init>	()V
        //     295: ldc_w 592
        //     298: invokevirtual 184	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     301: aload 18
        //     303: invokevirtual 187	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     306: invokevirtual 192	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     309: invokestatic 313	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     312: pop
        //     313: aload 14
        //     315: invokevirtual 576	com/android/server/TextServicesManagerService$SpellCheckerBindGroup:removeAll	()V
        //     318: goto -128 -> 190
        //     321: astore 13
        //     323: lload 11
        //     325: invokestatic 251	android/os/Binder:restoreCallingIdentity	(J)V
        //     328: aload 13
        //     330: athrow
        //     331: iconst_0
        //     332: istore 16
        //     334: goto -170 -> 164
        //
        // Exception table:
        //     from	to	target	type
        //     46	68	63	finally
        //     71	195	63	finally
        //     209	227	63	finally
        //     232	269	63	finally
        //     269	272	63	finally
        //     275	280	63	finally
        //     285	331	63	finally
        //     232	269	283	android/os/RemoteException
        //     275	280	283	android/os/RemoteException
        //     195	209	321	finally
    }

    public boolean isSpellCheckerEnabled()
    {
        synchronized (this.mSpellCheckerMap)
        {
            boolean bool = isSpellCheckerEnabledLocked();
            return bool;
        }
    }

    // ERROR //
    public void setCurrentSpellChecker(String paramString1, String paramString2)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 54	com/android/server/TextServicesManagerService:mSpellCheckerMap	Ljava/util/HashMap;
        //     4: astore_3
        //     5: aload_3
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 65	com/android/server/TextServicesManagerService:mContext	Landroid/content/Context;
        //     11: ldc_w 598
        //     14: invokevirtual 324	android/content/Context:checkCallingOrSelfPermission	(Ljava/lang/String;)I
        //     17: ifeq +21 -> 38
        //     20: new 600	java/lang/SecurityException
        //     23: dup
        //     24: ldc_w 602
        //     27: invokespecial 603	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     30: athrow
        //     31: astore 4
        //     33: aload_3
        //     34: monitorexit
        //     35: aload 4
        //     37: athrow
        //     38: aload_0
        //     39: aload_2
        //     40: invokespecial 95	com/android/server/TextServicesManagerService:setCurrentSpellCheckerLocked	(Ljava/lang/String;)V
        //     43: aload_3
        //     44: monitorexit
        //     45: return
        //
        // Exception table:
        //     from	to	target	type
        //     7	35	31	finally
        //     38	45	31	finally
    }

    // ERROR //
    public void setCurrentSpellCheckerSubtype(String paramString, int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 54	com/android/server/TextServicesManagerService:mSpellCheckerMap	Ljava/util/HashMap;
        //     4: astore_3
        //     5: aload_3
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 65	com/android/server/TextServicesManagerService:mContext	Landroid/content/Context;
        //     11: ldc_w 598
        //     14: invokevirtual 324	android/content/Context:checkCallingOrSelfPermission	(Ljava/lang/String;)I
        //     17: ifeq +21 -> 38
        //     20: new 600	java/lang/SecurityException
        //     23: dup
        //     24: ldc_w 602
        //     27: invokespecial 603	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     30: athrow
        //     31: astore 4
        //     33: aload_3
        //     34: monitorexit
        //     35: aload 4
        //     37: athrow
        //     38: aload_0
        //     39: iload_2
        //     40: invokespecial 270	com/android/server/TextServicesManagerService:setCurrentSpellCheckerSubtypeLocked	(I)V
        //     43: aload_3
        //     44: monitorexit
        //     45: return
        //
        // Exception table:
        //     from	to	target	type
        //     7	35	31	finally
        //     38	45	31	finally
    }

    // ERROR //
    public void setSpellCheckerEnabled(boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 54	com/android/server/TextServicesManagerService:mSpellCheckerMap	Ljava/util/HashMap;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 65	com/android/server/TextServicesManagerService:mContext	Landroid/content/Context;
        //     11: ldc_w 598
        //     14: invokevirtual 324	android/content/Context:checkCallingOrSelfPermission	(Ljava/lang/String;)I
        //     17: ifeq +19 -> 36
        //     20: new 600	java/lang/SecurityException
        //     23: dup
        //     24: ldc_w 602
        //     27: invokespecial 603	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     30: athrow
        //     31: astore_3
        //     32: aload_2
        //     33: monitorexit
        //     34: aload_3
        //     35: athrow
        //     36: aload_0
        //     37: iload_1
        //     38: invokespecial 608	com/android/server/TextServicesManagerService:setSpellCheckerEnabledLocked	(Z)V
        //     41: aload_2
        //     42: monitorexit
        //     43: return
        //
        // Exception table:
        //     from	to	target	type
        //     7	34	31	finally
        //     36	43	31	finally
    }

    public void systemReady()
    {
        if (!this.mSystemReady)
            this.mSystemReady = true;
    }

    private class InternalDeathRecipient
        implements IBinder.DeathRecipient
    {
        public final Bundle mBundle;
        private final TextServicesManagerService.SpellCheckerBindGroup mGroup;
        public final ISpellCheckerSessionListener mScListener;
        public final String mScLocale;
        public final ITextServicesSessionListener mTsListener;
        public final int mUid;

        public InternalDeathRecipient(TextServicesManagerService.SpellCheckerBindGroup paramITextServicesSessionListener, ITextServicesSessionListener paramString, String paramISpellCheckerSessionListener, ISpellCheckerSessionListener paramInt, int paramBundle, Bundle arg7)
        {
            this.mTsListener = paramString;
            this.mScListener = paramInt;
            this.mScLocale = paramISpellCheckerSessionListener;
            this.mGroup = paramITextServicesSessionListener;
            this.mUid = paramBundle;
            Object localObject;
            this.mBundle = localObject;
        }

        public void binderDied()
        {
            this.mGroup.removeListener(this.mScListener);
        }

        public boolean hasSpellCheckerListener(ISpellCheckerSessionListener paramISpellCheckerSessionListener)
        {
            return paramISpellCheckerSessionListener.asBinder().equals(this.mScListener.asBinder());
        }
    }

    private class InternalServiceConnection
        implements ServiceConnection
    {
        private final Bundle mBundle;
        private final String mLocale;
        private final String mSciId;

        public InternalServiceConnection(String paramString1, String paramBundle, Bundle arg4)
        {
            this.mSciId = paramString1;
            this.mLocale = paramBundle;
            Object localObject;
            this.mBundle = localObject;
        }

        public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
        {
            synchronized (TextServicesManagerService.this.mSpellCheckerMap)
            {
                ISpellCheckerService localISpellCheckerService = ISpellCheckerService.Stub.asInterface(paramIBinder);
                TextServicesManagerService.SpellCheckerBindGroup localSpellCheckerBindGroup = (TextServicesManagerService.SpellCheckerBindGroup)TextServicesManagerService.this.mSpellCheckerBindGroups.get(this.mSciId);
                if ((localSpellCheckerBindGroup != null) && (this == TextServicesManagerService.SpellCheckerBindGroup.access$700(localSpellCheckerBindGroup)))
                    localSpellCheckerBindGroup.onServiceConnected(localISpellCheckerService);
                return;
            }
        }

        public void onServiceDisconnected(ComponentName paramComponentName)
        {
            synchronized (TextServicesManagerService.this.mSpellCheckerMap)
            {
                TextServicesManagerService.SpellCheckerBindGroup localSpellCheckerBindGroup = (TextServicesManagerService.SpellCheckerBindGroup)TextServicesManagerService.this.mSpellCheckerBindGroups.get(this.mSciId);
                if ((localSpellCheckerBindGroup != null) && (this == TextServicesManagerService.SpellCheckerBindGroup.access$700(localSpellCheckerBindGroup)))
                    TextServicesManagerService.this.mSpellCheckerBindGroups.remove(this.mSciId);
                return;
            }
        }
    }

    private class SpellCheckerBindGroup
    {
        private final String TAG = SpellCheckerBindGroup.class.getSimpleName();
        public boolean mBound;
        public boolean mConnected;
        private final TextServicesManagerService.InternalServiceConnection mInternalConnection;
        private final CopyOnWriteArrayList<TextServicesManagerService.InternalDeathRecipient> mListeners = new CopyOnWriteArrayList();
        public ISpellCheckerService mSpellChecker;

        public SpellCheckerBindGroup(TextServicesManagerService.InternalServiceConnection paramITextServicesSessionListener, ITextServicesSessionListener paramString, String paramISpellCheckerSessionListener, ISpellCheckerSessionListener paramInt, int paramBundle, Bundle arg7)
        {
            this.mInternalConnection = paramITextServicesSessionListener;
            this.mBound = true;
            this.mConnected = false;
            Bundle localBundle;
            addListener(paramString, paramISpellCheckerSessionListener, paramInt, paramBundle, localBundle);
        }

        private void cleanLocked()
        {
            if ((this.mBound) && (this.mListeners.isEmpty()))
            {
                this.mBound = false;
                String str = this.mInternalConnection.mSciId;
                if ((SpellCheckerBindGroup)TextServicesManagerService.this.mSpellCheckerBindGroups.get(str) == this)
                    TextServicesManagerService.this.mSpellCheckerBindGroups.remove(str);
                TextServicesManagerService.this.mContext.unbindService(this.mInternalConnection);
            }
        }

        // ERROR //
        public TextServicesManagerService.InternalDeathRecipient addListener(ITextServicesSessionListener paramITextServicesSessionListener, String paramString, ISpellCheckerSessionListener paramISpellCheckerSessionListener, int paramInt, Bundle paramBundle)
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 25	com/android/server/TextServicesManagerService$SpellCheckerBindGroup:this$0	Lcom/android/server/TextServicesManagerService;
            //     4: invokestatic 94	com/android/server/TextServicesManagerService:access$100	(Lcom/android/server/TextServicesManagerService;)Ljava/util/HashMap;
            //     7: astore 6
            //     9: aload 6
            //     11: monitorenter
            //     12: aload_0
            //     13: getfield 41	com/android/server/TextServicesManagerService$SpellCheckerBindGroup:mListeners	Ljava/util/concurrent/CopyOnWriteArrayList;
            //     16: invokevirtual 98	java/util/concurrent/CopyOnWriteArrayList:size	()I
            //     19: istore 11
            //     21: iconst_0
            //     22: istore 12
            //     24: iload 12
            //     26: iload 11
            //     28: if_icmpge +41 -> 69
            //     31: aload_0
            //     32: getfield 41	com/android/server/TextServicesManagerService$SpellCheckerBindGroup:mListeners	Ljava/util/concurrent/CopyOnWriteArrayList;
            //     35: iload 12
            //     37: invokevirtual 101	java/util/concurrent/CopyOnWriteArrayList:get	(I)Ljava/lang/Object;
            //     40: checkcast 103	com/android/server/TextServicesManagerService$InternalDeathRecipient
            //     43: aload_3
            //     44: invokevirtual 107	com/android/server/TextServicesManagerService$InternalDeathRecipient:hasSpellCheckerListener	(Lcom/android/internal/textservice/ISpellCheckerSessionListener;)Z
            //     47: istore 15
            //     49: iload 15
            //     51: ifeq +12 -> 63
            //     54: aload 6
            //     56: monitorexit
            //     57: aconst_null
            //     58: astore 10
            //     60: aload 10
            //     62: areturn
            //     63: iinc 12 1
            //     66: goto -42 -> 24
            //     69: new 103	com/android/server/TextServicesManagerService$InternalDeathRecipient
            //     72: dup
            //     73: aload_0
            //     74: getfield 25	com/android/server/TextServicesManagerService$SpellCheckerBindGroup:this$0	Lcom/android/server/TextServicesManagerService;
            //     77: aload_0
            //     78: aload_1
            //     79: aload_2
            //     80: aload_3
            //     81: iload 4
            //     83: aload 5
            //     85: invokespecial 110	com/android/server/TextServicesManagerService$InternalDeathRecipient:<init>	(Lcom/android/server/TextServicesManagerService;Lcom/android/server/TextServicesManagerService$SpellCheckerBindGroup;Lcom/android/internal/textservice/ITextServicesSessionListener;Ljava/lang/String;Lcom/android/internal/textservice/ISpellCheckerSessionListener;ILandroid/os/Bundle;)V
            //     88: astore 8
            //     90: aload_3
            //     91: invokeinterface 116 1 0
            //     96: aload 8
            //     98: iconst_0
            //     99: invokeinterface 122 3 0
            //     104: aload_0
            //     105: getfield 41	com/android/server/TextServicesManagerService$SpellCheckerBindGroup:mListeners	Ljava/util/concurrent/CopyOnWriteArrayList;
            //     108: aload 8
            //     110: invokevirtual 126	java/util/concurrent/CopyOnWriteArrayList:add	(Ljava/lang/Object;)Z
            //     113: pop
            //     114: aload_0
            //     115: invokespecial 128	com/android/server/TextServicesManagerService$SpellCheckerBindGroup:cleanLocked	()V
            //     118: aload 6
            //     120: monitorexit
            //     121: aload 8
            //     123: astore 10
            //     125: goto -65 -> 60
            //     128: aload 6
            //     130: monitorexit
            //     131: aload 9
            //     133: athrow
            //     134: astore 9
            //     136: goto -8 -> 128
            //     139: astore 7
            //     141: aconst_null
            //     142: astore 8
            //     144: goto -30 -> 114
            //     147: astore 13
            //     149: goto -35 -> 114
            //     152: astore 9
            //     154: goto -26 -> 128
            //
            // Exception table:
            //     from	to	target	type
            //     90	114	134	finally
            //     114	131	134	finally
            //     12	49	139	android/os/RemoteException
            //     69	90	139	android/os/RemoteException
            //     90	114	147	android/os/RemoteException
            //     12	49	152	finally
            //     54	57	152	finally
            //     69	90	152	finally
        }

        public void onServiceConnected(ISpellCheckerService paramISpellCheckerService)
        {
            Iterator localIterator = this.mListeners.iterator();
            if (localIterator.hasNext())
            {
                TextServicesManagerService.InternalDeathRecipient localInternalDeathRecipient = (TextServicesManagerService.InternalDeathRecipient)localIterator.next();
                try
                {
                    while (true)
                    {
                        ISpellCheckerSession localISpellCheckerSession = paramISpellCheckerService.getISpellCheckerSession(localInternalDeathRecipient.mScLocale, localInternalDeathRecipient.mScListener, localInternalDeathRecipient.mBundle);
                        synchronized (TextServicesManagerService.this.mSpellCheckerMap)
                        {
                            if (this.mListeners.contains(localInternalDeathRecipient))
                                localInternalDeathRecipient.mTsListener.onServiceConnected(localISpellCheckerSession);
                        }
                    }
                }
                catch (RemoteException localRemoteException)
                {
                    Slog.e(this.TAG, "Exception in getting the spell checker session.Reconnect to the spellchecker. ", localRemoteException);
                    removeAll();
                }
            }
            while (true)
            {
                return;
                synchronized (TextServicesManagerService.this.mSpellCheckerMap)
                {
                    this.mSpellChecker = paramISpellCheckerService;
                    this.mConnected = true;
                }
            }
        }

        public void removeAll()
        {
            Slog.e(this.TAG, "Remove the spell checker bind unexpectedly.");
            synchronized (TextServicesManagerService.this.mSpellCheckerMap)
            {
                int i = this.mListeners.size();
                for (int j = 0; j < i; j++)
                {
                    TextServicesManagerService.InternalDeathRecipient localInternalDeathRecipient = (TextServicesManagerService.InternalDeathRecipient)this.mListeners.get(j);
                    localInternalDeathRecipient.mScListener.asBinder().unlinkToDeath(localInternalDeathRecipient, 0);
                }
                this.mListeners.clear();
                cleanLocked();
                return;
            }
        }

        public void removeListener(ISpellCheckerSessionListener paramISpellCheckerSessionListener)
        {
            while (true)
            {
                int j;
                synchronized (TextServicesManagerService.this.mSpellCheckerMap)
                {
                    int i = this.mListeners.size();
                    ArrayList localArrayList = new ArrayList();
                    j = 0;
                    if (j < i)
                    {
                        TextServicesManagerService.InternalDeathRecipient localInternalDeathRecipient2 = (TextServicesManagerService.InternalDeathRecipient)this.mListeners.get(j);
                        if (localInternalDeathRecipient2.hasSpellCheckerListener(paramISpellCheckerSessionListener))
                            localArrayList.add(localInternalDeathRecipient2);
                    }
                    else
                    {
                        int k = localArrayList.size();
                        int m = 0;
                        if (m < k)
                        {
                            TextServicesManagerService.InternalDeathRecipient localInternalDeathRecipient1 = (TextServicesManagerService.InternalDeathRecipient)localArrayList.get(m);
                            localInternalDeathRecipient1.mScListener.asBinder().unlinkToDeath(localInternalDeathRecipient1, 0);
                            this.mListeners.remove(localInternalDeathRecipient1);
                            m++;
                            continue;
                        }
                        cleanLocked();
                        return;
                    }
                }
                j++;
            }
        }
    }

    private class TextServicesMonitor extends PackageMonitor
    {
        private TextServicesMonitor()
        {
        }

        public void onSomePackagesChanged()
        {
            synchronized (TextServicesManagerService.this.mSpellCheckerMap)
            {
                TextServicesManagerService.buildSpellCheckerMapLocked(TextServicesManagerService.this.mContext, TextServicesManagerService.this.mSpellCheckerList, TextServicesManagerService.this.mSpellCheckerMap);
                SpellCheckerInfo localSpellCheckerInfo1 = TextServicesManagerService.this.getCurrentSpellChecker(null);
                if (localSpellCheckerInfo1 != null)
                {
                    String str = localSpellCheckerInfo1.getPackageName();
                    int i = isPackageDisappearing(str);
                    if ((i == 3) || (i == 2) || (isPackageModified(str)))
                    {
                        SpellCheckerInfo localSpellCheckerInfo2 = TextServicesManagerService.this.findAvailSpellCheckerLocked(null, str);
                        if (localSpellCheckerInfo2 != null)
                            TextServicesManagerService.this.setCurrentSpellCheckerLocked(localSpellCheckerInfo2.getId());
                    }
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.TextServicesManagerService
 * JD-Core Version:        0.6.2
 */