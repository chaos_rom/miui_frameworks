package com.android.server;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.AlarmManager;
import android.app.IActivityManager;
import android.app.IBackupAgent;
import android.app.IBackupAgent.Stub;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.app.backup.BackupDataOutput;
import android.app.backup.FullBackup;
import android.app.backup.IBackupManager;
import android.app.backup.IBackupManager.Stub;
import android.app.backup.IFullBackupRestoreObserver;
import android.app.backup.IRestoreObserver;
import android.app.backup.IRestoreSession.Stub;
import android.app.backup.RestoreSet;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageDataObserver.Stub;
import android.content.pm.IPackageDeleteObserver.Stub;
import android.content.pm.IPackageInstallObserver.Stub;
import android.content.pm.IPackageManager;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Binder;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.WorkSource;
import android.os.storage.IMountService;
import android.provider.Settings.Secure;
import android.util.EventLog;
import android.util.Slog;
import android.util.SparseArray;
import android.util.StringBuilderPrinter;
import com.android.internal.backup.IBackupTransport;
import com.android.internal.backup.IBackupTransport.Stub;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.zip.DeflaterOutputStream;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import miui.provider.ExtraSettings.Secure;

class BackupManagerService extends IBackupManager.Stub
{
    static final String BACKUP_FILE_HEADER_MAGIC = "ANDROID BACKUP\n";
    static final int BACKUP_FILE_VERSION = 1;
    private static final long BACKUP_INTERVAL = 3600000L;
    static final String BACKUP_MANIFEST_FILENAME = "_manifest";
    static final int BACKUP_MANIFEST_VERSION = 1;
    static final boolean COMPRESS_FULL_BACKUPS = true;
    static final int CURRENT_ANCESTRAL_RECORD_VERSION = 1;
    private static final boolean DEBUG = true;
    static final boolean DEBUG_BACKUP_TRACE = true;
    static final String ENCRYPTION_ALGORITHM_NAME = "AES-256";
    private static final long FIRST_BACKUP_INTERVAL = 43200000L;
    private static final int FUZZ_MILLIS = 300000;
    static final String INIT_SENTINEL_FILE_NAME = "_need_init_";
    private static final boolean MORE_DEBUG = false;
    static final int MSG_BACKUP_RESTORE_STEP = 20;
    private static final int MSG_FULL_CONFIRMATION_TIMEOUT = 9;
    static final int MSG_OP_COMPLETE = 21;
    private static final int MSG_RESTORE_TIMEOUT = 8;
    private static final int MSG_RUN_BACKUP = 1;
    private static final int MSG_RUN_CLEAR = 4;
    private static final int MSG_RUN_FULL_BACKUP = 2;
    private static final int MSG_RUN_FULL_RESTORE = 10;
    private static final int MSG_RUN_GET_RESTORE_SETS = 6;
    private static final int MSG_RUN_INITIALIZE = 5;
    private static final int MSG_RUN_RESTORE = 3;
    private static final int MSG_TIMEOUT = 7;
    static final int OP_ACKNOWLEDGED = 1;
    static final int OP_PENDING = 0;
    static final int OP_TIMEOUT = -1;
    static final String PACKAGE_MANAGER_SENTINEL = "@pm@";
    static final int PBKDF2_HASH_ROUNDS = 10000;
    static final int PBKDF2_KEY_SIZE = 256;
    static final int PBKDF2_SALT_SIZE = 512;
    private static final String RUN_BACKUP_ACTION = "android.app.backup.intent.RUN";
    private static final String RUN_CLEAR_ACTION = "android.app.backup.intent.CLEAR";
    private static final String RUN_INITIALIZE_ACTION = "android.app.backup.intent.INIT";
    static final String SHARED_BACKUP_AGENT_PACKAGE = "com.android.sharedstoragebackup";
    private static final String TAG = "BackupManagerService";
    static final long TIMEOUT_BACKUP_INTERVAL = 30000L;
    static final long TIMEOUT_FULL_BACKUP_INTERVAL = 300000L;
    static final long TIMEOUT_FULL_CONFIRMATION = 60000L;
    static final long TIMEOUT_INTERVAL = 10000L;
    static final long TIMEOUT_RESTORE_INTERVAL = 60000L;
    static final long TIMEOUT_SHARED_BACKUP_INTERVAL = 1800000L;
    ActiveRestoreSession mActiveRestoreSession;
    private IActivityManager mActivityManager;
    final Object mAgentConnectLock;
    private AlarmManager mAlarmManager;
    Set<String> mAncestralPackages;
    long mAncestralToken;
    boolean mAutoRestore;
    BackupHandler mBackupHandler;
    IBackupManager mBackupManagerBinder;
    final SparseArray<HashSet<String>> mBackupParticipants;
    volatile boolean mBackupRunning;
    final List<String> mBackupTrace;
    File mBaseStateDir;
    BroadcastReceiver mBroadcastReceiver;
    final Object mClearDataLock;
    volatile boolean mClearingData;
    IBackupAgent mConnectedAgent;
    volatile boolean mConnecting;
    private Context mContext;
    final Object mCurrentOpLock;
    final SparseArray<Operation> mCurrentOperations;
    long mCurrentToken;
    String mCurrentTransport;
    File mDataDir;
    boolean mEnabled;
    private File mEverStored;
    HashSet<String> mEverStoredApps;
    final SparseArray<FullParams> mFullConfirmations;
    ServiceConnection mGoogleConnection;
    IBackupTransport mGoogleTransport;
    HandlerThread mHandlerThread;
    File mJournal;
    File mJournalDir;
    volatile long mLastBackupPass;
    IBackupTransport mLocalTransport;
    private IMountService mMountService;
    volatile long mNextBackupPass;
    private PackageManager mPackageManager;
    IPackageManager mPackageManagerBinder;
    private String mPasswordHash;
    private File mPasswordHashFile;
    private byte[] mPasswordSalt;
    HashMap<String, BackupRequest> mPendingBackups;
    HashSet<String> mPendingInits;
    private PowerManager mPowerManager;
    boolean mProvisioned;
    ContentObserver mProvisionedObserver;
    final Object mQueueLock;
    private final SecureRandom mRng;
    PendingIntent mRunBackupIntent;
    BroadcastReceiver mRunBackupReceiver;
    PendingIntent mRunInitIntent;
    BroadcastReceiver mRunInitReceiver;
    File mTokenFile;
    final Random mTokenGenerator;
    final HashMap<String, IBackupTransport> mTransports;
    PowerManager.WakeLock mWakelock;

    // ERROR //
    public BackupManagerService(Context paramContext)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 292	android/app/backup/IBackupManager$Stub:<init>	()V
        //     4: aload_0
        //     5: new 294	android/util/SparseArray
        //     8: dup
        //     9: invokespecial 295	android/util/SparseArray:<init>	()V
        //     12: putfield 297	com/android/server/BackupManagerService:mBackupParticipants	Landroid/util/SparseArray;
        //     15: aload_0
        //     16: new 299	java/util/HashMap
        //     19: dup
        //     20: invokespecial 300	java/util/HashMap:<init>	()V
        //     23: putfield 302	com/android/server/BackupManagerService:mPendingBackups	Ljava/util/HashMap;
        //     26: aload_0
        //     27: new 304	java/lang/Object
        //     30: dup
        //     31: invokespecial 305	java/lang/Object:<init>	()V
        //     34: putfield 307	com/android/server/BackupManagerService:mQueueLock	Ljava/lang/Object;
        //     37: aload_0
        //     38: new 304	java/lang/Object
        //     41: dup
        //     42: invokespecial 305	java/lang/Object:<init>	()V
        //     45: putfield 309	com/android/server/BackupManagerService:mAgentConnectLock	Ljava/lang/Object;
        //     48: aload_0
        //     49: new 311	java/util/ArrayList
        //     52: dup
        //     53: invokespecial 312	java/util/ArrayList:<init>	()V
        //     56: putfield 314	com/android/server/BackupManagerService:mBackupTrace	Ljava/util/List;
        //     59: aload_0
        //     60: new 304	java/lang/Object
        //     63: dup
        //     64: invokespecial 305	java/lang/Object:<init>	()V
        //     67: putfield 316	com/android/server/BackupManagerService:mClearDataLock	Ljava/lang/Object;
        //     70: aload_0
        //     71: new 299	java/util/HashMap
        //     74: dup
        //     75: invokespecial 300	java/util/HashMap:<init>	()V
        //     78: putfield 318	com/android/server/BackupManagerService:mTransports	Ljava/util/HashMap;
        //     81: aload_0
        //     82: new 294	android/util/SparseArray
        //     85: dup
        //     86: invokespecial 295	android/util/SparseArray:<init>	()V
        //     89: putfield 320	com/android/server/BackupManagerService:mCurrentOperations	Landroid/util/SparseArray;
        //     92: aload_0
        //     93: new 304	java/lang/Object
        //     96: dup
        //     97: invokespecial 305	java/lang/Object:<init>	()V
        //     100: putfield 322	com/android/server/BackupManagerService:mCurrentOpLock	Ljava/lang/Object;
        //     103: aload_0
        //     104: new 324	java/util/Random
        //     107: dup
        //     108: invokespecial 325	java/util/Random:<init>	()V
        //     111: putfield 327	com/android/server/BackupManagerService:mTokenGenerator	Ljava/util/Random;
        //     114: aload_0
        //     115: new 294	android/util/SparseArray
        //     118: dup
        //     119: invokespecial 295	android/util/SparseArray:<init>	()V
        //     122: putfield 329	com/android/server/BackupManagerService:mFullConfirmations	Landroid/util/SparseArray;
        //     125: aload_0
        //     126: new 331	java/security/SecureRandom
        //     129: dup
        //     130: invokespecial 332	java/security/SecureRandom:<init>	()V
        //     133: putfield 334	com/android/server/BackupManagerService:mRng	Ljava/security/SecureRandom;
        //     136: aload_0
        //     137: new 336	java/util/HashSet
        //     140: dup
        //     141: invokespecial 337	java/util/HashSet:<init>	()V
        //     144: putfield 339	com/android/server/BackupManagerService:mEverStoredApps	Ljava/util/HashSet;
        //     147: aload_0
        //     148: aconst_null
        //     149: putfield 341	com/android/server/BackupManagerService:mAncestralPackages	Ljava/util/Set;
        //     152: aload_0
        //     153: lconst_0
        //     154: putfield 343	com/android/server/BackupManagerService:mAncestralToken	J
        //     157: aload_0
        //     158: lconst_0
        //     159: putfield 345	com/android/server/BackupManagerService:mCurrentToken	J
        //     162: aload_0
        //     163: new 336	java/util/HashSet
        //     166: dup
        //     167: invokespecial 337	java/util/HashSet:<init>	()V
        //     170: putfield 347	com/android/server/BackupManagerService:mPendingInits	Ljava/util/HashSet;
        //     173: new 6	com/android/server/BackupManagerService$1
        //     176: dup
        //     177: aload_0
        //     178: invokespecial 350	com/android/server/BackupManagerService$1:<init>	(Lcom/android/server/BackupManagerService;)V
        //     181: astore_2
        //     182: aload_0
        //     183: aload_2
        //     184: putfield 352	com/android/server/BackupManagerService:mBroadcastReceiver	Landroid/content/BroadcastReceiver;
        //     187: new 8	com/android/server/BackupManagerService$2
        //     190: dup
        //     191: aload_0
        //     192: invokespecial 353	com/android/server/BackupManagerService$2:<init>	(Lcom/android/server/BackupManagerService;)V
        //     195: astore_3
        //     196: aload_0
        //     197: aload_3
        //     198: putfield 355	com/android/server/BackupManagerService:mGoogleConnection	Landroid/content/ServiceConnection;
        //     201: aload_0
        //     202: aload_1
        //     203: putfield 357	com/android/server/BackupManagerService:mContext	Landroid/content/Context;
        //     206: aload_0
        //     207: aload_1
        //     208: invokevirtual 363	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
        //     211: putfield 365	com/android/server/BackupManagerService:mPackageManager	Landroid/content/pm/PackageManager;
        //     214: aload_0
        //     215: invokestatic 370	android/app/AppGlobals:getPackageManager	()Landroid/content/pm/IPackageManager;
        //     218: putfield 372	com/android/server/BackupManagerService:mPackageManagerBinder	Landroid/content/pm/IPackageManager;
        //     221: aload_0
        //     222: invokestatic 378	android/app/ActivityManagerNative:getDefault	()Landroid/app/IActivityManager;
        //     225: putfield 380	com/android/server/BackupManagerService:mActivityManager	Landroid/app/IActivityManager;
        //     228: aload_0
        //     229: aload_1
        //     230: ldc_w 382
        //     233: invokevirtual 386	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
        //     236: checkcast 388	android/app/AlarmManager
        //     239: putfield 390	com/android/server/BackupManagerService:mAlarmManager	Landroid/app/AlarmManager;
        //     242: aload_0
        //     243: aload_1
        //     244: ldc_w 392
        //     247: invokevirtual 386	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
        //     250: checkcast 394	android/os/PowerManager
        //     253: putfield 396	com/android/server/BackupManagerService:mPowerManager	Landroid/os/PowerManager;
        //     256: aload_0
        //     257: ldc_w 398
        //     260: invokestatic 404	android/os/ServiceManager:getService	(Ljava/lang/String;)Landroid/os/IBinder;
        //     263: invokestatic 410	android/os/storage/IMountService$Stub:asInterface	(Landroid/os/IBinder;)Landroid/os/storage/IMountService;
        //     266: putfield 412	com/android/server/BackupManagerService:mMountService	Landroid/os/storage/IMountService;
        //     269: aload_0
        //     270: aload_0
        //     271: invokevirtual 416	com/android/server/BackupManagerService:asBinder	()Landroid/os/IBinder;
        //     274: invokestatic 419	com/android/server/BackupManagerService:asInterface	(Landroid/os/IBinder;)Landroid/app/backup/IBackupManager;
        //     277: putfield 421	com/android/server/BackupManagerService:mBackupManagerBinder	Landroid/app/backup/IBackupManager;
        //     280: aload_0
        //     281: new 423	android/os/HandlerThread
        //     284: dup
        //     285: ldc_w 425
        //     288: bipush 10
        //     290: invokespecial 428	android/os/HandlerThread:<init>	(Ljava/lang/String;I)V
        //     293: putfield 430	com/android/server/BackupManagerService:mHandlerThread	Landroid/os/HandlerThread;
        //     296: aload_0
        //     297: getfield 430	com/android/server/BackupManagerService:mHandlerThread	Landroid/os/HandlerThread;
        //     300: invokevirtual 433	android/os/HandlerThread:start	()V
        //     303: new 59	com/android/server/BackupManagerService$BackupHandler
        //     306: dup
        //     307: aload_0
        //     308: aload_0
        //     309: getfield 430	com/android/server/BackupManagerService:mHandlerThread	Landroid/os/HandlerThread;
        //     312: invokevirtual 437	android/os/HandlerThread:getLooper	()Landroid/os/Looper;
        //     315: invokespecial 440	com/android/server/BackupManagerService$BackupHandler:<init>	(Lcom/android/server/BackupManagerService;Landroid/os/Looper;)V
        //     318: astore 4
        //     320: aload_0
        //     321: aload 4
        //     323: putfield 442	com/android/server/BackupManagerService:mBackupHandler	Lcom/android/server/BackupManagerService$BackupHandler;
        //     326: aload_1
        //     327: invokevirtual 446	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
        //     330: astore 5
        //     332: aload 5
        //     334: ldc_w 448
        //     337: iconst_0
        //     338: invokestatic 454	android/provider/Settings$Secure:getInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)I
        //     341: ifeq +649 -> 990
        //     344: iconst_1
        //     345: istore 6
        //     347: aload 5
        //     349: ldc_w 456
        //     352: iconst_0
        //     353: invokestatic 454	android/provider/Settings$Secure:getInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)I
        //     356: ifeq +640 -> 996
        //     359: iconst_1
        //     360: istore 7
        //     362: aload_0
        //     363: iload 7
        //     365: putfield 458	com/android/server/BackupManagerService:mProvisioned	Z
        //     368: aload 5
        //     370: ldc_w 460
        //     373: iconst_1
        //     374: invokestatic 454	android/provider/Settings$Secure:getInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)I
        //     377: ifeq +625 -> 1002
        //     380: iconst_1
        //     381: istore 8
        //     383: aload_0
        //     384: iload 8
        //     386: putfield 462	com/android/server/BackupManagerService:mAutoRestore	Z
        //     389: new 83	com/android/server/BackupManagerService$ProvisionedObserver
        //     392: dup
        //     393: aload_0
        //     394: aload_0
        //     395: getfield 442	com/android/server/BackupManagerService:mBackupHandler	Lcom/android/server/BackupManagerService$BackupHandler;
        //     398: invokespecial 465	com/android/server/BackupManagerService$ProvisionedObserver:<init>	(Lcom/android/server/BackupManagerService;Landroid/os/Handler;)V
        //     401: astore 9
        //     403: aload_0
        //     404: aload 9
        //     406: putfield 467	com/android/server/BackupManagerService:mProvisionedObserver	Landroid/database/ContentObserver;
        //     409: aload 5
        //     411: ldc_w 456
        //     414: invokestatic 471	android/provider/Settings$Secure:getUriFor	(Ljava/lang/String;)Landroid/net/Uri;
        //     417: iconst_0
        //     418: aload_0
        //     419: getfield 467	com/android/server/BackupManagerService:mProvisionedObserver	Landroid/database/ContentObserver;
        //     422: invokevirtual 477	android/content/ContentResolver:registerContentObserver	(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
        //     425: aload_0
        //     426: new 479	java/io/File
        //     429: dup
        //     430: invokestatic 485	android/os/Environment:getSecureDataDirectory	()Ljava/io/File;
        //     433: ldc_w 425
        //     436: invokespecial 488	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     439: putfield 490	com/android/server/BackupManagerService:mBaseStateDir	Ljava/io/File;
        //     442: aload_0
        //     443: getfield 490	com/android/server/BackupManagerService:mBaseStateDir	Ljava/io/File;
        //     446: invokevirtual 494	java/io/File:mkdirs	()Z
        //     449: pop
        //     450: aload_0
        //     451: invokestatic 497	android/os/Environment:getDownloadCacheDirectory	()Ljava/io/File;
        //     454: putfield 499	com/android/server/BackupManagerService:mDataDir	Ljava/io/File;
        //     457: aload_0
        //     458: new 479	java/io/File
        //     461: dup
        //     462: aload_0
        //     463: getfield 490	com/android/server/BackupManagerService:mBaseStateDir	Ljava/io/File;
        //     466: ldc_w 501
        //     469: invokespecial 488	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     472: putfield 503	com/android/server/BackupManagerService:mPasswordHashFile	Ljava/io/File;
        //     475: aload_0
        //     476: getfield 503	com/android/server/BackupManagerService:mPasswordHashFile	Ljava/io/File;
        //     479: invokevirtual 506	java/io/File:exists	()Z
        //     482: ifeq +95 -> 577
        //     485: aconst_null
        //     486: astore 33
        //     488: aconst_null
        //     489: astore 34
        //     491: new 508	java/io/FileInputStream
        //     494: dup
        //     495: aload_0
        //     496: getfield 503	com/android/server/BackupManagerService:mPasswordHashFile	Ljava/io/File;
        //     499: invokespecial 511	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //     502: astore 35
        //     504: new 513	java/io/BufferedInputStream
        //     507: dup
        //     508: aload 35
        //     510: invokespecial 516	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
        //     513: astore 36
        //     515: new 518	java/io/DataInputStream
        //     518: dup
        //     519: aload 36
        //     521: invokespecial 519	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
        //     524: astore 37
        //     526: aload 37
        //     528: invokevirtual 523	java/io/DataInputStream:readInt	()I
        //     531: newarray byte
        //     533: astore 47
        //     535: aload 37
        //     537: aload 47
        //     539: invokevirtual 527	java/io/DataInputStream:readFully	([B)V
        //     542: aload_0
        //     543: aload 37
        //     545: invokevirtual 531	java/io/DataInputStream:readUTF	()Ljava/lang/String;
        //     548: putfield 533	com/android/server/BackupManagerService:mPasswordHash	Ljava/lang/String;
        //     551: aload_0
        //     552: aload 47
        //     554: putfield 535	com/android/server/BackupManagerService:mPasswordSalt	[B
        //     557: aload 37
        //     559: ifnull +8 -> 567
        //     562: aload 37
        //     564: invokevirtual 538	java/io/DataInputStream:close	()V
        //     567: aload 35
        //     569: ifnull +8 -> 577
        //     572: aload 35
        //     574: invokevirtual 539	java/io/FileInputStream:close	()V
        //     577: new 56	com/android/server/BackupManagerService$RunBackupReceiver
        //     580: dup
        //     581: aload_0
        //     582: aconst_null
        //     583: invokespecial 542	com/android/server/BackupManagerService$RunBackupReceiver:<init>	(Lcom/android/server/BackupManagerService;Lcom/android/server/BackupManagerService$1;)V
        //     586: astore 11
        //     588: aload_0
        //     589: aload 11
        //     591: putfield 544	com/android/server/BackupManagerService:mRunBackupReceiver	Landroid/content/BroadcastReceiver;
        //     594: new 546	android/content/IntentFilter
        //     597: dup
        //     598: invokespecial 547	android/content/IntentFilter:<init>	()V
        //     601: astore 12
        //     603: aload 12
        //     605: ldc 162
        //     607: invokevirtual 551	android/content/IntentFilter:addAction	(Ljava/lang/String;)V
        //     610: aload_1
        //     611: aload_0
        //     612: getfield 544	com/android/server/BackupManagerService:mRunBackupReceiver	Landroid/content/BroadcastReceiver;
        //     615: aload 12
        //     617: ldc_w 553
        //     620: aconst_null
        //     621: invokevirtual 557	android/content/Context:registerReceiver	(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;
        //     624: pop
        //     625: new 53	com/android/server/BackupManagerService$RunInitializeReceiver
        //     628: dup
        //     629: aload_0
        //     630: aconst_null
        //     631: invokespecial 558	com/android/server/BackupManagerService$RunInitializeReceiver:<init>	(Lcom/android/server/BackupManagerService;Lcom/android/server/BackupManagerService$1;)V
        //     634: astore 14
        //     636: aload_0
        //     637: aload 14
        //     639: putfield 560	com/android/server/BackupManagerService:mRunInitReceiver	Landroid/content/BroadcastReceiver;
        //     642: new 546	android/content/IntentFilter
        //     645: dup
        //     646: invokespecial 547	android/content/IntentFilter:<init>	()V
        //     649: astore 15
        //     651: aload 15
        //     653: ldc 168
        //     655: invokevirtual 551	android/content/IntentFilter:addAction	(Ljava/lang/String;)V
        //     658: aload_1
        //     659: aload_0
        //     660: getfield 560	com/android/server/BackupManagerService:mRunInitReceiver	Landroid/content/BroadcastReceiver;
        //     663: aload 15
        //     665: ldc_w 553
        //     668: aconst_null
        //     669: invokevirtual 557	android/content/Context:registerReceiver	(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;
        //     672: pop
        //     673: new 562	android/content/Intent
        //     676: dup
        //     677: ldc 162
        //     679: invokespecial 564	android/content/Intent:<init>	(Ljava/lang/String;)V
        //     682: astore 17
        //     684: aload 17
        //     686: ldc_w 565
        //     689: invokevirtual 569	android/content/Intent:addFlags	(I)Landroid/content/Intent;
        //     692: pop
        //     693: aload_0
        //     694: aload_1
        //     695: iconst_1
        //     696: aload 17
        //     698: iconst_0
        //     699: invokestatic 575	android/app/PendingIntent:getBroadcast	(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
        //     702: putfield 577	com/android/server/BackupManagerService:mRunBackupIntent	Landroid/app/PendingIntent;
        //     705: new 562	android/content/Intent
        //     708: dup
        //     709: ldc 168
        //     711: invokespecial 564	android/content/Intent:<init>	(Ljava/lang/String;)V
        //     714: astore 19
        //     716: aload 17
        //     718: ldc_w 565
        //     721: invokevirtual 569	android/content/Intent:addFlags	(I)Landroid/content/Intent;
        //     724: pop
        //     725: aload_0
        //     726: aload_1
        //     727: iconst_5
        //     728: aload 19
        //     730: iconst_0
        //     731: invokestatic 575	android/app/PendingIntent:getBroadcast	(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
        //     734: putfield 579	com/android/server/BackupManagerService:mRunInitIntent	Landroid/app/PendingIntent;
        //     737: aload_0
        //     738: new 479	java/io/File
        //     741: dup
        //     742: aload_0
        //     743: getfield 490	com/android/server/BackupManagerService:mBaseStateDir	Ljava/io/File;
        //     746: ldc_w 581
        //     749: invokespecial 488	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     752: putfield 583	com/android/server/BackupManagerService:mJournalDir	Ljava/io/File;
        //     755: aload_0
        //     756: getfield 583	com/android/server/BackupManagerService:mJournalDir	Ljava/io/File;
        //     759: invokevirtual 494	java/io/File:mkdirs	()Z
        //     762: pop
        //     763: aload_0
        //     764: aconst_null
        //     765: putfield 585	com/android/server/BackupManagerService:mJournal	Ljava/io/File;
        //     768: aload_0
        //     769: invokespecial 588	com/android/server/BackupManagerService:initPackageTracking	()V
        //     772: aload_0
        //     773: getfield 297	com/android/server/BackupManagerService:mBackupParticipants	Landroid/util/SparseArray;
        //     776: astore 22
        //     778: aload 22
        //     780: monitorenter
        //     781: aload_0
        //     782: aconst_null
        //     783: invokevirtual 592	com/android/server/BackupManagerService:addPackageParticipantsLocked	([Ljava/lang/String;)V
        //     786: aload 22
        //     788: monitorexit
        //     789: new 594	com/android/internal/backup/LocalTransport
        //     792: dup
        //     793: aload_1
        //     794: invokespecial 596	com/android/internal/backup/LocalTransport:<init>	(Landroid/content/Context;)V
        //     797: astore 24
        //     799: aload_0
        //     800: aload 24
        //     802: putfield 598	com/android/server/BackupManagerService:mLocalTransport	Lcom/android/internal/backup/IBackupTransport;
        //     805: aload_0
        //     806: new 600	android/content/ComponentName
        //     809: dup
        //     810: aload_1
        //     811: ldc_w 594
        //     814: invokespecial 603	android/content/ComponentName:<init>	(Landroid/content/Context;Ljava/lang/Class;)V
        //     817: invokevirtual 606	android/content/ComponentName:flattenToShortString	()Ljava/lang/String;
        //     820: aload_0
        //     821: getfield 598	com/android/server/BackupManagerService:mLocalTransport	Lcom/android/internal/backup/IBackupTransport;
        //     824: invokespecial 610	com/android/server/BackupManagerService:registerTransport	(Ljava/lang/String;Lcom/android/internal/backup/IBackupTransport;)V
        //     827: aload_0
        //     828: aconst_null
        //     829: putfield 612	com/android/server/BackupManagerService:mGoogleTransport	Lcom/android/internal/backup/IBackupTransport;
        //     832: aload_0
        //     833: aload_1
        //     834: invokevirtual 446	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
        //     837: ldc_w 614
        //     840: invokestatic 618	android/provider/Settings$Secure:getString	(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
        //     843: putfield 620	com/android/server/BackupManagerService:mCurrentTransport	Ljava/lang/String;
        //     846: ldc_w 622
        //     849: aload_0
        //     850: getfield 620	com/android/server/BackupManagerService:mCurrentTransport	Ljava/lang/String;
        //     853: invokevirtual 628	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     856: ifeq +8 -> 864
        //     859: aload_0
        //     860: aconst_null
        //     861: putfield 620	com/android/server/BackupManagerService:mCurrentTransport	Ljava/lang/String;
        //     864: ldc 174
        //     866: new 630	java/lang/StringBuilder
        //     869: dup
        //     870: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     873: ldc_w 633
        //     876: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     879: aload_0
        //     880: getfield 620	com/android/server/BackupManagerService:mCurrentTransport	Ljava/lang/String;
        //     883: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     886: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     889: invokestatic 646	android/util/Slog:v	(Ljava/lang/String;Ljava/lang/String;)I
        //     892: pop
        //     893: new 600	android/content/ComponentName
        //     896: dup
        //     897: ldc_w 648
        //     900: ldc_w 650
        //     903: invokespecial 653	android/content/ComponentName:<init>	(Ljava/lang/String;Ljava/lang/String;)V
        //     906: astore 26
        //     908: aload_0
        //     909: getfield 365	com/android/server/BackupManagerService:mPackageManager	Landroid/content/pm/PackageManager;
        //     912: aload 26
        //     914: invokevirtual 656	android/content/ComponentName:getPackageName	()Ljava/lang/String;
        //     917: iconst_0
        //     918: invokevirtual 662	android/content/pm/PackageManager:getApplicationInfo	(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
        //     921: astore 29
        //     923: iconst_1
        //     924: aload 29
        //     926: getfield 667	android/content/pm/ApplicationInfo:flags	I
        //     929: iand
        //     930: ifeq +167 -> 1097
        //     933: ldc 174
        //     935: ldc_w 669
        //     938: invokestatic 646	android/util/Slog:v	(Ljava/lang/String;Ljava/lang/String;)I
        //     941: pop
        //     942: aload_1
        //     943: new 562	android/content/Intent
        //     946: dup
        //     947: invokespecial 670	android/content/Intent:<init>	()V
        //     950: aload 26
        //     952: invokevirtual 674	android/content/Intent:setComponent	(Landroid/content/ComponentName;)Landroid/content/Intent;
        //     955: aload_0
        //     956: getfield 355	com/android/server/BackupManagerService:mGoogleConnection	Landroid/content/ServiceConnection;
        //     959: iconst_1
        //     960: invokevirtual 678	android/content/Context:bindService	(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
        //     963: pop
        //     964: aload_0
        //     965: invokespecial 681	com/android/server/BackupManagerService:parseLeftoverJournals	()V
        //     968: aload_0
        //     969: aload_0
        //     970: getfield 396	com/android/server/BackupManagerService:mPowerManager	Landroid/os/PowerManager;
        //     973: iconst_1
        //     974: ldc_w 683
        //     977: invokevirtual 687	android/os/PowerManager:newWakeLock	(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;
        //     980: putfield 689	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
        //     983: aload_0
        //     984: iload 6
        //     986: invokevirtual 693	com/android/server/BackupManagerService:setBackupEnabled	(Z)V
        //     989: return
        //     990: iconst_0
        //     991: istore 6
        //     993: goto -646 -> 347
        //     996: iconst_0
        //     997: istore 7
        //     999: goto -637 -> 362
        //     1002: iconst_0
        //     1003: istore 8
        //     1005: goto -622 -> 383
        //     1008: astore 50
        //     1010: ldc 174
        //     1012: ldc_w 695
        //     1015: invokestatic 698	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     1018: pop
        //     1019: aload 34
        //     1021: ifnull +8 -> 1029
        //     1024: aload 34
        //     1026: invokevirtual 538	java/io/DataInputStream:close	()V
        //     1029: aload 33
        //     1031: ifnull -454 -> 577
        //     1034: aload 33
        //     1036: invokevirtual 539	java/io/FileInputStream:close	()V
        //     1039: goto -462 -> 577
        //     1042: astore 43
        //     1044: ldc 174
        //     1046: astore 44
        //     1048: ldc_w 700
        //     1051: astore 45
        //     1053: aload 44
        //     1055: aload 45
        //     1057: invokestatic 703	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     1060: pop
        //     1061: goto -484 -> 577
        //     1064: astore 39
        //     1066: aload 34
        //     1068: ifnull +8 -> 1076
        //     1071: aload 34
        //     1073: invokevirtual 538	java/io/DataInputStream:close	()V
        //     1076: aload 33
        //     1078: ifnull +8 -> 1086
        //     1081: aload 33
        //     1083: invokevirtual 539	java/io/FileInputStream:close	()V
        //     1086: aload 39
        //     1088: athrow
        //     1089: astore 23
        //     1091: aload 22
        //     1093: monitorexit
        //     1094: aload 23
        //     1096: athrow
        //     1097: ldc 174
        //     1099: new 630	java/lang/StringBuilder
        //     1102: dup
        //     1103: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     1106: ldc_w 705
        //     1109: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1112: aload 29
        //     1114: invokevirtual 708	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     1117: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1120: invokestatic 703	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     1123: pop
        //     1124: goto -160 -> 964
        //     1127: astore 27
        //     1129: ldc 174
        //     1131: ldc_w 710
        //     1134: invokestatic 646	android/util/Slog:v	(Ljava/lang/String;Ljava/lang/String;)I
        //     1137: pop
        //     1138: goto -174 -> 964
        //     1141: astore 40
        //     1143: ldc 174
        //     1145: ldc_w 700
        //     1148: invokestatic 703	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     1151: pop
        //     1152: goto -66 -> 1086
        //     1155: astore 48
        //     1157: ldc 174
        //     1159: astore 44
        //     1161: ldc_w 700
        //     1164: astore 45
        //     1166: goto -113 -> 1053
        //     1169: astore 39
        //     1171: aload 35
        //     1173: astore 33
        //     1175: goto -109 -> 1066
        //     1178: astore 39
        //     1180: aload 37
        //     1182: astore 34
        //     1184: aload 35
        //     1186: astore 33
        //     1188: goto -122 -> 1066
        //     1191: astore 49
        //     1193: aload 35
        //     1195: astore 33
        //     1197: goto -187 -> 1010
        //     1200: astore 38
        //     1202: aload 37
        //     1204: astore 34
        //     1206: aload 35
        //     1208: astore 33
        //     1210: goto -200 -> 1010
        //
        // Exception table:
        //     from	to	target	type
        //     491	504	1008	java/io/IOException
        //     1024	1039	1042	java/io/IOException
        //     491	504	1064	finally
        //     1010	1019	1064	finally
        //     781	789	1089	finally
        //     1091	1094	1089	finally
        //     908	964	1127	android/content/pm/PackageManager$NameNotFoundException
        //     1097	1124	1127	android/content/pm/PackageManager$NameNotFoundException
        //     1071	1086	1141	java/io/IOException
        //     562	577	1155	java/io/IOException
        //     504	526	1169	finally
        //     526	557	1178	finally
        //     504	526	1191	java/io/IOException
        //     526	557	1200	java/io/IOException
    }

    private void addPackageParticipantsLockedInner(String paramString, List<PackageInfo> paramList)
    {
        Iterator localIterator = paramList.iterator();
        while (localIterator.hasNext())
        {
            PackageInfo localPackageInfo = (PackageInfo)localIterator.next();
            if ((paramString == null) || (localPackageInfo.packageName.equals(paramString)))
            {
                int i = localPackageInfo.applicationInfo.uid;
                HashSet localHashSet = (HashSet)this.mBackupParticipants.get(i);
                if (localHashSet == null)
                {
                    localHashSet = new HashSet();
                    this.mBackupParticipants.put(i, localHashSet);
                }
                localHashSet.add(localPackageInfo.packageName);
                if (!this.mEverStoredApps.contains(localPackageInfo.packageName))
                {
                    Slog.i("BackupManagerService", "New app " + localPackageInfo.packageName + " never backed up; scheduling");
                    dataChangedImpl(localPackageInfo.packageName);
                }
            }
        }
    }

    private SecretKey buildCharArrayKey(char[] paramArrayOfChar, byte[] paramArrayOfByte, int paramInt)
    {
        try
        {
            SecretKey localSecretKey2 = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1").generateSecret(new PBEKeySpec(paramArrayOfChar, paramArrayOfByte, paramInt, 256));
            localSecretKey1 = localSecretKey2;
            return localSecretKey1;
        }
        catch (InvalidKeySpecException localInvalidKeySpecException)
        {
            while (true)
            {
                Slog.e("BackupManagerService", "Invalid key spec for PBKDF2!");
                SecretKey localSecretKey1 = null;
            }
        }
        catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
        {
            while (true)
                Slog.e("BackupManagerService", "PBKDF2 unavailable!");
        }
    }

    private String buildPasswordHash(String paramString, byte[] paramArrayOfByte, int paramInt)
    {
        SecretKey localSecretKey = buildPasswordKey(paramString, paramArrayOfByte, paramInt);
        if (localSecretKey != null);
        for (String str = byteArrayToHex(localSecretKey.getEncoded()); ; str = null)
            return str;
    }

    private SecretKey buildPasswordKey(String paramString, byte[] paramArrayOfByte, int paramInt)
    {
        return buildCharArrayKey(paramString.toCharArray(), paramArrayOfByte, paramInt);
    }

    private String byteArrayToHex(byte[] paramArrayOfByte)
    {
        StringBuilder localStringBuilder = new StringBuilder(2 * paramArrayOfByte.length);
        for (int i = 0; i < paramArrayOfByte.length; i++)
            localStringBuilder.append(Byte.toHexString(paramArrayOfByte[i], true));
        return localStringBuilder.toString();
    }

    private void dataChangedImpl(String paramString)
    {
        dataChangedImpl(paramString, dataChangedTargets(paramString));
    }

    private void dataChangedImpl(String paramString, HashSet<String> paramHashSet)
    {
        EventLog.writeEvent(2820, paramString);
        if (paramHashSet == null)
            Slog.w("BackupManagerService", "dataChanged but no participant pkg='" + paramString + "'" + " uid=" + Binder.getCallingUid());
        while (true)
        {
            return;
            synchronized (this.mQueueLock)
            {
                if (paramHashSet.contains(paramString))
                {
                    BackupRequest localBackupRequest = new BackupRequest(paramString);
                    if (this.mPendingBackups.put(paramString, localBackupRequest) == null)
                    {
                        Slog.d("BackupManagerService", "Now staging backup of " + paramString);
                        writeToJournalLocked(paramString);
                    }
                }
            }
        }
    }

    private HashSet<String> dataChangedTargets(String paramString)
    {
        Object localObject2;
        if (this.mContext.checkPermission("android.permission.BACKUP", Binder.getCallingPid(), Binder.getCallingUid()) == -1)
            synchronized (this.mBackupParticipants)
            {
                localObject2 = (HashSet)this.mBackupParticipants.get(Binder.getCallingUid());
            }
        HashSet localHashSet1 = new HashSet();
        while (true)
        {
            int j;
            synchronized (this.mBackupParticipants)
            {
                int i = this.mBackupParticipants.size();
                j = 0;
                if (j < i)
                {
                    HashSet localHashSet2 = (HashSet)this.mBackupParticipants.valueAt(j);
                    if (localHashSet2 != null)
                        localHashSet1.addAll(localHashSet2);
                }
                else
                {
                    localObject2 = localHashSet1;
                }
            }
            j++;
        }
    }

    // ERROR //
    private void dumpInternal(PrintWriter paramPrintWriter)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 307	com/android/server/BackupManagerService:mQueueLock	Ljava/lang/Object;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: new 630	java/lang/StringBuilder
        //     10: dup
        //     11: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     14: ldc_w 940
        //     17: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     20: astore 4
        //     22: aload_0
        //     23: getfield 942	com/android/server/BackupManagerService:mEnabled	Z
        //     26: ifeq +462 -> 488
        //     29: ldc_w 944
        //     32: astore 5
        //     34: aload 4
        //     36: aload 5
        //     38: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     41: ldc_w 946
        //     44: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     47: astore 6
        //     49: aload_0
        //     50: getfield 458	com/android/server/BackupManagerService:mProvisioned	Z
        //     53: ifne +1107 -> 1160
        //     56: ldc_w 948
        //     59: astore 7
        //     61: aload 6
        //     63: aload 7
        //     65: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     68: ldc_w 950
        //     71: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     74: astore 8
        //     76: aload_0
        //     77: getfield 347	com/android/server/BackupManagerService:mPendingInits	Ljava/util/HashSet;
        //     80: invokevirtual 951	java/util/HashSet:size	()I
        //     83: ifne +1085 -> 1168
        //     86: ldc_w 948
        //     89: astore 9
        //     91: aload_1
        //     92: aload 8
        //     94: aload 9
        //     96: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     99: ldc_w 953
        //     102: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     105: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     108: invokevirtual 958	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     111: new 630	java/lang/StringBuilder
        //     114: dup
        //     115: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     118: ldc_w 960
        //     121: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     124: astore 10
        //     126: aload_0
        //     127: getfield 462	com/android/server/BackupManagerService:mAutoRestore	Z
        //     130: ifeq +1046 -> 1176
        //     133: ldc_w 944
        //     136: astore 11
        //     138: aload_1
        //     139: aload 10
        //     141: aload 11
        //     143: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     146: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     149: invokevirtual 958	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     152: aload_0
        //     153: getfield 962	com/android/server/BackupManagerService:mBackupRunning	Z
        //     156: ifeq +10 -> 166
        //     159: aload_1
        //     160: ldc_w 964
        //     163: invokevirtual 958	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     166: aload_1
        //     167: new 630	java/lang/StringBuilder
        //     170: dup
        //     171: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     174: ldc_w 966
        //     177: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     180: aload_0
        //     181: getfield 968	com/android/server/BackupManagerService:mLastBackupPass	J
        //     184: invokevirtual 971	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     187: ldc_w 973
        //     190: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     193: invokestatic 979	java/lang/System:currentTimeMillis	()J
        //     196: invokevirtual 971	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     199: bipush 41
        //     201: invokevirtual 982	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
        //     204: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     207: invokevirtual 958	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     210: aload_1
        //     211: new 630	java/lang/StringBuilder
        //     214: dup
        //     215: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     218: ldc_w 984
        //     221: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     224: aload_0
        //     225: getfield 986	com/android/server/BackupManagerService:mNextBackupPass	J
        //     228: invokevirtual 971	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     231: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     234: invokevirtual 958	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     237: aload_1
        //     238: ldc_w 988
        //     241: invokevirtual 958	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     244: aload_0
        //     245: invokevirtual 992	com/android/server/BackupManagerService:listAllTransports	()[Ljava/lang/String;
        //     248: astore 12
        //     250: aload 12
        //     252: arraylength
        //     253: istore 13
        //     255: iconst_0
        //     256: istore 14
        //     258: iload 14
        //     260: iload 13
        //     262: if_icmpge +275 -> 537
        //     265: aload 12
        //     267: iload 14
        //     269: aaload
        //     270: astore 34
        //     272: new 630	java/lang/StringBuilder
        //     275: dup
        //     276: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     279: astore 35
        //     281: aload 34
        //     283: aload_0
        //     284: getfield 620	com/android/server/BackupManagerService:mCurrentTransport	Ljava/lang/String;
        //     287: invokevirtual 628	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     290: ifeq +894 -> 1184
        //     293: ldc_w 994
        //     296: astore 36
        //     298: aload_1
        //     299: aload 35
        //     301: aload 36
        //     303: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     306: aload 34
        //     308: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     311: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     314: invokevirtual 958	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     317: aload_0
        //     318: aload 34
        //     320: invokespecial 722	com/android/server/BackupManagerService:getTransport	(Ljava/lang/String;)Lcom/android/internal/backup/IBackupTransport;
        //     323: astore 39
        //     325: new 479	java/io/File
        //     328: dup
        //     329: aload_0
        //     330: getfield 490	com/android/server/BackupManagerService:mBaseStateDir	Ljava/io/File;
        //     333: aload 39
        //     335: invokeinterface 999 1 0
        //     340: invokespecial 488	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     343: astore 40
        //     345: aload_1
        //     346: new 630	java/lang/StringBuilder
        //     349: dup
        //     350: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     353: ldc_w 1001
        //     356: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     359: aload 39
        //     361: invokeinterface 1004 1 0
        //     366: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     369: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     372: invokevirtual 958	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     375: aload_1
        //     376: new 630	java/lang/StringBuilder
        //     379: dup
        //     380: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     383: ldc_w 1006
        //     386: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     389: aload 39
        //     391: invokeinterface 1010 1 0
        //     396: invokevirtual 708	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     399: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     402: invokevirtual 958	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     405: aload 40
        //     407: invokevirtual 1014	java/io/File:listFiles	()[Ljava/io/File;
        //     410: astore 41
        //     412: aload 41
        //     414: arraylength
        //     415: istore 42
        //     417: iconst_0
        //     418: istore 43
        //     420: iload 43
        //     422: iload 42
        //     424: if_icmpge +768 -> 1192
        //     427: aload 41
        //     429: iload 43
        //     431: aaload
        //     432: astore 44
        //     434: aload_1
        //     435: new 630	java/lang/StringBuilder
        //     438: dup
        //     439: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     442: ldc_w 1016
        //     445: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     448: aload 44
        //     450: invokevirtual 1019	java/io/File:getName	()Ljava/lang/String;
        //     453: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     456: ldc_w 1021
        //     459: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     462: aload 44
        //     464: invokevirtual 1024	java/io/File:length	()J
        //     467: invokevirtual 971	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     470: ldc_w 1026
        //     473: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     476: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     479: invokevirtual 958	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     482: iinc 43 1
        //     485: goto -65 -> 420
        //     488: ldc_w 1028
        //     491: astore 5
        //     493: goto -459 -> 34
        //     496: astore 37
        //     498: ldc 174
        //     500: ldc_w 1030
        //     503: aload 37
        //     505: invokestatic 1033	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     508: pop
        //     509: aload_1
        //     510: new 630	java/lang/StringBuilder
        //     513: dup
        //     514: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     517: ldc_w 1035
        //     520: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     523: aload 37
        //     525: invokevirtual 708	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     528: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     531: invokevirtual 958	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     534: goto +658 -> 1192
        //     537: aload_1
        //     538: new 630	java/lang/StringBuilder
        //     541: dup
        //     542: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     545: ldc_w 1037
        //     548: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     551: aload_0
        //     552: getfield 347	com/android/server/BackupManagerService:mPendingInits	Ljava/util/HashSet;
        //     555: invokevirtual 951	java/util/HashSet:size	()I
        //     558: invokevirtual 904	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     561: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     564: invokevirtual 958	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     567: aload_0
        //     568: getfield 347	com/android/server/BackupManagerService:mPendingInits	Ljava/util/HashSet;
        //     571: invokevirtual 1038	java/util/HashSet:iterator	()Ljava/util/Iterator;
        //     574: astore 15
        //     576: aload 15
        //     578: invokeinterface 793 1 0
        //     583: ifeq +48 -> 631
        //     586: aload 15
        //     588: invokeinterface 797 1 0
        //     593: checkcast 624	java/lang/String
        //     596: astore 33
        //     598: aload_1
        //     599: new 630	java/lang/StringBuilder
        //     602: dup
        //     603: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     606: ldc_w 1040
        //     609: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     612: aload 33
        //     614: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     617: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     620: invokevirtual 958	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     623: goto -47 -> 576
        //     626: astore_3
        //     627: aload_2
        //     628: monitorexit
        //     629: aload_3
        //     630: athrow
        //     631: aload_0
        //     632: getfield 314	com/android/server/BackupManagerService:mBackupTrace	Ljava/util/List;
        //     635: astore 16
        //     637: aload 16
        //     639: monitorenter
        //     640: aload_0
        //     641: getfield 314	com/android/server/BackupManagerService:mBackupTrace	Ljava/util/List;
        //     644: invokeinterface 1043 1 0
        //     649: ifne +79 -> 728
        //     652: aload_1
        //     653: ldc_w 1045
        //     656: invokevirtual 958	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     659: aload_0
        //     660: getfield 314	com/android/server/BackupManagerService:mBackupTrace	Ljava/util/List;
        //     663: invokeinterface 788 1 0
        //     668: astore 31
        //     670: aload 31
        //     672: invokeinterface 793 1 0
        //     677: ifeq +51 -> 728
        //     680: aload 31
        //     682: invokeinterface 797 1 0
        //     687: checkcast 624	java/lang/String
        //     690: astore 32
        //     692: aload_1
        //     693: new 630	java/lang/StringBuilder
        //     696: dup
        //     697: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     700: ldc_w 1047
        //     703: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     706: aload 32
        //     708: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     711: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     714: invokevirtual 958	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     717: goto -47 -> 670
        //     720: astore 17
        //     722: aload 16
        //     724: monitorexit
        //     725: aload 17
        //     727: athrow
        //     728: aload 16
        //     730: monitorexit
        //     731: aload_0
        //     732: getfield 297	com/android/server/BackupManagerService:mBackupParticipants	Landroid/util/SparseArray;
        //     735: invokevirtual 927	android/util/SparseArray:size	()I
        //     738: istore 18
        //     740: aload_1
        //     741: ldc_w 1049
        //     744: invokevirtual 958	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     747: iconst_0
        //     748: istore 19
        //     750: iload 19
        //     752: iload 18
        //     754: if_icmpge +94 -> 848
        //     757: aload_0
        //     758: getfield 297	com/android/server/BackupManagerService:mBackupParticipants	Landroid/util/SparseArray;
        //     761: iload 19
        //     763: invokevirtual 1053	android/util/SparseArray:keyAt	(I)I
        //     766: istore 28
        //     768: aload_1
        //     769: ldc_w 1055
        //     772: invokevirtual 1058	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     775: aload_1
        //     776: iload 28
        //     778: invokevirtual 1060	java/io/PrintWriter:println	(I)V
        //     781: aload_0
        //     782: getfield 297	com/android/server/BackupManagerService:mBackupParticipants	Landroid/util/SparseArray;
        //     785: iload 19
        //     787: invokevirtual 930	android/util/SparseArray:valueAt	(I)Ljava/lang/Object;
        //     790: checkcast 336	java/util/HashSet
        //     793: invokevirtual 1038	java/util/HashSet:iterator	()Ljava/util/Iterator;
        //     796: astore 29
        //     798: aload 29
        //     800: invokeinterface 793 1 0
        //     805: ifeq +393 -> 1198
        //     808: aload 29
        //     810: invokeinterface 797 1 0
        //     815: checkcast 624	java/lang/String
        //     818: astore 30
        //     820: aload_1
        //     821: new 630	java/lang/StringBuilder
        //     824: dup
        //     825: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     828: ldc_w 1040
        //     831: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     834: aload 30
        //     836: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     839: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     842: invokevirtual 958	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     845: goto -47 -> 798
        //     848: new 630	java/lang/StringBuilder
        //     851: dup
        //     852: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     855: ldc_w 1062
        //     858: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     861: astore 20
        //     863: aload_0
        //     864: getfield 341	com/android/server/BackupManagerService:mAncestralPackages	Ljava/util/Set;
        //     867: ifnonnull +90 -> 957
        //     870: ldc_w 1064
        //     873: astore 21
        //     875: aload_1
        //     876: aload 20
        //     878: aload 21
        //     880: invokevirtual 708	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     883: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     886: invokevirtual 958	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     889: aload_0
        //     890: getfield 341	com/android/server/BackupManagerService:mAncestralPackages	Ljava/util/Set;
        //     893: ifnull +81 -> 974
        //     896: aload_0
        //     897: getfield 341	com/android/server/BackupManagerService:mAncestralPackages	Ljava/util/Set;
        //     900: invokeinterface 1067 1 0
        //     905: astore 26
        //     907: aload 26
        //     909: invokeinterface 793 1 0
        //     914: ifeq +60 -> 974
        //     917: aload 26
        //     919: invokeinterface 797 1 0
        //     924: checkcast 624	java/lang/String
        //     927: astore 27
        //     929: aload_1
        //     930: new 630	java/lang/StringBuilder
        //     933: dup
        //     934: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     937: ldc_w 1040
        //     940: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     943: aload 27
        //     945: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     948: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     951: invokevirtual 958	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     954: goto -47 -> 907
        //     957: aload_0
        //     958: getfield 341	com/android/server/BackupManagerService:mAncestralPackages	Ljava/util/Set;
        //     961: invokeinterface 1068 1 0
        //     966: invokestatic 1074	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     969: astore 21
        //     971: goto -96 -> 875
        //     974: aload_1
        //     975: new 630	java/lang/StringBuilder
        //     978: dup
        //     979: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     982: ldc_w 1076
        //     985: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     988: aload_0
        //     989: getfield 339	com/android/server/BackupManagerService:mEverStoredApps	Ljava/util/HashSet;
        //     992: invokevirtual 951	java/util/HashSet:size	()I
        //     995: invokevirtual 904	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     998: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1001: invokevirtual 958	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     1004: aload_0
        //     1005: getfield 339	com/android/server/BackupManagerService:mEverStoredApps	Ljava/util/HashSet;
        //     1008: invokevirtual 1038	java/util/HashSet:iterator	()Ljava/util/Iterator;
        //     1011: astore 22
        //     1013: aload 22
        //     1015: invokeinterface 793 1 0
        //     1020: ifeq +43 -> 1063
        //     1023: aload 22
        //     1025: invokeinterface 797 1 0
        //     1030: checkcast 624	java/lang/String
        //     1033: astore 25
        //     1035: aload_1
        //     1036: new 630	java/lang/StringBuilder
        //     1039: dup
        //     1040: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     1043: ldc_w 1040
        //     1046: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1049: aload 25
        //     1051: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1054: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1057: invokevirtual 958	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     1060: goto -47 -> 1013
        //     1063: aload_1
        //     1064: new 630	java/lang/StringBuilder
        //     1067: dup
        //     1068: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     1071: ldc_w 1078
        //     1074: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1077: aload_0
        //     1078: getfield 302	com/android/server/BackupManagerService:mPendingBackups	Ljava/util/HashMap;
        //     1081: invokevirtual 1079	java/util/HashMap:size	()I
        //     1084: invokevirtual 904	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     1087: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1090: invokevirtual 958	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     1093: aload_0
        //     1094: getfield 302	com/android/server/BackupManagerService:mPendingBackups	Ljava/util/HashMap;
        //     1097: invokevirtual 1083	java/util/HashMap:values	()Ljava/util/Collection;
        //     1100: invokeinterface 1086 1 0
        //     1105: astore 23
        //     1107: aload 23
        //     1109: invokeinterface 793 1 0
        //     1114: ifeq +43 -> 1157
        //     1117: aload 23
        //     1119: invokeinterface 797 1 0
        //     1124: checkcast 86	com/android/server/BackupManagerService$BackupRequest
        //     1127: astore 24
        //     1129: aload_1
        //     1130: new 630	java/lang/StringBuilder
        //     1133: dup
        //     1134: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     1137: ldc_w 1040
        //     1140: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1143: aload 24
        //     1145: invokevirtual 708	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     1148: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1151: invokevirtual 958	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     1154: goto -47 -> 1107
        //     1157: aload_2
        //     1158: monitorexit
        //     1159: return
        //     1160: ldc_w 622
        //     1163: astore 7
        //     1165: goto -1104 -> 61
        //     1168: ldc_w 622
        //     1171: astore 9
        //     1173: goto -1082 -> 91
        //     1176: ldc_w 1028
        //     1179: astore 11
        //     1181: goto -1043 -> 138
        //     1184: ldc_w 1040
        //     1187: astore 36
        //     1189: goto -891 -> 298
        //     1192: iinc 14 1
        //     1195: goto -937 -> 258
        //     1198: iinc 19 1
        //     1201: goto -451 -> 750
        //
        // Exception table:
        //     from	to	target	type
        //     317	482	496	java/lang/Exception
        //     7	317	626	finally
        //     317	482	626	finally
        //     488	629	626	finally
        //     631	640	626	finally
        //     725	728	626	finally
        //     731	1159	626	finally
        //     640	725	720	finally
        //     728	731	720	finally
    }

    private IBackupTransport getTransport(String paramString)
    {
        synchronized (this.mTransports)
        {
            IBackupTransport localIBackupTransport = (IBackupTransport)this.mTransports.get(paramString);
            if (localIBackupTransport == null)
                Slog.w("BackupManagerService", "Requested unavailable transport: " + paramString);
            return localIBackupTransport;
        }
    }

    private byte[] hexToByteArray(String paramString)
    {
        int i = paramString.length() / 2;
        if (i * 2 != paramString.length())
            throw new IllegalArgumentException("Hex string must have an even number of digits");
        byte[] arrayOfByte = new byte[i];
        for (int j = 0; j < paramString.length(); j += 2)
            arrayOfByte[(j / 2)] = ((byte)Integer.parseInt(paramString.substring(j, j + 2), 16));
        return arrayOfByte;
    }

    // ERROR //
    private void initPackageTracking()
    {
        // Byte code:
        //     0: ldc 174
        //     2: ldc_w 1112
        //     5: invokestatic 646	android/util/Slog:v	(Ljava/lang/String;Ljava/lang/String;)I
        //     8: pop
        //     9: aload_0
        //     10: new 479	java/io/File
        //     13: dup
        //     14: aload_0
        //     15: getfield 490	com/android/server/BackupManagerService:mBaseStateDir	Ljava/io/File;
        //     18: ldc_w 1114
        //     21: invokespecial 488	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     24: putfield 1116	com/android/server/BackupManagerService:mTokenFile	Ljava/io/File;
        //     27: new 1118	java/io/RandomAccessFile
        //     30: dup
        //     31: aload_0
        //     32: getfield 1116	com/android/server/BackupManagerService:mTokenFile	Ljava/io/File;
        //     35: ldc_w 1120
        //     38: invokespecial 1121	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     41: astore_2
        //     42: aload_2
        //     43: invokevirtual 1122	java/io/RandomAccessFile:readInt	()I
        //     46: iconst_1
        //     47: if_icmpne +75 -> 122
        //     50: aload_0
        //     51: aload_2
        //     52: invokevirtual 1125	java/io/RandomAccessFile:readLong	()J
        //     55: putfield 343	com/android/server/BackupManagerService:mAncestralToken	J
        //     58: aload_0
        //     59: aload_2
        //     60: invokevirtual 1125	java/io/RandomAccessFile:readLong	()J
        //     63: putfield 345	com/android/server/BackupManagerService:mCurrentToken	J
        //     66: aload_2
        //     67: invokevirtual 1122	java/io/RandomAccessFile:readInt	()I
        //     70: istore 33
        //     72: iload 33
        //     74: iflt +48 -> 122
        //     77: aload_0
        //     78: new 336	java/util/HashSet
        //     81: dup
        //     82: invokespecial 337	java/util/HashSet:<init>	()V
        //     85: putfield 341	com/android/server/BackupManagerService:mAncestralPackages	Ljava/util/Set;
        //     88: iconst_0
        //     89: istore 34
        //     91: iload 34
        //     93: iload 33
        //     95: if_icmpge +27 -> 122
        //     98: aload_2
        //     99: invokevirtual 1126	java/io/RandomAccessFile:readUTF	()Ljava/lang/String;
        //     102: astore 35
        //     104: aload_0
        //     105: getfield 341	com/android/server/BackupManagerService:mAncestralPackages	Ljava/util/Set;
        //     108: aload 35
        //     110: invokeinterface 1127 2 0
        //     115: pop
        //     116: iinc 34 1
        //     119: goto -28 -> 91
        //     122: aload_2
        //     123: invokevirtual 1128	java/io/RandomAccessFile:close	()V
        //     126: aload_0
        //     127: new 479	java/io/File
        //     130: dup
        //     131: aload_0
        //     132: getfield 490	com/android/server/BackupManagerService:mBaseStateDir	Ljava/io/File;
        //     135: ldc_w 1130
        //     138: invokespecial 488	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     141: putfield 1132	com/android/server/BackupManagerService:mEverStored	Ljava/io/File;
        //     144: new 479	java/io/File
        //     147: dup
        //     148: aload_0
        //     149: getfield 490	com/android/server/BackupManagerService:mBaseStateDir	Ljava/io/File;
        //     152: ldc_w 1134
        //     155: invokespecial 488	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     158: astore 5
        //     160: aload 5
        //     162: invokevirtual 506	java/io/File:exists	()Z
        //     165: ifeq +9 -> 174
        //     168: aload 5
        //     170: invokevirtual 1137	java/io/File:delete	()Z
        //     173: pop
        //     174: aload_0
        //     175: getfield 1132	com/android/server/BackupManagerService:mEverStored	Ljava/io/File;
        //     178: invokevirtual 506	java/io/File:exists	()Z
        //     181: ifeq +184 -> 365
        //     184: aconst_null
        //     185: astore 10
        //     187: aconst_null
        //     188: astore 11
        //     190: new 1118	java/io/RandomAccessFile
        //     193: dup
        //     194: aload 5
        //     196: ldc_w 1139
        //     199: invokespecial 1121	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     202: astore 12
        //     204: new 1118	java/io/RandomAccessFile
        //     207: dup
        //     208: aload_0
        //     209: getfield 1132	com/android/server/BackupManagerService:mEverStored	Ljava/io/File;
        //     212: ldc_w 1120
        //     215: invokespecial 1121	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     218: astore 13
        //     220: aload 13
        //     222: invokevirtual 1126	java/io/RandomAccessFile:readUTF	()Ljava/lang/String;
        //     225: astore 24
        //     227: aload_0
        //     228: getfield 365	com/android/server/BackupManagerService:mPackageManager	Landroid/content/pm/PackageManager;
        //     231: aload 24
        //     233: iconst_0
        //     234: invokevirtual 1143	android/content/pm/PackageManager:getPackageInfo	(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
        //     237: pop
        //     238: aload_0
        //     239: getfield 339	com/android/server/BackupManagerService:mEverStoredApps	Ljava/util/HashSet;
        //     242: aload 24
        //     244: invokevirtual 820	java/util/HashSet:add	(Ljava/lang/Object;)Z
        //     247: pop
        //     248: aload 12
        //     250: aload 24
        //     252: invokevirtual 1146	java/io/RandomAccessFile:writeUTF	(Ljava/lang/String;)V
        //     255: goto -35 -> 220
        //     258: astore 25
        //     260: goto -40 -> 220
        //     263: astore 31
        //     265: ldc 174
        //     267: ldc_w 1148
        //     270: invokestatic 646	android/util/Slog:v	(Ljava/lang/String;Ljava/lang/String;)I
        //     273: pop
        //     274: goto -148 -> 126
        //     277: astore_3
        //     278: ldc 174
        //     280: ldc_w 1150
        //     283: aload_3
        //     284: invokestatic 1152	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     287: pop
        //     288: goto -162 -> 126
        //     291: astore 29
        //     293: aload 5
        //     295: aload_0
        //     296: getfield 1132	com/android/server/BackupManagerService:mEverStored	Ljava/io/File;
        //     299: invokevirtual 1156	java/io/File:renameTo	(Ljava/io/File;)Z
        //     302: ifne +43 -> 345
        //     305: ldc 174
        //     307: new 630	java/lang/StringBuilder
        //     310: dup
        //     311: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     314: ldc_w 1158
        //     317: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     320: aload 5
        //     322: invokevirtual 708	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     325: ldc_w 1160
        //     328: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     331: aload_0
        //     332: getfield 1132	com/android/server/BackupManagerService:mEverStored	Ljava/io/File;
        //     335: invokevirtual 708	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     338: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     341: invokestatic 698	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     344: pop
        //     345: aload 10
        //     347: ifnull +8 -> 355
        //     350: aload 10
        //     352: invokevirtual 1128	java/io/RandomAccessFile:close	()V
        //     355: aload 11
        //     357: ifnull +8 -> 365
        //     360: aload 11
        //     362: invokevirtual 1128	java/io/RandomAccessFile:close	()V
        //     365: new 546	android/content/IntentFilter
        //     368: dup
        //     369: invokespecial 547	android/content/IntentFilter:<init>	()V
        //     372: astore 6
        //     374: aload 6
        //     376: ldc_w 1162
        //     379: invokevirtual 551	android/content/IntentFilter:addAction	(Ljava/lang/String;)V
        //     382: aload 6
        //     384: ldc_w 1164
        //     387: invokevirtual 551	android/content/IntentFilter:addAction	(Ljava/lang/String;)V
        //     390: aload 6
        //     392: ldc_w 1166
        //     395: invokevirtual 1169	android/content/IntentFilter:addDataScheme	(Ljava/lang/String;)V
        //     398: aload_0
        //     399: getfield 357	com/android/server/BackupManagerService:mContext	Landroid/content/Context;
        //     402: aload_0
        //     403: getfield 352	com/android/server/BackupManagerService:mBroadcastReceiver	Landroid/content/BroadcastReceiver;
        //     406: aload 6
        //     408: invokevirtual 1172	android/content/Context:registerReceiver	(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
        //     411: pop
        //     412: new 546	android/content/IntentFilter
        //     415: dup
        //     416: invokespecial 547	android/content/IntentFilter:<init>	()V
        //     419: astore 8
        //     421: aload 8
        //     423: ldc_w 1174
        //     426: invokevirtual 551	android/content/IntentFilter:addAction	(Ljava/lang/String;)V
        //     429: aload 8
        //     431: ldc_w 1176
        //     434: invokevirtual 551	android/content/IntentFilter:addAction	(Ljava/lang/String;)V
        //     437: aload_0
        //     438: getfield 357	com/android/server/BackupManagerService:mContext	Landroid/content/Context;
        //     441: aload_0
        //     442: getfield 352	com/android/server/BackupManagerService:mBroadcastReceiver	Landroid/content/BroadcastReceiver;
        //     445: aload 8
        //     447: invokevirtual 1172	android/content/Context:registerReceiver	(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
        //     450: pop
        //     451: return
        //     452: astore 21
        //     454: ldc 174
        //     456: ldc_w 1178
        //     459: aload 21
        //     461: invokestatic 1033	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     464: pop
        //     465: aload 10
        //     467: ifnull +8 -> 475
        //     470: aload 10
        //     472: invokevirtual 1128	java/io/RandomAccessFile:close	()V
        //     475: aload 11
        //     477: ifnull -112 -> 365
        //     480: aload 11
        //     482: invokevirtual 1128	java/io/RandomAccessFile:close	()V
        //     485: goto -120 -> 365
        //     488: astore 18
        //     490: goto -125 -> 365
        //     493: astore 15
        //     495: aload 10
        //     497: ifnull +8 -> 505
        //     500: aload 10
        //     502: invokevirtual 1128	java/io/RandomAccessFile:close	()V
        //     505: aload 11
        //     507: ifnull +8 -> 515
        //     510: aload 11
        //     512: invokevirtual 1128	java/io/RandomAccessFile:close	()V
        //     515: aload 15
        //     517: athrow
        //     518: astore 17
        //     520: goto -15 -> 505
        //     523: astore 16
        //     525: goto -10 -> 515
        //     528: astore 19
        //     530: goto -175 -> 355
        //     533: astore 23
        //     535: goto -60 -> 475
        //     538: astore 15
        //     540: aload 12
        //     542: astore 10
        //     544: goto -49 -> 495
        //     547: astore 15
        //     549: aload 13
        //     551: astore 11
        //     553: aload 12
        //     555: astore 10
        //     557: goto -62 -> 495
        //     560: astore 21
        //     562: aload 12
        //     564: astore 10
        //     566: goto -112 -> 454
        //     569: astore 21
        //     571: aload 13
        //     573: astore 11
        //     575: aload 12
        //     577: astore 10
        //     579: goto -125 -> 454
        //     582: astore 28
        //     584: aload 12
        //     586: astore 10
        //     588: goto -295 -> 293
        //     591: astore 14
        //     593: aload 13
        //     595: astore 11
        //     597: aload 12
        //     599: astore 10
        //     601: goto -308 -> 293
        //
        // Exception table:
        //     from	to	target	type
        //     227	255	258	android/content/pm/PackageManager$NameNotFoundException
        //     27	126	263	java/io/FileNotFoundException
        //     27	126	277	java/io/IOException
        //     190	204	291	java/io/EOFException
        //     190	204	452	java/io/IOException
        //     360	365	488	java/io/IOException
        //     480	485	488	java/io/IOException
        //     190	204	493	finally
        //     293	345	493	finally
        //     454	465	493	finally
        //     500	505	518	java/io/IOException
        //     510	515	523	java/io/IOException
        //     350	355	528	java/io/IOException
        //     470	475	533	java/io/IOException
        //     204	220	538	finally
        //     220	227	547	finally
        //     227	255	547	finally
        //     204	220	560	java/io/IOException
        //     220	227	569	java/io/IOException
        //     227	255	569	java/io/IOException
        //     204	220	582	java/io/EOFException
        //     220	227	591	java/io/EOFException
        //     227	255	591	java/io/EOFException
    }

    private byte[] makeKeyChecksum(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, int paramInt)
    {
        char[] arrayOfChar = new char[paramArrayOfByte1.length];
        for (int i = 0; i < paramArrayOfByte1.length; i++)
            arrayOfChar[i] = ((char)paramArrayOfByte1[i]);
        return buildCharArrayKey(arrayOfChar, paramArrayOfByte2, paramInt).getEncoded();
    }

    // ERROR //
    private void parseLeftoverJournals()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 583	com/android/server/BackupManagerService:mJournalDir	Ljava/io/File;
        //     4: invokevirtual 1014	java/io/File:listFiles	()[Ljava/io/File;
        //     7: astore_1
        //     8: aload_1
        //     9: arraylength
        //     10: istore_2
        //     11: iconst_0
        //     12: istore_3
        //     13: iload_3
        //     14: iload_2
        //     15: if_icmpge +194 -> 209
        //     18: aload_1
        //     19: iload_3
        //     20: aaload
        //     21: astore 4
        //     23: aload_0
        //     24: getfield 585	com/android/server/BackupManagerService:mJournal	Ljava/io/File;
        //     27: ifnull +15 -> 42
        //     30: aload 4
        //     32: aload_0
        //     33: getfield 585	com/android/server/BackupManagerService:mJournal	Ljava/io/File;
        //     36: invokevirtual 1185	java/io/File:compareTo	(Ljava/io/File;)I
        //     39: ifeq +94 -> 133
        //     42: aconst_null
        //     43: astore 5
        //     45: ldc 174
        //     47: ldc_w 1187
        //     50: invokestatic 830	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     53: pop
        //     54: new 1118	java/io/RandomAccessFile
        //     57: dup
        //     58: aload 4
        //     60: ldc_w 1120
        //     63: invokespecial 1121	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     66: astore 16
        //     68: aload 16
        //     70: invokevirtual 1126	java/io/RandomAccessFile:readUTF	()Ljava/lang/String;
        //     73: astore 18
        //     75: ldc 174
        //     77: new 630	java/lang/StringBuilder
        //     80: dup
        //     81: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     84: ldc_w 1189
        //     87: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     90: aload 18
        //     92: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     95: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     98: invokestatic 830	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     101: pop
        //     102: aload_0
        //     103: aload 18
        //     105: invokespecial 778	com/android/server/BackupManagerService:dataChangedImpl	(Ljava/lang/String;)V
        //     108: goto -40 -> 68
        //     111: astore 17
        //     113: aload 16
        //     115: astore 5
        //     117: aload 5
        //     119: ifnull +8 -> 127
        //     122: aload 5
        //     124: invokevirtual 1128	java/io/RandomAccessFile:close	()V
        //     127: aload 4
        //     129: invokevirtual 1137	java/io/File:delete	()Z
        //     132: pop
        //     133: iinc 3 1
        //     136: goto -123 -> 13
        //     139: astore 12
        //     141: ldc 174
        //     143: new 630	java/lang/StringBuilder
        //     146: dup
        //     147: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     150: ldc_w 1191
        //     153: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     156: aload 4
        //     158: invokevirtual 708	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     161: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     164: aload 12
        //     166: invokestatic 1033	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     169: pop
        //     170: aload 5
        //     172: ifnull -45 -> 127
        //     175: aload 5
        //     177: invokevirtual 1128	java/io/RandomAccessFile:close	()V
        //     180: goto -53 -> 127
        //     183: astore 14
        //     185: goto -58 -> 127
        //     188: astore 9
        //     190: aload 5
        //     192: ifnull +8 -> 200
        //     195: aload 5
        //     197: invokevirtual 1128	java/io/RandomAccessFile:close	()V
        //     200: aload 4
        //     202: invokevirtual 1137	java/io/File:delete	()Z
        //     205: pop
        //     206: aload 9
        //     208: athrow
        //     209: return
        //     210: astore 8
        //     212: goto -85 -> 127
        //     215: astore 11
        //     217: goto -17 -> 200
        //     220: astore 9
        //     222: aload 16
        //     224: astore 5
        //     226: goto -36 -> 190
        //     229: astore 12
        //     231: aload 16
        //     233: astore 5
        //     235: goto -94 -> 141
        //     238: astore 6
        //     240: goto -123 -> 117
        //
        // Exception table:
        //     from	to	target	type
        //     68	108	111	java/io/EOFException
        //     45	68	139	java/lang/Exception
        //     175	180	183	java/io/IOException
        //     45	68	188	finally
        //     141	170	188	finally
        //     122	127	210	java/io/IOException
        //     195	200	215	java/io/IOException
        //     68	108	220	finally
        //     68	108	229	java/lang/Exception
        //     45	68	238	java/io/EOFException
    }

    private byte[] randomBytes(int paramInt)
    {
        byte[] arrayOfByte = new byte[paramInt / 8];
        this.mRng.nextBytes(arrayOfByte);
        return arrayOfByte;
    }

    private void registerTransport(String paramString, IBackupTransport paramIBackupTransport)
    {
        synchronized (this.mTransports)
        {
            Slog.v("BackupManagerService", "Registering transport " + paramString + " = " + paramIBackupTransport);
            if (paramIBackupTransport != null)
                this.mTransports.put(paramString, paramIBackupTransport);
        }
        try
        {
            String str = paramIBackupTransport.transportDirName();
            File localFile = new File(this.mBaseStateDir, str);
            localFile.mkdirs();
            if (new File(localFile, "_need_init_").exists());
            synchronized (this.mQueueLock)
            {
                this.mPendingInits.add(str);
                this.mAlarmManager.set(0, 60000L + System.currentTimeMillis(), this.mRunInitIntent);
                while (true)
                {
                    label146: return;
                    this.mTransports.remove(paramString);
                    if ((this.mCurrentTransport != null) && (this.mCurrentTransport.equals(paramString)))
                        this.mCurrentTransport = null;
                }
                localObject1 = finally;
                throw localObject1;
            }
        }
        catch (RemoteException localRemoteException)
        {
            break label146;
        }
    }

    private void removePackageFromSetLocked(HashSet<String> paramHashSet, String paramString)
    {
        if (paramHashSet.contains(paramString))
        {
            removeEverBackedUp(paramString);
            paramHashSet.remove(paramString);
        }
    }

    private boolean signaturesMatch(Signature[] paramArrayOfSignature, PackageInfo paramPackageInfo)
    {
        boolean bool = true;
        if ((0x1 & paramPackageInfo.applicationInfo.flags) != 0)
            Slog.v("BackupManagerService", "System app " + paramPackageInfo.packageName + " - skipping sig check");
        label157: label161: 
        while (true)
        {
            return bool;
            Signature[] arrayOfSignature = paramPackageInfo.signatures;
            if (((paramArrayOfSignature != null) && (paramArrayOfSignature.length != 0)) || ((arrayOfSignature != null) && (arrayOfSignature.length != 0)))
                if ((paramArrayOfSignature == null) || (arrayOfSignature == null))
                {
                    bool = false;
                }
                else
                {
                    int i = paramArrayOfSignature.length;
                    int j = arrayOfSignature.length;
                    for (int k = 0; ; k++)
                    {
                        if (k >= i)
                            break label161;
                        int m = 0;
                        for (int n = 0; ; n++)
                            if (n < j)
                            {
                                if (paramArrayOfSignature[k].equals(arrayOfSignature[n]))
                                    m = 1;
                            }
                            else
                            {
                                if (m != 0)
                                    break label157;
                                bool = false;
                                break;
                            }
                    }
                }
        }
    }

    private void startBackupAlarmsLocked(long paramLong)
    {
        Random localRandom = new Random();
        long l = paramLong + System.currentTimeMillis() + localRandom.nextInt(300000);
        this.mAlarmManager.setRepeating(0, l, 3600000L + localRandom.nextInt(300000), this.mRunBackupIntent);
        this.mNextBackupPass = l;
    }

    // ERROR //
    private void writeToJournalLocked(String paramString)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: aload_0
        //     3: getfield 585	com/android/server/BackupManagerService:mJournal	Ljava/io/File;
        //     6: ifnonnull +18 -> 24
        //     9: aload_0
        //     10: ldc_w 1234
        //     13: aconst_null
        //     14: aload_0
        //     15: getfield 583	com/android/server/BackupManagerService:mJournalDir	Ljava/io/File;
        //     18: invokestatic 1238	java/io/File:createTempFile	(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
        //     21: putfield 585	com/android/server/BackupManagerService:mJournal	Ljava/io/File;
        //     24: new 1118	java/io/RandomAccessFile
        //     27: dup
        //     28: aload_0
        //     29: getfield 585	com/android/server/BackupManagerService:mJournal	Ljava/io/File;
        //     32: ldc_w 1139
        //     35: invokespecial 1121	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     38: astore 8
        //     40: aload 8
        //     42: aload 8
        //     44: invokevirtual 1239	java/io/RandomAccessFile:length	()J
        //     47: invokevirtual 1242	java/io/RandomAccessFile:seek	(J)V
        //     50: aload 8
        //     52: aload_1
        //     53: invokevirtual 1146	java/io/RandomAccessFile:writeUTF	(Ljava/lang/String;)V
        //     56: aload 8
        //     58: ifnull +8 -> 66
        //     61: aload 8
        //     63: invokevirtual 1128	java/io/RandomAccessFile:close	()V
        //     66: return
        //     67: astore 5
        //     69: ldc 174
        //     71: new 630	java/lang/StringBuilder
        //     74: dup
        //     75: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     78: ldc_w 1244
        //     81: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     84: aload_1
        //     85: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     88: ldc_w 1246
        //     91: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     94: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     97: aload 5
        //     99: invokestatic 1033	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     102: pop
        //     103: aload_0
        //     104: aconst_null
        //     105: putfield 585	com/android/server/BackupManagerService:mJournal	Ljava/io/File;
        //     108: aload_2
        //     109: ifnull -43 -> 66
        //     112: aload_2
        //     113: invokevirtual 1128	java/io/RandomAccessFile:close	()V
        //     116: goto -50 -> 66
        //     119: astore 7
        //     121: goto -55 -> 66
        //     124: astore_3
        //     125: aload_2
        //     126: ifnull +7 -> 133
        //     129: aload_2
        //     130: invokevirtual 1128	java/io/RandomAccessFile:close	()V
        //     133: aload_3
        //     134: athrow
        //     135: astore 4
        //     137: goto -4 -> 133
        //     140: astore 9
        //     142: goto -76 -> 66
        //     145: astore_3
        //     146: aload 8
        //     148: astore_2
        //     149: goto -24 -> 125
        //     152: astore 5
        //     154: aload 8
        //     156: astore_2
        //     157: goto -88 -> 69
        //
        // Exception table:
        //     from	to	target	type
        //     2	40	67	java/io/IOException
        //     112	116	119	java/io/IOException
        //     2	40	124	finally
        //     69	108	124	finally
        //     129	133	135	java/io/IOException
        //     61	66	140	java/io/IOException
        //     40	56	145	finally
        //     40	56	152	java/io/IOException
    }

    public void acknowledgeFullBackupOrRestore(int paramInt, boolean paramBoolean, String paramString1, String paramString2, IFullBackupRestoreObserver paramIFullBackupRestoreObserver)
    {
        Slog.d("BackupManagerService", "acknowledgeFullBackupOrRestore : token=" + paramInt + " allow=" + paramBoolean);
        this.mContext.enforceCallingPermission("android.permission.BACKUP", "acknowledgeFullBackupOrRestore");
        long l = Binder.clearCallingIdentity();
        while (true)
        {
            try
            {
                synchronized (this.mFullConfirmations)
                {
                    FullParams localFullParams = (FullParams)this.mFullConfirmations.get(paramInt);
                    if (localFullParams != null)
                    {
                        this.mBackupHandler.removeMessages(9, localFullParams);
                        this.mFullConfirmations.delete(paramInt);
                        if (paramBoolean)
                        {
                            int i;
                            if ((localFullParams instanceof FullBackupParams))
                            {
                                i = 2;
                                localFullParams.observer = paramIFullBackupRestoreObserver;
                                localFullParams.curPassword = paramString1;
                            }
                            try
                            {
                                if (this.mMountService.getEncryptionState() != 1)
                                {
                                    j = 1;
                                    if (j != 0)
                                        Slog.w("BackupManagerService", "Device is encrypted; forcing enc password");
                                    if (j == 0)
                                        break label313;
                                    localFullParams.encryptPassword = paramString1;
                                    Slog.d("BackupManagerService", "Sending conf message with verb " + i);
                                    this.mWakelock.acquire();
                                    Message localMessage = this.mBackupHandler.obtainMessage(i, localFullParams);
                                    this.mBackupHandler.sendMessage(localMessage);
                                    return;
                                    i = 10;
                                    continue;
                                }
                                j = 0;
                                continue;
                            }
                            catch (RemoteException localRemoteException)
                            {
                                Slog.e("BackupManagerService", "Unable to contact mount service!");
                                int j = 1;
                                continue;
                            }
                        }
                        Slog.w("BackupManagerService", "User rejected full backup/restore operation");
                        signalFullBackupRestoreCompletion(localFullParams);
                    }
                }
            }
            finally
            {
                Binder.restoreCallingIdentity(l);
            }
            Slog.w("BackupManagerService", "Attempted to ack full backup/restore with invalid token");
            continue;
            label313: paramString1 = paramString2;
        }
    }

    void addBackupTrace(String paramString)
    {
        synchronized (this.mBackupTrace)
        {
            this.mBackupTrace.add(paramString);
            return;
        }
    }

    void addPackageParticipantsLocked(String[] paramArrayOfString)
    {
        List localList = allAgentPackages();
        if (paramArrayOfString != null)
        {
            Slog.v("BackupManagerService", "addPackageParticipantsLocked: #" + paramArrayOfString.length);
            int i = paramArrayOfString.length;
            for (int j = 0; j < i; j++)
                addPackageParticipantsLockedInner(paramArrayOfString[j], localList);
        }
        Slog.v("BackupManagerService", "addPackageParticipantsLocked: all");
        addPackageParticipantsLockedInner(null, localList);
    }

    public void agentConnected(String paramString, IBinder paramIBinder)
    {
        synchronized (this.mAgentConnectLock)
        {
            if (Binder.getCallingUid() == 1000)
            {
                Slog.d("BackupManagerService", "agentConnected pkg=" + paramString + " agent=" + paramIBinder);
                this.mConnectedAgent = IBackupAgent.Stub.asInterface(paramIBinder);
                this.mConnecting = false;
                this.mAgentConnectLock.notifyAll();
                return;
            }
            Slog.w("BackupManagerService", "Non-system process uid=" + Binder.getCallingUid() + " claiming agent connected");
        }
    }

    public void agentDisconnected(String paramString)
    {
        synchronized (this.mAgentConnectLock)
        {
            if (Binder.getCallingUid() == 1000)
            {
                this.mConnectedAgent = null;
                this.mConnecting = false;
                this.mAgentConnectLock.notifyAll();
                return;
            }
            Slog.w("BackupManagerService", "Non-system process uid=" + Binder.getCallingUid() + " claiming agent disconnected");
        }
    }

    List<PackageInfo> allAgentPackages()
    {
        List localList = this.mPackageManager.getInstalledPackages(64);
        for (int i = -1 + localList.size(); ; i--)
            if (i >= 0)
            {
                PackageInfo localPackageInfo = (PackageInfo)localList.get(i);
                try
                {
                    ApplicationInfo localApplicationInfo1 = localPackageInfo.applicationInfo;
                    if (((0x8000 & localApplicationInfo1.flags) == 0) || (localApplicationInfo1.backupAgentName == null))
                    {
                        localList.remove(i);
                    }
                    else
                    {
                        ApplicationInfo localApplicationInfo2 = this.mPackageManager.getApplicationInfo(localPackageInfo.packageName, 1024);
                        localPackageInfo.applicationInfo.sharedLibraryFiles = localApplicationInfo2.sharedLibraryFiles;
                    }
                }
                catch (PackageManager.NameNotFoundException localNameNotFoundException)
                {
                    localList.remove(i);
                }
            }
            else
            {
                return localList;
            }
    }

    public void backupNow()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BACKUP", "backupNow");
        Slog.v("BackupManagerService", "Scheduling immediate backup pass");
        synchronized (this.mQueueLock)
        {
            startBackupAlarmsLocked(3600000L);
            try
            {
                this.mRunBackupIntent.send();
                return;
            }
            catch (PendingIntent.CanceledException localCanceledException)
            {
                while (true)
                    Slog.e("BackupManagerService", "run-backup intent cancelled!");
            }
        }
    }

    // ERROR //
    public android.app.backup.IRestoreSession beginRestoreSession(String paramString1, String paramString2)
    {
        // Byte code:
        //     0: ldc 174
        //     2: new 630	java/lang/StringBuilder
        //     5: dup
        //     6: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     9: ldc_w 1383
        //     12: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     15: aload_1
        //     16: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     19: ldc_w 1385
        //     22: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     25: aload_2
        //     26: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     29: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     32: invokestatic 646	android/util/Slog:v	(Ljava/lang/String;Ljava/lang/String;)I
        //     35: pop
        //     36: iconst_1
        //     37: istore 4
        //     39: aload_2
        //     40: ifnonnull +40 -> 80
        //     43: aload_0
        //     44: getfield 620	com/android/server/BackupManagerService:mCurrentTransport	Ljava/lang/String;
        //     47: astore_2
        //     48: aload_1
        //     49: ifnull +31 -> 80
        //     52: aload_0
        //     53: getfield 365	com/android/server/BackupManagerService:mPackageManager	Landroid/content/pm/PackageManager;
        //     56: aload_1
        //     57: iconst_0
        //     58: invokevirtual 1143	android/content/pm/PackageManager:getPackageInfo	(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
        //     61: astore 12
        //     63: aload 12
        //     65: getfield 806	android/content/pm/PackageInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     68: getfield 809	android/content/pm/ApplicationInfo:uid	I
        //     71: invokestatic 901	android/os/Binder:getCallingUid	()I
        //     74: if_icmpne +6 -> 80
        //     77: iconst_0
        //     78: istore 4
        //     80: iload 4
        //     82: ifeq +104 -> 186
        //     85: aload_0
        //     86: getfield 357	com/android/server/BackupManagerService:mContext	Landroid/content/Context;
        //     89: ldc_w 553
        //     92: ldc_w 1386
        //     95: invokevirtual 1372	android/content/Context:enforceCallingOrSelfPermission	(Ljava/lang/String;Ljava/lang/String;)V
        //     98: aload_0
        //     99: monitorenter
        //     100: aload_0
        //     101: getfield 1388	com/android/server/BackupManagerService:mActiveRestoreSession	Lcom/android/server/BackupManagerService$ActiveRestoreSession;
        //     104: ifnull +94 -> 198
        //     107: ldc 174
        //     109: ldc_w 1390
        //     112: invokestatic 914	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     115: pop
        //     116: aconst_null
        //     117: astore 8
        //     119: aload_0
        //     120: monitorexit
        //     121: aload 8
        //     123: areturn
        //     124: astore 10
        //     126: ldc 174
        //     128: new 630	java/lang/StringBuilder
        //     131: dup
        //     132: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     135: ldc_w 1392
        //     138: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     141: aload_1
        //     142: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     145: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     148: invokestatic 703	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     151: pop
        //     152: new 1095	java/lang/IllegalArgumentException
        //     155: dup
        //     156: new 630	java/lang/StringBuilder
        //     159: dup
        //     160: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     163: ldc_w 1394
        //     166: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     169: aload_1
        //     170: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     173: ldc_w 1396
        //     176: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     179: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     182: invokespecial 1098	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     185: athrow
        //     186: ldc 174
        //     188: ldc_w 1398
        //     191: invokestatic 914	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     194: pop
        //     195: goto -97 -> 98
        //     198: aload_0
        //     199: new 14	com/android/server/BackupManagerService$ActiveRestoreSession
        //     202: dup
        //     203: aload_0
        //     204: aload_1
        //     205: aload_2
        //     206: invokespecial 1401	com/android/server/BackupManagerService$ActiveRestoreSession:<init>	(Lcom/android/server/BackupManagerService;Ljava/lang/String;Ljava/lang/String;)V
        //     209: putfield 1388	com/android/server/BackupManagerService:mActiveRestoreSession	Lcom/android/server/BackupManagerService$ActiveRestoreSession;
        //     212: aload_0
        //     213: getfield 442	com/android/server/BackupManagerService:mBackupHandler	Lcom/android/server/BackupManagerService$BackupHandler;
        //     216: bipush 8
        //     218: ldc2_w 182
        //     221: invokevirtual 1405	com/android/server/BackupManagerService$BackupHandler:sendEmptyMessageDelayed	(IJ)Z
        //     224: pop
        //     225: aload_0
        //     226: monitorexit
        //     227: aload_0
        //     228: getfield 1388	com/android/server/BackupManagerService:mActiveRestoreSession	Lcom/android/server/BackupManagerService$ActiveRestoreSession;
        //     231: astore 8
        //     233: goto -112 -> 121
        //     236: astore 6
        //     238: aload_0
        //     239: monitorexit
        //     240: aload 6
        //     242: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     52	63	124	android/content/pm/PackageManager$NameNotFoundException
        //     100	121	236	finally
        //     198	227	236	finally
        //     238	240	236	finally
    }

    IBackupAgent bindToAgentSynchronous(ApplicationInfo paramApplicationInfo, int paramInt)
    {
        Object localObject1 = null;
        IBackupAgent localIBackupAgent = null;
        synchronized (this.mAgentConnectLock)
        {
            this.mConnecting = true;
            this.mConnectedAgent = null;
        }
        try
        {
            if (this.mActivityManager.bindBackupAgent(paramApplicationInfo, paramInt))
            {
                Slog.d("BackupManagerService", "awaiting agent for " + paramApplicationInfo);
                long l1 = 10000L + System.currentTimeMillis();
                while (true)
                    if ((this.mConnecting) && (this.mConnectedAgent == null))
                    {
                        long l2 = System.currentTimeMillis();
                        if (l2 < l1)
                            try
                            {
                                this.mAgentConnectLock.wait(5000L);
                            }
                            catch (InterruptedException localInterruptedException)
                            {
                                Slog.w("BackupManagerService", "Interrupted: " + localInterruptedException);
                            }
                    }
            }
            while (true)
            {
                return localObject1;
                if (this.mConnecting == true)
                {
                    Slog.w("BackupManagerService", "Timeout waiting for agent " + paramApplicationInfo);
                    continue;
                    localObject3 = finally;
                    throw localObject3;
                }
                else
                {
                    Slog.i("BackupManagerService", "got agent " + this.mConnectedAgent);
                    localIBackupAgent = this.mConnectedAgent;
                    label230: localObject1 = localIBackupAgent;
                }
            }
        }
        catch (RemoteException localRemoteException)
        {
            break label230;
        }
    }

    void clearApplicationDataSynchronous(String paramString)
    {
        while (true)
        {
            ClearDataObserver localClearDataObserver;
            try
            {
                int i = this.mPackageManager.getPackageInfo(paramString, 0).applicationInfo.flags;
                if ((i & 0x40) == 0)
                    return;
            }
            catch (PackageManager.NameNotFoundException localNameNotFoundException)
            {
                Slog.w("BackupManagerService", "Tried to clear data for " + paramString + " but not found");
                continue;
                localClearDataObserver = new ClearDataObserver();
                synchronized (this.mClearDataLock)
                {
                    this.mClearingData = true;
                }
            }
            try
            {
                this.mActivityManager.clearApplicationUserData(paramString, localClearDataObserver, Binder.getOrigCallingUser());
                label102: long l1 = 10000L + System.currentTimeMillis();
                while (this.mClearingData)
                {
                    long l2 = System.currentTimeMillis();
                    if (l2 < l1)
                    {
                        try
                        {
                            this.mClearDataLock.wait(5000L);
                        }
                        catch (InterruptedException localInterruptedException)
                        {
                            this.mClearingData = false;
                        }
                        continue;
                        localObject2 = finally;
                        throw localObject2;
                    }
                }
            }
            catch (RemoteException localRemoteException)
            {
                break label102;
            }
        }
    }

    public void clearBackupData(String paramString)
    {
        Slog.v("BackupManagerService", "clearBackupData() of " + paramString);
        try
        {
            localPackageInfo = this.mPackageManager.getPackageInfo(paramString, 64);
            if (this.mContext.checkPermission("android.permission.BACKUP", Binder.getCallingPid(), Binder.getCallingUid()) == -1)
            {
                localHashSet1 = (HashSet)this.mBackupParticipants.get(Binder.getCallingUid());
                if (localHashSet1.contains(paramString))
                    Slog.v("BackupManagerService", "Found the app - running clear process");
            }
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            synchronized (this.mQueueLock)
            {
                HashSet localHashSet1;
                int i;
                int j;
                do
                {
                    PackageInfo localPackageInfo;
                    long l = Binder.clearCallingIdentity();
                    this.mWakelock.acquire();
                    Message localMessage = this.mBackupHandler.obtainMessage(4, new ClearParams(getTransport(this.mCurrentTransport), localPackageInfo));
                    this.mBackupHandler.sendMessage(localMessage);
                    Binder.restoreCallingIdentity(l);
                    while (true)
                    {
                        return;
                        localNameNotFoundException = localNameNotFoundException;
                        Slog.d("BackupManagerService", "No such package '" + paramString + "' - not clearing backup data");
                    }
                    Slog.v("BackupManagerService", "Privileged caller, allowing clear of other apps");
                    localHashSet1 = new HashSet();
                    i = this.mBackupParticipants.size();
                    j = 0;
                }
                while (j >= i);
                HashSet localHashSet2 = (HashSet)this.mBackupParticipants.valueAt(j);
                if (localHashSet2 != null)
                    localHashSet1.addAll(localHashSet2);
                j++;
            }
        }
    }

    void clearBackupTrace()
    {
        synchronized (this.mBackupTrace)
        {
            this.mBackupTrace.clear();
            return;
        }
    }

    void clearRestoreSession(ActiveRestoreSession paramActiveRestoreSession)
    {
        try
        {
            if (paramActiveRestoreSession != this.mActiveRestoreSession)
            {
                Slog.e("BackupManagerService", "ending non-current restore session");
                return;
            }
            Slog.v("BackupManagerService", "Clearing restore session and halting timeout");
            this.mActiveRestoreSession = null;
            this.mBackupHandler.removeMessages(8);
        }
        finally
        {
        }
    }

    public void dataChanged(final String paramString)
    {
        final HashSet localHashSet = dataChangedTargets(paramString);
        if (localHashSet == null)
            Slog.w("BackupManagerService", "dataChanged but no participant pkg='" + paramString + "'" + " uid=" + Binder.getCallingUid());
        while (true)
        {
            return;
            this.mBackupHandler.post(new Runnable()
            {
                public void run()
                {
                    BackupManagerService.this.dataChangedImpl(paramString, localHashSet);
                }
            });
        }
    }

    boolean deviceIsProvisioned()
    {
        boolean bool = false;
        if (Settings.Secure.getInt(this.mContext.getContentResolver(), "device_provisioned", 0) != 0)
            bool = true;
        return bool;
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.DUMP", "BackupManagerService");
        long l = Binder.clearCallingIdentity();
        try
        {
            dumpInternal(paramPrintWriter);
            return;
        }
        finally
        {
            Binder.restoreCallingIdentity(l);
        }
    }

    // ERROR //
    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public void fullBackup(ParcelFileDescriptor paramParcelFileDescriptor, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, String[] paramArrayOfString)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 357	com/android/server/BackupManagerService:mContext	Landroid/content/Context;
        //     4: ldc_w 553
        //     7: ldc_w 1490
        //     10: invokevirtual 1259	android/content/Context:enforceCallingPermission	(Ljava/lang/String;Ljava/lang/String;)V
        //     13: iload 4
        //     15: ifne +29 -> 44
        //     18: iload_3
        //     19: ifne +25 -> 44
        //     22: aload 6
        //     24: ifnull +9 -> 33
        //     27: aload 6
        //     29: arraylength
        //     30: ifne +14 -> 44
        //     33: new 1095	java/lang/IllegalArgumentException
        //     36: dup
        //     37: ldc_w 1492
        //     40: invokespecial 1098	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     43: athrow
        //     44: invokestatic 1262	android/os/Binder:clearCallingIdentity	()J
        //     47: lstore 7
        //     49: aload_0
        //     50: invokevirtual 1494	com/android/server/BackupManagerService:deviceIsProvisioned	()Z
        //     53: ifne +39 -> 92
        //     56: ldc 174
        //     58: ldc_w 1496
        //     61: invokestatic 830	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     64: pop
        //     65: aload_1
        //     66: invokevirtual 1499	android/os/ParcelFileDescriptor:close	()V
        //     69: lload 7
        //     71: invokestatic 1302	android/os/Binder:restoreCallingIdentity	(J)V
        //     74: ldc 174
        //     76: astore 21
        //     78: ldc_w 1501
        //     81: astore 22
        //     83: aload 21
        //     85: aload 22
        //     87: invokestatic 914	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     90: pop
        //     91: return
        //     92: ldc 174
        //     94: new 630	java/lang/StringBuilder
        //     97: dup
        //     98: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     101: ldc_w 1503
        //     104: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     107: iload_2
        //     108: invokevirtual 1255	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     111: ldc_w 1505
        //     114: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     117: iload_3
        //     118: invokevirtual 1255	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     121: ldc_w 1507
        //     124: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     127: iload 4
        //     129: invokevirtual 1255	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     132: ldc_w 1509
        //     135: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     138: aload 6
        //     140: invokevirtual 708	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     143: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     146: invokestatic 646	android/util/Slog:v	(Ljava/lang/String;Ljava/lang/String;)I
        //     149: pop
        //     150: ldc 174
        //     152: ldc_w 1511
        //     155: invokestatic 830	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     158: pop
        //     159: new 68	com/android/server/BackupManagerService$FullBackupParams
        //     162: dup
        //     163: aload_0
        //     164: aload_1
        //     165: iload_2
        //     166: iload_3
        //     167: iload 4
        //     169: iload 5
        //     171: aload 6
        //     173: invokespecial 1514	com/android/server/BackupManagerService$FullBackupParams:<init>	(Lcom/android/server/BackupManagerService;Landroid/os/ParcelFileDescriptor;ZZZZ[Ljava/lang/String;)V
        //     176: astore 14
        //     178: aload_0
        //     179: invokevirtual 1517	com/android/server/BackupManagerService:generateToken	()I
        //     182: istore 15
        //     184: aload_0
        //     185: getfield 329	com/android/server/BackupManagerService:mFullConfirmations	Landroid/util/SparseArray;
        //     188: astore 16
        //     190: aload 16
        //     192: monitorenter
        //     193: aload_0
        //     194: getfield 329	com/android/server/BackupManagerService:mFullConfirmations	Landroid/util/SparseArray;
        //     197: iload 15
        //     199: aload 14
        //     201: invokevirtual 817	android/util/SparseArray:put	(ILjava/lang/Object;)V
        //     204: aload 16
        //     206: monitorexit
        //     207: ldc 174
        //     209: new 630	java/lang/StringBuilder
        //     212: dup
        //     213: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     216: ldc_w 1519
        //     219: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     222: iload 15
        //     224: invokevirtual 904	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     227: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     230: invokestatic 914	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     233: pop
        //     234: aload_0
        //     235: iload 15
        //     237: ldc_w 1521
        //     240: invokestatic 1525	com/android/server/BackupManagerService$Injector:startConfirmationUi	(Lcom/android/server/BackupManagerService;ILjava/lang/String;)Z
        //     243: ifne +73 -> 316
        //     246: ldc 174
        //     248: ldc_w 1527
        //     251: invokestatic 698	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     254: pop
        //     255: aload_0
        //     256: getfield 329	com/android/server/BackupManagerService:mFullConfirmations	Landroid/util/SparseArray;
        //     259: iload 15
        //     261: invokevirtual 1267	android/util/SparseArray:delete	(I)V
        //     264: aload_1
        //     265: invokevirtual 1499	android/os/ParcelFileDescriptor:close	()V
        //     268: lload 7
        //     270: invokestatic 1302	android/os/Binder:restoreCallingIdentity	(J)V
        //     273: ldc 174
        //     275: astore 21
        //     277: ldc_w 1501
        //     280: astore 22
        //     282: goto -199 -> 83
        //     285: astore 17
        //     287: aload 16
        //     289: monitorexit
        //     290: aload 17
        //     292: athrow
        //     293: astore 9
        //     295: aload_1
        //     296: invokevirtual 1499	android/os/ParcelFileDescriptor:close	()V
        //     299: lload 7
        //     301: invokestatic 1302	android/os/Binder:restoreCallingIdentity	(J)V
        //     304: ldc 174
        //     306: ldc_w 1501
        //     309: invokestatic 914	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     312: pop
        //     313: aload 9
        //     315: athrow
        //     316: aload_0
        //     317: getfield 396	com/android/server/BackupManagerService:mPowerManager	Landroid/os/PowerManager;
        //     320: invokestatic 1532	android/os/SystemClock:uptimeMillis	()J
        //     323: iconst_0
        //     324: invokevirtual 1536	android/os/PowerManager:userActivity	(JZ)V
        //     327: aload_0
        //     328: iload 15
        //     330: aload 14
        //     332: invokestatic 1540	com/android/server/BackupManagerService$Injector:startConfirmationTimeout	(Lcom/android/server/BackupManagerService;ILcom/android/server/BackupManagerService$FullParams;)V
        //     335: ldc 174
        //     337: ldc_w 1542
        //     340: invokestatic 914	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     343: pop
        //     344: aload_0
        //     345: aload 14
        //     347: invokevirtual 1545	com/android/server/BackupManagerService:waitForCompletion	(Lcom/android/server/BackupManagerService$FullParams;)V
        //     350: aload_1
        //     351: invokevirtual 1499	android/os/ParcelFileDescriptor:close	()V
        //     354: lload 7
        //     356: invokestatic 1302	android/os/Binder:restoreCallingIdentity	(J)V
        //     359: ldc 174
        //     361: astore 21
        //     363: ldc_w 1501
        //     366: astore 22
        //     368: goto -285 -> 83
        //     371: astore 10
        //     373: goto -74 -> 299
        //     376: astore 27
        //     378: goto -309 -> 69
        //     381: astore 25
        //     383: goto -115 -> 268
        //     386: astore 20
        //     388: goto -34 -> 354
        //
        // Exception table:
        //     from	to	target	type
        //     193	207	285	finally
        //     287	290	285	finally
        //     49	65	293	finally
        //     92	193	293	finally
        //     207	264	293	finally
        //     290	293	293	finally
        //     316	350	293	finally
        //     295	299	371	java/io/IOException
        //     65	69	376	java/io/IOException
        //     264	268	381	java/io/IOException
        //     350	354	386	java/io/IOException
    }

    // ERROR //
    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public void fullRestore(ParcelFileDescriptor paramParcelFileDescriptor)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 357	com/android/server/BackupManagerService:mContext	Landroid/content/Context;
        //     4: ldc_w 553
        //     7: ldc_w 1548
        //     10: invokevirtual 1259	android/content/Context:enforceCallingPermission	(Ljava/lang/String;Ljava/lang/String;)V
        //     13: invokestatic 1262	android/os/Binder:clearCallingIdentity	()J
        //     16: lstore_2
        //     17: aload_0
        //     18: invokevirtual 1494	com/android/server/BackupManagerService:deviceIsProvisioned	()Z
        //     21: ifne +38 -> 59
        //     24: ldc 174
        //     26: ldc_w 1550
        //     29: invokestatic 830	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     32: pop
        //     33: aload_1
        //     34: invokevirtual 1499	android/os/ParcelFileDescriptor:close	()V
        //     37: lload_2
        //     38: invokestatic 1302	android/os/Binder:restoreCallingIdentity	(J)V
        //     41: ldc 174
        //     43: astore 17
        //     45: ldc_w 1552
        //     48: astore 18
        //     50: aload 17
        //     52: aload 18
        //     54: invokestatic 830	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     57: pop
        //     58: return
        //     59: ldc 174
        //     61: ldc_w 1554
        //     64: invokestatic 830	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     67: pop
        //     68: new 65	com/android/server/BackupManagerService$FullRestoreParams
        //     71: dup
        //     72: aload_0
        //     73: aload_1
        //     74: invokespecial 1557	com/android/server/BackupManagerService$FullRestoreParams:<init>	(Lcom/android/server/BackupManagerService;Landroid/os/ParcelFileDescriptor;)V
        //     77: astore 9
        //     79: aload_0
        //     80: invokevirtual 1517	com/android/server/BackupManagerService:generateToken	()I
        //     83: istore 10
        //     85: aload_0
        //     86: getfield 329	com/android/server/BackupManagerService:mFullConfirmations	Landroid/util/SparseArray;
        //     89: astore 11
        //     91: aload 11
        //     93: monitorenter
        //     94: aload_0
        //     95: getfield 329	com/android/server/BackupManagerService:mFullConfirmations	Landroid/util/SparseArray;
        //     98: iload 10
        //     100: aload 9
        //     102: invokevirtual 817	android/util/SparseArray:put	(ILjava/lang/Object;)V
        //     105: aload 11
        //     107: monitorexit
        //     108: ldc 174
        //     110: new 630	java/lang/StringBuilder
        //     113: dup
        //     114: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     117: ldc_w 1559
        //     120: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     123: iload 10
        //     125: invokevirtual 904	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     128: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     131: invokestatic 914	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     134: pop
        //     135: aload_0
        //     136: iload 10
        //     138: ldc_w 1561
        //     141: invokestatic 1525	com/android/server/BackupManagerService$Injector:startConfirmationUi	(Lcom/android/server/BackupManagerService;ILjava/lang/String;)Z
        //     144: ifne +71 -> 215
        //     147: ldc 174
        //     149: ldc_w 1563
        //     152: invokestatic 698	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     155: pop
        //     156: aload_0
        //     157: getfield 329	com/android/server/BackupManagerService:mFullConfirmations	Landroid/util/SparseArray;
        //     160: iload 10
        //     162: invokevirtual 1267	android/util/SparseArray:delete	(I)V
        //     165: aload_1
        //     166: invokevirtual 1499	android/os/ParcelFileDescriptor:close	()V
        //     169: lload_2
        //     170: invokestatic 1302	android/os/Binder:restoreCallingIdentity	(J)V
        //     173: ldc 174
        //     175: astore 17
        //     177: ldc_w 1552
        //     180: astore 18
        //     182: goto -132 -> 50
        //     185: astore 12
        //     187: aload 11
        //     189: monitorexit
        //     190: aload 12
        //     192: athrow
        //     193: astore 4
        //     195: aload_1
        //     196: invokevirtual 1499	android/os/ParcelFileDescriptor:close	()V
        //     199: lload_2
        //     200: invokestatic 1302	android/os/Binder:restoreCallingIdentity	(J)V
        //     203: ldc 174
        //     205: ldc_w 1552
        //     208: invokestatic 830	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     211: pop
        //     212: aload 4
        //     214: athrow
        //     215: aload_0
        //     216: getfield 396	com/android/server/BackupManagerService:mPowerManager	Landroid/os/PowerManager;
        //     219: invokestatic 1532	android/os/SystemClock:uptimeMillis	()J
        //     222: iconst_0
        //     223: invokevirtual 1536	android/os/PowerManager:userActivity	(JZ)V
        //     226: aload_0
        //     227: iload 10
        //     229: aload 9
        //     231: invokestatic 1540	com/android/server/BackupManagerService$Injector:startConfirmationTimeout	(Lcom/android/server/BackupManagerService;ILcom/android/server/BackupManagerService$FullParams;)V
        //     234: ldc 174
        //     236: ldc_w 1565
        //     239: invokestatic 914	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     242: pop
        //     243: aload_0
        //     244: aload 9
        //     246: invokevirtual 1545	com/android/server/BackupManagerService:waitForCompletion	(Lcom/android/server/BackupManagerService$FullParams;)V
        //     249: aload_1
        //     250: invokevirtual 1499	android/os/ParcelFileDescriptor:close	()V
        //     253: lload_2
        //     254: invokestatic 1302	android/os/Binder:restoreCallingIdentity	(J)V
        //     257: ldc 174
        //     259: astore 17
        //     261: ldc_w 1552
        //     264: astore 18
        //     266: goto -216 -> 50
        //     269: astore 5
        //     271: ldc 174
        //     273: new 630	java/lang/StringBuilder
        //     276: dup
        //     277: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     280: ldc_w 1567
        //     283: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     286: aload 5
        //     288: invokevirtual 708	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     291: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     294: invokestatic 703	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     297: pop
        //     298: goto -99 -> 199
        //     301: astore 24
        //     303: ldc 174
        //     305: new 630	java/lang/StringBuilder
        //     308: dup
        //     309: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     312: ldc_w 1567
        //     315: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     318: aload 24
        //     320: invokevirtual 708	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     323: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     326: invokestatic 703	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     329: pop
        //     330: goto -293 -> 37
        //     333: astore 21
        //     335: ldc 174
        //     337: new 630	java/lang/StringBuilder
        //     340: dup
        //     341: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     344: ldc_w 1567
        //     347: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     350: aload 21
        //     352: invokevirtual 708	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     355: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     358: invokestatic 703	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     361: pop
        //     362: goto -193 -> 169
        //     365: astore 15
        //     367: ldc 174
        //     369: new 630	java/lang/StringBuilder
        //     372: dup
        //     373: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     376: ldc_w 1567
        //     379: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     382: aload 15
        //     384: invokevirtual 708	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     387: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     390: invokestatic 703	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     393: pop
        //     394: goto -141 -> 253
        //
        // Exception table:
        //     from	to	target	type
        //     94	108	185	finally
        //     187	190	185	finally
        //     17	33	193	finally
        //     59	94	193	finally
        //     108	165	193	finally
        //     190	193	193	finally
        //     215	249	193	finally
        //     195	199	269	java/io/IOException
        //     33	37	301	java/io/IOException
        //     165	169	333	java/io/IOException
        //     249	253	365	java/io/IOException
    }

    int generateToken()
    {
        synchronized (this.mTokenGenerator)
        {
            int i;
            do
                i = this.mTokenGenerator.nextInt();
            while (i < 0);
            return i;
        }
    }

    long getAvailableRestoreToken(String paramString)
    {
        long l = this.mAncestralToken;
        synchronized (this.mQueueLock)
        {
            if (this.mEverStoredApps.contains(paramString))
                l = this.mCurrentToken;
            return l;
        }
    }

    public Intent getConfigurationIntent(String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BACKUP", "getConfigurationIntent");
        IBackupTransport localIBackupTransport;
        synchronized (this.mTransports)
        {
            localIBackupTransport = (IBackupTransport)this.mTransports.get(paramString);
            if (localIBackupTransport == null);
        }
        try
        {
            Intent localIntent2 = localIBackupTransport.configurationIntent();
            Intent localIntent1 = localIntent2;
            break label69;
            label56: localIntent1 = null;
            break label69;
            localObject = finally;
            throw localObject;
            label69: return localIntent1;
        }
        catch (RemoteException localRemoteException)
        {
            break label56;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    Context getContext()
    {
        return this.mContext;
    }

    public String getCurrentTransport()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BACKUP", "getCurrentTransport");
        return this.mCurrentTransport;
    }

    public String getDestinationString(String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BACKUP", "getDestinationString");
        IBackupTransport localIBackupTransport;
        synchronized (this.mTransports)
        {
            localIBackupTransport = (IBackupTransport)this.mTransports.get(paramString);
            if (localIBackupTransport == null);
        }
        try
        {
            String str2 = localIBackupTransport.currentDestinationString();
            String str1 = str2;
            break label69;
            label56: str1 = null;
            break label69;
            localObject = finally;
            throw localObject;
            label69: return str1;
        }
        catch (RemoteException localRemoteException)
        {
            break label56;
        }
    }

    void handleTimeout(int paramInt, Object paramObject)
    {
        int i = -1;
        synchronized (this.mCurrentOpLock)
        {
            Operation localOperation = (Operation)this.mCurrentOperations.get(paramInt);
            if (localOperation != null)
                i = localOperation.state;
            if (i == 0)
            {
                Slog.v("BackupManagerService", "TIMEOUT: token=" + Integer.toHexString(paramInt));
                localOperation.state = -1;
                this.mCurrentOperations.put(paramInt, localOperation);
            }
            this.mCurrentOpLock.notifyAll();
            if ((localOperation != null) && (localOperation.callback != null))
                localOperation.callback.handleTimeout();
            return;
        }
    }

    public boolean hasBackupPassword()
    {
        int i = 1;
        this.mContext.enforceCallingOrSelfPermission("android.permission.BACKUP", "hasBackupPassword");
        try
        {
            if (this.mMountService.getEncryptionState() == i)
            {
                if (this.mPasswordHash == null)
                    break label49;
                int k = this.mPasswordHash.length();
                if (k <= 0)
                    break label49;
            }
            while (true)
            {
                label47: return i;
                label49: int j = 0;
            }
        }
        catch (Exception localException)
        {
            break label47;
        }
    }

    public boolean isBackupEnabled()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BACKUP", "isBackupEnabled");
        return this.mEnabled;
    }

    public String[] listAllTransports()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BACKUP", "listAllTransports");
        String[] arrayOfString = null;
        ArrayList localArrayList = new ArrayList();
        Iterator localIterator = this.mTransports.entrySet().iterator();
        while (localIterator.hasNext())
        {
            Map.Entry localEntry = (Map.Entry)localIterator.next();
            if (localEntry.getValue() != null)
                localArrayList.add(localEntry.getKey());
        }
        if (localArrayList.size() > 0)
        {
            arrayOfString = new String[localArrayList.size()];
            localArrayList.toArray(arrayOfString);
        }
        return arrayOfString;
    }

    // ERROR //
    void logBackupComplete(String paramString)
    {
        // Byte code:
        //     0: aload_1
        //     1: ldc 153
        //     3: invokevirtual 628	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     6: ifeq +4 -> 10
        //     9: return
        //     10: aload_0
        //     11: getfield 339	com/android/server/BackupManagerService:mEverStoredApps	Ljava/util/HashSet;
        //     14: astore_2
        //     15: aload_2
        //     16: monitorenter
        //     17: aload_0
        //     18: getfield 339	com/android/server/BackupManagerService:mEverStoredApps	Ljava/util/HashSet;
        //     21: aload_1
        //     22: invokevirtual 820	java/util/HashSet:add	(Ljava/lang/Object;)Z
        //     25: ifne +13 -> 38
        //     28: aload_2
        //     29: monitorexit
        //     30: goto -21 -> 9
        //     33: astore_3
        //     34: aload_2
        //     35: monitorexit
        //     36: aload_3
        //     37: athrow
        //     38: aconst_null
        //     39: astore 4
        //     41: new 1118	java/io/RandomAccessFile
        //     44: dup
        //     45: aload_0
        //     46: getfield 1132	com/android/server/BackupManagerService:mEverStored	Ljava/io/File;
        //     49: ldc_w 1139
        //     52: invokespecial 1121	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     55: astore 5
        //     57: aload 5
        //     59: aload 5
        //     61: invokevirtual 1239	java/io/RandomAccessFile:length	()J
        //     64: invokevirtual 1242	java/io/RandomAccessFile:seek	(J)V
        //     67: aload 5
        //     69: aload_1
        //     70: invokevirtual 1146	java/io/RandomAccessFile:writeUTF	(Ljava/lang/String;)V
        //     73: aload 5
        //     75: ifnull +8 -> 83
        //     78: aload 5
        //     80: invokevirtual 1128	java/io/RandomAccessFile:close	()V
        //     83: aload_2
        //     84: monitorexit
        //     85: goto -76 -> 9
        //     88: astore 12
        //     90: ldc 174
        //     92: new 630	java/lang/StringBuilder
        //     95: dup
        //     96: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     99: ldc_w 1623
        //     102: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     105: aload_1
        //     106: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     109: ldc_w 1160
        //     112: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     115: aload_0
        //     116: getfield 1132	com/android/server/BackupManagerService:mEverStored	Ljava/io/File;
        //     119: invokevirtual 708	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     122: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     125: invokestatic 698	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     128: pop
        //     129: aload 4
        //     131: ifnull -48 -> 83
        //     134: aload 4
        //     136: invokevirtual 1128	java/io/RandomAccessFile:close	()V
        //     139: goto -56 -> 83
        //     142: astore 10
        //     144: goto -61 -> 83
        //     147: astore 7
        //     149: aload 4
        //     151: ifnull +8 -> 159
        //     154: aload 4
        //     156: invokevirtual 1128	java/io/RandomAccessFile:close	()V
        //     159: aload 7
        //     161: athrow
        //     162: astore 8
        //     164: goto -5 -> 159
        //     167: astore 11
        //     169: goto -86 -> 83
        //     172: astore 7
        //     174: aload 5
        //     176: astore 4
        //     178: goto -29 -> 149
        //     181: astore 6
        //     183: aload 5
        //     185: astore 4
        //     187: goto -97 -> 90
        //
        // Exception table:
        //     from	to	target	type
        //     17	36	33	finally
        //     78	83	33	finally
        //     83	85	33	finally
        //     134	139	33	finally
        //     154	159	33	finally
        //     159	162	33	finally
        //     41	57	88	java/io/IOException
        //     134	139	142	java/io/IOException
        //     41	57	147	finally
        //     90	129	147	finally
        //     154	159	162	java/io/IOException
        //     78	83	167	java/io/IOException
        //     57	73	172	finally
        //     57	73	181	java/io/IOException
    }

    public void opComplete(int paramInt)
    {
        synchronized (this.mCurrentOpLock)
        {
            Operation localOperation = (Operation)this.mCurrentOperations.get(paramInt);
            if (localOperation != null)
                localOperation.state = 1;
            this.mCurrentOpLock.notifyAll();
            if ((localOperation != null) && (localOperation.callback != null))
            {
                Message localMessage = this.mBackupHandler.obtainMessage(21, localOperation.callback);
                this.mBackupHandler.sendMessage(localMessage);
            }
            return;
        }
    }

    boolean passwordMatchesSaved(String paramString, int paramInt)
    {
        int i = 1;
        int n;
        try
        {
            if (this.mMountService.getEncryptionState() == i)
                break label136;
            int k = i;
            if (k != 0)
            {
                Slog.i("BackupManagerService", "Device encrypted; verifying against device data pw");
                n = this.mMountService.verifyEncryptionPassword(paramString);
                if (n != 0)
                    break label142;
                break label134;
                label52: Slog.e("BackupManagerService", "verified encryption state mismatch against query; no match allowed");
                i = 0;
            }
        }
        catch (Exception localException)
        {
            i = 0;
        }
        if (this.mPasswordHash == null)
        {
            if ((paramString == null) || ("".equals(paramString)));
        }
        else
        {
            String str;
            do
            {
                do
                {
                    i = 0;
                    break;
                }
                while ((paramString == null) || (paramString.length() <= 0));
                str = buildPasswordHash(paramString, this.mPasswordSalt, paramInt);
            }
            while (!this.mPasswordHash.equalsIgnoreCase(str));
        }
        while (true)
        {
            label134: return i;
            label136: int m = 0;
            break;
            label142: if (n == -2)
                break label52;
            int j = 0;
        }
    }

    void prepareOperationTimeout(int paramInt, long paramLong, BackupRestoreTask paramBackupRestoreTask)
    {
        synchronized (this.mCurrentOpLock)
        {
            this.mCurrentOperations.put(paramInt, new Operation(0, paramBackupRestoreTask));
            Message localMessage = this.mBackupHandler.obtainMessage(7, paramInt, 0, paramBackupRestoreTask);
            this.mBackupHandler.sendMessageDelayed(localMessage, paramLong);
            return;
        }
    }

    void recordInitPendingLocked(boolean paramBoolean, String paramString)
    {
        Slog.i("BackupManagerService", "recordInitPendingLocked: " + paramBoolean + " on transport " + paramString);
        try
        {
            String str = getTransport(paramString).transportDirName();
            localFile = new File(new File(this.mBaseStateDir, str), "_need_init_");
            if (paramBoolean)
                this.mPendingInits.add(paramString);
        }
        catch (RemoteException localRemoteException)
        {
            try
            {
                File localFile;
                new FileOutputStream(localFile).close();
                while (true)
                {
                    label97: return;
                    localFile.delete();
                    this.mPendingInits.remove(paramString);
                    continue;
                    localRemoteException = localRemoteException;
                }
            }
            catch (IOException localIOException)
            {
                break label97;
            }
        }
    }

    // ERROR //
    void removeEverBackedUp(String paramString)
    {
        // Byte code:
        //     0: ldc 174
        //     2: new 630	java/lang/StringBuilder
        //     5: dup
        //     6: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     9: ldc_w 1664
        //     12: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     15: aload_1
        //     16: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     19: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     22: invokestatic 646	android/util/Slog:v	(Ljava/lang/String;Ljava/lang/String;)I
        //     25: pop
        //     26: aload_0
        //     27: getfield 339	com/android/server/BackupManagerService:mEverStoredApps	Ljava/util/HashSet;
        //     30: astore_3
        //     31: aload_3
        //     32: monitorenter
        //     33: new 479	java/io/File
        //     36: dup
        //     37: aload_0
        //     38: getfield 490	com/android/server/BackupManagerService:mBaseStateDir	Ljava/io/File;
        //     41: ldc_w 1134
        //     44: invokespecial 488	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     47: astore 4
        //     49: aconst_null
        //     50: astore 5
        //     52: new 1118	java/io/RandomAccessFile
        //     55: dup
        //     56: aload 4
        //     58: ldc_w 1139
        //     61: invokespecial 1121	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     64: astore 6
        //     66: aload_0
        //     67: getfield 339	com/android/server/BackupManagerService:mEverStoredApps	Ljava/util/HashSet;
        //     70: aload_1
        //     71: invokevirtual 1214	java/util/HashSet:remove	(Ljava/lang/Object;)Z
        //     74: pop
        //     75: aload_0
        //     76: getfield 339	com/android/server/BackupManagerService:mEverStoredApps	Ljava/util/HashSet;
        //     79: invokevirtual 1038	java/util/HashSet:iterator	()Ljava/util/Iterator;
        //     82: astore 16
        //     84: aload 16
        //     86: invokeinterface 793 1 0
        //     91: ifeq +92 -> 183
        //     94: aload 6
        //     96: aload 16
        //     98: invokeinterface 797 1 0
        //     103: checkcast 624	java/lang/String
        //     106: invokevirtual 1146	java/io/RandomAccessFile:writeUTF	(Ljava/lang/String;)V
        //     109: goto -25 -> 84
        //     112: astore 10
        //     114: aload 6
        //     116: astore 5
        //     118: ldc 174
        //     120: new 630	java/lang/StringBuilder
        //     123: dup
        //     124: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     127: ldc_w 1666
        //     130: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     133: aload_0
        //     134: getfield 1132	com/android/server/BackupManagerService:mEverStored	Ljava/io/File;
        //     137: invokevirtual 708	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     140: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     143: aload 10
        //     145: invokestatic 1152	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     148: pop
        //     149: aload_0
        //     150: getfield 339	com/android/server/BackupManagerService:mEverStoredApps	Ljava/util/HashSet;
        //     153: invokevirtual 1667	java/util/HashSet:clear	()V
        //     156: aload 4
        //     158: invokevirtual 1137	java/io/File:delete	()Z
        //     161: pop
        //     162: aload_0
        //     163: getfield 1132	com/android/server/BackupManagerService:mEverStored	Ljava/io/File;
        //     166: invokevirtual 1137	java/io/File:delete	()Z
        //     169: pop
        //     170: aload 5
        //     172: ifnull +8 -> 180
        //     175: aload 5
        //     177: invokevirtual 1128	java/io/RandomAccessFile:close	()V
        //     180: aload_3
        //     181: monitorexit
        //     182: return
        //     183: aload 6
        //     185: invokevirtual 1128	java/io/RandomAccessFile:close	()V
        //     188: aconst_null
        //     189: astore 5
        //     191: aload 4
        //     193: aload_0
        //     194: getfield 1132	com/android/server/BackupManagerService:mEverStored	Ljava/io/File;
        //     197: invokevirtual 1156	java/io/File:renameTo	(Ljava/io/File;)Z
        //     200: ifne +82 -> 282
        //     203: new 287	java/io/IOException
        //     206: dup
        //     207: new 630	java/lang/StringBuilder
        //     210: dup
        //     211: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     214: ldc_w 1669
        //     217: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     220: aload 4
        //     222: invokevirtual 708	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     225: ldc_w 1160
        //     228: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     231: aload_0
        //     232: getfield 1132	com/android/server/BackupManagerService:mEverStored	Ljava/io/File;
        //     235: invokevirtual 708	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     238: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     241: invokespecial 1670	java/io/IOException:<init>	(Ljava/lang/String;)V
        //     244: athrow
        //     245: astore 10
        //     247: goto -129 -> 118
        //     250: astore 7
        //     252: aload 5
        //     254: ifnull +8 -> 262
        //     257: aload 5
        //     259: invokevirtual 1128	java/io/RandomAccessFile:close	()V
        //     262: aload 7
        //     264: athrow
        //     265: astore 8
        //     267: aload_3
        //     268: monitorexit
        //     269: aload 8
        //     271: athrow
        //     272: astore 9
        //     274: goto -12 -> 262
        //     277: astore 14
        //     279: goto -99 -> 180
        //     282: iconst_0
        //     283: ifeq -103 -> 180
        //     286: aconst_null
        //     287: athrow
        //     288: astore 7
        //     290: aload 6
        //     292: astore 5
        //     294: goto -42 -> 252
        //
        // Exception table:
        //     from	to	target	type
        //     66	109	112	java/io/IOException
        //     183	188	112	java/io/IOException
        //     52	66	245	java/io/IOException
        //     191	245	245	java/io/IOException
        //     52	66	250	finally
        //     118	170	250	finally
        //     191	245	250	finally
        //     33	49	265	finally
        //     175	180	265	finally
        //     180	182	265	finally
        //     257	262	265	finally
        //     262	269	265	finally
        //     286	288	265	finally
        //     257	262	272	java/io/IOException
        //     175	180	277	java/io/IOException
        //     286	288	277	java/io/IOException
        //     66	109	288	finally
        //     183	188	288	finally
    }

    void removePackageParticipantsLocked(String[] paramArrayOfString, int paramInt)
    {
        if (paramArrayOfString == null)
            Slog.w("BackupManagerService", "removePackageParticipants with null list");
        while (true)
        {
            return;
            Slog.v("BackupManagerService", "removePackageParticipantsLocked: uid=" + paramInt + " #" + paramArrayOfString.length);
            int i = paramArrayOfString.length;
            for (int j = 0; j < i; j++)
            {
                String str = paramArrayOfString[j];
                HashSet localHashSet = (HashSet)this.mBackupParticipants.get(paramInt);
                if ((localHashSet != null) && (localHashSet.contains(str)))
                {
                    removePackageFromSetLocked(localHashSet, str);
                    if (localHashSet.isEmpty())
                        this.mBackupParticipants.remove(paramInt);
                }
            }
        }
    }

    // ERROR //
    void resetBackupState(File paramFile)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 307	com/android/server/BackupManagerService:mQueueLock	Ljava/lang/Object;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 339	com/android/server/BackupManagerService:mEverStoredApps	Ljava/util/HashSet;
        //     11: invokevirtual 1667	java/util/HashSet:clear	()V
        //     14: aload_0
        //     15: getfield 1132	com/android/server/BackupManagerService:mEverStored	Ljava/io/File;
        //     18: invokevirtual 1137	java/io/File:delete	()Z
        //     21: pop
        //     22: aload_0
        //     23: lconst_0
        //     24: putfield 345	com/android/server/BackupManagerService:mCurrentToken	J
        //     27: aload_0
        //     28: invokevirtual 1687	com/android/server/BackupManagerService:writeRestoreTokens	()V
        //     31: aload_1
        //     32: invokevirtual 1014	java/io/File:listFiles	()[Ljava/io/File;
        //     35: astore 5
        //     37: aload 5
        //     39: arraylength
        //     40: istore 6
        //     42: iconst_0
        //     43: istore 7
        //     45: iload 7
        //     47: iload 6
        //     49: if_icmpge +32 -> 81
        //     52: aload 5
        //     54: iload 7
        //     56: aaload
        //     57: astore 14
        //     59: aload 14
        //     61: invokevirtual 1019	java/io/File:getName	()Ljava/lang/String;
        //     64: ldc 121
        //     66: invokevirtual 628	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     69: ifne +118 -> 187
        //     72: aload 14
        //     74: invokevirtual 1137	java/io/File:delete	()Z
        //     77: pop
        //     78: goto +109 -> 187
        //     81: aload_2
        //     82: monitorexit
        //     83: aload_0
        //     84: getfield 297	com/android/server/BackupManagerService:mBackupParticipants	Landroid/util/SparseArray;
        //     87: astore 8
        //     89: aload 8
        //     91: monitorenter
        //     92: aload_0
        //     93: getfield 297	com/android/server/BackupManagerService:mBackupParticipants	Landroid/util/SparseArray;
        //     96: invokevirtual 927	android/util/SparseArray:size	()I
        //     99: istore 10
        //     101: iconst_0
        //     102: istore 11
        //     104: iload 11
        //     106: iload 10
        //     108: if_icmpge +75 -> 183
        //     111: aload_0
        //     112: getfield 297	com/android/server/BackupManagerService:mBackupParticipants	Landroid/util/SparseArray;
        //     115: iload 11
        //     117: invokevirtual 930	android/util/SparseArray:valueAt	(I)Ljava/lang/Object;
        //     120: checkcast 336	java/util/HashSet
        //     123: astore 12
        //     125: aload 12
        //     127: ifnull +50 -> 177
        //     130: aload 12
        //     132: invokevirtual 1038	java/util/HashSet:iterator	()Ljava/util/Iterator;
        //     135: astore 13
        //     137: aload 13
        //     139: invokeinterface 793 1 0
        //     144: ifeq +33 -> 177
        //     147: aload_0
        //     148: aload 13
        //     150: invokeinterface 797 1 0
        //     155: checkcast 624	java/lang/String
        //     158: invokespecial 778	com/android/server/BackupManagerService:dataChangedImpl	(Ljava/lang/String;)V
        //     161: goto -24 -> 137
        //     164: astore 9
        //     166: aload 8
        //     168: monitorexit
        //     169: aload 9
        //     171: athrow
        //     172: astore_3
        //     173: aload_2
        //     174: monitorexit
        //     175: aload_3
        //     176: athrow
        //     177: iinc 11 1
        //     180: goto -76 -> 104
        //     183: aload 8
        //     185: monitorexit
        //     186: return
        //     187: iinc 7 1
        //     190: goto -145 -> 45
        //
        // Exception table:
        //     from	to	target	type
        //     92	169	164	finally
        //     183	186	164	finally
        //     7	83	172	finally
        //     173	175	172	finally
    }

    public void restoreAtInstall(String paramString, int paramInt)
    {
        if (Binder.getCallingUid() != 1000)
            Slog.w("BackupManagerService", "Non-system process uid=" + Binder.getCallingUid() + " attemping install-time restore");
        while (true)
        {
            return;
            long l = getAvailableRestoreToken(paramString);
            Slog.v("BackupManagerService", "restoreAtInstall pkg=" + paramString + " token=" + Integer.toHexString(paramInt));
            if ((this.mAutoRestore) && (this.mProvisioned) && (l != 0L))
            {
                PackageInfo localPackageInfo = new PackageInfo();
                localPackageInfo.packageName = paramString;
                this.mWakelock.acquire();
                Message localMessage = this.mBackupHandler.obtainMessage(3);
                localMessage.obj = new RestoreParams(getTransport(this.mCurrentTransport), null, l, localPackageInfo, paramInt, true);
                this.mBackupHandler.sendMessage(localMessage);
            }
            else
            {
                Slog.v("BackupManagerService", "No restore set -- skipping restore");
                try
                {
                    this.mPackageManagerBinder.finishPackageInstall(paramInt);
                }
                catch (RemoteException localRemoteException)
                {
                }
            }
        }
    }

    public String selectBackupTransport(String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BACKUP", "selectBackupTransport");
        HashMap localHashMap = this.mTransports;
        String str = null;
        try
        {
            if (this.mTransports.get(paramString) != null)
            {
                str = this.mCurrentTransport;
                this.mCurrentTransport = paramString;
                Settings.Secure.putString(this.mContext.getContentResolver(), "backup_transport", paramString);
                Slog.v("BackupManagerService", "selectBackupTransport() set " + this.mCurrentTransport + " returning " + str);
                return str;
            }
            Slog.w("BackupManagerService", "Attempt to select unavailable transport " + paramString);
        }
        finally
        {
        }
    }

    public void setAutoRestore(boolean paramBoolean)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BACKUP", "setAutoRestore");
        Slog.i("BackupManagerService", "Auto restore => " + paramBoolean);
        while (true)
        {
            try
            {
                ContentResolver localContentResolver = this.mContext.getContentResolver();
                if (paramBoolean)
                {
                    i = 1;
                    Settings.Secure.putInt(localContentResolver, "backup_auto_restore", i);
                    this.mAutoRestore = paramBoolean;
                    return;
                }
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
            int i = 0;
        }
    }

    // ERROR //
    public void setBackupEnabled(boolean paramBoolean)
    {
        // Byte code:
        //     0: iconst_1
        //     1: istore_2
        //     2: aload_0
        //     3: getfield 357	com/android/server/BackupManagerService:mContext	Landroid/content/Context;
        //     6: ldc_w 553
        //     9: ldc_w 1736
        //     12: invokevirtual 1372	android/content/Context:enforceCallingOrSelfPermission	(Ljava/lang/String;Ljava/lang/String;)V
        //     15: ldc 174
        //     17: new 630	java/lang/StringBuilder
        //     20: dup
        //     21: invokespecial 631	java/lang/StringBuilder:<init>	()V
        //     24: ldc_w 1738
        //     27: invokevirtual 637	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     30: iload_1
        //     31: invokevirtual 1255	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     34: invokevirtual 640	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     37: invokestatic 830	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     40: pop
        //     41: aload_0
        //     42: getfield 942	com/android/server/BackupManagerService:mEnabled	Z
        //     45: istore 4
        //     47: aload_0
        //     48: monitorenter
        //     49: aload_0
        //     50: getfield 357	com/android/server/BackupManagerService:mContext	Landroid/content/Context;
        //     53: invokevirtual 446	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
        //     56: astore 6
        //     58: iload_1
        //     59: ifeq +56 -> 115
        //     62: aload 6
        //     64: ldc_w 448
        //     67: iload_2
        //     68: invokestatic 1735	android/provider/Settings$Secure:putInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
        //     71: pop
        //     72: aload_0
        //     73: iload_1
        //     74: putfield 942	com/android/server/BackupManagerService:mEnabled	Z
        //     77: aload_0
        //     78: monitorexit
        //     79: aload_0
        //     80: getfield 307	com/android/server/BackupManagerService:mQueueLock	Ljava/lang/Object;
        //     83: astore 8
        //     85: aload 8
        //     87: monitorenter
        //     88: iload_1
        //     89: ifeq +38 -> 127
        //     92: iload 4
        //     94: ifne +33 -> 127
        //     97: aload_0
        //     98: getfield 458	com/android/server/BackupManagerService:mProvisioned	Z
        //     101: ifeq +26 -> 127
        //     104: aload_0
        //     105: ldc2_w 100
        //     108: invokespecial 716	com/android/server/BackupManagerService:startBackupAlarmsLocked	(J)V
        //     111: aload 8
        //     113: monitorexit
        //     114: return
        //     115: iconst_0
        //     116: istore_2
        //     117: goto -55 -> 62
        //     120: astore 5
        //     122: aload_0
        //     123: monitorexit
        //     124: aload 5
        //     126: athrow
        //     127: iload_1
        //     128: ifne -17 -> 111
        //     131: ldc 174
        //     133: ldc_w 1740
        //     136: invokestatic 830	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     139: pop
        //     140: aload_0
        //     141: getfield 390	com/android/server/BackupManagerService:mAlarmManager	Landroid/app/AlarmManager;
        //     144: aload_0
        //     145: getfield 577	com/android/server/BackupManagerService:mRunBackupIntent	Landroid/app/PendingIntent;
        //     148: invokevirtual 1744	android/app/AlarmManager:cancel	(Landroid/app/PendingIntent;)V
        //     151: iload 4
        //     153: ifeq -42 -> 111
        //     156: aload_0
        //     157: getfield 458	com/android/server/BackupManagerService:mProvisioned	Z
        //     160: ifeq -49 -> 111
        //     163: aload_0
        //     164: getfield 318	com/android/server/BackupManagerService:mTransports	Ljava/util/HashMap;
        //     167: astore 11
        //     169: aload 11
        //     171: monitorenter
        //     172: new 336	java/util/HashSet
        //     175: dup
        //     176: aload_0
        //     177: getfield 318	com/android/server/BackupManagerService:mTransports	Ljava/util/HashMap;
        //     180: invokevirtual 1747	java/util/HashMap:keySet	()Ljava/util/Set;
        //     183: invokespecial 1750	java/util/HashSet:<init>	(Ljava/util/Collection;)V
        //     186: astore 12
        //     188: aload 11
        //     190: monitorexit
        //     191: aload 12
        //     193: invokevirtual 1038	java/util/HashSet:iterator	()Ljava/util/Iterator;
        //     196: astore 14
        //     198: aload 14
        //     200: invokeinterface 793 1 0
        //     205: ifeq +37 -> 242
        //     208: aload_0
        //     209: iconst_1
        //     210: aload 14
        //     212: invokeinterface 797 1 0
        //     217: checkcast 624	java/lang/String
        //     220: invokevirtual 1752	com/android/server/BackupManagerService:recordInitPendingLocked	(ZLjava/lang/String;)V
        //     223: goto -25 -> 198
        //     226: astore 9
        //     228: aload 8
        //     230: monitorexit
        //     231: aload 9
        //     233: athrow
        //     234: astore 13
        //     236: aload 11
        //     238: monitorexit
        //     239: aload 13
        //     241: athrow
        //     242: aload_0
        //     243: getfield 390	com/android/server/BackupManagerService:mAlarmManager	Landroid/app/AlarmManager;
        //     246: iconst_0
        //     247: invokestatic 979	java/lang/System:currentTimeMillis	()J
        //     250: aload_0
        //     251: getfield 579	com/android/server/BackupManagerService:mRunInitIntent	Landroid/app/PendingIntent;
        //     254: invokevirtual 1204	android/app/AlarmManager:set	(IJLandroid/app/PendingIntent;)V
        //     257: goto -146 -> 111
        //
        // Exception table:
        //     from	to	target	type
        //     49	79	120	finally
        //     122	124	120	finally
        //     97	114	226	finally
        //     131	172	226	finally
        //     191	231	226	finally
        //     239	257	226	finally
        //     172	191	234	finally
        //     236	239	234	finally
    }

    // ERROR //
    public boolean setBackupPassword(String paramString1, String paramString2)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 357	com/android/server/BackupManagerService:mContext	Landroid/content/Context;
        //     4: ldc_w 553
        //     7: ldc_w 1755
        //     10: invokevirtual 1372	android/content/Context:enforceCallingOrSelfPermission	(Ljava/lang/String;Ljava/lang/String;)V
        //     13: aload_0
        //     14: aload_1
        //     15: sipush 10000
        //     18: invokevirtual 1757	com/android/server/BackupManagerService:passwordMatchesSaved	(Ljava/lang/String;I)Z
        //     21: ifne +7 -> 28
        //     24: iconst_0
        //     25: istore_3
        //     26: iload_3
        //     27: ireturn
        //     28: aload_2
        //     29: ifnull +10 -> 39
        //     32: aload_2
        //     33: invokevirtual 1758	java/lang/String:isEmpty	()Z
        //     36: ifeq +52 -> 88
        //     39: aload_0
        //     40: getfield 503	com/android/server/BackupManagerService:mPasswordHashFile	Ljava/io/File;
        //     43: invokevirtual 506	java/io/File:exists	()Z
        //     46: ifeq +27 -> 73
        //     49: aload_0
        //     50: getfield 503	com/android/server/BackupManagerService:mPasswordHashFile	Ljava/io/File;
        //     53: invokevirtual 1137	java/io/File:delete	()Z
        //     56: ifne +17 -> 73
        //     59: ldc 174
        //     61: ldc_w 1760
        //     64: invokestatic 698	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     67: pop
        //     68: iconst_0
        //     69: istore_3
        //     70: goto -44 -> 26
        //     73: aload_0
        //     74: aconst_null
        //     75: putfield 533	com/android/server/BackupManagerService:mPasswordHash	Ljava/lang/String;
        //     78: aload_0
        //     79: aconst_null
        //     80: putfield 535	com/android/server/BackupManagerService:mPasswordSalt	[B
        //     83: iconst_1
        //     84: istore_3
        //     85: goto -59 -> 26
        //     88: aload_0
        //     89: sipush 512
        //     92: invokespecial 728	com/android/server/BackupManagerService:randomBytes	(I)[B
        //     95: astore 7
        //     97: aload_0
        //     98: aload_2
        //     99: aload 7
        //     101: sipush 10000
        //     104: invokespecial 1636	com/android/server/BackupManagerService:buildPasswordHash	(Ljava/lang/String;[BI)Ljava/lang/String;
        //     107: astore 8
        //     109: aconst_null
        //     110: astore 9
        //     112: aconst_null
        //     113: astore 10
        //     115: aconst_null
        //     116: astore 11
        //     118: new 1660	java/io/FileOutputStream
        //     121: dup
        //     122: aload_0
        //     123: getfield 503	com/android/server/BackupManagerService:mPasswordHashFile	Ljava/io/File;
        //     126: invokespecial 1661	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
        //     129: astore 12
        //     131: new 1762	java/io/BufferedOutputStream
        //     134: dup
        //     135: aload 12
        //     137: invokespecial 1765	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;)V
        //     140: astore 13
        //     142: new 1767	java/io/DataOutputStream
        //     145: dup
        //     146: aload 13
        //     148: invokespecial 1768	java/io/DataOutputStream:<init>	(Ljava/io/OutputStream;)V
        //     151: astore 14
        //     153: aload 14
        //     155: aload 7
        //     157: arraylength
        //     158: invokevirtual 1771	java/io/DataOutputStream:writeInt	(I)V
        //     161: aload 14
        //     163: aload 7
        //     165: invokevirtual 1774	java/io/DataOutputStream:write	([B)V
        //     168: aload 14
        //     170: aload 8
        //     172: invokevirtual 1775	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
        //     175: aload 14
        //     177: invokevirtual 1778	java/io/DataOutputStream:flush	()V
        //     180: aload_0
        //     181: aload 8
        //     183: putfield 533	com/android/server/BackupManagerService:mPasswordHash	Ljava/lang/String;
        //     186: aload_0
        //     187: aload 7
        //     189: putfield 535	com/android/server/BackupManagerService:mPasswordSalt	[B
        //     192: iconst_1
        //     193: istore_3
        //     194: aload 14
        //     196: ifnull +8 -> 204
        //     199: aload 14
        //     201: invokevirtual 1779	java/io/DataOutputStream:close	()V
        //     204: aload 13
        //     206: ifnull +8 -> 214
        //     209: aload 13
        //     211: invokevirtual 1780	java/io/BufferedOutputStream:close	()V
        //     214: aload 12
        //     216: ifnull -190 -> 26
        //     219: aload 12
        //     221: invokevirtual 1662	java/io/FileOutputStream:close	()V
        //     224: goto -198 -> 26
        //     227: astore 5
        //     229: ldc 174
        //     231: ldc_w 1782
        //     234: invokestatic 698	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     237: pop
        //     238: iconst_0
        //     239: istore_3
        //     240: goto -214 -> 26
        //     243: astore 15
        //     245: aload 11
        //     247: ifnull +8 -> 255
        //     250: aload 11
        //     252: invokevirtual 1779	java/io/DataOutputStream:close	()V
        //     255: aload 10
        //     257: ifnull +8 -> 265
        //     260: aload 10
        //     262: invokevirtual 1780	java/io/BufferedOutputStream:close	()V
        //     265: aload 9
        //     267: ifnull +8 -> 275
        //     270: aload 9
        //     272: invokevirtual 1662	java/io/FileOutputStream:close	()V
        //     275: aload 15
        //     277: athrow
        //     278: astore 15
        //     280: aload 12
        //     282: astore 9
        //     284: goto -39 -> 245
        //     287: astore 15
        //     289: aload 13
        //     291: astore 10
        //     293: aload 12
        //     295: astore 9
        //     297: goto -52 -> 245
        //     300: astore 15
        //     302: aload 14
        //     304: astore 11
        //     306: aload 13
        //     308: astore 10
        //     310: aload 12
        //     312: astore 9
        //     314: goto -69 -> 245
        //
        // Exception table:
        //     from	to	target	type
        //     88	109	227	java/io/IOException
        //     199	224	227	java/io/IOException
        //     250	278	227	java/io/IOException
        //     118	131	243	finally
        //     131	142	278	finally
        //     142	153	287	finally
        //     153	192	300	finally
    }

    public void setBackupProvisioned(boolean paramBoolean)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BACKUP", "setBackupProvisioned");
    }

    void signalFullBackupRestoreCompletion(FullParams paramFullParams)
    {
        synchronized (paramFullParams.latch)
        {
            paramFullParams.latch.set(true);
            paramFullParams.latch.notifyAll();
            return;
        }
    }

    void startConfirmationTimeout(int paramInt, FullParams paramFullParams)
    {
        Message localMessage = this.mBackupHandler.obtainMessage(9, paramInt, 0, paramFullParams);
        this.mBackupHandler.sendMessageDelayed(localMessage, 60000L);
    }

    boolean startConfirmationUi(int paramInt, String paramString)
    {
        try
        {
            Intent localIntent = new Intent(paramString);
            localIntent.setClassName("com.android.backupconfirm", "com.android.backupconfirm.BackupRestoreConfirmation");
            localIntent.putExtra("conftoken", paramInt);
            localIntent.addFlags(268435456);
            this.mContext.startActivity(localIntent);
            bool = true;
            return bool;
        }
        catch (ActivityNotFoundException localActivityNotFoundException)
        {
            while (true)
                boolean bool = false;
        }
    }

    void waitForCompletion(FullParams paramFullParams)
    {
        synchronized (paramFullParams.latch)
        {
            while (true)
            {
                boolean bool = paramFullParams.latch.get();
                if (bool)
                    break;
                try
                {
                    paramFullParams.latch.wait();
                }
                catch (InterruptedException localInterruptedException)
                {
                }
            }
            return;
        }
    }

    boolean waitUntilOperationComplete(int paramInt)
    {
        int i = 0;
        while (true)
        {
            synchronized (this.mCurrentOpLock)
            {
                Operation localOperation = (Operation)this.mCurrentOperations.get(paramInt);
                if (localOperation == null)
                {
                    this.mBackupHandler.removeMessages(7);
                    if (i == 1)
                    {
                        bool = true;
                        return bool;
                    }
                }
                else
                {
                    int j = localOperation.state;
                    if (j == 0)
                    {
                        try
                        {
                            this.mCurrentOpLock.wait();
                        }
                        catch (InterruptedException localInterruptedException)
                        {
                        }
                        continue;
                    }
                    i = localOperation.state;
                }
            }
            boolean bool = false;
        }
    }

    void writeRestoreTokens()
    {
        try
        {
            RandomAccessFile localRandomAccessFile = new RandomAccessFile(this.mTokenFile, "rwd");
            localRandomAccessFile.writeInt(1);
            localRandomAccessFile.writeLong(this.mAncestralToken);
            localRandomAccessFile.writeLong(this.mCurrentToken);
            if (this.mAncestralPackages == null)
                localRandomAccessFile.writeInt(-1);
            while (true)
            {
                localRandomAccessFile.close();
                break;
                localRandomAccessFile.writeInt(this.mAncestralPackages.size());
                Slog.v("BackupManagerService", "Ancestral packages:    " + this.mAncestralPackages.size());
                Iterator localIterator = this.mAncestralPackages.iterator();
                while (localIterator.hasNext())
                    localRandomAccessFile.writeUTF((String)localIterator.next());
            }
        }
        catch (IOException localIOException)
        {
            Slog.w("BackupManagerService", "Unable to write token file:", localIOException);
        }
    }

    class ActiveRestoreSession extends IRestoreSession.Stub
    {
        private static final String TAG = "RestoreSession";
        boolean mEnded = false;
        private String mPackageName;
        RestoreSet[] mRestoreSets = null;
        private IBackupTransport mRestoreTransport = null;

        ActiveRestoreSession(String paramString1, String arg3)
        {
            this.mPackageName = paramString1;
            String str;
            this.mRestoreTransport = BackupManagerService.this.getTransport(str);
        }

        /** @deprecated */
        public void endRestoreSession()
        {
            try
            {
                Slog.d("RestoreSession", "endRestoreSession");
                if (this.mEnded)
                    throw new IllegalStateException("Restore session already ended");
            }
            finally
            {
            }
            BackupManagerService.this.mBackupHandler.post(new EndRestoreRunnable(BackupManagerService.this, this));
        }

        /** @deprecated */
        public int getAvailableRestoreSets(IRestoreObserver paramIRestoreObserver)
        {
            int i = -1;
            try
            {
                BackupManagerService.this.mContext.enforceCallingOrSelfPermission("android.permission.BACKUP", "getAvailableRestoreSets");
                if (paramIRestoreObserver == null)
                    throw new IllegalArgumentException("Observer must not be null");
            }
            finally
            {
            }
            if (this.mEnded)
                throw new IllegalStateException("Restore session already ended");
            long l = Binder.clearCallingIdentity();
            try
            {
                if (this.mRestoreTransport == null)
                {
                    Slog.w("RestoreSession", "Null transport getting restore sets");
                    Binder.restoreCallingIdentity(l);
                }
                while (true)
                {
                    return i;
                    BackupManagerService.this.mWakelock.acquire();
                    Message localMessage = BackupManagerService.this.mBackupHandler.obtainMessage(6, new BackupManagerService.RestoreGetSetsParams(BackupManagerService.this, this.mRestoreTransport, this, paramIRestoreObserver));
                    BackupManagerService.this.mBackupHandler.sendMessage(localMessage);
                    i = 0;
                    Binder.restoreCallingIdentity(l);
                }
            }
            catch (Exception localException)
            {
                while (true)
                {
                    Slog.e("RestoreSession", "Error in getAvailableRestoreSets", localException);
                    Binder.restoreCallingIdentity(l);
                }
            }
            finally
            {
                Binder.restoreCallingIdentity(l);
            }
        }

        /** @deprecated */
        // ERROR //
        public int restoreAll(long paramLong, IRestoreObserver paramIRestoreObserver)
        {
            // Byte code:
            //     0: bipush 255
            //     2: istore 4
            //     4: aload_0
            //     5: monitorenter
            //     6: aload_0
            //     7: getfield 27	com/android/server/BackupManagerService$ActiveRestoreSession:this$0	Lcom/android/server/BackupManagerService;
            //     10: invokestatic 82	com/android/server/BackupManagerService:access$1900	(Lcom/android/server/BackupManagerService;)Landroid/content/Context;
            //     13: ldc 84
            //     15: ldc 143
            //     17: invokevirtual 91	android/content/Context:enforceCallingOrSelfPermission	(Ljava/lang/String;Ljava/lang/String;)V
            //     20: ldc 14
            //     22: new 145	java/lang/StringBuilder
            //     25: dup
            //     26: invokespecial 146	java/lang/StringBuilder:<init>	()V
            //     29: ldc 148
            //     31: invokevirtual 152	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     34: lload_1
            //     35: invokestatic 158	java/lang/Long:toHexString	(J)Ljava/lang/String;
            //     38: invokevirtual 152	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     41: ldc 160
            //     43: invokevirtual 152	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     46: aload_3
            //     47: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     50: invokevirtual 167	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     53: invokestatic 54	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     56: pop
            //     57: aload_0
            //     58: getfield 36	com/android/server/BackupManagerService$ActiveRestoreSession:mEnded	Z
            //     61: ifeq +20 -> 81
            //     64: new 56	java/lang/IllegalStateException
            //     67: dup
            //     68: ldc 58
            //     70: invokespecial 61	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
            //     73: athrow
            //     74: astore 5
            //     76: aload_0
            //     77: monitorexit
            //     78: aload 5
            //     80: athrow
            //     81: aload_0
            //     82: getfield 32	com/android/server/BackupManagerService$ActiveRestoreSession:mRestoreTransport	Lcom/android/internal/backup/IBackupTransport;
            //     85: ifnull +10 -> 95
            //     88: aload_0
            //     89: getfield 34	com/android/server/BackupManagerService$ActiveRestoreSession:mRestoreSets	[Landroid/app/backup/RestoreSet;
            //     92: ifnonnull +16 -> 108
            //     95: ldc 14
            //     97: ldc 169
            //     99: invokestatic 171	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     102: pop
            //     103: aload_0
            //     104: monitorexit
            //     105: iload 4
            //     107: ireturn
            //     108: aload_0
            //     109: getfield 38	com/android/server/BackupManagerService$ActiveRestoreSession:mPackageName	Ljava/lang/String;
            //     112: ifnull +14 -> 126
            //     115: ldc 14
            //     117: ldc 173
            //     119: invokestatic 171	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     122: pop
            //     123: goto -20 -> 103
            //     126: aload_0
            //     127: getfield 27	com/android/server/BackupManagerService$ActiveRestoreSession:this$0	Lcom/android/server/BackupManagerService;
            //     130: getfield 177	com/android/server/BackupManagerService:mQueueLock	Ljava/lang/Object;
            //     133: astore 8
            //     135: aload 8
            //     137: monitorenter
            //     138: iconst_0
            //     139: istore 9
            //     141: iload 9
            //     143: aload_0
            //     144: getfield 34	com/android/server/BackupManagerService$ActiveRestoreSession:mRestoreSets	[Landroid/app/backup/RestoreSet;
            //     147: arraylength
            //     148: if_icmpge +110 -> 258
            //     151: lload_1
            //     152: aload_0
            //     153: getfield 34	com/android/server/BackupManagerService$ActiveRestoreSession:mRestoreSets	[Landroid/app/backup/RestoreSet;
            //     156: iload 9
            //     158: aaload
            //     159: getfield 183	android/app/backup/RestoreSet:token	J
            //     162: lcmp
            //     163: ifne +89 -> 252
            //     166: invokestatic 102	android/os/Binder:clearCallingIdentity	()J
            //     169: lstore 12
            //     171: aload_0
            //     172: getfield 27	com/android/server/BackupManagerService$ActiveRestoreSession:this$0	Lcom/android/server/BackupManagerService;
            //     175: getfield 115	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     178: invokevirtual 120	android/os/PowerManager$WakeLock:acquire	()V
            //     181: aload_0
            //     182: getfield 27	com/android/server/BackupManagerService$ActiveRestoreSession:this$0	Lcom/android/server/BackupManagerService;
            //     185: getfield 65	com/android/server/BackupManagerService:mBackupHandler	Lcom/android/server/BackupManagerService$BackupHandler;
            //     188: iconst_3
            //     189: invokevirtual 186	com/android/server/BackupManagerService$BackupHandler:obtainMessage	(I)Landroid/os/Message;
            //     192: astore 14
            //     194: aload 14
            //     196: new 188	com/android/server/BackupManagerService$RestoreParams
            //     199: dup
            //     200: aload_0
            //     201: getfield 27	com/android/server/BackupManagerService$ActiveRestoreSession:this$0	Lcom/android/server/BackupManagerService;
            //     204: aload_0
            //     205: getfield 32	com/android/server/BackupManagerService$ActiveRestoreSession:mRestoreTransport	Lcom/android/internal/backup/IBackupTransport;
            //     208: aload_3
            //     209: lload_1
            //     210: iconst_1
            //     211: invokespecial 191	com/android/server/BackupManagerService$RestoreParams:<init>	(Lcom/android/server/BackupManagerService;Lcom/android/internal/backup/IBackupTransport;Landroid/app/backup/IRestoreObserver;JZ)V
            //     214: putfield 196	android/os/Message:obj	Ljava/lang/Object;
            //     217: aload_0
            //     218: getfield 27	com/android/server/BackupManagerService$ActiveRestoreSession:this$0	Lcom/android/server/BackupManagerService;
            //     221: getfield 65	com/android/server/BackupManagerService:mBackupHandler	Lcom/android/server/BackupManagerService$BackupHandler;
            //     224: aload 14
            //     226: invokevirtual 133	com/android/server/BackupManagerService$BackupHandler:sendMessage	(Landroid/os/Message;)Z
            //     229: pop
            //     230: lload 12
            //     232: invokestatic 111	android/os/Binder:restoreCallingIdentity	(J)V
            //     235: iconst_0
            //     236: istore 4
            //     238: aload 8
            //     240: monitorexit
            //     241: goto -138 -> 103
            //     244: astore 10
            //     246: aload 8
            //     248: monitorexit
            //     249: aload 10
            //     251: athrow
            //     252: iinc 9 1
            //     255: goto -114 -> 141
            //     258: aload 8
            //     260: monitorexit
            //     261: ldc 14
            //     263: new 145	java/lang/StringBuilder
            //     266: dup
            //     267: invokespecial 146	java/lang/StringBuilder:<init>	()V
            //     270: ldc 198
            //     272: invokevirtual 152	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     275: lload_1
            //     276: invokestatic 158	java/lang/Long:toHexString	(J)Ljava/lang/String;
            //     279: invokevirtual 152	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     282: ldc 200
            //     284: invokevirtual 152	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     287: invokevirtual 167	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     290: invokestatic 107	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     293: pop
            //     294: goto -191 -> 103
            //
            // Exception table:
            //     from	to	target	type
            //     6	74	74	finally
            //     81	103	74	finally
            //     108	138	74	finally
            //     249	252	74	finally
            //     261	294	74	finally
            //     141	249	244	finally
            //     258	261	244	finally
        }

        /** @deprecated */
        public int restorePackage(String paramString, IRestoreObserver paramIRestoreObserver)
        {
            try
            {
                Slog.v("RestoreSession", "restorePackage pkg=" + paramString + " obs=" + paramIRestoreObserver);
                if (this.mEnded)
                    throw new IllegalStateException("Restore session already ended");
            }
            finally
            {
            }
            int i;
            if ((this.mPackageName != null) && (!this.mPackageName.equals(paramString)))
            {
                Slog.e("RestoreSession", "Ignoring attempt to restore pkg=" + paramString + " on session for package " + this.mPackageName);
                i = -1;
            }
            while (true)
            {
                return i;
                PackageInfo localPackageInfo;
                try
                {
                    localPackageInfo = BackupManagerService.this.mPackageManager.getPackageInfo(paramString, 0);
                    if ((BackupManagerService.this.mContext.checkPermission("android.permission.BACKUP", Binder.getCallingPid(), Binder.getCallingUid()) != -1) || (localPackageInfo.applicationInfo.uid == Binder.getCallingUid()))
                        break label257;
                    Slog.w("RestoreSession", "restorePackage: bad packageName=" + paramString + " or calling uid=" + Binder.getCallingUid());
                    throw new SecurityException("No permission to restore other packages");
                }
                catch (PackageManager.NameNotFoundException localNameNotFoundException)
                {
                    Slog.w("RestoreSession", "Asked to restore nonexistent pkg " + paramString);
                    i = -1;
                }
                continue;
                label257: if (localPackageInfo.applicationInfo.backupAgentName == null)
                {
                    Slog.w("RestoreSession", "Asked to restore package " + paramString + " with no agent");
                    i = -1;
                }
                else
                {
                    long l1 = BackupManagerService.this.getAvailableRestoreToken(paramString);
                    if (l1 == 0L)
                    {
                        Slog.w("RestoreSession", "No data available for this package; not restoring");
                        i = -1;
                    }
                    else
                    {
                        long l2 = Binder.clearCallingIdentity();
                        BackupManagerService.this.mWakelock.acquire();
                        Message localMessage = BackupManagerService.this.mBackupHandler.obtainMessage(3);
                        localMessage.obj = new BackupManagerService.RestoreParams(BackupManagerService.this, this.mRestoreTransport, paramIRestoreObserver, l1, localPackageInfo, 0, false);
                        BackupManagerService.this.mBackupHandler.sendMessage(localMessage);
                        Binder.restoreCallingIdentity(l2);
                        i = 0;
                    }
                }
            }
        }

        /** @deprecated */
        // ERROR //
        public int restoreSome(long paramLong, IRestoreObserver paramIRestoreObserver, String[] paramArrayOfString)
        {
            // Byte code:
            //     0: aload_0
            //     1: monitorenter
            //     2: aload_0
            //     3: getfield 27	com/android/server/BackupManagerService$ActiveRestoreSession:this$0	Lcom/android/server/BackupManagerService;
            //     6: invokestatic 82	com/android/server/BackupManagerService:access$1900	(Lcom/android/server/BackupManagerService;)Landroid/content/Context;
            //     9: ldc 84
            //     11: ldc 143
            //     13: invokevirtual 91	android/content/Context:enforceCallingOrSelfPermission	(Ljava/lang/String;Ljava/lang/String;)V
            //     16: new 145	java/lang/StringBuilder
            //     19: dup
            //     20: sipush 128
            //     23: invokespecial 289	java/lang/StringBuilder:<init>	(I)V
            //     26: astore 6
            //     28: aload 6
            //     30: ldc_w 291
            //     33: invokevirtual 152	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     36: pop
            //     37: aload 6
            //     39: lload_1
            //     40: invokestatic 158	java/lang/Long:toHexString	(J)Ljava/lang/String;
            //     43: invokevirtual 152	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     46: pop
            //     47: aload 6
            //     49: ldc 160
            //     51: invokevirtual 152	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     54: pop
            //     55: aload 6
            //     57: aload_3
            //     58: invokevirtual 294	java/lang/Object:toString	()Ljava/lang/String;
            //     61: invokevirtual 152	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     64: pop
            //     65: aload 6
            //     67: ldc_w 296
            //     70: invokevirtual 152	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     73: pop
            //     74: aload 4
            //     76: ifnonnull +47 -> 123
            //     79: aload 6
            //     81: ldc_w 298
            //     84: invokevirtual 152	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     87: pop
            //     88: ldc 14
            //     90: aload 6
            //     92: invokevirtual 167	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     95: invokestatic 54	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     98: pop
            //     99: aload_0
            //     100: getfield 36	com/android/server/BackupManagerService$ActiveRestoreSession:mEnded	Z
            //     103: ifeq +92 -> 195
            //     106: new 56	java/lang/IllegalStateException
            //     109: dup
            //     110: ldc 58
            //     112: invokespecial 61	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
            //     115: athrow
            //     116: astore 5
            //     118: aload_0
            //     119: monitorexit
            //     120: aload 5
            //     122: athrow
            //     123: aload 6
            //     125: bipush 123
            //     127: invokevirtual 301	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
            //     130: pop
            //     131: iconst_1
            //     132: istore 13
            //     134: aload 4
            //     136: arraylength
            //     137: istore 14
            //     139: iconst_0
            //     140: istore 15
            //     142: iload 15
            //     144: iload 14
            //     146: if_icmpge +38 -> 184
            //     149: aload 4
            //     151: iload 15
            //     153: aaload
            //     154: astore 29
            //     156: iload 13
            //     158: ifne +267 -> 425
            //     161: aload 6
            //     163: ldc_w 303
            //     166: invokevirtual 152	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     169: pop
            //     170: aload 6
            //     172: aload 29
            //     174: invokevirtual 152	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     177: pop
            //     178: iinc 15 1
            //     181: goto -39 -> 142
            //     184: aload 6
            //     186: bipush 125
            //     188: invokevirtual 301	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
            //     191: pop
            //     192: goto -104 -> 88
            //     195: aload_0
            //     196: getfield 32	com/android/server/BackupManagerService$ActiveRestoreSession:mRestoreTransport	Lcom/android/internal/backup/IBackupTransport;
            //     199: ifnull +10 -> 209
            //     202: aload_0
            //     203: getfield 34	com/android/server/BackupManagerService$ActiveRestoreSession:mRestoreSets	[Landroid/app/backup/RestoreSet;
            //     206: ifnonnull +20 -> 226
            //     209: ldc 14
            //     211: ldc 169
            //     213: invokestatic 171	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     216: pop
            //     217: bipush 255
            //     219: istore 19
            //     221: aload_0
            //     222: monitorexit
            //     223: iload 19
            //     225: ireturn
            //     226: aload_0
            //     227: getfield 38	com/android/server/BackupManagerService$ActiveRestoreSession:mPackageName	Ljava/lang/String;
            //     230: ifnull +18 -> 248
            //     233: ldc 14
            //     235: ldc 173
            //     237: invokestatic 171	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     240: pop
            //     241: bipush 255
            //     243: istore 19
            //     245: goto -24 -> 221
            //     248: aload_0
            //     249: getfield 27	com/android/server/BackupManagerService$ActiveRestoreSession:this$0	Lcom/android/server/BackupManagerService;
            //     252: getfield 177	com/android/server/BackupManagerService:mQueueLock	Ljava/lang/Object;
            //     255: astore 20
            //     257: aload 20
            //     259: monitorenter
            //     260: iconst_0
            //     261: istore 21
            //     263: iload 21
            //     265: aload_0
            //     266: getfield 34	com/android/server/BackupManagerService$ActiveRestoreSession:mRestoreSets	[Landroid/app/backup/RestoreSet;
            //     269: arraylength
            //     270: if_icmpge +112 -> 382
            //     273: lload_1
            //     274: aload_0
            //     275: getfield 34	com/android/server/BackupManagerService$ActiveRestoreSession:mRestoreSets	[Landroid/app/backup/RestoreSet;
            //     278: iload 21
            //     280: aaload
            //     281: getfield 183	android/app/backup/RestoreSet:token	J
            //     284: lcmp
            //     285: ifne +91 -> 376
            //     288: invokestatic 102	android/os/Binder:clearCallingIdentity	()J
            //     291: lstore 24
            //     293: aload_0
            //     294: getfield 27	com/android/server/BackupManagerService$ActiveRestoreSession:this$0	Lcom/android/server/BackupManagerService;
            //     297: getfield 115	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     300: invokevirtual 120	android/os/PowerManager$WakeLock:acquire	()V
            //     303: aload_0
            //     304: getfield 27	com/android/server/BackupManagerService$ActiveRestoreSession:this$0	Lcom/android/server/BackupManagerService;
            //     307: getfield 65	com/android/server/BackupManagerService:mBackupHandler	Lcom/android/server/BackupManagerService$BackupHandler;
            //     310: iconst_3
            //     311: invokevirtual 186	com/android/server/BackupManagerService$BackupHandler:obtainMessage	(I)Landroid/os/Message;
            //     314: astore 26
            //     316: aload 26
            //     318: new 188	com/android/server/BackupManagerService$RestoreParams
            //     321: dup
            //     322: aload_0
            //     323: getfield 27	com/android/server/BackupManagerService$ActiveRestoreSession:this$0	Lcom/android/server/BackupManagerService;
            //     326: aload_0
            //     327: getfield 32	com/android/server/BackupManagerService$ActiveRestoreSession:mRestoreTransport	Lcom/android/internal/backup/IBackupTransport;
            //     330: aload_3
            //     331: lload_1
            //     332: aload 4
            //     334: iconst_1
            //     335: invokespecial 306	com/android/server/BackupManagerService$RestoreParams:<init>	(Lcom/android/server/BackupManagerService;Lcom/android/internal/backup/IBackupTransport;Landroid/app/backup/IRestoreObserver;J[Ljava/lang/String;Z)V
            //     338: putfield 196	android/os/Message:obj	Ljava/lang/Object;
            //     341: aload_0
            //     342: getfield 27	com/android/server/BackupManagerService$ActiveRestoreSession:this$0	Lcom/android/server/BackupManagerService;
            //     345: getfield 65	com/android/server/BackupManagerService:mBackupHandler	Lcom/android/server/BackupManagerService$BackupHandler;
            //     348: aload 26
            //     350: invokevirtual 133	com/android/server/BackupManagerService$BackupHandler:sendMessage	(Landroid/os/Message;)Z
            //     353: pop
            //     354: lload 24
            //     356: invokestatic 111	android/os/Binder:restoreCallingIdentity	(J)V
            //     359: iconst_0
            //     360: istore 19
            //     362: aload 20
            //     364: monitorexit
            //     365: goto -144 -> 221
            //     368: astore 22
            //     370: aload 20
            //     372: monitorexit
            //     373: aload 22
            //     375: athrow
            //     376: iinc 21 1
            //     379: goto -116 -> 263
            //     382: aload 20
            //     384: monitorexit
            //     385: ldc 14
            //     387: new 145	java/lang/StringBuilder
            //     390: dup
            //     391: invokespecial 146	java/lang/StringBuilder:<init>	()V
            //     394: ldc 198
            //     396: invokevirtual 152	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     399: lload_1
            //     400: invokestatic 158	java/lang/Long:toHexString	(J)Ljava/lang/String;
            //     403: invokevirtual 152	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     406: ldc 200
            //     408: invokevirtual 152	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     411: invokevirtual 167	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     414: invokestatic 107	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     417: pop
            //     418: bipush 255
            //     420: istore 19
            //     422: goto -201 -> 221
            //     425: iconst_0
            //     426: istore 13
            //     428: goto -258 -> 170
            //
            // Exception table:
            //     from	to	target	type
            //     2	116	116	finally
            //     123	217	116	finally
            //     226	260	116	finally
            //     373	376	116	finally
            //     385	418	116	finally
            //     263	373	368	finally
            //     382	385	368	finally
        }

        class EndRestoreRunnable
            implements Runnable
        {
            BackupManagerService mBackupManager;
            BackupManagerService.ActiveRestoreSession mSession;

            EndRestoreRunnable(BackupManagerService paramActiveRestoreSession, BackupManagerService.ActiveRestoreSession arg3)
            {
                this.mBackupManager = paramActiveRestoreSession;
                Object localObject;
                this.mSession = localObject;
            }

            public void run()
            {
                BackupManagerService.ActiveRestoreSession localActiveRestoreSession = this.mSession;
                try
                {
                    if (this.mSession.mRestoreTransport != null)
                        this.mSession.mRestoreTransport.finishRestore();
                }
                catch (Exception localException)
                {
                }
                finally
                {
                    BackupManagerService.ActiveRestoreSession.access$2002(this.mSession, null);
                    this.mSession.mEnded = true;
                }
            }
        }
    }

    class PerformInitializeTask
        implements Runnable
    {
        HashSet<String> mQueue;

        PerformInitializeTask()
        {
            Object localObject;
            this.mQueue = localObject;
        }

        // ERROR //
        public void run()
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 23	com/android/server/BackupManagerService$PerformInitializeTask:mQueue	Ljava/util/HashSet;
            //     4: invokevirtual 34	java/util/HashSet:iterator	()Ljava/util/Iterator;
            //     7: astore 6
            //     9: aload 6
            //     11: invokeinterface 40 1 0
            //     16: ifeq +436 -> 452
            //     19: aload 6
            //     21: invokeinterface 44 1 0
            //     26: checkcast 46	java/lang/String
            //     29: astore 7
            //     31: aload_0
            //     32: getfield 18	com/android/server/BackupManagerService$PerformInitializeTask:this$0	Lcom/android/server/BackupManagerService;
            //     35: aload 7
            //     37: invokestatic 50	com/android/server/BackupManagerService:access$100	(Lcom/android/server/BackupManagerService;Ljava/lang/String;)Lcom/android/internal/backup/IBackupTransport;
            //     40: astore 8
            //     42: aload 8
            //     44: ifnonnull +54 -> 98
            //     47: ldc 52
            //     49: new 54	java/lang/StringBuilder
            //     52: dup
            //     53: invokespecial 55	java/lang/StringBuilder:<init>	()V
            //     56: ldc 57
            //     58: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     61: aload 7
            //     63: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     66: ldc 63
            //     68: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     71: invokevirtual 67	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     74: invokestatic 73	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     77: pop
            //     78: goto -69 -> 9
            //     81: astore 5
            //     83: aload_0
            //     84: getfield 18	com/android/server/BackupManagerService$PerformInitializeTask:this$0	Lcom/android/server/BackupManagerService;
            //     87: getfield 77	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     90: astore 4
            //     92: aload 4
            //     94: invokevirtual 82	android/os/PowerManager$WakeLock:release	()V
            //     97: return
            //     98: ldc 52
            //     100: new 54	java/lang/StringBuilder
            //     103: dup
            //     104: invokespecial 55	java/lang/StringBuilder:<init>	()V
            //     107: ldc 84
            //     109: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     112: aload 7
            //     114: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     117: invokevirtual 67	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     120: invokestatic 87	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
            //     123: pop
            //     124: sipush 2821
            //     127: aload 8
            //     129: invokeinterface 92 1 0
            //     134: invokestatic 98	android/util/EventLog:writeEvent	(ILjava/lang/String;)I
            //     137: pop
            //     138: invokestatic 104	android/os/SystemClock:elapsedRealtime	()J
            //     141: lstore 11
            //     143: aload 8
            //     145: invokeinterface 108 1 0
            //     150: istore 13
            //     152: iload 13
            //     154: ifne +12 -> 166
            //     157: aload 8
            //     159: invokeinterface 111 1 0
            //     164: istore 13
            //     166: iload 13
            //     168: ifne +149 -> 317
            //     171: ldc 52
            //     173: ldc 113
            //     175: invokestatic 87	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
            //     178: pop
            //     179: invokestatic 104	android/os/SystemClock:elapsedRealtime	()J
            //     182: lload 11
            //     184: lsub
            //     185: l2i
            //     186: istore 22
            //     188: sipush 2827
            //     191: iconst_0
            //     192: anewarray 4	java/lang/Object
            //     195: invokestatic 116	android/util/EventLog:writeEvent	(I[Ljava/lang/Object;)I
            //     198: pop
            //     199: aload_0
            //     200: getfield 18	com/android/server/BackupManagerService$PerformInitializeTask:this$0	Lcom/android/server/BackupManagerService;
            //     203: new 118	java/io/File
            //     206: dup
            //     207: aload_0
            //     208: getfield 18	com/android/server/BackupManagerService$PerformInitializeTask:this$0	Lcom/android/server/BackupManagerService;
            //     211: getfield 122	com/android/server/BackupManagerService:mBaseStateDir	Ljava/io/File;
            //     214: aload 8
            //     216: invokeinterface 92 1 0
            //     221: invokespecial 125	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
            //     224: invokevirtual 129	com/android/server/BackupManagerService:resetBackupState	(Ljava/io/File;)V
            //     227: iconst_2
            //     228: anewarray 4	java/lang/Object
            //     231: astore 24
            //     233: aload 24
            //     235: iconst_0
            //     236: iconst_0
            //     237: invokestatic 135	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
            //     240: aastore
            //     241: aload 24
            //     243: iconst_1
            //     244: iload 22
            //     246: invokestatic 135	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
            //     249: aastore
            //     250: sipush 2825
            //     253: aload 24
            //     255: invokestatic 116	android/util/EventLog:writeEvent	(I[Ljava/lang/Object;)I
            //     258: pop
            //     259: aload_0
            //     260: getfield 18	com/android/server/BackupManagerService$PerformInitializeTask:this$0	Lcom/android/server/BackupManagerService;
            //     263: getfield 139	com/android/server/BackupManagerService:mQueueLock	Ljava/lang/Object;
            //     266: astore 26
            //     268: aload 26
            //     270: monitorenter
            //     271: aload_0
            //     272: getfield 18	com/android/server/BackupManagerService$PerformInitializeTask:this$0	Lcom/android/server/BackupManagerService;
            //     275: iconst_0
            //     276: aload 7
            //     278: invokevirtual 143	com/android/server/BackupManagerService:recordInitPendingLocked	(ZLjava/lang/String;)V
            //     281: aload 26
            //     283: monitorexit
            //     284: goto -275 -> 9
            //     287: astore 27
            //     289: aload 26
            //     291: monitorexit
            //     292: aload 27
            //     294: athrow
            //     295: astore_2
            //     296: ldc 52
            //     298: ldc 145
            //     300: aload_2
            //     301: invokestatic 148	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     304: pop
            //     305: aload_0
            //     306: getfield 18	com/android/server/BackupManagerService$PerformInitializeTask:this$0	Lcom/android/server/BackupManagerService;
            //     309: getfield 77	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     312: astore 4
            //     314: goto -222 -> 92
            //     317: ldc 52
            //     319: ldc 150
            //     321: invokestatic 73	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     324: pop
            //     325: sipush 2822
            //     328: ldc 152
            //     330: invokestatic 98	android/util/EventLog:writeEvent	(ILjava/lang/String;)I
            //     333: pop
            //     334: aload_0
            //     335: getfield 18	com/android/server/BackupManagerService$PerformInitializeTask:this$0	Lcom/android/server/BackupManagerService;
            //     338: getfield 139	com/android/server/BackupManagerService:mQueueLock	Ljava/lang/Object;
            //     341: astore 16
            //     343: aload 16
            //     345: monitorenter
            //     346: aload_0
            //     347: getfield 18	com/android/server/BackupManagerService$PerformInitializeTask:this$0	Lcom/android/server/BackupManagerService;
            //     350: iconst_1
            //     351: aload 7
            //     353: invokevirtual 143	com/android/server/BackupManagerService:recordInitPendingLocked	(ZLjava/lang/String;)V
            //     356: aload 16
            //     358: monitorexit
            //     359: aload 8
            //     361: invokeinterface 155 1 0
            //     366: lstore 18
            //     368: ldc 52
            //     370: new 54	java/lang/StringBuilder
            //     373: dup
            //     374: invokespecial 55	java/lang/StringBuilder:<init>	()V
            //     377: ldc 157
            //     379: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     382: aload 7
            //     384: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     387: ldc 159
            //     389: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     392: lload 18
            //     394: invokevirtual 162	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
            //     397: invokevirtual 67	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     400: invokestatic 165	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     403: pop
            //     404: aload_0
            //     405: getfield 18	com/android/server/BackupManagerService$PerformInitializeTask:this$0	Lcom/android/server/BackupManagerService;
            //     408: invokestatic 169	com/android/server/BackupManagerService:access$400	(Lcom/android/server/BackupManagerService;)Landroid/app/AlarmManager;
            //     411: iconst_0
            //     412: lload 18
            //     414: invokestatic 174	java/lang/System:currentTimeMillis	()J
            //     417: ladd
            //     418: aload_0
            //     419: getfield 18	com/android/server/BackupManagerService$PerformInitializeTask:this$0	Lcom/android/server/BackupManagerService;
            //     422: getfield 178	com/android/server/BackupManagerService:mRunInitIntent	Landroid/app/PendingIntent;
            //     425: invokevirtual 184	android/app/AlarmManager:set	(IJLandroid/app/PendingIntent;)V
            //     428: goto -419 -> 9
            //     431: astore_1
            //     432: aload_0
            //     433: getfield 18	com/android/server/BackupManagerService$PerformInitializeTask:this$0	Lcom/android/server/BackupManagerService;
            //     436: getfield 77	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     439: invokevirtual 82	android/os/PowerManager$WakeLock:release	()V
            //     442: aload_1
            //     443: athrow
            //     444: astore 17
            //     446: aload 16
            //     448: monitorexit
            //     449: aload 17
            //     451: athrow
            //     452: aload_0
            //     453: getfield 18	com/android/server/BackupManagerService$PerformInitializeTask:this$0	Lcom/android/server/BackupManagerService;
            //     456: getfield 77	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     459: astore 4
            //     461: goto -369 -> 92
            //
            // Exception table:
            //     from	to	target	type
            //     0	78	81	android/os/RemoteException
            //     98	271	81	android/os/RemoteException
            //     292	295	81	android/os/RemoteException
            //     317	346	81	android/os/RemoteException
            //     359	428	81	android/os/RemoteException
            //     449	452	81	android/os/RemoteException
            //     271	292	287	finally
            //     0	78	295	java/lang/Exception
            //     98	271	295	java/lang/Exception
            //     292	295	295	java/lang/Exception
            //     317	346	295	java/lang/Exception
            //     359	428	295	java/lang/Exception
            //     449	452	295	java/lang/Exception
            //     0	78	431	finally
            //     98	271	431	finally
            //     292	295	431	finally
            //     296	305	431	finally
            //     317	346	431	finally
            //     359	428	431	finally
            //     449	452	431	finally
            //     346	359	444	finally
            //     446	449	444	finally
        }
    }

    class PerformClearTask
        implements Runnable
    {
        PackageInfo mPackage;
        IBackupTransport mTransport;

        PerformClearTask(IBackupTransport paramPackageInfo, PackageInfo arg3)
        {
            this.mTransport = paramPackageInfo;
            Object localObject;
            this.mPackage = localObject;
        }

        // ERROR //
        public void run()
        {
            // Byte code:
            //     0: new 33	java/io/File
            //     3: dup
            //     4: new 33	java/io/File
            //     7: dup
            //     8: aload_0
            //     9: getfield 19	com/android/server/BackupManagerService$PerformClearTask:this$0	Lcom/android/server/BackupManagerService;
            //     12: getfield 37	com/android/server/BackupManagerService:mBaseStateDir	Ljava/io/File;
            //     15: aload_0
            //     16: getfield 24	com/android/server/BackupManagerService$PerformClearTask:mTransport	Lcom/android/internal/backup/IBackupTransport;
            //     19: invokeinterface 43 1 0
            //     24: invokespecial 46	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
            //     27: aload_0
            //     28: getfield 26	com/android/server/BackupManagerService$PerformClearTask:mPackage	Landroid/content/pm/PackageInfo;
            //     31: getfield 52	android/content/pm/PackageInfo:packageName	Ljava/lang/String;
            //     34: invokespecial 46	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
            //     37: invokevirtual 56	java/io/File:delete	()Z
            //     40: pop
            //     41: aload_0
            //     42: getfield 24	com/android/server/BackupManagerService$PerformClearTask:mTransport	Lcom/android/internal/backup/IBackupTransport;
            //     45: aload_0
            //     46: getfield 26	com/android/server/BackupManagerService$PerformClearTask:mPackage	Landroid/content/pm/PackageInfo;
            //     49: invokeinterface 60 2 0
            //     54: pop
            //     55: aload_0
            //     56: getfield 24	com/android/server/BackupManagerService$PerformClearTask:mTransport	Lcom/android/internal/backup/IBackupTransport;
            //     59: invokeinterface 64 1 0
            //     64: pop
            //     65: aload_0
            //     66: getfield 19	com/android/server/BackupManagerService$PerformClearTask:this$0	Lcom/android/server/BackupManagerService;
            //     69: getfield 68	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     72: astore_3
            //     73: aload_3
            //     74: invokevirtual 73	android/os/PowerManager$WakeLock:release	()V
            //     77: return
            //     78: astore 8
            //     80: ldc 75
            //     82: new 77	java/lang/StringBuilder
            //     85: dup
            //     86: invokespecial 78	java/lang/StringBuilder:<init>	()V
            //     89: ldc 80
            //     91: invokevirtual 84	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     94: aload_0
            //     95: getfield 26	com/android/server/BackupManagerService$PerformClearTask:mPackage	Landroid/content/pm/PackageInfo;
            //     98: invokevirtual 87	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     101: invokevirtual 90	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     104: invokestatic 96	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     107: pop
            //     108: aload_0
            //     109: getfield 24	com/android/server/BackupManagerService$PerformClearTask:mTransport	Lcom/android/internal/backup/IBackupTransport;
            //     112: invokeinterface 64 1 0
            //     117: pop
            //     118: aload_0
            //     119: getfield 19	com/android/server/BackupManagerService$PerformClearTask:this$0	Lcom/android/server/BackupManagerService;
            //     122: getfield 68	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     125: astore_3
            //     126: goto -53 -> 73
            //     129: astore 5
            //     131: aload_0
            //     132: getfield 24	com/android/server/BackupManagerService$PerformClearTask:mTransport	Lcom/android/internal/backup/IBackupTransport;
            //     135: invokeinterface 64 1 0
            //     140: pop
            //     141: aload_0
            //     142: getfield 19	com/android/server/BackupManagerService$PerformClearTask:this$0	Lcom/android/server/BackupManagerService;
            //     145: getfield 68	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     148: invokevirtual 73	android/os/PowerManager$WakeLock:release	()V
            //     151: aload 5
            //     153: athrow
            //     154: astore_1
            //     155: aload_0
            //     156: getfield 24	com/android/server/BackupManagerService$PerformClearTask:mTransport	Lcom/android/internal/backup/IBackupTransport;
            //     159: invokeinterface 64 1 0
            //     164: pop
            //     165: aload_0
            //     166: getfield 19	com/android/server/BackupManagerService$PerformClearTask:this$0	Lcom/android/server/BackupManagerService;
            //     169: getfield 68	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     172: astore_3
            //     173: goto -100 -> 73
            //     176: astore 14
            //     178: goto -113 -> 65
            //     181: astore 10
            //     183: goto -65 -> 118
            //     186: astore 6
            //     188: goto -47 -> 141
            //     191: astore_2
            //     192: goto -27 -> 165
            //
            // Exception table:
            //     from	to	target	type
            //     0	55	78	java/lang/Exception
            //     0	55	129	finally
            //     80	108	129	finally
            //     0	55	154	android/os/RemoteException
            //     55	65	176	android/os/RemoteException
            //     108	118	181	android/os/RemoteException
            //     131	141	186	android/os/RemoteException
            //     155	165	191	android/os/RemoteException
        }
    }

    class PerformRestoreTask
        implements BackupManagerService.BackupRestoreTask
    {
        private List<PackageInfo> mAgentPackages;
        private ParcelFileDescriptor mBackupData;
        private File mBackupDataName;
        private int mCount;
        private PackageInfo mCurrentPackage;
        private BackupManagerService.RestoreState mCurrentState = BackupManagerService.RestoreState.INITIAL;
        private HashSet<String> mFilterSet;
        private boolean mFinished = false;
        private boolean mNeedFullBackup;
        private ParcelFileDescriptor mNewState;
        private File mNewStateName;
        private IRestoreObserver mObserver;
        private PackageManagerBackupAgent mPmAgent = null;
        private int mPmToken;
        private ArrayList<PackageInfo> mRestorePackages;
        private File mSavedStateName;
        private long mStartRealtime;
        private File mStateDir;
        private int mStatus;
        private PackageInfo mTargetPackage;
        private long mToken;
        private IBackupTransport mTransport;

        PerformRestoreTask(IBackupTransport paramIRestoreObserver, IRestoreObserver paramLong, long arg4, PackageInfo paramInt, int paramBoolean, boolean paramArrayOfString, String[] arg9)
        {
            this.mTransport = paramIRestoreObserver;
            this.mObserver = paramLong;
            this.mToken = ???;
            this.mTargetPackage = paramInt;
            this.mPmToken = paramBoolean;
            this.mNeedFullBackup = paramArrayOfString;
            Object localObject1;
            if (localObject1 != null)
            {
                this.mFilterSet = new HashSet();
                int i = localObject1.length;
                for (int j = 0; j < i; j++)
                {
                    Object localObject2 = localObject1[j];
                    this.mFilterSet.add(localObject2);
                }
            }
            this.mFilterSet = null;
            try
            {
                this.mStateDir = new File(BackupManagerService.this.mBaseStateDir, paramIRestoreObserver.transportDirName());
                label140: return;
            }
            catch (RemoteException localRemoteException)
            {
                break label140;
            }
        }

        void agentCleanup()
        {
            this.mBackupDataName.delete();
            try
            {
                if (this.mBackupData != null)
                    this.mBackupData.close();
                try
                {
                    label22: if (this.mNewState != null)
                        this.mNewState.close();
                    label36: this.mNewState = null;
                    this.mBackupData = null;
                    this.mNewStateName.delete();
                    if (this.mCurrentPackage.applicationInfo != null);
                    try
                    {
                        BackupManagerService.this.mActivityManager.unbindBackupAgent(this.mCurrentPackage.applicationInfo);
                        if ((this.mTargetPackage == null) && ((0x10000 & this.mCurrentPackage.applicationInfo.flags) != 0))
                        {
                            Slog.d("BackupManagerService", "Restore complete, killing host process of " + this.mCurrentPackage.applicationInfo.processName);
                            BackupManagerService.this.mActivityManager.killApplicationProcess(this.mCurrentPackage.applicationInfo.processName, this.mCurrentPackage.applicationInfo.uid);
                        }
                        label172: BackupManagerService.this.mBackupHandler.removeMessages(7, this);
                        synchronized (BackupManagerService.this.mCurrentOpLock)
                        {
                            BackupManagerService.this.mCurrentOperations.clear();
                            return;
                        }
                    }
                    catch (RemoteException localRemoteException)
                    {
                        break label172;
                    }
                }
                catch (IOException localIOException2)
                {
                    break label36;
                }
            }
            catch (IOException localIOException1)
            {
                break label22;
            }
        }

        void agentErrorCleanup()
        {
            BackupManagerService.this.clearApplicationDataSynchronous(this.mCurrentPackage.packageName);
            agentCleanup();
        }

        void beginRestore()
        {
            BackupManagerService.this.mBackupHandler.removeMessages(8);
            this.mStatus = 1;
            try
            {
                Object[] arrayOfObject = new Object[2];
                arrayOfObject[0] = this.mTransport.transportDirName();
                arrayOfObject[1] = Long.valueOf(this.mToken);
                EventLog.writeEvent(2830, arrayOfObject);
                this.mRestorePackages = new ArrayList();
                PackageInfo localPackageInfo1 = new PackageInfo();
                localPackageInfo1.packageName = "@pm@";
                this.mRestorePackages.add(localPackageInfo1);
                this.mAgentPackages = BackupManagerService.this.allAgentPackages();
                if (this.mTargetPackage == null)
                {
                    if (this.mFilterSet != null)
                    {
                        i = -1 + this.mAgentPackages.size();
                        if (i >= 0)
                        {
                            PackageInfo localPackageInfo2 = (PackageInfo)this.mAgentPackages.get(i);
                            if (this.mFilterSet.contains(localPackageInfo2.packageName))
                                break label285;
                            this.mAgentPackages.remove(i);
                            break label285;
                        }
                    }
                    this.mRestorePackages.addAll(this.mAgentPackages);
                    IRestoreObserver localIRestoreObserver = this.mObserver;
                    if (localIRestoreObserver == null);
                }
            }
            catch (RemoteException localRemoteException1)
            {
                while (true)
                {
                    int i;
                    try
                    {
                        this.mObserver.restoreStarting(this.mRestorePackages.size());
                        this.mStatus = 0;
                        executeNextState(BackupManagerService.RestoreState.DOWNLOAD_DATA);
                        return;
                        this.mRestorePackages.add(this.mTargetPackage);
                        continue;
                        localRemoteException1 = localRemoteException1;
                        Slog.e("BackupManagerService", "Error communicating with transport for restore");
                        executeNextState(BackupManagerService.RestoreState.FINAL);
                        continue;
                    }
                    catch (RemoteException localRemoteException2)
                    {
                        Slog.d("BackupManagerService", "Restore observer died at restoreStarting");
                        this.mObserver = null;
                        continue;
                    }
                    label285: i--;
                }
            }
        }

        void downloadRestoreData()
        {
            try
            {
                this.mStatus = this.mTransport.startRestore(this.mToken, (PackageInfo[])this.mRestorePackages.toArray(new PackageInfo[0]));
                if (this.mStatus != 0)
                {
                    Slog.e("BackupManagerService", "Error starting restore operation");
                    EventLog.writeEvent(2831, new Object[0]);
                    executeNextState(BackupManagerService.RestoreState.FINAL);
                    return;
                }
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                {
                    Slog.e("BackupManagerService", "Error communicating with transport for restore");
                    EventLog.writeEvent(2831, new Object[0]);
                    this.mStatus = 1;
                    executeNextState(BackupManagerService.RestoreState.FINAL);
                    continue;
                    executeNextState(BackupManagerService.RestoreState.PM_METADATA);
                }
            }
        }

        public void execute()
        {
            switch (BackupManagerService.4.$SwitchMap$com$android$server$BackupManagerService$RestoreState[this.mCurrentState.ordinal()])
            {
            default:
            case 1:
            case 2:
            case 3:
            case 4:
                while (true)
                {
                    return;
                    beginRestore();
                    continue;
                    downloadRestoreData();
                    continue;
                    restorePmMetadata();
                    continue;
                    restoreNextAgent();
                }
            case 5:
            }
            if (!this.mFinished)
                finalizeRestore();
            while (true)
            {
                this.mFinished = true;
                break;
                Slog.e("BackupManagerService", "Duplicate finish");
            }
        }

        void executeNextState(BackupManagerService.RestoreState paramRestoreState)
        {
            this.mCurrentState = paramRestoreState;
            Message localMessage = BackupManagerService.this.mBackupHandler.obtainMessage(20, this);
            BackupManagerService.this.mBackupHandler.sendMessage(localMessage);
        }

        void finalizeRestore()
        {
            try
            {
                this.mTransport.finishRestore();
                if (this.mObserver == null);
            }
            catch (RemoteException localRemoteException3)
            {
                try
                {
                    this.mObserver.restoreFinished(this.mStatus);
                    if ((this.mTargetPackage == null) && (this.mPmAgent != null))
                    {
                        BackupManagerService.this.mAncestralPackages = this.mPmAgent.getRestoredPackages();
                        BackupManagerService.this.mAncestralToken = this.mToken;
                        BackupManagerService.this.writeRestoreTokens();
                    }
                    if (this.mPmToken <= 0);
                }
                catch (RemoteException localRemoteException3)
                {
                    try
                    {
                        while (true)
                        {
                            BackupManagerService.this.mPackageManagerBinder.finishPackageInstall(this.mPmToken);
                            label98: BackupManagerService.this.mBackupHandler.removeMessages(8);
                            BackupManagerService.this.mBackupHandler.sendEmptyMessageDelayed(8, 60000L);
                            Slog.i("BackupManagerService", "Restore complete.");
                            BackupManagerService.this.mWakelock.release();
                            return;
                            localRemoteException1 = localRemoteException1;
                            Slog.e("BackupManagerService", "Error finishing restore", localRemoteException1);
                        }
                        localRemoteException3 = localRemoteException3;
                        Slog.d("BackupManagerService", "Restore observer died at restoreFinished");
                    }
                    catch (RemoteException localRemoteException2)
                    {
                        break label98;
                    }
                }
            }
        }

        public void handleTimeout()
        {
            Slog.e("BackupManagerService", "Timeout restoring application " + this.mCurrentPackage.packageName);
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = this.mCurrentPackage.packageName;
            arrayOfObject[1] = "restore timeout";
            EventLog.writeEvent(2832, arrayOfObject);
            agentErrorCleanup();
            executeNextState(BackupManagerService.RestoreState.RUNNING_QUEUE);
        }

        void initiateOneRestore(PackageInfo paramPackageInfo, int paramInt, IBackupAgent paramIBackupAgent, boolean paramBoolean)
        {
            this.mCurrentPackage = paramPackageInfo;
            String str = paramPackageInfo.packageName;
            Slog.d("BackupManagerService", "initiateOneRestore packageName=" + str);
            this.mBackupDataName = new File(BackupManagerService.this.mDataDir, str + ".restore");
            this.mNewStateName = new File(this.mStateDir, str + ".new");
            this.mSavedStateName = new File(this.mStateDir, str);
            int i = BackupManagerService.this.generateToken();
            try
            {
                this.mBackupData = ParcelFileDescriptor.open(this.mBackupDataName, 1006632960);
                if (this.mTransport.getRestoreData(this.mBackupData) != 0)
                {
                    Slog.e("BackupManagerService", "Error getting restore data for " + str);
                    EventLog.writeEvent(2831, new Object[0]);
                    this.mBackupData.close();
                    this.mBackupDataName.delete();
                    executeNextState(BackupManagerService.RestoreState.FINAL);
                }
                else
                {
                    this.mBackupData.close();
                    this.mBackupData = ParcelFileDescriptor.open(this.mBackupDataName, 268435456);
                    this.mNewState = ParcelFileDescriptor.open(this.mNewStateName, 1006632960);
                    BackupManagerService.this.prepareOperationTimeout(i, 60000L, this);
                    paramIBackupAgent.doRestore(this.mBackupData, paramInt, this.mNewState, i, BackupManagerService.this.mBackupManagerBinder);
                }
            }
            catch (Exception localException)
            {
                Slog.e("BackupManagerService", "Unable to call app for restore: " + str, localException);
                Object[] arrayOfObject = new Object[2];
                arrayOfObject[0] = str;
                arrayOfObject[1] = localException.toString();
                EventLog.writeEvent(2832, arrayOfObject);
                agentErrorCleanup();
                executeNextState(BackupManagerService.RestoreState.RUNNING_QUEUE);
            }
        }

        public void operationComplete()
        {
            int i = (int)this.mBackupDataName.length();
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = this.mCurrentPackage.packageName;
            arrayOfObject[1] = Integer.valueOf(i);
            EventLog.writeEvent(2833, arrayOfObject);
            agentCleanup();
            executeNextState(BackupManagerService.RestoreState.RUNNING_QUEUE);
        }

        void restoreNextAgent()
        {
            String str1;
            try
            {
                str1 = this.mTransport.nextRestorePackage();
                if (str1 == null)
                {
                    Slog.e("BackupManagerService", "Error getting next restore package");
                    EventLog.writeEvent(2831, new Object[0]);
                    executeNextState(BackupManagerService.RestoreState.FINAL);
                }
                else if (str1.equals(""))
                {
                    Slog.v("BackupManagerService", "No next package, finishing restore");
                    int i = (int)(SystemClock.elapsedRealtime() - this.mStartRealtime);
                    Object[] arrayOfObject6 = new Object[2];
                    arrayOfObject6[0] = Integer.valueOf(this.mCount);
                    arrayOfObject6[1] = Integer.valueOf(i);
                    EventLog.writeEvent(2834, arrayOfObject6);
                    executeNextState(BackupManagerService.RestoreState.FINAL);
                }
            }
            catch (RemoteException localRemoteException1)
            {
                Slog.e("BackupManagerService", "Unable to fetch restore data from transport");
                this.mStatus = 1;
                executeNextState(BackupManagerService.RestoreState.FINAL);
            }
            IRestoreObserver localIRestoreObserver = this.mObserver;
            if (localIRestoreObserver != null);
            try
            {
                this.mObserver.onUpdate(this.mCount, str1);
                localMetadata = this.mPmAgent.getRestoredMetadata(str1);
                if (localMetadata == null)
                {
                    Slog.e("BackupManagerService", "Missing metadata for " + str1);
                    Object[] arrayOfObject5 = new Object[2];
                    arrayOfObject5[0] = str1;
                    arrayOfObject5[1] = "Package metadata missing";
                    EventLog.writeEvent(2832, arrayOfObject5);
                    executeNextState(BackupManagerService.RestoreState.RUNNING_QUEUE);
                }
            }
            catch (RemoteException localRemoteException2)
            {
                PackageManagerBackupAgent.Metadata localMetadata;
                while (true)
                {
                    Slog.d("BackupManagerService", "Restore observer died in onUpdate");
                    this.mObserver = null;
                }
                PackageInfo localPackageInfo;
                try
                {
                    localPackageInfo = BackupManagerService.this.mPackageManager.getPackageInfo(str1, 64);
                    if (localMetadata.versionCode <= localPackageInfo.versionCode)
                        break label522;
                    if ((0x20000 & localPackageInfo.applicationInfo.flags) == 0)
                    {
                        String str2 = "Version " + localMetadata.versionCode + " > installed version " + localPackageInfo.versionCode;
                        Slog.w("BackupManagerService", "Package " + str1 + ": " + str2);
                        Object[] arrayOfObject4 = new Object[2];
                        arrayOfObject4[0] = str1;
                        arrayOfObject4[1] = str2;
                        EventLog.writeEvent(2832, arrayOfObject4);
                        executeNextState(BackupManagerService.RestoreState.RUNNING_QUEUE);
                    }
                }
                catch (PackageManager.NameNotFoundException localNameNotFoundException)
                {
                    Slog.e("BackupManagerService", "Invalid package restoring data", localNameNotFoundException);
                    Object[] arrayOfObject1 = new Object[2];
                    arrayOfObject1[0] = str1;
                    arrayOfObject1[1] = "Package missing on device";
                    EventLog.writeEvent(2832, arrayOfObject1);
                    executeNextState(BackupManagerService.RestoreState.RUNNING_QUEUE);
                }
                Slog.v("BackupManagerService", "Version " + localMetadata.versionCode + " > installed " + localPackageInfo.versionCode + " but restoreAnyVersion");
                label522: if (!BackupManagerService.this.signaturesMatch(localMetadata.signatures, localPackageInfo))
                {
                    Slog.w("BackupManagerService", "Signature mismatch restoring " + str1);
                    Object[] arrayOfObject3 = new Object[2];
                    arrayOfObject3[0] = str1;
                    arrayOfObject3[1] = "Signature mismatch";
                    EventLog.writeEvent(2832, arrayOfObject3);
                    executeNextState(BackupManagerService.RestoreState.RUNNING_QUEUE);
                }
                else
                {
                    Slog.v("BackupManagerService", "Package " + str1 + " restore version [" + localMetadata.versionCode + "] is compatible with installed version [" + localPackageInfo.versionCode + "]");
                    IBackupAgent localIBackupAgent = BackupManagerService.this.bindToAgentSynchronous(localPackageInfo.applicationInfo, 0);
                    if (localIBackupAgent == null)
                    {
                        Slog.w("BackupManagerService", "Can't find backup agent for " + str1);
                        Object[] arrayOfObject2 = new Object[2];
                        arrayOfObject2[0] = str1;
                        arrayOfObject2[1] = "Restore agent missing";
                        EventLog.writeEvent(2832, arrayOfObject2);
                        executeNextState(BackupManagerService.RestoreState.RUNNING_QUEUE);
                    }
                    else
                    {
                        try
                        {
                            initiateOneRestore(localPackageInfo, localMetadata.versionCode, localIBackupAgent, this.mNeedFullBackup);
                            this.mCount = (1 + this.mCount);
                        }
                        catch (Exception localException)
                        {
                            Slog.e("BackupManagerService", "Error when attempting restore: " + localException.toString());
                            agentErrorCleanup();
                            executeNextState(BackupManagerService.RestoreState.RUNNING_QUEUE);
                        }
                    }
                }
            }
        }

        void restorePmMetadata()
        {
            String str;
            try
            {
                str = this.mTransport.nextRestorePackage();
                if (str == null)
                {
                    Slog.e("BackupManagerService", "Error getting first restore package");
                    EventLog.writeEvent(2831, new Object[0]);
                    this.mStatus = 1;
                    executeNextState(BackupManagerService.RestoreState.FINAL);
                }
                else if (str.equals(""))
                {
                    Slog.i("BackupManagerService", "No restore data available");
                    int i = (int)(SystemClock.elapsedRealtime() - this.mStartRealtime);
                    Object[] arrayOfObject3 = new Object[2];
                    arrayOfObject3[0] = Integer.valueOf(0);
                    arrayOfObject3[1] = Integer.valueOf(i);
                    EventLog.writeEvent(2834, arrayOfObject3);
                    this.mStatus = 0;
                    executeNextState(BackupManagerService.RestoreState.FINAL);
                }
            }
            catch (RemoteException localRemoteException)
            {
                Slog.e("BackupManagerService", "Error communicating with transport for restore");
                EventLog.writeEvent(2831, new Object[0]);
                this.mStatus = 1;
                BackupManagerService.this.mBackupHandler.removeMessages(20, this);
                executeNextState(BackupManagerService.RestoreState.FINAL);
            }
            if (!str.equals("@pm@"))
            {
                Slog.e("BackupManagerService", "Expected restore data for \"@pm@\", found only \"" + str + "\"");
                Object[] arrayOfObject2 = new Object[2];
                arrayOfObject2[0] = "@pm@";
                arrayOfObject2[1] = "Package manager data missing";
                EventLog.writeEvent(2832, arrayOfObject2);
                executeNextState(BackupManagerService.RestoreState.FINAL);
            }
            else
            {
                PackageInfo localPackageInfo = new PackageInfo();
                localPackageInfo.packageName = "@pm@";
                this.mPmAgent = new PackageManagerBackupAgent(BackupManagerService.this.mPackageManager, this.mAgentPackages);
                initiateOneRestore(localPackageInfo, 0, IBackupAgent.Stub.asInterface(this.mPmAgent.onBind()), this.mNeedFullBackup);
                if (!this.mPmAgent.hasMetadata())
                {
                    Slog.e("BackupManagerService", "No restore metadata available, so not restoring settings");
                    Object[] arrayOfObject1 = new Object[2];
                    arrayOfObject1[0] = "@pm@";
                    arrayOfObject1[1] = "Package manager restore metadata missing";
                    EventLog.writeEvent(2832, arrayOfObject1);
                    this.mStatus = 1;
                    BackupManagerService.this.mBackupHandler.removeMessages(20, this);
                    executeNextState(BackupManagerService.RestoreState.FINAL);
                }
            }
        }

        class RestoreRequest
        {
            public PackageInfo app;
            public int storedAppVersion;

            RestoreRequest(PackageInfo paramInt, int arg3)
            {
                this.app = paramInt;
                int i;
                this.storedAppVersion = i;
            }
        }
    }

    static enum RestoreState
    {
        static
        {
            DOWNLOAD_DATA = new RestoreState("DOWNLOAD_DATA", 1);
            PM_METADATA = new RestoreState("PM_METADATA", 2);
            RUNNING_QUEUE = new RestoreState("RUNNING_QUEUE", 3);
            FINAL = new RestoreState("FINAL", 4);
            RestoreState[] arrayOfRestoreState = new RestoreState[5];
            arrayOfRestoreState[0] = INITIAL;
            arrayOfRestoreState[1] = DOWNLOAD_DATA;
            arrayOfRestoreState[2] = PM_METADATA;
            arrayOfRestoreState[3] = RUNNING_QUEUE;
            arrayOfRestoreState[4] = FINAL;
        }
    }

    class PerformFullRestoreTask
        implements Runnable
    {
        IBackupAgent mAgent;
        String mAgentPackage;
        long mBytes;
        final HashSet<String> mClearedPackages = new HashSet();
        String mCurrentPassword;
        String mDecryptPassword;
        final RestoreDeleteObserver mDeleteObserver = new RestoreDeleteObserver();
        ParcelFileDescriptor mInputFile;
        final RestoreInstallObserver mInstallObserver = new RestoreInstallObserver();
        AtomicBoolean mLatchObject;
        final HashMap<String, Signature[]> mManifestSignatures = new HashMap();
        IFullBackupRestoreObserver mObserver;
        final HashMap<String, String> mPackageInstallers = new HashMap();
        final HashMap<String, BackupManagerService.RestorePolicy> mPackagePolicies = new HashMap();
        ParcelFileDescriptor[] mPipes = null;
        ApplicationInfo mTargetApp;

        PerformFullRestoreTask(ParcelFileDescriptor paramString1, String paramString2, String paramIFullBackupRestoreObserver, IFullBackupRestoreObserver paramAtomicBoolean, AtomicBoolean arg6)
        {
            this.mInputFile = paramString1;
            this.mCurrentPassword = paramString2;
            this.mDecryptPassword = paramIFullBackupRestoreObserver;
            this.mObserver = paramAtomicBoolean;
            Object localObject;
            this.mLatchObject = localObject;
            this.mAgent = null;
            this.mAgentPackage = null;
            this.mTargetApp = null;
            this.mClearedPackages.add("android");
            this.mClearedPackages.add("com.android.providers.settings");
        }

        private void HEXLOG(byte[] paramArrayOfByte)
        {
            int i = 0;
            int j = paramArrayOfByte.length;
            StringBuilder localStringBuilder = new StringBuilder(64);
            while (j > 0)
            {
                Object[] arrayOfObject1 = new Object[1];
                arrayOfObject1[0] = Integer.valueOf(i);
                localStringBuilder.append(String.format("%04x     ", arrayOfObject1));
                if (j > 16);
                for (int k = 16; ; k = j)
                    for (int m = 0; m < k; m++)
                    {
                        Object[] arrayOfObject2 = new Object[1];
                        arrayOfObject2[0] = Byte.valueOf(paramArrayOfByte[(i + m)]);
                        localStringBuilder.append(String.format("%02x ", arrayOfObject2));
                    }
                Slog.i("hexdump", localStringBuilder.toString());
                localStringBuilder.setLength(0);
                j -= k;
                i += k;
            }
        }

        InputStream decodeAesHeaderAndInitialize(String paramString, InputStream paramInputStream)
        {
            Object localObject = null;
            try
            {
                if (paramString.equals("AES-256"))
                {
                    String str1 = readHeaderLine(paramInputStream);
                    byte[] arrayOfByte1 = BackupManagerService.this.hexToByteArray(str1);
                    String str2 = readHeaderLine(paramInputStream);
                    byte[] arrayOfByte2 = BackupManagerService.this.hexToByteArray(str2);
                    int i = Integer.parseInt(readHeaderLine(paramInputStream));
                    String str3 = readHeaderLine(paramInputStream);
                    String str4 = readHeaderLine(paramInputStream);
                    Cipher localCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
                    SecretKey localSecretKey = BackupManagerService.this.buildPasswordKey(this.mDecryptPassword, arrayOfByte1, i);
                    IvParameterSpec localIvParameterSpec1 = new IvParameterSpec(BackupManagerService.this.hexToByteArray(str3));
                    localCipher.init(2, new SecretKeySpec(localSecretKey.getEncoded(), "AES"), localIvParameterSpec1);
                    byte[] arrayOfByte3 = localCipher.doFinal(BackupManagerService.this.hexToByteArray(str4));
                    int j = 0 + 1;
                    int k = arrayOfByte3[0];
                    byte[] arrayOfByte4 = Arrays.copyOfRange(arrayOfByte3, j, k + 1);
                    int m = k + 1;
                    int n = m + 1;
                    int i1 = arrayOfByte3[m];
                    byte[] arrayOfByte5 = Arrays.copyOfRange(arrayOfByte3, n, n + i1);
                    int i2 = n + i1;
                    int i3 = i2 + 1;
                    byte[] arrayOfByte6 = Arrays.copyOfRange(arrayOfByte3, i3, i3 + arrayOfByte3[i2]);
                    if (Arrays.equals(BackupManagerService.this.makeKeyChecksum(arrayOfByte5, arrayOfByte2, i), arrayOfByte6))
                    {
                        IvParameterSpec localIvParameterSpec2 = new IvParameterSpec(arrayOfByte4);
                        SecretKeySpec localSecretKeySpec = new SecretKeySpec(arrayOfByte5, "AES");
                        localCipher.init(2, localSecretKeySpec, localIvParameterSpec2);
                        CipherInputStream localCipherInputStream = new CipherInputStream(paramInputStream, localCipher);
                        localObject = localCipherInputStream;
                    }
                    else
                    {
                        Slog.w("BackupManagerService", "Incorrect password");
                    }
                }
            }
            catch (InvalidAlgorithmParameterException localInvalidAlgorithmParameterException)
            {
                Slog.e("BackupManagerService", "Needed parameter spec unavailable!", localInvalidAlgorithmParameterException);
                break label462;
                Slog.w("BackupManagerService", "Unsupported encryption method: " + paramString);
            }
            catch (BadPaddingException localBadPaddingException)
            {
                Slog.w("BackupManagerService", "Incorrect password");
            }
            catch (IllegalBlockSizeException localIllegalBlockSizeException)
            {
                Slog.w("BackupManagerService", "Invalid block size in master key");
            }
            catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
            {
                Slog.e("BackupManagerService", "Needed decryption algorithm unavailable!");
            }
            catch (NoSuchPaddingException localNoSuchPaddingException)
            {
                Slog.e("BackupManagerService", "Needed padding mechanism unavailable!");
            }
            catch (InvalidKeyException localInvalidKeyException)
            {
                Slog.w("BackupManagerService", "Illegal password; aborting");
            }
            catch (NumberFormatException localNumberFormatException)
            {
                Slog.w("BackupManagerService", "Can't parse restore data header");
            }
            catch (IOException localIOException)
            {
                Slog.w("BackupManagerService", "Can't read input header");
            }
            label462: return localObject;
        }

        void dumpFileMetadata(BackupManagerService.FileMetadata paramFileMetadata)
        {
            char c1 = 'x';
            char c2 = 'w';
            char c3 = 'r';
            StringBuilder localStringBuilder = new StringBuilder(128);
            char c4;
            char c5;
            label59: char c6;
            label83: char c7;
            label107: char c8;
            label132: char c9;
            label156: char c10;
            if (paramFileMetadata.type == 2)
            {
                c4 = 'd';
                localStringBuilder.append(c4);
                if ((0x100 & paramFileMetadata.mode) == 0L)
                    break label379;
                c5 = c3;
                localStringBuilder.append(c5);
                if ((0x80 & paramFileMetadata.mode) == 0L)
                    break label386;
                c6 = c2;
                localStringBuilder.append(c6);
                if ((0x40 & paramFileMetadata.mode) == 0L)
                    break label393;
                c7 = c1;
                localStringBuilder.append(c7);
                if ((0x20 & paramFileMetadata.mode) == 0L)
                    break label400;
                c8 = c3;
                localStringBuilder.append(c8);
                if ((0x10 & paramFileMetadata.mode) == 0L)
                    break label407;
                c9 = c2;
                localStringBuilder.append(c9);
                if ((0x8 & paramFileMetadata.mode) == 0L)
                    break label414;
                c10 = c1;
                label180: localStringBuilder.append(c10);
                if ((0x4 & paramFileMetadata.mode) == 0L)
                    break label421;
                label201: localStringBuilder.append(c3);
                if ((0x2 & paramFileMetadata.mode) == 0L)
                    break label428;
                label222: localStringBuilder.append(c2);
                if ((1L & paramFileMetadata.mode) == 0L)
                    break label434;
            }
            while (true)
            {
                localStringBuilder.append(c1);
                Object[] arrayOfObject = new Object[1];
                arrayOfObject[0] = Long.valueOf(paramFileMetadata.size);
                localStringBuilder.append(String.format(" %9d ", arrayOfObject));
                Date localDate = new Date(paramFileMetadata.mtime);
                localStringBuilder.append(new SimpleDateFormat("MMM dd kk:mm:ss ").format(localDate));
                localStringBuilder.append(paramFileMetadata.packageName);
                localStringBuilder.append(" :: ");
                localStringBuilder.append(paramFileMetadata.domain);
                localStringBuilder.append(" :: ");
                localStringBuilder.append(paramFileMetadata.path);
                Slog.i("BackupManagerService", localStringBuilder.toString());
                return;
                c4 = '-';
                break;
                label379: c5 = '-';
                break label59;
                label386: c6 = '-';
                break label83;
                label393: c7 = '-';
                break label107;
                label400: c8 = '-';
                break label132;
                label407: c9 = '-';
                break label156;
                label414: c10 = '-';
                break label180;
                label421: c3 = '-';
                break label201;
                label428: c2 = '-';
                break label222;
                label434: c1 = '-';
            }
        }

        int extractLine(byte[] paramArrayOfByte, int paramInt, String[] paramArrayOfString)
            throws IOException
        {
            int i = paramArrayOfByte.length;
            if (paramInt >= i)
                throw new IOException("Incomplete data");
            for (int j = paramInt; ; j++)
                if ((j >= i) || (paramArrayOfByte[j] == 10))
                {
                    paramArrayOfString[0] = new String(paramArrayOfByte, paramInt, j - paramInt);
                    return j + 1;
                }
        }

        long extractRadix(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3)
            throws IOException
        {
            long l = 0L;
            int i = paramInt1 + paramInt2;
            for (int j = paramInt1; ; j++)
            {
                int k;
                if (j < i)
                {
                    k = paramArrayOfByte[j];
                    if ((k != 0) && (k != 32));
                }
                else
                {
                    return l;
                }
                if ((k < 48) || (k > -1 + (paramInt3 + 48)))
                    throw new IOException("Invalid number in header: '" + (char)k + "' for radix " + paramInt3);
                l = l * paramInt3 + (k - 48);
            }
        }

        String extractString(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
            throws IOException
        {
            int i = paramInt1 + paramInt2;
            for (int j = paramInt1; (j < i) && (paramArrayOfByte[j] != 0); j++);
            return new String(paramArrayOfByte, paramInt1, j - paramInt1, "US-ASCII");
        }

        @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
        boolean installApk(BackupManagerService.FileMetadata paramFileMetadata, String paramString, InputStream paramInputStream)
        {
            boolean bool = true;
            Slog.d("BackupManagerService", "Installing from backup: " + paramFileMetadata.packageName);
            File localFile = new File(BackupManagerService.this.mDataDir, paramFileMetadata.packageName);
            while (true)
            {
                long l1;
                try
                {
                    FileOutputStream localFileOutputStream = new FileOutputStream(localFile);
                    byte[] arrayOfByte = new byte[32768];
                    l1 = paramFileMetadata.size;
                    if (l1 > 0L)
                    {
                        if (arrayOfByte.length >= l1)
                            break label584;
                        l2 = arrayOfByte.length;
                        int j = paramInputStream.read(arrayOfByte, 0, (int)l2);
                        if (j >= 0)
                        {
                            this.mBytes += j;
                            localFileOutputStream.write(arrayOfByte, 0, j);
                            l1 -= j;
                            continue;
                        }
                    }
                    localFileOutputStream.close();
                    localFile.setReadable(true, false);
                    Uri localUri = Uri.fromFile(localFile);
                    this.mInstallObserver.reset();
                    BackupManagerService.this.mPackageManager.installPackage(localUri, this.mInstallObserver, 34, paramString);
                    this.mInstallObserver.waitForCompletion();
                    if (this.mInstallObserver.getResult() != 1)
                    {
                        Object localObject2 = this.mPackagePolicies.get(paramFileMetadata.packageName);
                        BackupManagerService.RestorePolicy localRestorePolicy = BackupManagerService.RestorePolicy.ACCEPT;
                        if (localObject2 != localRestorePolicy)
                            bool = false;
                        return bool;
                    }
                    i = 0;
                    if (!this.mInstallObserver.mPackageName.equals(paramFileMetadata.packageName))
                    {
                        Slog.w("BackupManagerService", "Restore stream claimed to include apk for " + paramFileMetadata.packageName + " but apk was really " + this.mInstallObserver.mPackageName);
                        bool = false;
                        i = 1;
                        if (i == 0)
                            continue;
                        this.mDeleteObserver.reset();
                        BackupManagerService.this.mPackageManager.deletePackage(this.mInstallObserver.mPackageName, this.mDeleteObserver, 0);
                        this.mDeleteObserver.waitForCompletion();
                        continue;
                    }
                }
                catch (IOException localIOException)
                {
                    int i;
                    Slog.e("BackupManagerService", "Unable to transcribe restored apk for install");
                    bool = false;
                    continue;
                    try
                    {
                        PackageInfo localPackageInfo = BackupManagerService.this.mPackageManager.getPackageInfo(paramFileMetadata.packageName, 64);
                        if ((0x8000 & localPackageInfo.applicationInfo.flags) == 0)
                        {
                            Slog.w("BackupManagerService", "Restore stream contains apk of package " + paramFileMetadata.packageName + " but it disallows backup/restore");
                            bool = false;
                            continue;
                        }
                        Signature[] arrayOfSignature = (Signature[])this.mManifestSignatures.get(paramFileMetadata.packageName);
                        if (BackupManagerService.this.signaturesMatch(arrayOfSignature, localPackageInfo))
                            continue;
                        Slog.w("BackupManagerService", "Installed app " + paramFileMetadata.packageName + " signatures do not match restore manifest");
                        bool = false;
                        i = 1;
                    }
                    catch (PackageManager.NameNotFoundException localNameNotFoundException)
                    {
                        Slog.w("BackupManagerService", "Install of package " + paramFileMetadata.packageName + " succeeded but now not found");
                        bool = false;
                    }
                    continue;
                }
                finally
                {
                    localFile.delete();
                }
                label584: long l2 = l1;
            }
        }

        BackupManagerService.RestorePolicy readAppManifest(BackupManagerService.FileMetadata paramFileMetadata, InputStream paramInputStream)
            throws IOException
        {
            if (paramFileMetadata.size > 65536L)
                throw new IOException("Restore manifest too big; corrupt? size=" + paramFileMetadata.size);
            byte[] arrayOfByte = new byte[(int)paramFileMetadata.size];
            BackupManagerService.RestorePolicy localRestorePolicy;
            String[] arrayOfString;
            if (readExactly(paramInputStream, arrayOfByte, 0, (int)paramFileMetadata.size) == paramFileMetadata.size)
            {
                this.mBytes += paramFileMetadata.size;
                localRestorePolicy = BackupManagerService.RestorePolicy.IGNORE;
                arrayOfString = new String[1];
            }
            try
            {
                int i = extractLine(arrayOfByte, 0, arrayOfString);
                j = Integer.parseInt(arrayOfString[0]);
                if (j == 1)
                {
                    int k = extractLine(arrayOfByte, i, arrayOfString);
                    str1 = arrayOfString[0];
                    if (str1.equals(paramFileMetadata.packageName))
                    {
                        int m = extractLine(arrayOfByte, k, arrayOfString);
                        int n = Integer.parseInt(arrayOfString[0]);
                        int i1 = extractLine(arrayOfByte, m, arrayOfString);
                        Integer.parseInt(arrayOfString[0]);
                        int i2 = extractLine(arrayOfByte, i1, arrayOfString);
                        if (arrayOfString[0].length() > 0);
                        boolean bool;
                        Signature[] arrayOfSignature;
                        for (String str2 = arrayOfString[0]; ; str2 = null)
                        {
                            paramFileMetadata.installerPackageName = str2;
                            int i3 = extractLine(arrayOfByte, i2, arrayOfString);
                            bool = arrayOfString[0].equals("1");
                            int i4 = extractLine(arrayOfByte, i3, arrayOfString);
                            int i5 = Integer.parseInt(arrayOfString[0]);
                            if (i5 <= 0)
                                break label755;
                            arrayOfSignature = new Signature[i5];
                            for (int i6 = 0; i6 < i5; i6++)
                            {
                                i4 = extractLine(arrayOfByte, i4, arrayOfString);
                                arrayOfSignature[i6] = new Signature(arrayOfString[0]);
                            }
                            throw new IOException("Unexpected EOF in manifest");
                        }
                        this.mManifestSignatures.put(paramFileMetadata.packageName, arrayOfSignature);
                        try
                        {
                            PackageInfo localPackageInfo = BackupManagerService.this.mPackageManager.getPackageInfo(paramFileMetadata.packageName, 64);
                            if ((0x8000 & localPackageInfo.applicationInfo.flags) != 0)
                                if ((localPackageInfo.applicationInfo.uid >= 10000) || (localPackageInfo.applicationInfo.backupAgentName != null))
                                {
                                    if (BackupManagerService.this.signaturesMatch(arrayOfSignature, localPackageInfo))
                                        if (localPackageInfo.versionCode >= n)
                                        {
                                            Slog.i("BackupManagerService", "Sig + version match; taking data");
                                            localRestorePolicy = BackupManagerService.RestorePolicy.ACCEPT;
                                        }
                                    while (true)
                                    {
                                        if ((localRestorePolicy == BackupManagerService.RestorePolicy.ACCEPT_IF_APK) && (!bool))
                                            Slog.i("BackupManagerService", "Cannot restore package " + paramFileMetadata.packageName + " without the matching .apk");
                                        return localRestorePolicy;
                                        Slog.d("BackupManagerService", "Data version " + n + " is newer than installed version " + localPackageInfo.versionCode + " - requiring apk");
                                        localRestorePolicy = BackupManagerService.RestorePolicy.ACCEPT_IF_APK;
                                        continue;
                                        Slog.w("BackupManagerService", "Restore manifest signatures do not match installed application for " + paramFileMetadata.packageName);
                                    }
                                }
                        }
                        catch (PackageManager.NameNotFoundException localNameNotFoundException)
                        {
                            while (true)
                            {
                                Slog.i("BackupManagerService", "Package " + paramFileMetadata.packageName + " not installed; requiring apk in dataset");
                                localRestorePolicy = BackupManagerService.RestorePolicy.ACCEPT_IF_APK;
                                continue;
                                Slog.w("BackupManagerService", "Package " + paramFileMetadata.packageName + " is system level with no agent");
                            }
                        }
                    }
                }
            }
            catch (NumberFormatException localNumberFormatException)
            {
                while (true)
                {
                    Slog.w("BackupManagerService", "Corrupt restore manifest for package " + paramFileMetadata.packageName);
                    continue;
                    Slog.i("BackupManagerService", "Restore manifest from " + paramFileMetadata.packageName + " but allowBackup=false");
                }
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
                while (true)
                {
                    int j;
                    String str1;
                    Slog.w("BackupManagerService", localIllegalArgumentException.getMessage());
                    continue;
                    label755: Slog.i("BackupManagerService", "Missing signature on backed-up package " + paramFileMetadata.packageName);
                    continue;
                    Slog.i("BackupManagerService", "Expected package " + paramFileMetadata.packageName + " but restore manifest claims " + str1);
                    continue;
                    Slog.i("BackupManagerService", "Unknown restore manifest version " + j + " for package " + paramFileMetadata.packageName);
                }
            }
        }

        int readExactly(InputStream paramInputStream, byte[] paramArrayOfByte, int paramInt1, int paramInt2)
            throws IOException
        {
            if (paramInt2 <= 0)
                throw new IllegalArgumentException("size must be > 0");
            int i = 0;
            while (true)
            {
                int j;
                if (i < paramInt2)
                {
                    j = paramInputStream.read(paramArrayOfByte, paramInt1 + i, paramInt2 - i);
                    if (j > 0);
                }
                else
                {
                    return i;
                }
                i += j;
            }
        }

        String readHeaderLine(InputStream paramInputStream)
            throws IOException
        {
            StringBuilder localStringBuilder = new StringBuilder(80);
            while (true)
            {
                int i = paramInputStream.read();
                if ((i < 0) || (i == 10))
                    return localStringBuilder.toString();
                localStringBuilder.append((char)i);
            }
        }

        boolean readPaxExtendedHeader(InputStream paramInputStream, BackupManagerService.FileMetadata paramFileMetadata)
            throws IOException
        {
            if (paramFileMetadata.size > 32768L)
            {
                Slog.w("BackupManagerService", "Suspiciously large pax header size " + paramFileMetadata.size + " - aborting");
                throw new IOException("Sanity failure: pax header size " + paramFileMetadata.size);
            }
            byte[] arrayOfByte = new byte[512 * (int)(511L + paramFileMetadata.size >> 9)];
            if (readExactly(paramInputStream, arrayOfByte, 0, arrayOfByte.length) < arrayOfByte.length)
                throw new IOException("Unable to read full pax header");
            this.mBytes += arrayOfByte.length;
            int i = (int)paramFileMetadata.size;
            int j = 0;
            for (int k = j + 1; (k < i) && (arrayOfByte[k] != 32); k++);
            if (k >= i)
                throw new IOException("Invalid pax data");
            int m = (int)extractRadix(arrayOfByte, j, k - j, 10);
            int n = k + 1;
            int i1 = -1 + (j + m);
            for (int i2 = n + 1; (arrayOfByte[i2] != 61) && (i2 <= i1); i2++);
            if (i2 > i1)
                throw new IOException("Invalid pax declaration");
            String str1 = new String(arrayOfByte, n, i2 - n, "UTF-8");
            String str2 = new String(arrayOfByte, i2 + 1, -1 + (i1 - i2), "UTF-8");
            if ("path".equals(str1))
                paramFileMetadata.path = str2;
            while (true)
            {
                j += m;
                if (j < i)
                    break;
                return true;
                if ("size".equals(str1))
                    paramFileMetadata.size = Long.parseLong(str2);
                else
                    Slog.i("BackupManagerService", "Unhandled pax key: " + n);
            }
        }

        boolean readTarHeader(InputStream paramInputStream, byte[] paramArrayOfByte)
            throws IOException
        {
            boolean bool = false;
            int i = readExactly(paramInputStream, paramArrayOfByte, 0, 512);
            if (i == 0);
            while (true)
            {
                return bool;
                if (i < 512)
                    throw new IOException("Unable to read full block header");
                this.mBytes = (512L + this.mBytes);
                bool = true;
            }
        }

        BackupManagerService.FileMetadata readTarHeaders(InputStream paramInputStream)
            throws IOException
        {
            byte[] arrayOfByte = new byte[512];
            Object localObject1 = null;
            label230: Object localObject2;
            if (readTarHeader(paramInputStream, arrayOfByte))
            {
                BackupManagerService.FileMetadata localFileMetadata;
                try
                {
                    localFileMetadata = new BackupManagerService.FileMetadata();
                    try
                    {
                        localFileMetadata.size = extractRadix(arrayOfByte, 124, 12, 8);
                        localFileMetadata.mtime = extractRadix(arrayOfByte, 136, 12, 8);
                        localFileMetadata.mode = extractRadix(arrayOfByte, 100, 8, 8);
                        localFileMetadata.path = extractString(arrayOfByte, 345, 155);
                        String str = extractString(arrayOfByte, 0, 100);
                        if (str.length() > 0)
                        {
                            if (localFileMetadata.path.length() > 0)
                                localFileMetadata.path += '/';
                            localFileMetadata.path += str;
                        }
                        i = arrayOfByte[''];
                        if (i != 120)
                            break label854;
                        boolean bool = readPaxExtendedHeader(paramInputStream, localFileMetadata);
                        if (bool)
                            bool = readTarHeader(paramInputStream, arrayOfByte);
                        if (!bool)
                            throw new IOException("Bad or missing pax header");
                    }
                    catch (IOException localIOException1)
                    {
                        Slog.e("BackupManagerService", "Parse error in header: " + localIOException1.getMessage());
                        HEXLOG(arrayOfByte);
                        throw localIOException1;
                    }
                    i = arrayOfByte[''];
                    break label854;
                    Slog.e("BackupManagerService", "Unknown tar entity type: " + i);
                    throw new IOException("Unknown entity type " + i);
                    localFileMetadata.type = 1;
                    while ("shared/".regionMatches(0, localFileMetadata.path, 0, "shared/".length()))
                    {
                        localFileMetadata.path = localFileMetadata.path.substring("shared/".length());
                        localFileMetadata.packageName = "com.android.sharedstoragebackup";
                        localFileMetadata.domain = "shared";
                        Slog.i("BackupManagerService", "File in shared storage: " + localFileMetadata.path);
                        break label892;
                        localFileMetadata.type = 2;
                        if (localFileMetadata.size != 0L)
                        {
                            Slog.w("BackupManagerService", "Directory entry with nonzero size in header");
                            localFileMetadata.size = 0L;
                            continue;
                            Slog.w("BackupManagerService", "Saw type=0 in tar header block, info=" + localFileMetadata);
                            localObject2 = null;
                            break label898;
                        }
                    }
                    if ("apps/".regionMatches(0, localFileMetadata.path, 0, "apps/".length()))
                    {
                        localFileMetadata.path = localFileMetadata.path.substring("apps/".length());
                        int j = localFileMetadata.path.indexOf('/');
                        if (j < 0)
                            throw new IOException("Illegal semantic path in " + localFileMetadata.path);
                        localFileMetadata.packageName = localFileMetadata.path.substring(0, j);
                        localFileMetadata.path = localFileMetadata.path.substring(j + 1);
                        if (!localFileMetadata.path.equals("_manifest"))
                        {
                            int k = localFileMetadata.path.indexOf('/');
                            if (k < 0)
                                throw new IOException("Illegal semantic path in non-manifest " + localFileMetadata.path);
                            localFileMetadata.domain = localFileMetadata.path.substring(0, k);
                            if ((!localFileMetadata.domain.equals("a")) && (!localFileMetadata.domain.equals("f")) && (!localFileMetadata.domain.equals("db")) && (!localFileMetadata.domain.equals("r")) && (!localFileMetadata.domain.equals("sp")) && (!localFileMetadata.domain.equals("obb")) && (!localFileMetadata.domain.equals("c")))
                                throw new IOException("Unrecognized domain " + localFileMetadata.domain);
                            localFileMetadata.path = localFileMetadata.path.substring(k + 1);
                        }
                    }
                }
                catch (IOException localIOException2)
                {
                    int i;
                    break label230;
                    label854: switch (i)
                    {
                    default:
                    case 48:
                    case 53:
                    case 0:
                    }
                }
                label892: localObject1 = localFileMetadata;
            }
            else
            {
                localObject2 = localObject1;
            }
            label898: return localObject2;
        }

        // ERROR //
        boolean restoreOneFile(InputStream paramInputStream, byte[] paramArrayOfByte)
        {
            // Byte code:
            //     0: aload_0
            //     1: aload_1
            //     2: invokevirtual 708	com/android/server/BackupManagerService$PerformFullRestoreTask:readTarHeaders	(Ljava/io/InputStream;)Lcom/android/server/BackupManagerService$FileMetadata;
            //     5: astore 5
            //     7: aload 5
            //     9: ifnull +1224 -> 1233
            //     12: aload 5
            //     14: getfield 335	com/android/server/BackupManagerService$FileMetadata:packageName	Ljava/lang/String;
            //     17: astore 7
            //     19: aload 7
            //     21: aload_0
            //     22: getfield 97	com/android/server/BackupManagerService$PerformFullRestoreTask:mAgentPackage	Ljava/lang/String;
            //     25: invokevirtual 177	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     28: ifne +66 -> 94
            //     31: aload_0
            //     32: getfield 66	com/android/server/BackupManagerService$PerformFullRestoreTask:mPackagePolicies	Ljava/util/HashMap;
            //     35: aload 7
            //     37: invokevirtual 711	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
            //     40: ifne +16 -> 56
            //     43: aload_0
            //     44: getfield 66	com/android/server/BackupManagerService$PerformFullRestoreTask:mPackagePolicies	Ljava/util/HashMap;
            //     47: aload 7
            //     49: getstatic 518	com/android/server/BackupManagerService$RestorePolicy:IGNORE	Lcom/android/server/BackupManagerService$RestorePolicy;
            //     52: invokevirtual 537	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
            //     55: pop
            //     56: aload_0
            //     57: getfield 95	com/android/server/BackupManagerService$PerformFullRestoreTask:mAgent	Landroid/app/IBackupAgent;
            //     60: ifnull +34 -> 94
            //     63: ldc 246
            //     65: ldc_w 713
            //     68: invokestatic 380	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     71: pop
            //     72: aload_0
            //     73: invokevirtual 716	com/android/server/BackupManagerService$PerformFullRestoreTask:tearDownPipes	()V
            //     76: aload_0
            //     77: aload_0
            //     78: getfield 99	com/android/server/BackupManagerService$PerformFullRestoreTask:mTargetApp	Landroid/content/pm/ApplicationInfo;
            //     81: invokevirtual 720	com/android/server/BackupManagerService$PerformFullRestoreTask:tearDownAgent	(Landroid/content/pm/ApplicationInfo;)V
            //     84: aload_0
            //     85: aconst_null
            //     86: putfield 99	com/android/server/BackupManagerService$PerformFullRestoreTask:mTargetApp	Landroid/content/pm/ApplicationInfo;
            //     89: aload_0
            //     90: aconst_null
            //     91: putfield 97	com/android/server/BackupManagerService$PerformFullRestoreTask:mAgentPackage	Ljava/lang/String;
            //     94: aload 5
            //     96: getfield 343	com/android/server/BackupManagerService$FileMetadata:path	Ljava/lang/String;
            //     99: ldc_w 685
            //     102: invokevirtual 177	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     105: ifeq +54 -> 159
            //     108: aload_0
            //     109: getfield 66	com/android/server/BackupManagerService$PerformFullRestoreTask:mPackagePolicies	Ljava/util/HashMap;
            //     112: aload 7
            //     114: aload_0
            //     115: aload 5
            //     117: aload_1
            //     118: invokevirtual 722	com/android/server/BackupManagerService$PerformFullRestoreTask:readAppManifest	(Lcom/android/server/BackupManagerService$FileMetadata;Ljava/io/InputStream;)Lcom/android/server/BackupManagerService$RestorePolicy;
            //     121: invokevirtual 537	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
            //     124: pop
            //     125: aload_0
            //     126: getfield 68	com/android/server/BackupManagerService$PerformFullRestoreTask:mPackageInstallers	Ljava/util/HashMap;
            //     129: aload 7
            //     131: aload 5
            //     133: getfield 526	com/android/server/BackupManagerService$FileMetadata:installerPackageName	Ljava/lang/String;
            //     136: invokevirtual 537	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
            //     139: pop
            //     140: aload_0
            //     141: aload 5
            //     143: getfield 307	com/android/server/BackupManagerService$FileMetadata:size	J
            //     146: aload_1
            //     147: invokevirtual 726	com/android/server/BackupManagerService$PerformFullRestoreTask:skipTarPadding	(JLjava/io/InputStream;)V
            //     150: aload_0
            //     151: aload 7
            //     153: invokevirtual 729	com/android/server/BackupManagerService$PerformFullRestoreTask:sendOnRestorePackage	(Ljava/lang/String;)V
            //     156: goto +1077 -> 1233
            //     159: iconst_1
            //     160: istore 8
            //     162: aload_0
            //     163: getfield 66	com/android/server/BackupManagerService$PerformFullRestoreTask:mPackagePolicies	Ljava/util/HashMap;
            //     166: aload 7
            //     168: invokevirtual 443	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
            //     171: checkcast 445	com/android/server/BackupManagerService$RestorePolicy
            //     174: astore 9
            //     176: getstatic 735	com/android/server/BackupManagerService$4:$SwitchMap$com$android$server$BackupManagerService$RestorePolicy	[I
            //     179: aload 9
            //     181: invokevirtual 738	com/android/server/BackupManagerService$RestorePolicy:ordinal	()I
            //     184: iaload
            //     185: tableswitch	default:+27 -> 212, 1:+1084->1269, 2:+668->853, 3:+781->966
            //     213: <illegal opcode>
            //     214: ldc_w 740
            //     217: invokestatic 266	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     220: pop
            //     221: iconst_0
            //     222: istore 8
            //     224: aload_0
            //     225: getfield 66	com/android/server/BackupManagerService$PerformFullRestoreTask:mPackagePolicies	Ljava/util/HashMap;
            //     228: aload 7
            //     230: getstatic 518	com/android/server/BackupManagerService$RestorePolicy:IGNORE	Lcom/android/server/BackupManagerService$RestorePolicy;
            //     233: invokevirtual 537	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
            //     236: pop
            //     237: iload 8
            //     239: ifeq +19 -> 258
            //     242: aload_0
            //     243: getfield 95	com/android/server/BackupManagerService$PerformFullRestoreTask:mAgent	Landroid/app/IBackupAgent;
            //     246: ifnull +12 -> 258
            //     249: ldc 246
            //     251: ldc_w 742
            //     254: invokestatic 151	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
            //     257: pop
            //     258: iload 8
            //     260: ifeq +184 -> 444
            //     263: aload_0
            //     264: getfield 95	com/android/server/BackupManagerService$PerformFullRestoreTask:mAgent	Landroid/app/IBackupAgent;
            //     267: ifnonnull +177 -> 444
            //     270: ldc 246
            //     272: new 111	java/lang/StringBuilder
            //     275: dup
            //     276: invokespecial 258	java/lang/StringBuilder:<init>	()V
            //     279: ldc_w 744
            //     282: invokevirtual 132	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     285: aload 7
            //     287: invokevirtual 132	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     290: invokevirtual 145	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     293: invokestatic 380	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     296: pop
            //     297: aload_0
            //     298: aload_0
            //     299: getfield 56	com/android/server/BackupManagerService$PerformFullRestoreTask:this$0	Lcom/android/server/BackupManagerService;
            //     302: invokestatic 426	com/android/server/BackupManagerService:access$600	(Lcom/android/server/BackupManagerService;)Landroid/content/pm/PackageManager;
            //     305: aload 7
            //     307: iconst_0
            //     308: invokevirtual 748	android/content/pm/PackageManager:getApplicationInfo	(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
            //     311: putfield 99	com/android/server/BackupManagerService$PerformFullRestoreTask:mTargetApp	Landroid/content/pm/ApplicationInfo;
            //     314: aload_0
            //     315: getfield 75	com/android/server/BackupManagerService$PerformFullRestoreTask:mClearedPackages	Ljava/util/HashSet;
            //     318: aload 7
            //     320: invokevirtual 751	java/util/HashSet:contains	(Ljava/lang/Object;)Z
            //     323: ifne +713 -> 1036
            //     326: aload_0
            //     327: getfield 99	com/android/server/BackupManagerService$PerformFullRestoreTask:mTargetApp	Landroid/content/pm/ApplicationInfo;
            //     330: getfield 543	android/content/pm/ApplicationInfo:backupAgentName	Ljava/lang/String;
            //     333: ifnonnull +662 -> 995
            //     336: ldc 246
            //     338: ldc_w 753
            //     341: invokestatic 380	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     344: pop
            //     345: aload_0
            //     346: getfield 56	com/android/server/BackupManagerService$PerformFullRestoreTask:this$0	Lcom/android/server/BackupManagerService;
            //     349: aload 7
            //     351: invokevirtual 756	com/android/server/BackupManagerService:clearApplicationDataSynchronous	(Ljava/lang/String;)V
            //     354: aload_0
            //     355: getfield 75	com/android/server/BackupManagerService$PerformFullRestoreTask:mClearedPackages	Ljava/util/HashSet;
            //     358: aload 7
            //     360: invokevirtual 105	java/util/HashSet:add	(Ljava/lang/Object;)Z
            //     363: pop
            //     364: aload_0
            //     365: invokevirtual 759	com/android/server/BackupManagerService$PerformFullRestoreTask:setUpPipes	()V
            //     368: aload_0
            //     369: aload_0
            //     370: getfield 56	com/android/server/BackupManagerService$PerformFullRestoreTask:this$0	Lcom/android/server/BackupManagerService;
            //     373: aload_0
            //     374: getfield 99	com/android/server/BackupManagerService$PerformFullRestoreTask:mTargetApp	Landroid/content/pm/ApplicationInfo;
            //     377: iconst_3
            //     378: invokevirtual 763	com/android/server/BackupManagerService:bindToAgentSynchronous	(Landroid/content/pm/ApplicationInfo;I)Landroid/app/IBackupAgent;
            //     381: putfield 95	com/android/server/BackupManagerService$PerformFullRestoreTask:mAgent	Landroid/app/IBackupAgent;
            //     384: aload_0
            //     385: aload 7
            //     387: putfield 97	com/android/server/BackupManagerService$PerformFullRestoreTask:mAgentPackage	Ljava/lang/String;
            //     390: aload_0
            //     391: getfield 95	com/android/server/BackupManagerService$PerformFullRestoreTask:mAgent	Landroid/app/IBackupAgent;
            //     394: ifnonnull +50 -> 444
            //     397: ldc 246
            //     399: new 111	java/lang/StringBuilder
            //     402: dup
            //     403: invokespecial 258	java/lang/StringBuilder:<init>	()V
            //     406: ldc_w 765
            //     409: invokevirtual 132	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     412: aload 7
            //     414: invokevirtual 132	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     417: invokevirtual 145	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     420: invokestatic 380	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     423: pop
            //     424: iconst_0
            //     425: istore 8
            //     427: aload_0
            //     428: invokevirtual 716	com/android/server/BackupManagerService$PerformFullRestoreTask:tearDownPipes	()V
            //     431: aload_0
            //     432: getfield 66	com/android/server/BackupManagerService$PerformFullRestoreTask:mPackagePolicies	Ljava/util/HashMap;
            //     435: aload 7
            //     437: getstatic 518	com/android/server/BackupManagerService$RestorePolicy:IGNORE	Lcom/android/server/BackupManagerService$RestorePolicy;
            //     440: invokevirtual 537	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
            //     443: pop
            //     444: iload 8
            //     446: ifeq +58 -> 504
            //     449: aload 7
            //     451: aload_0
            //     452: getfield 97	com/android/server/BackupManagerService$PerformFullRestoreTask:mAgentPackage	Ljava/lang/String;
            //     455: invokevirtual 177	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     458: ifne +46 -> 504
            //     461: ldc 246
            //     463: new 111	java/lang/StringBuilder
            //     466: dup
            //     467: invokespecial 258	java/lang/StringBuilder:<init>	()V
            //     470: ldc_w 767
            //     473: invokevirtual 132	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     476: aload 7
            //     478: invokevirtual 132	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     481: ldc_w 769
            //     484: invokevirtual 132	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     487: aload_0
            //     488: getfield 97	com/android/server/BackupManagerService$PerformFullRestoreTask:mAgentPackage	Ljava/lang/String;
            //     491: invokevirtual 132	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     494: invokevirtual 145	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     497: invokestatic 266	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     500: pop
            //     501: iconst_0
            //     502: istore 8
            //     504: iload 8
            //     506: ifeq +266 -> 772
            //     509: iconst_1
            //     510: istore 17
            //     512: aload 5
            //     514: getfield 307	com/android/server/BackupManagerService$FileMetadata:size	J
            //     517: lstore 18
            //     519: aload_0
            //     520: getfield 56	com/android/server/BackupManagerService$PerformFullRestoreTask:this$0	Lcom/android/server/BackupManagerService;
            //     523: invokevirtual 772	com/android/server/BackupManagerService:generateToken	()I
            //     526: istore 20
            //     528: ldc 246
            //     530: new 111	java/lang/StringBuilder
            //     533: dup
            //     534: invokespecial 258	java/lang/StringBuilder:<init>	()V
            //     537: ldc_w 774
            //     540: invokevirtual 132	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     543: aload 5
            //     545: getfield 343	com/android/server/BackupManagerService$FileMetadata:path	Ljava/lang/String;
            //     548: invokevirtual 132	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     551: invokevirtual 145	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     554: invokestatic 380	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     557: pop
            //     558: aload_0
            //     559: getfield 56	com/android/server/BackupManagerService$PerformFullRestoreTask:this$0	Lcom/android/server/BackupManagerService;
            //     562: iload 20
            //     564: ldc2_w 775
            //     567: aconst_null
            //     568: invokevirtual 780	com/android/server/BackupManagerService:prepareOperationTimeout	(IJLcom/android/server/BackupManagerService$BackupRestoreTask;)V
            //     571: aload_0
            //     572: getfield 99	com/android/server/BackupManagerService$PerformFullRestoreTask:mTargetApp	Landroid/content/pm/ApplicationInfo;
            //     575: getfield 783	android/content/pm/ApplicationInfo:processName	Ljava/lang/String;
            //     578: ldc_w 785
            //     581: invokevirtual 177	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     584: ifeq +469 -> 1053
            //     587: ldc 246
            //     589: ldc_w 787
            //     592: invokestatic 380	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     595: pop
            //     596: new 789	java/lang/Thread
            //     599: dup
            //     600: new 17	com/android/server/BackupManagerService$PerformFullRestoreTask$RestoreFileRunnable
            //     603: dup
            //     604: aload_0
            //     605: aload_0
            //     606: getfield 95	com/android/server/BackupManagerService$PerformFullRestoreTask:mAgent	Landroid/app/IBackupAgent;
            //     609: aload 5
            //     611: aload_0
            //     612: getfield 61	com/android/server/BackupManagerService$PerformFullRestoreTask:mPipes	[Landroid/os/ParcelFileDescriptor;
            //     615: iconst_0
            //     616: aaload
            //     617: iload 20
            //     619: invokespecial 792	com/android/server/BackupManagerService$PerformFullRestoreTask$RestoreFileRunnable:<init>	(Lcom/android/server/BackupManagerService$PerformFullRestoreTask;Landroid/app/IBackupAgent;Lcom/android/server/BackupManagerService$FileMetadata;Landroid/os/ParcelFileDescriptor;I)V
            //     622: invokespecial 795	java/lang/Thread:<init>	(Ljava/lang/Runnable;)V
            //     625: invokevirtual 798	java/lang/Thread:start	()V
            //     628: iload 8
            //     630: ifeq +95 -> 725
            //     633: iconst_1
            //     634: istore 24
            //     636: new 391	java/io/FileOutputStream
            //     639: dup
            //     640: aload_0
            //     641: getfield 61	com/android/server/BackupManagerService$PerformFullRestoreTask:mPipes	[Landroid/os/ParcelFileDescriptor;
            //     644: iconst_1
            //     645: aaload
            //     646: invokevirtual 804	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
            //     649: invokespecial 807	java/io/FileOutputStream:<init>	(Ljava/io/FileDescriptor;)V
            //     652: astore 25
            //     654: lload 18
            //     656: lconst_0
            //     657: lcmp
            //     658: ifle +46 -> 704
            //     661: lload 18
            //     663: aload_2
            //     664: arraylength
            //     665: i2l
            //     666: lcmp
            //     667: ifle +483 -> 1150
            //     670: aload_2
            //     671: arraylength
            //     672: istore 26
            //     674: aload_1
            //     675: aload_2
            //     676: iconst_0
            //     677: iload 26
            //     679: invokevirtual 401	java/io/InputStream:read	([BII)I
            //     682: istore 27
            //     684: iload 27
            //     686: iflt +558 -> 1244
            //     689: aload_0
            //     690: aload_0
            //     691: getfield 403	com/android/server/BackupManagerService$PerformFullRestoreTask:mBytes	J
            //     694: iload 27
            //     696: i2l
            //     697: ladd
            //     698: putfield 403	com/android/server/BackupManagerService$PerformFullRestoreTask:mBytes	J
            //     701: goto +543 -> 1244
            //     704: aload_0
            //     705: aload 5
            //     707: getfield 307	com/android/server/BackupManagerService$FileMetadata:size	J
            //     710: aload_1
            //     711: invokevirtual 726	com/android/server/BackupManagerService$PerformFullRestoreTask:skipTarPadding	(JLjava/io/InputStream;)V
            //     714: aload_0
            //     715: getfield 56	com/android/server/BackupManagerService$PerformFullRestoreTask:this$0	Lcom/android/server/BackupManagerService;
            //     718: iload 20
            //     720: invokevirtual 811	com/android/server/BackupManagerService:waitUntilOperationComplete	(I)Z
            //     723: istore 17
            //     725: iload 17
            //     727: ifne +45 -> 772
            //     730: aload_0
            //     731: getfield 56	com/android/server/BackupManagerService$PerformFullRestoreTask:this$0	Lcom/android/server/BackupManagerService;
            //     734: getfield 815	com/android/server/BackupManagerService:mBackupHandler	Lcom/android/server/BackupManagerService$BackupHandler;
            //     737: bipush 7
            //     739: invokevirtual 820	com/android/server/BackupManagerService$BackupHandler:removeMessages	(I)V
            //     742: aload_0
            //     743: invokevirtual 716	com/android/server/BackupManagerService$PerformFullRestoreTask:tearDownPipes	()V
            //     746: aload_0
            //     747: aload_0
            //     748: getfield 99	com/android/server/BackupManagerService$PerformFullRestoreTask:mTargetApp	Landroid/content/pm/ApplicationInfo;
            //     751: invokevirtual 720	com/android/server/BackupManagerService$PerformFullRestoreTask:tearDownAgent	(Landroid/content/pm/ApplicationInfo;)V
            //     754: aload_0
            //     755: aconst_null
            //     756: putfield 95	com/android/server/BackupManagerService$PerformFullRestoreTask:mAgent	Landroid/app/IBackupAgent;
            //     759: aload_0
            //     760: getfield 66	com/android/server/BackupManagerService$PerformFullRestoreTask:mPackagePolicies	Ljava/util/HashMap;
            //     763: aload 7
            //     765: getstatic 518	com/android/server/BackupManagerService$RestorePolicy:IGNORE	Lcom/android/server/BackupManagerService$RestorePolicy;
            //     768: invokevirtual 537	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
            //     771: pop
            //     772: iload 8
            //     774: ifne +459 -> 1233
            //     777: ldc 246
            //     779: ldc_w 822
            //     782: invokestatic 380	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     785: pop
            //     786: ldc2_w 823
            //     789: ldc2_w 604
            //     792: aload 5
            //     794: getfield 307	com/android/server/BackupManagerService$FileMetadata:size	J
            //     797: ladd
            //     798: land
            //     799: lstore 12
            //     801: lload 12
            //     803: lconst_0
            //     804: lcmp
            //     805: ifle +428 -> 1233
            //     808: lload 12
            //     810: aload_2
            //     811: arraylength
            //     812: i2l
            //     813: lcmp
            //     814: ifle +388 -> 1202
            //     817: aload_2
            //     818: arraylength
            //     819: istore 14
            //     821: aload_1
            //     822: aload_2
            //     823: iconst_0
            //     824: iload 14
            //     826: invokevirtual 401	java/io/InputStream:read	([BII)I
            //     829: i2l
            //     830: lstore 15
            //     832: lload 15
            //     834: lconst_0
            //     835: lcmp
            //     836: iflt +416 -> 1252
            //     839: aload_0
            //     840: lload 15
            //     842: aload_0
            //     843: getfield 403	com/android/server/BackupManagerService$PerformFullRestoreTask:mBytes	J
            //     846: ladd
            //     847: putfield 403	com/android/server/BackupManagerService$PerformFullRestoreTask:mBytes	J
            //     850: goto +402 -> 1252
            //     853: aload 5
            //     855: getfield 340	com/android/server/BackupManagerService$FileMetadata:domain	Ljava/lang/String;
            //     858: ldc_w 689
            //     861: invokevirtual 177	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     864: ifeq +83 -> 947
            //     867: ldc 246
            //     869: ldc_w 826
            //     872: invokestatic 380	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     875: pop
            //     876: aload_0
            //     877: aload 5
            //     879: aload_0
            //     880: getfield 68	com/android/server/BackupManagerService$PerformFullRestoreTask:mPackageInstallers	Ljava/util/HashMap;
            //     883: aload 7
            //     885: invokevirtual 443	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
            //     888: checkcast 124	java/lang/String
            //     891: aload_1
            //     892: invokevirtual 828	com/android/server/BackupManagerService$PerformFullRestoreTask:installApk	(Lcom/android/server/BackupManagerService$FileMetadata;Ljava/lang/String;Ljava/io/InputStream;)Z
            //     895: istore 47
            //     897: aload_0
            //     898: getfield 66	com/android/server/BackupManagerService$PerformFullRestoreTask:mPackagePolicies	Ljava/util/HashMap;
            //     901: astore 48
            //     903: iload 47
            //     905: ifeq +34 -> 939
            //     908: getstatic 449	com/android/server/BackupManagerService$RestorePolicy:ACCEPT	Lcom/android/server/BackupManagerService$RestorePolicy;
            //     911: astore 49
            //     913: aload 48
            //     915: aload 7
            //     917: aload 49
            //     919: invokevirtual 537	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
            //     922: pop
            //     923: aload_0
            //     924: aload 5
            //     926: getfield 307	com/android/server/BackupManagerService$FileMetadata:size	J
            //     929: aload_1
            //     930: invokevirtual 726	com/android/server/BackupManagerService$PerformFullRestoreTask:skipTarPadding	(JLjava/io/InputStream;)V
            //     933: iconst_1
            //     934: istore 6
            //     936: goto +305 -> 1241
            //     939: getstatic 518	com/android/server/BackupManagerService$RestorePolicy:IGNORE	Lcom/android/server/BackupManagerService$RestorePolicy;
            //     942: astore 49
            //     944: goto -31 -> 913
            //     947: aload_0
            //     948: getfield 66	com/android/server/BackupManagerService$PerformFullRestoreTask:mPackagePolicies	Ljava/util/HashMap;
            //     951: aload 7
            //     953: getstatic 518	com/android/server/BackupManagerService$RestorePolicy:IGNORE	Lcom/android/server/BackupManagerService$RestorePolicy;
            //     956: invokevirtual 537	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
            //     959: pop
            //     960: iconst_0
            //     961: istore 8
            //     963: goto -726 -> 237
            //     966: aload 5
            //     968: getfield 340	com/android/server/BackupManagerService$FileMetadata:domain	Ljava/lang/String;
            //     971: ldc_w 689
            //     974: invokevirtual 177	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     977: ifeq -740 -> 237
            //     980: ldc 246
            //     982: ldc_w 830
            //     985: invokestatic 380	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     988: pop
            //     989: iconst_0
            //     990: istore 8
            //     992: goto -755 -> 237
            //     995: ldc 246
            //     997: new 111	java/lang/StringBuilder
            //     1000: dup
            //     1001: invokespecial 258	java/lang/StringBuilder:<init>	()V
            //     1004: ldc_w 832
            //     1007: invokevirtual 132	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     1010: aload_0
            //     1011: getfield 99	com/android/server/BackupManagerService$PerformFullRestoreTask:mTargetApp	Landroid/content/pm/ApplicationInfo;
            //     1014: getfield 543	android/content/pm/ApplicationInfo:backupAgentName	Ljava/lang/String;
            //     1017: invokevirtual 132	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     1020: ldc_w 834
            //     1023: invokevirtual 132	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     1026: invokevirtual 145	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     1029: invokestatic 380	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     1032: pop
            //     1033: goto -679 -> 354
            //     1036: ldc 246
            //     1038: ldc_w 836
            //     1041: invokestatic 380	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     1044: pop
            //     1045: goto -681 -> 364
            //     1048: astore 36
            //     1050: goto -660 -> 390
            //     1053: aload_0
            //     1054: getfield 95	com/android/server/BackupManagerService$PerformFullRestoreTask:mAgent	Landroid/app/IBackupAgent;
            //     1057: aload_0
            //     1058: getfield 61	com/android/server/BackupManagerService$PerformFullRestoreTask:mPipes	[Landroid/os/ParcelFileDescriptor;
            //     1061: iconst_0
            //     1062: aaload
            //     1063: aload 5
            //     1065: getfield 307	com/android/server/BackupManagerService$FileMetadata:size	J
            //     1068: aload 5
            //     1070: getfield 282	com/android/server/BackupManagerService$FileMetadata:type	I
            //     1073: aload 5
            //     1075: getfield 340	com/android/server/BackupManagerService$FileMetadata:domain	Ljava/lang/String;
            //     1078: aload 5
            //     1080: getfield 343	com/android/server/BackupManagerService$FileMetadata:path	Ljava/lang/String;
            //     1083: aload 5
            //     1085: getfield 290	com/android/server/BackupManagerService$FileMetadata:mode	J
            //     1088: aload 5
            //     1090: getfield 319	com/android/server/BackupManagerService$FileMetadata:mtime	J
            //     1093: iload 20
            //     1095: aload_0
            //     1096: getfield 56	com/android/server/BackupManagerService$PerformFullRestoreTask:this$0	Lcom/android/server/BackupManagerService;
            //     1099: getfield 840	com/android/server/BackupManagerService:mBackupManagerBinder	Landroid/app/backup/IBackupManager;
            //     1102: invokeinterface 846 13 0
            //     1107: goto -479 -> 628
            //     1110: astore 30
            //     1112: ldc 246
            //     1114: ldc_w 848
            //     1117: invokestatic 380	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     1120: pop
            //     1121: iconst_0
            //     1122: istore 17
            //     1124: iconst_0
            //     1125: istore 8
            //     1127: goto -499 -> 628
            //     1130: astore 21
            //     1132: ldc 246
            //     1134: ldc_w 850
            //     1137: invokestatic 266	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     1140: pop
            //     1141: iconst_0
            //     1142: istore 17
            //     1144: iconst_0
            //     1145: istore 8
            //     1147: goto -519 -> 628
            //     1150: lload 18
            //     1152: l2i
            //     1153: istore 26
            //     1155: goto -481 -> 674
            //     1158: lload 18
            //     1160: iload 27
            //     1162: i2l
            //     1163: lsub
            //     1164: lstore 18
            //     1166: iload 24
            //     1168: ifeq -514 -> 654
            //     1171: aload 25
            //     1173: aload_2
            //     1174: iconst_0
            //     1175: iload 27
            //     1177: invokevirtual 406	java/io/FileOutputStream:write	([BII)V
            //     1180: goto -526 -> 654
            //     1183: astore 28
            //     1185: ldc 246
            //     1187: ldc_w 852
            //     1190: aload 28
            //     1192: invokestatic 257	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     1195: pop
            //     1196: iconst_0
            //     1197: istore 24
            //     1199: goto -545 -> 654
            //     1202: lload 12
            //     1204: l2i
            //     1205: istore 14
            //     1207: goto -386 -> 821
            //     1210: astore_3
            //     1211: ldc 246
            //     1213: ldc_w 854
            //     1216: aload_3
            //     1217: invokestatic 856	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     1220: pop
            //     1221: aconst_null
            //     1222: astore 5
            //     1224: goto +9 -> 1233
            //     1227: iconst_0
            //     1228: istore 6
            //     1230: goto +11 -> 1241
            //     1233: aload 5
            //     1235: ifnull -8 -> 1227
            //     1238: iconst_1
            //     1239: istore 6
            //     1241: iload 6
            //     1243: ireturn
            //     1244: iload 27
            //     1246: ifgt -88 -> 1158
            //     1249: goto -545 -> 704
            //     1252: lload 15
            //     1254: lconst_0
            //     1255: lcmp
            //     1256: ifle -23 -> 1233
            //     1259: lload 12
            //     1261: lload 15
            //     1263: lsub
            //     1264: lstore 12
            //     1266: goto -465 -> 801
            //     1269: iconst_0
            //     1270: istore 8
            //     1272: goto -1035 -> 237
            //     1275: astore 39
            //     1277: goto -887 -> 390
            //
            // Exception table:
            //     from	to	target	type
            //     297	390	1048	android/content/pm/PackageManager$NameNotFoundException
            //     995	1045	1048	android/content/pm/PackageManager$NameNotFoundException
            //     528	628	1110	java/io/IOException
            //     1053	1107	1110	java/io/IOException
            //     528	628	1130	android/os/RemoteException
            //     1053	1107	1130	android/os/RemoteException
            //     1171	1180	1183	java/io/IOException
            //     0	297	1210	java/io/IOException
            //     390	528	1210	java/io/IOException
            //     636	989	1210	java/io/IOException
            //     1112	1141	1210	java/io/IOException
            //     1185	1196	1210	java/io/IOException
            //     297	390	1275	java/io/IOException
            //     995	1045	1275	java/io/IOException
        }

        // ERROR //
        public void run()
        {
            // Byte code:
            //     0: ldc 246
            //     2: ldc_w 859
            //     5: invokestatic 151	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
            //     8: pop
            //     9: aload_0
            //     10: invokevirtual 862	com/android/server/BackupManagerService$PerformFullRestoreTask:sendStartRestore	()V
            //     13: invokestatic 867	android/os/Environment:getExternalStorageState	()Ljava/lang/String;
            //     16: ldc_w 869
            //     19: invokevirtual 177	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     22: ifeq +17 -> 39
            //     25: aload_0
            //     26: getfield 66	com/android/server/BackupManagerService$PerformFullRestoreTask:mPackagePolicies	Ljava/util/HashMap;
            //     29: ldc_w 661
            //     32: getstatic 449	com/android/server/BackupManagerService$RestorePolicy:ACCEPT	Lcom/android/server/BackupManagerService$RestorePolicy;
            //     35: invokevirtual 537	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
            //     38: pop
            //     39: aconst_null
            //     40: astore_2
            //     41: aconst_null
            //     42: astore_3
            //     43: aload_0
            //     44: getfield 56	com/android/server/BackupManagerService$PerformFullRestoreTask:this$0	Lcom/android/server/BackupManagerService;
            //     47: invokevirtual 872	com/android/server/BackupManagerService:hasBackupPassword	()Z
            //     50: ifeq +140 -> 190
            //     53: aload_0
            //     54: getfield 56	com/android/server/BackupManagerService$PerformFullRestoreTask:this$0	Lcom/android/server/BackupManagerService;
            //     57: aload_0
            //     58: getfield 87	com/android/server/BackupManagerService$PerformFullRestoreTask:mCurrentPassword	Ljava/lang/String;
            //     61: sipush 10000
            //     64: invokevirtual 876	com/android/server/BackupManagerService:passwordMatchesSaved	(Ljava/lang/String;I)Z
            //     67: ifne +123 -> 190
            //     70: ldc 246
            //     72: ldc_w 878
            //     75: invokestatic 251	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     78: pop
            //     79: aload_0
            //     80: invokevirtual 716	com/android/server/BackupManagerService$PerformFullRestoreTask:tearDownPipes	()V
            //     83: aload_0
            //     84: aload_0
            //     85: getfield 99	com/android/server/BackupManagerService$PerformFullRestoreTask:mTargetApp	Landroid/content/pm/ApplicationInfo;
            //     88: invokevirtual 720	com/android/server/BackupManagerService$PerformFullRestoreTask:tearDownAgent	(Landroid/content/pm/ApplicationInfo;)V
            //     91: iconst_0
            //     92: ifeq +5 -> 97
            //     95: aconst_null
            //     96: athrow
            //     97: iconst_0
            //     98: ifeq +5 -> 103
            //     101: aconst_null
            //     102: athrow
            //     103: aload_0
            //     104: getfield 85	com/android/server/BackupManagerService$PerformFullRestoreTask:mInputFile	Landroid/os/ParcelFileDescriptor;
            //     107: invokevirtual 879	android/os/ParcelFileDescriptor:close	()V
            //     110: aload_0
            //     111: getfield 56	com/android/server/BackupManagerService$PerformFullRestoreTask:this$0	Lcom/android/server/BackupManagerService;
            //     114: getfield 883	com/android/server/BackupManagerService:mCurrentOpLock	Ljava/lang/Object;
            //     117: astore 56
            //     119: aload 56
            //     121: monitorenter
            //     122: aload_0
            //     123: getfield 56	com/android/server/BackupManagerService$PerformFullRestoreTask:this$0	Lcom/android/server/BackupManagerService;
            //     126: getfield 887	com/android/server/BackupManagerService:mCurrentOperations	Landroid/util/SparseArray;
            //     129: invokevirtual 892	android/util/SparseArray:clear	()V
            //     132: aload 56
            //     134: monitorexit
            //     135: aload_0
            //     136: getfield 93	com/android/server/BackupManagerService$PerformFullRestoreTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     139: astore 58
            //     141: aload 58
            //     143: monitorenter
            //     144: aload_0
            //     145: getfield 93	com/android/server/BackupManagerService$PerformFullRestoreTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     148: iconst_1
            //     149: invokevirtual 898	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
            //     152: aload_0
            //     153: getfield 93	com/android/server/BackupManagerService$PerformFullRestoreTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     156: invokevirtual 901	java/lang/Object:notifyAll	()V
            //     159: aload 58
            //     161: monitorexit
            //     162: aload_0
            //     163: invokevirtual 904	com/android/server/BackupManagerService$PerformFullRestoreTask:sendEndRestore	()V
            //     166: ldc 246
            //     168: ldc_w 906
            //     171: invokestatic 380	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     174: pop
            //     175: aload_0
            //     176: getfield 56	com/android/server/BackupManagerService$PerformFullRestoreTask:this$0	Lcom/android/server/BackupManagerService;
            //     179: getfield 910	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     182: astore 21
            //     184: aload 21
            //     186: invokevirtual 915	android/os/PowerManager$WakeLock:release	()V
            //     189: return
            //     190: aload_0
            //     191: lconst_0
            //     192: putfield 403	com/android/server/BackupManagerService$PerformFullRestoreTask:mBytes	J
            //     195: ldc_w 395
            //     198: newarray byte
            //     200: astore 22
            //     202: new 917	java/io/FileInputStream
            //     205: dup
            //     206: aload_0
            //     207: getfield 85	com/android/server/BackupManagerService$PerformFullRestoreTask:mInputFile	Landroid/os/ParcelFileDescriptor;
            //     210: invokevirtual 804	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
            //     213: invokespecial 918	java/io/FileInputStream:<init>	(Ljava/io/FileDescriptor;)V
            //     216: astore 23
            //     218: new 920	java/io/DataInputStream
            //     221: dup
            //     222: aload 23
            //     224: invokespecial 923	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
            //     227: astore 24
            //     229: iconst_0
            //     230: istore 25
            //     232: aload 23
            //     234: astore 26
            //     236: iconst_0
            //     237: istore 27
            //     239: ldc_w 925
            //     242: invokevirtual 523	java/lang/String:length	()I
            //     245: newarray byte
            //     247: astore 29
            //     249: aload 24
            //     251: aload 29
            //     253: invokevirtual 928	java/io/DataInputStream:readFully	([B)V
            //     256: ldc_w 925
            //     259: ldc_w 615
            //     262: invokevirtual 932	java/lang/String:getBytes	(Ljava/lang/String;)[B
            //     265: aload 29
            //     267: invokestatic 239	java/util/Arrays:equals	([B[B)Z
            //     270: ifeq +524 -> 794
            //     273: aload_0
            //     274: aload 23
            //     276: invokevirtual 181	com/android/server/BackupManagerService$PerformFullRestoreTask:readHeaderLine	(Ljava/io/InputStream;)Ljava/lang/String;
            //     279: astore 48
            //     281: aload 48
            //     283: invokestatic 189	java/lang/Integer:parseInt	(Ljava/lang/String;)I
            //     286: iconst_1
            //     287: if_icmpne +356 -> 643
            //     290: aload_0
            //     291: aload 23
            //     293: invokevirtual 181	com/android/server/BackupManagerService$PerformFullRestoreTask:readHeaderLine	(Ljava/io/InputStream;)Ljava/lang/String;
            //     296: invokestatic 189	java/lang/Integer:parseInt	(Ljava/lang/String;)I
            //     299: ifeq +159 -> 458
            //     302: iconst_1
            //     303: istore 25
            //     305: aload_0
            //     306: aload 23
            //     308: invokevirtual 181	com/android/server/BackupManagerService$PerformFullRestoreTask:readHeaderLine	(Ljava/io/InputStream;)Ljava/lang/String;
            //     311: astore 50
            //     313: aload 50
            //     315: ldc_w 934
            //     318: invokevirtual 177	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     321: ifeq +143 -> 464
            //     324: iconst_1
            //     325: istore 27
            //     327: iload 27
            //     329: ifne +477 -> 806
            //     332: ldc 246
            //     334: ldc_w 936
            //     337: invokestatic 251	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     340: pop
            //     341: aload_0
            //     342: invokevirtual 716	com/android/server/BackupManagerService$PerformFullRestoreTask:tearDownPipes	()V
            //     345: aload_0
            //     346: aload_0
            //     347: getfield 99	com/android/server/BackupManagerService$PerformFullRestoreTask:mTargetApp	Landroid/content/pm/ApplicationInfo;
            //     350: invokevirtual 720	com/android/server/BackupManagerService$PerformFullRestoreTask:tearDownAgent	(Landroid/content/pm/ApplicationInfo;)V
            //     353: aload 24
            //     355: ifnull +8 -> 363
            //     358: aload 24
            //     360: invokevirtual 937	java/io/DataInputStream:close	()V
            //     363: aload 23
            //     365: ifnull +8 -> 373
            //     368: aload 23
            //     370: invokevirtual 938	java/io/FileInputStream:close	()V
            //     373: aload_0
            //     374: getfield 85	com/android/server/BackupManagerService$PerformFullRestoreTask:mInputFile	Landroid/os/ParcelFileDescriptor;
            //     377: invokevirtual 879	android/os/ParcelFileDescriptor:close	()V
            //     380: aload_0
            //     381: getfield 56	com/android/server/BackupManagerService$PerformFullRestoreTask:this$0	Lcom/android/server/BackupManagerService;
            //     384: getfield 883	com/android/server/BackupManagerService:mCurrentOpLock	Ljava/lang/Object;
            //     387: astore 43
            //     389: aload 43
            //     391: monitorenter
            //     392: aload_0
            //     393: getfield 56	com/android/server/BackupManagerService$PerformFullRestoreTask:this$0	Lcom/android/server/BackupManagerService;
            //     396: getfield 887	com/android/server/BackupManagerService:mCurrentOperations	Landroid/util/SparseArray;
            //     399: invokevirtual 892	android/util/SparseArray:clear	()V
            //     402: aload 43
            //     404: monitorexit
            //     405: aload_0
            //     406: getfield 93	com/android/server/BackupManagerService$PerformFullRestoreTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     409: astore 45
            //     411: aload 45
            //     413: monitorenter
            //     414: aload_0
            //     415: getfield 93	com/android/server/BackupManagerService$PerformFullRestoreTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     418: iconst_1
            //     419: invokevirtual 898	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
            //     422: aload_0
            //     423: getfield 93	com/android/server/BackupManagerService$PerformFullRestoreTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     426: invokevirtual 901	java/lang/Object:notifyAll	()V
            //     429: aload 45
            //     431: monitorexit
            //     432: aload_0
            //     433: invokevirtual 904	com/android/server/BackupManagerService$PerformFullRestoreTask:sendEndRestore	()V
            //     436: ldc 246
            //     438: ldc_w 906
            //     441: invokestatic 380	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     444: pop
            //     445: aload_0
            //     446: getfield 56	com/android/server/BackupManagerService$PerformFullRestoreTask:this$0	Lcom/android/server/BackupManagerService;
            //     449: getfield 910	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     452: invokevirtual 915	android/os/PowerManager$WakeLock:release	()V
            //     455: goto -266 -> 189
            //     458: iconst_0
            //     459: istore 25
            //     461: goto -156 -> 305
            //     464: aload_0
            //     465: getfield 89	com/android/server/BackupManagerService$PerformFullRestoreTask:mDecryptPassword	Ljava/lang/String;
            //     468: ifnull +34 -> 502
            //     471: aload_0
            //     472: getfield 89	com/android/server/BackupManagerService$PerformFullRestoreTask:mDecryptPassword	Ljava/lang/String;
            //     475: invokevirtual 523	java/lang/String:length	()I
            //     478: ifle +24 -> 502
            //     481: aload_0
            //     482: aload 50
            //     484: aload 23
            //     486: invokevirtual 940	com/android/server/BackupManagerService$PerformFullRestoreTask:decodeAesHeaderAndInitialize	(Ljava/lang/String;Ljava/io/InputStream;)Ljava/io/InputStream;
            //     489: astore 26
            //     491: aload 26
            //     493: ifnull -166 -> 327
            //     496: iconst_1
            //     497: istore 27
            //     499: goto -172 -> 327
            //     502: ldc 246
            //     504: ldc_w 942
            //     507: invokestatic 251	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     510: pop
            //     511: goto -184 -> 327
            //     514: astore 28
            //     516: aload 24
            //     518: astore_3
            //     519: aload 23
            //     521: astore_2
            //     522: ldc 246
            //     524: ldc_w 944
            //     527: invokestatic 266	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     530: pop
            //     531: aload_0
            //     532: invokevirtual 716	com/android/server/BackupManagerService$PerformFullRestoreTask:tearDownPipes	()V
            //     535: aload_0
            //     536: aload_0
            //     537: getfield 99	com/android/server/BackupManagerService$PerformFullRestoreTask:mTargetApp	Landroid/content/pm/ApplicationInfo;
            //     540: invokevirtual 720	com/android/server/BackupManagerService$PerformFullRestoreTask:tearDownAgent	(Landroid/content/pm/ApplicationInfo;)V
            //     543: aload_3
            //     544: ifnull +7 -> 551
            //     547: aload_3
            //     548: invokevirtual 937	java/io/DataInputStream:close	()V
            //     551: aload_2
            //     552: ifnull +7 -> 559
            //     555: aload_2
            //     556: invokevirtual 938	java/io/FileInputStream:close	()V
            //     559: aload_0
            //     560: getfield 85	com/android/server/BackupManagerService$PerformFullRestoreTask:mInputFile	Landroid/os/ParcelFileDescriptor;
            //     563: invokevirtual 879	android/os/ParcelFileDescriptor:close	()V
            //     566: aload_0
            //     567: getfield 56	com/android/server/BackupManagerService$PerformFullRestoreTask:this$0	Lcom/android/server/BackupManagerService;
            //     570: getfield 883	com/android/server/BackupManagerService:mCurrentOpLock	Ljava/lang/Object;
            //     573: astore 16
            //     575: aload 16
            //     577: monitorenter
            //     578: aload_0
            //     579: getfield 56	com/android/server/BackupManagerService$PerformFullRestoreTask:this$0	Lcom/android/server/BackupManagerService;
            //     582: getfield 887	com/android/server/BackupManagerService:mCurrentOperations	Landroid/util/SparseArray;
            //     585: invokevirtual 892	android/util/SparseArray:clear	()V
            //     588: aload 16
            //     590: monitorexit
            //     591: aload_0
            //     592: getfield 93	com/android/server/BackupManagerService$PerformFullRestoreTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     595: astore 18
            //     597: aload 18
            //     599: monitorenter
            //     600: aload_0
            //     601: getfield 93	com/android/server/BackupManagerService$PerformFullRestoreTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     604: iconst_1
            //     605: invokevirtual 898	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
            //     608: aload_0
            //     609: getfield 93	com/android/server/BackupManagerService$PerformFullRestoreTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     612: invokevirtual 901	java/lang/Object:notifyAll	()V
            //     615: aload 18
            //     617: monitorexit
            //     618: aload_0
            //     619: invokevirtual 904	com/android/server/BackupManagerService$PerformFullRestoreTask:sendEndRestore	()V
            //     622: ldc 246
            //     624: ldc_w 906
            //     627: invokestatic 380	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     630: pop
            //     631: aload_0
            //     632: getfield 56	com/android/server/BackupManagerService$PerformFullRestoreTask:this$0	Lcom/android/server/BackupManagerService;
            //     635: getfield 910	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     638: astore 21
            //     640: goto -456 -> 184
            //     643: ldc 246
            //     645: new 111	java/lang/StringBuilder
            //     648: dup
            //     649: invokespecial 258	java/lang/StringBuilder:<init>	()V
            //     652: ldc_w 946
            //     655: invokevirtual 132	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     658: aload 48
            //     660: invokevirtual 132	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     663: invokevirtual 145	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     666: invokestatic 251	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     669: pop
            //     670: goto -343 -> 327
            //     673: astore 5
            //     675: aload 24
            //     677: astore_3
            //     678: aload 23
            //     680: astore_2
            //     681: aload_0
            //     682: invokevirtual 716	com/android/server/BackupManagerService$PerformFullRestoreTask:tearDownPipes	()V
            //     685: aload_0
            //     686: aload_0
            //     687: getfield 99	com/android/server/BackupManagerService$PerformFullRestoreTask:mTargetApp	Landroid/content/pm/ApplicationInfo;
            //     690: invokevirtual 720	com/android/server/BackupManagerService$PerformFullRestoreTask:tearDownAgent	(Landroid/content/pm/ApplicationInfo;)V
            //     693: aload_3
            //     694: ifnull +7 -> 701
            //     697: aload_3
            //     698: invokevirtual 937	java/io/DataInputStream:close	()V
            //     701: aload_2
            //     702: ifnull +7 -> 709
            //     705: aload_2
            //     706: invokevirtual 938	java/io/FileInputStream:close	()V
            //     709: aload_0
            //     710: getfield 85	com/android/server/BackupManagerService$PerformFullRestoreTask:mInputFile	Landroid/os/ParcelFileDescriptor;
            //     713: invokevirtual 879	android/os/ParcelFileDescriptor:close	()V
            //     716: aload_0
            //     717: getfield 56	com/android/server/BackupManagerService$PerformFullRestoreTask:this$0	Lcom/android/server/BackupManagerService;
            //     720: getfield 883	com/android/server/BackupManagerService:mCurrentOpLock	Ljava/lang/Object;
            //     723: astore 8
            //     725: aload 8
            //     727: monitorenter
            //     728: aload_0
            //     729: getfield 56	com/android/server/BackupManagerService$PerformFullRestoreTask:this$0	Lcom/android/server/BackupManagerService;
            //     732: getfield 887	com/android/server/BackupManagerService:mCurrentOperations	Landroid/util/SparseArray;
            //     735: invokevirtual 892	android/util/SparseArray:clear	()V
            //     738: aload 8
            //     740: monitorexit
            //     741: aload_0
            //     742: getfield 93	com/android/server/BackupManagerService$PerformFullRestoreTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     745: astore 10
            //     747: aload 10
            //     749: monitorenter
            //     750: aload_0
            //     751: getfield 93	com/android/server/BackupManagerService$PerformFullRestoreTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     754: iconst_1
            //     755: invokevirtual 898	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
            //     758: aload_0
            //     759: getfield 93	com/android/server/BackupManagerService$PerformFullRestoreTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     762: invokevirtual 901	java/lang/Object:notifyAll	()V
            //     765: aload 10
            //     767: monitorexit
            //     768: aload_0
            //     769: invokevirtual 904	com/android/server/BackupManagerService$PerformFullRestoreTask:sendEndRestore	()V
            //     772: ldc 246
            //     774: ldc_w 906
            //     777: invokestatic 380	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     780: pop
            //     781: aload_0
            //     782: getfield 56	com/android/server/BackupManagerService$PerformFullRestoreTask:this$0	Lcom/android/server/BackupManagerService;
            //     785: getfield 910	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     788: invokevirtual 915	android/os/PowerManager$WakeLock:release	()V
            //     791: aload 5
            //     793: athrow
            //     794: ldc 246
            //     796: ldc_w 948
            //     799: invokestatic 251	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     802: pop
            //     803: goto -476 -> 327
            //     806: iload 25
            //     808: ifeq +146 -> 954
            //     811: new 950	java/util/zip/InflaterInputStream
            //     814: dup
            //     815: aload 26
            //     817: invokespecial 951	java/util/zip/InflaterInputStream:<init>	(Ljava/io/InputStream;)V
            //     820: astore 31
            //     822: aload_0
            //     823: aload 31
            //     825: aload 22
            //     827: invokevirtual 953	com/android/server/BackupManagerService$PerformFullRestoreTask:restoreOneFile	(Ljava/io/InputStream;[B)Z
            //     830: istore 32
            //     832: iload 32
            //     834: ifne -12 -> 822
            //     837: aload_0
            //     838: invokevirtual 716	com/android/server/BackupManagerService$PerformFullRestoreTask:tearDownPipes	()V
            //     841: aload_0
            //     842: aload_0
            //     843: getfield 99	com/android/server/BackupManagerService$PerformFullRestoreTask:mTargetApp	Landroid/content/pm/ApplicationInfo;
            //     846: invokevirtual 720	com/android/server/BackupManagerService$PerformFullRestoreTask:tearDownAgent	(Landroid/content/pm/ApplicationInfo;)V
            //     849: aload 24
            //     851: ifnull +8 -> 859
            //     854: aload 24
            //     856: invokevirtual 937	java/io/DataInputStream:close	()V
            //     859: aload 23
            //     861: ifnull +8 -> 869
            //     864: aload 23
            //     866: invokevirtual 938	java/io/FileInputStream:close	()V
            //     869: aload_0
            //     870: getfield 85	com/android/server/BackupManagerService$PerformFullRestoreTask:mInputFile	Landroid/os/ParcelFileDescriptor;
            //     873: invokevirtual 879	android/os/ParcelFileDescriptor:close	()V
            //     876: aload_0
            //     877: getfield 56	com/android/server/BackupManagerService$PerformFullRestoreTask:this$0	Lcom/android/server/BackupManagerService;
            //     880: getfield 883	com/android/server/BackupManagerService:mCurrentOpLock	Ljava/lang/Object;
            //     883: astore 35
            //     885: aload 35
            //     887: monitorenter
            //     888: aload_0
            //     889: getfield 56	com/android/server/BackupManagerService$PerformFullRestoreTask:this$0	Lcom/android/server/BackupManagerService;
            //     892: getfield 887	com/android/server/BackupManagerService:mCurrentOperations	Landroid/util/SparseArray;
            //     895: invokevirtual 892	android/util/SparseArray:clear	()V
            //     898: aload 35
            //     900: monitorexit
            //     901: aload_0
            //     902: getfield 93	com/android/server/BackupManagerService$PerformFullRestoreTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     905: astore 37
            //     907: aload 37
            //     909: monitorenter
            //     910: aload_0
            //     911: getfield 93	com/android/server/BackupManagerService$PerformFullRestoreTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     914: iconst_1
            //     915: invokevirtual 898	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
            //     918: aload_0
            //     919: getfield 93	com/android/server/BackupManagerService$PerformFullRestoreTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     922: invokevirtual 901	java/lang/Object:notifyAll	()V
            //     925: aload 37
            //     927: monitorexit
            //     928: aload_0
            //     929: invokevirtual 904	com/android/server/BackupManagerService$PerformFullRestoreTask:sendEndRestore	()V
            //     932: ldc 246
            //     934: ldc_w 906
            //     937: invokestatic 380	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     940: pop
            //     941: aload_0
            //     942: getfield 56	com/android/server/BackupManagerService$PerformFullRestoreTask:this$0	Lcom/android/server/BackupManagerService;
            //     945: getfield 910	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     948: invokevirtual 915	android/os/PowerManager$WakeLock:release	()V
            //     951: goto -762 -> 189
            //     954: aload 26
            //     956: astore 31
            //     958: goto -136 -> 822
            //     961: astore 6
            //     963: ldc 246
            //     965: ldc_w 955
            //     968: aload 6
            //     970: invokestatic 856	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     973: pop
            //     974: goto -258 -> 716
            //     977: astore 9
            //     979: aload 8
            //     981: monitorexit
            //     982: aload 9
            //     984: athrow
            //     985: astore 11
            //     987: aload 10
            //     989: monitorexit
            //     990: aload 11
            //     992: athrow
            //     993: astore 14
            //     995: ldc 246
            //     997: ldc_w 955
            //     1000: aload 14
            //     1002: invokestatic 856	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     1005: pop
            //     1006: goto -440 -> 566
            //     1009: astore 17
            //     1011: aload 16
            //     1013: monitorexit
            //     1014: aload 17
            //     1016: athrow
            //     1017: astore 19
            //     1019: aload 18
            //     1021: monitorexit
            //     1022: aload 19
            //     1024: athrow
            //     1025: astore 54
            //     1027: ldc 246
            //     1029: ldc_w 955
            //     1032: aload 54
            //     1034: invokestatic 856	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     1037: pop
            //     1038: goto -928 -> 110
            //     1041: astore 57
            //     1043: aload 56
            //     1045: monitorexit
            //     1046: aload 57
            //     1048: athrow
            //     1049: astore 59
            //     1051: aload 58
            //     1053: monitorexit
            //     1054: aload 59
            //     1056: athrow
            //     1057: astore 41
            //     1059: ldc 246
            //     1061: ldc_w 955
            //     1064: aload 41
            //     1066: invokestatic 856	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     1069: pop
            //     1070: goto -690 -> 380
            //     1073: astore 44
            //     1075: aload 43
            //     1077: monitorexit
            //     1078: aload 44
            //     1080: athrow
            //     1081: astore 46
            //     1083: aload 45
            //     1085: monitorexit
            //     1086: aload 46
            //     1088: athrow
            //     1089: astore 33
            //     1091: ldc 246
            //     1093: ldc_w 955
            //     1096: aload 33
            //     1098: invokestatic 856	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     1101: pop
            //     1102: goto -226 -> 876
            //     1105: astore 36
            //     1107: aload 35
            //     1109: monitorexit
            //     1110: aload 36
            //     1112: athrow
            //     1113: astore 38
            //     1115: aload 37
            //     1117: monitorexit
            //     1118: aload 38
            //     1120: athrow
            //     1121: astore 5
            //     1123: goto -442 -> 681
            //     1126: astore 5
            //     1128: aload 23
            //     1130: astore_2
            //     1131: goto -450 -> 681
            //     1134: astore 4
            //     1136: goto -614 -> 522
            //     1139: astore 52
            //     1141: aload 23
            //     1143: astore_2
            //     1144: goto -622 -> 522
            //
            // Exception table:
            //     from	to	target	type
            //     239	341	514	java/io/IOException
            //     464	511	514	java/io/IOException
            //     643	670	514	java/io/IOException
            //     794	832	514	java/io/IOException
            //     239	341	673	finally
            //     464	511	673	finally
            //     643	670	673	finally
            //     794	832	673	finally
            //     697	716	961	java/io/IOException
            //     728	741	977	finally
            //     979	982	977	finally
            //     750	768	985	finally
            //     987	990	985	finally
            //     547	566	993	java/io/IOException
            //     578	591	1009	finally
            //     1011	1014	1009	finally
            //     600	618	1017	finally
            //     1019	1022	1017	finally
            //     95	110	1025	java/io/IOException
            //     122	135	1041	finally
            //     1043	1046	1041	finally
            //     144	162	1049	finally
            //     1051	1054	1049	finally
            //     358	380	1057	java/io/IOException
            //     392	405	1073	finally
            //     1075	1078	1073	finally
            //     414	432	1081	finally
            //     1083	1086	1081	finally
            //     854	876	1089	java/io/IOException
            //     888	901	1105	finally
            //     1107	1110	1105	finally
            //     910	928	1113	finally
            //     1115	1118	1113	finally
            //     43	79	1121	finally
            //     190	218	1121	finally
            //     522	531	1121	finally
            //     218	229	1126	finally
            //     43	79	1134	java/io/IOException
            //     190	218	1134	java/io/IOException
            //     218	229	1139	java/io/IOException
        }

        void sendEndRestore()
        {
            if (this.mObserver != null);
            try
            {
                this.mObserver.onEndRestore();
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                {
                    Slog.w("BackupManagerService", "full restore observer went away: endRestore");
                    this.mObserver = null;
                }
            }
        }

        void sendOnRestorePackage(String paramString)
        {
            if (this.mObserver != null);
            try
            {
                this.mObserver.onRestorePackage(paramString);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                {
                    Slog.w("BackupManagerService", "full restore observer went away: restorePackage");
                    this.mObserver = null;
                }
            }
        }

        void sendStartRestore()
        {
            if (this.mObserver != null);
            try
            {
                this.mObserver.onStartRestore();
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                {
                    Slog.w("BackupManagerService", "full restore observer went away: startRestore");
                    this.mObserver = null;
                }
            }
        }

        void setUpPipes()
            throws IOException
        {
            this.mPipes = ParcelFileDescriptor.createPipe();
        }

        void skipTarPadding(long paramLong, InputStream paramInputStream)
            throws IOException
        {
            long l = (paramLong + 512L) % 512L;
            if (l > 0L)
            {
                int i = 512 - (int)l;
                if (readExactly(paramInputStream, new byte[i], 0, i) == i)
                    this.mBytes += i;
            }
            else
            {
                return;
            }
            throw new IOException("Unexpected EOF in padding");
        }

        void tearDownAgent(ApplicationInfo paramApplicationInfo)
        {
            if (this.mAgent != null);
            try
            {
                BackupManagerService.this.mActivityManager.unbindBackupAgent(paramApplicationInfo);
                if ((paramApplicationInfo.uid != 1000) && (!paramApplicationInfo.packageName.equals("com.android.backupconfirm")))
                {
                    Slog.d("BackupManagerService", "Killing host process");
                    BackupManagerService.this.mActivityManager.killApplicationProcess(paramApplicationInfo.processName, paramApplicationInfo.uid);
                }
                while (true)
                {
                    this.mAgent = null;
                    return;
                    Slog.d("BackupManagerService", "Not killing after full restore");
                }
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Slog.d("BackupManagerService", "Lost app trying to shut down");
            }
        }

        void tearDownPipes()
        {
            if (this.mPipes != null);
            try
            {
                this.mPipes[0].close();
                this.mPipes[0] = null;
                this.mPipes[1].close();
                this.mPipes[1] = null;
                this.mPipes = null;
                return;
            }
            catch (IOException localIOException)
            {
                while (true)
                    Slog.w("BackupManagerService", "Couldn't close agent pipes", localIOException);
            }
        }

        class RestoreDeleteObserver extends IPackageDeleteObserver.Stub
        {
            final AtomicBoolean mDone = new AtomicBoolean();
            int mResult;

            RestoreDeleteObserver()
            {
            }

            public void packageDeleted(String paramString, int paramInt)
                throws RemoteException
            {
                synchronized (this.mDone)
                {
                    this.mResult = paramInt;
                    this.mDone.set(true);
                    this.mDone.notifyAll();
                    return;
                }
            }

            public void reset()
            {
                synchronized (this.mDone)
                {
                    this.mDone.set(false);
                    return;
                }
            }

            public void waitForCompletion()
            {
                synchronized (this.mDone)
                {
                    while (true)
                    {
                        boolean bool = this.mDone.get();
                        if (bool)
                            break;
                        try
                        {
                            this.mDone.wait();
                        }
                        catch (InterruptedException localInterruptedException)
                        {
                        }
                    }
                    return;
                }
            }
        }

        class RestoreInstallObserver extends IPackageInstallObserver.Stub
        {
            final AtomicBoolean mDone = new AtomicBoolean();
            String mPackageName;
            int mResult;

            RestoreInstallObserver()
            {
            }

            int getResult()
            {
                return this.mResult;
            }

            public void packageInstalled(String paramString, int paramInt)
                throws RemoteException
            {
                synchronized (this.mDone)
                {
                    this.mResult = paramInt;
                    this.mPackageName = paramString;
                    this.mDone.set(true);
                    this.mDone.notifyAll();
                    return;
                }
            }

            public void reset()
            {
                synchronized (this.mDone)
                {
                    this.mDone.set(false);
                    return;
                }
            }

            public void waitForCompletion()
            {
                synchronized (this.mDone)
                {
                    while (true)
                    {
                        boolean bool = this.mDone.get();
                        if (bool)
                            break;
                        try
                        {
                            this.mDone.wait();
                        }
                        catch (InterruptedException localInterruptedException)
                        {
                        }
                    }
                    return;
                }
            }
        }

        class RestoreFileRunnable
            implements Runnable
        {
            IBackupAgent mAgent;
            BackupManagerService.FileMetadata mInfo;
            ParcelFileDescriptor mSocket;
            int mToken;

            RestoreFileRunnable(IBackupAgent paramFileMetadata, BackupManagerService.FileMetadata paramParcelFileDescriptor, ParcelFileDescriptor paramInt, int arg5)
                throws IOException
            {
                this.mAgent = paramFileMetadata;
                this.mInfo = paramParcelFileDescriptor;
                int i;
                this.mToken = i;
                this.mSocket = ParcelFileDescriptor.dup(paramInt.getFileDescriptor());
            }

            public void run()
            {
                try
                {
                    this.mAgent.doRestoreFile(this.mSocket, this.mInfo.size, this.mInfo.type, this.mInfo.domain, this.mInfo.path, this.mInfo.mode, this.mInfo.mtime, this.mToken, BackupManagerService.this.mBackupManagerBinder);
                    label69: return;
                }
                catch (RemoteException localRemoteException)
                {
                    break label69;
                }
            }
        }
    }

    static enum RestorePolicy
    {
        static
        {
            ACCEPT = new RestorePolicy("ACCEPT", 1);
            ACCEPT_IF_APK = new RestorePolicy("ACCEPT_IF_APK", 2);
            RestorePolicy[] arrayOfRestorePolicy = new RestorePolicy[3];
            arrayOfRestorePolicy[0] = IGNORE;
            arrayOfRestorePolicy[1] = ACCEPT;
            arrayOfRestorePolicy[2] = ACCEPT_IF_APK;
        }
    }

    static class FileMetadata
    {
        String domain;
        String installerPackageName;
        long mode;
        long mtime;
        String packageName;
        String path;
        long size;
        int type;

        public String toString()
        {
            StringBuilder localStringBuilder = new StringBuilder(128);
            localStringBuilder.append("FileMetadata{");
            localStringBuilder.append(this.packageName);
            localStringBuilder.append(',');
            localStringBuilder.append(this.type);
            localStringBuilder.append(',');
            localStringBuilder.append(this.domain);
            localStringBuilder.append(':');
            localStringBuilder.append(this.path);
            localStringBuilder.append(',');
            localStringBuilder.append(this.size);
            localStringBuilder.append('}');
            return localStringBuilder.toString();
        }
    }

    class PerformFullBackupTask
        implements Runnable
    {
        boolean mAllApps;
        String mCurrentPassword;
        DeflaterOutputStream mDeflater;
        String mEncryptPassword;
        File mFilesDir;
        boolean mIncludeApks;
        boolean mIncludeShared;
        final boolean mIncludeSystem;
        AtomicBoolean mLatchObject;
        File mManifestFile;
        IFullBackupRestoreObserver mObserver;
        ParcelFileDescriptor mOutputFile;
        String[] mPackages;

        PerformFullBackupTask(ParcelFileDescriptor paramIFullBackupRestoreObserver, IFullBackupRestoreObserver paramBoolean1, boolean paramBoolean2, boolean paramString1, String paramString2, String paramBoolean3, boolean paramBoolean4, boolean paramArrayOfString, String[] paramAtomicBoolean, AtomicBoolean arg11)
        {
            this.mOutputFile = paramIFullBackupRestoreObserver;
            this.mObserver = paramBoolean1;
            this.mIncludeApks = paramBoolean2;
            this.mIncludeShared = paramString1;
            this.mAllApps = paramBoolean4;
            this.mIncludeSystem = paramArrayOfString;
            this.mPackages = paramAtomicBoolean;
            this.mCurrentPassword = paramString2;
            if ((paramBoolean3 == null) || ("".equals(paramBoolean3)));
            for (this.mEncryptPassword = paramString2; ; this.mEncryptPassword = paramBoolean3)
            {
                Object localObject;
                this.mLatchObject = localObject;
                this.mFilesDir = new File("/data/system");
                this.mManifestFile = new File(this.mFilesDir, "_manifest");
                return;
            }
        }

        // ERROR //
        private void backupOnePackage(PackageInfo paramPackageInfo, OutputStream paramOutputStream)
            throws RemoteException
        {
            // Byte code:
            //     0: ldc 106
            //     2: new 108	java/lang/StringBuilder
            //     5: dup
            //     6: invokespecial 109	java/lang/StringBuilder:<init>	()V
            //     9: ldc 111
            //     11: invokevirtual 115	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     14: aload_1
            //     15: getfield 120	android/content/pm/PackageInfo:packageName	Ljava/lang/String;
            //     18: invokevirtual 115	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     21: invokevirtual 124	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     24: invokestatic 130	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     27: pop
            //     28: aload_0
            //     29: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     32: aload_1
            //     33: getfield 134	android/content/pm/PackageInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
            //     36: iconst_1
            //     37: invokevirtual 138	com/android/server/BackupManagerService:bindToAgentSynchronous	(Landroid/content/pm/ApplicationInfo;I)Landroid/app/IBackupAgent;
            //     40: astore 4
            //     42: aload 4
            //     44: ifnull +525 -> 569
            //     47: aconst_null
            //     48: astore 6
            //     50: invokestatic 144	android/os/ParcelFileDescriptor:createPipe	()[Landroid/os/ParcelFileDescriptor;
            //     53: astore 6
            //     55: aload_1
            //     56: getfield 134	android/content/pm/PackageInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
            //     59: astore 16
            //     61: aload_1
            //     62: getfield 120	android/content/pm/PackageInfo:packageName	Ljava/lang/String;
            //     65: ldc 146
            //     67: invokevirtual 66	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     70: istore 17
            //     72: aload_0
            //     73: getfield 48	com/android/server/BackupManagerService$PerformFullBackupTask:mIncludeApks	Z
            //     76: ifeq +217 -> 293
            //     79: iload 17
            //     81: ifne +212 -> 293
            //     84: ldc 147
            //     86: aload 16
            //     88: getfield 153	android/content/pm/ApplicationInfo:flags	I
            //     91: iand
            //     92: ifne +201 -> 293
            //     95: iconst_1
            //     96: aload 16
            //     98: getfield 153	android/content/pm/ApplicationInfo:flags	I
            //     101: iand
            //     102: ifeq +524 -> 626
            //     105: sipush 128
            //     108: aload 16
            //     110: getfield 153	android/content/pm/ApplicationInfo:flags	I
            //     113: iand
            //     114: ifeq +179 -> 293
            //     117: goto +509 -> 626
            //     120: aload_0
            //     121: aload 19
            //     123: invokevirtual 156	com/android/server/BackupManagerService$PerformFullBackupTask:sendOnBackupPackage	(Ljava/lang/String;)V
            //     126: aload_0
            //     127: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     130: invokevirtual 160	com/android/server/BackupManagerService:generateToken	()I
            //     133: istore 20
            //     135: aload 6
            //     137: iconst_1
            //     138: aaload
            //     139: astore 21
            //     141: iload 17
            //     143: ifne +498 -> 641
            //     146: iconst_1
            //     147: istore 22
            //     149: new 11	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner
            //     152: dup
            //     153: aload_0
            //     154: aload_1
            //     155: aload 4
            //     157: aload 21
            //     159: iload 20
            //     161: iload 18
            //     163: iload 22
            //     165: invokespecial 163	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:<init>	(Lcom/android/server/BackupManagerService$PerformFullBackupTask;Landroid/content/pm/PackageInfo;Landroid/app/IBackupAgent;Landroid/os/ParcelFileDescriptor;IZZ)V
            //     168: astore 23
            //     170: aload 6
            //     172: iconst_1
            //     173: aaload
            //     174: invokevirtual 166	android/os/ParcelFileDescriptor:close	()V
            //     177: aload 6
            //     179: iconst_1
            //     180: aconst_null
            //     181: aastore
            //     182: new 168	java/lang/Thread
            //     185: dup
            //     186: aload 23
            //     188: invokespecial 171	java/lang/Thread:<init>	(Ljava/lang/Runnable;)V
            //     191: astore 24
            //     193: aload 24
            //     195: invokevirtual 174	java/lang/Thread:start	()V
            //     198: new 176	java/io/FileInputStream
            //     201: dup
            //     202: aload 6
            //     204: iconst_0
            //     205: aaload
            //     206: invokevirtual 180	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
            //     209: invokespecial 183	java/io/FileInputStream:<init>	(Ljava/io/FileDescriptor;)V
            //     212: astore 25
            //     214: new 185	java/io/DataInputStream
            //     217: dup
            //     218: aload 25
            //     220: invokespecial 188	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
            //     223: astore 26
            //     225: sipush 16384
            //     228: newarray byte
            //     230: astore 32
            //     232: aload 26
            //     234: invokevirtual 191	java/io/DataInputStream:readInt	()I
            //     237: istore 33
            //     239: iload 33
            //     241: ifle +79 -> 320
            //     244: iload 33
            //     246: ifle -14 -> 232
            //     249: iload 33
            //     251: aload 32
            //     253: arraylength
            //     254: if_icmple +393 -> 647
            //     257: aload 32
            //     259: arraylength
            //     260: istore 34
            //     262: aload 26
            //     264: aload 32
            //     266: iconst_0
            //     267: iload 34
            //     269: invokevirtual 195	java/io/DataInputStream:read	([BII)I
            //     272: istore 35
            //     274: aload_2
            //     275: aload 32
            //     277: iconst_0
            //     278: iload 35
            //     280: invokevirtual 201	java/io/OutputStream:write	([BII)V
            //     283: iload 33
            //     285: iload 35
            //     287: isub
            //     288: istore 33
            //     290: goto -46 -> 244
            //     293: iconst_0
            //     294: istore 18
            //     296: goto +333 -> 629
            //     299: aload_1
            //     300: getfield 120	android/content/pm/PackageInfo:packageName	Ljava/lang/String;
            //     303: astore 19
            //     305: goto -185 -> 120
            //     308: astore 27
            //     310: ldc 106
            //     312: ldc 203
            //     314: aload 27
            //     316: invokestatic 207	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     319: pop
            //     320: aload_0
            //     321: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     324: iload 20
            //     326: invokevirtual 211	com/android/server/BackupManagerService:waitUntilOperationComplete	(I)Z
            //     329: ifne +74 -> 403
            //     332: ldc 106
            //     334: new 108	java/lang/StringBuilder
            //     337: dup
            //     338: invokespecial 109	java/lang/StringBuilder:<init>	()V
            //     341: ldc 213
            //     343: invokevirtual 115	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     346: aload_1
            //     347: getfield 120	android/content/pm/PackageInfo:packageName	Ljava/lang/String;
            //     350: invokevirtual 115	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     353: invokevirtual 124	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     356: invokestatic 216	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     359: pop
            //     360: aload_2
            //     361: invokevirtual 219	java/io/OutputStream:flush	()V
            //     364: aload 6
            //     366: ifnull +31 -> 397
            //     369: aload 6
            //     371: iconst_0
            //     372: aaload
            //     373: ifnull +10 -> 383
            //     376: aload 6
            //     378: iconst_0
            //     379: aaload
            //     380: invokevirtual 166	android/os/ParcelFileDescriptor:close	()V
            //     383: aload 6
            //     385: iconst_1
            //     386: aaload
            //     387: ifnull +10 -> 397
            //     390: aload 6
            //     392: iconst_1
            //     393: aaload
            //     394: invokevirtual 166	android/os/ParcelFileDescriptor:close	()V
            //     397: aload_0
            //     398: aload_1
            //     399: invokespecial 223	com/android/server/BackupManagerService$PerformFullBackupTask:tearDown	(Landroid/content/pm/PackageInfo;)V
            //     402: return
            //     403: ldc 106
            //     405: new 108	java/lang/StringBuilder
            //     408: dup
            //     409: invokespecial 109	java/lang/StringBuilder:<init>	()V
            //     412: ldc 225
            //     414: invokevirtual 115	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     417: aload_1
            //     418: getfield 120	android/content/pm/PackageInfo:packageName	Ljava/lang/String;
            //     421: invokevirtual 115	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     424: invokevirtual 124	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     427: invokestatic 130	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     430: pop
            //     431: goto -71 -> 360
            //     434: astore 10
            //     436: ldc 106
            //     438: new 108	java/lang/StringBuilder
            //     441: dup
            //     442: invokespecial 109	java/lang/StringBuilder:<init>	()V
            //     445: ldc 227
            //     447: invokevirtual 115	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     450: aload_1
            //     451: getfield 120	android/content/pm/PackageInfo:packageName	Ljava/lang/String;
            //     454: invokevirtual 115	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     457: invokevirtual 124	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     460: aload 10
            //     462: invokestatic 229	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     465: pop
            //     466: aload_2
            //     467: invokevirtual 219	java/io/OutputStream:flush	()V
            //     470: aload 6
            //     472: ifnull -75 -> 397
            //     475: aload 6
            //     477: iconst_0
            //     478: aaload
            //     479: ifnull +10 -> 489
            //     482: aload 6
            //     484: iconst_0
            //     485: aaload
            //     486: invokevirtual 166	android/os/ParcelFileDescriptor:close	()V
            //     489: aload 6
            //     491: iconst_1
            //     492: aaload
            //     493: ifnull -96 -> 397
            //     496: aload 6
            //     498: iconst_1
            //     499: aaload
            //     500: invokevirtual 166	android/os/ParcelFileDescriptor:close	()V
            //     503: goto -106 -> 397
            //     506: astore 12
            //     508: ldc 106
            //     510: astore 13
            //     512: ldc 231
            //     514: astore 14
            //     516: aload 13
            //     518: aload 14
            //     520: invokestatic 234	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     523: pop
            //     524: goto -127 -> 397
            //     527: astore 7
            //     529: aload_2
            //     530: invokevirtual 219	java/io/OutputStream:flush	()V
            //     533: aload 6
            //     535: ifnull +31 -> 566
            //     538: aload 6
            //     540: iconst_0
            //     541: aaload
            //     542: ifnull +10 -> 552
            //     545: aload 6
            //     547: iconst_0
            //     548: aaload
            //     549: invokevirtual 166	android/os/ParcelFileDescriptor:close	()V
            //     552: aload 6
            //     554: iconst_1
            //     555: aaload
            //     556: ifnull +10 -> 566
            //     559: aload 6
            //     561: iconst_1
            //     562: aaload
            //     563: invokevirtual 166	android/os/ParcelFileDescriptor:close	()V
            //     566: aload 7
            //     568: athrow
            //     569: ldc 106
            //     571: new 108	java/lang/StringBuilder
            //     574: dup
            //     575: invokespecial 109	java/lang/StringBuilder:<init>	()V
            //     578: ldc 236
            //     580: invokevirtual 115	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     583: aload_1
            //     584: getfield 120	android/content/pm/PackageInfo:packageName	Ljava/lang/String;
            //     587: invokevirtual 115	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     590: invokevirtual 124	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     593: invokestatic 234	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     596: pop
            //     597: goto -200 -> 397
            //     600: astore 8
            //     602: ldc 106
            //     604: ldc 231
            //     606: invokestatic 234	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     609: pop
            //     610: goto -44 -> 566
            //     613: astore 30
            //     615: ldc 106
            //     617: astore 13
            //     619: ldc 231
            //     621: astore 14
            //     623: goto -107 -> 516
            //     626: iconst_1
            //     627: istore 18
            //     629: iload 17
            //     631: ifeq -332 -> 299
            //     634: ldc 238
            //     636: astore 19
            //     638: goto -518 -> 120
            //     641: iconst_0
            //     642: istore 22
            //     644: goto -495 -> 149
            //     647: iload 33
            //     649: istore 34
            //     651: goto -389 -> 262
            //
            // Exception table:
            //     from	to	target	type
            //     198	283	308	java/io/IOException
            //     50	198	434	java/io/IOException
            //     299	360	434	java/io/IOException
            //     403	431	434	java/io/IOException
            //     466	503	506	java/io/IOException
            //     50	198	527	finally
            //     198	283	527	finally
            //     299	360	527	finally
            //     403	431	527	finally
            //     436	466	527	finally
            //     529	566	600	java/io/IOException
            //     360	397	613	java/io/IOException
        }

        private OutputStream emitAesBackupHeader(StringBuilder paramStringBuilder, OutputStream paramOutputStream)
            throws Exception
        {
            byte[] arrayOfByte1 = BackupManagerService.this.randomBytes(512);
            SecretKey localSecretKey = BackupManagerService.this.buildPasswordKey(this.mEncryptPassword, arrayOfByte1, 10000);
            byte[] arrayOfByte2 = new byte[32];
            BackupManagerService.this.mRng.nextBytes(arrayOfByte2);
            byte[] arrayOfByte3 = BackupManagerService.this.randomBytes(512);
            Cipher localCipher1 = Cipher.getInstance("AES/CBC/PKCS5Padding");
            SecretKeySpec localSecretKeySpec = new SecretKeySpec(arrayOfByte2, "AES");
            localCipher1.init(1, localSecretKeySpec);
            CipherOutputStream localCipherOutputStream = new CipherOutputStream(paramOutputStream, localCipher1);
            paramStringBuilder.append("AES-256");
            paramStringBuilder.append('\n');
            paramStringBuilder.append(BackupManagerService.this.byteArrayToHex(arrayOfByte1));
            paramStringBuilder.append('\n');
            paramStringBuilder.append(BackupManagerService.this.byteArrayToHex(arrayOfByte3));
            paramStringBuilder.append('\n');
            paramStringBuilder.append(10000);
            paramStringBuilder.append('\n');
            Cipher localCipher2 = Cipher.getInstance("AES/CBC/PKCS5Padding");
            localCipher2.init(1, localSecretKey);
            byte[] arrayOfByte4 = localCipher2.getIV();
            paramStringBuilder.append(BackupManagerService.this.byteArrayToHex(arrayOfByte4));
            paramStringBuilder.append('\n');
            byte[] arrayOfByte5 = localCipher1.getIV();
            byte[] arrayOfByte6 = localSecretKeySpec.getEncoded();
            byte[] arrayOfByte7 = BackupManagerService.this.makeKeyChecksum(localSecretKeySpec.getEncoded(), arrayOfByte3, 10000);
            ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream(3 + (arrayOfByte5.length + arrayOfByte6.length + arrayOfByte7.length));
            DataOutputStream localDataOutputStream = new DataOutputStream(localByteArrayOutputStream);
            localDataOutputStream.writeByte(arrayOfByte5.length);
            localDataOutputStream.write(arrayOfByte5);
            localDataOutputStream.writeByte(arrayOfByte6.length);
            localDataOutputStream.write(arrayOfByte6);
            localDataOutputStream.writeByte(arrayOfByte7.length);
            localDataOutputStream.write(arrayOfByte7);
            localDataOutputStream.flush();
            byte[] arrayOfByte8 = localCipher2.doFinal(localByteArrayOutputStream.toByteArray());
            paramStringBuilder.append(BackupManagerService.this.byteArrayToHex(arrayOfByte8));
            paramStringBuilder.append('\n');
            return localCipherOutputStream;
        }

        private void finalizeBackup(OutputStream paramOutputStream)
        {
            try
            {
                paramOutputStream.write(new byte[1024]);
                return;
            }
            catch (IOException localIOException)
            {
                while (true)
                    Slog.w("BackupManagerService", "Error attempting to finalize backup stream");
            }
        }

        private void tearDown(PackageInfo paramPackageInfo)
        {
            ApplicationInfo localApplicationInfo;
            if (paramPackageInfo != null)
            {
                localApplicationInfo = paramPackageInfo.applicationInfo;
                if (localApplicationInfo == null);
            }
            try
            {
                BackupManagerService.this.mActivityManager.unbindBackupAgent(localApplicationInfo);
                if ((localApplicationInfo.uid != 1000) && (localApplicationInfo.uid != 1001))
                    BackupManagerService.this.mActivityManager.killApplicationProcess(localApplicationInfo.processName, localApplicationInfo.uid);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Slog.d("BackupManagerService", "Lost app trying to shut down");
            }
        }

        private void writeApkToBackup(PackageInfo paramPackageInfo, BackupDataOutput paramBackupDataOutput)
        {
            String str1 = paramPackageInfo.applicationInfo.sourceDir;
            String str2 = new File(str1).getParent();
            FullBackup.backupToTar(paramPackageInfo.packageName, "a", null, str2, str1, paramBackupDataOutput);
            File localFile1 = Environment.getExternalStorageAppObbDirectory(paramPackageInfo.packageName);
            if (localFile1 != null)
            {
                File[] arrayOfFile = localFile1.listFiles();
                if (arrayOfFile != null)
                {
                    String str3 = localFile1.getAbsolutePath();
                    int i = arrayOfFile.length;
                    for (int j = 0; j < i; j++)
                    {
                        File localFile2 = arrayOfFile[j];
                        FullBackup.backupToTar(paramPackageInfo.packageName, "obb", null, str3, localFile2.getAbsolutePath(), paramBackupDataOutput);
                    }
                }
            }
        }

        private void writeAppManifest(PackageInfo paramPackageInfo, File paramFile, boolean paramBoolean)
            throws IOException
        {
            StringBuilder localStringBuilder = new StringBuilder(4096);
            StringBuilderPrinter localStringBuilderPrinter = new StringBuilderPrinter(localStringBuilder);
            localStringBuilderPrinter.println(Integer.toString(1));
            localStringBuilderPrinter.println(paramPackageInfo.packageName);
            localStringBuilderPrinter.println(Integer.toString(paramPackageInfo.versionCode));
            localStringBuilderPrinter.println(Integer.toString(Build.VERSION.SDK_INT));
            String str1 = BackupManagerService.this.mPackageManager.getInstallerPackageName(paramPackageInfo.packageName);
            String str2;
            if (str1 != null)
            {
                localStringBuilderPrinter.println(str1);
                if (!paramBoolean)
                    break label159;
                str2 = "1";
                label101: localStringBuilderPrinter.println(str2);
                if (paramPackageInfo.signatures != null)
                    break label167;
                localStringBuilderPrinter.println("0");
            }
            while (true)
            {
                FileOutputStream localFileOutputStream = new FileOutputStream(paramFile);
                localFileOutputStream.write(localStringBuilder.toString().getBytes());
                localFileOutputStream.close();
                return;
                str1 = "";
                break;
                label159: str2 = "0";
                break label101;
                label167: localStringBuilderPrinter.println(Integer.toString(paramPackageInfo.signatures.length));
                Signature[] arrayOfSignature = paramPackageInfo.signatures;
                int i = arrayOfSignature.length;
                for (int j = 0; j < i; j++)
                    localStringBuilderPrinter.println(arrayOfSignature[j].toCharsString());
            }
        }

        // ERROR //
        public void run()
        {
            // Byte code:
            //     0: new 445	java/util/ArrayList
            //     3: dup
            //     4: invokespecial 446	java/util/ArrayList:<init>	()V
            //     7: astore_1
            //     8: ldc 106
            //     10: ldc_w 448
            //     13: invokestatic 450	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
            //     16: pop
            //     17: aload_0
            //     18: invokevirtual 453	com/android/server/BackupManagerService$PerformFullBackupTask:sendStartBackup	()V
            //     21: aload_0
            //     22: getfield 52	com/android/server/BackupManagerService$PerformFullBackupTask:mAllApps	Z
            //     25: ifeq +77 -> 102
            //     28: aload_0
            //     29: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     32: invokestatic 411	com/android/server/BackupManagerService:access$600	(Lcom/android/server/BackupManagerService;)Landroid/content/pm/PackageManager;
            //     35: bipush 64
            //     37: invokevirtual 457	android/content/pm/PackageManager:getInstalledPackages	(I)Ljava/util/List;
            //     40: astore_1
            //     41: aload_0
            //     42: getfield 54	com/android/server/BackupManagerService$PerformFullBackupTask:mIncludeSystem	Z
            //     45: ifne +57 -> 102
            //     48: iconst_0
            //     49: istore 82
            //     51: iload 82
            //     53: aload_1
            //     54: invokeinterface 462 1 0
            //     59: if_icmpge +43 -> 102
            //     62: iconst_1
            //     63: aload_1
            //     64: iload 82
            //     66: invokeinterface 466 2 0
            //     71: checkcast 117	android/content/pm/PackageInfo
            //     74: getfield 134	android/content/pm/PackageInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
            //     77: getfield 153	android/content/pm/ApplicationInfo:flags	I
            //     80: iand
            //     81: ifeq +15 -> 96
            //     84: aload_1
            //     85: iload 82
            //     87: invokeinterface 469 2 0
            //     92: pop
            //     93: goto -42 -> 51
            //     96: iinc 82 1
            //     99: goto -48 -> 51
            //     102: aload_0
            //     103: getfield 56	com/android/server/BackupManagerService$PerformFullBackupTask:mPackages	[Ljava/lang/String;
            //     106: ifnull +100 -> 206
            //     109: aload_0
            //     110: getfield 56	com/android/server/BackupManagerService$PerformFullBackupTask:mPackages	[Ljava/lang/String;
            //     113: astore 74
            //     115: aload 74
            //     117: arraylength
            //     118: istore 75
            //     120: iconst_0
            //     121: istore 76
            //     123: iload 76
            //     125: iload 75
            //     127: if_icmpge +79 -> 206
            //     130: aload 74
            //     132: iload 76
            //     134: aaload
            //     135: astore 77
            //     137: aload_0
            //     138: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     141: invokestatic 411	com/android/server/BackupManagerService:access$600	(Lcom/android/server/BackupManagerService;)Landroid/content/pm/PackageManager;
            //     144: aload 77
            //     146: bipush 64
            //     148: invokevirtual 473	android/content/pm/PackageManager:getPackageInfo	(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
            //     151: astore 80
            //     153: aload_1
            //     154: aload 80
            //     156: invokeinterface 476 2 0
            //     161: pop
            //     162: iinc 76 1
            //     165: goto -42 -> 123
            //     168: astore 78
            //     170: ldc 106
            //     172: new 108	java/lang/StringBuilder
            //     175: dup
            //     176: invokespecial 109	java/lang/StringBuilder:<init>	()V
            //     179: ldc_w 478
            //     182: invokevirtual 115	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     185: aload 77
            //     187: invokevirtual 115	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     190: ldc_w 480
            //     193: invokevirtual 115	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     196: invokevirtual 124	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     199: invokestatic 234	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     202: pop
            //     203: goto -41 -> 162
            //     206: iconst_0
            //     207: istore_3
            //     208: iload_3
            //     209: aload_1
            //     210: invokeinterface 462 1 0
            //     215: if_icmpge +60 -> 275
            //     218: aload_1
            //     219: iload_3
            //     220: invokeinterface 466 2 0
            //     225: checkcast 117	android/content/pm/PackageInfo
            //     228: astore 72
            //     230: ldc_w 481
            //     233: aload 72
            //     235: getfield 134	android/content/pm/PackageInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
            //     238: getfield 153	android/content/pm/ApplicationInfo:flags	I
            //     241: iand
            //     242: ifeq +16 -> 258
            //     245: aload 72
            //     247: getfield 120	android/content/pm/PackageInfo:packageName	Ljava/lang/String;
            //     250: ldc 146
            //     252: invokevirtual 66	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     255: ifeq +14 -> 269
            //     258: aload_1
            //     259: iload_3
            //     260: invokeinterface 469 2 0
            //     265: pop
            //     266: goto -58 -> 208
            //     269: iinc 3 1
            //     272: goto -64 -> 208
            //     275: iconst_0
            //     276: istore 4
            //     278: iload 4
            //     280: aload_1
            //     281: invokeinterface 462 1 0
            //     286: if_icmpge +59 -> 345
            //     289: aload_1
            //     290: iload 4
            //     292: invokeinterface 466 2 0
            //     297: checkcast 117	android/content/pm/PackageInfo
            //     300: astore 70
            //     302: aload 70
            //     304: getfield 134	android/content/pm/PackageInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
            //     307: getfield 347	android/content/pm/ApplicationInfo:uid	I
            //     310: sipush 10000
            //     313: if_icmpge +26 -> 339
            //     316: aload 70
            //     318: getfield 134	android/content/pm/PackageInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
            //     321: getfield 484	android/content/pm/ApplicationInfo:backupAgentName	Ljava/lang/String;
            //     324: ifnonnull +15 -> 339
            //     327: aload_1
            //     328: iload 4
            //     330: invokeinterface 469 2 0
            //     335: pop
            //     336: goto -58 -> 278
            //     339: iinc 4 1
            //     342: goto -64 -> 278
            //     345: new 427	java/io/FileOutputStream
            //     348: dup
            //     349: aload_0
            //     350: getfield 44	com/android/server/BackupManagerService$PerformFullBackupTask:mOutputFile	Landroid/os/ParcelFileDescriptor;
            //     353: invokevirtual 180	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
            //     356: invokespecial 485	java/io/FileOutputStream:<init>	(Ljava/io/FileDescriptor;)V
            //     359: astore 5
            //     361: aconst_null
            //     362: astore 6
            //     364: aconst_null
            //     365: astore 7
            //     367: aload_0
            //     368: getfield 68	com/android/server/BackupManagerService$PerformFullBackupTask:mEncryptPassword	Ljava/lang/String;
            //     371: ifnull +150 -> 521
            //     374: aload_0
            //     375: getfield 68	com/android/server/BackupManagerService$PerformFullBackupTask:mEncryptPassword	Ljava/lang/String;
            //     378: invokevirtual 488	java/lang/String:length	()I
            //     381: ifle +140 -> 521
            //     384: iconst_1
            //     385: istore 32
            //     387: aload_0
            //     388: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     391: invokevirtual 492	com/android/server/BackupManagerService:hasBackupPassword	()Z
            //     394: ifeq +133 -> 527
            //     397: aload_0
            //     398: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     401: aload_0
            //     402: getfield 58	com/android/server/BackupManagerService$PerformFullBackupTask:mCurrentPassword	Ljava/lang/String;
            //     405: sipush 10000
            //     408: invokevirtual 496	com/android/server/BackupManagerService:passwordMatchesSaved	(Ljava/lang/String;I)Z
            //     411: ifne +116 -> 527
            //     414: ldc 106
            //     416: ldc_w 498
            //     419: invokestatic 234	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     422: pop
            //     423: aload_0
            //     424: aconst_null
            //     425: invokespecial 223	com/android/server/BackupManagerService$PerformFullBackupTask:tearDown	(Landroid/content/pm/PackageInfo;)V
            //     428: iconst_0
            //     429: ifeq +5 -> 434
            //     432: aconst_null
            //     433: athrow
            //     434: aload_0
            //     435: getfield 44	com/android/server/BackupManagerService$PerformFullBackupTask:mOutputFile	Landroid/os/ParcelFileDescriptor;
            //     438: invokevirtual 166	android/os/ParcelFileDescriptor:close	()V
            //     441: aload_0
            //     442: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     445: getfield 502	com/android/server/BackupManagerService:mCurrentOpLock	Ljava/lang/Object;
            //     448: astore 65
            //     450: aload 65
            //     452: monitorenter
            //     453: aload_0
            //     454: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     457: getfield 506	com/android/server/BackupManagerService:mCurrentOperations	Landroid/util/SparseArray;
            //     460: invokevirtual 511	android/util/SparseArray:clear	()V
            //     463: aload 65
            //     465: monitorexit
            //     466: aload_0
            //     467: getfield 70	com/android/server/BackupManagerService$PerformFullBackupTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     470: astore 67
            //     472: aload 67
            //     474: monitorenter
            //     475: aload_0
            //     476: getfield 70	com/android/server/BackupManagerService$PerformFullBackupTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     479: iconst_1
            //     480: invokevirtual 517	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
            //     483: aload_0
            //     484: getfield 70	com/android/server/BackupManagerService$PerformFullBackupTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     487: invokevirtual 520	java/lang/Object:notifyAll	()V
            //     490: aload 67
            //     492: monitorexit
            //     493: aload_0
            //     494: invokevirtual 523	com/android/server/BackupManagerService$PerformFullBackupTask:sendEndBackup	()V
            //     497: ldc 106
            //     499: ldc_w 525
            //     502: invokestatic 130	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     505: pop
            //     506: aload_0
            //     507: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     510: getfield 529	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     513: astore 23
            //     515: aload 23
            //     517: invokevirtual 534	android/os/PowerManager$WakeLock:release	()V
            //     520: return
            //     521: iconst_0
            //     522: istore 32
            //     524: goto -137 -> 387
            //     527: new 108	java/lang/StringBuilder
            //     530: dup
            //     531: sipush 1024
            //     534: invokespecial 386	java/lang/StringBuilder:<init>	(I)V
            //     537: astore 33
            //     539: aload 33
            //     541: ldc_w 536
            //     544: invokevirtual 115	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     547: pop
            //     548: aload 33
            //     550: iconst_1
            //     551: invokevirtual 296	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     554: pop
            //     555: iconst_1
            //     556: ifeq +160 -> 716
            //     559: ldc_w 538
            //     562: astore 36
            //     564: aload 33
            //     566: aload 36
            //     568: invokevirtual 115	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     571: pop
            //     572: iload 32
            //     574: ifeq +150 -> 724
            //     577: aload_0
            //     578: aload 33
            //     580: aload 5
            //     582: invokespecial 540	com/android/server/BackupManagerService$PerformFullBackupTask:emitAesBackupHeader	(Ljava/lang/StringBuilder;Ljava/io/OutputStream;)Ljava/io/OutputStream;
            //     585: astore 62
            //     587: aload 62
            //     589: astore 47
            //     591: aload 5
            //     593: aload 33
            //     595: invokevirtual 124	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     598: ldc_w 542
            //     601: invokevirtual 545	java/lang/String:getBytes	(Ljava/lang/String;)[B
            //     604: invokevirtual 434	java/io/FileOutputStream:write	([B)V
            //     607: iconst_1
            //     608: ifeq +821 -> 1429
            //     611: new 547	java/util/zip/DeflaterOutputStream
            //     614: dup
            //     615: aload 47
            //     617: new 549	java/util/zip/Deflater
            //     620: dup
            //     621: bipush 9
            //     623: invokespecial 550	java/util/zip/Deflater:<init>	(I)V
            //     626: iconst_1
            //     627: invokespecial 553	java/util/zip/DeflaterOutputStream:<init>	(Ljava/io/OutputStream;Ljava/util/zip/Deflater;Z)V
            //     630: astore 49
            //     632: aload 49
            //     634: astore 6
            //     636: aload_0
            //     637: getfield 50	com/android/server/BackupManagerService$PerformFullBackupTask:mIncludeShared	Z
            //     640: istore 50
            //     642: iload 50
            //     644: ifeq +27 -> 671
            //     647: aload_0
            //     648: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     651: invokestatic 411	com/android/server/BackupManagerService:access$600	(Lcom/android/server/BackupManagerService;)Landroid/content/pm/PackageManager;
            //     654: ldc 146
            //     656: iconst_0
            //     657: invokevirtual 473	android/content/pm/PackageManager:getPackageInfo	(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
            //     660: astore 7
            //     662: aload_1
            //     663: aload 7
            //     665: invokeinterface 476 2 0
            //     670: pop
            //     671: aload_1
            //     672: invokeinterface 462 1 0
            //     677: istore 51
            //     679: iconst_0
            //     680: istore 52
            //     682: iload 52
            //     684: iload 51
            //     686: if_icmpge +287 -> 973
            //     689: aload_1
            //     690: iload 52
            //     692: invokeinterface 466 2 0
            //     697: checkcast 117	android/content/pm/PackageInfo
            //     700: astore 7
            //     702: aload_0
            //     703: aload 7
            //     705: aload 6
            //     707: invokespecial 555	com/android/server/BackupManagerService$PerformFullBackupTask:backupOnePackage	(Landroid/content/pm/PackageInfo;Ljava/io/OutputStream;)V
            //     710: iinc 52 1
            //     713: goto -31 -> 682
            //     716: ldc_w 557
            //     719: astore 36
            //     721: goto -157 -> 564
            //     724: aload 33
            //     726: ldc_w 559
            //     729: invokevirtual 115	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     732: pop
            //     733: aload 5
            //     735: astore 47
            //     737: goto -146 -> 591
            //     740: astore 38
            //     742: ldc 106
            //     744: ldc_w 561
            //     747: aload 38
            //     749: invokestatic 229	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     752: pop
            //     753: aload_0
            //     754: aconst_null
            //     755: invokespecial 223	com/android/server/BackupManagerService$PerformFullBackupTask:tearDown	(Landroid/content/pm/PackageInfo;)V
            //     758: iconst_0
            //     759: ifeq +5 -> 764
            //     762: aconst_null
            //     763: athrow
            //     764: aload_0
            //     765: getfield 44	com/android/server/BackupManagerService$PerformFullBackupTask:mOutputFile	Landroid/os/ParcelFileDescriptor;
            //     768: invokevirtual 166	android/os/ParcelFileDescriptor:close	()V
            //     771: aload_0
            //     772: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     775: getfield 502	com/android/server/BackupManagerService:mCurrentOpLock	Ljava/lang/Object;
            //     778: astore 41
            //     780: aload 41
            //     782: monitorenter
            //     783: aload_0
            //     784: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     787: getfield 506	com/android/server/BackupManagerService:mCurrentOperations	Landroid/util/SparseArray;
            //     790: invokevirtual 511	android/util/SparseArray:clear	()V
            //     793: aload 41
            //     795: monitorexit
            //     796: aload_0
            //     797: getfield 70	com/android/server/BackupManagerService$PerformFullBackupTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     800: astore 43
            //     802: aload 43
            //     804: monitorenter
            //     805: aload_0
            //     806: getfield 70	com/android/server/BackupManagerService$PerformFullBackupTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     809: iconst_1
            //     810: invokevirtual 517	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
            //     813: aload_0
            //     814: getfield 70	com/android/server/BackupManagerService$PerformFullBackupTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     817: invokevirtual 520	java/lang/Object:notifyAll	()V
            //     820: aload 43
            //     822: monitorexit
            //     823: aload_0
            //     824: invokevirtual 523	com/android/server/BackupManagerService$PerformFullBackupTask:sendEndBackup	()V
            //     827: ldc 106
            //     829: ldc_w 525
            //     832: invokestatic 130	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     835: pop
            //     836: aload_0
            //     837: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     840: getfield 529	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     843: astore 23
            //     845: goto -330 -> 515
            //     848: astore 59
            //     850: ldc 106
            //     852: ldc_w 563
            //     855: invokestatic 216	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     858: pop
            //     859: goto -188 -> 671
            //     862: astore 24
            //     864: ldc 106
            //     866: ldc_w 565
            //     869: invokestatic 216	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     872: pop
            //     873: aload_0
            //     874: aload 7
            //     876: invokespecial 223	com/android/server/BackupManagerService$PerformFullBackupTask:tearDown	(Landroid/content/pm/PackageInfo;)V
            //     879: aload 6
            //     881: ifnull +8 -> 889
            //     884: aload 6
            //     886: invokevirtual 566	java/io/OutputStream:close	()V
            //     889: aload_0
            //     890: getfield 44	com/android/server/BackupManagerService$PerformFullBackupTask:mOutputFile	Landroid/os/ParcelFileDescriptor;
            //     893: invokevirtual 166	android/os/ParcelFileDescriptor:close	()V
            //     896: aload_0
            //     897: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     900: getfield 502	com/android/server/BackupManagerService:mCurrentOpLock	Ljava/lang/Object;
            //     903: astore 27
            //     905: aload 27
            //     907: monitorenter
            //     908: aload_0
            //     909: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     912: getfield 506	com/android/server/BackupManagerService:mCurrentOperations	Landroid/util/SparseArray;
            //     915: invokevirtual 511	android/util/SparseArray:clear	()V
            //     918: aload 27
            //     920: monitorexit
            //     921: aload_0
            //     922: getfield 70	com/android/server/BackupManagerService$PerformFullBackupTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     925: astore 29
            //     927: aload 29
            //     929: monitorenter
            //     930: aload_0
            //     931: getfield 70	com/android/server/BackupManagerService$PerformFullBackupTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     934: iconst_1
            //     935: invokevirtual 517	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
            //     938: aload_0
            //     939: getfield 70	com/android/server/BackupManagerService$PerformFullBackupTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     942: invokevirtual 520	java/lang/Object:notifyAll	()V
            //     945: aload 29
            //     947: monitorexit
            //     948: aload_0
            //     949: invokevirtual 523	com/android/server/BackupManagerService$PerformFullBackupTask:sendEndBackup	()V
            //     952: ldc 106
            //     954: ldc_w 525
            //     957: invokestatic 130	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     960: pop
            //     961: aload_0
            //     962: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     965: getfield 529	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     968: astore 23
            //     970: goto -455 -> 515
            //     973: aload_0
            //     974: aload 6
            //     976: invokespecial 568	com/android/server/BackupManagerService$PerformFullBackupTask:finalizeBackup	(Ljava/io/OutputStream;)V
            //     979: aload_0
            //     980: aload 7
            //     982: invokespecial 223	com/android/server/BackupManagerService$PerformFullBackupTask:tearDown	(Landroid/content/pm/PackageInfo;)V
            //     985: aload 6
            //     987: ifnull +8 -> 995
            //     990: aload 6
            //     992: invokevirtual 566	java/io/OutputStream:close	()V
            //     995: aload_0
            //     996: getfield 44	com/android/server/BackupManagerService$PerformFullBackupTask:mOutputFile	Landroid/os/ParcelFileDescriptor;
            //     999: invokevirtual 166	android/os/ParcelFileDescriptor:close	()V
            //     1002: aload_0
            //     1003: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     1006: getfield 502	com/android/server/BackupManagerService:mCurrentOpLock	Ljava/lang/Object;
            //     1009: astore 54
            //     1011: aload 54
            //     1013: monitorenter
            //     1014: aload_0
            //     1015: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     1018: getfield 506	com/android/server/BackupManagerService:mCurrentOperations	Landroid/util/SparseArray;
            //     1021: invokevirtual 511	android/util/SparseArray:clear	()V
            //     1024: aload 54
            //     1026: monitorexit
            //     1027: aload_0
            //     1028: getfield 70	com/android/server/BackupManagerService$PerformFullBackupTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     1031: astore 56
            //     1033: aload 56
            //     1035: monitorenter
            //     1036: aload_0
            //     1037: getfield 70	com/android/server/BackupManagerService$PerformFullBackupTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     1040: iconst_1
            //     1041: invokevirtual 517	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
            //     1044: aload_0
            //     1045: getfield 70	com/android/server/BackupManagerService$PerformFullBackupTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     1048: invokevirtual 520	java/lang/Object:notifyAll	()V
            //     1051: aload 56
            //     1053: monitorexit
            //     1054: aload_0
            //     1055: invokevirtual 523	com/android/server/BackupManagerService$PerformFullBackupTask:sendEndBackup	()V
            //     1058: ldc 106
            //     1060: ldc_w 525
            //     1063: invokestatic 130	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     1066: pop
            //     1067: aload_0
            //     1068: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     1071: getfield 529	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     1074: astore 23
            //     1076: goto -561 -> 515
            //     1079: astore 15
            //     1081: ldc 106
            //     1083: ldc_w 570
            //     1086: aload 15
            //     1088: invokestatic 229	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     1091: pop
            //     1092: aload_0
            //     1093: aload 7
            //     1095: invokespecial 223	com/android/server/BackupManagerService$PerformFullBackupTask:tearDown	(Landroid/content/pm/PackageInfo;)V
            //     1098: aload 6
            //     1100: ifnull +8 -> 1108
            //     1103: aload 6
            //     1105: invokevirtual 566	java/io/OutputStream:close	()V
            //     1108: aload_0
            //     1109: getfield 44	com/android/server/BackupManagerService$PerformFullBackupTask:mOutputFile	Landroid/os/ParcelFileDescriptor;
            //     1112: invokevirtual 166	android/os/ParcelFileDescriptor:close	()V
            //     1115: aload_0
            //     1116: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     1119: getfield 502	com/android/server/BackupManagerService:mCurrentOpLock	Ljava/lang/Object;
            //     1122: astore 18
            //     1124: aload 18
            //     1126: monitorenter
            //     1127: aload_0
            //     1128: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     1131: getfield 506	com/android/server/BackupManagerService:mCurrentOperations	Landroid/util/SparseArray;
            //     1134: invokevirtual 511	android/util/SparseArray:clear	()V
            //     1137: aload 18
            //     1139: monitorexit
            //     1140: aload_0
            //     1141: getfield 70	com/android/server/BackupManagerService$PerformFullBackupTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     1144: astore 20
            //     1146: aload 20
            //     1148: monitorenter
            //     1149: aload_0
            //     1150: getfield 70	com/android/server/BackupManagerService$PerformFullBackupTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     1153: iconst_1
            //     1154: invokevirtual 517	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
            //     1157: aload_0
            //     1158: getfield 70	com/android/server/BackupManagerService$PerformFullBackupTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     1161: invokevirtual 520	java/lang/Object:notifyAll	()V
            //     1164: aload 20
            //     1166: monitorexit
            //     1167: aload_0
            //     1168: invokevirtual 523	com/android/server/BackupManagerService$PerformFullBackupTask:sendEndBackup	()V
            //     1171: ldc 106
            //     1173: ldc_w 525
            //     1176: invokestatic 130	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     1179: pop
            //     1180: aload_0
            //     1181: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     1184: getfield 529	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     1187: astore 23
            //     1189: goto -674 -> 515
            //     1192: astore 8
            //     1194: aload_0
            //     1195: aload 7
            //     1197: invokespecial 223	com/android/server/BackupManagerService$PerformFullBackupTask:tearDown	(Landroid/content/pm/PackageInfo;)V
            //     1200: aload 6
            //     1202: ifnull +8 -> 1210
            //     1205: aload 6
            //     1207: invokevirtual 566	java/io/OutputStream:close	()V
            //     1210: aload_0
            //     1211: getfield 44	com/android/server/BackupManagerService$PerformFullBackupTask:mOutputFile	Landroid/os/ParcelFileDescriptor;
            //     1214: invokevirtual 166	android/os/ParcelFileDescriptor:close	()V
            //     1217: aload_0
            //     1218: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     1221: getfield 502	com/android/server/BackupManagerService:mCurrentOpLock	Ljava/lang/Object;
            //     1224: astore 10
            //     1226: aload 10
            //     1228: monitorenter
            //     1229: aload_0
            //     1230: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     1233: getfield 506	com/android/server/BackupManagerService:mCurrentOperations	Landroid/util/SparseArray;
            //     1236: invokevirtual 511	android/util/SparseArray:clear	()V
            //     1239: aload 10
            //     1241: monitorexit
            //     1242: aload_0
            //     1243: getfield 70	com/android/server/BackupManagerService$PerformFullBackupTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     1246: astore 12
            //     1248: aload 12
            //     1250: monitorenter
            //     1251: aload_0
            //     1252: getfield 70	com/android/server/BackupManagerService$PerformFullBackupTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     1255: iconst_1
            //     1256: invokevirtual 517	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
            //     1259: aload_0
            //     1260: getfield 70	com/android/server/BackupManagerService$PerformFullBackupTask:mLatchObject	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     1263: invokevirtual 520	java/lang/Object:notifyAll	()V
            //     1266: aload 12
            //     1268: monitorexit
            //     1269: aload_0
            //     1270: invokevirtual 523	com/android/server/BackupManagerService$PerformFullBackupTask:sendEndBackup	()V
            //     1273: ldc 106
            //     1275: ldc_w 525
            //     1278: invokestatic 130	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     1281: pop
            //     1282: aload_0
            //     1283: getfield 39	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
            //     1286: getfield 529	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     1289: invokevirtual 534	android/os/PowerManager$WakeLock:release	()V
            //     1292: aload 8
            //     1294: athrow
            //     1295: astore 11
            //     1297: aload 10
            //     1299: monitorexit
            //     1300: aload 11
            //     1302: athrow
            //     1303: astore 13
            //     1305: aload 12
            //     1307: monitorexit
            //     1308: aload 13
            //     1310: athrow
            //     1311: astore 28
            //     1313: aload 27
            //     1315: monitorexit
            //     1316: aload 28
            //     1318: athrow
            //     1319: astore 30
            //     1321: aload 29
            //     1323: monitorexit
            //     1324: aload 30
            //     1326: athrow
            //     1327: astore 19
            //     1329: aload 18
            //     1331: monitorexit
            //     1332: aload 19
            //     1334: athrow
            //     1335: astore 21
            //     1337: aload 20
            //     1339: monitorexit
            //     1340: aload 21
            //     1342: athrow
            //     1343: astore 66
            //     1345: aload 65
            //     1347: monitorexit
            //     1348: aload 66
            //     1350: athrow
            //     1351: astore 68
            //     1353: aload 67
            //     1355: monitorexit
            //     1356: aload 68
            //     1358: athrow
            //     1359: astore 42
            //     1361: aload 41
            //     1363: monitorexit
            //     1364: aload 42
            //     1366: athrow
            //     1367: astore 44
            //     1369: aload 43
            //     1371: monitorexit
            //     1372: aload 44
            //     1374: athrow
            //     1375: astore 55
            //     1377: aload 54
            //     1379: monitorexit
            //     1380: aload 55
            //     1382: athrow
            //     1383: astore 57
            //     1385: aload 56
            //     1387: monitorexit
            //     1388: aload 57
            //     1390: athrow
            //     1391: astore 53
            //     1393: goto -391 -> 1002
            //     1396: astore 40
            //     1398: goto -627 -> 771
            //     1401: astore 64
            //     1403: goto -962 -> 441
            //     1406: astore 17
            //     1408: goto -293 -> 1115
            //     1411: astore 26
            //     1413: goto -517 -> 896
            //     1416: astore 9
            //     1418: goto -201 -> 1217
            //     1421: astore 38
            //     1423: aload 47
            //     1425: pop
            //     1426: goto -684 -> 742
            //     1429: aload 47
            //     1431: astore 49
            //     1433: goto -801 -> 632
            //
            // Exception table:
            //     from	to	target	type
            //     137	162	168	android/content/pm/PackageManager$NameNotFoundException
            //     577	587	740	java/lang/Exception
            //     724	733	740	java/lang/Exception
            //     647	671	848	android/content/pm/PackageManager$NameNotFoundException
            //     367	423	862	android/os/RemoteException
            //     527	572	862	android/os/RemoteException
            //     577	587	862	android/os/RemoteException
            //     591	632	862	android/os/RemoteException
            //     636	642	862	android/os/RemoteException
            //     647	671	862	android/os/RemoteException
            //     671	721	862	android/os/RemoteException
            //     724	733	862	android/os/RemoteException
            //     742	753	862	android/os/RemoteException
            //     850	859	862	android/os/RemoteException
            //     973	979	862	android/os/RemoteException
            //     367	423	1079	java/lang/Exception
            //     527	572	1079	java/lang/Exception
            //     636	642	1079	java/lang/Exception
            //     647	671	1079	java/lang/Exception
            //     671	721	1079	java/lang/Exception
            //     742	753	1079	java/lang/Exception
            //     850	859	1079	java/lang/Exception
            //     973	979	1079	java/lang/Exception
            //     367	423	1192	finally
            //     527	572	1192	finally
            //     577	587	1192	finally
            //     591	632	1192	finally
            //     636	642	1192	finally
            //     647	671	1192	finally
            //     671	721	1192	finally
            //     724	733	1192	finally
            //     742	753	1192	finally
            //     850	859	1192	finally
            //     864	873	1192	finally
            //     973	979	1192	finally
            //     1081	1092	1192	finally
            //     1229	1242	1295	finally
            //     1297	1300	1295	finally
            //     1251	1269	1303	finally
            //     1305	1308	1303	finally
            //     908	921	1311	finally
            //     1313	1316	1311	finally
            //     930	948	1319	finally
            //     1321	1324	1319	finally
            //     1127	1140	1327	finally
            //     1329	1332	1327	finally
            //     1149	1167	1335	finally
            //     1337	1340	1335	finally
            //     453	466	1343	finally
            //     1345	1348	1343	finally
            //     475	493	1351	finally
            //     1353	1356	1351	finally
            //     783	796	1359	finally
            //     1361	1364	1359	finally
            //     805	823	1367	finally
            //     1369	1372	1367	finally
            //     1014	1027	1375	finally
            //     1377	1380	1375	finally
            //     1036	1054	1383	finally
            //     1385	1388	1383	finally
            //     990	1002	1391	java/io/IOException
            //     762	771	1396	java/io/IOException
            //     432	441	1401	java/io/IOException
            //     1103	1115	1406	java/io/IOException
            //     884	896	1411	java/io/IOException
            //     1205	1217	1416	java/io/IOException
            //     591	632	1421	java/lang/Exception
        }

        void sendEndBackup()
        {
            if (this.mObserver != null);
            try
            {
                this.mObserver.onEndBackup();
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                {
                    Slog.w("BackupManagerService", "full backup observer went away: endBackup");
                    this.mObserver = null;
                }
            }
        }

        void sendOnBackupPackage(String paramString)
        {
            if (this.mObserver != null);
            try
            {
                this.mObserver.onBackupPackage(paramString);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                {
                    Slog.w("BackupManagerService", "full backup observer went away: backupPackage");
                    this.mObserver = null;
                }
            }
        }

        void sendStartBackup()
        {
            if (this.mObserver != null);
            try
            {
                this.mObserver.onStartBackup();
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                {
                    Slog.w("BackupManagerService", "full backup observer went away: startBackup");
                    this.mObserver = null;
                }
            }
        }

        class FullBackupRunner
            implements Runnable
        {
            IBackupAgent mAgent;
            PackageInfo mPackage;
            ParcelFileDescriptor mPipe;
            boolean mSendApk;
            int mToken;
            boolean mWriteManifest;

            FullBackupRunner(PackageInfo paramIBackupAgent, IBackupAgent paramParcelFileDescriptor, ParcelFileDescriptor paramInt, int paramBoolean1, boolean paramBoolean2, boolean arg7)
                throws IOException
            {
                this.mPackage = paramIBackupAgent;
                this.mAgent = paramParcelFileDescriptor;
                this.mPipe = ParcelFileDescriptor.dup(paramInt.getFileDescriptor());
                this.mToken = paramBoolean1;
                this.mSendApk = paramBoolean2;
                boolean bool;
                this.mWriteManifest = bool;
            }

            // ERROR //
            public void run()
            {
                // Byte code:
                //     0: new 58	android/app/backup/BackupDataOutput
                //     3: dup
                //     4: aload_0
                //     5: getfield 47	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:mPipe	Landroid/os/ParcelFileDescriptor;
                //     8: invokevirtual 41	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
                //     11: invokespecial 61	android/app/backup/BackupDataOutput:<init>	(Ljava/io/FileDescriptor;)V
                //     14: astore_1
                //     15: aload_0
                //     16: getfield 53	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:mWriteManifest	Z
                //     19: ifeq +59 -> 78
                //     22: aload_0
                //     23: getfield 28	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:this$1	Lcom/android/server/BackupManagerService$PerformFullBackupTask;
                //     26: aload_0
                //     27: getfield 33	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:mPackage	Landroid/content/pm/PackageInfo;
                //     30: aload_0
                //     31: getfield 28	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:this$1	Lcom/android/server/BackupManagerService$PerformFullBackupTask;
                //     34: getfield 65	com/android/server/BackupManagerService$PerformFullBackupTask:mManifestFile	Ljava/io/File;
                //     37: aload_0
                //     38: getfield 51	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:mSendApk	Z
                //     41: invokestatic 69	com/android/server/BackupManagerService$PerformFullBackupTask:access$900	(Lcom/android/server/BackupManagerService$PerformFullBackupTask;Landroid/content/pm/PackageInfo;Ljava/io/File;Z)V
                //     44: aload_0
                //     45: getfield 33	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:mPackage	Landroid/content/pm/PackageInfo;
                //     48: getfield 75	android/content/pm/PackageInfo:packageName	Ljava/lang/String;
                //     51: aconst_null
                //     52: aconst_null
                //     53: aload_0
                //     54: getfield 28	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:this$1	Lcom/android/server/BackupManagerService$PerformFullBackupTask;
                //     57: getfield 78	com/android/server/BackupManagerService$PerformFullBackupTask:mFilesDir	Ljava/io/File;
                //     60: invokevirtual 84	java/io/File:getAbsolutePath	()Ljava/lang/String;
                //     63: aload_0
                //     64: getfield 28	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:this$1	Lcom/android/server/BackupManagerService$PerformFullBackupTask;
                //     67: getfield 65	com/android/server/BackupManagerService$PerformFullBackupTask:mManifestFile	Ljava/io/File;
                //     70: invokevirtual 84	java/io/File:getAbsolutePath	()Ljava/lang/String;
                //     73: aload_1
                //     74: invokestatic 90	android/app/backup/FullBackup:backupToTar	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/backup/BackupDataOutput;)I
                //     77: pop
                //     78: aload_0
                //     79: getfield 51	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:mSendApk	Z
                //     82: ifeq +15 -> 97
                //     85: aload_0
                //     86: getfield 28	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:this$1	Lcom/android/server/BackupManagerService$PerformFullBackupTask;
                //     89: aload_0
                //     90: getfield 33	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:mPackage	Landroid/content/pm/PackageInfo;
                //     93: aload_1
                //     94: invokestatic 94	com/android/server/BackupManagerService$PerformFullBackupTask:access$1000	(Lcom/android/server/BackupManagerService$PerformFullBackupTask;Landroid/content/pm/PackageInfo;Landroid/app/backup/BackupDataOutput;)V
                //     97: ldc 96
                //     99: new 98	java/lang/StringBuilder
                //     102: dup
                //     103: invokespecial 99	java/lang/StringBuilder:<init>	()V
                //     106: ldc 101
                //     108: invokevirtual 105	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //     111: aload_0
                //     112: getfield 33	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:mPackage	Landroid/content/pm/PackageInfo;
                //     115: getfield 75	android/content/pm/PackageInfo:packageName	Ljava/lang/String;
                //     118: invokevirtual 105	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //     121: invokevirtual 108	java/lang/StringBuilder:toString	()Ljava/lang/String;
                //     124: invokestatic 114	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
                //     127: pop
                //     128: aload_0
                //     129: getfield 28	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:this$1	Lcom/android/server/BackupManagerService$PerformFullBackupTask;
                //     132: getfield 118	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
                //     135: aload_0
                //     136: getfield 49	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:mToken	I
                //     139: ldc2_w 119
                //     142: aconst_null
                //     143: invokevirtual 126	com/android/server/BackupManagerService:prepareOperationTimeout	(IJLcom/android/server/BackupManagerService$BackupRestoreTask;)V
                //     146: aload_0
                //     147: getfield 35	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:mAgent	Landroid/app/IBackupAgent;
                //     150: aload_0
                //     151: getfield 47	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:mPipe	Landroid/os/ParcelFileDescriptor;
                //     154: aload_0
                //     155: getfield 49	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:mToken	I
                //     158: aload_0
                //     159: getfield 28	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:this$1	Lcom/android/server/BackupManagerService$PerformFullBackupTask;
                //     162: getfield 118	com/android/server/BackupManagerService$PerformFullBackupTask:this$0	Lcom/android/server/BackupManagerService;
                //     165: getfield 130	com/android/server/BackupManagerService:mBackupManagerBinder	Landroid/app/backup/IBackupManager;
                //     168: invokeinterface 136 4 0
                //     173: aload_0
                //     174: getfield 47	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:mPipe	Landroid/os/ParcelFileDescriptor;
                //     177: invokevirtual 139	android/os/ParcelFileDescriptor:close	()V
                //     180: return
                //     181: astore 7
                //     183: ldc 96
                //     185: new 98	java/lang/StringBuilder
                //     188: dup
                //     189: invokespecial 99	java/lang/StringBuilder:<init>	()V
                //     192: ldc 141
                //     194: invokevirtual 105	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //     197: aload_0
                //     198: getfield 33	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:mPackage	Landroid/content/pm/PackageInfo;
                //     201: getfield 75	android/content/pm/PackageInfo:packageName	Ljava/lang/String;
                //     204: invokevirtual 105	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //     207: invokevirtual 108	java/lang/StringBuilder:toString	()Ljava/lang/String;
                //     210: invokestatic 144	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
                //     213: pop
                //     214: aload_0
                //     215: getfield 47	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:mPipe	Landroid/os/ParcelFileDescriptor;
                //     218: invokevirtual 139	android/os/ParcelFileDescriptor:close	()V
                //     221: goto -41 -> 180
                //     224: astore 6
                //     226: goto -46 -> 180
                //     229: astore 4
                //     231: ldc 96
                //     233: new 98	java/lang/StringBuilder
                //     236: dup
                //     237: invokespecial 99	java/lang/StringBuilder:<init>	()V
                //     240: ldc 146
                //     242: invokevirtual 105	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //     245: aload_0
                //     246: getfield 33	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:mPackage	Landroid/content/pm/PackageInfo;
                //     249: getfield 75	android/content/pm/PackageInfo:packageName	Ljava/lang/String;
                //     252: invokevirtual 105	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //     255: invokevirtual 108	java/lang/StringBuilder:toString	()Ljava/lang/String;
                //     258: invokestatic 144	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
                //     261: pop
                //     262: aload_0
                //     263: getfield 47	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:mPipe	Landroid/os/ParcelFileDescriptor;
                //     266: invokevirtual 139	android/os/ParcelFileDescriptor:close	()V
                //     269: goto -89 -> 180
                //     272: astore_2
                //     273: aload_0
                //     274: getfield 47	com/android/server/BackupManagerService$PerformFullBackupTask$FullBackupRunner:mPipe	Landroid/os/ParcelFileDescriptor;
                //     277: invokevirtual 139	android/os/ParcelFileDescriptor:close	()V
                //     280: aload_2
                //     281: athrow
                //     282: astore_3
                //     283: goto -3 -> 280
                //
                // Exception table:
                //     from	to	target	type
                //     0	173	181	java/io/IOException
                //     173	180	224	java/io/IOException
                //     214	221	224	java/io/IOException
                //     262	269	224	java/io/IOException
                //     0	173	229	android/os/RemoteException
                //     0	173	272	finally
                //     183	214	272	finally
                //     231	262	272	finally
                //     273	280	282	java/io/IOException
            }
        }
    }

    class PerformBackupTask
        implements BackupManagerService.BackupRestoreTask
    {
        private static final String TAG = "PerformBackupTask";
        ParcelFileDescriptor mBackupData;
        File mBackupDataName;
        PackageInfo mCurrentPackage;
        BackupManagerService.BackupState mCurrentState;
        boolean mFinished;
        File mJournal;
        ParcelFileDescriptor mNewState;
        File mNewStateName;
        ArrayList<BackupManagerService.BackupRequest> mOriginalQueue;
        ArrayList<BackupManagerService.BackupRequest> mQueue;
        ParcelFileDescriptor mSavedState;
        File mSavedStateName;
        File mStateDir;
        int mStatus;
        IBackupTransport mTransport;

        public PerformBackupTask(ArrayList<BackupManagerService.BackupRequest> paramFile, File arg3)
        {
            this.mTransport = paramFile;
            Object localObject1;
            this.mOriginalQueue = localObject1;
            Object localObject2;
            this.mJournal = localObject2;
            try
            {
                this.mStateDir = new File(BackupManagerService.this.mBaseStateDir, paramFile.transportDirName());
                label46: this.mCurrentState = BackupManagerService.BackupState.INITIAL;
                this.mFinished = false;
                BackupManagerService.this.addBackupTrace("STATE => INITIAL");
                return;
            }
            catch (RemoteException localRemoteException)
            {
                break label46;
            }
        }

        void agentErrorCleanup()
        {
            this.mBackupDataName.delete();
            this.mNewStateName.delete();
            clearAgentState();
            if (this.mQueue.isEmpty());
            for (BackupManagerService.BackupState localBackupState = BackupManagerService.BackupState.FINAL; ; localBackupState = BackupManagerService.BackupState.RUNNING_QUEUE)
            {
                executeNextState(localBackupState);
                return;
            }
        }

        void beginBackup()
        {
            BackupManagerService.this.clearBackupTrace();
            StringBuilder localStringBuilder = new StringBuilder(256);
            localStringBuilder.append("beginBackup: [");
            Iterator localIterator = this.mOriginalQueue.iterator();
            while (localIterator.hasNext())
            {
                BackupManagerService.BackupRequest localBackupRequest = (BackupManagerService.BackupRequest)localIterator.next();
                localStringBuilder.append(' ');
                localStringBuilder.append(localBackupRequest.packageName);
            }
            localStringBuilder.append(" ]");
            BackupManagerService.this.addBackupTrace(localStringBuilder.toString());
            this.mStatus = 0;
            if (this.mOriginalQueue.isEmpty())
            {
                Slog.w("PerformBackupTask", "Backup begun with an empty queue - nothing to do.");
                BackupManagerService.this.addBackupTrace("queue empty at begin");
                executeNextState(BackupManagerService.BackupState.FINAL);
            }
            while (true)
            {
                return;
                this.mQueue = ((ArrayList)this.mOriginalQueue.clone());
                Slog.v("PerformBackupTask", "Beginning backup of " + this.mQueue.size() + " targets");
                File localFile = new File(this.mStateDir, "@pm@");
                try
                {
                    String str = this.mTransport.transportDirName();
                    EventLog.writeEvent(2821, str);
                    if ((this.mStatus == 0) && (localFile.length() <= 0L))
                    {
                        Slog.i("PerformBackupTask", "Initializing (wiping) backup state and transport storage");
                        BackupManagerService.this.addBackupTrace("initializing transport " + str);
                        BackupManagerService.this.resetBackupState(this.mStateDir);
                        this.mStatus = this.mTransport.initializeDevice();
                        BackupManagerService.this.addBackupTrace("transport.initializeDevice() == " + this.mStatus);
                        if (this.mStatus != 0)
                            break label497;
                        EventLog.writeEvent(2827, new Object[0]);
                    }
                    while (true)
                    {
                        if (this.mStatus == 0)
                        {
                            this.mStatus = invokeAgentForBackup("@pm@", IBackupAgent.Stub.asInterface(new PackageManagerBackupAgent(BackupManagerService.this.mPackageManager, BackupManagerService.this.allAgentPackages()).onBind()), this.mTransport);
                            BackupManagerService.this.addBackupTrace("PMBA invoke: " + this.mStatus);
                        }
                        if (this.mStatus == 2)
                            EventLog.writeEvent(2826, this.mTransport.transportDirName());
                        BackupManagerService.this.addBackupTrace("exiting prelim: " + this.mStatus);
                        if (this.mStatus == 0)
                            break;
                        BackupManagerService.this.resetBackupState(this.mStateDir);
                        executeNextState(BackupManagerService.BackupState.FINAL);
                        break;
                        label497: EventLog.writeEvent(2822, "(initialize)");
                        Slog.e("PerformBackupTask", "Transport error in initializeDevice()");
                    }
                }
                catch (Exception localException)
                {
                    Slog.e("PerformBackupTask", "Error in backup thread", localException);
                    BackupManagerService.this.addBackupTrace("Exception in backup thread: " + localException);
                    this.mStatus = 1;
                    BackupManagerService.this.addBackupTrace("exiting prelim: " + this.mStatus);
                    if (this.mStatus == 0)
                        continue;
                    BackupManagerService.this.resetBackupState(this.mStateDir);
                    executeNextState(BackupManagerService.BackupState.FINAL);
                }
                finally
                {
                    BackupManagerService.this.addBackupTrace("exiting prelim: " + this.mStatus);
                    if (this.mStatus != 0)
                    {
                        BackupManagerService.this.resetBackupState(this.mStateDir);
                        executeNextState(BackupManagerService.BackupState.FINAL);
                    }
                }
            }
        }

        void clearAgentState()
        {
            try
            {
                if (this.mSavedState != null)
                    this.mSavedState.close();
                try
                {
                    label14: if (this.mBackupData != null)
                        this.mBackupData.close();
                    try
                    {
                        label28: if (this.mNewState != null)
                            this.mNewState.close();
                        label42: this.mNewState = null;
                        this.mBackupData = null;
                        this.mSavedState = null;
                        synchronized (BackupManagerService.this.mCurrentOpLock)
                        {
                            BackupManagerService.this.mCurrentOperations.clear();
                            if (this.mCurrentPackage.applicationInfo != null)
                                BackupManagerService.this.addBackupTrace("unbinding " + this.mCurrentPackage.packageName);
                        }
                        try
                        {
                            BackupManagerService.this.mActivityManager.unbindBackupAgent(this.mCurrentPackage.applicationInfo);
                            label144: return;
                            localObject2 = finally;
                            throw localObject2;
                        }
                        catch (RemoteException localRemoteException)
                        {
                            break label144;
                        }
                    }
                    catch (IOException localIOException3)
                    {
                        break label42;
                    }
                }
                catch (IOException localIOException2)
                {
                    break label28;
                }
            }
            catch (IOException localIOException1)
            {
                break label14;
            }
        }

        void clearMetadata()
        {
            File localFile = new File(this.mStateDir, "@pm@");
            if (localFile.exists())
                localFile.delete();
        }

        public void execute()
        {
            switch (BackupManagerService.4.$SwitchMap$com$android$server$BackupManagerService$BackupState[this.mCurrentState.ordinal()])
            {
            default:
            case 1:
            case 2:
                while (true)
                {
                    return;
                    beginBackup();
                    continue;
                    invokeNextAgent();
                }
            case 3:
            }
            if (!this.mFinished)
                finalizeBackup();
            while (true)
            {
                this.mFinished = true;
                break;
                Slog.e("PerformBackupTask", "Duplicate finish");
            }
        }

        void executeNextState(BackupManagerService.BackupState paramBackupState)
        {
            BackupManagerService.this.addBackupTrace("executeNextState => " + paramBackupState);
            this.mCurrentState = paramBackupState;
            Message localMessage = BackupManagerService.this.mBackupHandler.obtainMessage(20, this);
            BackupManagerService.this.mBackupHandler.sendMessage(localMessage);
        }

        void finalizeBackup()
        {
            BackupManagerService.this.addBackupTrace("finishing");
            if ((this.mJournal != null) && (!this.mJournal.delete()))
                Slog.e("PerformBackupTask", "Unable to remove backup journal file " + this.mJournal);
            if ((BackupManagerService.this.mCurrentToken == 0L) && (this.mStatus == 0))
                BackupManagerService.this.addBackupTrace("success; recording token");
            try
            {
                BackupManagerService.this.mCurrentToken = this.mTransport.getCurrentRestoreSet();
                label101: BackupManagerService.this.writeRestoreTokens();
                synchronized (BackupManagerService.this.mQueueLock)
                {
                    BackupManagerService.this.mBackupRunning = false;
                    if (this.mStatus == 2)
                    {
                        clearMetadata();
                        Slog.d("PerformBackupTask", "Server requires init; rerunning");
                        BackupManagerService.this.addBackupTrace("init required; rerunning");
                        BackupManagerService.this.backupNow();
                    }
                    BackupManagerService.this.clearBackupTrace();
                    Slog.i("PerformBackupTask", "Backup pass finished.");
                    BackupManagerService.this.mWakelock.release();
                    return;
                }
            }
            catch (RemoteException localRemoteException)
            {
                break label101;
            }
        }

        public void handleTimeout()
        {
            Slog.e("PerformBackupTask", "Timeout backing up " + this.mCurrentPackage.packageName);
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = this.mCurrentPackage.packageName;
            arrayOfObject[1] = "timeout";
            EventLog.writeEvent(2823, arrayOfObject);
            BackupManagerService.this.addBackupTrace("timeout of " + this.mCurrentPackage.packageName);
            agentErrorCleanup();
            BackupManagerService.this.dataChangedImpl(this.mCurrentPackage.packageName);
        }

        int invokeAgentForBackup(String paramString, IBackupAgent paramIBackupAgent, IBackupTransport paramIBackupTransport)
        {
            Slog.d("PerformBackupTask", "invokeAgentForBackup on " + paramString);
            BackupManagerService.this.addBackupTrace("invoking " + paramString);
            this.mSavedStateName = new File(this.mStateDir, paramString);
            this.mBackupDataName = new File(BackupManagerService.this.mDataDir, paramString + ".data");
            this.mNewStateName = new File(this.mStateDir, paramString + ".new");
            this.mSavedState = null;
            this.mBackupData = null;
            this.mNewState = null;
            int i = BackupManagerService.this.generateToken();
            try
            {
                if (paramString.equals("@pm@"))
                {
                    this.mCurrentPackage = new PackageInfo();
                    this.mCurrentPackage.packageName = paramString;
                }
                this.mSavedState = ParcelFileDescriptor.open(this.mSavedStateName, 402653184);
                this.mBackupData = ParcelFileDescriptor.open(this.mBackupDataName, 1006632960);
                this.mNewState = ParcelFileDescriptor.open(this.mNewStateName, 1006632960);
                BackupManagerService.this.addBackupTrace("setting timeout");
                BackupManagerService.this.prepareOperationTimeout(i, 30000L, this);
                BackupManagerService.this.addBackupTrace("calling agent doBackup()");
                paramIBackupAgent.doBackup(this.mSavedState, this.mBackupData, this.mNewState, i, BackupManagerService.this.mBackupManagerBinder);
                BackupManagerService.this.addBackupTrace("invoke success");
                j = 0;
                return j;
            }
            catch (Exception localException)
            {
                while (true)
                {
                    Slog.e("PerformBackupTask", "Error invoking for backup on " + paramString);
                    BackupManagerService.this.addBackupTrace("exception: " + localException);
                    Object[] arrayOfObject = new Object[2];
                    arrayOfObject[0] = paramString;
                    arrayOfObject[1] = localException.toString();
                    EventLog.writeEvent(2823, arrayOfObject);
                    agentErrorCleanup();
                    int j = 3;
                }
            }
        }

        void invokeNextAgent()
        {
            this.mStatus = 0;
            BackupManagerService.this.addBackupTrace("invoke q=" + this.mQueue.size());
            if (this.mQueue.isEmpty())
            {
                Slog.i("PerformBackupTask", "queue now empty");
                executeNextState(BackupManagerService.BackupState.FINAL);
                return;
            }
            BackupManagerService.BackupRequest localBackupRequest = (BackupManagerService.BackupRequest)this.mQueue.get(0);
            this.mQueue.remove(0);
            Slog.d("PerformBackupTask", "starting agent for backup of " + localBackupRequest);
            BackupManagerService.this.addBackupTrace("launch agent for " + localBackupRequest.packageName);
            label773: label787: label815: label829: 
            while (true)
            {
                BackupManagerService.BackupState localBackupState2;
                BackupManagerService localBackupManagerService1;
                String str;
                try
                {
                    this.mCurrentPackage = BackupManagerService.this.mPackageManager.getPackageInfo(localBackupRequest.packageName, 64);
                    if (this.mCurrentPackage.applicationInfo.backupAgentName == null)
                    {
                        Slog.i("PerformBackupTask", "Package " + localBackupRequest.packageName + " no longer supports backup; skipping");
                        BackupManagerService.this.addBackupTrace("skipping - no agent, completion is noop");
                        executeNextState(BackupManagerService.BackupState.RUNNING_QUEUE);
                        BackupManagerService.this.mWakelock.setWorkSource(null);
                        if (this.mStatus == 0)
                            break label773;
                        localBackupState2 = BackupManagerService.BackupState.RUNNING_QUEUE;
                        if (this.mStatus != 3)
                            break label787;
                        BackupManagerService.this.dataChangedImpl(localBackupRequest.packageName);
                        this.mStatus = 0;
                        if (this.mQueue.isEmpty())
                            localBackupState2 = BackupManagerService.BackupState.FINAL;
                        executeNextState(localBackupState2);
                        break;
                    }
                    try
                    {
                        BackupManagerService.this.mWakelock.setWorkSource(new WorkSource(this.mCurrentPackage.applicationInfo.uid));
                        IBackupAgent localIBackupAgent = BackupManagerService.this.bindToAgentSynchronous(this.mCurrentPackage.applicationInfo, 0);
                        BackupManagerService localBackupManagerService2 = BackupManagerService.this;
                        StringBuilder localStringBuilder = new StringBuilder().append("agent bound; a? = ");
                        if (localIBackupAgent != null)
                        {
                            bool = true;
                            localBackupManagerService2.addBackupTrace(bool);
                            if (localIBackupAgent == null)
                                continue;
                            this.mStatus = invokeAgentForBackup(localBackupRequest.packageName, localIBackupAgent, this.mTransport);
                            BackupManagerService.this.mWakelock.setWorkSource(null);
                            if (this.mStatus == 0)
                                break label815;
                            localBackupState2 = BackupManagerService.BackupState.RUNNING_QUEUE;
                            if (this.mStatus != 3)
                                break label829;
                            BackupManagerService.this.dataChangedImpl(localBackupRequest.packageName);
                            this.mStatus = 0;
                            if (!this.mQueue.isEmpty())
                                continue;
                            localBackupState2 = BackupManagerService.BackupState.FINAL;
                            continue;
                        }
                        boolean bool = false;
                        continue;
                        this.mStatus = 3;
                        continue;
                    }
                    catch (SecurityException localSecurityException)
                    {
                        Slog.d("PerformBackupTask", "error in bind/backup", localSecurityException);
                        this.mStatus = 3;
                        BackupManagerService.this.addBackupTrace("agent SE");
                        continue;
                    }
                }
                catch (PackageManager.NameNotFoundException localNameNotFoundException)
                {
                    Slog.d("PerformBackupTask", "Package does not exist; skipping");
                    BackupManagerService.this.addBackupTrace("no such package");
                    this.mStatus = 4;
                    BackupManagerService.this.mWakelock.setWorkSource(null);
                    if (this.mStatus != 0)
                    {
                        localBackupState2 = BackupManagerService.BackupState.RUNNING_QUEUE;
                        if (this.mStatus != 3)
                            break label745;
                        BackupManagerService.this.dataChangedImpl(localBackupRequest.packageName);
                        this.mStatus = 0;
                        if (!this.mQueue.isEmpty())
                            continue;
                        localBackupState2 = BackupManagerService.BackupState.FINAL;
                        continue;
                    }
                }
                finally
                {
                    BackupManagerService.this.mWakelock.setWorkSource(null);
                    if (this.mStatus != 0)
                    {
                        localBackupState1 = BackupManagerService.BackupState.RUNNING_QUEUE;
                        if (this.mStatus == 3)
                        {
                            BackupManagerService.this.dataChangedImpl(localBackupRequest.packageName);
                            this.mStatus = 0;
                            if (this.mQueue.isEmpty())
                                localBackupState1 = BackupManagerService.BackupState.FINAL;
                            executeNextState(localBackupState1);
                            throw localObject;
                        }
                    }
                    else
                    {
                        BackupManagerService.this.addBackupTrace("expecting completion/timeout callback");
                        continue;
                    }
                    if (this.mStatus == 4)
                    {
                        this.mStatus = 0;
                        continue;
                    }
                    revertAndEndBackup();
                    BackupManagerService.BackupState localBackupState1 = BackupManagerService.BackupState.FINAL;
                    continue;
                    localBackupManagerService1 = BackupManagerService.this;
                    str = "expecting completion/timeout callback";
                    localBackupManagerService1.addBackupTrace(str);
                }
                break;
                label745: if (this.mStatus == 4)
                {
                    this.mStatus = 0;
                }
                else
                {
                    revertAndEndBackup();
                    localBackupState2 = BackupManagerService.BackupState.FINAL;
                    continue;
                    localBackupManagerService1 = BackupManagerService.this;
                    str = "expecting completion/timeout callback";
                    continue;
                    if (this.mStatus == 4)
                    {
                        this.mStatus = 0;
                    }
                    else
                    {
                        revertAndEndBackup();
                        localBackupState2 = BackupManagerService.BackupState.FINAL;
                        continue;
                        localBackupManagerService1 = BackupManagerService.this;
                        str = "expecting completion/timeout callback";
                        continue;
                        if (this.mStatus == 4)
                        {
                            this.mStatus = 0;
                        }
                        else
                        {
                            revertAndEndBackup();
                            localBackupState2 = BackupManagerService.BackupState.FINAL;
                        }
                    }
                }
            }
        }

        public void operationComplete()
        {
            BackupManagerService.this.mBackupHandler.removeMessages(7);
            clearAgentState();
            BackupManagerService.this.addBackupTrace("operation complete");
            ParcelFileDescriptor localParcelFileDescriptor = null;
            this.mStatus = 0;
            while (true)
            {
                BackupManagerService.BackupState localBackupState;
                try
                {
                    int i = (int)this.mBackupDataName.length();
                    if (i > 0)
                    {
                        if (this.mStatus == 0)
                        {
                            localParcelFileDescriptor = ParcelFileDescriptor.open(this.mBackupDataName, 268435456);
                            BackupManagerService.this.addBackupTrace("sending data to transport");
                            this.mStatus = this.mTransport.performBackup(this.mCurrentPackage, localParcelFileDescriptor);
                        }
                        BackupManagerService.this.addBackupTrace("data delivered: " + this.mStatus);
                        if (this.mStatus == 0)
                        {
                            BackupManagerService.this.addBackupTrace("finishing op on transport");
                            this.mStatus = this.mTransport.finishBackup();
                            BackupManagerService.this.addBackupTrace("finished: " + this.mStatus);
                        }
                        if (this.mStatus != 0)
                            continue;
                        this.mBackupDataName.delete();
                        this.mNewStateName.renameTo(this.mSavedStateName);
                        Object[] arrayOfObject = new Object[2];
                        arrayOfObject[0] = this.mCurrentPackage.packageName;
                        arrayOfObject[1] = Integer.valueOf(i);
                        EventLog.writeEvent(2824, arrayOfObject);
                        BackupManagerService.this.logBackupComplete(this.mCurrentPackage.packageName);
                    }
                }
                catch (Exception localException)
                {
                    EventLog.writeEvent(2822, this.mCurrentPackage.packageName);
                    continue;
                }
                finally
                {
                    if (localParcelFileDescriptor == null);
                }
                try
                {
                    localParcelFileDescriptor.close();
                    label410: throw localObject;
                    if (this.mQueue.isEmpty());
                    for (localBackupState = BackupManagerService.BackupState.FINAL; ; localBackupState = BackupManagerService.BackupState.RUNNING_QUEUE)
                        break;
                }
                catch (IOException localIOException1)
                {
                    break label410;
                }
            }
        }

        void restartBackupAlarm()
        {
            BackupManagerService.this.addBackupTrace("setting backup trigger");
            try
            {
                synchronized (BackupManagerService.this.mQueueLock)
                {
                    BackupManagerService.this.startBackupAlarmsLocked(this.mTransport.requestBackupTime());
                    label36: return;
                }
            }
            catch (RemoteException localRemoteException)
            {
                break label36;
            }
        }

        void revertAndEndBackup()
        {
            BackupManagerService.this.addBackupTrace("transport error; reverting");
            Iterator localIterator = this.mOriginalQueue.iterator();
            while (localIterator.hasNext())
            {
                BackupManagerService.BackupRequest localBackupRequest = (BackupManagerService.BackupRequest)localIterator.next();
                BackupManagerService.this.dataChangedImpl(localBackupRequest.packageName);
            }
            restartBackupAlarm();
        }
    }

    static enum BackupState
    {
        static
        {
            FINAL = new BackupState("FINAL", 2);
            BackupState[] arrayOfBackupState = new BackupState[3];
            arrayOfBackupState[0] = INITIAL;
            arrayOfBackupState[1] = RUNNING_QUEUE;
            arrayOfBackupState[2] = FINAL;
        }
    }

    static abstract interface BackupRestoreTask
    {
        public abstract void execute();

        public abstract void handleTimeout();

        public abstract void operationComplete();
    }

    class ClearDataObserver extends IPackageDataObserver.Stub
    {
        ClearDataObserver()
        {
        }

        public void onRemoveCompleted(String paramString, boolean paramBoolean)
        {
            synchronized (BackupManagerService.this.mClearDataLock)
            {
                BackupManagerService.this.mClearingData = false;
                BackupManagerService.this.mClearDataLock.notifyAll();
                return;
            }
        }
    }

    private class RunInitializeReceiver extends BroadcastReceiver
    {
        private RunInitializeReceiver()
        {
        }

        public void onReceive(Context paramContext, Intent paramIntent)
        {
            if ("android.app.backup.intent.INIT".equals(paramIntent.getAction()))
                synchronized (BackupManagerService.this.mQueueLock)
                {
                    Slog.v("BackupManagerService", "Running a device init");
                    BackupManagerService.this.mWakelock.acquire();
                    Message localMessage = BackupManagerService.this.mBackupHandler.obtainMessage(5);
                    BackupManagerService.this.mBackupHandler.sendMessage(localMessage);
                }
        }
    }

    private class RunBackupReceiver extends BroadcastReceiver
    {
        private RunBackupReceiver()
        {
        }

        public void onReceive(Context paramContext, Intent paramIntent)
        {
            if ("android.app.backup.intent.RUN".equals(paramIntent.getAction()))
                while (true)
                {
                    synchronized (BackupManagerService.this.mQueueLock)
                    {
                        if (BackupManagerService.this.mPendingInits.size() > 0)
                        {
                            Slog.v("BackupManagerService", "Init pending at scheduled backup");
                            try
                            {
                                BackupManagerService.this.mAlarmManager.cancel(BackupManagerService.this.mRunInitIntent);
                                BackupManagerService.this.mRunInitIntent.send();
                            }
                            catch (PendingIntent.CanceledException localCanceledException)
                            {
                                Slog.e("BackupManagerService", "Run init intent cancelled");
                                continue;
                            }
                        }
                    }
                    if ((BackupManagerService.this.mEnabled) && (BackupManagerService.this.mProvisioned))
                    {
                        if (!BackupManagerService.this.mBackupRunning)
                        {
                            Slog.v("BackupManagerService", "Running a backup pass");
                            BackupManagerService.this.mBackupRunning = true;
                            BackupManagerService.this.mWakelock.acquire();
                            Message localMessage = BackupManagerService.this.mBackupHandler.obtainMessage(1);
                            BackupManagerService.this.mBackupHandler.sendMessage(localMessage);
                        }
                        else
                        {
                            Slog.i("BackupManagerService", "Backup time but one already running");
                        }
                    }
                    else
                        Slog.w("BackupManagerService", "Backup pass but e=" + BackupManagerService.this.mEnabled + " p=" + BackupManagerService.this.mProvisioned);
                }
        }
    }

    private class BackupHandler extends Handler
    {
        public BackupHandler(Looper arg2)
        {
            super();
        }

        // ERROR //
        public void handleMessage(Message paramMessage)
        {
            // Byte code:
            //     0: aload_1
            //     1: getfield 30	android/os/Message:what	I
            //     4: tableswitch	default:+100 -> 104, 1:+101->105, 2:+519->523, 3:+602->606, 4:+758->762, 5:+794->798, 6:+862->866, 7:+1183->1187, 8:+1201->1205, 9:+1272->1276, 10:+700->704, 11:+100->104, 12:+100->104, 13:+100->104, 14:+100->104, 15:+100->104, 16:+100->104, 17:+100->104, 18:+100->104, 19:+100->104, 20:+423->427, 21:+471->475
            //     105: aload_0
            //     106: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     109: invokestatic 36	java/lang/System:currentTimeMillis	()J
            //     112: putfield 40	com/android/server/BackupManagerService:mLastBackupPass	J
            //     115: aload_0
            //     116: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     119: ldc2_w 41
            //     122: aload_0
            //     123: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     126: getfield 40	com/android/server/BackupManagerService:mLastBackupPass	J
            //     129: ladd
            //     130: putfield 45	com/android/server/BackupManagerService:mNextBackupPass	J
            //     133: aload_0
            //     134: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     137: aload_0
            //     138: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     141: getfield 49	com/android/server/BackupManagerService:mCurrentTransport	Ljava/lang/String;
            //     144: invokestatic 53	com/android/server/BackupManagerService:access$100	(Lcom/android/server/BackupManagerService;Ljava/lang/String;)Lcom/android/internal/backup/IBackupTransport;
            //     147: astore 51
            //     149: aload 51
            //     151: ifnonnull +55 -> 206
            //     154: ldc 55
            //     156: ldc 57
            //     158: invokestatic 63	android/util/Slog:v	(Ljava/lang/String;Ljava/lang/String;)I
            //     161: pop
            //     162: aload_0
            //     163: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     166: getfield 67	com/android/server/BackupManagerService:mQueueLock	Ljava/lang/Object;
            //     169: astore 65
            //     171: aload 65
            //     173: monitorenter
            //     174: aload_0
            //     175: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     178: iconst_0
            //     179: putfield 71	com/android/server/BackupManagerService:mBackupRunning	Z
            //     182: aload 65
            //     184: monitorexit
            //     185: aload_0
            //     186: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     189: getfield 75	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     192: invokevirtual 81	android/os/PowerManager$WakeLock:release	()V
            //     195: goto -91 -> 104
            //     198: astore 66
            //     200: aload 65
            //     202: monitorexit
            //     203: aload 66
            //     205: athrow
            //     206: new 83	java/util/ArrayList
            //     209: dup
            //     210: invokespecial 85	java/util/ArrayList:<init>	()V
            //     213: astore 52
            //     215: aload_0
            //     216: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     219: getfield 89	com/android/server/BackupManagerService:mJournal	Ljava/io/File;
            //     222: astore 53
            //     224: aload_0
            //     225: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     228: getfield 67	com/android/server/BackupManagerService:mQueueLock	Ljava/lang/Object;
            //     231: astore 54
            //     233: aload 54
            //     235: monitorenter
            //     236: aload_0
            //     237: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     240: getfield 93	com/android/server/BackupManagerService:mPendingBackups	Ljava/util/HashMap;
            //     243: invokevirtual 99	java/util/HashMap:size	()I
            //     246: ifle +83 -> 329
            //     249: aload_0
            //     250: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     253: getfield 93	com/android/server/BackupManagerService:mPendingBackups	Ljava/util/HashMap;
            //     256: invokevirtual 103	java/util/HashMap:values	()Ljava/util/Collection;
            //     259: invokeinterface 109 1 0
            //     264: astore 61
            //     266: aload 61
            //     268: invokeinterface 115 1 0
            //     273: ifeq +30 -> 303
            //     276: aload 52
            //     278: aload 61
            //     280: invokeinterface 119 1 0
            //     285: checkcast 121	com/android/server/BackupManagerService$BackupRequest
            //     288: invokevirtual 125	java/util/ArrayList:add	(Ljava/lang/Object;)Z
            //     291: pop
            //     292: goto -26 -> 266
            //     295: astore 55
            //     297: aload 54
            //     299: monitorexit
            //     300: aload 55
            //     302: athrow
            //     303: ldc 55
            //     305: ldc 127
            //     307: invokestatic 63	android/util/Slog:v	(Ljava/lang/String;Ljava/lang/String;)I
            //     310: pop
            //     311: aload_0
            //     312: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     315: getfield 93	com/android/server/BackupManagerService:mPendingBackups	Ljava/util/HashMap;
            //     318: invokevirtual 130	java/util/HashMap:clear	()V
            //     321: aload_0
            //     322: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     325: aconst_null
            //     326: putfield 89	com/android/server/BackupManagerService:mJournal	Ljava/io/File;
            //     329: aload 54
            //     331: monitorexit
            //     332: aload 52
            //     334: invokevirtual 131	java/util/ArrayList:size	()I
            //     337: ifle +38 -> 375
            //     340: new 133	com/android/server/BackupManagerService$PerformBackupTask
            //     343: dup
            //     344: aload_0
            //     345: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     348: aload 51
            //     350: aload 52
            //     352: aload 53
            //     354: invokespecial 136	com/android/server/BackupManagerService$PerformBackupTask:<init>	(Lcom/android/server/BackupManagerService;Lcom/android/internal/backup/IBackupTransport;Ljava/util/ArrayList;Ljava/io/File;)V
            //     357: astore 56
            //     359: aload_0
            //     360: aload_0
            //     361: bipush 20
            //     363: aload 56
            //     365: invokevirtual 140	com/android/server/BackupManagerService$BackupHandler:obtainMessage	(ILjava/lang/Object;)Landroid/os/Message;
            //     368: invokevirtual 144	com/android/server/BackupManagerService$BackupHandler:sendMessage	(Landroid/os/Message;)Z
            //     371: pop
            //     372: goto -268 -> 104
            //     375: ldc 55
            //     377: ldc 146
            //     379: invokestatic 63	android/util/Slog:v	(Ljava/lang/String;Ljava/lang/String;)I
            //     382: pop
            //     383: aload_0
            //     384: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     387: getfield 67	com/android/server/BackupManagerService:mQueueLock	Ljava/lang/Object;
            //     390: astore 59
            //     392: aload 59
            //     394: monitorenter
            //     395: aload_0
            //     396: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     399: iconst_0
            //     400: putfield 71	com/android/server/BackupManagerService:mBackupRunning	Z
            //     403: aload 59
            //     405: monitorexit
            //     406: aload_0
            //     407: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     410: getfield 75	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     413: invokevirtual 81	android/os/PowerManager$WakeLock:release	()V
            //     416: goto -312 -> 104
            //     419: astore 60
            //     421: aload 59
            //     423: monitorexit
            //     424: aload 60
            //     426: athrow
            //     427: aload_1
            //     428: getfield 149	android/os/Message:obj	Ljava/lang/Object;
            //     431: checkcast 151	com/android/server/BackupManagerService$BackupRestoreTask
            //     434: invokeinterface 154 1 0
            //     439: goto -335 -> 104
            //     442: astore 49
            //     444: ldc 55
            //     446: new 156	java/lang/StringBuilder
            //     449: dup
            //     450: invokespecial 157	java/lang/StringBuilder:<init>	()V
            //     453: ldc 159
            //     455: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     458: aload_1
            //     459: getfield 149	android/os/Message:obj	Ljava/lang/Object;
            //     462: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     465: invokevirtual 170	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     468: invokestatic 173	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     471: pop
            //     472: goto -368 -> 104
            //     475: aload_1
            //     476: getfield 149	android/os/Message:obj	Ljava/lang/Object;
            //     479: checkcast 151	com/android/server/BackupManagerService$BackupRestoreTask
            //     482: invokeinterface 176 1 0
            //     487: goto -383 -> 104
            //     490: astore 47
            //     492: ldc 55
            //     494: new 156	java/lang/StringBuilder
            //     497: dup
            //     498: invokespecial 157	java/lang/StringBuilder:<init>	()V
            //     501: ldc 178
            //     503: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     506: aload_1
            //     507: getfield 149	android/os/Message:obj	Ljava/lang/Object;
            //     510: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     513: invokevirtual 170	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     516: invokestatic 173	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     519: pop
            //     520: goto -416 -> 104
            //     523: aload_1
            //     524: getfield 149	android/os/Message:obj	Ljava/lang/Object;
            //     527: checkcast 180	com/android/server/BackupManagerService$FullBackupParams
            //     530: astore 46
            //     532: new 182	java/lang/Thread
            //     535: dup
            //     536: new 184	com/android/server/BackupManagerService$PerformFullBackupTask
            //     539: dup
            //     540: aload_0
            //     541: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     544: aload 46
            //     546: getfield 190	com/android/server/BackupManagerService$FullParams:fd	Landroid/os/ParcelFileDescriptor;
            //     549: aload 46
            //     551: getfield 194	com/android/server/BackupManagerService$FullParams:observer	Landroid/app/backup/IFullBackupRestoreObserver;
            //     554: aload 46
            //     556: getfield 197	com/android/server/BackupManagerService$FullBackupParams:includeApks	Z
            //     559: aload 46
            //     561: getfield 200	com/android/server/BackupManagerService$FullBackupParams:includeShared	Z
            //     564: aload 46
            //     566: getfield 203	com/android/server/BackupManagerService$FullParams:curPassword	Ljava/lang/String;
            //     569: aload 46
            //     571: getfield 206	com/android/server/BackupManagerService$FullParams:encryptPassword	Ljava/lang/String;
            //     574: aload 46
            //     576: getfield 209	com/android/server/BackupManagerService$FullBackupParams:allApps	Z
            //     579: aload 46
            //     581: getfield 212	com/android/server/BackupManagerService$FullBackupParams:includeSystem	Z
            //     584: aload 46
            //     586: getfield 216	com/android/server/BackupManagerService$FullBackupParams:packages	[Ljava/lang/String;
            //     589: aload 46
            //     591: getfield 220	com/android/server/BackupManagerService$FullParams:latch	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     594: invokespecial 223	com/android/server/BackupManagerService$PerformFullBackupTask:<init>	(Lcom/android/server/BackupManagerService;Landroid/os/ParcelFileDescriptor;Landroid/app/backup/IFullBackupRestoreObserver;ZZLjava/lang/String;Ljava/lang/String;ZZ[Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicBoolean;)V
            //     597: invokespecial 226	java/lang/Thread:<init>	(Ljava/lang/Runnable;)V
            //     600: invokevirtual 229	java/lang/Thread:start	()V
            //     603: goto -499 -> 104
            //     606: aload_1
            //     607: getfield 149	android/os/Message:obj	Ljava/lang/Object;
            //     610: checkcast 231	com/android/server/BackupManagerService$RestoreParams
            //     613: astore 43
            //     615: ldc 55
            //     617: new 156	java/lang/StringBuilder
            //     620: dup
            //     621: invokespecial 157	java/lang/StringBuilder:<init>	()V
            //     624: ldc 233
            //     626: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     629: aload 43
            //     631: getfield 236	com/android/server/BackupManagerService$RestoreParams:observer	Landroid/app/backup/IRestoreObserver;
            //     634: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     637: invokevirtual 170	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     640: invokestatic 239	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     643: pop
            //     644: aload_0
            //     645: aload_0
            //     646: bipush 20
            //     648: new 241	com/android/server/BackupManagerService$PerformRestoreTask
            //     651: dup
            //     652: aload_0
            //     653: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     656: aload 43
            //     658: getfield 245	com/android/server/BackupManagerService$RestoreParams:transport	Lcom/android/internal/backup/IBackupTransport;
            //     661: aload 43
            //     663: getfield 236	com/android/server/BackupManagerService$RestoreParams:observer	Landroid/app/backup/IRestoreObserver;
            //     666: aload 43
            //     668: getfield 248	com/android/server/BackupManagerService$RestoreParams:token	J
            //     671: aload 43
            //     673: getfield 252	com/android/server/BackupManagerService$RestoreParams:pkgInfo	Landroid/content/pm/PackageInfo;
            //     676: aload 43
            //     678: getfield 255	com/android/server/BackupManagerService$RestoreParams:pmToken	I
            //     681: aload 43
            //     683: getfield 258	com/android/server/BackupManagerService$RestoreParams:needFullBackup	Z
            //     686: aload 43
            //     688: getfield 261	com/android/server/BackupManagerService$RestoreParams:filterSet	[Ljava/lang/String;
            //     691: invokespecial 264	com/android/server/BackupManagerService$PerformRestoreTask:<init>	(Lcom/android/server/BackupManagerService;Lcom/android/internal/backup/IBackupTransport;Landroid/app/backup/IRestoreObserver;JLandroid/content/pm/PackageInfo;IZ[Ljava/lang/String;)V
            //     694: invokevirtual 140	com/android/server/BackupManagerService$BackupHandler:obtainMessage	(ILjava/lang/Object;)Landroid/os/Message;
            //     697: invokevirtual 144	com/android/server/BackupManagerService$BackupHandler:sendMessage	(Landroid/os/Message;)Z
            //     700: pop
            //     701: goto -597 -> 104
            //     704: aload_1
            //     705: getfield 149	android/os/Message:obj	Ljava/lang/Object;
            //     708: checkcast 266	com/android/server/BackupManagerService$FullRestoreParams
            //     711: astore 42
            //     713: new 182	java/lang/Thread
            //     716: dup
            //     717: new 268	com/android/server/BackupManagerService$PerformFullRestoreTask
            //     720: dup
            //     721: aload_0
            //     722: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     725: aload 42
            //     727: getfield 190	com/android/server/BackupManagerService$FullParams:fd	Landroid/os/ParcelFileDescriptor;
            //     730: aload 42
            //     732: getfield 203	com/android/server/BackupManagerService$FullParams:curPassword	Ljava/lang/String;
            //     735: aload 42
            //     737: getfield 206	com/android/server/BackupManagerService$FullParams:encryptPassword	Ljava/lang/String;
            //     740: aload 42
            //     742: getfield 194	com/android/server/BackupManagerService$FullParams:observer	Landroid/app/backup/IFullBackupRestoreObserver;
            //     745: aload 42
            //     747: getfield 220	com/android/server/BackupManagerService$FullParams:latch	Ljava/util/concurrent/atomic/AtomicBoolean;
            //     750: invokespecial 271	com/android/server/BackupManagerService$PerformFullRestoreTask:<init>	(Lcom/android/server/BackupManagerService;Landroid/os/ParcelFileDescriptor;Ljava/lang/String;Ljava/lang/String;Landroid/app/backup/IFullBackupRestoreObserver;Ljava/util/concurrent/atomic/AtomicBoolean;)V
            //     753: invokespecial 226	java/lang/Thread:<init>	(Ljava/lang/Runnable;)V
            //     756: invokevirtual 229	java/lang/Thread:start	()V
            //     759: goto -655 -> 104
            //     762: aload_1
            //     763: getfield 149	android/os/Message:obj	Ljava/lang/Object;
            //     766: checkcast 273	com/android/server/BackupManagerService$ClearParams
            //     769: astore 41
            //     771: new 275	com/android/server/BackupManagerService$PerformClearTask
            //     774: dup
            //     775: aload_0
            //     776: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     779: aload 41
            //     781: getfield 276	com/android/server/BackupManagerService$ClearParams:transport	Lcom/android/internal/backup/IBackupTransport;
            //     784: aload 41
            //     786: getfield 279	com/android/server/BackupManagerService$ClearParams:packageInfo	Landroid/content/pm/PackageInfo;
            //     789: invokespecial 282	com/android/server/BackupManagerService$PerformClearTask:<init>	(Lcom/android/server/BackupManagerService;Lcom/android/internal/backup/IBackupTransport;Landroid/content/pm/PackageInfo;)V
            //     792: invokevirtual 285	com/android/server/BackupManagerService$PerformClearTask:run	()V
            //     795: goto -691 -> 104
            //     798: aload_0
            //     799: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     802: getfield 67	com/android/server/BackupManagerService:mQueueLock	Ljava/lang/Object;
            //     805: astore 38
            //     807: aload 38
            //     809: monitorenter
            //     810: new 287	java/util/HashSet
            //     813: dup
            //     814: aload_0
            //     815: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     818: getfield 291	com/android/server/BackupManagerService:mPendingInits	Ljava/util/HashSet;
            //     821: invokespecial 294	java/util/HashSet:<init>	(Ljava/util/Collection;)V
            //     824: astore 39
            //     826: aload_0
            //     827: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     830: getfield 291	com/android/server/BackupManagerService:mPendingInits	Ljava/util/HashSet;
            //     833: invokevirtual 295	java/util/HashSet:clear	()V
            //     836: aload 38
            //     838: monitorexit
            //     839: new 297	com/android/server/BackupManagerService$PerformInitializeTask
            //     842: dup
            //     843: aload_0
            //     844: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     847: aload 39
            //     849: invokespecial 300	com/android/server/BackupManagerService$PerformInitializeTask:<init>	(Lcom/android/server/BackupManagerService;Ljava/util/HashSet;)V
            //     852: invokevirtual 301	com/android/server/BackupManagerService$PerformInitializeTask:run	()V
            //     855: goto -751 -> 104
            //     858: astore 40
            //     860: aload 38
            //     862: monitorexit
            //     863: aload 40
            //     865: athrow
            //     866: aconst_null
            //     867: astore 15
            //     869: aload_1
            //     870: getfield 149	android/os/Message:obj	Ljava/lang/Object;
            //     873: checkcast 303	com/android/server/BackupManagerService$RestoreGetSetsParams
            //     876: astore 16
            //     878: aload 16
            //     880: getfield 304	com/android/server/BackupManagerService$RestoreGetSetsParams:transport	Lcom/android/internal/backup/IBackupTransport;
            //     883: invokeinterface 310 1 0
            //     888: astore 15
            //     890: aload 16
            //     892: getfield 314	com/android/server/BackupManagerService$RestoreGetSetsParams:session	Lcom/android/server/BackupManagerService$ActiveRestoreSession;
            //     895: astore 30
            //     897: aload 30
            //     899: monitorenter
            //     900: aload 16
            //     902: getfield 314	com/android/server/BackupManagerService$RestoreGetSetsParams:session	Lcom/android/server/BackupManagerService$ActiveRestoreSession;
            //     905: aload 15
            //     907: putfield 320	com/android/server/BackupManagerService$ActiveRestoreSession:mRestoreSets	[Landroid/app/backup/RestoreSet;
            //     910: aload 30
            //     912: monitorexit
            //     913: aload 15
            //     915: ifnonnull +14 -> 929
            //     918: sipush 2831
            //     921: iconst_0
            //     922: anewarray 322	java/lang/Object
            //     925: invokestatic 328	android/util/EventLog:writeEvent	(I[Ljava/lang/Object;)I
            //     928: pop
            //     929: aload 16
            //     931: getfield 329	com/android/server/BackupManagerService$RestoreGetSetsParams:observer	Landroid/app/backup/IRestoreObserver;
            //     934: ifnull +15 -> 949
            //     937: aload 16
            //     939: getfield 329	com/android/server/BackupManagerService$RestoreGetSetsParams:observer	Landroid/app/backup/IRestoreObserver;
            //     942: aload 15
            //     944: invokeinterface 335 2 0
            //     949: aload_0
            //     950: bipush 8
            //     952: invokevirtual 339	com/android/server/BackupManagerService$BackupHandler:removeMessages	(I)V
            //     955: aload_0
            //     956: bipush 8
            //     958: ldc2_w 340
            //     961: invokevirtual 345	com/android/server/BackupManagerService$BackupHandler:sendEmptyMessageDelayed	(IJ)Z
            //     964: pop
            //     965: aload_0
            //     966: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     969: getfield 75	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     972: invokevirtual 81	android/os/PowerManager$WakeLock:release	()V
            //     975: goto -871 -> 104
            //     978: astore 31
            //     980: aload 30
            //     982: monitorexit
            //     983: aload 31
            //     985: athrow
            //     986: astore 23
            //     988: ldc 55
            //     990: ldc_w 347
            //     993: invokestatic 173	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     996: pop
            //     997: aload 16
            //     999: getfield 329	com/android/server/BackupManagerService$RestoreGetSetsParams:observer	Landroid/app/backup/IRestoreObserver;
            //     1002: ifnull +15 -> 1017
            //     1005: aload 16
            //     1007: getfield 329	com/android/server/BackupManagerService$RestoreGetSetsParams:observer	Landroid/app/backup/IRestoreObserver;
            //     1010: aload 15
            //     1012: invokeinterface 335 2 0
            //     1017: aload_0
            //     1018: bipush 8
            //     1020: invokevirtual 339	com/android/server/BackupManagerService$BackupHandler:removeMessages	(I)V
            //     1023: aload_0
            //     1024: bipush 8
            //     1026: ldc2_w 340
            //     1029: invokevirtual 345	com/android/server/BackupManagerService$BackupHandler:sendEmptyMessageDelayed	(IJ)Z
            //     1032: pop
            //     1033: aload_0
            //     1034: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     1037: getfield 75	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     1040: invokevirtual 81	android/os/PowerManager$WakeLock:release	()V
            //     1043: goto -939 -> 104
            //     1046: astore 35
            //     1048: ldc 55
            //     1050: ldc_w 349
            //     1053: invokestatic 173	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     1056: pop
            //     1057: goto -108 -> 949
            //     1060: astore 33
            //     1062: ldc 55
            //     1064: ldc_w 351
            //     1067: aload 33
            //     1069: invokestatic 354	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     1072: pop
            //     1073: goto -124 -> 949
            //     1076: astore 28
            //     1078: ldc 55
            //     1080: ldc_w 349
            //     1083: invokestatic 173	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     1086: pop
            //     1087: goto -70 -> 1017
            //     1090: astore 26
            //     1092: ldc 55
            //     1094: ldc_w 351
            //     1097: aload 26
            //     1099: invokestatic 354	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     1102: pop
            //     1103: goto -86 -> 1017
            //     1106: astore 17
            //     1108: aload 16
            //     1110: getfield 329	com/android/server/BackupManagerService$RestoreGetSetsParams:observer	Landroid/app/backup/IRestoreObserver;
            //     1113: ifnull +15 -> 1128
            //     1116: aload 16
            //     1118: getfield 329	com/android/server/BackupManagerService$RestoreGetSetsParams:observer	Landroid/app/backup/IRestoreObserver;
            //     1121: aload 15
            //     1123: invokeinterface 335 2 0
            //     1128: aload_0
            //     1129: bipush 8
            //     1131: invokevirtual 339	com/android/server/BackupManagerService$BackupHandler:removeMessages	(I)V
            //     1134: aload_0
            //     1135: bipush 8
            //     1137: ldc2_w 340
            //     1140: invokevirtual 345	com/android/server/BackupManagerService$BackupHandler:sendEmptyMessageDelayed	(IJ)Z
            //     1143: pop
            //     1144: aload_0
            //     1145: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     1148: getfield 75	com/android/server/BackupManagerService:mWakelock	Landroid/os/PowerManager$WakeLock;
            //     1151: invokevirtual 81	android/os/PowerManager$WakeLock:release	()V
            //     1154: aload 17
            //     1156: athrow
            //     1157: astore 21
            //     1159: ldc 55
            //     1161: ldc_w 349
            //     1164: invokestatic 173	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     1167: pop
            //     1168: goto -40 -> 1128
            //     1171: astore 19
            //     1173: ldc 55
            //     1175: ldc_w 351
            //     1178: aload 19
            //     1180: invokestatic 354	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     1183: pop
            //     1184: goto -56 -> 1128
            //     1187: aload_0
            //     1188: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     1191: aload_1
            //     1192: getfield 357	android/os/Message:arg1	I
            //     1195: aload_1
            //     1196: getfield 149	android/os/Message:obj	Ljava/lang/Object;
            //     1199: invokevirtual 361	com/android/server/BackupManagerService:handleTimeout	(ILjava/lang/Object;)V
            //     1202: goto -1098 -> 104
            //     1205: aload_0
            //     1206: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     1209: astore 9
            //     1211: aload 9
            //     1213: monitorenter
            //     1214: aload_0
            //     1215: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     1218: getfield 364	com/android/server/BackupManagerService:mActiveRestoreSession	Lcom/android/server/BackupManagerService$ActiveRestoreSession;
            //     1221: ifnull +52 -> 1273
            //     1224: ldc 55
            //     1226: ldc_w 366
            //     1229: invokestatic 369	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     1232: pop
            //     1233: aload_0
            //     1234: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     1237: getfield 364	com/android/server/BackupManagerService:mActiveRestoreSession	Lcom/android/server/BackupManagerService$ActiveRestoreSession;
            //     1240: astore 12
            //     1242: aload 12
            //     1244: invokevirtual 373	java/lang/Object:getClass	()Ljava/lang/Class;
            //     1247: pop
            //     1248: aload_0
            //     1249: new 375	com/android/server/BackupManagerService$ActiveRestoreSession$EndRestoreRunnable
            //     1252: dup
            //     1253: aload 12
            //     1255: aload_0
            //     1256: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     1259: aload_0
            //     1260: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     1263: getfield 364	com/android/server/BackupManagerService:mActiveRestoreSession	Lcom/android/server/BackupManagerService$ActiveRestoreSession;
            //     1266: invokespecial 378	com/android/server/BackupManagerService$ActiveRestoreSession$EndRestoreRunnable:<init>	(Lcom/android/server/BackupManagerService$ActiveRestoreSession;Lcom/android/server/BackupManagerService;Lcom/android/server/BackupManagerService$ActiveRestoreSession;)V
            //     1269: invokevirtual 382	com/android/server/BackupManagerService$BackupHandler:post	(Ljava/lang/Runnable;)Z
            //     1272: pop
            //     1273: aload 9
            //     1275: monitorexit
            //     1276: aload_0
            //     1277: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     1280: getfield 386	com/android/server/BackupManagerService:mFullConfirmations	Landroid/util/SparseArray;
            //     1283: astore_2
            //     1284: aload_2
            //     1285: monitorenter
            //     1286: aload_0
            //     1287: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     1290: getfield 386	com/android/server/BackupManagerService:mFullConfirmations	Landroid/util/SparseArray;
            //     1293: aload_1
            //     1294: getfield 357	android/os/Message:arg1	I
            //     1297: invokevirtual 392	android/util/SparseArray:get	(I)Ljava/lang/Object;
            //     1300: checkcast 186	com/android/server/BackupManagerService$FullParams
            //     1303: astore 4
            //     1305: aload 4
            //     1307: ifnull +75 -> 1382
            //     1310: ldc 55
            //     1312: ldc_w 394
            //     1315: invokestatic 397	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
            //     1318: pop
            //     1319: aload_0
            //     1320: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     1323: aload 4
            //     1325: invokevirtual 401	com/android/server/BackupManagerService:signalFullBackupRestoreCompletion	(Lcom/android/server/BackupManagerService$FullParams;)V
            //     1328: aload_0
            //     1329: getfield 13	com/android/server/BackupManagerService$BackupHandler:this$0	Lcom/android/server/BackupManagerService;
            //     1332: getfield 386	com/android/server/BackupManagerService:mFullConfirmations	Landroid/util/SparseArray;
            //     1335: aload_1
            //     1336: getfield 357	android/os/Message:arg1	I
            //     1339: invokevirtual 404	android/util/SparseArray:delete	(I)V
            //     1342: aload 4
            //     1344: getfield 194	com/android/server/BackupManagerService$FullParams:observer	Landroid/app/backup/IFullBackupRestoreObserver;
            //     1347: astore 7
            //     1349: aload 7
            //     1351: ifnull +13 -> 1364
            //     1354: aload 4
            //     1356: getfield 194	com/android/server/BackupManagerService$FullParams:observer	Landroid/app/backup/IFullBackupRestoreObserver;
            //     1359: invokeinterface 409 1 0
            //     1364: aload_2
            //     1365: monitorexit
            //     1366: goto -1262 -> 104
            //     1369: astore_3
            //     1370: aload_2
            //     1371: monitorexit
            //     1372: aload_3
            //     1373: athrow
            //     1374: astore 10
            //     1376: aload 9
            //     1378: monitorexit
            //     1379: aload 10
            //     1381: athrow
            //     1382: ldc 55
            //     1384: new 156	java/lang/StringBuilder
            //     1387: dup
            //     1388: invokespecial 157	java/lang/StringBuilder:<init>	()V
            //     1391: ldc_w 411
            //     1394: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     1397: aload_1
            //     1398: getfield 357	android/os/Message:arg1	I
            //     1401: invokevirtual 414	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     1404: invokevirtual 170	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     1407: invokestatic 239	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     1410: pop
            //     1411: goto -47 -> 1364
            //     1414: astore 8
            //     1416: goto -52 -> 1364
            //
            // Exception table:
            //     from	to	target	type
            //     174	185	198	finally
            //     200	203	198	finally
            //     236	300	295	finally
            //     303	332	295	finally
            //     395	406	419	finally
            //     421	424	419	finally
            //     427	439	442	java/lang/ClassCastException
            //     475	487	490	java/lang/ClassCastException
            //     810	839	858	finally
            //     860	863	858	finally
            //     900	913	978	finally
            //     980	983	978	finally
            //     878	900	986	java/lang/Exception
            //     918	929	986	java/lang/Exception
            //     983	986	986	java/lang/Exception
            //     937	949	1046	android/os/RemoteException
            //     937	949	1060	java/lang/Exception
            //     1005	1017	1076	android/os/RemoteException
            //     1005	1017	1090	java/lang/Exception
            //     878	900	1106	finally
            //     918	929	1106	finally
            //     983	986	1106	finally
            //     988	997	1106	finally
            //     1116	1128	1157	android/os/RemoteException
            //     1116	1128	1171	java/lang/Exception
            //     1286	1349	1369	finally
            //     1354	1364	1369	finally
            //     1364	1372	1369	finally
            //     1382	1411	1369	finally
            //     1214	1276	1374	finally
            //     1376	1379	1374	finally
            //     1354	1364	1414	android/os/RemoteException
        }
    }

    class Operation
    {
        public BackupManagerService.BackupRestoreTask callback;
        public int state;

        Operation(int paramBackupRestoreTask, BackupManagerService.BackupRestoreTask arg3)
        {
            this.state = paramBackupRestoreTask;
            Object localObject;
            this.callback = localObject;
        }
    }

    class FullRestoreParams extends BackupManagerService.FullParams
    {
        FullRestoreParams(ParcelFileDescriptor arg2)
        {
            super();
            Object localObject;
            this.fd = localObject;
        }
    }

    class FullBackupParams extends BackupManagerService.FullParams
    {
        public boolean allApps;
        public boolean includeApks;
        public boolean includeShared;
        public boolean includeSystem;
        public String[] packages;

        FullBackupParams(ParcelFileDescriptor paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramArrayOfString, String[] arg7)
        {
            super();
            this.fd = paramBoolean1;
            this.includeApks = paramBoolean2;
            this.includeShared = paramBoolean3;
            this.allApps = paramBoolean4;
            this.includeSystem = paramArrayOfString;
            Object localObject;
            this.packages = localObject;
        }
    }

    class FullParams
    {
        public String curPassword;
        public String encryptPassword;
        public ParcelFileDescriptor fd;
        public final AtomicBoolean latch = new AtomicBoolean(false);
        public IFullBackupRestoreObserver observer;

        FullParams()
        {
        }
    }

    class ClearParams
    {
        public PackageInfo packageInfo;
        public IBackupTransport transport;

        ClearParams(IBackupTransport paramPackageInfo, PackageInfo arg3)
        {
            this.transport = paramPackageInfo;
            Object localObject;
            this.packageInfo = localObject;
        }
    }

    class RestoreParams
    {
        public String[] filterSet;
        public boolean needFullBackup;
        public IRestoreObserver observer;
        public PackageInfo pkgInfo;
        public int pmToken;
        public long token;
        public IBackupTransport transport;

        RestoreParams(IBackupTransport paramIRestoreObserver, IRestoreObserver paramLong, long arg4, PackageInfo paramInt, int paramBoolean, boolean arg8)
        {
            this.transport = paramIRestoreObserver;
            this.observer = paramLong;
            this.token = ???;
            this.pkgInfo = paramInt;
            this.pmToken = paramBoolean;
            boolean bool1;
            this.needFullBackup = bool1;
            this.filterSet = null;
        }

        RestoreParams(IBackupTransport paramIRestoreObserver, IRestoreObserver paramLong, long arg4, boolean arg6)
        {
            this.transport = paramIRestoreObserver;
            this.observer = paramLong;
            this.token = ???;
            this.pkgInfo = null;
            this.pmToken = 0;
            boolean bool1;
            this.needFullBackup = bool1;
            this.filterSet = null;
        }

        RestoreParams(IBackupTransport paramIRestoreObserver, IRestoreObserver paramLong, long arg4, String[] paramBoolean, boolean arg7)
        {
            this.transport = paramIRestoreObserver;
            this.observer = paramLong;
            this.token = ???;
            this.pkgInfo = null;
            this.pmToken = 0;
            boolean bool1;
            this.needFullBackup = bool1;
            this.filterSet = paramBoolean;
        }
    }

    class RestoreGetSetsParams
    {
        public IRestoreObserver observer;
        public BackupManagerService.ActiveRestoreSession session;
        public IBackupTransport transport;

        RestoreGetSetsParams(IBackupTransport paramActiveRestoreSession, BackupManagerService.ActiveRestoreSession paramIRestoreObserver, IRestoreObserver arg4)
        {
            this.transport = paramActiveRestoreSession;
            this.session = paramIRestoreObserver;
            Object localObject;
            this.observer = localObject;
        }
    }

    class ProvisionedObserver extends ContentObserver
    {
        public ProvisionedObserver(Handler arg2)
        {
            super();
        }

        public void onChange(boolean paramBoolean)
        {
            boolean bool1 = BackupManagerService.this.mProvisioned;
            boolean bool2 = BackupManagerService.this.deviceIsProvisioned();
            BackupManagerService localBackupManagerService = BackupManagerService.this;
            if ((bool1) || (bool2));
            for (boolean bool3 = true; ; bool3 = false)
            {
                localBackupManagerService.mProvisioned = bool3;
                synchronized (BackupManagerService.this.mQueueLock)
                {
                    if ((BackupManagerService.this.mProvisioned) && (!bool1) && (BackupManagerService.this.mEnabled))
                        BackupManagerService.this.startBackupAlarmsLocked(43200000L);
                    return;
                }
            }
        }
    }

    class BackupRequest
    {
        public String packageName;

        BackupRequest(String arg2)
        {
            Object localObject;
            this.packageName = localObject;
        }

        public String toString()
        {
            return "BackupRequest{pkg=" + this.packageName + "}";
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static void startConfirmationTimeout(BackupManagerService paramBackupManagerService, int paramInt, BackupManagerService.FullParams paramFullParams)
        {
        }

        static boolean startConfirmationUi(BackupManagerService paramBackupManagerService, int paramInt, String paramString)
        {
            while (true)
            {
                int i;
                synchronized (paramBackupManagerService.mFullConfirmations)
                {
                    BackupManagerService.FullParams localFullParams = (BackupManagerService.FullParams)paramBackupManagerService.mFullConfirmations.get(paramInt);
                    if (localFullParams != null)
                    {
                        paramBackupManagerService.mFullConfirmations.delete(paramInt);
                        if ((localFullParams instanceof BackupManagerService.FullBackupParams))
                        {
                            i = 2;
                            localFullParams.observer = null;
                            localFullParams.curPassword = "";
                            localFullParams.encryptPassword = Settings.Secure.getString(paramBackupManagerService.getContext().getContentResolver(), ExtraSettings.Secure.APP_ENCRYPT_PASSWORD);
                            Slog.d("BackupManagerService", "Sending conf message with verb " + i);
                            paramBackupManagerService.mWakelock.acquire();
                            Message localMessage = paramBackupManagerService.mBackupHandler.obtainMessage(i, localFullParams);
                            paramBackupManagerService.mBackupHandler.sendMessage(localMessage);
                            return true;
                        }
                    }
                    else
                    {
                        Slog.w("BackupManagerService", "Attempted to ack full backup/restore with invalid token");
                    }
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.BackupManagerService
 * JD-Core Version:        0.6.2
 */