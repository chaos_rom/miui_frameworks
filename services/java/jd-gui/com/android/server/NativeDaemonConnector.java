package com.android.server;

import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.Message;
import android.os.SystemClock;
import android.util.LocalLog;
import android.util.Slog;
import com.google.android.collect.Lists;
import java.io.FileDescriptor;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicInteger;

final class NativeDaemonConnector
    implements Runnable, Handler.Callback, Watchdog.Monitor
{
    private static final int DEFAULT_TIMEOUT = 60000;
    private static final boolean LOGD = false;
    private static final long WARN_EXECUTE_DELAY_MS = 500L;
    private final int BUFFER_SIZE = 4096;
    private final String TAG;
    private Handler mCallbackHandler;
    private INativeDaemonConnectorCallbacks mCallbacks;
    private final Object mDaemonLock = new Object();
    private LocalLog mLocalLog;
    private OutputStream mOutputStream;
    private final ResponseQueue mResponseQueue;
    private AtomicInteger mSequenceNumber;
    private String mSocket;

    NativeDaemonConnector(INativeDaemonConnectorCallbacks paramINativeDaemonConnectorCallbacks, String paramString1, int paramInt1, String paramString2, int paramInt2)
    {
        this.mCallbacks = paramINativeDaemonConnectorCallbacks;
        this.mSocket = paramString1;
        this.mResponseQueue = new ResponseQueue(paramInt1);
        this.mSequenceNumber = new AtomicInteger(0);
        if (paramString2 != null);
        while (true)
        {
            this.TAG = paramString2;
            this.mLocalLog = new LocalLog(paramInt2);
            return;
            paramString2 = "NativeDaemonConnector";
        }
    }

    static void appendEscaped(StringBuilder paramStringBuilder, String paramString)
    {
        int i;
        int k;
        label30: char c;
        if (paramString.indexOf(' ') >= 0)
        {
            i = 1;
            if (i != 0)
                paramStringBuilder.append('"');
            int j = paramString.length();
            k = 0;
            if (k >= j)
                break label96;
            c = paramString.charAt(k);
            if (c != '"')
                break label69;
            paramStringBuilder.append("\\\"");
        }
        while (true)
        {
            k++;
            break label30;
            i = 0;
            break;
            label69: if (c == '\\')
                paramStringBuilder.append("\\\\");
            else
                paramStringBuilder.append(c);
        }
        label96: if (i != 0)
            paramStringBuilder.append('"');
    }

    // ERROR //
    private void listenToSocket()
        throws java.io.IOException
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_1
        //     2: new 118	android/net/LocalSocket
        //     5: dup
        //     6: invokespecial 119	android/net/LocalSocket:<init>	()V
        //     9: astore_2
        //     10: aload_2
        //     11: new 121	android/net/LocalSocketAddress
        //     14: dup
        //     15: aload_0
        //     16: getfield 63	com/android/server/NativeDaemonConnector:mSocket	Ljava/lang/String;
        //     19: getstatic 127	android/net/LocalSocketAddress$Namespace:RESERVED	Landroid/net/LocalSocketAddress$Namespace;
        //     22: invokespecial 130	android/net/LocalSocketAddress:<init>	(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V
        //     25: invokevirtual 134	android/net/LocalSocket:connect	(Landroid/net/LocalSocketAddress;)V
        //     28: aload_2
        //     29: invokevirtual 138	android/net/LocalSocket:getInputStream	()Ljava/io/InputStream;
        //     32: astore 10
        //     34: aload_0
        //     35: getfield 57	com/android/server/NativeDaemonConnector:mDaemonLock	Ljava/lang/Object;
        //     38: astore 11
        //     40: aload 11
        //     42: monitorenter
        //     43: aload_0
        //     44: aload_2
        //     45: invokevirtual 142	android/net/LocalSocket:getOutputStream	()Ljava/io/OutputStream;
        //     48: putfield 144	com/android/server/NativeDaemonConnector:mOutputStream	Ljava/io/OutputStream;
        //     51: aload 11
        //     53: monitorexit
        //     54: aload_0
        //     55: getfield 61	com/android/server/NativeDaemonConnector:mCallbacks	Lcom/android/server/INativeDaemonConnectorCallbacks;
        //     58: invokeinterface 149 1 0
        //     63: sipush 4096
        //     66: newarray byte
        //     68: astore 13
        //     70: iconst_0
        //     71: istore 14
        //     73: aload 10
        //     75: aload 13
        //     77: iload 14
        //     79: sipush 4096
        //     82: iload 14
        //     84: isub
        //     85: invokevirtual 155	java/io/InputStream:read	([BII)I
        //     88: istore 15
        //     90: iload 15
        //     92: ifge +218 -> 310
        //     95: aload_0
        //     96: new 92	java/lang/StringBuilder
        //     99: dup
        //     100: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     103: ldc 158
        //     105: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     108: iload 15
        //     110: invokevirtual 161	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     113: ldc 163
        //     115: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     118: iload 14
        //     120: invokevirtual 161	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     123: invokevirtual 167	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     126: invokespecial 171	com/android/server/NativeDaemonConnector:loge	(Ljava/lang/String;)V
        //     129: aload_0
        //     130: getfield 57	com/android/server/NativeDaemonConnector:mDaemonLock	Ljava/lang/Object;
        //     133: astore 25
        //     135: aload 25
        //     137: monitorenter
        //     138: aload_0
        //     139: getfield 144	com/android/server/NativeDaemonConnector:mOutputStream	Ljava/io/OutputStream;
        //     142: astore 27
        //     144: aload 27
        //     146: ifnull +41 -> 187
        //     149: aload_0
        //     150: new 92	java/lang/StringBuilder
        //     153: dup
        //     154: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     157: ldc 173
        //     159: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     162: aload_0
        //     163: getfield 63	com/android/server/NativeDaemonConnector:mSocket	Ljava/lang/String;
        //     166: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     169: invokevirtual 167	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     172: invokespecial 171	com/android/server/NativeDaemonConnector:loge	(Ljava/lang/String;)V
        //     175: aload_0
        //     176: getfield 144	com/android/server/NativeDaemonConnector:mOutputStream	Ljava/io/OutputStream;
        //     179: invokevirtual 178	java/io/OutputStream:close	()V
        //     182: aload_0
        //     183: aconst_null
        //     184: putfield 144	com/android/server/NativeDaemonConnector:mOutputStream	Ljava/io/OutputStream;
        //     187: aload 25
        //     189: monitorexit
        //     190: aload_2
        //     191: ifnull +7 -> 198
        //     194: aload_2
        //     195: invokevirtual 179	android/net/LocalSocket:close	()V
        //     198: return
        //     199: astore 12
        //     201: aload 11
        //     203: monitorexit
        //     204: aload 12
        //     206: athrow
        //     207: astore 9
        //     209: aload_2
        //     210: astore_1
        //     211: aload_0
        //     212: new 92	java/lang/StringBuilder
        //     215: dup
        //     216: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     219: ldc 181
        //     221: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     224: aload 9
        //     226: invokevirtual 184	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     229: invokevirtual 167	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     232: invokespecial 171	com/android/server/NativeDaemonConnector:loge	(Ljava/lang/String;)V
        //     235: aload 9
        //     237: athrow
        //     238: astore_3
        //     239: aload_0
        //     240: getfield 57	com/android/server/NativeDaemonConnector:mDaemonLock	Ljava/lang/Object;
        //     243: astore 4
        //     245: aload 4
        //     247: monitorenter
        //     248: aload_0
        //     249: getfield 144	com/android/server/NativeDaemonConnector:mOutputStream	Ljava/io/OutputStream;
        //     252: astore 6
        //     254: aload 6
        //     256: ifnull +41 -> 297
        //     259: aload_0
        //     260: new 92	java/lang/StringBuilder
        //     263: dup
        //     264: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     267: ldc 173
        //     269: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     272: aload_0
        //     273: getfield 63	com/android/server/NativeDaemonConnector:mSocket	Ljava/lang/String;
        //     276: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     279: invokevirtual 167	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     282: invokespecial 171	com/android/server/NativeDaemonConnector:loge	(Ljava/lang/String;)V
        //     285: aload_0
        //     286: getfield 144	com/android/server/NativeDaemonConnector:mOutputStream	Ljava/io/OutputStream;
        //     289: invokevirtual 178	java/io/OutputStream:close	()V
        //     292: aload_0
        //     293: aconst_null
        //     294: putfield 144	com/android/server/NativeDaemonConnector:mOutputStream	Ljava/io/OutputStream;
        //     297: aload 4
        //     299: monitorexit
        //     300: aload_1
        //     301: ifnull +7 -> 308
        //     304: aload_1
        //     305: invokevirtual 179	android/net/LocalSocket:close	()V
        //     308: aload_3
        //     309: athrow
        //     310: iload 15
        //     312: iload 14
        //     314: iadd
        //     315: istore 16
        //     317: iconst_0
        //     318: istore 17
        //     320: iconst_0
        //     321: istore 18
        //     323: iload 18
        //     325: iload 16
        //     327: if_icmpge +160 -> 487
        //     330: aload 13
        //     332: iload 18
        //     334: baload
        //     335: ifne +387 -> 722
        //     338: new 86	java/lang/String
        //     341: dup
        //     342: aload 13
        //     344: iload 17
        //     346: iload 18
        //     348: iload 17
        //     350: isub
        //     351: getstatic 190	java/nio/charset/Charsets:UTF_8	Ljava/nio/charset/Charset;
        //     354: invokespecial 193	java/lang/String:<init>	([BIILjava/nio/charset/Charset;)V
        //     357: astore 21
        //     359: aload_0
        //     360: new 92	java/lang/StringBuilder
        //     363: dup
        //     364: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     367: ldc 195
        //     369: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     372: aload 21
        //     374: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     377: ldc 197
        //     379: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     382: invokevirtual 167	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     385: invokespecial 200	com/android/server/NativeDaemonConnector:log	(Ljava/lang/String;)V
        //     388: aload 21
        //     390: invokestatic 206	com/android/server/NativeDaemonEvent:parseRawEvent	(Ljava/lang/String;)Lcom/android/server/NativeDaemonEvent;
        //     393: astore 23
        //     395: aload 23
        //     397: invokevirtual 210	com/android/server/NativeDaemonEvent:isClassUnsolicited	()Z
        //     400: ifeq +31 -> 431
        //     403: aload_0
        //     404: getfield 212	com/android/server/NativeDaemonConnector:mCallbackHandler	Landroid/os/Handler;
        //     407: aload_0
        //     408: getfield 212	com/android/server/NativeDaemonConnector:mCallbackHandler	Landroid/os/Handler;
        //     411: aload 23
        //     413: invokevirtual 215	com/android/server/NativeDaemonEvent:getCode	()I
        //     416: aload 23
        //     418: invokevirtual 218	com/android/server/NativeDaemonEvent:getRawEvent	()Ljava/lang/String;
        //     421: invokevirtual 224	android/os/Handler:obtainMessage	(ILjava/lang/Object;)Landroid/os/Message;
        //     424: invokevirtual 228	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
        //     427: pop
        //     428: goto +288 -> 716
        //     431: aload_0
        //     432: getfield 68	com/android/server/NativeDaemonConnector:mResponseQueue	Lcom/android/server/NativeDaemonConnector$ResponseQueue;
        //     435: aload 23
        //     437: invokevirtual 231	com/android/server/NativeDaemonEvent:getCmdNumber	()I
        //     440: aload 23
        //     442: invokevirtual 235	com/android/server/NativeDaemonConnector$ResponseQueue:add	(ILcom/android/server/NativeDaemonEvent;)V
        //     445: goto +271 -> 716
        //     448: astore 22
        //     450: aload_0
        //     451: new 92	java/lang/StringBuilder
        //     454: dup
        //     455: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     458: ldc 237
        //     460: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     463: aload 21
        //     465: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     468: ldc 239
        //     470: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     473: aload 22
        //     475: invokevirtual 184	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     478: invokevirtual 167	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     481: invokespecial 200	com/android/server/NativeDaemonConnector:log	(Ljava/lang/String;)V
        //     484: goto +232 -> 716
        //     487: iload 17
        //     489: ifne +50 -> 539
        //     492: new 86	java/lang/String
        //     495: dup
        //     496: aload 13
        //     498: iload 17
        //     500: iload 16
        //     502: getstatic 190	java/nio/charset/Charsets:UTF_8	Ljava/nio/charset/Charset;
        //     505: invokespecial 193	java/lang/String:<init>	([BIILjava/nio/charset/Charset;)V
        //     508: astore 19
        //     510: aload_0
        //     511: new 92	java/lang/StringBuilder
        //     514: dup
        //     515: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     518: ldc 241
        //     520: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     523: aload 19
        //     525: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     528: ldc 197
        //     530: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     533: invokevirtual 167	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     536: invokespecial 200	com/android/server/NativeDaemonConnector:log	(Ljava/lang/String;)V
        //     539: iload 17
        //     541: iload 16
        //     543: if_icmpeq +30 -> 573
        //     546: sipush 4096
        //     549: iload 17
        //     551: isub
        //     552: istore 20
        //     554: aload 13
        //     556: iload 17
        //     558: aload 13
        //     560: iconst_0
        //     561: iload 20
        //     563: invokestatic 247	java/lang/System:arraycopy	(Ljava/lang/Object;ILjava/lang/Object;II)V
        //     566: iload 20
        //     568: istore 14
        //     570: goto -497 -> 73
        //     573: iconst_0
        //     574: istore 14
        //     576: goto -503 -> 73
        //     579: astore 5
        //     581: aload 4
        //     583: monitorexit
        //     584: aload 5
        //     586: athrow
        //     587: astore 7
        //     589: aload_0
        //     590: new 92	java/lang/StringBuilder
        //     593: dup
        //     594: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     597: ldc 249
        //     599: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     602: aload 7
        //     604: invokevirtual 184	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     607: invokevirtual 167	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     610: invokespecial 171	com/android/server/NativeDaemonConnector:loge	(Ljava/lang/String;)V
        //     613: goto -305 -> 308
        //     616: astore 8
        //     618: aload_0
        //     619: new 92	java/lang/StringBuilder
        //     622: dup
        //     623: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     626: ldc 251
        //     628: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     631: aload 8
        //     633: invokevirtual 184	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     636: invokevirtual 167	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     639: invokespecial 171	com/android/server/NativeDaemonConnector:loge	(Ljava/lang/String;)V
        //     642: goto -350 -> 292
        //     645: astore 26
        //     647: aload 25
        //     649: monitorexit
        //     650: aload 26
        //     652: athrow
        //     653: astore 28
        //     655: aload_0
        //     656: new 92	java/lang/StringBuilder
        //     659: dup
        //     660: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     663: ldc 249
        //     665: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     668: aload 28
        //     670: invokevirtual 184	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     673: invokevirtual 167	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     676: invokespecial 171	com/android/server/NativeDaemonConnector:loge	(Ljava/lang/String;)V
        //     679: goto -481 -> 198
        //     682: astore 29
        //     684: aload_0
        //     685: new 92	java/lang/StringBuilder
        //     688: dup
        //     689: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     692: ldc 251
        //     694: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     697: aload 29
        //     699: invokevirtual 184	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     702: invokevirtual 167	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     705: invokespecial 171	com/android/server/NativeDaemonConnector:loge	(Ljava/lang/String;)V
        //     708: goto -526 -> 182
        //     711: astore 9
        //     713: goto -502 -> 211
        //     716: iload 18
        //     718: iconst_1
        //     719: iadd
        //     720: istore 17
        //     722: iinc 18 1
        //     725: goto -402 -> 323
        //     728: astore_3
        //     729: aload_2
        //     730: astore_1
        //     731: goto -492 -> 239
        //
        // Exception table:
        //     from	to	target	type
        //     43	54	199	finally
        //     201	204	199	finally
        //     10	43	207	java/io/IOException
        //     54	129	207	java/io/IOException
        //     204	207	207	java/io/IOException
        //     330	388	207	java/io/IOException
        //     388	445	207	java/io/IOException
        //     450	566	207	java/io/IOException
        //     2	10	238	finally
        //     211	238	238	finally
        //     388	445	448	java/lang/IllegalArgumentException
        //     248	254	579	finally
        //     259	292	579	finally
        //     292	300	579	finally
        //     581	584	579	finally
        //     618	642	579	finally
        //     304	308	587	java/io/IOException
        //     259	292	616	java/io/IOException
        //     138	144	645	finally
        //     149	182	645	finally
        //     182	190	645	finally
        //     647	650	645	finally
        //     684	708	645	finally
        //     194	198	653	java/io/IOException
        //     149	182	682	java/io/IOException
        //     2	10	711	java/io/IOException
        //     10	43	728	finally
        //     54	129	728	finally
        //     204	207	728	finally
        //     330	388	728	finally
        //     388	445	728	finally
        //     450	566	728	finally
    }

    private void log(String paramString)
    {
        this.mLocalLog.log(paramString);
    }

    private void loge(String paramString)
    {
        Slog.e(this.TAG, paramString);
        this.mLocalLog.log(paramString);
    }

    private void makeCommand(StringBuilder paramStringBuilder, String paramString, Object[] paramArrayOfObject)
        throws NativeDaemonConnectorException
    {
        if (paramString.indexOf(0) >= 0)
            throw new IllegalArgumentException("unexpected command: " + paramString);
        paramStringBuilder.append(paramString);
        int i = paramArrayOfObject.length;
        for (int j = 0; j < i; j++)
        {
            Object localObject = paramArrayOfObject[j];
            String str = String.valueOf(localObject);
            if (str.indexOf(0) >= 0)
                throw new IllegalArgumentException("unexpected argument: " + localObject);
            paramStringBuilder.append(' ');
            appendEscaped(paramStringBuilder, str);
        }
    }

    @Deprecated
    public ArrayList<String> doCommand(String paramString)
        throws NativeDaemonConnectorException
    {
        ArrayList localArrayList = Lists.newArrayList();
        NativeDaemonEvent[] arrayOfNativeDaemonEvent = executeForList(paramString, new Object[0]);
        int i = arrayOfNativeDaemonEvent.length;
        for (int j = 0; j < i; j++)
            localArrayList.add(arrayOfNativeDaemonEvent[j].getRawEvent());
        return localArrayList;
    }

    @Deprecated
    public String[] doListCommand(String paramString, int paramInt)
        throws NativeDaemonConnectorException
    {
        ArrayList localArrayList = Lists.newArrayList();
        NativeDaemonEvent[] arrayOfNativeDaemonEvent = executeForList(paramString, new Object[0]);
        int i = 0;
        while (i < -1 + arrayOfNativeDaemonEvent.length)
        {
            NativeDaemonEvent localNativeDaemonEvent2 = arrayOfNativeDaemonEvent[i];
            int j = localNativeDaemonEvent2.getCode();
            if (j == paramInt)
            {
                localArrayList.add(localNativeDaemonEvent2.getMessage());
                i++;
            }
            else
            {
                throw new NativeDaemonConnectorException("unexpected list response " + j + " instead of " + paramInt);
            }
        }
        NativeDaemonEvent localNativeDaemonEvent1 = arrayOfNativeDaemonEvent[(-1 + arrayOfNativeDaemonEvent.length)];
        if (!localNativeDaemonEvent1.isClassOk())
            throw new NativeDaemonConnectorException("unexpected final event: " + localNativeDaemonEvent1);
        return (String[])localArrayList.toArray(new String[localArrayList.size()]);
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        this.mLocalLog.dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
        paramPrintWriter.println();
        this.mResponseQueue.dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
    }

    public NativeDaemonEvent execute(Command paramCommand)
        throws NativeDaemonConnectorException
    {
        return execute(paramCommand.mCmd, paramCommand.mArguments.toArray());
    }

    public NativeDaemonEvent execute(String paramString, Object[] paramArrayOfObject)
        throws NativeDaemonConnectorException
    {
        NativeDaemonEvent[] arrayOfNativeDaemonEvent = executeForList(paramString, paramArrayOfObject);
        if (arrayOfNativeDaemonEvent.length != 1)
            throw new NativeDaemonConnectorException("Expected exactly one response, but received " + arrayOfNativeDaemonEvent.length);
        return arrayOfNativeDaemonEvent[0];
    }

    // ERROR //
    public NativeDaemonEvent[] execute(int paramInt, String paramString, Object[] paramArrayOfObject)
        throws NativeDaemonConnectorException
    {
        // Byte code:
        //     0: invokestatic 283	com/google/android/collect/Lists:newArrayList	()Ljava/util/ArrayList;
        //     3: astore 4
        //     5: aload_0
        //     6: getfield 73	com/android/server/NativeDaemonConnector:mSequenceNumber	Ljava/util/concurrent/atomic/AtomicInteger;
        //     9: invokevirtual 348	java/util/concurrent/atomic/AtomicInteger:incrementAndGet	()I
        //     12: istore 5
        //     14: new 92	java/lang/StringBuilder
        //     17: dup
        //     18: iload 5
        //     20: invokestatic 353	java/lang/Integer:toString	(I)Ljava/lang/String;
        //     23: invokespecial 354	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
        //     26: bipush 32
        //     28: invokevirtual 96	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
        //     31: astore 6
        //     33: invokestatic 360	android/os/SystemClock:elapsedRealtime	()J
        //     36: lstore 7
        //     38: aload_0
        //     39: aload 6
        //     41: aload_2
        //     42: aload_3
        //     43: invokespecial 362	com/android/server/NativeDaemonConnector:makeCommand	(Ljava/lang/StringBuilder;Ljava/lang/String;[Ljava/lang/Object;)V
        //     46: aload 6
        //     48: invokevirtual 167	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     51: astore 9
        //     53: aload_0
        //     54: new 92	java/lang/StringBuilder
        //     57: dup
        //     58: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     61: ldc_w 364
        //     64: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     67: aload 9
        //     69: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     72: ldc 197
        //     74: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     77: invokevirtual 167	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     80: invokespecial 200	com/android/server/NativeDaemonConnector:log	(Ljava/lang/String;)V
        //     83: aload 6
        //     85: iconst_0
        //     86: invokevirtual 96	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
        //     89: pop
        //     90: aload 6
        //     92: invokevirtual 167	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     95: astore 11
        //     97: aload_0
        //     98: getfield 57	com/android/server/NativeDaemonConnector:mDaemonLock	Ljava/lang/Object;
        //     101: astore 12
        //     103: aload 12
        //     105: monitorenter
        //     106: aload_0
        //     107: getfield 144	com/android/server/NativeDaemonConnector:mOutputStream	Ljava/io/OutputStream;
        //     110: ifnonnull +22 -> 132
        //     113: new 262	com/android/server/NativeDaemonConnectorException
        //     116: dup
        //     117: ldc_w 366
        //     120: invokespecial 302	com/android/server/NativeDaemonConnectorException:<init>	(Ljava/lang/String;)V
        //     123: athrow
        //     124: astore 13
        //     126: aload 12
        //     128: monitorexit
        //     129: aload 13
        //     131: athrow
        //     132: aload_0
        //     133: getfield 144	com/android/server/NativeDaemonConnector:mOutputStream	Ljava/io/OutputStream;
        //     136: aload 11
        //     138: getstatic 190	java/nio/charset/Charsets:UTF_8	Ljava/nio/charset/Charset;
        //     141: invokevirtual 370	java/lang/String:getBytes	(Ljava/nio/charset/Charset;)[B
        //     144: invokevirtual 374	java/io/OutputStream:write	([B)V
        //     147: aload 12
        //     149: monitorexit
        //     150: aload_0
        //     151: getfield 68	com/android/server/NativeDaemonConnector:mResponseQueue	Lcom/android/server/NativeDaemonConnector$ResponseQueue;
        //     154: iload 5
        //     156: iload_1
        //     157: aload 11
        //     159: invokevirtual 378	com/android/server/NativeDaemonConnector$ResponseQueue:remove	(IILjava/lang/String;)Lcom/android/server/NativeDaemonEvent;
        //     162: astore 15
        //     164: aload 15
        //     166: ifnonnull +55 -> 221
        //     169: aload_0
        //     170: new 92	java/lang/StringBuilder
        //     173: dup
        //     174: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     177: ldc_w 380
        //     180: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     183: aload 9
        //     185: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     188: invokevirtual 167	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     191: invokespecial 171	com/android/server/NativeDaemonConnector:loge	(Ljava/lang/String;)V
        //     194: new 18	com/android/server/NativeDaemonConnector$NativeDaemonFailureException
        //     197: dup
        //     198: aload 9
        //     200: aload 15
        //     202: invokespecial 383	com/android/server/NativeDaemonConnector$NativeDaemonFailureException:<init>	(Ljava/lang/String;Lcom/android/server/NativeDaemonEvent;)V
        //     205: athrow
        //     206: astore 14
        //     208: new 262	com/android/server/NativeDaemonConnectorException
        //     211: dup
        //     212: ldc_w 385
        //     215: aload 14
        //     217: invokespecial 388	com/android/server/NativeDaemonConnectorException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     220: athrow
        //     221: aload_0
        //     222: new 92	java/lang/StringBuilder
        //     225: dup
        //     226: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     229: ldc_w 390
        //     232: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     235: aload 15
        //     237: invokevirtual 184	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     240: ldc 197
        //     242: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     245: invokevirtual 167	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     248: invokespecial 200	com/android/server/NativeDaemonConnector:log	(Ljava/lang/String;)V
        //     251: aload 4
        //     253: aload 15
        //     255: invokevirtual 292	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     258: pop
        //     259: aload 15
        //     261: invokevirtual 393	com/android/server/NativeDaemonEvent:isClassContinue	()Z
        //     264: ifne -114 -> 150
        //     267: invokestatic 360	android/os/SystemClock:elapsedRealtime	()J
        //     270: lstore 17
        //     272: lload 17
        //     274: lload 7
        //     276: lsub
        //     277: ldc2_w 31
        //     280: lcmp
        //     281: ifle +48 -> 329
        //     284: aload_0
        //     285: new 92	java/lang/StringBuilder
        //     288: dup
        //     289: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     292: ldc_w 395
        //     295: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     298: aload 9
        //     300: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     303: ldc_w 397
        //     306: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     309: lload 17
        //     311: lload 7
        //     313: lsub
        //     314: invokevirtual 400	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     317: ldc_w 402
        //     320: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     323: invokevirtual 167	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     326: invokespecial 171	com/android/server/NativeDaemonConnector:loge	(Ljava/lang/String;)V
        //     329: aload 15
        //     331: invokevirtual 405	com/android/server/NativeDaemonEvent:isClassClientError	()Z
        //     334: ifeq +15 -> 349
        //     337: new 21	com/android/server/NativeDaemonConnector$NativeDaemonArgumentException
        //     340: dup
        //     341: aload 9
        //     343: aload 15
        //     345: invokespecial 406	com/android/server/NativeDaemonConnector$NativeDaemonArgumentException:<init>	(Ljava/lang/String;Lcom/android/server/NativeDaemonEvent;)V
        //     348: athrow
        //     349: aload 15
        //     351: invokevirtual 409	com/android/server/NativeDaemonEvent:isClassServerError	()Z
        //     354: ifeq +15 -> 369
        //     357: new 18	com/android/server/NativeDaemonConnector$NativeDaemonFailureException
        //     360: dup
        //     361: aload 9
        //     363: aload 15
        //     365: invokespecial 383	com/android/server/NativeDaemonConnector$NativeDaemonFailureException:<init>	(Ljava/lang/String;Lcom/android/server/NativeDaemonEvent;)V
        //     368: athrow
        //     369: aload 4
        //     371: aload 4
        //     373: invokevirtual 310	java/util/ArrayList:size	()I
        //     376: anewarray 202	com/android/server/NativeDaemonEvent
        //     379: invokevirtual 314	java/util/ArrayList:toArray	([Ljava/lang/Object;)[Ljava/lang/Object;
        //     382: checkcast 411	[Lcom/android/server/NativeDaemonEvent;
        //     385: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     106	129	124	finally
        //     132	147	124	finally
        //     147	150	124	finally
        //     208	221	124	finally
        //     132	147	206	java/io/IOException
    }

    public NativeDaemonEvent[] executeForList(Command paramCommand)
        throws NativeDaemonConnectorException
    {
        return executeForList(paramCommand.mCmd, paramCommand.mArguments.toArray());
    }

    public NativeDaemonEvent[] executeForList(String paramString, Object[] paramArrayOfObject)
        throws NativeDaemonConnectorException
    {
        return execute(60000, paramString, paramArrayOfObject);
    }

    public boolean handleMessage(Message paramMessage)
    {
        String str = (String)paramMessage.obj;
        try
        {
            if (!this.mCallbacks.onEvent(paramMessage.what, str, NativeDaemonEvent.unescapeArgs(str)))
            {
                Object[] arrayOfObject = new Object[1];
                arrayOfObject[0] = str;
                log(String.format("Unhandled event '%s'", arrayOfObject));
            }
            return true;
        }
        catch (Exception localException)
        {
            while (true)
                loge("Error handling '" + str + "': " + localException);
        }
    }

    public void monitor()
    {
        synchronized (this.mDaemonLock)
        {
        }
    }

    public void run()
    {
        HandlerThread localHandlerThread = new HandlerThread(this.TAG + ".CallbackHandler");
        localHandlerThread.start();
        this.mCallbackHandler = new Handler(localHandlerThread.getLooper(), this);
        while (true)
            try
            {
                listenToSocket();
            }
            catch (Exception localException)
            {
                loge("Error in NativeDaemonConnector: " + localException);
                SystemClock.sleep(5000L);
            }
    }

    private static class ResponseQueue
    {
        private int mMaxCount;
        private final LinkedList<Response> mResponses = new LinkedList();

        ResponseQueue(int paramInt)
        {
            this.mMaxCount = paramInt;
        }

        public void add(int paramInt, NativeDaemonEvent paramNativeDaemonEvent)
        {
            Response localResponse1;
            Response localResponse2;
            synchronized (this.mResponses)
            {
                Iterator localIterator = this.mResponses.iterator();
                Response localResponse4;
                int i;
                do
                {
                    if (!localIterator.hasNext())
                        break;
                    localResponse4 = (Response)localIterator.next();
                    i = localResponse4.cmdNum;
                }
                while (i != paramInt);
                localResponse1 = localResponse4;
                if (localResponse1 == null)
                {
                    try
                    {
                        while (this.mResponses.size() >= this.mMaxCount)
                        {
                            Slog.e("NativeDaemonConnector.ResponseQueue", "more buffered than allowed: " + this.mResponses.size() + " >= " + this.mMaxCount);
                            Response localResponse3 = (Response)this.mResponses.remove();
                            Slog.e("NativeDaemonConnector.ResponseQueue", "Removing request: " + localResponse3.request + " (" + localResponse3.cmdNum + ")");
                        }
                    }
                    finally
                    {
                    }
                    throw localObject1;
                    localResponse2 = new Response(paramInt, null);
                    this.mResponses.add(localResponse2);
                    localResponse2.responses.add(paramNativeDaemonEvent);
                    try
                    {
                        localResponse2.notify();
                        return;
                    }
                    finally
                    {
                        localObject3 = finally;
                        throw localObject3;
                    }
                }
            }
        }

        // ERROR //
        public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
        {
            // Byte code:
            //     0: aload_2
            //     1: ldc 105
            //     3: invokevirtual 111	java/io/PrintWriter:println	(Ljava/lang/String;)V
            //     6: aload_0
            //     7: getfield 25	com/android/server/NativeDaemonConnector$ResponseQueue:mResponses	Ljava/util/LinkedList;
            //     10: astore 4
            //     12: aload 4
            //     14: monitorenter
            //     15: aload_0
            //     16: getfield 25	com/android/server/NativeDaemonConnector$ResponseQueue:mResponses	Ljava/util/LinkedList;
            //     19: invokevirtual 33	java/util/LinkedList:iterator	()Ljava/util/Iterator;
            //     22: astore 6
            //     24: aload 6
            //     26: invokeinterface 39 1 0
            //     31: ifeq +66 -> 97
            //     34: aload 6
            //     36: invokeinterface 43 1 0
            //     41: checkcast 9	com/android/server/NativeDaemonConnector$ResponseQueue$Response
            //     44: astore 7
            //     46: aload_2
            //     47: new 54	java/lang/StringBuilder
            //     50: dup
            //     51: invokespecial 55	java/lang/StringBuilder:<init>	()V
            //     54: ldc 113
            //     56: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     59: aload 7
            //     61: getfield 46	com/android/server/NativeDaemonConnector$ResponseQueue$Response:cmdNum	I
            //     64: invokevirtual 64	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     67: ldc 115
            //     69: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     72: aload 7
            //     74: getfield 85	com/android/server/NativeDaemonConnector$ResponseQueue$Response:request	Ljava/lang/String;
            //     77: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     80: invokevirtual 70	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     83: invokevirtual 111	java/io/PrintWriter:println	(Ljava/lang/String;)V
            //     86: goto -62 -> 24
            //     89: astore 5
            //     91: aload 4
            //     93: monitorexit
            //     94: aload 5
            //     96: athrow
            //     97: aload 4
            //     99: monitorexit
            //     100: return
            //
            // Exception table:
            //     from	to	target	type
            //     15	94	89	finally
            //     97	100	89	finally
        }

        public NativeDaemonEvent remove(int paramInt1, int paramInt2, String paramString)
        {
            long l1 = SystemClock.uptimeMillis() + paramInt2;
            Object localObject1 = null;
            synchronized (this.mResponses)
            {
                NativeDaemonEvent localNativeDaemonEvent;
                while (true)
                {
                    Iterator localIterator = this.mResponses.iterator();
                    Object localObject5 = localObject1;
                    try
                    {
                        while (localIterator.hasNext())
                        {
                            Response localResponse = (Response)localIterator.next();
                            int i = localResponse.cmdNum;
                            Object localObject7;
                            if (i == paramInt1)
                                localObject7 = localResponse;
                            switch (localResponse.responses.size())
                            {
                            default:
                            case 1:
                                while (true)
                                {
                                    localResponse.request = paramString;
                                    localNativeDaemonEvent = (NativeDaemonEvent)localResponse.responses.remove();
                                    break;
                                    this.mResponses.remove(localResponse);
                                }
                                Object localObject2;
                                throw localObject2;
                                localObject7 = localObject5;
                            case 0:
                            }
                            localObject5 = localObject7;
                        }
                        long l2 = SystemClock.uptimeMillis();
                        if (l1 <= l2)
                        {
                            Slog.e("NativeDaemonConnector.ResponseQueue", "Timeout waiting for response");
                            localNativeDaemonEvent = null;
                        }
                        else if (localObject5 == null)
                        {
                            localObject1 = new Response(paramInt1, paramString);
                            this.mResponses.add(localObject1);
                            try
                            {
                                long l3 = l1 - l2;
                                try
                                {
                                    localObject1.wait(l3);
                                    continue;
                                }
                                finally
                                {
                                    localObject6 = finally;
                                    throw localObject6;
                                }
                            }
                            catch (InterruptedException localInterruptedException)
                            {
                            }
                            continue;
                        }
                    }
                    finally
                    {
                        while (true)
                        {
                            continue;
                            localObject1 = localObject5;
                        }
                    }
                }
                return localNativeDaemonEvent;
            }
        }

        private static class Response
        {
            public int cmdNum;
            public String request;
            public LinkedList<NativeDaemonEvent> responses = new LinkedList();

            public Response(int paramInt, String paramString)
            {
                this.cmdNum = paramInt;
                this.request = paramString;
            }
        }
    }

    public static class Command
    {
        private ArrayList<Object> mArguments = Lists.newArrayList();
        private String mCmd;

        public Command(String paramString, Object[] paramArrayOfObject)
        {
            this.mCmd = paramString;
            int i = paramArrayOfObject.length;
            for (int j = 0; j < i; j++)
                appendArg(paramArrayOfObject[j]);
        }

        public Command appendArg(Object paramObject)
        {
            this.mArguments.add(paramObject);
            return this;
        }
    }

    private static class NativeDaemonFailureException extends NativeDaemonConnectorException
    {
        public NativeDaemonFailureException(String paramString, NativeDaemonEvent paramNativeDaemonEvent)
        {
            super(paramNativeDaemonEvent);
        }
    }

    private static class NativeDaemonArgumentException extends NativeDaemonConnectorException
    {
        public NativeDaemonArgumentException(String paramString, NativeDaemonEvent paramNativeDaemonEvent)
        {
            super(paramNativeDaemonEvent);
        }

        public IllegalArgumentException rethrowAsParcelableException()
        {
            throw new IllegalArgumentException(getMessage(), this);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.NativeDaemonConnector
 * JD-Core Version:        0.6.2
 */