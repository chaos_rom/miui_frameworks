package com.android.server;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemService;
import android.util.Slog;

public class BrickReceiver extends BroadcastReceiver
{
    public void onReceive(Context paramContext, Intent paramIntent)
    {
        Slog.w("BrickReceiver", "!!! BRICKING DEVICE !!!");
        SystemService.start("brick");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.BrickReceiver
 * JD-Core Version:        0.6.2
 */