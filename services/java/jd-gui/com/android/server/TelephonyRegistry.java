package com.android.server;

import android.content.Context;
import android.content.Intent;
import android.net.LinkCapabilities;
import android.net.LinkProperties;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.telephony.CellInfo;
import android.telephony.CellLocation;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.text.TextUtils;
import com.android.internal.app.IBatteryStats;
import com.android.internal.telephony.DefaultPhoneNotifier;
import com.android.internal.telephony.IPhoneStateListener;
import com.android.internal.telephony.ITelephonyRegistry.Stub;
import com.android.internal.telephony.Phone.DataState;
import com.android.internal.telephony.Phone.State;
import com.android.server.am.BatteryStatsService;
import java.util.ArrayList;
import java.util.Iterator;

class TelephonyRegistry extends ITelephonyRegistry.Stub
{
    private static final boolean DBG = false;
    static final int PHONE_STATE_PERMISSION_MASK = 236;
    private static final String TAG = "TelephonyRegistry";
    private final IBatteryStats mBatteryStats;
    private boolean mCallForwarding = false;
    private String mCallIncomingNumber = "";
    private int mCallState = 0;
    private CellInfo mCellInfo = null;
    private Bundle mCellLocation = new Bundle();
    private ArrayList<String> mConnectedApns;
    private final Context mContext;
    private int mDataActivity = 0;
    private String mDataConnectionApn = "";
    private LinkCapabilities mDataConnectionLinkCapabilities;
    private LinkProperties mDataConnectionLinkProperties;
    private int mDataConnectionNetworkType;
    private boolean mDataConnectionPossible = false;
    private String mDataConnectionReason = "";
    private int mDataConnectionState = -1;
    private boolean mMessageWaiting = false;
    private int mOtaspMode = 1;
    private final ArrayList<Record> mRecords = new ArrayList();
    private final ArrayList<IBinder> mRemoveList = new ArrayList();
    private ServiceState mServiceState = new ServiceState();
    private SignalStrength mSignalStrength = new SignalStrength();

    TelephonyRegistry(Context paramContext)
    {
        CellLocation localCellLocation = CellLocation.getEmpty();
        if (localCellLocation != null)
            localCellLocation.fillInNotifierBundle(this.mCellLocation);
        this.mContext = paramContext;
        this.mBatteryStats = BatteryStatsService.getService();
        this.mConnectedApns = new ArrayList();
    }

    private void broadcastCallStateChanged(int paramInt, String paramString)
    {
        long l = Binder.clearCallingIdentity();
        if (paramInt == 0);
        try
        {
            this.mBatteryStats.notePhoneOff();
            while (true)
            {
                label17: Binder.restoreCallingIdentity(l);
                Intent localIntent = new Intent("android.intent.action.PHONE_STATE");
                localIntent.putExtra("state", DefaultPhoneNotifier.convertCallState(paramInt).toString());
                if (!TextUtils.isEmpty(paramString))
                    localIntent.putExtra("incoming_number", paramString);
                this.mContext.sendBroadcast(localIntent, "android.permission.READ_PHONE_STATE");
                return;
                this.mBatteryStats.notePhoneOn();
            }
        }
        catch (RemoteException localRemoteException)
        {
            break label17;
        }
        finally
        {
            Binder.restoreCallingIdentity(l);
        }
    }

    private void broadcastDataConnectionFailed(String paramString1, String paramString2)
    {
        Intent localIntent = new Intent("android.intent.action.DATA_CONNECTION_FAILED");
        localIntent.putExtra("reason", paramString1);
        localIntent.putExtra("apnType", paramString2);
        this.mContext.sendStickyBroadcast(localIntent);
    }

    private void broadcastDataConnectionStateChanged(int paramInt, boolean paramBoolean1, String paramString1, String paramString2, String paramString3, LinkProperties paramLinkProperties, LinkCapabilities paramLinkCapabilities, boolean paramBoolean2)
    {
        Intent localIntent = new Intent("android.intent.action.ANY_DATA_STATE");
        localIntent.putExtra("state", DefaultPhoneNotifier.convertDataState(paramInt).toString());
        if (!paramBoolean1)
            localIntent.putExtra("networkUnvailable", true);
        if (paramString1 != null)
            localIntent.putExtra("reason", paramString1);
        if (paramLinkProperties != null)
        {
            localIntent.putExtra("linkProperties", paramLinkProperties);
            String str = paramLinkProperties.getInterfaceName();
            if (str != null)
                localIntent.putExtra("iface", str);
        }
        if (paramLinkCapabilities != null)
            localIntent.putExtra("linkCapabilities", paramLinkCapabilities);
        if (paramBoolean2)
            localIntent.putExtra("networkRoaming", true);
        localIntent.putExtra("apn", paramString2);
        localIntent.putExtra("apnType", paramString3);
        this.mContext.sendStickyBroadcast(localIntent);
    }

    // ERROR //
    private void broadcastServiceStateChanged(ServiceState paramServiceState)
    {
        // Byte code:
        //     0: invokestatic 136	android/os/Binder:clearCallingIdentity	()J
        //     3: lstore_2
        //     4: aload_0
        //     5: getfield 124	com/android/server/TelephonyRegistry:mBatteryStats	Lcom/android/internal/app/IBatteryStats;
        //     8: aload_1
        //     9: invokevirtual 241	android/telephony/ServiceState:getState	()I
        //     12: invokeinterface 245 2 0
        //     17: lload_2
        //     18: invokestatic 145	android/os/Binder:restoreCallingIdentity	(J)V
        //     21: new 147	android/content/Intent
        //     24: dup
        //     25: ldc 247
        //     27: invokespecial 152	android/content/Intent:<init>	(Ljava/lang/String;)V
        //     30: astore 5
        //     32: new 97	android/os/Bundle
        //     35: dup
        //     36: invokespecial 98	android/os/Bundle:<init>	()V
        //     39: astore 6
        //     41: aload_1
        //     42: aload 6
        //     44: invokevirtual 248	android/telephony/ServiceState:fillInNotifierBundle	(Landroid/os/Bundle;)V
        //     47: aload 5
        //     49: aload 6
        //     51: invokevirtual 252	android/content/Intent:putExtras	(Landroid/os/Bundle;)Landroid/content/Intent;
        //     54: pop
        //     55: aload_0
        //     56: getfield 116	com/android/server/TelephonyRegistry:mContext	Landroid/content/Context;
        //     59: aload 5
        //     61: invokevirtual 201	android/content/Context:sendStickyBroadcast	(Landroid/content/Intent;)V
        //     64: return
        //     65: astore 8
        //     67: lload_2
        //     68: invokestatic 145	android/os/Binder:restoreCallingIdentity	(J)V
        //     71: aload 8
        //     73: athrow
        //     74: astore 4
        //     76: goto -59 -> 17
        //
        // Exception table:
        //     from	to	target	type
        //     4	17	65	finally
        //     4	17	74	android/os/RemoteException
    }

    // ERROR //
    private void broadcastSignalStrengthChanged(SignalStrength paramSignalStrength)
    {
        // Byte code:
        //     0: invokestatic 136	android/os/Binder:clearCallingIdentity	()J
        //     3: lstore_2
        //     4: aload_0
        //     5: getfield 124	com/android/server/TelephonyRegistry:mBatteryStats	Lcom/android/internal/app/IBatteryStats;
        //     8: aload_1
        //     9: invokeinterface 257 2 0
        //     14: lload_2
        //     15: invokestatic 145	android/os/Binder:restoreCallingIdentity	(J)V
        //     18: new 147	android/content/Intent
        //     21: dup
        //     22: ldc_w 259
        //     25: invokespecial 152	android/content/Intent:<init>	(Ljava/lang/String;)V
        //     28: astore 5
        //     30: aload 5
        //     32: ldc_w 260
        //     35: invokevirtual 264	android/content/Intent:addFlags	(I)Landroid/content/Intent;
        //     38: pop
        //     39: new 97	android/os/Bundle
        //     42: dup
        //     43: invokespecial 98	android/os/Bundle:<init>	()V
        //     46: astore 7
        //     48: aload_1
        //     49: aload 7
        //     51: invokevirtual 265	android/telephony/SignalStrength:fillInNotifierBundle	(Landroid/os/Bundle;)V
        //     54: aload 5
        //     56: aload 7
        //     58: invokevirtual 252	android/content/Intent:putExtras	(Landroid/os/Bundle;)Landroid/content/Intent;
        //     61: pop
        //     62: aload_0
        //     63: getfield 116	com/android/server/TelephonyRegistry:mContext	Landroid/content/Context;
        //     66: aload 5
        //     68: invokevirtual 201	android/content/Context:sendStickyBroadcast	(Landroid/content/Intent;)V
        //     71: return
        //     72: astore 9
        //     74: lload_2
        //     75: invokestatic 145	android/os/Binder:restoreCallingIdentity	(J)V
        //     78: aload 9
        //     80: athrow
        //     81: astore 4
        //     83: goto -69 -> 14
        //
        // Exception table:
        //     from	to	target	type
        //     4	14	72	finally
        //     4	14	81	android/os/RemoteException
    }

    private void checkListenerPermission(int paramInt)
    {
        if ((paramInt & 0x10) != 0)
            this.mContext.enforceCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION", null);
        if ((paramInt & 0x400) != 0)
            this.mContext.enforceCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION", null);
        if ((paramInt & 0xEC) != 0)
            this.mContext.enforceCallingOrSelfPermission("android.permission.READ_PHONE_STATE", null);
    }

    private boolean checkNotifyPermission(String paramString)
    {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.MODIFY_PHONE_STATE") == 0);
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            new StringBuilder().append("Modify Phone State Permission Denial: ").append(paramString).append(" from pid=").append(Binder.getCallingPid()).append(", uid=").append(Binder.getCallingUid()).toString();
        }
    }

    private void handleRemoveListLocked()
    {
        if (this.mRemoveList.size() > 0)
        {
            Iterator localIterator = this.mRemoveList.iterator();
            while (localIterator.hasNext())
                remove((IBinder)localIterator.next());
            this.mRemoveList.clear();
        }
    }

    private void remove(IBinder paramIBinder)
    {
        while (true)
        {
            int j;
            synchronized (this.mRecords)
            {
                int i = this.mRecords.size();
                j = 0;
                if (j < i)
                {
                    if (((Record)this.mRecords.get(j)).binder != paramIBinder)
                        break label71;
                    this.mRecords.remove(j);
                }
            }
            return;
            label71: j++;
        }
    }

    // ERROR //
    public void dump(java.io.FileDescriptor paramFileDescriptor, java.io.PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 116	com/android/server/TelephonyRegistry:mContext	Landroid/content/Context;
        //     4: ldc_w 343
        //     7: invokevirtual 279	android/content/Context:checkCallingOrSelfPermission	(Ljava/lang/String;)I
        //     10: ifeq +42 -> 52
        //     13: aload_2
        //     14: new 281	java/lang/StringBuilder
        //     17: dup
        //     18: invokespecial 282	java/lang/StringBuilder:<init>	()V
        //     21: ldc_w 345
        //     24: invokevirtual 288	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     27: invokestatic 293	android/os/Binder:getCallingPid	()I
        //     30: invokevirtual 296	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     33: ldc_w 298
        //     36: invokevirtual 288	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     39: invokestatic 301	android/os/Binder:getCallingUid	()I
        //     42: invokevirtual 296	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     45: invokevirtual 302	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     48: invokevirtual 350	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     51: return
        //     52: aload_0
        //     53: getfield 65	com/android/server/TelephonyRegistry:mRecords	Ljava/util/ArrayList;
        //     56: astore 4
        //     58: aload 4
        //     60: monitorenter
        //     61: aload_0
        //     62: getfield 65	com/android/server/TelephonyRegistry:mRecords	Ljava/util/ArrayList;
        //     65: invokevirtual 306	java/util/ArrayList:size	()I
        //     68: istore 6
        //     70: aload_2
        //     71: ldc_w 352
        //     74: invokevirtual 350	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     77: aload_2
        //     78: new 281	java/lang/StringBuilder
        //     81: dup
        //     82: invokespecial 282	java/lang/StringBuilder:<init>	()V
        //     85: ldc_w 354
        //     88: invokevirtual 288	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     91: aload_0
        //     92: getfield 67	com/android/server/TelephonyRegistry:mCallState	I
        //     95: invokevirtual 296	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     98: invokevirtual 302	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     101: invokevirtual 350	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     104: aload_2
        //     105: new 281	java/lang/StringBuilder
        //     108: dup
        //     109: invokespecial 282	java/lang/StringBuilder:<init>	()V
        //     112: ldc_w 356
        //     115: invokevirtual 288	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     118: aload_0
        //     119: getfield 71	com/android/server/TelephonyRegistry:mCallIncomingNumber	Ljava/lang/String;
        //     122: invokevirtual 288	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     125: invokevirtual 302	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     128: invokevirtual 350	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     131: aload_2
        //     132: new 281	java/lang/StringBuilder
        //     135: dup
        //     136: invokespecial 282	java/lang/StringBuilder:<init>	()V
        //     139: ldc_w 358
        //     142: invokevirtual 288	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     145: aload_0
        //     146: getfield 76	com/android/server/TelephonyRegistry:mServiceState	Landroid/telephony/ServiceState;
        //     149: invokevirtual 361	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     152: invokevirtual 302	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     155: invokevirtual 350	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     158: aload_2
        //     159: new 281	java/lang/StringBuilder
        //     162: dup
        //     163: invokespecial 282	java/lang/StringBuilder:<init>	()V
        //     166: ldc_w 363
        //     169: invokevirtual 288	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     172: aload_0
        //     173: getfield 81	com/android/server/TelephonyRegistry:mSignalStrength	Landroid/telephony/SignalStrength;
        //     176: invokevirtual 361	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     179: invokevirtual 302	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     182: invokevirtual 350	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     185: aload_2
        //     186: new 281	java/lang/StringBuilder
        //     189: dup
        //     190: invokespecial 282	java/lang/StringBuilder:<init>	()V
        //     193: ldc_w 365
        //     196: invokevirtual 288	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     199: aload_0
        //     200: getfield 83	com/android/server/TelephonyRegistry:mMessageWaiting	Z
        //     203: invokevirtual 368	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     206: invokevirtual 302	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     209: invokevirtual 350	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     212: aload_2
        //     213: new 281	java/lang/StringBuilder
        //     216: dup
        //     217: invokespecial 282	java/lang/StringBuilder:<init>	()V
        //     220: ldc_w 370
        //     223: invokevirtual 288	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     226: aload_0
        //     227: getfield 85	com/android/server/TelephonyRegistry:mCallForwarding	Z
        //     230: invokevirtual 368	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     233: invokevirtual 302	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     236: invokevirtual 350	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     239: aload_2
        //     240: new 281	java/lang/StringBuilder
        //     243: dup
        //     244: invokespecial 282	java/lang/StringBuilder:<init>	()V
        //     247: ldc_w 372
        //     250: invokevirtual 288	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     253: aload_0
        //     254: getfield 87	com/android/server/TelephonyRegistry:mDataActivity	I
        //     257: invokevirtual 296	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     260: invokevirtual 302	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     263: invokevirtual 350	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     266: aload_2
        //     267: new 281	java/lang/StringBuilder
        //     270: dup
        //     271: invokespecial 282	java/lang/StringBuilder:<init>	()V
        //     274: ldc_w 374
        //     277: invokevirtual 288	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     280: aload_0
        //     281: getfield 89	com/android/server/TelephonyRegistry:mDataConnectionState	I
        //     284: invokevirtual 296	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     287: invokevirtual 302	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     290: invokevirtual 350	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     293: aload_2
        //     294: new 281	java/lang/StringBuilder
        //     297: dup
        //     298: invokespecial 282	java/lang/StringBuilder:<init>	()V
        //     301: ldc_w 376
        //     304: invokevirtual 288	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     307: aload_0
        //     308: getfield 91	com/android/server/TelephonyRegistry:mDataConnectionPossible	Z
        //     311: invokevirtual 368	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     314: invokevirtual 302	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     317: invokevirtual 350	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     320: aload_2
        //     321: new 281	java/lang/StringBuilder
        //     324: dup
        //     325: invokespecial 282	java/lang/StringBuilder:<init>	()V
        //     328: ldc_w 378
        //     331: invokevirtual 288	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     334: aload_0
        //     335: getfield 93	com/android/server/TelephonyRegistry:mDataConnectionReason	Ljava/lang/String;
        //     338: invokevirtual 288	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     341: invokevirtual 302	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     344: invokevirtual 350	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     347: aload_2
        //     348: new 281	java/lang/StringBuilder
        //     351: dup
        //     352: invokespecial 282	java/lang/StringBuilder:<init>	()V
        //     355: ldc_w 380
        //     358: invokevirtual 288	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     361: aload_0
        //     362: getfield 95	com/android/server/TelephonyRegistry:mDataConnectionApn	Ljava/lang/String;
        //     365: invokevirtual 288	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     368: invokevirtual 302	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     371: invokevirtual 350	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     374: aload_2
        //     375: new 281	java/lang/StringBuilder
        //     378: dup
        //     379: invokespecial 282	java/lang/StringBuilder:<init>	()V
        //     382: ldc_w 382
        //     385: invokevirtual 288	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     388: aload_0
        //     389: getfield 384	com/android/server/TelephonyRegistry:mDataConnectionLinkProperties	Landroid/net/LinkProperties;
        //     392: invokevirtual 361	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     395: invokevirtual 302	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     398: invokevirtual 350	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     401: aload_2
        //     402: new 281	java/lang/StringBuilder
        //     405: dup
        //     406: invokespecial 282	java/lang/StringBuilder:<init>	()V
        //     409: ldc_w 386
        //     412: invokevirtual 288	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     415: aload_0
        //     416: getfield 388	com/android/server/TelephonyRegistry:mDataConnectionLinkCapabilities	Landroid/net/LinkCapabilities;
        //     419: invokevirtual 361	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     422: invokevirtual 302	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     425: invokevirtual 350	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     428: aload_2
        //     429: new 281	java/lang/StringBuilder
        //     432: dup
        //     433: invokespecial 282	java/lang/StringBuilder:<init>	()V
        //     436: ldc_w 390
        //     439: invokevirtual 288	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     442: aload_0
        //     443: getfield 100	com/android/server/TelephonyRegistry:mCellLocation	Landroid/os/Bundle;
        //     446: invokevirtual 361	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     449: invokevirtual 302	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     452: invokevirtual 350	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     455: aload_2
        //     456: new 281	java/lang/StringBuilder
        //     459: dup
        //     460: invokespecial 282	java/lang/StringBuilder:<init>	()V
        //     463: ldc_w 392
        //     466: invokevirtual 288	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     469: aload_0
        //     470: getfield 104	com/android/server/TelephonyRegistry:mCellInfo	Landroid/telephony/CellInfo;
        //     473: invokevirtual 361	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     476: invokevirtual 302	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     479: invokevirtual 350	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     482: aload_2
        //     483: new 281	java/lang/StringBuilder
        //     486: dup
        //     487: invokespecial 282	java/lang/StringBuilder:<init>	()V
        //     490: ldc_w 394
        //     493: invokevirtual 288	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     496: iload 6
        //     498: invokevirtual 296	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     501: invokevirtual 302	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     504: invokevirtual 350	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     507: aload_0
        //     508: getfield 65	com/android/server/TelephonyRegistry:mRecords	Ljava/util/ArrayList;
        //     511: invokevirtual 310	java/util/ArrayList:iterator	()Ljava/util/Iterator;
        //     514: astore 7
        //     516: aload 7
        //     518: invokeinterface 316 1 0
        //     523: ifeq +71 -> 594
        //     526: aload 7
        //     528: invokeinterface 320 1 0
        //     533: checkcast 8	com/android/server/TelephonyRegistry$Record
        //     536: astore 8
        //     538: aload_2
        //     539: new 281	java/lang/StringBuilder
        //     542: dup
        //     543: invokespecial 282	java/lang/StringBuilder:<init>	()V
        //     546: ldc_w 396
        //     549: invokevirtual 288	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     552: aload 8
        //     554: getfield 399	com/android/server/TelephonyRegistry$Record:pkgForDebug	Ljava/lang/String;
        //     557: invokevirtual 288	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     560: ldc_w 401
        //     563: invokevirtual 288	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     566: aload 8
        //     568: getfield 404	com/android/server/TelephonyRegistry$Record:events	I
        //     571: invokestatic 410	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     574: invokevirtual 288	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     577: invokevirtual 302	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     580: invokevirtual 350	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     583: goto -67 -> 516
        //     586: astore 5
        //     588: aload 4
        //     590: monitorexit
        //     591: aload 5
        //     593: athrow
        //     594: aload 4
        //     596: monitorexit
        //     597: goto -546 -> 51
        //
        // Exception table:
        //     from	to	target	type
        //     61	591	586	finally
        //     594	597	586	finally
    }

    public void listen(String paramString, IPhoneStateListener paramIPhoneStateListener, int paramInt, boolean paramBoolean)
    {
        if (paramInt != 0)
            checkListenerPermission(paramInt);
        synchronized (this.mRecords)
        {
            IBinder localIBinder = paramIPhoneStateListener.asBinder();
            int i = this.mRecords.size();
            int j = 0;
            Object localObject4 = null;
            while (true)
            {
                if (j < i);
                try
                {
                    Record localRecord = (Record)this.mRecords.get(j);
                    if (localIBinder == localRecord.binder)
                    {
                        label72: (paramInt & (paramInt ^ localRecord.events));
                        localRecord.events = paramInt;
                        if ((paramBoolean) && ((paramInt & 0x1) == 0));
                    }
                    try
                    {
                        localRecord.callback.onServiceStateChanged(new ServiceState(this.mServiceState));
                        if ((paramInt & 0x2) == 0);
                    }
                    catch (RemoteException localRemoteException2)
                    {
                        try
                        {
                            int k = this.mSignalStrength.getGsmSignalStrength();
                            IPhoneStateListener localIPhoneStateListener = localRecord.callback;
                            if (k == 99)
                                k = -1;
                            localIPhoneStateListener.onSignalStrengthChanged(k);
                            if ((paramInt & 0x4) == 0);
                        }
                        catch (RemoteException localRemoteException2)
                        {
                            try
                            {
                                localRecord.callback.onMessageWaitingIndicatorChanged(this.mMessageWaiting);
                                if ((paramInt & 0x8) == 0);
                            }
                            catch (RemoteException localRemoteException2)
                            {
                                try
                                {
                                    localRecord.callback.onCallForwardingIndicatorChanged(this.mCallForwarding);
                                    if ((paramInt & 0x10) == 0);
                                }
                                catch (RemoteException localRemoteException2)
                                {
                                    try
                                    {
                                        localRecord.callback.onCellLocationChanged(new Bundle(this.mCellLocation));
                                        if ((paramInt & 0x20) == 0);
                                    }
                                    catch (RemoteException localRemoteException2)
                                    {
                                        try
                                        {
                                            localRecord.callback.onCallStateChanged(this.mCallState, this.mCallIncomingNumber);
                                            if ((paramInt & 0x40) == 0);
                                        }
                                        catch (RemoteException localRemoteException2)
                                        {
                                            try
                                            {
                                                localRecord.callback.onDataConnectionStateChanged(this.mDataConnectionState, this.mDataConnectionNetworkType);
                                                if ((paramInt & 0x80) == 0);
                                            }
                                            catch (RemoteException localRemoteException2)
                                            {
                                                try
                                                {
                                                    localRecord.callback.onDataActivity(this.mDataActivity);
                                                    if ((paramInt & 0x100) == 0);
                                                }
                                                catch (RemoteException localRemoteException2)
                                                {
                                                    try
                                                    {
                                                        localRecord.callback.onSignalStrengthsChanged(this.mSignalStrength);
                                                        if ((paramInt & 0x200) == 0);
                                                    }
                                                    catch (RemoteException localRemoteException2)
                                                    {
                                                        try
                                                        {
                                                            localRecord.callback.onOtaspChanged(this.mOtaspMode);
                                                            if ((paramInt & 0x400) == 0);
                                                        }
                                                        catch (RemoteException localRemoteException2)
                                                        {
                                                            try
                                                            {
                                                                while (true)
                                                                {
                                                                    localRecord.callback.onCellInfoChanged(new CellInfo(this.mCellInfo));
                                                                    return;
                                                                    j++;
                                                                    localObject4 = localRecord;
                                                                    break;
                                                                    localRecord = new Record(null);
                                                                    localRecord.binder = localIBinder;
                                                                    localRecord.callback = paramIPhoneStateListener;
                                                                    localRecord.pkgForDebug = paramString;
                                                                    this.mRecords.add(localRecord);
                                                                    break label72;
                                                                    label432: Object localObject1;
                                                                    throw localObject1;
                                                                    localRemoteException11 = localRemoteException11;
                                                                    remove(localRecord.binder);
                                                                    continue;
                                                                    localRemoteException10 = localRemoteException10;
                                                                    remove(localRecord.binder);
                                                                    continue;
                                                                    localRemoteException9 = localRemoteException9;
                                                                    remove(localRecord.binder);
                                                                    continue;
                                                                    localRemoteException8 = localRemoteException8;
                                                                    remove(localRecord.binder);
                                                                    continue;
                                                                    localRemoteException7 = localRemoteException7;
                                                                    remove(localRecord.binder);
                                                                    continue;
                                                                    localRemoteException6 = localRemoteException6;
                                                                    remove(localRecord.binder);
                                                                    continue;
                                                                    localRemoteException5 = localRemoteException5;
                                                                    remove(localRecord.binder);
                                                                    continue;
                                                                    localRemoteException4 = localRemoteException4;
                                                                    remove(localRecord.binder);
                                                                    continue;
                                                                    localRemoteException3 = localRemoteException3;
                                                                    remove(localRecord.binder);
                                                                }
                                                                localRemoteException2 = localRemoteException2;
                                                                remove(localRecord.binder);
                                                            }
                                                            catch (RemoteException localRemoteException1)
                                                            {
                                                                remove(localRecord.binder);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    remove(paramIPhoneStateListener.asBinder());
                }
                finally
                {
                    break label432;
                }
            }
        }
    }

    public void notifyCallForwardingChanged(boolean paramBoolean)
    {
        if (!checkNotifyPermission("notifyCallForwardingChanged()"));
        while (true)
        {
            return;
            synchronized (this.mRecords)
            {
                this.mCallForwarding = paramBoolean;
                Iterator localIterator = this.mRecords.iterator();
                while (true)
                    if (localIterator.hasNext())
                    {
                        Record localRecord = (Record)localIterator.next();
                        int i = localRecord.events;
                        if ((i & 0x8) == 0)
                            continue;
                        try
                        {
                            localRecord.callback.onCallForwardingIndicatorChanged(paramBoolean);
                        }
                        catch (RemoteException localRemoteException)
                        {
                            this.mRemoveList.add(localRecord.binder);
                        }
                    }
            }
            handleRemoveListLocked();
        }
    }

    public void notifyCallState(int paramInt, String paramString)
    {
        if (!checkNotifyPermission("notifyCallState()"));
        while (true)
        {
            return;
            synchronized (this.mRecords)
            {
                this.mCallState = paramInt;
                this.mCallIncomingNumber = paramString;
                Iterator localIterator = this.mRecords.iterator();
                while (true)
                    if (localIterator.hasNext())
                    {
                        Record localRecord = (Record)localIterator.next();
                        int i = localRecord.events;
                        if ((i & 0x20) == 0)
                            continue;
                        try
                        {
                            localRecord.callback.onCallStateChanged(paramInt, paramString);
                        }
                        catch (RemoteException localRemoteException)
                        {
                            this.mRemoveList.add(localRecord.binder);
                        }
                    }
            }
            handleRemoveListLocked();
            broadcastCallStateChanged(paramInt, paramString);
        }
    }

    public void notifyCellInfo(CellInfo paramCellInfo)
    {
        if (!checkNotifyPermission("notifyCellInfo()"));
        while (true)
        {
            return;
            synchronized (this.mRecords)
            {
                this.mCellInfo = paramCellInfo;
                Iterator localIterator = this.mRecords.iterator();
                while (true)
                    if (localIterator.hasNext())
                    {
                        Record localRecord = (Record)localIterator.next();
                        int i = localRecord.events;
                        if ((i & 0x400) == 0)
                            continue;
                        try
                        {
                            localRecord.callback.onCellInfoChanged(new CellInfo(paramCellInfo));
                        }
                        catch (RemoteException localRemoteException)
                        {
                            this.mRemoveList.add(localRecord.binder);
                        }
                    }
            }
            handleRemoveListLocked();
        }
    }

    public void notifyCellLocation(Bundle paramBundle)
    {
        if (!checkNotifyPermission("notifyCellLocation()"));
        while (true)
        {
            return;
            synchronized (this.mRecords)
            {
                this.mCellLocation = paramBundle;
                Iterator localIterator = this.mRecords.iterator();
                while (true)
                    if (localIterator.hasNext())
                    {
                        Record localRecord = (Record)localIterator.next();
                        int i = localRecord.events;
                        if ((i & 0x10) == 0)
                            continue;
                        try
                        {
                            localRecord.callback.onCellLocationChanged(new Bundle(paramBundle));
                        }
                        catch (RemoteException localRemoteException)
                        {
                            this.mRemoveList.add(localRecord.binder);
                        }
                    }
            }
            handleRemoveListLocked();
        }
    }

    public void notifyDataActivity(int paramInt)
    {
        if (!checkNotifyPermission("notifyDataActivity()"));
        while (true)
        {
            return;
            synchronized (this.mRecords)
            {
                this.mDataActivity = paramInt;
                Iterator localIterator = this.mRecords.iterator();
                while (true)
                    if (localIterator.hasNext())
                    {
                        Record localRecord = (Record)localIterator.next();
                        int i = localRecord.events;
                        if ((i & 0x80) == 0)
                            continue;
                        try
                        {
                            localRecord.callback.onDataActivity(paramInt);
                        }
                        catch (RemoteException localRemoteException)
                        {
                            this.mRemoveList.add(localRecord.binder);
                        }
                    }
            }
            handleRemoveListLocked();
        }
    }

    public void notifyDataConnection(int paramInt1, boolean paramBoolean1, String paramString1, String paramString2, String paramString3, LinkProperties paramLinkProperties, LinkCapabilities paramLinkCapabilities, int paramInt2, boolean paramBoolean2)
    {
        if (!checkNotifyPermission("notifyDataConnection()"));
        while (true)
        {
            return;
            ArrayList localArrayList = this.mRecords;
            int i = 0;
            if (paramInt1 == 2);
            while (true)
            {
                try
                {
                    if (!this.mConnectedApns.contains(paramString3))
                    {
                        this.mConnectedApns.add(paramString3);
                        if (this.mDataConnectionState != paramInt1)
                        {
                            this.mDataConnectionState = paramInt1;
                            i = 1;
                        }
                    }
                    this.mDataConnectionPossible = paramBoolean1;
                    this.mDataConnectionReason = paramString1;
                    this.mDataConnectionLinkProperties = paramLinkProperties;
                    this.mDataConnectionLinkCapabilities = paramLinkCapabilities;
                    if (this.mDataConnectionNetworkType != paramInt2)
                    {
                        this.mDataConnectionNetworkType = paramInt2;
                        i = 1;
                    }
                    if (i == 0)
                        break label241;
                    Iterator localIterator = this.mRecords.iterator();
                    if (!localIterator.hasNext())
                        break;
                    Record localRecord = (Record)localIterator.next();
                    int j = localRecord.events;
                    if ((j & 0x40) == 0)
                        continue;
                    try
                    {
                        localRecord.callback.onDataConnectionStateChanged(this.mDataConnectionState, this.mDataConnectionNetworkType);
                    }
                    catch (RemoteException localRemoteException)
                    {
                        this.mRemoveList.add(localRecord.binder);
                    }
                    continue;
                }
                finally
                {
                }
                if ((this.mConnectedApns.remove(paramString3)) && (this.mConnectedApns.isEmpty()))
                {
                    this.mDataConnectionState = paramInt1;
                    i = 1;
                }
            }
            handleRemoveListLocked();
            label241: broadcastDataConnectionStateChanged(paramInt1, paramBoolean1, paramString1, paramString2, paramString3, paramLinkProperties, paramLinkCapabilities, paramBoolean2);
        }
    }

    public void notifyDataConnectionFailed(String paramString1, String paramString2)
    {
        if (!checkNotifyPermission("notifyDataConnectionFailed()"));
        while (true)
        {
            return;
            broadcastDataConnectionFailed(paramString1, paramString2);
        }
    }

    public void notifyMessageWaitingChanged(boolean paramBoolean)
    {
        if (!checkNotifyPermission("notifyMessageWaitingChanged()"));
        while (true)
        {
            return;
            synchronized (this.mRecords)
            {
                this.mMessageWaiting = paramBoolean;
                Iterator localIterator = this.mRecords.iterator();
                while (true)
                    if (localIterator.hasNext())
                    {
                        Record localRecord = (Record)localIterator.next();
                        int i = localRecord.events;
                        if ((i & 0x4) == 0)
                            continue;
                        try
                        {
                            localRecord.callback.onMessageWaitingIndicatorChanged(paramBoolean);
                        }
                        catch (RemoteException localRemoteException)
                        {
                            this.mRemoveList.add(localRecord.binder);
                        }
                    }
            }
            handleRemoveListLocked();
        }
    }

    public void notifyOtaspChanged(int paramInt)
    {
        if (!checkNotifyPermission("notifyOtaspChanged()"));
        while (true)
        {
            return;
            synchronized (this.mRecords)
            {
                this.mOtaspMode = paramInt;
                Iterator localIterator = this.mRecords.iterator();
                while (true)
                    if (localIterator.hasNext())
                    {
                        Record localRecord = (Record)localIterator.next();
                        int i = localRecord.events;
                        if ((i & 0x200) == 0)
                            continue;
                        try
                        {
                            localRecord.callback.onOtaspChanged(paramInt);
                        }
                        catch (RemoteException localRemoteException)
                        {
                            this.mRemoveList.add(localRecord.binder);
                        }
                    }
            }
            handleRemoveListLocked();
        }
    }

    public void notifyServiceState(ServiceState paramServiceState)
    {
        if (!checkNotifyPermission("notifyServiceState()"));
        while (true)
        {
            return;
            synchronized (this.mRecords)
            {
                this.mServiceState = paramServiceState;
                Iterator localIterator = this.mRecords.iterator();
                while (true)
                    if (localIterator.hasNext())
                    {
                        Record localRecord = (Record)localIterator.next();
                        int i = localRecord.events;
                        if ((i & 0x1) == 0)
                            continue;
                        try
                        {
                            localRecord.callback.onServiceStateChanged(new ServiceState(paramServiceState));
                        }
                        catch (RemoteException localRemoteException)
                        {
                            this.mRemoveList.add(localRecord.binder);
                        }
                    }
            }
            handleRemoveListLocked();
            broadcastServiceStateChanged(paramServiceState);
        }
    }

    public void notifySignalStrength(SignalStrength paramSignalStrength)
    {
        if (!checkNotifyPermission("notifySignalStrength()"));
        while (true)
        {
            return;
            while (true)
            {
                Record localRecord;
                synchronized (this.mRecords)
                {
                    this.mSignalStrength = paramSignalStrength;
                    Iterator localIterator = this.mRecords.iterator();
                    if (!localIterator.hasNext())
                        break label179;
                    localRecord = (Record)localIterator.next();
                    int i = localRecord.events;
                    if ((i & 0x100) == 0);
                }
                try
                {
                    localRecord.callback.onSignalStrengthsChanged(new SignalStrength(paramSignalStrength));
                    int j = localRecord.events;
                    if ((j & 0x2) != 0)
                    {
                        try
                        {
                            int k = paramSignalStrength.getGsmSignalStrength();
                            IPhoneStateListener localIPhoneStateListener = localRecord.callback;
                            if (k == 99)
                                k = -1;
                            localIPhoneStateListener.onSignalStrengthChanged(k);
                        }
                        catch (RemoteException localRemoteException1)
                        {
                            this.mRemoveList.add(localRecord.binder);
                        }
                        continue;
                        localObject = finally;
                        throw localObject;
                    }
                }
                catch (RemoteException localRemoteException2)
                {
                    while (true)
                        this.mRemoveList.add(localRecord.binder);
                }
            }
            label179: handleRemoveListLocked();
            broadcastSignalStrengthChanged(paramSignalStrength);
        }
    }

    private static class Record
    {
        IBinder binder;
        IPhoneStateListener callback;
        int events;
        String pkgForDebug;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.TelephonyRegistry
 * JD-Core Version:        0.6.2
 */