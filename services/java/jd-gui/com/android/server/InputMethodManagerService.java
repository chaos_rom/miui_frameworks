package com.android.server;

import android.app.ActivityManagerNative;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.content.res.TypedArray;
import android.database.ContentObserver;
import android.os.Binder;
import android.os.Environment;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Message;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.text.TextUtils;
import android.text.TextUtils.SimpleStringSplitter;
import android.text.style.SuggestionSpan;
import android.util.EventLog;
import android.util.LruCache;
import android.util.Pair;
import android.util.Slog;
import android.util.Xml;
import android.view.IWindowManager;
import android.view.IWindowManager.Stub;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputBinding;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodSubtype;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import com.android.internal.R.styleable;
import com.android.internal.content.PackageMonitor;
import com.android.internal.os.AtomicFile;
import com.android.internal.os.HandlerCaller;
import com.android.internal.os.HandlerCaller.Callback;
import com.android.internal.os.HandlerCaller.SomeArgs;
import com.android.internal.util.FastXmlSerializer;
import com.android.internal.view.IInputContext;
import com.android.internal.view.IInputMethod;
import com.android.internal.view.IInputMethod.Stub;
import com.android.internal.view.IInputMethodCallback;
import com.android.internal.view.IInputMethodCallback.Stub;
import com.android.internal.view.IInputMethodClient;
import com.android.internal.view.IInputMethodManager.Stub;
import com.android.internal.view.IInputMethodSession;
import com.android.internal.view.InputBindResult;
import com.android.server.wm.WindowManagerService;
import com.android.server.wm.WindowManagerService.OnHardKeyboardStatusChangeListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeMap;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class InputMethodManagerService extends IInputMethodManager.Stub
    implements ServiceConnection, Handler.Callback
{
    static final boolean DEBUG = false;
    private static final Locale ENGLISH_LOCALE = new Locale("en");
    static final int MSG_ATTACH_TOKEN = 1040;
    static final int MSG_BIND_INPUT = 1010;
    static final int MSG_BIND_METHOD = 3010;
    static final int MSG_CREATE_SESSION = 1050;
    static final int MSG_HARD_KEYBOARD_SWITCH_CHANGED = 4000;
    static final int MSG_HIDE_SOFT_INPUT = 1030;
    static final int MSG_RESTART_INPUT = 2010;
    static final int MSG_SET_ACTIVE = 3020;
    static final int MSG_SHOW_IM_CONFIG = 4;
    static final int MSG_SHOW_IM_PICKER = 1;
    static final int MSG_SHOW_IM_SUBTYPE_ENABLER = 3;
    static final int MSG_SHOW_IM_SUBTYPE_PICKER = 2;
    static final int MSG_SHOW_SOFT_INPUT = 1020;
    static final int MSG_START_INPUT = 2000;
    static final int MSG_UNBIND_INPUT = 1000;
    static final int MSG_UNBIND_METHOD = 3000;
    private static final int NOT_A_SUBTYPE_ID = -1;
    private static final String NOT_A_SUBTYPE_ID_STR = String.valueOf(-1);
    static final int SECURE_SUGGESTION_SPANS_MAX_SIZE = 20;
    private static final String SUBTYPE_MODE_KEYBOARD = "keyboard";
    private static final String SUBTYPE_MODE_VOICE = "voice";
    static final String TAG = "InputMethodManagerService";
    private static final String TAG_ASCII_CAPABLE = "AsciiCapable";
    private static final String TAG_ENABLED_WHEN_DEFAULT_IS_NOT_ASCII_CAPABLE = "EnabledWhenDefaultIsNotAsciiCapable";
    private static final String TAG_TRY_SUPPRESSING_IME_SWITCHER = "TrySuppressingImeSwitcher";
    static final long TIME_TO_RECONNECT = 10000L;
    int mBackDisposition;
    boolean mBoundToMethod;
    final HandlerCaller mCaller;
    final HashMap<IBinder, ClientState> mClients = new HashMap();
    final Context mContext;
    EditorInfo mCurAttribute;
    ClientState mCurClient;
    IBinder mCurFocusedWindow;
    String mCurId;
    IInputContext mCurInputContext;
    Intent mCurIntent;
    IInputMethod mCurMethod;
    String mCurMethodId;
    int mCurSeq;
    IBinder mCurToken;
    private InputMethodSubtype mCurrentSubtype;
    private AlertDialog.Builder mDialogBuilder;
    SessionState mEnabledSession;
    private final InputMethodFileManager mFileManager;
    final Handler mHandler;
    private final HardKeyboardListener mHardKeyboardListener;
    boolean mHaveConnection;
    final IWindowManager mIWindowManager;
    private final InputMethodAndSubtypeListManager mImListManager;
    private final boolean mImeSelectedOnBoot;
    private PendingIntent mImeSwitchPendingIntent;
    private Notification mImeSwitcherNotification;
    int mImeWindowVis;
    private InputMethodInfo[] mIms;
    boolean mInputShown;
    private KeyguardManager mKeyguardManager;
    long mLastBindTime;
    private Locale mLastSystemLocale;
    final ArrayList<InputMethodInfo> mMethodList = new ArrayList();
    final HashMap<String, InputMethodInfo> mMethodMap = new HashMap();
    final InputBindResult mNoBinding = new InputBindResult(null, null, -1);
    private NotificationManager mNotificationManager;
    private boolean mNotificationShown;
    final Resources mRes;
    boolean mScreenOn;
    private final LruCache<SuggestionSpan, InputMethodInfo> mSecureSuggestionSpans = new LruCache(20);
    final InputMethodSettings mSettings;
    final SettingsObserver mSettingsObserver;
    private final HashMap<InputMethodInfo, ArrayList<InputMethodSubtype>> mShortcutInputMethodsAndSubtypes = new HashMap();
    boolean mShowExplicitlyRequested;
    boolean mShowForced;
    private boolean mShowOngoingImeSwitcherForPhones;
    boolean mShowRequested;
    private StatusBarManagerService mStatusBar;
    private int[] mSubtypeIds;
    private AlertDialog mSwitchingDialog;
    private View mSwitchingDialogTitleView;
    boolean mSystemReady;
    boolean mVisibleBound = false;
    final ServiceConnection mVisibleConnection = new ServiceConnection()
    {
        public void onServiceConnected(ComponentName paramAnonymousComponentName, IBinder paramAnonymousIBinder)
        {
        }

        public void onServiceDisconnected(ComponentName paramAnonymousComponentName)
        {
        }
    };
    private final WindowManagerService mWindowManagerService;

    public InputMethodManagerService(Context paramContext, WindowManagerService paramWindowManagerService)
    {
        this.mScreenOn = bool;
        this.mBackDisposition = 0;
        this.mContext = paramContext;
        this.mRes = paramContext.getResources();
        this.mHandler = new Handler(this);
        this.mIWindowManager = IWindowManager.Stub.asInterface(ServiceManager.getService("window"));
        this.mCaller = new HandlerCaller(paramContext, new HandlerCaller.Callback()
        {
            public void executeMessage(Message paramAnonymousMessage)
            {
                InputMethodManagerService.this.handleMessage(paramAnonymousMessage);
            }
        });
        this.mWindowManagerService = paramWindowManagerService;
        this.mHardKeyboardListener = new HardKeyboardListener(null);
        this.mImeSwitcherNotification = new Notification();
        this.mImeSwitcherNotification.icon = 17302351;
        this.mImeSwitcherNotification.when = 0L;
        this.mImeSwitcherNotification.flags = 2;
        this.mImeSwitcherNotification.tickerText = null;
        this.mImeSwitcherNotification.defaults = 0;
        this.mImeSwitcherNotification.sound = null;
        this.mImeSwitcherNotification.vibrate = null;
        Notification localNotification = this.mImeSwitcherNotification;
        String[] arrayOfString = new String[bool];
        arrayOfString[0] = "android.system.imeswitcher";
        localNotification.kind = arrayOfString;
        Intent localIntent = new Intent("android.settings.SHOW_INPUT_METHOD_PICKER");
        this.mImeSwitchPendingIntent = PendingIntent.getBroadcast(this.mContext, 0, localIntent, 0);
        this.mShowOngoingImeSwitcherForPhones = false;
        while (true)
        {
            synchronized (this.mMethodMap)
            {
                this.mFileManager = new InputMethodFileManager(this.mMethodMap);
                this.mImListManager = new InputMethodAndSubtypeListManager(paramContext, this);
                new MyPackageMonitor().register(this.mContext, null, bool);
                IntentFilter localIntentFilter1 = new IntentFilter();
                localIntentFilter1.addAction("android.intent.action.SCREEN_ON");
                localIntentFilter1.addAction("android.intent.action.SCREEN_OFF");
                localIntentFilter1.addAction("android.intent.action.CLOSE_SYSTEM_DIALOGS");
                this.mContext.registerReceiver(new ScreenOnOffReceiver(), localIntentFilter1);
                this.mNotificationShown = false;
                this.mSettings = new InputMethodSettings(this.mRes, paramContext.getContentResolver(), this.mMethodMap, this.mMethodList);
                if (!TextUtils.isEmpty(Settings.Secure.getString(this.mContext.getContentResolver(), "default_input_method")))
                {
                    this.mImeSelectedOnBoot = bool;
                    buildInputMethodListLocked(this.mMethodList, this.mMethodMap);
                    this.mSettings.enableAllIMEsIfThereIsNoEnabledIME();
                    if (!this.mImeSelectedOnBoot)
                    {
                        Slog.w("InputMethodManagerService", "No IME selected. Choose the most applicable IME.");
                        resetDefaultImeLocked(paramContext);
                    }
                    this.mSettingsObserver = new SettingsObserver(this.mHandler);
                    updateFromSettingsLocked();
                    IntentFilter localIntentFilter2 = new IntentFilter();
                    localIntentFilter2.addAction("android.intent.action.LOCALE_CHANGED");
                    this.mContext.registerReceiver(new BroadcastReceiver()
                    {
                        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
                        {
                            synchronized (InputMethodManagerService.this.mMethodMap)
                            {
                                InputMethodManagerService.this.checkCurrentLocaleChangedLocked();
                                return;
                            }
                        }
                    }
                    , localIntentFilter2);
                    return;
                }
            }
            bool = false;
        }
    }

    private void addShortcutInputMethodAndSubtypes(InputMethodInfo paramInputMethodInfo, InputMethodSubtype paramInputMethodSubtype)
    {
        if (this.mShortcutInputMethodsAndSubtypes.containsKey(paramInputMethodInfo))
            ((ArrayList)this.mShortcutInputMethodsAndSubtypes.get(paramInputMethodInfo)).add(paramInputMethodSubtype);
        while (true)
        {
            return;
            ArrayList localArrayList = new ArrayList();
            localArrayList.add(paramInputMethodSubtype);
            this.mShortcutInputMethodsAndSubtypes.put(paramInputMethodInfo, localArrayList);
        }
    }

    private boolean canAddToLastInputMethod(InputMethodSubtype paramInputMethodSubtype)
    {
        boolean bool = true;
        if (paramInputMethodSubtype == null);
        while (true)
        {
            return bool;
            if (paramInputMethodSubtype.isAuxiliary())
                bool = false;
        }
    }

    private void checkCurrentLocaleChangedLocked()
    {
        if (!this.mSystemReady);
        while (true)
        {
            return;
            Locale localLocale = this.mRes.getConfiguration().locale;
            if ((localLocale != null) && (!localLocale.equals(this.mLastSystemLocale)))
            {
                buildInputMethodListLocked(this.mMethodList, this.mMethodMap);
                resetDefaultImeLocked(this.mContext);
                updateFromSettingsLocked();
                this.mLastSystemLocale = localLocale;
            }
        }
    }

    private boolean chooseNewDefaultIMELocked()
    {
        InputMethodInfo localInputMethodInfo = getMostApplicableDefaultIMELocked();
        if (localInputMethodInfo != null)
            resetSelectedInputMethodAndSubtypeLocked(localInputMethodInfo.getId());
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean containsSubtypeOf(InputMethodInfo paramInputMethodInfo, String paramString)
    {
        int i = paramInputMethodInfo.getSubtypeCount();
        int j = 0;
        if (j < i)
            if (!paramInputMethodInfo.getSubtypeAt(j).getLocale().startsWith(paramString));
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            j++;
            break;
        }
    }

    private Pair<InputMethodInfo, InputMethodSubtype> findLastResortApplicableShortcutInputMethodAndSubtypeLocked(String paramString)
    {
        List localList1 = this.mSettings.getEnabledInputMethodListLocked();
        Object localObject1 = null;
        Object localObject2 = null;
        int i = 0;
        Iterator localIterator = localList1.iterator();
        InputMethodInfo localInputMethodInfo;
        InputMethodSubtype localInputMethodSubtype;
        ArrayList localArrayList1;
        ArrayList localArrayList2;
        while (localIterator.hasNext())
        {
            localInputMethodInfo = (InputMethodInfo)localIterator.next();
            String str = localInputMethodInfo.getId();
            if ((i == 0) || (str.equals(this.mCurMethodId)))
            {
                localInputMethodSubtype = null;
                List localList2 = getEnabledInputMethodSubtypeList(localInputMethodInfo, true);
                if (this.mCurrentSubtype != null)
                    localInputMethodSubtype = findLastResortApplicableSubtypeLocked(this.mRes, localList2, paramString, this.mCurrentSubtype.getLocale(), false);
                if (localInputMethodSubtype == null)
                    localInputMethodSubtype = findLastResortApplicableSubtypeLocked(this.mRes, localList2, paramString, null, true);
                localArrayList1 = getOverridingImplicitlyEnabledSubtypes(localInputMethodInfo, paramString);
                if (!localArrayList1.isEmpty())
                    break label245;
                localArrayList2 = getSubtypes(localInputMethodInfo);
                label151: if ((localInputMethodSubtype == null) && (this.mCurrentSubtype != null))
                    localInputMethodSubtype = findLastResortApplicableSubtypeLocked(this.mRes, localArrayList2, paramString, this.mCurrentSubtype.getLocale(), false);
                if (localInputMethodSubtype == null)
                    localInputMethodSubtype = findLastResortApplicableSubtypeLocked(this.mRes, localArrayList2, paramString, null, true);
                if (localInputMethodSubtype != null)
                {
                    if (!str.equals(this.mCurMethodId))
                        break label252;
                    localObject1 = localInputMethodInfo;
                    localObject2 = localInputMethodSubtype;
                }
            }
        }
        if (localObject1 != null);
        for (Pair localPair = new Pair(localObject1, localObject2); ; localPair = null)
        {
            return localPair;
            label245: localArrayList2 = localArrayList1;
            break label151;
            label252: if (i != 0)
                break;
            localObject1 = localInputMethodInfo;
            localObject2 = localInputMethodSubtype;
            if ((0x1 & localInputMethodInfo.getServiceInfo().applicationInfo.flags) == 0)
                break;
            i = 1;
            break;
        }
    }

    private static InputMethodSubtype findLastResortApplicableSubtypeLocked(Resources paramResources, List<InputMethodSubtype> paramList, String paramString1, String paramString2, boolean paramBoolean)
    {
        if ((paramList == null) || (paramList.size() == 0))
        {
            localObject1 = null;
            return localObject1;
        }
        if (TextUtils.isEmpty(paramString2))
            paramString2 = paramResources.getConfiguration().locale.toString();
        String str1 = paramString2.substring(0, 2);
        int i = 0;
        Object localObject2 = null;
        Object localObject1 = null;
        int j = paramList.size();
        for (int k = 0; ; k++)
        {
            InputMethodSubtype localInputMethodSubtype;
            String str2;
            if (k < j)
            {
                localInputMethodSubtype = (InputMethodSubtype)paramList.get(k);
                str2 = localInputMethodSubtype.getLocale();
                if ((paramString1 != null) && (!((InputMethodSubtype)paramList.get(k)).getMode().equalsIgnoreCase(paramString1)))
                    continue;
                if (localObject1 == null)
                    localObject1 = localInputMethodSubtype;
                if (paramString2.equals(str2))
                    localObject2 = localInputMethodSubtype;
            }
            else
            {
                if ((localObject2 == null) && (paramBoolean))
                    break;
                localObject1 = localObject2;
                break;
            }
            if ((i == 0) && (str2.startsWith(str1)))
            {
                localObject2 = localInputMethodSubtype;
                i = 1;
            }
        }
    }

    private void finishSession(SessionState paramSessionState)
    {
        if ((paramSessionState != null) && (paramSessionState.session != null));
        try
        {
            paramSessionState.session.finishSession();
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Slog.w("InputMethodManagerService", "Session failed to close due to remote exception", localRemoteException);
                setImeWindowVisibilityStatusHiddenLocked();
            }
        }
    }

    private int getAppShowFlags()
    {
        int i = 0;
        if (this.mShowForced)
            i = 0x0 | 0x2;
        while (true)
        {
            return i;
            if (!this.mShowExplicitlyRequested)
                i = 0x0 | 0x1;
        }
    }

    private HashMap<InputMethodInfo, List<InputMethodSubtype>> getExplicitlyOrImplicitlyEnabledInputMethodsAndSubtypeListLocked()
    {
        HashMap localHashMap = new HashMap();
        Iterator localIterator = getEnabledInputMethodList().iterator();
        while (localIterator.hasNext())
        {
            InputMethodInfo localInputMethodInfo = (InputMethodInfo)localIterator.next();
            localHashMap.put(localInputMethodInfo, getEnabledInputMethodSubtypeListLocked(localInputMethodInfo, true));
        }
        return localHashMap;
    }

    private int getImeShowFlags()
    {
        int i = 0;
        if (this.mShowForced)
            i = 0x0 | 0x3;
        while (true)
        {
            return i;
            if (this.mShowExplicitlyRequested)
                i = 0x0 | 0x1;
        }
    }

    private static ArrayList<InputMethodSubtype> getImplicitlyApplicableSubtypesLocked(Resources paramResources, InputMethodInfo paramInputMethodInfo)
    {
        ArrayList localArrayList1 = getSubtypes(paramInputMethodInfo);
        String str1 = paramResources.getConfiguration().locale.toString();
        ArrayList localArrayList2;
        if (TextUtils.isEmpty(str1))
            localArrayList2 = new ArrayList();
        while (true)
        {
            return localArrayList2;
            HashMap localHashMap = new HashMap();
            int i = localArrayList1.size();
            for (int j = 0; j < i; j++)
            {
                InputMethodSubtype localInputMethodSubtype6 = (InputMethodSubtype)localArrayList1.get(j);
                if (localInputMethodSubtype6.overridesImplicitlyEnabledSubtype())
                {
                    String str4 = localInputMethodSubtype6.getMode();
                    if (!localHashMap.containsKey(str4))
                        localHashMap.put(str4, localInputMethodSubtype6);
                }
            }
            if (localHashMap.size() > 0)
            {
                localArrayList2 = new ArrayList(localHashMap.values());
            }
            else
            {
                int k = 0;
                if (k < i)
                {
                    InputMethodSubtype localInputMethodSubtype4 = (InputMethodSubtype)localArrayList1.get(k);
                    String str2 = localInputMethodSubtype4.getLocale();
                    String str3 = localInputMethodSubtype4.getMode();
                    if (str1.startsWith(str2))
                    {
                        InputMethodSubtype localInputMethodSubtype5 = (InputMethodSubtype)localHashMap.get(str3);
                        if (localInputMethodSubtype5 == null)
                            break label231;
                        if (!str1.equals(localInputMethodSubtype5.getLocale()))
                            break label222;
                    }
                    while (true)
                    {
                        k++;
                        break;
                        label222: if (str1.equals(str2))
                            label231: localHashMap.put(str3, localInputMethodSubtype4);
                    }
                }
                InputMethodSubtype localInputMethodSubtype1 = (InputMethodSubtype)localHashMap.get("keyboard");
                localArrayList2 = new ArrayList(localHashMap.values());
                if ((localInputMethodSubtype1 != null) && (!localInputMethodSubtype1.containsExtraValueKey("AsciiCapable")))
                    for (int m = 0; m < i; m++)
                    {
                        InputMethodSubtype localInputMethodSubtype3 = (InputMethodSubtype)localArrayList1.get(m);
                        if (("keyboard".equals(localInputMethodSubtype3.getMode())) && (localInputMethodSubtype3.containsExtraValueKey("EnabledWhenDefaultIsNotAsciiCapable")))
                            localArrayList2.add(localInputMethodSubtype3);
                    }
                if (localInputMethodSubtype1 == null)
                {
                    InputMethodSubtype localInputMethodSubtype2 = findLastResortApplicableSubtypeLocked(paramResources, localArrayList1, "keyboard", str1, true);
                    if (localInputMethodSubtype2 != null)
                        localArrayList2.add(localInputMethodSubtype2);
                }
            }
        }
    }

    private InputMethodInfo getMostApplicableDefaultIMELocked()
    {
        List localList = this.mSettings.getEnabledInputMethodListLocked();
        int i;
        int j;
        InputMethodInfo localInputMethodInfo;
        if ((localList != null) && (localList.size() > 0))
        {
            i = localList.size();
            j = -1;
            if (i > 0)
            {
                i--;
                localInputMethodInfo = (InputMethodInfo)localList.get(i);
                if ((!isSystemImeThatHasEnglishSubtype(localInputMethodInfo)) || (localInputMethodInfo.isAuxiliaryIme()));
            }
        }
        while (true)
        {
            return localInputMethodInfo;
            if ((j >= 0) || (!isSystemIme(localInputMethodInfo)) || (localInputMethodInfo.isAuxiliaryIme()))
                break;
            j = i;
            break;
            localInputMethodInfo = (InputMethodInfo)localList.get(Math.max(j, 0));
            continue;
            localInputMethodInfo = null;
        }
    }

    private static ArrayList<InputMethodSubtype> getOverridingImplicitlyEnabledSubtypes(InputMethodInfo paramInputMethodInfo, String paramString)
    {
        ArrayList localArrayList = new ArrayList();
        int i = paramInputMethodInfo.getSubtypeCount();
        for (int j = 0; j < i; j++)
        {
            InputMethodSubtype localInputMethodSubtype = paramInputMethodInfo.getSubtypeAt(j);
            if ((localInputMethodSubtype.overridesImplicitlyEnabledSubtype()) && (localInputMethodSubtype.getMode().equals(paramString)))
                localArrayList.add(localInputMethodSubtype);
        }
        return localArrayList;
    }

    private int getSelectedInputMethodSubtypeId(String paramString)
    {
        int i = -1;
        InputMethodInfo localInputMethodInfo = (InputMethodInfo)this.mMethodMap.get(paramString);
        if (localInputMethodInfo == null);
        while (true)
        {
            return i;
            try
            {
                int j = Settings.Secure.getInt(this.mContext.getContentResolver(), "selected_input_method_subtype");
                i = getSubtypeIdFromHashCode(localInputMethodInfo, j);
            }
            catch (Settings.SettingNotFoundException localSettingNotFoundException)
            {
            }
        }
    }

    private static int getSubtypeIdFromHashCode(InputMethodInfo paramInputMethodInfo, int paramInt)
    {
        int i;
        if (paramInputMethodInfo != null)
        {
            int j = paramInputMethodInfo.getSubtypeCount();
            i = 0;
            if (i < j)
                if (paramInt != paramInputMethodInfo.getSubtypeAt(i).hashCode());
        }
        while (true)
        {
            return i;
            i++;
            break;
            i = -1;
        }
    }

    private static ArrayList<InputMethodSubtype> getSubtypes(InputMethodInfo paramInputMethodInfo)
    {
        ArrayList localArrayList = new ArrayList();
        int i = paramInputMethodInfo.getSubtypeCount();
        for (int j = 0; j < i; j++)
            localArrayList.add(paramInputMethodInfo.getSubtypeAt(j));
        return localArrayList;
    }

    private boolean isScreenLocked()
    {
        if ((this.mKeyguardManager != null) && (this.mKeyguardManager.isKeyguardLocked()) && (this.mKeyguardManager.isKeyguardSecure()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isSystemIme(InputMethodInfo paramInputMethodInfo)
    {
        if ((0x1 & paramInputMethodInfo.getServiceInfo().applicationInfo.flags) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isSystemImeThatHasEnglishSubtype(InputMethodInfo paramInputMethodInfo)
    {
        if (!isSystemIme(paramInputMethodInfo));
        for (boolean bool = false; ; bool = containsSubtypeOf(paramInputMethodInfo, ENGLISH_LOCALE.getLanguage()))
            return bool;
    }

    private static boolean isValidSubtypeId(InputMethodInfo paramInputMethodInfo, int paramInt)
    {
        if (getSubtypeIdFromHashCode(paramInputMethodInfo, paramInt) != -1);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isValidSystemDefaultIme(InputMethodInfo paramInputMethodInfo, Context paramContext)
    {
        boolean bool1 = false;
        if (!this.mSystemReady);
        while (true)
        {
            return bool1;
            if (!isSystemIme(paramInputMethodInfo))
                continue;
            if (paramInputMethodInfo.getIsDefaultResourceId() != 0);
            try
            {
                if (paramContext.createPackageContext(paramInputMethodInfo.getPackageName(), 0).getResources().getBoolean(paramInputMethodInfo.getIsDefaultResourceId()))
                {
                    boolean bool2 = containsSubtypeOf(paramInputMethodInfo, paramContext.getResources().getConfiguration().locale.getLanguage());
                    if (bool2)
                        bool1 = true;
                }
            }
            catch (Resources.NotFoundException localNotFoundException)
            {
                if (paramInputMethodInfo.getSubtypeCount() != 0)
                    continue;
                Slog.w("InputMethodManagerService", "Found no subtypes in a system IME: " + paramInputMethodInfo.getPackageName());
            }
            catch (PackageManager.NameNotFoundException localNameNotFoundException)
            {
                label78: break label78;
            }
        }
    }

    // ERROR //
    private boolean needsToShowImeSwitchOngoingNotification()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 380	com/android/server/InputMethodManagerService:mShowOngoingImeSwitcherForPhones	Z
        //     4: ifne +9 -> 13
        //     7: iconst_0
        //     8: istore 10
        //     10: iload 10
        //     12: ireturn
        //     13: aload_0
        //     14: invokespecial 810	com/android/server/InputMethodManagerService:isScreenLocked	()Z
        //     17: ifeq +9 -> 26
        //     20: iconst_0
        //     21: istore 10
        //     23: goto -13 -> 10
        //     26: aload_0
        //     27: getfield 259	com/android/server/InputMethodManagerService:mMethodMap	Ljava/util/HashMap;
        //     30: astore_1
        //     31: aload_1
        //     32: monitorenter
        //     33: aload_0
        //     34: getfield 423	com/android/server/InputMethodManagerService:mSettings	Lcom/android/server/InputMethodManagerService$InputMethodSettings;
        //     37: invokevirtual 618	com/android/server/InputMethodManagerService$InputMethodSettings:getEnabledInputMethodListLocked	()Ljava/util/List;
        //     40: astore_3
        //     41: aload_3
        //     42: invokeinterface 673 1 0
        //     47: istore 4
        //     49: iload 4
        //     51: iconst_2
        //     52: if_icmple +16 -> 68
        //     55: iconst_1
        //     56: istore 10
        //     58: aload_1
        //     59: monitorexit
        //     60: goto -50 -> 10
        //     63: astore_2
        //     64: aload_1
        //     65: monitorexit
        //     66: aload_2
        //     67: athrow
        //     68: iload 4
        //     70: iconst_1
        //     71: if_icmpge +191 -> 262
        //     74: iconst_0
        //     75: istore 10
        //     77: aload_1
        //     78: monitorexit
        //     79: goto -69 -> 10
        //     82: iload 9
        //     84: iload 4
        //     86: if_icmpge +222 -> 308
        //     89: aload_0
        //     90: aload_3
        //     91: iload 9
        //     93: invokeinterface 683 2 0
        //     98: checkcast 592	android/view/inputmethod/InputMethodInfo
        //     101: iconst_1
        //     102: invokevirtual 717	com/android/server/InputMethodManagerService:getEnabledInputMethodSubtypeListLocked	(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;
        //     105: astore 11
        //     107: aload 11
        //     109: invokeinterface 673 1 0
        //     114: istore 12
        //     116: iload 12
        //     118: ifne +168 -> 286
        //     121: iinc 5 1
        //     124: goto +156 -> 280
        //     127: iload 13
        //     129: iload 12
        //     131: if_icmpge +149 -> 280
        //     134: aload 11
        //     136: iload 13
        //     138: invokeinterface 683 2 0
        //     143: checkcast 565	android/view/inputmethod/InputMethodSubtype
        //     146: astore 14
        //     148: aload 14
        //     150: invokevirtual 568	android/view/inputmethod/InputMethodSubtype:isAuxiliary	()Z
        //     153: ifne +145 -> 298
        //     156: iinc 5 1
        //     159: aload 14
        //     161: astore 7
        //     163: goto +129 -> 292
        //     166: iconst_1
        //     167: istore 10
        //     169: aload_1
        //     170: monitorexit
        //     171: goto -161 -> 10
        //     174: iload 5
        //     176: iconst_1
        //     177: if_icmpne +77 -> 254
        //     180: iload 6
        //     182: iconst_1
        //     183: if_icmpne +71 -> 254
        //     186: aload 7
        //     188: ifnull +58 -> 246
        //     191: aload 8
        //     193: ifnull +53 -> 246
        //     196: aload 7
        //     198: invokevirtual 608	android/view/inputmethod/InputMethodSubtype:getLocale	()Ljava/lang/String;
        //     201: aload 8
        //     203: invokevirtual 608	android/view/inputmethod/InputMethodSubtype:getLocale	()Ljava/lang/String;
        //     206: invokevirtual 636	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     209: ifne +19 -> 228
        //     212: aload 8
        //     214: invokevirtual 721	android/view/inputmethod/InputMethodSubtype:overridesImplicitlyEnabledSubtype	()Z
        //     217: ifne +11 -> 228
        //     220: aload 7
        //     222: invokevirtual 721	android/view/inputmethod/InputMethodSubtype:overridesImplicitlyEnabledSubtype	()Z
        //     225: ifeq +21 -> 246
        //     228: aload 7
        //     230: ldc 120
        //     232: invokevirtual 732	android/view/inputmethod/InputMethodSubtype:containsExtraValueKey	(Ljava/lang/String;)Z
        //     235: ifeq +11 -> 246
        //     238: iconst_0
        //     239: istore 10
        //     241: aload_1
        //     242: monitorexit
        //     243: goto -233 -> 10
        //     246: iconst_1
        //     247: istore 10
        //     249: aload_1
        //     250: monitorexit
        //     251: goto -241 -> 10
        //     254: iconst_0
        //     255: istore 10
        //     257: aload_1
        //     258: monitorexit
        //     259: goto -249 -> 10
        //     262: iconst_0
        //     263: istore 5
        //     265: iconst_0
        //     266: istore 6
        //     268: aconst_null
        //     269: astore 7
        //     271: aconst_null
        //     272: astore 8
        //     274: iconst_0
        //     275: istore 9
        //     277: goto -195 -> 82
        //     280: iinc 9 1
        //     283: goto -201 -> 82
        //     286: iconst_0
        //     287: istore 13
        //     289: goto -162 -> 127
        //     292: iinc 13 1
        //     295: goto -168 -> 127
        //     298: iinc 6 1
        //     301: aload 14
        //     303: astore 8
        //     305: goto -13 -> 292
        //     308: iload 5
        //     310: iconst_1
        //     311: if_icmpgt -145 -> 166
        //     314: iload 6
        //     316: iconst_1
        //     317: if_icmple -143 -> 174
        //     320: goto -154 -> 166
        //
        // Exception table:
        //     from	to	target	type
        //     33	66	63	finally
        //     77	259	63	finally
    }

    private void refreshImeWindowVisibilityLocked()
    {
        int i = 0;
        Configuration localConfiguration = this.mRes.getConfiguration();
        int j;
        int k;
        if (localConfiguration.keyboard != 1)
        {
            j = 1;
            if ((j == 0) || (localConfiguration.hardKeyboardHidden == 2))
                break label99;
            k = 1;
            label35: if ((this.mKeyguardManager == null) || (!this.mKeyguardManager.isKeyguardLocked()) || (!this.mKeyguardManager.isKeyguardSecure()))
                break label105;
        }
        label99: label105: for (int m = 1; ; m = 0)
        {
            if ((m == 0) && ((this.mInputShown) || (k != 0)))
                i = 3;
            this.mImeWindowVis = i;
            updateImeWindowStatusLocked();
            return;
            j = 0;
            break;
            k = 0;
            break label35;
        }
    }

    private void resetDefaultImeLocked(Context paramContext)
    {
        if ((this.mCurMethodId != null) && (!isSystemIme((InputMethodInfo)this.mMethodMap.get(this.mCurMethodId))));
        while (true)
        {
            return;
            Object localObject = null;
            Iterator localIterator = this.mMethodList.iterator();
            while (localIterator.hasNext())
            {
                InputMethodInfo localInputMethodInfo = (InputMethodInfo)localIterator.next();
                if ((localObject == null) && (isValidSystemDefaultIme(localInputMethodInfo, paramContext)))
                {
                    localObject = localInputMethodInfo;
                    Slog.i("InputMethodManagerService", "Selected default: " + localInputMethodInfo.getId());
                }
            }
            if ((localObject == null) && (this.mMethodList.size() > 0))
            {
                localObject = getMostApplicableDefaultIMELocked();
                Slog.i("InputMethodManagerService", "No default found, using " + ((InputMethodInfo)localObject).getId());
            }
            if (localObject != null)
                setSelectedInputMethodAndSubtypeLocked((InputMethodInfo)localObject, -1, false);
        }
    }

    private void resetSelectedInputMethodAndSubtypeLocked(String paramString)
    {
        InputMethodInfo localInputMethodInfo = (InputMethodInfo)this.mMethodMap.get(paramString);
        int i = -1;
        String str;
        if ((localInputMethodInfo != null) && (!TextUtils.isEmpty(paramString)))
        {
            str = this.mSettings.getLastSubtypeForInputMethodLocked(paramString);
            if (str == null);
        }
        try
        {
            int j = getSubtypeIdFromHashCode(localInputMethodInfo, Integer.valueOf(str).intValue());
            i = j;
            setSelectedInputMethodAndSubtypeLocked(localInputMethodInfo, i, false);
            return;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            while (true)
                Slog.w("InputMethodManagerService", "HashCode for subtype looks broken: " + str, localNumberFormatException);
        }
    }

    private void saveCurrentInputMethodAndSubtypeToHistory()
    {
        String str = NOT_A_SUBTYPE_ID_STR;
        if (this.mCurrentSubtype != null)
            str = String.valueOf(this.mCurrentSubtype.hashCode());
        if (canAddToLastInputMethod(this.mCurrentSubtype))
            this.mSettings.addSubtypeToHistory(this.mCurMethodId, str);
    }

    private void setImeWindowVisibilityStatusHiddenLocked()
    {
        this.mImeWindowVis = 0;
        updateImeWindowStatusLocked();
    }

    // ERROR //
    private void setInputMethodWithSubtypeId(IBinder paramIBinder, String paramString, int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 259	com/android/server/InputMethodManagerService:mMethodMap	Ljava/util/HashMap;
        //     4: astore 4
        //     6: aload 4
        //     8: monitorenter
        //     9: aload_1
        //     10: ifnonnull +35 -> 45
        //     13: aload_0
        //     14: getfield 283	com/android/server/InputMethodManagerService:mContext	Landroid/content/Context;
        //     17: ldc_w 864
        //     20: invokevirtual 867	android/content/Context:checkCallingOrSelfPermission	(Ljava/lang/String;)I
        //     23: ifeq +74 -> 97
        //     26: new 869	java/lang/SecurityException
        //     29: dup
        //     30: ldc_w 871
        //     33: invokespecial 872	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     36: athrow
        //     37: astore 8
        //     39: aload 4
        //     41: monitorexit
        //     42: aload 8
        //     44: athrow
        //     45: aload_0
        //     46: getfield 874	com/android/server/InputMethodManagerService:mCurToken	Landroid/os/IBinder;
        //     49: aload_1
        //     50: if_acmpeq +47 -> 97
        //     53: ldc 111
        //     55: new 799	java/lang/StringBuilder
        //     58: dup
        //     59: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     62: ldc_w 876
        //     65: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     68: invokestatic 881	android/os/Binder:getCallingUid	()I
        //     71: invokevirtual 884	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     74: ldc_w 886
        //     77: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     80: aload_1
        //     81: invokevirtual 889	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     84: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     87: invokestatic 454	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     90: pop
        //     91: aload 4
        //     93: monitorexit
        //     94: goto +35 -> 129
        //     97: invokestatic 893	android/os/Binder:clearCallingIdentity	()J
        //     100: lstore 5
        //     102: aload_0
        //     103: aload_2
        //     104: iload_3
        //     105: invokevirtual 897	com/android/server/InputMethodManagerService:setInputMethodLocked	(Ljava/lang/String;I)V
        //     108: lload 5
        //     110: invokestatic 901	android/os/Binder:restoreCallingIdentity	(J)V
        //     113: aload 4
        //     115: monitorexit
        //     116: goto +13 -> 129
        //     119: astore 7
        //     121: lload 5
        //     123: invokestatic 901	android/os/Binder:restoreCallingIdentity	(J)V
        //     126: aload 7
        //     128: athrow
        //     129: return
        //
        // Exception table:
        //     from	to	target	type
        //     13	42	37	finally
        //     45	102	37	finally
        //     108	129	37	finally
        //     102	108	119	finally
    }

    private void setSelectedInputMethodAndSubtypeLocked(InputMethodInfo paramInputMethodInfo, int paramInt, boolean paramBoolean)
    {
        saveCurrentInputMethodAndSubtypeToHistory();
        InputMethodSettings localInputMethodSettings;
        if ((paramInputMethodInfo == null) || (paramInt < 0))
        {
            this.mSettings.putSelectedSubtype(-1);
            this.mCurrentSubtype = null;
            if ((this.mSystemReady) && (!paramBoolean))
            {
                localInputMethodSettings = this.mSettings;
                if (paramInputMethodInfo == null)
                    break label117;
            }
        }
        label117: for (String str = paramInputMethodInfo.getId(); ; str = "")
        {
            localInputMethodSettings.putSelectedInputMethod(str);
            return;
            if (paramInt < paramInputMethodInfo.getSubtypeCount())
            {
                InputMethodSubtype localInputMethodSubtype = paramInputMethodInfo.getSubtypeAt(paramInt);
                this.mSettings.putSelectedSubtype(localInputMethodSubtype.hashCode());
                this.mCurrentSubtype = localInputMethodSubtype;
                break;
            }
            this.mSettings.putSelectedSubtype(-1);
            this.mCurrentSubtype = getCurrentInputMethodSubtype();
            break;
        }
    }

    private void showConfigureInputMethods()
    {
        Intent localIntent = new Intent("android.settings.INPUT_METHOD_SETTINGS");
        localIntent.setFlags(337641472);
        this.mContext.startActivity(localIntent);
    }

    private void showInputMethodAndSubtypeEnabler(String paramString)
    {
        Intent localIntent = new Intent("android.settings.INPUT_METHOD_SUBTYPE_SETTINGS");
        localIntent.setFlags(337641472);
        if (!TextUtils.isEmpty(paramString))
            localIntent.putExtra("input_method_id", paramString);
        this.mContext.startActivity(localIntent);
    }

    private void showInputMethodMenu()
    {
        showInputMethodMenuInternal(false);
    }

    private void showInputMethodMenuInternal(boolean paramBoolean)
    {
        Context localContext = this.mContext;
        localContext.getPackageManager();
        boolean bool = isScreenLocked();
        String str = Settings.Secure.getString(localContext.getContentResolver(), "default_input_method");
        int i = getSelectedInputMethodSubtypeId(str);
        while (true)
        {
            int k;
            int m;
            int i1;
            synchronized (this.mMethodMap)
            {
                HashMap localHashMap2 = getExplicitlyOrImplicitlyEnabledInputMethodsAndSubtypeListLocked();
                if ((localHashMap2 != null) && (localHashMap2.size() == 0))
                    break label588;
                hideInputMethodMenuLocked();
                List localList = this.mImListManager.getSortedInputMethodAndSubtypeList(paramBoolean, this.mInputShown, bool);
                if (i == -1)
                {
                    InputMethodSubtype localInputMethodSubtype = getCurrentInputMethodSubtype();
                    if (localInputMethodSubtype != null)
                        i = getSubtypeIdFromHashCode((InputMethodInfo)this.mMethodMap.get(this.mCurMethodId), localInputMethodSubtype.hashCode());
                }
                int j = localList.size();
                this.mIms = new InputMethodInfo[j];
                this.mSubtypeIds = new int[j];
                k = 0;
                m = 0;
                if (m < j)
                {
                    ImeSubtypeListItem localImeSubtypeListItem = (ImeSubtypeListItem)localList.get(m);
                    this.mIms[m] = localImeSubtypeListItem.mImi;
                    this.mSubtypeIds[m] = localImeSubtypeListItem.mSubtypeId;
                    if (!this.mIms[m].getId().equals(str))
                        break label600;
                    i1 = this.mSubtypeIds[m];
                    if (i1 == -1)
                        break label596;
                    if (i != -1)
                        break label589;
                    if (i1 == 0)
                        break label596;
                    break label589;
                }
                TypedArray localTypedArray = localContext.obtainStyledAttributes(null, R.styleable.DialogPreference, 16842845, 0);
                AlertDialog.Builder localBuilder1 = new AlertDialog.Builder(localContext);
                DialogInterface.OnCancelListener local4 = new DialogInterface.OnCancelListener()
                {
                    public void onCancel(DialogInterface paramAnonymousDialogInterface)
                    {
                        InputMethodManagerService.this.hideInputMethodMenu();
                    }
                };
                this.mDialogBuilder = localBuilder1.setOnCancelListener(local4).setIcon(localTypedArray.getDrawable(0));
                localTypedArray.recycle();
                View localView1 = ((LayoutInflater)this.mContext.getSystemService("layout_inflater")).inflate(17367113, null);
                this.mDialogBuilder.setCustomTitle(localView1);
                this.mSwitchingDialogTitleView = localView1;
                View localView2 = this.mSwitchingDialogTitleView.findViewById(16908944);
                if (this.mWindowManagerService.isHardKeyboardAvailable())
                {
                    n = 0;
                    localView2.setVisibility(n);
                    Switch localSwitch = (Switch)this.mSwitchingDialogTitleView.findViewById(16908945);
                    localSwitch.setChecked(this.mWindowManagerService.isHardKeyboardEnabled());
                    CompoundButton.OnCheckedChangeListener local5 = new CompoundButton.OnCheckedChangeListener()
                    {
                        public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
                        {
                            InputMethodManagerService.this.mWindowManagerService.setHardKeyboardEnabled(paramAnonymousBoolean);
                        }
                    };
                    localSwitch.setOnCheckedChangeListener(local5);
                    ImeSubtypeListAdapter localImeSubtypeListAdapter = new ImeSubtypeListAdapter(localContext, 17367211, localList, k);
                    AlertDialog.Builder localBuilder2 = this.mDialogBuilder;
                    DialogInterface.OnClickListener local6 = new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
                        {
                            while (true)
                            {
                                synchronized (InputMethodManagerService.this.mMethodMap)
                                {
                                    if ((InputMethodManagerService.this.mIms == null) || (InputMethodManagerService.this.mIms.length <= paramAnonymousInt) || (InputMethodManagerService.this.mSubtypeIds == null) || (InputMethodManagerService.this.mSubtypeIds.length > paramAnonymousInt))
                                    {
                                        InputMethodInfo localInputMethodInfo = InputMethodManagerService.this.mIms[paramAnonymousInt];
                                        i = InputMethodManagerService.this.mSubtypeIds[paramAnonymousInt];
                                        InputMethodManagerService.this.hideInputMethodMenu();
                                        if (localInputMethodInfo != null)
                                        {
                                            if ((i < 0) || (i >= localInputMethodInfo.getSubtypeCount()))
                                                break label138;
                                            InputMethodManagerService.this.setInputMethodLocked(localInputMethodInfo.getId(), i);
                                        }
                                    }
                                }
                                return;
                                label138: int i = -1;
                            }
                        }
                    };
                    localBuilder2.setSingleChoiceItems(localImeSubtypeListAdapter, k, local6);
                    if ((paramBoolean) && (!bool))
                    {
                        AlertDialog.Builder localBuilder3 = this.mDialogBuilder;
                        DialogInterface.OnClickListener local7 = new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
                            {
                                InputMethodManagerService.this.showConfigureInputMethods();
                            }
                        };
                        localBuilder3.setPositiveButton(17040456, local7);
                    }
                    this.mSwitchingDialog = this.mDialogBuilder.create();
                    this.mSwitchingDialog.setCanceledOnTouchOutside(true);
                    this.mSwitchingDialog.getWindow().setType(2012);
                    this.mSwitchingDialog.getWindow().getAttributes().setTitle("Select input method");
                    this.mSwitchingDialog.show();
                }
            }
            int n = 8;
            continue;
            label588: return;
            label589: if (i1 == i)
                label596: k = m;
            label600: m++;
        }
    }

    private void showInputMethodSubtypeMenu()
    {
        showInputMethodMenuInternal(true);
    }

    private void updateImeWindowStatusLocked()
    {
        setImeWindowStatus(this.mCurToken, this.mImeWindowVis, this.mBackDisposition);
    }

    public void addClient(IInputMethodClient paramIInputMethodClient, IInputContext paramIInputContext, int paramInt1, int paramInt2)
    {
        synchronized (this.mMethodMap)
        {
            this.mClients.put(paramIInputMethodClient.asBinder(), new ClientState(paramIInputMethodClient, paramIInputContext, paramInt1, paramInt2));
            return;
        }
    }

    InputBindResult attachNewInputLocked(boolean paramBoolean)
    {
        if (!this.mBoundToMethod)
        {
            executeOrSendMessage(this.mCurMethod, this.mCaller.obtainMessageOO(1010, this.mCurMethod, this.mCurClient.binding));
            this.mBoundToMethod = true;
        }
        SessionState localSessionState = this.mCurClient.curSession;
        if (paramBoolean)
            executeOrSendMessage(localSessionState.method, this.mCaller.obtainMessageOOO(2000, localSessionState, this.mCurInputContext, this.mCurAttribute));
        while (true)
        {
            if (this.mShowRequested)
                showCurrentInputLocked(getAppShowFlags(), null);
            return new InputBindResult(localSessionState.session, this.mCurId, this.mCurSeq);
            executeOrSendMessage(localSessionState.method, this.mCaller.obtainMessageOOO(2010, localSessionState, this.mCurInputContext, this.mCurAttribute));
        }
    }

    void buildInputMethodListLocked(ArrayList<InputMethodInfo> paramArrayList, HashMap<String, InputMethodInfo> paramHashMap)
    {
        paramArrayList.clear();
        paramHashMap.clear();
        PackageManager localPackageManager = this.mContext.getPackageManager();
        int i;
        String str1;
        HashMap localHashMap;
        int j;
        label89: ResolveInfo localResolveInfo;
        ComponentName localComponentName;
        if (this.mRes.getConfiguration().keyboard == 2)
        {
            i = 1;
            str1 = Settings.Secure.getString(this.mContext.getContentResolver(), "disabled_system_input_methods");
            if (str1 == null)
                str1 = "";
            List localList = localPackageManager.queryIntentServices(new Intent("android.view.InputMethod"), 128);
            localHashMap = this.mFileManager.getAllAdditionalInputMethodSubtypes();
            j = 0;
            if (j >= localList.size())
                break label361;
            localResolveInfo = (ResolveInfo)localList.get(j);
            ServiceInfo localServiceInfo = localResolveInfo.serviceInfo;
            localComponentName = new ComponentName(localServiceInfo.packageName, localServiceInfo.name);
            if ("android.permission.BIND_INPUT_METHOD".equals(localServiceInfo.permission))
                break label206;
            Slog.w("InputMethodManagerService", "Skipping input method " + localComponentName + ": it does not require the permission " + "android.permission.BIND_INPUT_METHOD");
        }
        while (true)
        {
            j++;
            break label89;
            i = 0;
            break;
            try
            {
                label206: InputMethodInfo localInputMethodInfo = new InputMethodInfo(this.mContext, localResolveInfo, localHashMap);
                paramArrayList.add(localInputMethodInfo);
                String str3 = localInputMethodInfo.getId();
                paramHashMap.put(str3, localInputMethodInfo);
                if (((isValidSystemDefaultIme(localInputMethodInfo, this.mContext)) || (isSystemImeThatHasEnglishSubtype(localInputMethodInfo))) && ((i == 0) || (str1.indexOf(str3) < 0)))
                    setInputMethodEnabledLocked(str3, true);
            }
            catch (XmlPullParserException localXmlPullParserException)
            {
                Slog.w("InputMethodManagerService", "Unable to load input method " + localComponentName, localXmlPullParserException);
            }
            catch (IOException localIOException)
            {
                Slog.w("InputMethodManagerService", "Unable to load input method " + localComponentName, localIOException);
            }
        }
        label361: String str2 = Settings.Secure.getString(this.mContext.getContentResolver(), "default_input_method");
        if (!TextUtils.isEmpty(str2))
        {
            if (paramHashMap.containsKey(str2))
                break label414;
            Slog.w("InputMethodManagerService", "Default IME is uninstalled. Choose new default IME.");
            if (chooseNewDefaultIMELocked())
                updateFromSettingsLocked();
        }
        while (true)
        {
            return;
            label414: setInputMethodEnabledLocked(str2, true);
        }
    }

    void clearCurMethodLocked()
    {
        if (this.mCurMethod != null)
        {
            Iterator localIterator = this.mClients.values().iterator();
            while (localIterator.hasNext())
            {
                ClientState localClientState = (ClientState)localIterator.next();
                localClientState.sessionRequested = false;
                finishSession(localClientState.curSession);
                localClientState.curSession = null;
            }
            finishSession(this.mEnabledSession);
            this.mEnabledSession = null;
            this.mCurMethod = null;
        }
        if (this.mStatusBar != null)
            this.mStatusBar.setIconVisibility("ime", false);
    }

    // ERROR //
    protected void dump(java.io.FileDescriptor paramFileDescriptor, java.io.PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 283	com/android/server/InputMethodManagerService:mContext	Landroid/content/Context;
        //     4: ldc_w 1237
        //     7: invokevirtual 867	android/content/Context:checkCallingOrSelfPermission	(Ljava/lang/String;)I
        //     10: ifeq +42 -> 52
        //     13: aload_2
        //     14: new 799	java/lang/StringBuilder
        //     17: dup
        //     18: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     21: ldc_w 1239
        //     24: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     27: invokestatic 1242	android/os/Binder:getCallingPid	()I
        //     30: invokevirtual 884	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     33: ldc_w 1244
        //     36: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     39: invokestatic 881	android/os/Binder:getCallingUid	()I
        //     42: invokevirtual 884	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     45: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     48: invokevirtual 1249	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     51: return
        //     52: new 1251	android/util/PrintWriterPrinter
        //     55: dup
        //     56: aload_2
        //     57: invokespecial 1254	android/util/PrintWriterPrinter:<init>	(Ljava/io/PrintWriter;)V
        //     60: astore 4
        //     62: aload_0
        //     63: getfield 259	com/android/server/InputMethodManagerService:mMethodMap	Ljava/util/HashMap;
        //     66: astore 5
        //     68: aload 5
        //     70: monitorenter
        //     71: aload 4
        //     73: ldc_w 1256
        //     76: invokeinterface 1259 2 0
        //     81: aload_0
        //     82: getfield 254	com/android/server/InputMethodManagerService:mMethodList	Ljava/util/ArrayList;
        //     85: invokevirtual 831	java/util/ArrayList:size	()I
        //     88: istore 7
        //     90: aload 4
        //     92: ldc_w 1261
        //     95: invokeinterface 1259 2 0
        //     100: iconst_0
        //     101: istore 8
        //     103: iload 8
        //     105: iload 7
        //     107: if_icmpge +67 -> 174
        //     110: aload_0
        //     111: getfield 254	com/android/server/InputMethodManagerService:mMethodList	Ljava/util/ArrayList;
        //     114: iload 8
        //     116: invokevirtual 1262	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     119: checkcast 592	android/view/inputmethod/InputMethodInfo
        //     122: astore 15
        //     124: aload 4
        //     126: new 799	java/lang/StringBuilder
        //     129: dup
        //     130: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     133: ldc_w 1264
        //     136: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     139: iload 8
        //     141: invokevirtual 884	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     144: ldc_w 1266
        //     147: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     150: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     153: invokeinterface 1259 2 0
        //     158: aload 15
        //     160: aload 4
        //     162: ldc_w 1268
        //     165: invokevirtual 1271	android/view/inputmethod/InputMethodInfo:dump	(Landroid/util/Printer;Ljava/lang/String;)V
        //     168: iinc 8 1
        //     171: goto -68 -> 103
        //     174: aload 4
        //     176: ldc_w 1273
        //     179: invokeinterface 1259 2 0
        //     184: aload_0
        //     185: getfield 275	com/android/server/InputMethodManagerService:mClients	Ljava/util/HashMap;
        //     188: invokevirtual 726	java/util/HashMap:values	()Ljava/util/Collection;
        //     191: invokeinterface 1216 1 0
        //     196: astore 9
        //     198: aload 9
        //     200: invokeinterface 629 1 0
        //     205: ifeq +184 -> 389
        //     208: aload 9
        //     210: invokeinterface 633 1 0
        //     215: checkcast 54	com/android/server/InputMethodManagerService$ClientState
        //     218: astore 14
        //     220: aload 4
        //     222: new 799	java/lang/StringBuilder
        //     225: dup
        //     226: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     229: ldc_w 1275
        //     232: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     235: aload 14
        //     237: invokevirtual 889	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     240: ldc_w 1266
        //     243: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     246: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     249: invokeinterface 1259 2 0
        //     254: aload 4
        //     256: new 799	java/lang/StringBuilder
        //     259: dup
        //     260: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     263: ldc_w 1277
        //     266: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     269: aload 14
        //     271: getfield 1281	com/android/server/InputMethodManagerService$ClientState:client	Lcom/android/internal/view/IInputMethodClient;
        //     274: invokevirtual 889	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     277: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     280: invokeinterface 1259 2 0
        //     285: aload 4
        //     287: new 799	java/lang/StringBuilder
        //     290: dup
        //     291: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     294: ldc_w 1283
        //     297: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     300: aload 14
        //     302: getfield 1286	com/android/server/InputMethodManagerService$ClientState:inputContext	Lcom/android/internal/view/IInputContext;
        //     305: invokevirtual 889	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     308: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     311: invokeinterface 1259 2 0
        //     316: aload 4
        //     318: new 799	java/lang/StringBuilder
        //     321: dup
        //     322: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     325: ldc_w 1288
        //     328: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     331: aload 14
        //     333: getfield 1219	com/android/server/InputMethodManagerService$ClientState:sessionRequested	Z
        //     336: invokevirtual 1291	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     339: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     342: invokeinterface 1259 2 0
        //     347: aload 4
        //     349: new 799	java/lang/StringBuilder
        //     352: dup
        //     353: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     356: ldc_w 1293
        //     359: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     362: aload 14
        //     364: getfield 1127	com/android/server/InputMethodManagerService$ClientState:curSession	Lcom/android/server/InputMethodManagerService$SessionState;
        //     367: invokevirtual 889	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     370: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     373: invokeinterface 1259 2 0
        //     378: goto -180 -> 198
        //     381: astore 6
        //     383: aload 5
        //     385: monitorexit
        //     386: aload 6
        //     388: athrow
        //     389: aload 4
        //     391: new 799	java/lang/StringBuilder
        //     394: dup
        //     395: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     398: ldc_w 1295
        //     401: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     404: aload_0
        //     405: getfield 635	com/android/server/InputMethodManagerService:mCurMethodId	Ljava/lang/String;
        //     408: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     411: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     414: invokeinterface 1259 2 0
        //     419: aload_0
        //     420: getfield 1112	com/android/server/InputMethodManagerService:mCurClient	Lcom/android/server/InputMethodManagerService$ClientState;
        //     423: astore 10
        //     425: aload 4
        //     427: new 799	java/lang/StringBuilder
        //     430: dup
        //     431: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     434: ldc_w 1297
        //     437: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     440: aload 10
        //     442: invokevirtual 889	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     445: ldc_w 1299
        //     448: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     451: aload_0
        //     452: getfield 1150	com/android/server/InputMethodManagerService:mCurSeq	I
        //     455: invokevirtual 884	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     458: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     461: invokeinterface 1259 2 0
        //     466: aload 4
        //     468: new 799	java/lang/StringBuilder
        //     471: dup
        //     472: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     475: ldc_w 1301
        //     478: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     481: aload_0
        //     482: getfield 1303	com/android/server/InputMethodManagerService:mCurFocusedWindow	Landroid/os/IBinder;
        //     485: invokevirtual 889	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     488: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     491: invokeinterface 1259 2 0
        //     496: aload 4
        //     498: new 799	java/lang/StringBuilder
        //     501: dup
        //     502: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     505: ldc_w 1305
        //     508: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     511: aload_0
        //     512: getfield 1148	com/android/server/InputMethodManagerService:mCurId	Ljava/lang/String;
        //     515: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     518: ldc_w 1307
        //     521: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     524: aload_0
        //     525: getfield 1309	com/android/server/InputMethodManagerService:mHaveConnection	Z
        //     528: invokevirtual 1291	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     531: ldc_w 1311
        //     534: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     537: aload_0
        //     538: getfield 1108	com/android/server/InputMethodManagerService:mBoundToMethod	Z
        //     541: invokevirtual 1291	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     544: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     547: invokeinterface 1259 2 0
        //     552: aload 4
        //     554: new 799	java/lang/StringBuilder
        //     557: dup
        //     558: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     561: ldc_w 1313
        //     564: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     567: aload_0
        //     568: getfield 874	com/android/server/InputMethodManagerService:mCurToken	Landroid/os/IBinder;
        //     571: invokevirtual 889	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     574: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     577: invokeinterface 1259 2 0
        //     582: aload 4
        //     584: new 799	java/lang/StringBuilder
        //     587: dup
        //     588: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     591: ldc_w 1315
        //     594: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     597: aload_0
        //     598: getfield 1317	com/android/server/InputMethodManagerService:mCurIntent	Landroid/content/Intent;
        //     601: invokevirtual 889	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     604: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     607: invokeinterface 1259 2 0
        //     612: aload_0
        //     613: getfield 1110	com/android/server/InputMethodManagerService:mCurMethod	Lcom/android/internal/view/IInputMethod;
        //     616: astore 11
        //     618: aload 4
        //     620: new 799	java/lang/StringBuilder
        //     623: dup
        //     624: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     627: ldc_w 1319
        //     630: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     633: aload_0
        //     634: getfield 1110	com/android/server/InputMethodManagerService:mCurMethod	Lcom/android/internal/view/IInputMethod;
        //     637: invokevirtual 889	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     640: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     643: invokeinterface 1259 2 0
        //     648: aload 4
        //     650: new 799	java/lang/StringBuilder
        //     653: dup
        //     654: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     657: ldc_w 1321
        //     660: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     663: aload_0
        //     664: getfield 1223	com/android/server/InputMethodManagerService:mEnabledSession	Lcom/android/server/InputMethodManagerService$SessionState;
        //     667: invokevirtual 889	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     670: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     673: invokeinterface 1259 2 0
        //     678: aload 4
        //     680: new 799	java/lang/StringBuilder
        //     683: dup
        //     684: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     687: ldc_w 1323
        //     690: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     693: aload_0
        //     694: getfield 1140	com/android/server/InputMethodManagerService:mShowRequested	Z
        //     697: invokevirtual 1291	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     700: ldc_w 1325
        //     703: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     706: aload_0
        //     707: getfield 711	com/android/server/InputMethodManagerService:mShowExplicitlyRequested	Z
        //     710: invokevirtual 1291	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     713: ldc_w 1327
        //     716: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     719: aload_0
        //     720: getfield 709	com/android/server/InputMethodManagerService:mShowForced	Z
        //     723: invokevirtual 1291	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     726: ldc_w 1329
        //     729: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     732: aload_0
        //     733: getfield 817	com/android/server/InputMethodManagerService:mInputShown	Z
        //     736: invokevirtual 1291	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     739: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     742: invokeinterface 1259 2 0
        //     747: aload 4
        //     749: new 799	java/lang/StringBuilder
        //     752: dup
        //     753: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     756: ldc_w 1331
        //     759: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     762: aload_0
        //     763: getfield 570	com/android/server/InputMethodManagerService:mSystemReady	Z
        //     766: invokevirtual 1291	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     769: ldc_w 1333
        //     772: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     775: aload_0
        //     776: getfield 279	com/android/server/InputMethodManagerService:mScreenOn	Z
        //     779: invokevirtual 1291	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     782: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     785: invokeinterface 1259 2 0
        //     790: aload 5
        //     792: monitorexit
        //     793: aload 4
        //     795: ldc_w 1335
        //     798: invokeinterface 1259 2 0
        //     803: aload 10
        //     805: ifnull +126 -> 931
        //     808: aload_2
        //     809: invokevirtual 1338	java/io/PrintWriter:flush	()V
        //     812: aload 10
        //     814: getfield 1281	com/android/server/InputMethodManagerService$ClientState:client	Lcom/android/internal/view/IInputMethodClient;
        //     817: invokeinterface 1101 1 0
        //     822: aload_1
        //     823: aload_3
        //     824: invokeinterface 1343 3 0
        //     829: aload 4
        //     831: ldc_w 1335
        //     834: invokeinterface 1259 2 0
        //     839: aload 11
        //     841: ifnull +103 -> 944
        //     844: aload_2
        //     845: invokevirtual 1338	java/io/PrintWriter:flush	()V
        //     848: aload 11
        //     850: invokeinterface 1346 1 0
        //     855: aload_1
        //     856: aload_3
        //     857: invokeinterface 1343 3 0
        //     862: goto -811 -> 51
        //     865: astore 12
        //     867: aload 4
        //     869: new 799	java/lang/StringBuilder
        //     872: dup
        //     873: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     876: ldc_w 1348
        //     879: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     882: aload 12
        //     884: invokevirtual 889	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     887: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     890: invokeinterface 1259 2 0
        //     895: goto -844 -> 51
        //     898: astore 13
        //     900: aload 4
        //     902: new 799	java/lang/StringBuilder
        //     905: dup
        //     906: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     909: ldc_w 1350
        //     912: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     915: aload 13
        //     917: invokevirtual 889	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     920: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     923: invokeinterface 1259 2 0
        //     928: goto -99 -> 829
        //     931: aload 4
        //     933: ldc_w 1352
        //     936: invokeinterface 1259 2 0
        //     941: goto -112 -> 829
        //     944: aload 4
        //     946: ldc_w 1354
        //     949: invokeinterface 1259 2 0
        //     954: goto -903 -> 51
        //
        // Exception table:
        //     from	to	target	type
        //     71	386	381	finally
        //     389	793	381	finally
        //     848	862	865	android/os/RemoteException
        //     812	829	898	android/os/RemoteException
    }

    void executeOrSendMessage(IInterface paramIInterface, Message paramMessage)
    {
        if ((paramIInterface.asBinder() instanceof Binder))
            this.mCaller.sendMessage(paramMessage);
        while (true)
        {
            return;
            handleMessage(paramMessage);
            paramMessage.recycle();
        }
    }

    public void finishInput(IInputMethodClient paramIInputMethodClient)
    {
    }

    // ERROR //
    public InputMethodSubtype getCurrentInputMethodSubtype()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 635	com/android/server/InputMethodManagerService:mCurMethodId	Ljava/lang/String;
        //     4: ifnonnull +9 -> 13
        //     7: aconst_null
        //     8: astore 6
        //     10: aload 6
        //     12: areturn
        //     13: iconst_0
        //     14: istore_1
        //     15: aload_0
        //     16: getfield 283	com/android/server/InputMethodManagerService:mContext	Landroid/content/Context;
        //     19: invokevirtual 418	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
        //     22: ldc_w 754
        //     25: invokestatic 758	android/provider/Settings$Secure:getInt	(Landroid/content/ContentResolver;Ljava/lang/String;)I
        //     28: istore 9
        //     30: iload 9
        //     32: bipush 255
        //     34: if_icmpeq +223 -> 257
        //     37: iconst_1
        //     38: istore_1
        //     39: aload_0
        //     40: getfield 259	com/android/server/InputMethodManagerService:mMethodMap	Ljava/util/HashMap;
        //     43: astore_3
        //     44: aload_3
        //     45: monitorenter
        //     46: aload_0
        //     47: getfield 259	com/android/server/InputMethodManagerService:mMethodMap	Ljava/util/HashMap;
        //     50: aload_0
        //     51: getfield 635	com/android/server/InputMethodManagerService:mCurMethodId	Ljava/lang/String;
        //     54: invokevirtual 554	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     57: checkcast 592	android/view/inputmethod/InputMethodInfo
        //     60: astore 5
        //     62: aload 5
        //     64: ifnull +11 -> 75
        //     67: aload 5
        //     69: invokevirtual 601	android/view/inputmethod/InputMethodInfo:getSubtypeCount	()I
        //     72: ifne +11 -> 83
        //     75: aload_3
        //     76: monitorexit
        //     77: aconst_null
        //     78: astore 6
        //     80: goto -70 -> 10
        //     83: iload_1
        //     84: ifeq +25 -> 109
        //     87: aload_0
        //     88: getfield 642	com/android/server/InputMethodManagerService:mCurrentSubtype	Landroid/view/inputmethod/InputMethodSubtype;
        //     91: ifnull +18 -> 109
        //     94: aload 5
        //     96: aload_0
        //     97: getfield 642	com/android/server/InputMethodManagerService:mCurrentSubtype	Landroid/view/inputmethod/InputMethodSubtype;
        //     100: invokevirtual 761	android/view/inputmethod/InputMethodSubtype:hashCode	()I
        //     103: invokestatic 524	com/android/server/InputMethodManagerService:isValidSubtypeId	(Landroid/view/inputmethod/InputMethodInfo;I)Z
        //     106: ifne +55 -> 161
        //     109: aload_0
        //     110: aload_0
        //     111: getfield 635	com/android/server/InputMethodManagerService:mCurMethodId	Ljava/lang/String;
        //     114: invokespecial 946	com/android/server/InputMethodManagerService:getSelectedInputMethodSubtypeId	(Ljava/lang/String;)I
        //     117: istore 7
        //     119: iload 7
        //     121: bipush 255
        //     123: if_icmpne +110 -> 233
        //     126: aload_0
        //     127: aload 5
        //     129: iconst_1
        //     130: invokevirtual 640	com/android/server/InputMethodManagerService:getEnabledInputMethodSubtypeList	(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;
        //     133: astore 8
        //     135: aload 8
        //     137: invokeinterface 673 1 0
        //     142: iconst_1
        //     143: if_icmpne +36 -> 179
        //     146: aload_0
        //     147: aload 8
        //     149: iconst_0
        //     150: invokeinterface 683 2 0
        //     155: checkcast 565	android/view/inputmethod/InputMethodSubtype
        //     158: putfield 642	com/android/server/InputMethodManagerService:mCurrentSubtype	Landroid/view/inputmethod/InputMethodSubtype;
        //     161: aload_0
        //     162: getfield 642	com/android/server/InputMethodManagerService:mCurrentSubtype	Landroid/view/inputmethod/InputMethodSubtype;
        //     165: astore 6
        //     167: aload_3
        //     168: monitorexit
        //     169: goto -159 -> 10
        //     172: astore 4
        //     174: aload_3
        //     175: monitorexit
        //     176: aload 4
        //     178: athrow
        //     179: aload 8
        //     181: invokeinterface 673 1 0
        //     186: iconst_1
        //     187: if_icmple -26 -> 161
        //     190: aload_0
        //     191: aload_0
        //     192: getfield 291	com/android/server/InputMethodManagerService:mRes	Landroid/content/res/Resources;
        //     195: aload 8
        //     197: ldc 105
        //     199: aconst_null
        //     200: iconst_1
        //     201: invokestatic 646	com/android/server/InputMethodManagerService:findLastResortApplicableSubtypeLocked	(Landroid/content/res/Resources;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Z)Landroid/view/inputmethod/InputMethodSubtype;
        //     204: putfield 642	com/android/server/InputMethodManagerService:mCurrentSubtype	Landroid/view/inputmethod/InputMethodSubtype;
        //     207: aload_0
        //     208: getfield 642	com/android/server/InputMethodManagerService:mCurrentSubtype	Landroid/view/inputmethod/InputMethodSubtype;
        //     211: ifnonnull -50 -> 161
        //     214: aload_0
        //     215: aload_0
        //     216: getfield 291	com/android/server/InputMethodManagerService:mRes	Landroid/content/res/Resources;
        //     219: aload 8
        //     221: aconst_null
        //     222: aconst_null
        //     223: iconst_1
        //     224: invokestatic 646	com/android/server/InputMethodManagerService:findLastResortApplicableSubtypeLocked	(Landroid/content/res/Resources;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Z)Landroid/view/inputmethod/InputMethodSubtype;
        //     227: putfield 642	com/android/server/InputMethodManagerService:mCurrentSubtype	Landroid/view/inputmethod/InputMethodSubtype;
        //     230: goto -69 -> 161
        //     233: aload_0
        //     234: aload 5
        //     236: invokestatic 507	com/android/server/InputMethodManagerService:getSubtypes	(Landroid/view/inputmethod/InputMethodInfo;)Ljava/util/ArrayList;
        //     239: iload 7
        //     241: invokevirtual 1262	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     244: checkcast 565	android/view/inputmethod/InputMethodSubtype
        //     247: putfield 642	com/android/server/InputMethodManagerService:mCurrentSubtype	Landroid/view/inputmethod/InputMethodSubtype;
        //     250: goto -89 -> 161
        //     253: astore_2
        //     254: goto -215 -> 39
        //     257: iconst_0
        //     258: istore_1
        //     259: goto -220 -> 39
        //
        // Exception table:
        //     from	to	target	type
        //     46	176	172	finally
        //     179	250	172	finally
        //     15	30	253	android/provider/Settings$SettingNotFoundException
    }

    public List<InputMethodInfo> getEnabledInputMethodList()
    {
        synchronized (this.mMethodMap)
        {
            List localList = this.mSettings.getEnabledInputMethodListLocked();
            return localList;
        }
    }

    public List<InputMethodSubtype> getEnabledInputMethodSubtypeList(InputMethodInfo paramInputMethodInfo, boolean paramBoolean)
    {
        synchronized (this.mMethodMap)
        {
            List localList = getEnabledInputMethodSubtypeListLocked(paramInputMethodInfo, paramBoolean);
            return localList;
        }
    }

    public List<InputMethodSubtype> getEnabledInputMethodSubtypeListLocked(InputMethodInfo paramInputMethodInfo, boolean paramBoolean)
    {
        if ((paramInputMethodInfo == null) && (this.mCurMethodId != null))
            paramInputMethodInfo = (InputMethodInfo)this.mMethodMap.get(this.mCurMethodId);
        Object localObject = this.mSettings.getEnabledInputMethodSubtypeListLocked(paramInputMethodInfo);
        if ((paramBoolean) && (((List)localObject).isEmpty()))
            localObject = getImplicitlyApplicableSubtypesLocked(this.mRes, paramInputMethodInfo);
        return InputMethodSubtype.sort(this.mContext, 0, paramInputMethodInfo, (List)localObject);
    }

    public List<InputMethodInfo> getInputMethodList()
    {
        synchronized (this.mMethodMap)
        {
            ArrayList localArrayList = new ArrayList(this.mMethodList);
            return localArrayList;
        }
    }

    public InputMethodSubtype getLastInputMethodSubtype()
    {
        Pair localPair;
        Object localObject2;
        InputMethodInfo localInputMethodInfo;
        synchronized (this.mMethodMap)
        {
            localPair = this.mSettings.getLastInputMethodAndSubtypeLocked();
            if ((localPair == null) || (TextUtils.isEmpty((CharSequence)localPair.first)) || (TextUtils.isEmpty((CharSequence)localPair.second)))
            {
                localObject2 = null;
            }
            else
            {
                localInputMethodInfo = (InputMethodInfo)this.mMethodMap.get(localPair.first);
                if (localInputMethodInfo == null)
                    localObject2 = null;
            }
        }
        try
        {
            int i = getSubtypeIdFromHashCode(localInputMethodInfo, Integer.valueOf((String)localPair.second).intValue());
            if (i >= 0)
            {
                int j = localInputMethodInfo.getSubtypeCount();
                if (i < j);
            }
            else
            {
                localObject2 = null;
                break label159;
            }
            InputMethodSubtype localInputMethodSubtype = localInputMethodInfo.getSubtypeAt(i);
            localObject2 = localInputMethodSubtype;
            break label159;
            localObject1 = finally;
            throw localObject1;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            localObject2 = null;
        }
        label159: return localObject2;
    }

    // ERROR //
    public List getShortcutInputMethodsAndSubtypes()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 259	com/android/server/InputMethodManagerService:mMethodMap	Ljava/util/HashMap;
        //     4: astore_1
        //     5: aload_1
        //     6: monitorenter
        //     7: new 251	java/util/ArrayList
        //     10: dup
        //     11: invokespecial 252	java/util/ArrayList:<init>	()V
        //     14: astore_2
        //     15: aload_0
        //     16: getfield 277	com/android/server/InputMethodManagerService:mShortcutInputMethodsAndSubtypes	Ljava/util/HashMap;
        //     19: invokevirtual 722	java/util/HashMap:size	()I
        //     22: ifne +41 -> 63
        //     25: aload_0
        //     26: ldc 108
        //     28: invokespecial 1396	com/android/server/InputMethodManagerService:findLastResortApplicableShortcutInputMethodAndSubtypeLocked	(Ljava/lang/String;)Landroid/util/Pair;
        //     31: astore 9
        //     33: aload 9
        //     35: ifnull +23 -> 58
        //     38: aload_2
        //     39: aload 9
        //     41: getfield 1388	android/util/Pair:first	Ljava/lang/Object;
        //     44: invokevirtual 557	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     47: pop
        //     48: aload_2
        //     49: aload 9
        //     51: getfield 1393	android/util/Pair:second	Ljava/lang/Object;
        //     54: invokevirtual 557	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     57: pop
        //     58: aload_1
        //     59: monitorexit
        //     60: goto +98 -> 158
        //     63: aload_0
        //     64: getfield 277	com/android/server/InputMethodManagerService:mShortcutInputMethodsAndSubtypes	Ljava/util/HashMap;
        //     67: invokevirtual 1400	java/util/HashMap:keySet	()Ljava/util/Set;
        //     70: invokeinterface 1403 1 0
        //     75: astore 4
        //     77: aload 4
        //     79: invokeinterface 629 1 0
        //     84: ifeq +72 -> 156
        //     87: aload 4
        //     89: invokeinterface 633 1 0
        //     94: checkcast 592	android/view/inputmethod/InputMethodInfo
        //     97: astore 5
        //     99: aload_2
        //     100: aload 5
        //     102: invokevirtual 557	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     105: pop
        //     106: aload_0
        //     107: getfield 277	com/android/server/InputMethodManagerService:mShortcutInputMethodsAndSubtypes	Ljava/util/HashMap;
        //     110: aload 5
        //     112: invokevirtual 554	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     115: checkcast 251	java/util/ArrayList
        //     118: invokevirtual 823	java/util/ArrayList:iterator	()Ljava/util/Iterator;
        //     121: astore 7
        //     123: aload 7
        //     125: invokeinterface 629 1 0
        //     130: ifeq -53 -> 77
        //     133: aload_2
        //     134: aload 7
        //     136: invokeinterface 633 1 0
        //     141: checkcast 565	android/view/inputmethod/InputMethodSubtype
        //     144: invokevirtual 557	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     147: pop
        //     148: goto -25 -> 123
        //     151: astore_3
        //     152: aload_1
        //     153: monitorexit
        //     154: aload_3
        //     155: athrow
        //     156: aload_1
        //     157: monitorexit
        //     158: aload_2
        //     159: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     7	154	151	finally
        //     156	158	151	finally
    }

    public boolean handleMessage(Message paramMessage)
    {
        boolean bool = false;
        int i = 1;
        switch (paramMessage.what)
        {
        default:
            i = 0;
        case 1:
        case 2:
        case 3:
        case 4:
        case 1000:
        case 1010:
        case 1020:
        case 1030:
        case 1040:
        case 1050:
        case 2000:
        case 2010:
        case 3000:
        case 3010:
        case 3020:
            while (true)
            {
                return i;
                showInputMethodMenu();
                continue;
                showInputMethodSubtypeMenu();
                continue;
                showInputMethodAndSubtypeEnabler((String)((HandlerCaller.SomeArgs)paramMessage.obj).arg1);
                continue;
                showConfigureInputMethods();
                continue;
                try
                {
                    ((IInputMethod)paramMessage.obj).unbindInput();
                }
                catch (RemoteException localRemoteException11)
                {
                }
                continue;
                HandlerCaller.SomeArgs localSomeArgs8 = (HandlerCaller.SomeArgs)paramMessage.obj;
                try
                {
                    ((IInputMethod)localSomeArgs8.arg1).bindInput((InputBinding)localSomeArgs8.arg2);
                }
                catch (RemoteException localRemoteException10)
                {
                }
                continue;
                HandlerCaller.SomeArgs localSomeArgs7 = (HandlerCaller.SomeArgs)paramMessage.obj;
                try
                {
                    ((IInputMethod)localSomeArgs7.arg1).showSoftInput(paramMessage.arg1, (ResultReceiver)localSomeArgs7.arg2);
                }
                catch (RemoteException localRemoteException9)
                {
                }
                continue;
                HandlerCaller.SomeArgs localSomeArgs6 = (HandlerCaller.SomeArgs)paramMessage.obj;
                try
                {
                    ((IInputMethod)localSomeArgs6.arg1).hideSoftInput(0, (ResultReceiver)localSomeArgs6.arg2);
                }
                catch (RemoteException localRemoteException8)
                {
                }
                continue;
                HandlerCaller.SomeArgs localSomeArgs5 = (HandlerCaller.SomeArgs)paramMessage.obj;
                try
                {
                    ((IInputMethod)localSomeArgs5.arg1).attachToken((IBinder)localSomeArgs5.arg2);
                }
                catch (RemoteException localRemoteException7)
                {
                }
                continue;
                HandlerCaller.SomeArgs localSomeArgs4 = (HandlerCaller.SomeArgs)paramMessage.obj;
                try
                {
                    ((IInputMethod)localSomeArgs4.arg1).createSession((IInputMethodCallback)localSomeArgs4.arg2);
                }
                catch (RemoteException localRemoteException6)
                {
                }
                continue;
                HandlerCaller.SomeArgs localSomeArgs3 = (HandlerCaller.SomeArgs)paramMessage.obj;
                try
                {
                    SessionState localSessionState2 = (SessionState)localSomeArgs3.arg1;
                    setEnabledSessionInMainThread(localSessionState2);
                    localSessionState2.method.startInput((IInputContext)localSomeArgs3.arg2, (EditorInfo)localSomeArgs3.arg3);
                }
                catch (RemoteException localRemoteException5)
                {
                }
                continue;
                HandlerCaller.SomeArgs localSomeArgs2 = (HandlerCaller.SomeArgs)paramMessage.obj;
                try
                {
                    SessionState localSessionState1 = (SessionState)localSomeArgs2.arg1;
                    setEnabledSessionInMainThread(localSessionState1);
                    localSessionState1.method.restartInput((IInputContext)localSomeArgs2.arg2, (EditorInfo)localSomeArgs2.arg3);
                }
                catch (RemoteException localRemoteException4)
                {
                }
                continue;
                try
                {
                    ((IInputMethodClient)paramMessage.obj).onUnbindMethod(paramMessage.arg1);
                }
                catch (RemoteException localRemoteException3)
                {
                }
                continue;
                HandlerCaller.SomeArgs localSomeArgs1 = (HandlerCaller.SomeArgs)paramMessage.obj;
                try
                {
                    ((IInputMethodClient)localSomeArgs1.arg1).onBindMethod((InputBindResult)localSomeArgs1.arg2);
                }
                catch (RemoteException localRemoteException2)
                {
                    Slog.w("InputMethodManagerService", "Client died receiving input method " + localSomeArgs1.arg2);
                }
                continue;
                try
                {
                    IInputMethodClient localIInputMethodClient = ((ClientState)paramMessage.obj).client;
                    if (paramMessage.arg1 != 0)
                        bool = i;
                    localIInputMethodClient.setActive(bool);
                }
                catch (RemoteException localRemoteException1)
                {
                    Slog.w("InputMethodManagerService", "Got RemoteException sending setActive(false) notification to pid " + ((ClientState)paramMessage.obj).pid + " uid " + ((ClientState)paramMessage.obj).uid);
                }
            }
        case 4000:
        }
        HardKeyboardListener localHardKeyboardListener = this.mHardKeyboardListener;
        if (paramMessage.arg1 == i);
        int k;
        for (int j = i; ; k = 0)
        {
            if (paramMessage.arg2 == i)
                bool = i;
            localHardKeyboardListener.handleHardKeyboardStatusChange(j, bool);
            break;
        }
    }

    boolean hideCurrentInputLocked(int paramInt, ResultReceiver paramResultReceiver)
    {
        if (((paramInt & 0x1) != 0) && ((this.mShowExplicitlyRequested) || (this.mShowForced)));
        for (boolean bool = false; ; bool = false)
        {
            return bool;
            if ((!this.mShowForced) || ((paramInt & 0x2) == 0))
                break;
        }
        if ((this.mInputShown) && (this.mCurMethod != null))
            executeOrSendMessage(this.mCurMethod, this.mCaller.obtainMessageOO(1030, this.mCurMethod, paramResultReceiver));
        for (bool = true; ; bool = false)
        {
            if ((this.mHaveConnection) && (this.mVisibleBound))
            {
                this.mContext.unbindService(this.mVisibleConnection);
                this.mVisibleBound = false;
            }
            this.mInputShown = false;
            this.mShowRequested = false;
            this.mShowExplicitlyRequested = false;
            this.mShowForced = false;
            break;
        }
    }

    void hideInputMethodMenu()
    {
        synchronized (this.mMethodMap)
        {
            hideInputMethodMenuLocked();
            return;
        }
    }

    void hideInputMethodMenuLocked()
    {
        if (this.mSwitchingDialog != null)
        {
            this.mSwitchingDialog.dismiss();
            this.mSwitchingDialog = null;
        }
        this.mDialogBuilder = null;
        this.mIms = null;
    }

    // ERROR //
    public void hideMySoftInput(IBinder paramIBinder, int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 259	com/android/server/InputMethodManagerService:mMethodMap	Ljava/util/HashMap;
        //     4: astore_3
        //     5: aload_3
        //     6: monitorenter
        //     7: aload_1
        //     8: ifnull +11 -> 19
        //     11: aload_0
        //     12: getfield 874	com/android/server/InputMethodManagerService:mCurToken	Landroid/os/IBinder;
        //     15: aload_1
        //     16: if_acmpeq +8 -> 24
        //     19: aload_3
        //     20: monitorexit
        //     21: goto +42 -> 63
        //     24: invokestatic 893	android/os/Binder:clearCallingIdentity	()J
        //     27: lstore 5
        //     29: aload_0
        //     30: iload_2
        //     31: aconst_null
        //     32: invokevirtual 1511	com/android/server/InputMethodManagerService:hideCurrentInputLocked	(ILandroid/os/ResultReceiver;)Z
        //     35: pop
        //     36: lload 5
        //     38: invokestatic 901	android/os/Binder:restoreCallingIdentity	(J)V
        //     41: aload_3
        //     42: monitorexit
        //     43: goto +20 -> 63
        //     46: astore 4
        //     48: aload_3
        //     49: monitorexit
        //     50: aload 4
        //     52: athrow
        //     53: astore 7
        //     55: lload 5
        //     57: invokestatic 901	android/os/Binder:restoreCallingIdentity	(J)V
        //     60: aload 7
        //     62: athrow
        //     63: return
        //
        // Exception table:
        //     from	to	target	type
        //     11	29	46	finally
        //     36	50	46	finally
        //     55	63	46	finally
        //     29	36	53	finally
    }

    public boolean hideSoftInput(IInputMethodClient paramIInputMethodClient, int paramInt, ResultReceiver paramResultReceiver)
    {
        boolean bool = false;
        Binder.getCallingUid();
        long l = Binder.clearCallingIdentity();
        while (true)
        {
            try
            {
                synchronized (this.mMethodMap)
                {
                    if ((this.mCurClient != null) && (paramIInputMethodClient != null))
                    {
                        IBinder localIBinder1 = this.mCurClient.client.asBinder();
                        IBinder localIBinder2 = paramIInputMethodClient.asBinder();
                        if (localIBinder1 == localIBinder2);
                    }
                    else
                    {
                        try
                        {
                            if (!this.mIWindowManager.inputMethodClientHasFocus(paramIInputMethodClient))
                            {
                                setImeWindowVisibilityStatusHiddenLocked();
                                return bool;
                            }
                        }
                        catch (RemoteException localRemoteException)
                        {
                            setImeWindowVisibilityStatusHiddenLocked();
                            continue;
                        }
                    }
                }
            }
            finally
            {
                Binder.restoreCallingIdentity(l);
            }
            bool = hideCurrentInputLocked(paramInt, paramResultReceiver);
        }
    }

    // ERROR //
    public boolean notifySuggestionPicked(SuggestionSpan paramSuggestionSpan, String paramString, int paramInt)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore 4
        //     3: aload_0
        //     4: getfield 259	com/android/server/InputMethodManagerService:mMethodMap	Ljava/util/HashMap;
        //     7: astore 5
        //     9: aload 5
        //     11: monitorenter
        //     12: aload_0
        //     13: getfield 266	com/android/server/InputMethodManagerService:mSecureSuggestionSpans	Landroid/util/LruCache;
        //     16: aload_1
        //     17: invokevirtual 1521	android/util/LruCache:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     20: checkcast 592	android/view/inputmethod/InputMethodInfo
        //     23: astore 7
        //     25: aload 7
        //     27: ifnull +125 -> 152
        //     30: aload_1
        //     31: invokevirtual 1527	android/text/style/SuggestionSpan:getSuggestions	()[Ljava/lang/String;
        //     34: astore 8
        //     36: iload_3
        //     37: iflt +10 -> 47
        //     40: iload_3
        //     41: aload 8
        //     43: arraylength
        //     44: if_icmplt +9 -> 53
        //     47: aload 5
        //     49: monitorexit
        //     50: goto +105 -> 155
        //     53: aload_1
        //     54: invokevirtual 1530	android/text/style/SuggestionSpan:getNotificationTargetClassName	()Ljava/lang/String;
        //     57: astore 9
        //     59: new 367	android/content/Intent
        //     62: dup
        //     63: invokespecial 1531	android/content/Intent:<init>	()V
        //     66: astore 10
        //     68: aload 10
        //     70: aload 7
        //     72: invokevirtual 789	android/view/inputmethod/InputMethodInfo:getPackageName	()Ljava/lang/String;
        //     75: aload 9
        //     77: invokevirtual 1534	android/content/Intent:setClassName	(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
        //     80: pop
        //     81: aload 10
        //     83: ldc_w 1536
        //     86: invokevirtual 1540	android/content/Intent:setAction	(Ljava/lang/String;)Landroid/content/Intent;
        //     89: pop
        //     90: aload 10
        //     92: ldc_w 1542
        //     95: aload_2
        //     96: invokevirtual 935	android/content/Intent:putExtra	(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
        //     99: pop
        //     100: aload 10
        //     102: ldc_w 1544
        //     105: aload 8
        //     107: iload_3
        //     108: aaload
        //     109: invokevirtual 935	android/content/Intent:putExtra	(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
        //     112: pop
        //     113: aload 10
        //     115: ldc_w 1546
        //     118: aload_1
        //     119: invokevirtual 1547	android/text/style/SuggestionSpan:hashCode	()I
        //     122: invokevirtual 1550	android/content/Intent:putExtra	(Ljava/lang/String;I)Landroid/content/Intent;
        //     125: pop
        //     126: aload_0
        //     127: getfield 283	com/android/server/InputMethodManagerService:mContext	Landroid/content/Context;
        //     130: aload 10
        //     132: invokevirtual 1553	android/content/Context:sendBroadcast	(Landroid/content/Intent;)V
        //     135: iconst_1
        //     136: istore 4
        //     138: aload 5
        //     140: monitorexit
        //     141: goto +14 -> 155
        //     144: astore 6
        //     146: aload 5
        //     148: monitorexit
        //     149: aload 6
        //     151: athrow
        //     152: aload 5
        //     154: monitorexit
        //     155: iload 4
        //     157: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     12	149	144	finally
        //     152	155	144	finally
    }

    public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
    {
        synchronized (this.mMethodMap)
        {
            if ((this.mCurIntent != null) && (paramComponentName.equals(this.mCurIntent.getComponent())))
            {
                this.mCurMethod = IInputMethod.Stub.asInterface(paramIBinder);
                if (this.mCurToken == null)
                {
                    Slog.w("InputMethodManagerService", "Service connected without a token!");
                    unbindCurrentMethodLocked(false);
                    return;
                }
                executeOrSendMessage(this.mCurMethod, this.mCaller.obtainMessageOO(1040, this.mCurMethod, this.mCurToken));
                if (this.mCurClient != null)
                    executeOrSendMessage(this.mCurMethod, this.mCaller.obtainMessageOO(1050, this.mCurMethod, new MethodCallback(this.mCurMethod, this)));
            }
        }
    }

    public void onServiceDisconnected(ComponentName paramComponentName)
    {
        synchronized (this.mMethodMap)
        {
            if ((this.mCurMethod != null) && (this.mCurIntent != null) && (paramComponentName.equals(this.mCurIntent.getComponent())))
            {
                clearCurMethodLocked();
                this.mLastBindTime = SystemClock.uptimeMillis();
                this.mShowRequested = this.mInputShown;
                this.mInputShown = false;
                if (this.mCurClient != null)
                    executeOrSendMessage(this.mCurClient.client, this.mCaller.obtainMessageIO(3000, this.mCurSeq, this.mCurClient.client));
            }
            return;
        }
    }

    void onSessionCreated(IInputMethod paramIInputMethod, IInputMethodSession paramIInputMethodSession)
    {
        synchronized (this.mMethodMap)
        {
            if ((this.mCurMethod != null) && (paramIInputMethod != null) && (this.mCurMethod.asBinder() == paramIInputMethod.asBinder()) && (this.mCurClient != null))
            {
                this.mCurClient.curSession = new SessionState(this.mCurClient, paramIInputMethod, paramIInputMethodSession);
                this.mCurClient.sessionRequested = false;
                InputBindResult localInputBindResult = attachNewInputLocked(true);
                if (localInputBindResult.method != null)
                    executeOrSendMessage(this.mCurClient.client, this.mCaller.obtainMessageOO(3010, this.mCurClient.client, localInputBindResult));
            }
            return;
        }
    }

    public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
        throws RemoteException
    {
        try
        {
            boolean bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            return bool;
        }
        catch (RuntimeException localRuntimeException)
        {
            if (!(localRuntimeException instanceof SecurityException))
                Slog.e("InputMethodManagerService", "Input Method Manager Crash", localRuntimeException);
            throw localRuntimeException;
        }
    }

    public void registerSuggestionSpansForNotification(SuggestionSpan[] paramArrayOfSuggestionSpan)
    {
        while (true)
        {
            int i;
            synchronized (this.mMethodMap)
            {
                InputMethodInfo localInputMethodInfo = (InputMethodInfo)this.mMethodMap.get(this.mCurMethodId);
                i = 0;
                if (i < paramArrayOfSuggestionSpan.length)
                {
                    SuggestionSpan localSuggestionSpan = paramArrayOfSuggestionSpan[i];
                    if (!TextUtils.isEmpty(localSuggestionSpan.getNotificationTargetClassName()))
                    {
                        this.mSecureSuggestionSpans.put(localSuggestionSpan, localInputMethodInfo);
                        ((InputMethodInfo)this.mSecureSuggestionSpans.get(localSuggestionSpan));
                    }
                }
                else
                {
                    return;
                }
            }
            i++;
        }
    }

    public void removeClient(IInputMethodClient paramIInputMethodClient)
    {
        synchronized (this.mMethodMap)
        {
            this.mClients.remove(paramIInputMethodClient.asBinder());
            return;
        }
    }

    // ERROR //
    public void setAdditionalInputMethodSubtypes(String paramString, InputMethodSubtype[] paramArrayOfInputMethodSubtype)
    {
        // Byte code:
        //     0: aload_1
        //     1: invokestatic 437	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
        //     4: ifne +12 -> 16
        //     7: aload_2
        //     8: ifnull +8 -> 16
        //     11: aload_2
        //     12: arraylength
        //     13: ifne +4 -> 17
        //     16: return
        //     17: aload_0
        //     18: getfield 259	com/android/server/InputMethodManagerService:mMethodMap	Ljava/util/HashMap;
        //     21: astore_3
        //     22: aload_3
        //     23: monitorenter
        //     24: aload_0
        //     25: getfield 259	com/android/server/InputMethodManagerService:mMethodMap	Ljava/util/HashMap;
        //     28: aload_1
        //     29: invokevirtual 554	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     32: checkcast 592	android/view/inputmethod/InputMethodInfo
        //     35: astore 5
        //     37: aload 5
        //     39: ifnonnull +15 -> 54
        //     42: aload_3
        //     43: monitorexit
        //     44: goto -28 -> 16
        //     47: astore 4
        //     49: aload_3
        //     50: monitorexit
        //     51: aload 4
        //     53: athrow
        //     54: aload_0
        //     55: getfield 283	com/android/server/InputMethodManagerService:mContext	Landroid/content/Context;
        //     58: invokevirtual 944	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
        //     61: invokestatic 881	android/os/Binder:getCallingUid	()I
        //     64: invokevirtual 1621	android/content/pm/PackageManager:getPackagesForUid	(I)[Ljava/lang/String;
        //     67: astore 6
        //     69: aload 6
        //     71: ifnull +81 -> 152
        //     74: aload 6
        //     76: arraylength
        //     77: istore 7
        //     79: iconst_0
        //     80: istore 8
        //     82: iload 8
        //     84: iload 7
        //     86: if_icmpge +66 -> 152
        //     89: aload 6
        //     91: iload 8
        //     93: aaload
        //     94: aload 5
        //     96: invokevirtual 789	android/view/inputmethod/InputMethodInfo:getPackageName	()Ljava/lang/String;
        //     99: invokevirtual 636	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     102: ifeq +55 -> 157
        //     105: aload_0
        //     106: getfield 385	com/android/server/InputMethodManagerService:mFileManager	Lcom/android/server/InputMethodManagerService$InputMethodFileManager;
        //     109: aload 5
        //     111: aload_2
        //     112: invokevirtual 1625	com/android/server/InputMethodManagerService$InputMethodFileManager:addInputMethodSubtypes	(Landroid/view/inputmethod/InputMethodInfo;[Landroid/view/inputmethod/InputMethodSubtype;)V
        //     115: invokestatic 893	android/os/Binder:clearCallingIdentity	()J
        //     118: lstore 9
        //     120: aload_0
        //     121: aload_0
        //     122: getfield 254	com/android/server/InputMethodManagerService:mMethodList	Ljava/util/ArrayList;
        //     125: aload_0
        //     126: getfield 259	com/android/server/InputMethodManagerService:mMethodMap	Ljava/util/HashMap;
        //     129: invokevirtual 443	com/android/server/InputMethodManagerService:buildInputMethodListLocked	(Ljava/util/ArrayList;Ljava/util/HashMap;)V
        //     132: lload 9
        //     134: invokestatic 901	android/os/Binder:restoreCallingIdentity	(J)V
        //     137: aload_3
        //     138: monitorexit
        //     139: goto -123 -> 16
        //     142: astore 11
        //     144: lload 9
        //     146: invokestatic 901	android/os/Binder:restoreCallingIdentity	(J)V
        //     149: aload 11
        //     151: athrow
        //     152: aload_3
        //     153: monitorexit
        //     154: goto -138 -> 16
        //     157: iinc 8 1
        //     160: goto -78 -> 82
        //
        // Exception table:
        //     from	to	target	type
        //     24	51	47	finally
        //     54	120	47	finally
        //     132	154	47	finally
        //     120	132	142	finally
    }

    public boolean setCurrentInputMethodSubtype(InputMethodSubtype paramInputMethodSubtype)
    {
        HashMap localHashMap = this.mMethodMap;
        if (paramInputMethodSubtype != null);
        boolean bool;
        try
        {
            if (this.mCurMethodId != null)
            {
                int i = getSubtypeIdFromHashCode((InputMethodInfo)this.mMethodMap.get(this.mCurMethodId), paramInputMethodSubtype.hashCode());
                if (i != -1)
                {
                    setInputMethodLocked(this.mCurMethodId, i);
                    bool = true;
                    break label79;
                }
            }
            bool = false;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
        label79: return bool;
    }

    void setEnabledSessionInMainThread(SessionState paramSessionState)
    {
        if ((this.mEnabledSession == paramSessionState) || (this.mEnabledSession != null));
        try
        {
            this.mEnabledSession.method.setSessionEnabled(this.mEnabledSession.session, false);
            label35: this.mEnabledSession = paramSessionState;
            try
            {
                paramSessionState.method.setSessionEnabled(paramSessionState.session, true);
                label54: return;
            }
            catch (RemoteException localRemoteException1)
            {
                break label54;
            }
        }
        catch (RemoteException localRemoteException2)
        {
            break label35;
        }
    }

    // ERROR //
    public void setImeWindowStatus(IBinder paramIBinder, int paramInt1, int paramInt2)
    {
        // Byte code:
        //     0: invokestatic 881	android/os/Binder:getCallingUid	()I
        //     3: istore 4
        //     5: invokestatic 893	android/os/Binder:clearCallingIdentity	()J
        //     8: lstore 5
        //     10: aload_1
        //     11: ifnull +11 -> 22
        //     14: aload_0
        //     15: getfield 874	com/android/server/InputMethodManagerService:mCurToken	Landroid/os/IBinder;
        //     18: aload_1
        //     19: if_acmpeq +46 -> 65
        //     22: ldc 111
        //     24: new 799	java/lang/StringBuilder
        //     27: dup
        //     28: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     31: ldc_w 1632
        //     34: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     37: iload 4
        //     39: invokevirtual 884	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     42: ldc_w 886
        //     45: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     48: aload_1
        //     49: invokevirtual 889	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     52: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     55: invokestatic 454	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     58: pop
        //     59: lload 5
        //     61: invokestatic 901	android/os/Binder:restoreCallingIdentity	(J)V
        //     64: return
        //     65: aload_0
        //     66: getfield 259	com/android/server/InputMethodManagerService:mMethodMap	Ljava/util/HashMap;
        //     69: astore 9
        //     71: aload 9
        //     73: monitorenter
        //     74: aload_0
        //     75: iload_2
        //     76: putfield 819	com/android/server/InputMethodManagerService:mImeWindowVis	I
        //     79: aload_0
        //     80: iload_3
        //     81: putfield 281	com/android/server/InputMethodManagerService:mBackDisposition	I
        //     84: aload_0
        //     85: getfield 1225	com/android/server/InputMethodManagerService:mStatusBar	Lcom/android/server/StatusBarManagerService;
        //     88: ifnull +279 -> 367
        //     91: aload_0
        //     92: getfield 1225	com/android/server/InputMethodManagerService:mStatusBar	Lcom/android/server/StatusBarManagerService;
        //     95: aload_1
        //     96: iload_2
        //     97: iload_3
        //     98: invokevirtual 1633	com/android/server/StatusBarManagerService:setImeWindowStatus	(Landroid/os/IBinder;II)V
        //     101: goto +266 -> 367
        //     104: aload_0
        //     105: getfield 259	com/android/server/InputMethodManagerService:mMethodMap	Ljava/util/HashMap;
        //     108: aload_0
        //     109: getfield 635	com/android/server/InputMethodManagerService:mCurMethodId	Ljava/lang/String;
        //     112: invokevirtual 554	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     115: checkcast 592	android/view/inputmethod/InputMethodInfo
        //     118: astore 12
        //     120: aload 12
        //     122: ifnull +213 -> 335
        //     125: iload 11
        //     127: ifeq +208 -> 335
        //     130: aload_0
        //     131: invokespecial 1635	com/android/server/InputMethodManagerService:needsToShowImeSwitchOngoingNotification	()Z
        //     134: ifeq +201 -> 335
        //     137: aload_0
        //     138: getfield 283	com/android/server/InputMethodManagerService:mContext	Landroid/content/Context;
        //     141: invokevirtual 944	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
        //     144: astore 13
        //     146: aload_0
        //     147: getfield 291	com/android/server/InputMethodManagerService:mRes	Landroid/content/res/Resources;
        //     150: ldc_w 1636
        //     153: invokevirtual 1640	android/content/res/Resources:getText	(I)Ljava/lang/CharSequence;
        //     156: astore 14
        //     158: aload 12
        //     160: aload 13
        //     162: invokevirtual 1644	android/view/inputmethod/InputMethodInfo:loadLabel	(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
        //     165: astore 15
        //     167: aload_0
        //     168: getfield 642	com/android/server/InputMethodManagerService:mCurrentSubtype	Landroid/view/inputmethod/InputMethodSubtype;
        //     171: ifnull +208 -> 379
        //     174: iconst_2
        //     175: anewarray 1390	java/lang/CharSequence
        //     178: astore 17
        //     180: aload 17
        //     182: iconst_0
        //     183: aload_0
        //     184: getfield 642	com/android/server/InputMethodManagerService:mCurrentSubtype	Landroid/view/inputmethod/InputMethodSubtype;
        //     187: aload_0
        //     188: getfield 283	com/android/server/InputMethodManagerService:mContext	Landroid/content/Context;
        //     191: aload 12
        //     193: invokevirtual 789	android/view/inputmethod/InputMethodInfo:getPackageName	()Ljava/lang/String;
        //     196: aload 12
        //     198: invokevirtual 661	android/view/inputmethod/InputMethodInfo:getServiceInfo	()Landroid/content/pm/ServiceInfo;
        //     201: getfield 667	android/content/pm/ServiceInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     204: invokevirtual 1648	android/view/inputmethod/InputMethodSubtype:getDisplayName	(Landroid/content/Context;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
        //     207: aastore
        //     208: aload 15
        //     210: invokestatic 437	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
        //     213: ifeq +96 -> 309
        //     216: ldc_w 915
        //     219: astore 18
        //     221: aload 17
        //     223: iconst_1
        //     224: aload 18
        //     226: aastore
        //     227: aload 17
        //     229: invokestatic 1652	android/text/TextUtils:concat	([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
        //     232: astore 16
        //     234: aload_0
        //     235: getfield 334	com/android/server/InputMethodManagerService:mImeSwitcherNotification	Landroid/app/Notification;
        //     238: aload_0
        //     239: getfield 283	com/android/server/InputMethodManagerService:mContext	Landroid/content/Context;
        //     242: aload 14
        //     244: aload 16
        //     246: aload_0
        //     247: getfield 378	com/android/server/InputMethodManagerService:mImeSwitchPendingIntent	Landroid/app/PendingIntent;
        //     250: invokevirtual 1656	android/app/Notification:setLatestEventInfo	(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V
        //     253: aload_0
        //     254: getfield 1658	com/android/server/InputMethodManagerService:mNotificationManager	Landroid/app/NotificationManager;
        //     257: ifnull +22 -> 279
        //     260: aload_0
        //     261: getfield 1658	com/android/server/InputMethodManagerService:mNotificationManager	Landroid/app/NotificationManager;
        //     264: ldc_w 1636
        //     267: aload_0
        //     268: getfield 334	com/android/server/InputMethodManagerService:mImeSwitcherNotification	Landroid/app/Notification;
        //     271: invokevirtual 1664	android/app/NotificationManager:notify	(ILandroid/app/Notification;)V
        //     274: aload_0
        //     275: iconst_1
        //     276: putfield 414	com/android/server/InputMethodManagerService:mNotificationShown	Z
        //     279: aload 9
        //     281: monitorexit
        //     282: goto -223 -> 59
        //     285: astore 10
        //     287: aload 9
        //     289: monitorexit
        //     290: aload 10
        //     292: athrow
        //     293: astore 7
        //     295: lload 5
        //     297: invokestatic 901	android/os/Binder:restoreCallingIdentity	(J)V
        //     300: aload 7
        //     302: athrow
        //     303: iconst_0
        //     304: istore 11
        //     306: goto -202 -> 104
        //     309: new 799	java/lang/StringBuilder
        //     312: dup
        //     313: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     316: ldc_w 1666
        //     319: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     322: aload 15
        //     324: invokevirtual 889	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     327: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     330: astore 18
        //     332: goto -111 -> 221
        //     335: aload_0
        //     336: getfield 414	com/android/server/InputMethodManagerService:mNotificationShown	Z
        //     339: ifeq -60 -> 279
        //     342: aload_0
        //     343: getfield 1658	com/android/server/InputMethodManagerService:mNotificationManager	Landroid/app/NotificationManager;
        //     346: ifnull -67 -> 279
        //     349: aload_0
        //     350: getfield 1658	com/android/server/InputMethodManagerService:mNotificationManager	Landroid/app/NotificationManager;
        //     353: ldc_w 1636
        //     356: invokevirtual 1669	android/app/NotificationManager:cancel	(I)V
        //     359: aload_0
        //     360: iconst_0
        //     361: putfield 414	com/android/server/InputMethodManagerService:mNotificationShown	Z
        //     364: goto -85 -> 279
        //     367: iload_2
        //     368: iconst_1
        //     369: iand
        //     370: ifeq -67 -> 303
        //     373: iconst_1
        //     374: istore 11
        //     376: goto -272 -> 104
        //     379: aload 15
        //     381: astore 16
        //     383: goto -149 -> 234
        //
        // Exception table:
        //     from	to	target	type
        //     74	290	285	finally
        //     309	364	285	finally
        //     14	59	293	finally
        //     65	74	293	finally
        //     290	293	293	finally
    }

    public void setInputMethod(IBinder paramIBinder, String paramString)
    {
        setInputMethodWithSubtypeId(paramIBinder, paramString, -1);
    }

    public void setInputMethodAndSubtype(IBinder paramIBinder, String paramString, InputMethodSubtype paramInputMethodSubtype)
    {
        HashMap localHashMap = this.mMethodMap;
        if (paramInputMethodSubtype != null);
        try
        {
            setInputMethodWithSubtypeId(paramIBinder, paramString, getSubtypeIdFromHashCode((InputMethodInfo)this.mMethodMap.get(paramString), paramInputMethodSubtype.hashCode()));
            return;
            setInputMethod(paramIBinder, paramString);
        }
        finally
        {
        }
    }

    // ERROR //
    public boolean setInputMethodEnabled(String paramString, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 259	com/android/server/InputMethodManagerService:mMethodMap	Ljava/util/HashMap;
        //     4: astore_3
        //     5: aload_3
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 283	com/android/server/InputMethodManagerService:mContext	Landroid/content/Context;
        //     11: ldc_w 864
        //     14: invokevirtual 867	android/content/Context:checkCallingOrSelfPermission	(Ljava/lang/String;)I
        //     17: ifeq +21 -> 38
        //     20: new 869	java/lang/SecurityException
        //     23: dup
        //     24: ldc_w 1680
        //     27: invokespecial 872	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     30: athrow
        //     31: astore 4
        //     33: aload_3
        //     34: monitorexit
        //     35: aload 4
        //     37: athrow
        //     38: invokestatic 893	android/os/Binder:clearCallingIdentity	()J
        //     41: lstore 5
        //     43: aload_0
        //     44: aload_1
        //     45: iload_2
        //     46: invokevirtual 1208	com/android/server/InputMethodManagerService:setInputMethodEnabledLocked	(Ljava/lang/String;Z)Z
        //     49: istore 8
        //     51: lload 5
        //     53: invokestatic 901	android/os/Binder:restoreCallingIdentity	(J)V
        //     56: aload_3
        //     57: monitorexit
        //     58: iload 8
        //     60: ireturn
        //     61: astore 7
        //     63: lload 5
        //     65: invokestatic 901	android/os/Binder:restoreCallingIdentity	(J)V
        //     68: aload 7
        //     70: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     7	35	31	finally
        //     38	43	31	finally
        //     51	71	31	finally
        //     43	51	61	finally
    }

    boolean setInputMethodEnabledLocked(String paramString, boolean paramBoolean)
    {
        if ((InputMethodInfo)this.mMethodMap.get(paramString) == null)
            throw new IllegalArgumentException("Unknown id: " + this.mCurMethodId);
        List localList = this.mSettings.getEnabledInputMethodsAndSubtypeListLocked();
        boolean bool;
        if (paramBoolean)
        {
            Iterator localIterator = localList.iterator();
            while (localIterator.hasNext())
                if (((String)((Pair)localIterator.next()).first).equals(paramString))
                    bool = true;
        }
        while (true)
        {
            return bool;
            this.mSettings.appendAndPutEnabledInputMethodLocked(paramString, false);
            bool = false;
            continue;
            StringBuilder localStringBuilder = new StringBuilder();
            if (this.mSettings.buildAndPutEnabledInputMethodsStrRemovingIdLocked(localStringBuilder, localList, paramString))
            {
                if ((paramString.equals(Settings.Secure.getString(this.mContext.getContentResolver(), "default_input_method"))) && (!chooseNewDefaultIMELocked()))
                {
                    Slog.i("InputMethodManagerService", "Can't find new IME, unsetting the current input method.");
                    resetSelectedInputMethodAndSubtypeLocked("");
                }
                bool = true;
            }
            else
            {
                bool = false;
            }
        }
    }

    void setInputMethodLocked(String paramString, int paramInt)
    {
        InputMethodInfo localInputMethodInfo = (InputMethodInfo)this.mMethodMap.get(paramString);
        if (localInputMethodInfo == null)
            throw new IllegalArgumentException("Unknown id: " + paramString);
        int i;
        if (paramString.equals(this.mCurMethodId))
        {
            i = localInputMethodInfo.getSubtypeCount();
            if (i > 0);
        }
        while (true)
        {
            return;
            InputMethodSubtype localInputMethodSubtype1 = this.mCurrentSubtype;
            if ((paramInt >= 0) && (paramInt < i));
            for (InputMethodSubtype localInputMethodSubtype2 = localInputMethodInfo.getSubtypeAt(paramInt); ; localInputMethodSubtype2 = getCurrentInputMethodSubtype())
            {
                if ((localInputMethodSubtype2 != null) && (localInputMethodSubtype1 != null))
                    break label150;
                Slog.w("InputMethodManagerService", "Illegal subtype state: old subtype = " + localInputMethodSubtype1 + ", new subtype = " + localInputMethodSubtype2);
                break;
            }
            label150: if (localInputMethodSubtype2 == localInputMethodSubtype1)
                continue;
            setSelectedInputMethodAndSubtypeLocked(localInputMethodInfo, paramInt, true);
            if (this.mCurMethod == null)
                continue;
            try
            {
                refreshImeWindowVisibilityLocked();
                this.mCurMethod.changeInputMethodSubtype(localInputMethodSubtype2);
            }
            catch (RemoteException localRemoteException)
            {
                Slog.w("InputMethodManagerService", "Failed to call changeInputMethodSubtype");
            }
            continue;
            long l = Binder.clearCallingIdentity();
            try
            {
                setSelectedInputMethodAndSubtypeLocked(localInputMethodInfo, paramInt, false);
                this.mCurMethodId = paramString;
                if (ActivityManagerNative.isSystemReady())
                {
                    Intent localIntent = new Intent("android.intent.action.INPUT_METHOD_CHANGED");
                    localIntent.addFlags(536870912);
                    localIntent.putExtra("input_method_id", paramString);
                    this.mContext.sendBroadcast(localIntent);
                }
                unbindCurrentClientLocked();
                Binder.restoreCallingIdentity(l);
            }
            finally
            {
                Binder.restoreCallingIdentity(l);
            }
        }
    }

    boolean showCurrentInputLocked(int paramInt, ResultReceiver paramResultReceiver)
    {
        this.mShowRequested = true;
        if ((paramInt & 0x1) == 0)
            this.mShowExplicitlyRequested = true;
        if ((paramInt & 0x2) != 0)
        {
            this.mShowExplicitlyRequested = true;
            this.mShowForced = true;
        }
        boolean bool;
        if (!this.mSystemReady)
            bool = false;
        while (true)
        {
            return bool;
            bool = false;
            if (this.mCurMethod != null)
            {
                executeOrSendMessage(this.mCurMethod, this.mCaller.obtainMessageIOO(1020, getImeShowFlags(), this.mCurMethod, paramResultReceiver));
                this.mInputShown = true;
                if ((this.mHaveConnection) && (!this.mVisibleBound))
                {
                    this.mContext.bindService(this.mCurIntent, this.mVisibleConnection, 1);
                    this.mVisibleBound = true;
                }
                bool = true;
            }
            else if ((this.mHaveConnection) && (SystemClock.uptimeMillis() >= 10000L + this.mLastBindTime))
            {
                Object[] arrayOfObject = new Object[3];
                arrayOfObject[0] = this.mCurMethodId;
                arrayOfObject[1] = Long.valueOf(SystemClock.uptimeMillis() - this.mLastBindTime);
                arrayOfObject[2] = Integer.valueOf(1);
                EventLog.writeEvent(32000, arrayOfObject);
                Slog.w("InputMethodManagerService", "Force disconnect/connect to the IME in showCurrentInputLocked()");
                this.mContext.unbindService(this);
                this.mContext.bindService(this.mCurIntent, this, 1073741825);
            }
        }
    }

    public void showInputMethodAndSubtypeEnablerFromClient(IInputMethodClient paramIInputMethodClient, String paramString)
    {
        synchronized (this.mMethodMap)
        {
            if ((this.mCurClient == null) || (paramIInputMethodClient == null) || (this.mCurClient.client.asBinder() != paramIInputMethodClient.asBinder()))
                Slog.w("InputMethodManagerService", "Ignoring showInputMethodAndSubtypeEnablerFromClient of: " + paramIInputMethodClient);
            executeOrSendMessage(this.mCurMethod, this.mCaller.obtainMessageO(3, paramString));
            return;
        }
    }

    public void showInputMethodPickerFromClient(IInputMethodClient paramIInputMethodClient)
    {
        synchronized (this.mMethodMap)
        {
            if ((this.mCurClient == null) || (paramIInputMethodClient == null) || (this.mCurClient.client.asBinder() != paramIInputMethodClient.asBinder()))
                Slog.w("InputMethodManagerService", "Ignoring showInputMethodPickerFromClient of uid " + Binder.getCallingUid() + ": " + paramIInputMethodClient);
            this.mHandler.sendEmptyMessage(2);
            return;
        }
    }

    // ERROR //
    public void showMySoftInput(IBinder paramIBinder, int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 259	com/android/server/InputMethodManagerService:mMethodMap	Ljava/util/HashMap;
        //     4: astore_3
        //     5: aload_3
        //     6: monitorenter
        //     7: aload_1
        //     8: ifnull +11 -> 19
        //     11: aload_0
        //     12: getfield 874	com/android/server/InputMethodManagerService:mCurToken	Landroid/os/IBinder;
        //     15: aload_1
        //     16: if_acmpeq +46 -> 62
        //     19: ldc 111
        //     21: new 799	java/lang/StringBuilder
        //     24: dup
        //     25: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     28: ldc_w 1770
        //     31: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     34: invokestatic 881	android/os/Binder:getCallingUid	()I
        //     37: invokevirtual 884	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     40: ldc_w 886
        //     43: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     46: aload_1
        //     47: invokevirtual 889	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     50: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     53: invokestatic 454	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     56: pop
        //     57: aload_3
        //     58: monitorexit
        //     59: goto +42 -> 101
        //     62: invokestatic 893	android/os/Binder:clearCallingIdentity	()J
        //     65: lstore 6
        //     67: aload_0
        //     68: iload_2
        //     69: aconst_null
        //     70: invokevirtual 1146	com/android/server/InputMethodManagerService:showCurrentInputLocked	(ILandroid/os/ResultReceiver;)Z
        //     73: pop
        //     74: lload 6
        //     76: invokestatic 901	android/os/Binder:restoreCallingIdentity	(J)V
        //     79: aload_3
        //     80: monitorexit
        //     81: goto +20 -> 101
        //     84: astore 4
        //     86: aload_3
        //     87: monitorexit
        //     88: aload 4
        //     90: athrow
        //     91: astore 8
        //     93: lload 6
        //     95: invokestatic 901	android/os/Binder:restoreCallingIdentity	(J)V
        //     98: aload 8
        //     100: athrow
        //     101: return
        //
        // Exception table:
        //     from	to	target	type
        //     11	67	84	finally
        //     74	88	84	finally
        //     93	101	84	finally
        //     67	74	91	finally
    }

    public boolean showSoftInput(IInputMethodClient paramIInputMethodClient, int paramInt, ResultReceiver paramResultReceiver)
    {
        boolean bool = false;
        int i = Binder.getCallingUid();
        long l = Binder.clearCallingIdentity();
        while (true)
        {
            try
            {
                synchronized (this.mMethodMap)
                {
                    if ((this.mCurClient != null) && (paramIInputMethodClient != null))
                    {
                        IBinder localIBinder1 = this.mCurClient.client.asBinder();
                        IBinder localIBinder2 = paramIInputMethodClient.asBinder();
                        if (localIBinder1 == localIBinder2);
                    }
                    else
                    {
                        try
                        {
                            if (!this.mIWindowManager.inputMethodClientHasFocus(paramIInputMethodClient))
                            {
                                Slog.w("InputMethodManagerService", "Ignoring showSoftInput of uid " + i + ": " + paramIInputMethodClient);
                                return bool;
                            }
                        }
                        catch (RemoteException localRemoteException)
                        {
                            continue;
                        }
                    }
                }
            }
            finally
            {
                Binder.restoreCallingIdentity(l);
            }
            bool = showCurrentInputLocked(paramInt, paramResultReceiver);
        }
    }

    public InputBindResult startInput(IInputMethodClient paramIInputMethodClient, IInputContext paramIInputContext, EditorInfo paramEditorInfo, int paramInt)
    {
        synchronized (this.mMethodMap)
        {
            long l = Binder.clearCallingIdentity();
            try
            {
                InputBindResult localInputBindResult = startInputLocked(paramIInputMethodClient, paramIInputContext, paramEditorInfo, paramInt);
                Binder.restoreCallingIdentity(l);
                return localInputBindResult;
            }
            finally
            {
                localObject2 = finally;
                Binder.restoreCallingIdentity(l);
                throw localObject2;
            }
        }
    }

    InputBindResult startInputInnerLocked()
    {
        InputBindResult localInputBindResult;
        if (this.mCurMethodId == null)
            localInputBindResult = this.mNoBinding;
        while (true)
        {
            return localInputBindResult;
            if (!this.mSystemReady)
            {
                localInputBindResult = new InputBindResult(null, this.mCurMethodId, this.mCurSeq);
                continue;
            }
            InputMethodInfo localInputMethodInfo = (InputMethodInfo)this.mMethodMap.get(this.mCurMethodId);
            if (localInputMethodInfo == null)
                throw new IllegalArgumentException("Unknown id: " + this.mCurMethodId);
            unbindCurrentMethodLocked(false);
            this.mCurIntent = new Intent("android.view.InputMethod");
            this.mCurIntent.setComponent(localInputMethodInfo.getComponent());
            this.mCurIntent.putExtra("android.intent.extra.client_label", 17040501);
            this.mCurIntent.putExtra("android.intent.extra.client_intent", PendingIntent.getActivity(this.mContext, 0, new Intent("android.settings.INPUT_METHOD_SETTINGS"), 0));
            if (this.mContext.bindService(this.mCurIntent, this, 1073741825))
            {
                this.mLastBindTime = SystemClock.uptimeMillis();
                this.mHaveConnection = true;
                this.mCurId = localInputMethodInfo.getId();
                this.mCurToken = new Binder();
            }
            try
            {
                this.mIWindowManager.addWindowToken(this.mCurToken, 2011);
                label231: localInputBindResult = new InputBindResult(null, this.mCurId, this.mCurSeq);
                continue;
                this.mCurIntent = null;
                Slog.w("InputMethodManagerService", "Failure connecting to input method service: " + this.mCurIntent);
                localInputBindResult = null;
            }
            catch (RemoteException localRemoteException)
            {
                break label231;
            }
        }
    }

    InputBindResult startInputLocked(IInputMethodClient paramIInputMethodClient, IInputContext paramIInputContext, EditorInfo paramEditorInfo, int paramInt)
    {
        InputBindResult localInputBindResult;
        if (this.mCurMethodId == null)
            localInputBindResult = this.mNoBinding;
        while (true)
        {
            return localInputBindResult;
            ClientState localClientState = (ClientState)this.mClients.get(paramIInputMethodClient.asBinder());
            if (localClientState == null)
                throw new IllegalArgumentException("unknown client " + paramIInputMethodClient.asBinder());
            try
            {
                if (!this.mIWindowManager.inputMethodClientHasFocus(localClientState.client))
                {
                    Slog.w("InputMethodManagerService", "Starting input on non-focused client " + localClientState.client + " (uid=" + localClientState.uid + " pid=" + localClientState.pid + ")");
                    localInputBindResult = null;
                }
            }
            catch (RemoteException localRemoteException)
            {
                localInputBindResult = startInputUncheckedLocked(localClientState, paramIInputContext, paramEditorInfo, paramInt);
            }
        }
    }

    InputBindResult startInputUncheckedLocked(ClientState paramClientState, IInputContext paramIInputContext, EditorInfo paramEditorInfo, int paramInt)
    {
        int i = 1;
        InputBindResult localInputBindResult;
        if (this.mCurMethodId == null)
            localInputBindResult = this.mNoBinding;
        while (true)
        {
            return localInputBindResult;
            if (this.mCurClient != paramClientState)
            {
                unbindCurrentClientLocked();
                if (this.mScreenOn)
                {
                    IInputMethodClient localIInputMethodClient = paramClientState.client;
                    HandlerCaller localHandlerCaller = this.mCaller;
                    if (!this.mScreenOn)
                        break label164;
                    int j = i;
                    label61: executeOrSendMessage(localIInputMethodClient, localHandlerCaller.obtainMessageIO(3020, j, paramClientState));
                }
            }
            this.mCurSeq = (1 + this.mCurSeq);
            if (this.mCurSeq <= 0)
                this.mCurSeq = i;
            this.mCurClient = paramClientState;
            this.mCurInputContext = paramIInputContext;
            this.mCurAttribute = paramEditorInfo;
            if ((this.mCurId != null) && (this.mCurId.equals(this.mCurMethodId)))
            {
                if (paramClientState.curSession != null)
                {
                    if ((paramInt & 0x100) != 0);
                    while (true)
                    {
                        localInputBindResult = attachNewInputLocked(i);
                        break;
                        label164: int k = 0;
                        break label61;
                        i = 0;
                    }
                }
                if (this.mHaveConnection)
                {
                    if (this.mCurMethod != null)
                    {
                        if (!paramClientState.sessionRequested)
                        {
                            paramClientState.sessionRequested = i;
                            executeOrSendMessage(this.mCurMethod, this.mCaller.obtainMessageOO(1050, this.mCurMethod, new MethodCallback(this.mCurMethod, this)));
                        }
                        localInputBindResult = new InputBindResult(null, this.mCurId, this.mCurSeq);
                        continue;
                    }
                    if (SystemClock.uptimeMillis() < 10000L + this.mLastBindTime)
                    {
                        localInputBindResult = new InputBindResult(null, this.mCurId, this.mCurSeq);
                        continue;
                    }
                    Object[] arrayOfObject = new Object[3];
                    arrayOfObject[0] = this.mCurMethodId;
                    arrayOfObject[i] = Long.valueOf(SystemClock.uptimeMillis() - this.mLastBindTime);
                    arrayOfObject[2] = Integer.valueOf(0);
                    EventLog.writeEvent(32000, arrayOfObject);
                }
            }
            else
            {
                localInputBindResult = startInputInnerLocked();
            }
        }
    }

    public boolean switchToLastInputMethod(IBinder paramIBinder)
    {
        while (true)
        {
            InputMethodInfo localInputMethodInfo1;
            boolean bool2;
            int m;
            int n;
            int k;
            boolean bool1;
            synchronized (this.mMethodMap)
            {
                Pair localPair = this.mSettings.getLastInputMethodAndSubtypeLocked();
                if (localPair == null)
                    break label364;
                localInputMethodInfo1 = (InputMethodInfo)this.mMethodMap.get(localPair.first);
                String str1 = null;
                int i = -1;
                if ((localPair != null) && (localInputMethodInfo1 != null))
                {
                    bool2 = localInputMethodInfo1.getId().equals(this.mCurMethodId);
                    m = Integer.valueOf((String)localPair.second).intValue();
                    if (this.mCurrentSubtype == null)
                    {
                        n = -1;
                        break label340;
                        str1 = (String)localPair.first;
                        i = getSubtypeIdFromHashCode(localInputMethodInfo1, m);
                    }
                }
                else
                {
                    if ((TextUtils.isEmpty(str1)) && (!canAddToLastInputMethod(this.mCurrentSubtype)))
                    {
                        List localList = this.mSettings.getEnabledInputMethodListLocked();
                        if (localList != null)
                        {
                            int j = localList.size();
                            if (this.mCurrentSubtype != null)
                                continue;
                            str2 = this.mRes.getConfiguration().locale.toString();
                            break label355;
                            if (k < j)
                            {
                                InputMethodInfo localInputMethodInfo2 = (InputMethodInfo)localList.get(k);
                                if ((localInputMethodInfo2.getSubtypeCount() <= 0) || (!isSystemIme(localInputMethodInfo2)))
                                    break label370;
                                InputMethodSubtype localInputMethodSubtype = findLastResortApplicableSubtypeLocked(this.mRes, getSubtypes(localInputMethodInfo2), "keyboard", str2, true);
                                if (localInputMethodSubtype == null)
                                    break label370;
                                str1 = localInputMethodInfo2.getId();
                                i = getSubtypeIdFromHashCode(localInputMethodInfo2, localInputMethodSubtype.hashCode());
                                if (!localInputMethodSubtype.getLocale().equals(str2))
                                    break label370;
                            }
                        }
                    }
                    if (TextUtils.isEmpty(str1))
                        continue;
                    setInputMethodWithSubtypeId(paramIBinder, str1, i);
                    bool1 = true;
                    break label361;
                }
                n = this.mCurrentSubtype.hashCode();
                break label340;
                String str2 = this.mCurrentSubtype.getLocale();
                break label355;
                bool1 = false;
            }
            label340: if (bool2)
                if (m != n)
                {
                    continue;
                    label355: k = 0;
                    continue;
                    label361: return bool1;
                    label364: localInputMethodInfo1 = null;
                    continue;
                    label370: k++;
                }
        }
    }

    public boolean switchToNextInputMethod(IBinder paramIBinder, boolean paramBoolean)
    {
        boolean bool;
        synchronized (this.mMethodMap)
        {
            ImeSubtypeListItem localImeSubtypeListItem = this.mImListManager.getNextInputMethod(paramBoolean, (InputMethodInfo)this.mMethodMap.get(this.mCurMethodId), this.mCurrentSubtype);
            if (localImeSubtypeListItem == null)
            {
                bool = false;
            }
            else
            {
                setInputMethodWithSubtypeId(paramIBinder, localImeSubtypeListItem.mImi.getId(), localImeSubtypeListItem.mSubtypeId);
                bool = true;
            }
        }
        return bool;
    }

    public void systemReady(StatusBarManagerService paramStatusBarManagerService)
    {
        synchronized (this.mMethodMap)
        {
            if (!this.mSystemReady)
            {
                this.mSystemReady = true;
                this.mKeyguardManager = ((KeyguardManager)this.mContext.getSystemService("keyguard"));
                this.mNotificationManager = ((NotificationManager)this.mContext.getSystemService("notification"));
                this.mStatusBar = paramStatusBarManagerService;
                paramStatusBarManagerService.setIconVisibility("ime", false);
                updateImeWindowStatusLocked();
                this.mShowOngoingImeSwitcherForPhones = this.mRes.getBoolean(17891332);
                if (this.mShowOngoingImeSwitcherForPhones)
                    this.mWindowManagerService.setOnHardKeyboardStatusChangeListener(this.mHardKeyboardListener);
                buildInputMethodListLocked(this.mMethodList, this.mMethodMap);
                if (!this.mImeSelectedOnBoot)
                {
                    Slog.w("InputMethodManagerService", "Reset the default IME as \"Resource\" is ready here.");
                    checkCurrentLocaleChangedLocked();
                }
                this.mLastSystemLocale = this.mRes.getConfiguration().locale;
            }
            try
            {
                startInputInnerLocked();
                return;
            }
            catch (RuntimeException localRuntimeException)
            {
                while (true)
                    Slog.w("InputMethodManagerService", "Unexpected exception", localRuntimeException);
            }
        }
    }

    void unbindCurrentClientLocked()
    {
        if (this.mCurClient != null)
        {
            if (this.mBoundToMethod)
            {
                this.mBoundToMethod = false;
                if (this.mCurMethod != null)
                    executeOrSendMessage(this.mCurMethod, this.mCaller.obtainMessageO(1000, this.mCurMethod));
            }
            executeOrSendMessage(this.mCurClient.client, this.mCaller.obtainMessageIO(3020, 0, this.mCurClient));
            executeOrSendMessage(this.mCurClient.client, this.mCaller.obtainMessageIO(3000, this.mCurSeq, this.mCurClient.client));
            this.mCurClient.sessionRequested = false;
            this.mCurClient = null;
            hideInputMethodMenuLocked();
        }
    }

    void unbindCurrentMethodLocked(boolean paramBoolean)
    {
        if (this.mVisibleBound)
        {
            this.mContext.unbindService(this.mVisibleConnection);
            this.mVisibleBound = false;
        }
        if (this.mHaveConnection)
        {
            this.mContext.unbindService(this);
            this.mHaveConnection = false;
        }
        if (this.mCurToken != null);
        try
        {
            if ((0x1 & this.mImeWindowVis) != 0)
                this.mWindowManagerService.saveLastInputMethodWindowForTransition();
            this.mIWindowManager.removeWindowToken(this.mCurToken);
            label79: this.mCurToken = null;
            this.mCurId = null;
            clearCurMethodLocked();
            if ((paramBoolean) && (this.mCurClient != null))
                executeOrSendMessage(this.mCurClient.client, this.mCaller.obtainMessageIO(3000, this.mCurSeq, this.mCurClient.client));
            return;
        }
        catch (RemoteException localRemoteException)
        {
            break label79;
        }
    }

    void updateFromSettingsLocked()
    {
        String str = Settings.Secure.getString(this.mContext.getContentResolver(), "default_input_method");
        if ((TextUtils.isEmpty(str)) && (chooseNewDefaultIMELocked()))
            str = Settings.Secure.getString(this.mContext.getContentResolver(), "default_input_method");
        if (!TextUtils.isEmpty(str));
        while (true)
        {
            try
            {
                setInputMethodLocked(str, getSelectedInputMethodSubtypeId(str));
                this.mShortcutInputMethodsAndSubtypes.clear();
                return;
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
                Slog.w("InputMethodManagerService", "Unknown input method from prefs: " + str, localIllegalArgumentException);
                this.mCurMethodId = null;
                unbindCurrentMethodLocked(true);
                continue;
            }
            this.mCurMethodId = null;
            unbindCurrentMethodLocked(true);
        }
    }

    // ERROR //
    public void updateStatusIcon(IBinder paramIBinder, String paramString, int paramInt)
    {
        // Byte code:
        //     0: invokestatic 881	android/os/Binder:getCallingUid	()I
        //     3: istore 4
        //     5: invokestatic 893	android/os/Binder:clearCallingIdentity	()J
        //     8: lstore 5
        //     10: aload_1
        //     11: ifnull +11 -> 22
        //     14: aload_0
        //     15: getfield 874	com/android/server/InputMethodManagerService:mCurToken	Landroid/os/IBinder;
        //     18: aload_1
        //     19: if_acmpeq +46 -> 65
        //     22: ldc 111
        //     24: new 799	java/lang/StringBuilder
        //     27: dup
        //     28: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     31: ldc_w 876
        //     34: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     37: iload 4
        //     39: invokevirtual 884	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     42: ldc_w 886
        //     45: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     48: aload_1
        //     49: invokevirtual 889	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     52: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     55: invokestatic 454	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     58: pop
        //     59: lload 5
        //     61: invokestatic 901	android/os/Binder:restoreCallingIdentity	(J)V
        //     64: return
        //     65: aload_0
        //     66: getfield 259	com/android/server/InputMethodManagerService:mMethodMap	Ljava/util/HashMap;
        //     69: astore 9
        //     71: aload 9
        //     73: monitorenter
        //     74: iload_3
        //     75: ifne +45 -> 120
        //     78: aload_0
        //     79: getfield 1225	com/android/server/InputMethodManagerService:mStatusBar	Lcom/android/server/StatusBarManagerService;
        //     82: ifnull +14 -> 96
        //     85: aload_0
        //     86: getfield 1225	com/android/server/InputMethodManagerService:mStatusBar	Lcom/android/server/StatusBarManagerService;
        //     89: ldc_w 1227
        //     92: iconst_0
        //     93: invokevirtual 1233	com/android/server/StatusBarManagerService:setIconVisibility	(Ljava/lang/String;Z)V
        //     96: aload 9
        //     98: monitorexit
        //     99: goto -40 -> 59
        //     102: astore 14
        //     104: aload 9
        //     106: monitorexit
        //     107: aload 14
        //     109: athrow
        //     110: astore 7
        //     112: lload 5
        //     114: invokestatic 901	android/os/Binder:restoreCallingIdentity	(J)V
        //     117: aload 7
        //     119: athrow
        //     120: aload_2
        //     121: ifnull -25 -> 96
        //     124: aconst_null
        //     125: astore 10
        //     127: aload_0
        //     128: getfield 283	com/android/server/InputMethodManagerService:mContext	Landroid/content/Context;
        //     131: invokevirtual 944	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
        //     134: astore 15
        //     136: aload 15
        //     138: aload 15
        //     140: aload_2
        //     141: iconst_0
        //     142: invokevirtual 1852	android/content/pm/PackageManager:getApplicationInfo	(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
        //     145: invokevirtual 1856	android/content/pm/PackageManager:getApplicationLabel	(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
        //     148: astore 16
        //     150: aload 16
        //     152: astore 10
        //     154: aload_0
        //     155: getfield 1225	com/android/server/InputMethodManagerService:mStatusBar	Lcom/android/server/StatusBarManagerService;
        //     158: ifnull -62 -> 96
        //     161: aload_0
        //     162: getfield 1225	com/android/server/InputMethodManagerService:mStatusBar	Lcom/android/server/StatusBarManagerService;
        //     165: astore 12
        //     167: aload 10
        //     169: ifnull +37 -> 206
        //     172: aload 10
        //     174: invokevirtual 1857	java/lang/Object:toString	()Ljava/lang/String;
        //     177: astore 13
        //     179: aload 12
        //     181: ldc_w 1227
        //     184: aload_2
        //     185: iload_3
        //     186: iconst_0
        //     187: aload 13
        //     189: invokevirtual 1860	com/android/server/StatusBarManagerService:setIcon	(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
        //     192: aload_0
        //     193: getfield 1225	com/android/server/InputMethodManagerService:mStatusBar	Lcom/android/server/StatusBarManagerService;
        //     196: ldc_w 1227
        //     199: iconst_1
        //     200: invokevirtual 1233	com/android/server/StatusBarManagerService:setIconVisibility	(Ljava/lang/String;Z)V
        //     203: goto -107 -> 96
        //     206: aconst_null
        //     207: astore 13
        //     209: goto -30 -> 179
        //     212: astore 11
        //     214: goto -60 -> 154
        //
        // Exception table:
        //     from	to	target	type
        //     78	107	102	finally
        //     127	150	102	finally
        //     154	203	102	finally
        //     14	59	110	finally
        //     65	74	110	finally
        //     107	110	110	finally
        //     127	150	212	android/content/pm/PackageManager$NameNotFoundException
    }

    // ERROR //
    public InputBindResult windowGainedFocus(IInputMethodClient paramIInputMethodClient, IBinder paramIBinder, int paramInt1, int paramInt2, int paramInt3, EditorInfo paramEditorInfo, IInputContext paramIInputContext)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore 8
        //     3: invokestatic 893	android/os/Binder:clearCallingIdentity	()J
        //     6: lstore 9
        //     8: aload_0
        //     9: getfield 259	com/android/server/InputMethodManagerService:mMethodMap	Ljava/util/HashMap;
        //     12: astore 12
        //     14: aload 12
        //     16: monitorenter
        //     17: aload_0
        //     18: getfield 275	com/android/server/InputMethodManagerService:mClients	Ljava/util/HashMap;
        //     21: aload_1
        //     22: invokeinterface 1101 1 0
        //     27: invokevirtual 554	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     30: checkcast 54	com/android/server/InputMethodManagerService$ClientState
        //     33: astore 14
        //     35: aload 14
        //     37: ifnonnull +54 -> 91
        //     40: new 1682	java/lang/IllegalArgumentException
        //     43: dup
        //     44: new 799	java/lang/StringBuilder
        //     47: dup
        //     48: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     51: ldc_w 1802
        //     54: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     57: aload_1
        //     58: invokeinterface 1101 1 0
        //     63: invokevirtual 889	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     66: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     69: invokespecial 1685	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     72: athrow
        //     73: astore 13
        //     75: aload 12
        //     77: monitorexit
        //     78: aload 13
        //     80: athrow
        //     81: astore 11
        //     83: lload 9
        //     85: invokestatic 901	android/os/Binder:restoreCallingIdentity	(J)V
        //     88: aload 11
        //     90: athrow
        //     91: aload_0
        //     92: getfield 314	com/android/server/InputMethodManagerService:mIWindowManager	Landroid/view/IWindowManager;
        //     95: aload 14
        //     97: getfield 1281	com/android/server/InputMethodManagerService$ClientState:client	Lcom/android/internal/view/IInputMethodClient;
        //     100: invokeinterface 1518 2 0
        //     105: ifne +83 -> 188
        //     108: ldc 111
        //     110: new 799	java/lang/StringBuilder
        //     113: dup
        //     114: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     117: ldc_w 1864
        //     120: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     123: aload 14
        //     125: getfield 1281	com/android/server/InputMethodManagerService$ClientState:client	Lcom/android/internal/view/IInputMethodClient;
        //     128: invokevirtual 889	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     131: ldc_w 1806
        //     134: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     137: aload 14
        //     139: getfield 1492	com/android/server/InputMethodManagerService$ClientState:uid	I
        //     142: invokevirtual 884	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     145: ldc_w 1808
        //     148: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     151: aload 14
        //     153: getfield 1487	com/android/server/InputMethodManagerService$ClientState:pid	I
        //     156: invokevirtual 884	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     159: ldc_w 1810
        //     162: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     165: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     168: invokestatic 454	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     171: pop
        //     172: aconst_null
        //     173: astore 20
        //     175: aload 12
        //     177: monitorexit
        //     178: lload 9
        //     180: invokestatic 901	android/os/Binder:restoreCallingIdentity	(J)V
        //     183: aload 20
        //     185: areturn
        //     186: astore 15
        //     188: aload_0
        //     189: getfield 1303	com/android/server/InputMethodManagerService:mCurFocusedWindow	Landroid/os/IBinder;
        //     192: aload_2
        //     193: if_acmpne +73 -> 266
        //     196: ldc 111
        //     198: new 799	java/lang/StringBuilder
        //     201: dup
        //     202: invokespecial 800	java/lang/StringBuilder:<init>	()V
        //     205: ldc_w 1866
        //     208: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     211: aload_1
        //     212: invokevirtual 889	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     215: ldc_w 1868
        //     218: invokevirtual 806	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     221: aload 6
        //     223: invokevirtual 889	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     226: invokevirtual 807	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     229: invokestatic 454	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     232: pop
        //     233: aload 6
        //     235: ifnull +22 -> 257
        //     238: aload_0
        //     239: aload 14
        //     241: aload 7
        //     243: aload 6
        //     245: iload_3
        //     246: invokevirtual 1814	com/android/server/InputMethodManagerService:startInputUncheckedLocked	(Lcom/android/server/InputMethodManagerService$ClientState;Lcom/android/internal/view/IInputContext;Landroid/view/inputmethod/EditorInfo;I)Lcom/android/internal/view/InputBindResult;
        //     249: astore 20
        //     251: aload 12
        //     253: monitorexit
        //     254: goto -76 -> 178
        //     257: aconst_null
        //     258: astore 20
        //     260: aload 12
        //     262: monitorexit
        //     263: goto -85 -> 178
        //     266: aload_0
        //     267: aload_2
        //     268: putfield 1303	com/android/server/InputMethodManagerService:mCurFocusedWindow	Landroid/os/IBinder;
        //     271: iload 4
        //     273: sipush 240
        //     276: iand
        //     277: bipush 16
        //     279: if_icmpeq +248 -> 527
        //     282: aload_0
        //     283: getfield 291	com/android/server/InputMethodManagerService:mRes	Landroid/content/res/Resources;
        //     286: invokevirtual 576	android/content/res/Resources:getConfiguration	()Landroid/content/res/Configuration;
        //     289: iconst_3
        //     290: invokevirtual 1871	android/content/res/Configuration:isLayoutSizeAtLeast	(I)Z
        //     293: ifeq +44 -> 337
        //     296: goto +231 -> 527
        //     299: iload 18
        //     301: ifne +21 -> 322
        //     304: aload 6
        //     306: ifnull +16 -> 322
        //     309: aload_0
        //     310: aload 14
        //     312: aload 7
        //     314: aload 6
        //     316: iload_3
        //     317: invokevirtual 1814	com/android/server/InputMethodManagerService:startInputUncheckedLocked	(Lcom/android/server/InputMethodManagerService$ClientState;Lcom/android/internal/view/IInputContext;Landroid/view/inputmethod/EditorInfo;I)Lcom/android/internal/view/InputBindResult;
        //     320: astore 8
        //     322: aload 12
        //     324: monitorexit
        //     325: lload 9
        //     327: invokestatic 901	android/os/Binder:restoreCallingIdentity	(J)V
        //     330: aload 8
        //     332: astore 20
        //     334: goto -151 -> 183
        //     337: iconst_0
        //     338: istore 16
        //     340: goto +190 -> 530
        //     343: iconst_0
        //     344: istore 17
        //     346: goto +193 -> 539
        //     349: iload 17
        //     351: ifeq +8 -> 359
        //     354: iload 16
        //     356: ifne +21 -> 377
        //     359: iload 5
        //     361: invokestatic 1874	android/view/WindowManager$LayoutParams:mayUseInputMethod	(I)Z
        //     364: ifeq -65 -> 299
        //     367: aload_0
        //     368: iconst_2
        //     369: aconst_null
        //     370: invokevirtual 1511	com/android/server/InputMethodManagerService:hideCurrentInputLocked	(ILandroid/os/ResultReceiver;)Z
        //     373: pop
        //     374: goto -75 -> 299
        //     377: iload 17
        //     379: ifeq -80 -> 299
        //     382: iload 16
        //     384: ifeq -85 -> 299
        //     387: iload 4
        //     389: sipush 256
        //     392: iand
        //     393: ifeq -94 -> 299
        //     396: aload 6
        //     398: ifnull +19 -> 417
        //     401: aload_0
        //     402: aload 14
        //     404: aload 7
        //     406: aload 6
        //     408: iload_3
        //     409: invokevirtual 1814	com/android/server/InputMethodManagerService:startInputUncheckedLocked	(Lcom/android/server/InputMethodManagerService$ClientState;Lcom/android/internal/view/IInputContext;Landroid/view/inputmethod/EditorInfo;I)Lcom/android/internal/view/InputBindResult;
        //     412: astore 8
        //     414: iconst_1
        //     415: istore 18
        //     417: aload_0
        //     418: iconst_1
        //     419: aconst_null
        //     420: invokevirtual 1146	com/android/server/InputMethodManagerService:showCurrentInputLocked	(ILandroid/os/ResultReceiver;)Z
        //     423: pop
        //     424: goto -125 -> 299
        //     427: iload 4
        //     429: sipush 256
        //     432: iand
        //     433: ifeq -134 -> 299
        //     436: aload_0
        //     437: iconst_0
        //     438: aconst_null
        //     439: invokevirtual 1511	com/android/server/InputMethodManagerService:hideCurrentInputLocked	(ILandroid/os/ResultReceiver;)Z
        //     442: pop
        //     443: goto -144 -> 299
        //     446: aload_0
        //     447: iconst_0
        //     448: aconst_null
        //     449: invokevirtual 1511	com/android/server/InputMethodManagerService:hideCurrentInputLocked	(ILandroid/os/ResultReceiver;)Z
        //     452: pop
        //     453: goto -154 -> 299
        //     456: iload 4
        //     458: sipush 256
        //     461: iand
        //     462: ifeq -163 -> 299
        //     465: aload 6
        //     467: ifnull +19 -> 486
        //     470: aload_0
        //     471: aload 14
        //     473: aload 7
        //     475: aload 6
        //     477: iload_3
        //     478: invokevirtual 1814	com/android/server/InputMethodManagerService:startInputUncheckedLocked	(Lcom/android/server/InputMethodManagerService$ClientState;Lcom/android/internal/view/IInputContext;Landroid/view/inputmethod/EditorInfo;I)Lcom/android/internal/view/InputBindResult;
        //     481: astore 8
        //     483: iconst_1
        //     484: istore 18
        //     486: aload_0
        //     487: iconst_1
        //     488: aconst_null
        //     489: invokevirtual 1146	com/android/server/InputMethodManagerService:showCurrentInputLocked	(ILandroid/os/ResultReceiver;)Z
        //     492: pop
        //     493: goto -194 -> 299
        //     496: aload 6
        //     498: ifnull +19 -> 517
        //     501: aload_0
        //     502: aload 14
        //     504: aload 7
        //     506: aload 6
        //     508: iload_3
        //     509: invokevirtual 1814	com/android/server/InputMethodManagerService:startInputUncheckedLocked	(Lcom/android/server/InputMethodManagerService$ClientState;Lcom/android/internal/view/IInputContext;Landroid/view/inputmethod/EditorInfo;I)Lcom/android/internal/view/InputBindResult;
        //     512: astore 8
        //     514: iconst_1
        //     515: istore 18
        //     517: aload_0
        //     518: iconst_1
        //     519: aconst_null
        //     520: invokevirtual 1146	com/android/server/InputMethodManagerService:showCurrentInputLocked	(ILandroid/os/ResultReceiver;)Z
        //     523: pop
        //     524: goto -225 -> 299
        //     527: iconst_1
        //     528: istore 16
        //     530: iload_3
        //     531: iconst_2
        //     532: iand
        //     533: ifeq -190 -> 343
        //     536: iconst_1
        //     537: istore 17
        //     539: iconst_0
        //     540: istore 18
        //     542: iload 4
        //     544: bipush 15
        //     546: iand
        //     547: tableswitch	default:+-248 -> 299, 0:+-198->349, 1:+-248->299, 2:+-120->427, 3:+-101->446, 4:+-91->456, 5:+-51->496
        //
        // Exception table:
        //     from	to	target	type
        //     17	78	73	finally
        //     91	172	73	finally
        //     175	178	73	finally
        //     188	325	73	finally
        //     359	524	73	finally
        //     8	17	81	finally
        //     78	81	81	finally
        //     91	172	186	android/os/RemoteException
    }

    private static class InputMethodFileManager
    {
        private static final String ADDITIONAL_SUBTYPES_FILE_NAME = "subtypes.xml";
        private static final String ATTR_ICON = "icon";
        private static final String ATTR_ID = "id";
        private static final String ATTR_IME_SUBTYPE_EXTRA_VALUE = "imeSubtypeExtraValue";
        private static final String ATTR_IME_SUBTYPE_LOCALE = "imeSubtypeLocale";
        private static final String ATTR_IME_SUBTYPE_MODE = "imeSubtypeMode";
        private static final String ATTR_IS_AUXILIARY = "isAuxiliary";
        private static final String ATTR_LABEL = "label";
        private static final String INPUT_METHOD_PATH = "inputmethod";
        private static final String NODE_IMI = "imi";
        private static final String NODE_SUBTYPE = "subtype";
        private static final String NODE_SUBTYPES = "subtypes";
        private static final String SYSTEM_PATH = "system";
        private final AtomicFile mAdditionalInputMethodSubtypeFile;
        private final HashMap<String, InputMethodInfo> mMethodMap;
        private final HashMap<String, List<InputMethodSubtype>> mSubtypesMap = new HashMap();

        public InputMethodFileManager(HashMap<String, InputMethodInfo> paramHashMap)
        {
            if (paramHashMap == null)
                throw new NullPointerException("methodMap is null");
            this.mMethodMap = paramHashMap;
            File localFile1 = new File(new File(Environment.getDataDirectory(), "system"), "inputmethod");
            if (!localFile1.mkdirs())
                Slog.w("InputMethodManagerService", "Couldn't create dir.: " + localFile1.getAbsolutePath());
            File localFile2 = new File(localFile1, "subtypes.xml");
            this.mAdditionalInputMethodSubtypeFile = new AtomicFile(localFile2);
            if (!localFile2.exists())
                writeAdditionalInputMethodSubtypes(this.mSubtypesMap, this.mAdditionalInputMethodSubtypeFile, paramHashMap);
            while (true)
            {
                return;
                readAdditionalInputMethodSubtypes(this.mSubtypesMap, this.mAdditionalInputMethodSubtypeFile);
            }
        }

        private void deleteAllInputMethodSubtypes(String paramString)
        {
            synchronized (this.mMethodMap)
            {
                this.mSubtypesMap.remove(paramString);
                writeAdditionalInputMethodSubtypes(this.mSubtypesMap, this.mAdditionalInputMethodSubtypeFile, this.mMethodMap);
                return;
            }
        }

        private static void readAdditionalInputMethodSubtypes(HashMap<String, List<InputMethodSubtype>> paramHashMap, AtomicFile paramAtomicFile)
        {
            if ((paramHashMap == null) || (paramAtomicFile == null))
                return;
            paramHashMap.clear();
            FileInputStream localFileInputStream = null;
            while (true)
                while (true)
                {
                    String str1;
                    String str2;
                    try
                    {
                        localFileInputStream = paramAtomicFile.openRead();
                        localXmlPullParser = Xml.newPullParser();
                        localXmlPullParser.setInput(localFileInputStream, null);
                        localXmlPullParser.getEventType();
                        int i = localXmlPullParser.next();
                        if ((i != 2) && (i != 1))
                            continue;
                        if (!"subtypes".equals(localXmlPullParser.getName()))
                            throw new XmlPullParserException("Xml doesn't start with subtypes");
                    }
                    catch (XmlPullParserException localXmlPullParserException)
                    {
                        while (true)
                        {
                            Slog.w("InputMethodManagerService", "Error reading subtypes: " + localXmlPullParserException);
                            if (localFileInputStream == null)
                                break;
                            try
                            {
                                localFileInputStream.close();
                            }
                            catch (IOException localIOException5)
                            {
                                str1 = "InputMethodManagerService";
                                str2 = "Failed to close.";
                            }
                        }
                        Slog.w(str1, str2);
                        break;
                        int j = localXmlPullParser.getDepth();
                        str3 = null;
                        localArrayList = null;
                        int k = localXmlPullParser.next();
                        if (((k != 3) || (localXmlPullParser.getDepth() > j)) && (k != 1))
                        {
                            if (k != 2)
                                continue;
                            str4 = localXmlPullParser.getName();
                            if ("imi".equals(str4))
                            {
                                str3 = localXmlPullParser.getAttributeValue(null, "id");
                                if (TextUtils.isEmpty(str3))
                                {
                                    Slog.w("InputMethodManagerService", "Invalid imi id found in subtypes.xml");
                                    continue;
                                }
                            }
                        }
                    }
                    catch (IOException localIOException3)
                    {
                        while (true)
                        {
                            Slog.w("InputMethodManagerService", "Error reading subtypes: " + localIOException3);
                            if (localFileInputStream == null)
                                break;
                            try
                            {
                                localFileInputStream.close();
                            }
                            catch (IOException localIOException4)
                            {
                                str1 = "InputMethodManagerService";
                                str2 = "Failed to close.";
                            }
                        }
                        continue;
                        localArrayList = new ArrayList();
                        paramHashMap.put(str3, localArrayList);
                        continue;
                    }
                    catch (NumberFormatException localNumberFormatException)
                    {
                        String str3;
                        String str4;
                        while (true)
                        {
                            Slog.w("InputMethodManagerService", "Error reading subtypes: " + localNumberFormatException);
                            if (localFileInputStream == null)
                                break;
                            try
                            {
                                localFileInputStream.close();
                            }
                            catch (IOException localIOException2)
                            {
                                str1 = "InputMethodManagerService";
                                str2 = "Failed to close.";
                            }
                        }
                        continue;
                        if (!"subtype".equals(str4))
                            continue;
                        if ((TextUtils.isEmpty(str3)) || (localArrayList == null))
                        {
                            Slog.w("InputMethodManagerService", "IME uninstalled or not valid.: " + str3);
                            continue;
                        }
                    }
                    finally
                    {
                        XmlPullParser localXmlPullParser;
                        ArrayList localArrayList;
                        if (localFileInputStream != null);
                        try
                        {
                            localFileInputStream.close();
                            throw localObject;
                            int m = Integer.valueOf(localXmlPullParser.getAttributeValue(null, "icon")).intValue();
                            InputMethodSubtype localInputMethodSubtype = new InputMethodSubtype(Integer.valueOf(localXmlPullParser.getAttributeValue(null, "label")).intValue(), m, localXmlPullParser.getAttributeValue(null, "imeSubtypeLocale"), localXmlPullParser.getAttributeValue(null, "imeSubtypeMode"), localXmlPullParser.getAttributeValue(null, "imeSubtypeExtraValue"), "1".equals(String.valueOf(localXmlPullParser.getAttributeValue(null, "isAuxiliary"))));
                            localArrayList.add(localInputMethodSubtype);
                        }
                        catch (IOException localIOException1)
                        {
                            Slog.w("InputMethodManagerService", "Failed to close.");
                            continue;
                        }
                    }
                    if (localFileInputStream == null)
                        break;
                    try
                    {
                        localFileInputStream.close();
                    }
                    catch (IOException localIOException6)
                    {
                        str1 = "InputMethodManagerService";
                        str2 = "Failed to close.";
                    }
                }
        }

        private static void writeAdditionalInputMethodSubtypes(HashMap<String, List<InputMethodSubtype>> paramHashMap, AtomicFile paramAtomicFile, HashMap<String, InputMethodInfo> paramHashMap1)
        {
            if ((paramHashMap1 != null) && (paramHashMap1.size() > 0));
            FileOutputStream localFileOutputStream;
            FastXmlSerializer localFastXmlSerializer;
            String str;
            for (int i = 1; ; i = 0)
            {
                localFileOutputStream = null;
                try
                {
                    localFileOutputStream = paramAtomicFile.startWrite();
                    localFastXmlSerializer = new FastXmlSerializer();
                    localFastXmlSerializer.setOutput(localFileOutputStream, "utf-8");
                    localFastXmlSerializer.startDocument(null, Boolean.valueOf(true));
                    localFastXmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
                    localFastXmlSerializer.startTag(null, "subtypes");
                    Iterator localIterator = paramHashMap.keySet().iterator();
                    while (true)
                    {
                        if (!localIterator.hasNext())
                            break label404;
                        str = (String)localIterator.next();
                        if ((i == 0) || (paramHashMap1.containsKey(str)))
                            break;
                        Slog.w("InputMethodManagerService", "IME uninstalled or not valid.: " + str);
                    }
                }
                catch (IOException localIOException)
                {
                    Slog.w("InputMethodManagerService", "Error writing subtypes", localIOException);
                    if (localFileOutputStream != null)
                        paramAtomicFile.failWrite(localFileOutputStream);
                }
                label175: return;
            }
            localFastXmlSerializer.startTag(null, "imi");
            localFastXmlSerializer.attribute(null, "id", str);
            List localList = (List)paramHashMap.get(str);
            int j = localList.size();
            int k = 0;
            label228: if (k < j)
            {
                InputMethodSubtype localInputMethodSubtype = (InputMethodSubtype)localList.get(k);
                localFastXmlSerializer.startTag(null, "subtype");
                localFastXmlSerializer.attribute(null, "icon", String.valueOf(localInputMethodSubtype.getIconResId()));
                localFastXmlSerializer.attribute(null, "label", String.valueOf(localInputMethodSubtype.getNameResId()));
                localFastXmlSerializer.attribute(null, "imeSubtypeLocale", localInputMethodSubtype.getLocale());
                localFastXmlSerializer.attribute(null, "imeSubtypeMode", localInputMethodSubtype.getMode());
                localFastXmlSerializer.attribute(null, "imeSubtypeExtraValue", localInputMethodSubtype.getExtraValue());
                if (!localInputMethodSubtype.isAuxiliary())
                    break label431;
            }
            label404: label431: for (int m = 1; ; m = 0)
            {
                localFastXmlSerializer.attribute(null, "isAuxiliary", String.valueOf(m));
                localFastXmlSerializer.endTag(null, "subtype");
                k++;
                break label228;
                localFastXmlSerializer.endTag(null, "imi");
                break;
                localFastXmlSerializer.endTag(null, "subtypes");
                localFastXmlSerializer.endDocument();
                paramAtomicFile.finishWrite(localFileOutputStream);
                break label175;
            }
        }

        public void addInputMethodSubtypes(InputMethodInfo paramInputMethodInfo, InputMethodSubtype[] paramArrayOfInputMethodSubtype)
        {
            while (true)
            {
                int j;
                synchronized (this.mMethodMap)
                {
                    ArrayList localArrayList = new ArrayList();
                    int i = paramArrayOfInputMethodSubtype.length;
                    j = 0;
                    if (j < i)
                    {
                        InputMethodSubtype localInputMethodSubtype = paramArrayOfInputMethodSubtype[j];
                        if (!localArrayList.contains(localInputMethodSubtype))
                            localArrayList.add(localInputMethodSubtype);
                    }
                    else
                    {
                        this.mSubtypesMap.put(paramInputMethodInfo.getId(), localArrayList);
                        writeAdditionalInputMethodSubtypes(this.mSubtypesMap, this.mAdditionalInputMethodSubtypeFile, this.mMethodMap);
                        return;
                    }
                }
                j++;
            }
        }

        public HashMap<String, List<InputMethodSubtype>> getAllAdditionalInputMethodSubtypes()
        {
            synchronized (this.mMethodMap)
            {
                HashMap localHashMap2 = this.mSubtypesMap;
                return localHashMap2;
            }
        }
    }

    private static class InputMethodSettings
    {
        private static final char INPUT_METHOD_SEPARATER = ':';
        private static final char INPUT_METHOD_SUBTYPE_SEPARATER = ';';
        private String mEnabledInputMethodsStrCache;
        private final TextUtils.SimpleStringSplitter mInputMethodSplitter = new TextUtils.SimpleStringSplitter(':');
        private final ArrayList<InputMethodInfo> mMethodList;
        private final HashMap<String, InputMethodInfo> mMethodMap;
        private final Resources mRes;
        private final ContentResolver mResolver;
        private final TextUtils.SimpleStringSplitter mSubtypeSplitter = new TextUtils.SimpleStringSplitter(';');

        public InputMethodSettings(Resources paramResources, ContentResolver paramContentResolver, HashMap<String, InputMethodInfo> paramHashMap, ArrayList<InputMethodInfo> paramArrayList)
        {
            this.mRes = paramResources;
            this.mResolver = paramContentResolver;
            this.mMethodMap = paramHashMap;
            this.mMethodList = paramArrayList;
        }

        private static void buildEnabledInputMethodsSettingString(StringBuilder paramStringBuilder, Pair<String, ArrayList<String>> paramPair)
        {
            String str1 = (String)paramPair.first;
            ArrayList localArrayList = (ArrayList)paramPair.second;
            paramStringBuilder.append(str1);
            Iterator localIterator = localArrayList.iterator();
            while (localIterator.hasNext())
            {
                String str2 = (String)localIterator.next();
                paramStringBuilder.append(';').append(str2);
            }
        }

        private List<Pair<InputMethodInfo, ArrayList<String>>> createEnabledInputMethodAndSubtypeHashCodeListLocked(List<Pair<String, ArrayList<String>>> paramList)
        {
            ArrayList localArrayList = new ArrayList();
            Iterator localIterator = paramList.iterator();
            while (localIterator.hasNext())
            {
                Pair localPair = (Pair)localIterator.next();
                InputMethodInfo localInputMethodInfo = (InputMethodInfo)this.mMethodMap.get(localPair.first);
                if (localInputMethodInfo != null)
                    localArrayList.add(new Pair(localInputMethodInfo, localPair.second));
            }
            return localArrayList;
        }

        private List<InputMethodInfo> createEnabledInputMethodListLocked(List<Pair<String, ArrayList<String>>> paramList)
        {
            ArrayList localArrayList = new ArrayList();
            Iterator localIterator = paramList.iterator();
            while (localIterator.hasNext())
            {
                Pair localPair = (Pair)localIterator.next();
                InputMethodInfo localInputMethodInfo = (InputMethodInfo)this.mMethodMap.get(localPair.first);
                if (localInputMethodInfo != null)
                    localArrayList.add(localInputMethodInfo);
            }
            return localArrayList;
        }

        private List<Pair<String, ArrayList<String>>> getEnabledInputMethodsAndSubtypeListLocked()
        {
            ArrayList localArrayList1 = new ArrayList();
            String str1 = getEnabledInputMethodsStr();
            if (TextUtils.isEmpty(str1));
            while (true)
            {
                return localArrayList1;
                this.mInputMethodSplitter.setString(str1);
                while (this.mInputMethodSplitter.hasNext())
                {
                    String str2 = this.mInputMethodSplitter.next();
                    this.mSubtypeSplitter.setString(str2);
                    if (this.mSubtypeSplitter.hasNext())
                    {
                        ArrayList localArrayList2 = new ArrayList();
                        String str3 = this.mSubtypeSplitter.next();
                        while (this.mSubtypeSplitter.hasNext())
                            localArrayList2.add(this.mSubtypeSplitter.next());
                        localArrayList1.add(new Pair(str3, localArrayList2));
                    }
                }
            }
        }

        private String getEnabledInputMethodsStr()
        {
            this.mEnabledInputMethodsStrCache = Settings.Secure.getString(this.mResolver, "enabled_input_methods");
            return this.mEnabledInputMethodsStrCache;
        }

        private String getEnabledSubtypeHashCodeForInputMethodAndSubtypeLocked(List<Pair<String, ArrayList<String>>> paramList, String paramString1, String paramString2)
        {
            Iterator localIterator1 = paramList.iterator();
            ArrayList localArrayList1;
            InputMethodInfo localInputMethodInfo;
            int j;
            while (true)
                if (localIterator1.hasNext())
                {
                    Pair localPair = (Pair)localIterator1.next();
                    if (((String)localPair.first).equals(paramString1))
                    {
                        localArrayList1 = (ArrayList)localPair.second;
                        localInputMethodInfo = (InputMethodInfo)this.mMethodMap.get(paramString1);
                        if (localArrayList1.size() == 0)
                        {
                            if ((localInputMethodInfo == null) || (localInputMethodInfo.getSubtypeCount() <= 0))
                                break label236;
                            ArrayList localArrayList2 = InputMethodManagerService.getImplicitlyApplicableSubtypesLocked(this.mRes, localInputMethodInfo);
                            if (localArrayList2 == null)
                                break label236;
                            int i = localArrayList2.size();
                            j = 0;
                            if (j >= i)
                                break label236;
                            if (!String.valueOf(((InputMethodSubtype)localArrayList2.get(j)).hashCode()).equals(paramString2))
                                break;
                        }
                    }
                }
            while (true)
            {
                return paramString2;
                j++;
                break;
                Iterator localIterator2 = localArrayList1.iterator();
                while (true)
                {
                    while (true)
                    {
                        if (!localIterator2.hasNext())
                            break label236;
                        String str1 = (String)localIterator2.next();
                        if (str1.equals(paramString2))
                            try
                            {
                                if (InputMethodManagerService.isValidSubtypeId(localInputMethodInfo, Integer.valueOf(paramString2).intValue()))
                                {
                                    paramString2 = str1;
                                    break;
                                }
                                String str2 = InputMethodManagerService.NOT_A_SUBTYPE_ID_STR;
                                paramString2 = str2;
                            }
                            catch (NumberFormatException localNumberFormatException)
                            {
                                paramString2 = InputMethodManagerService.NOT_A_SUBTYPE_ID_STR;
                            }
                    }
                    break;
                }
                label236: paramString2 = InputMethodManagerService.NOT_A_SUBTYPE_ID_STR;
                continue;
                paramString2 = null;
            }
        }

        private Pair<String, String> getLastSubtypeForInputMethodLockedInternal(String paramString)
        {
            List localList = getEnabledInputMethodsAndSubtypeListLocked();
            Iterator localIterator = loadInputMethodAndSubtypeHistoryLocked().iterator();
            String str1;
            String str2;
            do
            {
                Pair localPair2;
                do
                {
                    if (!localIterator.hasNext())
                        break;
                    localPair2 = (Pair)localIterator.next();
                    str1 = (String)localPair2.first;
                }
                while ((!TextUtils.isEmpty(paramString)) && (!str1.equals(paramString)));
                str2 = getEnabledSubtypeHashCodeForInputMethodAndSubtypeLocked(localList, str1, (String)localPair2.second);
            }
            while (TextUtils.isEmpty(str2));
            for (Pair localPair1 = new Pair(str1, str2); ; localPair1 = null)
                return localPair1;
        }

        private String getSubtypeHistoryStr()
        {
            return Settings.Secure.getString(this.mResolver, "input_methods_subtype_history");
        }

        private List<Pair<String, String>> loadInputMethodAndSubtypeHistoryLocked()
        {
            ArrayList localArrayList = new ArrayList();
            String str1 = getSubtypeHistoryStr();
            if (TextUtils.isEmpty(str1));
            while (true)
            {
                return localArrayList;
                this.mInputMethodSplitter.setString(str1);
                while (this.mInputMethodSplitter.hasNext())
                {
                    String str2 = this.mInputMethodSplitter.next();
                    this.mSubtypeSplitter.setString(str2);
                    if (this.mSubtypeSplitter.hasNext())
                    {
                        String str3 = InputMethodManagerService.NOT_A_SUBTYPE_ID_STR;
                        String str4 = this.mSubtypeSplitter.next();
                        if (this.mSubtypeSplitter.hasNext())
                            str3 = this.mSubtypeSplitter.next();
                        localArrayList.add(new Pair(str4, str3));
                    }
                }
            }
        }

        private void putEnabledInputMethodsStr(String paramString)
        {
            Settings.Secure.putString(this.mResolver, "enabled_input_methods", paramString);
            this.mEnabledInputMethodsStrCache = paramString;
        }

        private void putSubtypeHistoryStr(String paramString)
        {
            Settings.Secure.putString(this.mResolver, "input_methods_subtype_history", paramString);
        }

        private void saveSubtypeHistory(List<Pair<String, String>> paramList, String paramString1, String paramString2)
        {
            StringBuilder localStringBuilder = new StringBuilder();
            int i = 0;
            if ((!TextUtils.isEmpty(paramString1)) && (!TextUtils.isEmpty(paramString2)))
            {
                localStringBuilder.append(paramString1).append(';').append(paramString2);
                i = 1;
            }
            Iterator localIterator = paramList.iterator();
            if (localIterator.hasNext())
            {
                Pair localPair = (Pair)localIterator.next();
                String str1 = (String)localPair.first;
                String str2 = (String)localPair.second;
                if (TextUtils.isEmpty(str2))
                    str2 = InputMethodManagerService.NOT_A_SUBTYPE_ID_STR;
                if (i != 0)
                    localStringBuilder.append(':');
                while (true)
                {
                    localStringBuilder.append(str1).append(';').append(str2);
                    break;
                    i = 1;
                }
            }
            putSubtypeHistoryStr(localStringBuilder.toString());
        }

        public void addSubtypeToHistory(String paramString1, String paramString2)
        {
            List localList = loadInputMethodAndSubtypeHistoryLocked();
            Iterator localIterator = localList.iterator();
            while (localIterator.hasNext())
            {
                Pair localPair = (Pair)localIterator.next();
                if (((String)localPair.first).equals(paramString1))
                    localList.remove(localPair);
            }
            saveSubtypeHistory(localList, paramString1, paramString2);
        }

        public void appendAndPutEnabledInputMethodLocked(String paramString, boolean paramBoolean)
        {
            if (paramBoolean)
                getEnabledInputMethodsStr();
            if (TextUtils.isEmpty(this.mEnabledInputMethodsStrCache))
                putEnabledInputMethodsStr(paramString);
            while (true)
            {
                return;
                putEnabledInputMethodsStr(this.mEnabledInputMethodsStrCache + ':' + paramString);
            }
        }

        public boolean buildAndPutEnabledInputMethodsStrRemovingIdLocked(StringBuilder paramStringBuilder, List<Pair<String, ArrayList<String>>> paramList, String paramString)
        {
            boolean bool = false;
            int i = 0;
            Iterator localIterator = paramList.iterator();
            while (localIterator.hasNext())
            {
                Pair localPair = (Pair)localIterator.next();
                if (((String)localPair.first).equals(paramString))
                {
                    bool = true;
                }
                else
                {
                    if (i != 0)
                        paramStringBuilder.append(':');
                    while (true)
                    {
                        buildEnabledInputMethodsSettingString(paramStringBuilder, localPair);
                        break;
                        i = 1;
                    }
                }
            }
            if (bool)
                putEnabledInputMethodsStr(paramStringBuilder.toString());
            return bool;
        }

        public void enableAllIMEsIfThereIsNoEnabledIME()
        {
            if (TextUtils.isEmpty(getEnabledInputMethodsStr()))
            {
                StringBuilder localStringBuilder = new StringBuilder();
                int i = this.mMethodList.size();
                for (int j = 0; j < i; j++)
                {
                    InputMethodInfo localInputMethodInfo = (InputMethodInfo)this.mMethodList.get(j);
                    Slog.i("InputMethodManagerService", "Adding: " + localInputMethodInfo.getId());
                    if (j > 0)
                        localStringBuilder.append(':');
                    localStringBuilder.append(localInputMethodInfo.getId());
                }
                putEnabledInputMethodsStr(localStringBuilder.toString());
            }
        }

        public List<Pair<InputMethodInfo, ArrayList<String>>> getEnabledInputMethodAndSubtypeHashCodeListLocked()
        {
            return createEnabledInputMethodAndSubtypeHashCodeListLocked(getEnabledInputMethodsAndSubtypeListLocked());
        }

        public List<InputMethodInfo> getEnabledInputMethodListLocked()
        {
            return createEnabledInputMethodListLocked(getEnabledInputMethodsAndSubtypeListLocked());
        }

        public List<InputMethodSubtype> getEnabledInputMethodSubtypeListLocked(InputMethodInfo paramInputMethodInfo)
        {
            List localList = getEnabledInputMethodsAndSubtypeListLocked();
            ArrayList localArrayList = new ArrayList();
            if (paramInputMethodInfo != null)
            {
                Iterator localIterator1 = localList.iterator();
                while (localIterator1.hasNext())
                {
                    Pair localPair = (Pair)localIterator1.next();
                    InputMethodInfo localInputMethodInfo = (InputMethodInfo)this.mMethodMap.get(localPair.first);
                    if ((localInputMethodInfo != null) && (localInputMethodInfo.getId().equals(paramInputMethodInfo.getId())))
                    {
                        int i = localInputMethodInfo.getSubtypeCount();
                        for (int j = 0; j < i; j++)
                        {
                            InputMethodSubtype localInputMethodSubtype = localInputMethodInfo.getSubtypeAt(j);
                            Iterator localIterator2 = ((ArrayList)localPair.second).iterator();
                            while (localIterator2.hasNext())
                            {
                                String str = (String)localIterator2.next();
                                if (String.valueOf(localInputMethodSubtype.hashCode()).equals(str))
                                    localArrayList.add(localInputMethodSubtype);
                            }
                        }
                    }
                }
            }
            return localArrayList;
        }

        public Pair<String, String> getLastInputMethodAndSubtypeLocked()
        {
            return getLastSubtypeForInputMethodLockedInternal(null);
        }

        public String getLastSubtypeForInputMethodLocked(String paramString)
        {
            Pair localPair = getLastSubtypeForInputMethodLockedInternal(paramString);
            if (localPair != null);
            for (String str = (String)localPair.second; ; str = null)
                return str;
        }

        public void putSelectedInputMethod(String paramString)
        {
            Settings.Secure.putString(this.mResolver, "default_input_method", paramString);
        }

        public void putSelectedSubtype(int paramInt)
        {
            Settings.Secure.putInt(this.mResolver, "selected_input_method_subtype", paramInt);
        }
    }

    private static class InputMethodAndSubtypeListManager
    {
        private final Context mContext;
        private final InputMethodManagerService mImms;
        private final PackageManager mPm;
        private final TreeMap<InputMethodInfo, List<InputMethodSubtype>> mSortedImmis = new TreeMap(new Comparator()
        {
            public int compare(InputMethodInfo paramAnonymousInputMethodInfo1, InputMethodInfo paramAnonymousInputMethodInfo2)
            {
                int i;
                if (paramAnonymousInputMethodInfo2 == null)
                    i = 0;
                while (true)
                {
                    return i;
                    if (paramAnonymousInputMethodInfo1 == null)
                    {
                        i = 1;
                    }
                    else if (InputMethodManagerService.InputMethodAndSubtypeListManager.this.mPm == null)
                    {
                        i = paramAnonymousInputMethodInfo1.getId().compareTo(paramAnonymousInputMethodInfo2.getId());
                    }
                    else
                    {
                        String str1 = paramAnonymousInputMethodInfo1.loadLabel(InputMethodManagerService.InputMethodAndSubtypeListManager.this.mPm) + "/" + paramAnonymousInputMethodInfo1.getId();
                        String str2 = paramAnonymousInputMethodInfo2.loadLabel(InputMethodManagerService.InputMethodAndSubtypeListManager.this.mPm) + "/" + paramAnonymousInputMethodInfo2.getId();
                        i = str1.toString().compareTo(str2.toString());
                    }
                }
            }
        });
        private final String mSystemLocaleStr;

        public InputMethodAndSubtypeListManager(Context paramContext, InputMethodManagerService paramInputMethodManagerService)
        {
            this.mContext = paramContext;
            this.mPm = paramContext.getPackageManager();
            this.mImms = paramInputMethodManagerService;
            Locale localLocale = paramContext.getResources().getConfiguration().locale;
            if (localLocale != null);
            for (String str = localLocale.toString(); ; str = "")
            {
                this.mSystemLocaleStr = str;
                return;
            }
        }

        public InputMethodManagerService.ImeSubtypeListItem getNextInputMethod(boolean paramBoolean, InputMethodInfo paramInputMethodInfo, InputMethodSubtype paramInputMethodSubtype)
        {
            Object localObject = null;
            if (paramInputMethodInfo == null);
            label50: label186: label192: 
            while (true)
            {
                return localObject;
                List localList = getSortedInputMethodAndSubtypeList();
                if (localList.size() > 1)
                {
                    int i = localList.size();
                    int j;
                    if (paramInputMethodSubtype != null)
                        j = InputMethodManagerService.getSubtypeIdFromHashCode(paramInputMethodInfo, paramInputMethodSubtype.hashCode());
                    for (int k = 0; ; k++)
                    {
                        if (k >= i)
                            break label192;
                        InputMethodManagerService.ImeSubtypeListItem localImeSubtypeListItem1 = (InputMethodManagerService.ImeSubtypeListItem)localList.get(k);
                        if ((localImeSubtypeListItem1.mImi.equals(paramInputMethodInfo)) && (localImeSubtypeListItem1.mSubtypeId == j))
                        {
                            if (!paramBoolean)
                            {
                                localObject = (InputMethodManagerService.ImeSubtypeListItem)localList.get((k + 1) % i);
                                break;
                                j = -1;
                                break label50;
                            }
                            for (int m = 0; ; m++)
                            {
                                if (m >= i - 1)
                                    break label186;
                                InputMethodManagerService.ImeSubtypeListItem localImeSubtypeListItem2 = (InputMethodManagerService.ImeSubtypeListItem)localList.get((1 + (k + m)) % i);
                                if (localImeSubtypeListItem2.mImi.equals(paramInputMethodInfo))
                                {
                                    localObject = localImeSubtypeListItem2;
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }

        public List<InputMethodManagerService.ImeSubtypeListItem> getSortedInputMethodAndSubtypeList()
        {
            return getSortedInputMethodAndSubtypeList(true, false, false);
        }

        public List<InputMethodManagerService.ImeSubtypeListItem> getSortedInputMethodAndSubtypeList(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
        {
            Object localObject = new ArrayList();
            HashMap localHashMap = this.mImms.getExplicitlyOrImplicitlyEnabledInputMethodsAndSubtypeListLocked();
            if ((localHashMap == null) || (localHashMap.size() == 0))
                localObject = Collections.emptyList();
            while (true)
            {
                return localObject;
                this.mSortedImmis.clear();
                this.mSortedImmis.putAll(localHashMap);
                Iterator localIterator1 = this.mSortedImmis.keySet().iterator();
                while (localIterator1.hasNext())
                {
                    InputMethodInfo localInputMethodInfo = (InputMethodInfo)localIterator1.next();
                    if (localInputMethodInfo != null)
                    {
                        List localList = (List)localHashMap.get(localInputMethodInfo);
                        HashSet localHashSet = new HashSet();
                        Iterator localIterator2 = localList.iterator();
                        while (localIterator2.hasNext())
                            localHashSet.add(String.valueOf(((InputMethodSubtype)localIterator2.next()).hashCode()));
                        InputMethodManagerService.getSubtypes(localInputMethodInfo);
                        CharSequence localCharSequence1 = localInputMethodInfo.loadLabel(this.mPm);
                        if ((paramBoolean1) && (localHashSet.size() > 0))
                        {
                            int i = localInputMethodInfo.getSubtypeCount();
                            int j = 0;
                            label200: InputMethodSubtype localInputMethodSubtype;
                            String str;
                            if (j < i)
                            {
                                localInputMethodSubtype = localInputMethodInfo.getSubtypeAt(j);
                                str = String.valueOf(localInputMethodSubtype.hashCode());
                                if ((localHashSet.contains(str)) && (((paramBoolean2) && (!paramBoolean3)) || (!localInputMethodSubtype.isAuxiliary())))
                                    if (!localInputMethodSubtype.overridesImplicitlyEnabledSubtype())
                                        break label311;
                            }
                            label311: for (CharSequence localCharSequence2 = null; ; localCharSequence2 = localInputMethodSubtype.getDisplayName(this.mContext, localInputMethodInfo.getPackageName(), localInputMethodInfo.getServiceInfo().applicationInfo))
                            {
                                InputMethodManagerService.ImeSubtypeListItem localImeSubtypeListItem2 = new InputMethodManagerService.ImeSubtypeListItem(localCharSequence1, localCharSequence2, localInputMethodInfo, j, localInputMethodSubtype.getLocale(), this.mSystemLocaleStr);
                                ((ArrayList)localObject).add(localImeSubtypeListItem2);
                                localHashSet.remove(str);
                                j++;
                                break label200;
                                break;
                            }
                        }
                        InputMethodManagerService.ImeSubtypeListItem localImeSubtypeListItem1 = new InputMethodManagerService.ImeSubtypeListItem(localCharSequence1, null, localInputMethodInfo, -1, null, this.mSystemLocaleStr);
                        ((ArrayList)localObject).add(localImeSubtypeListItem1);
                    }
                }
                Collections.sort((List)localObject);
            }
        }
    }

    private static class ImeSubtypeListAdapter extends ArrayAdapter<InputMethodManagerService.ImeSubtypeListItem>
    {
        private final int mCheckedItem;
        private final LayoutInflater mInflater;
        private final List<InputMethodManagerService.ImeSubtypeListItem> mItemsList;
        private final int mTextViewResourceId;

        public ImeSubtypeListAdapter(Context paramContext, int paramInt1, List<InputMethodManagerService.ImeSubtypeListItem> paramList, int paramInt2)
        {
            super(paramInt1, paramList);
            this.mTextViewResourceId = paramInt1;
            this.mItemsList = paramList;
            this.mCheckedItem = paramInt2;
            this.mInflater = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
        }

        public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
        {
            boolean bool = false;
            if (paramView != null);
            for (View localView = paramView; (paramInt < 0) || (paramInt >= this.mItemsList.size()); localView = this.mInflater.inflate(this.mTextViewResourceId, null))
                return localView;
            InputMethodManagerService.ImeSubtypeListItem localImeSubtypeListItem = (InputMethodManagerService.ImeSubtypeListItem)this.mItemsList.get(paramInt);
            CharSequence localCharSequence1 = localImeSubtypeListItem.mImeName;
            CharSequence localCharSequence2 = localImeSubtypeListItem.mSubtypeName;
            TextView localTextView1 = (TextView)localView.findViewById(16908308);
            TextView localTextView2 = (TextView)localView.findViewById(16908309);
            if (TextUtils.isEmpty(localCharSequence2))
            {
                localTextView1.setText(localCharSequence1);
                localTextView2.setVisibility(8);
            }
            while (true)
            {
                RadioButton localRadioButton = (RadioButton)localView.findViewById(16909011);
                if (paramInt == this.mCheckedItem)
                    bool = true;
                localRadioButton.setChecked(bool);
                break;
                localTextView1.setText(localCharSequence2);
                localTextView2.setText(localCharSequence1);
                localTextView2.setVisibility(0);
            }
        }
    }

    private static class ImeSubtypeListItem
        implements Comparable<ImeSubtypeListItem>
    {
        public final CharSequence mImeName;
        public final InputMethodInfo mImi;
        private final boolean mIsSystemLanguage;
        private final boolean mIsSystemLocale;
        public final int mSubtypeId;
        public final CharSequence mSubtypeName;

        public ImeSubtypeListItem(CharSequence paramCharSequence1, CharSequence paramCharSequence2, InputMethodInfo paramInputMethodInfo, int paramInt, String paramString1, String paramString2)
        {
            this.mImeName = paramCharSequence1;
            this.mSubtypeName = paramCharSequence2;
            this.mImi = paramInputMethodInfo;
            this.mSubtypeId = paramInt;
            if (TextUtils.isEmpty(paramString1))
                this.mIsSystemLocale = false;
            for (this.mIsSystemLanguage = false; ; this.mIsSystemLanguage = bool)
            {
                return;
                this.mIsSystemLocale = paramString1.equals(paramString2);
                if ((this.mIsSystemLocale) || (paramString1.startsWith(paramString2.substring(0, 2))))
                    bool = true;
            }
        }

        public int compareTo(ImeSubtypeListItem paramImeSubtypeListItem)
        {
            int i = 1;
            if (TextUtils.isEmpty(this.mImeName));
            while (true)
            {
                return i;
                if (TextUtils.isEmpty(paramImeSubtypeListItem.mImeName))
                    i = -1;
                else if (!TextUtils.equals(this.mImeName, paramImeSubtypeListItem.mImeName))
                    i = this.mImeName.toString().compareTo(paramImeSubtypeListItem.mImeName.toString());
                else if (TextUtils.equals(this.mSubtypeName, paramImeSubtypeListItem.mSubtypeName))
                    i = 0;
                else if (this.mIsSystemLocale)
                    i = -1;
                else if (!paramImeSubtypeListItem.mIsSystemLocale)
                    if (this.mIsSystemLanguage)
                        i = -1;
                    else if ((!paramImeSubtypeListItem.mIsSystemLanguage) && (!TextUtils.isEmpty(this.mSubtypeName)))
                        if (TextUtils.isEmpty(paramImeSubtypeListItem.mSubtypeName))
                            i = -1;
                        else
                            i = this.mSubtypeName.toString().compareTo(paramImeSubtypeListItem.mSubtypeName.toString());
            }
        }
    }

    private class HardKeyboardListener
        implements WindowManagerService.OnHardKeyboardStatusChangeListener
    {
        private HardKeyboardListener()
        {
        }

        public void handleHardKeyboardStatusChange(boolean paramBoolean1, boolean paramBoolean2)
        {
            while (true)
            {
                synchronized (InputMethodManagerService.this.mMethodMap)
                {
                    if ((InputMethodManagerService.this.mSwitchingDialog != null) && (InputMethodManagerService.this.mSwitchingDialogTitleView != null) && (InputMethodManagerService.this.mSwitchingDialog.isShowing()))
                    {
                        View localView = InputMethodManagerService.this.mSwitchingDialogTitleView.findViewById(16908944);
                        if (paramBoolean1)
                        {
                            i = 0;
                            localView.setVisibility(i);
                        }
                    }
                    else
                    {
                        return;
                    }
                }
                int i = 8;
            }
        }

        public void onHardKeyboardStatusChange(boolean paramBoolean1, boolean paramBoolean2)
        {
            int i = 1;
            Handler localHandler1 = InputMethodManagerService.this.mHandler;
            Handler localHandler2 = InputMethodManagerService.this.mHandler;
            int j;
            if (paramBoolean1)
            {
                j = i;
                if (!paramBoolean2)
                    break label55;
            }
            while (true)
            {
                localHandler1.sendMessage(localHandler2.obtainMessage(4000, j, i));
                return;
                j = 0;
                break;
                label55: i = 0;
            }
        }
    }

    private static class MethodCallback extends IInputMethodCallback.Stub
    {
        private final IInputMethod mMethod;
        private final InputMethodManagerService mParentIMMS;

        MethodCallback(IInputMethod paramIInputMethod, InputMethodManagerService paramInputMethodManagerService)
        {
            this.mMethod = paramIInputMethod;
            this.mParentIMMS = paramInputMethodManagerService;
        }

        public void finishedEvent(int paramInt, boolean paramBoolean)
            throws RemoteException
        {
        }

        public void sessionCreated(IInputMethodSession paramIInputMethodSession)
            throws RemoteException
        {
            this.mParentIMMS.onSessionCreated(this.mMethod, paramIInputMethodSession);
        }
    }

    class MyPackageMonitor extends PackageMonitor
    {
        MyPackageMonitor()
        {
        }

        // ERROR //
        public boolean onHandleForceStop(Intent paramIntent, String[] paramArrayOfString, int paramInt, boolean paramBoolean)
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 13	com/android/server/InputMethodManagerService$MyPackageMonitor:this$0	Lcom/android/server/InputMethodManagerService;
            //     4: getfield 22	com/android/server/InputMethodManagerService:mMethodMap	Ljava/util/HashMap;
            //     7: astore 5
            //     9: aload 5
            //     11: monitorenter
            //     12: aload_0
            //     13: getfield 13	com/android/server/InputMethodManagerService$MyPackageMonitor:this$0	Lcom/android/server/InputMethodManagerService;
            //     16: getfield 26	com/android/server/InputMethodManagerService:mContext	Landroid/content/Context;
            //     19: invokevirtual 32	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
            //     22: ldc 34
            //     24: invokestatic 40	android/provider/Settings$Secure:getString	(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
            //     27: astore 7
            //     29: aload_0
            //     30: getfield 13	com/android/server/InputMethodManagerService$MyPackageMonitor:this$0	Lcom/android/server/InputMethodManagerService;
            //     33: getfield 44	com/android/server/InputMethodManagerService:mMethodList	Ljava/util/ArrayList;
            //     36: invokevirtual 50	java/util/ArrayList:size	()I
            //     39: istore 8
            //     41: aload 7
            //     43: ifnull +136 -> 179
            //     46: iconst_0
            //     47: istore 10
            //     49: iload 10
            //     51: iload 8
            //     53: if_icmpge +126 -> 179
            //     56: aload_0
            //     57: getfield 13	com/android/server/InputMethodManagerService$MyPackageMonitor:this$0	Lcom/android/server/InputMethodManagerService;
            //     60: getfield 44	com/android/server/InputMethodManagerService:mMethodList	Ljava/util/ArrayList;
            //     63: iload 10
            //     65: invokevirtual 54	java/util/ArrayList:get	(I)Ljava/lang/Object;
            //     68: checkcast 56	android/view/inputmethod/InputMethodInfo
            //     71: astore 11
            //     73: aload 11
            //     75: invokevirtual 60	android/view/inputmethod/InputMethodInfo:getId	()Ljava/lang/String;
            //     78: aload 7
            //     80: invokevirtual 66	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     83: ifeq +90 -> 173
            //     86: aload_2
            //     87: arraylength
            //     88: istore 12
            //     90: iconst_0
            //     91: istore 13
            //     93: iload 13
            //     95: iload 12
            //     97: if_icmpge +76 -> 173
            //     100: aload_2
            //     101: iload 13
            //     103: aaload
            //     104: astore 14
            //     106: aload 11
            //     108: invokevirtual 69	android/view/inputmethod/InputMethodInfo:getPackageName	()Ljava/lang/String;
            //     111: aload 14
            //     113: invokevirtual 66	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     116: ifeq +51 -> 167
            //     119: iload 4
            //     121: ifne +12 -> 133
            //     124: iconst_1
            //     125: istore 9
            //     127: aload 5
            //     129: monitorexit
            //     130: goto +55 -> 185
            //     133: aload_0
            //     134: getfield 13	com/android/server/InputMethodManagerService$MyPackageMonitor:this$0	Lcom/android/server/InputMethodManagerService;
            //     137: ldc 71
            //     139: invokestatic 75	com/android/server/InputMethodManagerService:access$200	(Lcom/android/server/InputMethodManagerService;Ljava/lang/String;)V
            //     142: aload_0
            //     143: getfield 13	com/android/server/InputMethodManagerService$MyPackageMonitor:this$0	Lcom/android/server/InputMethodManagerService;
            //     146: invokestatic 79	com/android/server/InputMethodManagerService:access$300	(Lcom/android/server/InputMethodManagerService;)Z
            //     149: pop
            //     150: iconst_1
            //     151: istore 9
            //     153: aload 5
            //     155: monitorexit
            //     156: goto +29 -> 185
            //     159: astore 6
            //     161: aload 5
            //     163: monitorexit
            //     164: aload 6
            //     166: athrow
            //     167: iinc 13 1
            //     170: goto -77 -> 93
            //     173: iinc 10 1
            //     176: goto -127 -> 49
            //     179: aload 5
            //     181: monitorexit
            //     182: iconst_0
            //     183: istore 9
            //     185: iload 9
            //     187: ireturn
            //
            // Exception table:
            //     from	to	target	type
            //     12	164	159	finally
            //     179	182	159	finally
        }

        // ERROR //
        public void onSomePackagesChanged()
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 13	com/android/server/InputMethodManagerService$MyPackageMonitor:this$0	Lcom/android/server/InputMethodManagerService;
            //     4: getfield 22	com/android/server/InputMethodManagerService:mMethodMap	Ljava/util/HashMap;
            //     7: astore_1
            //     8: aload_1
            //     9: monitorenter
            //     10: aconst_null
            //     11: astore_2
            //     12: aload_0
            //     13: getfield 13	com/android/server/InputMethodManagerService$MyPackageMonitor:this$0	Lcom/android/server/InputMethodManagerService;
            //     16: getfield 26	com/android/server/InputMethodManagerService:mContext	Landroid/content/Context;
            //     19: invokevirtual 32	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
            //     22: ldc 34
            //     24: invokestatic 40	android/provider/Settings$Secure:getString	(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
            //     27: astore 4
            //     29: aload_0
            //     30: getfield 13	com/android/server/InputMethodManagerService$MyPackageMonitor:this$0	Lcom/android/server/InputMethodManagerService;
            //     33: getfield 44	com/android/server/InputMethodManagerService:mMethodList	Ljava/util/ArrayList;
            //     36: invokevirtual 50	java/util/ArrayList:size	()I
            //     39: istore 5
            //     41: aload 4
            //     43: ifnull +134 -> 177
            //     46: iconst_0
            //     47: istore 13
            //     49: iload 13
            //     51: iload 5
            //     53: if_icmpge +124 -> 177
            //     56: aload_0
            //     57: getfield 13	com/android/server/InputMethodManagerService$MyPackageMonitor:this$0	Lcom/android/server/InputMethodManagerService;
            //     60: getfield 44	com/android/server/InputMethodManagerService:mMethodList	Ljava/util/ArrayList;
            //     63: iload 13
            //     65: invokevirtual 54	java/util/ArrayList:get	(I)Ljava/lang/Object;
            //     68: checkcast 56	android/view/inputmethod/InputMethodInfo
            //     71: astore 14
            //     73: aload 14
            //     75: invokevirtual 60	android/view/inputmethod/InputMethodInfo:getId	()Ljava/lang/String;
            //     78: astore 15
            //     80: aload 15
            //     82: aload 4
            //     84: invokevirtual 66	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     87: ifeq +6 -> 93
            //     90: aload 14
            //     92: astore_2
            //     93: aload_0
            //     94: aload 14
            //     96: invokevirtual 69	android/view/inputmethod/InputMethodInfo:getPackageName	()Ljava/lang/String;
            //     99: invokevirtual 86	com/android/server/InputMethodManagerService$MyPackageMonitor:isPackageDisappearing	(Ljava/lang/String;)I
            //     102: istore 16
            //     104: aload_0
            //     105: aload 14
            //     107: invokevirtual 69	android/view/inputmethod/InputMethodInfo:getPackageName	()Ljava/lang/String;
            //     110: invokevirtual 90	com/android/server/InputMethodManagerService$MyPackageMonitor:isPackageModified	(Ljava/lang/String;)Z
            //     113: ifeq +249 -> 362
            //     116: aload_0
            //     117: getfield 13	com/android/server/InputMethodManagerService$MyPackageMonitor:this$0	Lcom/android/server/InputMethodManagerService;
            //     120: invokestatic 94	com/android/server/InputMethodManagerService:access$400	(Lcom/android/server/InputMethodManagerService;)Lcom/android/server/InputMethodManagerService$InputMethodFileManager;
            //     123: aload 15
            //     125: invokestatic 100	com/android/server/InputMethodManagerService$InputMethodFileManager:access$500	(Lcom/android/server/InputMethodManagerService$InputMethodFileManager;Ljava/lang/String;)V
            //     128: goto +234 -> 362
            //     131: ldc 102
            //     133: new 104	java/lang/StringBuilder
            //     136: dup
            //     137: invokespecial 105	java/lang/StringBuilder:<init>	()V
            //     140: ldc 107
            //     142: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     145: aload 14
            //     147: invokevirtual 115	android/view/inputmethod/InputMethodInfo:getComponent	()Landroid/content/ComponentName;
            //     150: invokevirtual 118	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     153: invokevirtual 121	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     156: invokestatic 127	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
            //     159: pop
            //     160: aload_0
            //     161: getfield 13	com/android/server/InputMethodManagerService$MyPackageMonitor:this$0	Lcom/android/server/InputMethodManagerService;
            //     164: aload 14
            //     166: invokevirtual 60	android/view/inputmethod/InputMethodInfo:getId	()Ljava/lang/String;
            //     169: iconst_0
            //     170: invokevirtual 131	com/android/server/InputMethodManagerService:setInputMethodEnabledLocked	(Ljava/lang/String;Z)Z
            //     173: pop
            //     174: goto +203 -> 377
            //     177: aload_0
            //     178: getfield 13	com/android/server/InputMethodManagerService$MyPackageMonitor:this$0	Lcom/android/server/InputMethodManagerService;
            //     181: aload_0
            //     182: getfield 13	com/android/server/InputMethodManagerService$MyPackageMonitor:this$0	Lcom/android/server/InputMethodManagerService;
            //     185: getfield 44	com/android/server/InputMethodManagerService:mMethodList	Ljava/util/ArrayList;
            //     188: aload_0
            //     189: getfield 13	com/android/server/InputMethodManagerService$MyPackageMonitor:this$0	Lcom/android/server/InputMethodManagerService;
            //     192: getfield 22	com/android/server/InputMethodManagerService:mMethodMap	Ljava/util/HashMap;
            //     195: invokevirtual 135	com/android/server/InputMethodManagerService:buildInputMethodListLocked	(Ljava/util/ArrayList;Ljava/util/HashMap;)V
            //     198: iconst_0
            //     199: istore 6
            //     201: aload_2
            //     202: ifnull +122 -> 324
            //     205: aload_0
            //     206: aload_2
            //     207: invokevirtual 69	android/view/inputmethod/InputMethodInfo:getPackageName	()Ljava/lang/String;
            //     210: invokevirtual 86	com/android/server/InputMethodManagerService$MyPackageMonitor:isPackageDisappearing	(Ljava/lang/String;)I
            //     213: istore 7
            //     215: iload 7
            //     217: iconst_2
            //     218: if_icmpeq +9 -> 227
            //     221: iload 7
            //     223: iconst_3
            //     224: if_icmpne +100 -> 324
            //     227: aconst_null
            //     228: astore 8
            //     230: aload_0
            //     231: getfield 13	com/android/server/InputMethodManagerService$MyPackageMonitor:this$0	Lcom/android/server/InputMethodManagerService;
            //     234: getfield 26	com/android/server/InputMethodManagerService:mContext	Landroid/content/Context;
            //     237: invokevirtual 139	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
            //     240: aload_2
            //     241: invokevirtual 115	android/view/inputmethod/InputMethodInfo:getComponent	()Landroid/content/ComponentName;
            //     244: iconst_0
            //     245: invokevirtual 145	android/content/pm/PackageManager:getServiceInfo	(Landroid/content/ComponentName;I)Landroid/content/pm/ServiceInfo;
            //     248: astore 12
            //     250: aload 12
            //     252: astore 8
            //     254: aload 8
            //     256: ifnonnull +68 -> 324
            //     259: ldc 102
            //     261: new 104	java/lang/StringBuilder
            //     264: dup
            //     265: invokespecial 105	java/lang/StringBuilder:<init>	()V
            //     268: ldc 147
            //     270: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     273: aload 4
            //     275: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     278: invokevirtual 121	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     281: invokestatic 127	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
            //     284: pop
            //     285: aload_0
            //     286: getfield 13	com/android/server/InputMethodManagerService$MyPackageMonitor:this$0	Lcom/android/server/InputMethodManagerService;
            //     289: invokestatic 150	com/android/server/InputMethodManagerService:access$100	(Lcom/android/server/InputMethodManagerService;)V
            //     292: aload_0
            //     293: getfield 13	com/android/server/InputMethodManagerService$MyPackageMonitor:this$0	Lcom/android/server/InputMethodManagerService;
            //     296: invokestatic 79	com/android/server/InputMethodManagerService:access$300	(Lcom/android/server/InputMethodManagerService;)Z
            //     299: ifne +25 -> 324
            //     302: iconst_1
            //     303: istore 6
            //     305: aconst_null
            //     306: astore_2
            //     307: ldc 102
            //     309: ldc 152
            //     311: invokestatic 127	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
            //     314: pop
            //     315: aload_0
            //     316: getfield 13	com/android/server/InputMethodManagerService$MyPackageMonitor:this$0	Lcom/android/server/InputMethodManagerService;
            //     319: ldc 71
            //     321: invokestatic 75	com/android/server/InputMethodManagerService:access$200	(Lcom/android/server/InputMethodManagerService;Ljava/lang/String;)V
            //     324: aload_2
            //     325: ifnonnull +12 -> 337
            //     328: aload_0
            //     329: getfield 13	com/android/server/InputMethodManagerService$MyPackageMonitor:this$0	Lcom/android/server/InputMethodManagerService;
            //     332: invokestatic 79	com/android/server/InputMethodManagerService:access$300	(Lcom/android/server/InputMethodManagerService;)Z
            //     335: istore 6
            //     337: iload 6
            //     339: ifeq +10 -> 349
            //     342: aload_0
            //     343: getfield 13	com/android/server/InputMethodManagerService$MyPackageMonitor:this$0	Lcom/android/server/InputMethodManagerService;
            //     346: invokevirtual 155	com/android/server/InputMethodManagerService:updateFromSettingsLocked	()V
            //     349: aload_1
            //     350: monitorexit
            //     351: return
            //     352: astore_3
            //     353: aload_1
            //     354: monitorexit
            //     355: aload_3
            //     356: athrow
            //     357: astore 9
            //     359: goto -105 -> 254
            //     362: iload 16
            //     364: iconst_2
            //     365: if_icmpeq -234 -> 131
            //     368: iload 16
            //     370: iconst_3
            //     371: if_icmpne +6 -> 377
            //     374: goto -243 -> 131
            //     377: iinc 13 1
            //     380: goto -331 -> 49
            //
            // Exception table:
            //     from	to	target	type
            //     12	215	352	finally
            //     230	250	352	finally
            //     259	355	352	finally
            //     230	250	357	android/content/pm/PackageManager$NameNotFoundException
        }
    }

    class ScreenOnOffReceiver extends BroadcastReceiver
    {
        ScreenOnOffReceiver()
        {
        }

        public void onReceive(Context paramContext, Intent paramIntent)
        {
            int i = 1;
            InputMethodManagerService localInputMethodManagerService;
            IInputMethodClient localIInputMethodClient;
            HandlerCaller localHandlerCaller;
            if (paramIntent.getAction().equals("android.intent.action.SCREEN_ON"))
            {
                InputMethodManagerService.this.mScreenOn = i;
                InputMethodManagerService.this.refreshImeWindowVisibilityLocked();
                if ((InputMethodManagerService.this.mCurClient != null) && (InputMethodManagerService.this.mCurClient.client != null))
                {
                    localInputMethodManagerService = InputMethodManagerService.this;
                    localIInputMethodClient = InputMethodManagerService.this.mCurClient.client;
                    localHandlerCaller = InputMethodManagerService.this.mCaller;
                    if (!InputMethodManagerService.this.mScreenOn)
                        break label193;
                }
            }
            while (true)
            {
                localInputMethodManagerService.executeOrSendMessage(localIInputMethodClient, localHandlerCaller.obtainMessageIO(3020, i, InputMethodManagerService.this.mCurClient));
                while (true)
                {
                    return;
                    if (paramIntent.getAction().equals("android.intent.action.SCREEN_OFF"))
                    {
                        InputMethodManagerService.this.mScreenOn = false;
                        InputMethodManagerService.this.setImeWindowVisibilityStatusHiddenLocked();
                        break;
                    }
                    if (!paramIntent.getAction().equals("android.intent.action.CLOSE_SYSTEM_DIALOGS"))
                        break label165;
                    InputMethodManagerService.this.hideInputMethodMenu();
                }
                label165: Slog.w("InputMethodManagerService", "Unexpected intent " + paramIntent);
                break;
                label193: i = 0;
            }
        }
    }

    class SettingsObserver extends ContentObserver
    {
        SettingsObserver(Handler arg2)
        {
            super();
            ContentResolver localContentResolver = InputMethodManagerService.this.mContext.getContentResolver();
            localContentResolver.registerContentObserver(Settings.Secure.getUriFor("default_input_method"), false, this);
            localContentResolver.registerContentObserver(Settings.Secure.getUriFor("enabled_input_methods"), false, this);
            localContentResolver.registerContentObserver(Settings.Secure.getUriFor("selected_input_method_subtype"), false, this);
        }

        public void onChange(boolean paramBoolean)
        {
            synchronized (InputMethodManagerService.this.mMethodMap)
            {
                InputMethodManagerService.this.updateFromSettingsLocked();
                return;
            }
        }
    }

    class ClientState
    {
        final InputBinding binding;
        final IInputMethodClient client;
        InputMethodManagerService.SessionState curSession;
        final IInputContext inputContext;
        final int pid;
        boolean sessionRequested;
        final int uid;

        ClientState(IInputMethodClient paramIInputContext, IInputContext paramInt1, int paramInt2, int arg5)
        {
            this.client = paramIInputContext;
            this.inputContext = paramInt1;
            this.uid = paramInt2;
            int i;
            this.pid = i;
            this.binding = new InputBinding(null, this.inputContext.asBinder(), this.uid, this.pid);
        }

        public String toString()
        {
            return "ClientState{" + Integer.toHexString(System.identityHashCode(this)) + " uid " + this.uid + " pid " + this.pid + "}";
        }
    }

    class SessionState
    {
        final InputMethodManagerService.ClientState client;
        final IInputMethod method;
        final IInputMethodSession session;

        SessionState(InputMethodManagerService.ClientState paramIInputMethod, IInputMethod paramIInputMethodSession, IInputMethodSession arg4)
        {
            this.client = paramIInputMethod;
            this.method = paramIInputMethodSession;
            Object localObject;
            this.session = localObject;
        }

        public String toString()
        {
            return "SessionState{uid " + this.client.uid + " pid " + this.client.pid + " method " + Integer.toHexString(System.identityHashCode(this.method)) + " session " + Integer.toHexString(System.identityHashCode(this.session)) + "}";
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.InputMethodManagerService
 * JD-Core Version:        0.6.2
 */