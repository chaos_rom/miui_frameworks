package com.android.server;

public class NativeDaemonConnectorException extends Exception
{
    private String mCmd;
    private NativeDaemonEvent mEvent;

    public NativeDaemonConnectorException(String paramString)
    {
        super(paramString);
    }

    public NativeDaemonConnectorException(String paramString, NativeDaemonEvent paramNativeDaemonEvent)
    {
        super("command '" + paramString + "' failed with '" + paramNativeDaemonEvent + "'");
        this.mCmd = paramString;
        this.mEvent = paramNativeDaemonEvent;
    }

    public NativeDaemonConnectorException(String paramString, Throwable paramThrowable)
    {
        super(paramString, paramThrowable);
    }

    public String getCmd()
    {
        return this.mCmd;
    }

    public int getCode()
    {
        return this.mEvent.getCode();
    }

    public IllegalArgumentException rethrowAsParcelableException()
    {
        throw new IllegalStateException(getMessage(), this);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.NativeDaemonConnectorException
 * JD-Core Version:        0.6.2
 */