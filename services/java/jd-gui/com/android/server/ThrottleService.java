package com.android.server;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.net.INetworkManagementEventObserver.Stub;
import android.net.IThrottleManager.Stub;
import android.net.NetworkStats;
import android.net.NetworkStats.Entry;
import android.os.Binder;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.INetworkManagementService;
import android.os.INetworkManagementService.Stub;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.NtpTrustedTime;
import android.util.Slog;
import android.util.TrustedTime;
import java.io.File;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class ThrottleService extends IThrottleManager.Stub
{
    private static final String ACTION_POLL = "com.android.server.ThrottleManager.action.POLL";
    private static final String ACTION_RESET = "com.android.server.ThorottleManager.action.RESET";
    private static final boolean DBG = true;
    private static final int EVENT_IFACE_UP = 4;
    private static final int EVENT_POLICY_CHANGED = 1;
    private static final int EVENT_POLL_ALARM = 2;
    private static final int EVENT_REBOOT_RECOVERY = 0;
    private static final int EVENT_RESET_ALARM = 3;
    private static final int INITIAL_POLL_DELAY_SEC = 90;
    private static final long MAX_NTP_CACHE_AGE = 86400000L;
    private static final int NOTIFICATION_WARNING = 2;
    private static int POLL_REQUEST = 0;
    private static int RESET_REQUEST = 0;
    private static final String TAG = "ThrottleService";
    private static final String TESTING_ENABLED_PROPERTY = "persist.throttle.testing";
    private static final int TESTING_POLLING_PERIOD_SEC = 60;
    private static final int TESTING_RESET_PERIOD_SEC = 600;
    private static final long TESTING_THRESHOLD = 1048576L;
    private static final int THROTTLE_INDEX_UNINITIALIZED = -1;
    private static final int THROTTLE_INDEX_UNTHROTTLED;
    private static final boolean VDBG;
    private AlarmManager mAlarmManager;
    private Context mContext;
    private Handler mHandler;
    private String mIface;
    private InterfaceObserver mInterfaceObserver;
    private long mLastRead;
    private long mLastWrite;
    private long mMaxNtpCacheAge = 86400000L;
    private INetworkManagementService mNMService;
    private NotificationManager mNotificationManager;
    private PendingIntent mPendingPollIntent;
    private PendingIntent mPendingResetIntent;
    private int mPolicyNotificationsAllowedMask;
    private int mPolicyPollPeriodSec;
    private int mPolicyResetDay;
    private AtomicLong mPolicyThreshold;
    private AtomicInteger mPolicyThrottleValue;
    private Intent mPollStickyBroadcast;
    private DataRecorder mRecorder;
    private SettingsObserver mSettingsObserver;
    private HandlerThread mThread;
    private AtomicInteger mThrottleIndex;
    private Notification mThrottlingNotification;
    private TrustedTime mTime;
    private boolean mWarningNotificationSent = false;

    public ThrottleService(Context paramContext)
    {
        this(paramContext, getNetworkManagementService(), NtpTrustedTime.getInstance(paramContext), paramContext.getResources().getString(17039390));
    }

    public ThrottleService(Context paramContext, INetworkManagementService paramINetworkManagementService, TrustedTime paramTrustedTime, String paramString)
    {
        this.mContext = paramContext;
        this.mPolicyThreshold = new AtomicLong();
        this.mPolicyThrottleValue = new AtomicInteger();
        this.mThrottleIndex = new AtomicInteger();
        this.mIface = paramString;
        this.mAlarmManager = ((AlarmManager)this.mContext.getSystemService("alarm"));
        Intent localIntent1 = new Intent("com.android.server.ThrottleManager.action.POLL", null);
        this.mPendingPollIntent = PendingIntent.getBroadcast(this.mContext, POLL_REQUEST, localIntent1, 0);
        Intent localIntent2 = new Intent("com.android.server.ThorottleManager.action.RESET", null);
        this.mPendingResetIntent = PendingIntent.getBroadcast(this.mContext, RESET_REQUEST, localIntent2, 0);
        this.mNMService = paramINetworkManagementService;
        this.mTime = paramTrustedTime;
        this.mNotificationManager = ((NotificationManager)this.mContext.getSystemService("notification"));
    }

    private void enforceAccessPermission()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE", "ThrottleService");
    }

    private static INetworkManagementService getNetworkManagementService()
    {
        return INetworkManagementService.Stub.asInterface(ServiceManager.getService("network_management"));
    }

    private long ntpToWallTime(long paramLong)
    {
        if (this.mTime.hasCache());
        for (long l = this.mTime.currentTimeMillis(); ; l = System.currentTimeMillis())
            return System.currentTimeMillis() + (paramLong - l);
    }

    void dispatchPoll()
    {
        this.mHandler.obtainMessage(2).sendToTarget();
    }

    void dispatchReset()
    {
        this.mHandler.obtainMessage(3).sendToTarget();
    }

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.DUMP") != 0)
            paramPrintWriter.println("Permission Denial: can't dump ThrottleService from from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid());
        while (true)
        {
            return;
            paramPrintWriter.println();
            paramPrintWriter.println("The threshold is " + this.mPolicyThreshold.get() + ", after which you experince throttling to " + this.mPolicyThrottleValue.get() + "kbps");
            paramPrintWriter.println("Current period is " + (this.mRecorder.getPeriodEnd() - this.mRecorder.getPeriodStart()) / 1000L + " seconds long " + "and ends in " + (getResetTime(this.mIface) - System.currentTimeMillis()) / 1000L + " seconds.");
            paramPrintWriter.println("Polling every " + this.mPolicyPollPeriodSec + " seconds");
            paramPrintWriter.println("Current Throttle Index is " + this.mThrottleIndex.get());
            paramPrintWriter.println("mMaxNtpCacheAge=" + this.mMaxNtpCacheAge);
            for (int i = 0; i < this.mRecorder.getPeriodCount(); i++)
                paramPrintWriter.println(" Period[" + i + "] - read:" + this.mRecorder.getPeriodRx(i) + ", written:" + this.mRecorder.getPeriodTx(i));
        }
    }

    public long getByteCount(String paramString, int paramInt1, int paramInt2, int paramInt3)
    {
        enforceAccessPermission();
        long l;
        if ((paramInt2 == 0) && (this.mRecorder != null))
            if (paramInt1 == 0)
                l = this.mRecorder.getPeriodTx(paramInt3);
        while (true)
        {
            return l;
            if (paramInt1 == 1)
                l = this.mRecorder.getPeriodRx(paramInt3);
            else
                l = 0L;
        }
    }

    public int getCliffLevel(String paramString, int paramInt)
    {
        enforceAccessPermission();
        if (paramInt == 1);
        for (int i = this.mPolicyThrottleValue.get(); ; i = 0)
            return i;
    }

    public long getCliffThreshold(String paramString, int paramInt)
    {
        enforceAccessPermission();
        if (paramInt == 1);
        for (long l = this.mPolicyThreshold.get(); ; l = 0L)
            return l;
    }

    public String getHelpUri()
    {
        enforceAccessPermission();
        return Settings.Secure.getString(this.mContext.getContentResolver(), "throttle_help_uri");
    }

    public long getPeriodStartTime(String paramString)
    {
        long l = 0L;
        enforceAccessPermission();
        if (this.mRecorder != null)
            l = this.mRecorder.getPeriodStart();
        return ntpToWallTime(l);
    }

    public long getResetTime(String paramString)
    {
        enforceAccessPermission();
        long l = 0L;
        if (this.mRecorder != null)
            l = this.mRecorder.getPeriodEnd();
        return ntpToWallTime(l);
    }

    public int getThrottle(String paramString)
    {
        enforceAccessPermission();
        if (this.mThrottleIndex.get() == 1);
        for (int i = this.mPolicyThrottleValue.get(); ; i = 0)
            return i;
    }

    void shutdown()
    {
        if (this.mThread != null)
            this.mThread.quit();
        if (this.mSettingsObserver != null)
            this.mSettingsObserver.unregister(this.mContext);
        if (this.mPollStickyBroadcast != null)
            this.mContext.removeStickyBroadcast(this.mPollStickyBroadcast);
    }

    void systemReady()
    {
        this.mContext.registerReceiver(new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                ThrottleService.this.dispatchPoll();
            }
        }
        , new IntentFilter("com.android.server.ThrottleManager.action.POLL"));
        this.mContext.registerReceiver(new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                ThrottleService.this.dispatchReset();
            }
        }
        , new IntentFilter("com.android.server.ThorottleManager.action.RESET"));
        this.mThread = new HandlerThread("ThrottleService");
        this.mThread.start();
        this.mHandler = new MyHandler(this.mThread.getLooper());
        this.mHandler.obtainMessage(0).sendToTarget();
        this.mInterfaceObserver = new InterfaceObserver(this.mHandler, 4, this.mIface);
        try
        {
            this.mNMService.registerObserver(this.mInterfaceObserver);
            this.mSettingsObserver = new SettingsObserver(this.mHandler, 1);
            this.mSettingsObserver.register(this.mContext);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Slog.e("ThrottleService", "Could not register InterfaceObserver " + localRemoteException);
        }
    }

    private static class DataRecorder
    {
        private static final int DATA_FILE_VERSION = 1;
        private static final int MAX_SIMS_SUPPORTED = 3;
        Context mContext;
        int mCurrentPeriod;
        String mImsi = null;
        ThrottleService mParent;
        int mPeriodCount;
        Calendar mPeriodEnd;
        long[] mPeriodRxData;
        Calendar mPeriodStart;
        long[] mPeriodTxData;
        TelephonyManager mTelephonyManager;

        DataRecorder(Context paramContext, ThrottleService paramThrottleService)
        {
            this.mContext = paramContext;
            this.mParent = paramThrottleService;
            this.mTelephonyManager = ((TelephonyManager)this.mContext.getSystemService("phone"));
            synchronized (this.mParent)
            {
                this.mPeriodCount = 6;
                this.mPeriodRxData = new long[this.mPeriodCount];
                this.mPeriodTxData = new long[this.mPeriodCount];
                this.mPeriodStart = Calendar.getInstance();
                this.mPeriodEnd = Calendar.getInstance();
                retrieve();
                return;
            }
        }

        private void checkAndDeleteLRUDataFile(File paramFile)
        {
            File[] arrayOfFile1 = paramFile.listFiles();
            if ((arrayOfFile1 == null) || (arrayOfFile1.length <= 3));
            while (true)
            {
                return;
                Slog.d("ThrottleService", "Too many data files");
                do
                {
                    Object localObject = null;
                    for (File localFile : arrayOfFile1)
                        if ((localObject == null) || (localObject.lastModified() > localFile.lastModified()))
                            localObject = localFile;
                    if (localObject == null)
                        break;
                    Slog.d("ThrottleService", " deleting " + localObject);
                    localObject.delete();
                    arrayOfFile1 = paramFile.listFiles();
                }
                while (arrayOfFile1.length > 3);
            }
        }

        private void checkForSubscriberId()
        {
            if (this.mImsi != null);
            while (true)
            {
                return;
                this.mImsi = this.mTelephonyManager.getSubscriberId();
                if (this.mImsi != null)
                    retrieve();
            }
        }

        private File getDataFile()
        {
            File localFile1 = new File(Environment.getDataDirectory(), "system/throttle");
            localFile1.mkdirs();
            String str = this.mTelephonyManager.getSubscriberId();
            if (str == null);
            for (File localFile2 = useMRUFile(localFile1); ; localFile2 = new File(localFile1, Integer.toString(str.hashCode())))
            {
                localFile2.setLastModified(System.currentTimeMillis());
                checkAndDeleteLRUDataFile(localFile1);
                return localFile2;
            }
        }

        // ERROR //
        private void record()
        {
            // Byte code:
            //     0: new 94	java/lang/StringBuilder
            //     3: dup
            //     4: invokespecial 95	java/lang/StringBuilder:<init>	()V
            //     7: astore_1
            //     8: aload_1
            //     9: iconst_1
            //     10: invokevirtual 165	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     13: pop
            //     14: aload_1
            //     15: ldc 167
            //     17: invokevirtual 101	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     20: pop
            //     21: aload_1
            //     22: aload_0
            //     23: getfield 53	com/android/server/ThrottleService$DataRecorder:mPeriodCount	I
            //     26: invokevirtual 165	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     29: pop
            //     30: aload_1
            //     31: ldc 167
            //     33: invokevirtual 101	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     36: pop
            //     37: iconst_0
            //     38: istore 6
            //     40: iload 6
            //     42: aload_0
            //     43: getfield 53	com/android/server/ThrottleService$DataRecorder:mPeriodCount	I
            //     46: if_icmpge +28 -> 74
            //     49: aload_1
            //     50: aload_0
            //     51: getfield 55	com/android/server/ThrottleService$DataRecorder:mPeriodRxData	[J
            //     54: iload 6
            //     56: laload
            //     57: invokevirtual 170	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
            //     60: pop
            //     61: aload_1
            //     62: ldc 167
            //     64: invokevirtual 101	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     67: pop
            //     68: iinc 6 1
            //     71: goto -31 -> 40
            //     74: iconst_0
            //     75: istore 7
            //     77: iload 7
            //     79: aload_0
            //     80: getfield 53	com/android/server/ThrottleService$DataRecorder:mPeriodCount	I
            //     83: if_icmpge +28 -> 111
            //     86: aload_1
            //     87: aload_0
            //     88: getfield 57	com/android/server/ThrottleService$DataRecorder:mPeriodTxData	[J
            //     91: iload 7
            //     93: laload
            //     94: invokevirtual 170	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
            //     97: pop
            //     98: aload_1
            //     99: ldc 167
            //     101: invokevirtual 101	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     104: pop
            //     105: iinc 7 1
            //     108: goto -31 -> 77
            //     111: aload_1
            //     112: aload_0
            //     113: getfield 172	com/android/server/ThrottleService$DataRecorder:mCurrentPeriod	I
            //     116: invokevirtual 165	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     119: pop
            //     120: aload_1
            //     121: ldc 167
            //     123: invokevirtual 101	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     126: pop
            //     127: aload_1
            //     128: aload_0
            //     129: getfield 65	com/android/server/ThrottleService$DataRecorder:mPeriodStart	Ljava/util/Calendar;
            //     132: invokevirtual 175	java/util/Calendar:getTimeInMillis	()J
            //     135: invokevirtual 170	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
            //     138: pop
            //     139: aload_1
            //     140: ldc 167
            //     142: invokevirtual 101	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     145: pop
            //     146: aload_1
            //     147: aload_0
            //     148: getfield 67	com/android/server/ThrottleService$DataRecorder:mPeriodEnd	Ljava/util/Calendar;
            //     151: invokevirtual 175	java/util/Calendar:getTimeInMillis	()J
            //     154: invokevirtual 170	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
            //     157: pop
            //     158: aconst_null
            //     159: astore 13
            //     161: new 177	java/io/BufferedWriter
            //     164: dup
            //     165: new 179	java/io/FileWriter
            //     168: dup
            //     169: aload_0
            //     170: invokespecial 181	com/android/server/ThrottleService$DataRecorder:getDataFile	()Ljava/io/File;
            //     173: invokespecial 183	java/io/FileWriter:<init>	(Ljava/io/File;)V
            //     176: sipush 256
            //     179: invokespecial 186	java/io/BufferedWriter:<init>	(Ljava/io/Writer;I)V
            //     182: astore 14
            //     184: aload 14
            //     186: aload_1
            //     187: invokevirtual 108	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     190: invokevirtual 190	java/io/BufferedWriter:write	(Ljava/lang/String;)V
            //     193: aload 14
            //     195: ifnull +8 -> 203
            //     198: aload 14
            //     200: invokevirtual 193	java/io/BufferedWriter:close	()V
            //     203: return
            //     204: astore 21
            //     206: ldc 80
            //     208: ldc 195
            //     210: invokestatic 198	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     213: pop
            //     214: aload 13
            //     216: ifnull -13 -> 203
            //     219: aload 13
            //     221: invokevirtual 193	java/io/BufferedWriter:close	()V
            //     224: goto -21 -> 203
            //     227: astore 19
            //     229: goto -26 -> 203
            //     232: astore 16
            //     234: aload 13
            //     236: ifnull +8 -> 244
            //     239: aload 13
            //     241: invokevirtual 193	java/io/BufferedWriter:close	()V
            //     244: aload 16
            //     246: athrow
            //     247: astore 17
            //     249: goto -5 -> 244
            //     252: astore 20
            //     254: goto -51 -> 203
            //     257: astore 16
            //     259: aload 14
            //     261: astore 13
            //     263: goto -29 -> 234
            //     266: astore 15
            //     268: aload 14
            //     270: astore 13
            //     272: goto -66 -> 206
            //
            // Exception table:
            //     from	to	target	type
            //     161	184	204	java/io/IOException
            //     219	224	227	java/lang/Exception
            //     161	184	232	finally
            //     206	214	232	finally
            //     239	244	247	java/lang/Exception
            //     198	203	252	java/lang/Exception
            //     184	193	257	finally
            //     184	193	266	java/io/IOException
        }

        // ERROR //
        private void retrieve()
        {
            // Byte code:
            //     0: aload_0
            //     1: iconst_0
            //     2: invokespecial 202	com/android/server/ThrottleService$DataRecorder:zeroData	(I)V
            //     5: aload_0
            //     6: invokespecial 181	com/android/server/ThrottleService$DataRecorder:getDataFile	()Ljava/io/File;
            //     9: astore_1
            //     10: aconst_null
            //     11: astore_2
            //     12: aload_1
            //     13: invokevirtual 205	java/io/File:length	()J
            //     16: l2i
            //     17: newarray byte
            //     19: astore 8
            //     21: new 207	java/io/FileInputStream
            //     24: dup
            //     25: aload_1
            //     26: invokespecial 208	java/io/FileInputStream:<init>	(Ljava/io/File;)V
            //     29: astore 9
            //     31: aload 9
            //     33: aload 8
            //     35: invokevirtual 212	java/io/FileInputStream:read	([B)I
            //     38: pop
            //     39: aload 9
            //     41: ifnull +8 -> 49
            //     44: aload 9
            //     46: invokevirtual 213	java/io/FileInputStream:close	()V
            //     49: new 148	java/lang/String
            //     52: dup
            //     53: aload 8
            //     55: invokespecial 216	java/lang/String:<init>	([B)V
            //     58: astore 12
            //     60: aload 12
            //     62: ifnull +11 -> 73
            //     65: aload 12
            //     67: invokevirtual 218	java/lang/String:length	()I
            //     70: ifne +49 -> 119
            //     73: ldc 80
            //     75: ldc 220
            //     77: invokestatic 88	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
            //     80: pop
            //     81: return
            //     82: astore 5
            //     84: ldc 80
            //     86: ldc 222
            //     88: invokestatic 198	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     91: pop
            //     92: aload_2
            //     93: ifnull -12 -> 81
            //     96: aload_2
            //     97: invokevirtual 213	java/io/FileInputStream:close	()V
            //     100: goto -19 -> 81
            //     103: astore 7
            //     105: goto -24 -> 81
            //     108: astore_3
            //     109: aload_2
            //     110: ifnull +7 -> 117
            //     113: aload_2
            //     114: invokevirtual 213	java/io/FileInputStream:close	()V
            //     117: aload_3
            //     118: athrow
            //     119: aload 12
            //     121: ldc 167
            //     123: invokevirtual 226	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
            //     126: astore 14
            //     128: aload 14
            //     130: arraylength
            //     131: bipush 6
            //     133: if_icmpge +14 -> 147
            //     136: ldc 80
            //     138: ldc 228
            //     140: invokestatic 198	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     143: pop
            //     144: goto -63 -> 81
            //     147: iconst_0
            //     148: iconst_1
            //     149: iadd
            //     150: istore 15
            //     152: aload 14
            //     154: iconst_0
            //     155: aaload
            //     156: invokestatic 232	java/lang/Integer:parseInt	(Ljava/lang/String;)I
            //     159: iconst_1
            //     160: if_icmpeq +14 -> 174
            //     163: ldc 80
            //     165: ldc 234
            //     167: invokestatic 198	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     170: pop
            //     171: goto -90 -> 81
            //     174: iload 15
            //     176: iconst_1
            //     177: iadd
            //     178: istore 19
            //     180: aload 14
            //     182: iload 15
            //     184: aaload
            //     185: invokestatic 232	java/lang/Integer:parseInt	(Ljava/lang/String;)I
            //     188: istore 21
            //     190: aload 14
            //     192: arraylength
            //     193: iconst_5
            //     194: iload 21
            //     196: iconst_2
            //     197: imul
            //     198: iadd
            //     199: if_icmpeq +52 -> 251
            //     202: ldc 80
            //     204: new 94	java/lang/StringBuilder
            //     207: dup
            //     208: invokespecial 95	java/lang/StringBuilder:<init>	()V
            //     211: ldc 236
            //     213: invokevirtual 101	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     216: aload 14
            //     218: arraylength
            //     219: invokevirtual 165	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     222: ldc 238
            //     224: invokevirtual 101	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     227: iconst_5
            //     228: iload 21
            //     230: iconst_2
            //     231: imul
            //     232: iadd
            //     233: invokevirtual 165	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     236: ldc 240
            //     238: invokevirtual 101	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     241: invokevirtual 108	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     244: invokestatic 198	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     247: pop
            //     248: goto -167 -> 81
            //     251: iload 21
            //     253: newarray long
            //     255: astore 22
            //     257: iconst_0
            //     258: istore 23
            //     260: iload 19
            //     262: istore 15
            //     264: iload 23
            //     266: iload 21
            //     268: if_icmpge +32 -> 300
            //     271: iload 15
            //     273: iconst_1
            //     274: iadd
            //     275: istore 34
            //     277: aload 22
            //     279: iload 23
            //     281: aload 14
            //     283: iload 15
            //     285: aaload
            //     286: invokestatic 246	java/lang/Long:parseLong	(Ljava/lang/String;)J
            //     289: lastore
            //     290: iinc 23 1
            //     293: iload 34
            //     295: istore 15
            //     297: goto -33 -> 264
            //     300: iload 21
            //     302: newarray long
            //     304: astore 24
            //     306: iconst_0
            //     307: istore 25
            //     309: iload 25
            //     311: iload 21
            //     313: if_icmpge +32 -> 345
            //     316: iload 15
            //     318: iconst_1
            //     319: iadd
            //     320: istore 33
            //     322: aload 24
            //     324: iload 25
            //     326: aload 14
            //     328: iload 15
            //     330: aaload
            //     331: invokestatic 246	java/lang/Long:parseLong	(Ljava/lang/String;)J
            //     334: lastore
            //     335: iinc 25 1
            //     338: iload 33
            //     340: istore 15
            //     342: goto -33 -> 309
            //     345: iload 15
            //     347: iconst_1
            //     348: iadd
            //     349: istore 26
            //     351: aload 14
            //     353: iload 15
            //     355: aaload
            //     356: invokestatic 232	java/lang/Integer:parseInt	(Ljava/lang/String;)I
            //     359: istore 27
            //     361: new 248	java/util/GregorianCalendar
            //     364: dup
            //     365: invokespecial 249	java/util/GregorianCalendar:<init>	()V
            //     368: astore 28
            //     370: iload 26
            //     372: iconst_1
            //     373: iadd
            //     374: istore 15
            //     376: aload 28
            //     378: aload 14
            //     380: iload 26
            //     382: aaload
            //     383: invokestatic 246	java/lang/Long:parseLong	(Ljava/lang/String;)J
            //     386: invokevirtual 253	java/util/Calendar:setTimeInMillis	(J)V
            //     389: new 248	java/util/GregorianCalendar
            //     392: dup
            //     393: invokespecial 249	java/util/GregorianCalendar:<init>	()V
            //     396: astore 29
            //     398: iload 15
            //     400: iconst_1
            //     401: iadd
            //     402: pop
            //     403: aload 29
            //     405: aload 14
            //     407: iload 15
            //     409: aaload
            //     410: invokestatic 246	java/lang/Long:parseLong	(Ljava/lang/String;)J
            //     413: invokevirtual 253	java/util/Calendar:setTimeInMillis	(J)V
            //     416: aload_0
            //     417: getfield 39	com/android/server/ThrottleService$DataRecorder:mParent	Lcom/android/server/ThrottleService;
            //     420: astore 31
            //     422: aload 31
            //     424: monitorenter
            //     425: aload_0
            //     426: iload 21
            //     428: putfield 53	com/android/server/ThrottleService$DataRecorder:mPeriodCount	I
            //     431: aload_0
            //     432: aload 22
            //     434: putfield 55	com/android/server/ThrottleService$DataRecorder:mPeriodRxData	[J
            //     437: aload_0
            //     438: aload 24
            //     440: putfield 57	com/android/server/ThrottleService$DataRecorder:mPeriodTxData	[J
            //     443: aload_0
            //     444: iload 27
            //     446: putfield 172	com/android/server/ThrottleService$DataRecorder:mCurrentPeriod	I
            //     449: aload_0
            //     450: aload 28
            //     452: putfield 65	com/android/server/ThrottleService$DataRecorder:mPeriodStart	Ljava/util/Calendar;
            //     455: aload_0
            //     456: aload 29
            //     458: putfield 67	com/android/server/ThrottleService$DataRecorder:mPeriodEnd	Ljava/util/Calendar;
            //     461: aload 31
            //     463: monitorexit
            //     464: goto -383 -> 81
            //     467: astore 16
            //     469: iload 15
            //     471: pop
            //     472: ldc 80
            //     474: ldc 255
            //     476: invokestatic 198	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     479: pop
            //     480: goto -399 -> 81
            //     483: astore 32
            //     485: aload 31
            //     487: monitorexit
            //     488: aload 32
            //     490: athrow
            //     491: astore 4
            //     493: goto -376 -> 117
            //     496: astore 38
            //     498: goto -449 -> 49
            //     501: astore 20
            //     503: goto -31 -> 472
            //     506: astore_3
            //     507: aload 9
            //     509: astore_2
            //     510: goto -401 -> 109
            //     513: astore 10
            //     515: aload 9
            //     517: astore_2
            //     518: goto -434 -> 84
            //
            // Exception table:
            //     from	to	target	type
            //     12	31	82	java/io/IOException
            //     96	100	103	java/lang/Exception
            //     12	31	108	finally
            //     84	92	108	finally
            //     152	171	467	java/lang/Exception
            //     300	306	467	java/lang/Exception
            //     376	398	467	java/lang/Exception
            //     425	464	483	finally
            //     485	488	483	finally
            //     113	117	491	java/lang/Exception
            //     44	49	496	java/lang/Exception
            //     180	290	501	java/lang/Exception
            //     322	370	501	java/lang/Exception
            //     403	416	501	java/lang/Exception
            //     31	39	506	finally
            //     31	39	513	java/io/IOException
        }

        private void setPeriodEnd(Calendar paramCalendar)
        {
            synchronized (this.mParent)
            {
                this.mPeriodEnd = paramCalendar;
                return;
            }
        }

        private void setPeriodStart(Calendar paramCalendar)
        {
            synchronized (this.mParent)
            {
                this.mPeriodStart = paramCalendar;
                return;
            }
        }

        private File useMRUFile(File paramFile)
        {
            Object localObject = null;
            File[] arrayOfFile = paramFile.listFiles();
            if (arrayOfFile != null)
            {
                int i = arrayOfFile.length;
                for (int j = 0; j < i; j++)
                {
                    File localFile = arrayOfFile[j];
                    if ((localObject == null) || (((File)localObject).lastModified() < localFile.lastModified()))
                        localObject = localFile;
                }
            }
            if (localObject == null)
                localObject = new File(paramFile, "temp");
            return localObject;
        }

        private void zeroData(int paramInt)
        {
            ThrottleService localThrottleService = this.mParent;
            int i = 0;
            try
            {
                while (i < this.mPeriodCount)
                {
                    this.mPeriodRxData[i] = 0L;
                    this.mPeriodTxData[i] = 0L;
                    i++;
                }
                this.mCurrentPeriod = 0;
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        void addData(long paramLong1, long paramLong2)
        {
            checkForSubscriberId();
            synchronized (this.mParent)
            {
                long[] arrayOfLong1 = this.mPeriodRxData;
                int i = this.mCurrentPeriod;
                arrayOfLong1[i] = (paramLong1 + arrayOfLong1[i]);
                long[] arrayOfLong2 = this.mPeriodTxData;
                int j = this.mCurrentPeriod;
                arrayOfLong2[j] = (paramLong2 + arrayOfLong2[j]);
                record();
                return;
            }
        }

        public int getPeriodCount()
        {
            synchronized (this.mParent)
            {
                int i = this.mPeriodCount;
                return i;
            }
        }

        public long getPeriodEnd()
        {
            synchronized (this.mParent)
            {
                long l = this.mPeriodEnd.getTimeInMillis();
                return l;
            }
        }

        long getPeriodRx(int paramInt)
        {
            long l;
            synchronized (this.mParent)
            {
                if (paramInt > this.mPeriodCount)
                {
                    l = 0L;
                }
                else
                {
                    int i = this.mCurrentPeriod - paramInt;
                    if (i < 0)
                        i += this.mPeriodCount;
                    l = this.mPeriodRxData[i];
                }
            }
            return l;
        }

        public long getPeriodStart()
        {
            synchronized (this.mParent)
            {
                long l = this.mPeriodStart.getTimeInMillis();
                return l;
            }
        }

        long getPeriodTx(int paramInt)
        {
            long l;
            synchronized (this.mParent)
            {
                if (paramInt > this.mPeriodCount)
                {
                    l = 0L;
                }
                else
                {
                    int i = this.mCurrentPeriod - paramInt;
                    if (i < 0)
                        i += this.mPeriodCount;
                    l = this.mPeriodTxData[i];
                }
            }
            return l;
        }

        boolean setNextPeriod(Calendar paramCalendar1, Calendar paramCalendar2)
        {
            checkForSubscriberId();
            boolean bool = true;
            if ((paramCalendar1.equals(this.mPeriodStart)) && (paramCalendar2.equals(this.mPeriodEnd)))
                bool = false;
            while (true)
            {
                setPeriodStart(paramCalendar1);
                setPeriodEnd(paramCalendar2);
                record();
                return bool;
                synchronized (this.mParent)
                {
                    this.mCurrentPeriod = (1 + this.mCurrentPeriod);
                    if (this.mCurrentPeriod >= this.mPeriodCount)
                        this.mCurrentPeriod = 0;
                    this.mPeriodRxData[this.mCurrentPeriod] = 0L;
                    this.mPeriodTxData[this.mCurrentPeriod] = 0L;
                }
            }
        }
    }

    private class MyHandler extends Handler
    {
        public MyHandler(Looper arg2)
        {
            super();
        }

        private Calendar calculatePeriodEnd(long paramLong)
        {
            Calendar localCalendar = GregorianCalendar.getInstance();
            localCalendar.setTimeInMillis(paramLong);
            int i = localCalendar.get(5);
            localCalendar.set(5, ThrottleService.this.mPolicyResetDay);
            localCalendar.set(11, 0);
            localCalendar.set(12, 0);
            localCalendar.set(13, 0);
            localCalendar.set(14, 0);
            if (i >= ThrottleService.this.mPolicyResetDay)
            {
                int j = localCalendar.get(2);
                if (j == 11)
                {
                    localCalendar.set(1, 1 + localCalendar.get(1));
                    j = -1;
                }
                localCalendar.set(2, j + 1);
            }
            if (SystemProperties.get("persist.throttle.testing").equals("true"))
            {
                localCalendar = GregorianCalendar.getInstance();
                localCalendar.setTimeInMillis(paramLong);
                localCalendar.add(13, 600);
            }
            return localCalendar;
        }

        private Calendar calculatePeriodStart(Calendar paramCalendar)
        {
            Calendar localCalendar = (Calendar)paramCalendar.clone();
            int i = paramCalendar.get(2);
            if (paramCalendar.get(2) == 0)
            {
                i = 12;
                localCalendar.set(1, -1 + localCalendar.get(1));
            }
            localCalendar.set(2, i - 1);
            if (SystemProperties.get("persist.throttle.testing").equals("true"))
            {
                localCalendar = (Calendar)paramCalendar.clone();
                localCalendar.add(13, -600);
            }
            return localCalendar;
        }

        private void checkThrottleAndPostNotification(long paramLong)
        {
            long l1 = ThrottleService.this.mPolicyThreshold.get();
            if (l1 == 0L)
                clearThrottleAndNotification();
            while (true)
            {
                return;
                if (!ThrottleService.this.mTime.hasCache())
                {
                    Slog.w("ThrottleService", "missing trusted time, skipping throttle check");
                }
                else if (paramLong > l1)
                {
                    if (ThrottleService.this.mThrottleIndex.get() != 1)
                    {
                        ThrottleService.this.mThrottleIndex.set(1);
                        Slog.d("ThrottleService", "Threshold " + l1 + " exceeded!");
                        try
                        {
                            ThrottleService.this.mNMService.setInterfaceThrottle(ThrottleService.this.mIface, ThrottleService.this.mPolicyThrottleValue.get(), ThrottleService.this.mPolicyThrottleValue.get());
                            ThrottleService.this.mNotificationManager.cancel(17302874);
                            postNotification(17040524, 17040525, 17302874, 2);
                            Intent localIntent = new Intent("android.net.thrott.THROTTLE_ACTION");
                            localIntent.putExtra("level", ThrottleService.this.mPolicyThrottleValue.get());
                            ThrottleService.this.mContext.sendStickyBroadcast(localIntent);
                        }
                        catch (Exception localException)
                        {
                            while (true)
                                Slog.e("ThrottleService", "error setting Throttle: " + localException);
                        }
                    }
                }
                else
                {
                    clearThrottleAndNotification();
                    if ((0x2 & ThrottleService.this.mPolicyNotificationsAllowedMask) != 0)
                    {
                        long l2 = ThrottleService.this.mRecorder.getPeriodStart();
                        long l3 = ThrottleService.this.mRecorder.getPeriodEnd() - l2;
                        long l4 = System.currentTimeMillis() - l2;
                        if ((paramLong > l4 * (2L * l1) / (l4 + l3)) && (paramLong > l1 / 4L))
                        {
                            if (!ThrottleService.this.mWarningNotificationSent)
                            {
                                ThrottleService.access$1902(ThrottleService.this, true);
                                ThrottleService.this.mNotificationManager.cancel(17302874);
                                postNotification(17040522, 17040523, 17302874, 0);
                            }
                        }
                        else if (ThrottleService.this.mWarningNotificationSent == true)
                        {
                            ThrottleService.this.mNotificationManager.cancel(17302874);
                            ThrottleService.access$1902(ThrottleService.this, false);
                        }
                    }
                }
            }
        }

        private void clearThrottleAndNotification()
        {
            if (ThrottleService.this.mThrottleIndex.get() != 0)
                ThrottleService.this.mThrottleIndex.set(0);
            try
            {
                ThrottleService.this.mNMService.setInterfaceThrottle(ThrottleService.this.mIface, -1, -1);
                Intent localIntent = new Intent("android.net.thrott.THROTTLE_ACTION");
                localIntent.putExtra("level", -1);
                ThrottleService.this.mContext.sendStickyBroadcast(localIntent);
                ThrottleService.this.mNotificationManager.cancel(17302874);
                ThrottleService.access$1902(ThrottleService.this, false);
                return;
            }
            catch (Exception localException)
            {
                while (true)
                    Slog.e("ThrottleService", "error clearing Throttle: " + localException);
            }
        }

        private void onIfaceUp()
        {
            if (ThrottleService.this.mThrottleIndex.get() == 1);
            try
            {
                ThrottleService.this.mNMService.setInterfaceThrottle(ThrottleService.this.mIface, -1, -1);
                ThrottleService.this.mNMService.setInterfaceThrottle(ThrottleService.this.mIface, ThrottleService.this.mPolicyThrottleValue.get(), ThrottleService.this.mPolicyThrottleValue.get());
                return;
            }
            catch (Exception localException)
            {
                while (true)
                    Slog.e("ThrottleService", "error setting Throttle: " + localException);
            }
        }

        private void onPolicyChanged()
        {
            boolean bool = SystemProperties.get("persist.throttle.testing").equals("true");
            int i = ThrottleService.this.mContext.getResources().getInteger(17694757);
            ThrottleService.access$402(ThrottleService.this, Settings.Secure.getInt(ThrottleService.this.mContext.getContentResolver(), "throttle_polling_sec", i));
            long l1 = ThrottleService.this.mContext.getResources().getInteger(17694758);
            int j = ThrottleService.this.mContext.getResources().getInteger(17694759);
            long l2 = Settings.Secure.getLong(ThrottleService.this.mContext.getContentResolver(), "throttle_threshold_bytes", l1);
            int k = Settings.Secure.getInt(ThrottleService.this.mContext.getContentResolver(), "throttle_value_kbitsps", j);
            ThrottleService.this.mPolicyThreshold.set(l2);
            ThrottleService.this.mPolicyThrottleValue.set(k);
            if (bool)
            {
                ThrottleService.access$402(ThrottleService.this, 60);
                ThrottleService.this.mPolicyThreshold.set(1048576L);
            }
            ThrottleService.access$702(ThrottleService.this, Settings.Secure.getInt(ThrottleService.this.mContext.getContentResolver(), "throttle_reset_day", -1));
            if ((ThrottleService.this.mPolicyResetDay == -1) || (ThrottleService.this.mPolicyResetDay < 1) || (ThrottleService.this.mPolicyResetDay > 28))
            {
                Random localRandom = new Random();
                ThrottleService.access$702(ThrottleService.this, 1 + localRandom.nextInt(28));
                Settings.Secure.putInt(ThrottleService.this.mContext.getContentResolver(), "throttle_reset_day", ThrottleService.this.mPolicyResetDay);
            }
            if (ThrottleService.this.mIface == null)
                ThrottleService.this.mPolicyThreshold.set(0L);
            int m = ThrottleService.this.mContext.getResources().getInteger(17694760);
            ThrottleService.access$902(ThrottleService.this, Settings.Secure.getInt(ThrottleService.this.mContext.getContentResolver(), "throttle_notification_type", m));
            int n = Settings.Secure.getInt(ThrottleService.this.mContext.getContentResolver(), "throttle_max_ntp_cache_age_sec", 86400);
            ThrottleService.access$1002(ThrottleService.this, n * 1000);
            if (ThrottleService.this.mPolicyThreshold.get() != 0L)
                Slog.d("ThrottleService", "onPolicyChanged testing=" + bool + ", period=" + ThrottleService.this.mPolicyPollPeriodSec + ", threshold=" + ThrottleService.this.mPolicyThreshold.get() + ", value=" + ThrottleService.this.mPolicyThrottleValue.get() + ", resetDay=" + ThrottleService.this.mPolicyResetDay + ", noteType=" + ThrottleService.this.mPolicyNotificationsAllowedMask + ", mMaxNtpCacheAge=" + ThrottleService.this.mMaxNtpCacheAge);
            ThrottleService.this.mThrottleIndex.set(-1);
            onResetAlarm();
            onPollAlarm();
            Intent localIntent = new Intent("android.net.thrott.POLICY_CHANGED_ACTION");
            ThrottleService.this.mContext.sendBroadcast(localIntent);
        }

        private void onPollAlarm()
        {
            long l1 = SystemClock.elapsedRealtime() + 1000 * ThrottleService.this.mPolicyPollPeriodSec;
            if ((ThrottleService.this.mTime.getCacheAge() > ThrottleService.this.mMaxNtpCacheAge) && (ThrottleService.this.mTime.forceRefresh()))
                ThrottleService.this.dispatchReset();
            long l2 = 0L;
            long l3 = 0L;
            while (true)
            {
                try
                {
                    NetworkStats localNetworkStats = ThrottleService.this.mNMService.getNetworkStatsSummaryDev();
                    int i = localNetworkStats.findIndex(ThrottleService.this.mIface, -1, 0, 0);
                    if (i != -1)
                    {
                        NetworkStats.Entry localEntry = localNetworkStats.getValues(i, null);
                        l2 = localEntry.rxBytes - ThrottleService.this.mLastRead;
                        l3 = localEntry.txBytes - ThrottleService.this.mLastWrite;
                        break label589;
                        l2 += ThrottleService.this.mLastRead;
                        l3 += ThrottleService.this.mLastWrite;
                        ThrottleService.access$1302(ThrottleService.this, 0L);
                        ThrottleService.access$1402(ThrottleService.this, 0L);
                        boolean bool = "true".equals(SystemProperties.get("gsm.operator.isroaming"));
                        if (!bool)
                            ThrottleService.this.mRecorder.addData(l2, l3);
                        long l4 = ThrottleService.this.mRecorder.getPeriodRx(0);
                        long l5 = ThrottleService.this.mRecorder.getPeriodTx(0);
                        long l6 = l4 + l5;
                        if (ThrottleService.this.mPolicyThreshold.get() != 0L)
                            Slog.d("ThrottleService", "onPollAlarm - roaming =" + bool + ", read =" + l2 + ", written =" + l3 + ", new total =" + l6);
                        ThrottleService.access$1314(ThrottleService.this, l2);
                        ThrottleService.access$1414(ThrottleService.this, l3);
                        checkThrottleAndPostNotification(l6);
                        Intent localIntent = new Intent("android.net.thrott.POLL_ACTION");
                        localIntent.putExtra("cycleRead", l4);
                        localIntent.putExtra("cycleWrite", l5);
                        localIntent.putExtra("cycleStart", ThrottleService.this.getPeriodStartTime(ThrottleService.this.mIface));
                        localIntent.putExtra("cycleEnd", ThrottleService.this.getResetTime(ThrottleService.this.mIface));
                        ThrottleService.this.mContext.sendStickyBroadcast(localIntent);
                        ThrottleService.access$1502(ThrottleService.this, localIntent);
                        ThrottleService.this.mAlarmManager.cancel(ThrottleService.this.mPendingPollIntent);
                        ThrottleService.this.mAlarmManager.set(3, l1, ThrottleService.this.mPendingPollIntent);
                    }
                    else
                    {
                        Slog.w("ThrottleService", "unable to find stats for iface " + ThrottleService.this.mIface);
                    }
                }
                catch (IllegalStateException localIllegalStateException)
                {
                    Slog.e("ThrottleService", "problem during onPollAlarm: " + localIllegalStateException);
                    continue;
                }
                catch (RemoteException localRemoteException)
                {
                    Slog.e("ThrottleService", "problem during onPollAlarm: " + localRemoteException);
                    continue;
                }
                label589: if (l2 >= 0L)
                    if (l3 >= 0L);
            }
        }

        private void onRebootRecovery()
        {
            ThrottleService.this.mThrottleIndex.set(-1);
            ThrottleService.access$102(ThrottleService.this, new ThrottleService.DataRecorder(ThrottleService.this.mContext, ThrottleService.this));
            ThrottleService.this.mHandler.obtainMessage(1).sendToTarget();
            ThrottleService.this.mHandler.sendMessageDelayed(ThrottleService.this.mHandler.obtainMessage(2), 90000L);
        }

        private void onResetAlarm()
        {
            if (ThrottleService.this.mPolicyThreshold.get() != 0L)
                Slog.d("ThrottleService", "onResetAlarm - last period had " + ThrottleService.this.mRecorder.getPeriodRx(0) + " bytes read and " + ThrottleService.this.mRecorder.getPeriodTx(0) + " written");
            if (ThrottleService.this.mTime.getCacheAge() > ThrottleService.this.mMaxNtpCacheAge)
                ThrottleService.this.mTime.forceRefresh();
            if (ThrottleService.this.mTime.hasCache())
            {
                long l1 = ThrottleService.this.mTime.currentTimeMillis();
                Calendar localCalendar1 = calculatePeriodEnd(l1);
                Calendar localCalendar2 = calculatePeriodStart(localCalendar1);
                if (ThrottleService.this.mRecorder.setNextPeriod(localCalendar2, localCalendar1))
                    onPollAlarm();
                ThrottleService.this.mAlarmManager.cancel(ThrottleService.this.mPendingResetIntent);
                long l2 = localCalendar1.getTimeInMillis() - l1;
                ThrottleService.this.mAlarmManager.set(3, l2 + SystemClock.elapsedRealtime(), ThrottleService.this.mPendingResetIntent);
            }
        }

        private void postNotification(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        {
            Intent localIntent = new Intent();
            localIntent.setClassName("com.android.phone", "com.android.phone.DataUsage");
            localIntent.setFlags(1073741824);
            PendingIntent localPendingIntent = PendingIntent.getActivity(ThrottleService.this.mContext, 0, localIntent, 0);
            Resources localResources = Resources.getSystem();
            CharSequence localCharSequence1 = localResources.getText(paramInt1);
            CharSequence localCharSequence2 = localResources.getText(paramInt2);
            if (ThrottleService.this.mThrottlingNotification == null)
            {
                ThrottleService.access$2002(ThrottleService.this, new Notification());
                ThrottleService.this.mThrottlingNotification.when = 0L;
                ThrottleService.this.mThrottlingNotification.icon = paramInt3;
                Notification localNotification = ThrottleService.this.mThrottlingNotification;
                localNotification.defaults = (0xFFFFFFFE & localNotification.defaults);
            }
            ThrottleService.this.mThrottlingNotification.flags = paramInt4;
            ThrottleService.this.mThrottlingNotification.tickerText = localCharSequence1;
            ThrottleService.this.mThrottlingNotification.setLatestEventInfo(ThrottleService.this.mContext, localCharSequence1, localCharSequence2, localPendingIntent);
            ThrottleService.this.mNotificationManager.notify(ThrottleService.this.mThrottlingNotification.icon, ThrottleService.this.mThrottlingNotification);
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            }
            while (true)
            {
                return;
                onRebootRecovery();
                continue;
                onPolicyChanged();
                continue;
                onPollAlarm();
                continue;
                onResetAlarm();
                continue;
                onIfaceUp();
            }
        }
    }

    private static class SettingsObserver extends ContentObserver
    {
        private Handler mHandler;
        private int mMsg;

        SettingsObserver(Handler paramHandler, int paramInt)
        {
            super();
            this.mHandler = paramHandler;
            this.mMsg = paramInt;
        }

        public void onChange(boolean paramBoolean)
        {
            this.mHandler.obtainMessage(this.mMsg).sendToTarget();
        }

        void register(Context paramContext)
        {
            ContentResolver localContentResolver = paramContext.getContentResolver();
            localContentResolver.registerContentObserver(Settings.Secure.getUriFor("throttle_polling_sec"), false, this);
            localContentResolver.registerContentObserver(Settings.Secure.getUriFor("throttle_threshold_bytes"), false, this);
            localContentResolver.registerContentObserver(Settings.Secure.getUriFor("throttle_value_kbitsps"), false, this);
            localContentResolver.registerContentObserver(Settings.Secure.getUriFor("throttle_reset_day"), false, this);
            localContentResolver.registerContentObserver(Settings.Secure.getUriFor("throttle_notification_type"), false, this);
            localContentResolver.registerContentObserver(Settings.Secure.getUriFor("throttle_help_uri"), false, this);
            localContentResolver.registerContentObserver(Settings.Secure.getUriFor("throttle_max_ntp_cache_age_sec"), false, this);
        }

        void unregister(Context paramContext)
        {
            paramContext.getContentResolver().unregisterContentObserver(this);
        }
    }

    private static class InterfaceObserver extends INetworkManagementEventObserver.Stub
    {
        private Handler mHandler;
        private String mIface;
        private int mMsg;

        InterfaceObserver(Handler paramHandler, int paramInt, String paramString)
        {
            this.mHandler = paramHandler;
            this.mMsg = paramInt;
            this.mIface = paramString;
        }

        public void interfaceAdded(String paramString)
        {
            if (TextUtils.equals(paramString, this.mIface))
                this.mHandler.obtainMessage(this.mMsg).sendToTarget();
        }

        public void interfaceLinkStateChanged(String paramString, boolean paramBoolean)
        {
        }

        public void interfaceRemoved(String paramString)
        {
        }

        public void interfaceStatusChanged(String paramString, boolean paramBoolean)
        {
            if ((paramBoolean) && (TextUtils.equals(paramString, this.mIface)))
                this.mHandler.obtainMessage(this.mMsg).sendToTarget();
        }

        public void limitReached(String paramString1, String paramString2)
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.ThrottleService
 * JD-Core Version:        0.6.2
 */