package com.android.server;

import com.google.android.collect.Lists;
import java.util.ArrayList;

public class NativeDaemonEvent
{
    private final int mCmdNumber;
    private final int mCode;
    private final String mMessage;
    private String[] mParsed;
    private final String mRawEvent;

    private NativeDaemonEvent(int paramInt1, int paramInt2, String paramString1, String paramString2)
    {
        this.mCmdNumber = paramInt1;
        this.mCode = paramInt2;
        this.mMessage = paramString1;
        this.mRawEvent = paramString2;
        this.mParsed = null;
    }

    public static String[] filterMessageList(NativeDaemonEvent[] paramArrayOfNativeDaemonEvent, int paramInt)
    {
        ArrayList localArrayList = Lists.newArrayList();
        int i = paramArrayOfNativeDaemonEvent.length;
        for (int j = 0; j < i; j++)
        {
            NativeDaemonEvent localNativeDaemonEvent = paramArrayOfNativeDaemonEvent[j];
            if (localNativeDaemonEvent.getCode() == paramInt)
                localArrayList.add(localNativeDaemonEvent.getMessage());
        }
        return (String[])localArrayList.toArray(new String[localArrayList.size()]);
    }

    private static boolean isClassUnsolicited(int paramInt)
    {
        if ((paramInt >= 600) && (paramInt < 700));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static NativeDaemonEvent parseRawEvent(String paramString)
    {
        String[] arrayOfString = paramString.split(" ");
        if (arrayOfString.length < 2)
            throw new IllegalArgumentException("Insufficient arguments");
        int i;
        int k;
        int m;
        try
        {
            i = Integer.parseInt(arrayOfString[0]);
            int j = arrayOfString[0].length();
            k = j + 1;
            m = -1;
            if (!isClassUnsolicited(i))
                if (arrayOfString.length < 3)
                    throw new IllegalArgumentException("Insufficient arguemnts");
        }
        catch (NumberFormatException localNumberFormatException1)
        {
            throw new IllegalArgumentException("problem parsing code", localNumberFormatException1);
        }
        try
        {
            m = Integer.parseInt(arrayOfString[1]);
            int n = arrayOfString[1].length();
            k += n + 1;
            return new NativeDaemonEvent(m, i, paramString.substring(k), paramString);
        }
        catch (NumberFormatException localNumberFormatException2)
        {
            throw new IllegalArgumentException("problem parsing cmdNumber", localNumberFormatException2);
        }
    }

    public static String[] unescapeArgs(String paramString)
    {
        ArrayList localArrayList = new ArrayList();
        int i = paramString.length();
        int j = 0;
        int k = 0;
        if (paramString.charAt(0) == '"')
        {
            k = 1;
            j = 0 + 1;
        }
        while (j < i)
        {
            int i2;
            label48: int m;
            label78: String str;
            if (k != 0)
            {
                i2 = j;
                m = paramString.indexOf('"', i2);
                if ((m == -1) || (paramString.charAt(m - 1) != '\\'))
                {
                    if (m == -1)
                        m = i;
                    str = paramString.substring(j, m);
                    j += str.length();
                    if (k != 0)
                        break label208;
                    str = str.trim();
                }
            }
            int n;
            while (true)
            {
                str.replace("\\\\", "\\");
                str.replace("\\\"", "\"");
                localArrayList.add(str);
                n = paramString.indexOf(' ', j);
                int i1 = paramString.indexOf(" \"", j);
                if ((i1 <= -1) || (i1 > n))
                    break label214;
                k = 1;
                j = i1 + 2;
                break;
                i2 = m + 1;
                break label48;
                m = paramString.indexOf(' ', j);
                break label78;
                label208: j++;
            }
            label214: k = 0;
            if (n > -1)
                j = n + 1;
        }
        return (String[])localArrayList.toArray(new String[localArrayList.size()]);
    }

    public void checkCode(int paramInt)
    {
        if (this.mCode != paramInt)
            throw new IllegalStateException("Expected " + paramInt + " but was: " + this);
    }

    public int getCmdNumber()
    {
        return this.mCmdNumber;
    }

    public int getCode()
    {
        return this.mCode;
    }

    public String getField(int paramInt)
    {
        if (this.mParsed == null)
            this.mParsed = unescapeArgs(this.mRawEvent);
        int i = paramInt + 2;
        if (i > this.mParsed.length);
        for (String str = null; ; str = this.mParsed[i])
            return str;
    }

    public String getMessage()
    {
        return this.mMessage;
    }

    @Deprecated
    public String getRawEvent()
    {
        return this.mRawEvent;
    }

    public boolean isClassClientError()
    {
        if ((this.mCode >= 500) && (this.mCode < 600));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isClassContinue()
    {
        if ((this.mCode >= 100) && (this.mCode < 200));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isClassOk()
    {
        if ((this.mCode >= 200) && (this.mCode < 300));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isClassServerError()
    {
        if ((this.mCode >= 400) && (this.mCode < 500));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isClassUnsolicited()
    {
        return isClassUnsolicited(this.mCode);
    }

    public String toString()
    {
        return this.mRawEvent;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.NativeDaemonEvent
 * JD-Core Version:        0.6.2
 */