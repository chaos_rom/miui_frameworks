package com.android.server;

import android.content.Context;
import android.os.Binder;
import android.os.StatFs;
import java.io.File;
import java.io.PrintWriter;

public class DiskStatsService extends Binder
{
    private static final String TAG = "DiskStatsService";
    private final Context mContext;

    public DiskStatsService(Context paramContext)
    {
        this.mContext = paramContext;
    }

    private void reportFreeSpace(File paramFile, String paramString, PrintWriter paramPrintWriter)
    {
        long l1;
        long l2;
        long l3;
        try
        {
            StatFs localStatFs = new StatFs(paramFile.getPath());
            l1 = localStatFs.getBlockSize();
            l2 = localStatFs.getAvailableBlocks();
            l3 = localStatFs.getBlockCount();
            if ((l1 <= 0L) || (l3 <= 0L))
                throw new IllegalArgumentException("Invalid stat: bsize=" + l1 + " avail=" + l2 + " total=" + l3);
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("-Error: ");
            paramPrintWriter.println(localIllegalArgumentException.toString());
        }
        while (true)
        {
            return;
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("-Free: ");
            paramPrintWriter.print(l2 * l1 / 1024L);
            paramPrintWriter.print("K / ");
            paramPrintWriter.print(l3 * l1 / 1024L);
            paramPrintWriter.print("K total = ");
            paramPrintWriter.print(100L * l2 / l3);
            paramPrintWriter.println("% free");
        }
    }

    // ERROR //
    protected void dump(java.io.FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 17	com/android/server/DiskStatsService:mContext	Landroid/content/Context;
        //     4: ldc 94
        //     6: ldc 8
        //     8: invokevirtual 100	android/content/Context:enforceCallingOrSelfPermission	(Ljava/lang/String;Ljava/lang/String;)V
        //     11: sipush 512
        //     14: newarray byte
        //     16: astore 4
        //     18: iconst_0
        //     19: istore 5
        //     21: iload 5
        //     23: aload 4
        //     25: arraylength
        //     26: if_icmpge +17 -> 43
        //     29: aload 4
        //     31: iload 5
        //     33: iload 5
        //     35: i2b
        //     36: bastore
        //     37: iinc 5 1
        //     40: goto -19 -> 21
        //     43: new 25	java/io/File
        //     46: dup
        //     47: invokestatic 106	android/os/Environment:getDataDirectory	()Ljava/io/File;
        //     50: ldc 108
        //     52: invokespecial 111	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     55: astore 6
        //     57: aconst_null
        //     58: astore 7
        //     60: aconst_null
        //     61: astore 8
        //     63: invokestatic 117	android/os/SystemClock:uptimeMillis	()J
        //     66: lstore 9
        //     68: new 119	java/io/FileOutputStream
        //     71: dup
        //     72: aload 6
        //     74: invokespecial 122	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
        //     77: astore 11
        //     79: aload 11
        //     81: aload 4
        //     83: invokevirtual 126	java/io/FileOutputStream:write	([B)V
        //     86: aload 11
        //     88: ifnull +8 -> 96
        //     91: aload 11
        //     93: invokevirtual 129	java/io/FileOutputStream:close	()V
        //     96: invokestatic 117	android/os/SystemClock:uptimeMillis	()J
        //     99: lstore 14
        //     101: aload 6
        //     103: invokevirtual 133	java/io/File:exists	()Z
        //     106: ifeq +9 -> 115
        //     109: aload 6
        //     111: invokevirtual 136	java/io/File:delete	()Z
        //     114: pop
        //     115: aload 8
        //     117: ifnull +94 -> 211
        //     120: aload_2
        //     121: ldc 138
        //     123: invokevirtual 67	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     126: aload_2
        //     127: aload 8
        //     129: invokevirtual 139	java/io/IOException:toString	()Ljava/lang/String;
        //     132: invokevirtual 73	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     135: aload_0
        //     136: invokestatic 106	android/os/Environment:getDataDirectory	()Ljava/io/File;
        //     139: ldc 141
        //     141: aload_2
        //     142: invokespecial 143	com/android/server/DiskStatsService:reportFreeSpace	(Ljava/io/File;Ljava/lang/String;Ljava/io/PrintWriter;)V
        //     145: aload_0
        //     146: invokestatic 146	android/os/Environment:getDownloadCacheDirectory	()Ljava/io/File;
        //     149: ldc 148
        //     151: aload_2
        //     152: invokespecial 143	com/android/server/DiskStatsService:reportFreeSpace	(Ljava/io/File;Ljava/lang/String;Ljava/io/PrintWriter;)V
        //     155: aload_0
        //     156: new 25	java/io/File
        //     159: dup
        //     160: ldc 150
        //     162: invokespecial 151	java/io/File:<init>	(Ljava/lang/String;)V
        //     165: ldc 153
        //     167: aload_2
        //     168: invokespecial 143	com/android/server/DiskStatsService:reportFreeSpace	(Ljava/io/File;Ljava/lang/String;Ljava/io/PrintWriter;)V
        //     171: return
        //     172: astore 12
        //     174: aload 12
        //     176: astore 8
        //     178: aload 7
        //     180: ifnull -84 -> 96
        //     183: aload 7
        //     185: invokevirtual 129	java/io/FileOutputStream:close	()V
        //     188: goto -92 -> 96
        //     191: astore 13
        //     193: goto -97 -> 96
        //     196: astore 17
        //     198: aload 7
        //     200: ifnull +8 -> 208
        //     203: aload 7
        //     205: invokevirtual 129	java/io/FileOutputStream:close	()V
        //     208: aload 17
        //     210: athrow
        //     211: aload_2
        //     212: ldc 155
        //     214: invokevirtual 67	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     217: aload_2
        //     218: lload 14
        //     220: lload 9
        //     222: lsub
        //     223: invokevirtual 80	java/io/PrintWriter:print	(J)V
        //     226: aload_2
        //     227: ldc 157
        //     229: invokevirtual 73	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     232: goto -97 -> 135
        //     235: astore 18
        //     237: goto -29 -> 208
        //     240: astore 19
        //     242: goto -146 -> 96
        //     245: astore 17
        //     247: aload 11
        //     249: astore 7
        //     251: goto -53 -> 198
        //     254: astore 12
        //     256: aload 11
        //     258: astore 7
        //     260: goto -86 -> 174
        //
        // Exception table:
        //     from	to	target	type
        //     68	79	172	java/io/IOException
        //     183	188	191	java/io/IOException
        //     68	79	196	finally
        //     203	208	235	java/io/IOException
        //     91	96	240	java/io/IOException
        //     79	86	245	finally
        //     79	86	254	java/io/IOException
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.DiskStatsService
 * JD-Core Version:        0.6.2
 */