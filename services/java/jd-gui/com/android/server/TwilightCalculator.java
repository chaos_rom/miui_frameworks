package com.android.server;

import android.util.FloatMath;

public class TwilightCalculator
{
    private static final float ALTIDUTE_CORRECTION_CIVIL_TWILIGHT = -0.1047198F;
    private static final float C1 = 0.0334196F;
    private static final float C2 = 0.000349066F;
    private static final float C3 = 5.236E-06F;
    public static final int DAY = 0;
    private static final float DEGREES_TO_RADIANS = 0.01745329F;
    private static final float J0 = 0.0009F;
    public static final int NIGHT = 1;
    private static final float OBLIQUITY = 0.40928F;
    private static final long UTC_2000 = 946728000000L;
    public int mState;
    public long mSunrise;
    public long mSunset;

    public void calculateTwilight(long paramLong, double paramDouble1, double paramDouble2)
    {
        float f1 = (float)(paramLong - 946728000000L) / 86400000.0F;
        float f2 = 6.24006F + 0.01720197F * f1;
        float f3 = 3.141593F + (1.796593F + (f2 + 0.0334196F * FloatMath.sin(f2) + 0.000349066F * FloatMath.sin(2.0F * f2) + 5.236E-06F * FloatMath.sin(3.0F * f2)));
        double d1 = -paramDouble2 / 360.0D;
        double d2 = d1 + (0.0009F + (float)Math.round(f1 - 0.0009F - d1)) + 0.0053F * FloatMath.sin(f2) + -0.0069F * FloatMath.sin(2.0F * f3);
        double d3 = Math.asin(FloatMath.sin(f3) * FloatMath.sin(0.40928F));
        double d4 = paramDouble1 * 0.0174532923847437D;
        double d5 = (FloatMath.sin(-0.1047198F) - Math.sin(d4) * Math.sin(d3)) / (Math.cos(d4) * Math.cos(d3));
        if (d5 >= 1.0D)
        {
            this.mState = 1;
            this.mSunset = -1L;
            this.mSunrise = -1L;
        }
        while (true)
        {
            return;
            if (d5 <= -1.0D)
            {
                this.mState = 0;
                this.mSunset = -1L;
                this.mSunrise = -1L;
            }
            else
            {
                float f4 = (float)(Math.acos(d5) / 6.283185307179586D);
                this.mSunset = (946728000000L + Math.round(86400000.0D * (d2 + f4)));
                this.mSunrise = (946728000000L + Math.round(86400000.0D * (d2 - f4)));
                if ((this.mSunrise < paramLong) && (this.mSunset > paramLong))
                    this.mState = 0;
                else
                    this.mState = 1;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.TwilightCalculator
 * JD-Core Version:        0.6.2
 */