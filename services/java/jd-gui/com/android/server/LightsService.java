package com.android.server;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.Context;
import android.os.Handler;
import android.os.IHardwareService.Stub;
import android.os.Message;
import android.os.ServiceManager;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class LightsService
{
    static final int BRIGHTNESS_MODE_SENSOR = 1;
    static final int BRIGHTNESS_MODE_USER = 0;
    private static final boolean DEBUG = false;
    static final int LIGHT_FLASH_HARDWARE = 2;
    static final int LIGHT_FLASH_NONE = 0;
    static final int LIGHT_FLASH_TIMED = 1;
    static final int LIGHT_ID_ATTENTION = 5;
    static final int LIGHT_ID_BACKLIGHT = 0;
    static final int LIGHT_ID_BATTERY = 3;
    static final int LIGHT_ID_BLUETOOTH = 6;
    static final int LIGHT_ID_BUTTONS = 2;
    static final int LIGHT_ID_COUNT = 8;
    static final int LIGHT_ID_KEYBOARD = 1;
    static final int LIGHT_ID_NOTIFICATIONS = 4;
    static final int LIGHT_ID_WIFI = 7;
    private static final String TAG = "LightsService";
    private final Context mContext;
    private Handler mH = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            LightsService.Light.access$500((LightsService.Light)paramAnonymousMessage.obj);
        }
    };
    private final IHardwareService.Stub mLegacyFlashlightHack = new IHardwareService.Stub()
    {
        private static final String FLASHLIGHT_FILE = "/sys/class/leds/spotlight/brightness";

        public boolean getFlashlightEnabled()
        {
            boolean bool = false;
            try
            {
                FileInputStream localFileInputStream = new FileInputStream("/sys/class/leds/spotlight/brightness");
                int i = localFileInputStream.read();
                localFileInputStream.close();
                if (i != 48)
                    bool = true;
                label31: return bool;
            }
            catch (Exception localException)
            {
                break label31;
            }
        }

        public void setFlashlightEnabled(boolean paramAnonymousBoolean)
        {
            if ((LightsService.this.mContext.checkCallingOrSelfPermission("android.permission.FLASHLIGHT") != 0) && (LightsService.this.mContext.checkCallingOrSelfPermission("android.permission.HARDWARE_TEST") != 0))
                throw new SecurityException("Requires FLASHLIGHT or HARDWARE_TEST permission");
            try
            {
                FileOutputStream localFileOutputStream = new FileOutputStream("/sys/class/leds/spotlight/brightness");
                byte[] arrayOfByte = new byte[2];
                if (paramAnonymousBoolean);
                for (int i = 49; ; i = 48)
                {
                    arrayOfByte[0] = ((byte)i);
                    arrayOfByte[1] = 10;
                    localFileOutputStream.write(arrayOfByte);
                    localFileOutputStream.close();
                    label86: return;
                }
            }
            catch (Exception localException)
            {
                break label86;
            }
        }
    };
    private final Light[] mLights = new Light[8];
    private int mNativePointer = init_native();

    LightsService(Context paramContext)
    {
        this.mContext = paramContext;
        ServiceManager.addService("hardware", this.mLegacyFlashlightHack);
        for (int i = 0; i < 8; i++)
            this.mLights[i] = new Light(i, null);
    }

    private static native void finalize_native(int paramInt);

    private static native int init_native();

    private static native void setLight_native(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7);

    protected void finalize()
        throws Throwable
    {
        finalize_native(this.mNativePointer);
        super.finalize();
    }

    public Light getLight(int paramInt)
    {
        return this.mLights[paramInt];
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void setLight(int paramInt, Light paramLight)
    {
        this.mLights[paramInt] = paramLight;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_ACCESS)
    public class Light
    {
        private int mColor;
        private boolean mFlashing;
        private int mId;
        private int mMode;
        private int mOffMS;
        private int mOnMS;

        private Light(int arg2)
        {
            int i;
            this.mId = i;
        }

        @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
        Light(int paramInt1, int arg3)
        {
            this(paramInt1);
        }

        private void stopFlashing()
        {
            try
            {
                setLightLocked(this.mColor, 0, 0, 0, 0);
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public void pulse()
        {
            pulse(16777215, 7);
        }

        public void pulse(int paramInt1, int paramInt2)
        {
            try
            {
                if ((this.mColor == 0) && (!this.mFlashing))
                {
                    setLightLocked(paramInt1, 2, paramInt2, 1000, 0);
                    LightsService.this.mH.sendMessageDelayed(Message.obtain(LightsService.this.mH, 1, this), paramInt2);
                }
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public void setBrightness(int paramInt)
        {
            setBrightness(paramInt, 0);
        }

        public void setBrightness(int paramInt1, int paramInt2)
        {
            int i = paramInt1 & 0xFF;
            int j = i | (0xFF000000 | i << 16 | i << 8);
            try
            {
                setLightLocked(j, 0, 0, 0, paramInt2);
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public void setColor(int paramInt)
        {
            try
            {
                setLightLocked(paramInt, 0, 0, 0, 0);
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public void setFlashing(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        {
            try
            {
                setLightLocked(paramInt1, paramInt2, paramInt3, paramInt4, 0);
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        @MiuiHook(MiuiHook.MiuiHookType.CHANGE_ACCESS)
        void setLightLocked(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
        {
            if ((paramInt1 != this.mColor) || (paramInt2 != this.mMode) || (paramInt3 != this.mOnMS) || (paramInt4 != this.mOffMS))
            {
                this.mColor = paramInt1;
                this.mMode = paramInt2;
                this.mOnMS = paramInt3;
                this.mOffMS = paramInt4;
                LightsService.setLight_native(LightsService.this.mNativePointer, this.mId, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
            }
        }

        public void turnOff()
        {
            try
            {
                setLightLocked(0, 0, 0, 0, 0);
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.LightsService
 * JD-Core Version:        0.6.2
 */