package com.android.server;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.os.UEventObserver;
import android.os.UEventObserver.UEvent;
import android.util.Log;
import android.util.Slog;
import java.io.FileNotFoundException;
import java.io.FileReader;

class DockObserver extends UEventObserver
{
    private static final String DOCK_STATE_PATH = "/sys/class/switch/dock/state";
    private static final String DOCK_UEVENT_MATCH = "DEVPATH=/devices/virtual/switch/dock";
    private static final boolean LOG;
    private static final int MSG_DOCK_STATE;
    private static final String TAG = DockObserver.class.getSimpleName();
    private final Context mContext;
    private int mDockState = 0;
    private final Handler mHandler = new Handler()
    {
        // ERROR //
        public void handleMessage(android.os.Message paramAnonymousMessage)
        {
            // Byte code:
            //     0: aload_1
            //     1: getfield 23	android/os/Message:what	I
            //     4: tableswitch	default:+20 -> 24, 0:+21->25
            //     25: aload_0
            //     26: monitorenter
            //     27: invokestatic 27	com/android/server/DockObserver:access$000	()Ljava/lang/String;
            //     30: new 29	java/lang/StringBuilder
            //     33: dup
            //     34: invokespecial 30	java/lang/StringBuilder:<init>	()V
            //     37: ldc 32
            //     39: invokevirtual 36	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     42: aload_0
            //     43: getfield 12	com/android/server/DockObserver$1:this$0	Lcom/android/server/DockObserver;
            //     46: invokestatic 40	com/android/server/DockObserver:access$100	(Lcom/android/server/DockObserver;)I
            //     49: invokevirtual 43	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     52: invokevirtual 46	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     55: invokestatic 52	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
            //     58: pop
            //     59: aload_0
            //     60: getfield 12	com/android/server/DockObserver$1:this$0	Lcom/android/server/DockObserver;
            //     63: invokestatic 56	com/android/server/DockObserver:access$200	(Lcom/android/server/DockObserver;)Landroid/content/Context;
            //     66: invokevirtual 62	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
            //     69: astore 4
            //     71: aload 4
            //     73: ldc 64
            //     75: iconst_0
            //     76: invokestatic 70	android/provider/Settings$Secure:getInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)I
            //     79: ifne +22 -> 101
            //     82: invokestatic 27	com/android/server/DockObserver:access$000	()Ljava/lang/String;
            //     85: ldc 72
            //     87: invokestatic 52	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
            //     90: pop
            //     91: aload_0
            //     92: monitorexit
            //     93: goto -69 -> 24
            //     96: astore_2
            //     97: aload_0
            //     98: monitorexit
            //     99: aload_2
            //     100: athrow
            //     101: new 74	android/content/Intent
            //     104: dup
            //     105: ldc 76
            //     107: invokespecial 79	android/content/Intent:<init>	(Ljava/lang/String;)V
            //     110: astore 5
            //     112: aload 5
            //     114: ldc 80
            //     116: invokevirtual 84	android/content/Intent:addFlags	(I)Landroid/content/Intent;
            //     119: pop
            //     120: aload 5
            //     122: ldc 86
            //     124: aload_0
            //     125: getfield 12	com/android/server/DockObserver$1:this$0	Lcom/android/server/DockObserver;
            //     128: invokestatic 40	com/android/server/DockObserver:access$100	(Lcom/android/server/DockObserver;)I
            //     131: invokevirtual 90	android/content/Intent:putExtra	(Ljava/lang/String;I)Landroid/content/Intent;
            //     134: pop
            //     135: invokestatic 95	android/server/BluetoothService:readDockBluetoothAddress	()Ljava/lang/String;
            //     138: astore 8
            //     140: aload 8
            //     142: ifnull +19 -> 161
            //     145: aload 5
            //     147: ldc 97
            //     149: invokestatic 103	android/bluetooth/BluetoothAdapter:getDefaultAdapter	()Landroid/bluetooth/BluetoothAdapter;
            //     152: aload 8
            //     154: invokevirtual 107	android/bluetooth/BluetoothAdapter:getRemoteDevice	(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;
            //     157: invokevirtual 110	android/content/Intent:putExtra	(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
            //     160: pop
            //     161: aload 4
            //     163: ldc 112
            //     165: iconst_1
            //     166: invokestatic 115	android/provider/Settings$System:getInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)I
            //     169: iconst_1
            //     170: if_icmpne +131 -> 301
            //     173: aconst_null
            //     174: astore 9
            //     176: aload_0
            //     177: getfield 12	com/android/server/DockObserver$1:this$0	Lcom/android/server/DockObserver;
            //     180: invokestatic 40	com/android/server/DockObserver:access$100	(Lcom/android/server/DockObserver;)I
            //     183: ifne +153 -> 336
            //     186: aload_0
            //     187: getfield 12	com/android/server/DockObserver$1:this$0	Lcom/android/server/DockObserver;
            //     190: invokestatic 118	com/android/server/DockObserver:access$300	(Lcom/android/server/DockObserver;)I
            //     193: iconst_1
            //     194: if_icmpeq +196 -> 390
            //     197: aload_0
            //     198: getfield 12	com/android/server/DockObserver$1:this$0	Lcom/android/server/DockObserver;
            //     201: invokestatic 118	com/android/server/DockObserver:access$300	(Lcom/android/server/DockObserver;)I
            //     204: iconst_3
            //     205: if_icmpeq +185 -> 390
            //     208: aload_0
            //     209: getfield 12	com/android/server/DockObserver$1:this$0	Lcom/android/server/DockObserver;
            //     212: invokestatic 118	com/android/server/DockObserver:access$300	(Lcom/android/server/DockObserver;)I
            //     215: iconst_4
            //     216: if_icmpne +102 -> 318
            //     219: goto +171 -> 390
            //     222: aload 9
            //     224: ifnull +77 -> 301
            //     227: aload 4
            //     229: aload 9
            //     231: invokestatic 122	android/provider/Settings$System:getString	(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
            //     234: astore 10
            //     236: aload 10
            //     238: ifnull +63 -> 301
            //     241: new 29	java/lang/StringBuilder
            //     244: dup
            //     245: invokespecial 30	java/lang/StringBuilder:<init>	()V
            //     248: ldc 124
            //     250: invokevirtual 36	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     253: aload 10
            //     255: invokevirtual 36	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     258: invokevirtual 46	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     261: invokestatic 130	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
            //     264: astore 11
            //     266: aload 11
            //     268: ifnull +33 -> 301
            //     271: aload_0
            //     272: getfield 12	com/android/server/DockObserver$1:this$0	Lcom/android/server/DockObserver;
            //     275: invokestatic 56	com/android/server/DockObserver:access$200	(Lcom/android/server/DockObserver;)Landroid/content/Context;
            //     278: aload 11
            //     280: invokestatic 136	android/media/RingtoneManager:getRingtone	(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;
            //     283: astore 12
            //     285: aload 12
            //     287: ifnull +14 -> 301
            //     290: aload 12
            //     292: iconst_1
            //     293: invokevirtual 142	android/media/Ringtone:setStreamType	(I)V
            //     296: aload 12
            //     298: invokevirtual 145	android/media/Ringtone:play	()V
            //     301: aload_0
            //     302: getfield 12	com/android/server/DockObserver$1:this$0	Lcom/android/server/DockObserver;
            //     305: invokestatic 56	com/android/server/DockObserver:access$200	(Lcom/android/server/DockObserver;)Landroid/content/Context;
            //     308: aload 5
            //     310: invokevirtual 149	android/content/Context:sendStickyBroadcast	(Landroid/content/Intent;)V
            //     313: aload_0
            //     314: monitorexit
            //     315: goto -291 -> 24
            //     318: aload_0
            //     319: getfield 12	com/android/server/DockObserver$1:this$0	Lcom/android/server/DockObserver;
            //     322: invokestatic 118	com/android/server/DockObserver:access$300	(Lcom/android/server/DockObserver;)I
            //     325: iconst_2
            //     326: if_icmpne -104 -> 222
            //     329: ldc 151
            //     331: astore 9
            //     333: goto -111 -> 222
            //     336: aload_0
            //     337: getfield 12	com/android/server/DockObserver$1:this$0	Lcom/android/server/DockObserver;
            //     340: invokestatic 40	com/android/server/DockObserver:access$100	(Lcom/android/server/DockObserver;)I
            //     343: iconst_1
            //     344: if_icmpeq +53 -> 397
            //     347: aload_0
            //     348: getfield 12	com/android/server/DockObserver$1:this$0	Lcom/android/server/DockObserver;
            //     351: invokestatic 40	com/android/server/DockObserver:access$100	(Lcom/android/server/DockObserver;)I
            //     354: iconst_3
            //     355: if_icmpeq +42 -> 397
            //     358: aload_0
            //     359: getfield 12	com/android/server/DockObserver$1:this$0	Lcom/android/server/DockObserver;
            //     362: invokestatic 40	com/android/server/DockObserver:access$100	(Lcom/android/server/DockObserver;)I
            //     365: iconst_4
            //     366: if_icmpne +6 -> 372
            //     369: goto +28 -> 397
            //     372: aload_0
            //     373: getfield 12	com/android/server/DockObserver$1:this$0	Lcom/android/server/DockObserver;
            //     376: invokestatic 40	com/android/server/DockObserver:access$100	(Lcom/android/server/DockObserver;)I
            //     379: iconst_2
            //     380: if_icmpne -158 -> 222
            //     383: ldc 153
            //     385: astore 9
            //     387: goto -165 -> 222
            //     390: ldc 155
            //     392: astore 9
            //     394: goto -172 -> 222
            //     397: ldc 157
            //     399: astore 9
            //     401: goto -179 -> 222
            //
            // Exception table:
            //     from	to	target	type
            //     27	99	96	finally
            //     101	387	96	finally
        }
    };
    private PowerManagerService mPowerManager;
    private int mPreviousDockState = 0;
    private boolean mSystemReady;

    public DockObserver(Context paramContext, PowerManagerService paramPowerManagerService)
    {
        this.mContext = paramContext;
        this.mPowerManager = paramPowerManagerService;
        init();
        startObserving("DEVPATH=/devices/virtual/switch/dock");
    }

    private final void init()
    {
        char[] arrayOfChar = new char[1024];
        try
        {
            FileReader localFileReader = new FileReader("/sys/class/switch/dock/state");
            int i = localFileReader.read(arrayOfChar, 0, 1024);
            localFileReader.close();
            int j = Integer.valueOf(new String(arrayOfChar, 0, i).trim()).intValue();
            this.mDockState = j;
            this.mPreviousDockState = j;
            return;
        }
        catch (FileNotFoundException localFileNotFoundException)
        {
            while (true)
                Slog.w(TAG, "This kernel does not have dock station support");
        }
        catch (Exception localException)
        {
            while (true)
                Slog.e(TAG, "", localException);
        }
    }

    private final void update()
    {
        this.mHandler.sendEmptyMessage(0);
    }

    public void onUEvent(UEventObserver.UEvent paramUEvent)
    {
        if (Log.isLoggable(TAG, 2))
            Slog.v(TAG, "Dock UEVENT: " + paramUEvent.toString());
        try
        {
            int i = Integer.parseInt(paramUEvent.get("SWITCH_STATE"));
            if (i != this.mDockState)
            {
                this.mPreviousDockState = this.mDockState;
                this.mDockState = i;
                if (this.mSystemReady)
                {
                    if (((this.mPreviousDockState != 1) && (this.mPreviousDockState != 3) && (this.mPreviousDockState != 4)) || (this.mDockState != 0))
                        this.mPowerManager.userActivityWithForce(SystemClock.uptimeMillis(), false, true);
                    update();
                }
            }
            return;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            while (true)
                Slog.e(TAG, "Could not parse switch state from event " + paramUEvent);
        }
        finally
        {
        }
    }

    void systemReady()
    {
        try
        {
            if (this.mDockState != 0)
                update();
            this.mSystemReady = true;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.DockObserver
 * JD-Core Version:        0.6.2
 */