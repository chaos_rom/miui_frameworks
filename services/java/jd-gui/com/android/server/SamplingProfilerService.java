package com.android.server;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.os.Binder;
import android.os.DropBoxManager;
import android.os.FileObserver;
import android.os.SystemProperties;
import android.provider.Settings.Secure;
import android.util.Slog;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.PrintWriter;

public class SamplingProfilerService extends Binder
{
    private static final boolean LOCAL_LOGV = false;
    public static final String SNAPSHOT_DIR = "/data/snapshots";
    private static final String TAG = "SamplingProfilerService";
    private final Context mContext;
    private FileObserver snapshotObserver;

    public SamplingProfilerService(Context paramContext)
    {
        this.mContext = paramContext;
        registerSettingObserver(paramContext);
        startWorking(paramContext);
    }

    private void handleSnapshotFile(File paramFile, DropBoxManager paramDropBoxManager)
    {
        try
        {
            paramDropBoxManager.addFile("SamplingProfilerService", paramFile, 0);
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
                Slog.e("SamplingProfilerService", "Can't add " + paramFile.getPath() + " to dropbox", localIOException);
        }
        finally
        {
            paramFile.delete();
        }
    }

    private void registerSettingObserver(Context paramContext)
    {
        ContentResolver localContentResolver = paramContext.getContentResolver();
        localContentResolver.registerContentObserver(Settings.Secure.getUriFor("sampling_profiler_ms"), false, new SamplingProfilerSettingsObserver(localContentResolver));
    }

    private void startWorking(Context paramContext)
    {
        final DropBoxManager localDropBoxManager = (DropBoxManager)paramContext.getSystemService("dropbox");
        File[] arrayOfFile = new File("/data/snapshots").listFiles();
        for (int i = 0; (arrayOfFile != null) && (i < arrayOfFile.length); i++)
            handleSnapshotFile(arrayOfFile[i], localDropBoxManager);
        this.snapshotObserver = new FileObserver("/data/snapshots", 4)
        {
            public void onEvent(int paramAnonymousInt, String paramAnonymousString)
            {
                SamplingProfilerService.this.handleSnapshotFile(new File("/data/snapshots", paramAnonymousString), localDropBoxManager);
            }
        };
        this.snapshotObserver.startWatching();
    }

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.DUMP", "SamplingProfilerService");
        paramPrintWriter.println("SamplingProfilerService:");
        paramPrintWriter.println("Watching directory: /data/snapshots");
    }

    private class SamplingProfilerSettingsObserver extends ContentObserver
    {
        private ContentResolver mContentResolver;

        public SamplingProfilerSettingsObserver(ContentResolver arg2)
        {
            super();
            Object localObject;
            this.mContentResolver = localObject;
            onChange(false);
        }

        public void onChange(boolean paramBoolean)
        {
            SystemProperties.set("persist.sys.profiler_ms", Integer.valueOf(Settings.Secure.getInt(this.mContentResolver, "sampling_profiler_ms", 0)).toString());
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.SamplingProfilerService
 * JD-Core Version:        0.6.2
 */