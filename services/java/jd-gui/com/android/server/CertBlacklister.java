package com.android.server;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.os.Binder;
import android.provider.Settings.Secure;
import java.io.File;

public class CertBlacklister extends Binder
{
    private static final String BLACKLIST_ROOT = System.getenv("ANDROID_DATA") + "/misc/keychain/";
    public static final String PUBKEY_BLACKLIST_KEY = "pubkey_blacklist";
    public static final String PUBKEY_PATH = BLACKLIST_ROOT + "pubkey_blacklist.txt";
    public static final String SERIAL_BLACKLIST_KEY = "serial_blacklist";
    public static final String SERIAL_PATH = BLACKLIST_ROOT + "serial_blacklist.txt";
    private static final String TAG = "CertBlacklister";

    public CertBlacklister(Context paramContext)
    {
        registerObservers(paramContext.getContentResolver());
    }

    private BlacklistObserver buildPubkeyObserver(ContentResolver paramContentResolver)
    {
        return new BlacklistObserver("pubkey_blacklist", "pubkey", PUBKEY_PATH, paramContentResolver);
    }

    private BlacklistObserver buildSerialObserver(ContentResolver paramContentResolver)
    {
        return new BlacklistObserver("serial_blacklist", "serial", SERIAL_PATH, paramContentResolver);
    }

    private void registerObservers(ContentResolver paramContentResolver)
    {
        paramContentResolver.registerContentObserver(Settings.Secure.getUriFor("pubkey_blacklist"), true, buildPubkeyObserver(paramContentResolver));
        paramContentResolver.registerContentObserver(Settings.Secure.getUriFor("serial_blacklist"), true, buildSerialObserver(paramContentResolver));
    }

    private static class BlacklistObserver extends ContentObserver
    {
        private final ContentResolver mContentResolver;
        private final String mKey;
        private final String mName;
        private final String mPath;
        private final File mTmpDir;

        public BlacklistObserver(String paramString1, String paramString2, String paramString3, ContentResolver paramContentResolver)
        {
            super();
            this.mKey = paramString1;
            this.mName = paramString2;
            this.mPath = paramString3;
            this.mTmpDir = new File(this.mPath).getParentFile();
            this.mContentResolver = paramContentResolver;
        }

        private void writeBlacklist()
        {
            new Thread("BlacklistUpdater")
            {
                // ERROR //
                public void run()
                {
                    // Byte code:
                    //     0: aload_0
                    //     1: getfield 15	com/android/server/CertBlacklister$BlacklistObserver$1:this$0	Lcom/android/server/CertBlacklister$BlacklistObserver;
                    //     4: invokestatic 25	com/android/server/CertBlacklister$BlacklistObserver:access$000	(Lcom/android/server/CertBlacklister$BlacklistObserver;)Ljava/io/File;
                    //     7: astore_1
                    //     8: aload_1
                    //     9: monitorenter
                    //     10: aload_0
                    //     11: getfield 15	com/android/server/CertBlacklister$BlacklistObserver$1:this$0	Lcom/android/server/CertBlacklister$BlacklistObserver;
                    //     14: invokevirtual 29	com/android/server/CertBlacklister$BlacklistObserver:getValue	()Ljava/lang/String;
                    //     17: astore_3
                    //     18: aload_3
                    //     19: ifnull +97 -> 116
                    //     22: ldc 31
                    //     24: ldc 33
                    //     26: invokestatic 39	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
                    //     29: pop
                    //     30: aconst_null
                    //     31: astore 5
                    //     33: ldc 41
                    //     35: ldc 43
                    //     37: aload_0
                    //     38: getfield 15	com/android/server/CertBlacklister$BlacklistObserver$1:this$0	Lcom/android/server/CertBlacklister$BlacklistObserver;
                    //     41: invokestatic 25	com/android/server/CertBlacklister$BlacklistObserver:access$000	(Lcom/android/server/CertBlacklister$BlacklistObserver;)Ljava/io/File;
                    //     44: invokestatic 49	java/io/File:createTempFile	(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
                    //     47: astore 9
                    //     49: aload 9
                    //     51: iconst_1
                    //     52: iconst_0
                    //     53: invokevirtual 53	java/io/File:setReadable	(ZZ)Z
                    //     56: pop
                    //     57: new 55	java/io/FileOutputStream
                    //     60: dup
                    //     61: aload 9
                    //     63: invokespecial 58	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
                    //     66: astore 11
                    //     68: aload 11
                    //     70: aload_3
                    //     71: invokevirtual 64	java/lang/String:getBytes	()[B
                    //     74: invokevirtual 68	java/io/FileOutputStream:write	([B)V
                    //     77: aload 11
                    //     79: invokestatic 74	android/os/FileUtils:sync	(Ljava/io/FileOutputStream;)Z
                    //     82: pop
                    //     83: aload 9
                    //     85: new 45	java/io/File
                    //     88: dup
                    //     89: aload_0
                    //     90: getfield 15	com/android/server/CertBlacklister$BlacklistObserver$1:this$0	Lcom/android/server/CertBlacklister$BlacklistObserver;
                    //     93: invokestatic 78	com/android/server/CertBlacklister$BlacklistObserver:access$100	(Lcom/android/server/CertBlacklister$BlacklistObserver;)Ljava/lang/String;
                    //     96: invokespecial 79	java/io/File:<init>	(Ljava/lang/String;)V
                    //     99: invokevirtual 83	java/io/File:renameTo	(Ljava/io/File;)Z
                    //     102: pop
                    //     103: ldc 31
                    //     105: ldc 85
                    //     107: invokestatic 39	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
                    //     110: pop
                    //     111: aload 11
                    //     113: invokestatic 91	libcore/io/IoUtils:closeQuietly	(Ljava/lang/AutoCloseable;)V
                    //     116: aload_1
                    //     117: monitorexit
                    //     118: return
                    //     119: astore 7
                    //     121: ldc 31
                    //     123: ldc 93
                    //     125: aload 7
                    //     127: invokestatic 97	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
                    //     130: pop
                    //     131: aload 5
                    //     133: invokestatic 91	libcore/io/IoUtils:closeQuietly	(Ljava/lang/AutoCloseable;)V
                    //     136: goto -20 -> 116
                    //     139: astore_2
                    //     140: aload_1
                    //     141: monitorexit
                    //     142: aload_2
                    //     143: athrow
                    //     144: astore 6
                    //     146: aload 5
                    //     148: invokestatic 91	libcore/io/IoUtils:closeQuietly	(Ljava/lang/AutoCloseable;)V
                    //     151: aload 6
                    //     153: athrow
                    //     154: astore 6
                    //     156: aload 11
                    //     158: astore 5
                    //     160: goto -14 -> 146
                    //     163: astore 7
                    //     165: aload 11
                    //     167: astore 5
                    //     169: goto -48 -> 121
                    //
                    // Exception table:
                    //     from	to	target	type
                    //     33	68	119	java/io/IOException
                    //     10	30	139	finally
                    //     111	118	139	finally
                    //     131	142	139	finally
                    //     146	154	139	finally
                    //     33	68	144	finally
                    //     121	131	144	finally
                    //     68	111	154	finally
                    //     68	111	163	java/io/IOException
                }
            }
            .start();
        }

        public String getValue()
        {
            return Settings.Secure.getString(this.mContentResolver, this.mKey);
        }

        public void onChange(boolean paramBoolean)
        {
            super.onChange(paramBoolean);
            writeBlacklist();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.CertBlacklister
 * JD-Core Version:        0.6.2
 */