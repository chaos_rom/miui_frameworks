package com.android.server;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.AlarmManager;
import android.app.AppGlobals;
import android.app.PendingIntent;
import android.appwidget.AppWidgetProviderInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.Intent.FilterComparison;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.IPackageManager;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.UserId;
import android.util.Pair;
import android.util.Slog;
import android.view.Display;
import android.view.WindowManager;
import android.widget.RemoteViews;
import com.android.internal.appwidget.IAppWidgetHost;
import com.android.internal.os.AtomicFile;
import com.android.internal.util.FastXmlSerializer;
import com.android.internal.widget.IRemoteViewsAdapterConnection;
import com.android.internal.widget.IRemoteViewsAdapterConnection.Stub;
import com.android.internal.widget.IRemoteViewsFactory;
import com.android.internal.widget.IRemoteViewsFactory.Stub;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.xmlpull.v1.XmlSerializer;

class AppWidgetServiceImpl
{
    private static final int MIN_UPDATE_PERIOD = 1800000;
    private static final String SETTINGS_FILENAME = "appwidgets.xml";
    private static final String TAG = "AppWidgetServiceImpl";
    AlarmManager mAlarmManager;
    final ArrayList<AppWidgetId> mAppWidgetIds = new ArrayList();
    private final HashMap<Pair<Integer, Intent.FilterComparison>, ServiceConnection> mBoundRemoteViewsServices = new HashMap();
    Context mContext;
    ArrayList<Host> mDeletedHosts = new ArrayList();
    ArrayList<Provider> mDeletedProviders = new ArrayList();
    ArrayList<Host> mHosts = new ArrayList();
    ArrayList<Provider> mInstalledProviders = new ArrayList();
    Locale mLocale;
    int mMaxWidgetBitmapMemory;
    int mNextAppWidgetId = 1;
    HashSet<String> mPackagesWithBindWidgetPermission = new HashSet();
    IPackageManager mPm;
    private final HashMap<Intent.FilterComparison, HashSet<Integer>> mRemoteViewsServicesAppWidgets = new HashMap();
    boolean mSafeMode;
    boolean mStateLoaded;
    int mUserId;

    AppWidgetServiceImpl(Context paramContext, int paramInt)
    {
        this.mContext = paramContext;
        this.mPm = AppGlobals.getPackageManager();
        this.mAlarmManager = ((AlarmManager)this.mContext.getSystemService("alarm"));
        this.mUserId = paramInt;
        computeMaximumWidgetBitmapMemory();
    }

    // ERROR //
    private void bindAppWidgetIdImpl(int paramInt, ComponentName paramComponentName)
    {
        // Byte code:
        //     0: invokestatic 131	android/os/Binder:clearCallingIdentity	()J
        //     3: lstore_3
        //     4: aload_0
        //     5: getfield 85	com/android/server/AppWidgetServiceImpl:mAppWidgetIds	Ljava/util/ArrayList;
        //     8: astore 6
        //     10: aload 6
        //     12: monitorenter
        //     13: aload_0
        //     14: invokespecial 134	com/android/server/AppWidgetServiceImpl:ensureStateLoadedLocked	()V
        //     17: aload_0
        //     18: iload_1
        //     19: invokevirtual 138	com/android/server/AppWidgetServiceImpl:lookupAppWidgetIdLocked	(I)Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
        //     22: astore 8
        //     24: aload 8
        //     26: ifnonnull +30 -> 56
        //     29: new 140	java/lang/IllegalArgumentException
        //     32: dup
        //     33: ldc 142
        //     35: invokespecial 145	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     38: athrow
        //     39: astore 7
        //     41: aload 6
        //     43: monitorexit
        //     44: aload 7
        //     46: athrow
        //     47: astore 5
        //     49: lload_3
        //     50: invokestatic 149	android/os/Binder:restoreCallingIdentity	(J)V
        //     53: aload 5
        //     55: athrow
        //     56: aload 8
        //     58: getfield 153	com/android/server/AppWidgetServiceImpl$AppWidgetId:provider	Lcom/android/server/AppWidgetServiceImpl$Provider;
        //     61: ifnull +49 -> 110
        //     64: new 140	java/lang/IllegalArgumentException
        //     67: dup
        //     68: new 155	java/lang/StringBuilder
        //     71: dup
        //     72: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     75: ldc 158
        //     77: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     80: iload_1
        //     81: invokevirtual 165	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     84: ldc 167
        //     86: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     89: aload 8
        //     91: getfield 153	com/android/server/AppWidgetServiceImpl$AppWidgetId:provider	Lcom/android/server/AppWidgetServiceImpl$Provider;
        //     94: getfield 171	com/android/server/AppWidgetServiceImpl$Provider:info	Landroid/appwidget/AppWidgetProviderInfo;
        //     97: getfield 176	android/appwidget/AppWidgetProviderInfo:provider	Landroid/content/ComponentName;
        //     100: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     103: invokevirtual 183	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     106: invokespecial 145	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     109: athrow
        //     110: aload_0
        //     111: aload_2
        //     112: invokevirtual 187	com/android/server/AppWidgetServiceImpl:lookupProviderLocked	(Landroid/content/ComponentName;)Lcom/android/server/AppWidgetServiceImpl$Provider;
        //     115: astore 9
        //     117: aload 9
        //     119: ifnonnull +30 -> 149
        //     122: new 140	java/lang/IllegalArgumentException
        //     125: dup
        //     126: new 155	java/lang/StringBuilder
        //     129: dup
        //     130: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     133: ldc 189
        //     135: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     138: aload_2
        //     139: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     142: invokevirtual 183	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     145: invokespecial 145	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     148: athrow
        //     149: aload 9
        //     151: getfield 192	com/android/server/AppWidgetServiceImpl$Provider:zombie	Z
        //     154: ifeq +30 -> 184
        //     157: new 140	java/lang/IllegalArgumentException
        //     160: dup
        //     161: new 155	java/lang/StringBuilder
        //     164: dup
        //     165: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     168: ldc 194
        //     170: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     173: aload_2
        //     174: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     177: invokevirtual 183	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     180: invokespecial 145	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     183: athrow
        //     184: aload 8
        //     186: aload 9
        //     188: putfield 153	com/android/server/AppWidgetServiceImpl$AppWidgetId:provider	Lcom/android/server/AppWidgetServiceImpl$Provider;
        //     191: aload 9
        //     193: getfield 197	com/android/server/AppWidgetServiceImpl$Provider:instances	Ljava/util/ArrayList;
        //     196: aload 8
        //     198: invokevirtual 201	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     201: pop
        //     202: aload 9
        //     204: getfield 197	com/android/server/AppWidgetServiceImpl$Provider:instances	Ljava/util/ArrayList;
        //     207: invokevirtual 205	java/util/ArrayList:size	()I
        //     210: iconst_1
        //     211: if_icmpne +9 -> 220
        //     214: aload_0
        //     215: aload 9
        //     217: invokevirtual 209	com/android/server/AppWidgetServiceImpl:sendEnableIntentLocked	(Lcom/android/server/AppWidgetServiceImpl$Provider;)V
        //     220: iconst_1
        //     221: newarray int
        //     223: astore 11
        //     225: aload 11
        //     227: iconst_0
        //     228: iload_1
        //     229: iastore
        //     230: aload_0
        //     231: aload 9
        //     233: aload 11
        //     235: invokevirtual 213	com/android/server/AppWidgetServiceImpl:sendUpdateIntentLocked	(Lcom/android/server/AppWidgetServiceImpl$Provider;[I)V
        //     238: aload_0
        //     239: aload 9
        //     241: aload 9
        //     243: invokestatic 217	com/android/server/AppWidgetServiceImpl:getAppWidgetIds	(Lcom/android/server/AppWidgetServiceImpl$Provider;)[I
        //     246: invokevirtual 220	com/android/server/AppWidgetServiceImpl:registerForBroadcastsLocked	(Lcom/android/server/AppWidgetServiceImpl$Provider;[I)V
        //     249: aload_0
        //     250: invokevirtual 223	com/android/server/AppWidgetServiceImpl:saveStateLocked	()V
        //     253: aload 6
        //     255: monitorexit
        //     256: lload_3
        //     257: invokestatic 149	android/os/Binder:restoreCallingIdentity	(J)V
        //     260: return
        //
        // Exception table:
        //     from	to	target	type
        //     13	44	39	finally
        //     56	256	39	finally
        //     4	13	47	finally
        //     44	47	47	finally
    }

    private boolean callerHasBindAppWidgetPermission(String paramString)
    {
        boolean bool1 = false;
        int i = Binder.getCallingUid();
        try
        {
            boolean bool2 = UserId.isSameApp(i, getUidForPackage(paramString));
            if (!bool2)
                return bool1;
        }
        catch (Exception localException)
        {
            while (true)
            {
                continue;
                synchronized (this.mAppWidgetIds)
                {
                    ensureStateLoadedLocked();
                    bool1 = this.mPackagesWithBindWidgetPermission.contains(paramString);
                }
            }
        }
    }

    private void decrementAppWidgetServiceRefCount(AppWidgetId paramAppWidgetId)
    {
        Iterator localIterator = this.mRemoteViewsServicesAppWidgets.keySet().iterator();
        while (localIterator.hasNext())
        {
            Intent.FilterComparison localFilterComparison = (Intent.FilterComparison)localIterator.next();
            HashSet localHashSet = (HashSet)this.mRemoteViewsServicesAppWidgets.get(localFilterComparison);
            if ((localHashSet.remove(Integer.valueOf(paramAppWidgetId.appWidgetId))) && (localHashSet.isEmpty()))
            {
                destroyRemoteViewsService(localFilterComparison.getIntent(), paramAppWidgetId);
                localIterator.remove();
            }
        }
    }

    private void destroyRemoteViewsService(final Intent paramIntent, AppWidgetId paramAppWidgetId)
    {
        ServiceConnection local1 = new ServiceConnection()
        {
            public void onServiceConnected(ComponentName paramAnonymousComponentName, IBinder paramAnonymousIBinder)
            {
                IRemoteViewsFactory localIRemoteViewsFactory = IRemoteViewsFactory.Stub.asInterface(paramAnonymousIBinder);
                try
                {
                    localIRemoteViewsFactory.onDestroy(paramIntent);
                    AppWidgetServiceImpl.this.mContext.unbindService(this);
                    return;
                }
                catch (RemoteException localRemoteException)
                {
                    while (true)
                        localRemoteException.printStackTrace();
                }
                catch (RuntimeException localRuntimeException)
                {
                    while (true)
                        localRuntimeException.printStackTrace();
                }
            }

            public void onServiceDisconnected(ComponentName paramAnonymousComponentName)
            {
            }
        };
        int i = UserId.getUserId(paramAppWidgetId.provider.uid);
        long l = Binder.clearCallingIdentity();
        try
        {
            this.mContext.bindService(paramIntent, local1, 1, i);
            return;
        }
        finally
        {
            Binder.restoreCallingIdentity(l);
        }
    }

    private void dumpAppWidgetId(AppWidgetId paramAppWidgetId, int paramInt, PrintWriter paramPrintWriter)
    {
        paramPrintWriter.print("    [");
        paramPrintWriter.print(paramInt);
        paramPrintWriter.print("] id=");
        paramPrintWriter.println(paramAppWidgetId.appWidgetId);
        paramPrintWriter.print("        hostId=");
        paramPrintWriter.print(paramAppWidgetId.host.hostId);
        paramPrintWriter.print(' ');
        paramPrintWriter.print(paramAppWidgetId.host.packageName);
        paramPrintWriter.print('/');
        paramPrintWriter.println(paramAppWidgetId.host.uid);
        if (paramAppWidgetId.provider != null)
        {
            paramPrintWriter.print("        provider=");
            paramPrintWriter.println(paramAppWidgetId.provider.info.provider.flattenToShortString());
        }
        if (paramAppWidgetId.host != null)
        {
            paramPrintWriter.print("        host.callbacks=");
            paramPrintWriter.println(paramAppWidgetId.host.callbacks);
        }
        if (paramAppWidgetId.views != null)
        {
            paramPrintWriter.print("        views=");
            paramPrintWriter.println(paramAppWidgetId.views);
        }
    }

    private void dumpHost(Host paramHost, int paramInt, PrintWriter paramPrintWriter)
    {
        paramPrintWriter.print("    [");
        paramPrintWriter.print(paramInt);
        paramPrintWriter.print("] hostId=");
        paramPrintWriter.print(paramHost.hostId);
        paramPrintWriter.print(' ');
        paramPrintWriter.print(paramHost.packageName);
        paramPrintWriter.print('/');
        paramPrintWriter.print(paramHost.uid);
        paramPrintWriter.println(':');
        paramPrintWriter.print("        callbacks=");
        paramPrintWriter.println(paramHost.callbacks);
        paramPrintWriter.print("        instances.size=");
        paramPrintWriter.print(paramHost.instances.size());
        paramPrintWriter.print(" zombie=");
        paramPrintWriter.println(paramHost.zombie);
    }

    private void dumpProvider(Provider paramProvider, int paramInt, PrintWriter paramPrintWriter)
    {
        AppWidgetProviderInfo localAppWidgetProviderInfo = paramProvider.info;
        paramPrintWriter.print("    [");
        paramPrintWriter.print(paramInt);
        paramPrintWriter.print("] provider ");
        paramPrintWriter.print(localAppWidgetProviderInfo.provider.flattenToShortString());
        paramPrintWriter.println(':');
        paramPrintWriter.print("        min=(");
        paramPrintWriter.print(localAppWidgetProviderInfo.minWidth);
        paramPrintWriter.print("x");
        paramPrintWriter.print(localAppWidgetProviderInfo.minHeight);
        paramPrintWriter.print(")     minResize=(");
        paramPrintWriter.print(localAppWidgetProviderInfo.minResizeWidth);
        paramPrintWriter.print("x");
        paramPrintWriter.print(localAppWidgetProviderInfo.minResizeHeight);
        paramPrintWriter.print(") updatePeriodMillis=");
        paramPrintWriter.print(localAppWidgetProviderInfo.updatePeriodMillis);
        paramPrintWriter.print(" resizeMode=");
        paramPrintWriter.print(localAppWidgetProviderInfo.resizeMode);
        paramPrintWriter.print(" autoAdvanceViewId=");
        paramPrintWriter.print(localAppWidgetProviderInfo.autoAdvanceViewId);
        paramPrintWriter.print(" initialLayout=#");
        paramPrintWriter.print(Integer.toHexString(localAppWidgetProviderInfo.initialLayout));
        paramPrintWriter.print(" zombie=");
        paramPrintWriter.println(paramProvider.zombie);
    }

    private void ensureStateLoadedLocked()
    {
        if (!this.mStateLoaded)
        {
            loadAppWidgetList();
            loadStateLocked();
            this.mStateLoaded = true;
        }
    }

    static int[] getAppWidgetIds(Provider paramProvider)
    {
        int i = paramProvider.instances.size();
        int[] arrayOfInt = new int[i];
        for (int j = 0; j < i; j++)
            arrayOfInt[j] = ((AppWidgetId)paramProvider.instances.get(j)).appWidgetId;
        return arrayOfInt;
    }

    static File getSettingsFile(int paramInt)
    {
        return new File("/data/system/users/" + paramInt + "/" + "appwidgets.xml");
    }

    private void incrementAppWidgetServiceRefCount(int paramInt, Intent.FilterComparison paramFilterComparison)
    {
        HashSet localHashSet;
        if (this.mRemoteViewsServicesAppWidgets.containsKey(paramFilterComparison))
            localHashSet = (HashSet)this.mRemoteViewsServicesAppWidgets.get(paramFilterComparison);
        while (true)
        {
            localHashSet.add(Integer.valueOf(paramInt));
            return;
            localHashSet = new HashSet();
            this.mRemoteViewsServicesAppWidgets.put(paramFilterComparison, localHashSet);
        }
    }

    // ERROR //
    private Provider parseProviderInfoXml(ComponentName paramComponentName, ResolveInfo paramResolveInfo)
    {
        // Byte code:
        //     0: aload_2
        //     1: getfield 468	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     4: astore_3
        //     5: aconst_null
        //     6: astore 4
        //     8: aload_3
        //     9: aload_0
        //     10: getfield 98	com/android/server/AppWidgetServiceImpl:mContext	Landroid/content/Context;
        //     13: invokevirtual 471	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
        //     16: ldc_w 473
        //     19: invokevirtual 479	android/content/pm/ActivityInfo:loadXmlMetaData	(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;
        //     22: astore 4
        //     24: aload 4
        //     26: ifnonnull +52 -> 78
        //     29: ldc 33
        //     31: new 155	java/lang/StringBuilder
        //     34: dup
        //     35: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     38: ldc_w 481
        //     41: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     44: aload_1
        //     45: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     48: bipush 39
        //     50: invokevirtual 484	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
        //     53: invokevirtual 183	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     56: invokestatic 490	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     59: pop
        //     60: aconst_null
        //     61: astore 8
        //     63: aload 4
        //     65: ifnull +10 -> 75
        //     68: aload 4
        //     70: invokeinterface 495 1 0
        //     75: aload 8
        //     77: areturn
        //     78: aload 4
        //     80: invokestatic 501	android/util/Xml:asAttributeSet	(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;
        //     83: astore 9
        //     85: aload 4
        //     87: invokeinterface 503 1 0
        //     92: istore 10
        //     94: iload 10
        //     96: iconst_1
        //     97: if_icmpeq +9 -> 106
        //     100: iload 10
        //     102: iconst_2
        //     103: if_icmpne -18 -> 85
        //     106: ldc_w 505
        //     109: aload 4
        //     111: invokeinterface 508 1 0
        //     116: invokevirtual 513	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     119: ifne +52 -> 171
        //     122: ldc 33
        //     124: new 155	java/lang/StringBuilder
        //     127: dup
        //     128: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     131: ldc_w 515
        //     134: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     137: aload_1
        //     138: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     141: bipush 39
        //     143: invokevirtual 484	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
        //     146: invokevirtual 183	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     149: invokestatic 490	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     152: pop
        //     153: aconst_null
        //     154: astore 8
        //     156: aload 4
        //     158: ifnull -83 -> 75
        //     161: aload 4
        //     163: invokeinterface 495 1 0
        //     168: goto -93 -> 75
        //     171: new 19	com/android/server/AppWidgetServiceImpl$Provider
        //     174: dup
        //     175: invokespecial 516	com/android/server/AppWidgetServiceImpl$Provider:<init>	()V
        //     178: astore 8
        //     180: new 173	android/appwidget/AppWidgetProviderInfo
        //     183: dup
        //     184: invokespecial 517	android/appwidget/AppWidgetProviderInfo:<init>	()V
        //     187: astore 11
        //     189: aload 8
        //     191: aload 11
        //     193: putfield 171	com/android/server/AppWidgetServiceImpl$Provider:info	Landroid/appwidget/AppWidgetProviderInfo;
        //     196: aload 11
        //     198: aload_1
        //     199: putfield 176	android/appwidget/AppWidgetProviderInfo:provider	Landroid/content/ComponentName;
        //     202: aload 8
        //     204: aload_3
        //     205: getfield 521	android/content/pm/ActivityInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     208: getfield 524	android/content/pm/ApplicationInfo:uid	I
        //     211: putfield 302	com/android/server/AppWidgetServiceImpl$Provider:uid	I
        //     214: aload_0
        //     215: getfield 98	com/android/server/AppWidgetServiceImpl:mContext	Landroid/content/Context;
        //     218: invokevirtual 471	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
        //     221: aload_3
        //     222: getfield 521	android/content/pm/ActivityInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     225: invokevirtual 530	android/content/pm/PackageManager:getResourcesForApplication	(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;
        //     228: aload 9
        //     230: getstatic 536	com/android/internal/R$styleable:AppWidgetProviderInfo	[I
        //     233: invokevirtual 542	android/content/res/Resources:obtainAttributes	(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
        //     236: astore 14
        //     238: aload 14
        //     240: iconst_0
        //     241: invokevirtual 548	android/content/res/TypedArray:peekValue	(I)Landroid/util/TypedValue;
        //     244: astore 15
        //     246: aload 15
        //     248: ifnull +245 -> 493
        //     251: aload 15
        //     253: getfield 553	android/util/TypedValue:data	I
        //     256: istore 16
        //     258: aload 11
        //     260: iload 16
        //     262: putfield 393	android/appwidget/AppWidgetProviderInfo:minWidth	I
        //     265: aload 14
        //     267: iconst_1
        //     268: invokevirtual 548	android/content/res/TypedArray:peekValue	(I)Landroid/util/TypedValue;
        //     271: astore 17
        //     273: aload 17
        //     275: ifnull +224 -> 499
        //     278: aload 17
        //     280: getfield 553	android/util/TypedValue:data	I
        //     283: istore 18
        //     285: aload 11
        //     287: iload 18
        //     289: putfield 398	android/appwidget/AppWidgetProviderInfo:minHeight	I
        //     292: aload 14
        //     294: bipush 8
        //     296: invokevirtual 548	android/content/res/TypedArray:peekValue	(I)Landroid/util/TypedValue;
        //     299: astore 19
        //     301: aload 19
        //     303: ifnull +202 -> 505
        //     306: aload 19
        //     308: getfield 553	android/util/TypedValue:data	I
        //     311: istore 20
        //     313: aload 11
        //     315: iload 20
        //     317: putfield 403	android/appwidget/AppWidgetProviderInfo:minResizeWidth	I
        //     320: aload 14
        //     322: bipush 9
        //     324: invokevirtual 548	android/content/res/TypedArray:peekValue	(I)Landroid/util/TypedValue;
        //     327: astore 21
        //     329: aload 21
        //     331: ifnull +184 -> 515
        //     334: aload 21
        //     336: getfield 553	android/util/TypedValue:data	I
        //     339: istore 22
        //     341: aload 11
        //     343: iload 22
        //     345: putfield 406	android/appwidget/AppWidgetProviderInfo:minResizeHeight	I
        //     348: aload 11
        //     350: aload 14
        //     352: iconst_2
        //     353: iconst_0
        //     354: invokevirtual 557	android/content/res/TypedArray:getInt	(II)I
        //     357: putfield 411	android/appwidget/AppWidgetProviderInfo:updatePeriodMillis	I
        //     360: aload 11
        //     362: aload 14
        //     364: iconst_3
        //     365: iconst_0
        //     366: invokevirtual 560	android/content/res/TypedArray:getResourceId	(II)I
        //     369: putfield 426	android/appwidget/AppWidgetProviderInfo:initialLayout	I
        //     372: aload 14
        //     374: iconst_4
        //     375: invokevirtual 563	android/content/res/TypedArray:getString	(I)Ljava/lang/String;
        //     378: astore 23
        //     380: aload 23
        //     382: ifnull +21 -> 403
        //     385: aload 11
        //     387: new 347	android/content/ComponentName
        //     390: dup
        //     391: aload_1
        //     392: invokevirtual 566	android/content/ComponentName:getPackageName	()Ljava/lang/String;
        //     395: aload 23
        //     397: invokespecial 569	android/content/ComponentName:<init>	(Ljava/lang/String;Ljava/lang/String;)V
        //     400: putfield 572	android/appwidget/AppWidgetProviderInfo:configure	Landroid/content/ComponentName;
        //     403: aload 11
        //     405: aload_3
        //     406: aload_0
        //     407: getfield 98	com/android/server/AppWidgetServiceImpl:mContext	Landroid/content/Context;
        //     410: invokevirtual 471	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
        //     413: invokevirtual 576	android/content/pm/ActivityInfo:loadLabel	(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
        //     416: invokevirtual 577	java/lang/Object:toString	()Ljava/lang/String;
        //     419: putfield 580	android/appwidget/AppWidgetProviderInfo:label	Ljava/lang/String;
        //     422: aload 11
        //     424: aload_2
        //     425: invokevirtual 583	android/content/pm/ResolveInfo:getIconResource	()I
        //     428: putfield 586	android/appwidget/AppWidgetProviderInfo:icon	I
        //     431: aload 11
        //     433: aload 14
        //     435: iconst_5
        //     436: iconst_0
        //     437: invokevirtual 560	android/content/res/TypedArray:getResourceId	(II)I
        //     440: putfield 589	android/appwidget/AppWidgetProviderInfo:previewImage	I
        //     443: aload 11
        //     445: aload 14
        //     447: bipush 6
        //     449: bipush 255
        //     451: invokevirtual 560	android/content/res/TypedArray:getResourceId	(II)I
        //     454: putfield 421	android/appwidget/AppWidgetProviderInfo:autoAdvanceViewId	I
        //     457: aload 11
        //     459: aload 14
        //     461: bipush 7
        //     463: iconst_0
        //     464: invokevirtual 557	android/content/res/TypedArray:getInt	(II)I
        //     467: putfield 416	android/appwidget/AppWidgetProviderInfo:resizeMode	I
        //     470: aload 14
        //     472: invokevirtual 592	android/content/res/TypedArray:recycle	()V
        //     475: aload 4
        //     477: ifnull +10 -> 487
        //     480: aload 4
        //     482: invokeinterface 495 1 0
        //     487: aload 8
        //     489: pop
        //     490: goto -415 -> 75
        //     493: iconst_0
        //     494: istore 16
        //     496: goto -238 -> 258
        //     499: iconst_0
        //     500: istore 18
        //     502: goto -217 -> 285
        //     505: aload 11
        //     507: getfield 393	android/appwidget/AppWidgetProviderInfo:minWidth	I
        //     510: istore 20
        //     512: goto -199 -> 313
        //     515: aload 11
        //     517: getfield 398	android/appwidget/AppWidgetProviderInfo:minHeight	I
        //     520: istore 22
        //     522: goto -181 -> 341
        //     525: astore 6
        //     527: ldc 33
        //     529: new 155	java/lang/StringBuilder
        //     532: dup
        //     533: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     536: ldc_w 594
        //     539: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     542: aload_1
        //     543: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     546: bipush 39
        //     548: invokevirtual 484	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
        //     551: invokevirtual 183	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     554: aload 6
        //     556: invokestatic 597	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     559: pop
        //     560: aconst_null
        //     561: astore 8
        //     563: aload 4
        //     565: ifnull -490 -> 75
        //     568: aload 4
        //     570: invokeinterface 495 1 0
        //     575: goto -500 -> 75
        //     578: astore 5
        //     580: aload 4
        //     582: ifnull +10 -> 592
        //     585: aload 4
        //     587: invokeinterface 495 1 0
        //     592: aload 5
        //     594: athrow
        //     595: astore 5
        //     597: aload 8
        //     599: pop
        //     600: goto -20 -> 580
        //     603: astore 6
        //     605: aload 8
        //     607: pop
        //     608: goto -81 -> 527
        //
        // Exception table:
        //     from	to	target	type
        //     8	60	525	java/lang/Exception
        //     78	153	525	java/lang/Exception
        //     171	180	525	java/lang/Exception
        //     8	60	578	finally
        //     78	153	578	finally
        //     171	180	578	finally
        //     527	560	578	finally
        //     180	475	595	finally
        //     505	522	595	finally
        //     180	475	603	java/lang/Exception
        //     505	522	603	java/lang/Exception
    }

    private void unbindAppWidgetRemoteViewsServicesLocked(AppWidgetId paramAppWidgetId)
    {
        int i = paramAppWidgetId.appWidgetId;
        Iterator localIterator = this.mBoundRemoteViewsServices.keySet().iterator();
        while (localIterator.hasNext())
        {
            Pair localPair = (Pair)localIterator.next();
            if (((Integer)localPair.first).intValue() == i)
            {
                ServiceConnectionProxy localServiceConnectionProxy = (ServiceConnectionProxy)this.mBoundRemoteViewsServices.get(localPair);
                localServiceConnectionProxy.disconnect();
                this.mContext.unbindService(localServiceConnectionProxy);
                localIterator.remove();
            }
        }
        decrementAppWidgetServiceRefCount(paramAppWidgetId);
    }

    boolean addProviderLocked(ResolveInfo paramResolveInfo)
    {
        boolean bool = false;
        if ((0x40000 & paramResolveInfo.activityInfo.applicationInfo.flags) != 0);
        while (true)
        {
            return bool;
            if (paramResolveInfo.activityInfo.isEnabled())
            {
                Provider localProvider = parseProviderInfoXml(new ComponentName(paramResolveInfo.activityInfo.packageName, paramResolveInfo.activityInfo.name), paramResolveInfo);
                if (localProvider != null)
                {
                    this.mInstalledProviders.add(localProvider);
                    bool = true;
                }
            }
        }
    }

    void addProvidersForPackageLocked(String paramString)
    {
        Intent localIntent = new Intent("android.appwidget.action.APPWIDGET_UPDATE");
        localIntent.setPackage(paramString);
        while (true)
        {
            List localList;
            ResolveInfo localResolveInfo;
            ActivityInfo localActivityInfo;
            try
            {
                localList = this.mPm.queryIntentReceivers(localIntent, localIntent.resolveTypeIfNeeded(this.mContext.getContentResolver()), 128, this.mUserId);
                if (localList != null)
                    break label110;
                i = 0;
                int j = 0;
                if (j < i)
                {
                    localResolveInfo = (ResolveInfo)localList.get(j);
                    localActivityInfo = localResolveInfo.activityInfo;
                    if ((0x40000 & localActivityInfo.applicationInfo.flags) == 0)
                        break label122;
                    j++;
                    continue;
                }
            }
            catch (RemoteException localRemoteException)
            {
            }
            return;
            label110: int i = localList.size();
            continue;
            label122: if (paramString.equals(localActivityInfo.packageName))
                addProviderLocked(localResolveInfo);
        }
    }

    public int allocateAppWidgetId(String paramString, int paramInt)
    {
        int i = enforceCallingUid(paramString);
        synchronized (this.mAppWidgetIds)
        {
            ensureStateLoadedLocked();
            int j = this.mNextAppWidgetId;
            this.mNextAppWidgetId = (j + 1);
            Host localHost = lookupOrAddHostLocked(i, paramString, paramInt);
            AppWidgetId localAppWidgetId = new AppWidgetId();
            localAppWidgetId.appWidgetId = j;
            localAppWidgetId.host = localHost;
            localHost.instances.add(localAppWidgetId);
            this.mAppWidgetIds.add(localAppWidgetId);
            saveStateLocked();
            return j;
        }
    }

    public void bindAppWidgetId(int paramInt, ComponentName paramComponentName)
    {
        this.mContext.enforceCallingPermission("android.permission.BIND_APPWIDGET", "bindAppWidgetId appWidgetId=" + paramInt + " provider=" + paramComponentName);
        bindAppWidgetIdImpl(paramInt, paramComponentName);
    }

    public boolean bindAppWidgetIdIfAllowed(String paramString, int paramInt, ComponentName paramComponentName)
    {
        try
        {
            this.mContext.enforceCallingPermission("android.permission.BIND_APPWIDGET", null);
            bindAppWidgetIdImpl(paramInt, paramComponentName);
            bool = true;
            return bool;
        }
        catch (SecurityException localSecurityException)
        {
            while (true)
            {
                boolean bool;
                if (!callerHasBindAppWidgetPermission(paramString))
                    bool = false;
            }
        }
    }

    // ERROR //
    public void bindRemoteViewsService(int paramInt, Intent paramIntent, IBinder paramIBinder)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 85	com/android/server/AppWidgetServiceImpl:mAppWidgetIds	Ljava/util/ArrayList;
        //     4: astore 4
        //     6: aload 4
        //     8: monitorenter
        //     9: aload_0
        //     10: invokespecial 134	com/android/server/AppWidgetServiceImpl:ensureStateLoadedLocked	()V
        //     13: aload_0
        //     14: iload_1
        //     15: invokevirtual 138	com/android/server/AppWidgetServiceImpl:lookupAppWidgetIdLocked	(I)Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
        //     18: astore 6
        //     20: aload 6
        //     22: ifnonnull +21 -> 43
        //     25: new 140	java/lang/IllegalArgumentException
        //     28: dup
        //     29: ldc 142
        //     31: invokespecial 145	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     34: athrow
        //     35: astore 5
        //     37: aload 4
        //     39: monitorexit
        //     40: aload 5
        //     42: athrow
        //     43: aload_2
        //     44: invokevirtual 701	android/content/Intent:getComponent	()Landroid/content/ComponentName;
        //     47: astore 7
        //     49: ldc_w 703
        //     52: aload_0
        //     53: getfield 98	com/android/server/AppWidgetServiceImpl:mContext	Landroid/content/Context;
        //     56: invokevirtual 471	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
        //     59: aload 7
        //     61: sipush 4096
        //     64: invokevirtual 707	android/content/pm/PackageManager:getServiceInfo	(Landroid/content/ComponentName;I)Landroid/content/pm/ServiceInfo;
        //     67: getfield 712	android/content/pm/ServiceInfo:permission	Ljava/lang/String;
        //     70: invokevirtual 513	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     73: ifne +63 -> 136
        //     76: new 691	java/lang/SecurityException
        //     79: dup
        //     80: new 155	java/lang/StringBuilder
        //     83: dup
        //     84: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     87: ldc_w 714
        //     90: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     93: aload 7
        //     95: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     98: invokevirtual 183	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     101: invokespecial 715	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     104: athrow
        //     105: astore 8
        //     107: new 140	java/lang/IllegalArgumentException
        //     110: dup
        //     111: new 155	java/lang/StringBuilder
        //     114: dup
        //     115: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     118: ldc_w 717
        //     121: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     124: aload 7
        //     126: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     129: invokevirtual 183	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     132: invokespecial 145	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     135: athrow
        //     136: aconst_null
        //     137: astore 9
        //     139: new 267	android/content/Intent$FilterComparison
        //     142: dup
        //     143: aload_2
        //     144: invokespecial 720	android/content/Intent$FilterComparison:<init>	(Landroid/content/Intent;)V
        //     147: astore 10
        //     149: iload_1
        //     150: invokestatic 280	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     153: aload 10
        //     155: invokestatic 724	android/util/Pair:create	(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
        //     158: astore 11
        //     160: aload_0
        //     161: getfield 74	com/android/server/AppWidgetServiceImpl:mBoundRemoteViewsServices	Ljava/util/HashMap;
        //     164: aload 11
        //     166: invokevirtual 455	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
        //     169: ifeq +41 -> 210
        //     172: aload_0
        //     173: getfield 74	com/android/server/AppWidgetServiceImpl:mBoundRemoteViewsServices	Ljava/util/HashMap;
        //     176: aload 11
        //     178: invokevirtual 271	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     181: checkcast 10	com/android/server/AppWidgetServiceImpl$ServiceConnectionProxy
        //     184: astore 9
        //     186: aload 9
        //     188: invokevirtual 610	com/android/server/AppWidgetServiceImpl$ServiceConnectionProxy:disconnect	()V
        //     191: aload_0
        //     192: getfield 98	com/android/server/AppWidgetServiceImpl:mContext	Landroid/content/Context;
        //     195: aload 9
        //     197: invokevirtual 614	android/content/Context:unbindService	(Landroid/content/ServiceConnection;)V
        //     200: aload_0
        //     201: getfield 74	com/android/server/AppWidgetServiceImpl:mBoundRemoteViewsServices	Ljava/util/HashMap;
        //     204: aload 11
        //     206: invokevirtual 726	java/util/HashMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
        //     209: pop
        //     210: aload 9
        //     212: pop
        //     213: aload 6
        //     215: getfield 153	com/android/server/AppWidgetServiceImpl$AppWidgetId:provider	Lcom/android/server/AppWidgetServiceImpl$Provider;
        //     218: getfield 302	com/android/server/AppWidgetServiceImpl$Provider:uid	I
        //     221: invokestatic 306	android/os/UserId:getUserId	(I)I
        //     224: istore 13
        //     226: invokestatic 131	android/os/Binder:clearCallingIdentity	()J
        //     229: lstore 14
        //     231: new 10	com/android/server/AppWidgetServiceImpl$ServiceConnectionProxy
        //     234: dup
        //     235: aload 11
        //     237: aload_3
        //     238: invokespecial 729	com/android/server/AppWidgetServiceImpl$ServiceConnectionProxy:<init>	(Landroid/util/Pair;Landroid/os/IBinder;)V
        //     241: astore 16
        //     243: aload_0
        //     244: getfield 98	com/android/server/AppWidgetServiceImpl:mContext	Landroid/content/Context;
        //     247: aload_2
        //     248: aload 16
        //     250: iconst_1
        //     251: iload 13
        //     253: invokevirtual 310	android/content/Context:bindService	(Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z
        //     256: pop
        //     257: aload_0
        //     258: getfield 74	com/android/server/AppWidgetServiceImpl:mBoundRemoteViewsServices	Ljava/util/HashMap;
        //     261: aload 11
        //     263: aload 16
        //     265: invokevirtual 460	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     268: pop
        //     269: lload 14
        //     271: invokestatic 149	android/os/Binder:restoreCallingIdentity	(J)V
        //     274: aload_0
        //     275: iload_1
        //     276: aload 10
        //     278: invokespecial 731	com/android/server/AppWidgetServiceImpl:incrementAppWidgetServiceRefCount	(ILandroid/content/Intent$FilterComparison;)V
        //     281: aload 4
        //     283: monitorexit
        //     284: return
        //     285: lload 14
        //     287: invokestatic 149	android/os/Binder:restoreCallingIdentity	(J)V
        //     290: aload 17
        //     292: athrow
        //     293: astore 17
        //     295: goto -10 -> 285
        //     298: astore 17
        //     300: goto -15 -> 285
        //
        // Exception table:
        //     from	to	target	type
        //     9	40	35	finally
        //     43	49	35	finally
        //     49	105	35	finally
        //     107	231	35	finally
        //     269	293	35	finally
        //     49	105	105	android/content/pm/PackageManager$NameNotFoundException
        //     243	269	293	finally
        //     231	243	298	finally
    }

    boolean canAccessAppWidgetId(AppWidgetId paramAppWidgetId, int paramInt)
    {
        boolean bool = true;
        if (paramAppWidgetId.host.uid == paramInt);
        while (true)
        {
            return bool;
            if (((paramAppWidgetId.provider == null) || (paramAppWidgetId.provider.uid != paramInt)) && (this.mContext.checkCallingOrSelfPermission("android.permission.BIND_APPWIDGET") != 0))
                bool = false;
        }
    }

    void cancelBroadcasts(Provider paramProvider)
    {
        long l;
        if (paramProvider.broadcast != null)
        {
            this.mAlarmManager.cancel(paramProvider.broadcast);
            l = Binder.clearCallingIdentity();
        }
        try
        {
            paramProvider.broadcast.cancel();
            Binder.restoreCallingIdentity(l);
            paramProvider.broadcast = null;
            return;
        }
        finally
        {
            Binder.restoreCallingIdentity(l);
        }
    }

    void computeMaximumWidgetBitmapMemory()
    {
        WindowManager localWindowManager = (WindowManager)this.mContext.getSystemService("window");
        this.mMaxWidgetBitmapMemory = (localWindowManager.getDefaultDisplay().getRawHeight() * (6 * localWindowManager.getDefaultDisplay().getRawWidth()));
    }

    public void deleteAllHosts()
    {
        while (true)
        {
            int m;
            synchronized (this.mAppWidgetIds)
            {
                ensureStateLoadedLocked();
                int i = Binder.getCallingUid();
                int j = this.mHosts.size();
                int k = 0;
                m = j - 1;
                if (m >= 0)
                {
                    Host localHost = (Host)this.mHosts.get(m);
                    if (localHost.uid == i)
                    {
                        deleteHostLocked(localHost);
                        k = 1;
                    }
                }
                else
                {
                    if (k != 0)
                        saveStateLocked();
                    return;
                }
            }
            m--;
        }
    }

    public void deleteAppWidgetId(int paramInt)
    {
        synchronized (this.mAppWidgetIds)
        {
            ensureStateLoadedLocked();
            AppWidgetId localAppWidgetId = lookupAppWidgetIdLocked(paramInt);
            if (localAppWidgetId != null)
            {
                deleteAppWidgetLocked(localAppWidgetId);
                saveStateLocked();
            }
            return;
        }
    }

    void deleteAppWidgetLocked(AppWidgetId paramAppWidgetId)
    {
        unbindAppWidgetRemoteViewsServicesLocked(paramAppWidgetId);
        Host localHost = paramAppWidgetId.host;
        localHost.instances.remove(paramAppWidgetId);
        pruneHostLocked(localHost);
        this.mAppWidgetIds.remove(paramAppWidgetId);
        Provider localProvider = paramAppWidgetId.provider;
        if (localProvider != null)
        {
            localProvider.instances.remove(paramAppWidgetId);
            if (!localProvider.zombie)
            {
                Intent localIntent1 = new Intent("android.appwidget.action.APPWIDGET_DELETED");
                localIntent1.setComponent(localProvider.info.provider);
                localIntent1.putExtra("appWidgetId", paramAppWidgetId.appWidgetId);
                this.mContext.sendBroadcast(localIntent1, this.mUserId);
                if (localProvider.instances.size() == 0)
                {
                    cancelBroadcasts(localProvider);
                    Intent localIntent2 = new Intent("android.appwidget.action.APPWIDGET_DISABLED");
                    localIntent2.setComponent(localProvider.info.provider);
                    this.mContext.sendBroadcast(localIntent2, this.mUserId);
                }
            }
        }
    }

    public void deleteHost(int paramInt)
    {
        synchronized (this.mAppWidgetIds)
        {
            ensureStateLoadedLocked();
            Host localHost = lookupHostLocked(Binder.getCallingUid(), paramInt);
            if (localHost != null)
            {
                deleteHostLocked(localHost);
                saveStateLocked();
            }
            return;
        }
    }

    void deleteHostLocked(Host paramHost)
    {
        for (int i = -1 + paramHost.instances.size(); i >= 0; i--)
            deleteAppWidgetLocked((AppWidgetId)paramHost.instances.get(i));
        paramHost.instances.clear();
        this.mHosts.remove(paramHost);
        this.mDeletedHosts.add(paramHost);
        paramHost.callbacks = null;
    }

    void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.DUMP") != 0)
            paramPrintWriter.println("Permission Denial: can't dump from from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid());
        while (true)
        {
            return;
            synchronized (this.mAppWidgetIds)
            {
                int i = this.mInstalledProviders.size();
                paramPrintWriter.println("Providers:");
                for (int j = 0; j < i; j++)
                    dumpProvider((Provider)this.mInstalledProviders.get(j), j, paramPrintWriter);
                int k = this.mAppWidgetIds.size();
                paramPrintWriter.println(" ");
                paramPrintWriter.println("AppWidgetIds:");
                for (int m = 0; m < k; m++)
                    dumpAppWidgetId((AppWidgetId)this.mAppWidgetIds.get(m), m, paramPrintWriter);
                int n = this.mHosts.size();
                paramPrintWriter.println(" ");
                paramPrintWriter.println("Hosts:");
                for (int i1 = 0; i1 < n; i1++)
                    dumpHost((Host)this.mHosts.get(i1), i1, paramPrintWriter);
                int i2 = this.mDeletedProviders.size();
                paramPrintWriter.println(" ");
                paramPrintWriter.println("Deleted Providers:");
                for (int i3 = 0; i3 < i2; i3++)
                    dumpProvider((Provider)this.mDeletedProviders.get(i3), i3, paramPrintWriter);
                int i4 = this.mDeletedHosts.size();
                paramPrintWriter.println(" ");
                paramPrintWriter.println("Deleted Hosts:");
                for (int i5 = 0; i5 < i4; i5++)
                    dumpHost((Host)this.mDeletedHosts.get(i5), i5, paramPrintWriter);
            }
        }
    }

    int enforceCallingUid(String paramString)
        throws IllegalArgumentException
    {
        int i = Binder.getCallingUid();
        try
        {
            int j = getUidForPackage(paramString);
            if (!UserId.isSameApp(i, j))
                throw new IllegalArgumentException("packageName and uid don't match packageName=" + paramString);
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            throw new IllegalArgumentException("packageName and uid don't match packageName=" + paramString);
        }
        return i;
    }

    public int[] getAppWidgetIds(ComponentName paramComponentName)
    {
        int[] arrayOfInt;
        synchronized (this.mAppWidgetIds)
        {
            ensureStateLoadedLocked();
            Provider localProvider = lookupProviderLocked(paramComponentName);
            if ((localProvider != null) && (Binder.getCallingUid() == localProvider.uid))
                arrayOfInt = getAppWidgetIds(localProvider);
            else
                arrayOfInt = new int[0];
        }
        return arrayOfInt;
    }

    public AppWidgetProviderInfo getAppWidgetInfo(int paramInt)
    {
        AppWidgetProviderInfo localAppWidgetProviderInfo;
        synchronized (this.mAppWidgetIds)
        {
            ensureStateLoadedLocked();
            AppWidgetId localAppWidgetId = lookupAppWidgetIdLocked(paramInt);
            if ((localAppWidgetId != null) && (localAppWidgetId.provider != null) && (!localAppWidgetId.provider.zombie))
                localAppWidgetProviderInfo = localAppWidgetId.provider.info;
            else
                localAppWidgetProviderInfo = null;
        }
        return localAppWidgetProviderInfo;
    }

    public Bundle getAppWidgetOptions(int paramInt)
    {
        Bundle localBundle;
        synchronized (this.mAppWidgetIds)
        {
            ensureStateLoadedLocked();
            AppWidgetId localAppWidgetId = lookupAppWidgetIdLocked(paramInt);
            if ((localAppWidgetId != null) && (localAppWidgetId.options != null))
                localBundle = localAppWidgetId.options;
            else
                localBundle = Bundle.EMPTY;
        }
        return localBundle;
    }

    public RemoteViews getAppWidgetViews(int paramInt)
    {
        RemoteViews localRemoteViews;
        synchronized (this.mAppWidgetIds)
        {
            ensureStateLoadedLocked();
            AppWidgetId localAppWidgetId = lookupAppWidgetIdLocked(paramInt);
            if (localAppWidgetId != null)
                localRemoteViews = localAppWidgetId.views;
            else
                localRemoteViews = null;
        }
        return localRemoteViews;
    }

    public List<AppWidgetProviderInfo> getInstalledProviders()
    {
        while (true)
        {
            int j;
            synchronized (this.mAppWidgetIds)
            {
                ensureStateLoadedLocked();
                int i = this.mInstalledProviders.size();
                ArrayList localArrayList2 = new ArrayList(i);
                j = 0;
                if (j < i)
                {
                    Provider localProvider = (Provider)this.mInstalledProviders.get(j);
                    if (!localProvider.zombie)
                        localArrayList2.add(localProvider.info);
                }
                else
                {
                    return localArrayList2;
                }
            }
            j++;
        }
    }

    int getUidForPackage(String paramString)
        throws PackageManager.NameNotFoundException
    {
        Object localObject = null;
        try
        {
            PackageInfo localPackageInfo = this.mPm.getPackageInfo(paramString, 0, this.mUserId);
            localObject = localPackageInfo;
            label22: if ((localObject == null) || (localObject.applicationInfo == null))
                throw new PackageManager.NameNotFoundException();
            return localObject.applicationInfo.uid;
        }
        catch (RemoteException localRemoteException)
        {
            break label22;
        }
    }

    public boolean hasBindAppWidgetPermission(String paramString)
    {
        this.mContext.enforceCallingPermission("android.permission.MODIFY_APPWIDGET_BIND_PERMISSIONS", "hasBindAppWidgetPermission packageName=" + paramString);
        synchronized (this.mAppWidgetIds)
        {
            ensureStateLoadedLocked();
            boolean bool = this.mPackagesWithBindWidgetPermission.contains(paramString);
            return bool;
        }
    }

    void loadAppWidgetList()
    {
        Intent localIntent = new Intent("android.appwidget.action.APPWIDGET_UPDATE");
        while (true)
        {
            try
            {
                List localList = this.mPm.queryIntentReceivers(localIntent, localIntent.resolveTypeIfNeeded(this.mContext.getContentResolver()), 128, this.mUserId);
                int j;
                int i;
                if (localList == null)
                {
                    j = 0;
                    break label96;
                    if (k < j)
                    {
                        addProviderLocked((ResolveInfo)localList.get(k));
                        k++;
                        continue;
                    }
                }
                else
                {
                    i = localList.size();
                    j = i;
                }
            }
            catch (RemoteException localRemoteException)
            {
            }
            return;
            label96: int k = 0;
        }
    }

    void loadStateLocked()
    {
        AtomicFile localAtomicFile = savedStateFile();
        try
        {
            FileInputStream localFileInputStream = localAtomicFile.openRead();
            readStateFromFileLocked(localFileInputStream);
            if (localFileInputStream != null);
            try
            {
                localFileInputStream.close();
                return;
            }
            catch (IOException localIOException)
            {
                while (true)
                    Slog.w("AppWidgetServiceImpl", "Failed to close state FileInputStream " + localIOException);
            }
        }
        catch (FileNotFoundException localFileNotFoundException)
        {
            while (true)
                Slog.w("AppWidgetServiceImpl", "Failed to read state: " + localFileNotFoundException);
        }
    }

    AppWidgetId lookupAppWidgetIdLocked(int paramInt)
    {
        int i = Binder.getCallingUid();
        int j = this.mAppWidgetIds.size();
        int k = 0;
        AppWidgetId localAppWidgetId;
        if (k < j)
        {
            localAppWidgetId = (AppWidgetId)this.mAppWidgetIds.get(k);
            if ((localAppWidgetId.appWidgetId != paramInt) || (!canAccessAppWidgetId(localAppWidgetId, i)));
        }
        while (true)
        {
            return localAppWidgetId;
            k++;
            break;
            localAppWidgetId = null;
        }
    }

    Host lookupHostLocked(int paramInt1, int paramInt2)
    {
        int i = this.mHosts.size();
        int j = 0;
        Host localHost;
        if (j < i)
        {
            localHost = (Host)this.mHosts.get(j);
            if ((localHost.uid != paramInt1) || (localHost.hostId != paramInt2));
        }
        while (true)
        {
            return localHost;
            j++;
            break;
            localHost = null;
        }
    }

    Host lookupOrAddHostLocked(int paramInt1, String paramString, int paramInt2)
    {
        int i = this.mHosts.size();
        int j = 0;
        Object localObject;
        if (j < i)
        {
            localObject = (Host)this.mHosts.get(j);
            if ((((Host)localObject).hostId != paramInt2) || (!((Host)localObject).packageName.equals(paramString)));
        }
        while (true)
        {
            return localObject;
            j++;
            break;
            Host localHost = new Host();
            localHost.packageName = paramString;
            localHost.uid = paramInt1;
            localHost.hostId = paramInt2;
            this.mHosts.add(localHost);
            localObject = localHost;
        }
    }

    Provider lookupProviderLocked(ComponentName paramComponentName)
    {
        int i = this.mInstalledProviders.size();
        int j = 0;
        Provider localProvider;
        if (j < i)
        {
            localProvider = (Provider)this.mInstalledProviders.get(j);
            if (!localProvider.info.provider.equals(paramComponentName));
        }
        while (true)
        {
            return localProvider;
            j++;
            break;
            localProvider = null;
        }
    }

    public void notifyAppWidgetViewDataChanged(int[] paramArrayOfInt, int paramInt)
    {
        if (paramArrayOfInt == null);
        while (true)
        {
            return;
            if (paramArrayOfInt.length == 0)
                continue;
            int i = paramArrayOfInt.length;
            synchronized (this.mAppWidgetIds)
            {
                ensureStateLoadedLocked();
                for (int j = 0; j < i; j++)
                    notifyAppWidgetViewDataChangedInstanceLocked(lookupAppWidgetIdLocked(paramArrayOfInt[j]), paramInt);
            }
        }
    }

    void notifyAppWidgetViewDataChangedInstanceLocked(AppWidgetId paramAppWidgetId, int paramInt)
    {
        if ((paramAppWidgetId != null) && (paramAppWidgetId.provider != null) && (!paramAppWidgetId.provider.zombie) && (!paramAppWidgetId.host.zombie))
        {
            if (paramAppWidgetId.host.callbacks != null);
            try
            {
                paramAppWidgetId.host.callbacks.viewDataChanged(paramAppWidgetId.appWidgetId, paramInt);
                if (paramAppWidgetId.host.callbacks == null)
                {
                    Iterator localIterator = this.mRemoteViewsServicesAppWidgets.keySet().iterator();
                    while (localIterator.hasNext())
                    {
                        Intent.FilterComparison localFilterComparison = (Intent.FilterComparison)localIterator.next();
                        if (((HashSet)this.mRemoteViewsServicesAppWidgets.get(localFilterComparison)).contains(Integer.valueOf(paramAppWidgetId.appWidgetId)))
                        {
                            localIntent = localFilterComparison.getIntent();
                            local2 = new ServiceConnection()
                            {
                                public void onServiceConnected(ComponentName paramAnonymousComponentName, IBinder paramAnonymousIBinder)
                                {
                                    IRemoteViewsFactory localIRemoteViewsFactory = IRemoteViewsFactory.Stub.asInterface(paramAnonymousIBinder);
                                    try
                                    {
                                        localIRemoteViewsFactory.onDataSetChangedAsync();
                                        AppWidgetServiceImpl.this.mContext.unbindService(this);
                                        return;
                                    }
                                    catch (RemoteException localRemoteException)
                                    {
                                        while (true)
                                            localRemoteException.printStackTrace();
                                    }
                                    catch (RuntimeException localRuntimeException)
                                    {
                                        while (true)
                                            localRuntimeException.printStackTrace();
                                    }
                                }

                                public void onServiceDisconnected(ComponentName paramAnonymousComponentName)
                                {
                                }
                            };
                            i = UserId.getUserId(paramAppWidgetId.provider.uid);
                            l = Binder.clearCallingIdentity();
                        }
                    }
                }
            }
            catch (RemoteException localRemoteException)
            {
                try
                {
                    while (true)
                    {
                        Intent localIntent;
                        ServiceConnection local2;
                        int i;
                        this.mContext.bindService(localIntent, local2, 1, i);
                        Binder.restoreCallingIdentity(l);
                    }
                    localRemoteException = localRemoteException;
                    paramAppWidgetId.host.callbacks = null;
                }
                finally
                {
                    long l;
                    Binder.restoreCallingIdentity(l);
                }
            }
        }
    }

    void onBroadcastReceived(Intent paramIntent)
    {
        String str1 = paramIntent.getAction();
        boolean bool1 = false;
        String[] arrayOfString1;
        boolean bool2;
        if ("android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE".equals(str1))
        {
            arrayOfString1 = paramIntent.getStringArrayExtra("android.intent.extra.changed_package_list");
            bool2 = true;
            if ((arrayOfString1 != null) && (arrayOfString1.length != 0))
                break label121;
        }
        while (true)
        {
            return;
            if ("android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE".equals(str1))
            {
                arrayOfString1 = paramIntent.getStringArrayExtra("android.intent.extra.changed_package_list");
                bool2 = false;
                break;
            }
            Uri localUri = paramIntent.getData();
            if (localUri == null)
                continue;
            String str2 = localUri.getSchemeSpecificPart();
            if (str2 == null)
                continue;
            arrayOfString1 = new String[1];
            arrayOfString1[0] = str2;
            bool2 = "android.intent.action.PACKAGE_ADDED".equals(str1);
            bool1 = "android.intent.action.PACKAGE_CHANGED".equals(str1);
            break;
            label121: if ((bool2) || (bool1))
                synchronized (this.mAppWidgetIds)
                {
                    ensureStateLoadedLocked();
                    Bundle localBundle1 = paramIntent.getExtras();
                    String[] arrayOfString2;
                    int i;
                    int j;
                    if ((bool1) || ((localBundle1 != null) && (localBundle1.getBoolean("android.intent.extra.REPLACING", false))))
                    {
                        arrayOfString2 = arrayOfString1;
                        i = arrayOfString2.length;
                        j = 0;
                    }
                    while (j < i)
                    {
                        updateProvidersForPackageLocked(arrayOfString2[j]);
                        j++;
                        continue;
                        String[] arrayOfString3 = arrayOfString1;
                        int k = arrayOfString3.length;
                        for (int m = 0; m < k; m++)
                            addProvidersForPackageLocked(arrayOfString3[m]);
                    }
                    saveStateLocked();
                }
            Bundle localBundle2 = paramIntent.getExtras();
            if ((localBundle2 != null) && (localBundle2.getBoolean("android.intent.extra.REPLACING", false)))
                continue;
            synchronized (this.mAppWidgetIds)
            {
                ensureStateLoadedLocked();
                String[] arrayOfString4 = arrayOfString1;
                int n = arrayOfString4.length;
                for (int i1 = 0; i1 < n; i1++)
                {
                    removeProvidersForPackageLocked(arrayOfString4[i1]);
                    saveStateLocked();
                }
            }
        }
    }

    void onConfigurationChanged()
    {
        Locale localLocale = Locale.getDefault();
        if ((localLocale == null) || (this.mLocale == null) || (!localLocale.equals(this.mLocale)))
        {
            this.mLocale = localLocale;
            synchronized (this.mAppWidgetIds)
            {
                ensureStateLoadedLocked();
                for (int i = -1 + this.mInstalledProviders.size(); i >= 0; i--)
                    updateProvidersForPackageLocked(((Provider)this.mInstalledProviders.get(i)).info.provider.getPackageName());
                saveStateLocked();
            }
        }
    }

    void onUserRemoved()
    {
        for (int i = -1 + this.mInstalledProviders.size(); i >= 0; i--)
            cancelBroadcasts((Provider)this.mInstalledProviders.get(i));
        getSettingsFile(this.mUserId).delete();
    }

    public void partiallyUpdateAppWidgetIds(int[] paramArrayOfInt, RemoteViews paramRemoteViews)
    {
        if (paramArrayOfInt == null);
        while (true)
        {
            return;
            if (paramArrayOfInt.length == 0)
                continue;
            int i = paramArrayOfInt.length;
            synchronized (this.mAppWidgetIds)
            {
                ensureStateLoadedLocked();
                for (int j = 0; j < i; j++)
                    updateAppWidgetInstanceLocked(lookupAppWidgetIdLocked(paramArrayOfInt[j]), paramRemoteViews, true);
            }
        }
    }

    void pruneHostLocked(Host paramHost)
    {
        if ((paramHost.instances.size() == 0) && (paramHost.callbacks == null))
            this.mHosts.remove(paramHost);
    }

    // ERROR //
    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    void readStateFromFileLocked(FileInputStream paramFileInputStream)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore_2
        //     2: invokestatic 1000	android/util/Xml:newPullParser	()Lorg/xmlpull/v1/XmlPullParser;
        //     5: astore 17
        //     7: aload 17
        //     9: aload_1
        //     10: aconst_null
        //     11: invokeinterface 1006 3 0
        //     16: iconst_0
        //     17: istore 18
        //     19: new 71	java/util/HashMap
        //     22: dup
        //     23: invokespecial 72	java/util/HashMap:<init>	()V
        //     26: astore 19
        //     28: aload 17
        //     30: invokeinterface 1007 1 0
        //     35: istore 20
        //     37: iload 20
        //     39: iconst_2
        //     40: if_icmpne +198 -> 238
        //     43: aload 17
        //     45: invokeinterface 1008 1 0
        //     50: astore 21
        //     52: ldc_w 1010
        //     55: aload 21
        //     57: invokevirtual 513	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     60: ifeq +257 -> 317
        //     63: aload 17
        //     65: aconst_null
        //     66: ldc_w 1012
        //     69: invokeinterface 1016 3 0
        //     74: astore 33
        //     76: aload 17
        //     78: aconst_null
        //     79: ldc_w 1018
        //     82: invokeinterface 1016 3 0
        //     87: astore 34
        //     89: aload_0
        //     90: getfield 98	com/android/server/AppWidgetServiceImpl:mContext	Landroid/content/Context;
        //     93: invokevirtual 471	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
        //     96: astore 35
        //     98: new 347	android/content/ComponentName
        //     101: dup
        //     102: aload 33
        //     104: aload 34
        //     106: invokespecial 569	android/content/ComponentName:<init>	(Ljava/lang/String;Ljava/lang/String;)V
        //     109: astore 36
        //     111: aload 35
        //     113: aload 36
        //     115: iconst_0
        //     116: invokevirtual 1022	android/content/pm/PackageManager:getReceiverInfo	(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
        //     119: pop
        //     120: new 347	android/content/ComponentName
        //     123: dup
        //     124: aload 33
        //     126: aload 34
        //     128: invokespecial 569	android/content/ComponentName:<init>	(Ljava/lang/String;Ljava/lang/String;)V
        //     131: astore 39
        //     133: aload_0
        //     134: aload 39
        //     136: invokevirtual 187	com/android/server/AppWidgetServiceImpl:lookupProviderLocked	(Landroid/content/ComponentName;)Lcom/android/server/AppWidgetServiceImpl$Provider;
        //     139: astore 40
        //     141: aload 40
        //     143: ifnonnull +74 -> 217
        //     146: aload_0
        //     147: getfield 1024	com/android/server/AppWidgetServiceImpl:mSafeMode	Z
        //     150: ifeq +67 -> 217
        //     153: new 19	com/android/server/AppWidgetServiceImpl$Provider
        //     156: dup
        //     157: invokespecial 516	com/android/server/AppWidgetServiceImpl$Provider:<init>	()V
        //     160: astore 40
        //     162: aload 40
        //     164: new 173	android/appwidget/AppWidgetProviderInfo
        //     167: dup
        //     168: invokespecial 517	android/appwidget/AppWidgetProviderInfo:<init>	()V
        //     171: putfield 171	com/android/server/AppWidgetServiceImpl$Provider:info	Landroid/appwidget/AppWidgetProviderInfo;
        //     174: aload 40
        //     176: getfield 171	com/android/server/AppWidgetServiceImpl$Provider:info	Landroid/appwidget/AppWidgetProviderInfo;
        //     179: astore 42
        //     181: new 347	android/content/ComponentName
        //     184: dup
        //     185: aload 33
        //     187: aload 34
        //     189: invokespecial 569	android/content/ComponentName:<init>	(Ljava/lang/String;Ljava/lang/String;)V
        //     192: astore 43
        //     194: aload 42
        //     196: aload 43
        //     198: putfield 176	android/appwidget/AppWidgetProviderInfo:provider	Landroid/content/ComponentName;
        //     201: aload 40
        //     203: iconst_1
        //     204: putfield 192	com/android/server/AppWidgetServiceImpl$Provider:zombie	Z
        //     207: aload_0
        //     208: getfield 81	com/android/server/AppWidgetServiceImpl:mInstalledProviders	Ljava/util/ArrayList;
        //     211: aload 40
        //     213: invokevirtual 201	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     216: pop
        //     217: aload 40
        //     219: ifnull +16 -> 235
        //     222: aload 19
        //     224: iload 18
        //     226: invokestatic 280	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     229: aload 40
        //     231: invokevirtual 460	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     234: pop
        //     235: iinc 18 1
        //     238: iload 20
        //     240: iconst_1
        //     241: if_icmpne -213 -> 28
        //     244: iconst_1
        //     245: istore_2
        //     246: iload_2
        //     247: ifeq +597 -> 844
        //     250: bipush 255
        //     252: aload_0
        //     253: getfield 87	com/android/server/AppWidgetServiceImpl:mHosts	Ljava/util/ArrayList;
        //     256: invokevirtual 205	java/util/ArrayList:size	()I
        //     259: iadd
        //     260: istore 8
        //     262: iload 8
        //     264: iflt +646 -> 910
        //     267: aload_0
        //     268: aload_0
        //     269: getfield 87	com/android/server/AppWidgetServiceImpl:mHosts	Ljava/util/ArrayList;
        //     272: iload 8
        //     274: invokevirtual 441	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     277: checkcast 16	com/android/server/AppWidgetServiceImpl$Host
        //     280: invokevirtual 782	com/android/server/AppWidgetServiceImpl:pruneHostLocked	(Lcom/android/server/AppWidgetServiceImpl$Host;)V
        //     283: iinc 8 255
        //     286: goto -24 -> 262
        //     289: astore 37
        //     291: iconst_1
        //     292: anewarray 510	java/lang/String
        //     295: astore 38
        //     297: aload 38
        //     299: iconst_0
        //     300: aload 33
        //     302: aastore
        //     303: aload 35
        //     305: aload 38
        //     307: invokevirtual 1028	android/content/pm/PackageManager:currentToCanonicalPackageNames	([Ljava/lang/String;)[Ljava/lang/String;
        //     310: iconst_0
        //     311: aaload
        //     312: astore 33
        //     314: goto -194 -> 120
        //     317: ldc_w 1030
        //     320: aload 21
        //     322: invokevirtual 513	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     325: ifeq +166 -> 491
        //     328: new 16	com/android/server/AppWidgetServiceImpl$Host
        //     331: dup
        //     332: invokespecial 901	com/android/server/AppWidgetServiceImpl$Host:<init>	()V
        //     335: astore 22
        //     337: aload 22
        //     339: aload 17
        //     341: aconst_null
        //     342: ldc_w 1012
        //     345: invokeinterface 1016 3 0
        //     350: putfield 342	com/android/server/AppWidgetServiceImpl$Host:packageName	Ljava/lang/String;
        //     353: aload 22
        //     355: aload_0
        //     356: aload 22
        //     358: getfield 342	com/android/server/AppWidgetServiceImpl$Host:packageName	Ljava/lang/String;
        //     361: invokevirtual 234	com/android/server/AppWidgetServiceImpl:getUidForPackage	(Ljava/lang/String;)I
        //     364: putfield 343	com/android/server/AppWidgetServiceImpl$Host:uid	I
        //     367: aload 22
        //     369: getfield 381	com/android/server/AppWidgetServiceImpl$Host:zombie	Z
        //     372: ifeq +10 -> 382
        //     375: aload_0
        //     376: getfield 1024	com/android/server/AppWidgetServiceImpl:mSafeMode	Z
        //     379: ifeq -141 -> 238
        //     382: aload 22
        //     384: aload 17
        //     386: aconst_null
        //     387: ldc_w 1032
        //     390: invokeinterface 1016 3 0
        //     395: bipush 16
        //     397: invokestatic 1035	java/lang/Integer:parseInt	(Ljava/lang/String;I)I
        //     400: putfield 336	com/android/server/AppWidgetServiceImpl$Host:hostId	I
        //     403: aload_0
        //     404: getfield 87	com/android/server/AppWidgetServiceImpl:mHosts	Ljava/util/ArrayList;
        //     407: aload 22
        //     409: invokevirtual 201	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     412: pop
        //     413: goto -175 -> 238
        //     416: astore 15
        //     418: ldc 33
        //     420: new 155	java/lang/StringBuilder
        //     423: dup
        //     424: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     427: ldc_w 1037
        //     430: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     433: aload 15
        //     435: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     438: invokevirtual 183	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     441: invokestatic 490	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     444: pop
        //     445: goto -199 -> 246
        //     448: astore 23
        //     450: aload 22
        //     452: iconst_1
        //     453: putfield 381	com/android/server/AppWidgetServiceImpl$Host:zombie	Z
        //     456: goto -89 -> 367
        //     459: astore 13
        //     461: ldc 33
        //     463: new 155	java/lang/StringBuilder
        //     466: dup
        //     467: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     470: ldc_w 1037
        //     473: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     476: aload 13
        //     478: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     481: invokevirtual 183	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     484: invokestatic 490	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     487: pop
        //     488: goto -242 -> 246
        //     491: ldc_w 1039
        //     494: aload 21
        //     496: invokevirtual 513	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     499: ifeq +66 -> 565
        //     502: aload 17
        //     504: aconst_null
        //     505: ldc_w 1040
        //     508: invokeinterface 1016 3 0
        //     513: astore 31
        //     515: aload 31
        //     517: ifnull -279 -> 238
        //     520: aload_0
        //     521: getfield 92	com/android/server/AppWidgetServiceImpl:mPackagesWithBindWidgetPermission	Ljava/util/HashSet;
        //     524: aload 31
        //     526: invokevirtual 456	java/util/HashSet:add	(Ljava/lang/Object;)Z
        //     529: pop
        //     530: goto -292 -> 238
        //     533: astore 11
        //     535: ldc 33
        //     537: new 155	java/lang/StringBuilder
        //     540: dup
        //     541: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     544: ldc_w 1037
        //     547: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     550: aload 11
        //     552: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     555: invokevirtual 183	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     558: invokestatic 490	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     561: pop
        //     562: goto -316 -> 246
        //     565: ldc_w 1042
        //     568: aload 21
        //     570: invokevirtual 513	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     573: ifeq -335 -> 238
        //     576: new 13	com/android/server/AppWidgetServiceImpl$AppWidgetId
        //     579: dup
        //     580: invokespecial 675	com/android/server/AppWidgetServiceImpl$AppWidgetId:<init>	()V
        //     583: astore 25
        //     585: aload 25
        //     587: aload 17
        //     589: aconst_null
        //     590: ldc_w 1032
        //     593: invokeinterface 1016 3 0
        //     598: bipush 16
        //     600: invokestatic 1035	java/lang/Integer:parseInt	(Ljava/lang/String;I)I
        //     603: putfield 274	com/android/server/AppWidgetServiceImpl$AppWidgetId:appWidgetId	I
        //     606: aload_0
        //     607: aload 25
        //     609: getfield 274	com/android/server/AppWidgetServiceImpl$AppWidgetId:appWidgetId	I
        //     612: invokestatic 1046	com/android/server/AppWidgetServiceImpl$Injector:isDuplicateWidgetId	(Lcom/android/server/AppWidgetServiceImpl;I)Z
        //     615: ifne -377 -> 238
        //     618: aload 25
        //     620: getfield 274	com/android/server/AppWidgetServiceImpl$AppWidgetId:appWidgetId	I
        //     623: aload_0
        //     624: getfield 83	com/android/server/AppWidgetServiceImpl:mNextAppWidgetId	I
        //     627: if_icmplt +14 -> 641
        //     630: aload_0
        //     631: iconst_1
        //     632: aload 25
        //     634: getfield 274	com/android/server/AppWidgetServiceImpl$AppWidgetId:appWidgetId	I
        //     637: iadd
        //     638: putfield 83	com/android/server/AppWidgetServiceImpl:mNextAppWidgetId	I
        //     641: aload 17
        //     643: aconst_null
        //     644: ldc_w 1010
        //     647: invokeinterface 1016 3 0
        //     652: astore 26
        //     654: aload 26
        //     656: ifnull +34 -> 690
        //     659: aload 25
        //     661: aload 19
        //     663: aload 26
        //     665: bipush 16
        //     667: invokestatic 1035	java/lang/Integer:parseInt	(Ljava/lang/String;I)I
        //     670: invokestatic 280	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     673: invokevirtual 271	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     676: checkcast 19	com/android/server/AppWidgetServiceImpl$Provider
        //     679: putfield 153	com/android/server/AppWidgetServiceImpl$AppWidgetId:provider	Lcom/android/server/AppWidgetServiceImpl$Provider;
        //     682: aload 25
        //     684: getfield 153	com/android/server/AppWidgetServiceImpl$AppWidgetId:provider	Lcom/android/server/AppWidgetServiceImpl$Provider;
        //     687: ifnull -449 -> 238
        //     690: aload 17
        //     692: aconst_null
        //     693: ldc_w 1030
        //     696: invokeinterface 1016 3 0
        //     701: bipush 16
        //     703: invokestatic 1035	java/lang/Integer:parseInt	(Ljava/lang/String;I)I
        //     706: istore 27
        //     708: aload 25
        //     710: aload_0
        //     711: getfield 87	com/android/server/AppWidgetServiceImpl:mHosts	Ljava/util/ArrayList;
        //     714: iload 27
        //     716: invokevirtual 441	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     719: checkcast 16	com/android/server/AppWidgetServiceImpl$Host
        //     722: putfield 333	com/android/server/AppWidgetServiceImpl$AppWidgetId:host	Lcom/android/server/AppWidgetServiceImpl$Host;
        //     725: aload 25
        //     727: getfield 333	com/android/server/AppWidgetServiceImpl$AppWidgetId:host	Lcom/android/server/AppWidgetServiceImpl$Host;
        //     730: ifnull -492 -> 238
        //     733: aload 25
        //     735: getfield 153	com/android/server/AppWidgetServiceImpl$AppWidgetId:provider	Lcom/android/server/AppWidgetServiceImpl$Provider;
        //     738: ifnull +17 -> 755
        //     741: aload 25
        //     743: getfield 153	com/android/server/AppWidgetServiceImpl$AppWidgetId:provider	Lcom/android/server/AppWidgetServiceImpl$Provider;
        //     746: getfield 197	com/android/server/AppWidgetServiceImpl$Provider:instances	Ljava/util/ArrayList;
        //     749: aload 25
        //     751: invokevirtual 201	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     754: pop
        //     755: aload 25
        //     757: getfield 333	com/android/server/AppWidgetServiceImpl$AppWidgetId:host	Lcom/android/server/AppWidgetServiceImpl$Host;
        //     760: getfield 378	com/android/server/AppWidgetServiceImpl$Host:instances	Ljava/util/ArrayList;
        //     763: aload 25
        //     765: invokevirtual 201	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     768: pop
        //     769: aload_0
        //     770: getfield 85	com/android/server/AppWidgetServiceImpl:mAppWidgetIds	Ljava/util/ArrayList;
        //     773: aload 25
        //     775: invokevirtual 201	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     778: pop
        //     779: goto -541 -> 238
        //     782: astore 9
        //     784: ldc 33
        //     786: new 155	java/lang/StringBuilder
        //     789: dup
        //     790: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     793: ldc_w 1037
        //     796: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     799: aload 9
        //     801: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     804: invokevirtual 183	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     807: invokestatic 490	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     810: pop
        //     811: goto -565 -> 246
        //     814: astore_3
        //     815: ldc 33
        //     817: new 155	java/lang/StringBuilder
        //     820: dup
        //     821: invokespecial 156	java/lang/StringBuilder:<init>	()V
        //     824: ldc_w 1037
        //     827: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     830: aload_3
        //     831: invokevirtual 179	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     834: invokevirtual 183	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     837: invokestatic 490	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     840: pop
        //     841: goto -595 -> 246
        //     844: ldc 33
        //     846: ldc_w 1048
        //     849: invokestatic 490	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     852: pop
        //     853: aload_0
        //     854: getfield 85	com/android/server/AppWidgetServiceImpl:mAppWidgetIds	Ljava/util/ArrayList;
        //     857: invokevirtual 809	java/util/ArrayList:clear	()V
        //     860: aload_0
        //     861: getfield 87	com/android/server/AppWidgetServiceImpl:mHosts	Ljava/util/ArrayList;
        //     864: invokevirtual 809	java/util/ArrayList:clear	()V
        //     867: aload_0
        //     868: getfield 81	com/android/server/AppWidgetServiceImpl:mInstalledProviders	Ljava/util/ArrayList;
        //     871: invokevirtual 205	java/util/ArrayList:size	()I
        //     874: istore 6
        //     876: iconst_0
        //     877: istore 7
        //     879: iload 7
        //     881: iload 6
        //     883: if_icmpge +27 -> 910
        //     886: aload_0
        //     887: getfield 81	com/android/server/AppWidgetServiceImpl:mInstalledProviders	Ljava/util/ArrayList;
        //     890: iload 7
        //     892: invokevirtual 441	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     895: checkcast 19	com/android/server/AppWidgetServiceImpl$Provider
        //     898: getfield 197	com/android/server/AppWidgetServiceImpl$Provider:instances	Ljava/util/ArrayList;
        //     901: invokevirtual 809	java/util/ArrayList:clear	()V
        //     904: iinc 7 1
        //     907: goto -28 -> 879
        //     910: return
        //
        // Exception table:
        //     from	to	target	type
        //     98	120	289	android/content/pm/PackageManager$NameNotFoundException
        //     2	98	416	java/lang/NullPointerException
        //     98	120	416	java/lang/NullPointerException
        //     120	235	416	java/lang/NullPointerException
        //     291	353	416	java/lang/NullPointerException
        //     353	367	416	java/lang/NullPointerException
        //     367	413	416	java/lang/NullPointerException
        //     450	456	416	java/lang/NullPointerException
        //     491	530	416	java/lang/NullPointerException
        //     565	779	416	java/lang/NullPointerException
        //     353	367	448	android/content/pm/PackageManager$NameNotFoundException
        //     2	98	459	java/lang/NumberFormatException
        //     98	120	459	java/lang/NumberFormatException
        //     120	235	459	java/lang/NumberFormatException
        //     291	353	459	java/lang/NumberFormatException
        //     353	367	459	java/lang/NumberFormatException
        //     367	413	459	java/lang/NumberFormatException
        //     450	456	459	java/lang/NumberFormatException
        //     491	530	459	java/lang/NumberFormatException
        //     565	779	459	java/lang/NumberFormatException
        //     2	98	533	org/xmlpull/v1/XmlPullParserException
        //     98	120	533	org/xmlpull/v1/XmlPullParserException
        //     120	235	533	org/xmlpull/v1/XmlPullParserException
        //     291	353	533	org/xmlpull/v1/XmlPullParserException
        //     353	367	533	org/xmlpull/v1/XmlPullParserException
        //     367	413	533	org/xmlpull/v1/XmlPullParserException
        //     450	456	533	org/xmlpull/v1/XmlPullParserException
        //     491	530	533	org/xmlpull/v1/XmlPullParserException
        //     565	779	533	org/xmlpull/v1/XmlPullParserException
        //     2	98	782	java/io/IOException
        //     98	120	782	java/io/IOException
        //     120	235	782	java/io/IOException
        //     291	353	782	java/io/IOException
        //     353	367	782	java/io/IOException
        //     367	413	782	java/io/IOException
        //     450	456	782	java/io/IOException
        //     491	530	782	java/io/IOException
        //     565	779	782	java/io/IOException
        //     2	98	814	java/lang/IndexOutOfBoundsException
        //     98	120	814	java/lang/IndexOutOfBoundsException
        //     120	235	814	java/lang/IndexOutOfBoundsException
        //     291	353	814	java/lang/IndexOutOfBoundsException
        //     353	367	814	java/lang/IndexOutOfBoundsException
        //     367	413	814	java/lang/IndexOutOfBoundsException
        //     450	456	814	java/lang/IndexOutOfBoundsException
        //     491	530	814	java/lang/IndexOutOfBoundsException
        //     565	779	814	java/lang/IndexOutOfBoundsException
    }

    void registerForBroadcastsLocked(Provider paramProvider, int[] paramArrayOfInt)
    {
        int i = 1;
        if (paramProvider.info.updatePeriodMillis > 0)
            if (paramProvider.broadcast == null)
                break label130;
        while (true)
        {
            Intent localIntent = new Intent("android.appwidget.action.APPWIDGET_UPDATE");
            localIntent.putExtra("appWidgetIds", paramArrayOfInt);
            localIntent.setComponent(paramProvider.info.provider);
            long l1 = Binder.clearCallingIdentity();
            try
            {
                paramProvider.broadcast = PendingIntent.getBroadcast(this.mContext, 1, localIntent, 134217728);
                Binder.restoreCallingIdentity(l1);
                long l2;
                if (i == 0)
                {
                    l2 = paramProvider.info.updatePeriodMillis;
                    if (l2 < 1800000L)
                        l2 = 1800000L;
                }
                return;
                label130: i = 0;
            }
            finally
            {
                Binder.restoreCallingIdentity(l1);
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void reload()
    {
        synchronized (this.mAppWidgetIds)
        {
            this.mAppWidgetIds.clear();
            this.mHosts.clear();
            this.mInstalledProviders.clear();
            this.mStateLoaded = false;
            sendInitialBroadcasts();
            return;
        }
    }

    void removeProviderLocked(int paramInt, Provider paramProvider)
    {
        int i = paramProvider.instances.size();
        for (int j = 0; j < i; j++)
        {
            AppWidgetId localAppWidgetId = (AppWidgetId)paramProvider.instances.get(j);
            updateAppWidgetInstanceLocked(localAppWidgetId, null);
            cancelBroadcasts(paramProvider);
            localAppWidgetId.host.instances.remove(localAppWidgetId);
            this.mAppWidgetIds.remove(localAppWidgetId);
            localAppWidgetId.provider = null;
            pruneHostLocked(localAppWidgetId.host);
            localAppWidgetId.host = null;
        }
        paramProvider.instances.clear();
        this.mInstalledProviders.remove(paramInt);
        this.mDeletedProviders.add(paramProvider);
        cancelBroadcasts(paramProvider);
    }

    void removeProvidersForPackageLocked(String paramString)
    {
        for (int i = -1 + this.mInstalledProviders.size(); i >= 0; i--)
        {
            Provider localProvider = (Provider)this.mInstalledProviders.get(i);
            if (paramString.equals(localProvider.info.provider.getPackageName()))
                removeProviderLocked(i, localProvider);
        }
        for (int j = -1 + this.mHosts.size(); j >= 0; j--)
        {
            Host localHost = (Host)this.mHosts.get(j);
            if (paramString.equals(localHost.packageName))
                deleteHostLocked(localHost);
        }
    }

    void saveStateLocked()
    {
        AtomicFile localAtomicFile = savedStateFile();
        try
        {
            FileOutputStream localFileOutputStream = localAtomicFile.startWrite();
            if (writeStateToFileLocked(localFileOutputStream))
            {
                localAtomicFile.finishWrite(localFileOutputStream);
            }
            else
            {
                localAtomicFile.failWrite(localFileOutputStream);
                Slog.w("AppWidgetServiceImpl", "Failed to save state, restoring backup.");
            }
        }
        catch (IOException localIOException)
        {
            Slog.w("AppWidgetServiceImpl", "Failed open state file for write: " + localIOException);
        }
    }

    AtomicFile savedStateFile()
    {
        File localFile1 = new File("/data/system/users/" + this.mUserId);
        File localFile2 = getSettingsFile(this.mUserId);
        if ((!localFile2.exists()) && (this.mUserId == 0))
        {
            if (!localFile1.exists())
                localFile1.mkdirs();
            new File("/data/system/appwidgets.xml").renameTo(localFile2);
        }
        return new AtomicFile(localFile2);
    }

    void sendEnableIntentLocked(Provider paramProvider)
    {
        Intent localIntent = new Intent("android.appwidget.action.APPWIDGET_ENABLED");
        localIntent.setComponent(paramProvider.info.provider);
        this.mContext.sendBroadcast(localIntent, this.mUserId);
    }

    void sendInitialBroadcasts()
    {
        while (true)
        {
            int j;
            synchronized (this.mAppWidgetIds)
            {
                ensureStateLoadedLocked();
                int i = this.mInstalledProviders.size();
                j = 0;
                if (j < i)
                {
                    Provider localProvider = (Provider)this.mInstalledProviders.get(j);
                    if (localProvider.instances.size() > 0)
                    {
                        sendEnableIntentLocked(localProvider);
                        int[] arrayOfInt = getAppWidgetIds(localProvider);
                        sendUpdateIntentLocked(localProvider, arrayOfInt);
                        registerForBroadcastsLocked(localProvider, arrayOfInt);
                    }
                }
                else
                {
                    return;
                }
            }
            j++;
        }
    }

    void sendUpdateIntentLocked(Provider paramProvider, int[] paramArrayOfInt)
    {
        if ((paramArrayOfInt != null) && (paramArrayOfInt.length > 0))
        {
            Intent localIntent = new Intent("android.appwidget.action.APPWIDGET_UPDATE");
            localIntent.putExtra("appWidgetIds", paramArrayOfInt);
            localIntent.setComponent(paramProvider.info.provider);
            this.mContext.sendBroadcast(localIntent, this.mUserId);
        }
    }

    public void setBindAppWidgetPermission(String paramString, boolean paramBoolean)
    {
        this.mContext.enforceCallingPermission("android.permission.MODIFY_APPWIDGET_BIND_PERMISSIONS", "setBindAppWidgetPermission packageName=" + paramString);
        synchronized (this.mAppWidgetIds)
        {
            ensureStateLoadedLocked();
            if (paramBoolean)
            {
                this.mPackagesWithBindWidgetPermission.add(paramString);
                saveStateLocked();
                return;
            }
            this.mPackagesWithBindWidgetPermission.remove(paramString);
        }
    }

    public int[] startListening(IAppWidgetHost paramIAppWidgetHost, String paramString, int paramInt, List<RemoteViews> paramList)
    {
        int i = enforceCallingUid(paramString);
        synchronized (this.mAppWidgetIds)
        {
            ensureStateLoadedLocked();
            Host localHost = lookupOrAddHostLocked(i, paramString, paramInt);
            localHost.callbacks = paramIAppWidgetHost;
            paramList.clear();
            ArrayList localArrayList2 = localHost.instances;
            int j = localArrayList2.size();
            int[] arrayOfInt = new int[j];
            for (int k = 0; k < j; k++)
            {
                AppWidgetId localAppWidgetId = (AppWidgetId)localArrayList2.get(k);
                arrayOfInt[k] = localAppWidgetId.appWidgetId;
                paramList.add(localAppWidgetId.views);
            }
            return arrayOfInt;
        }
    }

    public void stopListening(int paramInt)
    {
        synchronized (this.mAppWidgetIds)
        {
            ensureStateLoadedLocked();
            Host localHost = lookupHostLocked(Binder.getCallingUid(), paramInt);
            if (localHost != null)
            {
                localHost.callbacks = null;
                pruneHostLocked(localHost);
            }
            return;
        }
    }

    public void systemReady(boolean paramBoolean)
    {
        this.mSafeMode = paramBoolean;
        synchronized (this.mAppWidgetIds)
        {
            ensureStateLoadedLocked();
            return;
        }
    }

    // ERROR //
    public void unbindRemoteViewsService(int paramInt, Intent paramIntent)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 85	com/android/server/AppWidgetServiceImpl:mAppWidgetIds	Ljava/util/ArrayList;
        //     4: astore_3
        //     5: aload_3
        //     6: monitorenter
        //     7: aload_0
        //     8: invokespecial 134	com/android/server/AppWidgetServiceImpl:ensureStateLoadedLocked	()V
        //     11: iload_1
        //     12: invokestatic 280	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     15: new 267	android/content/Intent$FilterComparison
        //     18: dup
        //     19: aload_2
        //     20: invokespecial 720	android/content/Intent$FilterComparison:<init>	(Landroid/content/Intent;)V
        //     23: invokestatic 724	android/util/Pair:create	(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
        //     26: astore 5
        //     28: aload_0
        //     29: getfield 74	com/android/server/AppWidgetServiceImpl:mBoundRemoteViewsServices	Ljava/util/HashMap;
        //     32: aload 5
        //     34: invokevirtual 455	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
        //     37: ifeq +69 -> 106
        //     40: aload_0
        //     41: iload_1
        //     42: invokevirtual 138	com/android/server/AppWidgetServiceImpl:lookupAppWidgetIdLocked	(I)Lcom/android/server/AppWidgetServiceImpl$AppWidgetId;
        //     45: ifnonnull +20 -> 65
        //     48: new 140	java/lang/IllegalArgumentException
        //     51: dup
        //     52: ldc 142
        //     54: invokespecial 145	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     57: athrow
        //     58: astore 4
        //     60: aload_3
        //     61: monitorexit
        //     62: aload 4
        //     64: athrow
        //     65: aload_0
        //     66: getfield 74	com/android/server/AppWidgetServiceImpl:mBoundRemoteViewsServices	Ljava/util/HashMap;
        //     69: aload 5
        //     71: invokevirtual 271	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     74: checkcast 10	com/android/server/AppWidgetServiceImpl$ServiceConnectionProxy
        //     77: astore 7
        //     79: aload 7
        //     81: invokevirtual 610	com/android/server/AppWidgetServiceImpl$ServiceConnectionProxy:disconnect	()V
        //     84: aload_0
        //     85: getfield 98	com/android/server/AppWidgetServiceImpl:mContext	Landroid/content/Context;
        //     88: aload 7
        //     90: invokevirtual 614	android/content/Context:unbindService	(Landroid/content/ServiceConnection;)V
        //     93: aload_0
        //     94: getfield 74	com/android/server/AppWidgetServiceImpl:mBoundRemoteViewsServices	Ljava/util/HashMap;
        //     97: aload 5
        //     99: invokevirtual 726	java/util/HashMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
        //     102: pop
        //     103: aload_3
        //     104: monitorexit
        //     105: return
        //     106: ldc_w 1133
        //     109: ldc_w 1135
        //     112: invokestatic 1140	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     115: pop
        //     116: goto -13 -> 103
        //
        // Exception table:
        //     from	to	target	type
        //     7	62	58	finally
        //     65	116	58	finally
    }

    public void updateAppWidgetIds(int[] paramArrayOfInt, RemoteViews paramRemoteViews)
    {
        if (paramArrayOfInt == null);
        while (true)
        {
            return;
            int i = 0;
            if (paramRemoteViews != null)
                i = paramRemoteViews.estimateMemoryUsage();
            if (i > this.mMaxWidgetBitmapMemory)
                throw new IllegalArgumentException("RemoteViews for widget update exceeds maximum bitmap memory usage (used: " + i + ", max: " + this.mMaxWidgetBitmapMemory + ") The total memory cannot exceed that required to" + " fill the device's screen once.");
            if (paramArrayOfInt.length == 0)
                continue;
            int j = paramArrayOfInt.length;
            synchronized (this.mAppWidgetIds)
            {
                ensureStateLoadedLocked();
                for (int k = 0; k < j; k++)
                    updateAppWidgetInstanceLocked(lookupAppWidgetIdLocked(paramArrayOfInt[k]), paramRemoteViews);
            }
        }
    }

    void updateAppWidgetInstanceLocked(AppWidgetId paramAppWidgetId, RemoteViews paramRemoteViews)
    {
        updateAppWidgetInstanceLocked(paramAppWidgetId, paramRemoteViews, false);
    }

    void updateAppWidgetInstanceLocked(AppWidgetId paramAppWidgetId, RemoteViews paramRemoteViews, boolean paramBoolean)
    {
        if ((paramAppWidgetId != null) && (paramAppWidgetId.provider != null) && (!paramAppWidgetId.provider.zombie) && (!paramAppWidgetId.host.zombie))
        {
            if (!paramBoolean)
                paramAppWidgetId.views = paramRemoteViews;
            if (paramAppWidgetId.host.callbacks == null);
        }
        try
        {
            paramAppWidgetId.host.callbacks.updateAppWidget(paramAppWidgetId.appWidgetId, paramRemoteViews);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                paramAppWidgetId.host.callbacks = null;
        }
    }

    public void updateAppWidgetOptions(int paramInt, Bundle paramBundle)
    {
        synchronized (this.mAppWidgetIds)
        {
            ensureStateLoadedLocked();
            AppWidgetId localAppWidgetId = lookupAppWidgetIdLocked(paramInt);
            if (localAppWidgetId != null)
            {
                Provider localProvider = localAppWidgetId.provider;
                localAppWidgetId.options = paramBundle;
                Intent localIntent = new Intent("android.appwidget.action.APPWIDGET_UPDATE_OPTIONS");
                localIntent.setComponent(localProvider.info.provider);
                localIntent.putExtra("appWidgetId", localAppWidgetId.appWidgetId);
                localIntent.putExtra("appWidgetOptions", paramBundle);
                this.mContext.sendBroadcast(localIntent, this.mUserId);
            }
        }
    }

    public void updateAppWidgetProvider(ComponentName paramComponentName, RemoteViews paramRemoteViews)
    {
        while (true)
        {
            int k;
            synchronized (this.mAppWidgetIds)
            {
                ensureStateLoadedLocked();
                Provider localProvider = lookupProviderLocked(paramComponentName);
                if (localProvider == null)
                {
                    Slog.w("AppWidgetServiceImpl", "updateAppWidgetProvider: provider doesn't exist: " + paramComponentName);
                }
                else
                {
                    ArrayList localArrayList2 = localProvider.instances;
                    int i = Binder.getCallingUid();
                    int j = localArrayList2.size();
                    k = 0;
                    if (k < j)
                    {
                        AppWidgetId localAppWidgetId = (AppWidgetId)localArrayList2.get(k);
                        if (!canAccessAppWidgetId(localAppWidgetId, i))
                            break label129;
                        updateAppWidgetInstanceLocked(localAppWidgetId, paramRemoteViews);
                        break label129;
                    }
                }
            }
            return;
            label129: k++;
        }
    }

    void updateProvidersForPackageLocked(String paramString)
    {
        HashSet localHashSet = new HashSet();
        Intent localIntent = new Intent("android.appwidget.action.APPWIDGET_UPDATE");
        localIntent.setPackage(paramString);
        List localList;
        int i;
        ResolveInfo localResolveInfo;
        ActivityInfo localActivityInfo;
        try
        {
            localList = this.mPm.queryIntentReceivers(localIntent, localIntent.resolveTypeIfNeeded(this.mContext.getContentResolver()), 128, this.mUserId);
            if (localList == null)
            {
                i = 0;
                label109: for (int j = 0; ; j++)
                {
                    if (j >= i)
                        break label371;
                    localResolveInfo = (ResolveInfo)localList.get(j);
                    localActivityInfo = localResolveInfo.activityInfo;
                    if ((0x40000 & localActivityInfo.applicationInfo.flags) == 0)
                        break;
                }
            }
        }
        catch (RemoteException localRemoteException1)
        {
        }
        while (true)
        {
            return;
            i = localList.size();
            break;
            if (!paramString.equals(localActivityInfo.packageName))
                break label109;
            ComponentName localComponentName = new ComponentName(localActivityInfo.packageName, localActivityInfo.name);
            Provider localProvider2 = lookupProviderLocked(localComponentName);
            if (localProvider2 == null)
            {
                if (!addProviderLocked(localResolveInfo))
                    break label109;
                localHashSet.add(localActivityInfo.name);
                break label109;
            }
            Provider localProvider3 = parseProviderInfoXml(localComponentName, localResolveInfo);
            if (localProvider3 == null)
                break label109;
            localHashSet.add(localActivityInfo.name);
            localProvider2.info = localProvider3.info;
            int m = localProvider2.instances.size();
            if (m <= 0)
                break label109;
            int[] arrayOfInt = getAppWidgetIds(localProvider2);
            cancelBroadcasts(localProvider2);
            registerForBroadcastsLocked(localProvider2, arrayOfInt);
            int n = 0;
            while (true)
                if (n < m)
                {
                    AppWidgetId localAppWidgetId = (AppWidgetId)localProvider2.instances.get(n);
                    localAppWidgetId.views = null;
                    if ((localAppWidgetId.host != null) && (localAppWidgetId.host.callbacks != null));
                    try
                    {
                        localAppWidgetId.host.callbacks.providerChanged(localAppWidgetId.appWidgetId, localProvider2.info);
                        n++;
                    }
                    catch (RemoteException localRemoteException2)
                    {
                        while (true)
                            localAppWidgetId.host.callbacks = null;
                    }
                }
            sendUpdateIntentLocked(localProvider2, arrayOfInt);
            break label109;
            label371: for (int k = -1 + this.mInstalledProviders.size(); k >= 0; k--)
            {
                Provider localProvider1 = (Provider)this.mInstalledProviders.get(k);
                if ((paramString.equals(localProvider1.info.provider.getPackageName())) && (!localHashSet.contains(localProvider1.info.provider.getClassName())))
                    removeProviderLocked(k, localProvider1);
            }
        }
    }

    boolean writeStateToFileLocked(FileOutputStream paramFileOutputStream)
    {
        while (true)
        {
            FastXmlSerializer localFastXmlSerializer;
            int k;
            try
            {
                localFastXmlSerializer = new FastXmlSerializer();
                localFastXmlSerializer.setOutput(paramFileOutputStream, "utf-8");
                localFastXmlSerializer.startDocument(null, Boolean.valueOf(true));
                localFastXmlSerializer.startTag(null, "gs");
                int i = 0;
                int j = this.mInstalledProviders.size();
                k = 0;
                if (k < j)
                {
                    Provider localProvider = (Provider)this.mInstalledProviders.get(k);
                    if (localProvider.instances.size() <= 0)
                        break label522;
                    localFastXmlSerializer.startTag(null, "p");
                    localFastXmlSerializer.attribute(null, "pkg", localProvider.info.provider.getPackageName());
                    localFastXmlSerializer.attribute(null, "cl", localProvider.info.provider.getClassName());
                    localFastXmlSerializer.endTag(null, "p");
                    localProvider.tag = i;
                    i++;
                    break label522;
                }
                int m = this.mHosts.size();
                int n = 0;
                if (n < m)
                {
                    Host localHost = (Host)this.mHosts.get(n);
                    localFastXmlSerializer.startTag(null, "h");
                    localFastXmlSerializer.attribute(null, "pkg", localHost.packageName);
                    localFastXmlSerializer.attribute(null, "id", Integer.toHexString(localHost.hostId));
                    localFastXmlSerializer.endTag(null, "h");
                    localHost.tag = n;
                    n++;
                    continue;
                }
                int i1 = this.mAppWidgetIds.size();
                int i2 = 0;
                if (i2 < i1)
                {
                    AppWidgetId localAppWidgetId = (AppWidgetId)this.mAppWidgetIds.get(i2);
                    localFastXmlSerializer.startTag(null, "g");
                    localFastXmlSerializer.attribute(null, "id", Integer.toHexString(localAppWidgetId.appWidgetId));
                    localFastXmlSerializer.attribute(null, "h", Integer.toHexString(localAppWidgetId.host.tag));
                    if (localAppWidgetId.provider != null)
                        localFastXmlSerializer.attribute(null, "p", Integer.toHexString(localAppWidgetId.provider.tag));
                    localFastXmlSerializer.endTag(null, "g");
                    i2++;
                    continue;
                }
                Iterator localIterator = this.mPackagesWithBindWidgetPermission.iterator();
                if (localIterator.hasNext())
                {
                    localFastXmlSerializer.startTag(null, "b");
                    localFastXmlSerializer.attribute(null, "packageName", (String)localIterator.next());
                    localFastXmlSerializer.endTag(null, "b");
                    continue;
                }
            }
            catch (IOException localIOException)
            {
                Slog.w("AppWidgetServiceImpl", "Failed to write state: " + localIOException);
                bool = false;
                return bool;
            }
            localFastXmlSerializer.endTag(null, "gs");
            localFastXmlSerializer.endDocument();
            boolean bool = true;
            continue;
            label522: k++;
        }
    }

    static class ServiceConnectionProxy
        implements ServiceConnection
    {
        private final IBinder mConnectionCb;

        ServiceConnectionProxy(Pair<Integer, Intent.FilterComparison> paramPair, IBinder paramIBinder)
        {
            this.mConnectionCb = paramIBinder;
        }

        public void disconnect()
        {
            IRemoteViewsAdapterConnection localIRemoteViewsAdapterConnection = IRemoteViewsAdapterConnection.Stub.asInterface(this.mConnectionCb);
            try
            {
                localIRemoteViewsAdapterConnection.onServiceDisconnected();
                return;
            }
            catch (Exception localException)
            {
                while (true)
                    localException.printStackTrace();
            }
        }

        public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
        {
            IRemoteViewsAdapterConnection localIRemoteViewsAdapterConnection = IRemoteViewsAdapterConnection.Stub.asInterface(this.mConnectionCb);
            try
            {
                localIRemoteViewsAdapterConnection.onServiceConnected(paramIBinder);
                return;
            }
            catch (Exception localException)
            {
                while (true)
                    localException.printStackTrace();
            }
        }

        public void onServiceDisconnected(ComponentName paramComponentName)
        {
            disconnect();
        }
    }

    static class AppWidgetId
    {
        int appWidgetId;
        AppWidgetServiceImpl.Host host;
        Bundle options;
        AppWidgetServiceImpl.Provider provider;
        RemoteViews views;
    }

    static class Host
    {
        IAppWidgetHost callbacks;
        int hostId;
        ArrayList<AppWidgetServiceImpl.AppWidgetId> instances = new ArrayList();
        String packageName;
        int tag;
        int uid;
        boolean zombie;
    }

    static class Provider
    {
        PendingIntent broadcast;
        AppWidgetProviderInfo info;
        ArrayList<AppWidgetServiceImpl.AppWidgetId> instances = new ArrayList();
        int tag;
        int uid;
        boolean zombie;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static boolean isDuplicateWidgetId(AppWidgetServiceImpl paramAppWidgetServiceImpl, int paramInt)
        {
            Iterator localIterator = paramAppWidgetServiceImpl.mAppWidgetIds.iterator();
            do
                if (!localIterator.hasNext())
                    break;
            while (((AppWidgetServiceImpl.AppWidgetId)localIterator.next()).appWidgetId != paramInt);
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.AppWidgetServiceImpl
 * JD-Core Version:        0.6.2
 */