package com.android.server;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.INetworkManagementEventObserver;
import android.net.INetworkManagementEventObserver.Stub;
import android.net.InterfaceConfiguration;
import android.os.Binder;
import android.os.CommonTimeConfig;
import android.os.CommonTimeConfig.OnServerDiedListener;
import android.os.Handler;
import android.os.INetworkManagementService;
import android.os.INetworkManagementService.Stub;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.util.Log;

class CommonTimeManagementService extends Binder
{
    private static final boolean ALLOW_WIFI = false;
    private static final String ALLOW_WIFI_PROP = "ro.common_time.allow_wifi";
    private static final boolean AUTO_DISABLE = false;
    private static final String AUTO_DISABLE_PROP = "ro.common_time.auto_disable";
    private static final byte BASE_SERVER_PRIO = 0;
    private static final InterfaceScoreRule[] IFACE_SCORE_RULES;
    private static final int NATIVE_SERVICE_RECONNECT_TIMEOUT = 5000;
    private static final int NO_INTERFACE_TIMEOUT = 0;
    private static final String NO_INTERFACE_TIMEOUT_PROP = "ro.common_time.no_iface_timeout";
    private static final String SERVER_PRIO_PROP = "ro.common_time.server_prio";
    private static final String TAG = CommonTimeManagementService.class.getSimpleName();
    private CommonTimeConfig mCTConfig;
    private CommonTimeConfig.OnServerDiedListener mCTServerDiedListener = new CommonTimeConfig.OnServerDiedListener()
    {
        public void onServerDied()
        {
            CommonTimeManagementService.this.scheduleTimeConfigReconnect();
        }
    };
    private BroadcastReceiver mConnectivityMangerObserver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            CommonTimeManagementService.this.reevaluateServiceState();
        }
    };
    private final Context mContext;
    private String mCurIface;
    private boolean mDetectedAtStartup = false;
    private byte mEffectivePrio = BASE_SERVER_PRIO;
    private INetworkManagementEventObserver mIfaceObserver = new INetworkManagementEventObserver.Stub()
    {
        public void interfaceAdded(String paramAnonymousString)
        {
            CommonTimeManagementService.this.reevaluateServiceState();
        }

        public void interfaceLinkStateChanged(String paramAnonymousString, boolean paramAnonymousBoolean)
        {
            CommonTimeManagementService.this.reevaluateServiceState();
        }

        public void interfaceRemoved(String paramAnonymousString)
        {
            CommonTimeManagementService.this.reevaluateServiceState();
        }

        public void interfaceStatusChanged(String paramAnonymousString, boolean paramAnonymousBoolean)
        {
            CommonTimeManagementService.this.reevaluateServiceState();
        }

        public void limitReached(String paramAnonymousString1, String paramAnonymousString2)
        {
        }
    };
    private Object mLock = new Object();
    private INetworkManagementService mNetMgr;
    private Handler mNoInterfaceHandler = new Handler();
    private Runnable mNoInterfaceRunnable = new Runnable()
    {
        public void run()
        {
            CommonTimeManagementService.this.handleNoInterfaceTimeout();
        }
    };
    private Handler mReconnectHandler = new Handler();
    private Runnable mReconnectRunnable = new Runnable()
    {
        public void run()
        {
            CommonTimeManagementService.this.connectToTimeConfig();
        }
    };

    static
    {
        boolean bool1;
        boolean bool2;
        label34: int i;
        label64: InterfaceScoreRule[] arrayOfInterfaceScoreRule2;
        if (SystemProperties.getInt("ro.common_time.auto_disable", 1) != 0)
        {
            bool1 = true;
            AUTO_DISABLE = bool1;
            if (SystemProperties.getInt("ro.common_time.allow_wifi", 0) == 0)
                break label115;
            bool2 = true;
            ALLOW_WIFI = bool2;
            i = SystemProperties.getInt("ro.common_time.server_prio", 1);
            NO_INTERFACE_TIMEOUT = SystemProperties.getInt("ro.common_time.no_iface_timeout", 60000);
            if (i >= 1)
                break label120;
            BASE_SERVER_PRIO = 1;
            if (!ALLOW_WIFI)
                break label141;
            arrayOfInterfaceScoreRule2 = new InterfaceScoreRule[2];
            arrayOfInterfaceScoreRule2[0] = new InterfaceScoreRule("wlan", 1);
            arrayOfInterfaceScoreRule2[1] = new InterfaceScoreRule("eth", 2);
        }
        label115: label120: InterfaceScoreRule[] arrayOfInterfaceScoreRule1;
        for (IFACE_SCORE_RULES = arrayOfInterfaceScoreRule2; ; IFACE_SCORE_RULES = arrayOfInterfaceScoreRule1)
        {
            return;
            bool1 = false;
            break;
            bool2 = false;
            break label34;
            if (i > 30)
            {
                BASE_SERVER_PRIO = 30;
                break label64;
            }
            BASE_SERVER_PRIO = i;
            break label64;
            label141: arrayOfInterfaceScoreRule1 = new InterfaceScoreRule[1];
            arrayOfInterfaceScoreRule1[0] = new InterfaceScoreRule("eth", 2);
        }
    }

    public CommonTimeManagementService(Context paramContext)
    {
        this.mContext = paramContext;
    }

    private void cleanupTimeConfig()
    {
        this.mReconnectHandler.removeCallbacks(this.mReconnectRunnable);
        this.mNoInterfaceHandler.removeCallbacks(this.mNoInterfaceRunnable);
        if (this.mCTConfig != null)
        {
            this.mCTConfig.release();
            this.mCTConfig = null;
        }
    }

    private void connectToTimeConfig()
    {
        cleanupTimeConfig();
        try
        {
            synchronized (this.mLock)
            {
                this.mCTConfig = new CommonTimeConfig();
                this.mCTConfig.setServerDiedListener(this.mCTServerDiedListener);
                this.mCurIface = this.mCTConfig.getInterfaceBinding();
                this.mCTConfig.setAutoDisable(AUTO_DISABLE);
                this.mCTConfig.setMasterElectionPriority(this.mEffectivePrio);
                if (NO_INTERFACE_TIMEOUT >= 0)
                    this.mNoInterfaceHandler.postDelayed(this.mNoInterfaceRunnable, NO_INTERFACE_TIMEOUT);
                reevaluateServiceState();
                return;
            }
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                scheduleTimeConfigReconnect();
        }
    }

    private void handleNoInterfaceTimeout()
    {
        if (this.mCTConfig != null)
        {
            Log.i(TAG, "Timeout waiting for interface to come up.    Forcing networkless master mode.");
            if (-7 == this.mCTConfig.forceNetworklessMasterMode())
                scheduleTimeConfigReconnect();
        }
    }

    private void reevaluateServiceState()
    {
        Object localObject1 = null;
        int i = -1;
        while (true)
        {
            int n;
            int i3;
            try
            {
                String[] arrayOfString = this.mNetMgr.listInterfaces();
                if (arrayOfString != null)
                {
                    int m = arrayOfString.length;
                    n = 0;
                    if (n < m)
                    {
                        String str3 = arrayOfString[n];
                        i1 = -1;
                        InterfaceScoreRule[] arrayOfInterfaceScoreRule = IFACE_SCORE_RULES;
                        int i2 = arrayOfInterfaceScoreRule.length;
                        i3 = 0;
                        if (i3 >= i2)
                            continue;
                        InterfaceScoreRule localInterfaceScoreRule = arrayOfInterfaceScoreRule[i3];
                        if (!str3.contains(localInterfaceScoreRule.mPrefix))
                            break label441;
                        i1 = localInterfaceScoreRule.mScore;
                        continue;
                        InterfaceConfiguration localInterfaceConfiguration = this.mNetMgr.getInterfaceConfig(str3);
                        if (localInterfaceConfiguration == null)
                            break label435;
                        boolean bool = localInterfaceConfiguration.isActive();
                        if (!bool)
                            break label435;
                        localObject1 = str3;
                        i = i1;
                    }
                }
            }
            catch (RemoteException localRemoteException)
            {
                int i1;
                localObject1 = null;
                int j = 1;
                Object localObject2 = this.mLock;
                if (localObject1 != null);
                try
                {
                    if (this.mCurIface == null)
                    {
                        String str2 = TAG;
                        Object[] arrayOfObject2 = new Object[1];
                        arrayOfObject2[0] = localObject1;
                        Log.e(str2, String.format("Binding common time service to %s.", arrayOfObject2));
                        this.mCurIface = localObject1;
                        if ((j != 0) && (this.mCTConfig != null))
                        {
                            if (i <= 0)
                                continue;
                            k = i * BASE_SERVER_PRIO;
                            if (k != this.mEffectivePrio)
                            {
                                this.mEffectivePrio = k;
                                this.mCTConfig.setMasterElectionPriority(this.mEffectivePrio);
                            }
                            if (this.mCTConfig.setNetworkBinding(this.mCurIface) == 0)
                                continue;
                            scheduleTimeConfigReconnect();
                        }
                        return;
                    }
                    if ((localObject1 == null) && (this.mCurIface != null))
                    {
                        Log.e(TAG, "Unbinding common time service.");
                        this.mCurIface = null;
                        continue;
                    }
                }
                finally
                {
                }
                if ((localObject1 != null) && (this.mCurIface != null) && (!localObject1.equals(this.mCurIface)))
                {
                    String str1 = TAG;
                    Object[] arrayOfObject1 = new Object[2];
                    arrayOfObject1[0] = this.mCurIface;
                    arrayOfObject1[1] = localObject1;
                    Log.e(str1, String.format("Switching common time service binding from %s to %s.", arrayOfObject1));
                    this.mCurIface = localObject1;
                    continue;
                }
                j = 0;
                continue;
                int k = BASE_SERVER_PRIO;
                continue;
                if (NO_INTERFACE_TIMEOUT < 0)
                    continue;
                this.mNoInterfaceHandler.removeCallbacks(this.mNoInterfaceRunnable);
                if (this.mCurIface != null)
                    continue;
                this.mNoInterfaceHandler.postDelayed(this.mNoInterfaceRunnable, NO_INTERFACE_TIMEOUT);
                continue;
                if (i1 > i)
                    continue;
            }
            label435: n++;
            continue;
            label441: i3++;
        }
    }

    private void scheduleTimeConfigReconnect()
    {
        cleanupTimeConfig();
        String str = TAG;
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = Integer.valueOf(5000);
        Log.w(str, String.format("Native service died, will reconnect in %d mSec", arrayOfObject));
        this.mReconnectHandler.postDelayed(this.mReconnectRunnable, 5000L);
    }

    // ERROR //
    protected void dump(java.io.FileDescriptor paramFileDescriptor, java.io.PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 139	com/android/server/CommonTimeManagementService:mContext	Landroid/content/Context;
        //     4: ldc_w 273
        //     7: invokevirtual 278	android/content/Context:checkCallingOrSelfPermission	(Ljava/lang/String;)I
        //     10: ifeq +42 -> 52
        //     13: iconst_2
        //     14: anewarray 113	java/lang/Object
        //     17: astore 16
        //     19: aload 16
        //     21: iconst_0
        //     22: invokestatic 281	android/os/Binder:getCallingPid	()I
        //     25: invokestatic 262	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     28: aastore
        //     29: aload 16
        //     31: iconst_1
        //     32: invokestatic 284	android/os/Binder:getCallingUid	()I
        //     35: invokestatic 262	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     38: aastore
        //     39: aload_2
        //     40: ldc_w 286
        //     43: aload 16
        //     45: invokestatic 241	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //     48: invokevirtual 292	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     51: return
        //     52: aload_0
        //     53: getfield 118	com/android/server/CommonTimeManagementService:mDetectedAtStartup	Z
        //     56: ifne +13 -> 69
        //     59: aload_2
        //     60: ldc_w 294
        //     63: invokevirtual 292	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     66: goto -15 -> 51
        //     69: aload_0
        //     70: getfield 116	com/android/server/CommonTimeManagementService:mLock	Ljava/lang/Object;
        //     73: astore 4
        //     75: aload 4
        //     77: monitorenter
        //     78: aload_2
        //     79: ldc_w 296
        //     82: invokevirtual 292	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     85: iconst_1
        //     86: anewarray 113	java/lang/Object
        //     89: astore 6
        //     91: aload_0
        //     92: getfield 162	com/android/server/CommonTimeManagementService:mCTConfig	Landroid/os/CommonTimeConfig;
        //     95: ifnonnull +203 -> 298
        //     98: ldc_w 298
        //     101: astore 7
        //     103: aload 6
        //     105: iconst_0
        //     106: aload 7
        //     108: aastore
        //     109: aload_2
        //     110: ldc_w 300
        //     113: aload 6
        //     115: invokestatic 241	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //     118: invokevirtual 292	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     121: iconst_1
        //     122: anewarray 113	java/lang/Object
        //     125: astore 8
        //     127: aload_0
        //     128: getfield 181	com/android/server/CommonTimeManagementService:mCurIface	Ljava/lang/String;
        //     131: ifnonnull +175 -> 306
        //     134: ldc_w 302
        //     137: astore 9
        //     139: aload 8
        //     141: iconst_0
        //     142: aload 9
        //     144: aastore
        //     145: aload_2
        //     146: ldc_w 304
        //     149: aload 8
        //     151: invokestatic 241	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //     154: invokevirtual 292	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     157: iconst_1
        //     158: anewarray 113	java/lang/Object
        //     161: astore 10
        //     163: getstatic 86	com/android/server/CommonTimeManagementService:ALLOW_WIFI	Z
        //     166: ifeq +157 -> 323
        //     169: ldc_w 306
        //     172: astore 11
        //     174: aload 10
        //     176: iconst_0
        //     177: aload 11
        //     179: aastore
        //     180: aload_2
        //     181: ldc_w 308
        //     184: aload 10
        //     186: invokestatic 241	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //     189: invokevirtual 292	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     192: iconst_1
        //     193: anewarray 113	java/lang/Object
        //     196: astore 12
        //     198: getstatic 84	com/android/server/CommonTimeManagementService:AUTO_DISABLE	Z
        //     201: ifeq +114 -> 315
        //     204: ldc_w 306
        //     207: astore 13
        //     209: aload 12
        //     211: iconst_0
        //     212: aload 13
        //     214: aastore
        //     215: aload_2
        //     216: ldc_w 310
        //     219: aload 12
        //     221: invokestatic 241	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //     224: invokevirtual 292	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     227: iconst_1
        //     228: anewarray 113	java/lang/Object
        //     231: astore 14
        //     233: aload 14
        //     235: iconst_0
        //     236: aload_0
        //     237: getfield 120	com/android/server/CommonTimeManagementService:mEffectivePrio	B
        //     240: invokestatic 315	java/lang/Byte:valueOf	(B)Ljava/lang/Byte;
        //     243: aastore
        //     244: aload_2
        //     245: ldc_w 317
        //     248: aload 14
        //     250: invokestatic 241	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //     253: invokevirtual 292	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     256: iconst_1
        //     257: anewarray 113	java/lang/Object
        //     260: astore 15
        //     262: aload 15
        //     264: iconst_0
        //     265: getstatic 89	com/android/server/CommonTimeManagementService:NO_INTERFACE_TIMEOUT	I
        //     268: invokestatic 262	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     271: aastore
        //     272: aload_2
        //     273: ldc_w 319
        //     276: aload 15
        //     278: invokestatic 241	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //     281: invokevirtual 292	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     284: aload 4
        //     286: monitorexit
        //     287: goto -236 -> 51
        //     290: astore 5
        //     292: aload 4
        //     294: monitorexit
        //     295: aload 5
        //     297: athrow
        //     298: ldc_w 321
        //     301: astore 7
        //     303: goto -200 -> 103
        //     306: aload_0
        //     307: getfield 181	com/android/server/CommonTimeManagementService:mCurIface	Ljava/lang/String;
        //     310: astore 9
        //     312: goto -173 -> 139
        //     315: ldc_w 323
        //     318: astore 13
        //     320: goto -111 -> 209
        //     323: ldc_w 323
        //     326: astore 11
        //     328: goto -154 -> 174
        //
        // Exception table:
        //     from	to	target	type
        //     78	295	290	finally
        //     298	320	290	finally
    }

    void systemReady()
    {
        if (ServiceManager.checkService("common_time.config") == null)
            Log.i(TAG, "No common time service detected on this platform.    Common time services will be unavailable.");
        while (true)
        {
            return;
            this.mDetectedAtStartup = true;
            this.mNetMgr = INetworkManagementService.Stub.asInterface(ServiceManager.getService("network_management"));
            try
            {
                this.mNetMgr.registerObserver(this.mIfaceObserver);
                label51: IntentFilter localIntentFilter = new IntentFilter();
                localIntentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
                this.mContext.registerReceiver(this.mConnectivityMangerObserver, localIntentFilter);
                connectToTimeConfig();
            }
            catch (RemoteException localRemoteException)
            {
                break label51;
            }
        }
    }

    private static class InterfaceScoreRule
    {
        public final String mPrefix;
        public final byte mScore;

        public InterfaceScoreRule(String paramString, byte paramByte)
        {
            this.mPrefix = paramString;
            this.mScore = paramByte;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.CommonTimeManagementService
 * JD-Core Version:        0.6.2
 */