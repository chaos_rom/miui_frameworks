package com.android.server;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.NetworkInfo.DetailedState;
import android.net.NetworkInfo.State;
import android.net.TrafficStats;
import android.net.wifi.IWifiManager.Stub;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiStateMachine;
import android.net.wifi.WifiWatchdogStateMachine;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Process;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.os.WorkSource;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.provider.Settings.System;
import android.util.Slog;
import com.android.internal.app.IBatteryStats;
import com.android.internal.util.AsyncChannel;
import com.android.server.am.BatteryStatsService;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class WifiService extends IWifiManager.Stub
{
    private static final String ACTION_DEVICE_IDLE = "com.android.server.WifiManager.action.DEVICE_IDLE";
    private static final boolean DBG = false;
    private static final long DEFAULT_IDLE_MS = 900000L;
    private static final int ICON_NETWORKS_AVAILABLE = 17302807;
    private static final int IDLE_REQUEST = 0;
    private static final int NUM_SCANS_BEFORE_ACTUALLY_SCANNING = 3;
    private static final int POLL_TRAFFIC_STATS_INTERVAL_MSECS = 1000;
    private static final String TAG = "WifiService";
    private static final int WIFI_DISABLED = 0;
    private static final int WIFI_DISABLED_AIRPLANE_ON = 3;
    private static final int WIFI_ENABLED = 1;
    private static final int WIFI_ENABLED_AIRPLANE_OVERRIDE = 2;
    private final long NOTIFICATION_REPEAT_DELAY_MS;
    private AtomicBoolean mAirplaneModeOn = new AtomicBoolean(false);
    private AlarmManager mAlarmManager;
    private AsyncServiceHandler mAsyncServiceHandler;
    private final IBatteryStats mBatteryStats;
    private List<AsyncChannel> mClients = new ArrayList();
    private Context mContext;
    private int mDataActivity;
    private boolean mDeviceIdle;
    private boolean mEmergencyCallbackMode = false;
    private boolean mEnableTrafficStatsPoll = false;
    private int mFullHighPerfLocksAcquired;
    private int mFullHighPerfLocksReleased;
    private int mFullLocksAcquired;
    private int mFullLocksReleased;
    private PendingIntent mIdleIntent;
    private String mInterfaceName;
    private boolean mIsReceiverRegistered = false;
    private final LockList mLocks = new LockList(null);
    private int mMulticastDisabled;
    private int mMulticastEnabled;
    private final List<Multicaster> mMulticasters = new ArrayList();
    NetworkInfo mNetworkInfo = new NetworkInfo(1, 0, "WIFI", "");
    private Notification mNotification;
    private boolean mNotificationEnabled;
    private NotificationEnabledSettingObserver mNotificationEnabledSettingObserver;
    private long mNotificationRepeatTime;
    private boolean mNotificationShown;
    private int mNumScansSinceNetworkStateChange;
    private AtomicInteger mPersistWifiState = new AtomicInteger(0);
    private int mPluggedType;
    private final BroadcastReceiver mReceiver = new BroadcastReceiver()
    {
        private boolean shouldDeviceStayAwake(int paramAnonymousInt1, int paramAnonymousInt2)
        {
            if ((paramAnonymousInt1 & paramAnonymousInt2) != 0);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        private boolean shouldWifiStayAwake(int paramAnonymousInt1, int paramAnonymousInt2)
        {
            int i = 1;
            int j = Settings.System.getInt(WifiService.this.mContext.getContentResolver(), "wifi_sleep_policy", 2);
            if (j == 2);
            while (true)
            {
                return i;
                if ((j != i) || (paramAnonymousInt2 == 0))
                    i = shouldDeviceStayAwake(paramAnonymousInt1, paramAnonymousInt2);
            }
        }

        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            String str = paramAnonymousIntent.getAction();
            long l1 = Settings.Secure.getLong(WifiService.this.mContext.getContentResolver(), "wifi_idle_ms", 900000L);
            int i = Settings.System.getInt(WifiService.this.mContext.getContentResolver(), "stay_on_while_plugged_in", 0);
            if (str.equals("android.intent.action.SCREEN_ON"))
            {
                WifiService.this.mAlarmManager.cancel(WifiService.this.mIdleIntent);
                WifiService.access$1802(WifiService.this, false);
                WifiService.this.evaluateTrafficStatsPolling();
                WifiService.this.setDeviceIdleAndUpdateWifi(false);
            }
            while (true)
            {
                return;
                if (str.equals("android.intent.action.SCREEN_OFF"))
                {
                    WifiService.access$1802(WifiService.this, true);
                    WifiService.this.evaluateTrafficStatsPolling();
                    if (!shouldWifiStayAwake(i, WifiService.this.mPluggedType))
                        if (WifiService.this.mNetworkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED)
                            WifiService.this.mAlarmManager.set(0, l1 + System.currentTimeMillis(), WifiService.this.mIdleIntent);
                        else
                            WifiService.this.setDeviceIdleAndUpdateWifi(true);
                }
                else if (str.equals("com.android.server.WifiManager.action.DEVICE_IDLE"))
                {
                    WifiService.this.setDeviceIdleAndUpdateWifi(true);
                }
                else
                {
                    if (str.equals("android.intent.action.BATTERY_CHANGED"))
                    {
                        int k = paramAnonymousIntent.getIntExtra("plugged", 0);
                        if ((WifiService.this.mScreenOff) && (shouldWifiStayAwake(i, WifiService.this.mPluggedType)) && (!shouldWifiStayAwake(i, k)))
                        {
                            long l2 = l1 + System.currentTimeMillis();
                            WifiService.this.mAlarmManager.set(0, l2, WifiService.this.mIdleIntent);
                        }
                        if (k == 0);
                        synchronized (WifiService.this.mScanCount)
                        {
                            WifiService.this.mScanCount.clear();
                            WifiService.access$2002(WifiService.this, k);
                        }
                    }
                    if (str.equals("android.bluetooth.adapter.action.CONNECTION_STATE_CHANGED"))
                    {
                        int j = paramAnonymousIntent.getIntExtra("android.bluetooth.adapter.extra.CONNECTION_STATE", 0);
                        WifiService.this.mWifiStateMachine.sendBluetoothAdapterStateChange(j);
                    }
                    else if (str.equals("android.intent.action.EMERGENCY_CALLBACK_MODE_CHANGED"))
                    {
                        WifiService.access$2202(WifiService.this, paramAnonymousIntent.getBooleanExtra("phoneinECMState", false));
                        WifiService.this.updateWifiState();
                    }
                }
            }
        }
    };
    private long mRxPkts;
    private HashMap<Integer, Integer> mScanCount = new HashMap();
    private int mScanLocksAcquired;
    private int mScanLocksReleased;
    private boolean mScreenOff;
    private final WorkSource mTmpWorkSource = new WorkSource();
    private int mTrafficStatsPollToken = 0;
    private long mTxPkts;
    private boolean mWifiEnabled;
    private final WifiStateMachine mWifiStateMachine;
    private AsyncChannel mWifiStateMachineChannel;
    WifiStateMachineHandler mWifiStateMachineHandler;
    private WifiWatchdogStateMachine mWifiWatchdogStateMachine;

    WifiService(Context paramContext)
    {
        this.mContext = paramContext;
        this.mInterfaceName = SystemProperties.get("wifi.interface", "wlan0");
        this.mWifiStateMachine = new WifiStateMachine(this.mContext, this.mInterfaceName);
        this.mWifiStateMachine.enableRssiPolling(true);
        this.mBatteryStats = BatteryStatsService.getService();
        this.mAlarmManager = ((AlarmManager)this.mContext.getSystemService("alarm"));
        Intent localIntent = new Intent("com.android.server.WifiManager.action.DEVICE_IDLE", null);
        this.mIdleIntent = PendingIntent.getBroadcast(this.mContext, 0, localIntent, 0);
        this.mContext.registerReceiver(new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                WifiService.this.mAirplaneModeOn.set(WifiService.this.isAirplaneModeOn());
                WifiService.this.handleAirplaneModeToggled(WifiService.this.mAirplaneModeOn.get());
                WifiService.this.updateWifiState();
            }
        }
        , new IntentFilter("android.intent.action.AIRPLANE_MODE"));
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        localIntentFilter.addAction("android.net.wifi.STATE_CHANGE");
        localIntentFilter.addAction("android.net.wifi.SCAN_RESULTS");
        this.mContext.registerReceiver(new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                int i = 1;
                if (paramAnonymousIntent.getAction().equals("android.net.wifi.WIFI_STATE_CHANGED"))
                {
                    int k = paramAnonymousIntent.getIntExtra("wifi_state", i);
                    WifiService localWifiService = WifiService.this;
                    if (k == 3)
                    {
                        WifiService.access$1202(localWifiService, i);
                        WifiService.this.resetNotification();
                    }
                }
                while (true)
                {
                    return;
                    int j = 0;
                    break;
                    if (paramAnonymousIntent.getAction().equals("android.net.wifi.STATE_CHANGE"))
                    {
                        WifiService.this.mNetworkInfo = ((NetworkInfo)paramAnonymousIntent.getParcelableExtra("networkInfo"));
                        switch (WifiService.4.$SwitchMap$android$net$NetworkInfo$DetailedState[WifiService.this.mNetworkInfo.getDetailedState().ordinal()])
                        {
                        default:
                            break;
                        case 1:
                        case 2:
                            WifiService.this.evaluateTrafficStatsPolling();
                            WifiService.this.resetNotification();
                            break;
                        }
                    }
                    else if (paramAnonymousIntent.getAction().equals("android.net.wifi.SCAN_RESULTS"))
                    {
                        WifiService.this.checkAndSetNotification();
                    }
                }
            }
        }
        , localIntentFilter);
        HandlerThread localHandlerThread = new HandlerThread("WifiService");
        localHandlerThread.start();
        this.mAsyncServiceHandler = new AsyncServiceHandler(localHandlerThread.getLooper());
        this.mWifiStateMachineHandler = new WifiStateMachineHandler(localHandlerThread.getLooper());
        this.NOTIFICATION_REPEAT_DELAY_MS = (1000L * Settings.Secure.getInt(paramContext.getContentResolver(), "wifi_networks_available_repeat_delay", 900));
        this.mNotificationEnabledSettingObserver = new NotificationEnabledSettingObserver(new Handler());
        this.mNotificationEnabledSettingObserver.register();
    }

    // ERROR //
    private boolean acquireWifiLockLocked(WifiLock paramWifiLock)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 140	com/android/server/WifiService:mLocks	Lcom/android/server/WifiService$LockList;
        //     4: aload_1
        //     5: invokestatic 428	com/android/server/WifiService$LockList:access$3300	(Lcom/android/server/WifiService$LockList;Lcom/android/server/WifiService$WifiLock;)V
        //     8: invokestatic 434	android/os/Binder:clearCallingIdentity	()J
        //     11: lstore_2
        //     12: aload_0
        //     13: aload_1
        //     14: invokespecial 438	com/android/server/WifiService:noteAcquireWifiLock	(Lcom/android/server/WifiService$WifiLock;)V
        //     17: aload_1
        //     18: getfield 441	com/android/server/WifiService$DeathRecipient:mMode	I
        //     21: tableswitch	default:+27 -> 48, 1:+45->66, 2:+80->101, 3:+58->79
        //     49: invokespecial 444	com/android/server/WifiService:reportStartWorkSource	()V
        //     52: aload_0
        //     53: invokespecial 329	com/android/server/WifiService:updateWifiState	()V
        //     56: iconst_1
        //     57: istore 6
        //     59: lload_2
        //     60: invokestatic 448	android/os/Binder:restoreCallingIdentity	(J)V
        //     63: iload 6
        //     65: ireturn
        //     66: aload_0
        //     67: iconst_1
        //     68: aload_0
        //     69: getfield 388	com/android/server/WifiService:mFullLocksAcquired	I
        //     72: iadd
        //     73: putfield 388	com/android/server/WifiService:mFullLocksAcquired	I
        //     76: goto -28 -> 48
        //     79: aload_0
        //     80: iconst_1
        //     81: aload_0
        //     82: getfield 380	com/android/server/WifiService:mFullHighPerfLocksAcquired	I
        //     85: iadd
        //     86: putfield 380	com/android/server/WifiService:mFullHighPerfLocksAcquired	I
        //     89: goto -41 -> 48
        //     92: astore 4
        //     94: lload_2
        //     95: invokestatic 448	android/os/Binder:restoreCallingIdentity	(J)V
        //     98: aload 4
        //     100: athrow
        //     101: aload_0
        //     102: iconst_1
        //     103: aload_0
        //     104: getfield 450	com/android/server/WifiService:mScanLocksAcquired	I
        //     107: iadd
        //     108: putfield 450	com/android/server/WifiService:mScanLocksAcquired	I
        //     111: goto -63 -> 48
        //     114: astore 5
        //     116: iconst_0
        //     117: istore 6
        //     119: goto -60 -> 59
        //
        // Exception table:
        //     from	to	target	type
        //     12	56	92	finally
        //     66	89	92	finally
        //     101	111	92	finally
        //     12	56	114	android/os/RemoteException
        //     66	89	114	android/os/RemoteException
        //     101	111	114	android/os/RemoteException
    }

    private void checkAndSetNotification()
    {
        if (!this.mNotificationEnabled);
        while (true)
        {
            return;
            NetworkInfo.State localState = this.mNetworkInfo.getState();
            if ((localState == NetworkInfo.State.DISCONNECTED) || (localState == NetworkInfo.State.UNKNOWN))
            {
                List localList = this.mWifiStateMachine.syncGetScanResultsList();
                if (localList != null)
                {
                    int i = 0;
                    for (int j = -1 + localList.size(); j >= 0; j--)
                    {
                        ScanResult localScanResult = (ScanResult)localList.get(j);
                        if ((localScanResult.capabilities != null) && (localScanResult.capabilities.equals("[ESS]")))
                            i++;
                    }
                    if (i > 0)
                    {
                        int k = 1 + this.mNumScansSinceNetworkStateChange;
                        this.mNumScansSinceNetworkStateChange = k;
                        if (k < 3)
                            continue;
                        setNotificationVisible(true, i, false, 0);
                    }
                }
            }
            else
            {
                setNotificationVisible(false, 0, false, 0);
            }
        }
    }

    private void enforceAccessPermission()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE", "WifiService");
    }

    private void enforceChangePermission()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CHANGE_WIFI_STATE", "WifiService");
    }

    private void enforceMulticastChangePermission()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CHANGE_WIFI_MULTICAST_STATE", "WifiService");
    }

    private void evaluateTrafficStatsPolling()
    {
        if ((this.mNetworkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED) && (!this.mScreenOff));
        for (Message localMessage = Message.obtain(this.mAsyncServiceHandler, 151573, 1, 0); ; localMessage = Message.obtain(this.mAsyncServiceHandler, 151573, 0, 0))
        {
            localMessage.sendToTarget();
            return;
        }
    }

    private int getPersistedWifiState()
    {
        int i = 0;
        ContentResolver localContentResolver = this.mContext.getContentResolver();
        try
        {
            int j = Settings.Secure.getInt(localContentResolver, "wifi_on");
            i = j;
            return i;
        }
        catch (Settings.SettingNotFoundException localSettingNotFoundException)
        {
            while (true)
                Settings.Secure.putInt(localContentResolver, "wifi_on", 0);
        }
    }

    private void handleAirplaneModeToggled(boolean paramBoolean)
    {
        if (paramBoolean)
            if (this.mWifiEnabled)
                persistWifiState(3);
        while (true)
        {
            return;
            if ((testAndClearWifiSavedState()) || (this.mPersistWifiState.get() == 2))
                persistWifiState(1);
        }
    }

    private void handleWifiToggled(boolean paramBoolean)
    {
        int i;
        if ((this.mAirplaneModeOn.get()) && (isAirplaneToggleable()))
        {
            i = 1;
            if (!paramBoolean)
                break label46;
            if (i == 0)
                break label38;
            persistWifiState(2);
        }
        while (true)
        {
            return;
            i = 0;
            break;
            label38: persistWifiState(1);
            continue;
            label46: persistWifiState(0);
        }
    }

    private boolean isAirplaneModeOn()
    {
        int i = 1;
        if ((isAirplaneSensitive()) && (Settings.System.getInt(this.mContext.getContentResolver(), "airplane_mode_on", 0) == i));
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private boolean isAirplaneSensitive()
    {
        String str = Settings.System.getString(this.mContext.getContentResolver(), "airplane_mode_radios");
        if ((str == null) || (str.contains("wifi")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isAirplaneToggleable()
    {
        String str = Settings.System.getString(this.mContext.getContentResolver(), "airplane_mode_toggleable_radios");
        if ((str != null) && (str.contains("wifi")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void noteAcquireWifiLock(WifiLock paramWifiLock)
        throws RemoteException
    {
        switch (paramWifiLock.mMode)
        {
        default:
        case 1:
        case 3:
        case 2:
        }
        while (true)
        {
            return;
            this.mBatteryStats.noteFullWifiLockAcquiredFromSource(paramWifiLock.mWorkSource);
            continue;
            this.mBatteryStats.noteScanWifiLockAcquiredFromSource(paramWifiLock.mWorkSource);
        }
    }

    private void noteReleaseWifiLock(WifiLock paramWifiLock)
        throws RemoteException
    {
        switch (paramWifiLock.mMode)
        {
        default:
        case 1:
        case 3:
        case 2:
        }
        while (true)
        {
            return;
            this.mBatteryStats.noteFullWifiLockReleasedFromSource(paramWifiLock.mWorkSource);
            continue;
            this.mBatteryStats.noteScanWifiLockReleasedFromSource(paramWifiLock.mWorkSource);
        }
    }

    private void notifyOnDataActivity()
    {
        long l1 = this.mTxPkts;
        long l2 = this.mRxPkts;
        int i = 0;
        this.mTxPkts = TrafficStats.getTxPackets(this.mInterfaceName);
        this.mRxPkts = TrafficStats.getRxPackets(this.mInterfaceName);
        if ((l1 > 0L) || (l2 > 0L))
        {
            long l3 = this.mTxPkts - l1;
            long l4 = this.mRxPkts - l2;
            if (l3 > 0L)
                i = 0x0 | 0x2;
            if (l4 > 0L)
                i |= 1;
            if ((i != this.mDataActivity) && (!this.mScreenOff))
            {
                this.mDataActivity = i;
                Iterator localIterator = this.mClients.iterator();
                while (localIterator.hasNext())
                    ((AsyncChannel)localIterator.next()).sendMessage(1, this.mDataActivity);
            }
        }
    }

    private void persistWifiState(int paramInt)
    {
        ContentResolver localContentResolver = this.mContext.getContentResolver();
        this.mPersistWifiState.set(paramInt);
        Settings.Secure.putInt(localContentResolver, "wifi_on", paramInt);
    }

    private void registerForBroadcasts()
    {
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("android.intent.action.SCREEN_ON");
        localIntentFilter.addAction("android.intent.action.SCREEN_OFF");
        localIntentFilter.addAction("android.intent.action.BATTERY_CHANGED");
        localIntentFilter.addAction("com.android.server.WifiManager.action.DEVICE_IDLE");
        localIntentFilter.addAction("android.bluetooth.adapter.action.CONNECTION_STATE_CHANGED");
        localIntentFilter.addAction("android.intent.action.EMERGENCY_CALLBACK_MODE_CHANGED");
        this.mContext.registerReceiver(this.mReceiver, localIntentFilter);
    }

    // ERROR //
    private boolean releaseWifiLockLocked(IBinder paramIBinder)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 140	com/android/server/WifiService:mLocks	Lcom/android/server/WifiService$LockList;
        //     4: aload_1
        //     5: invokestatic 646	com/android/server/WifiService$LockList:access$3500	(Lcom/android/server/WifiService$LockList;Landroid/os/IBinder;)Lcom/android/server/WifiService$WifiLock;
        //     8: astore_2
        //     9: aload_2
        //     10: ifnull +61 -> 71
        //     13: iconst_1
        //     14: istore_3
        //     15: invokestatic 434	android/os/Binder:clearCallingIdentity	()J
        //     18: lstore 4
        //     20: iload_3
        //     21: ifeq +39 -> 60
        //     24: aload_0
        //     25: aload_2
        //     26: invokespecial 648	com/android/server/WifiService:noteReleaseWifiLock	(Lcom/android/server/WifiService$WifiLock;)V
        //     29: aload_2
        //     30: getfield 441	com/android/server/WifiService$DeathRecipient:mMode	I
        //     33: tableswitch	default:+27 -> 60, 1:+43->76, 2:+79->112, 3:+56->89
        //     61: invokespecial 329	com/android/server/WifiService:updateWifiState	()V
        //     64: lload 4
        //     66: invokestatic 448	android/os/Binder:restoreCallingIdentity	(J)V
        //     69: iload_3
        //     70: ireturn
        //     71: iconst_0
        //     72: istore_3
        //     73: goto -58 -> 15
        //     76: aload_0
        //     77: iconst_1
        //     78: aload_0
        //     79: getfield 391	com/android/server/WifiService:mFullLocksReleased	I
        //     82: iadd
        //     83: putfield 391	com/android/server/WifiService:mFullLocksReleased	I
        //     86: goto -26 -> 60
        //     89: aload_0
        //     90: iconst_1
        //     91: aload_0
        //     92: getfield 384	com/android/server/WifiService:mFullHighPerfLocksReleased	I
        //     95: iadd
        //     96: putfield 384	com/android/server/WifiService:mFullHighPerfLocksReleased	I
        //     99: goto -39 -> 60
        //     102: astore 6
        //     104: lload 4
        //     106: invokestatic 448	android/os/Binder:restoreCallingIdentity	(J)V
        //     109: aload 6
        //     111: athrow
        //     112: aload_0
        //     113: iconst_1
        //     114: aload_0
        //     115: getfield 650	com/android/server/WifiService:mScanLocksReleased	I
        //     118: iadd
        //     119: putfield 650	com/android/server/WifiService:mScanLocksReleased	I
        //     122: goto -62 -> 60
        //     125: astore 7
        //     127: goto -63 -> 64
        //
        // Exception table:
        //     from	to	target	type
        //     24	64	102	finally
        //     76	99	102	finally
        //     112	122	102	finally
        //     24	64	125	android/os/RemoteException
        //     76	99	125	android/os/RemoteException
        //     112	122	125	android/os/RemoteException
    }

    // ERROR //
    private void removeMulticasterLocked(int paramInt1, int paramInt2)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 150	com/android/server/WifiService:mMulticasters	Ljava/util/List;
        //     4: iload_1
        //     5: invokeinterface 653 2 0
        //     10: checkcast 17	com/android/server/WifiService$Multicaster
        //     13: astore_3
        //     14: aload_3
        //     15: ifnull +7 -> 22
        //     18: aload_3
        //     19: invokevirtual 656	com/android/server/WifiService$Multicaster:unlinkDeathRecipient	()V
        //     22: aload_0
        //     23: getfield 150	com/android/server/WifiService:mMulticasters	Ljava/util/List;
        //     26: invokeinterface 473 1 0
        //     31: ifne +10 -> 41
        //     34: aload_0
        //     35: getfield 214	com/android/server/WifiService:mWifiStateMachine	Landroid/net/wifi/WifiStateMachine;
        //     38: invokevirtual 659	android/net/wifi/WifiStateMachine:startFilteringMulticastV4Packets	()V
        //     41: invokestatic 434	android/os/Binder:clearCallingIdentity	()J
        //     44: invokestatic 665	java/lang/Long:valueOf	(J)Ljava/lang/Long;
        //     47: astore 4
        //     49: aload_0
        //     50: getfield 225	com/android/server/WifiService:mBatteryStats	Lcom/android/internal/app/IBatteryStats;
        //     53: iload_2
        //     54: invokeinterface 668 2 0
        //     59: aload 4
        //     61: invokevirtual 671	java/lang/Long:longValue	()J
        //     64: lstore 6
        //     66: lload 6
        //     68: invokestatic 448	android/os/Binder:restoreCallingIdentity	(J)V
        //     71: return
        //     72: astore 8
        //     74: aload 4
        //     76: invokevirtual 671	java/lang/Long:longValue	()J
        //     79: invokestatic 448	android/os/Binder:restoreCallingIdentity	(J)V
        //     82: aload 8
        //     84: athrow
        //     85: astore 5
        //     87: aload 4
        //     89: invokevirtual 671	java/lang/Long:longValue	()J
        //     92: lstore 6
        //     94: goto -28 -> 66
        //
        // Exception table:
        //     from	to	target	type
        //     49	59	72	finally
        //     49	59	85	android/os/RemoteException
    }

    /** @deprecated */
    private void reportStartWorkSource()
    {
        try
        {
            this.mTmpWorkSource.clear();
            if (this.mDeviceIdle)
                for (int i = 0; i < this.mLocks.mList.size(); i++)
                    this.mTmpWorkSource.add(((WifiLock)this.mLocks.mList.get(i)).mWorkSource);
            this.mWifiStateMachine.updateBatteryWorkSource(this.mTmpWorkSource);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private void resetNotification()
    {
        this.mNotificationRepeatTime = 0L;
        this.mNumScansSinceNetworkStateChange = 0;
        setNotificationVisible(false, 0, false, 0);
    }

    private void setDeviceIdleAndUpdateWifi(boolean paramBoolean)
    {
        this.mDeviceIdle = paramBoolean;
        reportStartWorkSource();
        updateWifiState();
    }

    private void setNotificationVisible(boolean paramBoolean1, int paramInt1, boolean paramBoolean2, int paramInt2)
    {
        if ((!paramBoolean1) && (!this.mNotificationShown) && (!paramBoolean2));
        NotificationManager localNotificationManager;
        do
        {
            return;
            localNotificationManager = (NotificationManager)this.mContext.getSystemService("notification");
            if (!paramBoolean1)
                break;
        }
        while (System.currentTimeMillis() < this.mNotificationRepeatTime);
        if (this.mNotification == null)
        {
            this.mNotification = new Notification();
            this.mNotification.when = 0L;
            this.mNotification.icon = 17302807;
            this.mNotification.flags = 16;
            this.mNotification.contentIntent = PendingIntent.getActivity(this.mContext, 0, new Intent("android.net.wifi.PICK_WIFI_NETWORK"), 0);
        }
        CharSequence localCharSequence1 = this.mContext.getResources().getQuantityText(18022417, paramInt1);
        CharSequence localCharSequence2 = this.mContext.getResources().getQuantityText(18022418, paramInt1);
        this.mNotification.tickerText = localCharSequence1;
        this.mNotification.setLatestEventInfo(this.mContext, localCharSequence1, localCharSequence2, this.mNotification.contentIntent);
        this.mNotificationRepeatTime = (System.currentTimeMillis() + this.NOTIFICATION_REPEAT_DELAY_MS);
        localNotificationManager.notify(17302807, this.mNotification);
        while (true)
        {
            this.mNotificationShown = paramBoolean1;
            break;
            localNotificationManager.cancel(17302807);
        }
    }

    private boolean shouldWifiBeEnabled()
    {
        boolean bool = true;
        if (this.mAirplaneModeOn.get())
            if (this.mPersistWifiState.get() != 2);
        while (true)
        {
            return bool;
            bool = false;
            continue;
            if (this.mPersistWifiState.get() == 0)
                bool = false;
        }
    }

    private boolean testAndClearWifiSavedState()
    {
        int i = 1;
        ContentResolver localContentResolver = this.mContext.getContentResolver();
        int j = 0;
        try
        {
            j = Settings.Secure.getInt(localContentResolver, "wifi_saved_state");
            if (j == i)
                Settings.Secure.putInt(localContentResolver, "wifi_saved_state", 0);
            label34: if (j == i);
            while (true)
            {
                return i;
                i = 0;
            }
        }
        catch (Settings.SettingNotFoundException localSettingNotFoundException)
        {
            break label34;
        }
    }

    private void updateWifiState()
    {
        boolean bool1 = true;
        boolean bool2 = this.mLocks.hasLocks();
        int i = 1;
        int j;
        boolean bool3;
        if (this.mEmergencyCallbackMode)
        {
            j = 0;
            if (bool2)
                i = this.mLocks.getStrongestLockMode();
            if ((!this.mDeviceIdle) && (i == 2))
                i = 1;
            if (this.mAirplaneModeOn.get())
                this.mWifiStateMachine.setWifiApEnabled(null, false);
            if (!shouldWifiBeEnabled())
                break label191;
            if (j == 0)
                break label176;
            reportStartWorkSource();
            this.mWifiStateMachine.setWifiEnabled(bool1);
            WifiStateMachine localWifiStateMachine1 = this.mWifiStateMachine;
            if (i != 2)
                break label165;
            bool3 = bool1;
            label105: localWifiStateMachine1.setScanOnlyMode(bool3);
            this.mWifiStateMachine.setDriverStart(bool1, this.mEmergencyCallbackMode);
            WifiStateMachine localWifiStateMachine2 = this.mWifiStateMachine;
            if (i != 3)
                break label171;
            label135: localWifiStateMachine2.setHighPerfModeEnabled(bool1);
        }
        while (true)
        {
            return;
            if ((!this.mDeviceIdle) || (bool2));
            for (j = bool1; ; j = 0)
                break;
            label165: bool3 = false;
            break label105;
            label171: bool1 = false;
            break label135;
            label176: this.mWifiStateMachine.setDriverStart(false, this.mEmergencyCallbackMode);
            continue;
            label191: this.mWifiStateMachine.setWifiEnabled(false);
        }
    }

    // ERROR //
    public void acquireMulticastLock(IBinder paramIBinder, String paramString)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 784	com/android/server/WifiService:enforceMulticastChangePermission	()V
        //     4: aload_0
        //     5: getfield 150	com/android/server/WifiService:mMulticasters	Ljava/util/List;
        //     8: astore_3
        //     9: aload_3
        //     10: monitorenter
        //     11: aload_0
        //     12: iconst_1
        //     13: aload_0
        //     14: getfield 786	com/android/server/WifiService:mMulticastEnabled	I
        //     17: iadd
        //     18: putfield 786	com/android/server/WifiService:mMulticastEnabled	I
        //     21: aload_0
        //     22: getfield 150	com/android/server/WifiService:mMulticasters	Ljava/util/List;
        //     25: new 17	com/android/server/WifiService$Multicaster
        //     28: dup
        //     29: aload_0
        //     30: aload_2
        //     31: aload_1
        //     32: invokespecial 789	com/android/server/WifiService$Multicaster:<init>	(Lcom/android/server/WifiService;Ljava/lang/String;Landroid/os/IBinder;)V
        //     35: invokeinterface 791 2 0
        //     40: pop
        //     41: aload_0
        //     42: getfield 214	com/android/server/WifiService:mWifiStateMachine	Landroid/net/wifi/WifiStateMachine;
        //     45: invokevirtual 794	android/net/wifi/WifiStateMachine:stopFilteringMulticastV4Packets	()V
        //     48: aload_3
        //     49: monitorexit
        //     50: invokestatic 797	android/os/Binder:getCallingUid	()I
        //     53: istore 6
        //     55: invokestatic 434	android/os/Binder:clearCallingIdentity	()J
        //     58: invokestatic 665	java/lang/Long:valueOf	(J)Ljava/lang/Long;
        //     61: astore 7
        //     63: aload_0
        //     64: getfield 225	com/android/server/WifiService:mBatteryStats	Lcom/android/internal/app/IBatteryStats;
        //     67: iload 6
        //     69: invokeinterface 800 2 0
        //     74: aload 7
        //     76: invokevirtual 671	java/lang/Long:longValue	()J
        //     79: lstore 9
        //     81: lload 9
        //     83: invokestatic 448	android/os/Binder:restoreCallingIdentity	(J)V
        //     86: return
        //     87: astore 4
        //     89: aload_3
        //     90: monitorexit
        //     91: aload 4
        //     93: athrow
        //     94: astore 11
        //     96: aload 7
        //     98: invokevirtual 671	java/lang/Long:longValue	()J
        //     101: invokestatic 448	android/os/Binder:restoreCallingIdentity	(J)V
        //     104: aload 11
        //     106: athrow
        //     107: astore 8
        //     109: aload 7
        //     111: invokevirtual 671	java/lang/Long:longValue	()J
        //     114: lstore 9
        //     116: goto -35 -> 81
        //
        // Exception table:
        //     from	to	target	type
        //     11	50	87	finally
        //     89	91	87	finally
        //     63	74	94	finally
        //     63	74	107	android/os/RemoteException
    }

    public boolean acquireWifiLock(IBinder paramIBinder, int paramInt, String paramString, WorkSource paramWorkSource)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.WAKE_LOCK", null);
        boolean bool;
        if ((paramInt != 1) && (paramInt != 2) && (paramInt != 3))
        {
            Slog.e("WifiService", "Illegal argument, lockMode= " + paramInt);
            bool = false;
        }
        while (true)
        {
            return bool;
            if ((paramWorkSource != null) && (paramWorkSource.size() == 0))
                paramWorkSource = null;
            if (paramWorkSource != null)
                enforceWakeSourcePermission(Binder.getCallingUid(), Binder.getCallingPid());
            if (paramWorkSource == null)
                paramWorkSource = new WorkSource(Binder.getCallingUid());
            WifiLock localWifiLock = new WifiLock(paramInt, paramString, paramIBinder, paramWorkSource);
            synchronized (this.mLocks)
            {
                bool = acquireWifiLockLocked(localWifiLock);
            }
        }
    }

    public int addOrUpdateNetwork(WifiConfiguration paramWifiConfiguration)
    {
        enforceChangePermission();
        if (this.mWifiStateMachineChannel != null);
        for (int i = this.mWifiStateMachine.syncAddOrUpdateNetwork(this.mWifiStateMachineChannel, paramWifiConfiguration); ; i = -1)
        {
            return i;
            Slog.e("WifiService", "mWifiStateMachineChannel is not initialized");
        }
    }

    public void addToBlacklist(String paramString)
    {
        enforceChangePermission();
        this.mWifiStateMachine.addToBlacklist(paramString);
    }

    public void checkAndStartWifi()
    {
        this.mAirplaneModeOn.set(isAirplaneModeOn());
        this.mPersistWifiState.set(getPersistedWifiState());
        boolean bool;
        StringBuilder localStringBuilder;
        if ((shouldWifiBeEnabled()) || (testAndClearWifiSavedState()))
        {
            bool = true;
            localStringBuilder = new StringBuilder().append("WifiService starting up with Wi-Fi ");
            if (!bool)
                break label101;
        }
        label101: for (String str = "enabled"; ; str = "disabled")
        {
            Slog.i("WifiService", str);
            if (bool)
                setWifiEnabled(bool);
            this.mWifiWatchdogStateMachine = WifiWatchdogStateMachine.makeWifiWatchdogStateMachine(this.mContext);
            return;
            bool = false;
            break;
        }
    }

    public void clearBlacklist()
    {
        enforceChangePermission();
        this.mWifiStateMachine.clearBlacklist();
    }

    public boolean disableNetwork(int paramInt)
    {
        enforceChangePermission();
        if (this.mWifiStateMachineChannel != null);
        for (boolean bool = this.mWifiStateMachine.syncDisableNetwork(this.mWifiStateMachineChannel, paramInt); ; bool = false)
        {
            return bool;
            Slog.e("WifiService", "mWifiStateMachineChannel is not initialized");
        }
    }

    public void disconnect()
    {
        enforceChangePermission();
        this.mWifiStateMachine.disconnectCommand();
    }

    // ERROR //
    protected void dump(java.io.FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 195	com/android/server/WifiService:mContext	Landroid/content/Context;
        //     4: ldc_w 894
        //     7: invokevirtual 898	android/content/Context:checkCallingOrSelfPermission	(Ljava/lang/String;)I
        //     10: ifeq +42 -> 52
        //     13: aload_2
        //     14: new 806	java/lang/StringBuilder
        //     17: dup
        //     18: invokespecial 807	java/lang/StringBuilder:<init>	()V
        //     21: ldc_w 900
        //     24: invokevirtual 813	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     27: invokestatic 830	android/os/Binder:getCallingPid	()I
        //     30: invokevirtual 816	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     33: ldc_w 902
        //     36: invokevirtual 813	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     39: invokestatic 797	android/os/Binder:getCallingUid	()I
        //     42: invokevirtual 816	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     45: invokevirtual 820	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     48: invokevirtual 907	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     51: return
        //     52: aload_2
        //     53: new 806	java/lang/StringBuilder
        //     56: dup
        //     57: invokespecial 807	java/lang/StringBuilder:<init>	()V
        //     60: ldc_w 909
        //     63: invokevirtual 813	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     66: aload_0
        //     67: getfield 214	com/android/server/WifiService:mWifiStateMachine	Landroid/net/wifi/WifiStateMachine;
        //     70: invokevirtual 912	android/net/wifi/WifiStateMachine:syncGetWifiStateByName	()Ljava/lang/String;
        //     73: invokevirtual 813	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     76: invokevirtual 820	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     79: invokevirtual 907	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     82: aload_2
        //     83: new 806	java/lang/StringBuilder
        //     86: dup
        //     87: invokespecial 807	java/lang/StringBuilder:<init>	()V
        //     90: ldc_w 914
        //     93: invokevirtual 813	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     96: aload_0
        //     97: getfield 195	com/android/server/WifiService:mContext	Landroid/content/Context;
        //     100: invokevirtual 297	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
        //     103: ldc_w 916
        //     106: iconst_0
        //     107: invokestatic 562	android/provider/Settings$System:getInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)I
        //     110: invokevirtual 816	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     113: invokevirtual 820	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     116: invokevirtual 907	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     119: aload_2
        //     120: invokevirtual 918	java/io/PrintWriter:println	()V
        //     123: aload_2
        //     124: ldc_w 920
        //     127: invokevirtual 907	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     130: aload_2
        //     131: aload_0
        //     132: getfield 214	com/android/server/WifiService:mWifiStateMachine	Landroid/net/wifi/WifiStateMachine;
        //     135: invokevirtual 923	java/io/PrintWriter:println	(Ljava/lang/Object;)V
        //     138: aload_2
        //     139: invokevirtual 918	java/io/PrintWriter:println	()V
        //     142: aload_2
        //     143: ldc_w 925
        //     146: invokevirtual 907	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     149: aload_0
        //     150: getfield 214	com/android/server/WifiService:mWifiStateMachine	Landroid/net/wifi/WifiStateMachine;
        //     153: invokevirtual 467	android/net/wifi/WifiStateMachine:syncGetScanResultsList	()Ljava/util/List;
        //     156: astore 4
        //     158: aload 4
        //     160: ifnull +140 -> 300
        //     163: aload 4
        //     165: invokeinterface 473 1 0
        //     170: ifeq +130 -> 300
        //     173: aload_2
        //     174: ldc_w 927
        //     177: invokevirtual 907	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     180: aload 4
        //     182: invokeinterface 614 1 0
        //     187: astore 9
        //     189: aload 9
        //     191: invokeinterface 619 1 0
        //     196: ifeq +104 -> 300
        //     199: aload 9
        //     201: invokeinterface 623 1 0
        //     206: checkcast 478	android/net/wifi/ScanResult
        //     209: astore 10
        //     211: iconst_5
        //     212: anewarray 929	java/lang/Object
        //     215: astore 11
        //     217: aload 11
        //     219: iconst_0
        //     220: aload 10
        //     222: getfield 932	android/net/wifi/ScanResult:BSSID	Ljava/lang/String;
        //     225: aastore
        //     226: aload 11
        //     228: iconst_1
        //     229: aload 10
        //     231: getfield 935	android/net/wifi/ScanResult:frequency	I
        //     234: invokestatic 940	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     237: aastore
        //     238: aload 11
        //     240: iconst_2
        //     241: aload 10
        //     243: getfield 943	android/net/wifi/ScanResult:level	I
        //     246: invokestatic 940	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     249: aastore
        //     250: aload 11
        //     252: iconst_3
        //     253: aload 10
        //     255: getfield 481	android/net/wifi/ScanResult:capabilities	Ljava/lang/String;
        //     258: aastore
        //     259: aload 10
        //     261: getfield 946	android/net/wifi/ScanResult:SSID	Ljava/lang/String;
        //     264: ifnonnull +26 -> 290
        //     267: ldc 176
        //     269: astore 12
        //     271: aload 11
        //     273: iconst_4
        //     274: aload 12
        //     276: aastore
        //     277: aload_2
        //     278: ldc_w 948
        //     281: aload 11
        //     283: invokevirtual 952	java/io/PrintWriter:printf	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;
        //     286: pop
        //     287: goto -98 -> 189
        //     290: aload 10
        //     292: getfield 946	android/net/wifi/ScanResult:SSID	Ljava/lang/String;
        //     295: astore 12
        //     297: goto -26 -> 271
        //     300: aload_2
        //     301: invokevirtual 918	java/io/PrintWriter:println	()V
        //     304: aload_2
        //     305: new 806	java/lang/StringBuilder
        //     308: dup
        //     309: invokespecial 807	java/lang/StringBuilder:<init>	()V
        //     312: ldc_w 954
        //     315: invokevirtual 813	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     318: aload_0
        //     319: getfield 388	com/android/server/WifiService:mFullLocksAcquired	I
        //     322: invokevirtual 816	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     325: ldc_w 956
        //     328: invokevirtual 813	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     331: aload_0
        //     332: getfield 380	com/android/server/WifiService:mFullHighPerfLocksAcquired	I
        //     335: invokevirtual 816	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     338: ldc_w 958
        //     341: invokevirtual 813	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     344: aload_0
        //     345: getfield 450	com/android/server/WifiService:mScanLocksAcquired	I
        //     348: invokevirtual 816	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     351: ldc_w 960
        //     354: invokevirtual 813	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     357: invokevirtual 820	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     360: invokevirtual 907	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     363: aload_2
        //     364: new 806	java/lang/StringBuilder
        //     367: dup
        //     368: invokespecial 807	java/lang/StringBuilder:<init>	()V
        //     371: ldc_w 962
        //     374: invokevirtual 813	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     377: aload_0
        //     378: getfield 391	com/android/server/WifiService:mFullLocksReleased	I
        //     381: invokevirtual 816	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     384: ldc_w 956
        //     387: invokevirtual 813	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     390: aload_0
        //     391: getfield 384	com/android/server/WifiService:mFullHighPerfLocksReleased	I
        //     394: invokevirtual 816	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     397: ldc_w 958
        //     400: invokevirtual 813	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     403: aload_0
        //     404: getfield 650	com/android/server/WifiService:mScanLocksReleased	I
        //     407: invokevirtual 816	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     410: ldc_w 960
        //     413: invokevirtual 813	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     416: invokevirtual 820	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     419: invokevirtual 907	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     422: aload_2
        //     423: invokevirtual 918	java/io/PrintWriter:println	()V
        //     426: aload_2
        //     427: ldc_w 964
        //     430: invokevirtual 907	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     433: aload_0
        //     434: getfield 140	com/android/server/WifiService:mLocks	Lcom/android/server/WifiService$LockList;
        //     437: aload_2
        //     438: invokestatic 968	com/android/server/WifiService$LockList:access$2600	(Lcom/android/server/WifiService$LockList;Ljava/io/PrintWriter;)V
        //     441: aload_2
        //     442: ldc_w 970
        //     445: invokevirtual 907	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     448: aload_0
        //     449: getfield 145	com/android/server/WifiService:mScanCount	Ljava/util/HashMap;
        //     452: astore 5
        //     454: aload 5
        //     456: monitorenter
        //     457: aload_0
        //     458: getfield 145	com/android/server/WifiService:mScanCount	Ljava/util/HashMap;
        //     461: invokevirtual 974	java/util/HashMap:keySet	()Ljava/util/Set;
        //     464: invokeinterface 977 1 0
        //     469: astore 7
        //     471: aload 7
        //     473: invokeinterface 619 1 0
        //     478: ifeq +75 -> 553
        //     481: aload 7
        //     483: invokeinterface 623 1 0
        //     488: checkcast 937	java/lang/Integer
        //     491: invokevirtual 980	java/lang/Integer:intValue	()I
        //     494: istore 8
        //     496: aload_2
        //     497: new 806	java/lang/StringBuilder
        //     500: dup
        //     501: invokespecial 807	java/lang/StringBuilder:<init>	()V
        //     504: ldc_w 982
        //     507: invokevirtual 813	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     510: iload 8
        //     512: invokevirtual 816	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     515: ldc_w 984
        //     518: invokevirtual 813	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     521: aload_0
        //     522: getfield 145	com/android/server/WifiService:mScanCount	Ljava/util/HashMap;
        //     525: iload 8
        //     527: invokestatic 940	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     530: invokevirtual 987	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     533: invokevirtual 990	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     536: invokevirtual 820	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     539: invokevirtual 907	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     542: goto -71 -> 471
        //     545: astore 6
        //     547: aload 5
        //     549: monitorexit
        //     550: aload 6
        //     552: athrow
        //     553: aload 5
        //     555: monitorexit
        //     556: aload_2
        //     557: invokevirtual 918	java/io/PrintWriter:println	()V
        //     560: aload_2
        //     561: ldc_w 992
        //     564: invokevirtual 907	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     567: aload_0
        //     568: getfield 875	com/android/server/WifiService:mWifiWatchdogStateMachine	Landroid/net/wifi/WifiWatchdogStateMachine;
        //     571: aload_2
        //     572: invokevirtual 995	android/net/wifi/WifiWatchdogStateMachine:dump	(Ljava/io/PrintWriter;)V
        //     575: aload_2
        //     576: ldc_w 997
        //     579: invokevirtual 907	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     582: aload_0
        //     583: getfield 214	com/android/server/WifiService:mWifiStateMachine	Landroid/net/wifi/WifiStateMachine;
        //     586: aload_1
        //     587: aload_2
        //     588: aload_3
        //     589: invokevirtual 999	android/net/wifi/WifiStateMachine:dump	(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
        //     592: goto -541 -> 51
        //
        // Exception table:
        //     from	to	target	type
        //     457	550	545	finally
        //     553	556	545	finally
    }

    public boolean enableNetwork(int paramInt, boolean paramBoolean)
    {
        enforceChangePermission();
        if (this.mWifiStateMachineChannel != null);
        for (boolean bool = this.mWifiStateMachine.syncEnableNetwork(this.mWifiStateMachineChannel, paramInt, paramBoolean); ; bool = false)
        {
            return bool;
            Slog.e("WifiService", "mWifiStateMachineChannel is not initialized");
        }
    }

    void enforceWakeSourcePermission(int paramInt1, int paramInt2)
    {
        if (paramInt1 == Process.myUid());
        while (true)
        {
            return;
            this.mContext.enforcePermission("android.permission.UPDATE_DEVICE_STATS", paramInt2, paramInt1, null);
        }
    }

    public String getConfigFile()
    {
        enforceAccessPermission();
        return this.mWifiStateMachine.getConfigFile();
    }

    public List<WifiConfiguration> getConfiguredNetworks()
    {
        enforceAccessPermission();
        if (this.mWifiStateMachineChannel != null);
        for (List localList = this.mWifiStateMachine.syncGetConfiguredNetworks(this.mWifiStateMachineChannel); ; localList = null)
        {
            return localList;
            Slog.e("WifiService", "mWifiStateMachineChannel is not initialized");
        }
    }

    public WifiInfo getConnectionInfo()
    {
        enforceAccessPermission();
        return this.mWifiStateMachine.syncRequestConnectionInfo();
    }

    public DhcpInfo getDhcpInfo()
    {
        enforceAccessPermission();
        return this.mWifiStateMachine.syncGetDhcpInfo();
    }

    public int getFrequencyBand()
    {
        enforceAccessPermission();
        return this.mWifiStateMachine.getFrequencyBand();
    }

    public List<ScanResult> getScanResults()
    {
        enforceAccessPermission();
        return this.mWifiStateMachine.syncGetScanResultsList();
    }

    public WifiConfiguration getWifiApConfiguration()
    {
        enforceAccessPermission();
        return this.mWifiStateMachine.syncGetWifiApConfiguration();
    }

    public int getWifiApEnabledState()
    {
        enforceAccessPermission();
        return this.mWifiStateMachine.syncGetWifiApState();
    }

    public int getWifiEnabledState()
    {
        enforceAccessPermission();
        return this.mWifiStateMachine.syncGetWifiState();
    }

    public Messenger getWifiServiceMessenger()
    {
        enforceAccessPermission();
        enforceChangePermission();
        return new Messenger(this.mAsyncServiceHandler);
    }

    public Messenger getWifiStateMachineMessenger()
    {
        enforceAccessPermission();
        enforceChangePermission();
        return this.mWifiStateMachine.getMessenger();
    }

    public void initializeMulticastFiltering()
    {
        enforceMulticastChangePermission();
        synchronized (this.mMulticasters)
        {
            if (this.mMulticasters.size() == 0)
                this.mWifiStateMachine.startFilteringMulticastV4Packets();
        }
    }

    public boolean isDualBandSupported()
    {
        return this.mContext.getResources().getBoolean(17891342);
    }

    public boolean isMulticastEnabled()
    {
        enforceAccessPermission();
        while (true)
        {
            synchronized (this.mMulticasters)
            {
                if (this.mMulticasters.size() > 0)
                {
                    bool = true;
                    return bool;
                }
            }
            boolean bool = false;
        }
    }

    public boolean pingSupplicant()
    {
        enforceAccessPermission();
        if (this.mWifiStateMachineChannel != null);
        for (boolean bool = this.mWifiStateMachine.syncPingSupplicant(this.mWifiStateMachineChannel); ; bool = false)
        {
            return bool;
            Slog.e("WifiService", "mWifiStateMachineChannel is not initialized");
        }
    }

    public void reassociate()
    {
        enforceChangePermission();
        this.mWifiStateMachine.reassociateCommand();
    }

    public void reconnect()
    {
        enforceChangePermission();
        this.mWifiStateMachine.reconnectCommand();
    }

    public void releaseMulticastLock()
    {
        enforceMulticastChangePermission();
        int i = Binder.getCallingUid();
        while (true)
        {
            int j;
            synchronized (this.mMulticasters)
            {
                this.mMulticastDisabled = (1 + this.mMulticastDisabled);
                j = -1 + this.mMulticasters.size();
                if (j >= 0)
                {
                    Multicaster localMulticaster = (Multicaster)this.mMulticasters.get(j);
                    if ((localMulticaster != null) && (localMulticaster.getUid() == i))
                        removeMulticasterLocked(j, i);
                }
                else
                {
                    return;
                }
            }
            j--;
        }
    }

    public boolean releaseWifiLock(IBinder paramIBinder)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.WAKE_LOCK", null);
        synchronized (this.mLocks)
        {
            boolean bool = releaseWifiLockLocked(paramIBinder);
            return bool;
        }
    }

    public boolean removeNetwork(int paramInt)
    {
        enforceChangePermission();
        if (this.mWifiStateMachineChannel != null);
        for (boolean bool = this.mWifiStateMachine.syncRemoveNetwork(this.mWifiStateMachineChannel, paramInt); ; bool = false)
        {
            return bool;
            Slog.e("WifiService", "mWifiStateMachineChannel is not initialized");
        }
    }

    public boolean saveConfiguration()
    {
        enforceChangePermission();
        if (this.mWifiStateMachineChannel != null);
        for (boolean bool = this.mWifiStateMachine.syncSaveConfig(this.mWifiStateMachineChannel); ; bool = false)
        {
            return bool;
            Slog.e("WifiService", "mWifiStateMachineChannel is not initialized");
        }
    }

    public void setCountryCode(String paramString, boolean paramBoolean)
    {
        Slog.i("WifiService", "WifiService trying to set country code to " + paramString + " with persist set to " + paramBoolean);
        enforceChangePermission();
        this.mWifiStateMachine.setCountryCode(paramString, paramBoolean);
    }

    public void setFrequencyBand(int paramInt, boolean paramBoolean)
    {
        enforceChangePermission();
        if (!isDualBandSupported());
        while (true)
        {
            return;
            Slog.i("WifiService", "WifiService trying to set frequency band to " + paramInt + " with persist set to " + paramBoolean);
            this.mWifiStateMachine.setFrequencyBand(paramInt, paramBoolean);
        }
    }

    public void setWifiApConfiguration(WifiConfiguration paramWifiConfiguration)
    {
        enforceChangePermission();
        if (paramWifiConfiguration == null);
        while (true)
        {
            return;
            this.mWifiStateMachine.setWifiApConfiguration(paramWifiConfiguration);
        }
    }

    public void setWifiApEnabled(WifiConfiguration paramWifiConfiguration, boolean paramBoolean)
    {
        enforceChangePermission();
        this.mWifiStateMachine.setWifiApEnabled(paramWifiConfiguration, paramBoolean);
    }

    /** @deprecated */
    public boolean setWifiEnabled(boolean paramBoolean)
    {
        try
        {
            enforceChangePermission();
            Slog.d("WifiService", "setWifiEnabled: " + paramBoolean + " pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid());
            if (paramBoolean)
                reportStartWorkSource();
            this.mWifiStateMachine.setWifiEnabled(paramBoolean);
            long l = Binder.clearCallingIdentity();
            handleWifiToggled(paramBoolean);
            Binder.restoreCallingIdentity(l);
            if (paramBoolean)
                if (!this.mIsReceiverRegistered)
                {
                    registerForBroadcasts();
                    this.mIsReceiverRegistered = true;
                }
            while (true)
            {
                return true;
                if (this.mIsReceiverRegistered)
                {
                    this.mContext.unregisterReceiver(this.mReceiver);
                    this.mIsReceiverRegistered = false;
                }
            }
        }
        finally
        {
        }
    }

    public void startScan(boolean paramBoolean)
    {
        enforceChangePermission();
        int i = Binder.getCallingUid();
        int j = 0;
        synchronized (this.mScanCount)
        {
            if (this.mScanCount.containsKey(Integer.valueOf(i)))
                j = ((Integer)this.mScanCount.get(Integer.valueOf(i))).intValue();
            this.mScanCount.put(Integer.valueOf(i), Integer.valueOf(j + 1));
            this.mWifiStateMachine.startScan(paramBoolean);
            return;
        }
    }

    public void startWifi()
    {
        enforceChangePermission();
        this.mWifiStateMachine.setDriverStart(true, this.mEmergencyCallbackMode);
        this.mWifiStateMachine.reconnectCommand();
    }

    public void stopWifi()
    {
        enforceChangePermission();
        this.mWifiStateMachine.setDriverStart(false, this.mEmergencyCallbackMode);
    }

    // ERROR //
    public void updateWifiLockWorkSource(IBinder paramIBinder, WorkSource paramWorkSource)
    {
        // Byte code:
        //     0: invokestatic 797	android/os/Binder:getCallingUid	()I
        //     3: istore_3
        //     4: invokestatic 830	android/os/Binder:getCallingPid	()I
        //     7: istore 4
        //     9: aload_2
        //     10: ifnull +12 -> 22
        //     13: aload_2
        //     14: invokevirtual 827	android/os/WorkSource:size	()I
        //     17: ifne +5 -> 22
        //     20: aconst_null
        //     21: astore_2
        //     22: aload_2
        //     23: ifnull +10 -> 33
        //     26: aload_0
        //     27: iload_3
        //     28: iload 4
        //     30: invokevirtual 833	com/android/server/WifiService:enforceWakeSourcePermission	(II)V
        //     33: invokestatic 434	android/os/Binder:clearCallingIdentity	()J
        //     36: lstore 5
        //     38: aload_0
        //     39: getfield 140	com/android/server/WifiService:mLocks	Lcom/android/server/WifiService$LockList;
        //     42: astore 9
        //     44: aload 9
        //     46: monitorenter
        //     47: aload_0
        //     48: getfield 140	com/android/server/WifiService:mLocks	Lcom/android/server/WifiService$LockList;
        //     51: aload_1
        //     52: invokestatic 1155	com/android/server/WifiService$LockList:access$3400	(Lcom/android/server/WifiService$LockList;Landroid/os/IBinder;)I
        //     55: istore 11
        //     57: iload 11
        //     59: ifge +30 -> 89
        //     62: new 1157	java/lang/IllegalArgumentException
        //     65: dup
        //     66: ldc_w 1159
        //     69: invokespecial 1160	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     72: athrow
        //     73: astore 10
        //     75: aload 9
        //     77: monitorexit
        //     78: aload 10
        //     80: athrow
        //     81: astore 8
        //     83: lload 5
        //     85: invokestatic 448	android/os/Binder:restoreCallingIdentity	(J)V
        //     88: return
        //     89: aload_0
        //     90: getfield 140	com/android/server/WifiService:mLocks	Lcom/android/server/WifiService$LockList;
        //     93: invokestatic 680	com/android/server/WifiService$LockList:access$2300	(Lcom/android/server/WifiService$LockList;)Ljava/util/List;
        //     96: iload 11
        //     98: invokeinterface 476 2 0
        //     103: checkcast 26	com/android/server/WifiService$WifiLock
        //     106: astore 12
        //     108: aload_0
        //     109: aload 12
        //     111: invokespecial 648	com/android/server/WifiService:noteReleaseWifiLock	(Lcom/android/server/WifiService$WifiLock;)V
        //     114: aload_2
        //     115: ifnull +32 -> 147
        //     118: new 185	android/os/WorkSource
        //     121: dup
        //     122: aload_2
        //     123: invokespecial 1162	android/os/WorkSource:<init>	(Landroid/os/WorkSource;)V
        //     126: astore 13
        //     128: aload 12
        //     130: aload 13
        //     132: putfield 579	com/android/server/WifiService$DeathRecipient:mWorkSource	Landroid/os/WorkSource;
        //     135: aload_0
        //     136: aload 12
        //     138: invokespecial 438	com/android/server/WifiService:noteAcquireWifiLock	(Lcom/android/server/WifiService$WifiLock;)V
        //     141: aload 9
        //     143: monitorexit
        //     144: goto -61 -> 83
        //     147: new 185	android/os/WorkSource
        //     150: dup
        //     151: iload_3
        //     152: invokespecial 834	android/os/WorkSource:<init>	(I)V
        //     155: astore 13
        //     157: goto -29 -> 128
        //     160: astore 7
        //     162: lload 5
        //     164: invokestatic 448	android/os/Binder:restoreCallingIdentity	(J)V
        //     167: aload 7
        //     169: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     47	78	73	finally
        //     89	157	73	finally
        //     38	47	81	android/os/RemoteException
        //     78	81	81	android/os/RemoteException
        //     38	47	160	finally
        //     78	81	160	finally
    }

    private class NotificationEnabledSettingObserver extends ContentObserver
    {
        public NotificationEnabledSettingObserver(Handler arg2)
        {
            super();
        }

        private boolean getValue()
        {
            int i = 1;
            if (Settings.Secure.getInt(WifiService.this.mContext.getContentResolver(), "wifi_networks_available_notification_on", i) == i);
            while (true)
            {
                return i;
                int j = 0;
            }
        }

        public void onChange(boolean paramBoolean)
        {
            super.onChange(paramBoolean);
            WifiService.access$3802(WifiService.this, getValue());
            WifiService.this.resetNotification();
        }

        public void register()
        {
            WifiService.this.mContext.getContentResolver().registerContentObserver(Settings.Secure.getUriFor("wifi_networks_available_notification_on"), true, this);
            WifiService.access$3802(WifiService.this, getValue());
        }
    }

    private class Multicaster extends WifiService.DeathRecipient
    {
        Multicaster(String paramIBinder, IBinder arg3)
        {
            super(Binder.getCallingUid(), paramIBinder, localIBinder, null);
        }

        public void binderDied()
        {
            Slog.e("WifiService", "Multicaster binderDied");
            synchronized (WifiService.this.mMulticasters)
            {
                int i = WifiService.this.mMulticasters.indexOf(this);
                if (i != -1)
                    WifiService.this.removeMulticasterLocked(i, this.mMode);
                return;
            }
        }

        public int getUid()
        {
            return this.mMode;
        }

        public String toString()
        {
            return "Multicaster{" + this.mTag + " binder=" + this.mBinder + "}";
        }
    }

    private abstract class DeathRecipient
        implements IBinder.DeathRecipient
    {
        IBinder mBinder;
        int mMode;
        String mTag;
        WorkSource mWorkSource;

        DeathRecipient(int paramString, String paramIBinder, IBinder paramWorkSource, WorkSource arg5)
        {
            this.mTag = paramIBinder;
            this.mMode = paramString;
            this.mBinder = paramWorkSource;
            Object localObject;
            this.mWorkSource = localObject;
            try
            {
                this.mBinder.linkToDeath(this, 0);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    binderDied();
            }
        }

        void unlinkDeathRecipient()
        {
            this.mBinder.unlinkToDeath(this, 0);
        }
    }

    private class LockList
    {
        private List<WifiService.WifiLock> mList = new ArrayList();

        private LockList()
        {
        }

        private void addLock(WifiService.WifiLock paramWifiLock)
        {
            if (findLockByBinder(paramWifiLock.mBinder) < 0)
                this.mList.add(paramWifiLock);
        }

        private void dump(PrintWriter paramPrintWriter)
        {
            Iterator localIterator = this.mList.iterator();
            while (localIterator.hasNext())
            {
                WifiService.WifiLock localWifiLock = (WifiService.WifiLock)localIterator.next();
                paramPrintWriter.print("        ");
                paramPrintWriter.println(localWifiLock);
            }
        }

        private int findLockByBinder(IBinder paramIBinder)
        {
            int i = -1 + this.mList.size();
            if (i >= 0)
                if (((WifiService.WifiLock)this.mList.get(i)).mBinder != paramIBinder);
            while (true)
            {
                return i;
                i--;
                break;
                i = -1;
            }
        }

        /** @deprecated */
        private int getStrongestLockMode()
        {
            int i = 1;
            try
            {
                boolean bool = this.mList.isEmpty();
                if (bool);
                while (true)
                {
                    return i;
                    if (WifiService.this.mFullHighPerfLocksAcquired > WifiService.this.mFullHighPerfLocksReleased)
                    {
                        i = 3;
                    }
                    else
                    {
                        int j = WifiService.this.mFullLocksAcquired;
                        int k = WifiService.this.mFullLocksReleased;
                        if (j <= k)
                            i = 2;
                    }
                }
            }
            finally
            {
            }
        }

        /** @deprecated */
        private boolean hasLocks()
        {
            try
            {
                boolean bool1 = this.mList.isEmpty();
                if (!bool1)
                {
                    bool2 = true;
                    return bool2;
                }
                boolean bool2 = false;
            }
            finally
            {
            }
        }

        private WifiService.WifiLock removeLock(IBinder paramIBinder)
        {
            int i = findLockByBinder(paramIBinder);
            WifiService.WifiLock localWifiLock;
            if (i >= 0)
            {
                localWifiLock = (WifiService.WifiLock)this.mList.remove(i);
                localWifiLock.unlinkDeathRecipient();
            }
            while (true)
            {
                return localWifiLock;
                localWifiLock = null;
            }
        }
    }

    private class WifiLock extends WifiService.DeathRecipient
    {
        WifiLock(int paramString, String paramIBinder, IBinder paramWorkSource, WorkSource arg5)
        {
            super(paramString, paramIBinder, paramWorkSource, localWorkSource);
        }

        public void binderDied()
        {
            synchronized (WifiService.this.mLocks)
            {
                WifiService.this.releaseWifiLockLocked(this.mBinder);
                return;
            }
        }

        public String toString()
        {
            return "WifiLock{" + this.mTag + " type=" + this.mMode + " binder=" + this.mBinder + "}";
        }
    }

    private class WifiStateMachineHandler extends Handler
    {
        private AsyncChannel mWsmChannel = new AsyncChannel();

        WifiStateMachineHandler(Looper arg2)
        {
            super();
            this.mWsmChannel.connect(WifiService.this.mContext, this, WifiService.this.mWifiStateMachine.getHandler());
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
                Slog.d("WifiService", "WifiStateMachineHandler.handleMessage ignoring msg=" + paramMessage);
            case 69632:
            case 69636:
            }
            while (true)
            {
                return;
                if (paramMessage.arg1 == 0)
                {
                    WifiService.access$702(WifiService.this, this.mWsmChannel);
                }
                else
                {
                    Slog.e("WifiService", "WifiStateMachine connection failure, error=" + paramMessage.arg1);
                    WifiService.access$702(WifiService.this, null);
                    continue;
                    Slog.e("WifiService", "WifiStateMachine channel lost, msg.arg1 =" + paramMessage.arg1);
                    WifiService.access$702(WifiService.this, null);
                    this.mWsmChannel.connect(WifiService.this.mContext, this, WifiService.this.mWifiStateMachine.getHandler());
                }
            }
        }
    }

    private class AsyncServiceHandler extends Handler
    {
        AsyncServiceHandler(Looper arg2)
        {
            super();
        }

        public void handleMessage(Message paramMessage)
        {
            int i = 1;
            switch (paramMessage.what)
            {
            default:
                Slog.d("WifiService", "WifiServicehandler.handleMessage ignoring msg=" + paramMessage);
            case 69632:
            case 69636:
            case 69633:
            case 151573:
            case 151574:
            case 151553:
            case 151559:
            case 151556:
            case 151562:
            case 151566:
            case 151569:
            }
            while (true)
            {
                return;
                if (paramMessage.arg1 == 0)
                {
                    Slog.d("WifiService", "New client listening to asynchronous messages");
                    WifiService.this.mClients.add((AsyncChannel)paramMessage.obj);
                }
                else
                {
                    Slog.e("WifiService", "Client connection failure, error=" + paramMessage.arg1);
                    continue;
                    if (paramMessage.arg1 == 2)
                        Slog.d("WifiService", "Send failed, client connection lost");
                    while (true)
                    {
                        WifiService.this.mClients.remove((AsyncChannel)paramMessage.obj);
                        break;
                        Slog.d("WifiService", "Client connection lost with reason: " + paramMessage.arg1);
                    }
                    new AsyncChannel().connect(WifiService.this.mContext, this, paramMessage.replyTo);
                    continue;
                    WifiService localWifiService = WifiService.this;
                    if (paramMessage.arg1 == i);
                    while (true)
                    {
                        WifiService.access$302(localWifiService, i);
                        WifiService.access$408(WifiService.this);
                        if (!WifiService.this.mEnableTrafficStatsPoll)
                            break;
                        WifiService.this.notifyOnDataActivity();
                        sendMessageDelayed(Message.obtain(this, 151574, WifiService.this.mTrafficStatsPollToken, 0), 1000L);
                        break;
                        int j = 0;
                    }
                    if (paramMessage.arg1 == WifiService.this.mTrafficStatsPollToken)
                    {
                        WifiService.this.notifyOnDataActivity();
                        sendMessageDelayed(Message.obtain(this, 151574, WifiService.this.mTrafficStatsPollToken, 0), 1000L);
                        continue;
                        WifiService.this.mWifiStateMachine.sendMessage(Message.obtain(paramMessage));
                        continue;
                        WifiService.this.mWifiStateMachine.sendMessage(Message.obtain(paramMessage));
                        continue;
                        WifiService.this.mWifiStateMachine.sendMessage(Message.obtain(paramMessage));
                        continue;
                        WifiService.this.mWifiStateMachine.sendMessage(Message.obtain(paramMessage));
                        continue;
                        WifiService.this.mWifiStateMachine.sendMessage(Message.obtain(paramMessage));
                        continue;
                        WifiService.this.mWifiStateMachine.sendMessage(Message.obtain(paramMessage));
                    }
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.WifiService
 * JD-Core Version:        0.6.2
 */