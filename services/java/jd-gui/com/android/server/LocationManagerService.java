package com.android.server;

import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.app.PendingIntent.OnFinished;
import android.content.BroadcastReceiver;
import android.content.ContentQueryMap;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.location.Address;
import android.location.Criteria;
import android.location.GeocoderParams;
import android.location.IGpsStatusListener;
import android.location.IGpsStatusProvider;
import android.location.ILocationListener;
import android.location.ILocationListener.Stub;
import android.location.ILocationManager.Stub;
import android.location.INetInitiatedListener;
import android.location.Location;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Process;
import android.os.RemoteException;
import android.os.WorkSource;
import android.provider.Settings.Secure;
import android.util.Log;
import android.util.PrintWriterPrinter;
import android.util.Slog;
import com.android.internal.content.PackageMonitor;
import com.android.server.location.GeocoderProxy;
import com.android.server.location.GpsLocationProvider;
import com.android.server.location.LocationProviderInterface;
import com.android.server.location.LocationProviderProxy;
import com.android.server.location.MockProvider;
import com.android.server.location.PassiveProvider;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

public class LocationManagerService extends ILocationManager.Stub
    implements Runnable
{
    private static final String ACCESS_COARSE_LOCATION = "android.permission.ACCESS_COARSE_LOCATION";
    private static final String ACCESS_FINE_LOCATION = "android.permission.ACCESS_FINE_LOCATION";
    private static final String ACCESS_LOCATION_EXTRA_COMMANDS = "android.permission.ACCESS_LOCATION_EXTRA_COMMANDS";
    private static final String ACCESS_MOCK_LOCATION = "android.permission.ACCESS_MOCK_LOCATION";
    private static final String BLACKLIST_CONFIG_NAME = "locationPackagePrefixBlacklist";
    private static final String INSTALL_LOCATION_PROVIDER = "android.permission.INSTALL_LOCATION_PROVIDER";
    private static final boolean LOCAL_LOGV = false;
    private static final int MAX_PROVIDER_SCHEDULING_JITTER = 100;
    private static final int MESSAGE_LOCATION_CHANGED = 1;
    private static final int MESSAGE_PACKAGE_UPDATED = 2;
    private static final String TAG = "LocationManagerService";
    private static final String WAKELOCK_KEY = "LocationManagerService";
    private static final String WHITELIST_CONFIG_NAME = "locationPackagePrefixWhitelist";
    private static boolean sProvidersLoaded = false;
    private String[] mBlacklist = new String[0];
    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            String str = paramAnonymousIntent.getAction();
            boolean bool = str.equals("android.intent.action.QUERY_PACKAGE_RESTART");
            if ((bool) || (str.equals("android.intent.action.PACKAGE_REMOVED")) || (str.equals("android.intent.action.PACKAGE_RESTARTED")) || (str.equals("android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE")));
            while (true)
            {
                int j;
                int k;
                ArrayList localArrayList1;
                int i1;
                LocationManagerService.UpdateRecord localUpdateRecord;
                LocationManagerService.Receiver localReceiver1;
                LocationManagerService.Receiver localReceiver2;
                ArrayList localArrayList2;
                Iterator localIterator2;
                LocationManagerService.ProximityAlert localProximityAlert;
                int n;
                int m;
                NetworkInfo localNetworkInfo;
                int i2;
                LocationProviderInterface localLocationProviderInterface;
                synchronized (LocationManagerService.this.mLock)
                {
                    Object localObject3;
                    if (str.equals("android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"))
                    {
                        localObject3 = paramAnonymousIntent.getIntArrayExtra("android.intent.extra.changed_uid_list");
                        if ((localObject3 == null) || (localObject3.length == 0))
                            continue;
                    }
                    else
                    {
                        int[] arrayOfInt = new int[1];
                        arrayOfInt[0] = paramAnonymousIntent.getIntExtra("android.intent.extra.UID", -1);
                        localObject3 = arrayOfInt;
                        continue;
                    }
                    Object localObject4 = localObject3;
                    int i = localObject4.length;
                    j = 0;
                    if (j < i)
                    {
                        k = localObject4[j];
                        if (k < 0)
                            break label665;
                        localArrayList1 = null;
                        Iterator localIterator1 = LocationManagerService.this.mRecordsByProvider.values().iterator();
                        if (localIterator1.hasNext())
                        {
                            ArrayList localArrayList3 = (ArrayList)localIterator1.next();
                            i1 = -1 + localArrayList3.size();
                            if (i1 < 0)
                                continue;
                            localUpdateRecord = (LocationManagerService.UpdateRecord)localArrayList3.get(i1);
                            if ((!localUpdateRecord.mReceiver.isPendingIntent()) || (localUpdateRecord.mUid != k))
                                break label659;
                            if (bool)
                                setResultCode(-1);
                        }
                    }
                }
                label659: i1--;
                continue;
                label665: j++;
            }
        }
    };
    private final Context mContext;
    private final Set<String> mDisabledProviders = new HashSet();
    private final Set<String> mEnabledProviders = new HashSet();
    private GeocoderProxy mGeocodeProvider;
    private String mGeocodeProviderPackageName;
    LocationProviderInterface mGpsLocationProvider;
    private IGpsStatusProvider mGpsStatusProvider;
    private HashMap<String, Location> mLastKnownLocation = new HashMap();
    private HashMap<String, Long> mLastWriteTime = new HashMap();
    private LocationWorkerHandler mLocationHandler;
    private final Object mLock = new Object();
    private final HashMap<String, MockProvider> mMockProviders = new HashMap();
    private INetInitiatedListener mNetInitiatedListener;
    LocationProviderProxy mNetworkLocationProvider;
    private String mNetworkLocationProviderPackageName;
    private int mNetworkState = 1;
    private PackageManager mPackageManager;
    private final PackageMonitor mPackageMonitor = new PackageMonitor()
    {
        public void onPackageAdded(String paramAnonymousString, int paramAnonymousInt)
        {
            Message.obtain(LocationManagerService.this.mLocationHandler, 2, paramAnonymousString).sendToTarget();
        }

        public void onPackageUpdateFinished(String paramAnonymousString, int paramAnonymousInt)
        {
            Message.obtain(LocationManagerService.this.mLocationHandler, 2, paramAnonymousString).sendToTarget();
        }
    };
    private int mPendingBroadcasts;
    private final ArrayList<LocationProviderInterface> mProviders = new ArrayList();
    private final HashMap<String, LocationProviderInterface> mProvidersByName = new HashMap();
    private HashSet<ProximityAlert> mProximitiesEntered = new HashSet();
    private HashMap<PendingIntent, ProximityAlert> mProximityAlerts = new HashMap();
    private ILocationListener mProximityListener = null;
    private Receiver mProximityReceiver = null;
    private final HashMap<Object, Receiver> mReceivers = new HashMap();
    private final HashMap<String, ArrayList<UpdateRecord>> mRecordsByProvider = new HashMap();
    private ContentQueryMap mSettings;
    private final WorkSource mTmpWorkSource = new WorkSource();
    private PowerManager.WakeLock mWakeLock = null;
    private String[] mWhitelist = new String[0];

    public LocationManagerService(Context paramContext)
    {
        this.mContext = paramContext;
        Resources localResources = paramContext.getResources();
        this.mNetworkLocationProviderPackageName = localResources.getString(17039388);
        this.mGeocodeProviderPackageName = localResources.getString(17039389);
        this.mPackageMonitor.register(paramContext, null, true);
    }

    private List<String> _getAllProvidersLocked()
    {
        ArrayList localArrayList = new ArrayList(this.mProviders.size());
        for (int i = -1 + this.mProviders.size(); i >= 0; i--)
            localArrayList.add(((LocationProviderInterface)this.mProviders.get(i)).getName());
        return localArrayList;
    }

    private Location _getLastKnownLocationLocked(String paramString1, String paramString2)
    {
        Location localLocation = null;
        checkPermissionsSafe(paramString1, null);
        checkPackageName(Binder.getCallingUid(), paramString2);
        if ((LocationProviderInterface)this.mProvidersByName.get(paramString1) == null);
        while (true)
        {
            return localLocation;
            if ((isAllowedBySettingsLocked(paramString1)) && (!inBlacklist(paramString2)))
                localLocation = (Location)this.mLastKnownLocation.get(paramString1);
        }
    }

    private Bundle _getProviderInfoLocked(String paramString)
    {
        Bundle localBundle = null;
        LocationProviderInterface localLocationProviderInterface = (LocationProviderInterface)this.mProvidersByName.get(paramString);
        if (localLocationProviderInterface == null);
        while (true)
        {
            return localBundle;
            checkPermissionsSafe(paramString, null);
            localBundle = new Bundle();
            localBundle.putBoolean("network", localLocationProviderInterface.requiresNetwork());
            localBundle.putBoolean("satellite", localLocationProviderInterface.requiresSatellite());
            localBundle.putBoolean("cell", localLocationProviderInterface.requiresCell());
            localBundle.putBoolean("cost", localLocationProviderInterface.hasMonetaryCost());
            localBundle.putBoolean("altitude", localLocationProviderInterface.supportsAltitude());
            localBundle.putBoolean("speed", localLocationProviderInterface.supportsSpeed());
            localBundle.putBoolean("bearing", localLocationProviderInterface.supportsBearing());
            localBundle.putInt("power", localLocationProviderInterface.getPowerRequirement());
            localBundle.putInt("accuracy", localLocationProviderInterface.getAccuracy());
        }
    }

    private List<String> _getProvidersLocked(Criteria paramCriteria, boolean paramBoolean)
    {
        ArrayList localArrayList = new ArrayList(this.mProviders.size());
        int i = -1 + this.mProviders.size();
        if (i >= 0)
        {
            LocationProviderInterface localLocationProviderInterface = (LocationProviderInterface)this.mProviders.get(i);
            String str = localLocationProviderInterface.getName();
            if ((!isAllowedProviderSafe(str)) || ((paramBoolean) && (!isAllowedBySettingsLocked(str))));
            while (true)
            {
                i--;
                break;
                if ((paramCriteria == null) || (localLocationProviderInterface.meetsCriteria(paramCriteria)))
                    localArrayList.add(str);
            }
        }
        return localArrayList;
    }

    private boolean _isProviderEnabledLocked(String paramString)
    {
        checkPermissionsSafe(paramString, null);
        if ((LocationProviderInterface)this.mProvidersByName.get(paramString) == null);
        for (boolean bool = false; ; bool = isAllowedBySettingsLocked(paramString))
            return bool;
    }

    private void _loadProvidersLocked()
    {
        if (GpsLocationProvider.isSupported())
        {
            GpsLocationProvider localGpsLocationProvider = new GpsLocationProvider(this.mContext, this);
            this.mGpsStatusProvider = localGpsLocationProvider.getGpsStatusProvider();
            this.mNetInitiatedListener = localGpsLocationProvider.getNetInitiatedListener();
            addProvider(localGpsLocationProvider);
            this.mGpsLocationProvider = localGpsLocationProvider;
        }
        PassiveProvider localPassiveProvider = new PassiveProvider(this);
        addProvider(localPassiveProvider);
        this.mEnabledProviders.add(localPassiveProvider.getName());
        if (this.mNetworkLocationProviderPackageName != null)
        {
            String str2 = findBestPackage("com.android.location.service.NetworkLocationProvider", this.mNetworkLocationProviderPackageName);
            if (str2 != null)
            {
                this.mNetworkLocationProvider = new LocationProviderProxy(this.mContext, "network", str2, this.mLocationHandler);
                this.mNetworkLocationProviderPackageName = str2;
                addProvider(this.mNetworkLocationProvider);
            }
        }
        if (this.mGeocodeProviderPackageName != null)
        {
            String str1 = findBestPackage("com.android.location.service.GeocodeProvider", this.mGeocodeProviderPackageName);
            if (str1 != null)
            {
                this.mGeocodeProvider = new GeocoderProxy(this.mContext, str1);
                this.mGeocodeProviderPackageName = str1;
            }
        }
        updateProvidersLocked();
    }

    private void addProvider(LocationProviderInterface paramLocationProviderInterface)
    {
        this.mProviders.add(paramLocationProviderInterface);
        this.mProvidersByName.put(paramLocationProviderInterface.getName(), paramLocationProviderInterface);
    }

    private void addProximityAlertLocked(double paramDouble1, double paramDouble2, float paramFloat, long paramLong, PendingIntent paramPendingIntent, String paramString)
    {
        checkPackageName(Binder.getCallingUid(), paramString);
        if ((!isAllowedProviderSafe("gps")) || (!isAllowedProviderSafe("network")))
            throw new SecurityException("Requires ACCESS_FINE_LOCATION permission");
        if (paramLong != -1L)
            paramLong += System.currentTimeMillis();
        ProximityAlert localProximityAlert = new ProximityAlert(Binder.getCallingUid(), paramDouble1, paramDouble2, paramFloat, paramLong, paramPendingIntent, paramString);
        this.mProximityAlerts.put(paramPendingIntent, localProximityAlert);
        if (this.mProximityReceiver == null)
        {
            this.mProximityListener = new ProximityListener();
            this.mProximityReceiver = new Receiver(this.mProximityListener, paramString);
            for (int i = -1 + this.mProviders.size(); i >= 0; i--)
                requestLocationUpdatesLocked(((LocationProviderInterface)this.mProviders.get(i)).getName(), 1000L, 1.0F, false, this.mProximityReceiver);
        }
    }

    private static String arrayToString(String[] paramArrayOfString)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append('[');
        int i = 1;
        int j = paramArrayOfString.length;
        for (int k = 0; k < j; k++)
        {
            String str = paramArrayOfString[k];
            if (i == 0)
                localStringBuilder.append(',');
            i = 0;
            localStringBuilder.append(str);
        }
        localStringBuilder.append(']');
        return localStringBuilder.toString();
    }

    // ERROR //
    private LocationProviderInterface best(List<String> paramList)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 179	com/android/server/LocationManagerService:mLock	Ljava/lang/Object;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: new 169	java/util/ArrayList
        //     10: dup
        //     11: aload_1
        //     12: invokeinterface 540 1 0
        //     17: invokespecial 247	java/util/ArrayList:<init>	(I)V
        //     20: astore_3
        //     21: aload_1
        //     22: invokeinterface 544 1 0
        //     27: astore 5
        //     29: aload 5
        //     31: invokeinterface 549 1 0
        //     36: ifeq +39 -> 75
        //     39: aload 5
        //     41: invokeinterface 553 1 0
        //     46: checkcast 200	java/lang/String
        //     49: astore 17
        //     51: aload_3
        //     52: aload_0
        //     53: getfield 174	com/android/server/LocationManagerService:mProvidersByName	Ljava/util/HashMap;
        //     56: aload 17
        //     58: invokevirtual 279	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     61: invokevirtual 261	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     64: pop
        //     65: goto -36 -> 29
        //     68: astore 4
        //     70: aload_2
        //     71: monitorexit
        //     72: aload 4
        //     74: athrow
        //     75: aload_2
        //     76: monitorexit
        //     77: aload_3
        //     78: invokevirtual 244	java/util/ArrayList:size	()I
        //     81: iconst_2
        //     82: if_icmpge +16 -> 98
        //     85: aload_3
        //     86: iconst_0
        //     87: invokevirtual 251	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     90: checkcast 253	com/android/server/location/LocationProviderInterface
        //     93: astore 14
        //     95: aload 14
        //     97: areturn
        //     98: aload_3
        //     99: new 33	com/android/server/LocationManagerService$LpPowerComparator
        //     102: dup
        //     103: aload_0
        //     104: aconst_null
        //     105: invokespecial 556	com/android/server/LocationManagerService$LpPowerComparator:<init>	(Lcom/android/server/LocationManagerService;Lcom/android/server/LocationManagerService$1;)V
        //     108: invokestatic 562	java/util/Collections:sort	(Ljava/util/List;Ljava/util/Comparator;)V
        //     111: aload_3
        //     112: iconst_0
        //     113: invokevirtual 251	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     116: checkcast 253	com/android/server/location/LocationProviderInterface
        //     119: invokeinterface 338 1 0
        //     124: istore 6
        //     126: iload 6
        //     128: aload_3
        //     129: iconst_1
        //     130: invokevirtual 251	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     133: checkcast 253	com/android/server/location/LocationProviderInterface
        //     136: invokeinterface 338 1 0
        //     141: if_icmpge +16 -> 157
        //     144: aload_3
        //     145: iconst_0
        //     146: invokevirtual 251	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     149: checkcast 253	com/android/server/location/LocationProviderInterface
        //     152: astore 14
        //     154: goto -59 -> 95
        //     157: new 169	java/util/ArrayList
        //     160: dup
        //     161: invokespecial 170	java/util/ArrayList:<init>	()V
        //     164: astore 7
        //     166: iconst_0
        //     167: istore 8
        //     169: aload_3
        //     170: invokevirtual 244	java/util/ArrayList:size	()I
        //     173: istore 9
        //     175: iload 8
        //     177: iload 9
        //     179: if_icmpge +40 -> 219
        //     182: aload_3
        //     183: iload 8
        //     185: invokevirtual 251	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     188: checkcast 253	com/android/server/location/LocationProviderInterface
        //     191: invokeinterface 338 1 0
        //     196: iload 6
        //     198: if_icmpne +21 -> 219
        //     201: aload 7
        //     203: aload_3
        //     204: iload 8
        //     206: invokevirtual 251	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     209: invokevirtual 261	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     212: pop
        //     213: iinc 8 1
        //     216: goto -41 -> 175
        //     219: aload 7
        //     221: new 30	com/android/server/LocationManagerService$LpAccuracyComparator
        //     224: dup
        //     225: aload_0
        //     226: aconst_null
        //     227: invokespecial 563	com/android/server/LocationManagerService$LpAccuracyComparator:<init>	(Lcom/android/server/LocationManagerService;Lcom/android/server/LocationManagerService$1;)V
        //     230: invokestatic 562	java/util/Collections:sort	(Ljava/util/List;Ljava/util/Comparator;)V
        //     233: aload 7
        //     235: iconst_0
        //     236: invokevirtual 251	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     239: checkcast 253	com/android/server/location/LocationProviderInterface
        //     242: invokeinterface 347 1 0
        //     247: istore 10
        //     249: iload 10
        //     251: aload 7
        //     253: iconst_1
        //     254: invokevirtual 251	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     257: checkcast 253	com/android/server/location/LocationProviderInterface
        //     260: invokeinterface 347 1 0
        //     265: if_icmpge +17 -> 282
        //     268: aload 7
        //     270: iconst_0
        //     271: invokevirtual 251	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     274: checkcast 253	com/android/server/location/LocationProviderInterface
        //     277: astore 14
        //     279: goto -184 -> 95
        //     282: new 169	java/util/ArrayList
        //     285: dup
        //     286: invokespecial 170	java/util/ArrayList:<init>	()V
        //     289: astore 11
        //     291: iconst_0
        //     292: istore 12
        //     294: aload 7
        //     296: invokevirtual 244	java/util/ArrayList:size	()I
        //     299: istore 13
        //     301: iload 12
        //     303: iload 13
        //     305: if_icmpge +42 -> 347
        //     308: aload 7
        //     310: iload 12
        //     312: invokevirtual 251	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     315: checkcast 253	com/android/server/location/LocationProviderInterface
        //     318: invokeinterface 347 1 0
        //     323: iload 10
        //     325: if_icmpne +22 -> 347
        //     328: aload 11
        //     330: aload 7
        //     332: iload 12
        //     334: invokevirtual 251	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     337: invokevirtual 261	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     340: pop
        //     341: iinc 12 1
        //     344: goto -43 -> 301
        //     347: aload 11
        //     349: new 27	com/android/server/LocationManagerService$LpCapabilityComparator
        //     352: dup
        //     353: aload_0
        //     354: aconst_null
        //     355: invokespecial 564	com/android/server/LocationManagerService$LpCapabilityComparator:<init>	(Lcom/android/server/LocationManagerService;Lcom/android/server/LocationManagerService$1;)V
        //     358: invokestatic 562	java/util/Collections:sort	(Ljava/util/List;Ljava/util/Comparator;)V
        //     361: aload 11
        //     363: iconst_0
        //     364: invokevirtual 251	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     367: checkcast 253	com/android/server/location/LocationProviderInterface
        //     370: astore 14
        //     372: goto -277 -> 95
        //
        // Exception table:
        //     from	to	target	type
        //     7	72	68	finally
        //     75	77	68	finally
    }

    private void checkMockPermissionsSafe()
    {
        int i = 1;
        if (Settings.Secure.getInt(this.mContext.getContentResolver(), "mock_location", 0) == i);
        while (i == 0)
        {
            throw new SecurityException("Requires ACCESS_MOCK_LOCATION secure setting");
            i = 0;
        }
        if (this.mContext.checkCallingPermission("android.permission.ACCESS_MOCK_LOCATION") != 0)
            throw new SecurityException("Requires ACCESS_MOCK_LOCATION permission");
    }

    private void checkPackageName(int paramInt, String paramString)
    {
        if (paramString == null)
            throw new SecurityException("packageName cannot be null");
        String[] arrayOfString = this.mPackageManager.getPackagesForUid(paramInt);
        if (arrayOfString == null)
            throw new SecurityException("invalid UID " + paramInt);
        int i = arrayOfString.length;
        for (int j = 0; j < i; j++)
            if (paramString.equals(arrayOfString[j]))
                return;
        throw new SecurityException("invalid package name");
    }

    private String checkPermissionsSafe(String paramString1, String paramString2)
    {
        if (("gps".equals(paramString1)) || ("passive".equals(paramString1)))
        {
            if (this.mContext.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") != 0)
                throw new SecurityException("Provider " + paramString1 + " requires ACCESS_FINE_LOCATION permission");
            paramString2 = "android.permission.ACCESS_FINE_LOCATION";
        }
        while (true)
        {
            return paramString2;
            if (this.mContext.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0)
            {
                if (!"android.permission.ACCESS_FINE_LOCATION".equals(paramString2))
                    paramString2 = "android.permission.ACCESS_COARSE_LOCATION";
            }
            else
            {
                if (this.mContext.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") != 0)
                    break;
                paramString2 = "android.permission.ACCESS_FINE_LOCATION";
            }
        }
        throw new SecurityException("Provider " + paramString1 + " requires ACCESS_FINE_LOCATION or ACCESS_COARSE_LOCATION permission");
    }

    private void decrementPendingBroadcasts()
    {
        synchronized (this.mWakeLock)
        {
            int i = -1 + this.mPendingBroadcasts;
            this.mPendingBroadcasts = i;
            if (i == 0);
            try
            {
                if (this.mWakeLock.isHeld())
                {
                    this.mWakeLock.release();
                    log("Released wakelock");
                }
                while (true)
                {
                    return;
                    log("Can't release wakelock again!");
                }
            }
            catch (Exception localException)
            {
                while (true)
                    Slog.e("LocationManagerService", "exception in releaseWakeLock()", localException);
            }
        }
    }

    private long getMinTimeLocked(String paramString)
    {
        long l1 = 9223372036854775807L;
        ArrayList localArrayList = (ArrayList)this.mRecordsByProvider.get(paramString);
        this.mTmpWorkSource.clear();
        if (localArrayList != null)
        {
            for (int i = -1 + localArrayList.size(); i >= 0; i--)
            {
                long l3 = ((UpdateRecord)localArrayList.get(i)).mMinTime;
                if (l3 < l1)
                    l1 = l3;
            }
            long l2 = 3L * l1 / 2L;
            for (int j = -1 + localArrayList.size(); j >= 0; j--)
            {
                UpdateRecord localUpdateRecord = (UpdateRecord)localArrayList.get(j);
                if (localUpdateRecord.mMinTime <= l2)
                    this.mTmpWorkSource.add(localUpdateRecord.mUid);
            }
        }
        return l1;
    }

    private Receiver getReceiver(PendingIntent paramPendingIntent, String paramString)
    {
        Receiver localReceiver = (Receiver)this.mReceivers.get(paramPendingIntent);
        if (localReceiver == null)
        {
            localReceiver = new Receiver(paramPendingIntent, paramString);
            this.mReceivers.put(paramPendingIntent, localReceiver);
        }
        return localReceiver;
    }

    private Receiver getReceiver(ILocationListener paramILocationListener, String paramString)
    {
        IBinder localIBinder = paramILocationListener.asBinder();
        Receiver localReceiver1 = (Receiver)this.mReceivers.get(localIBinder);
        if (localReceiver1 == null)
        {
            localReceiver1 = new Receiver(paramILocationListener, paramString);
            this.mReceivers.put(localIBinder, localReceiver1);
        }
        try
        {
            if (localReceiver1.isListener())
                localReceiver1.getListener().asBinder().linkToDeath(localReceiver1, 0);
            localReceiver2 = localReceiver1;
            return localReceiver2;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Slog.e("LocationManagerService", "linkToDeath failed:", localRemoteException);
                Receiver localReceiver2 = null;
            }
        }
    }

    private String[] getStringArray(String paramString)
    {
        String str1 = Settings.Secure.getString(this.mContext.getContentResolver(), paramString);
        if (str1 == null);
        ArrayList localArrayList;
        for (String[] arrayOfString2 = new String[0]; ; arrayOfString2 = (String[])localArrayList.toArray(new String[localArrayList.size()]))
        {
            return arrayOfString2;
            String[] arrayOfString1 = str1.split(",");
            localArrayList = new ArrayList();
            int i = arrayOfString1.length;
            int j = 0;
            if (j < i)
            {
                String str2 = arrayOfString1[j].trim();
                if (str2.isEmpty());
                while (true)
                {
                    j++;
                    break;
                    localArrayList.add(str2);
                }
            }
        }
    }

    private void handleLocationChangedLocked(Location paramLocation, boolean paramBoolean)
    {
        String str;
        ArrayList localArrayList1;
        if (paramBoolean)
        {
            str = "passive";
            localArrayList1 = (ArrayList)this.mRecordsByProvider.get(str);
            if ((localArrayList1 != null) && (localArrayList1.size() != 0))
                break label43;
        }
        while (true)
        {
            return;
            str = paramLocation.getProvider();
            break;
            label43: LocationProviderInterface localLocationProviderInterface = (LocationProviderInterface)this.mProvidersByName.get(str);
            if (localLocationProviderInterface != null)
            {
                Location localLocation1 = (Location)this.mLastKnownLocation.get(str);
                if (localLocation1 == null)
                {
                    HashMap localHashMap = this.mLastKnownLocation;
                    Location localLocation3 = new Location(paramLocation);
                    localHashMap.put(str, localLocation3);
                }
                long l1;
                Bundle localBundle;
                int i;
                ArrayList localArrayList2;
                UpdateRecord localUpdateRecord;
                Receiver localReceiver;
                int n;
                while (true)
                {
                    l1 = localLocationProviderInterface.getStatusUpdateTime();
                    localBundle = new Bundle();
                    i = localLocationProviderInterface.getStatus(localBundle);
                    localArrayList2 = null;
                    int j = localArrayList1.size();
                    for (int k = 0; ; k++)
                    {
                        if (k >= j)
                            break label417;
                        localUpdateRecord = (UpdateRecord)localArrayList1.get(k);
                        localReceiver = localUpdateRecord.mReceiver;
                        n = 0;
                        if (!inBlacklist(localReceiver.mPackageName))
                            break;
                    }
                    localLocation1.set(paramLocation);
                }
                Location localLocation2 = localUpdateRecord.mLastFixBroadcast;
                if ((localLocation2 == null) || (shouldBroadcastSafe(paramLocation, localLocation2, localUpdateRecord)))
                {
                    if (localLocation2 != null)
                        break label408;
                    localUpdateRecord.mLastFixBroadcast = new Location(paramLocation);
                }
                while (true)
                {
                    if (!localReceiver.callLocationChangedLocked(paramLocation))
                    {
                        Slog.w("LocationManagerService", "RemoteException calling onLocationChanged on " + localReceiver);
                        n = 1;
                    }
                    long l2 = localUpdateRecord.mLastStatusBroadcast;
                    if ((l1 > l2) && ((l2 != 0L) || (i != 2)))
                    {
                        localUpdateRecord.mLastStatusBroadcast = l1;
                        if (!localReceiver.callStatusChangedLocked(str, i, localBundle))
                        {
                            n = 1;
                            Slog.w("LocationManagerService", "RemoteException calling onStatusChanged on " + localReceiver);
                        }
                    }
                    if ((n == 0) && (!localUpdateRecord.mSingleShot))
                        break;
                    if (localArrayList2 == null)
                        localArrayList2 = new ArrayList();
                    if (localArrayList2.contains(localReceiver))
                        break;
                    localArrayList2.add(localReceiver);
                    break;
                    label408: localLocation2.set(paramLocation);
                }
                label417: if (localArrayList2 != null)
                    for (int m = -1 + localArrayList2.size(); m >= 0; m--)
                        removeUpdatesLocked((Receiver)localArrayList2.get(m));
            }
        }
    }

    private boolean inBlacklist(String paramString)
    {
        boolean bool;
        while (true)
        {
            int j;
            synchronized (this.mLock)
            {
                String[] arrayOfString = this.mBlacklist;
                int i = arrayOfString.length;
                j = 0;
                if (j < i)
                {
                    if ((paramString.startsWith(arrayOfString[j])) && (!inWhitelist(paramString)))
                    {
                        bool = true;
                        break;
                    }
                }
                else
                    bool = false;
            }
        }
        return bool;
    }

    private boolean inWhitelist(String paramString)
    {
        while (true)
        {
            int j;
            boolean bool;
            synchronized (this.mLock)
            {
                String[] arrayOfString = this.mWhitelist;
                int i = arrayOfString.length;
                j = 0;
                if (j < i)
                {
                    if (!paramString.startsWith(arrayOfString[j]))
                        break label64;
                    bool = true;
                }
                else
                {
                    bool = false;
                }
            }
            label64: j++;
        }
    }

    private void incrementPendingBroadcasts()
    {
        synchronized (this.mWakeLock)
        {
            int i = this.mPendingBroadcasts;
            this.mPendingBroadcasts = (i + 1);
            if (i == 0);
            try
            {
                this.mWakeLock.acquire();
                log("Acquired wakelock");
                return;
            }
            catch (Exception localException)
            {
                while (true)
                    Slog.e("LocationManagerService", "exception in acquireWakeLock()", localException);
            }
        }
    }

    private void initialize()
    {
        this.mWakeLock = ((PowerManager)this.mContext.getSystemService("power")).newWakeLock(1, "LocationManagerService");
        this.mPackageManager = this.mContext.getPackageManager();
        loadProviders();
        loadBlacklist();
        IntentFilter localIntentFilter1 = new IntentFilter();
        localIntentFilter1.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        localIntentFilter1.addAction("android.intent.action.PACKAGE_REMOVED");
        localIntentFilter1.addAction("android.intent.action.PACKAGE_RESTARTED");
        localIntentFilter1.addAction("android.intent.action.QUERY_PACKAGE_RESTART");
        this.mContext.registerReceiver(this.mBroadcastReceiver, localIntentFilter1);
        IntentFilter localIntentFilter2 = new IntentFilter("android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE");
        this.mContext.registerReceiver(this.mBroadcastReceiver, localIntentFilter2);
        ContentResolver localContentResolver = this.mContext.getContentResolver();
        Uri localUri = Settings.Secure.CONTENT_URI;
        String[] arrayOfString = new String[1];
        arrayOfString[0] = "location_providers_allowed";
        this.mSettings = new ContentQueryMap(localContentResolver.query(localUri, null, "(name=?)", arrayOfString, null), "name", true, this.mLocationHandler);
        SettingsObserver localSettingsObserver = new SettingsObserver(null);
        this.mSettings.addObserver(localSettingsObserver);
    }

    private boolean isAllowedBySettingsLocked(String paramString)
    {
        boolean bool;
        if (this.mEnabledProviders.contains(paramString))
            bool = true;
        while (true)
        {
            return bool;
            if (this.mDisabledProviders.contains(paramString))
                bool = false;
            else
                bool = Settings.Secure.isLocationProviderEnabled(this.mContext.getContentResolver(), paramString);
        }
    }

    private boolean isAllowedProviderSafe(String paramString)
    {
        boolean bool = false;
        if ((("gps".equals(paramString)) || ("passive".equals(paramString))) && (this.mContext.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") != 0));
        while (true)
        {
            return bool;
            if ((!"network".equals(paramString)) || (this.mContext.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) || (this.mContext.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0))
                bool = true;
        }
    }

    private void loadBlacklist()
    {
        BlacklistObserver localBlacklistObserver = new BlacklistObserver(this.mLocationHandler);
        this.mContext.getContentResolver().registerContentObserver(Settings.Secure.getUriFor("locationPackagePrefixBlacklist"), false, localBlacklistObserver);
        this.mContext.getContentResolver().registerContentObserver(Settings.Secure.getUriFor("locationPackagePrefixWhitelist"), false, localBlacklistObserver);
        reloadBlacklist();
    }

    private void loadProviders()
    {
        synchronized (this.mLock)
        {
            if (!sProvidersLoaded)
            {
                loadProvidersLocked();
                sProvidersLoaded = true;
            }
        }
    }

    private void loadProvidersLocked()
    {
        try
        {
            _loadProvidersLocked();
            return;
        }
        catch (Exception localException)
        {
            while (true)
                Slog.e("LocationManagerService", "Exception loading providers:", localException);
        }
    }

    private void log(String paramString)
    {
        if (Log.isLoggable("LocationManagerService", 2))
            Slog.d("LocationManagerService", paramString);
    }

    private int nextAccuracy(int paramInt)
    {
        if (paramInt == 1);
        for (int i = 2; ; i = 0)
            return i;
    }

    private int nextPower(int paramInt)
    {
        int i = 0;
        switch (paramInt)
        {
        case 3:
        default:
        case 1:
        case 2:
        }
        while (true)
        {
            return i;
            i = 2;
            continue;
            i = 3;
        }
    }

    private boolean providerHasListener(String paramString, int paramInt, Receiver paramReceiver)
    {
        boolean bool = true;
        ArrayList localArrayList = (ArrayList)this.mRecordsByProvider.get(paramString);
        int i;
        if (localArrayList != null)
        {
            i = -1 + localArrayList.size();
            if (i >= 0)
            {
                UpdateRecord localUpdateRecord = (UpdateRecord)localArrayList.get(i);
                if ((localUpdateRecord.mUid != paramInt) || (localUpdateRecord.mReceiver == paramReceiver));
            }
        }
        while (true)
        {
            return bool;
            i--;
            break;
            Iterator localIterator = this.mProximityAlerts.values().iterator();
            while (true)
                if (localIterator.hasNext())
                    if (((ProximityAlert)localIterator.next()).mUid == paramInt)
                        break;
            bool = false;
        }
    }

    private void reloadBlacklist()
    {
        String[] arrayOfString1 = getStringArray("locationPackagePrefixBlacklist");
        String[] arrayOfString2 = getStringArray("locationPackagePrefixWhitelist");
        synchronized (this.mLock)
        {
            this.mWhitelist = arrayOfString2;
            Slog.i("LocationManagerService", "whitelist: " + arrayToString(this.mWhitelist));
            this.mBlacklist = arrayOfString1;
            Slog.i("LocationManagerService", "blacklist: " + arrayToString(this.mBlacklist));
            return;
        }
    }

    private void removeProvider(LocationProviderInterface paramLocationProviderInterface)
    {
        this.mProviders.remove(paramLocationProviderInterface);
        this.mProvidersByName.remove(paramLocationProviderInterface.getName());
    }

    private void removeProximityAlertLocked(PendingIntent paramPendingIntent)
    {
        this.mProximityAlerts.remove(paramPendingIntent);
        if (this.mProximityAlerts.size() == 0)
        {
            if (this.mProximityReceiver != null)
                removeUpdatesLocked(this.mProximityReceiver);
            this.mProximityReceiver = null;
            this.mProximityListener = null;
        }
    }

    // ERROR //
    private void removeUpdatesLocked(Receiver paramReceiver)
    {
        // Byte code:
        //     0: invokestatic 919	android/os/Binder:getCallingPid	()I
        //     3: istore_2
        //     4: invokestatic 272	android/os/Binder:getCallingUid	()I
        //     7: istore_3
        //     8: invokestatic 922	android/os/Binder:clearCallingIdentity	()J
        //     11: lstore 4
        //     13: aload_0
        //     14: getfield 167	com/android/server/LocationManagerService:mReceivers	Ljava/util/HashMap;
        //     17: aload_1
        //     18: getfield 925	com/android/server/LocationManagerService$Receiver:mKey	Ljava/lang/Object;
        //     21: invokevirtual 915	java/util/HashMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
        //     24: ifnull +47 -> 71
        //     27: aload_1
        //     28: invokevirtual 679	com/android/server/LocationManagerService$Receiver:isListener	()Z
        //     31: ifeq +40 -> 71
        //     34: aload_1
        //     35: invokevirtual 683	com/android/server/LocationManagerService$Receiver:getListener	()Landroid/location/ILocationListener;
        //     38: invokeinterface 676 1 0
        //     43: aload_1
        //     44: iconst_0
        //     45: invokeinterface 929 3 0
        //     50: pop
        //     51: aload_1
        //     52: monitorenter
        //     53: aload_1
        //     54: getfield 930	com/android/server/LocationManagerService$Receiver:mPendingBroadcasts	I
        //     57: ifle +12 -> 69
        //     60: aload_0
        //     61: invokespecial 479	com/android/server/LocationManagerService:decrementPendingBroadcasts	()V
        //     64: aload_1
        //     65: iconst_0
        //     66: putfield 930	com/android/server/LocationManagerService$Receiver:mPendingBroadcasts	I
        //     69: aload_1
        //     70: monitorexit
        //     71: new 156	java/util/HashSet
        //     74: dup
        //     75: invokespecial 157	java/util/HashSet:<init>	()V
        //     78: astore 7
        //     80: aload_1
        //     81: getfield 933	com/android/server/LocationManagerService$Receiver:mUpdateRecords	Ljava/util/HashMap;
        //     84: astore 8
        //     86: aload 8
        //     88: ifnull +117 -> 205
        //     91: aload 8
        //     93: invokevirtual 894	java/util/HashMap:values	()Ljava/util/Collection;
        //     96: invokeinterface 897 1 0
        //     101: astore 18
        //     103: aload 18
        //     105: invokeinterface 549 1 0
        //     110: ifeq +84 -> 194
        //     113: aload 18
        //     115: invokeinterface 553 1 0
        //     120: checkcast 24	com/android/server/LocationManagerService$UpdateRecord
        //     123: astore 20
        //     125: aload_0
        //     126: aload 20
        //     128: getfield 936	com/android/server/LocationManagerService$UpdateRecord:mProvider	Ljava/lang/String;
        //     131: iload_3
        //     132: aload_1
        //     133: invokespecial 938	com/android/server/LocationManagerService:providerHasListener	(Ljava/lang/String;ILcom/android/server/LocationManagerService$Receiver;)Z
        //     136: ifne +33 -> 169
        //     139: aload_0
        //     140: getfield 174	com/android/server/LocationManagerService:mProvidersByName	Ljava/util/HashMap;
        //     143: aload 20
        //     145: getfield 936	com/android/server/LocationManagerService$UpdateRecord:mProvider	Ljava/lang/String;
        //     148: invokevirtual 279	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     151: checkcast 253	com/android/server/location/LocationProviderInterface
        //     154: astore 21
        //     156: aload 21
        //     158: ifnull +11 -> 169
        //     161: aload 21
        //     163: iload_3
        //     164: invokeinterface 941 2 0
        //     169: aload 20
        //     171: invokevirtual 944	com/android/server/LocationManagerService$UpdateRecord:disposeLocked	()V
        //     174: goto -71 -> 103
        //     177: astore 6
        //     179: lload 4
        //     181: invokestatic 948	android/os/Binder:restoreCallingIdentity	(J)V
        //     184: aload 6
        //     186: athrow
        //     187: astore 23
        //     189: aload_1
        //     190: monitorexit
        //     191: aload 23
        //     193: athrow
        //     194: aload 7
        //     196: aload 8
        //     198: invokevirtual 952	java/util/HashMap:keySet	()Ljava/util/Set;
        //     201: invokevirtual 956	java/util/HashSet:addAll	(Ljava/util/Collection;)Z
        //     204: pop
        //     205: aload 7
        //     207: invokevirtual 957	java/util/HashSet:iterator	()Ljava/util/Iterator;
        //     210: astore 9
        //     212: aload 9
        //     214: invokeinterface 549 1 0
        //     219: ifeq +207 -> 426
        //     222: aload 9
        //     224: invokeinterface 553 1 0
        //     229: checkcast 200	java/lang/String
        //     232: astore 10
        //     234: aload_0
        //     235: aload 10
        //     237: invokespecial 283	com/android/server/LocationManagerService:isAllowedBySettingsLocked	(Ljava/lang/String;)Z
        //     240: ifeq -28 -> 212
        //     243: iconst_0
        //     244: istore 11
        //     246: aload_0
        //     247: getfield 181	com/android/server/LocationManagerService:mRecordsByProvider	Ljava/util/HashMap;
        //     250: aload 10
        //     252: invokevirtual 279	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     255: checkcast 169	java/util/ArrayList
        //     258: astore 12
        //     260: aload 12
        //     262: ifnull +14 -> 276
        //     265: aload 12
        //     267: invokevirtual 244	java/util/ArrayList:size	()I
        //     270: ifle +6 -> 276
        //     273: iconst_1
        //     274: istore 11
        //     276: aload_0
        //     277: getfield 174	com/android/server/LocationManagerService:mProvidersByName	Ljava/util/HashMap;
        //     280: aload 10
        //     282: invokevirtual 279	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     285: checkcast 253	com/android/server/location/LocationProviderInterface
        //     288: astore 13
        //     290: aload 13
        //     292: ifnull -80 -> 212
        //     295: iload 11
        //     297: ifeq +75 -> 372
        //     300: aload_0
        //     301: aload 10
        //     303: invokespecial 959	com/android/server/LocationManagerService:getMinTimeLocked	(Ljava/lang/String;)J
        //     306: lstore 15
        //     308: ldc 72
        //     310: new 524	java/lang/StringBuilder
        //     313: dup
        //     314: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     317: ldc_w 961
        //     320: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     323: aload 10
        //     325: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     328: ldc_w 963
        //     331: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     334: iload_2
        //     335: invokevirtual 598	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     338: ldc_w 965
        //     341: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     344: lload 15
        //     346: invokevirtual 968	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     349: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     352: invokestatic 907	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     355: pop
        //     356: aload 13
        //     358: lload 15
        //     360: aload_0
        //     361: getfield 186	com/android/server/LocationManagerService:mTmpWorkSource	Landroid/os/WorkSource;
        //     364: invokeinterface 972 4 0
        //     369: goto -157 -> 212
        //     372: ldc 72
        //     374: new 524	java/lang/StringBuilder
        //     377: dup
        //     378: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     381: ldc_w 961
        //     384: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     387: aload 10
        //     389: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     392: ldc_w 963
        //     395: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     398: iload_2
        //     399: invokevirtual 598	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     402: ldc_w 974
        //     405: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     408: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     411: invokestatic 907	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     414: pop
        //     415: aload 13
        //     417: iconst_0
        //     418: invokeinterface 978 2 0
        //     423: goto -211 -> 212
        //     426: lload 4
        //     428: invokestatic 948	android/os/Binder:restoreCallingIdentity	(J)V
        //     431: return
        //
        // Exception table:
        //     from	to	target	type
        //     13	53	177	finally
        //     71	174	177	finally
        //     191	423	177	finally
        //     53	71	187	finally
        //     189	191	187	finally
    }

    private void requestLocationUpdatesLocked(String paramString, long paramLong, float paramFloat, boolean paramBoolean, Receiver paramReceiver)
    {
        LocationProviderInterface localLocationProviderInterface = (LocationProviderInterface)this.mProvidersByName.get(paramString);
        if (localLocationProviderInterface == null)
            throw new IllegalArgumentException("requested provider " + paramString + " doesn't exisit");
        paramReceiver.mRequiredPermissions = checkPermissionsSafe(paramString, paramReceiver.mRequiredPermissions);
        int i = Binder.getCallingPid();
        int j = Binder.getCallingUid();
        int k;
        if (!providerHasListener(paramString, j, null))
            k = 1;
        while (true)
        {
            long l1 = Binder.clearCallingIdentity();
            try
            {
                UpdateRecord localUpdateRecord1 = new UpdateRecord(paramString, paramLong, paramFloat, paramBoolean, paramReceiver, j);
                UpdateRecord localUpdateRecord2 = (UpdateRecord)paramReceiver.mUpdateRecords.put(paramString, localUpdateRecord1);
                if (localUpdateRecord2 != null)
                    localUpdateRecord2.disposeLocked();
                if (k != 0)
                    localLocationProviderInterface.addListener(j);
                String str;
                if (isAllowedBySettingsLocked(paramString))
                {
                    long l2 = getMinTimeLocked(paramString);
                    StringBuilder localStringBuilder = new StringBuilder().append("request ").append(paramString).append(" (pid ").append(i).append(") ").append(paramLong).append(" ").append(l2);
                    if (paramBoolean)
                    {
                        str = " (singleshot)";
                        label232: Slog.i("LocationManagerService", str);
                        localLocationProviderInterface.setMinTime(l2, this.mTmpWorkSource);
                        if ((!paramBoolean) || (!localLocationProviderInterface.requestSingleShotFix()))
                            localLocationProviderInterface.enableLocationTracking(true);
                    }
                }
                while (true)
                {
                    return;
                    k = 0;
                    break;
                    str = "";
                    break label232;
                    paramReceiver.callProviderEnabledLocked(paramString, false);
                }
            }
            finally
            {
                Binder.restoreCallingIdentity(l1);
            }
        }
    }

    private static boolean shouldBroadcastSafe(Location paramLocation1, Location paramLocation2, UpdateRecord paramUpdateRecord)
    {
        boolean bool = true;
        if (paramLocation2 == null);
        while (true)
        {
            return bool;
            long l = paramUpdateRecord.mMinTime;
            if (paramLocation1.getTime() - paramLocation2.getTime() < l - 100L)
            {
                bool = false;
            }
            else
            {
                double d = paramUpdateRecord.mMinDistance;
                if ((d > 0.0D) && (paramLocation1.distanceTo(paramLocation2) <= d))
                    bool = false;
            }
        }
    }

    private void updateProviderListenersLocked(String paramString, boolean paramBoolean)
    {
        int i = 0;
        LocationProviderInterface localLocationProviderInterface = (LocationProviderInterface)this.mProvidersByName.get(paramString);
        if (localLocationProviderInterface == null);
        while (true)
        {
            return;
            ArrayList localArrayList1 = null;
            ArrayList localArrayList2 = (ArrayList)this.mRecordsByProvider.get(paramString);
            if (localArrayList2 != null)
            {
                int k = localArrayList2.size();
                for (int m = 0; m < k; m++)
                {
                    UpdateRecord localUpdateRecord = (UpdateRecord)localArrayList2.get(m);
                    if (!localUpdateRecord.mReceiver.callProviderEnabledLocked(paramString, paramBoolean))
                    {
                        if (localArrayList1 == null)
                            localArrayList1 = new ArrayList();
                        localArrayList1.add(localUpdateRecord.mReceiver);
                    }
                    i++;
                }
            }
            if (localArrayList1 != null)
                for (int j = -1 + localArrayList1.size(); j >= 0; j--)
                    removeUpdatesLocked((Receiver)localArrayList1.get(j));
            if (paramBoolean)
            {
                localLocationProviderInterface.enable();
                if (i > 0)
                {
                    localLocationProviderInterface.setMinTime(getMinTimeLocked(paramString), this.mTmpWorkSource);
                    localLocationProviderInterface.enableLocationTracking(true);
                }
            }
            else
            {
                localLocationProviderInterface.enableLocationTracking(false);
                localLocationProviderInterface.disable();
            }
        }
    }

    private void updateProvidersLocked()
    {
        int i = 0;
        int j = -1 + this.mProviders.size();
        if (j >= 0)
        {
            LocationProviderInterface localLocationProviderInterface = (LocationProviderInterface)this.mProviders.get(j);
            boolean bool1 = localLocationProviderInterface.isEnabled();
            String str = localLocationProviderInterface.getName();
            boolean bool2 = isAllowedBySettingsLocked(str);
            if ((bool1) && (!bool2))
                updateProviderListenersLocked(str, false);
            for (i = 1; ; i = 1)
            {
                do
                {
                    j--;
                    break;
                }
                while ((bool1) || (!bool2));
                updateProviderListenersLocked(str, true);
            }
        }
        if (i != 0)
            this.mContext.sendBroadcast(new Intent("android.location.PROVIDERS_CHANGED"));
    }

    public boolean addGpsStatusListener(IGpsStatusListener paramIGpsStatusListener)
    {
        boolean bool = false;
        if (this.mGpsStatusProvider == null);
        while (true)
        {
            return bool;
            if (this.mContext.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") != 0)
                throw new SecurityException("Requires ACCESS_FINE_LOCATION permission");
            try
            {
                this.mGpsStatusProvider.addGpsStatusListener(paramIGpsStatusListener);
                bool = true;
            }
            catch (RemoteException localRemoteException)
            {
                Slog.e("LocationManagerService", "mGpsStatusProvider.addGpsStatusListener failed", localRemoteException);
            }
        }
    }

    public void addProximityAlert(double paramDouble1, double paramDouble2, float paramFloat, long paramLong, PendingIntent paramPendingIntent, String paramString)
    {
        validatePendingIntent(paramPendingIntent);
        try
        {
            synchronized (this.mLock)
            {
                addProximityAlertLocked(paramDouble1, paramDouble2, paramFloat, paramLong, paramPendingIntent, paramString);
            }
        }
        catch (SecurityException localSecurityException)
        {
            throw localSecurityException;
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            throw localIllegalArgumentException;
        }
        catch (Exception localException)
        {
            Slog.e("LocationManagerService", "addProximityAlert got exception:", localException);
        }
    }

    // ERROR //
    public void addTestProvider(String paramString, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, boolean paramBoolean7, int paramInt1, int paramInt2)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 1066	com/android/server/LocationManagerService:checkMockPermissionsSafe	()V
        //     4: ldc_w 605
        //     7: aload_1
        //     8: invokevirtual 601	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     11: ifeq +14 -> 25
        //     14: new 980	java/lang/IllegalArgumentException
        //     17: dup
        //     18: ldc_w 1068
        //     21: invokespecial 985	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     24: athrow
        //     25: invokestatic 922	android/os/Binder:clearCallingIdentity	()J
        //     28: lstore 11
        //     30: aload_0
        //     31: getfield 179	com/android/server/LocationManagerService:mLock	Ljava/lang/Object;
        //     34: astore 13
        //     36: aload 13
        //     38: monitorenter
        //     39: new 1070	com/android/server/location/MockProvider
        //     42: dup
        //     43: aload_1
        //     44: aload_0
        //     45: iload_2
        //     46: iload_3
        //     47: iload 4
        //     49: iload 5
        //     51: iload 6
        //     53: iload 7
        //     55: iload 8
        //     57: iload 9
        //     59: iload 10
        //     61: invokespecial 1073	com/android/server/location/MockProvider:<init>	(Ljava/lang/String;Landroid/location/ILocationManager;ZZZZZZZII)V
        //     64: astore 14
        //     66: ldc_w 492
        //     69: aload_1
        //     70: invokevirtual 601	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     73: ifne +13 -> 86
        //     76: ldc_w 295
        //     79: aload_1
        //     80: invokevirtual 601	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     83: ifeq +35 -> 118
        //     86: aload_0
        //     87: getfield 174	com/android/server/LocationManagerService:mProvidersByName	Ljava/util/HashMap;
        //     90: aload_1
        //     91: invokevirtual 279	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     94: checkcast 253	com/android/server/location/LocationProviderInterface
        //     97: astore 16
        //     99: aload 16
        //     101: ifnull +17 -> 118
        //     104: aload 16
        //     106: iconst_0
        //     107: invokeinterface 978 2 0
        //     112: aload_0
        //     113: aload 16
        //     115: invokespecial 1075	com/android/server/LocationManagerService:removeProvider	(Lcom/android/server/location/LocationProviderInterface;)V
        //     118: aload_0
        //     119: getfield 174	com/android/server/LocationManagerService:mProvidersByName	Ljava/util/HashMap;
        //     122: aload_1
        //     123: invokevirtual 279	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     126: ifnull +45 -> 171
        //     129: new 980	java/lang/IllegalArgumentException
        //     132: dup
        //     133: new 524	java/lang/StringBuilder
        //     136: dup
        //     137: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     140: ldc_w 1077
        //     143: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     146: aload_1
        //     147: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     150: ldc_w 1079
        //     153: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     156: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     159: invokespecial 985	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     162: athrow
        //     163: astore 15
        //     165: aload 13
        //     167: monitorexit
        //     168: aload 15
        //     170: athrow
        //     171: aload_0
        //     172: aload 14
        //     174: invokespecial 382	com/android/server/LocationManagerService:addProvider	(Lcom/android/server/location/LocationProviderInterface;)V
        //     177: aload_0
        //     178: getfield 163	com/android/server/LocationManagerService:mMockProviders	Ljava/util/HashMap;
        //     181: aload_1
        //     182: aload 14
        //     184: invokevirtual 488	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     187: pop
        //     188: aload_0
        //     189: getfield 196	com/android/server/LocationManagerService:mLastKnownLocation	Ljava/util/HashMap;
        //     192: aload_1
        //     193: aconst_null
        //     194: invokevirtual 488	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     197: pop
        //     198: aload_0
        //     199: invokespecial 419	com/android/server/LocationManagerService:updateProvidersLocked	()V
        //     202: aload 13
        //     204: monitorexit
        //     205: lload 11
        //     207: invokestatic 948	android/os/Binder:restoreCallingIdentity	(J)V
        //     210: return
        //
        // Exception table:
        //     from	to	target	type
        //     39	168	163	finally
        //     171	205	163	finally
    }

    // ERROR //
    public void clearTestProviderEnabled(String paramString)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 1066	com/android/server/LocationManagerService:checkMockPermissionsSafe	()V
        //     4: aload_0
        //     5: getfield 179	com/android/server/LocationManagerService:mLock	Ljava/lang/Object;
        //     8: astore_2
        //     9: aload_2
        //     10: monitorenter
        //     11: aload_0
        //     12: getfield 163	com/android/server/LocationManagerService:mMockProviders	Ljava/util/HashMap;
        //     15: aload_1
        //     16: invokevirtual 279	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     19: checkcast 1070	com/android/server/location/MockProvider
        //     22: ifnonnull +42 -> 64
        //     25: new 980	java/lang/IllegalArgumentException
        //     28: dup
        //     29: new 524	java/lang/StringBuilder
        //     32: dup
        //     33: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     36: ldc_w 1077
        //     39: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     42: aload_1
        //     43: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     46: ldc_w 1082
        //     49: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     52: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     55: invokespecial 985	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     58: athrow
        //     59: astore_3
        //     60: aload_2
        //     61: monitorexit
        //     62: aload_3
        //     63: athrow
        //     64: invokestatic 922	android/os/Binder:clearCallingIdentity	()J
        //     67: lstore 4
        //     69: aload_0
        //     70: getfield 159	com/android/server/LocationManagerService:mEnabledProviders	Ljava/util/Set;
        //     73: aload_1
        //     74: invokeinterface 1083 2 0
        //     79: pop
        //     80: aload_0
        //     81: getfield 161	com/android/server/LocationManagerService:mDisabledProviders	Ljava/util/Set;
        //     84: aload_1
        //     85: invokeinterface 1083 2 0
        //     90: pop
        //     91: aload_0
        //     92: invokespecial 419	com/android/server/LocationManagerService:updateProvidersLocked	()V
        //     95: lload 4
        //     97: invokestatic 948	android/os/Binder:restoreCallingIdentity	(J)V
        //     100: aload_2
        //     101: monitorexit
        //     102: return
        //
        // Exception table:
        //     from	to	target	type
        //     11	62	59	finally
        //     64	102	59	finally
    }

    // ERROR //
    public void clearTestProviderLocation(String paramString)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 1066	com/android/server/LocationManagerService:checkMockPermissionsSafe	()V
        //     4: aload_0
        //     5: getfield 179	com/android/server/LocationManagerService:mLock	Ljava/lang/Object;
        //     8: astore_2
        //     9: aload_2
        //     10: monitorenter
        //     11: aload_0
        //     12: getfield 163	com/android/server/LocationManagerService:mMockProviders	Ljava/util/HashMap;
        //     15: aload_1
        //     16: invokevirtual 279	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     19: checkcast 1070	com/android/server/location/MockProvider
        //     22: astore 4
        //     24: aload 4
        //     26: ifnonnull +42 -> 68
        //     29: new 980	java/lang/IllegalArgumentException
        //     32: dup
        //     33: new 524	java/lang/StringBuilder
        //     36: dup
        //     37: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     40: ldc_w 1077
        //     43: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     46: aload_1
        //     47: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     50: ldc_w 1082
        //     53: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     56: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     59: invokespecial 985	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     62: athrow
        //     63: astore_3
        //     64: aload_2
        //     65: monitorexit
        //     66: aload_3
        //     67: athrow
        //     68: aload 4
        //     70: invokevirtual 1087	com/android/server/location/MockProvider:clearLocation	()V
        //     73: aload_2
        //     74: monitorexit
        //     75: return
        //
        // Exception table:
        //     from	to	target	type
        //     11	66	63	finally
        //     68	75	63	finally
    }

    // ERROR //
    public void clearTestProviderStatus(String paramString)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 1066	com/android/server/LocationManagerService:checkMockPermissionsSafe	()V
        //     4: aload_0
        //     5: getfield 179	com/android/server/LocationManagerService:mLock	Ljava/lang/Object;
        //     8: astore_2
        //     9: aload_2
        //     10: monitorenter
        //     11: aload_0
        //     12: getfield 163	com/android/server/LocationManagerService:mMockProviders	Ljava/util/HashMap;
        //     15: aload_1
        //     16: invokevirtual 279	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     19: checkcast 1070	com/android/server/location/MockProvider
        //     22: astore 4
        //     24: aload 4
        //     26: ifnonnull +42 -> 68
        //     29: new 980	java/lang/IllegalArgumentException
        //     32: dup
        //     33: new 524	java/lang/StringBuilder
        //     36: dup
        //     37: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     40: ldc_w 1077
        //     43: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     46: aload_1
        //     47: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     50: ldc_w 1082
        //     53: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     56: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     59: invokespecial 985	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     62: athrow
        //     63: astore_3
        //     64: aload_2
        //     65: monitorexit
        //     66: aload_3
        //     67: athrow
        //     68: aload 4
        //     70: invokevirtual 1091	com/android/server/location/MockProvider:clearStatus	()V
        //     73: aload_2
        //     74: monitorexit
        //     75: return
        //
        // Exception table:
        //     from	to	target	type
        //     11	66	63	finally
        //     68	75	63	finally
    }

    // ERROR //
    protected void dump(java.io.FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 214	com/android/server/LocationManagerService:mContext	Landroid/content/Context;
        //     4: ldc_w 1095
        //     7: invokevirtual 608	android/content/Context:checkCallingOrSelfPermission	(Ljava/lang/String;)I
        //     10: ifeq +42 -> 52
        //     13: aload_2
        //     14: new 524	java/lang/StringBuilder
        //     17: dup
        //     18: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     21: ldc_w 1097
        //     24: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     27: invokestatic 919	android/os/Binder:getCallingPid	()I
        //     30: invokevirtual 598	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     33: ldc_w 1099
        //     36: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     39: invokestatic 272	android/os/Binder:getCallingUid	()I
        //     42: invokevirtual 598	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     45: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     48: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     51: return
        //     52: aload_0
        //     53: getfield 179	com/android/server/LocationManagerService:mLock	Ljava/lang/Object;
        //     56: astore 4
        //     58: aload 4
        //     60: monitorenter
        //     61: aload_2
        //     62: ldc_w 1106
        //     65: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     68: aload_2
        //     69: new 524	java/lang/StringBuilder
        //     72: dup
        //     73: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     76: ldc_w 1108
        //     79: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     82: getstatic 145	com/android/server/LocationManagerService:sProvidersLoaded	Z
        //     85: invokevirtual 1111	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     88: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     91: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     94: aload_2
        //     95: ldc_w 1113
        //     98: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     101: aload_0
        //     102: getfield 167	com/android/server/LocationManagerService:mReceivers	Ljava/util/HashMap;
        //     105: invokevirtual 916	java/util/HashMap:size	()I
        //     108: istore 6
        //     110: iconst_0
        //     111: istore 7
        //     113: iload 7
        //     115: iload 6
        //     117: if_icmpge +44 -> 161
        //     120: aload_2
        //     121: new 524	java/lang/StringBuilder
        //     124: dup
        //     125: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     128: ldc_w 1115
        //     131: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     134: aload_0
        //     135: getfield 167	com/android/server/LocationManagerService:mReceivers	Ljava/util/HashMap;
        //     138: iload 7
        //     140: invokestatic 1121	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     143: invokevirtual 279	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     146: invokevirtual 751	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     149: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     152: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     155: iinc 7 1
        //     158: goto -45 -> 113
        //     161: aload_2
        //     162: ldc_w 1123
        //     165: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     168: aload_0
        //     169: getfield 167	com/android/server/LocationManagerService:mReceivers	Ljava/util/HashMap;
        //     172: invokevirtual 894	java/util/HashMap:values	()Ljava/util/Collection;
        //     175: invokeinterface 897 1 0
        //     180: astore 8
        //     182: aload 8
        //     184: invokeinterface 549 1 0
        //     189: ifeq +150 -> 339
        //     192: aload 8
        //     194: invokeinterface 553 1 0
        //     199: checkcast 39	com/android/server/LocationManagerService$Receiver
        //     202: astore 27
        //     204: aload_2
        //     205: new 524	java/lang/StringBuilder
        //     208: dup
        //     209: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     212: ldc_w 1115
        //     215: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     218: aload 27
        //     220: invokevirtual 751	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     223: ldc_w 1125
        //     226: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     229: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     232: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     235: aload 27
        //     237: getfield 933	com/android/server/LocationManagerService$Receiver:mUpdateRecords	Ljava/util/HashMap;
        //     240: invokevirtual 1128	java/util/HashMap:entrySet	()Ljava/util/Set;
        //     243: invokeinterface 1129 1 0
        //     248: astore 28
        //     250: aload 28
        //     252: invokeinterface 549 1 0
        //     257: ifeq -75 -> 182
        //     260: aload 28
        //     262: invokeinterface 553 1 0
        //     267: checkcast 1131	java/util/Map$Entry
        //     270: astore 29
        //     272: aload_2
        //     273: new 524	java/lang/StringBuilder
        //     276: dup
        //     277: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     280: ldc_w 1133
        //     283: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     286: aload 29
        //     288: invokeinterface 1136 1 0
        //     293: checkcast 200	java/lang/String
        //     296: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     299: ldc_w 1125
        //     302: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     305: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     308: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     311: aload 29
        //     313: invokeinterface 1139 1 0
        //     318: checkcast 24	com/android/server/LocationManagerService$UpdateRecord
        //     321: aload_2
        //     322: ldc_w 1141
        //     325: invokevirtual 1144	com/android/server/LocationManagerService$UpdateRecord:dump	(Ljava/io/PrintWriter;Ljava/lang/String;)V
        //     328: goto -78 -> 250
        //     331: astore 5
        //     333: aload 4
        //     335: monitorexit
        //     336: aload 5
        //     338: athrow
        //     339: aload_2
        //     340: new 524	java/lang/StringBuilder
        //     343: dup
        //     344: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     347: ldc_w 1146
        //     350: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     353: aload_0
        //     354: getfield 204	com/android/server/LocationManagerService:mBlacklist	[Ljava/lang/String;
        //     357: invokestatic 904	com/android/server/LocationManagerService:arrayToString	([Ljava/lang/String;)Ljava/lang/String;
        //     360: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     363: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     366: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     369: aload_2
        //     370: new 524	java/lang/StringBuilder
        //     373: dup
        //     374: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     377: ldc_w 1148
        //     380: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     383: aload_0
        //     384: getfield 202	com/android/server/LocationManagerService:mWhitelist	[Ljava/lang/String;
        //     387: invokestatic 904	com/android/server/LocationManagerService:arrayToString	([Ljava/lang/String;)Ljava/lang/String;
        //     390: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     393: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     396: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     399: aload_2
        //     400: ldc_w 1150
        //     403: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     406: aload_0
        //     407: getfield 181	com/android/server/LocationManagerService:mRecordsByProvider	Ljava/util/HashMap;
        //     410: invokevirtual 1128	java/util/HashMap:entrySet	()Ljava/util/Set;
        //     413: invokeinterface 1129 1 0
        //     418: astore 9
        //     420: aload 9
        //     422: invokeinterface 549 1 0
        //     427: ifeq +134 -> 561
        //     430: aload 9
        //     432: invokeinterface 553 1 0
        //     437: checkcast 1131	java/util/Map$Entry
        //     440: astore 24
        //     442: aload_2
        //     443: new 524	java/lang/StringBuilder
        //     446: dup
        //     447: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     450: ldc_w 1115
        //     453: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     456: aload 24
        //     458: invokeinterface 1136 1 0
        //     463: checkcast 200	java/lang/String
        //     466: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     469: ldc_w 1125
        //     472: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     475: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     478: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     481: aload 24
        //     483: invokeinterface 1139 1 0
        //     488: checkcast 169	java/util/ArrayList
        //     491: invokevirtual 1151	java/util/ArrayList:iterator	()Ljava/util/Iterator;
        //     494: astore 25
        //     496: aload 25
        //     498: invokeinterface 549 1 0
        //     503: ifeq -83 -> 420
        //     506: aload 25
        //     508: invokeinterface 553 1 0
        //     513: checkcast 24	com/android/server/LocationManagerService$UpdateRecord
        //     516: astore 26
        //     518: aload_2
        //     519: new 524	java/lang/StringBuilder
        //     522: dup
        //     523: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     526: ldc_w 1133
        //     529: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     532: aload 26
        //     534: invokevirtual 751	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     537: ldc_w 1125
        //     540: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     543: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     546: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     549: aload 26
        //     551: aload_2
        //     552: ldc_w 1141
        //     555: invokevirtual 1144	com/android/server/LocationManagerService$UpdateRecord:dump	(Ljava/io/PrintWriter;Ljava/lang/String;)V
        //     558: goto -62 -> 496
        //     561: aload_2
        //     562: ldc_w 1153
        //     565: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     568: aload_0
        //     569: getfield 196	com/android/server/LocationManagerService:mLastKnownLocation	Ljava/util/HashMap;
        //     572: invokevirtual 1128	java/util/HashMap:entrySet	()Ljava/util/Set;
        //     575: invokeinterface 1129 1 0
        //     580: astore 10
        //     582: aload 10
        //     584: invokeinterface 549 1 0
        //     589: ifeq +81 -> 670
        //     592: aload 10
        //     594: invokeinterface 553 1 0
        //     599: checkcast 1131	java/util/Map$Entry
        //     602: astore 23
        //     604: aload_2
        //     605: new 524	java/lang/StringBuilder
        //     608: dup
        //     609: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     612: ldc_w 1115
        //     615: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     618: aload 23
        //     620: invokeinterface 1136 1 0
        //     625: checkcast 200	java/lang/String
        //     628: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     631: ldc_w 1125
        //     634: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     637: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     640: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     643: aload 23
        //     645: invokeinterface 1139 1 0
        //     650: checkcast 288	android/location/Location
        //     653: new 1155	android/util/PrintWriterPrinter
        //     656: dup
        //     657: aload_2
        //     658: invokespecial 1158	android/util/PrintWriterPrinter:<init>	(Ljava/io/PrintWriter;)V
        //     661: ldc_w 1133
        //     664: invokevirtual 1161	android/location/Location:dump	(Landroid/util/Printer;Ljava/lang/String;)V
        //     667: goto -85 -> 582
        //     670: aload_0
        //     671: getfield 192	com/android/server/LocationManagerService:mProximityAlerts	Ljava/util/HashMap;
        //     674: invokevirtual 916	java/util/HashMap:size	()I
        //     677: ifle +102 -> 779
        //     680: aload_2
        //     681: ldc_w 1163
        //     684: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     687: aload_0
        //     688: getfield 192	com/android/server/LocationManagerService:mProximityAlerts	Ljava/util/HashMap;
        //     691: invokevirtual 1128	java/util/HashMap:entrySet	()Ljava/util/Set;
        //     694: invokeinterface 1129 1 0
        //     699: astore 21
        //     701: aload 21
        //     703: invokeinterface 549 1 0
        //     708: ifeq +71 -> 779
        //     711: aload 21
        //     713: invokeinterface 553 1 0
        //     718: checkcast 1131	java/util/Map$Entry
        //     721: astore 22
        //     723: aload_2
        //     724: new 524	java/lang/StringBuilder
        //     727: dup
        //     728: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     731: ldc_w 1115
        //     734: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     737: aload 22
        //     739: invokeinterface 1136 1 0
        //     744: invokevirtual 751	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     747: ldc_w 1125
        //     750: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     753: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     756: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     759: aload 22
        //     761: invokeinterface 1139 1 0
        //     766: checkcast 21	com/android/server/LocationManagerService$ProximityAlert
        //     769: aload_2
        //     770: ldc_w 1133
        //     773: invokevirtual 1164	com/android/server/LocationManagerService$ProximityAlert:dump	(Ljava/io/PrintWriter;Ljava/lang/String;)V
        //     776: goto -75 -> 701
        //     779: aload_0
        //     780: getfield 194	com/android/server/LocationManagerService:mProximitiesEntered	Ljava/util/HashSet;
        //     783: invokevirtual 1165	java/util/HashSet:size	()I
        //     786: ifle +84 -> 870
        //     789: aload_2
        //     790: ldc_w 1167
        //     793: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     796: aload_0
        //     797: getfield 194	com/android/server/LocationManagerService:mProximitiesEntered	Ljava/util/HashSet;
        //     800: invokevirtual 957	java/util/HashSet:iterator	()Ljava/util/Iterator;
        //     803: astore 19
        //     805: aload 19
        //     807: invokeinterface 549 1 0
        //     812: ifeq +58 -> 870
        //     815: aload 19
        //     817: invokeinterface 553 1 0
        //     822: checkcast 21	com/android/server/LocationManagerService$ProximityAlert
        //     825: astore 20
        //     827: aload_2
        //     828: new 524	java/lang/StringBuilder
        //     831: dup
        //     832: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     835: ldc_w 1115
        //     838: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     841: aload 20
        //     843: invokevirtual 751	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     846: ldc_w 1125
        //     849: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     852: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     855: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     858: aload 20
        //     860: aload_2
        //     861: ldc_w 1133
        //     864: invokevirtual 1164	com/android/server/LocationManagerService$ProximityAlert:dump	(Ljava/io/PrintWriter;Ljava/lang/String;)V
        //     867: goto -62 -> 805
        //     870: aload_2
        //     871: new 524	java/lang/StringBuilder
        //     874: dup
        //     875: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     878: ldc_w 1169
        //     881: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     884: aload_0
        //     885: getfield 188	com/android/server/LocationManagerService:mProximityReceiver	Lcom/android/server/LocationManagerService$Receiver;
        //     888: invokevirtual 751	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     891: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     894: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     897: aload_2
        //     898: new 524	java/lang/StringBuilder
        //     901: dup
        //     902: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     905: ldc_w 1171
        //     908: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     911: aload_0
        //     912: getfield 190	com/android/server/LocationManagerService:mProximityListener	Landroid/location/ILocationListener;
        //     915: invokevirtual 751	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     918: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     921: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     924: aload_0
        //     925: getfield 159	com/android/server/LocationManagerService:mEnabledProviders	Ljava/util/Set;
        //     928: invokeinterface 1172 1 0
        //     933: ifle +71 -> 1004
        //     936: aload_2
        //     937: ldc_w 1174
        //     940: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     943: aload_0
        //     944: getfield 159	com/android/server/LocationManagerService:mEnabledProviders	Ljava/util/Set;
        //     947: invokeinterface 1129 1 0
        //     952: astore 17
        //     954: aload 17
        //     956: invokeinterface 549 1 0
        //     961: ifeq +43 -> 1004
        //     964: aload 17
        //     966: invokeinterface 553 1 0
        //     971: checkcast 200	java/lang/String
        //     974: astore 18
        //     976: aload_2
        //     977: new 524	java/lang/StringBuilder
        //     980: dup
        //     981: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     984: ldc_w 1115
        //     987: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     990: aload 18
        //     992: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     995: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     998: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     1001: goto -47 -> 954
        //     1004: aload_0
        //     1005: getfield 161	com/android/server/LocationManagerService:mDisabledProviders	Ljava/util/Set;
        //     1008: invokeinterface 1172 1 0
        //     1013: ifle +71 -> 1084
        //     1016: aload_2
        //     1017: ldc_w 1176
        //     1020: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     1023: aload_0
        //     1024: getfield 161	com/android/server/LocationManagerService:mDisabledProviders	Ljava/util/Set;
        //     1027: invokeinterface 1129 1 0
        //     1032: astore 15
        //     1034: aload 15
        //     1036: invokeinterface 549 1 0
        //     1041: ifeq +43 -> 1084
        //     1044: aload 15
        //     1046: invokeinterface 553 1 0
        //     1051: checkcast 200	java/lang/String
        //     1054: astore 16
        //     1056: aload_2
        //     1057: new 524	java/lang/StringBuilder
        //     1060: dup
        //     1061: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     1064: ldc_w 1115
        //     1067: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1070: aload 16
        //     1072: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1075: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1078: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     1081: goto -47 -> 1034
        //     1084: aload_0
        //     1085: getfield 163	com/android/server/LocationManagerService:mMockProviders	Ljava/util/HashMap;
        //     1088: invokevirtual 916	java/util/HashMap:size	()I
        //     1091: ifle +62 -> 1153
        //     1094: aload_2
        //     1095: ldc_w 1178
        //     1098: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     1101: aload_0
        //     1102: getfield 163	com/android/server/LocationManagerService:mMockProviders	Ljava/util/HashMap;
        //     1105: invokevirtual 1128	java/util/HashMap:entrySet	()Ljava/util/Set;
        //     1108: invokeinterface 1129 1 0
        //     1113: astore 14
        //     1115: aload 14
        //     1117: invokeinterface 549 1 0
        //     1122: ifeq +31 -> 1153
        //     1125: aload 14
        //     1127: invokeinterface 553 1 0
        //     1132: checkcast 1131	java/util/Map$Entry
        //     1135: invokeinterface 1139 1 0
        //     1140: checkcast 1070	com/android/server/location/MockProvider
        //     1143: aload_2
        //     1144: ldc_w 1133
        //     1147: invokevirtual 1179	com/android/server/location/MockProvider:dump	(Ljava/io/PrintWriter;Ljava/lang/String;)V
        //     1150: goto -35 -> 1115
        //     1153: aload_0
        //     1154: getfield 172	com/android/server/LocationManagerService:mProviders	Ljava/util/ArrayList;
        //     1157: invokevirtual 1151	java/util/ArrayList:iterator	()Ljava/util/Iterator;
        //     1160: astore 11
        //     1162: aload 11
        //     1164: invokeinterface 549 1 0
        //     1169: ifeq +68 -> 1237
        //     1172: aload 11
        //     1174: invokeinterface 553 1 0
        //     1179: checkcast 253	com/android/server/location/LocationProviderInterface
        //     1182: astore 12
        //     1184: aload 12
        //     1186: invokeinterface 1182 1 0
        //     1191: astore 13
        //     1193: aload 13
        //     1195: ifnull -33 -> 1162
        //     1198: aload_2
        //     1199: new 524	java/lang/StringBuilder
        //     1202: dup
        //     1203: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     1206: aload 12
        //     1208: invokeinterface 257 1 0
        //     1213: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1216: ldc_w 1184
        //     1219: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1222: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1225: invokevirtual 1104	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     1228: aload_2
        //     1229: aload 13
        //     1231: invokevirtual 1187	java/io/PrintWriter:write	(Ljava/lang/String;)V
        //     1234: goto -72 -> 1162
        //     1237: aload 4
        //     1239: monitorexit
        //     1240: goto -1189 -> 51
        //
        // Exception table:
        //     from	to	target	type
        //     61	336	331	finally
        //     339	1240	331	finally
    }

    String findBestPackage(String paramString1, String paramString2)
    {
        Intent localIntent = new Intent(paramString1);
        List localList = this.mPackageManager.queryIntentServices(localIntent, 128);
        Object localObject;
        if (localList == null)
            localObject = null;
        while (true)
        {
            return localObject;
            int i = -2147483648;
            localObject = null;
            Iterator localIterator = localList.iterator();
            while (localIterator.hasNext())
            {
                ResolveInfo localResolveInfo = (ResolveInfo)localIterator.next();
                String str = localResolveInfo.serviceInfo.packageName;
                if (this.mPackageManager.checkSignatures(str, paramString2) != 0)
                {
                    Slog.w("LocationManagerService", str + " implements " + paramString1 + " but its signatures don't match those in " + paramString2 + ", ignoring");
                }
                else
                {
                    int j = 0;
                    if (localResolveInfo.serviceInfo.metaData != null)
                        j = localResolveInfo.serviceInfo.metaData.getInt("version", 0);
                    if (j > i)
                    {
                        i = j;
                        localObject = str;
                    }
                }
            }
        }
    }

    public boolean geocoderIsPresent()
    {
        if (this.mGeocodeProvider != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public List<String> getAllProviders()
    {
        List localList;
        try
        {
            synchronized (this.mLock)
            {
                localList = _getAllProvidersLocked();
            }
        }
        catch (SecurityException localSecurityException)
        {
            throw localSecurityException;
        }
        catch (Exception localException)
        {
            Slog.e("LocationManagerService", "getAllProviders got exception:", localException);
            localList = null;
        }
        return localList;
    }

    public String getBestProvider(Criteria paramCriteria, boolean paramBoolean)
    {
        List localList1 = getProviders(paramCriteria, paramBoolean);
        String str;
        if (!localList1.isEmpty())
            str = best(localList1).getName();
        while (true)
        {
            return str;
            Criteria localCriteria = new Criteria(paramCriteria);
            int i = localCriteria.getPowerRequirement();
            while ((localList1.isEmpty()) && (i != 0))
            {
                i = nextPower(i);
                localCriteria.setPowerRequirement(i);
                localList1 = getProviders(localCriteria, paramBoolean);
            }
            if (!localList1.isEmpty())
            {
                str = best(localList1).getName();
            }
            else
            {
                int j = localCriteria.getAccuracy();
                while ((localList1.isEmpty()) && (j != 0))
                {
                    j = nextAccuracy(j);
                    localCriteria.setAccuracy(j);
                    localList1 = getProviders(localCriteria, paramBoolean);
                }
                if (!localList1.isEmpty())
                {
                    str = best(localList1).getName();
                }
                else
                {
                    localCriteria.setBearingRequired(false);
                    List localList2 = getProviders(localCriteria, paramBoolean);
                    if (!localList2.isEmpty())
                    {
                        str = best(localList2).getName();
                    }
                    else
                    {
                        localCriteria.setSpeedRequired(false);
                        List localList3 = getProviders(localCriteria, paramBoolean);
                        if (!localList3.isEmpty())
                        {
                            str = best(localList3).getName();
                        }
                        else
                        {
                            localCriteria.setAltitudeRequired(false);
                            List localList4 = getProviders(localCriteria, paramBoolean);
                            if (!localList4.isEmpty())
                                str = best(localList4).getName();
                            else
                                str = null;
                        }
                    }
                }
            }
        }
    }

    public String getFromLocation(double paramDouble1, double paramDouble2, int paramInt, GeocoderParams paramGeocoderParams, List<Address> paramList)
    {
        if (this.mGeocodeProvider != null);
        for (String str = this.mGeocodeProvider.getFromLocation(paramDouble1, paramDouble2, paramInt, paramGeocoderParams, paramList); ; str = null)
            return str;
    }

    public String getFromLocationName(String paramString, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, int paramInt, GeocoderParams paramGeocoderParams, List<Address> paramList)
    {
        if (this.mGeocodeProvider != null);
        for (String str = this.mGeocodeProvider.getFromLocationName(paramString, paramDouble1, paramDouble2, paramDouble3, paramDouble4, paramInt, paramGeocoderParams, paramList); ; str = null)
            return str;
    }

    public Location getLastKnownLocation(String paramString1, String paramString2)
    {
        Location localLocation;
        try
        {
            synchronized (this.mLock)
            {
                localLocation = _getLastKnownLocationLocked(paramString1, paramString2);
            }
        }
        catch (SecurityException localSecurityException)
        {
            throw localSecurityException;
        }
        catch (Exception localException)
        {
            Slog.e("LocationManagerService", "getLastKnownLocation got exception:", localException);
            localLocation = null;
        }
        return localLocation;
    }

    public Bundle getProviderInfo(String paramString)
    {
        Bundle localBundle;
        try
        {
            synchronized (this.mLock)
            {
                localBundle = _getProviderInfoLocked(paramString);
            }
        }
        catch (SecurityException localSecurityException)
        {
            throw localSecurityException;
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            throw localIllegalArgumentException;
        }
        catch (Exception localException)
        {
            Slog.e("LocationManagerService", "_getProviderInfo got exception:", localException);
            localBundle = null;
        }
        return localBundle;
    }

    public List<String> getProviders(Criteria paramCriteria, boolean paramBoolean)
    {
        List localList;
        try
        {
            synchronized (this.mLock)
            {
                localList = _getProvidersLocked(paramCriteria, paramBoolean);
            }
        }
        catch (SecurityException localSecurityException)
        {
            throw localSecurityException;
        }
        catch (Exception localException)
        {
            Slog.e("LocationManagerService", "getProviders got exception:", localException);
            localList = null;
        }
        return localList;
    }

    public boolean isProviderEnabled(String paramString)
    {
        boolean bool;
        try
        {
            synchronized (this.mLock)
            {
                bool = _isProviderEnabledLocked(paramString);
            }
        }
        catch (SecurityException localSecurityException)
        {
            throw localSecurityException;
        }
        catch (Exception localException)
        {
            Slog.e("LocationManagerService", "isProviderEnabled got exception:", localException);
            bool = false;
        }
        return bool;
    }

    public void locationCallbackFinished(ILocationListener paramILocationListener)
    {
        IBinder localIBinder = paramILocationListener.asBinder();
        Receiver localReceiver = (Receiver)this.mReceivers.get(localIBinder);
        if (localReceiver != null)
            try
            {
                long l = Binder.clearCallingIdentity();
                localReceiver.decrementPendingBroadcastsLocked();
                Binder.restoreCallingIdentity(l);
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
    }

    public boolean providerMeetsCriteria(String paramString, Criteria paramCriteria)
    {
        LocationProviderInterface localLocationProviderInterface = (LocationProviderInterface)this.mProvidersByName.get(paramString);
        if (localLocationProviderInterface == null)
            throw new IllegalArgumentException("provider=" + paramString);
        return localLocationProviderInterface.meetsCriteria(paramCriteria);
    }

    public void removeGpsStatusListener(IGpsStatusListener paramIGpsStatusListener)
    {
        synchronized (this.mLock)
        {
            try
            {
                this.mGpsStatusProvider.removeGpsStatusListener(paramIGpsStatusListener);
                return;
            }
            catch (Exception localException)
            {
                while (true)
                    Slog.e("LocationManagerService", "mGpsStatusProvider.removeGpsStatusListener failed", localException);
            }
        }
    }

    public void removeProximityAlert(PendingIntent paramPendingIntent)
    {
        try
        {
            synchronized (this.mLock)
            {
                removeProximityAlertLocked(paramPendingIntent);
            }
        }
        catch (SecurityException localSecurityException)
        {
            throw localSecurityException;
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            throw localIllegalArgumentException;
        }
        catch (Exception localException)
        {
            Slog.e("LocationManagerService", "removeProximityAlert got exception:", localException);
        }
    }

    // ERROR //
    public void removeTestProvider(String paramString)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 1066	com/android/server/LocationManagerService:checkMockPermissionsSafe	()V
        //     4: aload_0
        //     5: getfield 179	com/android/server/LocationManagerService:mLock	Ljava/lang/Object;
        //     8: astore_2
        //     9: aload_2
        //     10: monitorenter
        //     11: aload_0
        //     12: getfield 163	com/android/server/LocationManagerService:mMockProviders	Ljava/util/HashMap;
        //     15: aload_1
        //     16: invokevirtual 279	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     19: checkcast 1070	com/android/server/location/MockProvider
        //     22: astore 4
        //     24: aload 4
        //     26: ifnonnull +42 -> 68
        //     29: new 980	java/lang/IllegalArgumentException
        //     32: dup
        //     33: new 524	java/lang/StringBuilder
        //     36: dup
        //     37: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     40: ldc_w 1077
        //     43: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     46: aload_1
        //     47: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     50: ldc_w 1082
        //     53: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     56: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     59: invokespecial 985	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     62: athrow
        //     63: astore_3
        //     64: aload_2
        //     65: monitorexit
        //     66: aload_3
        //     67: athrow
        //     68: invokestatic 922	android/os/Binder:clearCallingIdentity	()J
        //     71: lstore 5
        //     73: aload_0
        //     74: aload_0
        //     75: getfield 174	com/android/server/LocationManagerService:mProvidersByName	Ljava/util/HashMap;
        //     78: aload_1
        //     79: invokevirtual 279	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     82: checkcast 253	com/android/server/location/LocationProviderInterface
        //     85: invokespecial 1075	com/android/server/LocationManagerService:removeProvider	(Lcom/android/server/location/LocationProviderInterface;)V
        //     88: aload_0
        //     89: getfield 163	com/android/server/LocationManagerService:mMockProviders	Ljava/util/HashMap;
        //     92: aload 4
        //     94: invokevirtual 915	java/util/HashMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
        //     97: pop
        //     98: ldc_w 492
        //     101: aload_1
        //     102: invokevirtual 601	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     105: ifeq +40 -> 145
        //     108: aload_0
        //     109: getfield 384	com/android/server/LocationManagerService:mGpsLocationProvider	Lcom/android/server/location/LocationProviderInterface;
        //     112: ifnull +33 -> 145
        //     115: aload_0
        //     116: aload_0
        //     117: getfield 384	com/android/server/LocationManagerService:mGpsLocationProvider	Lcom/android/server/location/LocationProviderInterface;
        //     120: invokespecial 382	com/android/server/LocationManagerService:addProvider	(Lcom/android/server/location/LocationProviderInterface;)V
        //     123: aload_0
        //     124: getfield 196	com/android/server/LocationManagerService:mLastKnownLocation	Ljava/util/HashMap;
        //     127: aload_1
        //     128: aconst_null
        //     129: invokevirtual 488	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     132: pop
        //     133: aload_0
        //     134: invokespecial 419	com/android/server/LocationManagerService:updateProvidersLocked	()V
        //     137: lload 5
        //     139: invokestatic 948	android/os/Binder:restoreCallingIdentity	(J)V
        //     142: aload_2
        //     143: monitorexit
        //     144: return
        //     145: ldc_w 295
        //     148: aload_1
        //     149: invokevirtual 601	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     152: ifeq -29 -> 123
        //     155: aload_0
        //     156: getfield 407	com/android/server/LocationManagerService:mNetworkLocationProvider	Lcom/android/server/location/LocationProviderProxy;
        //     159: ifnull -36 -> 123
        //     162: aload_0
        //     163: aload_0
        //     164: getfield 407	com/android/server/LocationManagerService:mNetworkLocationProvider	Lcom/android/server/location/LocationProviderProxy;
        //     167: invokespecial 382	com/android/server/LocationManagerService:addProvider	(Lcom/android/server/location/LocationProviderInterface;)V
        //     170: goto -47 -> 123
        //
        // Exception table:
        //     from	to	target	type
        //     11	66	63	finally
        //     68	170	63	finally
    }

    public void removeUpdates(ILocationListener paramILocationListener, String paramString)
    {
        try
        {
            synchronized (this.mLock)
            {
                removeUpdatesLocked(getReceiver(paramILocationListener, paramString));
            }
        }
        catch (SecurityException localSecurityException)
        {
            throw localSecurityException;
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            throw localIllegalArgumentException;
        }
        catch (Exception localException)
        {
            Slog.e("LocationManagerService", "removeUpdates got exception:", localException);
        }
    }

    public void removeUpdatesPI(PendingIntent paramPendingIntent, String paramString)
    {
        try
        {
            synchronized (this.mLock)
            {
                removeUpdatesLocked(getReceiver(paramPendingIntent, paramString));
            }
        }
        catch (SecurityException localSecurityException)
        {
            throw localSecurityException;
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            throw localIllegalArgumentException;
        }
        catch (Exception localException)
        {
            Slog.e("LocationManagerService", "removeUpdates got exception:", localException);
        }
    }

    public void reportLocation(Location paramLocation, boolean paramBoolean)
    {
        int i = 1;
        if (this.mContext.checkCallingOrSelfPermission("android.permission.INSTALL_LOCATION_PROVIDER") != 0)
            throw new SecurityException("Requires INSTALL_LOCATION_PROVIDER permission");
        this.mLocationHandler.removeMessages(i, paramLocation);
        Message localMessage = Message.obtain(this.mLocationHandler, i, paramLocation);
        if (paramBoolean);
        while (true)
        {
            localMessage.arg1 = i;
            this.mLocationHandler.sendMessageAtFrontOfQueue(localMessage);
            return;
            i = 0;
        }
    }

    public void requestLocationUpdates(String paramString1, Criteria paramCriteria, long paramLong, float paramFloat, boolean paramBoolean, ILocationListener paramILocationListener, String paramString2)
    {
        checkPackageName(Binder.getCallingUid(), paramString2);
        if (paramCriteria != null)
        {
            paramString1 = getBestProvider(paramCriteria, true);
            if (paramString1 == null)
                throw new IllegalArgumentException("no providers found for criteria");
        }
        try
        {
            synchronized (this.mLock)
            {
                Receiver localReceiver = getReceiver(paramILocationListener, paramString2);
                requestLocationUpdatesLocked(paramString1, paramLong, paramFloat, paramBoolean, localReceiver);
            }
        }
        catch (SecurityException localSecurityException)
        {
            throw localSecurityException;
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            throw localIllegalArgumentException;
        }
        catch (Exception localException)
        {
            Slog.e("LocationManagerService", "requestUpdates got exception:", localException);
        }
    }

    public void requestLocationUpdatesPI(String paramString1, Criteria paramCriteria, long paramLong, float paramFloat, boolean paramBoolean, PendingIntent paramPendingIntent, String paramString2)
    {
        checkPackageName(Binder.getCallingUid(), paramString2);
        validatePendingIntent(paramPendingIntent);
        if (paramCriteria != null)
        {
            paramString1 = getBestProvider(paramCriteria, true);
            if (paramString1 == null)
                throw new IllegalArgumentException("no providers found for criteria");
        }
        try
        {
            synchronized (this.mLock)
            {
                Receiver localReceiver = getReceiver(paramPendingIntent, paramString2);
                requestLocationUpdatesLocked(paramString1, paramLong, paramFloat, paramBoolean, localReceiver);
            }
        }
        catch (SecurityException localSecurityException)
        {
            throw localSecurityException;
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            throw localIllegalArgumentException;
        }
        catch (Exception localException)
        {
            Slog.e("LocationManagerService", "requestUpdates got exception:", localException);
        }
    }

    public void run()
    {
        Process.setThreadPriority(10);
        Looper.prepare();
        this.mLocationHandler = new LocationWorkerHandler(null);
        initialize();
        Looper.loop();
    }

    public boolean sendExtraCommand(String paramString1, String paramString2, Bundle paramBundle)
    {
        if (paramString1 == null)
            throw new NullPointerException();
        checkPermissionsSafe(paramString1, null);
        if (this.mContext.checkCallingOrSelfPermission("android.permission.ACCESS_LOCATION_EXTRA_COMMANDS") != 0)
            throw new SecurityException("Requires ACCESS_LOCATION_EXTRA_COMMANDS permission");
        boolean bool;
        synchronized (this.mLock)
        {
            LocationProviderInterface localLocationProviderInterface = (LocationProviderInterface)this.mProvidersByName.get(paramString1);
            if (localLocationProviderInterface == null)
                bool = false;
            else
                bool = localLocationProviderInterface.sendExtraCommand(paramString2, paramBundle);
        }
        return bool;
    }

    public boolean sendNiResponse(int paramInt1, int paramInt2)
    {
        if (Binder.getCallingUid() != Process.myUid())
            throw new SecurityException("calling sendNiResponse from outside of the system is not allowed");
        try
        {
            boolean bool2 = this.mNetInitiatedListener.sendNiResponse(paramInt1, paramInt2);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Slog.e("LocationManagerService", "RemoteException in LocationManagerService.sendNiResponse");
                boolean bool1 = false;
            }
        }
    }

    // ERROR //
    public void setTestProviderEnabled(String paramString, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 1066	com/android/server/LocationManagerService:checkMockPermissionsSafe	()V
        //     4: aload_0
        //     5: getfield 179	com/android/server/LocationManagerService:mLock	Ljava/lang/Object;
        //     8: astore_3
        //     9: aload_3
        //     10: monitorenter
        //     11: aload_0
        //     12: getfield 163	com/android/server/LocationManagerService:mMockProviders	Ljava/util/HashMap;
        //     15: aload_1
        //     16: invokevirtual 279	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     19: checkcast 1070	com/android/server/location/MockProvider
        //     22: astore 5
        //     24: aload 5
        //     26: ifnonnull +44 -> 70
        //     29: new 980	java/lang/IllegalArgumentException
        //     32: dup
        //     33: new 524	java/lang/StringBuilder
        //     36: dup
        //     37: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     40: ldc_w 1077
        //     43: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     46: aload_1
        //     47: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     50: ldc_w 1082
        //     53: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     56: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     59: invokespecial 985	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     62: athrow
        //     63: astore 4
        //     65: aload_3
        //     66: monitorexit
        //     67: aload 4
        //     69: athrow
        //     70: invokestatic 922	android/os/Binder:clearCallingIdentity	()J
        //     73: lstore 6
        //     75: iload_2
        //     76: ifeq +42 -> 118
        //     79: aload 5
        //     81: invokevirtual 1392	com/android/server/location/MockProvider:enable	()V
        //     84: aload_0
        //     85: getfield 159	com/android/server/LocationManagerService:mEnabledProviders	Ljava/util/Set;
        //     88: aload_1
        //     89: invokeinterface 393 2 0
        //     94: pop
        //     95: aload_0
        //     96: getfield 161	com/android/server/LocationManagerService:mDisabledProviders	Ljava/util/Set;
        //     99: aload_1
        //     100: invokeinterface 1083 2 0
        //     105: pop
        //     106: aload_0
        //     107: invokespecial 419	com/android/server/LocationManagerService:updateProvidersLocked	()V
        //     110: lload 6
        //     112: invokestatic 948	android/os/Binder:restoreCallingIdentity	(J)V
        //     115: aload_3
        //     116: monitorexit
        //     117: return
        //     118: aload 5
        //     120: invokevirtual 1393	com/android/server/location/MockProvider:disable	()V
        //     123: aload_0
        //     124: getfield 159	com/android/server/LocationManagerService:mEnabledProviders	Ljava/util/Set;
        //     127: aload_1
        //     128: invokeinterface 1083 2 0
        //     133: pop
        //     134: aload_0
        //     135: getfield 161	com/android/server/LocationManagerService:mDisabledProviders	Ljava/util/Set;
        //     138: aload_1
        //     139: invokeinterface 393 2 0
        //     144: pop
        //     145: goto -39 -> 106
        //
        // Exception table:
        //     from	to	target	type
        //     11	67	63	finally
        //     70	145	63	finally
    }

    // ERROR //
    public void setTestProviderLocation(String paramString, Location paramLocation)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 1066	com/android/server/LocationManagerService:checkMockPermissionsSafe	()V
        //     4: aload_0
        //     5: getfield 179	com/android/server/LocationManagerService:mLock	Ljava/lang/Object;
        //     8: astore_3
        //     9: aload_3
        //     10: monitorenter
        //     11: aload_0
        //     12: getfield 163	com/android/server/LocationManagerService:mMockProviders	Ljava/util/HashMap;
        //     15: aload_1
        //     16: invokevirtual 279	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     19: checkcast 1070	com/android/server/location/MockProvider
        //     22: astore 5
        //     24: aload 5
        //     26: ifnonnull +44 -> 70
        //     29: new 980	java/lang/IllegalArgumentException
        //     32: dup
        //     33: new 524	java/lang/StringBuilder
        //     36: dup
        //     37: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     40: ldc_w 1077
        //     43: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     46: aload_1
        //     47: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     50: ldc_w 1082
        //     53: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     56: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     59: invokespecial 985	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     62: athrow
        //     63: astore 4
        //     65: aload_3
        //     66: monitorexit
        //     67: aload 4
        //     69: athrow
        //     70: invokestatic 922	android/os/Binder:clearCallingIdentity	()J
        //     73: lstore 6
        //     75: aload 5
        //     77: aload_2
        //     78: invokevirtual 1398	com/android/server/location/MockProvider:setLocation	(Landroid/location/Location;)V
        //     81: lload 6
        //     83: invokestatic 948	android/os/Binder:restoreCallingIdentity	(J)V
        //     86: aload_3
        //     87: monitorexit
        //     88: return
        //
        // Exception table:
        //     from	to	target	type
        //     11	67	63	finally
        //     70	88	63	finally
    }

    // ERROR //
    public void setTestProviderStatus(String paramString, int paramInt, Bundle paramBundle, long paramLong)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 1066	com/android/server/LocationManagerService:checkMockPermissionsSafe	()V
        //     4: aload_0
        //     5: getfield 179	com/android/server/LocationManagerService:mLock	Ljava/lang/Object;
        //     8: astore 6
        //     10: aload 6
        //     12: monitorenter
        //     13: aload_0
        //     14: getfield 163	com/android/server/LocationManagerService:mMockProviders	Ljava/util/HashMap;
        //     17: aload_1
        //     18: invokevirtual 279	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     21: checkcast 1070	com/android/server/location/MockProvider
        //     24: astore 8
        //     26: aload 8
        //     28: ifnonnull +45 -> 73
        //     31: new 980	java/lang/IllegalArgumentException
        //     34: dup
        //     35: new 524	java/lang/StringBuilder
        //     38: dup
        //     39: invokespecial 525	java/lang/StringBuilder:<init>	()V
        //     42: ldc_w 1077
        //     45: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     48: aload_1
        //     49: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     52: ldc_w 1082
        //     55: invokevirtual 532	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     58: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     61: invokespecial 985	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     64: athrow
        //     65: astore 7
        //     67: aload 6
        //     69: monitorexit
        //     70: aload 7
        //     72: athrow
        //     73: aload 8
        //     75: iload_2
        //     76: aload_3
        //     77: lload 4
        //     79: invokevirtual 1404	com/android/server/location/MockProvider:setStatus	(ILandroid/os/Bundle;J)V
        //     82: aload 6
        //     84: monitorexit
        //     85: return
        //
        // Exception table:
        //     from	to	target	type
        //     13	70	65	finally
        //     73	85	65	finally
    }

    void systemReady()
    {
        new Thread(null, this, "LocationManagerService").start();
    }

    void validatePendingIntent(PendingIntent paramPendingIntent)
    {
        if (paramPendingIntent.isTargetedToPackage());
        while (true)
        {
            return;
            Slog.i("LocationManagerService", "Given Intent does not require a specific package: " + paramPendingIntent);
        }
    }

    public class BlacklistObserver extends ContentObserver
    {
        public BlacklistObserver(Handler arg2)
        {
            super();
        }

        public void onChange(boolean paramBoolean)
        {
            LocationManagerService.this.reloadBlacklist();
        }
    }

    private class LocationWorkerHandler extends Handler
    {
        private LocationWorkerHandler()
        {
        }

        public void handleMessage(Message paramMessage)
        {
            int i = 1;
            try
            {
                if (paramMessage.what == i)
                    synchronized (LocationManagerService.this.mLock)
                    {
                        Location localLocation = (Location)paramMessage.obj;
                        String str4 = localLocation.getProvider();
                        if (paramMessage.arg1 == i)
                        {
                            if (i == 0)
                            {
                                k = -1 + LocationManagerService.this.mProviders.size();
                                if (k >= 0)
                                {
                                    LocationProviderInterface localLocationProviderInterface = (LocationProviderInterface)LocationManagerService.this.mProviders.get(k);
                                    if (str4.equals(localLocationProviderInterface.getName()))
                                        break label359;
                                    localLocationProviderInterface.updateLocation(localLocation);
                                    break label359;
                                }
                            }
                            if (LocationManagerService.this.isAllowedBySettingsLocked(str4))
                                LocationManagerService.this.handleLocationChangedLocked(localLocation, i);
                        }
                    }
            }
            catch (Exception localException)
            {
                while (true)
                {
                    int k;
                    Slog.e("LocationManagerService", "Exception in LocationWorkerHandler.handleMessage:", localException);
                    break;
                    if (paramMessage.what != 2)
                        break;
                    String str1 = (String)paramMessage.obj;
                    if ((LocationManagerService.this.mNetworkLocationProviderPackageName != null) && (LocationManagerService.this.mPackageManager.resolveService(new Intent("com.android.location.service.NetworkLocationProvider").setPackage(str1), 0) != null))
                    {
                        String str3 = LocationManagerService.this.findBestPackage("com.android.location.service.NetworkLocationProvider", LocationManagerService.this.mNetworkLocationProviderPackageName);
                        if (str1.equals(str3))
                        {
                            LocationManagerService.this.mNetworkLocationProvider.reconnect(str3);
                            LocationManagerService.access$2202(LocationManagerService.this, str1);
                        }
                    }
                    if ((LocationManagerService.this.mGeocodeProviderPackageName == null) || (LocationManagerService.this.mPackageManager.resolveService(new Intent("com.android.location.service.GeocodeProvider").setPackage(str1), 0) == null))
                        break;
                    String str2 = LocationManagerService.this.findBestPackage("com.android.location.service.GeocodeProvider", LocationManagerService.this.mGeocodeProviderPackageName);
                    if (!str1.equals(str2))
                        break;
                    LocationManagerService.this.mGeocodeProvider.reconnect(str2);
                    LocationManagerService.access$2402(LocationManagerService.this, str1);
                    break;
                    label359: k--;
                    continue;
                    int j = 0;
                }
            }
        }
    }

    class ProximityListener extends ILocationListener.Stub
        implements PendingIntent.OnFinished
    {
        boolean isGpsAvailable = false;

        ProximityListener()
        {
        }

        public void onLocationChanged(Location paramLocation)
        {
            if (paramLocation.getProvider().equals("gps"))
                this.isGpsAvailable = true;
            if ((this.isGpsAvailable) && (paramLocation.getProvider().equals("network")));
            while (true)
            {
                return;
                long l1 = System.currentTimeMillis();
                double d1 = paramLocation.getLatitude();
                double d2 = paramLocation.getLongitude();
                float f = paramLocation.getAccuracy();
                ArrayList localArrayList = null;
                Iterator localIterator1 = LocationManagerService.this.mProximityAlerts.values().iterator();
                while (localIterator1.hasNext())
                {
                    LocationManagerService.ProximityAlert localProximityAlert2 = (LocationManagerService.ProximityAlert)localIterator1.next();
                    PendingIntent localPendingIntent2 = localProximityAlert2.getIntent();
                    long l2 = localProximityAlert2.getExpiration();
                    if (!LocationManagerService.this.inBlacklist(localProximityAlert2.mPackageName))
                        if ((l2 == -1L) || (l1 <= l2))
                        {
                            boolean bool1 = LocationManagerService.this.mProximitiesEntered.contains(localProximityAlert2);
                            boolean bool2 = localProximityAlert2.isInProximity(d1, d2, f);
                            if ((!bool1) && (bool2))
                            {
                                LocationManagerService.this.mProximitiesEntered.add(localProximityAlert2);
                                Intent localIntent2 = new Intent();
                                localIntent2.putExtra("entering", true);
                                try
                                {
                                    try
                                    {
                                        localPendingIntent2.send(LocationManagerService.this.mContext, 0, localIntent2, this, LocationManagerService.this.mLocationHandler, "android.permission.ACCESS_FINE_LOCATION");
                                        LocationManagerService.this.incrementPendingBroadcasts();
                                        continue;
                                    }
                                    finally
                                    {
                                        localObject2 = finally;
                                        throw localObject2;
                                    }
                                }
                                catch (PendingIntent.CanceledException localCanceledException2)
                                {
                                    if (localArrayList == null)
                                        localArrayList = new ArrayList();
                                    localArrayList.add(localPendingIntent2);
                                }
                            }
                            else if ((bool1) && (!bool2))
                            {
                                LocationManagerService.this.mProximitiesEntered.remove(localProximityAlert2);
                                Intent localIntent1 = new Intent();
                                localIntent1.putExtra("entering", false);
                                try
                                {
                                    try
                                    {
                                        localPendingIntent2.send(LocationManagerService.this.mContext, 0, localIntent1, this, LocationManagerService.this.mLocationHandler, "android.permission.ACCESS_FINE_LOCATION");
                                        LocationManagerService.this.incrementPendingBroadcasts();
                                        continue;
                                    }
                                    finally
                                    {
                                        localObject1 = finally;
                                        throw localObject1;
                                    }
                                }
                                catch (PendingIntent.CanceledException localCanceledException1)
                                {
                                    if (localArrayList == null)
                                        localArrayList = new ArrayList();
                                    localArrayList.add(localPendingIntent2);
                                }
                            }
                        }
                        else
                        {
                            if (localArrayList == null)
                                localArrayList = new ArrayList();
                            PendingIntent localPendingIntent3 = localProximityAlert2.getIntent();
                            localArrayList.add(localPendingIntent3);
                        }
                }
                if (localArrayList != null)
                {
                    Iterator localIterator2 = localArrayList.iterator();
                    while (localIterator2.hasNext())
                    {
                        PendingIntent localPendingIntent1 = (PendingIntent)localIterator2.next();
                        LocationManagerService.ProximityAlert localProximityAlert1 = (LocationManagerService.ProximityAlert)LocationManagerService.this.mProximityAlerts.get(localPendingIntent1);
                        LocationManagerService.this.mProximitiesEntered.remove(localProximityAlert1);
                        LocationManagerService.this.removeProximityAlertLocked(localPendingIntent1);
                    }
                }
            }
        }

        public void onProviderDisabled(String paramString)
        {
            if (paramString.equals("gps"))
                this.isGpsAvailable = false;
        }

        public void onProviderEnabled(String paramString)
        {
        }

        public void onSendFinished(PendingIntent paramPendingIntent, Intent paramIntent, int paramInt, String paramString, Bundle paramBundle)
        {
            try
            {
                LocationManagerService.this.decrementPendingBroadcasts();
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public void onStatusChanged(String paramString, int paramInt, Bundle paramBundle)
        {
            if ((paramString.equals("gps")) && (paramInt != 2))
                this.isGpsAvailable = false;
        }
    }

    class ProximityAlert
    {
        final long mExpiration;
        final PendingIntent mIntent;
        final double mLatitude;
        final Location mLocation;
        final double mLongitude;
        final String mPackageName;
        final float mRadius;
        final int mUid;

        public ProximityAlert(int paramDouble1, double arg3, double arg5, float paramLong, long arg8, PendingIntent paramString, String arg11)
        {
            this.mUid = paramDouble1;
            this.mLatitude = ???;
            this.mLongitude = ???;
            this.mRadius = paramLong;
            Object localObject1;
            this.mExpiration = localObject1;
            this.mIntent = paramString;
            Object localObject2;
            this.mPackageName = localObject2;
            this.mLocation = new Location("");
            this.mLocation.setLatitude(???);
            this.mLocation.setLongitude(???);
        }

        void dump(PrintWriter paramPrintWriter, String paramString)
        {
            paramPrintWriter.println(paramString + this);
            paramPrintWriter.println(paramString + "mLatitude=" + this.mLatitude + " mLongitude=" + this.mLongitude);
            paramPrintWriter.println(paramString + "mRadius=" + this.mRadius + " mExpiration=" + this.mExpiration);
            paramPrintWriter.println(paramString + "mIntent=" + this.mIntent);
            paramPrintWriter.println(paramString + "mLocation:");
            this.mLocation.dump(new PrintWriterPrinter(paramPrintWriter), paramString + "    ");
        }

        long getExpiration()
        {
            return this.mExpiration;
        }

        PendingIntent getIntent()
        {
            return this.mIntent;
        }

        boolean isInProximity(double paramDouble1, double paramDouble2, float paramFloat)
        {
            Location localLocation = new Location("");
            localLocation.setLatitude(paramDouble1);
            localLocation.setLongitude(paramDouble2);
            if (localLocation.distanceTo(this.mLocation) <= Math.max(this.mRadius, paramFloat));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public String toString()
        {
            return "ProximityAlert{" + Integer.toHexString(System.identityHashCode(this)) + " uid " + this.mUid + this.mIntent + "}";
        }
    }

    private class UpdateRecord
    {
        Location mLastFixBroadcast;
        long mLastStatusBroadcast;
        final float mMinDistance;
        final long mMinTime;
        final String mProvider;
        final LocationManagerService.Receiver mReceiver;
        final boolean mSingleShot;
        final int mUid;

        UpdateRecord(String paramLong, long arg3, float paramBoolean, boolean paramReceiver, LocationManagerService.Receiver paramInt, int arg8)
        {
            this.mProvider = paramLong;
            this.mReceiver = paramInt;
            this.mMinTime = ???;
            this.mMinDistance = paramBoolean;
            this.mSingleShot = paramReceiver;
            int i;
            this.mUid = i;
            ArrayList localArrayList = (ArrayList)LocationManagerService.this.mRecordsByProvider.get(paramLong);
            if (localArrayList == null)
            {
                localArrayList = new ArrayList();
                LocationManagerService.this.mRecordsByProvider.put(paramLong, localArrayList);
            }
            if (!localArrayList.contains(this))
                localArrayList.add(this);
        }

        void disposeLocked()
        {
            ArrayList localArrayList = (ArrayList)LocationManagerService.this.mRecordsByProvider.get(this.mProvider);
            if (localArrayList != null)
                localArrayList.remove(this);
        }

        void dump(PrintWriter paramPrintWriter, String paramString)
        {
            paramPrintWriter.println(paramString + this);
            paramPrintWriter.println(paramString + "mProvider=" + this.mProvider + " mReceiver=" + this.mReceiver);
            paramPrintWriter.println(paramString + "mMinTime=" + this.mMinTime + " mMinDistance=" + this.mMinDistance);
            paramPrintWriter.println(paramString + "mSingleShot=" + this.mSingleShot);
            paramPrintWriter.println(paramString + "mUid=" + this.mUid);
            paramPrintWriter.println(paramString + "mLastFixBroadcast:");
            if (this.mLastFixBroadcast != null)
                this.mLastFixBroadcast.dump(new PrintWriterPrinter(paramPrintWriter), paramString + "    ");
            paramPrintWriter.println(paramString + "mLastStatusBroadcast=" + this.mLastStatusBroadcast);
        }

        public String toString()
        {
            return "UpdateRecord{" + Integer.toHexString(System.identityHashCode(this)) + " mProvider: " + this.mProvider + " mUid: " + this.mUid + "}";
        }
    }

    private class LpCapabilityComparator
        implements Comparator<LocationProviderInterface>
    {
        private static final int ALTITUDE_SCORE = 4;
        private static final int BEARING_SCORE = 4;
        private static final int SPEED_SCORE = 4;

        private LpCapabilityComparator()
        {
        }

        private int score(LocationProviderInterface paramLocationProviderInterface)
        {
            int i = 4;
            int j;
            int k;
            label25: int m;
            if (paramLocationProviderInterface.supportsAltitude())
            {
                j = i;
                if (!paramLocationProviderInterface.supportsBearing())
                    break label50;
                k = i;
                m = j + k;
                if (!paramLocationProviderInterface.supportsSpeed())
                    break label56;
            }
            while (true)
            {
                return m + i;
                j = 0;
                break;
                label50: k = 0;
                break label25;
                label56: i = 0;
            }
        }

        public int compare(LocationProviderInterface paramLocationProviderInterface1, LocationProviderInterface paramLocationProviderInterface2)
        {
            return score(paramLocationProviderInterface2) - score(paramLocationProviderInterface1);
        }

        public boolean equals(LocationProviderInterface paramLocationProviderInterface1, LocationProviderInterface paramLocationProviderInterface2)
        {
            if (score(paramLocationProviderInterface1) == score(paramLocationProviderInterface2));
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }

    private class LpAccuracyComparator
        implements Comparator<LocationProviderInterface>
    {
        private LpAccuracyComparator()
        {
        }

        public int compare(LocationProviderInterface paramLocationProviderInterface1, LocationProviderInterface paramLocationProviderInterface2)
        {
            return paramLocationProviderInterface1.getAccuracy() - paramLocationProviderInterface2.getAccuracy();
        }

        public boolean equals(LocationProviderInterface paramLocationProviderInterface1, LocationProviderInterface paramLocationProviderInterface2)
        {
            if (paramLocationProviderInterface1.getAccuracy() == paramLocationProviderInterface2.getAccuracy());
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }

    private class LpPowerComparator
        implements Comparator<LocationProviderInterface>
    {
        private LpPowerComparator()
        {
        }

        public int compare(LocationProviderInterface paramLocationProviderInterface1, LocationProviderInterface paramLocationProviderInterface2)
        {
            return paramLocationProviderInterface1.getPowerRequirement() - paramLocationProviderInterface2.getPowerRequirement();
        }

        public boolean equals(LocationProviderInterface paramLocationProviderInterface1, LocationProviderInterface paramLocationProviderInterface2)
        {
            if (paramLocationProviderInterface1.getPowerRequirement() == paramLocationProviderInterface2.getPowerRequirement());
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }

    private final class SettingsObserver
        implements Observer
    {
        private SettingsObserver()
        {
        }

        public void update(Observable paramObservable, Object paramObject)
        {
            synchronized (LocationManagerService.this.mLock)
            {
                LocationManagerService.this.updateProvidersLocked();
                return;
            }
        }
    }

    private final class Receiver
        implements IBinder.DeathRecipient, PendingIntent.OnFinished
    {
        final Object mKey;
        final ILocationListener mListener;
        final String mPackageName;
        int mPendingBroadcasts;
        final PendingIntent mPendingIntent;
        String mRequiredPermissions;
        final HashMap<String, LocationManagerService.UpdateRecord> mUpdateRecords = new HashMap();

        Receiver(PendingIntent paramString, String arg3)
        {
            this.mPendingIntent = paramString;
            this.mListener = null;
            this.mKey = paramString;
            Object localObject;
            this.mPackageName = localObject;
        }

        Receiver(ILocationListener paramString, String arg3)
        {
            this.mListener = paramString;
            this.mPendingIntent = null;
            this.mKey = paramString.asBinder();
            Object localObject;
            this.mPackageName = localObject;
        }

        private void decrementPendingBroadcastsLocked()
        {
            int i = -1 + this.mPendingBroadcasts;
            this.mPendingBroadcasts = i;
            if (i == 0)
                LocationManagerService.this.decrementPendingBroadcasts();
        }

        private void incrementPendingBroadcastsLocked()
        {
            int i = this.mPendingBroadcasts;
            this.mPendingBroadcasts = (i + 1);
            if (i == 0)
                LocationManagerService.this.incrementPendingBroadcasts();
        }

        public void binderDied()
        {
            synchronized (LocationManagerService.this.mLock)
            {
                LocationManagerService.this.removeUpdatesLocked(this);
            }
            try
            {
                if (this.mPendingBroadcasts > 0)
                {
                    LocationManagerService.this.decrementPendingBroadcasts();
                    this.mPendingBroadcasts = 0;
                }
                return;
                localObject2 = finally;
                throw localObject2;
            }
            finally
            {
            }
        }

        public boolean callLocationChangedLocked(Location paramLocation)
        {
            if (this.mListener != null)
            {
                try
                {
                    try
                    {
                        this.mListener.onLocationChanged(paramLocation);
                        if (this.mListener != LocationManagerService.this.mProximityListener)
                            incrementPendingBroadcastsLocked();
                        break label127;
                    }
                    finally
                    {
                        localObject2 = finally;
                        throw localObject2;
                    }
                }
                catch (RemoteException localRemoteException)
                {
                    bool = false;
                }
            }
            else
            {
                Intent localIntent = new Intent();
                localIntent.putExtra("location", paramLocation);
                try
                {
                    try
                    {
                        this.mPendingIntent.send(LocationManagerService.this.mContext, 0, localIntent, this, LocationManagerService.this.mLocationHandler, this.mRequiredPermissions);
                        incrementPendingBroadcastsLocked();
                    }
                    finally
                    {
                        localObject1 = finally;
                        throw localObject1;
                    }
                }
                catch (PendingIntent.CanceledException localCanceledException)
                {
                    bool = false;
                }
            }
            label127: boolean bool = true;
            return bool;
        }

        // ERROR //
        public boolean callProviderEnabledLocked(String paramString, boolean paramBoolean)
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 43	com/android/server/LocationManagerService$Receiver:mListener	Landroid/location/ILocationListener;
            //     4: ifnull +70 -> 74
            //     7: aload_0
            //     8: monitorenter
            //     9: iload_2
            //     10: ifeq +36 -> 46
            //     13: aload_0
            //     14: getfield 43	com/android/server/LocationManagerService$Receiver:mListener	Landroid/location/ILocationListener;
            //     17: aload_1
            //     18: invokeinterface 125 2 0
            //     23: aload_0
            //     24: getfield 43	com/android/server/LocationManagerService$Receiver:mListener	Landroid/location/ILocationListener;
            //     27: aload_0
            //     28: getfield 31	com/android/server/LocationManagerService$Receiver:this$0	Lcom/android/server/LocationManagerService;
            //     31: invokestatic 92	com/android/server/LocationManagerService:access$000	(Lcom/android/server/LocationManagerService;)Landroid/location/ILocationListener;
            //     34: if_acmpeq +7 -> 41
            //     37: aload_0
            //     38: invokespecial 94	com/android/server/LocationManagerService$Receiver:incrementPendingBroadcastsLocked	()V
            //     41: aload_0
            //     42: monitorexit
            //     43: goto +101 -> 144
            //     46: aload_0
            //     47: getfield 43	com/android/server/LocationManagerService$Receiver:mListener	Landroid/location/ILocationListener;
            //     50: aload_1
            //     51: invokeinterface 128 2 0
            //     56: goto -33 -> 23
            //     59: astore 9
            //     61: aload_0
            //     62: monitorexit
            //     63: aload 9
            //     65: athrow
            //     66: astore 8
            //     68: iconst_0
            //     69: istore 6
            //     71: goto +76 -> 147
            //     74: new 96	android/content/Intent
            //     77: dup
            //     78: invokespecial 97	android/content/Intent:<init>	()V
            //     81: astore_3
            //     82: aload_3
            //     83: ldc 130
            //     85: iload_2
            //     86: invokevirtual 133	android/content/Intent:putExtra	(Ljava/lang/String;Z)Landroid/content/Intent;
            //     89: pop
            //     90: aload_0
            //     91: monitorenter
            //     92: aload_0
            //     93: getfield 41	com/android/server/LocationManagerService$Receiver:mPendingIntent	Landroid/app/PendingIntent;
            //     96: aload_0
            //     97: getfield 31	com/android/server/LocationManagerService$Receiver:this$0	Lcom/android/server/LocationManagerService;
            //     100: invokestatic 107	com/android/server/LocationManagerService:access$100	(Lcom/android/server/LocationManagerService;)Landroid/content/Context;
            //     103: iconst_0
            //     104: aload_3
            //     105: aload_0
            //     106: aload_0
            //     107: getfield 31	com/android/server/LocationManagerService$Receiver:this$0	Lcom/android/server/LocationManagerService;
            //     110: invokestatic 111	com/android/server/LocationManagerService:access$200	(Lcom/android/server/LocationManagerService;)Lcom/android/server/LocationManagerService$LocationWorkerHandler;
            //     113: aload_0
            //     114: getfield 113	com/android/server/LocationManagerService$Receiver:mRequiredPermissions	Ljava/lang/String;
            //     117: invokevirtual 119	android/app/PendingIntent:send	(Landroid/content/Context;ILandroid/content/Intent;Landroid/app/PendingIntent$OnFinished;Landroid/os/Handler;Ljava/lang/String;)V
            //     120: aload_0
            //     121: invokespecial 94	com/android/server/LocationManagerService$Receiver:incrementPendingBroadcastsLocked	()V
            //     124: aload_0
            //     125: monitorexit
            //     126: goto +18 -> 144
            //     129: astore 7
            //     131: aload_0
            //     132: monitorexit
            //     133: aload 7
            //     135: athrow
            //     136: astore 5
            //     138: iconst_0
            //     139: istore 6
            //     141: goto +6 -> 147
            //     144: iconst_1
            //     145: istore 6
            //     147: iload 6
            //     149: ireturn
            //
            // Exception table:
            //     from	to	target	type
            //     13	63	59	finally
            //     7	9	66	android/os/RemoteException
            //     63	66	66	android/os/RemoteException
            //     92	133	129	finally
            //     90	92	136	android/app/PendingIntent$CanceledException
            //     133	136	136	android/app/PendingIntent$CanceledException
        }

        public boolean callStatusChangedLocked(String paramString, int paramInt, Bundle paramBundle)
        {
            if (this.mListener != null)
            {
                try
                {
                    try
                    {
                        this.mListener.onStatusChanged(paramString, paramInt, paramBundle);
                        if (this.mListener != LocationManagerService.this.mProximityListener)
                            incrementPendingBroadcastsLocked();
                        break label139;
                    }
                    finally
                    {
                        localObject2 = finally;
                        throw localObject2;
                    }
                }
                catch (RemoteException localRemoteException)
                {
                    bool = false;
                }
            }
            else
            {
                Intent localIntent = new Intent();
                localIntent.putExtras(paramBundle);
                localIntent.putExtra("status", paramInt);
                try
                {
                    try
                    {
                        this.mPendingIntent.send(LocationManagerService.this.mContext, 0, localIntent, this, LocationManagerService.this.mLocationHandler, this.mRequiredPermissions);
                        incrementPendingBroadcastsLocked();
                    }
                    finally
                    {
                        localObject1 = finally;
                        throw localObject1;
                    }
                }
                catch (PendingIntent.CanceledException localCanceledException)
                {
                    bool = false;
                }
            }
            label139: boolean bool = true;
            return bool;
        }

        public boolean equals(Object paramObject)
        {
            if ((paramObject instanceof Receiver));
            for (boolean bool = this.mKey.equals(((Receiver)paramObject).mKey); ; bool = false)
                return bool;
        }

        public ILocationListener getListener()
        {
            if (this.mListener != null)
                return this.mListener;
            throw new IllegalStateException("Request for non-existent listener");
        }

        public PendingIntent getPendingIntent()
        {
            if (this.mPendingIntent != null)
                return this.mPendingIntent;
            throw new IllegalStateException("Request for non-existent intent");
        }

        public int hashCode()
        {
            return this.mKey.hashCode();
        }

        public boolean isListener()
        {
            if (this.mListener != null);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public boolean isPendingIntent()
        {
            if (this.mPendingIntent != null);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void onSendFinished(PendingIntent paramPendingIntent, Intent paramIntent, int paramInt, String paramString, Bundle paramBundle)
        {
            try
            {
                decrementPendingBroadcastsLocked();
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public String toString()
        {
            if (this.mListener != null);
            for (String str = "Receiver{" + Integer.toHexString(System.identityHashCode(this)) + " Listener " + this.mKey + "}"; ; str = "Receiver{" + Integer.toHexString(System.identityHashCode(this)) + " Intent " + this.mKey + "}")
                return str + "mUpdateRecords: " + this.mUpdateRecords;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.LocationManagerService
 * JD-Core Version:        0.6.2
 */