package com.android.server;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.bluetooth.BluetoothTetheringDataTracker;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.net.ConnectivityManager;
import android.net.DummyDataStateTracker;
import android.net.EthernetDataTracker;
import android.net.IConnectivityManager.Stub;
import android.net.INetworkPolicyListener;
import android.net.INetworkPolicyListener.Stub;
import android.net.INetworkPolicyManager;
import android.net.INetworkStatsService;
import android.net.LinkAddress;
import android.net.LinkProperties;
import android.net.LinkProperties.CompareResult;
import android.net.MobileDataStateTracker;
import android.net.NetworkConfig;
import android.net.NetworkInfo;
import android.net.NetworkInfo.DetailedState;
import android.net.NetworkInfo.State;
import android.net.NetworkQuotaInfo;
import android.net.NetworkState;
import android.net.NetworkStateTracker;
import android.net.NetworkUtils;
import android.net.ProxyProperties;
import android.net.RouteInfo;
import android.net.wifi.WifiStateTracker;
import android.os.Binder;
import android.os.FileUtils;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.INetworkManagementService;
import android.os.INetworkManagementService.Stub;
import android.os.Looper;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.util.EventLog;
import android.util.Slog;
import android.util.SparseIntArray;
import com.android.internal.app.IBatteryStats;
import com.android.internal.net.LegacyVpnInfo;
import com.android.internal.net.VpnConfig;
import com.android.server.am.BatteryStatsService;
import com.android.server.connectivity.Tethering;
import com.android.server.connectivity.Vpn;
import com.google.android.collect.Lists;
import com.google.android.collect.Sets;
import com.miui.server.FirewallService;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import miui.net.FirewallManager;

public class ConnectivityService extends IConnectivityManager.Stub
{
    private static final boolean ADD = true;
    private static final boolean DBG = true;
    private static final int DISABLED = 0;
    private static final int ENABLED = 1;
    private static final int EVENT_APPLY_GLOBAL_HTTP_PROXY = 109;
    private static final int EVENT_CHANGE_MOBILE_DATA_ENABLED = 102;
    private static final int EVENT_CLEAR_NET_TRANSITION_WAKELOCK = 108;
    private static final int EVENT_INET_CONDITION_CHANGE = 104;
    private static final int EVENT_INET_CONDITION_HOLD_END = 105;
    private static final int EVENT_RESTORE_DEFAULT_NETWORK = 101;
    private static final int EVENT_RESTORE_DNS = 111;
    private static final int EVENT_SEND_STICKY_BROADCAST_INTENT = 112;
    private static final int EVENT_SET_DEPENDENCY_MET = 110;
    private static final int EVENT_SET_MOBILE_DATA = 107;
    private static final int EVENT_SET_NETWORK_PREFERENCE = 103;
    private static final int EVENT_SET_POLICY_DATA_ENABLE = 113;
    private static final int INET_CONDITION_LOG_MAX_SIZE = 15;
    private static final boolean LOGD_RULES = false;
    private static final int MAX_HOSTROUTE_CYCLE_COUNT = 10;
    private static final int MAX_NETWORK_STATE_TRACKER_EVENT = 100;
    private static final int MIN_NETWORK_STATE_TRACKER_EVENT = 1;
    private static final String NETWORK_RESTORE_DELAY_PROP_NAME = "android.telephony.apn-restore";
    private static final boolean REMOVE = false;
    private static final int RESTORE_DEFAULT_NETWORK_DELAY = 60000;
    private static final String TAG = "ConnectivityService";
    private static final boolean TO_DEFAULT_TABLE = true;
    private static final boolean TO_SECONDARY_TABLE;
    private static final boolean VDBG;
    private static ConnectivityService sServiceInstance;
    private int mActiveDefaultNetwork = -1;
    private Collection<RouteInfo> mAddedRoutes = new ArrayList();
    private Context mContext;
    private LinkProperties[] mCurrentLinkProperties;
    private int mDefaultConnectionSequence = 0;
    private InetAddress mDefaultDns;
    private int mDefaultInetCondition = 0;
    private int mDefaultInetConditionPublished = 0;
    private ProxyProperties mDefaultProxy = null;
    private boolean mDefaultProxyDisabled = false;
    private Object mDefaultProxyLock = new Object();
    private Object mDnsLock = new Object();
    private boolean mDnsOverridden = false;
    private List<FeatureUser> mFeatureUsers;
    private ProxyProperties mGlobalProxy = null;
    private final Object mGlobalProxyLock = new Object();
    private Handler mHandler;
    private boolean mInetConditionChangeInFlight = false;
    private ArrayList mInetLog;
    private Intent mInitialBroadcast;
    private HashSet<String> mMeteredIfaces = Sets.newHashSet();
    NetworkConfig[] mNetConfigs;
    private List[] mNetRequestersPids;
    private NetworkStateTracker[] mNetTrackers;
    private PowerManager.WakeLock mNetTransitionWakeLock;
    private String mNetTransitionWakeLockCausedBy = "";
    private int mNetTransitionWakeLockSerialNumber;
    private int mNetTransitionWakeLockTimeout;
    private INetworkManagementService mNetd;
    private int mNetworkPreference;
    int mNetworksDefined;
    private int mNumDnsEntries;
    private INetworkPolicyListener mPolicyListener = new INetworkPolicyListener.Stub()
    {
        public void onMeteredIfacesChanged(String[] paramAnonymousArrayOfString)
        {
            synchronized (ConnectivityService.this.mRulesLock)
            {
                ConnectivityService.this.mMeteredIfaces.clear();
                int i = paramAnonymousArrayOfString.length;
                for (int j = 0; j < i; j++)
                {
                    String str = paramAnonymousArrayOfString[j];
                    ConnectivityService.this.mMeteredIfaces.add(str);
                }
                return;
            }
        }

        public void onRestrictBackgroundChanged(boolean paramAnonymousBoolean)
        {
            int i = ConnectivityService.this.mActiveDefaultNetwork;
            if (ConnectivityManager.isNetworkTypeValid(i))
            {
                NetworkStateTracker localNetworkStateTracker = ConnectivityService.this.mNetTrackers[i];
                if (localNetworkStateTracker != null)
                {
                    NetworkInfo localNetworkInfo = localNetworkStateTracker.getNetworkInfo();
                    if ((localNetworkInfo != null) && (localNetworkInfo.isConnected()))
                        ConnectivityService.this.sendConnectedBroadcast(localNetworkInfo);
                }
            }
        }

        public void onUidRulesChanged(int paramAnonymousInt1, int paramAnonymousInt2)
        {
            synchronized (ConnectivityService.this.mRulesLock)
            {
                if (ConnectivityService.this.mUidRules.get(paramAnonymousInt1, 0) != paramAnonymousInt2)
                    ConnectivityService.this.mUidRules.put(paramAnonymousInt1, paramAnonymousInt2);
            }
        }
    };
    private INetworkPolicyManager mPolicyManager;
    private int[] mPriorityList;
    List mProtectedNetworks;
    RadioAttributes[] mRadioAttributes;
    private Object mRulesLock = new Object();
    private SettingsObserver mSettingsObserver;
    private boolean mSystemReady;
    private boolean mTestMode;
    private Tethering mTethering;
    private boolean mTetheringConfigValid = false;
    private SparseIntArray mUidRules = new SparseIntArray();
    private Vpn mVpn;

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public ConnectivityService(Context paramContext, INetworkManagementService paramINetworkManagementService, INetworkStatsService paramINetworkStatsService, INetworkPolicyManager paramINetworkPolicyManager)
    {
        log("ConnectivityService starting up");
        FirewallService.setupService(paramContext);
        HandlerThread localHandlerThread = new HandlerThread("ConnectivityServiceThread");
        localHandlerThread.start();
        this.mHandler = new MyHandler(localHandlerThread.getLooper());
        if (TextUtils.isEmpty(SystemProperties.get("net.hostname")))
        {
            String str3 = Settings.Secure.getString(paramContext.getContentResolver(), "android_id");
            if ((str3 != null) && (str3.length() > 0))
                SystemProperties.set("net.hostname", new String("android-").concat(str3));
        }
        String str1 = Settings.Secure.getString(paramContext.getContentResolver(), "default_dns_server");
        if ((str1 == null) || (str1.length() == 0))
            str1 = paramContext.getResources().getString(17039391);
        String[] arrayOfString2;
        int k;
        int m;
        try
        {
            this.mDefaultDns = NetworkUtils.numericToInetAddress(str1);
            this.mContext = ((Context)checkNotNull(paramContext, "missing Context"));
            this.mNetd = ((INetworkManagementService)checkNotNull(paramINetworkManagementService, "missing INetworkManagementService"));
            this.mPolicyManager = ((INetworkPolicyManager)checkNotNull(paramINetworkPolicyManager, "missing INetworkPolicyManager"));
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            try
            {
                this.mPolicyManager.registerListener(this.mPolicyListener);
                this.mNetTransitionWakeLock = ((PowerManager)paramContext.getSystemService("power")).newWakeLock(1, "ConnectivityService");
                this.mNetTransitionWakeLockTimeout = this.mContext.getResources().getInteger(17694729);
                this.mNetTrackers = new NetworkStateTracker[14];
                this.mCurrentLinkProperties = new LinkProperties[14];
                this.mNetworkPreference = getPersistedNetworkPreference();
                this.mRadioAttributes = new RadioAttributes[14];
                this.mNetConfigs = new NetworkConfig[14];
                String[] arrayOfString1 = paramContext.getResources().getStringArray(17235989);
                int i = arrayOfString1.length;
                int j = 0;
                while (true)
                    if (j < i)
                    {
                        localRadioAttributes = new RadioAttributes(arrayOfString1[j]);
                        if (localRadioAttributes.mType > 13)
                        {
                            loge("Error in radioAttributes - ignoring attempt to define type " + localRadioAttributes.mType);
                            j++;
                            continue;
                            localIllegalArgumentException = localIllegalArgumentException;
                            loge("Error setting defaultDns using " + str1);
                        }
                    }
            }
            catch (RemoteException localRemoteException1)
            {
                while (true)
                {
                    RadioAttributes localRadioAttributes;
                    loge("unable to register INetworkPolicyListener" + localRemoteException1.toString());
                    continue;
                    if (this.mRadioAttributes[localRadioAttributes.mType] != null)
                        loge("Error in radioAttributes - ignoring attempt to redefine type " + localRadioAttributes.mType);
                    else
                        this.mRadioAttributes[localRadioAttributes.mType] = localRadioAttributes;
                }
                arrayOfString2 = paramContext.getResources().getStringArray(17235987);
                k = arrayOfString2.length;
                m = 0;
            }
        }
        while (true)
        {
            String str2;
            if (m < k)
                str2 = arrayOfString2[m];
            try
            {
                NetworkConfig localNetworkConfig2 = new NetworkConfig(str2);
                if (localNetworkConfig2.type > 13)
                {
                    loge("Error in networkAttributes - ignoring attempt to define type " + localNetworkConfig2.type);
                }
                else if (this.mNetConfigs[localNetworkConfig2.type] != null)
                {
                    loge("Error in networkAttributes - ignoring attempt to redefine type " + localNetworkConfig2.type);
                }
                else if (this.mRadioAttributes[localNetworkConfig2.radio] == null)
                {
                    loge("Error in networkAttributes - ignoring attempt to use undefined radio " + localNetworkConfig2.radio + " in network type " + localNetworkConfig2.type);
                }
                else
                {
                    this.mNetConfigs[localNetworkConfig2.type] = localNetworkConfig2;
                    this.mNetworksDefined = (1 + this.mNetworksDefined);
                    break label1903;
                    this.mProtectedNetworks = new ArrayList();
                    int[] arrayOfInt1 = paramContext.getResources().getIntArray(17235988);
                    int n = arrayOfInt1.length;
                    int i1 = 0;
                    if (i1 < n)
                    {
                        int i15 = arrayOfInt1[i1];
                        if ((this.mNetConfigs[i15] != null) && (!this.mProtectedNetworks.contains(Integer.valueOf(i15))))
                            this.mProtectedNetworks.add(Integer.valueOf(i15));
                        while (true)
                        {
                            i1++;
                            break;
                            loge("Ignoring protectedNetwork " + i15);
                        }
                    }
                    this.mPriorityList = new int[this.mNetworksDefined];
                    int i2 = -1 + this.mNetworksDefined;
                    int i3 = 0;
                    int i4 = 0;
                    int i12;
                    int i13;
                    label1021: NetworkConfig localNetworkConfig1;
                    int i14;
                    if (i2 > -1)
                    {
                        NetworkConfig[] arrayOfNetworkConfig = this.mNetConfigs;
                        int i11 = arrayOfNetworkConfig.length;
                        i12 = 0;
                        i13 = i2;
                        if (i12 < i11)
                        {
                            localNetworkConfig1 = arrayOfNetworkConfig[i12];
                            if (localNetworkConfig1 == null)
                                i14 = i13;
                        }
                    }
                    while (true)
                    {
                        i12++;
                        i13 = i14;
                        break label1021;
                        if (localNetworkConfig1.priority < i3)
                        {
                            i14 = i13;
                        }
                        else if (localNetworkConfig1.priority > i3)
                        {
                            if ((localNetworkConfig1.priority < i4) || (i4 == 0))
                            {
                                i4 = localNetworkConfig1.priority;
                                i14 = i13;
                            }
                        }
                        else
                        {
                            int[] arrayOfInt4 = this.mPriorityList;
                            i14 = i13 - 1;
                            arrayOfInt4[i13] = localNetworkConfig1.type;
                            continue;
                            i3 = i4;
                            i4 = 0;
                            i2 = i13;
                            break;
                            this.mNetRequestersPids = new ArrayList[14];
                            for (int i10 : this.mPriorityList)
                                this.mNetRequestersPids[i10] = new ArrayList();
                            this.mFeatureUsers = new ArrayList();
                            this.mNumDnsEntries = 0;
                            if ((SystemProperties.get("cm.test.mode").equals("true")) && (SystemProperties.get("ro.build.type").equals("eng")));
                            int i9;
                            for (boolean bool1 = true; ; bool1 = false)
                            {
                                this.mTestMode = bool1;
                                for (i9 : this.mPriorityList)
                                    switch (this.mNetConfigs[i9].radio)
                                    {
                                    case 2:
                                    case 3:
                                    case 4:
                                    case 5:
                                    default:
                                        loge("Trying to create a DataStateTracker for an unknown radio type " + this.mNetConfigs[i9].radio);
                                    case 1:
                                    case 0:
                                    case 8:
                                    case 7:
                                    case 6:
                                    case 9:
                                    }
                            }
                            this.mNetTrackers[i9] = new WifiStateTracker(i9, this.mNetConfigs[i9].name);
                            this.mNetTrackers[i9].startMonitoring(paramContext, this.mHandler);
                            while (true)
                            {
                                this.mCurrentLinkProperties[i9] = null;
                                if ((this.mNetTrackers[i9] == null) || (!this.mNetConfigs[i9].isDefault()))
                                    break;
                                this.mNetTrackers[i9].reconnect();
                                break;
                                this.mNetTrackers[i9] = new MobileDataStateTracker(i9, this.mNetConfigs[i9].name);
                                this.mNetTrackers[i9].startMonitoring(paramContext, this.mHandler);
                                continue;
                                this.mNetTrackers[i9] = new DummyDataStateTracker(i9, this.mNetConfigs[i9].name);
                                this.mNetTrackers[i9].startMonitoring(paramContext, this.mHandler);
                                continue;
                                this.mNetTrackers[i9] = BluetoothTetheringDataTracker.getInstance();
                                this.mNetTrackers[i9].startMonitoring(paramContext, this.mHandler);
                                continue;
                                this.mNetTrackers[i9] = makeWimaxStateTracker();
                                if (this.mNetTrackers[i9] != null)
                                {
                                    this.mNetTrackers[i9].startMonitoring(paramContext, this.mHandler);
                                    continue;
                                    this.mNetTrackers[i9] = EthernetDataTracker.getInstance();
                                    this.mNetTrackers[i9].startMonitoring(paramContext, this.mHandler);
                                }
                            }
                            INetworkManagementService localINetworkManagementService = INetworkManagementService.Stub.asInterface(ServiceManager.getService("network_management"));
                            this.mTethering = new Tethering(this.mContext, localINetworkManagementService, paramINetworkStatsService, this, this.mHandler.getLooper());
                            boolean bool2;
                            if (((this.mTethering.getTetherableUsbRegexs().length != 0) || (this.mTethering.getTetherableWifiRegexs().length != 0) || (this.mTethering.getTetherableBluetoothRegexs().length != 0)) && (this.mTethering.getUpstreamIfaceTypes().length != 0))
                                bool2 = true;
                            while (true)
                            {
                                this.mTetheringConfigValid = bool2;
                                this.mVpn = new Vpn(this.mContext, new VpnCallback(null));
                                try
                                {
                                    localINetworkManagementService.registerObserver(this.mTethering);
                                    localINetworkManagementService.registerObserver(this.mVpn);
                                    this.mInetLog = new ArrayList();
                                    this.mSettingsObserver = new SettingsObserver(this.mHandler, 109);
                                    this.mSettingsObserver.observe(this.mContext);
                                    loadGlobalProxy();
                                    return;
                                    bool2 = false;
                                }
                                catch (RemoteException localRemoteException2)
                                {
                                    while (true)
                                        loge("Error registering observer :" + localRemoteException2);
                                }
                            }
                            i14 = i13;
                        }
                    }
                }
                label1903: m++;
            }
            catch (Exception localException)
            {
                break label1903;
            }
        }
    }

    private boolean addRoute(LinkProperties paramLinkProperties, RouteInfo paramRouteInfo, boolean paramBoolean)
    {
        return modifyRoute(paramLinkProperties.getInterfaceName(), paramLinkProperties, paramRouteInfo, 0, true, paramBoolean);
    }

    private boolean addRouteToAddress(LinkProperties paramLinkProperties, InetAddress paramInetAddress)
    {
        return modifyRouteToAddress(paramLinkProperties, paramInetAddress, true, true);
    }

    private void bumpDns()
    {
        String str = SystemProperties.get("net.dnschange");
        int i = 0;
        if (str.length() != 0);
        try
        {
            int j = Integer.parseInt(str);
            i = j;
            label25: SystemProperties.set("net.dnschange", "" + (i + 1));
            Intent localIntent = new Intent("android.intent.action.CLEAR_DNS_CACHE");
            localIntent.addFlags(536870912);
            localIntent.addFlags(134217728);
            this.mContext.sendBroadcast(localIntent);
            return;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            break label25;
        }
    }

    private static <T> T checkNotNull(T paramT, String paramString)
    {
        if (paramT == null)
            throw new NullPointerException(paramString);
        return paramT;
    }

    private void enforceAccessPermission()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE", "ConnectivityService");
    }

    private void enforceChangePermission()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CHANGE_NETWORK_STATE", "ConnectivityService");
    }

    private void enforceConnectivityInternalPermission()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "ConnectivityService");
    }

    private void enforcePreference()
    {
        if (this.mNetTrackers[this.mNetworkPreference].getNetworkInfo().isConnected());
        while (true)
        {
            return;
            if (this.mNetTrackers[this.mNetworkPreference].isAvailable())
                for (int i = 0; i <= 13; i++)
                    if ((i != this.mNetworkPreference) && (this.mNetTrackers[i] != null) && (this.mNetTrackers[i].getNetworkInfo().isConnected()))
                    {
                        log("tearing down " + this.mNetTrackers[i].getNetworkInfo() + " in enforcePreference");
                        teardown(this.mNetTrackers[i]);
                    }
        }
    }

    private void enforceTetherAccessPermission()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE", "ConnectivityService");
    }

    private void enforceTetherChangePermission()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CHANGE_NETWORK_STATE", "ConnectivityService");
    }

    private int getConnectivityChangeDelay()
    {
        return Settings.Secure.getInt(this.mContext.getContentResolver(), "connectivity_change_delay", SystemProperties.getInt("conn.connectivity_change_delay", 3000));
    }

    private NetworkInfo getFilteredNetworkInfo(NetworkStateTracker paramNetworkStateTracker, int paramInt)
    {
        Object localObject = paramNetworkStateTracker.getNetworkInfo();
        if (isNetworkBlocked(paramNetworkStateTracker, paramInt))
        {
            NetworkInfo localNetworkInfo = new NetworkInfo((NetworkInfo)localObject);
            localNetworkInfo.setDetailedState(NetworkInfo.DetailedState.BLOCKED, null, null);
            localObject = localNetworkInfo;
        }
        return localObject;
    }

    private NetworkInfo getNetworkInfo(int paramInt1, int paramInt2)
    {
        NetworkInfo localNetworkInfo = null;
        if (ConnectivityManager.isNetworkTypeValid(paramInt1))
        {
            NetworkStateTracker localNetworkStateTracker = this.mNetTrackers[paramInt1];
            if (localNetworkStateTracker != null)
                localNetworkInfo = getFilteredNetworkInfo(localNetworkStateTracker, paramInt2);
        }
        return localNetworkInfo;
    }

    private NetworkState getNetworkStateUnchecked(int paramInt)
    {
        NetworkStateTracker localNetworkStateTracker;
        if (ConnectivityManager.isNetworkTypeValid(paramInt))
        {
            localNetworkStateTracker = this.mNetTrackers[paramInt];
            if (localNetworkStateTracker == null);
        }
        for (NetworkState localNetworkState = new NetworkState(localNetworkStateTracker.getNetworkInfo(), localNetworkStateTracker.getLinkProperties(), localNetworkStateTracker.getLinkCapabilities()); ; localNetworkState = null)
            return localNetworkState;
    }

    private int getPersistedNetworkPreference()
    {
        int i = Settings.Secure.getInt(this.mContext.getContentResolver(), "network_preference", -1);
        if (i != -1);
        while (true)
        {
            return i;
            i = 1;
        }
    }

    private int getRestoreDefaultNetworkDelay(int paramInt)
    {
        String str = SystemProperties.get("android.telephony.apn-restore");
        if ((str != null) && (str.length() != 0));
        while (true)
        {
            try
            {
                int j = Integer.valueOf(str).intValue();
                i = j;
                return i;
            }
            catch (NumberFormatException localNumberFormatException)
            {
            }
            int i = 60000;
            if ((paramInt <= 13) && (this.mNetConfigs[paramInt] != null))
                i = this.mNetConfigs[paramInt].restoreTime;
        }
    }

    // ERROR //
    private void handleApplyDefaultProxy(ProxyProperties paramProxyProperties)
    {
        // Byte code:
        //     0: aload_1
        //     1: ifnull +15 -> 16
        //     4: aload_1
        //     5: invokevirtual 862	android/net/ProxyProperties:getHost	()Ljava/lang/String;
        //     8: invokestatic 268	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
        //     11: ifeq +5 -> 16
        //     14: aconst_null
        //     15: astore_1
        //     16: aload_0
        //     17: getfield 213	com/android/server/ConnectivityService:mDefaultProxyLock	Ljava/lang/Object;
        //     20: astore_2
        //     21: aload_2
        //     22: monitorenter
        //     23: aload_0
        //     24: getfield 211	com/android/server/ConnectivityService:mDefaultProxy	Landroid/net/ProxyProperties;
        //     27: ifnull +19 -> 46
        //     30: aload_0
        //     31: getfield 211	com/android/server/ConnectivityService:mDefaultProxy	Landroid/net/ProxyProperties;
        //     34: aload_1
        //     35: invokevirtual 863	android/net/ProxyProperties:equals	(Ljava/lang/Object;)Z
        //     38: ifeq +8 -> 46
        //     41: aload_2
        //     42: monitorexit
        //     43: goto +40 -> 83
        //     46: aload_0
        //     47: getfield 211	com/android/server/ConnectivityService:mDefaultProxy	Landroid/net/ProxyProperties;
        //     50: aload_1
        //     51: if_acmpne +13 -> 64
        //     54: aload_2
        //     55: monitorexit
        //     56: goto +27 -> 83
        //     59: astore_3
        //     60: aload_2
        //     61: monitorexit
        //     62: aload_3
        //     63: athrow
        //     64: aload_0
        //     65: aload_1
        //     66: putfield 211	com/android/server/ConnectivityService:mDefaultProxy	Landroid/net/ProxyProperties;
        //     69: aload_0
        //     70: getfield 215	com/android/server/ConnectivityService:mDefaultProxyDisabled	Z
        //     73: ifne +8 -> 81
        //     76: aload_0
        //     77: aload_1
        //     78: invokespecial 698	com/android/server/ConnectivityService:sendProxyBroadcast	(Landroid/net/ProxyProperties;)V
        //     81: aload_2
        //     82: monitorexit
        //     83: return
        //
        // Exception table:
        //     from	to	target	type
        //     23	62	59	finally
        //     64	83	59	finally
    }

    private void handleConnect(NetworkInfo paramNetworkInfo)
    {
        int i = paramNetworkInfo.getType();
        paramNetworkInfo.isFailover();
        NetworkStateTracker localNetworkStateTracker1 = this.mNetTrackers[i];
        if (this.mNetConfigs[i].isDefault())
            if ((this.mActiveDefaultNetwork != -1) && (this.mActiveDefaultNetwork != i))
                if (((i != this.mNetworkPreference) && (this.mNetConfigs[this.mActiveDefaultNetwork].priority > this.mNetConfigs[i].priority)) || (this.mNetworkPreference == this.mActiveDefaultNetwork))
                    teardown(localNetworkStateTracker1);
        while (true)
        {
            return;
            NetworkStateTracker localNetworkStateTracker2 = this.mNetTrackers[this.mActiveDefaultNetwork];
            log("Policy requires " + localNetworkStateTracker2.getNetworkInfo().getTypeName() + " teardown");
            if (!teardown(localNetworkStateTracker2))
            {
                loge("Network declined teardown request");
                teardown(localNetworkStateTracker1);
                continue;
            }
            try
            {
                if (this.mNetTransitionWakeLock.isHeld())
                    this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(108, this.mNetTransitionWakeLockSerialNumber, 0), 1000L);
                this.mActiveDefaultNetwork = i;
                this.mDefaultInetConditionPublished = 0;
                this.mDefaultConnectionSequence = (1 + this.mDefaultConnectionSequence);
                this.mInetConditionChangeInFlight = false;
                localNetworkStateTracker1.setTeardownRequested(false);
                updateNetworkSettings(localNetworkStateTracker1);
                handleConnectivityChange(i, false);
                sendConnectedBroadcastDelayed(paramNetworkInfo, getConnectivityChangeDelay());
                String str = localNetworkStateTracker1.getLinkProperties().getInterfaceName();
                if (str == null)
                    continue;
                try
                {
                    BatteryStatsService.getService().noteNetworkInterfaceType(str, i);
                }
                catch (RemoteException localRemoteException)
                {
                }
            }
            finally
            {
            }
        }
    }

    private void handleConnectionFailure(NetworkInfo paramNetworkInfo)
    {
        this.mNetTrackers[paramNetworkInfo.getType()].setTeardownRequested(false);
        String str1 = paramNetworkInfo.getReason();
        String str2 = paramNetworkInfo.getExtraInfo();
        String str3;
        Intent localIntent1;
        if (str1 == null)
        {
            str3 = ".";
            loge("Attempt to connect to " + paramNetworkInfo.getTypeName() + " failed" + str3);
            localIntent1 = new Intent("android.net.conn.CONNECTIVITY_CHANGE");
            localIntent1.putExtra("networkInfo", paramNetworkInfo);
            if (getActiveNetworkInfo() == null)
                localIntent1.putExtra("noConnectivity", true);
            if (str1 != null)
                localIntent1.putExtra("reason", str1);
            if (str2 != null)
                localIntent1.putExtra("extraInfo", str2);
            if (paramNetworkInfo.isFailover())
            {
                localIntent1.putExtra("isFailover", true);
                paramNetworkInfo.setFailover(false);
            }
            if (this.mNetConfigs[paramNetworkInfo.getType()].isDefault())
            {
                tryFailover(paramNetworkInfo.getType());
                if (this.mActiveDefaultNetwork == -1)
                    break label320;
                localIntent1.putExtra("otherNetwork", this.mNetTrackers[this.mActiveDefaultNetwork].getNetworkInfo());
            }
        }
        while (true)
        {
            localIntent1.putExtra("inetCondition", this.mDefaultInetConditionPublished);
            Intent localIntent2 = new Intent(localIntent1);
            localIntent2.setAction("android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE");
            sendStickyBroadcast(localIntent2);
            sendStickyBroadcast(localIntent1);
            if (this.mActiveDefaultNetwork != -1)
                sendConnectedBroadcast(this.mNetTrackers[this.mActiveDefaultNetwork].getNetworkInfo());
            return;
            str3 = " (" + str1 + ").";
            break;
            label320: this.mDefaultInetConditionPublished = 0;
            localIntent1.putExtra("noConnectivity", true);
        }
    }

    private void handleConnectivityChange(int paramInt, boolean paramBoolean)
    {
        if (paramBoolean);
        LinkProperties localLinkProperties1;
        LinkProperties localLinkProperties2;
        LinkProperties.CompareResult localCompareResult;
        for (int i = 3; ; i = 0)
        {
            handleDnsConfigurationChange(paramInt);
            localLinkProperties1 = this.mCurrentLinkProperties[paramInt];
            localLinkProperties2 = null;
            if (!this.mNetTrackers[paramInt].getNetworkInfo().isConnected())
                break label246;
            localLinkProperties2 = this.mNetTrackers[paramInt].getLinkProperties();
            if (localLinkProperties1 == null)
                break label225;
            if (!localLinkProperties1.isIdenticalInterfaceName(localLinkProperties2))
                break label480;
            localCompareResult = localLinkProperties1.compareAddresses(localLinkProperties2);
            if ((localCompareResult.removed.size() == 0) && (localCompareResult.added.size() == 0))
                break label437;
            Iterator localIterator = localCompareResult.removed.iterator();
            while (localIterator.hasNext())
            {
                LinkAddress localLinkAddress = (LinkAddress)localIterator.next();
                if ((localLinkAddress.getAddress() instanceof Inet4Address))
                    i |= 1;
                if ((localLinkAddress.getAddress() instanceof Inet6Address))
                    i |= 2;
            }
        }
        log("handleConnectivityChange: addresses changed linkProperty[" + paramInt + "]:" + " resetMask=" + i + "\n     car=" + localCompareResult);
        while (true)
        {
            label225: if (this.mNetConfigs[paramInt].isDefault())
                handleApplyDefaultProxy(localLinkProperties2.getHttpProxy());
            label246: this.mCurrentLinkProperties[paramInt] = localLinkProperties2;
            boolean bool = updateRoutes(localLinkProperties2, localLinkProperties1, this.mNetConfigs[paramInt].isDefault());
            String str;
            if ((i != 0) || (bool))
            {
                LinkProperties localLinkProperties3 = this.mNetTrackers[paramInt].getLinkProperties();
                if (localLinkProperties3 != null)
                {
                    str = localLinkProperties3.getInterfaceName();
                    if (!TextUtils.isEmpty(str))
                    {
                        if (i != 0)
                        {
                            log("resetConnections(" + str + ", " + i + ")");
                            NetworkUtils.resetConnections(str, i);
                            if ((i & 0x1) != 0)
                                this.mVpn.interfaceStatusChanged(str, false);
                        }
                        if (!bool);
                    }
                }
            }
            try
            {
                this.mNetd.flushInterfaceDnsCache(str);
                if ((TextUtils.equals(this.mNetTrackers[paramInt].getNetworkInfo().getReason(), "linkPropertiesChanged")) && (isTetheringSupported()))
                    this.mTethering.handleTetherIfaceChange();
                return;
                label437: log("handleConnectivityChange: address are the same reset per doReset linkProperty[" + paramInt + "]:" + " resetMask=" + i);
                continue;
                label480: i = 3;
                log("handleConnectivityChange: interface not not equivalent reset both linkProperty[" + paramInt + "]:" + " resetMask=" + i);
            }
            catch (Exception localException)
            {
                while (true)
                    loge("Exception resetting dns cache: " + localException);
            }
        }
    }

    private void handleDeprecatedGlobalHttpProxy()
    {
        String str = Settings.Secure.getString(this.mContext.getContentResolver(), "http_proxy");
        String[] arrayOfString;
        int i;
        if (!TextUtils.isEmpty(str))
        {
            arrayOfString = str.split(":");
            arrayOfString[0];
            i = 8080;
            if (arrayOfString.length <= 1);
        }
        try
        {
            int j = Integer.parseInt(arrayOfString[1]);
            i = j;
            setGlobalProxy(new ProxyProperties(arrayOfString[0], i, ""));
            label74: return;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            break label74;
        }
    }

    private void handleDisconnect(NetworkInfo paramNetworkInfo)
    {
        int i = paramNetworkInfo.getType();
        this.mNetTrackers[i].setTeardownRequested(false);
        if (!this.mNetConfigs[i].isDefault())
        {
            List localList = this.mNetRequestersPids[i];
            for (int m = 0; m < localList.size(); m++)
                reassessPidDns(((Integer)localList.get(m)).intValue(), false);
        }
        Intent localIntent1 = new Intent("android.net.conn.CONNECTIVITY_CHANGE");
        localIntent1.putExtra("networkInfo", paramNetworkInfo);
        if (paramNetworkInfo.isFailover())
        {
            localIntent1.putExtra("isFailover", true);
            paramNetworkInfo.setFailover(false);
        }
        if (paramNetworkInfo.getReason() != null)
            localIntent1.putExtra("reason", paramNetworkInfo.getReason());
        if (paramNetworkInfo.getExtraInfo() != null)
            localIntent1.putExtra("extraInfo", paramNetworkInfo.getExtraInfo());
        String str;
        int k;
        label267: NetworkStateTracker localNetworkStateTracker;
        if (this.mNetConfigs[i].isDefault())
        {
            tryFailover(i);
            if (this.mActiveDefaultNetwork != -1)
                localIntent1.putExtra("otherNetwork", this.mNetTrackers[this.mActiveDefaultNetwork].getNetworkInfo());
        }
        else
        {
            localIntent1.putExtra("inetCondition", this.mDefaultInetConditionPublished);
            bool = true;
            LinkProperties localLinkProperties1 = this.mNetTrackers[i].getLinkProperties();
            if (localLinkProperties1 == null)
                break label365;
            str = localLinkProperties1.getInterfaceName();
            if (TextUtils.isEmpty(str))
                break label365;
            NetworkStateTracker[] arrayOfNetworkStateTracker = this.mNetTrackers;
            int j = arrayOfNetworkStateTracker.length;
            k = 0;
            if (k >= j)
                break label365;
            localNetworkStateTracker = arrayOfNetworkStateTracker[k];
            if (localNetworkStateTracker != null)
                break label309;
        }
        label309: LinkProperties localLinkProperties2;
        do
        {
            NetworkInfo localNetworkInfo;
            do
            {
                k++;
                break label267;
                this.mDefaultInetConditionPublished = 0;
                localIntent1.putExtra("noConnectivity", true);
                break;
                localNetworkInfo = localNetworkStateTracker.getNetworkInfo();
            }
            while ((!localNetworkInfo.isConnected()) || (localNetworkInfo.getType() == i));
            localLinkProperties2 = localNetworkStateTracker.getLinkProperties();
        }
        while ((localLinkProperties2 == null) || (!str.equals(localLinkProperties2.getInterfaceName())));
        boolean bool = false;
        label365: handleConnectivityChange(i, bool);
        Intent localIntent2 = new Intent(localIntent1);
        localIntent2.setAction("android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE");
        sendStickyBroadcast(localIntent2);
        sendStickyBroadcastDelayed(localIntent1, getConnectivityChangeDelay());
        if (this.mActiveDefaultNetwork != -1)
            sendConnectedBroadcastDelayed(this.mNetTrackers[this.mActiveDefaultNetwork].getNetworkInfo(), getConnectivityChangeDelay());
    }

    private void handleDnsConfigurationChange(int paramInt)
    {
        NetworkStateTracker localNetworkStateTracker = this.mNetTrackers[paramInt];
        LinkProperties localLinkProperties;
        if ((localNetworkStateTracker != null) && (localNetworkStateTracker.getNetworkInfo().isConnected()) && (!localNetworkStateTracker.isTeardownRequested()))
        {
            localLinkProperties = localNetworkStateTracker.getLinkProperties();
            if (localLinkProperties != null);
        }
        else
        {
            return;
        }
        Collection localCollection = localLinkProperties.getDnses();
        boolean bool = false;
        String str;
        if (this.mNetConfigs[paramInt].isDefault())
            str = localNetworkStateTracker.getNetworkInfo().getTypeName();
        while (true)
        {
            synchronized (this.mDnsLock)
            {
                if (!this.mDnsOverridden)
                    bool = updateDns(str, localLinkProperties.getInterfaceName(), localCollection, "");
                if (!bool)
                    break;
                bumpDns();
            }
            try
            {
                this.mNetd.setDnsServersForInterface(localLinkProperties.getInterfaceName(), NetworkUtils.makeStrings(localCollection));
                List localList = this.mNetRequestersPids[paramInt];
                for (int i = 0; i < localList.size(); i++)
                    bool = writePidDns(localCollection, ((Integer)localList.get(i)).intValue());
            }
            catch (Exception localException)
            {
                while (true)
                    loge("exception setting dns servers: " + localException);
            }
        }
    }

    private void handleInetConditionChange(int paramInt1, int paramInt2)
    {
        if (this.mActiveDefaultNetwork == -1)
            log("handleInetConditionChange: no active default network - ignore");
        do
        {
            while (true)
            {
                return;
                if (this.mActiveDefaultNetwork == paramInt1)
                    break;
                log("handleInetConditionChange: net=" + paramInt1 + " != default=" + this.mActiveDefaultNetwork + " - ignore");
            }
            this.mDefaultInetCondition = paramInt2;
        }
        while (this.mInetConditionChangeInFlight);
        if (this.mDefaultInetCondition > 50);
        for (int i = Settings.Secure.getInt(this.mContext.getContentResolver(), "inet_condition_debounce_up_delay", 500); ; i = Settings.Secure.getInt(this.mContext.getContentResolver(), "inet_condition_debounce_down_delay", 3000))
        {
            this.mInetConditionChangeInFlight = true;
            this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(105, this.mActiveDefaultNetwork, this.mDefaultConnectionSequence), i);
            break;
        }
    }

    private void handleInetConditionHoldEnd(int paramInt1, int paramInt2)
    {
        log("handleInetConditionHoldEnd: net=" + paramInt1 + ", condition=" + this.mDefaultInetCondition + ", published condition=" + this.mDefaultInetConditionPublished);
        this.mInetConditionChangeInFlight = false;
        if (this.mActiveDefaultNetwork == -1)
            log("handleInetConditionHoldEnd: no active default network - ignoring");
        while (true)
        {
            return;
            if (this.mDefaultConnectionSequence != paramInt2)
            {
                log("handleInetConditionHoldEnd: event hold for obsolete network - ignoring");
            }
            else
            {
                NetworkInfo localNetworkInfo = this.mNetTrackers[this.mActiveDefaultNetwork].getNetworkInfo();
                if (!localNetworkInfo.isConnected())
                {
                    log("handleInetConditionHoldEnd: default network not connected - ignoring");
                }
                else
                {
                    this.mDefaultInetConditionPublished = this.mDefaultInetCondition;
                    sendInetConditionBroadcast(localNetworkInfo);
                }
            }
        }
    }

    private void handleSetDependencyMet(int paramInt, boolean paramBoolean)
    {
        if (this.mNetTrackers[paramInt] != null)
        {
            log("handleSetDependencyMet(" + paramInt + ", " + paramBoolean + ")");
            this.mNetTrackers[paramInt].setDependencyMet(paramBoolean);
        }
    }

    private void handleSetMobileData(boolean paramBoolean)
    {
        if (this.mNetTrackers[0] != null)
            this.mNetTrackers[0].setUserDataEnable(paramBoolean);
        if (this.mNetTrackers[6] != null)
            this.mNetTrackers[6].setUserDataEnable(paramBoolean);
    }

    private void handleSetNetworkPreference(int paramInt)
    {
        if ((ConnectivityManager.isNetworkTypeValid(paramInt)) && (this.mNetConfigs[paramInt] != null) && (this.mNetConfigs[paramInt].isDefault()) && (this.mNetworkPreference != paramInt))
            Settings.Secure.putInt(this.mContext.getContentResolver(), "network_preference", paramInt);
        try
        {
            this.mNetworkPreference = paramInt;
            enforcePreference();
            return;
        }
        finally
        {
        }
    }

    private void handleSetPolicyDataEnable(int paramInt, boolean paramBoolean)
    {
        if (ConnectivityManager.isNetworkTypeValid(paramInt))
        {
            NetworkStateTracker localNetworkStateTracker = this.mNetTrackers[paramInt];
            if (localNetworkStateTracker != null)
                localNetworkStateTracker.setPolicyDataEnable(paramBoolean);
        }
    }

    private boolean isNetworkBlocked(NetworkStateTracker paramNetworkStateTracker, int paramInt)
    {
        boolean bool1 = false;
        String str = paramNetworkStateTracker.getLinkProperties().getInterfaceName();
        synchronized (this.mRulesLock)
        {
            boolean bool2 = this.mMeteredIfaces.contains(str);
            int i = this.mUidRules.get(paramInt, 0);
            if ((bool2) && ((i & 0x1) != 0))
                bool1 = true;
        }
        return bool1;
    }

    private boolean isNetworkMeteredUnchecked(int paramInt)
    {
        NetworkState localNetworkState = getNetworkStateUnchecked(paramInt);
        if (localNetworkState != null);
        while (true)
        {
            try
            {
                boolean bool2 = this.mPolicyManager.isNetworkMetered(localNetworkState);
                bool1 = bool2;
                return bool1;
            }
            catch (RemoteException localRemoteException)
            {
            }
            boolean bool1 = false;
        }
    }

    private void loadGlobalProxy()
    {
        ContentResolver localContentResolver = this.mContext.getContentResolver();
        String str1 = Settings.Secure.getString(localContentResolver, "global_http_proxy_host");
        int i = Settings.Secure.getInt(localContentResolver, "global_http_proxy_port", 0);
        String str2 = Settings.Secure.getString(localContentResolver, "global_http_proxy_exclusion_list");
        if (!TextUtils.isEmpty(str1))
        {
            ProxyProperties localProxyProperties = new ProxyProperties(str1, i, str2);
            synchronized (this.mGlobalProxyLock)
            {
                this.mGlobalProxy = localProxyProperties;
            }
        }
    }

    private void log(String paramString)
    {
        Slog.d("ConnectivityService", paramString);
    }

    private void loge(String paramString)
    {
        Slog.e("ConnectivityService", paramString);
    }

    private Intent makeGeneralIntent(NetworkInfo paramNetworkInfo, String paramString)
    {
        Intent localIntent = new Intent(paramString);
        localIntent.putExtra("networkInfo", paramNetworkInfo);
        if (paramNetworkInfo.isFailover())
        {
            localIntent.putExtra("isFailover", true);
            paramNetworkInfo.setFailover(false);
        }
        if (paramNetworkInfo.getReason() != null)
            localIntent.putExtra("reason", paramNetworkInfo.getReason());
        if (paramNetworkInfo.getExtraInfo() != null)
            localIntent.putExtra("extraInfo", paramNetworkInfo.getExtraInfo());
        localIntent.putExtra("inetCondition", this.mDefaultInetConditionPublished);
        return localIntent;
    }

    // ERROR //
    private NetworkStateTracker makeWimaxStateTracker()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 326	com/android/server/ConnectivityService:mContext	Landroid/content/Context;
        //     4: invokevirtual 304	android/content/Context:getResources	()Landroid/content/res/Resources;
        //     7: ldc_w 1201
        //     10: invokevirtual 1204	android/content/res/Resources:getBoolean	(I)Z
        //     13: ifeq +397 -> 410
        //     16: aload_0
        //     17: getfield 326	com/android/server/ConnectivityService:mContext	Landroid/content/Context;
        //     20: invokevirtual 304	android/content/Context:getResources	()Landroid/content/res/Resources;
        //     23: ldc_w 1205
        //     26: invokevirtual 310	android/content/res/Resources:getString	(I)Ljava/lang/String;
        //     29: astore_3
        //     30: aload_0
        //     31: getfield 326	com/android/server/ConnectivityService:mContext	Landroid/content/Context;
        //     34: invokevirtual 304	android/content/Context:getResources	()Landroid/content/res/Resources;
        //     37: ldc_w 1206
        //     40: invokevirtual 310	android/content/res/Resources:getString	(I)Ljava/lang/String;
        //     43: astore 4
        //     45: aload_0
        //     46: getfield 326	com/android/server/ConnectivityService:mContext	Landroid/content/Context;
        //     49: invokevirtual 304	android/content/Context:getResources	()Landroid/content/res/Resources;
        //     52: ldc_w 1207
        //     55: invokevirtual 310	android/content/res/Resources:getString	(I)Ljava/lang/String;
        //     58: astore 5
        //     60: aload_0
        //     61: getfield 326	com/android/server/ConnectivityService:mContext	Landroid/content/Context;
        //     64: invokevirtual 304	android/content/Context:getResources	()Landroid/content/res/Resources;
        //     67: ldc_w 1208
        //     70: invokevirtual 310	android/content/res/Resources:getString	(I)Ljava/lang/String;
        //     73: astore 6
        //     75: aload_0
        //     76: getfield 326	com/android/server/ConnectivityService:mContext	Landroid/content/Context;
        //     79: invokevirtual 304	android/content/Context:getResources	()Landroid/content/res/Resources;
        //     82: ldc_w 1209
        //     85: invokevirtual 310	android/content/res/Resources:getString	(I)Ljava/lang/String;
        //     88: astore 7
        //     90: aload_0
        //     91: new 393	java/lang/StringBuilder
        //     94: dup
        //     95: invokespecial 394	java/lang/StringBuilder:<init>	()V
        //     98: ldc_w 1211
        //     101: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     104: aload_3
        //     105: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     108: invokevirtual 407	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     111: invokespecial 230	com/android/server/ConnectivityService:log	(Ljava/lang/String;)V
        //     114: new 1213	dalvik/system/DexClassLoader
        //     117: dup
        //     118: aload_3
        //     119: new 1215	android/content/ContextWrapper
        //     122: dup
        //     123: aload_0
        //     124: getfield 326	com/android/server/ConnectivityService:mContext	Landroid/content/Context;
        //     127: invokespecial 1217	android/content/ContextWrapper:<init>	(Landroid/content/Context;)V
        //     130: invokevirtual 1221	android/content/ContextWrapper:getCacheDir	()Ljava/io/File;
        //     133: invokevirtual 1226	java/io/File:getAbsolutePath	()Ljava/lang/String;
        //     136: aload 4
        //     138: invokestatic 1232	java/lang/ClassLoader:getSystemClassLoader	()Ljava/lang/ClassLoader;
        //     141: invokespecial 1235	dalvik/system/DexClassLoader:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V
        //     144: astore 8
        //     146: aload 8
        //     148: aload 5
        //     150: invokevirtual 1239	dalvik/system/DexClassLoader:loadClass	(Ljava/lang/String;)Ljava/lang/Class;
        //     153: pop
        //     154: aload 8
        //     156: aload 7
        //     158: invokevirtual 1239	dalvik/system/DexClassLoader:loadClass	(Ljava/lang/String;)Ljava/lang/Class;
        //     161: astore 11
        //     163: aload 8
        //     165: aload 6
        //     167: invokevirtual 1239	dalvik/system/DexClassLoader:loadClass	(Ljava/lang/String;)Ljava/lang/Class;
        //     170: astore 12
        //     172: aload_0
        //     173: ldc_w 1241
        //     176: invokespecial 230	com/android/server/ConnectivityService:log	(Ljava/lang/String;)V
        //     179: iconst_2
        //     180: anewarray 1243	java/lang/Class
        //     183: astore 14
        //     185: aload 14
        //     187: iconst_0
        //     188: ldc_w 270
        //     191: aastore
        //     192: aload 14
        //     194: iconst_1
        //     195: ldc_w 543
        //     198: aastore
        //     199: aload 11
        //     201: aload 14
        //     203: invokevirtual 1247	java/lang/Class:getConstructor	([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
        //     206: astore 15
        //     208: iconst_2
        //     209: anewarray 170	java/lang/Object
        //     212: astore 16
        //     214: aload 16
        //     216: iconst_0
        //     217: aload_0
        //     218: getfield 326	com/android/server/ConnectivityService:mContext	Landroid/content/Context;
        //     221: aastore
        //     222: aload 16
        //     224: iconst_1
        //     225: aload_0
        //     226: getfield 254	com/android/server/ConnectivityService:mHandler	Landroid/os/Handler;
        //     229: aastore
        //     230: aload 15
        //     232: aload 16
        //     234: invokevirtual 1253	java/lang/reflect/Constructor:newInstance	([Ljava/lang/Object;)Ljava/lang/Object;
        //     237: checkcast 365	android/net/NetworkStateTracker
        //     240: astore 17
        //     242: iconst_2
        //     243: anewarray 1243	java/lang/Class
        //     246: astore 18
        //     248: aload 18
        //     250: iconst_0
        //     251: ldc_w 270
        //     254: aastore
        //     255: aload 18
        //     257: iconst_1
        //     258: aload 11
        //     260: aastore
        //     261: aload 12
        //     263: aload 18
        //     265: invokevirtual 1256	java/lang/Class:getDeclaredConstructor	([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
        //     268: astore 19
        //     270: aload 19
        //     272: iconst_1
        //     273: invokevirtual 1259	java/lang/reflect/Constructor:setAccessible	(Z)V
        //     276: iconst_2
        //     277: anewarray 170	java/lang/Object
        //     280: astore 20
        //     282: aload 20
        //     284: iconst_0
        //     285: aload_0
        //     286: getfield 326	com/android/server/ConnectivityService:mContext	Landroid/content/Context;
        //     289: aastore
        //     290: aload 20
        //     292: iconst_1
        //     293: aload 17
        //     295: aastore
        //     296: aload 19
        //     298: aload 20
        //     300: invokevirtual 1253	java/lang/reflect/Constructor:newInstance	([Ljava/lang/Object;)Ljava/lang/Object;
        //     303: checkcast 1261	android/os/IBinder
        //     306: astore 21
        //     308: aload 19
        //     310: iconst_0
        //     311: invokevirtual 1259	java/lang/reflect/Constructor:setAccessible	(Z)V
        //     314: ldc_w 1263
        //     317: aload 21
        //     319: invokestatic 1267	android/os/ServiceManager:addService	(Ljava/lang/String;Landroid/os/IBinder;)V
        //     322: aload 17
        //     324: astore_1
        //     325: aload_1
        //     326: areturn
        //     327: astore 9
        //     329: aload_0
        //     330: new 393	java/lang/StringBuilder
        //     333: dup
        //     334: invokespecial 394	java/lang/StringBuilder:<init>	()V
        //     337: ldc_w 1269
        //     340: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     343: aload 9
        //     345: invokevirtual 1270	java/lang/ClassNotFoundException:toString	()Ljava/lang/String;
        //     348: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     351: invokevirtual 407	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     354: invokespecial 410	com/android/server/ConnectivityService:loge	(Ljava/lang/String;)V
        //     357: aconst_null
        //     358: astore_1
        //     359: goto -34 -> 325
        //     362: astore_2
        //     363: aload_0
        //     364: ldc_w 1272
        //     367: invokespecial 410	com/android/server/ConnectivityService:loge	(Ljava/lang/String;)V
        //     370: aconst_null
        //     371: astore_1
        //     372: goto -47 -> 325
        //     375: astore 13
        //     377: aload_0
        //     378: new 393	java/lang/StringBuilder
        //     381: dup
        //     382: invokespecial 394	java/lang/StringBuilder:<init>	()V
        //     385: ldc_w 1274
        //     388: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     391: aload 13
        //     393: invokevirtual 1275	java/lang/Exception:toString	()Ljava/lang/String;
        //     396: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     399: invokevirtual 407	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     402: invokespecial 410	com/android/server/ConnectivityService:loge	(Ljava/lang/String;)V
        //     405: aconst_null
        //     406: astore_1
        //     407: goto -82 -> 325
        //     410: aload_0
        //     411: ldc_w 1277
        //     414: invokespecial 410	com/android/server/ConnectivityService:loge	(Ljava/lang/String;)V
        //     417: aconst_null
        //     418: astore_1
        //     419: goto -94 -> 325
        //
        // Exception table:
        //     from	to	target	type
        //     146	172	327	java/lang/ClassNotFoundException
        //     16	146	362	android/content/res/Resources$NotFoundException
        //     146	172	362	android/content/res/Resources$NotFoundException
        //     329	357	362	android/content/res/Resources$NotFoundException
        //     172	322	375	java/lang/Exception
    }

    private boolean modifyRoute(String paramString, LinkProperties paramLinkProperties, RouteInfo paramRouteInfo, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    {
        boolean bool;
        if ((paramString == null) || (paramLinkProperties == null) || (paramRouteInfo == null))
        {
            log("modifyRoute got unexpected null: " + paramString + ", " + paramLinkProperties + ", " + paramRouteInfo);
            bool = false;
        }
        while (true)
        {
            return bool;
            if (paramInt > 10)
            {
                loge("Error modifying route - too much recursion");
                bool = false;
            }
            else
            {
                RouteInfo localRouteInfo1;
                RouteInfo localRouteInfo2;
                if (!paramRouteInfo.isHostRoute())
                {
                    localRouteInfo1 = RouteInfo.selectBestRoute(paramLinkProperties.getRoutes(), paramRouteInfo.getGateway());
                    if (localRouteInfo1 != null)
                    {
                        if (!localRouteInfo1.getGateway().equals(paramRouteInfo.getGateway()))
                            break label186;
                        localRouteInfo2 = RouteInfo.makeHostRoute(paramRouteInfo.getGateway());
                        label131: modifyRoute(paramString, paramLinkProperties, localRouteInfo2, paramInt + 1, paramBoolean1, paramBoolean2);
                    }
                }
                if (paramBoolean1)
                    if (!paramBoolean2);
                while (true)
                {
                    try
                    {
                        this.mAddedRoutes.add(paramRouteInfo);
                        this.mNetd.addRoute(paramString, paramRouteInfo);
                        bool = true;
                        break;
                        label186: localRouteInfo2 = RouteInfo.makeHostRoute(paramRouteInfo.getGateway(), localRouteInfo1.getGateway());
                        break label131;
                        this.mNetd.addSecondaryRoute(paramString, paramRouteInfo);
                        continue;
                    }
                    catch (Exception localException3)
                    {
                        loge("Exception trying to add a route: " + localException3);
                        bool = false;
                    }
                    break;
                    if (paramBoolean2)
                    {
                        this.mAddedRoutes.remove(paramRouteInfo);
                        if (this.mAddedRoutes.contains(paramRouteInfo))
                            continue;
                        try
                        {
                            this.mNetd.removeRoute(paramString, paramRouteInfo);
                        }
                        catch (Exception localException2)
                        {
                            bool = false;
                        }
                        break;
                    }
                    try
                    {
                        this.mNetd.removeSecondaryRoute(paramString, paramRouteInfo);
                    }
                    catch (Exception localException1)
                    {
                        bool = false;
                    }
                }
            }
        }
    }

    private boolean modifyRouteToAddress(LinkProperties paramLinkProperties, InetAddress paramInetAddress, boolean paramBoolean1, boolean paramBoolean2)
    {
        RouteInfo localRouteInfo1 = RouteInfo.selectBestRoute(paramLinkProperties.getRoutes(), paramInetAddress);
        RouteInfo localRouteInfo2;
        if (localRouteInfo1 == null)
            localRouteInfo2 = RouteInfo.makeHostRoute(paramInetAddress);
        while (true)
        {
            return modifyRoute(paramLinkProperties.getInterfaceName(), paramLinkProperties, localRouteInfo2, 0, paramBoolean1, paramBoolean2);
            if (localRouteInfo1.getGateway().equals(paramInetAddress))
                localRouteInfo2 = RouteInfo.makeHostRoute(paramInetAddress);
            else
                localRouteInfo2 = RouteInfo.makeHostRoute(paramInetAddress, localRouteInfo1.getGateway());
        }
    }

    private void reassessPidDns(int paramInt, boolean paramBoolean)
    {
        int[] arrayOfInt = this.mPriorityList;
        int i = arrayOfInt.length;
        int j = 0;
        if (j < i)
        {
            int m = arrayOfInt[j];
            if (this.mNetConfigs[m].isDefault());
            while (true)
            {
                j++;
                break;
                NetworkStateTracker localNetworkStateTracker = this.mNetTrackers[m];
                if ((localNetworkStateTracker.getNetworkInfo().isConnected()) && (!localNetworkStateTracker.isTeardownRequested()))
                {
                    LinkProperties localLinkProperties = localNetworkStateTracker.getLinkProperties();
                    if (localLinkProperties != null)
                    {
                        List localList = this.mNetRequestersPids[m];
                        for (int n = 0; n < localList.size(); n++)
                            if (((Integer)localList.get(n)).intValue() == paramInt)
                            {
                                writePidDns(localLinkProperties.getDnses(), paramInt);
                                if (paramBoolean)
                                    bumpDns();
                                return;
                            }
                    }
                }
            }
        }
        for (int k = 1; ; k++)
        {
            String str = "net.dns" + k + "." + paramInt;
            if (SystemProperties.get(str).length() == 0)
            {
                if (!paramBoolean)
                    break;
                bumpDns();
                break;
            }
            SystemProperties.set(str, "");
        }
    }

    private boolean removeRoute(LinkProperties paramLinkProperties, RouteInfo paramRouteInfo, boolean paramBoolean)
    {
        return modifyRoute(paramLinkProperties.getInterfaceName(), paramLinkProperties, paramRouteInfo, 0, false, paramBoolean);
    }

    private boolean removeRouteToAddress(LinkProperties paramLinkProperties, InetAddress paramInetAddress)
    {
        return modifyRouteToAddress(paramLinkProperties, paramInetAddress, false, true);
    }

    private void sendConnectedBroadcast(NetworkInfo paramNetworkInfo)
    {
        sendGeneralBroadcast(paramNetworkInfo, "android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE");
        sendGeneralBroadcast(paramNetworkInfo, "android.net.conn.CONNECTIVITY_CHANGE");
    }

    private void sendConnectedBroadcastDelayed(NetworkInfo paramNetworkInfo, int paramInt)
    {
        sendGeneralBroadcast(paramNetworkInfo, "android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE");
        sendGeneralBroadcastDelayed(paramNetworkInfo, "android.net.conn.CONNECTIVITY_CHANGE", paramInt);
    }

    private void sendGeneralBroadcast(NetworkInfo paramNetworkInfo, String paramString)
    {
        sendStickyBroadcast(makeGeneralIntent(paramNetworkInfo, paramString));
    }

    private void sendGeneralBroadcastDelayed(NetworkInfo paramNetworkInfo, String paramString, int paramInt)
    {
        sendStickyBroadcastDelayed(makeGeneralIntent(paramNetworkInfo, paramString), paramInt);
    }

    private void sendInetConditionBroadcast(NetworkInfo paramNetworkInfo)
    {
        sendGeneralBroadcast(paramNetworkInfo, "android.net.conn.INET_CONDITION_ACTION");
    }

    private void sendProxyBroadcast(ProxyProperties paramProxyProperties)
    {
        if (paramProxyProperties == null)
            paramProxyProperties = new ProxyProperties("", 0, "");
        log("sending Proxy Broadcast for " + paramProxyProperties);
        Intent localIntent = new Intent("android.intent.action.PROXY_CHANGE");
        localIntent.addFlags(671088640);
        localIntent.putExtra("proxy", paramProxyProperties);
        this.mContext.sendStickyBroadcast(localIntent);
    }

    private void sendStickyBroadcast(Intent paramIntent)
    {
        try
        {
            if (!this.mSystemReady)
                this.mInitialBroadcast = new Intent(paramIntent);
            paramIntent.addFlags(134217728);
            this.mContext.sendStickyBroadcast(paramIntent);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private void sendStickyBroadcastDelayed(Intent paramIntent, int paramInt)
    {
        if (paramInt <= 0)
            sendStickyBroadcast(paramIntent);
        while (true)
        {
            return;
            this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(112, paramIntent), paramInt);
        }
    }

    private void setBufferSize(String paramString)
    {
        try
        {
            String[] arrayOfString = paramString.split(",");
            if (arrayOfString.length == 6)
            {
                FileUtils.stringToFile("/sys/kernel/ipv4/tcp_rmem_min", arrayOfString[0]);
                FileUtils.stringToFile("/sys/kernel/ipv4/tcp_rmem_def", arrayOfString[1]);
                FileUtils.stringToFile("/sys/kernel/ipv4/tcp_rmem_max", arrayOfString[2]);
                FileUtils.stringToFile("/sys/kernel/ipv4/tcp_wmem_min", arrayOfString[3]);
                FileUtils.stringToFile("/sys/kernel/ipv4/tcp_wmem_def", arrayOfString[4]);
                FileUtils.stringToFile("/sys/kernel/ipv4/tcp_wmem_max", arrayOfString[5]);
            }
            else
            {
                loge("Invalid buffersize string: " + paramString);
            }
        }
        catch (IOException localIOException)
        {
            loge("Can't set tcp buffer sizes:" + localIOException);
        }
    }

    // ERROR //
    private int stopUsingNetworkFeature(FeatureUser paramFeatureUser, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_1
        //     1: getfield 1384	com/android/server/ConnectivityService$FeatureUser:mNetworkType	I
        //     4: istore_3
        //     5: aload_1
        //     6: getfield 1387	com/android/server/ConnectivityService$FeatureUser:mFeature	Ljava/lang/String;
        //     9: astore 4
        //     11: aload_1
        //     12: getfield 1390	com/android/server/ConnectivityService$FeatureUser:mPid	I
        //     15: istore 5
        //     17: aload_1
        //     18: getfield 1393	com/android/server/ConnectivityService$FeatureUser:mUid	I
        //     21: pop
        //     22: iconst_0
        //     23: istore 7
        //     25: iload_3
        //     26: invokestatic 827	android/net/ConnectivityManager:isNetworkTypeValid	(I)Z
        //     29: ifne +51 -> 80
        //     32: aload_0
        //     33: new 393	java/lang/StringBuilder
        //     36: dup
        //     37: invokespecial 394	java/lang/StringBuilder:<init>	()V
        //     40: ldc_w 1395
        //     43: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     46: iload_3
        //     47: invokevirtual 403	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     50: ldc_w 1397
        //     53: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     56: aload 4
        //     58: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     61: ldc_w 1399
        //     64: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     67: invokevirtual 407	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     70: invokespecial 230	com/android/server/ConnectivityService:log	(Ljava/lang/String;)V
        //     73: bipush 255
        //     75: istore 14
        //     77: iload 14
        //     79: ireturn
        //     80: aload_0
        //     81: monitorenter
        //     82: aload_0
        //     83: getfield 468	com/android/server/ConnectivityService:mFeatureUsers	Ljava/util/List;
        //     86: aload_1
        //     87: invokeinterface 454 2 0
        //     92: ifne +18 -> 110
        //     95: iconst_1
        //     96: istore 14
        //     98: aload_0
        //     99: monitorexit
        //     100: goto -23 -> 77
        //     103: astore 8
        //     105: aload_0
        //     106: monitorexit
        //     107: aload 8
        //     109: athrow
        //     110: aload_1
        //     111: invokevirtual 1402	com/android/server/ConnectivityService$FeatureUser:unlinkDeathRecipient	()V
        //     114: aload_0
        //     115: getfield 468	com/android/server/ConnectivityService:mFeatureUsers	Ljava/util/List;
        //     118: aload_0
        //     119: getfield 468	com/android/server/ConnectivityService:mFeatureUsers	Ljava/util/List;
        //     122: aload_1
        //     123: invokeinterface 1406 2 0
        //     128: invokeinterface 1408 2 0
        //     133: pop
        //     134: iload_2
        //     135: ifne +49 -> 184
        //     138: aload_0
        //     139: getfield 468	com/android/server/ConnectivityService:mFeatureUsers	Ljava/util/List;
        //     142: invokeinterface 1409 1 0
        //     147: astore 16
        //     149: aload 16
        //     151: invokeinterface 1008 1 0
        //     156: ifeq +28 -> 184
        //     159: aload 16
        //     161: invokeinterface 1012 1 0
        //     166: checkcast 17	com/android/server/ConnectivityService$FeatureUser
        //     169: aload_1
        //     170: invokevirtual 1413	com/android/server/ConnectivityService$FeatureUser:isSameUser	(Lcom/android/server/ConnectivityService$FeatureUser;)Z
        //     173: ifeq -24 -> 149
        //     176: iconst_1
        //     177: istore 14
        //     179: aload_0
        //     180: monitorexit
        //     181: goto -104 -> 77
        //     184: aload_0
        //     185: iload_3
        //     186: aload 4
        //     188: invokevirtual 1417	com/android/server/ConnectivityService:convertFeatureToNetworkType	(ILjava/lang/String;)I
        //     191: istore 10
        //     193: aload_1
        //     194: iload 10
        //     196: invokestatic 1420	com/android/server/ConnectivityService$Injector:stopUsingNetworkFeature	(Lcom/android/server/ConnectivityService$FeatureUser;I)V
        //     199: aload_0
        //     200: getfield 367	com/android/server/ConnectivityService:mNetTrackers	[Landroid/net/NetworkStateTracker;
        //     203: iload 10
        //     205: aaload
        //     206: astore 11
        //     208: aload 11
        //     210: ifnonnull +58 -> 268
        //     213: aload_0
        //     214: new 393	java/lang/StringBuilder
        //     217: dup
        //     218: invokespecial 394	java/lang/StringBuilder:<init>	()V
        //     221: ldc_w 1395
        //     224: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     227: iload_3
        //     228: invokevirtual 403	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     231: ldc_w 1397
        //     234: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     237: aload 4
        //     239: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     242: ldc_w 1422
        //     245: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     248: iload 10
        //     250: invokevirtual 403	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     253: invokevirtual 407	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     256: invokespecial 230	com/android/server/ConnectivityService:log	(Ljava/lang/String;)V
        //     259: bipush 255
        //     261: istore 14
        //     263: aload_0
        //     264: monitorexit
        //     265: goto -188 -> 77
        //     268: iload 10
        //     270: iload_3
        //     271: if_icmpeq +115 -> 386
        //     274: new 444	java/lang/Integer
        //     277: dup
        //     278: iload 5
        //     280: invokespecial 1424	java/lang/Integer:<init>	(I)V
        //     283: astore 12
        //     285: aload_0
        //     286: getfield 466	com/android/server/ConnectivityService:mNetRequestersPids	[Ljava/util/List;
        //     289: iload 10
        //     291: aaload
        //     292: aload 12
        //     294: invokeinterface 1425 2 0
        //     299: pop
        //     300: aload_0
        //     301: iload 5
        //     303: iconst_1
        //     304: invokespecial 1094	com/android/server/ConnectivityService:reassessPidDns	(IZ)V
        //     307: aload_0
        //     308: getfield 466	com/android/server/ConnectivityService:mNetRequestersPids	[Ljava/util/List;
        //     311: iload 10
        //     313: aaload
        //     314: invokeinterface 1088 1 0
        //     319: ifeq +118 -> 437
        //     322: iconst_1
        //     323: istore 14
        //     325: aload_0
        //     326: monitorexit
        //     327: goto -250 -> 77
        //     330: aload_0
        //     331: monitorexit
        //     332: iload 7
        //     334: ifeq +96 -> 430
        //     337: aload_0
        //     338: new 393	java/lang/StringBuilder
        //     341: dup
        //     342: invokespecial 394	java/lang/StringBuilder:<init>	()V
        //     345: ldc_w 1427
        //     348: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     351: iload_3
        //     352: invokevirtual 403	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     355: ldc_w 1397
        //     358: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     361: aload 4
        //     363: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     366: invokevirtual 407	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     369: invokespecial 230	com/android/server/ConnectivityService:log	(Ljava/lang/String;)V
        //     372: aload 11
        //     374: invokeinterface 1429 1 0
        //     379: pop
        //     380: iconst_1
        //     381: istore 14
        //     383: goto -306 -> 77
        //     386: aload_0
        //     387: new 393	java/lang/StringBuilder
        //     390: dup
        //     391: invokespecial 394	java/lang/StringBuilder:<init>	()V
        //     394: ldc_w 1395
        //     397: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     400: iload_3
        //     401: invokevirtual 403	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     404: ldc_w 1397
        //     407: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     410: aload 4
        //     412: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     415: ldc_w 1431
        //     418: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     421: invokevirtual 407	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     424: invokespecial 230	com/android/server/ConnectivityService:log	(Ljava/lang/String;)V
        //     427: goto -97 -> 330
        //     430: bipush 255
        //     432: istore 14
        //     434: goto -357 -> 77
        //     437: iconst_1
        //     438: istore 7
        //     440: goto -110 -> 330
        //
        // Exception table:
        //     from	to	target	type
        //     82	107	103	finally
        //     110	332	103	finally
        //     386	427	103	finally
    }

    private boolean teardown(NetworkStateTracker paramNetworkStateTracker)
    {
        boolean bool = true;
        if (paramNetworkStateTracker.teardown())
            paramNetworkStateTracker.setTeardownRequested(bool);
        while (true)
        {
            return bool;
            bool = false;
        }
    }

    private void tryFailover(int paramInt)
    {
        if (this.mNetConfigs[paramInt].isDefault())
        {
            if (this.mActiveDefaultNetwork == paramInt)
                this.mActiveDefaultNetwork = -1;
            int i = 0;
            if (i <= 13)
            {
                if (i == paramInt);
                while (true)
                {
                    i++;
                    break;
                    if ((this.mNetConfigs[i] != null) && (this.mNetConfigs[i].isDefault()) && (this.mNetTrackers[i] != null))
                    {
                        NetworkStateTracker localNetworkStateTracker = this.mNetTrackers[i];
                        NetworkInfo localNetworkInfo = localNetworkStateTracker.getNetworkInfo();
                        if ((!localNetworkInfo.isConnectedOrConnecting()) || (localNetworkStateTracker.isTeardownRequested()))
                        {
                            localNetworkInfo.setFailover(true);
                            localNetworkStateTracker.reconnect();
                        }
                        log("Attempting to switch to " + localNetworkInfo.getTypeName());
                    }
                }
            }
        }
    }

    private boolean updateDns(String paramString1, String paramString2, Collection<InetAddress> paramCollection, String paramString3)
    {
        boolean bool = false;
        int i = 0;
        if ((paramCollection.size() == 0) && (this.mDefaultDns != null))
        {
            i = 0 + 1;
            String str4 = this.mDefaultDns.getHostAddress();
            if (!str4.equals(SystemProperties.get("net.dns1")))
            {
                loge("no dns provided for " + paramString1 + " - using " + str4);
                bool = true;
                SystemProperties.set("net.dns1", str4);
            }
        }
        while (true)
        {
            for (int j = i + 1; j <= this.mNumDnsEntries; j++)
            {
                String str3 = "net.dns" + j;
                bool = true;
                SystemProperties.set(str3, "");
            }
            Iterator localIterator = paramCollection.iterator();
            while (localIterator.hasNext())
            {
                InetAddress localInetAddress = (InetAddress)localIterator.next();
                i++;
                String str1 = "net.dns" + i;
                String str2 = localInetAddress.getHostAddress();
                if ((bool) || (!str2.equals(SystemProperties.get(str1))))
                {
                    bool = true;
                    SystemProperties.set(str1, str2);
                }
            }
        }
        this.mNumDnsEntries = i;
        if (bool);
        try
        {
            this.mNetd.setDnsServersForInterface(paramString2, NetworkUtils.makeStrings(paramCollection));
            this.mNetd.setDefaultInterfaceForDns(paramString2);
            if (!paramString3.equals(SystemProperties.get("net.dns.search")))
            {
                SystemProperties.set("net.dns.search", paramString3);
                bool = true;
            }
            return bool;
        }
        catch (Exception localException)
        {
            while (true)
                loge("exception setting default dns interface: " + localException);
        }
    }

    private boolean updateRoutes(LinkProperties paramLinkProperties1, LinkProperties paramLinkProperties2, boolean paramBoolean)
    {
        LinkProperties.CompareResult localCompareResult1 = new LinkProperties.CompareResult();
        LinkProperties.CompareResult localCompareResult2 = new LinkProperties.CompareResult();
        if (paramLinkProperties2 != null)
        {
            localCompareResult2 = paramLinkProperties2.compareRoutes(paramLinkProperties1);
            localCompareResult1 = paramLinkProperties2.compareDnses(paramLinkProperties1);
            if ((localCompareResult2.removed.size() == 0) && (localCompareResult2.added.size() == 0))
                break label161;
        }
        label161: for (boolean bool = true; ; bool = false)
        {
            Iterator localIterator1 = localCompareResult2.removed.iterator();
            while (localIterator1.hasNext())
            {
                RouteInfo localRouteInfo2 = (RouteInfo)localIterator1.next();
                if ((paramBoolean) || (!localRouteInfo2.isDefaultRoute()))
                    removeRoute(paramLinkProperties2, localRouteInfo2, true);
                if (!paramBoolean)
                    removeRoute(paramLinkProperties2, localRouteInfo2, false);
            }
            if (paramLinkProperties1 == null)
                break;
            localCompareResult2.added = paramLinkProperties1.getRoutes();
            localCompareResult1.added = paramLinkProperties1.getDnses();
            break;
        }
        Iterator localIterator2 = localCompareResult2.added.iterator();
        while (localIterator2.hasNext())
        {
            RouteInfo localRouteInfo1 = (RouteInfo)localIterator2.next();
            if ((paramBoolean) || (!localRouteInfo1.isDefaultRoute()))
            {
                addRoute(paramLinkProperties1, localRouteInfo1, true);
            }
            else
            {
                addRoute(paramLinkProperties1, localRouteInfo1, false);
                String str = paramLinkProperties1.getInterfaceName();
                if ((!TextUtils.isEmpty(str)) && (!this.mAddedRoutes.contains(localRouteInfo1)))
                    try
                    {
                        this.mNetd.removeRoute(str, localRouteInfo1);
                    }
                    catch (Exception localException)
                    {
                        loge("Exception trying to remove a route: " + localException);
                    }
            }
        }
        if (!paramBoolean)
            if (bool)
            {
                if (paramLinkProperties2 != null)
                {
                    Iterator localIterator6 = paramLinkProperties2.getDnses().iterator();
                    while (localIterator6.hasNext())
                        removeRouteToAddress(paramLinkProperties2, (InetAddress)localIterator6.next());
                }
                if (paramLinkProperties1 != null)
                {
                    Iterator localIterator5 = paramLinkProperties1.getDnses().iterator();
                    while (localIterator5.hasNext())
                        addRouteToAddress(paramLinkProperties1, (InetAddress)localIterator5.next());
                }
            }
            else
            {
                Iterator localIterator3 = localCompareResult1.removed.iterator();
                while (localIterator3.hasNext())
                    removeRouteToAddress(paramLinkProperties2, (InetAddress)localIterator3.next());
                Iterator localIterator4 = localCompareResult1.added.iterator();
                while (localIterator4.hasNext())
                    addRouteToAddress(paramLinkProperties1, (InetAddress)localIterator4.next());
            }
        return bool;
    }

    private boolean writePidDns(Collection<InetAddress> paramCollection, int paramInt)
    {
        int i = 1;
        boolean bool = false;
        Iterator localIterator = paramCollection.iterator();
        while (localIterator.hasNext())
        {
            InetAddress localInetAddress = (InetAddress)localIterator.next();
            String str = localInetAddress.getHostAddress();
            if ((bool) || (!str.equals(SystemProperties.get("net.dns" + i + "." + paramInt))))
            {
                bool = true;
                SystemProperties.set("net.dns" + i + "." + paramInt, localInetAddress.getHostAddress());
            }
            i++;
        }
        return bool;
    }

    int convertFeatureToNetworkType(int paramInt, String paramString)
    {
        int i = paramInt;
        if (paramInt == 0)
            if (TextUtils.equals(paramString, "enableMMS"))
                i = 2;
        while (true)
        {
            return i;
            if (TextUtils.equals(paramString, "enableSUPL"))
            {
                i = 3;
            }
            else if ((TextUtils.equals(paramString, "enableDUN")) || (TextUtils.equals(paramString, "enableDUNAlways")))
            {
                i = 4;
            }
            else if (TextUtils.equals(paramString, "enableHIPRI"))
            {
                i = 5;
            }
            else if (TextUtils.equals(paramString, "enableFOTA"))
            {
                i = 10;
            }
            else if (TextUtils.equals(paramString, "enableIMS"))
            {
                i = 11;
            }
            else if (TextUtils.equals(paramString, "enableCBS"))
            {
                i = 12;
            }
            else
            {
                Slog.e("ConnectivityService", "Can't match any mobile netTracker!");
                continue;
                if (paramInt == 1)
                {
                    if (TextUtils.equals(paramString, "p2p"))
                        i = 13;
                    else
                        Slog.e("ConnectivityService", "Can't match any wifi netTracker!");
                }
                else
                    Slog.e("ConnectivityService", "Unexpected network type");
            }
        }
    }

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.DUMP") != 0)
            paramPrintWriter.println("Permission Denial: can't dump ConnectivityService from from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid());
        while (true)
        {
            return;
            paramPrintWriter.println();
            for (NetworkStateTracker localNetworkStateTracker : this.mNetTrackers)
                if (localNetworkStateTracker != null)
                {
                    if (localNetworkStateTracker.getNetworkInfo().isConnected())
                        paramPrintWriter.println("Active network: " + localNetworkStateTracker.getNetworkInfo().getTypeName());
                    paramPrintWriter.println(localNetworkStateTracker.getNetworkInfo());
                    paramPrintWriter.println(localNetworkStateTracker);
                    paramPrintWriter.println();
                }
            paramPrintWriter.println("Network Requester Pids:");
            for (int i1 : this.mPriorityList)
            {
                String str2 = i1 + ": ";
                Iterator localIterator2 = this.mNetRequestersPids[i1].iterator();
                while (localIterator2.hasNext())
                {
                    Object localObject2 = localIterator2.next();
                    str2 = str2 + localObject2.toString() + ", ";
                }
                paramPrintWriter.println(str2);
            }
            paramPrintWriter.println();
            paramPrintWriter.println("FeatureUsers:");
            Iterator localIterator1 = this.mFeatureUsers.iterator();
            while (localIterator1.hasNext())
                paramPrintWriter.println(((FeatureUser)localIterator1.next()).toString());
            paramPrintWriter.println();
            try
            {
                StringBuilder localStringBuilder = new StringBuilder().append("NetworkTranstionWakeLock is currently ");
                if (this.mNetTransitionWakeLock.isHeld());
                for (String str1 = ""; ; str1 = "not ")
                {
                    paramPrintWriter.println(str1 + "held.");
                    paramPrintWriter.println("It was last requested for " + this.mNetTransitionWakeLockCausedBy);
                    paramPrintWriter.println();
                    this.mTethering.dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
                    if (this.mInetLog == null)
                        break;
                    paramPrintWriter.println();
                    paramPrintWriter.println("Inet condition reports:");
                    for (int n = 0; n < this.mInetLog.size(); n++)
                        paramPrintWriter.println(this.mInetLog.get(n));
                }
            }
            finally
            {
            }
        }
    }

    public ParcelFileDescriptor establishVpn(VpnConfig paramVpnConfig)
    {
        return this.mVpn.establish(paramVpnConfig);
    }

    public LinkProperties getActiveLinkProperties()
    {
        return getLinkProperties(this.mActiveDefaultNetwork);
    }

    public NetworkInfo getActiveNetworkInfo()
    {
        enforceAccessPermission();
        int i = Binder.getCallingUid();
        return getNetworkInfo(this.mActiveDefaultNetwork, i);
    }

    public NetworkInfo getActiveNetworkInfoForUid(int paramInt)
    {
        enforceConnectivityInternalPermission();
        return getNetworkInfo(this.mActiveDefaultNetwork, paramInt);
    }

    public NetworkQuotaInfo getActiveNetworkQuotaInfo()
    {
        enforceAccessPermission();
        long l = Binder.clearCallingIdentity();
        try
        {
            NetworkState localNetworkState = getNetworkStateUnchecked(this.mActiveDefaultNetwork);
            if (localNetworkState != null);
            while (true)
            {
                try
                {
                    NetworkQuotaInfo localNetworkQuotaInfo2 = this.mPolicyManager.getNetworkQuotaInfo(localNetworkState);
                    localNetworkQuotaInfo1 = localNetworkQuotaInfo2;
                    return localNetworkQuotaInfo1;
                }
                catch (RemoteException localRemoteException)
                {
                }
                NetworkQuotaInfo localNetworkQuotaInfo1 = null;
            }
        }
        finally
        {
            Binder.restoreCallingIdentity(l);
        }
    }

    public NetworkInfo[] getAllNetworkInfo()
    {
        enforceAccessPermission();
        int i = Binder.getCallingUid();
        ArrayList localArrayList = Lists.newArrayList();
        while (true)
        {
            int k;
            synchronized (this.mRulesLock)
            {
                NetworkStateTracker[] arrayOfNetworkStateTracker = this.mNetTrackers;
                int j = arrayOfNetworkStateTracker.length;
                k = 0;
                if (k < j)
                {
                    NetworkStateTracker localNetworkStateTracker = arrayOfNetworkStateTracker[k];
                    if (localNetworkStateTracker != null)
                        localArrayList.add(getFilteredNetworkInfo(localNetworkStateTracker, i));
                }
                else
                {
                    return (NetworkInfo[])localArrayList.toArray(new NetworkInfo[localArrayList.size()]);
                }
            }
            k++;
        }
    }

    public NetworkState[] getAllNetworkState()
    {
        enforceAccessPermission();
        int i = Binder.getCallingUid();
        ArrayList localArrayList = Lists.newArrayList();
        while (true)
        {
            int k;
            synchronized (this.mRulesLock)
            {
                NetworkStateTracker[] arrayOfNetworkStateTracker = this.mNetTrackers;
                int j = arrayOfNetworkStateTracker.length;
                k = 0;
                if (k < j)
                {
                    NetworkStateTracker localNetworkStateTracker = arrayOfNetworkStateTracker[k];
                    if (localNetworkStateTracker != null)
                        localArrayList.add(new NetworkState(getFilteredNetworkInfo(localNetworkStateTracker, i), localNetworkStateTracker.getLinkProperties(), localNetworkStateTracker.getLinkCapabilities()));
                }
                else
                {
                    return (NetworkState[])localArrayList.toArray(new NetworkState[localArrayList.size()]);
                }
            }
            k++;
        }
    }

    public ProxyProperties getGlobalProxy()
    {
        synchronized (this.mGlobalProxyLock)
        {
            ProxyProperties localProxyProperties = this.mGlobalProxy;
            return localProxyProperties;
        }
    }

    public int getLastTetherError(String paramString)
    {
        enforceTetherAccessPermission();
        if (isTetheringSupported());
        for (int i = this.mTethering.getLastTetherError(paramString); ; i = 3)
            return i;
    }

    public LegacyVpnInfo getLegacyVpnInfo()
    {
        return this.mVpn.getLegacyVpnInfo();
    }

    public LinkProperties getLinkProperties(int paramInt)
    {
        enforceAccessPermission();
        NetworkStateTracker localNetworkStateTracker;
        if (ConnectivityManager.isNetworkTypeValid(paramInt))
        {
            localNetworkStateTracker = this.mNetTrackers[paramInt];
            if (localNetworkStateTracker == null);
        }
        for (LinkProperties localLinkProperties = localNetworkStateTracker.getLinkProperties(); ; localLinkProperties = null)
            return localLinkProperties;
    }

    public boolean getMobileDataEnabled()
    {
        int i = 1;
        enforceAccessPermission();
        if (Settings.Secure.getInt(this.mContext.getContentResolver(), "mobile_data", i) == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public NetworkInfo getNetworkInfo(int paramInt)
    {
        enforceAccessPermission();
        return getNetworkInfo(paramInt, Binder.getCallingUid());
    }

    public int getNetworkPreference()
    {
        enforceAccessPermission();
        try
        {
            int i = this.mNetworkPreference;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public ProxyProperties getProxy()
    {
        synchronized (this.mDefaultProxyLock)
        {
            if (this.mDefaultProxyDisabled)
            {
                localProxyProperties = null;
                return localProxyProperties;
            }
            ProxyProperties localProxyProperties = this.mDefaultProxy;
        }
    }

    public String[] getTetherableBluetoothRegexs()
    {
        enforceTetherAccessPermission();
        if (isTetheringSupported());
        for (String[] arrayOfString = this.mTethering.getTetherableBluetoothRegexs(); ; arrayOfString = new String[0])
            return arrayOfString;
    }

    public String[] getTetherableIfaces()
    {
        enforceTetherAccessPermission();
        return this.mTethering.getTetherableIfaces();
    }

    public String[] getTetherableUsbRegexs()
    {
        enforceTetherAccessPermission();
        if (isTetheringSupported());
        for (String[] arrayOfString = this.mTethering.getTetherableUsbRegexs(); ; arrayOfString = new String[0])
            return arrayOfString;
    }

    public String[] getTetherableWifiRegexs()
    {
        enforceTetherAccessPermission();
        if (isTetheringSupported());
        for (String[] arrayOfString = this.mTethering.getTetherableWifiRegexs(); ; arrayOfString = new String[0])
            return arrayOfString;
    }

    public String[] getTetheredIfacePairs()
    {
        enforceTetherAccessPermission();
        return this.mTethering.getTetheredIfacePairs();
    }

    public String[] getTetheredIfaces()
    {
        enforceTetherAccessPermission();
        return this.mTethering.getTetheredIfaces();
    }

    public String[] getTetheringErroredIfaces()
    {
        enforceTetherAccessPermission();
        return this.mTethering.getErroredIfaces();
    }

    public boolean isActiveNetworkMetered()
    {
        enforceAccessPermission();
        long l = Binder.clearCallingIdentity();
        try
        {
            boolean bool = isNetworkMeteredUnchecked(this.mActiveDefaultNetwork);
            return bool;
        }
        finally
        {
            Binder.restoreCallingIdentity(l);
        }
    }

    public boolean isNetworkSupported(int paramInt)
    {
        enforceAccessPermission();
        if ((ConnectivityManager.isNetworkTypeValid(paramInt)) && (this.mNetTrackers[paramInt] != null));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isTetheringSupported()
    {
        int i = 1;
        enforceTetherAccessPermission();
        int j;
        boolean bool;
        if (SystemProperties.get("ro.tether.denied").equals("true"))
        {
            j = 0;
            if (Settings.Secure.getInt(this.mContext.getContentResolver(), "tether_supported", j) == 0)
                break label60;
            bool = i;
            label42: if ((!bool) || (!this.mTetheringConfigValid))
                break label65;
        }
        while (true)
        {
            return i;
            j = i;
            break;
            label60: bool = false;
            break label42;
            label65: i = 0;
        }
    }

    public boolean prepareVpn(String paramString1, String paramString2)
    {
        return this.mVpn.prepare(paramString1, paramString2);
    }

    // ERROR //
    public boolean protectVpn(ParcelFileDescriptor paramParcelFileDescriptor)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 188	com/android/server/ConnectivityService:mActiveDefaultNetwork	I
        //     4: istore 7
        //     6: iload 7
        //     8: invokestatic 827	android/net/ConnectivityManager:isNetworkTypeValid	(I)Z
        //     11: ifeq +53 -> 64
        //     14: aload_0
        //     15: getfield 573	com/android/server/ConnectivityService:mVpn	Lcom/android/server/connectivity/Vpn;
        //     18: aload_1
        //     19: aload_0
        //     20: getfield 367	com/android/server/ConnectivityService:mNetTrackers	[Landroid/net/NetworkStateTracker;
        //     23: iload 7
        //     25: aaload
        //     26: invokeinterface 837 1 0
        //     31: invokevirtual 720	android/net/LinkProperties:getInterfaceName	()Ljava/lang/String;
        //     34: invokevirtual 1644	com/android/server/connectivity/Vpn:protect	(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)V
        //     37: iconst_1
        //     38: istore 4
        //     40: aload_1
        //     41: invokevirtual 1649	android/os/ParcelFileDescriptor:close	()V
        //     44: iload 4
        //     46: ireturn
        //     47: astore 5
        //     49: aload_1
        //     50: invokevirtual 1649	android/os/ParcelFileDescriptor:close	()V
        //     53: aload 5
        //     55: athrow
        //     56: astore_2
        //     57: aload_1
        //     58: invokevirtual 1649	android/os/ParcelFileDescriptor:close	()V
        //     61: goto +7 -> 68
        //     64: aload_1
        //     65: invokevirtual 1649	android/os/ParcelFileDescriptor:close	()V
        //     68: iconst_0
        //     69: istore 4
        //     71: goto -27 -> 44
        //     74: astore_3
        //     75: goto -7 -> 68
        //     78: astore 6
        //     80: goto -27 -> 53
        //     83: astore 8
        //     85: goto -41 -> 44
        //
        // Exception table:
        //     from	to	target	type
        //     0	37	47	finally
        //     0	37	56	java/lang/Exception
        //     57	68	74	java/lang/Exception
        //     49	53	78	java/lang/Exception
        //     40	44	83	java/lang/Exception
    }

    public void reportInetCondition(int paramInt1, int paramInt2)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.STATUS_BAR", "ConnectivityService");
        int i = getCallingPid();
        int j = getCallingUid();
        StringBuilder localStringBuilder = new StringBuilder().append(i).append("(").append(j).append(") reports inet is ");
        if (paramInt2 > 50);
        for (String str1 = "connected"; ; str1 = "disconnected")
        {
            String str2 = str1 + " (" + paramInt2 + ") on " + "network Type " + paramInt1 + " at " + GregorianCalendar.getInstance().getTime();
            this.mInetLog.add(str2);
            while (this.mInetLog.size() > 15)
                this.mInetLog.remove(0);
        }
        this.mHandler.sendMessage(this.mHandler.obtainMessage(104, paramInt1, paramInt2));
    }

    public void requestNetworkTransitionWakelock(String paramString)
    {
        enforceConnectivityInternalPermission();
        try
        {
            if (this.mNetTransitionWakeLock.isHeld())
                return;
            this.mNetTransitionWakeLockSerialNumber = (1 + this.mNetTransitionWakeLockSerialNumber);
            this.mNetTransitionWakeLock.acquire();
            this.mNetTransitionWakeLockCausedBy = paramString;
            this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(108, this.mNetTransitionWakeLockSerialNumber, 0), this.mNetTransitionWakeLockTimeout);
        }
        finally
        {
        }
    }

    public boolean requestRouteToHost(int paramInt1, int paramInt2)
    {
        InetAddress localInetAddress = NetworkUtils.intToInetAddress(paramInt2);
        if (localInetAddress == null);
        for (boolean bool = false; ; bool = requestRouteToHostAddress(paramInt1, localInetAddress.getAddress()))
            return bool;
    }

    public boolean requestRouteToHostAddress(int paramInt, byte[] paramArrayOfByte)
    {
        boolean bool1 = false;
        enforceChangePermission();
        if (this.mProtectedNetworks.contains(Integer.valueOf(paramInt)))
            enforceConnectivityInternalPermission();
        if (!ConnectivityManager.isNetworkTypeValid(paramInt))
            log("requestRouteToHostAddress on invalid network: " + paramInt);
        while (true)
        {
            return bool1;
            NetworkStateTracker localNetworkStateTracker = this.mNetTrackers[paramInt];
            if ((localNetworkStateTracker == null) || (!localNetworkStateTracker.getNetworkInfo().isConnected()) || (localNetworkStateTracker.isTeardownRequested()))
                continue;
            long l = Binder.clearCallingIdentity();
            try
            {
                InetAddress localInetAddress = InetAddress.getByAddress(paramArrayOfByte);
                boolean bool2 = addRouteToAddress(localNetworkStateTracker.getLinkProperties(), localInetAddress);
                bool1 = bool2;
                Binder.restoreCallingIdentity(l);
            }
            catch (UnknownHostException localUnknownHostException)
            {
                while (true)
                    log("requestRouteToHostAddress got " + localUnknownHostException.toString());
            }
            finally
            {
                Binder.restoreCallingIdentity(l);
            }
        }
    }

    public void setDataDependency(int paramInt, boolean paramBoolean)
    {
        enforceConnectivityInternalPermission();
        Handler localHandler1 = this.mHandler;
        Handler localHandler2 = this.mHandler;
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localHandler1.sendMessage(localHandler2.obtainMessage(110, i, paramInt));
            return;
        }
    }

    // ERROR //
    public void setGlobalProxy(ProxyProperties paramProxyProperties)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 1705	com/android/server/ConnectivityService:enforceChangePermission	()V
        //     4: aload_0
        //     5: getfield 219	com/android/server/ConnectivityService:mGlobalProxyLock	Ljava/lang/Object;
        //     8: astore_2
        //     9: aload_2
        //     10: monitorenter
        //     11: aload_1
        //     12: aload_0
        //     13: getfield 217	com/android/server/ConnectivityService:mGlobalProxy	Landroid/net/ProxyProperties;
        //     16: if_acmpne +8 -> 24
        //     19: aload_2
        //     20: monitorexit
        //     21: goto +182 -> 203
        //     24: aload_1
        //     25: ifnull +24 -> 49
        //     28: aload_1
        //     29: aload_0
        //     30: getfield 217	com/android/server/ConnectivityService:mGlobalProxy	Landroid/net/ProxyProperties;
        //     33: invokevirtual 863	android/net/ProxyProperties:equals	(Ljava/lang/Object;)Z
        //     36: ifeq +13 -> 49
        //     39: aload_2
        //     40: monitorexit
        //     41: goto +162 -> 203
        //     44: astore_3
        //     45: aload_2
        //     46: monitorexit
        //     47: aload_3
        //     48: athrow
        //     49: aload_0
        //     50: getfield 217	com/android/server/ConnectivityService:mGlobalProxy	Landroid/net/ProxyProperties;
        //     53: ifnull +19 -> 72
        //     56: aload_0
        //     57: getfield 217	com/android/server/ConnectivityService:mGlobalProxy	Landroid/net/ProxyProperties;
        //     60: aload_1
        //     61: invokevirtual 863	android/net/ProxyProperties:equals	(Ljava/lang/Object;)Z
        //     64: ifeq +8 -> 72
        //     67: aload_2
        //     68: monitorexit
        //     69: goto +134 -> 203
        //     72: ldc 202
        //     74: astore 4
        //     76: iconst_0
        //     77: istore 5
        //     79: ldc 202
        //     81: astore 6
        //     83: aload_1
        //     84: ifnull +111 -> 195
        //     87: aload_1
        //     88: invokevirtual 862	android/net/ProxyProperties:getHost	()Ljava/lang/String;
        //     91: invokestatic 268	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
        //     94: ifne +101 -> 195
        //     97: aload_0
        //     98: new 859	android/net/ProxyProperties
        //     101: dup
        //     102: aload_1
        //     103: invokespecial 1717	android/net/ProxyProperties:<init>	(Landroid/net/ProxyProperties;)V
        //     106: putfield 217	com/android/server/ConnectivityService:mGlobalProxy	Landroid/net/ProxyProperties;
        //     109: aload_0
        //     110: getfield 217	com/android/server/ConnectivityService:mGlobalProxy	Landroid/net/ProxyProperties;
        //     113: invokevirtual 862	android/net/ProxyProperties:getHost	()Ljava/lang/String;
        //     116: astore 4
        //     118: aload_0
        //     119: getfield 217	com/android/server/ConnectivityService:mGlobalProxy	Landroid/net/ProxyProperties;
        //     122: invokevirtual 1720	android/net/ProxyProperties:getPort	()I
        //     125: istore 5
        //     127: aload_0
        //     128: getfield 217	com/android/server/ConnectivityService:mGlobalProxy	Landroid/net/ProxyProperties;
        //     131: invokevirtual 1723	android/net/ProxyProperties:getExclusionList	()Ljava/lang/String;
        //     134: astore 6
        //     136: aload_0
        //     137: getfield 326	com/android/server/ConnectivityService:mContext	Landroid/content/Context;
        //     140: invokevirtual 274	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
        //     143: astore 7
        //     145: aload 7
        //     147: ldc_w 1181
        //     150: aload 4
        //     152: invokestatic 1727	android/provider/Settings$Secure:putString	(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
        //     155: pop
        //     156: aload 7
        //     158: ldc_w 1183
        //     161: iload 5
        //     163: invokestatic 1161	android/provider/Settings$Secure:putInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
        //     166: pop
        //     167: aload 7
        //     169: ldc_w 1185
        //     172: aload 6
        //     174: invokestatic 1727	android/provider/Settings$Secure:putString	(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
        //     177: pop
        //     178: aload_2
        //     179: monitorexit
        //     180: aload_0
        //     181: getfield 217	com/android/server/ConnectivityService:mGlobalProxy	Landroid/net/ProxyProperties;
        //     184: ifnonnull +19 -> 203
        //     187: aload_0
        //     188: getfield 211	com/android/server/ConnectivityService:mDefaultProxy	Landroid/net/ProxyProperties;
        //     191: pop
        //     192: goto +11 -> 203
        //     195: aload_0
        //     196: aconst_null
        //     197: putfield 217	com/android/server/ConnectivityService:mGlobalProxy	Landroid/net/ProxyProperties;
        //     200: goto -64 -> 136
        //     203: return
        //
        // Exception table:
        //     from	to	target	type
        //     11	47	44	finally
        //     49	180	44	finally
        //     195	200	44	finally
    }

    public void setMobileDataEnabled(boolean paramBoolean)
    {
        enforceChangePermission();
        log("setMobileDataEnabled(" + paramBoolean + ")");
        Handler localHandler1 = this.mHandler;
        Handler localHandler2 = this.mHandler;
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localHandler1.sendMessage(localHandler2.obtainMessage(107, i, 0));
            return;
        }
    }

    public void setNetworkPreference(int paramInt)
    {
        enforceChangePermission();
        this.mHandler.sendMessage(this.mHandler.obtainMessage(103, paramInt, 0));
    }

    public void setPolicyDataEnable(int paramInt, boolean paramBoolean)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.MANAGE_NETWORK_POLICY", "ConnectivityService");
        Handler localHandler1 = this.mHandler;
        Handler localHandler2 = this.mHandler;
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localHandler1.sendMessage(localHandler2.obtainMessage(113, paramInt, i));
            return;
        }
    }

    public boolean setRadio(int paramInt, boolean paramBoolean)
    {
        boolean bool = false;
        enforceChangePermission();
        if (!ConnectivityManager.isNetworkTypeValid(paramInt));
        while (true)
        {
            return bool;
            NetworkStateTracker localNetworkStateTracker = this.mNetTrackers[paramInt];
            if ((localNetworkStateTracker != null) && (localNetworkStateTracker.setRadio(paramBoolean)))
                bool = true;
        }
    }

    public boolean setRadios(boolean paramBoolean)
    {
        boolean bool = true;
        enforceChangePermission();
        NetworkStateTracker[] arrayOfNetworkStateTracker = this.mNetTrackers;
        int i = arrayOfNetworkStateTracker.length;
        int j = 0;
        if (j < i)
        {
            NetworkStateTracker localNetworkStateTracker = arrayOfNetworkStateTracker[j];
            if (localNetworkStateTracker != null)
                if ((!localNetworkStateTracker.setRadio(paramBoolean)) || (!bool))
                    break label59;
            label59: for (bool = true; ; bool = false)
            {
                j++;
                break;
            }
        }
        return bool;
    }

    public int setUsbTethering(boolean paramBoolean)
    {
        enforceTetherAccessPermission();
        if (isTetheringSupported());
        for (int i = this.mTethering.setUsbTethering(paramBoolean); ; i = 3)
            return i;
    }

    public void startLegacyVpn(VpnConfig paramVpnConfig, String[] paramArrayOfString1, String[] paramArrayOfString2)
    {
        this.mVpn.startLegacyVpn(paramVpnConfig, paramArrayOfString1, paramArrayOfString2);
    }

    // ERROR //
    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public int startUsingNetworkFeature(int paramInt, String paramString, IBinder paramIBinder)
    {
        // Byte code:
        //     0: invokestatic 1754	android/os/SystemClock:elapsedRealtime	()J
        //     3: lstore 4
        //     5: aload_0
        //     6: invokespecial 1705	com/android/server/ConnectivityService:enforceChangePermission	()V
        //     9: iload_1
        //     10: invokestatic 827	android/net/ConnectivityManager:isNetworkTypeValid	(I)Z
        //     13: ifeq +16 -> 29
        //     16: aload_0
        //     17: getfield 382	com/android/server/ConnectivityService:mNetConfigs	[Landroid/net/NetworkConfig;
        //     20: iload_1
        //     21: aaload
        //     22: astore 13
        //     24: aload 13
        //     26: ifnonnull +61 -> 87
        //     29: iconst_3
        //     30: istore 9
        //     32: invokestatic 1754	android/os/SystemClock:elapsedRealtime	()J
        //     35: lload 4
        //     37: lsub
        //     38: lstore 10
        //     40: lload 10
        //     42: ldc2_w 1755
        //     45: lcmp
        //     46: ifle +38 -> 84
        //     49: new 393	java/lang/StringBuilder
        //     52: dup
        //     53: invokespecial 394	java/lang/StringBuilder:<init>	()V
        //     56: ldc_w 1758
        //     59: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     62: lload 10
        //     64: invokevirtual 1761	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     67: ldc_w 1763
        //     70: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     73: invokevirtual 407	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     76: astore 12
        //     78: aload_0
        //     79: aload 12
        //     81: invokespecial 410	com/android/server/ConnectivityService:loge	(Ljava/lang/String;)V
        //     84: iload 9
        //     86: ireturn
        //     87: new 17	com/android/server/ConnectivityService$FeatureUser
        //     90: dup
        //     91: aload_0
        //     92: iload_1
        //     93: aload_2
        //     94: aload_3
        //     95: invokespecial 1766	com/android/server/ConnectivityService$FeatureUser:<init>	(Lcom/android/server/ConnectivityService;ILjava/lang/String;Landroid/os/IBinder;)V
        //     98: astore 14
        //     100: aload_0
        //     101: iload_1
        //     102: aload_2
        //     103: invokevirtual 1417	com/android/server/ConnectivityService:convertFeatureToNetworkType	(ILjava/lang/String;)I
        //     106: istore 15
        //     108: aload_0
        //     109: getfield 437	com/android/server/ConnectivityService:mProtectedNetworks	Ljava/util/List;
        //     112: iload 15
        //     114: invokestatic 448	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     117: invokeinterface 454 2 0
        //     122: ifeq +7 -> 129
        //     125: aload_0
        //     126: invokespecial 1564	com/android/server/ConnectivityService:enforceConnectivityInternalPermission	()V
        //     129: aload_0
        //     130: iload 15
        //     132: invokespecial 1628	com/android/server/ConnectivityService:isNetworkMeteredUnchecked	(I)Z
        //     135: istore 16
        //     137: aload_0
        //     138: getfield 173	com/android/server/ConnectivityService:mRulesLock	Ljava/lang/Object;
        //     141: astore 17
        //     143: aload 17
        //     145: monitorenter
        //     146: aload_0
        //     147: getfield 178	com/android/server/ConnectivityService:mUidRules	Landroid/util/SparseIntArray;
        //     150: invokestatic 1515	android/os/Binder:getCallingUid	()I
        //     153: iconst_0
        //     154: invokevirtual 1172	android/util/SparseIntArray:get	(II)I
        //     157: istore 19
        //     159: aload 17
        //     161: monitorexit
        //     162: iload 16
        //     164: ifeq +123 -> 287
        //     167: iload 19
        //     169: iconst_1
        //     170: iand
        //     171: ifeq +116 -> 287
        //     174: iconst_3
        //     175: istore 9
        //     177: invokestatic 1754	android/os/SystemClock:elapsedRealtime	()J
        //     180: lload 4
        //     182: lsub
        //     183: lstore 49
        //     185: lload 49
        //     187: ldc2_w 1755
        //     190: lcmp
        //     191: ifle -107 -> 84
        //     194: new 393	java/lang/StringBuilder
        //     197: dup
        //     198: invokespecial 394	java/lang/StringBuilder:<init>	()V
        //     201: ldc_w 1758
        //     204: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     207: lload 49
        //     209: invokevirtual 1761	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     212: ldc_w 1763
        //     215: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     218: invokevirtual 407	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     221: astore 12
        //     223: goto -145 -> 78
        //     226: astore 18
        //     228: aload 17
        //     230: monitorexit
        //     231: aload 18
        //     233: athrow
        //     234: astore 6
        //     236: invokestatic 1754	android/os/SystemClock:elapsedRealtime	()J
        //     239: lload 4
        //     241: lsub
        //     242: lstore 7
        //     244: lload 7
        //     246: ldc2_w 1755
        //     249: lcmp
        //     250: ifle +34 -> 284
        //     253: aload_0
        //     254: new 393	java/lang/StringBuilder
        //     257: dup
        //     258: invokespecial 394	java/lang/StringBuilder:<init>	()V
        //     261: ldc_w 1758
        //     264: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     267: lload 7
        //     269: invokevirtual 1761	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     272: ldc_w 1763
        //     275: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     278: invokevirtual 407	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     281: invokespecial 410	com/android/server/ConnectivityService:loge	(Ljava/lang/String;)V
        //     284: aload 6
        //     286: athrow
        //     287: aload_0
        //     288: getfield 367	com/android/server/ConnectivityService:mNetTrackers	[Landroid/net/NetworkStateTracker;
        //     291: iload 15
        //     293: aaload
        //     294: astore 20
        //     296: aload 20
        //     298: ifnull +666 -> 964
        //     301: new 444	java/lang/Integer
        //     304: dup
        //     305: invokestatic 1653	com/android/server/ConnectivityService:getCallingPid	()I
        //     308: invokespecial 1424	java/lang/Integer:<init>	(I)V
        //     311: astore 21
        //     313: iload 15
        //     315: iload_1
        //     316: if_icmpeq +540 -> 856
        //     319: aload 20
        //     321: invokeinterface 772 1 0
        //     326: astore 27
        //     328: aload 27
        //     330: invokevirtual 1767	android/net/NetworkInfo:isAvailable	()Z
        //     333: ifne +121 -> 454
        //     336: aload_2
        //     337: ldc_w 1480
        //     340: invokestatic 1061	android/text/TextUtils:equals	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
        //     343: ifne +83 -> 426
        //     346: aload_0
        //     347: new 393	java/lang/StringBuilder
        //     350: dup
        //     351: invokespecial 394	java/lang/StringBuilder:<init>	()V
        //     354: ldc_w 1769
        //     357: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     360: aload 27
        //     362: invokevirtual 874	android/net/NetworkInfo:getTypeName	()Ljava/lang/String;
        //     365: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     368: invokevirtual 407	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     371: invokespecial 230	com/android/server/ConnectivityService:log	(Ljava/lang/String;)V
        //     374: iconst_2
        //     375: istore 9
        //     377: invokestatic 1754	android/os/SystemClock:elapsedRealtime	()J
        //     380: lload 4
        //     382: lsub
        //     383: lstore 45
        //     385: lload 45
        //     387: ldc2_w 1755
        //     390: lcmp
        //     391: ifle -307 -> 84
        //     394: new 393	java/lang/StringBuilder
        //     397: dup
        //     398: invokespecial 394	java/lang/StringBuilder:<init>	()V
        //     401: ldc_w 1758
        //     404: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     407: lload 45
        //     409: invokevirtual 1761	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     412: ldc_w 1763
        //     415: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     418: invokevirtual 407	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     421: astore 12
        //     423: goto -345 -> 78
        //     426: aload_0
        //     427: new 393	java/lang/StringBuilder
        //     430: dup
        //     431: invokespecial 394	java/lang/StringBuilder:<init>	()V
        //     434: ldc_w 1771
        //     437: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     440: aload 27
        //     442: invokevirtual 874	android/net/NetworkInfo:getTypeName	()Ljava/lang/String;
        //     445: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     448: invokevirtual 407	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     451: invokespecial 230	com/android/server/ConnectivityService:log	(Ljava/lang/String;)V
        //     454: aload_0
        //     455: iload 15
        //     457: invokespecial 1773	com/android/server/ConnectivityService:getRestoreDefaultNetworkDelay	(I)I
        //     460: istore 28
        //     462: aload_0
        //     463: monitorenter
        //     464: iconst_1
        //     465: istore 29
        //     467: iload 28
        //     469: ifge +45 -> 514
        //     472: aload_0
        //     473: getfield 468	com/android/server/ConnectivityService:mFeatureUsers	Ljava/util/List;
        //     476: invokeinterface 1409 1 0
        //     481: astore 44
        //     483: aload 44
        //     485: invokeinterface 1008 1 0
        //     490: ifeq +24 -> 514
        //     493: aload 44
        //     495: invokeinterface 1012 1 0
        //     500: checkcast 17	com/android/server/ConnectivityService$FeatureUser
        //     503: aload 14
        //     505: invokevirtual 1413	com/android/server/ConnectivityService$FeatureUser:isSameUser	(Lcom/android/server/ConnectivityService$FeatureUser;)Z
        //     508: ifeq -25 -> 483
        //     511: iconst_0
        //     512: istore 29
        //     514: iload 29
        //     516: ifeq +15 -> 531
        //     519: aload_0
        //     520: getfield 468	com/android/server/ConnectivityService:mFeatureUsers	Ljava/util/List;
        //     523: aload 14
        //     525: invokeinterface 457 2 0
        //     530: pop
        //     531: aload_0
        //     532: getfield 466	com/android/server/ConnectivityService:mNetRequestersPids	[Ljava/util/List;
        //     535: iload 15
        //     537: aaload
        //     538: aload 21
        //     540: invokeinterface 454 2 0
        //     545: ifne +18 -> 563
        //     548: aload_0
        //     549: getfield 466	com/android/server/ConnectivityService:mNetRequestersPids	[Ljava/util/List;
        //     552: iload 15
        //     554: aaload
        //     555: aload 21
        //     557: invokeinterface 457 2 0
        //     562: pop
        //     563: aload_0
        //     564: monitorexit
        //     565: iload 15
        //     567: invokestatic 1775	com/android/server/ConnectivityService$Injector:startUsingNetworkFeature	(I)V
        //     570: iload 28
        //     572: iflt +25 -> 597
        //     575: aload_0
        //     576: getfield 254	com/android/server/ConnectivityService:mHandler	Landroid/os/Handler;
        //     579: aload_0
        //     580: getfield 254	com/android/server/ConnectivityService:mHandler	Landroid/os/Handler;
        //     583: bipush 101
        //     585: aload 14
        //     587: invokevirtual 1355	android/os/Handler:obtainMessage	(ILjava/lang/Object;)Landroid/os/Message;
        //     590: iload 28
        //     592: i2l
        //     593: invokevirtual 893	android/os/Handler:sendMessageDelayed	(Landroid/os/Message;J)Z
        //     596: pop
        //     597: aload 27
        //     599: invokevirtual 1434	android/net/NetworkInfo:isConnectedOrConnecting	()Z
        //     602: iconst_1
        //     603: if_icmpne +159 -> 762
        //     606: aload 20
        //     608: invokeinterface 1101 1 0
        //     613: ifne +149 -> 762
        //     616: aload 27
        //     618: invokevirtual 777	android/net/NetworkInfo:isConnected	()Z
        //     621: iconst_1
        //     622: if_icmpne +88 -> 710
        //     625: invokestatic 1570	android/os/Binder:clearCallingIdentity	()J
        //     628: lstore 36
        //     630: aload_0
        //     631: iload 15
        //     633: invokespecial 661	com/android/server/ConnectivityService:handleDnsConfigurationChange	(I)V
        //     636: lload 36
        //     638: invokestatic 1578	android/os/Binder:restoreCallingIdentity	(J)V
        //     641: iconst_0
        //     642: istore 9
        //     644: invokestatic 1754	android/os/SystemClock:elapsedRealtime	()J
        //     647: lload 4
        //     649: lsub
        //     650: lstore 39
        //     652: lload 39
        //     654: ldc2_w 1755
        //     657: lcmp
        //     658: ifle -574 -> 84
        //     661: new 393	java/lang/StringBuilder
        //     664: dup
        //     665: invokespecial 394	java/lang/StringBuilder:<init>	()V
        //     668: ldc_w 1758
        //     671: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     674: lload 39
        //     676: invokevirtual 1761	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     679: ldc_w 1763
        //     682: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     685: invokevirtual 407	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     688: astore 12
        //     690: goto -612 -> 78
        //     693: astore 30
        //     695: aload_0
        //     696: monitorexit
        //     697: aload 30
        //     699: athrow
        //     700: astore 38
        //     702: lload 36
        //     704: invokestatic 1578	android/os/Binder:restoreCallingIdentity	(J)V
        //     707: aload 38
        //     709: athrow
        //     710: iconst_1
        //     711: istore 9
        //     713: invokestatic 1754	android/os/SystemClock:elapsedRealtime	()J
        //     716: lload 4
        //     718: lsub
        //     719: lstore 34
        //     721: lload 34
        //     723: ldc2_w 1755
        //     726: lcmp
        //     727: ifle -643 -> 84
        //     730: new 393	java/lang/StringBuilder
        //     733: dup
        //     734: invokespecial 394	java/lang/StringBuilder:<init>	()V
        //     737: ldc_w 1758
        //     740: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     743: lload 34
        //     745: invokevirtual 1761	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     748: ldc_w 1763
        //     751: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     754: invokevirtual 407	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     757: astore 12
        //     759: goto -681 -> 78
        //     762: aload_0
        //     763: new 393	java/lang/StringBuilder
        //     766: dup
        //     767: invokespecial 394	java/lang/StringBuilder:<init>	()V
        //     770: ldc_w 1777
        //     773: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     776: iload_1
        //     777: invokevirtual 403	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     780: ldc_w 1397
        //     783: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     786: aload_2
        //     787: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     790: invokevirtual 407	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     793: invokespecial 230	com/android/server/ConnectivityService:log	(Ljava/lang/String;)V
        //     796: aload 20
        //     798: invokeinterface 504 1 0
        //     803: pop
        //     804: iconst_1
        //     805: istore 9
        //     807: invokestatic 1754	android/os/SystemClock:elapsedRealtime	()J
        //     810: lload 4
        //     812: lsub
        //     813: lstore 32
        //     815: lload 32
        //     817: ldc2_w 1755
        //     820: lcmp
        //     821: ifle -737 -> 84
        //     824: new 393	java/lang/StringBuilder
        //     827: dup
        //     828: invokespecial 394	java/lang/StringBuilder:<init>	()V
        //     831: ldc_w 1758
        //     834: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     837: lload 32
        //     839: invokevirtual 1761	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     842: ldc_w 1763
        //     845: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     848: invokevirtual 407	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     851: astore 12
        //     853: goto -775 -> 78
        //     856: aload_0
        //     857: monitorenter
        //     858: aload_0
        //     859: getfield 468	com/android/server/ConnectivityService:mFeatureUsers	Ljava/util/List;
        //     862: aload 14
        //     864: invokeinterface 457 2 0
        //     869: pop
        //     870: aload_0
        //     871: getfield 466	com/android/server/ConnectivityService:mNetRequestersPids	[Ljava/util/List;
        //     874: iload 15
        //     876: aaload
        //     877: aload 21
        //     879: invokeinterface 454 2 0
        //     884: ifne +18 -> 902
        //     887: aload_0
        //     888: getfield 466	com/android/server/ConnectivityService:mNetRequestersPids	[Ljava/util/List;
        //     891: iload 15
        //     893: aaload
        //     894: aload 21
        //     896: invokeinterface 457 2 0
        //     901: pop
        //     902: aload_0
        //     903: monitorexit
        //     904: bipush 255
        //     906: istore 9
        //     908: invokestatic 1754	android/os/SystemClock:elapsedRealtime	()J
        //     911: lload 4
        //     913: lsub
        //     914: lstore 24
        //     916: lload 24
        //     918: ldc2_w 1755
        //     921: lcmp
        //     922: ifle -838 -> 84
        //     925: new 393	java/lang/StringBuilder
        //     928: dup
        //     929: invokespecial 394	java/lang/StringBuilder:<init>	()V
        //     932: ldc_w 1758
        //     935: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     938: lload 24
        //     940: invokevirtual 1761	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     943: ldc_w 1763
        //     946: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     949: invokevirtual 407	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     952: astore 12
        //     954: goto -876 -> 78
        //     957: astore 22
        //     959: aload_0
        //     960: monitorexit
        //     961: aload 22
        //     963: athrow
        //     964: iconst_2
        //     965: istore 9
        //     967: invokestatic 1754	android/os/SystemClock:elapsedRealtime	()J
        //     970: lload 4
        //     972: lsub
        //     973: lstore 47
        //     975: lload 47
        //     977: ldc2_w 1755
        //     980: lcmp
        //     981: ifle -897 -> 84
        //     984: new 393	java/lang/StringBuilder
        //     987: dup
        //     988: invokespecial 394	java/lang/StringBuilder:<init>	()V
        //     991: ldc_w 1758
        //     994: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     997: lload 47
        //     999: invokevirtual 1761	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     1002: ldc_w 1763
        //     1005: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1008: invokevirtual 407	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1011: astore 12
        //     1013: goto -935 -> 78
        //
        // Exception table:
        //     from	to	target	type
        //     146	162	226	finally
        //     228	231	226	finally
        //     9	24	234	finally
        //     87	146	234	finally
        //     231	234	234	finally
        //     287	374	234	finally
        //     426	464	234	finally
        //     565	630	234	finally
        //     636	641	234	finally
        //     697	710	234	finally
        //     762	804	234	finally
        //     856	858	234	finally
        //     961	964	234	finally
        //     472	565	693	finally
        //     695	697	693	finally
        //     630	636	700	finally
        //     858	904	957	finally
        //     959	961	957	finally
    }

    public int stopUsingNetworkFeature(int paramInt, String paramString)
    {
        boolean bool = true;
        enforceChangePermission();
        int j = getCallingPid();
        int k = getCallingUid();
        Object localObject1 = null;
        int m = 0;
        try
        {
            Iterator localIterator = this.mFeatureUsers.iterator();
            while (localIterator.hasNext())
            {
                FeatureUser localFeatureUser = (FeatureUser)localIterator.next();
                if (localFeatureUser.isSameUser(j, k, paramInt, paramString))
                {
                    localObject1 = localFeatureUser;
                    m = 1;
                }
            }
            int i;
            if ((m != 0) && (localObject1 != null))
                i = stopUsingNetworkFeature(localObject1, bool);
            return i;
        }
        finally
        {
        }
    }

    void systemReady()
    {
        try
        {
            this.mSystemReady = true;
            if (this.mInitialBroadcast != null)
            {
                this.mContext.sendStickyBroadcast(this.mInitialBroadcast);
                this.mInitialBroadcast = null;
            }
            this.mHandler.sendMessage(this.mHandler.obtainMessage(109));
            return;
        }
        finally
        {
        }
    }

    public int tether(String paramString)
    {
        enforceTetherChangePermission();
        if (isTetheringSupported());
        for (int i = this.mTethering.tether(paramString); ; i = 3)
            return i;
    }

    public int untether(String paramString)
    {
        enforceTetherChangePermission();
        if (isTetheringSupported());
        for (int i = this.mTethering.untether(paramString); ; i = 3)
            return i;
    }

    public void updateNetworkSettings(NetworkStateTracker paramNetworkStateTracker)
    {
        String str = SystemProperties.get(paramNetworkStateTracker.getTcpBufferSizesPropName());
        if (str.length() == 0)
            str = SystemProperties.get("net.tcp.buffersize.default");
        if (str.length() != 0)
            setBufferSize(str);
    }

    public class VpnCallback
    {
        private VpnCallback()
        {
        }

        public void override(List<String> paramList1, List<String> paramList2)
        {
            if (paramList1 == null)
                restore();
            ArrayList localArrayList;
            while (true)
            {
                return;
                localArrayList = new ArrayList();
                Iterator localIterator1 = paramList1.iterator();
                while (localIterator1.hasNext())
                {
                    String str2 = (String)localIterator1.next();
                    try
                    {
                        localArrayList.add(InetAddress.parseNumericAddress(str2));
                    }
                    catch (Exception localException)
                    {
                    }
                }
                if (!localArrayList.isEmpty())
                    break;
                restore();
            }
            StringBuilder localStringBuilder = new StringBuilder();
            if (paramList2 != null)
            {
                Iterator localIterator2 = paramList2.iterator();
                while (localIterator2.hasNext())
                    localStringBuilder.append((String)localIterator2.next()).append(' ');
            }
            String str1 = localStringBuilder.toString().trim();
            synchronized (ConnectivityService.this.mDnsLock)
            {
                while (true)
                {
                    boolean bool = ConnectivityService.this.updateDns("VPN", "VPN", localArrayList, str1);
                    ConnectivityService.access$2702(ConnectivityService.this, true);
                    if (bool)
                        ConnectivityService.this.bumpDns();
                    synchronized (ConnectivityService.this.mDefaultProxyLock)
                    {
                        ConnectivityService.access$3002(ConnectivityService.this, true);
                        if (ConnectivityService.this.mDefaultProxy != null)
                            ConnectivityService.this.sendProxyBroadcast(null);
                    }
                }
            }
        }

        public void restore()
        {
            synchronized (ConnectivityService.this.mDnsLock)
            {
                if (ConnectivityService.this.mDnsOverridden)
                {
                    ConnectivityService.access$2702(ConnectivityService.this, false);
                    ConnectivityService.this.mHandler.sendEmptyMessage(111);
                }
            }
            synchronized (ConnectivityService.this.mDefaultProxyLock)
            {
                ConnectivityService.access$3002(ConnectivityService.this, false);
                if (ConnectivityService.this.mDefaultProxy != null)
                    ConnectivityService.this.sendProxyBroadcast(ConnectivityService.this.mDefaultProxy);
                return;
                localObject2 = finally;
                throw localObject2;
            }
        }
    }

    private static class SettingsObserver extends ContentObserver
    {
        private Handler mHandler;
        private int mWhat;

        SettingsObserver(Handler paramHandler, int paramInt)
        {
            super();
            this.mHandler = paramHandler;
            this.mWhat = paramInt;
        }

        void observe(Context paramContext)
        {
            paramContext.getContentResolver().registerContentObserver(Settings.Secure.getUriFor("http_proxy"), false, this);
        }

        public void onChange(boolean paramBoolean)
        {
            this.mHandler.obtainMessage(this.mWhat).sendToTarget();
        }
    }

    private class MyHandler extends Handler
    {
        public MyHandler(Looper arg2)
        {
            super();
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
            case 1:
            case 3:
            case 108:
            case 101:
            case 104:
            case 105:
            case 103:
            case 107:
            case 109:
            case 110:
            case 111:
            case 112:
                while (true)
                {
                    return;
                    NetworkInfo localNetworkInfo2 = (NetworkInfo)paramMessage.obj;
                    localNetworkInfo2.getType();
                    NetworkInfo.State localState = localNetworkInfo2.getState();
                    if ((localState == NetworkInfo.State.CONNECTED) || (localState == NetworkInfo.State.DISCONNECTED))
                        ConnectivityService.this.log("ConnectivityChange for " + localNetworkInfo2.getTypeName() + ": " + localState + "/" + localNetworkInfo2.getDetailedState());
                    EventLog.writeEvent(50020, 0xF & localNetworkInfo2.getType() | (0x3F & localNetworkInfo2.getDetailedState().ordinal()) << 4 | localNetworkInfo2.getSubtype() << 10);
                    if (localNetworkInfo2.getDetailedState() == NetworkInfo.DetailedState.FAILED)
                    {
                        ConnectivityService.this.handleConnectionFailure(localNetworkInfo2);
                    }
                    else if (localState == NetworkInfo.State.DISCONNECTED)
                    {
                        ConnectivityService.this.handleDisconnect(localNetworkInfo2);
                    }
                    else if (localState == NetworkInfo.State.SUSPENDED)
                    {
                        ConnectivityService.this.handleDisconnect(localNetworkInfo2);
                    }
                    else if (localState == NetworkInfo.State.CONNECTED)
                    {
                        ConnectivityService.this.handleConnect(localNetworkInfo2);
                        continue;
                        NetworkInfo localNetworkInfo1 = (NetworkInfo)paramMessage.obj;
                        ConnectivityService.this.handleConnectivityChange(localNetworkInfo1.getType(), false);
                        continue;
                        String str = null;
                        synchronized (ConnectivityService.this)
                        {
                            if ((paramMessage.arg1 == ConnectivityService.this.mNetTransitionWakeLockSerialNumber) && (ConnectivityService.this.mNetTransitionWakeLock.isHeld()))
                            {
                                ConnectivityService.this.mNetTransitionWakeLock.release();
                                str = ConnectivityService.this.mNetTransitionWakeLockCausedBy;
                            }
                            if (str == null)
                                continue;
                            ConnectivityService.this.log("NetTransition Wakelock for " + str + " released by timeout");
                        }
                        ((ConnectivityService.FeatureUser)paramMessage.obj).expire();
                        continue;
                        int n = paramMessage.arg1;
                        int i1 = paramMessage.arg2;
                        ConnectivityService.this.handleInetConditionChange(n, i1);
                        continue;
                        int k = paramMessage.arg1;
                        int m = paramMessage.arg2;
                        ConnectivityService.this.handleInetConditionHoldEnd(k, m);
                        continue;
                        int j = paramMessage.arg1;
                        ConnectivityService.this.handleSetNetworkPreference(j);
                        continue;
                        if (paramMessage.arg1 == 1);
                        for (boolean bool3 = true; ; bool3 = false)
                        {
                            ConnectivityService.this.handleSetMobileData(bool3);
                            break;
                        }
                        ConnectivityService.this.handleDeprecatedGlobalHttpProxy();
                        continue;
                        if (paramMessage.arg1 == 1);
                        for (boolean bool2 = true; ; bool2 = false)
                        {
                            ConnectivityService.this.handleSetDependencyMet(paramMessage.arg2, bool2);
                            break;
                        }
                        if (ConnectivityService.this.mActiveDefaultNetwork != -1)
                        {
                            ConnectivityService.this.handleDnsConfigurationChange(ConnectivityService.this.mActiveDefaultNetwork);
                            continue;
                            Intent localIntent = (Intent)paramMessage.obj;
                            ConnectivityService.this.sendStickyBroadcast(localIntent);
                        }
                    }
                }
            case 113:
            }
            int i = paramMessage.arg1;
            if (paramMessage.arg2 == 1);
            for (boolean bool1 = true; ; bool1 = false)
            {
                ConnectivityService.this.handleSetPolicyDataEnable(i, bool1);
                break;
            }
        }
    }

    private class FeatureUser
        implements IBinder.DeathRecipient
    {
        IBinder mBinder;
        long mCreateTime;
        String mFeature;
        int mNetworkType;
        int mPid;
        int mUid;

        FeatureUser(int paramString, String paramIBinder, IBinder arg4)
        {
            this.mNetworkType = paramString;
            this.mFeature = paramIBinder;
            Object localObject;
            this.mBinder = localObject;
            this.mPid = Binder.getCallingPid();
            this.mUid = Binder.getCallingUid();
            this.mCreateTime = System.currentTimeMillis();
            try
            {
                this.mBinder.linkToDeath(this, 0);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    binderDied();
            }
        }

        public void binderDied()
        {
            ConnectivityService.this.log("ConnectivityService FeatureUser binderDied(" + this.mNetworkType + ", " + this.mFeature + ", " + this.mBinder + "), created " + (System.currentTimeMillis() - this.mCreateTime) + " mSec ago");
            ConnectivityService.this.stopUsingNetworkFeature(this, false);
        }

        public void expire()
        {
            ConnectivityService.this.stopUsingNetworkFeature(this, false);
        }

        public boolean isSameUser(int paramInt1, int paramInt2, int paramInt3, String paramString)
        {
            if ((this.mPid == paramInt1) && (this.mUid == paramInt2) && (this.mNetworkType == paramInt3) && (TextUtils.equals(this.mFeature, paramString)));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public boolean isSameUser(FeatureUser paramFeatureUser)
        {
            if (paramFeatureUser == null);
            for (boolean bool = false; ; bool = isSameUser(paramFeatureUser.mPid, paramFeatureUser.mUid, paramFeatureUser.mNetworkType, paramFeatureUser.mFeature))
                return bool;
        }

        public String toString()
        {
            return "FeatureUser(" + this.mNetworkType + "," + this.mFeature + "," + this.mPid + "," + this.mUid + "), created " + (System.currentTimeMillis() - this.mCreateTime) + " mSec ago";
        }

        void unlinkDeathRecipient()
        {
            this.mBinder.unlinkToDeath(this, 0);
        }
    }

    private static class RadioAttributes
    {
        public int mSimultaneity;
        public int mType;

        public RadioAttributes(String paramString)
        {
            String[] arrayOfString = paramString.split(",");
            this.mType = Integer.parseInt(arrayOfString[0]);
            this.mSimultaneity = Integer.parseInt(arrayOfString[1]);
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static void startUsingNetworkFeature(int paramInt)
        {
            FirewallManager.getInstance().onStartUsingNetworkFeature(Binder.getCallingUid(), Binder.getCallingPid(), paramInt);
        }

        public static void stopUsingNetworkFeature(ConnectivityService.FeatureUser paramFeatureUser, int paramInt)
        {
            FirewallManager.getInstance().onStopUsingNetworkFeature(paramFeatureUser.mUid, paramFeatureUser.mPid, paramInt);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.ConnectivityService
 * JD-Core Version:        0.6.2
 */