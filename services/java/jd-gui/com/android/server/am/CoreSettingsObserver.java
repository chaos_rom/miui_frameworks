package com.android.server.am;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.util.Log;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

class CoreSettingsObserver extends ContentObserver
{
    private static final String LOG_TAG = CoreSettingsObserver.class.getSimpleName();
    private static final Map<String, Class<?>> sCoreSettingToTypeMap = new HashMap();
    private final ActivityManagerService mActivityManagerService;
    private final Bundle mCoreSettings = new Bundle();

    static
    {
        sCoreSettingToTypeMap.put("long_press_timeout", Integer.TYPE);
    }

    public CoreSettingsObserver(ActivityManagerService paramActivityManagerService)
    {
        super(paramActivityManagerService.mHandler);
        this.mActivityManagerService = paramActivityManagerService;
        beginObserveCoreSettings();
        sendCoreSettings();
    }

    private void beginObserveCoreSettings()
    {
        Iterator localIterator = sCoreSettingToTypeMap.keySet().iterator();
        while (localIterator.hasNext())
        {
            Uri localUri = Settings.Secure.getUriFor((String)localIterator.next());
            this.mActivityManagerService.mContext.getContentResolver().registerContentObserver(localUri, false, this);
        }
    }

    private void populateCoreSettings(Bundle paramBundle)
    {
        Context localContext = this.mActivityManagerService.mContext;
        Iterator localIterator = sCoreSettingToTypeMap.entrySet().iterator();
        while (localIterator.hasNext())
        {
            Map.Entry localEntry = (Map.Entry)localIterator.next();
            String str = (String)localEntry.getKey();
            Class localClass = (Class)localEntry.getValue();
            try
            {
                if (localClass != String.class)
                    break label130;
                paramBundle.putString(str, Settings.Secure.getString(localContext.getContentResolver(), str));
            }
            catch (Settings.SettingNotFoundException localSettingNotFoundException)
            {
                Log.w(LOG_TAG, "Cannot find setting \"" + str + "\"", localSettingNotFoundException);
            }
            continue;
            label130: if (localClass == Integer.TYPE)
                paramBundle.putInt(str, Settings.Secure.getInt(localContext.getContentResolver(), str));
            else if (localClass == Float.TYPE)
                paramBundle.putFloat(str, Settings.Secure.getFloat(localContext.getContentResolver(), str));
            else if (localClass == Long.TYPE)
                paramBundle.putLong(str, Settings.Secure.getLong(localContext.getContentResolver(), str));
        }
    }

    private void sendCoreSettings()
    {
        populateCoreSettings(this.mCoreSettings);
        this.mActivityManagerService.onCoreSettingsChange(this.mCoreSettings);
    }

    public Bundle getCoreSettingsLocked()
    {
        return (Bundle)this.mCoreSettings.clone();
    }

    public void onChange(boolean paramBoolean)
    {
        synchronized (this.mActivityManagerService)
        {
            sendCoreSettings();
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.CoreSettingsObserver
 * JD-Core Version:        0.6.2
 */