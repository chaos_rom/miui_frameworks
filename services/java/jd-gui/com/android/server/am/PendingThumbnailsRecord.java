package com.android.server.am;

import android.app.IThumbnailReceiver;
import java.util.HashSet;

class PendingThumbnailsRecord
{
    boolean finished;
    HashSet pendingRecords;
    final IThumbnailReceiver receiver;

    PendingThumbnailsRecord(IThumbnailReceiver paramIThumbnailReceiver)
    {
        this.receiver = paramIThumbnailReceiver;
        this.pendingRecords = new HashSet();
        this.finished = false;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.PendingThumbnailsRecord
 * JD-Core Version:        0.6.2
 */