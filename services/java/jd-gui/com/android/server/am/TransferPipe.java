package com.android.server.am;

import android.os.IBinder;
import android.os.IInterface;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

class TransferPipe
    implements Runnable
{
    static final boolean DEBUG = false;
    static final long DEFAULT_TIMEOUT = 5000L;
    static final String TAG = "TransferPipe";
    String mBufferPrefix;
    boolean mComplete;
    long mEndTime;
    String mFailure;
    final ParcelFileDescriptor[] mFds = ParcelFileDescriptor.createPipe();
    FileDescriptor mOutFd;
    final Thread mThread = new Thread(this, "TransferPipe");

    TransferPipe()
        throws IOException
    {
    }

    static void go(Caller paramCaller, IInterface paramIInterface, FileDescriptor paramFileDescriptor, String paramString, String[] paramArrayOfString)
        throws IOException, RemoteException
    {
        go(paramCaller, paramIInterface, paramFileDescriptor, paramString, paramArrayOfString, 5000L);
    }

    // ERROR //
    static void go(Caller paramCaller, IInterface paramIInterface, FileDescriptor paramFileDescriptor, String paramString, String[] paramArrayOfString, long paramLong)
        throws IOException, RemoteException
    {
        // Byte code:
        //     0: aload_1
        //     1: invokeinterface 64 1 0
        //     6: instanceof 66
        //     9: ifeq +15 -> 24
        //     12: aload_0
        //     13: aload_1
        //     14: aload_2
        //     15: aload_3
        //     16: aload 4
        //     18: invokeinterface 69 5 0
        //     23: return
        //     24: new 2	com/android/server/am/TransferPipe
        //     27: dup
        //     28: invokespecial 70	com/android/server/am/TransferPipe:<init>	()V
        //     31: astore 7
        //     33: aload_0
        //     34: aload_1
        //     35: aload 7
        //     37: invokevirtual 74	com/android/server/am/TransferPipe:getWriteFd	()Landroid/os/ParcelFileDescriptor;
        //     40: invokevirtual 78	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
        //     43: aload_3
        //     44: aload 4
        //     46: invokeinterface 69 5 0
        //     51: aload 7
        //     53: aload_2
        //     54: lload 5
        //     56: invokevirtual 81	com/android/server/am/TransferPipe:go	(Ljava/io/FileDescriptor;J)V
        //     59: aload 7
        //     61: invokevirtual 84	com/android/server/am/TransferPipe:kill	()V
        //     64: goto -41 -> 23
        //     67: astore 8
        //     69: aload 7
        //     71: invokevirtual 84	com/android/server/am/TransferPipe:kill	()V
        //     74: aload 8
        //     76: athrow
        //     77: astore 9
        //     79: goto -56 -> 23
        //
        // Exception table:
        //     from	to	target	type
        //     33	59	67	finally
        //     12	23	77	android/os/RemoteException
    }

    static void goDump(IBinder paramIBinder, FileDescriptor paramFileDescriptor, String[] paramArrayOfString)
        throws IOException, RemoteException
    {
        goDump(paramIBinder, paramFileDescriptor, paramArrayOfString, 5000L);
    }

    // ERROR //
    static void goDump(IBinder paramIBinder, FileDescriptor paramFileDescriptor, String[] paramArrayOfString, long paramLong)
        throws IOException, RemoteException
    {
        // Byte code:
        //     0: aload_0
        //     1: instanceof 66
        //     4: ifeq +12 -> 16
        //     7: aload_0
        //     8: aload_1
        //     9: aload_2
        //     10: invokeinterface 95 3 0
        //     15: return
        //     16: new 2	com/android/server/am/TransferPipe
        //     19: dup
        //     20: invokespecial 70	com/android/server/am/TransferPipe:<init>	()V
        //     23: astore 5
        //     25: aload_0
        //     26: aload 5
        //     28: invokevirtual 74	com/android/server/am/TransferPipe:getWriteFd	()Landroid/os/ParcelFileDescriptor;
        //     31: invokevirtual 78	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
        //     34: aload_2
        //     35: invokeinterface 98 3 0
        //     40: aload 5
        //     42: aload_1
        //     43: lload_3
        //     44: invokevirtual 81	com/android/server/am/TransferPipe:go	(Ljava/io/FileDescriptor;J)V
        //     47: aload 5
        //     49: invokevirtual 84	com/android/server/am/TransferPipe:kill	()V
        //     52: goto -37 -> 15
        //     55: astore 6
        //     57: aload 5
        //     59: invokevirtual 84	com/android/server/am/TransferPipe:kill	()V
        //     62: aload 6
        //     64: athrow
        //     65: astore 7
        //     67: goto -52 -> 15
        //
        // Exception table:
        //     from	to	target	type
        //     25	47	55	finally
        //     7	15	65	android/os/RemoteException
    }

    void closeFd(int paramInt)
    {
        if (this.mFds[paramInt] != null);
        try
        {
            this.mFds[paramInt].close();
            label18: this.mFds[paramInt] = null;
            return;
        }
        catch (IOException localIOException)
        {
            break label18;
        }
    }

    ParcelFileDescriptor getReadFd()
    {
        return this.mFds[0];
    }

    ParcelFileDescriptor getWriteFd()
    {
        return this.mFds[1];
    }

    void go(FileDescriptor paramFileDescriptor)
        throws IOException
    {
        go(paramFileDescriptor, 5000L);
    }

    // ERROR //
    void go(FileDescriptor paramFileDescriptor, long paramLong)
        throws IOException
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: aload_1
        //     4: putfield 109	com/android/server/am/TransferPipe:mOutFd	Ljava/io/FileDescriptor;
        //     7: aload_0
        //     8: lload_2
        //     9: invokestatic 115	android/os/SystemClock:uptimeMillis	()J
        //     12: ladd
        //     13: putfield 117	com/android/server/am/TransferPipe:mEndTime	J
        //     16: aload_0
        //     17: iconst_1
        //     18: invokevirtual 119	com/android/server/am/TransferPipe:closeFd	(I)V
        //     21: aload_0
        //     22: getfield 43	com/android/server/am/TransferPipe:mThread	Ljava/lang/Thread;
        //     25: invokevirtual 122	java/lang/Thread:start	()V
        //     28: aload_0
        //     29: getfield 124	com/android/server/am/TransferPipe:mFailure	Ljava/lang/String;
        //     32: ifnonnull +74 -> 106
        //     35: aload_0
        //     36: getfield 126	com/android/server/am/TransferPipe:mComplete	Z
        //     39: ifne +67 -> 106
        //     42: aload_0
        //     43: getfield 117	com/android/server/am/TransferPipe:mEndTime	J
        //     46: invokestatic 115	android/os/SystemClock:uptimeMillis	()J
        //     49: lsub
        //     50: lstore 6
        //     52: lload 6
        //     54: lconst_0
        //     55: lcmp
        //     56: ifgt +36 -> 92
        //     59: aload_0
        //     60: getfield 43	com/android/server/am/TransferPipe:mThread	Ljava/lang/Thread;
        //     63: invokevirtual 129	java/lang/Thread:interrupt	()V
        //     66: new 34	java/io/IOException
        //     69: dup
        //     70: ldc 131
        //     72: invokespecial 134	java/io/IOException:<init>	(Ljava/lang/String;)V
        //     75: athrow
        //     76: astore 5
        //     78: aload_0
        //     79: monitorexit
        //     80: aload 5
        //     82: athrow
        //     83: astore 4
        //     85: aload_0
        //     86: invokevirtual 84	com/android/server/am/TransferPipe:kill	()V
        //     89: aload 4
        //     91: athrow
        //     92: aload_0
        //     93: lload 6
        //     95: invokevirtual 138	java/lang/Object:wait	(J)V
        //     98: goto -70 -> 28
        //     101: astore 8
        //     103: goto -75 -> 28
        //     106: aload_0
        //     107: getfield 124	com/android/server/am/TransferPipe:mFailure	Ljava/lang/String;
        //     110: ifnull +15 -> 125
        //     113: new 34	java/io/IOException
        //     116: dup
        //     117: aload_0
        //     118: getfield 124	com/android/server/am/TransferPipe:mFailure	Ljava/lang/String;
        //     121: invokespecial 134	java/io/IOException:<init>	(Ljava/lang/String;)V
        //     124: athrow
        //     125: aload_0
        //     126: monitorexit
        //     127: aload_0
        //     128: invokevirtual 84	com/android/server/am/TransferPipe:kill	()V
        //     131: return
        //
        // Exception table:
        //     from	to	target	type
        //     2	80	76	finally
        //     92	98	76	finally
        //     106	127	76	finally
        //     0	2	83	finally
        //     80	83	83	finally
        //     92	98	101	java/lang/InterruptedException
    }

    void kill()
    {
        closeFd(0);
        closeFd(1);
    }

    public void run()
    {
        byte[] arrayOfByte1 = new byte[1024];
        FileInputStream localFileInputStream = new FileInputStream(getReadFd().getFileDescriptor());
        FileOutputStream localFileOutputStream = new FileOutputStream(this.mOutFd);
        byte[] arrayOfByte2 = null;
        int i = 1;
        if (this.mBufferPrefix != null)
            arrayOfByte2 = this.mBufferPrefix.getBytes();
        int j;
        try
        {
            while (true)
            {
                j = localFileInputStream.read(arrayOfByte1);
                if (j <= 0)
                    break label206;
                if (arrayOfByte2 != null)
                    break;
                localFileOutputStream.write(arrayOfByte1, 0, j);
            }
        }
        catch (IOException localIOException)
        {
        }
        while (true)
        {
            int m;
            try
            {
                this.mFailure = localIOException.toString();
                notifyAll();
                return;
                int k = 0;
                m = 0;
                if (m < j)
                {
                    if (arrayOfByte1[m] == 10)
                        break label260;
                    if (m > k)
                        localFileOutputStream.write(arrayOfByte1, k, m - k);
                    k = m;
                    if (i != 0)
                    {
                        localFileOutputStream.write(arrayOfByte2);
                        i = 0;
                    }
                    m++;
                    if (m < j)
                        if (arrayOfByte1[m] != 10)
                            continue;
                }
                else
                {
                    if (j <= k)
                        break;
                    localFileOutputStream.write(arrayOfByte1, k, j - k);
                    break;
                    label206: boolean bool = this.mThread.isInterrupted();
                    if (bool);
                    try
                    {
                        this.mComplete = true;
                        notifyAll();
                        continue;
                    }
                    finally
                    {
                        localObject2 = finally;
                        throw localObject2;
                    }
                }
            }
            finally
            {
            }
            if (m < j)
                i = 1;
            label260: m++;
        }
    }

    void setBufferPrefix(String paramString)
    {
        this.mBufferPrefix = paramString;
    }

    static abstract interface Caller
    {
        public abstract void go(IInterface paramIInterface, FileDescriptor paramFileDescriptor, String paramString, String[] paramArrayOfString)
            throws RemoteException;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.TransferPipe
 * JD-Core Version:        0.6.2
 */