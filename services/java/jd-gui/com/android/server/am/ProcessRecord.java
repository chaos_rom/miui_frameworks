package com.android.server.am;

import android.app.ActivityManager.ProcessErrorStateInfo;
import android.app.Dialog;
import android.app.IApplicationThread;
import android.app.IInstrumentationWatcher;
import android.content.ComponentName;
import android.content.pm.ApplicationInfo;
import android.content.res.CompatibilityInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.SystemClock;
import android.os.UserId;
import android.util.PrintWriterPrinter;
import android.util.TimeUtils;
import com.android.internal.os.BatteryStatsImpl;
import com.android.internal.os.BatteryStatsImpl.Uid.Proc;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

class ProcessRecord
{
    final ArrayList<ActivityRecord> activities = new ArrayList();
    int adjSeq;
    Object adjSource;
    int adjSourceOom;
    Object adjTarget;
    String adjType;
    int adjTypeCode;
    Dialog anrDialog;
    boolean bad;
    final BatteryStatsImpl.Uid.Proc batteryStats;
    CompatibilityInfo compat;
    final ArrayList<ContentProviderConnection> conProviders = new ArrayList();
    final HashSet<ConnectionRecord> connections = new HashSet();
    Dialog crashDialog;
    boolean crashing;
    ActivityManager.ProcessErrorStateInfo crashingReport;
    int curAdj;
    long curCpuTime;
    int curRawAdj;
    BroadcastRecord curReceiver;
    int curSchedGroup;
    IBinder.DeathRecipient deathRecipient;
    boolean debugging;
    boolean empty;
    ComponentName errorReportReceiver;
    final HashSet<ServiceRecord> executingServices = new HashSet();
    IBinder forcingToForeground;
    boolean foregroundActivities;
    boolean foregroundServices;
    boolean hasAboveClient;
    boolean hasShownUi;
    boolean hidden;
    int hiddenAdj;
    final ApplicationInfo info;
    Bundle instrumentationArguments;
    ComponentName instrumentationClass;
    ApplicationInfo instrumentationInfo;
    String instrumentationProfileFile;
    ComponentName instrumentationResultClass;
    IInstrumentationWatcher instrumentationWatcher;
    final boolean isolated;
    boolean keeping;
    boolean killedBackground;
    long lastActivityTime;
    long lastCpuTime;
    long lastLowMemory;
    int lastPss;
    long lastRequestedGc;
    long lastWakeTime;
    int lruSeq;
    long lruWeight;
    int maxAdj;
    int memImportance;
    int nonStoppingAdj;
    boolean notResponding;
    ActivityManager.ProcessErrorStateInfo notRespondingReport;
    boolean pendingUiClean;
    boolean persistent;
    int pid;
    final HashSet<String> pkgList = new HashSet();
    final String processName;
    final HashMap<String, ContentProviderRecord> pubProviders = new HashMap();
    final HashSet<ReceiverList> receivers = new HashSet();
    boolean removed;
    boolean reportLowMemory;
    boolean serviceb;
    final HashSet<ServiceRecord> services = new HashSet();
    int setAdj;
    boolean setIsForeground;
    int setRawAdj;
    int setSchedGroup;
    String shortStringName;
    boolean starting;
    String stringName;
    boolean systemNoUi;
    IApplicationThread thread;
    int trimMemoryLevel;
    final int uid;
    final int userId;
    boolean usingWrapper;
    Dialog waitDialog;
    boolean waitedForDebugger;
    String waitingToKill;

    ProcessRecord(BatteryStatsImpl.Uid.Proc paramProc, IApplicationThread paramIApplicationThread, ApplicationInfo paramApplicationInfo, String paramString, int paramInt)
    {
        this.batteryStats = paramProc;
        this.info = paramApplicationInfo;
        if (paramApplicationInfo.uid != paramInt);
        for (boolean bool = true; ; bool = false)
        {
            this.isolated = bool;
            this.uid = paramInt;
            this.userId = UserId.getUserId(paramInt);
            this.processName = paramString;
            this.pkgList.add(paramApplicationInfo.packageName);
            this.thread = paramIApplicationThread;
            this.maxAdj = 15;
            this.hiddenAdj = ProcessList.HIDDEN_APP_MIN_ADJ;
            this.setRawAdj = -100;
            this.curRawAdj = -100;
            this.setAdj = -100;
            this.curAdj = -100;
            this.persistent = false;
            this.removed = false;
            return;
        }
    }

    public boolean addPackage(String paramString)
    {
        if (!this.pkgList.contains(paramString))
            this.pkgList.add(paramString);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    void dump(PrintWriter paramPrintWriter, String paramString)
    {
        long l1 = SystemClock.uptimeMillis();
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("user #");
        paramPrintWriter.print(this.userId);
        paramPrintWriter.print(" uid=");
        paramPrintWriter.print(this.info.uid);
        if (this.uid != this.info.uid)
        {
            paramPrintWriter.print(" ISOLATED uid=");
            paramPrintWriter.print(this.uid);
        }
        paramPrintWriter.println();
        if (this.info.className != null)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("class=");
            paramPrintWriter.println(this.info.className);
        }
        if (this.info.manageSpaceActivityName != null)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("manageSpaceActivityName=");
            paramPrintWriter.println(this.info.manageSpaceActivityName);
        }
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("dir=");
        paramPrintWriter.print(this.info.sourceDir);
        paramPrintWriter.print(" publicDir=");
        paramPrintWriter.print(this.info.publicSourceDir);
        paramPrintWriter.print(" data=");
        paramPrintWriter.println(this.info.dataDir);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("packageList=");
        paramPrintWriter.println(this.pkgList);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("compat=");
        paramPrintWriter.println(this.compat);
        if ((this.instrumentationClass != null) || (this.instrumentationProfileFile != null) || (this.instrumentationArguments != null))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("instrumentationClass=");
            paramPrintWriter.print(this.instrumentationClass);
            paramPrintWriter.print(" instrumentationProfileFile=");
            paramPrintWriter.println(this.instrumentationProfileFile);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("instrumentationArguments=");
            paramPrintWriter.println(this.instrumentationArguments);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("instrumentationInfo=");
            paramPrintWriter.println(this.instrumentationInfo);
            if (this.instrumentationInfo != null)
            {
                ApplicationInfo localApplicationInfo = this.instrumentationInfo;
                PrintWriterPrinter localPrintWriterPrinter = new PrintWriterPrinter(paramPrintWriter);
                localApplicationInfo.dump(localPrintWriterPrinter, paramString + "    ");
            }
        }
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("thread=");
        paramPrintWriter.print(this.thread);
        paramPrintWriter.print(" curReceiver=");
        paramPrintWriter.println(this.curReceiver);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("pid=");
        paramPrintWriter.print(this.pid);
        paramPrintWriter.print(" starting=");
        paramPrintWriter.print(this.starting);
        paramPrintWriter.print(" lastPss=");
        paramPrintWriter.println(this.lastPss);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("lastActivityTime=");
        TimeUtils.formatDuration(this.lastActivityTime, l1, paramPrintWriter);
        paramPrintWriter.print(" lruWeight=");
        paramPrintWriter.print(this.lruWeight);
        paramPrintWriter.print(" serviceb=");
        paramPrintWriter.print(this.serviceb);
        paramPrintWriter.print(" keeping=");
        paramPrintWriter.print(this.keeping);
        paramPrintWriter.print(" hidden=");
        paramPrintWriter.print(this.hidden);
        paramPrintWriter.print(" empty=");
        paramPrintWriter.println(this.empty);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("oom: max=");
        paramPrintWriter.print(this.maxAdj);
        paramPrintWriter.print(" hidden=");
        paramPrintWriter.print(this.hiddenAdj);
        paramPrintWriter.print(" curRaw=");
        paramPrintWriter.print(this.curRawAdj);
        paramPrintWriter.print(" setRaw=");
        paramPrintWriter.print(this.setRawAdj);
        paramPrintWriter.print(" nonStopping=");
        paramPrintWriter.print(this.nonStoppingAdj);
        paramPrintWriter.print(" cur=");
        paramPrintWriter.print(this.curAdj);
        paramPrintWriter.print(" set=");
        paramPrintWriter.println(this.setAdj);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("curSchedGroup=");
        paramPrintWriter.print(this.curSchedGroup);
        paramPrintWriter.print(" setSchedGroup=");
        paramPrintWriter.print(this.setSchedGroup);
        paramPrintWriter.print(" systemNoUi=");
        paramPrintWriter.print(this.systemNoUi);
        paramPrintWriter.print(" trimMemoryLevel=");
        paramPrintWriter.println(this.trimMemoryLevel);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("hasShownUi=");
        paramPrintWriter.print(this.hasShownUi);
        paramPrintWriter.print(" pendingUiClean=");
        paramPrintWriter.print(this.pendingUiClean);
        paramPrintWriter.print(" hasAboveClient=");
        paramPrintWriter.println(this.hasAboveClient);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("setIsForeground=");
        paramPrintWriter.print(this.setIsForeground);
        paramPrintWriter.print(" foregroundServices=");
        paramPrintWriter.print(this.foregroundServices);
        paramPrintWriter.print(" forcingToForeground=");
        paramPrintWriter.println(this.forcingToForeground);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("persistent=");
        paramPrintWriter.print(this.persistent);
        paramPrintWriter.print(" removed=");
        paramPrintWriter.println(this.removed);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("adjSeq=");
        paramPrintWriter.print(this.adjSeq);
        paramPrintWriter.print(" lruSeq=");
        paramPrintWriter.println(this.lruSeq);
        if (!this.keeping);
        synchronized (this.batteryStats.getBatteryStats())
        {
            long l2 = this.batteryStats.getBatteryStats().getProcessWakeTime(this.info.uid, this.pid, SystemClock.elapsedRealtime());
            long l3 = l2 - this.lastWakeTime;
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("lastWakeTime=");
            paramPrintWriter.print(this.lastWakeTime);
            paramPrintWriter.print(" time used=");
            TimeUtils.formatDuration(l3, paramPrintWriter);
            paramPrintWriter.println("");
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("lastCpuTime=");
            paramPrintWriter.print(this.lastCpuTime);
            paramPrintWriter.print(" time used=");
            TimeUtils.formatDuration(this.curCpuTime - this.lastCpuTime, paramPrintWriter);
            paramPrintWriter.println("");
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("lastRequestedGc=");
            TimeUtils.formatDuration(this.lastRequestedGc, l1, paramPrintWriter);
            paramPrintWriter.print(" lastLowMemory=");
            TimeUtils.formatDuration(this.lastLowMemory, l1, paramPrintWriter);
            paramPrintWriter.print(" reportLowMemory=");
            paramPrintWriter.println(this.reportLowMemory);
            if ((this.killedBackground) || (this.waitingToKill != null))
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("killedBackground=");
                paramPrintWriter.print(this.killedBackground);
                paramPrintWriter.print(" waitingToKill=");
                paramPrintWriter.println(this.waitingToKill);
            }
            if ((this.debugging) || (this.crashing) || (this.crashDialog != null) || (this.notResponding) || (this.anrDialog != null) || (this.bad))
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("debugging=");
                paramPrintWriter.print(this.debugging);
                paramPrintWriter.print(" crashing=");
                paramPrintWriter.print(this.crashing);
                paramPrintWriter.print(" ");
                paramPrintWriter.print(this.crashDialog);
                paramPrintWriter.print(" notResponding=");
                paramPrintWriter.print(this.notResponding);
                paramPrintWriter.print(" ");
                paramPrintWriter.print(this.anrDialog);
                paramPrintWriter.print(" bad=");
                paramPrintWriter.print(this.bad);
                if (this.errorReportReceiver != null)
                {
                    paramPrintWriter.print(" errorReportReceiver=");
                    paramPrintWriter.print(this.errorReportReceiver.flattenToShortString());
                }
                paramPrintWriter.println();
            }
            if (this.activities.size() > 0)
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.println("Activities:");
                int j = 0;
                if (j < this.activities.size())
                {
                    paramPrintWriter.print(paramString);
                    paramPrintWriter.print("    - ");
                    paramPrintWriter.println(this.activities.get(j));
                    j++;
                }
            }
        }
        if (this.services.size() > 0)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.println("Services:");
            Iterator localIterator5 = this.services.iterator();
            while (localIterator5.hasNext())
            {
                ServiceRecord localServiceRecord2 = (ServiceRecord)localIterator5.next();
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("    - ");
                paramPrintWriter.println(localServiceRecord2);
            }
        }
        if (this.executingServices.size() > 0)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.println("Executing Services:");
            Iterator localIterator4 = this.executingServices.iterator();
            while (localIterator4.hasNext())
            {
                ServiceRecord localServiceRecord1 = (ServiceRecord)localIterator4.next();
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("    - ");
                paramPrintWriter.println(localServiceRecord1);
            }
        }
        if (this.connections.size() > 0)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.println("Connections:");
            Iterator localIterator3 = this.connections.iterator();
            while (localIterator3.hasNext())
            {
                ConnectionRecord localConnectionRecord = (ConnectionRecord)localIterator3.next();
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("    - ");
                paramPrintWriter.println(localConnectionRecord);
            }
        }
        if (this.pubProviders.size() > 0)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.println("Published Providers:");
            Iterator localIterator2 = this.pubProviders.entrySet().iterator();
            while (localIterator2.hasNext())
            {
                Map.Entry localEntry = (Map.Entry)localIterator2.next();
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("    - ");
                paramPrintWriter.println((String)localEntry.getKey());
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("        -> ");
                paramPrintWriter.println(localEntry.getValue());
            }
        }
        if (this.conProviders.size() > 0)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.println("Connected Providers:");
            for (int i = 0; i < this.conProviders.size(); i++)
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("    - ");
                paramPrintWriter.println(((ContentProviderConnection)this.conProviders.get(i)).toShortString());
            }
        }
        if (this.receivers.size() > 0)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.println("Receivers:");
            Iterator localIterator1 = this.receivers.iterator();
            while (localIterator1.hasNext())
            {
                ReceiverList localReceiverList = (ReceiverList)localIterator1.next();
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("    - ");
                paramPrintWriter.println(localReceiverList);
            }
        }
    }

    public String[] getPackageList()
    {
        int i = this.pkgList.size();
        String[] arrayOfString;
        if (i == 0)
            arrayOfString = null;
        while (true)
        {
            return arrayOfString;
            arrayOfString = new String[i];
            this.pkgList.toArray(arrayOfString);
        }
    }

    public boolean isInterestingToUserLocked()
    {
        int i = this.activities.size();
        int j = 0;
        if (j < i)
            if (!((ActivityRecord)this.activities.get(j)).isInterestingToUserLocked());
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            j++;
            break;
        }
    }

    public void resetPackageList()
    {
        this.pkgList.clear();
        this.pkgList.add(this.info.packageName);
    }

    public void setPid(int paramInt)
    {
        this.pid = paramInt;
        this.shortStringName = null;
        this.stringName = null;
    }

    public void stopFreezingAllLocked()
    {
        int i = this.activities.size();
        while (i > 0)
        {
            i--;
            ((ActivityRecord)this.activities.get(i)).stopFreezingScreenLocked(true);
        }
    }

    public String toShortString()
    {
        String str;
        if (this.shortStringName != null)
            str = this.shortStringName;
        while (true)
        {
            return str;
            StringBuilder localStringBuilder = new StringBuilder(128);
            toShortString(localStringBuilder);
            str = localStringBuilder.toString();
            this.shortStringName = str;
        }
    }

    void toShortString(StringBuilder paramStringBuilder)
    {
        paramStringBuilder.append(this.pid);
        paramStringBuilder.append(':');
        paramStringBuilder.append(this.processName);
        paramStringBuilder.append('/');
        if (this.info.uid < 10000)
            paramStringBuilder.append(this.uid);
        while (true)
        {
            return;
            paramStringBuilder.append('u');
            paramStringBuilder.append(this.userId);
            paramStringBuilder.append('a');
            paramStringBuilder.append(this.info.uid % 10000);
            if (this.uid != this.info.uid)
            {
                paramStringBuilder.append('i');
                paramStringBuilder.append(UserId.getAppId(this.uid) - 99000);
            }
        }
    }

    public String toString()
    {
        String str;
        if (this.stringName != null)
            str = this.stringName;
        while (true)
        {
            return str;
            StringBuilder localStringBuilder = new StringBuilder(128);
            localStringBuilder.append("ProcessRecord{");
            localStringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
            localStringBuilder.append(' ');
            toShortString(localStringBuilder);
            localStringBuilder.append('}');
            str = localStringBuilder.toString();
            this.stringName = str;
        }
    }

    public void unlinkDeathRecipient()
    {
        if ((this.deathRecipient != null) && (this.thread != null))
            this.thread.asBinder().unlinkToDeath(this.deathRecipient, 0);
        this.deathRecipient = null;
    }

    void updateHasAboveClientLocked()
    {
        this.hasAboveClient = false;
        if (this.connections.size() > 0)
        {
            Iterator localIterator = this.connections.iterator();
            while (localIterator.hasNext())
                if ((0x8 & ((ConnectionRecord)localIterator.next()).flags) != 0)
                    this.hasAboveClient = true;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.ProcessRecord
 * JD-Core Version:        0.6.2
 */