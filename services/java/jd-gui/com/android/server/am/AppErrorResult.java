package com.android.server.am;

class AppErrorResult
{
    boolean mHasResult = false;
    int mResult;

    public int get()
    {
        try
        {
            while (true)
            {
                boolean bool = this.mHasResult;
                if (bool)
                    break;
                try
                {
                    wait();
                }
                catch (InterruptedException localInterruptedException)
                {
                }
            }
            return this.mResult;
        }
        finally
        {
        }
    }

    public void set(int paramInt)
    {
        try
        {
            this.mHasResult = true;
            this.mResult = paramInt;
            notifyAll();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.AppErrorResult
 * JD-Core Version:        0.6.2
 */