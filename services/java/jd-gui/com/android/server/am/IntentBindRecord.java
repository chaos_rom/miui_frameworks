package com.android.server.am;

import android.content.Intent;
import android.content.Intent.FilterComparison;
import android.os.IBinder;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

class IntentBindRecord
{
    final HashMap<ProcessRecord, AppBindRecord> apps = new HashMap();
    IBinder binder;
    boolean doRebind;
    boolean hasBound;
    final Intent.FilterComparison intent;
    boolean received;
    boolean requested;
    final ServiceRecord service;
    String stringName;

    IntentBindRecord(ServiceRecord paramServiceRecord, Intent.FilterComparison paramFilterComparison)
    {
        this.service = paramServiceRecord;
        this.intent = paramFilterComparison;
    }

    void dump(PrintWriter paramPrintWriter, String paramString)
    {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("service=");
        paramPrintWriter.println(this.service);
        dumpInService(paramPrintWriter, paramString);
    }

    void dumpInService(PrintWriter paramPrintWriter, String paramString)
    {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("intent={");
        paramPrintWriter.print(this.intent.getIntent().toShortString(false, true, false, false));
        paramPrintWriter.println('}');
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("binder=");
        paramPrintWriter.println(this.binder);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("requested=");
        paramPrintWriter.print(this.requested);
        paramPrintWriter.print(" received=");
        paramPrintWriter.print(this.received);
        paramPrintWriter.print(" hasBound=");
        paramPrintWriter.print(this.hasBound);
        paramPrintWriter.print(" doRebind=");
        paramPrintWriter.println(this.doRebind);
        if (this.apps.size() > 0)
        {
            Iterator localIterator = this.apps.values().iterator();
            while (localIterator.hasNext())
            {
                AppBindRecord localAppBindRecord = (AppBindRecord)localIterator.next();
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("* Client AppBindRecord{");
                paramPrintWriter.print(Integer.toHexString(System.identityHashCode(localAppBindRecord)));
                paramPrintWriter.print(' ');
                paramPrintWriter.print(localAppBindRecord.client);
                paramPrintWriter.println('}');
                localAppBindRecord.dumpInIntentBind(paramPrintWriter, paramString + "    ");
            }
        }
    }

    public String toString()
    {
        String str;
        if (this.stringName != null)
            str = this.stringName;
        while (true)
        {
            return str;
            StringBuilder localStringBuilder = new StringBuilder(128);
            localStringBuilder.append("IntentBindRecord{");
            localStringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
            localStringBuilder.append(' ');
            localStringBuilder.append(this.service.shortName);
            localStringBuilder.append(':');
            if (this.intent != null)
                this.intent.getIntent().toShortString(localStringBuilder, false, false, false, false);
            localStringBuilder.append('}');
            str = localStringBuilder.toString();
            this.stringName = str;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.IntentBindRecord
 * JD-Core Version:        0.6.2
 */