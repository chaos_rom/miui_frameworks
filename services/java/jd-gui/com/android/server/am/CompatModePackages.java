package com.android.server.am;

import android.app.AppGlobals;
import android.app.IApplicationThread;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.IPackageManager;
import android.content.res.CompatibilityInfo;
import android.content.res.Configuration;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.util.Slog;
import com.android.internal.os.AtomicFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class CompatModePackages
{
    public static final int COMPAT_FLAG_DONT_ASK = 1;
    public static final int COMPAT_FLAG_ENABLED = 2;
    private static final int MSG_WRITE = 300;
    private final boolean DEBUG_CONFIGURATION;
    private final String TAG;
    private final AtomicFile mFile;
    private final Handler mHandler;
    private final HashMap<String, Integer> mPackages;
    private final ActivityManagerService mService;

    // ERROR //
    public CompatModePackages(ActivityManagerService paramActivityManagerService, java.io.File paramFile)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 37	java/lang/Object:<init>	()V
        //     4: aload_0
        //     5: ldc 39
        //     7: putfield 41	com/android/server/am/CompatModePackages:TAG	Ljava/lang/String;
        //     10: aload_0
        //     11: iconst_0
        //     12: putfield 43	com/android/server/am/CompatModePackages:DEBUG_CONFIGURATION	Z
        //     15: aload_0
        //     16: new 45	java/util/HashMap
        //     19: dup
        //     20: invokespecial 46	java/util/HashMap:<init>	()V
        //     23: putfield 48	com/android/server/am/CompatModePackages:mPackages	Ljava/util/HashMap;
        //     26: aload_0
        //     27: new 6	com/android/server/am/CompatModePackages$1
        //     30: dup
        //     31: aload_0
        //     32: invokespecial 51	com/android/server/am/CompatModePackages$1:<init>	(Lcom/android/server/am/CompatModePackages;)V
        //     35: putfield 53	com/android/server/am/CompatModePackages:mHandler	Landroid/os/Handler;
        //     38: aload_0
        //     39: aload_1
        //     40: putfield 55	com/android/server/am/CompatModePackages:mService	Lcom/android/server/am/ActivityManagerService;
        //     43: aload_0
        //     44: new 57	com/android/internal/os/AtomicFile
        //     47: dup
        //     48: new 59	java/io/File
        //     51: dup
        //     52: aload_2
        //     53: ldc 61
        //     55: invokespecial 64	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     58: invokespecial 67	com/android/internal/os/AtomicFile:<init>	(Ljava/io/File;)V
        //     61: putfield 69	com/android/server/am/CompatModePackages:mFile	Lcom/android/internal/os/AtomicFile;
        //     64: aconst_null
        //     65: astore_3
        //     66: aload_0
        //     67: getfield 69	com/android/server/am/CompatModePackages:mFile	Lcom/android/internal/os/AtomicFile;
        //     70: invokevirtual 73	com/android/internal/os/AtomicFile:openRead	()Ljava/io/FileInputStream;
        //     73: astore_3
        //     74: invokestatic 79	android/util/Xml:newPullParser	()Lorg/xmlpull/v1/XmlPullParser;
        //     77: astore 12
        //     79: aload 12
        //     81: aload_3
        //     82: aconst_null
        //     83: invokeinterface 85 3 0
        //     88: aload 12
        //     90: invokeinterface 89 1 0
        //     95: istore 13
        //     97: iload 13
        //     99: iconst_2
        //     100: if_icmpeq +15 -> 115
        //     103: aload 12
        //     105: invokeinterface 92 1 0
        //     110: istore 13
        //     112: goto -15 -> 97
        //     115: ldc 94
        //     117: aload 12
        //     119: invokeinterface 98 1 0
        //     124: invokevirtual 104	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     127: ifeq +130 -> 257
        //     130: aload 12
        //     132: invokeinterface 92 1 0
        //     137: istore 15
        //     139: iload 15
        //     141: iconst_2
        //     142: if_icmpne +96 -> 238
        //     145: aload 12
        //     147: invokeinterface 98 1 0
        //     152: astore 17
        //     154: aload 12
        //     156: invokeinterface 107 1 0
        //     161: iconst_2
        //     162: if_icmpne +76 -> 238
        //     165: ldc 109
        //     167: aload 17
        //     169: invokevirtual 104	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     172: ifeq +66 -> 238
        //     175: aload 12
        //     177: aconst_null
        //     178: ldc 111
        //     180: invokeinterface 115 3 0
        //     185: astore 18
        //     187: aload 18
        //     189: ifnull +49 -> 238
        //     192: aload 12
        //     194: aconst_null
        //     195: ldc 117
        //     197: invokeinterface 115 3 0
        //     202: astore 19
        //     204: iconst_0
        //     205: istore 20
        //     207: aload 19
        //     209: ifnull +14 -> 223
        //     212: aload 19
        //     214: invokestatic 123	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     217: istore 23
        //     219: iload 23
        //     221: istore 20
        //     223: aload_0
        //     224: getfield 48	com/android/server/am/CompatModePackages:mPackages	Ljava/util/HashMap;
        //     227: aload 18
        //     229: iload 20
        //     231: invokestatic 127	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     234: invokevirtual 131	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     237: pop
        //     238: aload 12
        //     240: invokeinterface 92 1 0
        //     245: istore 16
        //     247: iload 16
        //     249: istore 15
        //     251: iload 15
        //     253: iconst_1
        //     254: if_icmpne -115 -> 139
        //     257: aload_3
        //     258: ifnull +7 -> 265
        //     261: aload_3
        //     262: invokevirtual 136	java/io/FileInputStream:close	()V
        //     265: return
        //     266: astore 9
        //     268: ldc 39
        //     270: ldc 138
        //     272: aload 9
        //     274: invokestatic 144	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     277: pop
        //     278: aload_3
        //     279: ifnull -14 -> 265
        //     282: aload_3
        //     283: invokevirtual 136	java/io/FileInputStream:close	()V
        //     286: goto -21 -> 265
        //     289: astore 11
        //     291: goto -26 -> 265
        //     294: astore 6
        //     296: aload_3
        //     297: ifnull +13 -> 310
        //     300: ldc 39
        //     302: ldc 138
        //     304: aload 6
        //     306: invokestatic 144	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     309: pop
        //     310: aload_3
        //     311: ifnull -46 -> 265
        //     314: aload_3
        //     315: invokevirtual 136	java/io/FileInputStream:close	()V
        //     318: goto -53 -> 265
        //     321: astore 7
        //     323: goto -58 -> 265
        //     326: astore 4
        //     328: aload_3
        //     329: ifnull +7 -> 336
        //     332: aload_3
        //     333: invokevirtual 136	java/io/FileInputStream:close	()V
        //     336: aload 4
        //     338: athrow
        //     339: astore 22
        //     341: goto -118 -> 223
        //     344: astore 14
        //     346: goto -81 -> 265
        //     349: astore 5
        //     351: goto -15 -> 336
        //
        // Exception table:
        //     from	to	target	type
        //     66	204	266	org/xmlpull/v1/XmlPullParserException
        //     212	219	266	org/xmlpull/v1/XmlPullParserException
        //     223	247	266	org/xmlpull/v1/XmlPullParserException
        //     282	286	289	java/io/IOException
        //     66	204	294	java/io/IOException
        //     212	219	294	java/io/IOException
        //     223	247	294	java/io/IOException
        //     314	318	321	java/io/IOException
        //     66	204	326	finally
        //     212	219	326	finally
        //     223	247	326	finally
        //     268	278	326	finally
        //     300	310	326	finally
        //     212	219	339	java/lang/NumberFormatException
        //     261	265	344	java/io/IOException
        //     332	336	349	java/io/IOException
    }

    private int getPackageFlags(String paramString)
    {
        Integer localInteger = (Integer)this.mPackages.get(paramString);
        if (localInteger != null);
        for (int i = localInteger.intValue(); ; i = 0)
            return i;
    }

    private void setPackageScreenCompatModeLocked(ApplicationInfo paramApplicationInfo, int paramInt)
    {
        String str = paramApplicationInfo.packageName;
        int i = getPackageFlags(str);
        switch (paramInt)
        {
        default:
            Slog.w("ActivityManager", "Unknown screen compat mode req #" + paramInt + "; ignoring");
        case 0:
        case 1:
        case 2:
        }
        while (true)
        {
            return;
            int j = 0;
            int k;
            if (j != 0)
            {
                k = i | 0x2;
                label85: CompatibilityInfo localCompatibilityInfo1 = compatibilityInfoForPackageLocked(paramApplicationInfo);
                if (localCompatibilityInfo1.alwaysSupportsScreen())
                {
                    Slog.w("ActivityManager", "Ignoring compat mode change of " + str + "; compatibility never needed");
                    k = 0;
                }
                if (localCompatibilityInfo1.neverSupportsScreen())
                {
                    Slog.w("ActivityManager", "Ignoring compat mode change of " + str + "; compatibility always needed");
                    k = 0;
                }
                if (k != i)
                    if (k != 0)
                        this.mPackages.put(str, Integer.valueOf(k));
            }
            else
            {
                CompatibilityInfo localCompatibilityInfo2;
                ActivityRecord localActivityRecord1;
                while (true)
                {
                    localCompatibilityInfo2 = compatibilityInfoForPackageLocked(paramApplicationInfo);
                    this.mHandler.removeMessages(300);
                    Message localMessage = this.mHandler.obtainMessage(300);
                    this.mHandler.sendMessageDelayed(localMessage, 10000L);
                    localActivityRecord1 = this.mService.mMainStack.topRunningActivityLocked(null);
                    for (int m = -1 + this.mService.mMainStack.mHistory.size(); m >= 0; m--)
                    {
                        ActivityRecord localActivityRecord2 = (ActivityRecord)this.mService.mMainStack.mHistory.get(m);
                        if (localActivityRecord2.info.packageName.equals(str))
                        {
                            localActivityRecord2.forceNewConfig = true;
                            if ((localActivityRecord1 != null) && (localActivityRecord2 == localActivityRecord1) && (localActivityRecord2.visible))
                                localActivityRecord2.startFreezingScreenLocked(localActivityRecord1.app, 256);
                        }
                    }
                    j = 1;
                    break;
                    if ((i & 0x2) == 0);
                    for (j = 1; ; j = 0)
                        break;
                    k = i & 0xFFFFFFFD;
                    break label85;
                    this.mPackages.remove(str);
                }
                int n = -1 + this.mService.mLruProcesses.size();
                if (n >= 0)
                {
                    ProcessRecord localProcessRecord = (ProcessRecord)this.mService.mLruProcesses.get(n);
                    if (!localProcessRecord.pkgList.contains(str));
                    while (true)
                    {
                        n--;
                        break;
                        try
                        {
                            if (localProcessRecord.thread != null)
                                localProcessRecord.thread.updatePackageCompatibilityInfo(str, localCompatibilityInfo2);
                        }
                        catch (Exception localException)
                        {
                        }
                    }
                }
                if (localActivityRecord1 != null)
                {
                    this.mService.mMainStack.ensureActivityConfigurationLocked(localActivityRecord1, 0);
                    this.mService.mMainStack.ensureActivitiesVisibleLocked(localActivityRecord1, 0);
                }
            }
        }
    }

    public CompatibilityInfo compatibilityInfoForPackageLocked(ApplicationInfo paramApplicationInfo)
    {
        int i = this.mService.mConfiguration.screenLayout;
        int j = this.mService.mConfiguration.smallestScreenWidthDp;
        if ((0x2 & getPackageFlags(paramApplicationInfo.packageName)) != 0);
        for (boolean bool = true; ; bool = false)
            return new CompatibilityInfo(paramApplicationInfo, i, j, bool);
    }

    public int computeCompatModeLocked(ApplicationInfo paramApplicationInfo)
    {
        int i = 1;
        CompatibilityInfo localCompatibilityInfo;
        if ((0x2 & getPackageFlags(paramApplicationInfo.packageName)) != 0)
        {
            int j = i;
            localCompatibilityInfo = new CompatibilityInfo(paramApplicationInfo, this.mService.mConfiguration.screenLayout, this.mService.mConfiguration.smallestScreenWidthDp, j);
            if (!localCompatibilityInfo.alwaysSupportsScreen())
                break label66;
            i = -2;
        }
        while (true)
        {
            return i;
            int k = 0;
            break;
            label66: if (localCompatibilityInfo.neverSupportsScreen())
                i = -1;
            else if (k == 0)
                i = 0;
        }
    }

    public boolean getFrontActivityAskCompatModeLocked()
    {
        ActivityRecord localActivityRecord = this.mService.mMainStack.topRunningActivityLocked(null);
        if (localActivityRecord == null);
        for (boolean bool = false; ; bool = getPackageAskCompatModeLocked(localActivityRecord.packageName))
            return bool;
    }

    public int getFrontActivityScreenCompatModeLocked()
    {
        ActivityRecord localActivityRecord = this.mService.mMainStack.topRunningActivityLocked(null);
        if (localActivityRecord == null);
        for (int i = -3; ; i = computeCompatModeLocked(localActivityRecord.info.applicationInfo))
            return i;
    }

    public boolean getPackageAskCompatModeLocked(String paramString)
    {
        if ((0x1 & getPackageFlags(paramString)) == 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public int getPackageScreenCompatModeLocked(String paramString)
    {
        Object localObject = null;
        try
        {
            ApplicationInfo localApplicationInfo = AppGlobals.getPackageManager().getApplicationInfo(paramString, 0, 0);
            localObject = localApplicationInfo;
            label18: if (localObject == null);
            for (int i = -3; ; i = computeCompatModeLocked(localObject))
                return i;
        }
        catch (RemoteException localRemoteException)
        {
            break label18;
        }
    }

    public HashMap<String, Integer> getPackages()
    {
        return this.mPackages;
    }

    public void handlePackageAddedLocked(String paramString, boolean paramBoolean)
    {
        int i = 0;
        Object localObject = null;
        try
        {
            ApplicationInfo localApplicationInfo = AppGlobals.getPackageManager().getApplicationInfo(paramString, 0, 0);
            localObject = localApplicationInfo;
            label22: if (localObject == null);
            while (true)
            {
                return;
                CompatibilityInfo localCompatibilityInfo = compatibilityInfoForPackageLocked(localObject);
                if ((!localCompatibilityInfo.alwaysSupportsScreen()) && (!localCompatibilityInfo.neverSupportsScreen()))
                    i = 1;
                if ((paramBoolean) && (i == 0) && (this.mPackages.containsKey(paramString)))
                {
                    this.mPackages.remove(paramString);
                    this.mHandler.removeMessages(300);
                    Message localMessage = this.mHandler.obtainMessage(300);
                    this.mHandler.sendMessageDelayed(localMessage, 10000L);
                }
            }
        }
        catch (RemoteException localRemoteException)
        {
            break label22;
        }
    }

    // ERROR //
    void saveCompatModes()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 55	com/android/server/am/CompatModePackages:mService	Lcom/android/server/am/ActivityManagerService;
        //     4: astore_1
        //     5: aload_1
        //     6: monitorenter
        //     7: new 45	java/util/HashMap
        //     10: dup
        //     11: aload_0
        //     12: getfield 48	com/android/server/am/CompatModePackages:mPackages	Ljava/util/HashMap;
        //     15: invokespecial 358	java/util/HashMap:<init>	(Ljava/util/Map;)V
        //     18: astore_2
        //     19: aload_1
        //     20: monitorexit
        //     21: aconst_null
        //     22: astore 4
        //     24: aload_0
        //     25: getfield 69	com/android/server/am/CompatModePackages:mFile	Lcom/android/internal/os/AtomicFile;
        //     28: invokevirtual 362	com/android/internal/os/AtomicFile:startWrite	()Ljava/io/FileOutputStream;
        //     31: astore 4
        //     33: new 364	com/android/internal/util/FastXmlSerializer
        //     36: dup
        //     37: invokespecial 365	com/android/internal/util/FastXmlSerializer:<init>	()V
        //     40: astore 7
        //     42: aload 7
        //     44: aload 4
        //     46: ldc_w 367
        //     49: invokeinterface 373 3 0
        //     54: aload 7
        //     56: aconst_null
        //     57: iconst_1
        //     58: invokestatic 378	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
        //     61: invokeinterface 382 3 0
        //     66: aload 7
        //     68: ldc_w 384
        //     71: iconst_1
        //     72: invokeinterface 387 3 0
        //     77: aload 7
        //     79: aconst_null
        //     80: ldc 94
        //     82: invokeinterface 391 3 0
        //     87: pop
        //     88: invokestatic 341	android/app/AppGlobals:getPackageManager	()Landroid/content/pm/IPackageManager;
        //     91: astore 9
        //     93: aload_0
        //     94: getfield 55	com/android/server/am/CompatModePackages:mService	Lcom/android/server/am/ActivityManagerService;
        //     97: getfield 304	com/android/server/am/ActivityManagerService:mConfiguration	Landroid/content/res/Configuration;
        //     100: getfield 309	android/content/res/Configuration:screenLayout	I
        //     103: istore 10
        //     105: aload_0
        //     106: getfield 55	com/android/server/am/CompatModePackages:mService	Lcom/android/server/am/ActivityManagerService;
        //     109: getfield 304	com/android/server/am/ActivityManagerService:mConfiguration	Landroid/content/res/Configuration;
        //     112: getfield 312	android/content/res/Configuration:smallestScreenWidthDp	I
        //     115: istore 11
        //     117: aload_2
        //     118: invokevirtual 395	java/util/HashMap:entrySet	()Ljava/util/Set;
        //     121: invokeinterface 401 1 0
        //     126: astore 12
        //     128: aload 12
        //     130: invokeinterface 406 1 0
        //     135: ifeq +191 -> 326
        //     138: aload 12
        //     140: invokeinterface 409 1 0
        //     145: checkcast 411	java/util/Map$Entry
        //     148: astore 14
        //     150: aload 14
        //     152: invokeinterface 414 1 0
        //     157: checkcast 100	java/lang/String
        //     160: astore 15
        //     162: aload 14
        //     164: invokeinterface 417 1 0
        //     169: checkcast 119	java/lang/Integer
        //     172: invokevirtual 152	java/lang/Integer:intValue	()I
        //     175: istore 16
        //     177: iload 16
        //     179: ifeq -51 -> 128
        //     182: aconst_null
        //     183: astore 17
        //     185: aload 9
        //     187: aload 15
        //     189: iconst_0
        //     190: iconst_0
        //     191: invokeinterface 347 4 0
        //     196: astore 24
        //     198: aload 24
        //     200: astore 17
        //     202: aload 17
        //     204: ifnull -76 -> 128
        //     207: new 189	android/content/res/CompatibilityInfo
        //     210: dup
        //     211: aload 17
        //     213: iload 10
        //     215: iload 11
        //     217: iconst_0
        //     218: invokespecial 315	android/content/res/CompatibilityInfo:<init>	(Landroid/content/pm/ApplicationInfo;IIZ)V
        //     221: astore 19
        //     223: aload 19
        //     225: invokevirtual 193	android/content/res/CompatibilityInfo:alwaysSupportsScreen	()Z
        //     228: ifne -100 -> 128
        //     231: aload 19
        //     233: invokevirtual 200	android/content/res/CompatibilityInfo:neverSupportsScreen	()Z
        //     236: ifne -108 -> 128
        //     239: aload 7
        //     241: aconst_null
        //     242: ldc 109
        //     244: invokeinterface 391 3 0
        //     249: pop
        //     250: aload 7
        //     252: aconst_null
        //     253: ldc 111
        //     255: aload 15
        //     257: invokeinterface 421 4 0
        //     262: pop
        //     263: aload 7
        //     265: aconst_null
        //     266: ldc 117
        //     268: iload 16
        //     270: invokestatic 424	java/lang/Integer:toString	(I)Ljava/lang/String;
        //     273: invokeinterface 421 4 0
        //     278: pop
        //     279: aload 7
        //     281: aconst_null
        //     282: ldc 109
        //     284: invokeinterface 427 3 0
        //     289: pop
        //     290: goto -162 -> 128
        //     293: astore 5
        //     295: ldc 39
        //     297: ldc_w 429
        //     300: aload 5
        //     302: invokestatic 144	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     305: pop
        //     306: aload 4
        //     308: ifnull +12 -> 320
        //     311: aload_0
        //     312: getfield 69	com/android/server/am/CompatModePackages:mFile	Lcom/android/internal/os/AtomicFile;
        //     315: aload 4
        //     317: invokevirtual 433	com/android/internal/os/AtomicFile:failWrite	(Ljava/io/FileOutputStream;)V
        //     320: return
        //     321: astore_3
        //     322: aload_1
        //     323: monitorexit
        //     324: aload_3
        //     325: athrow
        //     326: aload 7
        //     328: aconst_null
        //     329: ldc 94
        //     331: invokeinterface 427 3 0
        //     336: pop
        //     337: aload 7
        //     339: invokeinterface 436 1 0
        //     344: aload_0
        //     345: getfield 69	com/android/server/am/CompatModePackages:mFile	Lcom/android/internal/os/AtomicFile;
        //     348: aload 4
        //     350: invokevirtual 439	com/android/internal/os/AtomicFile:finishWrite	(Ljava/io/FileOutputStream;)V
        //     353: goto -33 -> 320
        //     356: astore 18
        //     358: goto -156 -> 202
        //
        // Exception table:
        //     from	to	target	type
        //     24	177	293	java/io/IOException
        //     185	198	293	java/io/IOException
        //     207	290	293	java/io/IOException
        //     326	353	293	java/io/IOException
        //     7	21	321	finally
        //     322	324	321	finally
        //     185	198	356	android/os/RemoteException
    }

    public void setFrontActivityAskCompatModeLocked(boolean paramBoolean)
    {
        ActivityRecord localActivityRecord = this.mService.mMainStack.topRunningActivityLocked(null);
        if (localActivityRecord != null)
            setPackageAskCompatModeLocked(localActivityRecord.packageName, paramBoolean);
    }

    public void setFrontActivityScreenCompatModeLocked(int paramInt)
    {
        ActivityRecord localActivityRecord = this.mService.mMainStack.topRunningActivityLocked(null);
        if (localActivityRecord == null)
            Slog.w("ActivityManager", "setFrontActivityScreenCompatMode failed: no top activity");
        while (true)
        {
            return;
            setPackageScreenCompatModeLocked(localActivityRecord.info.applicationInfo, paramInt);
        }
    }

    public void setPackageAskCompatModeLocked(String paramString, boolean paramBoolean)
    {
        int i = getPackageFlags(paramString);
        int j;
        if (paramBoolean)
        {
            j = i & 0xFFFFFFFE;
            if (i != j)
            {
                if (j == 0)
                    break label85;
                this.mPackages.put(paramString, Integer.valueOf(j));
            }
        }
        while (true)
        {
            this.mHandler.removeMessages(300);
            Message localMessage = this.mHandler.obtainMessage(300);
            this.mHandler.sendMessageDelayed(localMessage, 10000L);
            return;
            j = i | 0x1;
            break;
            label85: this.mPackages.remove(paramString);
        }
    }

    public void setPackageScreenCompatModeLocked(String paramString, int paramInt)
    {
        Object localObject = null;
        try
        {
            ApplicationInfo localApplicationInfo = AppGlobals.getPackageManager().getApplicationInfo(paramString, 0, 0);
            localObject = localApplicationInfo;
            label18: if (localObject == null)
                Slog.w("ActivityManager", "setPackageScreenCompatMode failed: unknown package " + paramString);
            while (true)
            {
                return;
                setPackageScreenCompatModeLocked(localObject, paramInt);
            }
        }
        catch (RemoteException localRemoteException)
        {
            break label18;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.CompatModePackages
 * JD-Core Version:        0.6.2
 */