package com.android.server.am;

import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.util.Log;
import java.util.List;
import miui.util.AudioOutputHelper;

public class ExtraActivityManagerService
{
    private static String TAG = ExtraActivityManagerService.class.getName();

    public static void adjustMediaButtonReceivers(List<Object> paramList, List<ActivityManager.RunningAppProcessInfo> paramList1)
    {
        List localList = AudioOutputHelper.getActiveClientNameList(paramList1, null, true);
        if (localList == null);
        while (true)
        {
            return;
            int i = 0;
            for (int j = 0; j < paramList.size(); j++)
            {
                Object localObject = paramList.get(j);
                String str = getProcessName(localObject);
                if ((str != null) && (localList.contains(str)))
                {
                    paramList.remove(j);
                    paramList.add(i, localObject);
                    i++;
                }
            }
        }
    }

    public static void adjustMediaButtonReceivers(List<Object> paramList, List<ActivityManager.RunningAppProcessInfo> paramList1, String paramString)
    {
        if ("android.intent.action.MEDIA_BUTTON".equals(paramString))
            adjustMediaButtonReceivers(paramList, paramList1);
    }

    private static String getProcessName(Object paramObject)
    {
        String str = null;
        if ((paramObject instanceof BroadcastFilter))
        {
            BroadcastFilter localBroadcastFilter = (BroadcastFilter)paramObject;
            if ((localBroadcastFilter.receiverList != null) && (localBroadcastFilter.receiverList.app != null))
                str = localBroadcastFilter.receiverList.app.processName;
        }
        while (true)
        {
            return str;
            if ((paramObject instanceof ResolveInfo))
            {
                ResolveInfo localResolveInfo = (ResolveInfo)paramObject;
                if (localResolveInfo.activityInfo != null)
                    str = localResolveInfo.activityInfo.processName;
            }
            else
            {
                Log.e(TAG, "unknown receiver type " + paramObject);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.ExtraActivityManagerService
 * JD-Core Version:        0.6.2
 */