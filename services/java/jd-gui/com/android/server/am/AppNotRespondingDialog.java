package com.android.server.am;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Message;
import android.util.Slog;
import android.view.Window;
import java.util.HashSet;

class AppNotRespondingDialog extends BaseErrorDialog
{
    static final int FORCE_CLOSE = 1;
    private static final String TAG = "AppNotRespondingDialog";
    static final int WAIT = 2;
    static final int WAIT_AND_REPORT = 3;
    private final Handler mHandler = new Handler()
    {
        @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
        public void handleMessage(Message paramAnonymousMessage)
        {
            MiuiErrorReport.sendAnrErrorReport(AppNotRespondingDialog.this.getContext(), AppNotRespondingDialog.this.mProc, false);
            Intent localIntent = null;
            switch (paramAnonymousMessage.what)
            {
            default:
            case 1:
            case 2:
            case 3:
            }
            while (true)
            {
                if (localIntent != null);
                try
                {
                    AppNotRespondingDialog.this.getContext().startActivity(localIntent);
                    return;
                    AppNotRespondingDialog.this.mService.killAppAtUsersRequest(AppNotRespondingDialog.this.mProc, AppNotRespondingDialog.this);
                    continue;
                    synchronized (AppNotRespondingDialog.this.mService)
                    {
                        ProcessRecord localProcessRecord = AppNotRespondingDialog.this.mProc;
                        if (paramAnonymousMessage.what == 3)
                            localIntent = AppNotRespondingDialog.this.mService.createAppErrorIntentLocked(localProcessRecord, System.currentTimeMillis(), null);
                        localProcessRecord.notResponding = false;
                        localProcessRecord.notRespondingReport = null;
                        if (localProcessRecord.anrDialog == AppNotRespondingDialog.this)
                            localProcessRecord.anrDialog = null;
                    }
                }
                catch (ActivityNotFoundException localActivityNotFoundException)
                {
                    while (true)
                        Slog.w("AppNotRespondingDialog", "bug report receiver dissappeared", localActivityNotFoundException);
                }
            }
        }
    };
    private final ProcessRecord mProc;
    private final ActivityManagerService mService;

    public AppNotRespondingDialog(ActivityManagerService paramActivityManagerService, Context paramContext, ProcessRecord paramProcessRecord, ActivityRecord paramActivityRecord)
    {
        super(paramContext);
        this.mService = paramActivityManagerService;
        this.mProc = paramProcessRecord;
        Resources localResources = paramContext.getResources();
        setCancelable(false);
        Object localObject1;
        Object localObject2;
        int i;
        label98: Object[] arrayOfObject2;
        if (paramActivityRecord != null)
        {
            localObject1 = paramActivityRecord.info.loadLabel(paramContext.getPackageManager());
            localObject2 = null;
            if (paramProcessRecord.pkgList.size() != 1)
                break label290;
            localObject2 = paramContext.getPackageManager().getApplicationLabel(paramProcessRecord.info);
            if (localObject2 == null)
                break label290;
            if (localObject1 == null)
                break label273;
            i = 17040339;
            if (localObject2 == null)
                break label321;
            arrayOfObject2 = new Object[2];
            arrayOfObject2[0] = localObject1.toString();
            arrayOfObject2[1] = localObject2.toString();
        }
        label273: label290: label321: Object[] arrayOfObject1;
        for (String str = localResources.getString(i, arrayOfObject2); ; str = localResources.getString(i, arrayOfObject1))
        {
            setMessage(str);
            setButton(-1, localResources.getText(17040343), this.mHandler.obtainMessage(1));
            setButton(-2, localResources.getText(17040345), this.mHandler.obtainMessage(2));
            if (paramProcessRecord.errorReportReceiver != null)
                setButton(-3, localResources.getText(17040344), this.mHandler.obtainMessage(3));
            setTitle(localResources.getText(17040338));
            getWindow().addFlags(1073741824);
            getWindow().setTitle("Application Not Responding: " + paramProcessRecord.info.processName);
            return;
            localObject1 = null;
            break;
            localObject1 = localObject2;
            localObject2 = paramProcessRecord.processName;
            i = 17040341;
            break label98;
            if (localObject1 != null)
            {
                localObject2 = paramProcessRecord.processName;
                i = 17040340;
                break label98;
            }
            localObject1 = paramProcessRecord.processName;
            i = 17040342;
            break label98;
            arrayOfObject1 = new Object[1];
            arrayOfObject1[0] = localObject1.toString();
        }
    }

    public void onStop()
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.AppNotRespondingDialog
 * JD-Core Version:        0.6.2
 */