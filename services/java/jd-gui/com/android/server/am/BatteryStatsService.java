package com.android.server.am;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothProfile.ServiceListener;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Binder;
import android.os.Parcel;
import android.os.Process;
import android.os.ServiceManager;
import android.os.WorkSource;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Slog;
import com.android.internal.app.IBatteryStats;
import com.android.internal.app.IBatteryStats.Stub;
import com.android.internal.os.BatteryStatsImpl;
import com.android.internal.os.PowerProfile;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.List;

public final class BatteryStatsService extends IBatteryStats.Stub
{
    static IBatteryStats sService;
    private BluetoothHeadset mBluetoothHeadset;
    private boolean mBluetoothPendingStats;
    private BluetoothProfile.ServiceListener mBluetoothProfileServiceListener = new BluetoothProfile.ServiceListener()
    {
        public void onServiceConnected(int paramAnonymousInt, BluetoothProfile paramAnonymousBluetoothProfile)
        {
            BatteryStatsService.access$002(BatteryStatsService.this, (BluetoothHeadset)paramAnonymousBluetoothProfile);
            synchronized (BatteryStatsService.this.mStats)
            {
                if (BatteryStatsService.this.mBluetoothPendingStats)
                {
                    BatteryStatsService.this.mStats.noteBluetoothOnLocked();
                    BatteryStatsService.this.mStats.setBtHeadset(BatteryStatsService.this.mBluetoothHeadset);
                    BatteryStatsService.access$102(BatteryStatsService.this, false);
                }
                return;
            }
        }

        public void onServiceDisconnected(int paramAnonymousInt)
        {
            BatteryStatsService.access$002(BatteryStatsService.this, null);
        }
    };
    Context mContext;
    final BatteryStatsImpl mStats;

    BatteryStatsService(String paramString)
    {
        this.mStats = new BatteryStatsImpl(paramString);
    }

    private void dumpHelp(PrintWriter paramPrintWriter)
    {
        paramPrintWriter.println("Battery stats (batteryinfo) dump options:");
        paramPrintWriter.println("    [--checkin] [--reset] [--write] [-h]");
        paramPrintWriter.println("    --checkin: format output for a checkin report.");
        paramPrintWriter.println("    --reset: reset the stats, clearing all current data.");
        paramPrintWriter.println("    --write: force write current collected stats to disk.");
        paramPrintWriter.println("    -h: print this help text.");
    }

    public static IBatteryStats getService()
    {
        if (sService != null);
        for (IBatteryStats localIBatteryStats = sService; ; localIBatteryStats = sService)
        {
            return localIBatteryStats;
            sService = asInterface(ServiceManager.getService("batteryinfo"));
        }
    }

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.DUMP") != 0)
            paramPrintWriter.println("Permission Denial: can't dump BatteryStats from from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid() + " without permission " + "android.permission.DUMP");
        while (true)
        {
            return;
            int i = 0;
            int j = 0;
            if (paramArrayOfString != null)
            {
                int k = paramArrayOfString.length;
                int m = 0;
                label76: if (m < k)
                {
                    String str = paramArrayOfString[m];
                    if ("--checkin".equals(str))
                        i = 1;
                    while (true)
                    {
                        m++;
                        break label76;
                        if ("--reset".equals(str))
                            synchronized (this.mStats)
                            {
                                this.mStats.resetAllStatsLocked();
                                paramPrintWriter.println("Battery stats reset.");
                                j = 1;
                            }
                        if ("--write".equals(str))
                            synchronized (this.mStats)
                            {
                                this.mStats.writeSyncLocked();
                                paramPrintWriter.println("Battery stats written.");
                                j = 1;
                            }
                        if ("-h".equals(str))
                        {
                            dumpHelp(paramPrintWriter);
                            break;
                        }
                        if (!"-a".equals(str))
                        {
                            paramPrintWriter.println("Unknown option: " + str);
                            dumpHelp(paramPrintWriter);
                        }
                    }
                }
            }
            if (j != 0)
                continue;
            if (i != 0)
            {
                List localList = this.mContext.getPackageManager().getInstalledApplications(0);
                synchronized (this.mStats)
                {
                    this.mStats.dumpCheckinLocked(paramPrintWriter, paramArrayOfString, localList);
                }
            }
            synchronized (this.mStats)
            {
                this.mStats.dumpLocked(paramPrintWriter);
            }
        }
    }

    public void enforceCallingPermission()
    {
        if (Binder.getCallingPid() == Process.myPid());
        while (true)
        {
            return;
            this.mContext.enforcePermission("android.permission.UPDATE_DEVICE_STATS", Binder.getCallingPid(), Binder.getCallingUid(), null);
        }
    }

    public BatteryStatsImpl getActiveStatistics()
    {
        return this.mStats;
    }

    public long getAwakeTimeBattery()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BATTERY_STATS", null);
        return this.mStats.getAwakeTimeBattery();
    }

    public long getAwakeTimePlugged()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BATTERY_STATS", null);
        return this.mStats.getAwakeTimePlugged();
    }

    public byte[] getStatistics()
    {
        this.mContext.enforceCallingPermission("android.permission.BATTERY_STATS", null);
        Parcel localParcel = Parcel.obtain();
        this.mStats.writeToParcel(localParcel, 0);
        byte[] arrayOfByte = localParcel.marshall();
        localParcel.recycle();
        return arrayOfByte;
    }

    public boolean isOnBattery()
    {
        return this.mStats.isOnBattery();
    }

    public void noteBluetoothOff()
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mBluetoothPendingStats = false;
            this.mStats.noteBluetoothOffLocked();
            return;
        }
    }

    public void noteBluetoothOn()
    {
        enforceCallingPermission();
        BluetoothAdapter localBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (localBluetoothAdapter != null)
            localBluetoothAdapter.getProfileProxy(this.mContext, this.mBluetoothProfileServiceListener, 1);
        synchronized (this.mStats)
        {
            if (this.mBluetoothHeadset != null)
            {
                this.mStats.noteBluetoothOnLocked();
                this.mStats.setBtHeadset(this.mBluetoothHeadset);
                return;
            }
            this.mBluetoothPendingStats = true;
        }
    }

    public void noteFullWifiLockAcquired(int paramInt)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteFullWifiLockAcquiredLocked(paramInt);
            return;
        }
    }

    public void noteFullWifiLockAcquiredFromSource(WorkSource paramWorkSource)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteFullWifiLockAcquiredFromSourceLocked(paramWorkSource);
            return;
        }
    }

    public void noteFullWifiLockReleased(int paramInt)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteFullWifiLockReleasedLocked(paramInt);
            return;
        }
    }

    public void noteFullWifiLockReleasedFromSource(WorkSource paramWorkSource)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteFullWifiLockReleasedFromSourceLocked(paramWorkSource);
            return;
        }
    }

    public void noteInputEvent()
    {
        enforceCallingPermission();
        this.mStats.noteInputEventAtomic();
    }

    public void noteNetworkInterfaceType(String paramString, int paramInt)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteNetworkInterfaceTypeLocked(paramString, paramInt);
            return;
        }
    }

    public void notePhoneDataConnectionState(int paramInt, boolean paramBoolean)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.notePhoneDataConnectionStateLocked(paramInt, paramBoolean);
            return;
        }
    }

    public void notePhoneOff()
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.notePhoneOffLocked();
            return;
        }
    }

    public void notePhoneOn()
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.notePhoneOnLocked();
            return;
        }
    }

    public void notePhoneSignalStrength(SignalStrength paramSignalStrength)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.notePhoneSignalStrengthLocked(paramSignalStrength);
            return;
        }
    }

    public void notePhoneState(int paramInt)
    {
        enforceCallingPermission();
        int i = TelephonyManager.getDefault().getSimState();
        synchronized (this.mStats)
        {
            this.mStats.notePhoneStateLocked(paramInt, i);
            return;
        }
    }

    public void noteScanWifiLockAcquired(int paramInt)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteScanWifiLockAcquiredLocked(paramInt);
            return;
        }
    }

    public void noteScanWifiLockAcquiredFromSource(WorkSource paramWorkSource)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteScanWifiLockAcquiredFromSourceLocked(paramWorkSource);
            return;
        }
    }

    public void noteScanWifiLockReleased(int paramInt)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteScanWifiLockReleasedLocked(paramInt);
            return;
        }
    }

    public void noteScanWifiLockReleasedFromSource(WorkSource paramWorkSource)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteScanWifiLockReleasedFromSourceLocked(paramWorkSource);
            return;
        }
    }

    public void noteScreenBrightness(int paramInt)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteScreenBrightnessLocked(paramInt);
            return;
        }
    }

    public void noteScreenOff()
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteScreenOffLocked();
            return;
        }
    }

    public void noteScreenOn()
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteScreenOnLocked();
            return;
        }
    }

    public void noteStartAudio(int paramInt)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteAudioOnLocked(paramInt);
            return;
        }
    }

    public void noteStartGps(int paramInt)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteStartGpsLocked(paramInt);
            return;
        }
    }

    public void noteStartSensor(int paramInt1, int paramInt2)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteStartSensorLocked(paramInt1, paramInt2);
            return;
        }
    }

    public void noteStartVideo(int paramInt)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteVideoOnLocked(paramInt);
            return;
        }
    }

    public void noteStartWakelock(int paramInt1, int paramInt2, String paramString, int paramInt3)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteStartWakeLocked(paramInt1, paramInt2, paramString, paramInt3);
            return;
        }
    }

    public void noteStartWakelockFromSource(WorkSource paramWorkSource, int paramInt1, String paramString, int paramInt2)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteStartWakeFromSourceLocked(paramWorkSource, paramInt1, paramString, paramInt2);
            return;
        }
    }

    public void noteStopAudio(int paramInt)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteAudioOffLocked(paramInt);
            return;
        }
    }

    public void noteStopGps(int paramInt)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteStopGpsLocked(paramInt);
            return;
        }
    }

    public void noteStopSensor(int paramInt1, int paramInt2)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteStopSensorLocked(paramInt1, paramInt2);
            return;
        }
    }

    public void noteStopVideo(int paramInt)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteVideoOffLocked(paramInt);
            return;
        }
    }

    public void noteStopWakelock(int paramInt1, int paramInt2, String paramString, int paramInt3)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteStopWakeLocked(paramInt1, paramInt2, paramString, paramInt3);
            return;
        }
    }

    public void noteStopWakelockFromSource(WorkSource paramWorkSource, int paramInt1, String paramString, int paramInt2)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteStopWakeFromSourceLocked(paramWorkSource, paramInt1, paramString, paramInt2);
            return;
        }
    }

    public void noteUserActivity(int paramInt1, int paramInt2)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteUserActivityLocked(paramInt1, paramInt2);
            return;
        }
    }

    public void noteWifiMulticastDisabled(int paramInt)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteWifiMulticastDisabledLocked(paramInt);
            return;
        }
    }

    public void noteWifiMulticastDisabledFromSource(WorkSource paramWorkSource)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteWifiMulticastDisabledFromSourceLocked(paramWorkSource);
            return;
        }
    }

    public void noteWifiMulticastEnabled(int paramInt)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteWifiMulticastEnabledLocked(paramInt);
            return;
        }
    }

    public void noteWifiMulticastEnabledFromSource(WorkSource paramWorkSource)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteWifiMulticastEnabledFromSourceLocked(paramWorkSource);
            return;
        }
    }

    public void noteWifiOff()
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteWifiOffLocked();
            return;
        }
    }

    public void noteWifiOn()
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteWifiOnLocked();
            return;
        }
    }

    public void noteWifiRunning(WorkSource paramWorkSource)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteWifiRunningLocked(paramWorkSource);
            return;
        }
    }

    public void noteWifiRunningChanged(WorkSource paramWorkSource1, WorkSource paramWorkSource2)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteWifiRunningChangedLocked(paramWorkSource1, paramWorkSource2);
            return;
        }
    }

    public void noteWifiStopped(WorkSource paramWorkSource)
    {
        enforceCallingPermission();
        synchronized (this.mStats)
        {
            this.mStats.noteWifiStoppedLocked(paramWorkSource);
            return;
        }
    }

    public void publish(Context paramContext)
    {
        this.mContext = paramContext;
        ServiceManager.addService("batteryinfo", asBinder());
        this.mStats.setNumSpeedSteps(new PowerProfile(this.mContext).getNumSpeedSteps());
        this.mStats.setRadioScanningTimeout(1000L * this.mContext.getResources().getInteger(17694728));
    }

    public void setBatteryState(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
    {
        enforceCallingPermission();
        this.mStats.setBatteryState(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6);
    }

    public void shutdown()
    {
        Slog.w("BatteryStats", "Writing battery stats before shutdown...");
        synchronized (this.mStats)
        {
            this.mStats.shutdownLocked();
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.BatteryStatsService
 * JD-Core Version:        0.6.2
 */