package com.android.server.am;

import android.content.ComponentName;
import android.content.pm.ApplicationInfo;
import android.os.Binder;
import android.os.UserId;
import android.util.SparseArray;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public class ProviderMap
{
    private static final boolean DBG = false;
    private static final String TAG = "ProviderMap";
    private final HashMap<ComponentName, ContentProviderRecord> mGlobalByClass = new HashMap();
    private final HashMap<String, ContentProviderRecord> mGlobalByName = new HashMap();
    private final SparseArray<HashMap<ComponentName, ContentProviderRecord>> mProvidersByClassPerUser = new SparseArray();
    private final SparseArray<HashMap<String, ContentProviderRecord>> mProvidersByNamePerUser = new SparseArray();

    // ERROR //
    private void dumpProvider(String paramString, java.io.FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, ContentProviderRecord paramContentProviderRecord, String[] paramArrayOfString, boolean paramBoolean)
    {
        // Byte code:
        //     0: new 47	java/lang/StringBuilder
        //     3: dup
        //     4: invokespecial 48	java/lang/StringBuilder:<init>	()V
        //     7: aload_1
        //     8: invokevirtual 52	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     11: ldc 54
        //     13: invokevirtual 52	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     16: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     19: astore 7
        //     21: aload_0
        //     22: monitorenter
        //     23: aload_3
        //     24: aload_1
        //     25: invokevirtual 64	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     28: aload_3
        //     29: ldc 66
        //     31: invokevirtual 64	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     34: aload_3
        //     35: aload 4
        //     37: invokevirtual 69	java/io/PrintWriter:print	(Ljava/lang/Object;)V
        //     40: aload_3
        //     41: ldc 71
        //     43: invokevirtual 64	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     46: aload 4
        //     48: getfield 77	com/android/server/am/ContentProviderRecord:proc	Lcom/android/server/am/ProcessRecord;
        //     51: ifnull +124 -> 175
        //     54: aload_3
        //     55: aload 4
        //     57: getfield 77	com/android/server/am/ContentProviderRecord:proc	Lcom/android/server/am/ProcessRecord;
        //     60: getfield 83	com/android/server/am/ProcessRecord:pid	I
        //     63: invokevirtual 87	java/io/PrintWriter:println	(I)V
        //     66: iload 6
        //     68: ifeq +12 -> 80
        //     71: aload 4
        //     73: aload_3
        //     74: aload 7
        //     76: iconst_1
        //     77: invokevirtual 91	com/android/server/am/ContentProviderRecord:dump	(Ljava/io/PrintWriter;Ljava/lang/String;Z)V
        //     80: aload_0
        //     81: monitorexit
        //     82: aload 4
        //     84: getfield 77	com/android/server/am/ContentProviderRecord:proc	Lcom/android/server/am/ProcessRecord;
        //     87: ifnull +87 -> 174
        //     90: aload 4
        //     92: getfield 77	com/android/server/am/ContentProviderRecord:proc	Lcom/android/server/am/ProcessRecord;
        //     95: getfield 95	com/android/server/am/ProcessRecord:thread	Landroid/app/IApplicationThread;
        //     98: ifnull +76 -> 174
        //     101: aload_3
        //     102: ldc 97
        //     104: invokevirtual 99	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     107: aload_3
        //     108: invokevirtual 102	java/io/PrintWriter:flush	()V
        //     111: new 104	com/android/server/am/TransferPipe
        //     114: dup
        //     115: invokespecial 105	com/android/server/am/TransferPipe:<init>	()V
        //     118: astore 9
        //     120: aload 4
        //     122: getfield 77	com/android/server/am/ContentProviderRecord:proc	Lcom/android/server/am/ProcessRecord;
        //     125: getfield 95	com/android/server/am/ProcessRecord:thread	Landroid/app/IApplicationThread;
        //     128: aload 9
        //     130: invokevirtual 109	com/android/server/am/TransferPipe:getWriteFd	()Landroid/os/ParcelFileDescriptor;
        //     133: invokevirtual 115	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
        //     136: aload 4
        //     138: getfield 119	com/android/server/am/ContentProviderRecord:provider	Landroid/content/IContentProvider;
        //     141: invokeinterface 125 1 0
        //     146: aload 5
        //     148: invokeinterface 130 4 0
        //     153: aload 9
        //     155: ldc 132
        //     157: invokevirtual 135	com/android/server/am/TransferPipe:setBufferPrefix	(Ljava/lang/String;)V
        //     160: aload 9
        //     162: aload_2
        //     163: ldc2_w 136
        //     166: invokevirtual 141	com/android/server/am/TransferPipe:go	(Ljava/io/FileDescriptor;J)V
        //     169: aload 9
        //     171: invokevirtual 144	com/android/server/am/TransferPipe:kill	()V
        //     174: return
        //     175: aload_3
        //     176: ldc 146
        //     178: invokevirtual 99	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     181: goto -115 -> 66
        //     184: astore 8
        //     186: aload_0
        //     187: monitorexit
        //     188: aload 8
        //     190: athrow
        //     191: astore 10
        //     193: aload 9
        //     195: invokevirtual 144	com/android/server/am/TransferPipe:kill	()V
        //     198: aload 10
        //     200: athrow
        //     201: astore 12
        //     203: aload_3
        //     204: new 47	java/lang/StringBuilder
        //     207: dup
        //     208: invokespecial 48	java/lang/StringBuilder:<init>	()V
        //     211: ldc 148
        //     213: invokevirtual 52	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     216: aload 12
        //     218: invokevirtual 151	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     221: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     224: invokevirtual 99	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     227: goto -53 -> 174
        //     230: astore 11
        //     232: aload_3
        //     233: ldc 153
        //     235: invokevirtual 99	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     238: goto -64 -> 174
        //
        // Exception table:
        //     from	to	target	type
        //     23	82	184	finally
        //     175	188	184	finally
        //     120	169	191	finally
        //     111	120	201	java/io/IOException
        //     169	174	201	java/io/IOException
        //     193	201	201	java/io/IOException
        //     111	120	230	android/os/RemoteException
        //     169	174	230	android/os/RemoteException
        //     193	201	230	android/os/RemoteException
    }

    private void dumpProvidersByClassLocked(PrintWriter paramPrintWriter, boolean paramBoolean, HashMap<ComponentName, ContentProviderRecord> paramHashMap)
    {
        Iterator localIterator = paramHashMap.entrySet().iterator();
        while (localIterator.hasNext())
        {
            ContentProviderRecord localContentProviderRecord = (ContentProviderRecord)((Map.Entry)localIterator.next()).getValue();
            paramPrintWriter.print("    * ");
            paramPrintWriter.println(localContentProviderRecord);
            localContentProviderRecord.dump(paramPrintWriter, "        ", paramBoolean);
        }
    }

    private void dumpProvidersByNameLocked(PrintWriter paramPrintWriter, HashMap<String, ContentProviderRecord> paramHashMap)
    {
        Iterator localIterator = paramHashMap.entrySet().iterator();
        while (localIterator.hasNext())
        {
            Map.Entry localEntry = (Map.Entry)localIterator.next();
            ContentProviderRecord localContentProviderRecord = (ContentProviderRecord)localEntry.getValue();
            paramPrintWriter.print("    ");
            paramPrintWriter.print((String)localEntry.getKey());
            paramPrintWriter.print(": ");
            paramPrintWriter.println(localContentProviderRecord.toShortString());
        }
    }

    private HashMap<String, ContentProviderRecord> getProvidersByName(int paramInt)
    {
        int i;
        HashMap localHashMap1;
        HashMap localHashMap2;
        if (paramInt >= 0)
        {
            i = paramInt;
            localHashMap1 = (HashMap)this.mProvidersByNamePerUser.get(i);
            if (localHashMap1 != null)
                break label51;
            localHashMap2 = new HashMap();
            this.mProvidersByNamePerUser.put(i, localHashMap2);
        }
        while (true)
        {
            return localHashMap2;
            i = Binder.getOrigCallingUser();
            break;
            label51: localHashMap2 = localHashMap1;
        }
    }

    // ERROR //
    protected boolean dumpProvider(java.io.FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String paramString, String[] paramArrayOfString, int paramInt, boolean paramBoolean)
    {
        // Byte code:
        //     0: new 219	java/util/ArrayList
        //     3: dup
        //     4: invokespecial 220	java/util/ArrayList:<init>	()V
        //     7: astore 7
        //     9: ldc 222
        //     11: aload_3
        //     12: invokevirtual 226	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     15: ifeq +73 -> 88
        //     18: aload_0
        //     19: monitorenter
        //     20: aload_0
        //     21: bipush 255
        //     23: invokevirtual 229	com/android/server/am/ProviderMap:getProvidersByClass	(I)Ljava/util/HashMap;
        //     26: invokevirtual 233	java/util/HashMap:values	()Ljava/util/Collection;
        //     29: invokeinterface 236 1 0
        //     34: astore 22
        //     36: aload 22
        //     38: invokeinterface 171 1 0
        //     43: ifeq +29 -> 72
        //     46: aload 7
        //     48: aload 22
        //     50: invokeinterface 175 1 0
        //     55: checkcast 73	com/android/server/am/ContentProviderRecord
        //     58: invokevirtual 239	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     61: pop
        //     62: goto -26 -> 36
        //     65: astore 21
        //     67: aload_0
        //     68: monitorexit
        //     69: aload 21
        //     71: athrow
        //     72: aload_0
        //     73: monitorexit
        //     74: aload 7
        //     76: invokevirtual 242	java/util/ArrayList:size	()I
        //     79: ifgt +182 -> 261
        //     82: iconst_0
        //     83: istore 14
        //     85: iload 14
        //     87: ireturn
        //     88: aload_3
        //     89: ifnull +110 -> 199
        //     92: aload_3
        //     93: invokestatic 248	android/content/ComponentName:unflattenFromString	(Ljava/lang/String;)Landroid/content/ComponentName;
        //     96: astore 8
        //     98: iconst_0
        //     99: istore 9
        //     101: aload 8
        //     103: ifnonnull +20 -> 123
        //     106: aload_3
        //     107: bipush 16
        //     109: invokestatic 254	java/lang/Integer:parseInt	(Ljava/lang/String;I)I
        //     112: istore 20
        //     114: iload 20
        //     116: istore 9
        //     118: aconst_null
        //     119: astore_3
        //     120: aconst_null
        //     121: astore 8
        //     123: aload_0
        //     124: monitorenter
        //     125: aload_0
        //     126: bipush 255
        //     128: invokevirtual 229	com/android/server/am/ProviderMap:getProvidersByClass	(I)Ljava/util/HashMap;
        //     131: invokevirtual 233	java/util/HashMap:values	()Ljava/util/Collection;
        //     134: invokeinterface 236 1 0
        //     139: astore 11
        //     141: aload 11
        //     143: invokeinterface 171 1 0
        //     148: ifeq +108 -> 256
        //     151: aload 11
        //     153: invokeinterface 175 1 0
        //     158: checkcast 73	com/android/server/am/ContentProviderRecord
        //     161: astore 15
        //     163: aload 8
        //     165: ifnull +40 -> 205
        //     168: aload 15
        //     170: getfield 258	com/android/server/am/ContentProviderRecord:name	Landroid/content/ComponentName;
        //     173: aload 8
        //     175: invokevirtual 259	android/content/ComponentName:equals	(Ljava/lang/Object;)Z
        //     178: ifeq -37 -> 141
        //     181: aload 7
        //     183: aload 15
        //     185: invokevirtual 239	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     188: pop
        //     189: goto -48 -> 141
        //     192: astore 10
        //     194: aload_0
        //     195: monitorexit
        //     196: aload 10
        //     198: athrow
        //     199: aconst_null
        //     200: astore 8
        //     202: goto -104 -> 98
        //     205: aload_3
        //     206: ifnull +29 -> 235
        //     209: aload 15
        //     211: getfield 258	com/android/server/am/ContentProviderRecord:name	Landroid/content/ComponentName;
        //     214: invokevirtual 262	android/content/ComponentName:flattenToString	()Ljava/lang/String;
        //     217: aload_3
        //     218: invokevirtual 266	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
        //     221: ifeq -80 -> 141
        //     224: aload 7
        //     226: aload 15
        //     228: invokevirtual 239	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     231: pop
        //     232: goto -91 -> 141
        //     235: aload 15
        //     237: invokestatic 272	java/lang/System:identityHashCode	(Ljava/lang/Object;)I
        //     240: iload 9
        //     242: if_icmpne -101 -> 141
        //     245: aload 7
        //     247: aload 15
        //     249: invokevirtual 239	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     252: pop
        //     253: goto -112 -> 141
        //     256: aload_0
        //     257: monitorexit
        //     258: goto -184 -> 74
        //     261: iconst_0
        //     262: istore 12
        //     264: iconst_0
        //     265: istore 13
        //     267: iload 13
        //     269: aload 7
        //     271: invokevirtual 242	java/util/ArrayList:size	()I
        //     274: if_icmpge +44 -> 318
        //     277: iload 12
        //     279: ifeq +7 -> 286
        //     282: aload_2
        //     283: invokevirtual 274	java/io/PrintWriter:println	()V
        //     286: iconst_1
        //     287: istore 12
        //     289: aload_0
        //     290: ldc_w 276
        //     293: aload_1
        //     294: aload_2
        //     295: aload 7
        //     297: iload 13
        //     299: invokevirtual 277	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     302: checkcast 73	com/android/server/am/ContentProviderRecord
        //     305: aload 4
        //     307: iload 6
        //     309: invokespecial 279	com/android/server/am/ProviderMap:dumpProvider	(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;Lcom/android/server/am/ContentProviderRecord;[Ljava/lang/String;Z)V
        //     312: iinc 13 1
        //     315: goto -48 -> 267
        //     318: iconst_1
        //     319: istore 14
        //     321: goto -236 -> 85
        //     324: astore 19
        //     326: goto -203 -> 123
        //
        // Exception table:
        //     from	to	target	type
        //     20	69	65	finally
        //     72	74	65	finally
        //     125	196	192	finally
        //     209	258	192	finally
        //     106	114	324	java/lang/RuntimeException
    }

    void dumpProvidersLocked(PrintWriter paramPrintWriter, boolean paramBoolean)
    {
        if (this.mGlobalByClass.size() > 0)
        {
            if (0 != 0)
                paramPrintWriter.println(" ");
            paramPrintWriter.println("    Published content providers (by class):");
            dumpProvidersByClassLocked(paramPrintWriter, paramBoolean, this.mGlobalByClass);
        }
        if (this.mProvidersByClassPerUser.size() > 1)
        {
            paramPrintWriter.println("");
            for (int j = 0; j < this.mProvidersByClassPerUser.size(); j++)
            {
                HashMap localHashMap = (HashMap)this.mProvidersByClassPerUser.valueAt(j);
                paramPrintWriter.println("    User " + this.mProvidersByClassPerUser.keyAt(j) + ":");
                dumpProvidersByClassLocked(paramPrintWriter, paramBoolean, localHashMap);
                paramPrintWriter.println(" ");
            }
        }
        if (this.mProvidersByClassPerUser.size() == 1)
            dumpProvidersByClassLocked(paramPrintWriter, paramBoolean, (HashMap)this.mProvidersByClassPerUser.valueAt(0));
        if (paramBoolean)
        {
            paramPrintWriter.println(" ");
            paramPrintWriter.println("    Authority to provider mappings:");
            dumpProvidersByNameLocked(paramPrintWriter, this.mGlobalByName);
            for (int i = 0; i < this.mProvidersByNamePerUser.size(); i++)
            {
                if (i > 0)
                    paramPrintWriter.println("    User " + this.mProvidersByNamePerUser.keyAt(i) + ":");
                dumpProvidersByNameLocked(paramPrintWriter, (HashMap)this.mProvidersByNamePerUser.valueAt(i));
            }
        }
    }

    ContentProviderRecord getProviderByClass(ComponentName paramComponentName)
    {
        return getProviderByClass(paramComponentName, -1);
    }

    ContentProviderRecord getProviderByClass(ComponentName paramComponentName, int paramInt)
    {
        ContentProviderRecord localContentProviderRecord = (ContentProviderRecord)this.mGlobalByClass.get(paramComponentName);
        if (localContentProviderRecord != null);
        while (true)
        {
            return localContentProviderRecord;
            localContentProviderRecord = (ContentProviderRecord)getProvidersByClass(paramInt).get(paramComponentName);
        }
    }

    ContentProviderRecord getProviderByName(String paramString)
    {
        return getProviderByName(paramString, -1);
    }

    ContentProviderRecord getProviderByName(String paramString, int paramInt)
    {
        ContentProviderRecord localContentProviderRecord = (ContentProviderRecord)this.mGlobalByName.get(paramString);
        if (localContentProviderRecord != null);
        while (true)
        {
            return localContentProviderRecord;
            localContentProviderRecord = (ContentProviderRecord)getProvidersByName(paramInt).get(paramString);
        }
    }

    HashMap<ComponentName, ContentProviderRecord> getProvidersByClass(int paramInt)
    {
        int i;
        HashMap localHashMap1;
        HashMap localHashMap2;
        if (paramInt >= 0)
        {
            i = paramInt;
            localHashMap1 = (HashMap)this.mProvidersByClassPerUser.get(i);
            if (localHashMap1 != null)
                break label51;
            localHashMap2 = new HashMap();
            this.mProvidersByClassPerUser.put(i, localHashMap2);
        }
        while (true)
        {
            return localHashMap2;
            i = Binder.getOrigCallingUser();
            break;
            label51: localHashMap2 = localHashMap1;
        }
    }

    void putProviderByClass(ComponentName paramComponentName, ContentProviderRecord paramContentProviderRecord)
    {
        if (paramContentProviderRecord.appInfo.uid < 10000)
            this.mGlobalByClass.put(paramComponentName, paramContentProviderRecord);
        while (true)
        {
            return;
            getProvidersByClass(UserId.getUserId(paramContentProviderRecord.appInfo.uid)).put(paramComponentName, paramContentProviderRecord);
        }
    }

    void putProviderByName(String paramString, ContentProviderRecord paramContentProviderRecord)
    {
        if (paramContentProviderRecord.appInfo.uid < 10000)
            this.mGlobalByName.put(paramString, paramContentProviderRecord);
        while (true)
        {
            return;
            getProvidersByName(UserId.getUserId(paramContentProviderRecord.appInfo.uid)).put(paramString, paramContentProviderRecord);
        }
    }

    void removeProviderByClass(ComponentName paramComponentName, int paramInt)
    {
        if (this.mGlobalByClass.containsKey(paramComponentName))
            this.mGlobalByClass.remove(paramComponentName);
        while (true)
        {
            return;
            getProvidersByClass(paramInt).remove(paramComponentName);
        }
    }

    void removeProviderByName(String paramString, int paramInt)
    {
        if (this.mGlobalByName.containsKey(paramString))
            this.mGlobalByName.remove(paramString);
        while (true)
        {
            return;
            getProvidersByName(paramInt).remove(paramString);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.ProviderMap
 * JD-Core Version:        0.6.2
 */