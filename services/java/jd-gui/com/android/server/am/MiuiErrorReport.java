package com.android.server.am;

import android.app.ActivityManager.ProcessErrorStateInfo;
import android.app.ApplicationErrorReport.CrashInfo;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import android.os.SystemProperties;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import miui.os.Build;
import miui.util.ErrorReportUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class MiuiErrorReport
{
    // ERROR //
    private static String getAnrStackTrack()
    {
        // Byte code:
        //     0: new 14	java/lang/StringBuilder
        //     3: dup
        //     4: invokespecial 15	java/lang/StringBuilder:<init>	()V
        //     7: astore_0
        //     8: ldc 17
        //     10: aconst_null
        //     11: invokestatic 23	android/os/SystemProperties:get	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //     14: astore_1
        //     15: aload_1
        //     16: ifnull +10 -> 26
        //     19: aload_1
        //     20: invokevirtual 29	java/lang/String:length	()I
        //     23: ifne +8 -> 31
        //     26: ldc 31
        //     28: astore_2
        //     29: aload_2
        //     30: areturn
        //     31: new 33	java/io/File
        //     34: dup
        //     35: aload_1
        //     36: invokespecial 36	java/io/File:<init>	(Ljava/lang/String;)V
        //     39: astore_3
        //     40: aconst_null
        //     41: astore 4
        //     43: new 38	java/io/BufferedReader
        //     46: dup
        //     47: new 40	java/io/FileReader
        //     50: dup
        //     51: aload_3
        //     52: invokespecial 43	java/io/FileReader:<init>	(Ljava/io/File;)V
        //     55: invokespecial 46	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
        //     58: astore 5
        //     60: iconst_0
        //     61: istore 6
        //     63: iconst_0
        //     64: istore 7
        //     66: aload 5
        //     68: invokevirtual 49	java/io/BufferedReader:readLine	()Ljava/lang/String;
        //     71: astore 12
        //     73: aload 12
        //     75: ifnull +46 -> 121
        //     78: aload 12
        //     80: ldc 51
        //     82: invokevirtual 55	java/lang/String:startsWith	(Ljava/lang/String;)Z
        //     85: ifeq +54 -> 139
        //     88: iconst_1
        //     89: istore 6
        //     91: iload 6
        //     93: ifeq +20 -> 113
        //     96: aload_0
        //     97: aload 12
        //     99: invokevirtual 59	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     102: pop
        //     103: aload_0
        //     104: ldc 61
        //     106: invokevirtual 59	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     109: pop
        //     110: iinc 7 1
        //     113: iload 7
        //     115: sipush 300
        //     118: if_icmple -52 -> 66
        //     121: aload 5
        //     123: ifnull +8 -> 131
        //     126: aload 5
        //     128: invokevirtual 64	java/io/BufferedReader:close	()V
        //     131: aload_0
        //     132: invokevirtual 67	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     135: astore_2
        //     136: goto -107 -> 29
        //     139: iload 6
        //     141: ifeq -50 -> 91
        //     144: aload 12
        //     146: ldc 69
        //     148: invokevirtual 55	java/lang/String:startsWith	(Ljava/lang/String;)Z
        //     151: istore 14
        //     153: iload 14
        //     155: ifeq -64 -> 91
        //     158: goto -37 -> 121
        //     161: astore 10
        //     163: aload 4
        //     165: ifnull +8 -> 173
        //     168: aload 4
        //     170: invokevirtual 64	java/io/BufferedReader:close	()V
        //     173: aload 10
        //     175: athrow
        //     176: astore 17
        //     178: aload 4
        //     180: ifnull -49 -> 131
        //     183: aload 4
        //     185: invokevirtual 64	java/io/BufferedReader:close	()V
        //     188: goto -57 -> 131
        //     191: astore 9
        //     193: goto -62 -> 131
        //     196: astore 11
        //     198: goto -25 -> 173
        //     201: astore 13
        //     203: goto -72 -> 131
        //     206: astore 10
        //     208: aload 5
        //     210: astore 4
        //     212: goto -49 -> 163
        //     215: astore 8
        //     217: aload 5
        //     219: astore 4
        //     221: goto -43 -> 178
        //
        // Exception table:
        //     from	to	target	type
        //     43	60	161	finally
        //     43	60	176	java/io/IOException
        //     183	188	191	java/io/IOException
        //     168	173	196	java/io/IOException
        //     126	131	201	java/io/IOException
        //     66	110	206	finally
        //     144	153	206	finally
        //     66	110	215	java/io/IOException
        //     144	153	215	java/io/IOException
    }

    private static String getDeviceString()
    {
        String str = SystemProperties.get("ro.product.mod_device", null);
        if (TextUtils.isEmpty(str))
            str = Build.DEVICE;
        return str;
    }

    private static String getIMEI()
    {
        String str = TelephonyManager.getDefault().getDeviceId();
        if (TextUtils.isEmpty(str))
            str = "";
        return str;
    }

    private static String getNetworkName(Context paramContext)
    {
        return ((TelephonyManager)paramContext.getSystemService("phone")).getNetworkOperatorName();
    }

    private static String getPackageVersion(Context paramContext, String paramString)
    {
        PackageManager localPackageManager = paramContext.getPackageManager();
        try
        {
            localPackageInfo = localPackageManager.getPackageInfo(paramString, 0);
            if (((0x1 & localPackageInfo.applicationInfo.flags) != 0) || ((0x80 & localPackageInfo.applicationInfo.flags) != 0))
            {
                str = Build.VERSION.INCREMENTAL;
                return str;
            }
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            while (true)
            {
                PackageInfo localPackageInfo;
                localNameNotFoundException.printStackTrace();
                String str = "";
                continue;
                str = localPackageInfo.versionName + " (" + localPackageInfo.versionCode + ")";
            }
        }
    }

    protected static void populateAnrData(JSONObject paramJSONObject, ProcessRecord paramProcessRecord)
    {
        try
        {
            paramJSONObject.put("error_type", "anr");
            paramJSONObject.put("anr_cause", paramProcessRecord.notRespondingReport.shortMsg);
            if (paramProcessRecord.notRespondingReport.tag == null);
            for (String str = ""; ; str = paramProcessRecord.notRespondingReport.tag)
            {
                paramJSONObject.put("anr_activity", str);
                paramJSONObject.put("stack_track", getAnrStackTrack());
                break;
            }
        }
        catch (JSONException localJSONException)
        {
            localJSONException.printStackTrace();
        }
    }

    protected static void populateCommonData(JSONObject paramJSONObject, Context paramContext, ProcessRecord paramProcessRecord)
    {
        try
        {
            paramJSONObject.put("network", getNetworkName(paramContext));
            paramJSONObject.put("device", getDeviceString());
            paramJSONObject.put("imei", getIMEI());
            paramJSONObject.put("platform", Build.VERSION.RELEASE);
            paramJSONObject.put("build_version", Build.VERSION.INCREMENTAL);
            paramJSONObject.put("package_name", paramProcessRecord.info.packageName);
            paramJSONObject.put("app_version", getPackageVersion(paramContext, paramProcessRecord.info.packageName));
            return;
        }
        catch (JSONException localJSONException)
        {
            while (true)
                localJSONException.printStackTrace();
        }
    }

    protected static void populateFcData(JSONObject paramJSONObject, ApplicationErrorReport.CrashInfo paramCrashInfo)
    {
        if (paramCrashInfo == null);
        while (true)
        {
            return;
            try
            {
                paramJSONObject.put("error_type", "fc");
                paramJSONObject.put("exception_class", paramCrashInfo.exceptionClassName);
                paramJSONObject.put("exception_source_method", paramCrashInfo.throwClassName + "." + paramCrashInfo.throwMethodName);
                paramJSONObject.put("stack_track", paramCrashInfo.stackTrace);
            }
            catch (JSONException localJSONException)
            {
                localJSONException.printStackTrace();
            }
        }
    }

    public static void sendAnrErrorReport(Context paramContext, ProcessRecord paramProcessRecord, boolean paramBoolean)
    {
        try
        {
            if ((Build.isOfficialVersion()) && ((paramBoolean) || ((ErrorReportUtils.isUserAllowed(paramContext)) && (ErrorReportUtils.isWifiConnected(paramContext)))))
            {
                JSONObject localJSONObject = new JSONObject();
                populateCommonData(localJSONObject, paramContext, paramProcessRecord);
                populateAnrData(localJSONObject, paramProcessRecord);
                ErrorReportUtils.postErrorReport(paramContext, localJSONObject);
            }
        }
        catch (Exception localException)
        {
        }
    }

    public static void sendFcErrorReport(Context paramContext, ProcessRecord paramProcessRecord, ApplicationErrorReport.CrashInfo paramCrashInfo, boolean paramBoolean)
    {
        try
        {
            if (Build.isOfficialVersion())
            {
                JSONObject localJSONObject = new JSONObject();
                populateCommonData(localJSONObject, paramContext, paramProcessRecord);
                populateFcData(localJSONObject, paramCrashInfo);
                if (paramBoolean)
                {
                    Intent localIntent = new Intent();
                    localIntent.setPackage("com.miui.bugreport");
                    localIntent.setClassName("com.miui.bugreport", "com.miui.bugreport.ui.MiuiFcPreviewActivity");
                    localIntent.putExtra("extra_fc_report", localJSONObject.toString());
                    localIntent.setFlags(268435456);
                    try
                    {
                        paramContext.startActivity(localIntent);
                    }
                    catch (ActivityNotFoundException localActivityNotFoundException)
                    {
                    }
                }
                else if ((ErrorReportUtils.isUserAllowed(paramContext)) && (ErrorReportUtils.isWifiConnected(paramContext)))
                {
                    ErrorReportUtils.postErrorReport(paramContext, localJSONObject);
                }
            }
        }
        catch (Exception localException)
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.MiuiErrorReport
 * JD-Core Version:        0.6.2
 */