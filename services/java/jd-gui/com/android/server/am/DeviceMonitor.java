package com.android.server.am;

import android.util.Slog;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;

class DeviceMonitor
{
    private static final File BASE;
    private static final int INTERVAL = 1000;
    private static final String LOG_TAG = DeviceMonitor.class.getName();
    private static final int MAX_FILES = 30;
    private static final File[] PATHS = arrayOfFile;
    private static final File PROC = new File("/proc");
    private static final int SAMPLE_COUNT = 10;
    private static DeviceMonitor instance = new DeviceMonitor();
    private final byte[] buffer = new byte[1024];
    private boolean running = false;

    static
    {
        BASE = new File("/data/anr/");
        if ((!BASE.isDirectory()) && (!BASE.mkdirs()))
            throw new AssertionError("Couldn't create " + BASE + ".");
        File[] arrayOfFile = new File[4];
        arrayOfFile[0] = new File(PROC, "zoneinfo");
        arrayOfFile[1] = new File(PROC, "interrupts");
        arrayOfFile[2] = new File(PROC, "meminfo");
        arrayOfFile[3] = new File(PROC, "slabinfo");
    }

    private DeviceMonitor()
    {
        new Thread()
        {
            public void run()
            {
                DeviceMonitor.this.monitor();
            }
        }
        .start();
    }

    private static void closeQuietly(Closeable paramCloseable)
    {
        if (paramCloseable != null);
        try
        {
            paramCloseable.close();
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
                Slog.w(LOG_TAG, localIOException);
        }
    }

    private void dump()
        throws IOException
    {
        FileOutputStream localFileOutputStream = new FileOutputStream(new File(BASE, String.valueOf(System.currentTimeMillis())));
        while (true)
        {
            int j;
            try
            {
                File[] arrayOfFile1 = PROC.listFiles();
                int i = arrayOfFile1.length;
                j = 0;
                if (j < i)
                {
                    File localFile = arrayOfFile1[j];
                    if (isProcessDirectory(localFile))
                        dump(new File(localFile, "stat"), localFileOutputStream);
                }
                else
                {
                    File[] arrayOfFile2 = PATHS;
                    int k = arrayOfFile2.length;
                    int m = 0;
                    if (m < k)
                    {
                        dump(arrayOfFile2[m], localFileOutputStream);
                        m++;
                        continue;
                    }
                    return;
                }
            }
            finally
            {
                closeQuietly(localFileOutputStream);
            }
            j++;
        }
    }

    private void dump(File paramFile, OutputStream paramOutputStream)
        throws IOException
    {
        writeHeader(paramFile, paramOutputStream);
        Object localObject1 = null;
        try
        {
            FileInputStream localFileInputStream = new FileInputStream(paramFile);
            try
            {
                int i = localFileInputStream.read(this.buffer);
                if (i != -1)
                    paramOutputStream.write(this.buffer, 0, i);
            }
            finally
            {
                localObject1 = localFileInputStream;
                label54: closeQuietly(localObject1);
            }
        }
        finally
        {
            break label54;
        }
    }

    private static boolean isProcessDirectory(File paramFile)
    {
        try
        {
            Integer.parseInt(paramFile.getName());
            boolean bool2 = paramFile.isDirectory();
            bool1 = bool2;
            return bool1;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    private void monitor()
    {
        while (true)
        {
            waitForStart();
            purge();
            int i = 0;
            while (true)
                if (i < 10)
                    try
                    {
                        dump();
                        pause();
                        i++;
                    }
                    catch (IOException localIOException)
                    {
                        while (true)
                            Slog.w(LOG_TAG, "Dump failed.", localIOException);
                    }
            stop();
        }
    }

    private void pause()
    {
        try
        {
            Thread.sleep(1000L);
            label6: return;
        }
        catch (InterruptedException localInterruptedException)
        {
            break label6;
        }
    }

    private void purge()
    {
        File[] arrayOfFile = BASE.listFiles();
        int i = -30 + arrayOfFile.length;
        if (i > 0)
        {
            Arrays.sort(arrayOfFile);
            for (int j = 0; j < i; j++)
                if (!arrayOfFile[j].delete())
                    Slog.w(LOG_TAG, "Couldn't delete " + arrayOfFile[j] + ".");
        }
    }

    static void start()
    {
        instance.startMonitoring();
    }

    /** @deprecated */
    private void startMonitoring()
    {
        try
        {
            if (!this.running)
            {
                this.running = true;
                notifyAll();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    private void stop()
    {
        try
        {
            this.running = false;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    private void waitForStart()
    {
        try
        {
            while (true)
            {
                boolean bool = this.running;
                if (bool)
                    break;
                try
                {
                    wait();
                }
                catch (InterruptedException localInterruptedException)
                {
                }
            }
            return;
        }
        finally
        {
        }
    }

    private static void writeHeader(File paramFile, OutputStream paramOutputStream)
        throws IOException
    {
        paramOutputStream.write(("*** " + paramFile.toString() + "\n").getBytes());
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.DeviceMonitor
 * JD-Core Version:        0.6.2
 */