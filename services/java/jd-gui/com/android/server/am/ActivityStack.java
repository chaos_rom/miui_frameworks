package com.android.server.am;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.ActivityManager.TaskThumbnails;
import android.app.ActivityOptions;
import android.app.AppGlobals;
import android.app.IActivityController;
import android.app.IActivityManager.WaitResult;
import android.app.IApplicationThread;
import android.app.IThumbnailRetriever.Stub;
import android.app.MiuiThemeHelper;
import android.app.ResultInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.IPackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.CompatibilityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.UserId;
import android.util.EventLog;
import android.util.Log;
import android.util.Slog;
import android.view.IApplicationToken.Stub;
import com.android.internal.os.BatteryStatsImpl;
import com.android.internal.os.BatteryStatsImpl.Uid.Proc;
import com.android.internal.os.ProcessStats;
import com.android.server.ProcessMap;
import com.android.server.wm.WindowManagerService;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

final class ActivityStack
{
    static final long ACTIVITY_INACTIVE_RESET_TIME = 0L;
    static final boolean DEBUG_ADD_REMOVE = false;
    static final boolean DEBUG_CONFIGURATION = false;
    static final boolean DEBUG_PAUSE = false;
    static final boolean DEBUG_RESULTS = false;
    static final boolean DEBUG_SAVED_STATE = false;
    static final boolean DEBUG_STATES = false;
    static final boolean DEBUG_SWITCH = false;
    static final boolean DEBUG_TASKS = false;
    static final boolean DEBUG_TRANSITION = false;
    static final boolean DEBUG_USER_LEAVING = false;
    static final boolean DEBUG_VISBILITY = false;
    static final int DESTROY_ACTIVITIES_MSG = 109;
    static final int DESTROY_TIMEOUT = 10000;
    static final int DESTROY_TIMEOUT_MSG = 105;
    private static final int FINISH_AFTER_PAUSE = 1;
    private static final int FINISH_AFTER_VISIBLE = 2;
    private static final int FINISH_IMMEDIATELY = 0;
    static final int IDLE_NOW_MSG = 103;
    static final int IDLE_TIMEOUT = 10000;
    static final int IDLE_TIMEOUT_MSG = 102;
    static final int LAUNCH_TICK = 500;
    static final int LAUNCH_TICK_MSG = 107;
    static final int LAUNCH_TIMEOUT = 10000;
    static final int LAUNCH_TIMEOUT_MSG = 104;
    static final int PAUSE_TIMEOUT = 500;
    static final int PAUSE_TIMEOUT_MSG = 101;
    static final int RESUME_TOP_ACTIVITY_MSG = 106;
    static final boolean SHOW_APP_STARTING_PREVIEW = true;
    static final int SLEEP_TIMEOUT = 5000;
    static final int SLEEP_TIMEOUT_MSG = 100;
    static final long START_WARN_TIME = 5000L;
    static final int STOP_TIMEOUT = 10000;
    static final int STOP_TIMEOUT_MSG = 108;
    static final String TAG = "ActivityManager";
    static final boolean VALIDATE_TOKENS;
    static final boolean localLOGV;
    boolean mConfigWillChange;
    final Context mContext;
    private int mCurrentUser;
    boolean mDismissKeyguardOnNextActivity = false;
    final ArrayList<ActivityRecord> mFinishingActivities = new ArrayList();
    final PowerManager.WakeLock mGoingToSleep;
    final ArrayList<ActivityRecord> mGoingToSleepActivities = new ArrayList();
    final Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            IApplicationToken.Stub localStub1 = null;
            switch (paramAnonymousMessage.what)
            {
            default:
            case 100:
            case 101:
            case 102:
            case 107:
            case 105:
            case 103:
            case 104:
            case 106:
            case 108:
            case 109:
            }
            while (true)
            {
                return;
                synchronized (ActivityStack.this.mService)
                {
                    if (ActivityStack.this.mService.isSleeping())
                    {
                        Slog.w("ActivityManager", "Sleep timeout!    Sleeping now.");
                        ActivityStack.this.mSleepTimeout = true;
                        ActivityStack.this.checkReadyForSleepLocked();
                    }
                }
                ActivityRecord localActivityRecord6 = (ActivityRecord)paramAnonymousMessage.obj;
                Slog.w("ActivityManager", "Activity pause timeout for " + localActivityRecord6);
                synchronized (ActivityStack.this.mService)
                {
                    if (localActivityRecord6.app != null)
                        ActivityStack.this.mService.logAppTooSlow(localActivityRecord6.app, localActivityRecord6.pauseTime, "pausing " + localActivityRecord6);
                    ActivityStack localActivityStack4 = ActivityStack.this;
                    if (localActivityRecord6 != null)
                        localStub1 = localActivityRecord6.appToken;
                    localActivityStack4.activityPaused(localStub1, true);
                }
                if (ActivityStack.this.mService.mDidDexOpt)
                {
                    ActivityStack.this.mService.mDidDexOpt = false;
                    Message localMessage2 = ActivityStack.this.mHandler.obtainMessage(102);
                    localMessage2.obj = paramAnonymousMessage.obj;
                    ActivityStack.this.mHandler.sendMessageDelayed(localMessage2, 10000L);
                    continue;
                }
                ActivityRecord localActivityRecord5 = (ActivityRecord)paramAnonymousMessage.obj;
                Slog.w("ActivityManager", "Activity idle timeout for " + localActivityRecord5);
                ActivityStack localActivityStack3 = ActivityStack.this;
                if (localActivityRecord5 != null);
                for (IApplicationToken.Stub localStub3 = localActivityRecord5.appToken; ; localStub3 = null)
                {
                    localActivityStack3.activityIdleInternal(localStub3, true, null);
                    break;
                }
                ActivityRecord localActivityRecord4 = (ActivityRecord)paramAnonymousMessage.obj;
                synchronized (ActivityStack.this.mService)
                {
                    if (localActivityRecord4.continueLaunchTickingLocked())
                        ActivityStack.this.mService.logAppTooSlow(localActivityRecord4.app, localActivityRecord4.launchTickTime, "launching " + localActivityRecord4);
                }
                ActivityRecord localActivityRecord3 = (ActivityRecord)paramAnonymousMessage.obj;
                Slog.w("ActivityManager", "Activity destroy timeout for " + localActivityRecord3);
                ActivityStack localActivityStack2 = ActivityStack.this;
                if (localActivityRecord3 != null)
                    localStub1 = localActivityRecord3.appToken;
                localActivityStack2.activityDestroyed(localStub1);
                continue;
                ActivityRecord localActivityRecord2 = (ActivityRecord)paramAnonymousMessage.obj;
                ActivityStack localActivityStack1 = ActivityStack.this;
                if (localActivityRecord2 != null);
                for (IApplicationToken.Stub localStub2 = localActivityRecord2.appToken; ; localStub2 = null)
                {
                    localActivityStack1.activityIdleInternal(localStub2, false, null);
                    break;
                }
                if (ActivityStack.this.mService.mDidDexOpt)
                {
                    ActivityStack.this.mService.mDidDexOpt = false;
                    Message localMessage1 = ActivityStack.this.mHandler.obtainMessage(104);
                    ActivityStack.this.mHandler.sendMessageDelayed(localMessage1, 10000L);
                    continue;
                }
                synchronized (ActivityStack.this.mService)
                {
                    if (ActivityStack.this.mLaunchingActivity.isHeld())
                    {
                        Slog.w("ActivityManager", "Launch timeout has expired, giving up wake lock!");
                        ActivityStack.this.mLaunchingActivity.release();
                    }
                }
                synchronized (ActivityStack.this.mService)
                {
                    ActivityStack.this.resumeTopActivityLocked(null);
                }
                ActivityRecord localActivityRecord1 = (ActivityRecord)paramAnonymousMessage.obj;
                Slog.w("ActivityManager", "Activity stop timeout for " + localActivityRecord1);
                synchronized (ActivityStack.this.mService)
                {
                    if (localActivityRecord1.isInHistory())
                        ActivityStack.this.activityStoppedLocked(localActivityRecord1, null, null, null);
                }
                ActivityStack.ScheduleDestroyArgs localScheduleDestroyArgs = (ActivityStack.ScheduleDestroyArgs)paramAnonymousMessage.obj;
                synchronized (ActivityStack.this.mService)
                {
                    ActivityStack.this.destroyActivitiesLocked(localScheduleDestroyArgs.mOwner, localScheduleDestroyArgs.mOomAdj, localScheduleDestroyArgs.mReason);
                }
            }
        }
    };
    final ArrayList<ActivityRecord> mHistory = new ArrayList();
    long mInitialStartTime = 0L;
    final ArrayList<ActivityRecord> mLRUActivities = new ArrayList();
    ActivityRecord mLastPausedActivity = null;
    ActivityRecord mLastStartedActivity = null;
    final PowerManager.WakeLock mLaunchingActivity;
    final boolean mMainStack;
    final ArrayList<ActivityRecord> mNoAnimActivities = new ArrayList();
    ActivityRecord mPausingActivity = null;
    ActivityRecord mResumedActivity = null;
    final ActivityManagerService mService;
    boolean mSleepTimeout = false;
    final ArrayList<ActivityRecord> mStoppingActivities = new ArrayList();
    int mThumbnailHeight = -1;
    int mThumbnailWidth = -1;
    boolean mUserLeaving = false;
    final ArrayList<IBinder> mValidateAppTokens = new ArrayList();
    final ArrayList<IActivityManager.WaitResult> mWaitingActivityLaunched = new ArrayList();
    final ArrayList<IActivityManager.WaitResult> mWaitingActivityVisible = new ArrayList();
    final ArrayList<ActivityRecord> mWaitingVisibleActivities = new ArrayList();

    ActivityStack(ActivityManagerService paramActivityManagerService, Context paramContext, boolean paramBoolean)
    {
        this.mService = paramActivityManagerService;
        this.mContext = paramContext;
        this.mMainStack = paramBoolean;
        PowerManager localPowerManager = (PowerManager)paramContext.getSystemService("power");
        this.mGoingToSleep = localPowerManager.newWakeLock(1, "ActivityManager-Sleep");
        this.mLaunchingActivity = localPowerManager.newWakeLock(1, "ActivityManager-Launch");
        this.mLaunchingActivity.setReferenceCounted(false);
    }

    private final void completePauseLocked()
    {
        ActivityRecord localActivityRecord = this.mPausingActivity;
        if (localActivityRecord != null)
        {
            if (!localActivityRecord.finishing)
                break label184;
            localActivityRecord = finishCurrentActivityLocked(localActivityRecord, 2);
        }
        while (true)
        {
            this.mPausingActivity = null;
            label44: long l;
            if (!this.mService.isSleeping())
            {
                resumeTopActivityLocked(localActivityRecord);
                if (localActivityRecord != null)
                    localActivityRecord.resumeKeyDispatchingLocked();
                if ((localActivityRecord.app != null) && (localActivityRecord.cpuTimeAtResume > 0L) && (this.mService.mBatteryStatsService.isOnBattery()))
                    synchronized (this.mService.mProcessStatsThread)
                    {
                        l = this.mService.mProcessStats.getCpuTimeForPid(localActivityRecord.app.pid) - localActivityRecord.cpuTimeAtResume;
                        if (l <= 0L);
                    }
            }
            synchronized (this.mService.mBatteryStatsService.getActiveStatistics())
            {
                BatteryStatsImpl.Uid.Proc localProc = ???.getProcessStatsLocked(localActivityRecord.info.applicationInfo.uid, localActivityRecord.info.packageName);
                if (localProc != null)
                    localProc.addForegroundTimeLocked(l);
                localActivityRecord.cpuTimeAtResume = 0L;
                return;
                label184: if (localActivityRecord.app != null)
                {
                    if (localActivityRecord.waitingVisible)
                    {
                        localActivityRecord.waitingVisible = false;
                        this.mWaitingVisibleActivities.remove(localActivityRecord);
                    }
                    if (localActivityRecord.configDestroy)
                    {
                        destroyActivityLocked(localActivityRecord, true, false, "pause-config");
                        continue;
                    }
                    this.mStoppingActivities.add(localActivityRecord);
                    if (this.mStoppingActivities.size() > 3)
                    {
                        scheduleIdleLocked();
                        continue;
                    }
                    checkReadyForSleepLocked();
                    continue;
                }
                localActivityRecord = null;
                continue;
                checkReadyForSleepLocked();
                if (topRunningActivityLocked(null) != null)
                    break label44;
                resumeTopActivityLocked(null);
                break label44;
                localObject1 = finally;
                throw localObject1;
            }
        }
    }

    private final void completeResumeLocked(ActivityRecord paramActivityRecord)
    {
        paramActivityRecord.idle = false;
        paramActivityRecord.results = null;
        paramActivityRecord.newIntents = null;
        Message localMessage = this.mHandler.obtainMessage(102);
        localMessage.obj = paramActivityRecord;
        this.mHandler.sendMessageDelayed(localMessage, 10000L);
        if (this.mMainStack)
            this.mService.reportResumedActivityLocked(paramActivityRecord);
        paramActivityRecord.clearThumbnail();
        if (this.mMainStack)
            this.mService.setFocusedActivityLocked(paramActivityRecord);
        paramActivityRecord.resumeKeyDispatchingLocked();
        ensureActivitiesVisibleLocked(null, 0);
        this.mService.mWindowManager.executeAppTransition();
        this.mNoAnimActivities.clear();
        if (paramActivityRecord.app != null)
            synchronized (this.mService.mProcessStatsThread)
            {
                paramActivityRecord.cpuTimeAtResume = this.mService.mProcessStats.getCpuTimeForPid(paramActivityRecord.app.pid);
            }
        paramActivityRecord.cpuTimeAtResume = 0L;
    }

    private final int findActivityInHistoryLocked(ActivityRecord paramActivityRecord, int paramInt)
    {
        int i = this.mHistory.size();
        ActivityRecord localActivityRecord;
        while (i > 0)
        {
            i--;
            localActivityRecord = (ActivityRecord)this.mHistory.get(i);
            if (!localActivityRecord.finishing)
                if (localActivityRecord.task.taskId == paramInt)
                    break label55;
        }
        for (int j = -1; ; j = i)
        {
            return j;
            label55: if (!localActivityRecord.realActivity.equals(paramActivityRecord.realActivity))
                break;
        }
    }

    private ActivityRecord findActivityLocked(Intent paramIntent, ActivityInfo paramActivityInfo)
    {
        ComponentName localComponentName = paramIntent.getComponent();
        if (paramActivityInfo.targetActivity != null)
            localComponentName = new ComponentName(paramActivityInfo.packageName, paramActivityInfo.targetActivity);
        int i = UserId.getUserId(paramActivityInfo.applicationInfo.uid);
        int j = -1 + this.mHistory.size();
        ActivityRecord localActivityRecord;
        if (j >= 0)
        {
            localActivityRecord = (ActivityRecord)this.mHistory.get(j);
            if ((localActivityRecord.finishing) || (!localActivityRecord.intent.getComponent().equals(localComponentName)) || (localActivityRecord.userId != i));
        }
        while (true)
        {
            return localActivityRecord;
            j--;
            break;
            localActivityRecord = null;
        }
    }

    private ActivityRecord findTaskLocked(Intent paramIntent, ActivityInfo paramActivityInfo)
    {
        ComponentName localComponentName = paramIntent.getComponent();
        if (paramActivityInfo.targetActivity != null)
            localComponentName = new ComponentName(paramActivityInfo.packageName, paramActivityInfo.targetActivity);
        TaskRecord localTaskRecord = null;
        int i = UserId.getUserId(paramActivityInfo.applicationInfo.uid);
        int j = -1 + this.mHistory.size();
        ActivityRecord localActivityRecord;
        if (j >= 0)
        {
            localActivityRecord = (ActivityRecord)this.mHistory.get(j);
            if ((!localActivityRecord.finishing) && (localActivityRecord.task != localTaskRecord) && (localActivityRecord.userId == i) && (localActivityRecord.launchMode != 3))
            {
                localTaskRecord = localActivityRecord.task;
                if (localActivityRecord.task.affinity != null)
                    if (!localActivityRecord.task.affinity.equals(paramActivityInfo.taskAffinity))
                        break label208;
            }
        }
        while (true)
        {
            return localActivityRecord;
            if (((localActivityRecord.task.intent == null) || (!localActivityRecord.task.intent.getComponent().equals(localComponentName))) && ((localActivityRecord.task.affinityIntent == null) || (!localActivityRecord.task.affinityIntent.getComponent().equals(localComponentName))))
            {
                label208: j--;
                break;
                localActivityRecord = null;
            }
        }
    }

    private final ActivityRecord finishCurrentActivityLocked(ActivityRecord paramActivityRecord, int paramInt)
    {
        int i = indexOfActivityLocked(paramActivityRecord);
        if (i < 0);
        for (ActivityRecord localActivityRecord = null; ; localActivityRecord = finishCurrentActivityLocked(paramActivityRecord, i, paramInt))
            return localActivityRecord;
    }

    private final ActivityRecord finishCurrentActivityLocked(ActivityRecord paramActivityRecord, int paramInt1, int paramInt2)
    {
        if ((paramInt2 == 2) && (paramActivityRecord.nowVisible))
            if (!this.mStoppingActivities.contains(paramActivityRecord))
            {
                this.mStoppingActivities.add(paramActivityRecord);
                if (this.mStoppingActivities.size() > 3)
                    scheduleIdleLocked();
            }
            else
            {
                paramActivityRecord.state = ActivityState.STOPPING;
                this.mService.updateOomAdjLocked();
            }
        while (true)
        {
            return paramActivityRecord;
            checkReadyForSleepLocked();
            break;
            this.mStoppingActivities.remove(paramActivityRecord);
            this.mGoingToSleepActivities.remove(paramActivityRecord);
            this.mWaitingVisibleActivities.remove(paramActivityRecord);
            if (this.mResumedActivity == paramActivityRecord)
                this.mResumedActivity = null;
            ActivityState localActivityState = paramActivityRecord.state;
            paramActivityRecord.state = ActivityState.FINISHING;
            if ((paramInt2 == 0) || (localActivityState == ActivityState.STOPPED) || (localActivityState == ActivityState.INITIALIZING))
            {
                boolean bool = destroyActivityLocked(paramActivityRecord, true, true, "finish-imm");
                if (bool)
                    resumeTopActivityLocked(null);
                if (bool)
                    paramActivityRecord = null;
            }
            else
            {
                this.mFinishingActivities.add(paramActivityRecord);
                resumeTopActivityLocked(null);
            }
        }
    }

    private final void finishTaskMoveLocked(int paramInt)
    {
        resumeTopActivityLocked(null);
    }

    private final void logStartActivity(int paramInt, ActivityRecord paramActivityRecord, TaskRecord paramTaskRecord)
    {
        Object[] arrayOfObject = new Object[7];
        arrayOfObject[0] = Integer.valueOf(System.identityHashCode(paramActivityRecord));
        arrayOfObject[1] = Integer.valueOf(paramTaskRecord.taskId);
        arrayOfObject[2] = paramActivityRecord.shortComponentName;
        arrayOfObject[3] = paramActivityRecord.intent.getAction();
        arrayOfObject[4] = paramActivityRecord.intent.getType();
        arrayOfObject[5] = paramActivityRecord.intent.getDataString();
        arrayOfObject[6] = Integer.valueOf(paramActivityRecord.intent.getFlags());
        EventLog.writeEvent(paramInt, arrayOfObject);
    }

    private final ActivityRecord moveActivityToFrontLocked(int paramInt)
    {
        ActivityRecord localActivityRecord1 = (ActivityRecord)this.mHistory.remove(paramInt);
        int i = this.mHistory.size();
        ActivityRecord localActivityRecord2 = (ActivityRecord)this.mHistory.get(i - 1);
        this.mHistory.add(i, localActivityRecord1);
        localActivityRecord2.frontOfTask = false;
        localActivityRecord1.frontOfTask = true;
        return localActivityRecord1;
    }

    private final void performClearTaskAtIndexLocked(int paramInt1, int paramInt2)
    {
        while (true)
        {
            ActivityRecord localActivityRecord;
            if (paramInt2 < this.mHistory.size())
            {
                localActivityRecord = (ActivityRecord)this.mHistory.get(paramInt2);
                if (localActivityRecord.task.taskId == paramInt1);
            }
            else
            {
                return;
            }
            if (localActivityRecord.finishing)
                paramInt2++;
            else if (!finishActivityLocked(localActivityRecord, paramInt2, 0, null, "clear"))
                paramInt2++;
        }
    }

    private final ActivityRecord performClearTaskLocked(int paramInt1, ActivityRecord paramActivityRecord, int paramInt2)
    {
        Object localObject = null;
        int i = this.mHistory.size();
        while (i > 0)
        {
            i--;
            if (((ActivityRecord)this.mHistory.get(i)).task.taskId == paramInt1)
                i++;
        }
        ActivityRecord localActivityRecord1;
        while (i > 0)
        {
            i--;
            localActivityRecord1 = (ActivityRecord)this.mHistory.get(i);
            if (!localActivityRecord1.finishing)
                if (localActivityRecord1.task.taskId == paramInt1)
                    break label90;
        }
        while (true)
        {
            return localObject;
            label90: if (!localActivityRecord1.realActivity.equals(paramActivityRecord.realActivity))
                break;
            while (true)
            {
                ActivityRecord localActivityRecord2;
                if (i < -1 + this.mHistory.size())
                {
                    i++;
                    localActivityRecord2 = (ActivityRecord)this.mHistory.get(i);
                    if (localActivityRecord2.task.taskId == paramInt1);
                }
                else
                {
                    if ((localActivityRecord1.launchMode != 0) || ((0x20000000 & paramInt2) != 0) || (localActivityRecord1.finishing))
                        break label236;
                    int j = indexOfTokenLocked(localActivityRecord1.appToken);
                    if (j < 0)
                        break;
                    finishActivityLocked(localActivityRecord1, j, 0, null, "clear");
                    break;
                }
                if ((!localActivityRecord2.finishing) && (finishActivityLocked(localActivityRecord2, i, 0, null, "clear")))
                    i--;
            }
            label236: localObject = localActivityRecord1;
        }
    }

    private final void performClearTaskLocked(int paramInt)
    {
        int i = this.mHistory.size();
        while (i > 0)
        {
            i--;
            if (((ActivityRecord)this.mHistory.get(i)).task.taskId == paramInt)
                i++;
        }
        while (i > 0)
        {
            i--;
            ActivityRecord localActivityRecord = (ActivityRecord)this.mHistory.get(i);
            if ((!localActivityRecord.finishing) && (localActivityRecord.task.taskId != paramInt))
                performClearTaskAtIndexLocked(paramInt, i + 1);
        }
    }

    private final boolean relaunchActivityLocked(ActivityRecord paramActivityRecord, int paramInt, boolean paramBoolean)
    {
        boolean bool = false;
        ArrayList localArrayList1 = null;
        ArrayList localArrayList2 = null;
        if (paramBoolean)
        {
            localArrayList1 = paramActivityRecord.results;
            localArrayList2 = paramActivityRecord.newIntents;
        }
        int i;
        if (paramBoolean)
            i = 30019;
        while (true)
        {
            Object[] arrayOfObject = new Object[3];
            arrayOfObject[bool] = Integer.valueOf(System.identityHashCode(paramActivityRecord));
            arrayOfObject[1] = Integer.valueOf(paramActivityRecord.task.taskId);
            arrayOfObject[2] = paramActivityRecord.shortComponentName;
            EventLog.writeEvent(i, arrayOfObject);
            paramActivityRecord.startFreezingScreenLocked(paramActivityRecord.app, 0);
            try
            {
                paramActivityRecord.forceNewConfig = false;
                IApplicationThread localIApplicationThread = paramActivityRecord.app.thread;
                IApplicationToken.Stub localStub = paramActivityRecord.appToken;
                if (!paramBoolean)
                    bool = true;
                localIApplicationThread.scheduleRelaunchActivity(localStub, localArrayList1, localArrayList2, paramInt, bool, new Configuration(this.mService.mConfiguration));
                label148: if (paramBoolean)
                {
                    paramActivityRecord.results = null;
                    paramActivityRecord.newIntents = null;
                    if (this.mMainStack)
                        this.mService.reportResumedActivityLocked(paramActivityRecord);
                }
                for (paramActivityRecord.state = ActivityState.RESUMED; ; paramActivityRecord.state = ActivityState.PAUSED)
                {
                    return true;
                    i = 30020;
                    break;
                    this.mHandler.removeMessages(101, paramActivityRecord);
                }
            }
            catch (RemoteException localRemoteException)
            {
                break label148;
            }
        }
    }

    private void removeHistoryRecordsForAppLocked(ArrayList paramArrayList, ProcessRecord paramProcessRecord)
    {
        int i = paramArrayList.size();
        while (i > 0)
        {
            i--;
            ActivityRecord localActivityRecord = (ActivityRecord)paramArrayList.get(i);
            if (localActivityRecord.app == paramProcessRecord)
            {
                paramArrayList.remove(i);
                removeTimeoutsForActivityLocked(localActivityRecord);
            }
        }
    }

    private void removeTimeoutsForActivityLocked(ActivityRecord paramActivityRecord)
    {
        this.mHandler.removeMessages(101, paramActivityRecord);
        this.mHandler.removeMessages(108, paramActivityRecord);
        this.mHandler.removeMessages(102, paramActivityRecord);
        this.mHandler.removeMessages(105, paramActivityRecord);
        paramActivityRecord.finishLaunchTickingLocked();
    }

    private final ActivityRecord resetTaskIfNeededLocked(ActivityRecord paramActivityRecord1, ActivityRecord paramActivityRecord2)
    {
        int i;
        TaskRecord localTaskRecord1;
        Object localObject;
        int j;
        int k;
        int m;
        int n;
        int i1;
        label50: ActivityRecord localActivityRecord1;
        if ((0x4 & paramActivityRecord2.info.flags) != 0)
        {
            i = 1;
            localTaskRecord1 = paramActivityRecord1.task;
            localObject = null;
            j = 0;
            k = -1;
            m = -1;
            n = -1;
            i1 = -1 + this.mHistory.size();
            if (i1 < -1)
                break label123;
            if (i1 < 0)
                break label100;
            localActivityRecord1 = (ActivityRecord)this.mHistory.get(i1);
            label76: if ((localActivityRecord1 == null) || (!localActivityRecord1.finishing))
                break label106;
        }
        while (true)
        {
            i1--;
            break label50;
            i = 0;
            break;
            label100: localActivityRecord1 = null;
            break label76;
            label106: if ((localActivityRecord1 != null) && (localActivityRecord1.userId != paramActivityRecord1.userId))
                label123: return paramActivityRecord1;
            if (localObject != null)
                break label145;
            localObject = localActivityRecord1;
            j = i1;
            m = -1;
        }
        label145: int i2 = localObject.info.flags;
        int i3;
        label165: int i4;
        label176: int i8;
        if ((i2 & 0x2) != 0)
        {
            i3 = 1;
            if ((i2 & 0x40) == 0)
                break label272;
            i4 = 1;
            if (localObject.task != localTaskRecord1)
                break label858;
            if (k < 0)
                k = j;
            if ((localActivityRecord1 == null) || (localActivityRecord1.task != localTaskRecord1))
                break label851;
            if ((0x80000 & localObject.intent.getFlags()) == 0)
                break label278;
            i8 = 1;
            label228: if ((i3 != 0) || (i8 != 0) || (localObject.resultTo == null))
                break label284;
            if (m < 0)
                m = j;
        }
        label272: label278: label792: 
        while (true)
        {
            localObject = localActivityRecord1;
            j = i1;
            break;
            i3 = 0;
            break label165;
            i4 = 0;
            break label176;
            i8 = 0;
            break label228;
            label284: label548: label1194: if ((i3 == 0) && (i8 == 0) && (i4 != 0) && (localObject.taskAffinity != null) && (!localObject.taskAffinity.equals(localTaskRecord1.affinity)))
            {
                ActivityRecord localActivityRecord6 = (ActivityRecord)this.mHistory.get(0);
                int i12;
                ThumbnailHolder localThumbnailHolder1;
                int i13;
                if ((localObject.taskAffinity != null) && (localObject.taskAffinity.equals(localActivityRecord6.task.affinity)))
                {
                    TaskRecord localTaskRecord3 = localActivityRecord6.task;
                    ThumbnailHolder localThumbnailHolder2 = localActivityRecord6.thumbHolder;
                    localObject.setTask(localTaskRecord3, localThumbnailHolder2, false);
                    this.mService.mWindowManager.setAppGroupId(localObject.appToken, localTaskRecord1.taskId);
                    if (m < 0)
                        m = j;
                    i12 = 0;
                    localThumbnailHolder1 = localObject.thumbHolder;
                    i13 = j;
                    label430: if (i13 > m)
                        break label638;
                    localActivityRecord6 = (ActivityRecord)this.mHistory.get(i13);
                    if (!localActivityRecord6.finishing)
                        break label548;
                }
                while (true)
                {
                    i13++;
                    break label430;
                    ActivityManagerService localActivityManagerService = this.mService;
                    localActivityManagerService.mCurTask = (1 + localActivityManagerService.mCurTask);
                    if (this.mService.mCurTask <= 0)
                        this.mService.mCurTask = 1;
                    TaskRecord localTaskRecord2 = new TaskRecord(this.mService.mCurTask, localObject.info, null);
                    localObject.setTask(localTaskRecord2, null, false);
                    localObject.task.affinityIntent = localObject.intent;
                    break;
                    localActivityRecord6.setTask(localObject.task, localThumbnailHolder1, false);
                    localThumbnailHolder1 = localActivityRecord6.thumbHolder;
                    this.mHistory.remove(i13);
                    this.mHistory.add(i12, localActivityRecord6);
                    this.mService.mWindowManager.moveAppToken(i12, localActivityRecord6.appToken);
                    this.mService.mWindowManager.setAppGroupId(localActivityRecord6.appToken, localActivityRecord6.task.taskId);
                    i12++;
                    i1++;
                }
                label638: if (paramActivityRecord1 == localActivityRecord6)
                    paramActivityRecord1 = localActivityRecord1;
                if (k == m)
                    k = -1;
                m = -1;
            }
            else
            {
                label817: label1227: if ((i != 0) || (i3 != 0) || (i8 != 0))
                {
                    ActivityRecord localActivityRecord5;
                    int i9;
                    if (i8 != 0)
                    {
                        for (int i10 = j + 1; ; i10++)
                        {
                            int i11 = this.mHistory.size();
                            if ((i10 >= i11) || (((ActivityRecord)this.mHistory.get(i10)).task != localTaskRecord1))
                                break;
                        }
                        m = i10 - 1;
                        localActivityRecord5 = null;
                        i9 = j;
                        label745: if (i9 > m)
                            break label817;
                        localActivityRecord5 = (ActivityRecord)this.mHistory.get(i9);
                        if (!localActivityRecord5.finishing)
                            break label792;
                    }
                    while (true)
                    {
                        i9++;
                        break label745;
                        if (m >= 0)
                            break;
                        m = j;
                        break;
                        if (finishActivityLocked(localActivityRecord5, i9, 0, null, "reset"))
                        {
                            m--;
                            i9--;
                        }
                    }
                    if (paramActivityRecord1 == localActivityRecord5)
                        paramActivityRecord1 = localActivityRecord1;
                    if (k == m)
                        k = -1;
                    m = -1;
                }
                else
                {
                    m = -1;
                    continue;
                    label851: m = -1;
                    continue;
                    label858: if ((localObject.resultTo != null) && ((localActivityRecord1 == null) || (localActivityRecord1.task == localObject.task)))
                    {
                        if (m < 0)
                            m = j;
                    }
                    else if ((k >= 0) && (i4 != 0) && (localTaskRecord1.affinity != null) && (localTaskRecord1.affinity.equals(localObject.taskAffinity)))
                    {
                        if ((i != 0) || (i3 != 0))
                        {
                            if (m < 0)
                                m = j;
                            int i5 = j;
                            if (i5 <= m)
                            {
                                ActivityRecord localActivityRecord2 = (ActivityRecord)this.mHistory.get(i5);
                                if (localActivityRecord2.finishing);
                                while (true)
                                {
                                    i5++;
                                    break;
                                    if (finishActivityLocked(localActivityRecord2, i5, 0, null, "reset"))
                                    {
                                        k--;
                                        n--;
                                        m--;
                                        i5--;
                                    }
                                }
                            }
                            m = -1;
                        }
                        else
                        {
                            if (m < 0)
                                m = j;
                            int i6 = m;
                            while (i6 >= j)
                            {
                                ActivityRecord localActivityRecord4 = (ActivityRecord)this.mHistory.get(i6);
                                if (localActivityRecord4.finishing)
                                {
                                    i6--;
                                }
                                else
                                {
                                    if (n < 0)
                                    {
                                        n = k;
                                        paramActivityRecord1 = localActivityRecord4;
                                    }
                                    while (true)
                                    {
                                        this.mHistory.remove(i6);
                                        localActivityRecord4.setTask(localTaskRecord1, null, false);
                                        this.mHistory.add(n, localActivityRecord4);
                                        WindowManagerService localWindowManagerService = this.mService.mWindowManager;
                                        IApplicationToken.Stub localStub = localActivityRecord4.appToken;
                                        localWindowManagerService.moveAppToken(n, localStub);
                                        this.mService.mWindowManager.setAppGroupId(localActivityRecord4.appToken, localActivityRecord4.task.taskId);
                                        break;
                                        n--;
                                    }
                                }
                            }
                            m = -1;
                            if (localObject.info.launchMode == 1)
                            {
                                int i7 = n - 1;
                                ActivityRecord localActivityRecord3;
                                if (i7 >= 0)
                                {
                                    localActivityRecord3 = (ActivityRecord)this.mHistory.get(i7);
                                    if (!localActivityRecord3.finishing)
                                        break label1227;
                                }
                                while (true)
                                {
                                    i7--;
                                    break label1194;
                                    break;
                                    if ((localActivityRecord3.intent.getComponent().equals(localObject.intent.getComponent())) && (finishActivityLocked(localActivityRecord3, i7, 0, null, "replace")))
                                    {
                                        k--;
                                        n--;
                                    }
                                }
                            }
                        }
                    }
                    else if ((localActivityRecord1 != null) && (localActivityRecord1.task != localObject.task))
                        m = -1;
                }
            }
        }
    }

    private final void startActivityLocked(ActivityRecord paramActivityRecord, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, Bundle paramBundle)
    {
        int i = this.mHistory.size();
        int j = -1;
        int i4;
        ActivityRecord localActivityRecord2;
        if (!paramBoolean1)
        {
            i4 = 1;
            int i5 = i - 1;
            while (true)
                if (i5 >= 0)
                {
                    localActivityRecord2 = (ActivityRecord)this.mHistory.get(i5);
                    if (localActivityRecord2.finishing)
                    {
                        i5--;
                    }
                    else if (localActivityRecord2.task == paramActivityRecord.task)
                    {
                        j = i5 + 1;
                        if (i4 != 0)
                            break label150;
                        this.mHistory.add(j, paramActivityRecord);
                        paramActivityRecord.putInHistory();
                        this.mService.mWindowManager.addAppToken(j, paramActivityRecord.appToken, paramActivityRecord.task.taskId, paramActivityRecord.info.screenOrientation, paramActivityRecord.fullscreen);
                        ActivityOptions.abort(paramBundle);
                    }
                }
        }
        label135: label150: label370: label631: 
        while (true)
        {
            return;
            int m;
            ActivityRecord localActivityRecord1;
            IApplicationToken.Stub localStub2;
            if (localActivityRecord2.fullscreen)
            {
                i4 = 0;
                break;
                if (j < 0)
                    j = i;
                if (j < i)
                    this.mUserLeaving = false;
                this.mHistory.add(j, paramActivityRecord);
                paramActivityRecord.putInHistory();
                paramActivityRecord.frontOfTask = paramBoolean1;
                if (i <= 0)
                    break label591;
                boolean bool = paramBoolean1;
                ProcessRecord localProcessRecord = paramActivityRecord.app;
                if (localProcessRecord == null)
                    localProcessRecord = (ProcessRecord)this.mService.mProcessNames.get(paramActivityRecord.processName, paramActivityRecord.info.applicationInfo.uid);
                if ((localProcessRecord == null) || (localProcessRecord.thread == null))
                    bool = true;
                if ((0x10000 & paramActivityRecord.intent.getFlags()) == 0)
                    break label518;
                this.mService.mWindowManager.prepareAppTransition(0, paramBoolean3);
                this.mNoAnimActivities.add(paramActivityRecord);
                paramActivityRecord.updateOptionsLocked(paramBundle);
                this.mService.mWindowManager.addAppToken(j, paramActivityRecord.appToken, paramActivityRecord.task.taskId, paramActivityRecord.info.screenOrientation, paramActivityRecord.fullscreen);
                m = 1;
                if ((paramBoolean1) && ((0x200000 & paramActivityRecord.intent.getFlags()) != 0))
                {
                    resetTaskIfNeededLocked(paramActivityRecord, paramActivityRecord);
                    if (topRunningNonDelayedActivityLocked(null) != paramActivityRecord)
                        break label565;
                    m = 1;
                }
                if (m != 0)
                {
                    localActivityRecord1 = this.mResumedActivity;
                    if (localActivityRecord1 != null)
                    {
                        if (localActivityRecord1.task == paramActivityRecord.task)
                            break label571;
                        localActivityRecord1 = null;
                    }
                    WindowManagerService localWindowManagerService2 = this.mService.mWindowManager;
                    IApplicationToken.Stub localStub1 = paramActivityRecord.appToken;
                    String str = paramActivityRecord.packageName;
                    int n = paramActivityRecord.theme;
                    CompatibilityInfo localCompatibilityInfo = this.mService.compatibilityInfoForPackageLocked(paramActivityRecord.info.applicationInfo);
                    CharSequence localCharSequence = paramActivityRecord.nonLocalizedLabel;
                    int i1 = paramActivityRecord.labelRes;
                    int i2 = paramActivityRecord.icon;
                    int i3 = paramActivityRecord.windowFlags;
                    if (localActivityRecord1 == null)
                        break label585;
                    localStub2 = localActivityRecord1.appToken;
                    localWindowManagerService2.setAppStartingWindow(localStub1, str, n, localCompatibilityInfo, localCharSequence, i1, i2, i3, localStub2, bool);
                }
            }
            while (true)
            {
                if (!paramBoolean2)
                    break label631;
                resumeTopActivityLocked(null);
                break label135;
                break;
                WindowManagerService localWindowManagerService1 = this.mService.mWindowManager;
                if (paramBoolean1);
                for (int k = 4104; ; k = 4102)
                {
                    localWindowManagerService1.prepareAppTransition(k, paramBoolean3);
                    this.mNoAnimActivities.remove(paramActivityRecord);
                    break;
                }
                m = 0;
                break label370;
                if (!localActivityRecord1.nowVisible)
                    break label401;
                localActivityRecord1 = null;
                break label401;
                localStub2 = null;
                break label480;
                this.mService.mWindowManager.addAppToken(j, paramActivityRecord.appToken, paramActivityRecord.task.taskId, paramActivityRecord.info.screenOrientation, paramActivityRecord.fullscreen);
                ActivityOptions.abort(paramBundle);
            }
        }
    }

    private final void startPausingLocked(boolean paramBoolean1, boolean paramBoolean2)
    {
        if (this.mPausingActivity != null)
        {
            RuntimeException localRuntimeException = new RuntimeException();
            Slog.e("ActivityManager", "Trying to pause when pause is already pending for " + this.mPausingActivity, localRuntimeException);
        }
        ActivityRecord localActivityRecord = this.mResumedActivity;
        if (localActivityRecord == null)
        {
            Slog.e("ActivityManager", "Trying to pause when nothing is resumed", new RuntimeException());
            resumeTopActivityLocked(null);
        }
        while (true)
        {
            return;
            this.mResumedActivity = null;
            this.mPausingActivity = localActivityRecord;
            this.mLastPausedActivity = localActivityRecord;
            localActivityRecord.state = ActivityState.PAUSING;
            localActivityRecord.task.touchActiveTime();
            localActivityRecord.updateThumbnail(screenshotActivities(localActivityRecord), null);
            this.mService.updateCpuStats();
            if ((localActivityRecord.app != null) && (localActivityRecord.app.thread != null));
            while (true)
            {
                try
                {
                    Object[] arrayOfObject = new Object[2];
                    arrayOfObject[0] = Integer.valueOf(System.identityHashCode(localActivityRecord));
                    arrayOfObject[1] = localActivityRecord.shortComponentName;
                    EventLog.writeEvent(30013, arrayOfObject);
                    localActivityRecord.app.thread.schedulePauseActivity(localActivityRecord.appToken, localActivityRecord.finishing, paramBoolean1, localActivityRecord.configChangeFlags);
                    if (this.mMainStack)
                        this.mService.updateUsageStats(localActivityRecord, false);
                    if ((!this.mService.mSleeping) && (!this.mService.mShuttingDown))
                    {
                        this.mLaunchingActivity.acquire();
                        if (!this.mHandler.hasMessages(104))
                        {
                            Message localMessage2 = this.mHandler.obtainMessage(104);
                            this.mHandler.sendMessageDelayed(localMessage2, 10000L);
                        }
                    }
                    if (this.mPausingActivity == null)
                        break label392;
                    if (!paramBoolean2)
                        localActivityRecord.pauseKeyDispatchingLocked();
                    Message localMessage1 = this.mHandler.obtainMessage(101);
                    localMessage1.obj = localActivityRecord;
                    localActivityRecord.pauseTime = SystemClock.uptimeMillis();
                    this.mHandler.sendMessageDelayed(localMessage1, 500L);
                }
                catch (Exception localException)
                {
                    Slog.w("ActivityManager", "Exception thrown during pause", localException);
                    this.mPausingActivity = null;
                    this.mLastPausedActivity = null;
                    continue;
                }
                this.mPausingActivity = null;
                this.mLastPausedActivity = null;
            }
            label392: resumeTopActivityLocked(null);
        }
    }

    private final void startSpecificActivityLocked(ActivityRecord paramActivityRecord, boolean paramBoolean1, boolean paramBoolean2)
    {
        ProcessRecord localProcessRecord = this.mService.getProcessRecordLocked(paramActivityRecord.processName, paramActivityRecord.info.applicationInfo.uid);
        if (paramActivityRecord.launchTime == 0L)
        {
            paramActivityRecord.launchTime = SystemClock.uptimeMillis();
            if (this.mInitialStartTime == 0L)
                this.mInitialStartTime = paramActivityRecord.launchTime;
            if ((localProcessRecord == null) || (localProcessRecord.thread == null))
                break label151;
        }
        while (true)
        {
            try
            {
                localProcessRecord.addPackage(paramActivityRecord.info.packageName);
                realStartActivityLocked(paramActivityRecord, localProcessRecord, paramBoolean1, paramBoolean2);
                return;
                if (this.mInitialStartTime != 0L)
                    break;
                this.mInitialStartTime = SystemClock.uptimeMillis();
            }
            catch (RemoteException localRemoteException)
            {
                Slog.w("ActivityManager", "Exception when starting activity " + paramActivityRecord.intent.getComponent().flattenToShortString(), localRemoteException);
            }
            label151: this.mService.startProcessLocked(paramActivityRecord.processName, paramActivityRecord.info.applicationInfo, true, 0, "activity", paramActivityRecord.intent.getComponent(), false, false);
        }
    }

    private final void stopActivityLocked(ActivityRecord paramActivityRecord)
    {
        if ((((0x40000000 & paramActivityRecord.intent.getFlags()) != 0) || ((0x80 & paramActivityRecord.info.flags) != 0)) && (!paramActivityRecord.finishing) && (!this.mService.mSleeping))
            requestFinishActivityLocked(paramActivityRecord.appToken, 0, null, "no-history");
        if ((paramActivityRecord.app != null) && (paramActivityRecord.app.thread != null))
        {
            if ((this.mMainStack) && (this.mService.mFocusedActivity == paramActivityRecord))
                this.mService.setFocusedActivityLocked(topRunningActivityLocked(null));
            paramActivityRecord.resumeKeyDispatchingLocked();
        }
        try
        {
            paramActivityRecord.stopped = false;
            paramActivityRecord.state = ActivityState.STOPPING;
            if (!paramActivityRecord.visible)
                this.mService.mWindowManager.setAppVisibility(paramActivityRecord.appToken, false);
            paramActivityRecord.app.thread.scheduleStopActivity(paramActivityRecord.appToken, paramActivityRecord.visible, paramActivityRecord.configChangeFlags);
            if (this.mService.isSleeping())
                paramActivityRecord.setSleeping(true);
            Message localMessage = this.mHandler.obtainMessage(108);
            localMessage.obj = paramActivityRecord;
            this.mHandler.sendMessageDelayed(localMessage, 10000L);
            return;
        }
        catch (Exception localException)
        {
            while (true)
            {
                Slog.w("ActivityManager", "Exception thrown during pause", localException);
                paramActivityRecord.stopped = true;
                paramActivityRecord.state = ActivityState.STOPPED;
                if (paramActivityRecord.configDestroy)
                    destroyActivityLocked(paramActivityRecord, true, false, "stop-except");
            }
        }
    }

    private final boolean updateLRUListLocked(ActivityRecord paramActivityRecord)
    {
        boolean bool = this.mLRUActivities.remove(paramActivityRecord);
        this.mLRUActivities.add(paramActivityRecord);
        return bool;
    }

    final void activityDestroyed(IBinder paramIBinder)
    {
        synchronized (this.mService)
        {
            long l = Binder.clearCallingIdentity();
            try
            {
                ActivityRecord localActivityRecord = ActivityRecord.forToken(paramIBinder);
                if (localActivityRecord != null)
                    this.mHandler.removeMessages(105, localActivityRecord);
                if ((indexOfActivityLocked(localActivityRecord) >= 0) && (localActivityRecord.state == ActivityState.DESTROYING))
                {
                    cleanUpActivityLocked(localActivityRecord, true, false);
                    removeActivityFromHistoryLocked(localActivityRecord);
                }
                resumeTopActivityLocked(null);
                Binder.restoreCallingIdentity(l);
                return;
            }
            finally
            {
                localObject2 = finally;
                Binder.restoreCallingIdentity(l);
                throw localObject2;
            }
        }
    }

    // ERROR //
    final ActivityRecord activityIdleInternal(IBinder paramIBinder, boolean paramBoolean, Configuration paramConfiguration)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore 4
        //     3: aconst_null
        //     4: astore 5
        //     6: aconst_null
        //     7: astore 6
        //     9: aconst_null
        //     10: astore 7
        //     12: iconst_0
        //     13: istore 8
        //     15: iconst_0
        //     16: istore 9
        //     18: iconst_0
        //     19: istore 10
        //     21: aload_0
        //     22: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     25: astore 11
        //     27: aload 11
        //     29: monitorenter
        //     30: aload_1
        //     31: invokestatic 868	com/android/server/am/ActivityRecord:forToken	(Landroid/os/IBinder;)Lcom/android/server/am/ActivityRecord;
        //     34: astore 13
        //     36: aload 13
        //     38: ifnull +19 -> 57
        //     41: aload_0
        //     42: getfield 167	com/android/server/am/ActivityStack:mHandler	Landroid/os/Handler;
        //     45: bipush 102
        //     47: aload 13
        //     49: invokevirtual 588	android/os/Handler:removeMessages	(ILjava/lang/Object;)V
        //     52: aload 13
        //     54: invokevirtual 599	com/android/server/am/ActivityRecord:finishLaunchTickingLocked	()V
        //     57: aload_0
        //     58: aload 13
        //     60: invokevirtual 454	com/android/server/am/ActivityStack:indexOfActivityLocked	(Lcom/android/server/am/ActivityRecord;)I
        //     63: iflt +359 -> 422
        //     66: aload 13
        //     68: astore 4
        //     70: iload_2
        //     71: ifeq +16 -> 87
        //     74: aload_0
        //     75: iload_2
        //     76: aload 13
        //     78: ldc2_w 883
        //     81: ldc2_w 883
        //     84: invokevirtual 888	com/android/server/am/ActivityStack:reportActivityLaunchedLocked	(ZLcom/android/server/am/ActivityRecord;JJ)V
        //     87: aload_3
        //     88: ifnull +9 -> 97
        //     91: aload 13
        //     93: aload_3
        //     94: putfield 891	com/android/server/am/ActivityRecord:configuration	Landroid/content/res/Configuration;
        //     97: aload_0
        //     98: getfield 148	com/android/server/am/ActivityStack:mResumedActivity	Lcom/android/server/am/ActivityRecord;
        //     101: aload 13
        //     103: if_acmpne +29 -> 132
        //     106: aload_0
        //     107: getfield 195	com/android/server/am/ActivityStack:mLaunchingActivity	Landroid/os/PowerManager$WakeLock;
        //     110: invokevirtual 894	android/os/PowerManager$WakeLock:isHeld	()Z
        //     113: ifeq +19 -> 132
        //     116: aload_0
        //     117: getfield 167	com/android/server/am/ActivityStack:mHandler	Landroid/os/Handler;
        //     120: bipush 104
        //     122: invokevirtual 896	android/os/Handler:removeMessages	(I)V
        //     125: aload_0
        //     126: getfield 195	com/android/server/am/ActivityStack:mLaunchingActivity	Landroid/os/PowerManager$WakeLock;
        //     129: invokevirtual 899	android/os/PowerManager$WakeLock:release	()V
        //     132: aload 13
        //     134: iconst_1
        //     135: putfield 333	com/android/server/am/ActivityRecord:idle	Z
        //     138: aload_0
        //     139: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     142: invokevirtual 902	com/android/server/am/ActivityManagerService:scheduleAppGcsLocked	()V
        //     145: aload 13
        //     147: getfield 905	com/android/server/am/ActivityRecord:thumbnailNeeded	Z
        //     150: ifeq +38 -> 188
        //     153: aload 13
        //     155: getfield 228	com/android/server/am/ActivityRecord:app	Lcom/android/server/am/ProcessRecord;
        //     158: ifnull +30 -> 188
        //     161: aload 13
        //     163: getfield 228	com/android/server/am/ActivityRecord:app	Lcom/android/server/am/ProcessRecord;
        //     166: getfield 567	com/android/server/am/ProcessRecord:thread	Landroid/app/IApplicationThread;
        //     169: ifnull +19 -> 188
        //     172: aload 13
        //     174: getfield 228	com/android/server/am/ActivityRecord:app	Lcom/android/server/am/ProcessRecord;
        //     177: getfield 567	com/android/server/am/ProcessRecord:thread	Landroid/app/IApplicationThread;
        //     180: astore 7
        //     182: aload 13
        //     184: iconst_0
        //     185: putfield 905	com/android/server/am/ActivityRecord:thumbnailNeeded	Z
        //     188: aload_0
        //     189: aconst_null
        //     190: iconst_0
        //     191: invokevirtual 370	com/android/server/am/ActivityStack:ensureActivitiesVisibleLocked	(Lcom/android/server/am/ActivityRecord;I)V
        //     194: aload_0
        //     195: getfield 173	com/android/server/am/ActivityStack:mMainStack	Z
        //     198: ifeq +24 -> 222
        //     201: aload_0
        //     202: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     205: getfield 908	com/android/server/am/ActivityManagerService:mBooted	Z
        //     208: ifne +14 -> 222
        //     211: aload_0
        //     212: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     215: iconst_1
        //     216: putfield 908	com/android/server/am/ActivityManagerService:mBooted	Z
        //     219: iconst_1
        //     220: istore 9
        //     222: aload_0
        //     223: iconst_1
        //     224: invokevirtual 912	com/android/server/am/ActivityStack:processStoppingActivitiesLocked	(Z)Ljava/util/ArrayList;
        //     227: astore 14
        //     229: aload 14
        //     231: ifnull +216 -> 447
        //     234: aload 14
        //     236: invokevirtual 318	java/util/ArrayList:size	()I
        //     239: istore 15
        //     241: aload_0
        //     242: getfield 138	com/android/server/am/ActivityStack:mFinishingActivities	Ljava/util/ArrayList;
        //     245: invokevirtual 318	java/util/ArrayList:size	()I
        //     248: istore 16
        //     250: iload 16
        //     252: ifle +27 -> 279
        //     255: new 121	java/util/ArrayList
        //     258: dup
        //     259: aload_0
        //     260: getfield 138	com/android/server/am/ActivityStack:mFinishingActivities	Ljava/util/ArrayList;
        //     263: invokespecial 915	java/util/ArrayList:<init>	(Ljava/util/Collection;)V
        //     266: astore 17
        //     268: aload_0
        //     269: getfield 138	com/android/server/am/ActivityStack:mFinishingActivities	Ljava/util/ArrayList;
        //     272: invokevirtual 382	java/util/ArrayList:clear	()V
        //     275: aload 17
        //     277: astore 5
        //     279: aload_0
        //     280: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     283: getfield 918	com/android/server/am/ActivityManagerService:mCancelledThumbnails	Ljava/util/ArrayList;
        //     286: invokevirtual 318	java/util/ArrayList:size	()I
        //     289: istore 18
        //     291: iload 18
        //     293: ifle +33 -> 326
        //     296: new 121	java/util/ArrayList
        //     299: dup
        //     300: aload_0
        //     301: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     304: getfield 918	com/android/server/am/ActivityManagerService:mCancelledThumbnails	Ljava/util/ArrayList;
        //     307: invokespecial 915	java/util/ArrayList:<init>	(Ljava/util/Collection;)V
        //     310: astore 19
        //     312: aload_0
        //     313: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     316: getfield 918	com/android/server/am/ActivityManagerService:mCancelledThumbnails	Ljava/util/ArrayList;
        //     319: invokevirtual 382	java/util/ArrayList:clear	()V
        //     322: aload 19
        //     324: astore 6
        //     326: aload_0
        //     327: getfield 173	com/android/server/am/ActivityStack:mMainStack	Z
        //     330: ifeq +20 -> 350
        //     333: aload_0
        //     334: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     337: getfield 921	com/android/server/am/ActivityManagerService:mBooting	Z
        //     340: istore 8
        //     342: aload_0
        //     343: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     346: iconst_0
        //     347: putfield 921	com/android/server/am/ActivityManagerService:mBooting	Z
        //     350: aload 11
        //     352: monitorexit
        //     353: aload 7
        //     355: ifnull +11 -> 366
        //     358: aload 7
        //     360: aload_1
        //     361: invokeinterface 924 2 0
        //     366: iconst_0
        //     367: istore 20
        //     369: iload 20
        //     371: iload 15
        //     373: if_icmpge +125 -> 498
        //     376: aload 14
        //     378: iload 20
        //     380: invokevirtual 388	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     383: checkcast 204	com/android/server/am/ActivityRecord
        //     386: astore 28
        //     388: aload_0
        //     389: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     392: astore 29
        //     394: aload 29
        //     396: monitorenter
        //     397: aload 28
        //     399: getfield 207	com/android/server/am/ActivityRecord:finishing	Z
        //     402: ifeq +79 -> 481
        //     405: aload_0
        //     406: aload 28
        //     408: iconst_0
        //     409: invokespecial 211	com/android/server/am/ActivityStack:finishCurrentActivityLocked	(Lcom/android/server/am/ActivityRecord;I)Lcom/android/server/am/ActivityRecord;
        //     412: pop
        //     413: aload 29
        //     415: monitorexit
        //     416: iinc 20 1
        //     419: goto -50 -> 369
        //     422: iload_2
        //     423: ifeq -201 -> 222
        //     426: aload_0
        //     427: iload_2
        //     428: aconst_null
        //     429: ldc2_w 883
        //     432: ldc2_w 883
        //     435: invokevirtual 888	com/android/server/am/ActivityStack:reportActivityLaunchedLocked	(ZLcom/android/server/am/ActivityRecord;JJ)V
        //     438: goto -216 -> 222
        //     441: aload 11
        //     443: monitorexit
        //     444: aload 12
        //     446: athrow
        //     447: iconst_0
        //     448: istore 15
        //     450: goto -209 -> 241
        //     453: astore 32
        //     455: ldc 75
        //     457: ldc_w 926
        //     460: aload 32
        //     462: invokestatic 798	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     465: pop
        //     466: aload_0
        //     467: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     470: aconst_null
        //     471: aload_1
        //     472: aconst_null
        //     473: aconst_null
        //     474: iconst_1
        //     475: invokevirtual 930	com/android/server/am/ActivityManagerService:sendPendingThumbnail	(Lcom/android/server/am/ActivityRecord;Landroid/os/IBinder;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Z)V
        //     478: goto -112 -> 366
        //     481: aload_0
        //     482: aload 28
        //     484: invokespecial 932	com/android/server/am/ActivityStack:stopActivityLocked	(Lcom/android/server/am/ActivityRecord;)V
        //     487: goto -74 -> 413
        //     490: astore 30
        //     492: aload 29
        //     494: monitorexit
        //     495: aload 30
        //     497: athrow
        //     498: iconst_0
        //     499: istore 21
        //     501: iload 21
        //     503: iload 16
        //     505: if_icmpge +54 -> 559
        //     508: aload 5
        //     510: iload 21
        //     512: invokevirtual 388	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     515: checkcast 204	com/android/server/am/ActivityRecord
        //     518: astore 25
        //     520: aload_0
        //     521: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     524: astore 26
        //     526: aload 26
        //     528: monitorenter
        //     529: aload_0
        //     530: aload 25
        //     532: iconst_1
        //     533: iconst_0
        //     534: ldc_w 934
        //     537: invokevirtual 311	com/android/server/am/ActivityStack:destroyActivityLocked	(Lcom/android/server/am/ActivityRecord;ZZLjava/lang/String;)Z
        //     540: istore 10
        //     542: aload 26
        //     544: monitorexit
        //     545: iinc 21 1
        //     548: goto -47 -> 501
        //     551: astore 27
        //     553: aload 26
        //     555: monitorexit
        //     556: aload 27
        //     558: athrow
        //     559: iconst_0
        //     560: istore 22
        //     562: iload 22
        //     564: iload 18
        //     566: if_icmpge +34 -> 600
        //     569: aload 6
        //     571: iload 22
        //     573: invokevirtual 388	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     576: checkcast 204	com/android/server/am/ActivityRecord
        //     579: astore 24
        //     581: aload_0
        //     582: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     585: aload 24
        //     587: aconst_null
        //     588: aconst_null
        //     589: aconst_null
        //     590: iconst_1
        //     591: invokevirtual 930	com/android/server/am/ActivityManagerService:sendPendingThumbnail	(Lcom/android/server/am/ActivityRecord;Landroid/os/IBinder;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Z)V
        //     594: iinc 22 1
        //     597: goto -35 -> 562
        //     600: iload 8
        //     602: ifeq +10 -> 612
        //     605: aload_0
        //     606: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     609: invokevirtual 937	com/android/server/am/ActivityManagerService:finishBooting	()V
        //     612: aload_0
        //     613: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     616: invokevirtual 940	com/android/server/am/ActivityManagerService:trimApplications	()V
        //     619: iload 9
        //     621: ifeq +10 -> 631
        //     624: aload_0
        //     625: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     628: invokevirtual 943	com/android/server/am/ActivityManagerService:enableScreenAfterBoot	()V
        //     631: iload 10
        //     633: ifeq +9 -> 642
        //     636: aload_0
        //     637: aconst_null
        //     638: invokevirtual 221	com/android/server/am/ActivityStack:resumeTopActivityLocked	(Lcom/android/server/am/ActivityRecord;)Z
        //     641: pop
        //     642: aload 4
        //     644: areturn
        //     645: astore 12
        //     647: goto -206 -> 441
        //     650: astore 12
        //     652: goto -211 -> 441
        //     655: astore 12
        //     657: goto -216 -> 441
        //
        // Exception table:
        //     from	to	target	type
        //     358	366	453	java/lang/Exception
        //     397	416	490	finally
        //     481	495	490	finally
        //     529	556	551	finally
        //     268	275	645	finally
        //     312	322	650	finally
        //     30	268	655	finally
        //     279	312	655	finally
        //     326	353	655	finally
        //     426	444	655	finally
    }

    // ERROR //
    final void activityPaused(IBinder paramIBinder, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     4: astore_3
        //     5: aload_3
        //     6: monitorenter
        //     7: aload_0
        //     8: aload_1
        //     9: invokevirtual 550	com/android/server/am/ActivityStack:indexOfTokenLocked	(Landroid/os/IBinder;)I
        //     12: istore 5
        //     14: iload 5
        //     16: iflt +49 -> 65
        //     19: aload_0
        //     20: getfield 124	com/android/server/am/ActivityStack:mHistory	Ljava/util/ArrayList;
        //     23: iload 5
        //     25: invokevirtual 388	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     28: checkcast 204	com/android/server/am/ActivityRecord
        //     31: astore 6
        //     33: aload_0
        //     34: getfield 167	com/android/server/am/ActivityStack:mHandler	Landroid/os/Handler;
        //     37: bipush 101
        //     39: aload 6
        //     41: invokevirtual 588	android/os/Handler:removeMessages	(ILjava/lang/Object;)V
        //     44: aload_0
        //     45: getfield 144	com/android/server/am/ActivityStack:mPausingActivity	Lcom/android/server/am/ActivityRecord;
        //     48: aload 6
        //     50: if_acmpne +18 -> 68
        //     53: aload 6
        //     55: getstatic 591	com/android/server/am/ActivityStack$ActivityState:PAUSED	Lcom/android/server/am/ActivityStack$ActivityState;
        //     58: putfield 470	com/android/server/am/ActivityRecord:state	Lcom/android/server/am/ActivityStack$ActivityState;
        //     61: aload_0
        //     62: invokespecial 946	com/android/server/am/ActivityStack:completePauseLocked	()V
        //     65: aload_3
        //     66: monitorexit
        //     67: return
        //     68: iconst_3
        //     69: anewarray 4	java/lang/Object
        //     72: astore 7
        //     74: aload 7
        //     76: iconst_0
        //     77: aload 6
        //     79: invokestatic 494	java/lang/System:identityHashCode	(Ljava/lang/Object;)I
        //     82: invokestatic 500	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     85: aastore
        //     86: aload 7
        //     88: iconst_1
        //     89: aload 6
        //     91: getfield 503	com/android/server/am/ActivityRecord:shortComponentName	Ljava/lang/String;
        //     94: aastore
        //     95: aload_0
        //     96: getfield 144	com/android/server/am/ActivityStack:mPausingActivity	Lcom/android/server/am/ActivityRecord;
        //     99: ifnull +37 -> 136
        //     102: aload_0
        //     103: getfield 144	com/android/server/am/ActivityStack:mPausingActivity	Lcom/android/server/am/ActivityRecord;
        //     106: getfield 503	com/android/server/am/ActivityRecord:shortComponentName	Ljava/lang/String;
        //     109: astore 8
        //     111: aload 7
        //     113: iconst_2
        //     114: aload 8
        //     116: aastore
        //     117: sipush 30012
        //     120: aload 7
        //     122: invokestatic 522	android/util/EventLog:writeEvent	(I[Ljava/lang/Object;)I
        //     125: pop
        //     126: goto -61 -> 65
        //     129: astore 4
        //     131: aload_3
        //     132: monitorexit
        //     133: aload 4
        //     135: athrow
        //     136: ldc_w 948
        //     139: astore 8
        //     141: goto -30 -> 111
        //
        // Exception table:
        //     from	to	target	type
        //     7	133	129	finally
        //     136	141	129	finally
    }

    void activitySleptLocked(ActivityRecord paramActivityRecord)
    {
        this.mGoingToSleepActivities.remove(paramActivityRecord);
        checkReadyForSleepLocked();
    }

    final void activityStoppedLocked(ActivityRecord paramActivityRecord, Bundle paramBundle, Bitmap paramBitmap, CharSequence paramCharSequence)
    {
        if (paramActivityRecord.state != ActivityState.STOPPING)
        {
            Slog.i("ActivityManager", "Activity reported stop, but no longer stopping: " + paramActivityRecord);
            this.mHandler.removeMessages(108, paramActivityRecord);
        }
        label249: 
        while (true)
        {
            return;
            if (paramBundle != null)
            {
                paramActivityRecord.icicle = paramBundle;
                paramActivityRecord.haveState = true;
                paramActivityRecord.updateThumbnail(paramBitmap, paramCharSequence);
            }
            if (!paramActivityRecord.stopped)
            {
                this.mHandler.removeMessages(108, paramActivityRecord);
                paramActivityRecord.stopped = true;
                paramActivityRecord.state = ActivityState.STOPPED;
                if (paramActivityRecord.finishing)
                {
                    paramActivityRecord.clearOptionsLocked();
                }
                else if (paramActivityRecord.configDestroy)
                {
                    destroyActivityLocked(paramActivityRecord, true, false, "stop-config");
                    resumeTopActivityLocked(null);
                }
                else
                {
                    ProcessRecord localProcessRecord = null;
                    if (this.mResumedActivity != null)
                        localProcessRecord = this.mResumedActivity.app;
                    while (true)
                    {
                        if ((paramActivityRecord.app == null) || (localProcessRecord == null) || (paramActivityRecord.app == localProcessRecord) || (paramActivityRecord.lastVisibleTime <= this.mService.mPreviousProcessVisibleTime) || (paramActivityRecord.app == this.mService.mHomeProcess))
                            break label249;
                        this.mService.mPreviousProcess = paramActivityRecord.app;
                        this.mService.mPreviousProcessVisibleTime = paramActivityRecord.lastVisibleTime;
                        break;
                        if (this.mPausingActivity != null)
                            localProcessRecord = this.mPausingActivity.app;
                    }
                }
            }
        }
    }

    void awakeFromSleepingLocked()
    {
        this.mHandler.removeMessages(100);
        this.mSleepTimeout = false;
        if (this.mGoingToSleep.isHeld())
            this.mGoingToSleep.release();
        for (int i = -1 + this.mHistory.size(); i >= 0; i--)
            ((ActivityRecord)this.mHistory.get(i)).setSleeping(false);
        this.mGoingToSleepActivities.clear();
    }

    void checkReadyForSleepLocked()
    {
        if (!this.mService.isSleeping());
        while (true)
        {
            return;
            if (!this.mSleepTimeout)
            {
                if (this.mResumedActivity != null)
                    startPausingLocked(false, true);
                else if (this.mPausingActivity == null)
                    if (this.mStoppingActivities.size() > 0)
                    {
                        scheduleIdleLocked();
                    }
                    else
                    {
                        ensureActivitiesVisibleLocked(null, 0);
                        for (int i = -1 + this.mHistory.size(); i >= 0; i--)
                        {
                            ActivityRecord localActivityRecord = (ActivityRecord)this.mHistory.get(i);
                            if ((localActivityRecord.state == ActivityState.STOPPING) || (localActivityRecord.state == ActivityState.STOPPED))
                                localActivityRecord.setSleeping(true);
                        }
                        if (this.mGoingToSleepActivities.size() > 0);
                    }
            }
            else
            {
                this.mHandler.removeMessages(100);
                if (this.mGoingToSleep.isHeld())
                    this.mGoingToSleep.release();
                if (this.mService.mShuttingDown)
                    this.mService.notifyAll();
            }
        }
    }

    final void cleanUpActivityLocked(ActivityRecord paramActivityRecord, boolean paramBoolean1, boolean paramBoolean2)
    {
        if (this.mResumedActivity == paramActivityRecord)
            this.mResumedActivity = null;
        if (this.mService.mFocusedActivity == paramActivityRecord)
            this.mService.mFocusedActivity = null;
        paramActivityRecord.configDestroy = false;
        paramActivityRecord.frozenBeforeDestroy = false;
        if (paramBoolean2)
            paramActivityRecord.state = ActivityState.DESTROYED;
        this.mFinishingActivities.remove(paramActivityRecord);
        this.mWaitingVisibleActivities.remove(paramActivityRecord);
        if ((paramActivityRecord.finishing) && (paramActivityRecord.pendingResults != null))
        {
            Iterator localIterator = paramActivityRecord.pendingResults.iterator();
            while (localIterator.hasNext())
            {
                PendingIntentRecord localPendingIntentRecord = (PendingIntentRecord)((WeakReference)localIterator.next()).get();
                if (localPendingIntentRecord != null)
                    this.mService.cancelIntentSenderLocked(localPendingIntentRecord, false);
            }
            paramActivityRecord.pendingResults = null;
        }
        if (paramBoolean1)
            cleanUpActivityServicesLocked(paramActivityRecord);
        if (this.mService.mPendingThumbnails.size() > 0)
            this.mService.mCancelledThumbnails.add(paramActivityRecord);
        removeTimeoutsForActivityLocked(paramActivityRecord);
    }

    final void cleanUpActivityServicesLocked(ActivityRecord paramActivityRecord)
    {
        if (paramActivityRecord.connections != null)
        {
            Iterator localIterator = paramActivityRecord.connections.iterator();
            while (localIterator.hasNext())
            {
                ConnectionRecord localConnectionRecord = (ConnectionRecord)localIterator.next();
                this.mService.removeConnectionLocked(localConnectionRecord, null, paramActivityRecord);
            }
            paramActivityRecord.connections = null;
        }
    }

    final void destroyActivitiesLocked(ProcessRecord paramProcessRecord, boolean paramBoolean, String paramString)
    {
        int i = 0;
        int j = 0;
        int k = -1 + this.mHistory.size();
        if (k >= 0)
        {
            ActivityRecord localActivityRecord = (ActivityRecord)this.mHistory.get(k);
            if (localActivityRecord.finishing);
            while (true)
            {
                k--;
                break;
                if (localActivityRecord.fullscreen)
                    i = 1;
                if (((paramProcessRecord == null) || (localActivityRecord.app == paramProcessRecord)) && (i != 0) && (localActivityRecord.app != null) && (localActivityRecord != this.mResumedActivity) && (localActivityRecord != this.mPausingActivity) && (localActivityRecord.haveState) && (!localActivityRecord.visible) && (localActivityRecord.stopped) && (localActivityRecord.state != ActivityState.DESTROYING) && (localActivityRecord.state != ActivityState.DESTROYED) && (destroyActivityLocked(localActivityRecord, true, paramBoolean, paramString)))
                    j = 1;
            }
        }
        if (j != 0)
            resumeTopActivityLocked(null);
    }

    final boolean destroyActivityLocked(ActivityRecord paramActivityRecord, boolean paramBoolean1, boolean paramBoolean2, String paramString)
    {
        Object[] arrayOfObject = new Object[4];
        arrayOfObject[0] = Integer.valueOf(System.identityHashCode(paramActivityRecord));
        arrayOfObject[1] = Integer.valueOf(paramActivityRecord.task.taskId);
        arrayOfObject[2] = paramActivityRecord.shortComponentName;
        arrayOfObject[3] = paramString;
        EventLog.writeEvent(30018, arrayOfObject);
        boolean bool = false;
        cleanUpActivityLocked(paramActivityRecord, false, false);
        int i;
        int j;
        if (paramActivityRecord.app != null)
        {
            i = 1;
            if (i == 0)
                break label369;
            if (paramBoolean1)
            {
                int k = paramActivityRecord.app.activities.indexOf(paramActivityRecord);
                if (k >= 0)
                    paramActivityRecord.app.activities.remove(k);
                if ((this.mService.mHeavyWeightProcess == paramActivityRecord.app) && (paramActivityRecord.app.activities.size() <= 0))
                {
                    this.mService.mHeavyWeightProcess = null;
                    this.mService.mHandler.sendEmptyMessage(25);
                }
                if (paramActivityRecord.app.activities.size() == 0)
                    this.mService.updateLruProcessLocked(paramActivityRecord.app, paramBoolean2, false);
            }
            j = 0;
        }
        while (true)
        {
            try
            {
                paramActivityRecord.app.thread.scheduleDestroyActivity(paramActivityRecord.appToken, paramActivityRecord.finishing, paramActivityRecord.configChangeFlags);
                paramActivityRecord.app = null;
                paramActivityRecord.nowVisible = false;
                if ((paramActivityRecord.finishing) && (j == 0))
                {
                    paramActivityRecord.state = ActivityState.DESTROYING;
                    Message localMessage = this.mHandler.obtainMessage(105);
                    localMessage.obj = paramActivityRecord;
                    this.mHandler.sendMessageDelayed(localMessage, 10000L);
                    paramActivityRecord.configChangeFlags = 0;
                    if ((!this.mLRUActivities.remove(paramActivityRecord)) && (i != 0))
                        Slog.w("ActivityManager", "Activity " + paramActivityRecord + " being finished, but not in LRU list");
                    return bool;
                    i = 0;
                }
            }
            catch (Exception localException)
            {
                if (!paramActivityRecord.finishing)
                    continue;
                removeActivityFromHistoryLocked(paramActivityRecord);
                bool = true;
                j = 1;
                continue;
                paramActivityRecord.state = ActivityState.DESTROYED;
                continue;
            }
            label369: if (paramActivityRecord.finishing)
            {
                removeActivityFromHistoryLocked(paramActivityRecord);
                bool = true;
            }
            else
            {
                paramActivityRecord.state = ActivityState.DESTROYED;
            }
        }
    }

    public void dismissKeyguardOnNextActivityLocked()
    {
        this.mDismissKeyguardOnNextActivity = true;
    }

    final void ensureActivitiesVisibleLocked(ActivityRecord paramActivityRecord, int paramInt)
    {
        ActivityRecord localActivityRecord = topRunningActivityLocked(null);
        if (localActivityRecord != null)
            ensureActivitiesVisibleLocked(localActivityRecord, paramActivityRecord, null, paramInt);
    }

    final void ensureActivitiesVisibleLocked(ActivityRecord paramActivityRecord1, ActivityRecord paramActivityRecord2, String paramString, int paramInt)
    {
        for (int i = -1 + this.mHistory.size(); this.mHistory.get(i) != paramActivityRecord1; i--);
        int j = 0;
        ActivityRecord localActivityRecord2;
        int k;
        if (i >= 0)
        {
            localActivityRecord2 = (ActivityRecord)this.mHistory.get(i);
            if (localActivityRecord2.finishing);
            label196: 
            do
            {
                i--;
                break;
                if ((paramString != null) && (!paramString.equals(localActivityRecord2.processName)))
                    break label348;
                k = 1;
                if ((localActivityRecord2 != paramActivityRecord2) && (k != 0))
                    ensureActivityConfigurationLocked(localActivityRecord2, 0);
                if ((localActivityRecord2.app != null) && (localActivityRecord2.app.thread != null))
                    break label354;
                if ((paramString == null) || (paramString.equals(localActivityRecord2.processName)))
                {
                    if (localActivityRecord2 != paramActivityRecord2)
                        localActivityRecord2.startFreezingScreenLocked(localActivityRecord2.app, paramInt);
                    if (!localActivityRecord2.visible)
                        this.mService.mWindowManager.setAppVisibility(localActivityRecord2.appToken, true);
                    if (localActivityRecord2 != paramActivityRecord2)
                        startSpecificActivityLocked(localActivityRecord2, false, false);
                }
                paramInt |= localActivityRecord2.configChangeFlags;
            }
            while (!localActivityRecord2.fullscreen);
            j = 1;
            i--;
        }
        label220: if (i >= 0)
        {
            ActivityRecord localActivityRecord1 = (ActivityRecord)this.mHistory.get(i);
            if (!localActivityRecord1.finishing)
            {
                if (j == 0)
                    break label537;
                if (localActivityRecord1.visible)
                    localActivityRecord1.visible = false;
            }
            while (true)
            {
                try
                {
                    while (true)
                    {
                        this.mService.mWindowManager.setAppVisibility(localActivityRecord1.appToken, false);
                        if (((localActivityRecord1.state == ActivityState.STOPPING) || (localActivityRecord1.state == ActivityState.STOPPED)) && (localActivityRecord1.app != null) && (localActivityRecord1.app.thread != null))
                            localActivityRecord1.app.thread.scheduleWindowVisibility(localActivityRecord1.appToken, false);
                        i--;
                        break label220;
                        k = 0;
                        break;
                        label354: if (localActivityRecord2.visible)
                        {
                            localActivityRecord2.stopFreezingScreenLocked(false);
                            break label196;
                        }
                        if (paramString != null)
                            break label196;
                        localActivityRecord2.visible = true;
                        if ((localActivityRecord2.state == ActivityState.RESUMED) || (localActivityRecord2 == paramActivityRecord2))
                            break label196;
                        try
                        {
                            this.mService.mWindowManager.setAppVisibility(localActivityRecord2.appToken, true);
                            localActivityRecord2.sleeping = false;
                            localActivityRecord2.app.pendingUiClean = true;
                            localActivityRecord2.app.thread.scheduleWindowVisibility(localActivityRecord2.appToken, true);
                            localActivityRecord2.stopFreezingScreenLocked(false);
                        }
                        catch (Exception localException2)
                        {
                            Slog.w("ActivityManager", "Exception thrown making visibile: " + localActivityRecord2.intent.getComponent(), localException2);
                        }
                    }
                }
                catch (Exception localException1)
                {
                    Slog.w("ActivityManager", "Exception thrown making hidden: " + localActivityRecord1.intent.getComponent(), localException1);
                    continue;
                }
                if (localActivityRecord1.fullscreen)
                    j = 1;
            }
        }
        label348:
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    final boolean ensureActivityConfigurationLocked(ActivityRecord paramActivityRecord, int paramInt)
    {
        boolean bool = true;
        if (this.mConfigWillChange);
        while (true)
        {
            return bool;
            Configuration localConfiguration1 = this.mService.mConfiguration;
            if ((paramActivityRecord.configuration == localConfiguration1) && (!paramActivityRecord.forceNewConfig))
                continue;
            if (paramActivityRecord.finishing)
            {
                paramActivityRecord.stopFreezingScreenLocked(false);
                continue;
            }
            Configuration localConfiguration2 = paramActivityRecord.configuration;
            paramActivityRecord.configuration = localConfiguration1;
            int i = localConfiguration2.diff(localConfiguration1);
            if ((i == 0) && (!paramActivityRecord.forceNewConfig))
                continue;
            if ((paramActivityRecord.app == null) || (paramActivityRecord.app.thread == null))
            {
                paramActivityRecord.stopFreezingScreenLocked(false);
                paramActivityRecord.forceNewConfig = false;
                continue;
            }
            if (((i & (0xFFFFFFFF ^ paramActivityRecord.info.getRealConfigChanged())) != 0) || (paramActivityRecord.forceNewConfig))
            {
                if (MiuiThemeHelper.needRestartActivity(paramActivityRecord.info.packageName, i, localConfiguration1))
                    continue;
                paramActivityRecord.configChangeFlags = (i | paramActivityRecord.configChangeFlags);
                paramActivityRecord.startFreezingScreenLocked(paramActivityRecord.app, paramInt);
                paramActivityRecord.forceNewConfig = false;
                if ((paramActivityRecord.app == null) || (paramActivityRecord.app.thread == null))
                    destroyActivityLocked(paramActivityRecord, bool, false, "config");
                while (true)
                {
                    bool = false;
                    break;
                    if (paramActivityRecord.state == ActivityState.PAUSING)
                    {
                        paramActivityRecord.configDestroy = bool;
                        break;
                    }
                    if (paramActivityRecord.state == ActivityState.RESUMED)
                    {
                        relaunchActivityLocked(paramActivityRecord, paramActivityRecord.configChangeFlags, bool);
                        paramActivityRecord.configChangeFlags = 0;
                    }
                    else
                    {
                        relaunchActivityLocked(paramActivityRecord, paramActivityRecord.configChangeFlags, false);
                        paramActivityRecord.configChangeFlags = 0;
                    }
                }
            }
            if ((paramActivityRecord.app != null) && (paramActivityRecord.app.thread != null));
            try
            {
                paramActivityRecord.app.thread.scheduleActivityConfigurationChanged(paramActivityRecord.appToken);
                label311: paramActivityRecord.stopFreezingScreenLocked(false);
            }
            catch (RemoteException localRemoteException)
            {
                break label311;
            }
        }
    }

    final boolean finishActivityAffinityLocked(IBinder paramIBinder)
    {
        boolean bool = false;
        int i = indexOfTokenLocked(paramIBinder);
        if (i < 0)
            return bool;
        ActivityRecord localActivityRecord1 = (ActivityRecord)this.mHistory.get(i);
        while (true)
        {
            ActivityRecord localActivityRecord2;
            if (i >= 0)
            {
                localActivityRecord2 = (ActivityRecord)this.mHistory.get(i);
                if (localActivityRecord2.task == localActivityRecord1.task)
                    break label62;
            }
            label62: 
            while (((localActivityRecord2.taskAffinity == null) && (localActivityRecord1.taskAffinity != null)) || ((localActivityRecord2.taskAffinity != null) && (!localActivityRecord2.taskAffinity.equals(localActivityRecord1.taskAffinity))))
            {
                bool = true;
                break;
            }
            finishActivityLocked(localActivityRecord2, i, 0, null, "request-affinity");
            i--;
        }
    }

    final boolean finishActivityLocked(ActivityRecord paramActivityRecord, int paramInt1, int paramInt2, Intent paramIntent, String paramString)
    {
        return finishActivityLocked(paramActivityRecord, paramInt1, paramInt2, paramIntent, paramString, false);
    }

    final boolean finishActivityLocked(ActivityRecord paramActivityRecord, int paramInt1, int paramInt2, Intent paramIntent, String paramString, boolean paramBoolean)
    {
        int i = 1;
        boolean bool = false;
        if (paramActivityRecord.finishing)
            Slog.w("ActivityManager", "Duplicate finish request for " + paramActivityRecord);
        label321: 
        do
        {
            return bool;
            paramActivityRecord.makeFinishing();
            Object[] arrayOfObject = new Object[4];
            arrayOfObject[bool] = Integer.valueOf(System.identityHashCode(paramActivityRecord));
            arrayOfObject[i] = Integer.valueOf(paramActivityRecord.task.taskId);
            arrayOfObject[2] = paramActivityRecord.shortComponentName;
            arrayOfObject[3] = paramString;
            EventLog.writeEvent(30001, arrayOfObject);
            if (paramInt1 < -1 + this.mHistory.size())
            {
                ActivityRecord localActivityRecord = (ActivityRecord)this.mHistory.get(paramInt1 + 1);
                if (localActivityRecord.task == paramActivityRecord.task)
                {
                    if (paramActivityRecord.frontOfTask)
                        localActivityRecord.frontOfTask = i;
                    if ((0x80000 & paramActivityRecord.intent.getFlags()) != 0)
                        localActivityRecord.intent.addFlags(524288);
                }
            }
            paramActivityRecord.pauseKeyDispatchingLocked();
            if ((this.mMainStack) && (this.mService.mFocusedActivity == paramActivityRecord))
                this.mService.setFocusedActivityLocked(topRunningActivityLocked(null));
            finishActivityResultsLocked(paramActivityRecord, paramInt2, paramIntent);
            if (this.mService.mPendingThumbnails.size() > 0)
                this.mService.mCancelledThumbnails.add(paramActivityRecord);
            if (paramBoolean)
            {
                if (finishCurrentActivityLocked(paramActivityRecord, paramInt1, 0) == null);
                for (int m = i; ; m = 0)
                {
                    bool = m;
                    break;
                }
            }
            if (this.mResumedActivity == paramActivityRecord)
            {
                int j;
                WindowManagerService localWindowManagerService;
                if ((paramInt1 <= 0) || (((ActivityRecord)this.mHistory.get(paramInt1 - 1)).task != paramActivityRecord.task))
                {
                    j = i;
                    localWindowManagerService = this.mService.mWindowManager;
                    if (j == 0)
                        break label385;
                }
                for (int k = 8201; ; k = 8199)
                {
                    localWindowManagerService.prepareAppTransition(k, false);
                    this.mService.mWindowManager.setAppVisibility(paramActivityRecord.appToken, false);
                    if (this.mPausingActivity != null)
                        break;
                    startPausingLocked(false, false);
                    break;
                    j = 0;
                    break label321;
                }
            }
        }
        while (paramActivityRecord.state == ActivityState.PAUSING);
        label385: if (finishCurrentActivityLocked(paramActivityRecord, paramInt1, i) == null);
        while (true)
        {
            bool = i;
            break;
            i = 0;
        }
    }

    final void finishActivityResultsLocked(ActivityRecord paramActivityRecord, int paramInt, Intent paramIntent)
    {
        ActivityRecord localActivityRecord = paramActivityRecord.resultTo;
        if (localActivityRecord != null)
        {
            if (paramActivityRecord.info.applicationInfo.uid > 0)
                this.mService.grantUriPermissionFromIntentLocked(paramActivityRecord.info.applicationInfo.uid, localActivityRecord.packageName, paramIntent, localActivityRecord.getUriPermissionsLocked());
            localActivityRecord.addResultLocked(paramActivityRecord, paramActivityRecord.resultWho, paramActivityRecord.requestCode, paramInt, paramIntent);
            paramActivityRecord.resultTo = null;
        }
        paramActivityRecord.results = null;
        paramActivityRecord.pendingResults = null;
        paramActivityRecord.newIntents = null;
        paramActivityRecord.icicle = null;
    }

    final void finishSubActivityLocked(IBinder paramIBinder, String paramString, int paramInt)
    {
        ActivityRecord localActivityRecord1 = isInStackLocked(paramIBinder);
        if (localActivityRecord1 == null);
        while (true)
        {
            return;
            for (int i = -1 + this.mHistory.size(); i >= 0; i--)
            {
                ActivityRecord localActivityRecord2 = (ActivityRecord)this.mHistory.get(i);
                if ((localActivityRecord2.resultTo == localActivityRecord1) && (localActivityRecord2.requestCode == paramInt) && (((localActivityRecord2.resultWho == null) && (paramString == null)) || ((localActivityRecord2.resultWho != null) && (localActivityRecord2.resultWho.equals(paramString)))))
                    finishActivityLocked(localActivityRecord2, i, 0, null, "request-sub");
            }
        }
    }

    public TaskAccessInfo getTaskAccessInfoLocked(int paramInt, boolean paramBoolean)
    {
        ActivityRecord localActivityRecord1 = this.mResumedActivity;
        final TaskAccessInfo localTaskAccessInfo = new TaskAccessInfo();
        int i = this.mHistory.size();
        int j = 0;
        ThumbnailHolder localThumbnailHolder = null;
        while (true)
        {
            if (j < i)
            {
                ActivityRecord localActivityRecord3 = (ActivityRecord)this.mHistory.get(j);
                if ((!localActivityRecord3.finishing) && (localActivityRecord3.task.taskId == paramInt))
                    localThumbnailHolder = localActivityRecord3.thumbHolder;
            }
            else
            {
                if (j < i)
                    break;
                return localTaskAccessInfo;
            }
            j++;
        }
        localTaskAccessInfo.root = ((ActivityRecord)this.mHistory.get(j));
        localTaskAccessInfo.rootIndex = j;
        ArrayList localArrayList = new ArrayList();
        localTaskAccessInfo.subtasks = localArrayList;
        Object localObject = null;
        while (true)
        {
            ActivityRecord localActivityRecord2;
            if (j < i)
            {
                localActivityRecord2 = (ActivityRecord)this.mHistory.get(j);
                j++;
                if (!localActivityRecord2.finishing)
                    if (localActivityRecord2.task.taskId == paramInt);
            }
            else
            {
                if ((localObject != null) && (localArrayList.size() > 0) && (localActivityRecord1 == localObject))
                    ((TaskAccessInfo.SubTask)localArrayList.get(-1 + localArrayList.size())).thumbnail = ((ActivityRecord)localObject).stack.screenshotActivities((ActivityRecord)localObject);
                if (localTaskAccessInfo.numSubThumbbails <= 0)
                    break;
                localTaskAccessInfo.retriever = new IThumbnailRetriever.Stub()
                {
                    public Bitmap getThumbnail(int paramAnonymousInt)
                    {
                        if ((paramAnonymousInt < 0) || (paramAnonymousInt >= localTaskAccessInfo.subtasks.size()));
                        for (Bitmap localBitmap = null; ; localBitmap = ((TaskAccessInfo.SubTask)localTaskAccessInfo.subtasks.get(paramAnonymousInt)).thumbnail)
                            return localBitmap;
                    }
                };
                break;
                localObject = localActivityRecord2;
                if ((localActivityRecord2.thumbHolder != localThumbnailHolder) && (localThumbnailHolder != null))
                {
                    localTaskAccessInfo.numSubThumbbails = (1 + localTaskAccessInfo.numSubThumbbails);
                    localThumbnailHolder = localActivityRecord2.thumbHolder;
                    TaskAccessInfo.SubTask localSubTask = new TaskAccessInfo.SubTask();
                    localSubTask.thumbnail = localThumbnailHolder.lastThumbnail;
                    localSubTask.activity = localActivityRecord2;
                    localSubTask.index = (j - 1);
                    localArrayList.add(localSubTask);
                }
            }
        }
    }

    public ActivityManager.TaskThumbnails getTaskThumbnailsLocked(TaskRecord paramTaskRecord)
    {
        TaskAccessInfo localTaskAccessInfo = getTaskAccessInfoLocked(paramTaskRecord.taskId, true);
        ActivityRecord localActivityRecord = this.mResumedActivity;
        if ((localActivityRecord != null) && (localActivityRecord.thumbHolder == paramTaskRecord));
        for (localTaskAccessInfo.mainThumbnail = localActivityRecord.stack.screenshotActivities(localActivityRecord); ; localTaskAccessInfo.mainThumbnail = paramTaskRecord.lastThumbnail)
            return localTaskAccessInfo;
    }

    final int indexOfActivityLocked(ActivityRecord paramActivityRecord)
    {
        return this.mHistory.indexOf(paramActivityRecord);
    }

    final int indexOfTokenLocked(IBinder paramIBinder)
    {
        return this.mHistory.indexOf(ActivityRecord.forToken(paramIBinder));
    }

    final ActivityRecord isInStackLocked(IBinder paramIBinder)
    {
        ActivityRecord localActivityRecord = ActivityRecord.forToken(paramIBinder);
        if (this.mHistory.contains(localActivityRecord));
        while (true)
        {
            return localActivityRecord;
            localActivityRecord = null;
        }
    }

    final void moveHomeToFrontFromLaunchLocked(int paramInt)
    {
        if ((paramInt & 0x10004000) == 268451840)
            moveHomeToFrontLocked();
    }

    final void moveHomeToFrontLocked()
    {
        TaskRecord localTaskRecord = null;
        for (int i = -1 + this.mHistory.size(); ; i--)
            if (i >= 0)
            {
                ActivityRecord localActivityRecord = (ActivityRecord)this.mHistory.get(i);
                if (localActivityRecord.isHomeActivity)
                    localTaskRecord = localActivityRecord.task;
            }
            else
            {
                if (localTaskRecord != null)
                    moveTaskToFrontLocked(localTaskRecord, null, null);
                return;
            }
    }

    final boolean moveTaskToBackLocked(int paramInt, ActivityRecord paramActivityRecord)
    {
        boolean bool1 = false;
        Slog.i("ActivityManager", "moveTaskToBack: " + paramInt);
        if ((this.mMainStack) && (this.mService.mController != null))
        {
            ActivityRecord localActivityRecord3 = topRunningActivityLocked(null, paramInt);
            if (localActivityRecord3 == null)
                localActivityRecord3 = topRunningActivityLocked(null, 0);
            if (localActivityRecord3 != null)
            {
                int m = 1;
                try
                {
                    boolean bool2 = this.mService.mController.activityResuming(localActivityRecord3.packageName);
                    m = bool2;
                    if (m == 0)
                        return bool1;
                }
                catch (RemoteException localRemoteException)
                {
                    while (true)
                        this.mService.mController = null;
                }
            }
        }
        ArrayList localArrayList = new ArrayList();
        int i = this.mHistory.size();
        int j = 0;
        for (int k = 0; k < i; k++)
        {
            ActivityRecord localActivityRecord2 = (ActivityRecord)this.mHistory.get(k);
            if (localActivityRecord2.task.taskId == paramInt)
            {
                this.mHistory.remove(k);
                this.mHistory.add(j, localActivityRecord2);
                localArrayList.add(localActivityRecord2.appToken);
                j++;
            }
        }
        if ((paramActivityRecord != null) && ((0x10000 & paramActivityRecord.intent.getFlags()) != 0))
        {
            this.mService.mWindowManager.prepareAppTransition(0, false);
            ActivityRecord localActivityRecord1 = topRunningActivityLocked(null);
            if (localActivityRecord1 != null)
                this.mNoAnimActivities.add(localActivityRecord1);
        }
        while (true)
        {
            this.mService.mWindowManager.moveAppTokensToBottom(localArrayList);
            finishTaskMoveLocked(paramInt);
            bool1 = true;
            break;
            this.mService.mWindowManager.prepareAppTransition(8203, false);
        }
    }

    final void moveTaskToFrontLocked(TaskRecord paramTaskRecord, ActivityRecord paramActivityRecord, Bundle paramBundle)
    {
        int i = paramTaskRecord.taskId;
        int j = -1 + this.mHistory.size();
        if ((j < 0) || (((ActivityRecord)this.mHistory.get(j)).task.taskId == i))
        {
            if ((paramActivityRecord != null) && ((0x10000 & paramActivityRecord.intent.getFlags()) != 0))
                ActivityOptions.abort(paramBundle);
            while (true)
            {
                return;
                updateTransitLocked(4106, paramBundle);
            }
        }
        ArrayList localArrayList = new ArrayList();
        int k = -1 + this.mHistory.size();
        for (int m = k; m >= 0; m--)
        {
            ActivityRecord localActivityRecord2 = (ActivityRecord)this.mHistory.get(m);
            if (localActivityRecord2.task.taskId == i)
            {
                this.mHistory.remove(m);
                this.mHistory.add(k, localActivityRecord2);
                localArrayList.add(0, localActivityRecord2.appToken);
                k--;
            }
        }
        if ((paramActivityRecord != null) && ((0x10000 & paramActivityRecord.intent.getFlags()) != 0))
        {
            this.mService.mWindowManager.prepareAppTransition(0, false);
            ActivityRecord localActivityRecord1 = topRunningActivityLocked(null);
            if (localActivityRecord1 != null)
                this.mNoAnimActivities.add(localActivityRecord1);
            ActivityOptions.abort(paramBundle);
        }
        while (true)
        {
            this.mService.mWindowManager.moveAppTokensToTop(localArrayList);
            finishTaskMoveLocked(i);
            EventLog.writeEvent(30002, i);
            break;
            updateTransitLocked(4106, paramBundle);
        }
    }

    final ArrayList<ActivityRecord> processStoppingActivitiesLocked(boolean paramBoolean)
    {
        int i = this.mStoppingActivities.size();
        if (i <= 0)
        {
            localObject = null;
            return localObject;
        }
        Object localObject = null;
        if ((this.mResumedActivity != null) && (this.mResumedActivity.nowVisible) && (!this.mResumedActivity.waitingVisible));
        for (int j = 1; ; j = 0)
        {
            for (int k = 0; k < i; k++)
            {
                ActivityRecord localActivityRecord = (ActivityRecord)this.mStoppingActivities.get(k);
                if ((localActivityRecord.waitingVisible) && (j != 0))
                {
                    this.mWaitingVisibleActivities.remove(localActivityRecord);
                    localActivityRecord.waitingVisible = false;
                    if (localActivityRecord.finishing)
                        this.mService.mWindowManager.setAppVisibility(localActivityRecord.appToken, false);
                }
                if (((!localActivityRecord.waitingVisible) || (this.mService.isSleeping())) && (paramBoolean))
                {
                    if (localObject == null)
                        localObject = new ArrayList();
                    ((ArrayList)localObject).add(localActivityRecord);
                    this.mStoppingActivities.remove(k);
                    i--;
                    k--;
                }
            }
            break;
        }
    }

    final boolean realStartActivityLocked(ActivityRecord paramActivityRecord, ProcessRecord paramProcessRecord, boolean paramBoolean1, boolean paramBoolean2)
        throws RemoteException
    {
        paramActivityRecord.startFreezingScreenLocked(paramProcessRecord, 0);
        this.mService.mWindowManager.setAppVisibility(paramActivityRecord.appToken, true);
        paramActivityRecord.startLaunchTickingLocked();
        WindowManagerService localWindowManagerService;
        Configuration localConfiguration2;
        if (paramBoolean2)
        {
            localWindowManagerService = this.mService.mWindowManager;
            localConfiguration2 = this.mService.mConfiguration;
            if (!paramActivityRecord.mayFreezeScreenLocked(paramProcessRecord))
                break label229;
        }
        boolean bool1;
        label229: for (IApplicationToken.Stub localStub2 = paramActivityRecord.appToken; ; localStub2 = null)
        {
            Configuration localConfiguration3 = localWindowManagerService.updateOrientationFromAppTokens(localConfiguration2, localStub2);
            this.mService.updateConfigurationLocked(localConfiguration3, paramActivityRecord, false, false);
            paramActivityRecord.app = paramProcessRecord;
            paramProcessRecord.waitingToKill = null;
            if (paramProcessRecord.activities.indexOf(paramActivityRecord) < 0)
                paramProcessRecord.activities.add(paramActivityRecord);
            this.mService.updateLruProcessLocked(paramProcessRecord, true, true);
            try
            {
                if (paramProcessRecord.thread != null)
                    break;
                throw new RemoteException();
            }
            catch (RemoteException localRemoteException)
            {
                if (!paramActivityRecord.launchFailed)
                    break label883;
            }
            Slog.e("ActivityManager", "Second failure launching " + paramActivityRecord.intent.getComponent().flattenToShortString() + ", giving up", localRemoteException);
            this.mService.appDiedLocked(paramProcessRecord, paramProcessRecord.pid, paramProcessRecord.thread);
            requestFinishActivityLocked(paramActivityRecord.appToken, 0, null, "2nd-crash");
            bool1 = false;
            return bool1;
        }
        ArrayList localArrayList1 = null;
        ArrayList localArrayList2 = null;
        if (paramBoolean1)
        {
            localArrayList1 = paramActivityRecord.results;
            localArrayList2 = paramActivityRecord.newIntents;
        }
        if (paramBoolean1)
        {
            Object[] arrayOfObject = new Object[3];
            arrayOfObject[0] = Integer.valueOf(System.identityHashCode(paramActivityRecord));
            arrayOfObject[1] = Integer.valueOf(paramActivityRecord.task.taskId);
            arrayOfObject[2] = paramActivityRecord.shortComponentName;
            EventLog.writeEvent(30006, arrayOfObject);
        }
        if (paramActivityRecord.isHomeActivity)
            this.mService.mHomeProcess = paramProcessRecord;
        this.mService.ensurePackageDexOpt(paramActivityRecord.intent.getComponent().getPackageName());
        paramActivityRecord.sleeping = false;
        paramActivityRecord.forceNewConfig = false;
        showAskCompatModeDialogLocked(paramActivityRecord);
        paramActivityRecord.compat = this.mService.compatibilityInfoForPackageLocked(paramActivityRecord.info.applicationInfo);
        String str = null;
        Object localObject = null;
        boolean bool2 = false;
        if ((this.mService.mProfileApp != null) && (this.mService.mProfileApp.equals(paramProcessRecord.processName)) && ((this.mService.mProfileProc == null) || (this.mService.mProfileProc == paramProcessRecord)))
        {
            this.mService.mProfileProc = paramProcessRecord;
            str = this.mService.mProfileFile;
            localObject = this.mService.mProfileFd;
            bool2 = this.mService.mAutoStopProfiler;
        }
        paramProcessRecord.hasShownUi = true;
        paramProcessRecord.pendingUiClean = true;
        if (localObject != null);
        while (true)
        {
            try
            {
                ParcelFileDescriptor localParcelFileDescriptor = ((ParcelFileDescriptor)localObject).dup();
                localObject = localParcelFileDescriptor;
                IApplicationThread localIApplicationThread = paramProcessRecord.thread;
                Intent localIntent = new Intent(paramActivityRecord.intent);
                IApplicationToken.Stub localStub1 = paramActivityRecord.appToken;
                int i = System.identityHashCode(paramActivityRecord);
                ActivityInfo localActivityInfo = paramActivityRecord.info;
                Configuration localConfiguration1 = new Configuration(this.mService.mConfiguration);
                CompatibilityInfo localCompatibilityInfo = paramActivityRecord.compat;
                Bundle localBundle = paramActivityRecord.icicle;
                if (!paramBoolean1)
                {
                    bool3 = true;
                    localIApplicationThread.scheduleLaunchActivity(localIntent, localStub1, i, localActivityInfo, localConfiguration1, localCompatibilityInfo, localBundle, localArrayList1, localArrayList2, bool3, this.mService.isNextTransitionForward(), str, (ParcelFileDescriptor)localObject, bool2);
                    if (((0x10000000 & paramProcessRecord.info.flags) != 0) && (paramProcessRecord.processName.equals(paramProcessRecord.info.packageName)))
                    {
                        if ((this.mService.mHeavyWeightProcess != null) && (this.mService.mHeavyWeightProcess != paramProcessRecord))
                            Log.w("ActivityManager", "Starting new heavy weight process " + paramProcessRecord + " when already running " + this.mService.mHeavyWeightProcess);
                        this.mService.mHeavyWeightProcess = paramProcessRecord;
                        Message localMessage = this.mService.mHandler.obtainMessage(24);
                        localMessage.obj = paramActivityRecord;
                        this.mService.mHandler.sendMessage(localMessage);
                    }
                    paramActivityRecord.launchFailed = false;
                    if (updateLRUListLocked(paramActivityRecord))
                        Slog.w("ActivityManager", "Activity " + paramActivityRecord + " being launched, but already in LRU list");
                    if (!paramBoolean1)
                        break label895;
                    paramActivityRecord.state = ActivityState.RESUMED;
                    paramActivityRecord.stopped = false;
                    this.mResumedActivity = paramActivityRecord;
                    paramActivityRecord.task.touchActiveTime();
                    if (this.mMainStack)
                        this.mService.addRecentTaskLocked(paramActivityRecord.task);
                    completeResumeLocked(paramActivityRecord);
                    checkReadyForSleepLocked();
                    paramActivityRecord.icicle = null;
                    paramActivityRecord.haveState = false;
                    if (this.mMainStack)
                        this.mService.startSetupActivityLocked();
                    bool1 = true;
                }
            }
            catch (IOException localIOException)
            {
                localObject = null;
                continue;
                boolean bool3 = false;
                continue;
            }
            label883: paramProcessRecord.activities.remove(paramActivityRecord);
            throw localRemoteException;
            label895: paramActivityRecord.state = ActivityState.STOPPED;
            paramActivityRecord.stopped = true;
        }
    }

    final void removeActivityFromHistoryLocked(ActivityRecord paramActivityRecord)
    {
        if (paramActivityRecord.state != ActivityState.DESTROYED)
        {
            finishActivityResultsLocked(paramActivityRecord, 0, null);
            paramActivityRecord.makeFinishing();
            this.mHistory.remove(paramActivityRecord);
            paramActivityRecord.takeFromHistory();
            paramActivityRecord.state = ActivityState.DESTROYED;
            this.mService.mWindowManager.removeAppToken(paramActivityRecord.appToken);
            cleanUpActivityServicesLocked(paramActivityRecord);
            paramActivityRecord.removeUriPermissionsLocked();
        }
    }

    void removeHistoryRecordsForAppLocked(ProcessRecord paramProcessRecord)
    {
        removeHistoryRecordsForAppLocked(this.mLRUActivities, paramProcessRecord);
        removeHistoryRecordsForAppLocked(this.mStoppingActivities, paramProcessRecord);
        removeHistoryRecordsForAppLocked(this.mGoingToSleepActivities, paramProcessRecord);
        removeHistoryRecordsForAppLocked(this.mWaitingVisibleActivities, paramProcessRecord);
        removeHistoryRecordsForAppLocked(this.mFinishingActivities, paramProcessRecord);
    }

    public ActivityRecord removeTaskActivitiesLocked(int paramInt1, int paramInt2, boolean paramBoolean)
    {
        ActivityRecord localActivityRecord = null;
        TaskAccessInfo localTaskAccessInfo = getTaskAccessInfoLocked(paramInt1, false);
        if (localTaskAccessInfo.root == null)
            if (paramBoolean)
                Slog.w("ActivityManager", "removeTaskLocked: unknown taskId " + paramInt1);
        while (true)
        {
            return localActivityRecord;
            if (paramInt2 < 0)
            {
                performClearTaskAtIndexLocked(paramInt1, localTaskAccessInfo.rootIndex);
                localActivityRecord = localTaskAccessInfo.root;
            }
            else if (paramInt2 >= localTaskAccessInfo.subtasks.size())
            {
                if (paramBoolean)
                    Slog.w("ActivityManager", "removeTaskLocked: unknown subTaskIndex " + paramInt2);
            }
            else
            {
                TaskAccessInfo.SubTask localSubTask = (TaskAccessInfo.SubTask)localTaskAccessInfo.subtasks.get(paramInt2);
                performClearTaskAtIndexLocked(paramInt1, localSubTask.index);
                localActivityRecord = localSubTask.activity;
            }
        }
    }

    void reportActivityLaunchedLocked(boolean paramBoolean, ActivityRecord paramActivityRecord, long paramLong1, long paramLong2)
    {
        for (int i = -1 + this.mWaitingActivityLaunched.size(); i >= 0; i--)
        {
            IActivityManager.WaitResult localWaitResult = (IActivityManager.WaitResult)this.mWaitingActivityLaunched.get(i);
            localWaitResult.timeout = paramBoolean;
            if (paramActivityRecord != null)
                localWaitResult.who = new ComponentName(paramActivityRecord.info.packageName, paramActivityRecord.info.name);
            localWaitResult.thisTime = paramLong1;
            localWaitResult.totalTime = paramLong2;
        }
        this.mService.notifyAll();
    }

    void reportActivityVisibleLocked(ActivityRecord paramActivityRecord)
    {
        for (int i = -1 + this.mWaitingActivityVisible.size(); i >= 0; i--)
        {
            IActivityManager.WaitResult localWaitResult = (IActivityManager.WaitResult)this.mWaitingActivityVisible.get(i);
            localWaitResult.timeout = false;
            if (paramActivityRecord != null)
                localWaitResult.who = new ComponentName(paramActivityRecord.info.packageName, paramActivityRecord.info.name);
            localWaitResult.totalTime = (SystemClock.uptimeMillis() - localWaitResult.thisTime);
            localWaitResult.thisTime = localWaitResult.totalTime;
        }
        this.mService.notifyAll();
        if (this.mDismissKeyguardOnNextActivity)
        {
            this.mDismissKeyguardOnNextActivity = false;
            this.mService.mWindowManager.dismissKeyguard();
        }
    }

    final boolean requestFinishActivityLocked(IBinder paramIBinder, int paramInt, Intent paramIntent, String paramString)
    {
        int i = indexOfTokenLocked(paramIBinder);
        if (i < 0);
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            finishActivityLocked((ActivityRecord)this.mHistory.get(i), i, paramInt, paramIntent, paramString);
        }
    }

    ActivityInfo resolveActivity(Intent paramIntent, String paramString1, int paramInt1, String paramString2, ParcelFileDescriptor paramParcelFileDescriptor, int paramInt2)
    {
        boolean bool = true;
        try
        {
            ResolveInfo localResolveInfo = AppGlobals.getPackageManager().resolveIntent(paramIntent, paramString1, 66560, paramInt2);
            if (localResolveInfo != null);
            for (localActivityInfo = localResolveInfo.activityInfo; ; localActivityInfo = null)
            {
                if (localActivityInfo != null)
                {
                    paramIntent.setComponent(new ComponentName(localActivityInfo.applicationInfo.packageName, localActivityInfo.name));
                    if (((paramInt1 & 0x2) != 0) && (!localActivityInfo.processName.equals("system")))
                        this.mService.setDebugApp(localActivityInfo.processName, bool, false);
                    if (((paramInt1 & 0x4) != 0) && (!localActivityInfo.processName.equals("system")))
                        this.mService.setOpenGlTraceApp(localActivityInfo.applicationInfo, localActivityInfo.processName);
                    if ((paramString2 != null) && (!localActivityInfo.processName.equals("system")))
                    {
                        ActivityManagerService localActivityManagerService = this.mService;
                        ApplicationInfo localApplicationInfo = localActivityInfo.applicationInfo;
                        String str = localActivityInfo.processName;
                        if ((paramInt1 & 0x8) == 0)
                            break;
                        localActivityManagerService.setProfileApp(localApplicationInfo, str, paramString2, paramParcelFileDescriptor, bool);
                    }
                }
                return localActivityInfo;
            }
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                ActivityInfo localActivityInfo = null;
                continue;
                bool = false;
            }
        }
    }

    final boolean resumeTopActivityLocked(ActivityRecord paramActivityRecord)
    {
        return resumeTopActivityLocked(paramActivityRecord, null);
    }

    final boolean resumeTopActivityLocked(ActivityRecord paramActivityRecord, Bundle paramBundle)
    {
        ActivityRecord localActivityRecord1 = topRunningActivityLocked(null);
        boolean bool1 = this.mUserLeaving;
        this.mUserLeaving = false;
        boolean bool2;
        if ((localActivityRecord1 == null) && (this.mMainStack))
        {
            ActivityOptions.abort(paramBundle);
            bool2 = this.mService.startHomeActivityLocked(0);
        }
        while (true)
        {
            return bool2;
            localActivityRecord1.delayedResume = false;
            if ((this.mResumedActivity == localActivityRecord1) && (localActivityRecord1.state == ActivityState.RESUMED))
            {
                this.mService.mWindowManager.executeAppTransition();
                this.mNoAnimActivities.clear();
                ActivityOptions.abort(paramBundle);
                bool2 = false;
                continue;
            }
            if (((this.mService.mSleeping) || (this.mService.mShuttingDown)) && (this.mLastPausedActivity == localActivityRecord1) && ((localActivityRecord1.state == ActivityState.PAUSED) || (localActivityRecord1.state == ActivityState.STOPPED) || (localActivityRecord1.state == ActivityState.STOPPING)))
            {
                this.mService.mWindowManager.executeAppTransition();
                this.mNoAnimActivities.clear();
                ActivityOptions.abort(paramBundle);
                bool2 = false;
                continue;
            }
            this.mStoppingActivities.remove(localActivityRecord1);
            this.mGoingToSleepActivities.remove(localActivityRecord1);
            localActivityRecord1.sleeping = false;
            this.mWaitingVisibleActivities.remove(localActivityRecord1);
            localActivityRecord1.updateOptionsLocked(paramBundle);
            if (this.mPausingActivity != null)
            {
                bool2 = false;
                continue;
            }
            if (this.mResumedActivity != null)
            {
                startPausingLocked(bool1, false);
                bool2 = true;
                continue;
            }
            ActivityRecord localActivityRecord2 = this.mLastPausedActivity;
            if ((this.mService.mSleeping) && (localActivityRecord2 != null) && (!localActivityRecord2.finishing) && (((0x40000000 & localActivityRecord2.intent.getFlags()) != 0) || ((0x80 & localActivityRecord2.info.flags) != 0)))
                requestFinishActivityLocked(localActivityRecord2.appToken, 0, null, "no-history");
            if ((paramActivityRecord != null) && (paramActivityRecord != localActivityRecord1))
            {
                if ((paramActivityRecord.waitingVisible) || (localActivityRecord1 == null) || (localActivityRecord1.nowVisible))
                    break label728;
                paramActivityRecord.waitingVisible = true;
                this.mWaitingVisibleActivities.add(paramActivityRecord);
            }
            try
            {
                AppGlobals.getPackageManager().setPackageStoppedState(localActivityRecord1.packageName, false, localActivityRecord1.userId);
                label382: i = 0;
                if (paramActivityRecord != null)
                    if (paramActivityRecord.finishing)
                        if (this.mNoAnimActivities.contains(paramActivityRecord))
                        {
                            this.mService.mWindowManager.prepareAppTransition(0, false);
                            this.mService.mWindowManager.setAppWillBeHidden(paramActivityRecord.appToken);
                            this.mService.mWindowManager.setAppVisibility(paramActivityRecord.appToken, false);
                            if (i != 0)
                                break label972;
                            localActivityRecord1.applyOptionsLocked();
                            if ((localActivityRecord1.app == null) || (localActivityRecord1.app.thread == null))
                                break label1371;
                            this.mService.mWindowManager.setAppVisibility(localActivityRecord1.appToken, true);
                            localActivityRecord1.startLaunchTickingLocked();
                            localActivityRecord3 = this.mResumedActivity;
                            localActivityState = localActivityRecord1.state;
                            this.mService.updateCpuStats();
                            localActivityRecord1.state = ActivityState.RESUMED;
                            this.mResumedActivity = localActivityRecord1;
                            localActivityRecord1.task.touchActiveTime();
                            if (this.mMainStack)
                                this.mService.addRecentTaskLocked(localActivityRecord1.task);
                            this.mService.updateLruProcessLocked(localActivityRecord1.app, true, true);
                            updateLRUListLocked(localActivityRecord1);
                            bool3 = false;
                            if (!this.mMainStack);
                        }
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
                ActivityRecord localActivityRecord3;
                ActivityState localActivityState;
                synchronized (this.mService)
                {
                    while (true)
                    {
                        int i;
                        WindowManagerService localWindowManagerService1 = this.mService.mWindowManager;
                        Configuration localConfiguration1 = this.mService.mConfiguration;
                        if (!localActivityRecord1.mayFreezeScreenLocked(localActivityRecord1.app))
                            break label979;
                        localStub = localActivityRecord1.appToken;
                        Configuration localConfiguration2 = localWindowManagerService1.updateOrientationFromAppTokens(localConfiguration1, localStub);
                        if (localConfiguration2 != null)
                            localActivityRecord1.frozenBeforeDestroy = true;
                        boolean bool3 = this.mService.updateConfigurationLocked(localConfiguration2, localActivityRecord1, false, false);
                        if (bool3)
                            break label993;
                        if (topRunningActivityLocked(null) != localActivityRecord1)
                            this.mHandler.sendEmptyMessage(106);
                        if (this.mMainStack)
                            this.mService.setFocusedActivityLocked(localActivityRecord1);
                        ensureActivitiesVisibleLocked(null, 0);
                        this.mService.mWindowManager.executeAppTransition();
                        this.mNoAnimActivities.clear();
                        bool2 = true;
                        break;
                        if (paramActivityRecord.finishing)
                        {
                            this.mService.mWindowManager.setAppVisibility(paramActivityRecord.appToken, false);
                            continue;
                            localIllegalArgumentException = localIllegalArgumentException;
                            Slog.w("ActivityManager", "Failed trying to unstop package " + localActivityRecord1.packageName + ": " + localIllegalArgumentException);
                            continue;
                            WindowManagerService localWindowManagerService3 = this.mService.mWindowManager;
                            if (paramActivityRecord.task == localActivityRecord1.task);
                            for (int m = 8199; ; m = 8201)
                            {
                                localWindowManagerService3.prepareAppTransition(m, false);
                                break;
                            }
                            if (this.mNoAnimActivities.contains(localActivityRecord1))
                            {
                                i = 1;
                                this.mService.mWindowManager.prepareAppTransition(0, false);
                            }
                            else
                            {
                                WindowManagerService localWindowManagerService2 = this.mService.mWindowManager;
                                if (paramActivityRecord.task == localActivityRecord1.task);
                                for (int k = 4102; ; k = 4104)
                                {
                                    localWindowManagerService2.prepareAppTransition(k, false);
                                    break;
                                }
                                if (this.mHistory.size() > 1)
                                    if (this.mNoAnimActivities.contains(localActivityRecord1))
                                    {
                                        i = 1;
                                        this.mService.mWindowManager.prepareAppTransition(0, false);
                                    }
                                    else
                                    {
                                        this.mService.mWindowManager.prepareAppTransition(4102, false);
                                        continue;
                                        label972: localActivityRecord1.clearOptionsLocked();
                                    }
                            }
                        }
                    }
                    label979: IApplicationToken.Stub localStub = null;
                }
                try
                {
                    ArrayList localArrayList = localActivityRecord1.results;
                    if (localArrayList != null)
                    {
                        int j = localArrayList.size();
                        if ((!localActivityRecord1.finishing) && (j > 0))
                            localActivityRecord1.app.thread.scheduleSendResult(localActivityRecord1.appToken, localArrayList);
                    }
                    if (localActivityRecord1.newIntents != null)
                        localActivityRecord1.app.thread.scheduleNewIntent(localActivityRecord1.newIntents, localActivityRecord1.appToken);
                    Object[] arrayOfObject = new Object[3];
                    arrayOfObject[0] = Integer.valueOf(System.identityHashCode(localActivityRecord1));
                    arrayOfObject[1] = Integer.valueOf(localActivityRecord1.task.taskId);
                    arrayOfObject[2] = localActivityRecord1.shortComponentName;
                    EventLog.writeEvent(30007, arrayOfObject);
                    localActivityRecord1.sleeping = false;
                    showAskCompatModeDialogLocked(localActivityRecord1);
                    localActivityRecord1.app.pendingUiClean = true;
                    localActivityRecord1.app.thread.scheduleResumeActivity(localActivityRecord1.appToken, this.mService.isNextTransitionForward());
                    checkReadyForSleepLocked();
                }
                catch (Exception localException1)
                {
                    try
                    {
                        localActivityRecord1.visible = true;
                        completeResumeLocked(localActivityRecord1);
                        localActivityRecord1.icicle = null;
                        localActivityRecord1.haveState = false;
                        localActivityRecord1.stopped = false;
                        bool2 = true;
                        continue;
                        localException1 = localException1;
                        localActivityRecord1.state = localActivityState;
                        this.mResumedActivity = localActivityRecord3;
                        Slog.i("ActivityManager", "Restarting because process died: " + localActivityRecord1);
                        if (!localActivityRecord1.hasBeenLaunched)
                            localActivityRecord1.hasBeenLaunched = true;
                        while (true)
                        {
                            startSpecificActivityLocked(localActivityRecord1, true, false);
                            bool2 = true;
                            break;
                            if (this.mMainStack)
                                this.mService.mWindowManager.setAppStartingWindow(localActivityRecord1.appToken, localActivityRecord1.packageName, localActivityRecord1.theme, this.mService.compatibilityInfoForPackageLocked(localActivityRecord1.info.applicationInfo), localActivityRecord1.nonLocalizedLabel, localActivityRecord1.labelRes, localActivityRecord1.icon, localActivityRecord1.windowFlags, null, true);
                        }
                    }
                    catch (Exception localException2)
                    {
                        Slog.w("ActivityManager", "Exception thrown during resume of " + localActivityRecord1, localException2);
                        requestFinishActivityLocked(localActivityRecord1.appToken, 0, null, "resume-exception");
                        bool2 = true;
                    }
                }
                continue;
                label1371: if (!localActivityRecord1.hasBeenLaunched)
                    localActivityRecord1.hasBeenLaunched = true;
                while (true)
                {
                    startSpecificActivityLocked(localActivityRecord1, true, true);
                    break;
                    this.mService.mWindowManager.setAppStartingWindow(localActivityRecord1.appToken, localActivityRecord1.packageName, localActivityRecord1.theme, this.mService.compatibilityInfoForPackageLocked(localActivityRecord1.info.applicationInfo), localActivityRecord1.nonLocalizedLabel, localActivityRecord1.labelRes, localActivityRecord1.icon, localActivityRecord1.windowFlags, null, true);
                }
            }
            catch (RemoteException localRemoteException)
            {
                label728: label993: break label382;
            }
        }
    }

    final void scheduleDestroyActivities(ProcessRecord paramProcessRecord, boolean paramBoolean, String paramString)
    {
        Message localMessage = this.mHandler.obtainMessage(109);
        localMessage.obj = new ScheduleDestroyArgs(paramProcessRecord, paramBoolean, paramString);
        this.mHandler.sendMessage(localMessage);
    }

    final void scheduleIdleLocked()
    {
        Message localMessage = Message.obtain();
        localMessage.what = 103;
        this.mHandler.sendMessage(localMessage);
    }

    public final Bitmap screenshotActivities(ActivityRecord paramActivityRecord)
    {
        Bitmap localBitmap = null;
        if (paramActivityRecord.noDisplay);
        while (true)
        {
            return localBitmap;
            Resources localResources = this.mService.mContext.getResources();
            int i = this.mThumbnailWidth;
            int j = this.mThumbnailHeight;
            if (i < 0)
            {
                i = localResources.getDimensionPixelSize(17104898);
                this.mThumbnailWidth = i;
                j = localResources.getDimensionPixelSize(17104897);
                this.mThumbnailHeight = j;
            }
            if (i > 0)
                localBitmap = this.mService.mWindowManager.screenshotApplications(paramActivityRecord.appToken, i, j);
        }
    }

    void sendActivityResultLocked(int paramInt1, ActivityRecord paramActivityRecord, String paramString, int paramInt2, int paramInt3, Intent paramIntent)
    {
        if (paramInt1 > 0)
            this.mService.grantUriPermissionFromIntentLocked(paramInt1, paramActivityRecord.packageName, paramIntent, paramActivityRecord.getUriPermissionsLocked());
        if ((this.mResumedActivity == paramActivityRecord) && (paramActivityRecord.app != null) && (paramActivityRecord.app.thread != null));
        while (true)
        {
            try
            {
                ArrayList localArrayList = new ArrayList();
                localArrayList.add(new ResultInfo(paramString, paramInt2, paramInt3, paramIntent));
                paramActivityRecord.app.thread.scheduleSendResult(paramActivityRecord.appToken, localArrayList);
                return;
            }
            catch (Exception localException)
            {
                Slog.w("ActivityManager", "Exception thrown sending result to " + paramActivityRecord, localException);
            }
            paramActivityRecord.addResultLocked(null, paramString, paramInt2, paramInt3, paramIntent);
        }
    }

    final void showAskCompatModeDialogLocked(ActivityRecord paramActivityRecord)
    {
        Message localMessage = Message.obtain();
        localMessage.what = 30;
        if (paramActivityRecord.task.askedCompatMode)
            paramActivityRecord = null;
        localMessage.obj = paramActivityRecord;
        this.mService.mHandler.sendMessage(localMessage);
    }

    final int startActivities(IApplicationThread paramIApplicationThread, int paramInt1, Intent[] paramArrayOfIntent, String[] paramArrayOfString, IBinder paramIBinder, Bundle paramBundle, int paramInt2)
    {
        if (paramArrayOfIntent == null)
            throw new NullPointerException("intents is null");
        if (paramArrayOfString == null)
            throw new NullPointerException("resolvedTypes is null");
        if (paramArrayOfIntent.length != paramArrayOfString.length)
            throw new IllegalArgumentException("intents are length different than resolvedTypes");
        ActivityRecord[] arrayOfActivityRecord = new ActivityRecord[1];
        int i;
        if (paramInt1 >= 0)
            i = -1;
        long l;
        ActivityManagerService localActivityManagerService;
        int j;
        Intent localIntent1;
        while (true)
        {
            l = Binder.clearCallingIdentity();
            try
            {
                localActivityManagerService = this.mService;
                j = 0;
                try
                {
                    while (true)
                    {
                        int k = paramArrayOfIntent.length;
                        if (j >= k)
                            break label362;
                        localIntent1 = paramArrayOfIntent[j];
                        if (localIntent1 != null)
                            break;
                        j++;
                    }
                    if (paramIApplicationThread == null)
                    {
                        i = Binder.getCallingPid();
                        paramInt1 = Binder.getCallingUid();
                    }
                    else
                    {
                        paramInt1 = -1;
                        i = paramInt1;
                        continue;
                        if ((localIntent1 != null) && (localIntent1.hasFileDescriptors()))
                            throw new IllegalArgumentException("File descriptors passed in Intent");
                    }
                }
                finally
                {
                }
            }
            finally
            {
                Binder.restoreCallingIdentity(l);
            }
        }
        if (localIntent1.getComponent() != null);
        for (boolean bool = true; ; bool = false)
        {
            Intent localIntent2 = new Intent(localIntent1);
            ActivityInfo localActivityInfo1 = resolveActivity(localIntent2, paramArrayOfString[j], 0, null, null, paramInt2);
            ActivityInfo localActivityInfo2 = this.mService.getActivityInfoForUser(localActivityInfo1, paramInt2);
            if ((this.mMainStack) && (localActivityInfo2 != null) && ((0x10000000 & localActivityInfo2.applicationInfo.flags) != 0))
                throw new IllegalArgumentException("FLAG_CANT_SAVE_STATE not supported here");
            Bundle localBundle;
            int m;
            if (paramBundle != null)
            {
                int n = -1 + paramArrayOfIntent.length;
                if (j == n)
                {
                    localBundle = paramBundle;
                    m = startActivityLocked(paramIApplicationThread, localIntent2, paramArrayOfString[j], localActivityInfo2, paramIBinder, null, -1, i, paramInt1, 0, localBundle, bool, arrayOfActivityRecord);
                    if (m >= 0)
                        break label343;
                    Binder.restoreCallingIdentity(l);
                }
            }
            while (true)
            {
                return m;
                localBundle = null;
                break;
                label343: if (arrayOfActivityRecord[0] == null)
                    break label385;
                paramIBinder = arrayOfActivityRecord[0].appToken;
                break label382;
                label362: Binder.restoreCallingIdentity(l);
                m = 0;
            }
        }
        while (true)
        {
            label382: break;
            label385: paramIBinder = null;
        }
    }

    final int startActivityLocked(IApplicationThread paramIApplicationThread, Intent paramIntent, String paramString1, ActivityInfo paramActivityInfo, IBinder paramIBinder, String paramString2, int paramInt1, int paramInt2, int paramInt3, int paramInt4, Bundle paramBundle, boolean paramBoolean, ActivityRecord[] paramArrayOfActivityRecord)
    {
        int i = 0;
        ProcessRecord localProcessRecord = null;
        int i4;
        label65: int i5;
        label120: ActivityRecord localActivityRecord1;
        ActivityRecord localActivityRecord2;
        int i1;
        if (paramIApplicationThread != null)
        {
            localProcessRecord = this.mService.getRecordForAppLocked(paramIApplicationThread);
            if (localProcessRecord != null)
            {
                paramInt2 = localProcessRecord.pid;
                paramInt3 = localProcessRecord.info.uid;
            }
        }
        else
        {
            if (i == 0)
            {
                if (paramActivityInfo == null)
                    break label281;
                i4 = UserId.getUserId(paramActivityInfo.applicationInfo.uid);
                StringBuilder localStringBuilder = new StringBuilder().append("START {").append(paramIntent.toShortString(true, true, true, false)).append(" u=").append(i4).append("} from pid ");
                if (localProcessRecord == null)
                    break label287;
                i5 = localProcessRecord.pid;
                Slog.i("ActivityManager", i5);
            }
            localActivityRecord1 = null;
            localActivityRecord2 = null;
            if (paramIBinder != null)
            {
                int i3 = indexOfTokenLocked(paramIBinder);
                if (i3 >= 0)
                {
                    localActivityRecord1 = (ActivityRecord)this.mHistory.get(i3);
                    if ((paramInt1 >= 0) && (!localActivityRecord1.finishing))
                        localActivityRecord2 = localActivityRecord1;
                }
            }
            if (((0x2000000 & paramIntent.getFlags()) == 0) || (localActivityRecord1 == null))
                break label337;
            if (paramInt1 < 0)
                break label294;
            ActivityOptions.abort(paramBundle);
            i1 = -3;
        }
        label281: label287: label294: label337: ActivityRecord localActivityRecord3;
        while (true)
        {
            return i1;
            Slog.w("ActivityManager", "Unable to find app for caller " + paramIApplicationThread + " (pid=" + paramInt2 + ") when starting: " + paramIntent.toString());
            i = -4;
            break;
            i4 = 0;
            break label65;
            i5 = paramInt2;
            break label120;
            localActivityRecord2 = localActivityRecord1.resultTo;
            paramString2 = localActivityRecord1.resultWho;
            paramInt1 = localActivityRecord1.requestCode;
            localActivityRecord1.resultTo = null;
            if (localActivityRecord2 != null)
                localActivityRecord2.removeResultsLocked(localActivityRecord1, paramString2, paramInt1);
            if ((i == 0) && (paramIntent.getComponent() == null))
                i = -1;
            if ((i == 0) && (paramActivityInfo == null))
                i = -2;
            if (i != 0)
            {
                if (localActivityRecord2 != null)
                    sendActivityResultLocked(-1, localActivityRecord2, paramString2, paramInt1, 0, null);
                this.mDismissKeyguardOnNextActivity = false;
                ActivityOptions.abort(paramBundle);
                i1 = i;
            }
            else
            {
                int j = this.mService.checkPermission("android.permission.START_ANY_ACTIVITY", paramInt2, paramInt3);
                ActivityManagerService localActivityManagerService1 = this.mService;
                String str1 = paramActivityInfo.permission;
                int k = paramActivityInfo.applicationInfo.uid;
                boolean bool1 = paramActivityInfo.exported;
                int m = localActivityManagerService1.checkComponentPermission(str1, paramInt2, paramInt3, k, bool1);
                if ((j != 0) && (m != 0))
                {
                    if (localActivityRecord2 != null)
                        sendActivityResultLocked(-1, localActivityRecord2, paramString2, paramInt1, 0, null);
                    this.mDismissKeyguardOnNextActivity = false;
                    if (!paramActivityInfo.exported);
                    for (String str2 = "Permission Denial: starting " + paramIntent.toString() + " from " + localProcessRecord + " (pid=" + paramInt2 + ", uid=" + paramInt3 + ")" + " not exported from uid " + paramActivityInfo.applicationInfo.uid; ; str2 = "Permission Denial: starting " + paramIntent.toString() + " from " + localProcessRecord + " (pid=" + paramInt2 + ", uid=" + paramInt3 + ")" + " requires " + paramActivityInfo.permission)
                    {
                        Slog.w("ActivityManager", str2);
                        throw new SecurityException(str2);
                    }
                }
                if ((this.mMainStack) && (this.mService.mController != null))
                {
                    int i2 = 0;
                    try
                    {
                        Intent localIntent = paramIntent.cloneFilter();
                        boolean bool2 = this.mService.mController.activityStarting(localIntent, paramActivityInfo.applicationInfo.packageName);
                        if (!bool2);
                        for (i2 = 1; ; i2 = 0)
                        {
                            if (i2 == 0)
                                break label810;
                            if (localActivityRecord2 != null)
                                sendActivityResultLocked(-1, localActivityRecord2, paramString2, paramInt1, 0, null);
                            this.mDismissKeyguardOnNextActivity = false;
                            ActivityOptions.abort(paramBundle);
                            i1 = 0;
                            break;
                        }
                    }
                    catch (RemoteException localRemoteException)
                    {
                        while (true)
                            this.mService.mController = null;
                    }
                }
                else
                {
                    label810: ActivityManagerService localActivityManagerService2 = this.mService;
                    Configuration localConfiguration = this.mService.mConfiguration;
                    localActivityRecord3 = new ActivityRecord(localActivityManagerService2, this, localProcessRecord, paramInt3, paramIntent, paramString1, paramActivityInfo, localConfiguration, localActivityRecord2, paramString2, paramInt1, paramBoolean);
                    if (paramArrayOfActivityRecord != null)
                        paramArrayOfActivityRecord[0] = localActivityRecord3;
                    if (!this.mMainStack)
                        break label1000;
                    if (((this.mResumedActivity != null) && (this.mResumedActivity.info.applicationInfo.uid == paramInt3)) || (this.mService.checkAppSwitchAllowedLocked(paramInt2, paramInt3, "Activity start")))
                        break label974;
                    ActivityManagerService.PendingActivityLaunch localPendingActivityLaunch = new ActivityManagerService.PendingActivityLaunch();
                    localPendingActivityLaunch.r = localActivityRecord3;
                    localPendingActivityLaunch.sourceRecord = localActivityRecord1;
                    localPendingActivityLaunch.startFlags = paramInt4;
                    this.mService.mPendingActivityLaunches.add(localPendingActivityLaunch);
                    this.mDismissKeyguardOnNextActivity = false;
                    ActivityOptions.abort(paramBundle);
                    i1 = 4;
                }
            }
        }
        label974: if (this.mService.mDidAppSwitch)
            this.mService.mAppSwitchesAllowedTime = 0L;
        while (true)
        {
            this.mService.doPendingActivityLaunchesLocked(false);
            label1000: int n = startActivityUncheckedLocked(localActivityRecord3, localActivityRecord1, paramInt4, true, paramBundle);
            if ((this.mDismissKeyguardOnNextActivity) && (this.mPausingActivity == null))
            {
                this.mDismissKeyguardOnNextActivity = false;
                this.mService.mWindowManager.dismissKeyguard();
            }
            i1 = n;
            break;
            this.mService.mDidAppSwitch = true;
        }
    }

    // ERROR //
    final int startActivityMayWait(IApplicationThread paramIApplicationThread, int paramInt1, Intent paramIntent, String paramString1, IBinder paramIBinder, String paramString2, int paramInt2, int paramInt3, String paramString3, ParcelFileDescriptor paramParcelFileDescriptor, IActivityManager.WaitResult paramWaitResult, Configuration paramConfiguration, Bundle paramBundle, int paramInt4)
    {
        // Byte code:
        //     0: aload_3
        //     1: ifnull +21 -> 22
        //     4: aload_3
        //     5: invokevirtual 1560	android/content/Intent:hasFileDescriptors	()Z
        //     8: ifeq +14 -> 22
        //     11: new 1457	java/lang/IllegalArgumentException
        //     14: dup
        //     15: ldc_w 1562
        //     18: invokespecial 1551	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     21: athrow
        //     22: aload_3
        //     23: invokevirtual 414	android/content/Intent:getComponent	()Landroid/content/ComponentName;
        //     26: ifnull +694 -> 720
        //     29: iconst_1
        //     30: istore 15
        //     32: new 410	android/content/Intent
        //     35: dup
        //     36: aload_3
        //     37: invokespecial 1338	android/content/Intent:<init>	(Landroid/content/Intent;)V
        //     40: astore 16
        //     42: aload_0
        //     43: aload 16
        //     45: aload 4
        //     47: iload 8
        //     49: aload 9
        //     51: aload 10
        //     53: iload 14
        //     55: invokevirtual 1564	com/android/server/am/ActivityStack:resolveActivity	(Landroid/content/Intent;Ljava/lang/String;ILjava/lang/String;Landroid/os/ParcelFileDescriptor;I)Landroid/content/pm/ActivityInfo;
        //     58: astore 17
        //     60: aload 17
        //     62: ifnull +26 -> 88
        //     65: aload_0
        //     66: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     69: aload 17
        //     71: getfield 1438	android/content/pm/ActivityInfo:processName	Ljava/lang/String;
        //     74: aload 17
        //     76: getfield 273	android/content/pm/ActivityInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     79: invokevirtual 1685	com/android/server/am/ActivityManagerService:isSingleton	(Ljava/lang/String;Landroid/content/pm/ApplicationInfo;)Z
        //     82: ifeq +6 -> 88
        //     85: iconst_0
        //     86: istore 14
        //     88: aload_0
        //     89: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     92: aload 17
        //     94: iload 14
        //     96: invokevirtual 1568	com/android/server/am/ActivityManagerService:getActivityInfoForUser	(Landroid/content/pm/ActivityInfo;I)Landroid/content/pm/ActivityInfo;
        //     99: astore 18
        //     101: aload_0
        //     102: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     105: astore 19
        //     107: aload 19
        //     109: monitorenter
        //     110: iload_2
        //     111: iflt +615 -> 726
        //     114: bipush 255
        //     116: istore 20
        //     118: aload 12
        //     120: ifnull +871 -> 991
        //     123: aload_0
        //     124: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     127: getfield 573	com/android/server/am/ActivityManagerService:mConfiguration	Landroid/content/res/Configuration;
        //     130: aload 12
        //     132: invokevirtual 1101	android/content/res/Configuration:diff	(Landroid/content/res/Configuration;)I
        //     135: ifeq +856 -> 991
        //     138: iconst_1
        //     139: istore 21
        //     141: aload_0
        //     142: iload 21
        //     144: putfield 1097	com/android/server/am/ActivityStack:mConfigWillChange	Z
        //     147: invokestatic 864	android/os/Binder:clearCallingIdentity	()J
        //     150: lstore 23
        //     152: aload_0
        //     153: getfield 173	com/android/server/am/ActivityStack:mMainStack	Z
        //     156: ifeq +815 -> 971
        //     159: aload 18
        //     161: ifnull +810 -> 971
        //     164: ldc_w 1346
        //     167: aload 18
        //     169: getfield 273	android/content/pm/ActivityInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     172: getfield 1349	android/content/pm/ApplicationInfo:flags	I
        //     175: iand
        //     176: ifeq +795 -> 971
        //     179: aload 18
        //     181: getfield 1438	android/content/pm/ActivityInfo:processName	Ljava/lang/String;
        //     184: aload 18
        //     186: getfield 273	android/content/pm/ActivityInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     189: getfield 1350	android/content/pm/ApplicationInfo:packageName	Ljava/lang/String;
        //     192: invokevirtual 446	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     195: ifeq +776 -> 971
        //     198: aload_0
        //     199: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     202: getfield 1048	com/android/server/am/ActivityManagerService:mHeavyWeightProcess	Lcom/android/server/am/ProcessRecord;
        //     205: ifnull +766 -> 971
        //     208: aload_0
        //     209: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     212: getfield 1048	com/android/server/am/ActivityManagerService:mHeavyWeightProcess	Lcom/android/server/am/ProcessRecord;
        //     215: getfield 1348	com/android/server/am/ProcessRecord:info	Landroid/content/pm/ApplicationInfo;
        //     218: getfield 278	android/content/pm/ApplicationInfo:uid	I
        //     221: aload 18
        //     223: getfield 273	android/content/pm/ActivityInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     226: getfield 278	android/content/pm/ApplicationInfo:uid	I
        //     229: if_icmpne +24 -> 253
        //     232: aload_0
        //     233: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     236: getfield 1048	com/android/server/am/ActivityManagerService:mHeavyWeightProcess	Lcom/android/server/am/ProcessRecord;
        //     239: getfield 1313	com/android/server/am/ProcessRecord:processName	Ljava/lang/String;
        //     242: aload 18
        //     244: getfield 1438	android/content/pm/ActivityInfo:processName	Ljava/lang/String;
        //     247: invokevirtual 446	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     250: ifne +721 -> 971
        //     253: iload 20
        //     255: istore 39
        //     257: iload_2
        //     258: istore 40
        //     260: aload_1
        //     261: ifnull +34 -> 295
        //     264: aload_0
        //     265: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     268: aload_1
        //     269: invokevirtual 1577	com/android/server/am/ActivityManagerService:getRecordForAppLocked	(Landroid/app/IApplicationThread;)Lcom/android/server/am/ProcessRecord;
        //     272: astore 60
        //     274: aload 60
        //     276: ifnull +466 -> 742
        //     279: aload 60
        //     281: getfield 253	com/android/server/am/ProcessRecord:pid	I
        //     284: pop
        //     285: aload 60
        //     287: getfield 1348	com/android/server/am/ProcessRecord:info	Landroid/content/pm/ApplicationInfo;
        //     290: getfield 278	android/content/pm/ApplicationInfo:uid	I
        //     293: istore 40
        //     295: aload_0
        //     296: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     299: astore 41
        //     301: iconst_1
        //     302: anewarray 410	android/content/Intent
        //     305: astore 42
        //     307: aload 42
        //     309: iconst_0
        //     310: aload 16
        //     312: aastore
        //     313: iconst_1
        //     314: anewarray 445	java/lang/String
        //     317: astore 43
        //     319: aload 43
        //     321: iconst_0
        //     322: aload 4
        //     324: aastore
        //     325: aload 41
        //     327: iconst_2
        //     328: ldc_w 1687
        //     331: iload 40
        //     333: aconst_null
        //     334: aconst_null
        //     335: iconst_0
        //     336: aload 42
        //     338: aload 43
        //     340: ldc_w 1688
        //     343: aconst_null
        //     344: invokevirtual 1692	com/android/server/am/ActivityManagerService:getIntentSenderLocked	(ILjava/lang/String;ILandroid/os/IBinder;Ljava/lang/String;I[Landroid/content/Intent;[Ljava/lang/String;ILandroid/os/Bundle;)Landroid/content/IIntentSender;
        //     347: astore 44
        //     349: new 410	android/content/Intent
        //     352: dup
        //     353: invokespecial 1693	android/content/Intent:<init>	()V
        //     356: astore 45
        //     358: iload 7
        //     360: iflt +13 -> 373
        //     363: aload 45
        //     365: ldc_w 1695
        //     368: iconst_1
        //     369: invokevirtual 1699	android/content/Intent:putExtra	(Ljava/lang/String;Z)Landroid/content/Intent;
        //     372: pop
        //     373: aload 45
        //     375: ldc_w 1700
        //     378: new 1702	android/content/IntentSender
        //     381: dup
        //     382: aload 44
        //     384: invokespecial 1705	android/content/IntentSender:<init>	(Landroid/content/IIntentSender;)V
        //     387: invokevirtual 1708	android/content/Intent:putExtra	(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
        //     390: pop
        //     391: aload_0
        //     392: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     395: getfield 1048	com/android/server/am/ActivityManagerService:mHeavyWeightProcess	Lcom/android/server/am/ProcessRecord;
        //     398: getfield 1042	com/android/server/am/ProcessRecord:activities	Ljava/util/ArrayList;
        //     401: invokevirtual 318	java/util/ArrayList:size	()I
        //     404: ifle +53 -> 457
        //     407: aload_0
        //     408: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     411: getfield 1048	com/android/server/am/ActivityManagerService:mHeavyWeightProcess	Lcom/android/server/am/ProcessRecord;
        //     414: getfield 1042	com/android/server/am/ProcessRecord:activities	Ljava/util/ArrayList;
        //     417: iconst_0
        //     418: invokevirtual 388	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     421: checkcast 204	com/android/server/am/ActivityRecord
        //     424: astore 56
        //     426: aload 45
        //     428: ldc_w 1710
        //     431: aload 56
        //     433: getfield 684	com/android/server/am/ActivityRecord:packageName	Ljava/lang/String;
        //     436: invokevirtual 1713	android/content/Intent:putExtra	(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
        //     439: pop
        //     440: aload 45
        //     442: ldc_w 1715
        //     445: aload 56
        //     447: getfield 392	com/android/server/am/ActivityRecord:task	Lcom/android/server/am/TaskRecord;
        //     450: getfield 397	com/android/server/am/TaskRecord:taskId	I
        //     453: invokevirtual 1718	android/content/Intent:putExtra	(Ljava/lang/String;I)Landroid/content/Intent;
        //     456: pop
        //     457: aload 45
        //     459: ldc_w 1720
        //     462: aload 18
        //     464: getfield 283	android/content/pm/ComponentInfo:packageName	Ljava/lang/String;
        //     467: invokevirtual 1713	android/content/Intent:putExtra	(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
        //     470: pop
        //     471: aload 45
        //     473: aload 16
        //     475: invokevirtual 516	android/content/Intent:getFlags	()I
        //     478: invokevirtual 1723	android/content/Intent:setFlags	(I)Landroid/content/Intent;
        //     481: pop
        //     482: aload 45
        //     484: ldc_w 1687
        //     487: ldc_w 1725
        //     490: invokevirtual 1730	java/lang/Class:getName	()Ljava/lang/String;
        //     493: invokevirtual 1733	android/content/Intent:setClassName	(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
        //     496: pop
        //     497: aload 45
        //     499: astore 26
        //     501: aconst_null
        //     502: astore 4
        //     504: aconst_null
        //     505: astore_1
        //     506: invokestatic 1557	android/os/Binder:getCallingUid	()I
        //     509: istore_2
        //     510: invokestatic 1554	android/os/Binder:getCallingPid	()I
        //     513: istore 50
        //     515: iload 50
        //     517: istore 20
        //     519: iconst_1
        //     520: istore 15
        //     522: invokestatic 1421	android/app/AppGlobals:getPackageManager	()Landroid/content/pm/IPackageManager;
        //     525: aload 26
        //     527: aconst_null
        //     528: ldc_w 1422
        //     531: iload 14
        //     533: invokeinterface 1428 5 0
        //     538: astore 52
        //     540: aload 52
        //     542: ifnull +266 -> 808
        //     545: aload 52
        //     547: getfield 1433	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     550: astore 53
        //     552: aload_0
        //     553: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     556: aload 53
        //     558: iload 14
        //     560: invokevirtual 1568	com/android/server/am/ActivityManagerService:getActivityInfoForUser	(Landroid/content/pm/ActivityInfo;I)Landroid/content/pm/ActivityInfo;
        //     563: astore 55
        //     565: aload 55
        //     567: astore 25
        //     569: aload_1
        //     570: astore 27
        //     572: aload 26
        //     574: astore 28
        //     576: aload 4
        //     578: astore 29
        //     580: iload_2
        //     581: istore 30
        //     583: aload_0
        //     584: aload 27
        //     586: aload 28
        //     588: aload 29
        //     590: aload 25
        //     592: aload 5
        //     594: aload 6
        //     596: iload 7
        //     598: iload 20
        //     600: iload 30
        //     602: iload 8
        //     604: aload 13
        //     606: iload 15
        //     608: aconst_null
        //     609: invokevirtual 1573	com/android/server/am/ActivityStack:startActivityLocked	(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/content/pm/ActivityInfo;Landroid/os/IBinder;Ljava/lang/String;IIIILandroid/os/Bundle;Z[Lcom/android/server/am/ActivityRecord;)I
        //     612: istore 31
        //     614: aload_0
        //     615: getfield 1097	com/android/server/am/ActivityStack:mConfigWillChange	Z
        //     618: ifeq +41 -> 659
        //     621: aload_0
        //     622: getfield 173	com/android/server/am/ActivityStack:mMainStack	Z
        //     625: ifeq +34 -> 659
        //     628: aload_0
        //     629: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     632: ldc_w 1735
        //     635: ldc_w 1737
        //     638: invokevirtual 1740	com/android/server/am/ActivityManagerService:enforceCallingPermission	(Ljava/lang/String;Ljava/lang/String;)V
        //     641: aload_0
        //     642: iconst_0
        //     643: putfield 1097	com/android/server/am/ActivityStack:mConfigWillChange	Z
        //     646: aload_0
        //     647: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     650: aload 12
        //     652: aconst_null
        //     653: iconst_0
        //     654: iconst_0
        //     655: invokevirtual 1278	com/android/server/am/ActivityManagerService:updateConfigurationLocked	(Landroid/content/res/Configuration;Lcom/android/server/am/ActivityRecord;ZZ)Z
        //     658: pop
        //     659: lload 23
        //     661: invokestatic 880	android/os/Binder:restoreCallingIdentity	(J)V
        //     664: aload 11
        //     666: ifnull +48 -> 714
        //     669: aload 11
        //     671: iload 31
        //     673: putfield 1743	android/app/IActivityManager$WaitResult:result	I
        //     676: iload 31
        //     678: ifne +144 -> 822
        //     681: aload_0
        //     682: getfield 140	com/android/server/am/ActivityStack:mWaitingActivityLaunched	Ljava/util/ArrayList;
        //     685: aload 11
        //     687: invokevirtual 314	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     690: pop
        //     691: aload_0
        //     692: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     695: invokevirtual 1746	java/lang/Object:wait	()V
        //     698: aload 11
        //     700: getfield 1397	android/app/IActivityManager$WaitResult:timeout	Z
        //     703: ifne +11 -> 714
        //     706: aload 11
        //     708: getfield 1403	android/app/IActivityManager$WaitResult:who	Landroid/content/ComponentName;
        //     711: ifnull -20 -> 691
        //     714: aload 19
        //     716: monitorexit
        //     717: iload 31
        //     719: ireturn
        //     720: iconst_0
        //     721: istore 15
        //     723: goto -691 -> 32
        //     726: aload_1
        //     727: ifnonnull +255 -> 982
        //     730: invokestatic 1554	android/os/Binder:getCallingPid	()I
        //     733: istore 20
        //     735: invokestatic 1557	android/os/Binder:getCallingUid	()I
        //     738: istore_2
        //     739: goto -621 -> 118
        //     742: ldc 75
        //     744: new 717	java/lang/StringBuilder
        //     747: dup
        //     748: invokespecial 718	java/lang/StringBuilder:<init>	()V
        //     751: ldc_w 1590
        //     754: invokevirtual 724	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     757: aload_1
        //     758: invokevirtual 727	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     761: ldc_w 1592
        //     764: invokevirtual 724	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     767: iload 39
        //     769: invokevirtual 1233	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     772: ldc_w 1594
        //     775: invokevirtual 724	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     778: aload 16
        //     780: invokevirtual 1595	android/content/Intent:toString	()Ljava/lang/String;
        //     783: invokevirtual 724	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     786: invokevirtual 730	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     789: invokestatic 1065	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     792: pop
        //     793: aload 13
        //     795: invokestatic 657	android/app/ActivityOptions:abort	(Landroid/os/Bundle;)V
        //     798: bipush 252
        //     800: istore 31
        //     802: aload 19
        //     804: monitorexit
        //     805: goto -88 -> 717
        //     808: aconst_null
        //     809: astore 53
        //     811: goto -259 -> 552
        //     814: astore 51
        //     816: aconst_null
        //     817: astore 25
        //     819: goto -250 -> 569
        //     822: iload 31
        //     824: iconst_2
        //     825: if_icmpne -111 -> 714
        //     828: aload_0
        //     829: aconst_null
        //     830: invokevirtual 328	com/android/server/am/ActivityStack:topRunningActivityLocked	(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;
        //     833: astore 32
        //     835: aload 32
        //     837: getfield 460	com/android/server/am/ActivityRecord:nowVisible	Z
        //     840: ifeq +58 -> 898
        //     843: aload 11
        //     845: iconst_0
        //     846: putfield 1397	android/app/IActivityManager$WaitResult:timeout	Z
        //     849: aload 11
        //     851: new 403	android/content/ComponentName
        //     854: dup
        //     855: aload 32
        //     857: getfield 267	com/android/server/am/ActivityRecord:info	Landroid/content/pm/ActivityInfo;
        //     860: getfield 283	android/content/pm/ComponentInfo:packageName	Ljava/lang/String;
        //     863: aload 32
        //     865: getfield 267	com/android/server/am/ActivityRecord:info	Landroid/content/pm/ActivityInfo;
        //     868: getfield 1400	android/content/pm/ComponentInfo:name	Ljava/lang/String;
        //     871: invokespecial 420	android/content/ComponentName:<init>	(Ljava/lang/String;Ljava/lang/String;)V
        //     874: putfield 1403	android/app/IActivityManager$WaitResult:who	Landroid/content/ComponentName;
        //     877: aload 11
        //     879: lconst_0
        //     880: putfield 1409	android/app/IActivityManager$WaitResult:totalTime	J
        //     883: aload 11
        //     885: lconst_0
        //     886: putfield 1406	android/app/IActivityManager$WaitResult:thisTime	J
        //     889: goto -175 -> 714
        //     892: aload 19
        //     894: monitorexit
        //     895: aload 22
        //     897: athrow
        //     898: aload 11
        //     900: invokestatic 788	android/os/SystemClock:uptimeMillis	()J
        //     903: putfield 1406	android/app/IActivityManager$WaitResult:thisTime	J
        //     906: aload_0
        //     907: getfield 142	com/android/server/am/ActivityStack:mWaitingActivityVisible	Ljava/util/ArrayList;
        //     910: aload 11
        //     912: invokevirtual 314	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     915: pop
        //     916: aload_0
        //     917: getfield 169	com/android/server/am/ActivityStack:mService	Lcom/android/server/am/ActivityManagerService;
        //     920: invokevirtual 1746	java/lang/Object:wait	()V
        //     923: aload 11
        //     925: getfield 1397	android/app/IActivityManager$WaitResult:timeout	Z
        //     928: ifne -214 -> 714
        //     931: aload 11
        //     933: getfield 1403	android/app/IActivityManager$WaitResult:who	Landroid/content/ComponentName;
        //     936: astore 35
        //     938: aload 35
        //     940: ifnull -24 -> 916
        //     943: goto -229 -> 714
        //     946: astore 22
        //     948: goto -56 -> 892
        //     951: astore 22
        //     953: goto -61 -> 892
        //     956: astore 34
        //     958: goto -35 -> 923
        //     961: astore 37
        //     963: goto -265 -> 698
        //     966: astore 54
        //     968: goto -152 -> 816
        //     971: aload 18
        //     973: astore 25
        //     975: aload 16
        //     977: astore 26
        //     979: goto -410 -> 569
        //     982: bipush 255
        //     984: istore_2
        //     985: iload_2
        //     986: istore 20
        //     988: goto -870 -> 118
        //     991: iconst_0
        //     992: istore 21
        //     994: goto -853 -> 141
        //     997: astore 22
        //     999: goto -107 -> 892
        //
        // Exception table:
        //     from	to	target	type
        //     522	552	814	android/os/RemoteException
        //     123	497	946	finally
        //     730	805	946	finally
        //     506	515	951	finally
        //     522	552	951	finally
        //     916	923	956	java/lang/InterruptedException
        //     691	698	961	java/lang/InterruptedException
        //     552	565	966	android/os/RemoteException
        //     552	565	997	finally
        //     583	691	997	finally
        //     691	698	997	finally
        //     698	717	997	finally
        //     828	895	997	finally
        //     898	916	997	finally
        //     916	923	997	finally
        //     923	938	997	finally
    }

    final int startActivityUncheckedLocked(ActivityRecord paramActivityRecord1, ActivityRecord paramActivityRecord2, int paramInt, boolean paramBoolean, Bundle paramBundle)
    {
        Intent localIntent1 = paramActivityRecord1.intent;
        int i = paramActivityRecord1.launchedFromUid;
        int j = localIntent1.getFlags();
        boolean bool1;
        ActivityRecord localActivityRecord1;
        label64: int k;
        label154: int m;
        TaskRecord localTaskRecord1;
        ActivityRecord localActivityRecord6;
        label274: int i3;
        label360: int n;
        if ((0x40000 & j) == 0)
        {
            bool1 = true;
            this.mUserLeaving = bool1;
            if (!paramBoolean)
                paramActivityRecord1.delayedResume = true;
            if ((0x1000000 & j) == 0)
                break label438;
            localActivityRecord1 = paramActivityRecord1;
            if ((paramInt & 0x1) != 0)
            {
                ActivityRecord localActivityRecord9 = paramActivityRecord2;
                if (localActivityRecord9 == null)
                    localActivityRecord9 = topRunningNonDelayedActivityLocked(localActivityRecord1);
                if (!localActivityRecord9.realActivity.equals(paramActivityRecord1.realActivity))
                    paramInt &= -2;
            }
            if (paramActivityRecord2 != null)
                break label444;
            if ((0x10000000 & j) == 0)
            {
                Slog.w("ActivityManager", "startActivity called from non-Activity context; forcing Intent.FLAG_ACTIVITY_NEW_TASK for: " + localIntent1);
                j |= 268435456;
            }
            if ((paramActivityRecord1.resultTo != null) && ((0x10000000 & j) != 0))
            {
                Slog.w("ActivityManager", "Activity is launching as a new task, so cancelling activity result.");
                sendActivityResultLocked(-1, paramActivityRecord1.resultTo, paramActivityRecord1.resultWho, paramActivityRecord1.requestCode, 0, null);
                paramActivityRecord1.resultTo = null;
            }
            k = 0;
            m = 0;
            localTaskRecord1 = null;
            if ((((0x10000000 & j) == 0) || ((0x8000000 & j) != 0)) && (((paramActivityRecord1.launchMode != 2) && (paramActivityRecord1.launchMode != 3)) || (paramActivityRecord1.resultTo != null)))
                break label887;
            if (paramActivityRecord1.launchMode == 3)
                break label490;
            localActivityRecord6 = findTaskLocked(localIntent1, paramActivityRecord1.info);
            if (localActivityRecord6 == null)
                break label887;
            if (localActivityRecord6.task.intent == null)
                localActivityRecord6.task.setIntent(localIntent1, paramActivityRecord1.info);
            ActivityRecord localActivityRecord7 = topRunningNonDelayedActivityLocked(localActivityRecord1);
            if ((localActivityRecord7 != null) && (localActivityRecord7.task != localActivityRecord6.task))
            {
                paramActivityRecord1.intent.addFlags(4194304);
                if ((paramActivityRecord2 != null) && (localActivityRecord7.task != paramActivityRecord2.task))
                    break label505;
                i3 = 1;
                if (i3 != 0)
                {
                    m = 1;
                    moveHomeToFrontFromLaunchLocked(j);
                    moveTaskToFrontLocked(localActivityRecord6.task, paramActivityRecord1, paramBundle);
                    paramBundle = null;
                }
            }
            if ((0x200000 & j) != 0)
                localActivityRecord6 = resetTaskIfNeededLocked(localActivityRecord6, paramActivityRecord1);
            if ((paramInt & 0x1) == 0)
                break label519;
            if (!paramBoolean)
                break label511;
            resumeTopActivityLocked(null, paramBundle);
            label426: n = 1;
        }
        while (true)
        {
            return n;
            bool1 = false;
            break;
            label438: localActivityRecord1 = null;
            break label64;
            label444: if (paramActivityRecord2.launchMode == 3)
            {
                j |= 268435456;
                break label154;
            }
            if ((paramActivityRecord1.launchMode != 3) && (paramActivityRecord1.launchMode != 2))
                break label154;
            j |= 268435456;
            break label154;
            label490: localActivityRecord6 = findActivityLocked(localIntent1, paramActivityRecord1.info);
            break label274;
            label505: i3 = 0;
            break label360;
            label511: ActivityOptions.abort(paramBundle);
            break label426;
            label519: if ((0x10008000 & j) == 268468224)
            {
                localTaskRecord1 = localActivityRecord6.task;
                performClearTaskLocked(localActivityRecord6.task.taskId);
                Intent localIntent3 = paramActivityRecord1.intent;
                ActivityInfo localActivityInfo = paramActivityRecord1.info;
                localTaskRecord1.setIntent(localIntent3, localActivityInfo);
                label571: if ((k != 0) || (localTaskRecord1 != null))
                    break label887;
                if (!paramBoolean)
                    break label879;
                resumeTopActivityLocked(null, paramBundle);
            }
            while (true)
            {
                n = 2;
                break;
                if (((0x4000000 & j) != 0) || (paramActivityRecord1.launchMode == 2) || (paramActivityRecord1.launchMode == 3))
                {
                    ActivityRecord localActivityRecord8 = performClearTaskLocked(localActivityRecord6.task.taskId, paramActivityRecord1, j);
                    if (localActivityRecord8 != null)
                    {
                        if (localActivityRecord8.frontOfTask)
                            localActivityRecord8.task.setIntent(paramActivityRecord1.intent, paramActivityRecord1.info);
                        logStartActivity(30003, paramActivityRecord1, localActivityRecord8.task);
                        localActivityRecord8.deliverNewIntentLocked(i, paramActivityRecord1.intent);
                        break label571;
                    }
                    k = 1;
                    paramActivityRecord2 = localActivityRecord6;
                    break label571;
                }
                if (paramActivityRecord1.realActivity.equals(localActivityRecord6.task.realActivity))
                {
                    if (((0x20000000 & j) != 0) && (localActivityRecord6.realActivity.equals(paramActivityRecord1.realActivity)))
                    {
                        logStartActivity(30003, paramActivityRecord1, localActivityRecord6.task);
                        if (localActivityRecord6.frontOfTask)
                            localActivityRecord6.task.setIntent(paramActivityRecord1.intent, paramActivityRecord1.info);
                        Intent localIntent2 = paramActivityRecord1.intent;
                        localActivityRecord6.deliverNewIntentLocked(i, localIntent2);
                        break label571;
                    }
                    if (paramActivityRecord1.intent.filterEquals(localActivityRecord6.task.intent))
                        break label571;
                    k = 1;
                    paramActivityRecord2 = localActivityRecord6;
                    break label571;
                }
                if ((0x200000 & j) == 0)
                {
                    k = 1;
                    paramActivityRecord2 = localActivityRecord6;
                    break label571;
                }
                if (localActivityRecord6.task.rootWasReset)
                    break label571;
                localActivityRecord6.task.setIntent(paramActivityRecord1.intent, paramActivityRecord1.info);
                break label571;
                label879: ActivityOptions.abort(paramBundle);
            }
            label887: if (paramActivityRecord1.packageName != null)
            {
                ActivityRecord localActivityRecord2 = topRunningNonDelayedActivityLocked(localActivityRecord1);
                if ((localActivityRecord2 == null) || (paramActivityRecord1.resultTo != null) || (!localActivityRecord2.realActivity.equals(paramActivityRecord1.realActivity)) || (localActivityRecord2.userId != paramActivityRecord1.userId) || (localActivityRecord2.app == null) || (localActivityRecord2.app.thread == null) || (((0x20000000 & j) == 0) && (paramActivityRecord1.launchMode != 1) && (paramActivityRecord1.launchMode != 2)))
                    break label1083;
                logStartActivity(30003, localActivityRecord2, localActivityRecord2.task);
                if (paramBoolean)
                    resumeTopActivityLocked(null);
                ActivityOptions.abort(paramBundle);
                if ((paramInt & 0x1) != 0)
                {
                    n = 1;
                }
                else
                {
                    localActivityRecord2.deliverNewIntentLocked(i, paramActivityRecord1.intent);
                    n = 3;
                }
            }
            else
            {
                if (paramActivityRecord1.resultTo != null)
                    sendActivityResultLocked(-1, paramActivityRecord1.resultTo, paramActivityRecord1.resultWho, paramActivityRecord1.requestCode, 0, null);
                ActivityOptions.abort(paramBundle);
                n = -2;
            }
        }
        label1083: boolean bool2 = false;
        boolean bool3 = false;
        if ((paramActivityRecord1.resultTo == null) && (k == 0) && ((0x10000000 & j) != 0))
            if (localTaskRecord1 == null)
            {
                ActivityManagerService localActivityManagerService = this.mService;
                localActivityManagerService.mCurTask = (1 + localActivityManagerService.mCurTask);
                if (this.mService.mCurTask <= 0)
                    this.mService.mCurTask = 1;
                paramActivityRecord1.setTask(new TaskRecord(this.mService.mCurTask, paramActivityRecord1.info, localIntent1), null, true);
                label1177: bool2 = true;
                if (m == 0)
                    moveHomeToFrontFromLaunchLocked(j);
            }
        while (true)
        {
            this.mService.grantUriPermissionFromIntentLocked(i, paramActivityRecord1.packageName, localIntent1, paramActivityRecord1.getUriPermissionsLocked());
            if (bool2)
                EventLog.writeEvent(30004, paramActivityRecord1.task.taskId);
            logStartActivity(30005, paramActivityRecord1, paramActivityRecord1.task);
            startActivityLocked(paramActivityRecord1, bool2, paramBoolean, bool3, paramBundle);
            n = 0;
            break;
            paramActivityRecord1.setTask(localTaskRecord1, localTaskRecord1, true);
            break label1177;
            if (paramActivityRecord2 == null)
                break label1465;
            if ((k == 0) && ((0x4000000 & j) != 0))
            {
                ActivityRecord localActivityRecord5 = performClearTaskLocked(paramActivityRecord2.task.taskId, paramActivityRecord1, j);
                bool3 = true;
                if (localActivityRecord5 == null)
                    break label1449;
                logStartActivity(30003, paramActivityRecord1, localActivityRecord5.task);
                localActivityRecord5.deliverNewIntentLocked(i, paramActivityRecord1.intent);
                if (paramBoolean)
                    resumeTopActivityLocked(null);
                ActivityOptions.abort(paramBundle);
                n = 3;
                break;
            }
            if ((k == 0) && ((0x20000 & j) != 0))
            {
                int i2 = findActivityInHistoryLocked(paramActivityRecord1, paramActivityRecord2.task.taskId);
                if (i2 >= 0)
                {
                    ActivityRecord localActivityRecord4 = moveActivityToFrontLocked(i2);
                    logStartActivity(30003, paramActivityRecord1, localActivityRecord4.task);
                    localActivityRecord4.updateOptionsLocked(paramBundle);
                    localActivityRecord4.deliverNewIntentLocked(i, paramActivityRecord1.intent);
                    if (paramBoolean)
                        resumeTopActivityLocked(null);
                    n = 3;
                    break;
                }
            }
            label1449: paramActivityRecord1.setTask(paramActivityRecord2.task, paramActivityRecord2.thumbHolder, false);
        }
        label1465: int i1 = this.mHistory.size();
        ActivityRecord localActivityRecord3;
        if (i1 > 0)
        {
            localActivityRecord3 = (ActivityRecord)this.mHistory.get(i1 - 1);
            label1495: if (localActivityRecord3 == null)
                break label1524;
        }
        label1524: for (TaskRecord localTaskRecord2 = localActivityRecord3.task; ; localTaskRecord2 = new TaskRecord(this.mService.mCurTask, paramActivityRecord1.info, localIntent1))
        {
            paramActivityRecord1.setTask(localTaskRecord2, null, true);
            break;
            localActivityRecord3 = null;
            break label1495;
        }
    }

    void stopIfSleepingLocked()
    {
        if (this.mService.isSleeping())
        {
            if (!this.mGoingToSleep.isHeld())
            {
                this.mGoingToSleep.acquire();
                if (this.mLaunchingActivity.isHeld())
                {
                    this.mLaunchingActivity.release();
                    this.mService.mHandler.removeMessages(104);
                }
            }
            this.mHandler.removeMessages(100);
            Message localMessage = this.mHandler.obtainMessage(100);
            this.mHandler.sendMessageDelayed(localMessage, 5000L);
            checkReadyForSleepLocked();
        }
    }

    final boolean switchUser(int paramInt)
    {
        while (true)
        {
            boolean bool;
            ActivityRecord localActivityRecord1;
            int i;
            int j;
            ActivityRecord localActivityRecord2;
            synchronized (this.mService)
            {
                this.mCurrentUser = paramInt;
                if (this.mHistory.size() < 2)
                {
                    bool = false;
                }
                else
                {
                    bool = false;
                    localActivityRecord1 = (ActivityRecord)this.mHistory.get(-1 + this.mHistory.size());
                    if (localActivityRecord1.userId == paramInt)
                        bool = true;
                }
            }
            j++;
        }
    }

    final ActivityRecord topRunningActivityLocked(IBinder paramIBinder, int paramInt)
    {
        int i = -1 + this.mHistory.size();
        ActivityRecord localActivityRecord;
        if (i >= 0)
        {
            localActivityRecord = (ActivityRecord)this.mHistory.get(i);
            if ((localActivityRecord.finishing) || (paramIBinder == localActivityRecord.appToken) || (paramInt == localActivityRecord.task.taskId));
        }
        while (true)
        {
            return localActivityRecord;
            i--;
            break;
            localActivityRecord = null;
        }
    }

    final ActivityRecord topRunningActivityLocked(ActivityRecord paramActivityRecord)
    {
        int i = -1 + this.mHistory.size();
        ActivityRecord localActivityRecord;
        if (i >= 0)
        {
            localActivityRecord = (ActivityRecord)this.mHistory.get(i);
            if ((localActivityRecord.finishing) || (localActivityRecord == paramActivityRecord));
        }
        while (true)
        {
            return localActivityRecord;
            i--;
            break;
            localActivityRecord = null;
        }
    }

    final ActivityRecord topRunningNonDelayedActivityLocked(ActivityRecord paramActivityRecord)
    {
        int i = -1 + this.mHistory.size();
        ActivityRecord localActivityRecord;
        if (i >= 0)
        {
            localActivityRecord = (ActivityRecord)this.mHistory.get(i);
            if ((localActivityRecord.finishing) || (localActivityRecord.delayedResume) || (localActivityRecord == paramActivityRecord));
        }
        while (true)
        {
            return localActivityRecord;
            i--;
            break;
            localActivityRecord = null;
        }
    }

    final void updateTransitLocked(int paramInt, Bundle paramBundle)
    {
        if (paramBundle != null)
        {
            ActivityRecord localActivityRecord = topRunningActivityLocked(null);
            if ((localActivityRecord == null) || (localActivityRecord.state == ActivityState.RESUMED))
                break label42;
            localActivityRecord.updateOptionsLocked(paramBundle);
        }
        while (true)
        {
            this.mService.mWindowManager.prepareAppTransition(paramInt, false);
            return;
            label42: ActivityOptions.abort(paramBundle);
        }
    }

    final void validateAppTokensLocked()
    {
        this.mValidateAppTokens.clear();
        this.mValidateAppTokens.ensureCapacity(this.mHistory.size());
        for (int i = 0; i < this.mHistory.size(); i++)
            this.mValidateAppTokens.add(((ActivityRecord)this.mHistory.get(i)).appToken);
        this.mService.mWindowManager.validateAppTokens(this.mValidateAppTokens);
    }

    static class ScheduleDestroyArgs
    {
        final boolean mOomAdj;
        final ProcessRecord mOwner;
        final String mReason;

        ScheduleDestroyArgs(ProcessRecord paramProcessRecord, boolean paramBoolean, String paramString)
        {
            this.mOwner = paramProcessRecord;
            this.mOomAdj = paramBoolean;
            this.mReason = paramString;
        }
    }

    static enum ActivityState
    {
        static
        {
            PAUSING = new ActivityState("PAUSING", 2);
            PAUSED = new ActivityState("PAUSED", 3);
            STOPPING = new ActivityState("STOPPING", 4);
            STOPPED = new ActivityState("STOPPED", 5);
            FINISHING = new ActivityState("FINISHING", 6);
            DESTROYING = new ActivityState("DESTROYING", 7);
            DESTROYED = new ActivityState("DESTROYED", 8);
            ActivityState[] arrayOfActivityState = new ActivityState[9];
            arrayOfActivityState[0] = INITIALIZING;
            arrayOfActivityState[1] = RESUMED;
            arrayOfActivityState[2] = PAUSING;
            arrayOfActivityState[3] = PAUSED;
            arrayOfActivityState[4] = STOPPING;
            arrayOfActivityState[5] = STOPPED;
            arrayOfActivityState[6] = FINISHING;
            arrayOfActivityState[7] = DESTROYING;
            arrayOfActivityState[8] = DESTROYED;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.ActivityStack
 * JD-Core Version:        0.6.2
 */