package com.android.server.am;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import android.os.SystemClock;
import android.os.UserId;
import java.io.PrintWriter;

class TaskRecord extends ThumbnailHolder
{
    final String affinity;
    Intent affinityIntent;
    boolean askedCompatMode;
    Intent intent;
    long lastActiveTime;
    int numActivities;
    ComponentName origActivity;
    ComponentName realActivity;
    boolean rootWasReset;
    String stringName;
    final int taskId;
    int userId;

    TaskRecord(int paramInt, ActivityInfo paramActivityInfo, Intent paramIntent)
    {
        this.taskId = paramInt;
        this.affinity = paramActivityInfo.taskAffinity;
        setIntent(paramIntent, paramActivityInfo);
    }

    void dump(PrintWriter paramPrintWriter, String paramString)
    {
        if ((this.numActivities != 0) || (this.rootWasReset) || (this.userId != 0))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("numActivities=");
            paramPrintWriter.print(this.numActivities);
            paramPrintWriter.print(" rootWasReset=");
            paramPrintWriter.print(this.rootWasReset);
            paramPrintWriter.print(" userId=");
            paramPrintWriter.println(this.userId);
        }
        if (this.affinity != null)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("affinity=");
            paramPrintWriter.println(this.affinity);
        }
        if (this.intent != null)
        {
            StringBuilder localStringBuilder1 = new StringBuilder(128);
            localStringBuilder1.append(paramString);
            localStringBuilder1.append("intent={");
            this.intent.toShortString(localStringBuilder1, false, true, false, true);
            localStringBuilder1.append('}');
            paramPrintWriter.println(localStringBuilder1.toString());
        }
        if (this.affinityIntent != null)
        {
            StringBuilder localStringBuilder2 = new StringBuilder(128);
            localStringBuilder2.append(paramString);
            localStringBuilder2.append("affinityIntent={");
            this.affinityIntent.toShortString(localStringBuilder2, false, true, false, true);
            localStringBuilder2.append('}');
            paramPrintWriter.println(localStringBuilder2.toString());
        }
        if (this.origActivity != null)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("origActivity=");
            paramPrintWriter.println(this.origActivity.flattenToShortString());
        }
        if (this.realActivity != null)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("realActivity=");
            paramPrintWriter.println(this.realActivity.flattenToShortString());
        }
        if (!this.askedCompatMode)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("askedCompatMode=");
            paramPrintWriter.println(this.askedCompatMode);
        }
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("lastThumbnail=");
        paramPrintWriter.print(this.lastThumbnail);
        paramPrintWriter.print(" lastDescription=");
        paramPrintWriter.println(this.lastDescription);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("lastActiveTime=");
        paramPrintWriter.print(this.lastActiveTime);
        paramPrintWriter.print(" (inactive for ");
        paramPrintWriter.print(getInactiveDuration() / 1000L);
        paramPrintWriter.println("s)");
    }

    long getInactiveDuration()
    {
        return SystemClock.elapsedRealtime() - this.lastActiveTime;
    }

    void setIntent(Intent paramIntent, ActivityInfo paramActivityInfo)
    {
        this.stringName = null;
        ComponentName localComponentName2;
        if (paramActivityInfo.targetActivity == null)
        {
            if ((paramIntent != null) && ((paramIntent.getSelector() != null) || (paramIntent.getSourceBounds() != null)))
            {
                Intent localIntent2 = new Intent(paramIntent);
                localIntent2.setSelector(null);
                localIntent2.setSourceBounds(null);
                paramIntent = localIntent2;
            }
            this.intent = paramIntent;
            if (paramIntent != null)
            {
                localComponentName2 = paramIntent.getComponent();
                this.realActivity = localComponentName2;
                this.origActivity = null;
            }
        }
        while (true)
        {
            if ((this.intent != null) && ((0x200000 & this.intent.getFlags()) != 0))
                this.rootWasReset = true;
            if (paramActivityInfo.applicationInfo != null)
                this.userId = UserId.getUserId(paramActivityInfo.applicationInfo.uid);
            return;
            localComponentName2 = null;
            break;
            ComponentName localComponentName1 = new ComponentName(paramActivityInfo.packageName, paramActivityInfo.targetActivity);
            if (paramIntent != null)
            {
                Intent localIntent1 = new Intent(paramIntent);
                localIntent1.setComponent(localComponentName1);
                localIntent1.setSelector(null);
                localIntent1.setSourceBounds(null);
                this.intent = localIntent1;
                this.realActivity = localComponentName1;
                this.origActivity = paramIntent.getComponent();
            }
            else
            {
                this.intent = null;
                this.realActivity = localComponentName1;
                this.origActivity = new ComponentName(paramActivityInfo.packageName, paramActivityInfo.name);
            }
        }
    }

    public String toString()
    {
        String str;
        if (this.stringName != null)
        {
            str = this.stringName;
            return str;
        }
        StringBuilder localStringBuilder = new StringBuilder(128);
        localStringBuilder.append("TaskRecord{");
        localStringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
        localStringBuilder.append(" #");
        localStringBuilder.append(this.taskId);
        if (this.affinity != null)
        {
            localStringBuilder.append(" A ");
            localStringBuilder.append(this.affinity);
        }
        while (true)
        {
            localStringBuilder.append(" U ");
            localStringBuilder.append(this.userId);
            localStringBuilder.append('}');
            str = localStringBuilder.toString();
            this.stringName = str;
            break;
            if (this.intent != null)
            {
                localStringBuilder.append(" I ");
                localStringBuilder.append(this.intent.getComponent().flattenToShortString());
            }
            else if (this.affinityIntent != null)
            {
                localStringBuilder.append(" aI ");
                localStringBuilder.append(this.affinityIntent.getComponent().flattenToShortString());
            }
            else
            {
                localStringBuilder.append(" ??");
            }
        }
    }

    void touchActiveTime()
    {
        this.lastActiveTime = SystemClock.elapsedRealtime();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.TaskRecord
 * JD-Core Version:        0.6.2
 */