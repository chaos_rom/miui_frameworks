package com.android.server.am;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.ApplicationErrorReport.CrashInfo;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Message;
import android.view.Window;
import java.util.HashSet;

class AppErrorDialog extends BaseErrorDialog
{
    static final long DISMISS_TIMEOUT = 300000L;
    static final int FORCE_QUIT = 0;
    static final int FORCE_QUIT_AND_REPORT = 1;
    private static final String TAG = "AppErrorDialog";

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    ApplicationErrorReport.CrashInfo mCrashInfo;
    private final Handler mHandler = new Handler()
    {
        @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
        public void handleMessage(Message paramAnonymousMessage)
        {
            AppErrorDialog.Injector.sendFcReport(AppErrorDialog.this, paramAnonymousMessage);
            synchronized (AppErrorDialog.this.mProc)
            {
                if ((AppErrorDialog.this.mProc != null) && (AppErrorDialog.this.mProc.crashDialog == AppErrorDialog.this))
                    AppErrorDialog.this.mProc.crashDialog = null;
                AppErrorDialog.this.mResult.set(paramAnonymousMessage.what);
                AppErrorDialog.this.dismiss();
                return;
            }
        }
    };
    private final ProcessRecord mProc;
    private final AppErrorResult mResult;

    public AppErrorDialog(Context paramContext, AppErrorResult paramAppErrorResult, ProcessRecord paramProcessRecord)
    {
        super(paramContext);
        Resources localResources = paramContext.getResources();
        this.mProc = paramProcessRecord;
        this.mResult = paramAppErrorResult;
        if (paramProcessRecord.pkgList.size() == 1)
        {
            CharSequence localCharSequence = paramContext.getPackageManager().getApplicationLabel(paramProcessRecord.info);
            if (localCharSequence != null)
            {
                Object[] arrayOfObject2 = new Object[2];
                arrayOfObject2[0] = localCharSequence.toString();
                arrayOfObject2[1] = paramProcessRecord.info.processName;
                setMessage(localResources.getString(17040336, arrayOfObject2));
            }
        }
        while (true)
        {
            setCancelable(false);
            setButton(-1, localResources.getText(17040343), this.mHandler.obtainMessage(0));
            if (paramProcessRecord.errorReportReceiver != null)
                setButton(-2, localResources.getText(17040344), this.mHandler.obtainMessage(1));
            setTitle(localResources.getText(17040335));
            getWindow().addFlags(1073741824);
            getWindow().setTitle("Application Error: " + paramProcessRecord.info.processName);
            if (paramProcessRecord.persistent)
                getWindow().setType(2010);
            this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(0), 300000L);
            return;
            String str = paramProcessRecord.processName;
            Object[] arrayOfObject1 = new Object[1];
            arrayOfObject1[0] = str.toString();
            setMessage(localResources.getString(17040337, arrayOfObject1));
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public AppErrorDialog(Context paramContext, AppErrorResult paramAppErrorResult, ProcessRecord paramProcessRecord, ApplicationErrorReport.CrashInfo paramCrashInfo)
    {
        this(paramContext, paramAppErrorResult, paramProcessRecord);
        this.mCrashInfo = paramCrashInfo;
        setButton(-2, paramContext.getResources().getText(17040344) + " MIUI", this.mHandler.obtainMessage(1));
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    ProcessRecord getProc()
    {
        return this.mProc;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static void sendFcReport(AppErrorDialog paramAppErrorDialog, Message paramMessage)
        {
            int i = 1;
            Context localContext;
            ProcessRecord localProcessRecord;
            ApplicationErrorReport.CrashInfo localCrashInfo;
            if ((paramAppErrorDialog.getProc() != null) && (paramAppErrorDialog.mCrashInfo != null))
            {
                localContext = paramAppErrorDialog.getContext();
                localProcessRecord = paramAppErrorDialog.getProc();
                localCrashInfo = paramAppErrorDialog.mCrashInfo;
                if (paramMessage.what != i)
                    break label51;
            }
            while (true)
            {
                MiuiErrorReport.sendFcErrorReport(localContext, localProcessRecord, localCrashInfo, i);
                return;
                label51: int j = 0;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.AppErrorDialog
 * JD-Core Version:        0.6.2
 */