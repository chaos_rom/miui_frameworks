package com.android.server.am;

import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import java.util.HashSet;
import java.util.Iterator;

class UriPermissionOwner
{
    Binder externalToken;
    final Object owner;
    HashSet<UriPermission> readUriPermissions;
    final ActivityManagerService service;
    HashSet<UriPermission> writeUriPermissions;

    UriPermissionOwner(ActivityManagerService paramActivityManagerService, Object paramObject)
    {
        this.service = paramActivityManagerService;
        this.owner = paramObject;
    }

    static UriPermissionOwner fromExternalToken(IBinder paramIBinder)
    {
        if ((paramIBinder instanceof ExternalToken));
        for (UriPermissionOwner localUriPermissionOwner = ((ExternalToken)paramIBinder).getOwner(); ; localUriPermissionOwner = null)
            return localUriPermissionOwner;
    }

    public void addReadPermission(UriPermission paramUriPermission)
    {
        if (this.readUriPermissions == null)
            this.readUriPermissions = new HashSet();
        this.readUriPermissions.add(paramUriPermission);
    }

    public void addWritePermission(UriPermission paramUriPermission)
    {
        if (this.writeUriPermissions == null)
            this.writeUriPermissions = new HashSet();
        this.writeUriPermissions.add(paramUriPermission);
    }

    Binder getExternalTokenLocked()
    {
        if (this.externalToken == null)
            this.externalToken = new ExternalToken();
        return this.externalToken;
    }

    public void removeReadPermission(UriPermission paramUriPermission)
    {
        this.readUriPermissions.remove(paramUriPermission);
        if (this.readUriPermissions.size() == 0)
            this.readUriPermissions = null;
    }

    void removeUriPermissionLocked(Uri paramUri, int paramInt)
    {
        if (((paramInt & 0x1) != 0) && (this.readUriPermissions != null))
        {
            Iterator localIterator2 = this.readUriPermissions.iterator();
            while (localIterator2.hasNext())
            {
                UriPermission localUriPermission2 = (UriPermission)localIterator2.next();
                if (paramUri.equals(localUriPermission2.uri))
                {
                    localUriPermission2.readOwners.remove(this);
                    if ((localUriPermission2.readOwners.size() == 0) && ((0x1 & localUriPermission2.globalModeFlags) == 0))
                    {
                        localUriPermission2.modeFlags = (0xFFFFFFFE & localUriPermission2.modeFlags);
                        this.service.removeUriPermissionIfNeededLocked(localUriPermission2);
                    }
                    localIterator2.remove();
                }
            }
            if (this.readUriPermissions.size() == 0)
                this.readUriPermissions = null;
        }
        if (((paramInt & 0x2) != 0) && (this.writeUriPermissions != null))
        {
            Iterator localIterator1 = this.writeUriPermissions.iterator();
            while (localIterator1.hasNext())
            {
                UriPermission localUriPermission1 = (UriPermission)localIterator1.next();
                if (paramUri.equals(localUriPermission1.uri))
                {
                    localUriPermission1.writeOwners.remove(this);
                    if ((localUriPermission1.writeOwners.size() == 0) && ((0x2 & localUriPermission1.globalModeFlags) == 0))
                    {
                        localUriPermission1.modeFlags = (0xFFFFFFFD & localUriPermission1.modeFlags);
                        this.service.removeUriPermissionIfNeededLocked(localUriPermission1);
                    }
                    localIterator1.remove();
                }
            }
            if (this.writeUriPermissions.size() == 0)
                this.writeUriPermissions = null;
        }
    }

    void removeUriPermissionsLocked()
    {
        removeUriPermissionsLocked(3);
    }

    void removeUriPermissionsLocked(int paramInt)
    {
        if (((paramInt & 0x1) != 0) && (this.readUriPermissions != null))
        {
            Iterator localIterator2 = this.readUriPermissions.iterator();
            while (localIterator2.hasNext())
            {
                UriPermission localUriPermission2 = (UriPermission)localIterator2.next();
                localUriPermission2.readOwners.remove(this);
                if ((localUriPermission2.readOwners.size() == 0) && ((0x1 & localUriPermission2.globalModeFlags) == 0))
                {
                    localUriPermission2.modeFlags = (0xFFFFFFFE & localUriPermission2.modeFlags);
                    this.service.removeUriPermissionIfNeededLocked(localUriPermission2);
                }
            }
            this.readUriPermissions = null;
        }
        if (((paramInt & 0x2) != 0) && (this.writeUriPermissions != null))
        {
            Iterator localIterator1 = this.writeUriPermissions.iterator();
            while (localIterator1.hasNext())
            {
                UriPermission localUriPermission1 = (UriPermission)localIterator1.next();
                localUriPermission1.writeOwners.remove(this);
                if ((localUriPermission1.writeOwners.size() == 0) && ((0x2 & localUriPermission1.globalModeFlags) == 0))
                {
                    localUriPermission1.modeFlags = (0xFFFFFFFD & localUriPermission1.modeFlags);
                    this.service.removeUriPermissionIfNeededLocked(localUriPermission1);
                }
            }
            this.writeUriPermissions = null;
        }
    }

    public void removeWritePermission(UriPermission paramUriPermission)
    {
        this.writeUriPermissions.remove(paramUriPermission);
        if (this.writeUriPermissions.size() == 0)
            this.writeUriPermissions = null;
    }

    public String toString()
    {
        return this.owner.toString();
    }

    class ExternalToken extends Binder
    {
        ExternalToken()
        {
        }

        UriPermissionOwner getOwner()
        {
            return UriPermissionOwner.this;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.UriPermissionOwner
 * JD-Core Version:        0.6.2
 */