package com.android.server.am;

import android.app.ActivityManager.TaskThumbnails;
import android.graphics.Bitmap;
import java.util.ArrayList;

final class TaskAccessInfo extends ActivityManager.TaskThumbnails
{
    public ActivityRecord root;
    public int rootIndex;
    public ArrayList<SubTask> subtasks;

    static final class SubTask
    {
        ActivityRecord activity;
        int index;
        Bitmap thumbnail;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.TaskAccessInfo
 * JD-Core Version:        0.6.2
 */