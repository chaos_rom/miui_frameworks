package com.android.server.am;

import android.app.INotificationManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.Intent.FilterComparison;
import android.content.pm.ApplicationInfo;
import android.content.pm.ServiceInfo;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.UserId;
import android.util.Slog;
import android.util.TimeUtils;
import com.android.internal.os.BatteryStatsImpl.Uid.Pkg.Serv;
import com.android.server.NotificationManagerService;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

class ServiceRecord extends Binder
{
    static final int MAX_DELIVERY_COUNT = 3;
    static final int MAX_DONE_EXECUTING_COUNT = 6;
    final ActivityManagerService ams;
    ProcessRecord app;
    final ApplicationInfo appInfo;
    final String baseDir;
    final HashMap<Intent.FilterComparison, IntentBindRecord> bindings = new HashMap();
    boolean callStart;
    final HashMap<IBinder, ArrayList<ConnectionRecord>> connections = new HashMap();
    int crashCount;
    final long createTime;
    final String dataDir;
    final ArrayList<StartItem> deliveredStarts = new ArrayList();
    int executeNesting;
    long executingStart;
    final boolean exported;
    int foregroundId;
    Notification foregroundNoti;
    final Intent.FilterComparison intent;
    boolean isForeground;
    ProcessRecord isolatedProc;
    long lastActivity;
    private int lastStartId;
    final ComponentName name;
    long nextRestartTime;
    final String packageName;
    final ArrayList<StartItem> pendingStarts = new ArrayList();
    final String permission;
    final String processName;
    final String resDir;
    int restartCount;
    long restartDelay;
    long restartTime;
    final Runnable restarter;
    final ServiceInfo serviceInfo;
    final String shortName;
    boolean startRequested;
    final BatteryStatsImpl.Uid.Pkg.Serv stats;
    boolean stopIfKilled;
    String stringName;
    int totalRestartCount;
    final int userId;

    ServiceRecord(ActivityManagerService paramActivityManagerService, BatteryStatsImpl.Uid.Pkg.Serv paramServ, ComponentName paramComponentName, Intent.FilterComparison paramFilterComparison, ServiceInfo paramServiceInfo, Runnable paramRunnable)
    {
        this.ams = paramActivityManagerService;
        this.stats = paramServ;
        this.name = paramComponentName;
        this.shortName = paramComponentName.flattenToShortString();
        this.intent = paramFilterComparison;
        this.serviceInfo = paramServiceInfo;
        this.appInfo = paramServiceInfo.applicationInfo;
        this.packageName = paramServiceInfo.applicationInfo.packageName;
        this.processName = paramServiceInfo.processName;
        this.permission = paramServiceInfo.permission;
        this.baseDir = paramServiceInfo.applicationInfo.sourceDir;
        this.resDir = paramServiceInfo.applicationInfo.publicSourceDir;
        this.dataDir = paramServiceInfo.applicationInfo.dataDir;
        this.exported = paramServiceInfo.exported;
        this.restarter = paramRunnable;
        this.createTime = SystemClock.elapsedRealtime();
        this.lastActivity = SystemClock.uptimeMillis();
        this.userId = UserId.getUserId(this.appInfo.uid);
    }

    public void cancelNotification()
    {
        if (this.foregroundId != 0)
        {
            final String str = this.packageName;
            final int i = this.foregroundId;
            this.ams.mHandler.post(new Runnable()
            {
                public void run()
                {
                    INotificationManager localINotificationManager = NotificationManager.getService();
                    if (localINotificationManager == null);
                    while (true)
                    {
                        return;
                        try
                        {
                            localINotificationManager.cancelNotification(str, i);
                        }
                        catch (RuntimeException localRuntimeException)
                        {
                            Slog.w("ActivityManager", "Error canceling notification for service", localRuntimeException);
                        }
                        catch (RemoteException localRemoteException)
                        {
                        }
                    }
                }
            });
        }
    }

    public void clearDeliveredStartsLocked()
    {
        for (int i = -1 + this.deliveredStarts.size(); i >= 0; i--)
            ((StartItem)this.deliveredStarts.get(i)).removeUriPermissionsLocked();
        this.deliveredStarts.clear();
    }

    void dump(PrintWriter paramPrintWriter, String paramString)
    {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("intent={");
        paramPrintWriter.print(this.intent.getIntent().toShortString(false, true, false, true));
        paramPrintWriter.println('}');
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("packageName=");
        paramPrintWriter.println(this.packageName);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("processName=");
        paramPrintWriter.println(this.processName);
        if (this.permission != null)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("permission=");
            paramPrintWriter.println(this.permission);
        }
        long l1 = SystemClock.uptimeMillis();
        long l2 = SystemClock.elapsedRealtime();
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("baseDir=");
        paramPrintWriter.println(this.baseDir);
        if (!this.resDir.equals(this.baseDir))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("resDir=");
            paramPrintWriter.println(this.resDir);
        }
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("dataDir=");
        paramPrintWriter.println(this.dataDir);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("app=");
        paramPrintWriter.println(this.app);
        if (this.isolatedProc != null)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("isolatedProc=");
            paramPrintWriter.println(this.isolatedProc);
        }
        if ((this.isForeground) || (this.foregroundId != 0))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("isForeground=");
            paramPrintWriter.print(this.isForeground);
            paramPrintWriter.print(" foregroundId=");
            paramPrintWriter.print(this.foregroundId);
            paramPrintWriter.print(" foregroundNoti=");
            paramPrintWriter.println(this.foregroundNoti);
        }
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("createTime=");
        TimeUtils.formatDuration(this.createTime, l2, paramPrintWriter);
        paramPrintWriter.print(" lastActivity=");
        TimeUtils.formatDuration(this.lastActivity, l1, paramPrintWriter);
        paramPrintWriter.println("");
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("executingStart=");
        TimeUtils.formatDuration(this.executingStart, l1, paramPrintWriter);
        paramPrintWriter.print(" restartTime=");
        TimeUtils.formatDuration(this.restartTime, l1, paramPrintWriter);
        paramPrintWriter.println("");
        if ((this.startRequested) || (this.lastStartId != 0))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("startRequested=");
            paramPrintWriter.print(this.startRequested);
            paramPrintWriter.print(" stopIfKilled=");
            paramPrintWriter.print(this.stopIfKilled);
            paramPrintWriter.print(" callStart=");
            paramPrintWriter.print(this.callStart);
            paramPrintWriter.print(" lastStartId=");
            paramPrintWriter.println(this.lastStartId);
        }
        if ((this.executeNesting != 0) || (this.crashCount != 0) || (this.restartCount != 0) || (this.restartDelay != 0L) || (this.nextRestartTime != 0L))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("executeNesting=");
            paramPrintWriter.print(this.executeNesting);
            paramPrintWriter.print(" restartCount=");
            paramPrintWriter.print(this.restartCount);
            paramPrintWriter.print(" restartDelay=");
            TimeUtils.formatDuration(this.restartDelay, l1, paramPrintWriter);
            paramPrintWriter.print(" nextRestartTime=");
            TimeUtils.formatDuration(this.nextRestartTime, l1, paramPrintWriter);
            paramPrintWriter.print(" crashCount=");
            paramPrintWriter.println(this.crashCount);
        }
        if (this.deliveredStarts.size() > 0)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.println("Delivered Starts:");
            dumpStartList(paramPrintWriter, paramString, this.deliveredStarts, l1);
        }
        if (this.pendingStarts.size() > 0)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.println("Pending Starts:");
            dumpStartList(paramPrintWriter, paramString, this.pendingStarts, 0L);
        }
        if (this.bindings.size() > 0)
        {
            Iterator localIterator2 = this.bindings.values().iterator();
            paramPrintWriter.print(paramString);
            paramPrintWriter.println("Bindings:");
            while (localIterator2.hasNext())
            {
                IntentBindRecord localIntentBindRecord = (IntentBindRecord)localIterator2.next();
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("* IntentBindRecord{");
                paramPrintWriter.print(Integer.toHexString(System.identityHashCode(localIntentBindRecord)));
                paramPrintWriter.println("}:");
                localIntentBindRecord.dumpInService(paramPrintWriter, paramString + "    ");
            }
        }
        if (this.connections.size() > 0)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.println("All Connections:");
            Iterator localIterator1 = this.connections.values().iterator();
            if (localIterator1.hasNext())
            {
                ArrayList localArrayList = (ArrayList)localIterator1.next();
                for (int i = 0; ; i++)
                {
                    int j = localArrayList.size();
                    if (i >= j)
                        break;
                    paramPrintWriter.print(paramString);
                    paramPrintWriter.print("    ");
                    paramPrintWriter.println(localArrayList.get(i));
                }
            }
        }
    }

    void dumpStartList(PrintWriter paramPrintWriter, String paramString, List<StartItem> paramList, long paramLong)
    {
        int i = paramList.size();
        int j = 0;
        if (j < i)
        {
            StartItem localStartItem = (StartItem)paramList.get(j);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("#");
            paramPrintWriter.print(j);
            paramPrintWriter.print(" id=");
            paramPrintWriter.print(localStartItem.id);
            if (paramLong != 0L)
            {
                paramPrintWriter.print(" dur=");
                TimeUtils.formatDuration(localStartItem.deliveredTime, paramLong, paramPrintWriter);
            }
            if (localStartItem.deliveryCount != 0)
            {
                paramPrintWriter.print(" dc=");
                paramPrintWriter.print(localStartItem.deliveryCount);
            }
            if (localStartItem.doneExecutingCount != 0)
            {
                paramPrintWriter.print(" dxc=");
                paramPrintWriter.print(localStartItem.doneExecutingCount);
            }
            paramPrintWriter.println("");
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("    intent=");
            if (localStartItem.intent != null)
                paramPrintWriter.println(localStartItem.intent.toString());
            while (true)
            {
                if (localStartItem.neededGrants != null)
                {
                    paramPrintWriter.print(paramString);
                    paramPrintWriter.print("    neededGrants=");
                    paramPrintWriter.println(localStartItem.neededGrants);
                }
                if (localStartItem.uriPermissions != null)
                {
                    if (localStartItem.uriPermissions.readUriPermissions != null)
                    {
                        paramPrintWriter.print(paramString);
                        paramPrintWriter.print("    readUriPermissions=");
                        paramPrintWriter.println(localStartItem.uriPermissions.readUriPermissions);
                    }
                    if (localStartItem.uriPermissions.writeUriPermissions != null)
                    {
                        paramPrintWriter.print(paramString);
                        paramPrintWriter.print("    writeUriPermissions=");
                        paramPrintWriter.println(localStartItem.uriPermissions.writeUriPermissions);
                    }
                }
                j++;
                break;
                paramPrintWriter.println("null");
            }
        }
    }

    public StartItem findDeliveredStart(int paramInt, boolean paramBoolean)
    {
        int i = this.deliveredStarts.size();
        int j = 0;
        StartItem localStartItem;
        if (j < i)
        {
            localStartItem = (StartItem)this.deliveredStarts.get(j);
            if (localStartItem.id == paramInt)
                if (paramBoolean)
                    this.deliveredStarts.remove(j);
        }
        while (true)
        {
            return localStartItem;
            j++;
            break;
            localStartItem = null;
        }
    }

    public int getLastStartId()
    {
        return this.lastStartId;
    }

    public int makeNextStartId()
    {
        this.lastStartId = (1 + this.lastStartId);
        if (this.lastStartId < 1)
            this.lastStartId = 1;
        return this.lastStartId;
    }

    public void postNotification()
    {
        final int i = this.appInfo.uid;
        final int j = this.app.pid;
        if ((this.foregroundId != 0) && (this.foregroundNoti != null))
        {
            final String str = this.packageName;
            final int k = this.foregroundId;
            final Notification localNotification = this.foregroundNoti;
            this.ams.mHandler.post(new Runnable()
            {
                public void run()
                {
                    NotificationManagerService localNotificationManagerService = (NotificationManagerService)NotificationManager.getService();
                    if (localNotificationManagerService == null);
                    while (true)
                    {
                        return;
                        try
                        {
                            int[] arrayOfInt = new int[1];
                            localNotificationManagerService.enqueueNotificationInternal(str, i, j, null, k, localNotification, arrayOfInt);
                        }
                        catch (RuntimeException localRuntimeException)
                        {
                            Slog.w("ActivityManager", "Error showing notification for service", localRuntimeException);
                            ServiceRecord.this.ams.setServiceForeground(ServiceRecord.this.name, ServiceRecord.this, 0, null, true);
                            ServiceRecord.this.ams.crashApplication(i, j, str, "Bad notification for startForeground: " + localRuntimeException);
                        }
                    }
                }
            });
        }
    }

    public void resetRestartCounter()
    {
        this.restartCount = 0;
        this.restartDelay = 0L;
        this.restartTime = 0L;
    }

    public AppBindRecord retrieveAppBindingLocked(Intent paramIntent, ProcessRecord paramProcessRecord)
    {
        Intent.FilterComparison localFilterComparison = new Intent.FilterComparison(paramIntent);
        IntentBindRecord localIntentBindRecord = (IntentBindRecord)this.bindings.get(localFilterComparison);
        if (localIntentBindRecord == null)
        {
            localIntentBindRecord = new IntentBindRecord(this, localFilterComparison);
            this.bindings.put(localFilterComparison, localIntentBindRecord);
        }
        AppBindRecord localAppBindRecord1 = (AppBindRecord)localIntentBindRecord.apps.get(paramProcessRecord);
        if (localAppBindRecord1 != null);
        AppBindRecord localAppBindRecord2;
        for (Object localObject = localAppBindRecord1; ; localObject = localAppBindRecord2)
        {
            return localObject;
            localAppBindRecord2 = new AppBindRecord(this, localIntentBindRecord, paramProcessRecord);
            localIntentBindRecord.apps.put(paramProcessRecord, localAppBindRecord2);
        }
    }

    public String toString()
    {
        String str;
        if (this.stringName != null)
            str = this.stringName;
        while (true)
        {
            return str;
            StringBuilder localStringBuilder = new StringBuilder(128);
            localStringBuilder.append("ServiceRecord{").append(Integer.toHexString(System.identityHashCode(this))).append(' ').append(this.shortName).append('}');
            str = localStringBuilder.toString();
            this.stringName = str;
        }
    }

    static class StartItem
    {
        long deliveredTime;
        int deliveryCount;
        int doneExecutingCount;
        final int id;
        final Intent intent;
        final ActivityManagerService.NeededUriGrants neededGrants;
        final ServiceRecord sr;
        String stringName;
        final boolean taskRemoved;
        UriPermissionOwner uriPermissions;

        StartItem(ServiceRecord paramServiceRecord, boolean paramBoolean, int paramInt, Intent paramIntent, ActivityManagerService.NeededUriGrants paramNeededUriGrants)
        {
            this.sr = paramServiceRecord;
            this.taskRemoved = paramBoolean;
            this.id = paramInt;
            this.intent = paramIntent;
            this.neededGrants = paramNeededUriGrants;
        }

        UriPermissionOwner getUriPermissionsLocked()
        {
            if (this.uriPermissions == null)
                this.uriPermissions = new UriPermissionOwner(this.sr.ams, this);
            return this.uriPermissions;
        }

        void removeUriPermissionsLocked()
        {
            if (this.uriPermissions != null)
            {
                this.uriPermissions.removeUriPermissionsLocked();
                this.uriPermissions = null;
            }
        }

        public String toString()
        {
            String str;
            if (this.stringName != null)
                str = this.stringName;
            while (true)
            {
                return str;
                StringBuilder localStringBuilder = new StringBuilder(128);
                localStringBuilder.append("ServiceRecord{").append(Integer.toHexString(System.identityHashCode(this.sr))).append(' ').append(this.sr.shortName).append(" StartItem ").append(Integer.toHexString(System.identityHashCode(this))).append(" id=").append(this.id).append('}');
                str = localStringBuilder.toString();
                this.stringName = str;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.ServiceRecord
 * JD-Core Version:        0.6.2
 */