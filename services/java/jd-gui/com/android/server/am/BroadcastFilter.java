package com.android.server.am;

import android.content.IntentFilter;
import android.util.PrintWriterPrinter;
import android.util.Printer;
import java.io.PrintWriter;

class BroadcastFilter extends IntentFilter
{
    final String packageName;
    final ReceiverList receiverList;
    final String requiredPermission;

    BroadcastFilter(IntentFilter paramIntentFilter, ReceiverList paramReceiverList, String paramString1, String paramString2)
    {
        super(paramIntentFilter);
        this.receiverList = paramReceiverList;
        this.packageName = paramString1;
        this.requiredPermission = paramString2;
    }

    public void dump(PrintWriter paramPrintWriter, String paramString)
    {
        dumpInReceiverList(paramPrintWriter, new PrintWriterPrinter(paramPrintWriter), paramString);
        this.receiverList.dumpLocal(paramPrintWriter, paramString);
    }

    public void dumpBrief(PrintWriter paramPrintWriter, String paramString)
    {
        dumpBroadcastFilterState(paramPrintWriter, paramString);
    }

    void dumpBroadcastFilterState(PrintWriter paramPrintWriter, String paramString)
    {
        if (this.requiredPermission != null)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("requiredPermission=");
            paramPrintWriter.println(this.requiredPermission);
        }
    }

    public void dumpInReceiverList(PrintWriter paramPrintWriter, Printer paramPrinter, String paramString)
    {
        super.dump(paramPrinter, paramString);
        dumpBroadcastFilterState(paramPrintWriter, paramString);
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("BroadcastFilter{");
        localStringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
        localStringBuilder.append(' ');
        localStringBuilder.append(this.receiverList);
        localStringBuilder.append('}');
        return localStringBuilder.toString();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.BroadcastFilter
 * JD-Core Version:        0.6.2
 */