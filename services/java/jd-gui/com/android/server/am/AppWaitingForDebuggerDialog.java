package com.android.server.am;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Message;
import android.view.Window;

class AppWaitingForDebuggerDialog extends BaseErrorDialog
{
    private CharSequence mAppName;
    private final Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            switch (paramAnonymousMessage.what)
            {
            default:
            case 1:
            }
            while (true)
            {
                return;
                AppWaitingForDebuggerDialog.this.mService.killAppAtUsersRequest(AppWaitingForDebuggerDialog.this.mProc, AppWaitingForDebuggerDialog.this);
            }
        }
    };
    final ProcessRecord mProc;
    final ActivityManagerService mService;

    public AppWaitingForDebuggerDialog(ActivityManagerService paramActivityManagerService, Context paramContext, ProcessRecord paramProcessRecord)
    {
        super(paramContext);
        this.mService = paramActivityManagerService;
        this.mProc = paramProcessRecord;
        this.mAppName = paramContext.getPackageManager().getApplicationLabel(paramProcessRecord.info);
        setCancelable(false);
        StringBuilder localStringBuilder = new StringBuilder();
        if ((this.mAppName != null) && (this.mAppName.length() > 0))
        {
            localStringBuilder.append("Application ");
            localStringBuilder.append(this.mAppName);
            localStringBuilder.append(" (process ");
            localStringBuilder.append(paramProcessRecord.processName);
            localStringBuilder.append(")");
        }
        while (true)
        {
            localStringBuilder.append(" is waiting for the debugger to attach.");
            setMessage(localStringBuilder.toString());
            setButton(-1, "Force Close", this.mHandler.obtainMessage(1, paramProcessRecord));
            setTitle("Waiting For Debugger");
            getWindow().setTitle("Waiting For Debugger: " + paramProcessRecord.info.processName);
            return;
            localStringBuilder.append("Process ");
            localStringBuilder.append(paramProcessRecord.processName);
        }
    }

    public void onStop()
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.AppWaitingForDebuggerDialog
 * JD-Core Version:        0.6.2
 */