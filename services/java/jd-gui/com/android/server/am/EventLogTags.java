package com.android.server.am;

import android.util.EventLog;

public class EventLogTags
{
    public static final int ACTIVITY_LAUNCH_TIME = 30009;
    public static final int AM_ANR = 30008;
    public static final int AM_BROADCAST_DISCARD_APP = 30025;
    public static final int AM_BROADCAST_DISCARD_FILTER = 30024;
    public static final int AM_CRASH = 30039;
    public static final int AM_CREATE_ACTIVITY = 30005;
    public static final int AM_CREATE_SERVICE = 30030;
    public static final int AM_CREATE_TASK = 30004;
    public static final int AM_DESTROY_ACTIVITY = 30018;
    public static final int AM_DESTROY_SERVICE = 30031;
    public static final int AM_DROP_PROCESS = 30033;
    public static final int AM_FAILED_TO_PAUSE = 30012;
    public static final int AM_FINISH_ACTIVITY = 30001;
    public static final int AM_KILL = 30023;
    public static final int AM_LOW_MEMORY = 30017;
    public static final int AM_NEW_INTENT = 30003;
    public static final int AM_ON_PAUSED_CALLED = 30021;
    public static final int AM_ON_RESUME_CALLED = 30022;
    public static final int AM_PAUSE_ACTIVITY = 30013;
    public static final int AM_PROCESS_CRASHED_TOO_MUCH = 30032;
    public static final int AM_PROCESS_START_TIMEOUT = 30037;
    public static final int AM_PROC_BAD = 30015;
    public static final int AM_PROC_BOUND = 30010;
    public static final int AM_PROC_DIED = 30011;
    public static final int AM_PROC_GOOD = 30016;
    public static final int AM_PROC_START = 30014;
    public static final int AM_PROVIDER_LOST_PROCESS = 30036;
    public static final int AM_RELAUNCH_ACTIVITY = 30020;
    public static final int AM_RELAUNCH_RESUME_ACTIVITY = 30019;
    public static final int AM_RESTART_ACTIVITY = 30006;
    public static final int AM_RESUME_ACTIVITY = 30007;
    public static final int AM_SCHEDULE_SERVICE_RESTART = 30035;
    public static final int AM_SERVICE_CRASHED_TOO_MUCH = 30034;
    public static final int AM_TASK_TO_FRONT = 30002;
    public static final int AM_WTF = 30040;
    public static final int BOOT_PROGRESS_AMS_READY = 3040;
    public static final int BOOT_PROGRESS_ENABLE_SCREEN = 3050;
    public static final int CONFIGURATION_CHANGED = 2719;
    public static final int CPU = 2721;

    public static void writeActivityLaunchTime(int paramInt, String paramString, long paramLong)
    {
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = Integer.valueOf(paramInt);
        arrayOfObject[1] = paramString;
        arrayOfObject[2] = Long.valueOf(paramLong);
        EventLog.writeEvent(30009, arrayOfObject);
    }

    public static void writeAmAnr(int paramInt1, String paramString1, int paramInt2, String paramString2)
    {
        Object[] arrayOfObject = new Object[4];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = paramString1;
        arrayOfObject[2] = Integer.valueOf(paramInt2);
        arrayOfObject[3] = paramString2;
        EventLog.writeEvent(30008, arrayOfObject);
    }

    public static void writeAmBroadcastDiscardApp(int paramInt1, String paramString1, int paramInt2, String paramString2)
    {
        Object[] arrayOfObject = new Object[4];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = paramString1;
        arrayOfObject[2] = Integer.valueOf(paramInt2);
        arrayOfObject[3] = paramString2;
        EventLog.writeEvent(30025, arrayOfObject);
    }

    public static void writeAmBroadcastDiscardFilter(int paramInt1, String paramString, int paramInt2, int paramInt3)
    {
        Object[] arrayOfObject = new Object[4];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = paramString;
        arrayOfObject[2] = Integer.valueOf(paramInt2);
        arrayOfObject[3] = Integer.valueOf(paramInt3);
        EventLog.writeEvent(30024, arrayOfObject);
    }

    public static void writeAmCrash(int paramInt1, String paramString1, int paramInt2, String paramString2, String paramString3, String paramString4, int paramInt3)
    {
        Object[] arrayOfObject = new Object[7];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = paramString1;
        arrayOfObject[2] = Integer.valueOf(paramInt2);
        arrayOfObject[3] = paramString2;
        arrayOfObject[4] = paramString3;
        arrayOfObject[5] = paramString4;
        arrayOfObject[6] = Integer.valueOf(paramInt3);
        EventLog.writeEvent(30039, arrayOfObject);
    }

    public static void writeAmCreateActivity(int paramInt1, int paramInt2, String paramString1, String paramString2, String paramString3, String paramString4, int paramInt3)
    {
        Object[] arrayOfObject = new Object[7];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = paramString1;
        arrayOfObject[3] = paramString2;
        arrayOfObject[4] = paramString3;
        arrayOfObject[5] = paramString4;
        arrayOfObject[6] = Integer.valueOf(paramInt3);
        EventLog.writeEvent(30005, arrayOfObject);
    }

    public static void writeAmCreateService(int paramInt1, String paramString1, String paramString2, int paramInt2)
    {
        Object[] arrayOfObject = new Object[4];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = paramString1;
        arrayOfObject[2] = paramString2;
        arrayOfObject[3] = Integer.valueOf(paramInt2);
        EventLog.writeEvent(30030, arrayOfObject);
    }

    public static void writeAmCreateTask(int paramInt)
    {
        EventLog.writeEvent(30004, paramInt);
    }

    public static void writeAmDestroyActivity(int paramInt1, int paramInt2, String paramString1, String paramString2)
    {
        Object[] arrayOfObject = new Object[4];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = paramString1;
        arrayOfObject[3] = paramString2;
        EventLog.writeEvent(30018, arrayOfObject);
    }

    public static void writeAmDestroyService(int paramInt1, String paramString, int paramInt2)
    {
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = paramString;
        arrayOfObject[2] = Integer.valueOf(paramInt2);
        EventLog.writeEvent(30031, arrayOfObject);
    }

    public static void writeAmDropProcess(int paramInt)
    {
        EventLog.writeEvent(30033, paramInt);
    }

    public static void writeAmFailedToPause(int paramInt, String paramString1, String paramString2)
    {
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = Integer.valueOf(paramInt);
        arrayOfObject[1] = paramString1;
        arrayOfObject[2] = paramString2;
        EventLog.writeEvent(30012, arrayOfObject);
    }

    public static void writeAmFinishActivity(int paramInt1, int paramInt2, String paramString1, String paramString2)
    {
        Object[] arrayOfObject = new Object[4];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = paramString1;
        arrayOfObject[3] = paramString2;
        EventLog.writeEvent(30001, arrayOfObject);
    }

    public static void writeAmKill(int paramInt1, String paramString1, int paramInt2, String paramString2)
    {
        Object[] arrayOfObject = new Object[4];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = paramString1;
        arrayOfObject[2] = Integer.valueOf(paramInt2);
        arrayOfObject[3] = paramString2;
        EventLog.writeEvent(30023, arrayOfObject);
    }

    public static void writeAmLowMemory(int paramInt)
    {
        EventLog.writeEvent(30017, paramInt);
    }

    public static void writeAmNewIntent(int paramInt1, int paramInt2, String paramString1, String paramString2, String paramString3, String paramString4, int paramInt3)
    {
        Object[] arrayOfObject = new Object[7];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = paramString1;
        arrayOfObject[3] = paramString2;
        arrayOfObject[4] = paramString3;
        arrayOfObject[5] = paramString4;
        arrayOfObject[6] = Integer.valueOf(paramInt3);
        EventLog.writeEvent(30003, arrayOfObject);
    }

    public static void writeAmOnPausedCalled(String paramString)
    {
        EventLog.writeEvent(30021, paramString);
    }

    public static void writeAmOnResumeCalled(String paramString)
    {
        EventLog.writeEvent(30022, paramString);
    }

    public static void writeAmPauseActivity(int paramInt, String paramString)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = Integer.valueOf(paramInt);
        arrayOfObject[1] = paramString;
        EventLog.writeEvent(30013, arrayOfObject);
    }

    public static void writeAmProcBad(int paramInt, String paramString)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = Integer.valueOf(paramInt);
        arrayOfObject[1] = paramString;
        EventLog.writeEvent(30015, arrayOfObject);
    }

    public static void writeAmProcBound(int paramInt, String paramString)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = Integer.valueOf(paramInt);
        arrayOfObject[1] = paramString;
        EventLog.writeEvent(30010, arrayOfObject);
    }

    public static void writeAmProcDied(int paramInt, String paramString)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = Integer.valueOf(paramInt);
        arrayOfObject[1] = paramString;
        EventLog.writeEvent(30011, arrayOfObject);
    }

    public static void writeAmProcGood(int paramInt, String paramString)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = Integer.valueOf(paramInt);
        arrayOfObject[1] = paramString;
        EventLog.writeEvent(30016, arrayOfObject);
    }

    public static void writeAmProcStart(int paramInt1, int paramInt2, String paramString1, String paramString2, String paramString3)
    {
        Object[] arrayOfObject = new Object[5];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = paramString1;
        arrayOfObject[3] = paramString2;
        arrayOfObject[4] = paramString3;
        EventLog.writeEvent(30014, arrayOfObject);
    }

    public static void writeAmProcessCrashedTooMuch(String paramString, int paramInt)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = paramString;
        arrayOfObject[1] = Integer.valueOf(paramInt);
        EventLog.writeEvent(30032, arrayOfObject);
    }

    public static void writeAmProcessStartTimeout(int paramInt1, int paramInt2, String paramString)
    {
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = paramString;
        EventLog.writeEvent(30037, arrayOfObject);
    }

    public static void writeAmProviderLostProcess(String paramString1, int paramInt, String paramString2)
    {
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = paramString1;
        arrayOfObject[1] = Integer.valueOf(paramInt);
        arrayOfObject[2] = paramString2;
        EventLog.writeEvent(30036, arrayOfObject);
    }

    public static void writeAmRelaunchActivity(int paramInt1, int paramInt2, String paramString)
    {
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = paramString;
        EventLog.writeEvent(30020, arrayOfObject);
    }

    public static void writeAmRelaunchResumeActivity(int paramInt1, int paramInt2, String paramString)
    {
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = paramString;
        EventLog.writeEvent(30019, arrayOfObject);
    }

    public static void writeAmRestartActivity(int paramInt1, int paramInt2, String paramString)
    {
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = paramString;
        EventLog.writeEvent(30006, arrayOfObject);
    }

    public static void writeAmResumeActivity(int paramInt1, int paramInt2, String paramString)
    {
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = paramString;
        EventLog.writeEvent(30007, arrayOfObject);
    }

    public static void writeAmScheduleServiceRestart(String paramString, long paramLong)
    {
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = paramString;
        arrayOfObject[1] = Long.valueOf(paramLong);
        EventLog.writeEvent(30035, arrayOfObject);
    }

    public static void writeAmServiceCrashedTooMuch(int paramInt1, String paramString, int paramInt2)
    {
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = paramString;
        arrayOfObject[2] = Integer.valueOf(paramInt2);
        EventLog.writeEvent(30034, arrayOfObject);
    }

    public static void writeAmTaskToFront(int paramInt)
    {
        EventLog.writeEvent(30002, paramInt);
    }

    public static void writeAmWtf(int paramInt1, String paramString1, int paramInt2, String paramString2, String paramString3)
    {
        Object[] arrayOfObject = new Object[5];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = paramString1;
        arrayOfObject[2] = Integer.valueOf(paramInt2);
        arrayOfObject[3] = paramString2;
        arrayOfObject[4] = paramString3;
        EventLog.writeEvent(30040, arrayOfObject);
    }

    public static void writeBootProgressAmsReady(long paramLong)
    {
        EventLog.writeEvent(3040, paramLong);
    }

    public static void writeBootProgressEnableScreen(long paramLong)
    {
        EventLog.writeEvent(3050, paramLong);
    }

    public static void writeConfigurationChanged(int paramInt)
    {
        EventLog.writeEvent(2719, paramInt);
    }

    public static void writeCpu(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
    {
        Object[] arrayOfObject = new Object[6];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        arrayOfObject[2] = Integer.valueOf(paramInt3);
        arrayOfObject[3] = Integer.valueOf(paramInt4);
        arrayOfObject[4] = Integer.valueOf(paramInt5);
        arrayOfObject[5] = Integer.valueOf(paramInt6);
        EventLog.writeEvent(2721, arrayOfObject);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.EventLogTags
 * JD-Core Version:        0.6.2
 */