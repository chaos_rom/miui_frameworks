package com.android.server.am;

import android.graphics.Point;
import com.android.internal.util.MemInfoReader;
import com.android.server.wm.WindowManagerService;

class ProcessList
{
    static final int BACKUP_APP_ADJ = 4;
    static final long CONTENT_APP_IDLE_OFFSET = 15000L;
    static final long EMPTY_APP_IDLE_OFFSET = 120000L;
    static final int FOREGROUND_APP_ADJ = 0;
    static final int HEAVY_WEIGHT_APP_ADJ = 3;
    static final int HIDDEN_APP_MAX_ADJ = 15;
    static int HIDDEN_APP_MIN_ADJ = 0;
    static final int HOME_APP_ADJ = 6;
    static final int MAX_HIDDEN_APPS = 15;
    static final int MIN_CRASH_INTERVAL = 60000;
    static final int MIN_HIDDEN_APPS = 2;
    static final int PAGE_SIZE = 4096;
    static final int PERCEPTIBLE_APP_ADJ = 2;
    static final int PERSISTENT_PROC_ADJ = -12;
    static final int PREVIOUS_APP_ADJ = 7;
    static final int SERVICE_ADJ = 5;
    static final int SERVICE_B_ADJ = 8;
    static final int SYSTEM_ADJ = -16;
    static final int VISIBLE_APP_ADJ = 1;
    private boolean mHaveDisplaySize;
    private final int[] mOomAdj;
    private final long[] mOomMinFree;
    private final long[] mOomMinFreeHigh;
    private final long[] mOomMinFreeLow;
    private final long mTotalMemMb;

    ProcessList()
    {
        int[] arrayOfInt = new int[6];
        arrayOfInt[0] = 0;
        arrayOfInt[1] = 1;
        arrayOfInt[2] = 2;
        arrayOfInt[3] = 4;
        arrayOfInt[4] = HIDDEN_APP_MIN_ADJ;
        arrayOfInt[5] = 15;
        this.mOomAdj = arrayOfInt;
        long[] arrayOfLong1 = new long[6];
        arrayOfLong1[0] = 8192L;
        arrayOfLong1[1] = 12288L;
        arrayOfLong1[2] = 16384L;
        arrayOfLong1[3] = 24576L;
        arrayOfLong1[4] = 28672L;
        arrayOfLong1[5] = 32768L;
        this.mOomMinFreeLow = arrayOfLong1;
        long[] arrayOfLong2 = new long[6];
        arrayOfLong2[0] = 32768L;
        arrayOfLong2[1] = 40960L;
        arrayOfLong2[2] = 49152L;
        arrayOfLong2[3] = 57344L;
        arrayOfLong2[4] = 65536L;
        arrayOfLong2[5] = 81920L;
        this.mOomMinFreeHigh = arrayOfLong2;
        this.mOomMinFree = new long[this.mOomAdj.length];
        MemInfoReader localMemInfoReader = new MemInfoReader();
        localMemInfoReader.readMemInfo();
        this.mTotalMemMb = (localMemInfoReader.getTotalSize() / 1048576L);
        updateOomLevels(0, 0, false);
    }

    private void updateOomLevels(int paramInt1, int paramInt2, boolean paramBoolean)
    {
        float f1 = (float)(this.mTotalMemMb - 300L) / 400.0F;
        float f2 = (paramInt1 * paramInt2 - 153600) / 870400;
        StringBuilder localStringBuilder1 = new StringBuilder();
        StringBuilder localStringBuilder2 = new StringBuilder();
        float f3;
        if (f1 > f2)
        {
            f3 = f1;
            if (f3 >= 0.0F)
                break label188;
            f3 = 0.0F;
        }
        while (true)
        {
            for (int i = 0; i < this.mOomAdj.length; i++)
            {
                long l1 = this.mOomMinFreeLow[i];
                long l2 = this.mOomMinFreeHigh[i];
                this.mOomMinFree[i] = (()((float)l1 + f3 * (float)(l2 - l1)));
                if (i > 0)
                {
                    localStringBuilder1.append(',');
                    localStringBuilder2.append(',');
                }
                localStringBuilder1.append(this.mOomAdj[i]);
                localStringBuilder2.append(1024L * this.mOomMinFree[i] / 4096L);
            }
            f3 = f2;
            break;
            label188: if (f3 > 1.0F)
                f3 = 1.0F;
        }
        if (paramBoolean)
        {
            writeFile("/sys/module/lowmemorykiller/parameters/adj", localStringBuilder1.toString());
            writeFile("/sys/module/lowmemorykiller/parameters/minfree", localStringBuilder2.toString());
        }
    }

    // ERROR //
    private void writeFile(java.lang.String paramString1, java.lang.String paramString2)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_3
        //     2: new 145	java/io/FileOutputStream
        //     5: dup
        //     6: aload_1
        //     7: invokespecial 148	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
        //     10: astore 4
        //     12: aload 4
        //     14: aload_2
        //     15: invokevirtual 154	java/lang/String:getBytes	()[B
        //     18: invokevirtual 158	java/io/FileOutputStream:write	([B)V
        //     21: aload 4
        //     23: ifnull +91 -> 114
        //     26: aload 4
        //     28: invokevirtual 161	java/io/FileOutputStream:close	()V
        //     31: return
        //     32: astore 10
        //     34: goto -3 -> 31
        //     37: astore 11
        //     39: ldc 163
        //     41: new 114	java/lang/StringBuilder
        //     44: dup
        //     45: invokespecial 115	java/lang/StringBuilder:<init>	()V
        //     48: ldc 165
        //     50: invokevirtual 168	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     53: aload_1
        //     54: invokevirtual 168	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     57: invokevirtual 135	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     60: invokestatic 174	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     63: pop
        //     64: aload_3
        //     65: ifnull -34 -> 31
        //     68: aload_3
        //     69: invokevirtual 161	java/io/FileOutputStream:close	()V
        //     72: goto -41 -> 31
        //     75: astore 9
        //     77: goto -46 -> 31
        //     80: astore 6
        //     82: aload_3
        //     83: ifnull +7 -> 90
        //     86: aload_3
        //     87: invokevirtual 161	java/io/FileOutputStream:close	()V
        //     90: aload 6
        //     92: athrow
        //     93: astore 7
        //     95: goto -5 -> 90
        //     98: astore 6
        //     100: aload 4
        //     102: astore_3
        //     103: goto -21 -> 82
        //     106: astore 5
        //     108: aload 4
        //     110: astore_3
        //     111: goto -72 -> 39
        //     114: goto -83 -> 31
        //
        // Exception table:
        //     from	to	target	type
        //     26	31	32	java/io/IOException
        //     2	12	37	java/io/IOException
        //     68	72	75	java/io/IOException
        //     2	12	80	finally
        //     39	64	80	finally
        //     86	90	93	java/io/IOException
        //     12	21	98	finally
        //     12	21	106	java/io/IOException
    }

    void applyDisplaySize(WindowManagerService paramWindowManagerService)
    {
        if (!this.mHaveDisplaySize)
        {
            Point localPoint = new Point();
            paramWindowManagerService.getInitialDisplaySize(localPoint);
            if ((localPoint.x != 0) && (localPoint.y != 0))
            {
                updateOomLevels(localPoint.x, localPoint.y, true);
                this.mHaveDisplaySize = true;
            }
        }
    }

    long getMemLevel(int paramInt)
    {
        int i = 0;
        if (i < this.mOomAdj.length)
            if (paramInt > this.mOomAdj[i]);
        for (long l = 1024L * this.mOomMinFree[i]; ; l = 1024L * this.mOomMinFree[(-1 + this.mOomAdj.length)])
        {
            return l;
            i++;
            break;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.ProcessList
 * JD-Core Version:        0.6.2
 */