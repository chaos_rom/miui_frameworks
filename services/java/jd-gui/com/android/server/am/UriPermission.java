package com.android.server.am;

import android.net.Uri;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Iterator;

class UriPermission
{
    int globalModeFlags = 0;
    int modeFlags = 0;
    final HashSet<UriPermissionOwner> readOwners = new HashSet();
    String stringName;
    final int uid;
    final Uri uri;
    final HashSet<UriPermissionOwner> writeOwners = new HashSet();

    UriPermission(int paramInt, Uri paramUri)
    {
        this.uid = paramInt;
        this.uri = paramUri;
    }

    void clearModes(int paramInt)
    {
        if ((paramInt & 0x1) != 0)
        {
            this.globalModeFlags = (0xFFFFFFFE & this.globalModeFlags);
            this.modeFlags = (0xFFFFFFFE & this.modeFlags);
            if (this.readOwners.size() > 0)
            {
                Iterator localIterator2 = this.readOwners.iterator();
                while (localIterator2.hasNext())
                    ((UriPermissionOwner)localIterator2.next()).removeReadPermission(this);
                this.readOwners.clear();
            }
        }
        if ((paramInt & 0x2) != 0)
        {
            this.globalModeFlags = (0xFFFFFFFD & this.globalModeFlags);
            this.modeFlags = (0xFFFFFFFD & this.modeFlags);
            if (this.writeOwners.size() > 0)
            {
                Iterator localIterator1 = this.writeOwners.iterator();
                while (localIterator1.hasNext())
                    ((UriPermissionOwner)localIterator1.next()).removeWritePermission(this);
                this.writeOwners.clear();
            }
        }
    }

    void dump(PrintWriter paramPrintWriter, String paramString)
    {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("modeFlags=0x");
        paramPrintWriter.print(Integer.toHexString(this.modeFlags));
        paramPrintWriter.print(" uid=");
        paramPrintWriter.print(this.uid);
        paramPrintWriter.print(" globalModeFlags=0x");
        paramPrintWriter.println(Integer.toHexString(this.globalModeFlags));
        if (this.readOwners.size() != 0)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.println("readOwners:");
            Iterator localIterator2 = this.readOwners.iterator();
            while (localIterator2.hasNext())
            {
                UriPermissionOwner localUriPermissionOwner2 = (UriPermissionOwner)localIterator2.next();
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("    * ");
                paramPrintWriter.println(localUriPermissionOwner2);
            }
        }
        if (this.writeOwners.size() != 0)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.println("writeOwners:");
            Iterator localIterator1 = this.writeOwners.iterator();
            while (localIterator1.hasNext())
            {
                UriPermissionOwner localUriPermissionOwner1 = (UriPermissionOwner)localIterator1.next();
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("    * ");
                paramPrintWriter.println(localUriPermissionOwner1);
            }
        }
    }

    public String toString()
    {
        String str;
        if (this.stringName != null)
            str = this.stringName;
        while (true)
        {
            return str;
            StringBuilder localStringBuilder = new StringBuilder(128);
            localStringBuilder.append("UriPermission{");
            localStringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
            localStringBuilder.append(' ');
            localStringBuilder.append(this.uri);
            localStringBuilder.append('}');
            str = localStringBuilder.toString();
            this.stringName = str;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.UriPermission
 * JD-Core Version:        0.6.2
 */