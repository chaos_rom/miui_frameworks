package com.android.server.am;

import android.app.IActivityManager.ContentProviderHolder;
import android.content.ComponentName;
import android.content.IContentProvider;
import android.content.pm.ApplicationInfo;
import android.content.pm.ProviderInfo;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.RemoteException;
import android.util.Slog;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

class ContentProviderRecord
{
    final ApplicationInfo appInfo;
    final ArrayList<ContentProviderConnection> connections = new ArrayList();
    int externalProcessNoHandleCount;
    HashMap<IBinder, ExternalProcessHandle> externalProcessTokenToHandle;
    public final ProviderInfo info;
    ProcessRecord launchingApp;
    final ComponentName name;
    public boolean noReleaseNeeded;
    ProcessRecord proc;
    public IContentProvider provider;
    final ActivityManagerService service;
    String shortStringName;
    String stringName;
    final int uid;

    public ContentProviderRecord(ActivityManagerService paramActivityManagerService, ProviderInfo paramProviderInfo, ApplicationInfo paramApplicationInfo, ComponentName paramComponentName)
    {
        this.service = paramActivityManagerService;
        this.info = paramProviderInfo;
        this.uid = paramApplicationInfo.uid;
        this.appInfo = paramApplicationInfo;
        this.name = paramComponentName;
        if ((this.uid == 0) || (this.uid == 1000));
        for (boolean bool = true; ; bool = false)
        {
            this.noReleaseNeeded = bool;
            return;
        }
    }

    public ContentProviderRecord(ContentProviderRecord paramContentProviderRecord)
    {
        this.service = paramContentProviderRecord.service;
        this.info = paramContentProviderRecord.info;
        this.uid = paramContentProviderRecord.uid;
        this.appInfo = paramContentProviderRecord.appInfo;
        this.name = paramContentProviderRecord.name;
        this.noReleaseNeeded = paramContentProviderRecord.noReleaseNeeded;
    }

    private void removeExternalProcessHandleInternalLocked(IBinder paramIBinder)
    {
        ((ExternalProcessHandle)this.externalProcessTokenToHandle.get(paramIBinder)).unlinkFromOwnDeathLocked();
        this.externalProcessTokenToHandle.remove(paramIBinder);
        if (this.externalProcessTokenToHandle.size() == 0)
            this.externalProcessTokenToHandle = null;
    }

    public void addExternalProcessHandleLocked(IBinder paramIBinder)
    {
        if (paramIBinder == null)
            this.externalProcessNoHandleCount = (1 + this.externalProcessNoHandleCount);
        while (true)
        {
            return;
            if (this.externalProcessTokenToHandle == null)
                this.externalProcessTokenToHandle = new HashMap();
            ExternalProcessHandle localExternalProcessHandle = (ExternalProcessHandle)this.externalProcessTokenToHandle.get(paramIBinder);
            if (localExternalProcessHandle == null)
            {
                localExternalProcessHandle = new ExternalProcessHandle(paramIBinder);
                this.externalProcessTokenToHandle.put(paramIBinder, localExternalProcessHandle);
            }
            ExternalProcessHandle.access$008(localExternalProcessHandle);
        }
    }

    public boolean canRunHere(ProcessRecord paramProcessRecord)
    {
        if (((this.info.multiprocess) || (this.info.processName.equals(paramProcessRecord.processName))) && ((this.uid == 1000) || (this.uid == paramProcessRecord.info.uid)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    void dump(PrintWriter paramPrintWriter, String paramString, boolean paramBoolean)
    {
        if (paramBoolean)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("package=");
            paramPrintWriter.print(this.info.applicationInfo.packageName);
            paramPrintWriter.print(" process=");
            paramPrintWriter.println(this.info.processName);
        }
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("proc=");
        paramPrintWriter.println(this.proc);
        if (this.launchingApp != null)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("launchingApp=");
            paramPrintWriter.println(this.launchingApp);
        }
        if (paramBoolean)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("uid=");
            paramPrintWriter.print(this.uid);
            paramPrintWriter.print(" provider=");
            paramPrintWriter.println(this.provider);
        }
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("authority=");
        paramPrintWriter.println(this.info.authority);
        if ((paramBoolean) && ((this.info.isSyncable) || (this.info.multiprocess) || (this.info.initOrder != 0)))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("isSyncable=");
            paramPrintWriter.print(this.info.isSyncable);
            paramPrintWriter.print(" multiprocess=");
            paramPrintWriter.print(this.info.multiprocess);
            paramPrintWriter.print(" initOrder=");
            paramPrintWriter.println(this.info.initOrder);
        }
        if (paramBoolean)
            if (hasExternalProcessHandles())
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("externals=");
                paramPrintWriter.println(this.externalProcessTokenToHandle.size());
            }
        while (this.connections.size() > 0)
        {
            if (paramBoolean)
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.println("Connections:");
            }
            for (int i = 0; i < this.connections.size(); i++)
            {
                ContentProviderConnection localContentProviderConnection = (ContentProviderConnection)this.connections.get(i);
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("    -> ");
                paramPrintWriter.println(localContentProviderConnection.toClientString());
                if (localContentProviderConnection.provider != this)
                {
                    paramPrintWriter.print(paramString);
                    paramPrintWriter.print("        *** WRONG PROVIDER: ");
                    paramPrintWriter.println(localContentProviderConnection.provider);
                }
            }
            if ((this.connections.size() > 0) || (this.externalProcessNoHandleCount > 0))
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print(this.connections.size());
                paramPrintWriter.print(" connections, ");
                paramPrintWriter.print(this.externalProcessNoHandleCount);
                paramPrintWriter.println(" external handles");
            }
        }
    }

    public boolean hasExternalProcessHandles()
    {
        if ((this.externalProcessTokenToHandle != null) || (this.externalProcessNoHandleCount > 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public IActivityManager.ContentProviderHolder newHolder(ContentProviderConnection paramContentProviderConnection)
    {
        IActivityManager.ContentProviderHolder localContentProviderHolder = new IActivityManager.ContentProviderHolder(this.info);
        localContentProviderHolder.provider = this.provider;
        localContentProviderHolder.noReleaseNeeded = this.noReleaseNeeded;
        localContentProviderHolder.connection = paramContentProviderConnection;
        return localContentProviderHolder;
    }

    public boolean removeExternalProcessHandleLocked(IBinder paramIBinder)
    {
        boolean bool = true;
        int i;
        if (hasExternalProcessHandles())
        {
            i = 0;
            if (this.externalProcessTokenToHandle != null)
            {
                ExternalProcessHandle localExternalProcessHandle = (ExternalProcessHandle)this.externalProcessTokenToHandle.get(paramIBinder);
                if (localExternalProcessHandle != null)
                {
                    i = 1;
                    ExternalProcessHandle.access$010(localExternalProcessHandle);
                    if (localExternalProcessHandle.mAcquisitionCount == 0)
                        removeExternalProcessHandleInternalLocked(paramIBinder);
                }
            }
        }
        while (true)
        {
            return bool;
            if (i == 0)
                this.externalProcessNoHandleCount = (-1 + this.externalProcessNoHandleCount);
            else
                bool = false;
        }
    }

    public String toShortString()
    {
        String str;
        if (this.shortStringName != null)
            str = this.shortStringName;
        while (true)
        {
            return str;
            StringBuilder localStringBuilder = new StringBuilder(128);
            localStringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
            localStringBuilder.append('/');
            localStringBuilder.append(this.name.flattenToShortString());
            str = localStringBuilder.toString();
            this.shortStringName = str;
        }
    }

    public String toString()
    {
        String str;
        if (this.stringName != null)
            str = this.stringName;
        while (true)
        {
            return str;
            StringBuilder localStringBuilder = new StringBuilder(128);
            localStringBuilder.append("ContentProviderRecord{");
            localStringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
            localStringBuilder.append(' ');
            localStringBuilder.append(this.name.flattenToShortString());
            localStringBuilder.append('}');
            str = localStringBuilder.toString();
            this.stringName = str;
        }
    }

    private class ExternalProcessHandle
        implements IBinder.DeathRecipient
    {
        private static final String LOG_TAG = "ExternalProcessHanldle";
        private int mAcquisitionCount;
        private final IBinder mToken;

        public ExternalProcessHandle(IBinder arg2)
        {
            Object localObject;
            this.mToken = localObject;
            try
            {
                localObject.linkToDeath(this, 0);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Slog.e("ExternalProcessHanldle", "Couldn't register for death for token: " + this.mToken, localRemoteException);
            }
        }

        public void binderDied()
        {
            synchronized (ContentProviderRecord.this.service)
            {
                if ((ContentProviderRecord.this.hasExternalProcessHandles()) && (ContentProviderRecord.this.externalProcessTokenToHandle.get(this.mToken) != null))
                    ContentProviderRecord.this.removeExternalProcessHandleInternalLocked(this.mToken);
                return;
            }
        }

        public void unlinkFromOwnDeathLocked()
        {
            this.mToken.unlinkToDeath(this, 0);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.ContentProviderRecord
 * JD-Core Version:        0.6.2
 */