package com.android.server.am;

import android.app.Dialog;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;

public class CompatModeDialog extends Dialog
{
    final CheckBox mAlwaysShow;
    final ApplicationInfo mAppInfo;
    final Switch mCompatEnabled;
    final View mHint;
    final ActivityManagerService mService;

    public CompatModeDialog(ActivityManagerService paramActivityManagerService, Context paramContext, ApplicationInfo paramApplicationInfo)
    {
        super(paramContext, 16973936);
        setCancelable(true);
        setCanceledOnTouchOutside(true);
        getWindow().requestFeature(1);
        getWindow().setType(2002);
        getWindow().setGravity(81);
        this.mService = paramActivityManagerService;
        this.mAppInfo = paramApplicationInfo;
        setContentView(17367080);
        this.mCompatEnabled = ((Switch)findViewById(16908890));
        this.mCompatEnabled.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
            {
                while (true)
                {
                    synchronized (CompatModeDialog.this.mService)
                    {
                        CompatModePackages localCompatModePackages = CompatModeDialog.this.mService.mCompatModePackages;
                        String str = CompatModeDialog.this.mAppInfo.packageName;
                        if (CompatModeDialog.this.mCompatEnabled.isChecked())
                        {
                            i = 1;
                            localCompatModePackages.setPackageScreenCompatModeLocked(str, i);
                            CompatModeDialog.this.updateControls();
                            return;
                        }
                    }
                    int i = 0;
                }
            }
        });
        this.mAlwaysShow = ((CheckBox)findViewById(16908891));
        this.mAlwaysShow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
            {
                synchronized (CompatModeDialog.this.mService)
                {
                    CompatModeDialog.this.mService.mCompatModePackages.setPackageAskCompatModeLocked(CompatModeDialog.this.mAppInfo.packageName, CompatModeDialog.this.mAlwaysShow.isChecked());
                    CompatModeDialog.this.updateControls();
                    return;
                }
            }
        });
        this.mHint = findViewById(16908892);
        updateControls();
    }

    void updateControls()
    {
        int i = 1;
        int j = 0;
        while (true)
        {
            synchronized (this.mService)
            {
                int k = this.mService.mCompatModePackages.computeCompatModeLocked(this.mAppInfo);
                Switch localSwitch = this.mCompatEnabled;
                if (k == i)
                {
                    localSwitch.setChecked(i);
                    boolean bool = this.mService.mCompatModePackages.getPackageAskCompatModeLocked(this.mAppInfo.packageName);
                    this.mAlwaysShow.setChecked(bool);
                    View localView = this.mHint;
                    if (bool)
                        j = 4;
                    localView.setVisibility(j);
                    return;
                }
            }
            i = 0;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.CompatModeDialog
 * JD-Core Version:        0.6.2
 */