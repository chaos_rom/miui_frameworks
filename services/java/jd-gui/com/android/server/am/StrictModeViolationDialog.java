package com.android.server.am;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Message;
import android.view.Window;
import java.util.HashSet;

class StrictModeViolationDialog extends BaseErrorDialog
{
    static final int ACTION_OK = 0;
    static final int ACTION_OK_AND_REPORT = 1;
    static final long DISMISS_TIMEOUT = 60000L;
    private static final String TAG = "StrictModeViolationDialog";
    private final Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            synchronized (StrictModeViolationDialog.this.mProc)
            {
                if ((StrictModeViolationDialog.this.mProc != null) && (StrictModeViolationDialog.this.mProc.crashDialog == StrictModeViolationDialog.this))
                    StrictModeViolationDialog.this.mProc.crashDialog = null;
                StrictModeViolationDialog.this.mResult.set(paramAnonymousMessage.what);
                StrictModeViolationDialog.this.dismiss();
                return;
            }
        }
    };
    private final ProcessRecord mProc;
    private final AppErrorResult mResult;

    public StrictModeViolationDialog(Context paramContext, AppErrorResult paramAppErrorResult, ProcessRecord paramProcessRecord)
    {
        super(paramContext);
        Resources localResources = paramContext.getResources();
        this.mProc = paramProcessRecord;
        this.mResult = paramAppErrorResult;
        if (paramProcessRecord.pkgList.size() == 1)
        {
            CharSequence localCharSequence = paramContext.getPackageManager().getApplicationLabel(paramProcessRecord.info);
            if (localCharSequence != null)
            {
                Object[] arrayOfObject2 = new Object[2];
                arrayOfObject2[0] = localCharSequence.toString();
                arrayOfObject2[1] = paramProcessRecord.info.processName;
                setMessage(localResources.getString(17040353, arrayOfObject2));
            }
        }
        while (true)
        {
            setCancelable(false);
            setButton(-1, localResources.getText(17040443), this.mHandler.obtainMessage(0));
            if (paramProcessRecord.errorReportReceiver != null)
                setButton(-2, localResources.getText(17040344), this.mHandler.obtainMessage(1));
            setTitle(localResources.getText(17040335));
            getWindow().addFlags(1073741824);
            getWindow().setTitle("Strict Mode Violation: " + paramProcessRecord.info.processName);
            this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(0), 60000L);
            return;
            String str = paramProcessRecord.processName;
            Object[] arrayOfObject1 = new Object[1];
            arrayOfObject1[0] = str.toString();
            setMessage(localResources.getString(17040354, arrayOfObject1));
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.StrictModeViolationDialog
 * JD-Core Version:        0.6.2
 */