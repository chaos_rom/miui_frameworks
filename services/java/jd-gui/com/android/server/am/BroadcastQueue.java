package com.android.server.am;

import android.app.IApplicationThread;
import android.content.ComponentName;
import android.content.IIntentReceiver;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.EventLog;
import android.util.Slog;
import android.util.SparseArray;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class BroadcastQueue
{
    static final int BROADCAST_INTENT_MSG = 200;
    static final int BROADCAST_TIMEOUT_MSG = 201;
    static final boolean DEBUG_BROADCAST = false;
    static final boolean DEBUG_BROADCAST_LIGHT = false;
    static final boolean DEBUG_MU = false;
    static final int MAX_BROADCAST_HISTORY = 25;
    static final String TAG = "BroadcastQueue";
    static final String TAG_MU = "ActivityManagerServiceMU";
    final BroadcastRecord[] mBroadcastHistory = new BroadcastRecord[25];
    boolean mBroadcastsScheduled = false;
    final Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            switch (paramAnonymousMessage.what)
            {
            default:
            case 200:
            case 201:
            }
            while (true)
            {
                return;
                BroadcastQueue.this.processNextBroadcast(true);
                continue;
                synchronized (BroadcastQueue.this.mService)
                {
                    BroadcastQueue.this.broadcastTimeoutLocked(true);
                }
            }
        }
    };
    final ArrayList<BroadcastRecord> mOrderedBroadcasts = new ArrayList();
    final ArrayList<BroadcastRecord> mParallelBroadcasts = new ArrayList();
    BroadcastRecord mPendingBroadcast = null;
    int mPendingBroadcastRecvIndex;
    boolean mPendingBroadcastTimeoutMessage;
    final String mQueueName;
    final ActivityManagerService mService;
    final long mTimeoutPeriod;

    BroadcastQueue(ActivityManagerService paramActivityManagerService, String paramString, long paramLong)
    {
        this.mService = paramActivityManagerService;
        this.mQueueName = paramString;
        this.mTimeoutPeriod = paramLong;
    }

    private final void addBroadcastToHistoryLocked(BroadcastRecord paramBroadcastRecord)
    {
        if (paramBroadcastRecord.callingUid < 0);
        while (true)
        {
            return;
            System.arraycopy(this.mBroadcastHistory, 0, this.mBroadcastHistory, 1, 24);
            paramBroadcastRecord.finishTime = SystemClock.uptimeMillis();
            this.mBroadcastHistory[0] = paramBroadcastRecord;
        }
    }

    private final void deliverToRegisteredReceiverLocked(BroadcastRecord paramBroadcastRecord, BroadcastFilter paramBroadcastFilter, boolean paramBoolean)
    {
        int i = 0;
        if ((paramBroadcastFilter.requiredPermission != null) && (this.mService.checkComponentPermission(paramBroadcastFilter.requiredPermission, paramBroadcastRecord.callingPid, paramBroadcastRecord.callingUid, -1, true) != 0))
        {
            Slog.w("BroadcastQueue", "Permission Denial: broadcasting " + paramBroadcastRecord.intent.toString() + " from " + paramBroadcastRecord.callerPackage + " (pid=" + paramBroadcastRecord.callingPid + ", uid=" + paramBroadcastRecord.callingUid + ")" + " requires " + paramBroadcastFilter.requiredPermission + " due to registered receiver " + paramBroadcastFilter);
            i = 1;
        }
        if ((paramBroadcastRecord.requiredPermission != null) && (this.mService.checkComponentPermission(paramBroadcastRecord.requiredPermission, paramBroadcastFilter.receiverList.pid, paramBroadcastFilter.receiverList.uid, -1, true) != 0))
        {
            Slog.w("BroadcastQueue", "Permission Denial: receiving " + paramBroadcastRecord.intent.toString() + " to " + paramBroadcastFilter.receiverList.app + " (pid=" + paramBroadcastFilter.receiverList.pid + ", uid=" + paramBroadcastFilter.receiverList.uid + ")" + " requires " + paramBroadcastRecord.requiredPermission + " due to sender " + paramBroadcastRecord.callerPackage + " (uid " + paramBroadcastRecord.callingUid + ")");
            i = 1;
        }
        if (i == 0)
            if (paramBoolean)
            {
                paramBroadcastRecord.receiver = paramBroadcastFilter.receiverList.receiver.asBinder();
                paramBroadcastRecord.curFilter = paramBroadcastFilter;
                paramBroadcastFilter.receiverList.curBroadcast = paramBroadcastRecord;
                paramBroadcastRecord.state = 2;
                if (paramBroadcastFilter.receiverList.app != null)
                {
                    paramBroadcastRecord.curApp = paramBroadcastFilter.receiverList.app;
                    paramBroadcastFilter.receiverList.app.curReceiver = paramBroadcastRecord;
                    this.mService.updateOomAdjLocked();
                }
            }
        try
        {
            performReceiveLocked(paramBroadcastFilter.receiverList.app, paramBroadcastFilter.receiverList.receiver, new Intent(paramBroadcastRecord.intent), paramBroadcastRecord.resultCode, paramBroadcastRecord.resultData, paramBroadcastRecord.resultExtras, paramBroadcastRecord.ordered, paramBroadcastRecord.initialSticky);
            if (paramBoolean)
                paramBroadcastRecord.state = 3;
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Slog.w("BroadcastQueue", "Failure sending broadcast " + paramBroadcastRecord.intent, localRemoteException);
                if (paramBoolean)
                {
                    paramBroadcastRecord.receiver = null;
                    paramBroadcastRecord.curFilter = null;
                    paramBroadcastFilter.receiverList.curBroadcast = null;
                    if (paramBroadcastFilter.receiverList.app != null)
                        paramBroadcastFilter.receiverList.app.curReceiver = null;
                }
            }
        }
    }

    private static void performReceiveLocked(ProcessRecord paramProcessRecord, IIntentReceiver paramIIntentReceiver, Intent paramIntent, int paramInt, String paramString, Bundle paramBundle, boolean paramBoolean1, boolean paramBoolean2)
        throws RemoteException
    {
        if ((paramProcessRecord != null) && (paramProcessRecord.thread != null))
            paramProcessRecord.thread.scheduleRegisteredReceiver(paramIIntentReceiver, paramIntent, paramInt, paramString, paramBundle, paramBoolean1, paramBoolean2);
        while (true)
        {
            return;
            paramIIntentReceiver.performReceive(paramIntent, paramInt, paramString, paramBundle, paramBoolean1, paramBoolean2);
        }
    }

    private final void processCurBroadcastLocked(BroadcastRecord paramBroadcastRecord, ProcessRecord paramProcessRecord)
        throws RemoteException
    {
        if (paramProcessRecord.thread == null)
            throw new RemoteException();
        paramBroadcastRecord.receiver = paramProcessRecord.thread.asBinder();
        paramBroadcastRecord.curApp = paramProcessRecord;
        paramProcessRecord.curReceiver = paramBroadcastRecord;
        this.mService.updateLruProcessLocked(paramProcessRecord, true, true);
        paramBroadcastRecord.intent.setComponent(paramBroadcastRecord.curComponent);
        try
        {
            this.mService.ensurePackageDexOpt(paramBroadcastRecord.intent.getComponent().getPackageName());
            paramProcessRecord.thread.scheduleReceiver(new Intent(paramBroadcastRecord.intent), paramBroadcastRecord.curReceiver, this.mService.compatibilityInfoForPackageLocked(paramBroadcastRecord.curReceiver.applicationInfo), paramBroadcastRecord.resultCode, paramBroadcastRecord.resultData, paramBroadcastRecord.resultExtras, paramBroadcastRecord.ordered);
            if (1 == 0)
            {
                paramBroadcastRecord.receiver = null;
                paramBroadcastRecord.curApp = null;
                paramProcessRecord.curReceiver = null;
            }
            return;
        }
        finally
        {
            if (0 == 0)
            {
                paramBroadcastRecord.receiver = null;
                paramBroadcastRecord.curApp = null;
                paramProcessRecord.curReceiver = null;
            }
        }
    }

    final void broadcastTimeoutLocked(boolean paramBoolean)
    {
        if (paramBoolean)
            this.mPendingBroadcastTimeoutMessage = false;
        if (this.mOrderedBroadcasts.size() == 0);
        BroadcastRecord localBroadcastRecord;
        while (true)
        {
            return;
            long l1 = SystemClock.uptimeMillis();
            localBroadcastRecord = (BroadcastRecord)this.mOrderedBroadcasts.get(0);
            if (paramBoolean)
            {
                if (this.mService.mDidDexOpt)
                {
                    this.mService.mDidDexOpt = false;
                    setBroadcastTimeoutLocked(SystemClock.uptimeMillis() + this.mTimeoutPeriod);
                }
                else if (this.mService.mProcessesReady)
                {
                    long l2 = localBroadcastRecord.receiverTime + this.mTimeoutPeriod;
                    if (l2 > l1)
                        setBroadcastTimeoutLocked(l2);
                }
            }
            else
            {
                Slog.w("BroadcastQueue", "Timeout of broadcast " + localBroadcastRecord + " - receiver=" + localBroadcastRecord.receiver + ", started " + (l1 - localBroadcastRecord.receiverTime) + "ms ago");
                localBroadcastRecord.receiverTime = l1;
                localBroadcastRecord.anrCount = (1 + localBroadcastRecord.anrCount);
                if (localBroadcastRecord.nextReceiver > 0)
                    break;
                Slog.w("BroadcastQueue", "Timeout on receiver with nextReceiver <= 0");
            }
        }
        ProcessRecord localProcessRecord = null;
        String str = null;
        Object localObject1 = localBroadcastRecord.receivers.get(-1 + localBroadcastRecord.nextReceiver);
        Slog.w("BroadcastQueue", "Receiver during timeout: " + localObject1);
        logBroadcastReceiverDiscardLocked(localBroadcastRecord);
        BroadcastFilter localBroadcastFilter;
        if ((localObject1 instanceof BroadcastFilter))
        {
            localBroadcastFilter = (BroadcastFilter)localObject1;
            if ((localBroadcastFilter.receiverList.pid == 0) || (localBroadcastFilter.receiverList.pid == ActivityManagerService.MY_PID));
        }
        while (true)
        {
            synchronized (this.mService.mPidsSelfLocked)
            {
                localProcessRecord = (ProcessRecord)this.mService.mPidsSelfLocked.get(localBroadcastFilter.receiverList.pid);
                if (localProcessRecord != null)
                    str = "Broadcast of " + localBroadcastRecord.intent.toString();
                if (this.mPendingBroadcast == localBroadcastRecord)
                    this.mPendingBroadcast = null;
                finishReceiverLocked(localBroadcastRecord, localBroadcastRecord.resultCode, localBroadcastRecord.resultData, localBroadcastRecord.resultExtras, localBroadcastRecord.resultAbort, true);
                scheduleBroadcastsLocked();
                if (str == null)
                    break;
                this.mHandler.post(new AppNotResponding(localProcessRecord, str));
            }
            localProcessRecord = localBroadcastRecord.curApp;
        }
    }

    final void cancelBroadcastTimeoutLocked()
    {
        if (this.mPendingBroadcastTimeoutMessage)
        {
            this.mHandler.removeMessages(201, this);
            this.mPendingBroadcastTimeoutMessage = false;
        }
    }

    final boolean dumpLocked(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString, int paramInt, boolean paramBoolean1, String paramString, boolean paramBoolean2)
    {
        if ((this.mParallelBroadcasts.size() > 0) || (this.mOrderedBroadcasts.size() > 0) || (this.mPendingBroadcast != null))
        {
            int i = 0;
            int j = -1 + this.mParallelBroadcasts.size();
            if (j >= 0)
            {
                BroadcastRecord localBroadcastRecord3 = (BroadcastRecord)this.mParallelBroadcasts.get(j);
                if ((paramString != null) && (!paramString.equals(localBroadcastRecord3.callerPackage)));
                while (true)
                {
                    j--;
                    break;
                    if (i == 0)
                    {
                        if (paramBoolean2)
                        {
                            paramPrintWriter.println();
                            paramBoolean2 = false;
                        }
                        i = 1;
                        paramPrintWriter.println("    Active broadcasts [" + this.mQueueName + "]:");
                    }
                    paramPrintWriter.println("    Broadcast #" + j + ":");
                    localBroadcastRecord3.dump(paramPrintWriter, "        ");
                }
            }
            paramBoolean2 = true;
            int k = -1 + this.mOrderedBroadcasts.size();
            if (k >= 0)
            {
                BroadcastRecord localBroadcastRecord2 = (BroadcastRecord)this.mOrderedBroadcasts.get(k);
                if ((paramString != null) && (!paramString.equals(localBroadcastRecord2.callerPackage)));
                while (true)
                {
                    k--;
                    break;
                    if (0 == 0)
                    {
                        if (paramBoolean2)
                            paramPrintWriter.println();
                        paramBoolean2 = true;
                        paramPrintWriter.println("    Active ordered broadcasts [" + this.mQueueName + "]:");
                    }
                    paramPrintWriter.println("    Ordered Broadcast #" + k + ":");
                    ((BroadcastRecord)this.mOrderedBroadcasts.get(k)).dump(paramPrintWriter, "        ");
                }
            }
            if ((paramString == null) || ((this.mPendingBroadcast != null) && (paramString.equals(this.mPendingBroadcast.callerPackage))))
            {
                if (paramBoolean2)
                    paramPrintWriter.println();
                paramPrintWriter.println("    Pending broadcast [" + this.mQueueName + "]:");
                if (this.mPendingBroadcast == null)
                    break label461;
                this.mPendingBroadcast.dump(paramPrintWriter, "        ");
            }
        }
        int m;
        int n;
        BroadcastRecord localBroadcastRecord1;
        while (true)
        {
            paramBoolean2 = true;
            m = 0;
            n = 0;
            if (n < 25)
            {
                localBroadcastRecord1 = this.mBroadcastHistory[n];
                if (localBroadcastRecord1 != null)
                    break;
            }
            label458: return paramBoolean2;
            label461: paramPrintWriter.println("        (null)");
        }
        if ((paramString != null) && (!paramString.equals(localBroadcastRecord1.callerPackage)));
        while (true)
        {
            n++;
            break;
            if (m == 0)
            {
                if (paramBoolean2)
                    paramPrintWriter.println();
                paramBoolean2 = true;
                paramPrintWriter.println("    Historical broadcasts [" + this.mQueueName + "]:");
                m = 1;
            }
            if (paramBoolean1)
            {
                paramPrintWriter.print("    Historical Broadcast #");
                paramPrintWriter.print(n);
                paramPrintWriter.println(":");
                localBroadcastRecord1.dump(paramPrintWriter, "        ");
            }
            else
            {
                if (n >= 50)
                {
                    paramPrintWriter.println("    ...");
                    break label458;
                }
                paramPrintWriter.print("    #");
                paramPrintWriter.print(n);
                paramPrintWriter.print(": ");
                paramPrintWriter.println(localBroadcastRecord1);
            }
        }
    }

    public void enqueueOrderedBroadcastLocked(BroadcastRecord paramBroadcastRecord)
    {
        this.mOrderedBroadcasts.add(paramBroadcastRecord);
    }

    public void enqueueParallelBroadcastLocked(BroadcastRecord paramBroadcastRecord)
    {
        this.mParallelBroadcasts.add(paramBroadcastRecord);
    }

    public boolean finishReceiverLocked(BroadcastRecord paramBroadcastRecord, int paramInt, String paramString, Bundle paramBundle, boolean paramBoolean1, boolean paramBoolean2)
    {
        boolean bool = false;
        int i = paramBroadcastRecord.state;
        paramBroadcastRecord.state = 0;
        if ((i == 0) && (paramBoolean2))
            Slog.w("BroadcastQueue", "finishReceiver [" + this.mQueueName + "] called but state is IDLE");
        paramBroadcastRecord.receiver = null;
        paramBroadcastRecord.intent.setComponent(null);
        if (paramBroadcastRecord.curApp != null)
            paramBroadcastRecord.curApp.curReceiver = null;
        if (paramBroadcastRecord.curFilter != null)
            paramBroadcastRecord.curFilter.receiverList.curBroadcast = null;
        paramBroadcastRecord.curFilter = null;
        paramBroadcastRecord.curApp = null;
        paramBroadcastRecord.curComponent = null;
        paramBroadcastRecord.curReceiver = null;
        this.mPendingBroadcast = null;
        paramBroadcastRecord.resultCode = paramInt;
        paramBroadcastRecord.resultData = paramString;
        paramBroadcastRecord.resultExtras = paramBundle;
        paramBroadcastRecord.resultAbort = paramBoolean1;
        if ((i == 1) || (i == 3))
            bool = true;
        return bool;
    }

    public BroadcastRecord getMatchingOrderedReceiver(IBinder paramIBinder)
    {
        BroadcastRecord localBroadcastRecord;
        if (this.mOrderedBroadcasts.size() > 0)
        {
            localBroadcastRecord = (BroadcastRecord)this.mOrderedBroadcasts.get(0);
            if ((localBroadcastRecord == null) || (localBroadcastRecord.receiver != paramIBinder));
        }
        while (true)
        {
            return localBroadcastRecord;
            localBroadcastRecord = null;
        }
    }

    public boolean isPendingBroadcastProcessLocked(int paramInt)
    {
        if ((this.mPendingBroadcast != null) && (this.mPendingBroadcast.curApp.pid == paramInt));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    final void logBroadcastReceiverDiscardLocked(BroadcastRecord paramBroadcastRecord)
    {
        Object localObject;
        if (paramBroadcastRecord.nextReceiver > 0)
        {
            localObject = paramBroadcastRecord.receivers.get(-1 + paramBroadcastRecord.nextReceiver);
            if ((localObject instanceof BroadcastFilter))
            {
                BroadcastFilter localBroadcastFilter = (BroadcastFilter)localObject;
                Object[] arrayOfObject3 = new Object[4];
                arrayOfObject3[0] = Integer.valueOf(System.identityHashCode(paramBroadcastRecord));
                arrayOfObject3[1] = paramBroadcastRecord.intent.getAction();
                arrayOfObject3[2] = Integer.valueOf(-1 + paramBroadcastRecord.nextReceiver);
                arrayOfObject3[3] = Integer.valueOf(System.identityHashCode(localBroadcastFilter));
                EventLog.writeEvent(30024, arrayOfObject3);
            }
        }
        while (true)
        {
            return;
            Object[] arrayOfObject2 = new Object[4];
            arrayOfObject2[0] = Integer.valueOf(System.identityHashCode(paramBroadcastRecord));
            arrayOfObject2[1] = paramBroadcastRecord.intent.getAction();
            arrayOfObject2[2] = Integer.valueOf(-1 + paramBroadcastRecord.nextReceiver);
            arrayOfObject2[3] = ((ResolveInfo)localObject).toString();
            EventLog.writeEvent(30025, arrayOfObject2);
            continue;
            Slog.w("BroadcastQueue", "Discarding broadcast before first receiver is invoked: " + paramBroadcastRecord);
            Object[] arrayOfObject1 = new Object[4];
            arrayOfObject1[0] = Integer.valueOf(System.identityHashCode(paramBroadcastRecord));
            arrayOfObject1[1] = paramBroadcastRecord.intent.getAction();
            arrayOfObject1[2] = Integer.valueOf(paramBroadcastRecord.nextReceiver);
            arrayOfObject1[3] = "NONE";
            EventLog.writeEvent(30025, arrayOfObject1);
        }
    }

    // ERROR //
    final void processNextBroadcast(boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 73	com/android/server/am/BroadcastQueue:mService	Lcom/android/server/am/ActivityManagerService;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 73	com/android/server/am/BroadcastQueue:mService	Lcom/android/server/am/ActivityManagerService;
        //     11: invokevirtual 503	com/android/server/am/ActivityManagerService:updateCpuStats	()V
        //     14: iload_1
        //     15: ifeq +8 -> 23
        //     18: aload_0
        //     19: iconst_0
        //     20: putfield 64	com/android/server/am/BroadcastQueue:mBroadcastsScheduled	Z
        //     23: aload_0
        //     24: getfield 56	com/android/server/am/BroadcastQueue:mParallelBroadcasts	Ljava/util/ArrayList;
        //     27: invokevirtual 317	java/util/ArrayList:size	()I
        //     30: ifle +96 -> 126
        //     33: aload_0
        //     34: getfield 56	com/android/server/am/BroadcastQueue:mParallelBroadcasts	Ljava/util/ArrayList;
        //     37: iconst_0
        //     38: invokevirtual 506	java/util/ArrayList:remove	(I)Ljava/lang/Object;
        //     41: checkcast 60	com/android/server/am/BroadcastRecord
        //     44: astore 63
        //     46: aload 63
        //     48: invokestatic 94	android/os/SystemClock:uptimeMillis	()J
        //     51: putfield 509	com/android/server/am/BroadcastRecord:dispatchTime	J
        //     54: aload 63
        //     56: invokestatic 512	java/lang/System:currentTimeMillis	()J
        //     59: putfield 515	com/android/server/am/BroadcastRecord:dispatchClockTime	J
        //     62: aload 63
        //     64: getfield 357	com/android/server/am/BroadcastRecord:receivers	Ljava/util/List;
        //     67: invokeinterface 516 1 0
        //     72: istore 64
        //     74: iconst_0
        //     75: istore 65
        //     77: iload 65
        //     79: iload 64
        //     81: if_icmpge +31 -> 112
        //     84: aload_0
        //     85: aload 63
        //     87: aload 63
        //     89: getfield 357	com/android/server/am/BroadcastRecord:receivers	Ljava/util/List;
        //     92: iload 65
        //     94: invokeinterface 360 2 0
        //     99: checkcast 103	com/android/server/am/BroadcastFilter
        //     102: iconst_0
        //     103: invokespecial 518	com/android/server/am/BroadcastQueue:deliverToRegisteredReceiverLocked	(Lcom/android/server/am/BroadcastRecord;Lcom/android/server/am/BroadcastFilter;Z)V
        //     106: iinc 65 1
        //     109: goto -32 -> 77
        //     112: aload_0
        //     113: aload 63
        //     115: invokespecial 520	com/android/server/am/BroadcastQueue:addBroadcastToHistoryLocked	(Lcom/android/server/am/BroadcastRecord;)V
        //     118: goto -95 -> 23
        //     121: astore_3
        //     122: aload_2
        //     123: monitorexit
        //     124: aload_3
        //     125: athrow
        //     126: aload_0
        //     127: getfield 66	com/android/server/am/BroadcastQueue:mPendingBroadcast	Lcom/android/server/am/BroadcastRecord;
        //     130: ifnull +1858 -> 1988
        //     133: aload_0
        //     134: getfield 73	com/android/server/am/BroadcastQueue:mService	Lcom/android/server/am/ActivityManagerService;
        //     137: getfield 372	com/android/server/am/ActivityManagerService:mPidsSelfLocked	Landroid/util/SparseArray;
        //     140: astore 59
        //     142: aload 59
        //     144: monitorenter
        //     145: aload_0
        //     146: getfield 73	com/android/server/am/BroadcastQueue:mService	Lcom/android/server/am/ActivityManagerService;
        //     149: getfield 372	com/android/server/am/ActivityManagerService:mPidsSelfLocked	Landroid/util/SparseArray;
        //     152: aload_0
        //     153: getfield 66	com/android/server/am/BroadcastQueue:mPendingBroadcast	Lcom/android/server/am/BroadcastRecord;
        //     156: getfield 213	com/android/server/am/BroadcastRecord:curApp	Lcom/android/server/am/ProcessRecord;
        //     159: getfield 471	com/android/server/am/ProcessRecord:pid	I
        //     162: invokevirtual 375	android/util/SparseArray:get	(I)Ljava/lang/Object;
        //     165: ifnonnull +17 -> 182
        //     168: iconst_1
        //     169: istore 61
        //     171: aload 59
        //     173: monitorexit
        //     174: iload 61
        //     176: ifne +20 -> 196
        //     179: aload_2
        //     180: monitorexit
        //     181: return
        //     182: iconst_0
        //     183: istore 61
        //     185: goto -14 -> 171
        //     188: astore 60
        //     190: aload 59
        //     192: monitorexit
        //     193: aload 60
        //     195: athrow
        //     196: ldc 25
        //     198: new 117	java/lang/StringBuilder
        //     201: dup
        //     202: invokespecial 118	java/lang/StringBuilder:<init>	()V
        //     205: ldc_w 522
        //     208: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     211: aload_0
        //     212: getfield 75	com/android/server/am/BroadcastQueue:mQueueName	Ljava/lang/String;
        //     215: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     218: ldc_w 524
        //     221: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     224: aload_0
        //     225: getfield 66	com/android/server/am/BroadcastQueue:mPendingBroadcast	Lcom/android/server/am/BroadcastRecord;
        //     228: getfield 213	com/android/server/am/BroadcastRecord:curApp	Lcom/android/server/am/ProcessRecord;
        //     231: invokevirtual 155	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     234: ldc_w 526
        //     237: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     240: invokevirtual 156	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     243: invokestatic 162	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     246: pop
        //     247: aload_0
        //     248: getfield 66	com/android/server/am/BroadcastQueue:mPendingBroadcast	Lcom/android/server/am/BroadcastRecord;
        //     251: iconst_0
        //     252: putfield 210	com/android/server/am/BroadcastRecord:state	I
        //     255: aload_0
        //     256: getfield 66	com/android/server/am/BroadcastQueue:mPendingBroadcast	Lcom/android/server/am/BroadcastRecord;
        //     259: aload_0
        //     260: getfield 528	com/android/server/am/BroadcastQueue:mPendingBroadcastRecvIndex	I
        //     263: putfield 351	com/android/server/am/BroadcastRecord:nextReceiver	I
        //     266: aload_0
        //     267: aconst_null
        //     268: putfield 66	com/android/server/am/BroadcastQueue:mPendingBroadcast	Lcom/android/server/am/BroadcastRecord;
        //     271: goto +1717 -> 1988
        //     274: aload_0
        //     275: getfield 58	com/android/server/am/BroadcastQueue:mOrderedBroadcasts	Ljava/util/ArrayList;
        //     278: invokevirtual 317	java/util/ArrayList:size	()I
        //     281: ifne +27 -> 308
        //     284: aload_0
        //     285: getfield 73	com/android/server/am/BroadcastQueue:mService	Lcom/android/server/am/ActivityManagerService;
        //     288: invokevirtual 531	com/android/server/am/ActivityManagerService:scheduleAppGcsLocked	()V
        //     291: iload 4
        //     293: ifeq +10 -> 303
        //     296: aload_0
        //     297: getfield 73	com/android/server/am/BroadcastQueue:mService	Lcom/android/server/am/ActivityManagerService;
        //     300: invokevirtual 221	com/android/server/am/ActivityManagerService:updateOomAdjLocked	()V
        //     303: aload_2
        //     304: monitorexit
        //     305: goto -124 -> 181
        //     308: aload_0
        //     309: getfield 58	com/android/server/am/BroadcastQueue:mOrderedBroadcasts	Ljava/util/ArrayList;
        //     312: iconst_0
        //     313: invokevirtual 321	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     316: checkcast 60	com/android/server/am/BroadcastRecord
        //     319: astore 5
        //     321: iconst_0
        //     322: istore 6
        //     324: aload 5
        //     326: getfield 357	com/android/server/am/BroadcastRecord:receivers	Ljava/util/List;
        //     329: ifnull +1665 -> 1994
        //     332: aload 5
        //     334: getfield 357	com/android/server/am/BroadcastRecord:receivers	Ljava/util/List;
        //     337: invokeinterface 516 1 0
        //     342: istore 7
        //     344: aload_0
        //     345: getfield 73	com/android/server/am/BroadcastQueue:mService	Lcom/android/server/am/ActivityManagerService;
        //     348: getfield 331	com/android/server/am/ActivityManagerService:mProcessesReady	Z
        //     351: ifeq +188 -> 539
        //     354: aload 5
        //     356: getfield 509	com/android/server/am/BroadcastRecord:dispatchTime	J
        //     359: lconst_0
        //     360: lcmp
        //     361: ifle +178 -> 539
        //     364: invokestatic 94	android/os/SystemClock:uptimeMillis	()J
        //     367: lstore 56
        //     369: iload 7
        //     371: ifle +168 -> 539
        //     374: lload 56
        //     376: aload 5
        //     378: getfield 509	com/android/server/am/BroadcastRecord:dispatchTime	J
        //     381: ldc2_w 532
        //     384: aload_0
        //     385: getfield 77	com/android/server/am/BroadcastQueue:mTimeoutPeriod	J
        //     388: lmul
        //     389: iload 7
        //     391: i2l
        //     392: lmul
        //     393: ladd
        //     394: lcmp
        //     395: ifle +144 -> 539
        //     398: ldc 25
        //     400: new 117	java/lang/StringBuilder
        //     403: dup
        //     404: invokespecial 118	java/lang/StringBuilder:<init>	()V
        //     407: ldc_w 535
        //     410: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     413: aload_0
        //     414: getfield 75	com/android/server/am/BroadcastQueue:mQueueName	Ljava/lang/String;
        //     417: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     420: ldc_w 537
        //     423: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     426: ldc_w 539
        //     429: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     432: lload 56
        //     434: invokevirtual 343	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     437: ldc_w 541
        //     440: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     443: aload 5
        //     445: getfield 509	com/android/server/am/BroadcastRecord:dispatchTime	J
        //     448: invokevirtual 343	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     451: ldc_w 543
        //     454: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     457: aload 5
        //     459: getfield 334	com/android/server/am/BroadcastRecord:receiverTime	J
        //     462: invokevirtual 343	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     465: ldc_w 545
        //     468: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     471: aload 5
        //     473: getfield 128	com/android/server/am/BroadcastRecord:intent	Landroid/content/Intent;
        //     476: invokevirtual 155	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     479: ldc_w 547
        //     482: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     485: iload 7
        //     487: invokevirtual 144	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     490: ldc_w 549
        //     493: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     496: aload 5
        //     498: getfield 351	com/android/server/am/BroadcastRecord:nextReceiver	I
        //     501: invokevirtual 144	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     504: ldc_w 551
        //     507: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     510: aload 5
        //     512: getfield 210	com/android/server/am/BroadcastRecord:state	I
        //     515: invokevirtual 144	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     518: invokevirtual 156	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     521: invokestatic 162	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     524: pop
        //     525: aload_0
        //     526: iconst_0
        //     527: invokevirtual 553	com/android/server/am/BroadcastQueue:broadcastTimeoutLocked	(Z)V
        //     530: iconst_1
        //     531: istore 6
        //     533: aload 5
        //     535: iconst_0
        //     536: putfield 210	com/android/server/am/BroadcastRecord:state	I
        //     539: aload 5
        //     541: getfield 210	com/android/server/am/BroadcastRecord:state	I
        //     544: ifeq +8 -> 552
        //     547: aload_2
        //     548: monitorexit
        //     549: goto -368 -> 181
        //     552: aload 5
        //     554: getfield 357	com/android/server/am/BroadcastRecord:receivers	Ljava/util/List;
        //     557: ifnull +26 -> 583
        //     560: aload 5
        //     562: getfield 351	com/android/server/am/BroadcastRecord:nextReceiver	I
        //     565: iload 7
        //     567: if_icmpge +16 -> 583
        //     570: aload 5
        //     572: getfield 380	com/android/server/am/BroadcastRecord:resultAbort	Z
        //     575: ifne +8 -> 583
        //     578: iload 6
        //     580: ifeq +88 -> 668
        //     583: aload 5
        //     585: getfield 556	com/android/server/am/BroadcastRecord:resultTo	Landroid/content/IIntentReceiver;
        //     588: astore 8
        //     590: aload 8
        //     592: ifnull +51 -> 643
        //     595: aload 5
        //     597: getfield 559	com/android/server/am/BroadcastRecord:callerApp	Lcom/android/server/am/ProcessRecord;
        //     600: aload 5
        //     602: getfield 556	com/android/server/am/BroadcastRecord:resultTo	Landroid/content/IIntentReceiver;
        //     605: new 130	android/content/Intent
        //     608: dup
        //     609: aload 5
        //     611: getfield 128	com/android/server/am/BroadcastRecord:intent	Landroid/content/Intent;
        //     614: invokespecial 224	android/content/Intent:<init>	(Landroid/content/Intent;)V
        //     617: aload 5
        //     619: getfield 227	com/android/server/am/BroadcastRecord:resultCode	I
        //     622: aload 5
        //     624: getfield 230	com/android/server/am/BroadcastRecord:resultData	Ljava/lang/String;
        //     627: aload 5
        //     629: getfield 234	com/android/server/am/BroadcastRecord:resultExtras	Landroid/os/Bundle;
        //     632: iconst_0
        //     633: iconst_0
        //     634: invokestatic 244	com/android/server/am/BroadcastQueue:performReceiveLocked	(Lcom/android/server/am/ProcessRecord;Landroid/content/IIntentReceiver;Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;ZZ)V
        //     637: aload 5
        //     639: aconst_null
        //     640: putfield 556	com/android/server/am/BroadcastRecord:resultTo	Landroid/content/IIntentReceiver;
        //     643: aload_0
        //     644: invokevirtual 561	com/android/server/am/BroadcastQueue:cancelBroadcastTimeoutLocked	()V
        //     647: aload_0
        //     648: aload 5
        //     650: invokespecial 520	com/android/server/am/BroadcastQueue:addBroadcastToHistoryLocked	(Lcom/android/server/am/BroadcastRecord;)V
        //     653: aload_0
        //     654: getfield 58	com/android/server/am/BroadcastQueue:mOrderedBroadcasts	Ljava/util/ArrayList;
        //     657: iconst_0
        //     658: invokevirtual 506	java/util/ArrayList:remove	(I)Ljava/lang/Object;
        //     661: pop
        //     662: aconst_null
        //     663: astore 5
        //     665: iconst_1
        //     666: istore 4
        //     668: aload 5
        //     670: ifnull -396 -> 274
        //     673: aload 5
        //     675: getfield 351	com/android/server/am/BroadcastRecord:nextReceiver	I
        //     678: istore 10
        //     680: iload 10
        //     682: iconst_1
        //     683: iadd
        //     684: istore 11
        //     686: aload 5
        //     688: iload 11
        //     690: putfield 351	com/android/server/am/BroadcastRecord:nextReceiver	I
        //     693: invokestatic 94	android/os/SystemClock:uptimeMillis	()J
        //     696: lstore 12
        //     698: aload 5
        //     700: lload 12
        //     702: putfield 334	com/android/server/am/BroadcastRecord:receiverTime	J
        //     705: iload 10
        //     707: ifne +29 -> 736
        //     710: aload 5
        //     712: getfield 334	com/android/server/am/BroadcastRecord:receiverTime	J
        //     715: lstore 50
        //     717: aload 5
        //     719: lload 50
        //     721: putfield 509	com/android/server/am/BroadcastRecord:dispatchTime	J
        //     724: invokestatic 512	java/lang/System:currentTimeMillis	()J
        //     727: lstore 52
        //     729: aload 5
        //     731: lload 52
        //     733: putfield 515	com/android/server/am/BroadcastRecord:dispatchClockTime	J
        //     736: aload_0
        //     737: getfield 313	com/android/server/am/BroadcastQueue:mPendingBroadcastTimeoutMessage	Z
        //     740: ifne +17 -> 757
        //     743: aload_0
        //     744: aload 5
        //     746: getfield 334	com/android/server/am/BroadcastRecord:receiverTime	J
        //     749: aload_0
        //     750: getfield 77	com/android/server/am/BroadcastQueue:mTimeoutPeriod	J
        //     753: ladd
        //     754: invokevirtual 328	com/android/server/am/BroadcastQueue:setBroadcastTimeoutLocked	(J)V
        //     757: aload 5
        //     759: getfield 357	com/android/server/am/BroadcastRecord:receivers	Ljava/util/List;
        //     762: iload 10
        //     764: invokeinterface 360 2 0
        //     769: astore 14
        //     771: aload 14
        //     773: instanceof 103
        //     776: ifeq +108 -> 884
        //     779: aload 14
        //     781: checkcast 103	com/android/server/am/BroadcastFilter
        //     784: astore 48
        //     786: aload 5
        //     788: getfield 237	com/android/server/am/BroadcastRecord:ordered	Z
        //     791: istore 49
        //     793: aload_0
        //     794: aload 5
        //     796: aload 48
        //     798: iload 49
        //     800: invokespecial 518	com/android/server/am/BroadcastQueue:deliverToRegisteredReceiverLocked	(Lcom/android/server/am/BroadcastRecord;Lcom/android/server/am/BroadcastFilter;Z)V
        //     803: aload 5
        //     805: getfield 200	com/android/server/am/BroadcastRecord:receiver	Landroid/os/IBinder;
        //     808: ifnull +11 -> 819
        //     811: aload 5
        //     813: getfield 237	com/android/server/am/BroadcastRecord:ordered	Z
        //     816: ifne +13 -> 829
        //     819: aload 5
        //     821: iconst_0
        //     822: putfield 210	com/android/server/am/BroadcastRecord:state	I
        //     825: aload_0
        //     826: invokevirtual 387	com/android/server/am/BroadcastQueue:scheduleBroadcastsLocked	()V
        //     829: aload_2
        //     830: monitorexit
        //     831: goto -650 -> 181
        //     834: astore 54
        //     836: ldc 25
        //     838: new 117	java/lang/StringBuilder
        //     841: dup
        //     842: invokespecial 118	java/lang/StringBuilder:<init>	()V
        //     845: ldc_w 563
        //     848: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     851: aload_0
        //     852: getfield 75	com/android/server/am/BroadcastQueue:mQueueName	Ljava/lang/String;
        //     855: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     858: ldc_w 565
        //     861: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     864: aload 5
        //     866: getfield 128	com/android/server/am/BroadcastRecord:intent	Landroid/content/Intent;
        //     869: invokevirtual 155	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     872: invokevirtual 156	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     875: aload 54
        //     877: invokestatic 249	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     880: pop
        //     881: goto -238 -> 643
        //     884: aload 14
        //     886: checkcast 492	android/content/pm/ResolveInfo
        //     889: astore 15
        //     891: iconst_0
        //     892: istore 16
        //     894: aload_0
        //     895: getfield 73	com/android/server/am/BroadcastQueue:mService	Lcom/android/server/am/ActivityManagerService;
        //     898: aload 15
        //     900: getfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     903: getfield 571	android/content/pm/ActivityInfo:permission	Ljava/lang/String;
        //     906: aload 5
        //     908: getfield 109	com/android/server/am/BroadcastRecord:callingPid	I
        //     911: aload 5
        //     913: getfield 82	com/android/server/am/BroadcastRecord:callingUid	I
        //     916: aload 15
        //     918: getfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     921: getfield 301	android/content/pm/ActivityInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     924: getfield 574	android/content/pm/ApplicationInfo:uid	I
        //     927: aload 15
        //     929: getfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     932: getfield 577	android/content/pm/ActivityInfo:exported	Z
        //     935: invokevirtual 115	com/android/server/am/ActivityManagerService:checkComponentPermission	(Ljava/lang/String;IIIZ)I
        //     938: ifeq +147 -> 1085
        //     941: aload 15
        //     943: getfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     946: getfield 577	android/content/pm/ActivityInfo:exported	Z
        //     949: ifne +349 -> 1298
        //     952: ldc 25
        //     954: new 117	java/lang/StringBuilder
        //     957: dup
        //     958: invokespecial 118	java/lang/StringBuilder:<init>	()V
        //     961: ldc 120
        //     963: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     966: aload 5
        //     968: getfield 128	com/android/server/am/BroadcastRecord:intent	Landroid/content/Intent;
        //     971: invokevirtual 134	android/content/Intent:toString	()Ljava/lang/String;
        //     974: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     977: ldc 136
        //     979: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     982: aload 5
        //     984: getfield 139	com/android/server/am/BroadcastRecord:callerPackage	Ljava/lang/String;
        //     987: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     990: ldc 141
        //     992: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     995: aload 5
        //     997: getfield 109	com/android/server/am/BroadcastRecord:callingPid	I
        //     1000: invokevirtual 144	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     1003: ldc 146
        //     1005: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1008: aload 5
        //     1010: getfield 82	com/android/server/am/BroadcastRecord:callingUid	I
        //     1013: invokevirtual 144	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     1016: ldc 148
        //     1018: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1021: ldc_w 579
        //     1024: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1027: aload 15
        //     1029: getfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     1032: getfield 301	android/content/pm/ActivityInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     1035: getfield 574	android/content/pm/ApplicationInfo:uid	I
        //     1038: invokevirtual 144	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     1041: ldc_w 581
        //     1044: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1047: aload 15
        //     1049: getfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     1052: getfield 586	android/content/pm/ComponentInfo:packageName	Ljava/lang/String;
        //     1055: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1058: ldc_w 588
        //     1061: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1064: aload 15
        //     1066: getfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     1069: getfield 591	android/content/pm/ComponentInfo:name	Ljava/lang/String;
        //     1072: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1075: invokevirtual 156	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1078: invokestatic 162	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     1081: pop
        //     1082: goto +918 -> 2000
        //     1085: aload 15
        //     1087: getfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     1090: getfield 301	android/content/pm/ActivityInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     1093: getfield 574	android/content/pm/ApplicationInfo:uid	I
        //     1096: sipush 1000
        //     1099: if_icmpeq +145 -> 1244
        //     1102: aload 5
        //     1104: getfield 163	com/android/server/am/BroadcastRecord:requiredPermission	Ljava/lang/String;
        //     1107: astore 41
        //     1109: aload 41
        //     1111: ifnull +133 -> 1244
        //     1114: invokestatic 597	android/app/AppGlobals:getPackageManager	()Landroid/content/pm/IPackageManager;
        //     1117: aload 5
        //     1119: getfield 163	com/android/server/am/BroadcastRecord:requiredPermission	Ljava/lang/String;
        //     1122: aload 15
        //     1124: getfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     1127: getfield 301	android/content/pm/ActivityInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     1130: getfield 598	android/content/pm/ApplicationInfo:packageName	Ljava/lang/String;
        //     1133: invokeinterface 603 3 0
        //     1138: istore 45
        //     1140: iload 45
        //     1142: istore 43
        //     1144: iload 43
        //     1146: ifeq +98 -> 1244
        //     1149: ldc 25
        //     1151: new 117	java/lang/StringBuilder
        //     1154: dup
        //     1155: invokespecial 118	java/lang/StringBuilder:<init>	()V
        //     1158: ldc 177
        //     1160: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1163: aload 5
        //     1165: getfield 128	com/android/server/am/BroadcastRecord:intent	Landroid/content/Intent;
        //     1168: invokevirtual 155	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     1171: ldc 179
        //     1173: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1176: aload 15
        //     1178: getfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     1181: getfield 301	android/content/pm/ActivityInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     1184: getfield 598	android/content/pm/ApplicationInfo:packageName	Ljava/lang/String;
        //     1187: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1190: ldc 150
        //     1192: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1195: aload 5
        //     1197: getfield 163	com/android/server/am/BroadcastRecord:requiredPermission	Ljava/lang/String;
        //     1200: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1203: ldc 185
        //     1205: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1208: aload 5
        //     1210: getfield 139	com/android/server/am/BroadcastRecord:callerPackage	Ljava/lang/String;
        //     1213: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1216: ldc 187
        //     1218: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1221: aload 5
        //     1223: getfield 82	com/android/server/am/BroadcastRecord:callingUid	I
        //     1226: invokevirtual 144	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     1229: ldc 148
        //     1231: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1234: invokevirtual 156	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1237: invokestatic 162	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     1240: pop
        //     1241: iconst_1
        //     1242: istore 16
        //     1244: aload 5
        //     1246: getfield 213	com/android/server/am/BroadcastRecord:curApp	Lcom/android/server/am/ProcessRecord;
        //     1249: ifnull +17 -> 1266
        //     1252: aload 5
        //     1254: getfield 213	com/android/server/am/BroadcastRecord:curApp	Lcom/android/server/am/ProcessRecord;
        //     1257: getfield 606	com/android/server/am/ProcessRecord:crashing	Z
        //     1260: ifeq +6 -> 1266
        //     1263: iconst_1
        //     1264: istore 16
        //     1266: iload 16
        //     1268: ifeq +159 -> 1427
        //     1271: aload 5
        //     1273: aconst_null
        //     1274: putfield 200	com/android/server/am/BroadcastRecord:receiver	Landroid/os/IBinder;
        //     1277: aload 5
        //     1279: aconst_null
        //     1280: putfield 204	com/android/server/am/BroadcastRecord:curFilter	Lcom/android/server/am/BroadcastFilter;
        //     1283: aload 5
        //     1285: iconst_0
        //     1286: putfield 210	com/android/server/am/BroadcastRecord:state	I
        //     1289: aload_0
        //     1290: invokevirtual 387	com/android/server/am/BroadcastQueue:scheduleBroadcastsLocked	()V
        //     1293: aload_2
        //     1294: monitorexit
        //     1295: goto -1114 -> 181
        //     1298: ldc 25
        //     1300: new 117	java/lang/StringBuilder
        //     1303: dup
        //     1304: invokespecial 118	java/lang/StringBuilder:<init>	()V
        //     1307: ldc 120
        //     1309: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1312: aload 5
        //     1314: getfield 128	com/android/server/am/BroadcastRecord:intent	Landroid/content/Intent;
        //     1317: invokevirtual 134	android/content/Intent:toString	()Ljava/lang/String;
        //     1320: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1323: ldc 136
        //     1325: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1328: aload 5
        //     1330: getfield 139	com/android/server/am/BroadcastRecord:callerPackage	Ljava/lang/String;
        //     1333: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1336: ldc 141
        //     1338: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1341: aload 5
        //     1343: getfield 109	com/android/server/am/BroadcastRecord:callingPid	I
        //     1346: invokevirtual 144	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     1349: ldc 146
        //     1351: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1354: aload 5
        //     1356: getfield 82	com/android/server/am/BroadcastRecord:callingUid	I
        //     1359: invokevirtual 144	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     1362: ldc 148
        //     1364: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1367: ldc 150
        //     1369: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1372: aload 15
        //     1374: getfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     1377: getfield 571	android/content/pm/ActivityInfo:permission	Ljava/lang/String;
        //     1380: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1383: ldc_w 581
        //     1386: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1389: aload 15
        //     1391: getfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     1394: getfield 586	android/content/pm/ComponentInfo:packageName	Ljava/lang/String;
        //     1397: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1400: ldc_w 588
        //     1403: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1406: aload 15
        //     1408: getfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     1411: getfield 591	android/content/pm/ComponentInfo:name	Ljava/lang/String;
        //     1414: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1417: invokevirtual 156	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1420: invokestatic 162	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     1423: pop
        //     1424: goto +576 -> 2000
        //     1427: aload 5
        //     1429: iconst_1
        //     1430: putfield 210	com/android/server/am/BroadcastRecord:state	I
        //     1433: aload 15
        //     1435: getfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     1438: getfield 609	android/content/pm/ActivityInfo:processName	Ljava/lang/String;
        //     1441: astore 17
        //     1443: new 285	android/content/ComponentName
        //     1446: dup
        //     1447: aload 15
        //     1449: getfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     1452: getfield 301	android/content/pm/ActivityInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     1455: getfield 598	android/content/pm/ApplicationInfo:packageName	Ljava/lang/String;
        //     1458: aload 15
        //     1460: getfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     1463: getfield 591	android/content/pm/ComponentInfo:name	Ljava/lang/String;
        //     1466: invokespecial 612	android/content/ComponentName:<init>	(Ljava/lang/String;Ljava/lang/String;)V
        //     1469: astore 18
        //     1471: aload 5
        //     1473: aload 18
        //     1475: putfield 275	com/android/server/am/BroadcastRecord:curComponent	Landroid/content/ComponentName;
        //     1478: aload 5
        //     1480: getfield 82	com/android/server/am/BroadcastRecord:callingUid	I
        //     1483: sipush 1000
        //     1486: if_icmpeq +51 -> 1537
        //     1489: aload_0
        //     1490: getfield 73	com/android/server/am/BroadcastQueue:mService	Lcom/android/server/am/ActivityManagerService;
        //     1493: aload 15
        //     1495: getfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     1498: getfield 609	android/content/pm/ActivityInfo:processName	Ljava/lang/String;
        //     1501: aload 15
        //     1503: getfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     1506: getfield 301	android/content/pm/ActivityInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     1509: invokevirtual 616	com/android/server/am/ActivityManagerService:isSingleton	(Ljava/lang/String;Landroid/content/pm/ApplicationInfo;)Z
        //     1512: ifeq +130 -> 1642
        //     1515: iconst_0
        //     1516: istore 40
        //     1518: aload 15
        //     1520: aload_0
        //     1521: getfield 73	com/android/server/am/BroadcastQueue:mService	Lcom/android/server/am/ActivityManagerService;
        //     1524: aload 15
        //     1526: getfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     1529: iload 40
        //     1531: invokevirtual 620	com/android/server/am/ActivityManagerService:getActivityInfoForUser	(Landroid/content/pm/ActivityInfo;I)Landroid/content/pm/ActivityInfo;
        //     1534: putfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     1537: aload 15
        //     1539: getfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     1542: astore 19
        //     1544: aload 5
        //     1546: aload 19
        //     1548: putfield 295	com/android/server/am/BroadcastRecord:curReceiver	Landroid/content/pm/ActivityInfo;
        //     1551: invokestatic 597	android/app/AppGlobals:getPackageManager	()Landroid/content/pm/IPackageManager;
        //     1554: aload 5
        //     1556: getfield 275	com/android/server/am/BroadcastRecord:curComponent	Landroid/content/ComponentName;
        //     1559: invokevirtual 288	android/content/ComponentName:getPackageName	()Ljava/lang/String;
        //     1562: iconst_0
        //     1563: aload 5
        //     1565: getfield 82	com/android/server/am/BroadcastRecord:callingUid	I
        //     1568: invokestatic 626	android/os/UserId:getUserId	(I)I
        //     1571: invokeinterface 630 4 0
        //     1576: aload_0
        //     1577: getfield 73	com/android/server/am/BroadcastQueue:mService	Lcom/android/server/am/ActivityManagerService;
        //     1580: aload 17
        //     1582: aload 15
        //     1584: getfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     1587: getfield 301	android/content/pm/ActivityInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     1590: getfield 574	android/content/pm/ApplicationInfo:uid	I
        //     1593: invokevirtual 634	com/android/server/am/ActivityManagerService:getProcessRecordLocked	(Ljava/lang/String;I)Lcom/android/server/am/ProcessRecord;
        //     1596: astore 21
        //     1598: aload 21
        //     1600: ifnull +138 -> 1738
        //     1603: aload 21
        //     1605: getfield 253	com/android/server/am/ProcessRecord:thread	Landroid/app/IApplicationThread;
        //     1608: astore 34
        //     1610: aload 34
        //     1612: ifnull +126 -> 1738
        //     1615: aload 21
        //     1617: aload 15
        //     1619: getfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     1622: getfield 586	android/content/pm/ComponentInfo:packageName	Ljava/lang/String;
        //     1625: invokevirtual 638	com/android/server/am/ProcessRecord:addPackage	(Ljava/lang/String;)Z
        //     1628: pop
        //     1629: aload_0
        //     1630: aload 5
        //     1632: aload 21
        //     1634: invokespecial 640	com/android/server/am/BroadcastQueue:processCurBroadcastLocked	(Lcom/android/server/am/BroadcastRecord;Lcom/android/server/am/ProcessRecord;)V
        //     1637: aload_2
        //     1638: monitorexit
        //     1639: goto -1458 -> 181
        //     1642: aload 5
        //     1644: getfield 82	com/android/server/am/BroadcastRecord:callingUid	I
        //     1647: invokestatic 626	android/os/UserId:getUserId	(I)I
        //     1650: istore 40
        //     1652: goto -134 -> 1518
        //     1655: astore 38
        //     1657: ldc 25
        //     1659: new 117	java/lang/StringBuilder
        //     1662: dup
        //     1663: invokespecial 118	java/lang/StringBuilder:<init>	()V
        //     1666: ldc_w 642
        //     1669: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1672: aload 5
        //     1674: getfield 275	com/android/server/am/BroadcastRecord:curComponent	Landroid/content/ComponentName;
        //     1677: invokevirtual 288	android/content/ComponentName:getPackageName	()Ljava/lang/String;
        //     1680: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1683: ldc_w 454
        //     1686: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1689: aload 38
        //     1691: invokevirtual 155	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     1694: invokevirtual 156	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1697: invokestatic 162	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     1700: pop
        //     1701: goto -125 -> 1576
        //     1704: astore 35
        //     1706: ldc 25
        //     1708: new 117	java/lang/StringBuilder
        //     1711: dup
        //     1712: invokespecial 118	java/lang/StringBuilder:<init>	()V
        //     1715: ldc_w 644
        //     1718: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1721: aload 5
        //     1723: getfield 275	com/android/server/am/BroadcastRecord:curComponent	Landroid/content/ComponentName;
        //     1726: invokevirtual 155	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     1729: invokevirtual 156	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1732: aload 35
        //     1734: invokestatic 249	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     1737: pop
        //     1738: aload_0
        //     1739: getfield 73	com/android/server/am/BroadcastQueue:mService	Lcom/android/server/am/ActivityManagerService;
        //     1742: astore 22
        //     1744: aload 15
        //     1746: getfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     1749: getfield 301	android/content/pm/ActivityInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     1752: astore 23
        //     1754: iconst_4
        //     1755: aload 5
        //     1757: getfield 128	com/android/server/am/BroadcastRecord:intent	Landroid/content/Intent;
        //     1760: invokevirtual 647	android/content/Intent:getFlags	()I
        //     1763: ior
        //     1764: istore 24
        //     1766: aload 5
        //     1768: getfield 275	com/android/server/am/BroadcastRecord:curComponent	Landroid/content/ComponentName;
        //     1771: astore 25
        //     1773: ldc_w 648
        //     1776: aload 5
        //     1778: getfield 128	com/android/server/am/BroadcastRecord:intent	Landroid/content/Intent;
        //     1781: invokevirtual 647	android/content/Intent:getFlags	()I
        //     1784: iand
        //     1785: ifeq +230 -> 2015
        //     1788: iconst_1
        //     1789: istore 26
        //     1791: aload 22
        //     1793: aload 17
        //     1795: aload 23
        //     1797: iconst_1
        //     1798: iload 24
        //     1800: ldc_w 650
        //     1803: aload 25
        //     1805: iload 26
        //     1807: iconst_0
        //     1808: invokevirtual 654	com/android/server/am/ActivityManagerService:startProcessLocked	(Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZILjava/lang/String;Landroid/content/ComponentName;ZZ)Lcom/android/server/am/ProcessRecord;
        //     1811: astore 27
        //     1813: aload 5
        //     1815: aload 27
        //     1817: putfield 213	com/android/server/am/BroadcastRecord:curApp	Lcom/android/server/am/ProcessRecord;
        //     1820: aload 27
        //     1822: ifnonnull +144 -> 1966
        //     1825: ldc 25
        //     1827: new 117	java/lang/StringBuilder
        //     1830: dup
        //     1831: invokespecial 118	java/lang/StringBuilder:<init>	()V
        //     1834: ldc_w 656
        //     1837: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1840: aload 15
        //     1842: getfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     1845: getfield 301	android/content/pm/ActivityInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     1848: getfield 598	android/content/pm/ApplicationInfo:packageName	Ljava/lang/String;
        //     1851: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1854: ldc_w 588
        //     1857: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1860: aload 15
        //     1862: getfield 568	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     1865: getfield 301	android/content/pm/ActivityInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     1868: getfield 574	android/content/pm/ApplicationInfo:uid	I
        //     1871: invokevirtual 144	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     1874: ldc_w 658
        //     1877: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1880: aload 5
        //     1882: getfield 128	com/android/server/am/BroadcastRecord:intent	Landroid/content/Intent;
        //     1885: invokevirtual 155	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     1888: ldc_w 660
        //     1891: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1894: invokevirtual 156	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1897: invokestatic 162	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     1900: pop
        //     1901: aload_0
        //     1902: aload 5
        //     1904: invokevirtual 365	com/android/server/am/BroadcastQueue:logBroadcastReceiverDiscardLocked	(Lcom/android/server/am/BroadcastRecord;)V
        //     1907: aload 5
        //     1909: getfield 227	com/android/server/am/BroadcastRecord:resultCode	I
        //     1912: istore 29
        //     1914: aload 5
        //     1916: getfield 230	com/android/server/am/BroadcastRecord:resultData	Ljava/lang/String;
        //     1919: astore 30
        //     1921: aload 5
        //     1923: getfield 234	com/android/server/am/BroadcastRecord:resultExtras	Landroid/os/Bundle;
        //     1926: astore 31
        //     1928: aload 5
        //     1930: getfield 380	com/android/server/am/BroadcastRecord:resultAbort	Z
        //     1933: istore 32
        //     1935: aload_0
        //     1936: aload 5
        //     1938: iload 29
        //     1940: aload 30
        //     1942: aload 31
        //     1944: iload 32
        //     1946: iconst_1
        //     1947: invokevirtual 384	com/android/server/am/BroadcastQueue:finishReceiverLocked	(Lcom/android/server/am/BroadcastRecord;ILjava/lang/String;Landroid/os/Bundle;ZZ)Z
        //     1950: pop
        //     1951: aload_0
        //     1952: invokevirtual 387	com/android/server/am/BroadcastQueue:scheduleBroadcastsLocked	()V
        //     1955: aload 5
        //     1957: iconst_0
        //     1958: putfield 210	com/android/server/am/BroadcastRecord:state	I
        //     1961: aload_2
        //     1962: monitorexit
        //     1963: goto -1782 -> 181
        //     1966: aload_0
        //     1967: aload 5
        //     1969: putfield 66	com/android/server/am/BroadcastQueue:mPendingBroadcast	Lcom/android/server/am/BroadcastRecord;
        //     1972: aload_0
        //     1973: iload 10
        //     1975: putfield 528	com/android/server/am/BroadcastQueue:mPendingBroadcastRecvIndex	I
        //     1978: aload_2
        //     1979: monitorexit
        //     1980: goto -1799 -> 181
        //     1983: astore 20
        //     1985: goto -409 -> 1576
        //     1988: iconst_0
        //     1989: istore 4
        //     1991: goto -1717 -> 274
        //     1994: iconst_0
        //     1995: istore 7
        //     1997: goto -1653 -> 344
        //     2000: iconst_1
        //     2001: istore 16
        //     2003: goto -918 -> 1085
        //     2006: astore 42
        //     2008: bipush 255
        //     2010: istore 43
        //     2012: goto -868 -> 1144
        //     2015: iconst_0
        //     2016: istore 26
        //     2018: goto -227 -> 1791
        //
        // Exception table:
        //     from	to	target	type
        //     7	124	121	finally
        //     126	145	121	finally
        //     179	181	121	finally
        //     193	590	121	finally
        //     595	643	121	finally
        //     643	1109	121	finally
        //     1114	1140	121	finally
        //     1149	1551	121	finally
        //     1551	1576	121	finally
        //     1576	1610	121	finally
        //     1615	1637	121	finally
        //     1637	1980	121	finally
        //     145	174	188	finally
        //     190	193	188	finally
        //     595	643	834	android/os/RemoteException
        //     1551	1576	1655	java/lang/IllegalArgumentException
        //     1615	1637	1704	android/os/RemoteException
        //     1551	1576	1983	android/os/RemoteException
        //     1114	1140	2006	android/os/RemoteException
    }

    public final boolean replaceOrderedBroadcastLocked(BroadcastRecord paramBroadcastRecord)
    {
        int i = -1 + this.mOrderedBroadcasts.size();
        if (i > 0)
            if (paramBroadcastRecord.intent.filterEquals(((BroadcastRecord)this.mOrderedBroadcasts.get(i)).intent))
                this.mOrderedBroadcasts.set(i, paramBroadcastRecord);
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            i--;
            break;
        }
    }

    public final boolean replaceParallelBroadcastLocked(BroadcastRecord paramBroadcastRecord)
    {
        int i = -1 + this.mParallelBroadcasts.size();
        if (i >= 0)
            if (paramBroadcastRecord.intent.filterEquals(((BroadcastRecord)this.mParallelBroadcasts.get(i)).intent))
                this.mParallelBroadcasts.set(i, paramBroadcastRecord);
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            i--;
            break;
        }
    }

    public void scheduleBroadcastsLocked()
    {
        if (this.mBroadcastsScheduled);
        while (true)
        {
            return;
            this.mHandler.sendMessage(this.mHandler.obtainMessage(200, this));
            this.mBroadcastsScheduled = true;
        }
    }

    public boolean sendPendingBroadcastsLocked(ProcessRecord paramProcessRecord)
    {
        boolean bool = false;
        BroadcastRecord localBroadcastRecord = this.mPendingBroadcast;
        if ((localBroadcastRecord != null) && (localBroadcastRecord.curApp.pid == paramProcessRecord.pid));
        try
        {
            this.mPendingBroadcast = null;
            processCurBroadcastLocked(localBroadcastRecord, paramProcessRecord);
            bool = true;
            return bool;
        }
        catch (Exception localException)
        {
            Slog.w("BroadcastQueue", "Exception in new application when starting receiver " + localBroadcastRecord.curComponent.flattenToShortString(), localException);
            logBroadcastReceiverDiscardLocked(localBroadcastRecord);
            finishReceiverLocked(localBroadcastRecord, localBroadcastRecord.resultCode, localBroadcastRecord.resultData, localBroadcastRecord.resultExtras, localBroadcastRecord.resultAbort, true);
            scheduleBroadcastsLocked();
            localBroadcastRecord.state = 0;
            throw new RuntimeException(localException.getMessage());
        }
    }

    final void setBroadcastTimeoutLocked(long paramLong)
    {
        if (!this.mPendingBroadcastTimeoutMessage)
        {
            Message localMessage = this.mHandler.obtainMessage(201, this);
            this.mHandler.sendMessageAtTime(localMessage, paramLong);
            this.mPendingBroadcastTimeoutMessage = true;
        }
    }

    public void skipCurrentReceiverLocked(ProcessRecord paramProcessRecord)
    {
        int i = 0;
        BroadcastRecord localBroadcastRecord1 = paramProcessRecord.curReceiver;
        if (localBroadcastRecord1 != null)
        {
            logBroadcastReceiverDiscardLocked(localBroadcastRecord1);
            finishReceiverLocked(localBroadcastRecord1, localBroadcastRecord1.resultCode, localBroadcastRecord1.resultData, localBroadcastRecord1.resultExtras, localBroadcastRecord1.resultAbort, true);
            i = 1;
        }
        BroadcastRecord localBroadcastRecord2 = this.mPendingBroadcast;
        if ((localBroadcastRecord2 != null) && (localBroadcastRecord2.curApp == paramProcessRecord))
        {
            logBroadcastReceiverDiscardLocked(localBroadcastRecord2);
            finishReceiverLocked(localBroadcastRecord2, localBroadcastRecord2.resultCode, localBroadcastRecord2.resultData, localBroadcastRecord2.resultExtras, localBroadcastRecord2.resultAbort, true);
            i = 1;
        }
        if (i != 0)
            scheduleBroadcastsLocked();
    }

    public void skipPendingBroadcastLocked(int paramInt)
    {
        BroadcastRecord localBroadcastRecord = this.mPendingBroadcast;
        if ((localBroadcastRecord != null) && (localBroadcastRecord.curApp.pid == paramInt))
        {
            localBroadcastRecord.state = 0;
            localBroadcastRecord.nextReceiver = this.mPendingBroadcastRecvIndex;
            this.mPendingBroadcast = null;
            scheduleBroadcastsLocked();
        }
    }

    private final class AppNotResponding
        implements Runnable
    {
        private final String mAnnotation;
        private final ProcessRecord mApp;

        public AppNotResponding(ProcessRecord paramString, String arg3)
        {
            this.mApp = paramString;
            Object localObject;
            this.mAnnotation = localObject;
        }

        public void run()
        {
            BroadcastQueue.this.mService.appNotResponding(this.mApp, null, null, this.mAnnotation);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.BroadcastQueue
 * JD-Core Version:        0.6.2
 */