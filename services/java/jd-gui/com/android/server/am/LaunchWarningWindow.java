package com.android.server.am;

import android.app.Dialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.util.TypedValue;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

public class LaunchWarningWindow extends Dialog
{
    public LaunchWarningWindow(Context paramContext, ActivityRecord paramActivityRecord1, ActivityRecord paramActivityRecord2)
    {
        super(paramContext, 16974580);
        requestWindowFeature(3);
        getWindow().setType(2003);
        getWindow().addFlags(24);
        setContentView(17367133);
        setTitle(paramContext.getText(17040347));
        TypedValue localTypedValue = new TypedValue();
        getContext().getTheme().resolveAttribute(16843605, localTypedValue, true);
        getWindow().setFeatureDrawableResource(3, localTypedValue.resourceId);
        ((ImageView)findViewById(16909003)).setImageDrawable(paramActivityRecord2.info.applicationInfo.loadIcon(paramContext.getPackageManager()));
        TextView localTextView1 = (TextView)findViewById(16909004);
        Resources localResources1 = paramContext.getResources();
        Object[] arrayOfObject1 = new Object[1];
        arrayOfObject1[0] = paramActivityRecord2.info.applicationInfo.loadLabel(paramContext.getPackageManager()).toString();
        localTextView1.setText(localResources1.getString(17040348, arrayOfObject1));
        ((ImageView)findViewById(16909005)).setImageDrawable(paramActivityRecord1.info.applicationInfo.loadIcon(paramContext.getPackageManager()));
        TextView localTextView2 = (TextView)findViewById(16909006);
        Resources localResources2 = paramContext.getResources();
        Object[] arrayOfObject2 = new Object[1];
        arrayOfObject2[0] = paramActivityRecord1.info.applicationInfo.loadLabel(paramContext.getPackageManager()).toString();
        localTextView2.setText(localResources2.getString(17040349, arrayOfObject2));
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.LaunchWarningWindow
 * JD-Core Version:        0.6.2
 */