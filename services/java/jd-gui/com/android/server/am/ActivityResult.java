package com.android.server.am;

import android.app.ResultInfo;
import android.content.Intent;

class ActivityResult extends ResultInfo
{
    final ActivityRecord mFrom;

    public ActivityResult(ActivityRecord paramActivityRecord, String paramString, int paramInt1, int paramInt2, Intent paramIntent)
    {
        super(paramString, paramInt1, paramInt2, paramIntent);
        this.mFrom = paramActivityRecord;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.ActivityResult
 * JD-Core Version:        0.6.2
 */