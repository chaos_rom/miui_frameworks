package com.android.server.am;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.Button;

class BaseErrorDialog extends AlertDialog
{
    private boolean mConsuming = true;
    private Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            if (paramAnonymousMessage.what == 0)
            {
                BaseErrorDialog.access$002(BaseErrorDialog.this, false);
                BaseErrorDialog.this.setEnabled(true);
            }
        }
    };

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public BaseErrorDialog(Context paramContext)
    {
        super(paramContext, 101515296);
        getWindow().setType(2003);
        getWindow().setFlags(131072, 131072);
        getWindow().setTitle("Error Dialog");
        setIconAttribute(16843605);
    }

    private void setEnabled(boolean paramBoolean)
    {
        Button localButton1 = (Button)findViewById(16908313);
        if (localButton1 != null)
            localButton1.setEnabled(paramBoolean);
        Button localButton2 = (Button)findViewById(16908314);
        if (localButton2 != null)
            localButton2.setEnabled(paramBoolean);
        Button localButton3 = (Button)findViewById(16908315);
        if (localButton3 != null)
            localButton3.setEnabled(paramBoolean);
    }

    public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
    {
        if (this.mConsuming);
        for (boolean bool = true; ; bool = super.dispatchKeyEvent(paramKeyEvent))
            return bool;
    }

    public void onStart()
    {
        super.onStart();
        setEnabled(false);
        this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(0), 1000L);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.BaseErrorDialog
 * JD-Core Version:        0.6.2
 */