package com.android.server.am;

import android.content.IIntentReceiver;
import android.content.IIntentSender.Stub;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.UserId;
import android.util.Slog;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.HashMap;

class PendingIntentRecord extends IIntentSender.Stub
{
    boolean canceled = false;
    final Key key;
    final ActivityManagerService owner;
    final WeakReference<PendingIntentRecord> ref;
    boolean sent = false;
    String stringName;
    final int uid;

    PendingIntentRecord(ActivityManagerService paramActivityManagerService, Key paramKey, int paramInt)
    {
        this.owner = paramActivityManagerService;
        this.key = paramKey;
        this.uid = paramInt;
        this.ref = new WeakReference(this);
    }

    public void completeFinalize()
    {
        synchronized (this.owner)
        {
            if ((WeakReference)this.owner.mIntentSenderRecords.get(this.key) == this.ref)
                this.owner.mIntentSenderRecords.remove(this.key);
            return;
        }
    }

    void dump(PrintWriter paramPrintWriter, String paramString)
    {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("uid=");
        paramPrintWriter.print(this.uid);
        paramPrintWriter.print(" packageName=");
        paramPrintWriter.print(this.key.packageName);
        paramPrintWriter.print(" type=");
        paramPrintWriter.print(this.key.typeName());
        paramPrintWriter.print(" flags=0x");
        paramPrintWriter.println(Integer.toHexString(this.key.flags));
        if ((this.key.activity != null) || (this.key.who != null))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("activity=");
            paramPrintWriter.print(this.key.activity);
            paramPrintWriter.print(" who=");
            paramPrintWriter.println(this.key.who);
        }
        if ((this.key.requestCode != 0) || (this.key.requestResolvedType != null))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("requestCode=");
            paramPrintWriter.print(this.key.requestCode);
            paramPrintWriter.print(" requestResolvedType=");
            paramPrintWriter.println(this.key.requestResolvedType);
        }
        if (this.key.requestIntent != null)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("requestIntent=");
            paramPrintWriter.println(this.key.requestIntent.toShortString(false, true, true, true));
        }
        if ((this.sent) || (this.canceled))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("sent=");
            paramPrintWriter.print(this.sent);
            paramPrintWriter.print(" canceled=");
            paramPrintWriter.println(this.canceled);
        }
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            if (!this.canceled)
                this.owner.mHandler.sendMessage(this.owner.mHandler.obtainMessage(23, this));
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public int send(int paramInt, Intent paramIntent, String paramString1, IIntentReceiver paramIIntentReceiver, String paramString2)
    {
        return sendInner(paramInt, paramIntent, paramString1, paramIIntentReceiver, paramString2, null, null, 0, 0, 0, null);
    }

    int sendInner(int paramInt1, Intent paramIntent, String paramString1, IIntentReceiver paramIIntentReceiver, String paramString2, IBinder paramIBinder, String paramString3, int paramInt2, int paramInt3, int paramInt4, Bundle paramBundle)
    {
        while (true)
        {
            Intent localIntent;
            long l;
            synchronized (this.owner)
            {
                if (!this.canceled)
                {
                    this.sent = true;
                    if ((0x40000000 & this.key.flags) != 0)
                    {
                        this.owner.cancelIntentSenderLocked(this, true);
                        this.canceled = true;
                    }
                    if (this.key.requestIntent != null)
                    {
                        localIntent = new Intent(this.key.requestIntent);
                        if (paramIntent == null)
                            break label248;
                        if ((0x2 & localIntent.fillIn(paramIntent, this.key.flags)) == 0)
                            paramString1 = this.key.requestResolvedType;
                        int j = paramInt3 & 0xFFFFFFFC;
                        localIntent.setFlags(paramInt4 & j | localIntent.getFlags() & (j ^ 0xFFFFFFFF));
                        l = Binder.clearCallingIdentity();
                        if (paramIIntentReceiver == null)
                            break label685;
                        k = 1;
                        int m = this.key.type;
                        switch (m)
                        {
                        default:
                            if (k == 0);
                            break;
                        case 2:
                        case 3:
                        case 1:
                        case 4:
                        }
                    }
                }
            }
            label214: int i;
            try
            {
                paramIIntentReceiver.performReceive(new Intent(localIntent), 0, null, null, false, false);
                Binder.restoreCallingIdentity(l);
                i = 0;
                break label682;
                localIntent = new Intent();
                continue;
                localObject = finally;
                throw localObject;
                label248: paramString1 = this.key.requestResolvedType;
                continue;
                if (paramBundle == null)
                    paramBundle = this.key.options;
                while (true)
                {
                    try
                    {
                        if ((this.key.allIntents == null) || (this.key.allIntents.length <= 1))
                            break label474;
                        Intent[] arrayOfIntent = new Intent[this.key.allIntents.length];
                        String[] arrayOfString = new String[this.key.allIntents.length];
                        System.arraycopy(this.key.allIntents, 0, arrayOfIntent, 0, this.key.allIntents.length);
                        if (this.key.allResolvedTypes != null)
                            System.arraycopy(this.key.allResolvedTypes, 0, arrayOfString, 0, this.key.allResolvedTypes.length);
                        arrayOfIntent[(-1 + arrayOfIntent.length)] = localIntent;
                        arrayOfString[(-1 + arrayOfString.length)] = paramString1;
                        this.owner.startActivitiesInPackage(this.uid, arrayOfIntent, arrayOfString, paramIBinder, paramBundle);
                    }
                    catch (RuntimeException localRuntimeException3)
                    {
                        Slog.w("ActivityManager", "Unable to send startActivity intent", localRuntimeException3);
                    }
                    break;
                    if (this.key.options != null)
                    {
                        Bundle localBundle = new Bundle(this.key.options);
                        localBundle.putAll(paramBundle);
                        paramBundle = localBundle;
                    }
                }
                label474: this.owner.startActivityInPackage(this.uid, localIntent, paramString1, paramIBinder, paramString3, paramInt2, 0, paramBundle);
                continue;
                this.key.activity.stack.sendActivityResultLocked(-1, this.key.activity, this.key.who, this.key.requestCode, paramInt1, localIntent);
                continue;
                try
                {
                    ActivityManagerService localActivityManagerService2 = this.owner;
                    String str = this.key.packageName;
                    int n = this.uid;
                    if (paramIIntentReceiver != null);
                    for (boolean bool = true; ; bool = false)
                    {
                        int i1 = UserId.getUserId(this.uid);
                        localActivityManagerService2.broadcastIntentInPackage(str, n, localIntent, paramString1, paramIIntentReceiver, paramInt1, null, null, paramString2, bool, false, i1);
                        k = 0;
                        break;
                    }
                }
                catch (RuntimeException localRuntimeException2)
                {
                    Slog.w("ActivityManager", "Unable to send startActivity intent", localRuntimeException2);
                }
                continue;
                try
                {
                    this.owner.startServiceInPackage(this.uid, localIntent, paramString1);
                }
                catch (RuntimeException localRuntimeException1)
                {
                    Slog.w("ActivityManager", "Unable to send startService intent", localRuntimeException1);
                }
                continue;
                i = -6;
            }
            catch (RemoteException localRemoteException)
            {
                break label214;
            }
            label682: return i;
            label685: int k = 0;
        }
    }

    public String toString()
    {
        String str;
        if (this.stringName != null)
            str = this.stringName;
        while (true)
        {
            return str;
            StringBuilder localStringBuilder = new StringBuilder(128);
            localStringBuilder.append("PendingIntentRecord{");
            localStringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
            localStringBuilder.append(' ');
            localStringBuilder.append(this.key.packageName);
            localStringBuilder.append(' ');
            localStringBuilder.append(this.key.typeName());
            localStringBuilder.append('}');
            str = localStringBuilder.toString();
            this.stringName = str;
        }
    }

    static final class Key
    {
        private static final int ODD_PRIME_NUMBER = 37;
        final ActivityRecord activity;
        Intent[] allIntents;
        String[] allResolvedTypes;
        final int flags;
        final int hashCode;
        final Bundle options;
        final String packageName;
        final int requestCode;
        final Intent requestIntent;
        final String requestResolvedType;
        final int type;
        final String who;

        Key(int paramInt1, String paramString1, ActivityRecord paramActivityRecord, String paramString2, int paramInt2, Intent[] paramArrayOfIntent, String[] paramArrayOfString, int paramInt3, Bundle paramBundle)
        {
            this.type = paramInt1;
            this.packageName = paramString1;
            this.activity = paramActivityRecord;
            this.who = paramString2;
            this.requestCode = paramInt2;
            if (paramArrayOfIntent != null);
            for (Intent localIntent = paramArrayOfIntent[(-1 + paramArrayOfIntent.length)]; ; localIntent = null)
            {
                this.requestIntent = localIntent;
                if (paramArrayOfString != null)
                    str = paramArrayOfString[(-1 + paramArrayOfString.length)];
                this.requestResolvedType = str;
                this.allIntents = paramArrayOfIntent;
                this.allResolvedTypes = paramArrayOfString;
                this.flags = paramInt3;
                this.options = paramBundle;
                int i = paramInt2 + 37 * (paramInt3 + 851);
                if (paramString2 != null)
                    i = i * 37 + paramString2.hashCode();
                if (paramActivityRecord != null)
                    i = i * 37 + paramActivityRecord.hashCode();
                if (this.requestIntent != null)
                    i = i * 37 + this.requestIntent.filterHashCode();
                if (this.requestResolvedType != null)
                    i = i * 37 + this.requestResolvedType.hashCode();
                this.hashCode = (paramInt1 + 37 * (i * 37 + paramString1.hashCode()));
                return;
            }
        }

        public boolean equals(Object paramObject)
        {
            boolean bool = false;
            if (paramObject == null);
            while (true)
            {
                return bool;
                try
                {
                    Key localKey = (Key)paramObject;
                    if ((this.type == localKey.type) && (this.packageName.equals(localKey.packageName)) && (this.activity == localKey.activity))
                        label133: if (this.who != localKey.who)
                        {
                            if (this.who != null)
                                if (!this.who.equals(localKey.who))
                                    continue;
                        }
                        else
                            label87: if (this.requestCode == localKey.requestCode)
                                if (this.requestIntent != localKey.requestIntent)
                                {
                                    if (this.requestIntent != null)
                                        if (!this.requestIntent.filterEquals(localKey.requestIntent))
                                            continue;
                                }
                                else if (this.requestResolvedType != localKey.requestResolvedType)
                                {
                                    if (this.requestResolvedType != null)
                                        if (!this.requestResolvedType.equals(localKey.requestResolvedType))
                                            continue;
                                }
                                else
                                {
                                    String str;
                                    do
                                    {
                                        if (this.flags != localKey.flags)
                                            break;
                                        bool = true;
                                        break;
                                        if (localKey.who == null)
                                            break label87;
                                        break;
                                        if (localKey.requestIntent == null)
                                            break label133;
                                        break;
                                        str = localKey.requestResolvedType;
                                    }
                                    while (str == null);
                                }
                }
                catch (ClassCastException localClassCastException)
                {
                }
            }
        }

        public int hashCode()
        {
            return this.hashCode;
        }

        public String toString()
        {
            StringBuilder localStringBuilder = new StringBuilder().append("Key{").append(typeName()).append(" pkg=").append(this.packageName).append(" intent=");
            if (this.requestIntent != null);
            for (String str = this.requestIntent.toShortString(false, true, false, false); ; str = "<null>")
                return str + " flags=0x" + Integer.toHexString(this.flags) + "}";
        }

        String typeName()
        {
            String str;
            switch (this.type)
            {
            default:
                str = Integer.toString(this.type);
            case 2:
            case 1:
            case 4:
            case 3:
            }
            while (true)
            {
                return str;
                str = "startActivity";
                continue;
                str = "broadcastIntent";
                continue;
                str = "startService";
                continue;
                str = "activityResult";
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.PendingIntentRecord
 * JD-Core Version:        0.6.2
 */