package com.android.server.am;

import android.content.ComponentName;
import android.content.Context;
import android.os.Binder;
import android.os.FileUtils;
import android.os.Parcel;
import android.os.Process;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.util.Slog;
import com.android.internal.app.IUsageStats;
import com.android.internal.app.IUsageStats.Stub;
import com.android.internal.content.PackageMonitor;
import com.android.internal.os.AtomicFile;
import com.android.internal.os.PkgUsageStats;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public final class UsageStatsService extends IUsageStats.Stub
{
    private static final int CHECKIN_VERSION = 4;
    private static final String FILE_HISTORY = "usage-history.xml";
    private static final String FILE_PREFIX = "usage-";
    private static final int FILE_WRITE_INTERVAL = 1800000;
    private static final int[] LAUNCH_TIME_BINS = arrayOfInt;
    private static final int MAX_NUM_FILES = 5;
    private static final int NUM_LAUNCH_TIME_BINS = 10;
    private static final boolean REPORT_UNEXPECTED = false;
    public static final String SERVICE_NAME = "usagestats";
    private static final String TAG = "UsageStats";
    private static final int VERSION = 1007;
    private static final boolean localLOGV;
    static IUsageStats sService;
    private Calendar mCal;
    private Context mContext;
    private File mDir;
    private File mFile;
    private String mFileLeaf;
    final Object mFileLock = new Object();
    private AtomicFile mHistoryFile;
    private boolean mIsResumed;
    private final Map<String, Map<String, Long>> mLastResumeTimes = new HashMap();
    private String mLastResumedComp;
    private String mLastResumedPkg;
    private final AtomicInteger mLastWriteDay = new AtomicInteger(-1);
    private final AtomicLong mLastWriteElapsedTime = new AtomicLong(0L);
    private PackageMonitor mPackageMonitor;
    private final Map<String, PkgUsageStatsExtended> mStats = new HashMap();
    final Object mStatsLock = new Object();
    private final AtomicBoolean mUnforcedDiskWriteRunning = new AtomicBoolean(false);

    static
    {
        int[] arrayOfInt = new int[9];
        arrayOfInt[0] = 250;
        arrayOfInt[1] = 500;
        arrayOfInt[2] = 750;
        arrayOfInt[3] = 1000;
        arrayOfInt[4] = 1500;
        arrayOfInt[5] = 2000;
        arrayOfInt[6] = 3000;
        arrayOfInt[7] = 4000;
        arrayOfInt[8] = 5000;
    }

    UsageStatsService(String paramString)
    {
        this.mDir = new File(paramString);
        this.mCal = Calendar.getInstance(TimeZone.getTimeZone("GMT+0"));
        this.mDir.mkdir();
        File localFile = this.mDir.getParentFile();
        String[] arrayOfString = localFile.list();
        if (arrayOfString != null)
        {
            String str = this.mDir.getName() + ".";
            int i = arrayOfString.length;
            while (i > 0)
            {
                i--;
                if (arrayOfString[i].startsWith(str))
                {
                    Slog.i("UsageStats", "Deleting old usage file: " + arrayOfString[i]);
                    new File(localFile, arrayOfString[i]).delete();
                }
            }
        }
        this.mFileLeaf = getCurrentDateStr("usage-");
        this.mFile = new File(this.mDir, this.mFileLeaf);
        this.mHistoryFile = new AtomicFile(new File(this.mDir, "usage-history.xml"));
        readStatsFromFile();
        readHistoryStatsFromFile();
        this.mLastWriteElapsedTime.set(SystemClock.elapsedRealtime());
        this.mLastWriteDay.set(this.mCal.get(6));
    }

    private void checkFileLimitFLOCK()
    {
        ArrayList localArrayList = getUsageStatsFileListFLOCK();
        if (localArrayList == null);
        while (true)
        {
            return;
            int i = localArrayList.size();
            if (i > 5)
            {
                Collections.sort(localArrayList);
                int j = i - 5;
                for (int k = 0; k < j; k++)
                {
                    String str = (String)localArrayList.get(k);
                    File localFile = new File(this.mDir, str);
                    Slog.i("UsageStats", "Deleting usage file : " + str);
                    localFile.delete();
                }
            }
        }
    }

    private void collectDumpInfoFLOCK(PrintWriter paramPrintWriter, boolean paramBoolean1, boolean paramBoolean2, HashSet<String> paramHashSet)
    {
        ArrayList localArrayList = getUsageStatsFileListFLOCK();
        if (localArrayList == null);
        label204: 
        while (true)
        {
            return;
            Collections.sort(localArrayList);
            Iterator localIterator = localArrayList.iterator();
            while (true)
                while (true)
                {
                    if (!localIterator.hasNext())
                        break label204;
                    String str1 = (String)localIterator.next();
                    if ((!paramBoolean2) || (!str1.equalsIgnoreCase(this.mFileLeaf)))
                    {
                        File localFile = new File(this.mDir, str1);
                        String str2 = str1.substring("usage-".length());
                        try
                        {
                            collectDumpInfoFromParcelFLOCK(getParcelForFile(localFile), paramPrintWriter, str2, paramBoolean1, paramHashSet);
                            if (paramBoolean2)
                                localFile.delete();
                        }
                        catch (FileNotFoundException localFileNotFoundException)
                        {
                            Slog.w("UsageStats", "Failed with " + localFileNotFoundException + " when collecting dump info from file : " + str1);
                            break;
                        }
                        catch (IOException localIOException)
                        {
                            Slog.w("UsageStats", "Failed with " + localIOException + " when collecting dump info from file : " + str1);
                        }
                    }
                }
        }
    }

    private void collectDumpInfoFromParcelFLOCK(Parcel paramParcel, PrintWriter paramPrintWriter, String paramString, boolean paramBoolean, HashSet<String> paramHashSet)
    {
        StringBuilder localStringBuilder = new StringBuilder(512);
        if (paramBoolean)
        {
            localStringBuilder.append("D:");
            localStringBuilder.append(4);
            localStringBuilder.append(',');
            localStringBuilder.append(paramString);
            if (paramParcel.readInt() == 1007)
                break label89;
            localStringBuilder.append(" (old data version)");
            paramPrintWriter.println(localStringBuilder.toString());
        }
        label89: label104: String str1;
        do
        {
            return;
            localStringBuilder.append("Date: ");
            break;
            paramPrintWriter.println(localStringBuilder.toString());
            int i = paramParcel.readInt();
            if (i <= 0)
                break label165;
            i--;
            str1 = paramParcel.readString();
        }
        while (str1 == null);
        localStringBuilder.setLength(0);
        PkgUsageStatsExtended localPkgUsageStatsExtended = new PkgUsageStatsExtended(paramParcel);
        if ((paramHashSet != null) && (!paramHashSet.contains(str1)));
        while (true)
        {
            paramPrintWriter.write(localStringBuilder.toString());
            break label104;
            label165: break;
            if (paramBoolean)
            {
                localStringBuilder.append("P:");
                localStringBuilder.append(str1);
                localStringBuilder.append(',');
                localStringBuilder.append(localPkgUsageStatsExtended.mLaunchCount);
                localStringBuilder.append(',');
                localStringBuilder.append(localPkgUsageStatsExtended.mUsageTime);
                localStringBuilder.append('\n');
                if (localPkgUsageStatsExtended.mLaunchTimes.size() > 0)
                {
                    Iterator localIterator2 = localPkgUsageStatsExtended.mLaunchTimes.entrySet().iterator();
                    while (localIterator2.hasNext())
                    {
                        Map.Entry localEntry2 = (Map.Entry)localIterator2.next();
                        localStringBuilder.append("A:");
                        String str2 = (String)localEntry2.getKey();
                        if (str2.startsWith(str1))
                        {
                            localStringBuilder.append('*');
                            localStringBuilder.append(str2.substring(str1.length(), str2.length()));
                        }
                        while (true)
                        {
                            TimeStats localTimeStats2 = (TimeStats)localEntry2.getValue();
                            localStringBuilder.append(',');
                            localStringBuilder.append(localTimeStats2.count);
                            for (int m = 0; m < 10; m++)
                            {
                                localStringBuilder.append(",");
                                localStringBuilder.append(localTimeStats2.times[m]);
                            }
                            localStringBuilder.append(str2);
                        }
                        localStringBuilder.append('\n');
                    }
                }
            }
            else
            {
                localStringBuilder.append("    ");
                localStringBuilder.append(str1);
                localStringBuilder.append(": ");
                localStringBuilder.append(localPkgUsageStatsExtended.mLaunchCount);
                localStringBuilder.append(" times, ");
                localStringBuilder.append(localPkgUsageStatsExtended.mUsageTime);
                localStringBuilder.append(" ms");
                localStringBuilder.append('\n');
                if (localPkgUsageStatsExtended.mLaunchTimes.size() > 0)
                {
                    Iterator localIterator1 = localPkgUsageStatsExtended.mLaunchTimes.entrySet().iterator();
                    while (localIterator1.hasNext())
                    {
                        Map.Entry localEntry1 = (Map.Entry)localIterator1.next();
                        localStringBuilder.append("        ");
                        localStringBuilder.append((String)localEntry1.getKey());
                        TimeStats localTimeStats1 = (TimeStats)localEntry1.getValue();
                        localStringBuilder.append(": ");
                        localStringBuilder.append(localTimeStats1.count);
                        localStringBuilder.append(" starts");
                        int j = 0;
                        for (int k = 0; k < 9; k++)
                        {
                            if (localTimeStats1.times[k] != 0)
                            {
                                localStringBuilder.append(", ");
                                localStringBuilder.append(j);
                                localStringBuilder.append('-');
                                localStringBuilder.append(LAUNCH_TIME_BINS[k]);
                                localStringBuilder.append("ms=");
                                localStringBuilder.append(localTimeStats1.times[k]);
                            }
                            j = LAUNCH_TIME_BINS[k];
                        }
                        if (localTimeStats1.times[9] != 0)
                        {
                            localStringBuilder.append(", ");
                            localStringBuilder.append(">=");
                            localStringBuilder.append(j);
                            localStringBuilder.append("ms=");
                            localStringBuilder.append(localTimeStats1.times[9]);
                        }
                        localStringBuilder.append('\n');
                    }
                }
            }
        }
    }

    // ERROR //
    private void filterHistoryStats()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 116	com/android/server/am/UsageStatsService:mStatsLock	Ljava/lang/Object;
        //     4: astore_1
        //     5: aload_1
        //     6: monitorenter
        //     7: new 106	java/util/HashMap
        //     10: dup
        //     11: aload_0
        //     12: getfield 111	com/android/server/am/UsageStatsService:mLastResumeTimes	Ljava/util/Map;
        //     15: invokespecial 415	java/util/HashMap:<init>	(Ljava/util/Map;)V
        //     18: astore_2
        //     19: aload_0
        //     20: getfield 111	com/android/server/am/UsageStatsService:mLastResumeTimes	Ljava/util/Map;
        //     23: invokeinterface 420 1 0
        //     28: aload_0
        //     29: getfield 422	com/android/server/am/UsageStatsService:mContext	Landroid/content/Context;
        //     32: invokevirtual 428	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
        //     35: iconst_0
        //     36: invokevirtual 434	android/content/pm/PackageManager:getInstalledPackages	(I)Ljava/util/List;
        //     39: invokeinterface 270 1 0
        //     44: astore 4
        //     46: aload 4
        //     48: invokeinterface 275 1 0
        //     53: ifeq +63 -> 116
        //     56: aload 4
        //     58: invokeinterface 279 1 0
        //     63: checkcast 436	android/content/pm/PackageInfo
        //     66: astore 5
        //     68: aload_2
        //     69: aload 5
        //     71: getfield 439	android/content/pm/PackageInfo:packageName	Ljava/lang/String;
        //     74: invokeinterface 442 2 0
        //     79: ifeq -33 -> 46
        //     82: aload_0
        //     83: getfield 111	com/android/server/am/UsageStatsService:mLastResumeTimes	Ljava/util/Map;
        //     86: aload 5
        //     88: getfield 439	android/content/pm/PackageInfo:packageName	Ljava/lang/String;
        //     91: aload_2
        //     92: aload 5
        //     94: getfield 439	android/content/pm/PackageInfo:packageName	Ljava/lang/String;
        //     97: invokeinterface 445 2 0
        //     102: invokeinterface 449 3 0
        //     107: pop
        //     108: goto -62 -> 46
        //     111: astore_3
        //     112: aload_1
        //     113: monitorexit
        //     114: aload_3
        //     115: athrow
        //     116: aload_1
        //     117: monitorexit
        //     118: return
        //
        // Exception table:
        //     from	to	target	type
        //     7	114	111	finally
        //     116	118	111	finally
    }

    private String getCurrentDateStr(String paramString)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        synchronized (this.mCal)
        {
            this.mCal.setTimeInMillis(System.currentTimeMillis());
            if (paramString != null)
                localStringBuilder.append(paramString);
            localStringBuilder.append(this.mCal.get(1));
            int i = 1 + (0 + this.mCal.get(2));
            if (i < 10)
                localStringBuilder.append("0");
            localStringBuilder.append(i);
            int j = this.mCal.get(5);
            if (j < 10)
                localStringBuilder.append("0");
            localStringBuilder.append(j);
            return localStringBuilder.toString();
        }
    }

    private Parcel getParcelForFile(File paramFile)
        throws IOException
    {
        FileInputStream localFileInputStream = new FileInputStream(paramFile);
        byte[] arrayOfByte = readFully(localFileInputStream);
        Parcel localParcel = Parcel.obtain();
        localParcel.unmarshall(arrayOfByte, 0, arrayOfByte.length);
        localParcel.setDataPosition(0);
        localFileInputStream.close();
        return localParcel;
    }

    public static IUsageStats getService()
    {
        if (sService != null);
        for (IUsageStats localIUsageStats = sService; ; localIUsageStats = sService)
        {
            return localIUsageStats;
            sService = asInterface(ServiceManager.getService("usagestats"));
        }
    }

    private ArrayList<String> getUsageStatsFileListFLOCK()
    {
        String[] arrayOfString = this.mDir.list();
        if (arrayOfString == null)
        {
            localObject = null;
            return localObject;
        }
        Object localObject = new ArrayList();
        int i = arrayOfString.length;
        int j = 0;
        label30: String str;
        if (j < i)
        {
            str = arrayOfString[j];
            if (str.startsWith("usage-"))
                break label58;
        }
        while (true)
        {
            j++;
            break label30;
            break;
            label58: if (str.endsWith(".bak"))
                new File(this.mDir, str).delete();
            else
                ((ArrayList)localObject).add(str);
        }
    }

    static byte[] readFully(FileInputStream paramFileInputStream)
        throws IOException
    {
        int i = 0;
        Object localObject = new byte[paramFileInputStream.available()];
        while (true)
        {
            int j = paramFileInputStream.read((byte[])localObject, i, localObject.length - i);
            if (j <= 0)
                return localObject;
            i += j;
            int k = paramFileInputStream.available();
            if (k > localObject.length - i)
            {
                byte[] arrayOfByte = new byte[i + k];
                System.arraycopy(localObject, 0, arrayOfByte, 0, i);
                localObject = arrayOfByte;
            }
        }
    }

    // ERROR //
    private void readHistoryStatsFLOCK(AtomicFile paramAtomicFile)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: aload_0
        //     3: getfield 203	com/android/server/am/UsageStatsService:mHistoryFile	Lcom/android/internal/os/AtomicFile;
        //     6: invokevirtual 523	com/android/internal/os/AtomicFile:openRead	()Ljava/io/FileInputStream;
        //     9: astore_2
        //     10: invokestatic 529	android/util/Xml:newPullParser	()Lorg/xmlpull/v1/XmlPullParser;
        //     13: astore 11
        //     15: aload 11
        //     17: aload_2
        //     18: aconst_null
        //     19: invokeinterface 535 3 0
        //     24: aload 11
        //     26: invokeinterface 538 1 0
        //     31: istore 12
        //     33: iload 12
        //     35: iconst_2
        //     36: if_icmpeq +15 -> 51
        //     39: aload 11
        //     41: invokeinterface 540 1 0
        //     46: istore 12
        //     48: goto -15 -> 33
        //     51: ldc_w 542
        //     54: aload 11
        //     56: invokeinterface 543 1 0
        //     61: invokevirtual 546	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     64: ifeq +79 -> 143
        //     67: aconst_null
        //     68: astore 14
        //     70: aload 11
        //     72: invokeinterface 540 1 0
        //     77: istore 15
        //     79: iload 15
        //     81: iconst_2
        //     82: if_icmpne +222 -> 304
        //     85: aload 11
        //     87: invokeinterface 543 1 0
        //     92: astore 17
        //     94: aload 11
        //     96: invokeinterface 549 1 0
        //     101: istore 18
        //     103: ldc_w 551
        //     106: aload 17
        //     108: invokevirtual 546	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     111: ifeq +41 -> 152
        //     114: iload 18
        //     116: iconst_2
        //     117: if_icmpne +35 -> 152
        //     120: aload 11
        //     122: aconst_null
        //     123: ldc_w 553
        //     126: invokeinterface 557 3 0
        //     131: astore 29
        //     133: aload 29
        //     135: astore 14
        //     137: iload 15
        //     139: iconst_1
        //     140: if_icmpne -70 -> 70
        //     143: aload_2
        //     144: ifnull +7 -> 151
        //     147: aload_2
        //     148: invokevirtual 480	java/io/FileInputStream:close	()V
        //     151: return
        //     152: ldc_w 559
        //     155: aload 17
        //     157: invokevirtual 546	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     160: ifeq -23 -> 137
        //     163: iload 18
        //     165: iconst_3
        //     166: if_icmpne -29 -> 137
        //     169: aload 14
        //     171: ifnull -34 -> 137
        //     174: aload 11
        //     176: aconst_null
        //     177: ldc_w 553
        //     180: invokeinterface 557 3 0
        //     185: astore 19
        //     187: aload 11
        //     189: aconst_null
        //     190: ldc_w 561
        //     193: invokeinterface 557 3 0
        //     198: astore 20
        //     200: aload 19
        //     202: ifnull -65 -> 137
        //     205: aload 20
        //     207: ifnull -70 -> 137
        //     210: aload 20
        //     212: invokestatic 567	java/lang/Long:parseLong	(Ljava/lang/String;)J
        //     215: lstore 22
        //     217: aload_0
        //     218: getfield 116	com/android/server/am/UsageStatsService:mStatsLock	Ljava/lang/Object;
        //     221: astore 24
        //     223: aload 24
        //     225: monitorenter
        //     226: aload_0
        //     227: getfield 111	com/android/server/am/UsageStatsService:mLastResumeTimes	Ljava/util/Map;
        //     230: aload 14
        //     232: invokeinterface 445 2 0
        //     237: checkcast 417	java/util/Map
        //     240: astore 26
        //     242: aload 26
        //     244: ifnonnull +26 -> 270
        //     247: new 106	java/util/HashMap
        //     250: dup
        //     251: invokespecial 107	java/util/HashMap:<init>	()V
        //     254: astore 26
        //     256: aload_0
        //     257: getfield 111	com/android/server/am/UsageStatsService:mLastResumeTimes	Ljava/util/Map;
        //     260: aload 14
        //     262: aload 26
        //     264: invokeinterface 449 3 0
        //     269: pop
        //     270: aload 26
        //     272: aload 19
        //     274: lload 22
        //     276: invokestatic 571	java/lang/Long:valueOf	(J)Ljava/lang/Long;
        //     279: invokeinterface 449 3 0
        //     284: pop
        //     285: aload 24
        //     287: monitorexit
        //     288: goto -151 -> 137
        //     291: astore 25
        //     293: aload 24
        //     295: monitorexit
        //     296: aload 25
        //     298: athrow
        //     299: astore 21
        //     301: goto -164 -> 137
        //     304: iload 15
        //     306: iconst_3
        //     307: if_icmpne -170 -> 137
        //     310: ldc_w 551
        //     313: aload 11
        //     315: invokeinterface 543 1 0
        //     320: invokevirtual 546	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     323: istore 16
        //     325: iload 16
        //     327: ifeq -190 -> 137
        //     330: aconst_null
        //     331: astore 14
        //     333: goto -196 -> 137
        //     336: astore 8
        //     338: ldc 41
        //     340: new 154	java/lang/StringBuilder
        //     343: dup
        //     344: invokespecial 155	java/lang/StringBuilder:<init>	()V
        //     347: ldc_w 573
        //     350: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     353: aload 8
        //     355: invokevirtual 302	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     358: invokevirtual 168	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     361: invokestatic 307	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     364: pop
        //     365: aload_2
        //     366: ifnull -215 -> 151
        //     369: aload_2
        //     370: invokevirtual 480	java/io/FileInputStream:close	()V
        //     373: goto -222 -> 151
        //     376: astore 10
        //     378: goto -227 -> 151
        //     381: astore 5
        //     383: ldc 41
        //     385: new 154	java/lang/StringBuilder
        //     388: dup
        //     389: invokespecial 155	java/lang/StringBuilder:<init>	()V
        //     392: ldc_w 573
        //     395: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     398: aload 5
        //     400: invokevirtual 302	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     403: invokevirtual 168	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     406: invokestatic 307	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     409: pop
        //     410: aload_2
        //     411: ifnull -260 -> 151
        //     414: aload_2
        //     415: invokevirtual 480	java/io/FileInputStream:close	()V
        //     418: goto -267 -> 151
        //     421: astore 7
        //     423: goto -272 -> 151
        //     426: astore_3
        //     427: aload_2
        //     428: ifnull +7 -> 435
        //     431: aload_2
        //     432: invokevirtual 480	java/io/FileInputStream:close	()V
        //     435: aload_3
        //     436: athrow
        //     437: astore 13
        //     439: goto -288 -> 151
        //     442: astore 4
        //     444: goto -9 -> 435
        //
        // Exception table:
        //     from	to	target	type
        //     226	296	291	finally
        //     210	226	299	java/lang/NumberFormatException
        //     296	299	299	java/lang/NumberFormatException
        //     2	133	336	org/xmlpull/v1/XmlPullParserException
        //     152	200	336	org/xmlpull/v1/XmlPullParserException
        //     210	226	336	org/xmlpull/v1/XmlPullParserException
        //     296	299	336	org/xmlpull/v1/XmlPullParserException
        //     310	325	336	org/xmlpull/v1/XmlPullParserException
        //     369	373	376	java/io/IOException
        //     2	133	381	java/io/IOException
        //     152	200	381	java/io/IOException
        //     210	226	381	java/io/IOException
        //     296	299	381	java/io/IOException
        //     310	325	381	java/io/IOException
        //     414	418	421	java/io/IOException
        //     2	133	426	finally
        //     152	200	426	finally
        //     210	226	426	finally
        //     296	299	426	finally
        //     310	325	426	finally
        //     338	365	426	finally
        //     383	410	426	finally
        //     147	151	437	java/io/IOException
        //     431	435	442	java/io/IOException
    }

    private void readHistoryStatsFromFile()
    {
        synchronized (this.mFileLock)
        {
            if (this.mHistoryFile.getBaseFile().exists())
                readHistoryStatsFLOCK(this.mHistoryFile);
            return;
        }
    }

    private void readStatsFLOCK(File paramFile)
        throws IOException
    {
        Parcel localParcel = getParcelForFile(paramFile);
        if (localParcel.readInt() != 1007)
        {
            Slog.w("UsageStats", "Usage stats version changed; dropping");
            return;
        }
        int i = localParcel.readInt();
        while (i > 0)
        {
            i--;
            String str = localParcel.readString();
            if (str == null)
                break;
            PkgUsageStatsExtended localPkgUsageStatsExtended = new PkgUsageStatsExtended(localParcel);
            synchronized (this.mStatsLock)
            {
                this.mStats.put(str, localPkgUsageStatsExtended);
            }
        }
    }

    private void readStatsFromFile()
    {
        File localFile = this.mFile;
        synchronized (this.mFileLock)
        {
            try
            {
                if (localFile.exists())
                    readStatsFLOCK(localFile);
                while (true)
                {
                    return;
                    checkFileLimitFLOCK();
                    localFile.createNewFile();
                }
            }
            catch (IOException localIOException)
            {
                while (true)
                    Slog.w("UsageStats", "Error : " + localIOException + " reading data from file:" + localFile);
            }
        }
    }

    private static boolean scanArgs(String[] paramArrayOfString, String paramString)
    {
        int j;
        if (paramArrayOfString != null)
        {
            int i = paramArrayOfString.length;
            j = 0;
            if (j < i)
                if (!paramString.equals(paramArrayOfString[j]));
        }
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            j++;
            break;
        }
    }

    private static String scanArgsData(String[] paramArrayOfString, String paramString)
    {
        String str = null;
        int i;
        if (paramArrayOfString != null)
            i = paramArrayOfString.length;
        for (int j = 0; ; j++)
        {
            if (j < i)
            {
                if (!paramString.equals(paramArrayOfString[j]))
                    continue;
                int k = j + 1;
                if (k < i)
                    str = paramArrayOfString[k];
            }
            return str;
        }
    }

    // ERROR //
    private void writeHistoryStatsFLOCK(AtomicFile paramAtomicFile)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: aload_1
        //     3: invokevirtual 604	com/android/internal/os/AtomicFile:startWrite	()Ljava/io/FileOutputStream;
        //     6: astore_2
        //     7: new 606	com/android/internal/util/FastXmlSerializer
        //     10: dup
        //     11: invokespecial 607	com/android/internal/util/FastXmlSerializer:<init>	()V
        //     14: astore 5
        //     16: aload 5
        //     18: aload_2
        //     19: ldc_w 609
        //     22: invokeinterface 615 3 0
        //     27: aload 5
        //     29: aconst_null
        //     30: iconst_1
        //     31: invokestatic 620	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
        //     34: invokeinterface 624 3 0
        //     39: aload 5
        //     41: ldc_w 626
        //     44: iconst_1
        //     45: invokeinterface 630 3 0
        //     50: aload 5
        //     52: aconst_null
        //     53: ldc_w 542
        //     56: invokeinterface 634 3 0
        //     61: pop
        //     62: aload_0
        //     63: getfield 116	com/android/server/am/UsageStatsService:mStatsLock	Ljava/lang/Object;
        //     66: astore 7
        //     68: aload 7
        //     70: monitorenter
        //     71: aload_0
        //     72: getfield 111	com/android/server/am/UsageStatsService:mLastResumeTimes	Ljava/util/Map;
        //     75: invokeinterface 635 1 0
        //     80: invokeinterface 372 1 0
        //     85: astore 9
        //     87: aload 9
        //     89: invokeinterface 275 1 0
        //     94: ifeq +227 -> 321
        //     97: aload 9
        //     99: invokeinterface 279 1 0
        //     104: checkcast 374	java/util/Map$Entry
        //     107: astore 11
        //     109: aload 5
        //     111: aconst_null
        //     112: ldc_w 551
        //     115: invokeinterface 634 3 0
        //     120: pop
        //     121: aload 5
        //     123: aconst_null
        //     124: ldc_w 553
        //     127: aload 11
        //     129: invokeinterface 379 1 0
        //     134: checkcast 170	java/lang/String
        //     137: invokeinterface 639 4 0
        //     142: pop
        //     143: aload 11
        //     145: invokeinterface 385 1 0
        //     150: checkcast 417	java/util/Map
        //     153: invokeinterface 635 1 0
        //     158: invokeinterface 372 1 0
        //     163: astore 14
        //     165: aload 14
        //     167: invokeinterface 275 1 0
        //     172: ifeq +134 -> 306
        //     175: aload 14
        //     177: invokeinterface 279 1 0
        //     182: checkcast 374	java/util/Map$Entry
        //     185: astore 16
        //     187: aload 5
        //     189: aconst_null
        //     190: ldc_w 559
        //     193: invokeinterface 634 3 0
        //     198: pop
        //     199: aload 5
        //     201: aconst_null
        //     202: ldc_w 553
        //     205: aload 16
        //     207: invokeinterface 379 1 0
        //     212: checkcast 170	java/lang/String
        //     215: invokeinterface 639 4 0
        //     220: pop
        //     221: aload 5
        //     223: aconst_null
        //     224: ldc_w 561
        //     227: aload 16
        //     229: invokeinterface 385 1 0
        //     234: checkcast 563	java/lang/Long
        //     237: invokevirtual 640	java/lang/Long:toString	()Ljava/lang/String;
        //     240: invokeinterface 639 4 0
        //     245: pop
        //     246: aload 5
        //     248: aconst_null
        //     249: ldc_w 559
        //     252: invokeinterface 643 3 0
        //     257: pop
        //     258: goto -93 -> 165
        //     261: astore 8
        //     263: aload 7
        //     265: monitorexit
        //     266: aload 8
        //     268: athrow
        //     269: astore_3
        //     270: ldc 41
        //     272: new 154	java/lang/StringBuilder
        //     275: dup
        //     276: invokespecial 155	java/lang/StringBuilder:<init>	()V
        //     279: ldc_w 645
        //     282: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     285: aload_3
        //     286: invokevirtual 302	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     289: invokevirtual 168	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     292: invokestatic 307	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     295: pop
        //     296: aload_2
        //     297: ifnull +8 -> 305
        //     300: aload_1
        //     301: aload_2
        //     302: invokevirtual 649	com/android/internal/os/AtomicFile:failWrite	(Ljava/io/FileOutputStream;)V
        //     305: return
        //     306: aload 5
        //     308: aconst_null
        //     309: ldc_w 551
        //     312: invokeinterface 643 3 0
        //     317: pop
        //     318: goto -231 -> 87
        //     321: aload 7
        //     323: monitorexit
        //     324: aload 5
        //     326: aconst_null
        //     327: ldc_w 542
        //     330: invokeinterface 643 3 0
        //     335: pop
        //     336: aload 5
        //     338: invokeinterface 652 1 0
        //     343: aload_1
        //     344: aload_2
        //     345: invokevirtual 655	com/android/internal/os/AtomicFile:finishWrite	(Ljava/io/FileOutputStream;)V
        //     348: goto -43 -> 305
        //
        // Exception table:
        //     from	to	target	type
        //     71	266	261	finally
        //     306	324	261	finally
        //     2	71	269	java/io/IOException
        //     266	269	269	java/io/IOException
        //     324	348	269	java/io/IOException
    }

    private void writeStatsFLOCK(File paramFile)
        throws IOException
    {
        FileOutputStream localFileOutputStream = new FileOutputStream(paramFile);
        try
        {
            Parcel localParcel = Parcel.obtain();
            writeStatsToParcelFLOCK(localParcel);
            localFileOutputStream.write(localParcel.marshall());
            localParcel.recycle();
            localFileOutputStream.flush();
            return;
        }
        finally
        {
            FileUtils.sync(localFileOutputStream);
            localFileOutputStream.close();
        }
    }

    // ERROR //
    private void writeStatsToFile(boolean paramBoolean1, boolean paramBoolean2)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 140	com/android/server/am/UsageStatsService:mCal	Ljava/util/Calendar;
        //     4: astore_3
        //     5: aload_3
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 140	com/android/server/am/UsageStatsService:mCal	Ljava/util/Calendar;
        //     11: invokestatic 454	java/lang/System:currentTimeMillis	()J
        //     14: invokevirtual 457	java/util/Calendar:setTimeInMillis	(J)V
        //     17: aload_0
        //     18: getfield 140	com/android/server/am/UsageStatsService:mCal	Ljava/util/Calendar;
        //     21: bipush 6
        //     23: invokevirtual 222	java/util/Calendar:get	(I)I
        //     26: istore 5
        //     28: aload_3
        //     29: monitorexit
        //     30: iload 5
        //     32: aload_0
        //     33: getfield 90	com/android/server/am/UsageStatsService:mLastWriteDay	Ljava/util/concurrent/atomic/AtomicInteger;
        //     36: invokevirtual 685	java/util/concurrent/atomic/AtomicInteger:get	()I
        //     39: if_icmpeq +45 -> 84
        //     42: iconst_1
        //     43: istore 6
        //     45: invokestatic 215	android/os/SystemClock:elapsedRealtime	()J
        //     48: lstore 7
        //     50: iload_1
        //     51: ifne +68 -> 119
        //     54: iload 6
        //     56: ifne +34 -> 90
        //     59: lload 7
        //     61: aload_0
        //     62: getfield 97	com/android/server/am/UsageStatsService:mLastWriteElapsedTime	Ljava/util/concurrent/atomic/AtomicLong;
        //     65: invokevirtual 687	java/util/concurrent/atomic/AtomicLong:get	()J
        //     68: lsub
        //     69: ldc2_w 688
        //     72: lcmp
        //     73: ifge +17 -> 90
        //     76: return
        //     77: astore 4
        //     79: aload_3
        //     80: monitorexit
        //     81: aload 4
        //     83: athrow
        //     84: iconst_0
        //     85: istore 6
        //     87: goto -42 -> 45
        //     90: aload_0
        //     91: getfield 104	com/android/server/am/UsageStatsService:mUnforcedDiskWriteRunning	Ljava/util/concurrent/atomic/AtomicBoolean;
        //     94: iconst_0
        //     95: iconst_1
        //     96: invokevirtual 693	java/util/concurrent/atomic/AtomicBoolean:compareAndSet	(ZZ)Z
        //     99: ifeq -23 -> 76
        //     102: new 6	com/android/server/am/UsageStatsService$1
        //     105: dup
        //     106: aload_0
        //     107: ldc_w 695
        //     110: invokespecial 698	com/android/server/am/UsageStatsService$1:<init>	(Lcom/android/server/am/UsageStatsService;Ljava/lang/String;)V
        //     113: invokevirtual 701	com/android/server/am/UsageStatsService$1:start	()V
        //     116: goto -40 -> 76
        //     119: aload_0
        //     120: getfield 118	com/android/server/am/UsageStatsService:mFileLock	Ljava/lang/Object;
        //     123: astore 9
        //     125: aload 9
        //     127: monitorenter
        //     128: aload_0
        //     129: aload_0
        //     130: ldc 24
        //     132: invokespecial 192	com/android/server/am/UsageStatsService:getCurrentDateStr	(Ljava/lang/String;)Ljava/lang/String;
        //     135: putfield 194	com/android/server/am/UsageStatsService:mFileLeaf	Ljava/lang/String;
        //     138: aconst_null
        //     139: astore 11
        //     141: aload_0
        //     142: getfield 196	com/android/server/am/UsageStatsService:mFile	Ljava/io/File;
        //     145: ifnull +99 -> 244
        //     148: aload_0
        //     149: getfield 196	com/android/server/am/UsageStatsService:mFile	Ljava/io/File;
        //     152: invokevirtual 579	java/io/File:exists	()Z
        //     155: ifeq +89 -> 244
        //     158: new 120	java/io/File
        //     161: dup
        //     162: new 154	java/lang/StringBuilder
        //     165: dup
        //     166: invokespecial 155	java/lang/StringBuilder:<init>	()V
        //     169: aload_0
        //     170: getfield 196	com/android/server/am/UsageStatsService:mFile	Ljava/io/File;
        //     173: invokevirtual 704	java/io/File:getPath	()Ljava/lang/String;
        //     176: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     179: ldc_w 496
        //     182: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     185: invokevirtual 168	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     188: invokespecial 122	java/io/File:<init>	(Ljava/lang/String;)V
        //     191: astore 11
        //     193: aload 11
        //     195: invokevirtual 579	java/io/File:exists	()Z
        //     198: ifne +38 -> 236
        //     201: aload_0
        //     202: getfield 196	com/android/server/am/UsageStatsService:mFile	Ljava/io/File;
        //     205: aload 11
        //     207: invokevirtual 708	java/io/File:renameTo	(Ljava/io/File;)Z
        //     210: ifne +34 -> 244
        //     213: ldc 41
        //     215: ldc_w 710
        //     218: invokestatic 307	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     221: pop
        //     222: aload 9
        //     224: monitorexit
        //     225: goto -149 -> 76
        //     228: astore 10
        //     230: aload 9
        //     232: monitorexit
        //     233: aload 10
        //     235: athrow
        //     236: aload_0
        //     237: getfield 196	com/android/server/am/UsageStatsService:mFile	Ljava/io/File;
        //     240: invokevirtual 188	java/io/File:delete	()Z
        //     243: pop
        //     244: aload_0
        //     245: aload_0
        //     246: getfield 196	com/android/server/am/UsageStatsService:mFile	Ljava/io/File;
        //     249: invokespecial 712	com/android/server/am/UsageStatsService:writeStatsFLOCK	(Ljava/io/File;)V
        //     252: aload_0
        //     253: getfield 97	com/android/server/am/UsageStatsService:mLastWriteElapsedTime	Ljava/util/concurrent/atomic/AtomicLong;
        //     256: lload 7
        //     258: invokevirtual 218	java/util/concurrent/atomic/AtomicLong:set	(J)V
        //     261: iload 6
        //     263: ifeq +149 -> 412
        //     266: aload_0
        //     267: getfield 90	com/android/server/am/UsageStatsService:mLastWriteDay	Ljava/util/concurrent/atomic/AtomicInteger;
        //     270: iload 5
        //     272: invokevirtual 224	java/util/concurrent/atomic/AtomicInteger:set	(I)V
        //     275: aload_0
        //     276: getfield 109	com/android/server/am/UsageStatsService:mStats	Ljava/util/Map;
        //     279: astore 17
        //     281: aload 17
        //     283: monitorenter
        //     284: aload_0
        //     285: getfield 109	com/android/server/am/UsageStatsService:mStats	Ljava/util/Map;
        //     288: invokeinterface 420 1 0
        //     293: aload 17
        //     295: monitorexit
        //     296: aload_0
        //     297: new 120	java/io/File
        //     300: dup
        //     301: aload_0
        //     302: getfield 124	com/android/server/am/UsageStatsService:mDir	Ljava/io/File;
        //     305: aload_0
        //     306: getfield 194	com/android/server/am/UsageStatsService:mFileLeaf	Ljava/lang/String;
        //     309: invokespecial 185	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     312: putfield 196	com/android/server/am/UsageStatsService:mFile	Ljava/io/File;
        //     315: aload_0
        //     316: invokespecial 588	com/android/server/am/UsageStatsService:checkFileLimitFLOCK	()V
        //     319: goto +93 -> 412
        //     322: aload_0
        //     323: aload_0
        //     324: getfield 203	com/android/server/am/UsageStatsService:mHistoryFile	Lcom/android/internal/os/AtomicFile;
        //     327: invokespecial 714	com/android/server/am/UsageStatsService:writeHistoryStatsFLOCK	(Lcom/android/internal/os/AtomicFile;)V
        //     330: aload 11
        //     332: ifnull +9 -> 341
        //     335: aload 11
        //     337: invokevirtual 188	java/io/File:delete	()Z
        //     340: pop
        //     341: aload 9
        //     343: monitorexit
        //     344: goto -268 -> 76
        //     347: astore 18
        //     349: aload 17
        //     351: monitorexit
        //     352: aload 18
        //     354: athrow
        //     355: astore 12
        //     357: ldc 41
        //     359: new 154	java/lang/StringBuilder
        //     362: dup
        //     363: invokespecial 155	java/lang/StringBuilder:<init>	()V
        //     366: ldc_w 716
        //     369: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     372: aload_0
        //     373: getfield 196	com/android/server/am/UsageStatsService:mFile	Ljava/io/File;
        //     376: invokevirtual 302	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     379: invokevirtual 168	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     382: invokestatic 307	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     385: pop
        //     386: aload 11
        //     388: ifnull -47 -> 341
        //     391: aload_0
        //     392: getfield 196	com/android/server/am/UsageStatsService:mFile	Ljava/io/File;
        //     395: invokevirtual 188	java/io/File:delete	()Z
        //     398: pop
        //     399: aload 11
        //     401: aload_0
        //     402: getfield 196	com/android/server/am/UsageStatsService:mFile	Ljava/io/File;
        //     405: invokevirtual 708	java/io/File:renameTo	(Ljava/io/File;)Z
        //     408: pop
        //     409: goto -68 -> 341
        //     412: iload 6
        //     414: ifne -92 -> 322
        //     417: iload_2
        //     418: ifeq -88 -> 330
        //     421: goto -99 -> 322
        //
        // Exception table:
        //     from	to	target	type
        //     7	30	77	finally
        //     79	81	77	finally
        //     128	233	228	finally
        //     236	244	228	finally
        //     244	284	228	finally
        //     296	341	228	finally
        //     341	344	228	finally
        //     352	355	228	finally
        //     357	409	228	finally
        //     284	296	347	finally
        //     349	352	347	finally
        //     244	284	355	java/io/IOException
        //     296	341	355	java/io/IOException
        //     352	355	355	java/io/IOException
    }

    // ERROR //
    private void writeStatsToParcelFLOCK(Parcel paramParcel)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 116	com/android/server/am/UsageStatsService:mStatsLock	Ljava/lang/Object;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_1
        //     8: sipush 1007
        //     11: invokevirtual 719	android/os/Parcel:writeInt	(I)V
        //     14: aload_0
        //     15: getfield 109	com/android/server/am/UsageStatsService:mStats	Ljava/util/Map;
        //     18: invokeinterface 722 1 0
        //     23: astore 4
        //     25: aload_1
        //     26: aload 4
        //     28: invokeinterface 723 1 0
        //     33: invokevirtual 719	android/os/Parcel:writeInt	(I)V
        //     36: aload 4
        //     38: invokeinterface 372 1 0
        //     43: astore 5
        //     45: aload 5
        //     47: invokeinterface 275 1 0
        //     52: ifeq +51 -> 103
        //     55: aload 5
        //     57: invokeinterface 279 1 0
        //     62: checkcast 170	java/lang/String
        //     65: astore 6
        //     67: aload_0
        //     68: getfield 109	com/android/server/am/UsageStatsService:mStats	Ljava/util/Map;
        //     71: aload 6
        //     73: invokeinterface 445 2 0
        //     78: checkcast 10	com/android/server/am/UsageStatsService$PkgUsageStatsExtended
        //     81: astore 7
        //     83: aload_1
        //     84: aload 6
        //     86: invokevirtual 726	android/os/Parcel:writeString	(Ljava/lang/String;)V
        //     89: aload 7
        //     91: aload_1
        //     92: invokevirtual 729	com/android/server/am/UsageStatsService$PkgUsageStatsExtended:writeToParcel	(Landroid/os/Parcel;)V
        //     95: goto -50 -> 45
        //     98: astore_3
        //     99: aload_2
        //     100: monitorexit
        //     101: aload_3
        //     102: athrow
        //     103: aload_2
        //     104: monitorexit
        //     105: return
        //
        // Exception table:
        //     from	to	target	type
        //     7	101	98	finally
        //     103	105	98	finally
    }

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        if (this.mContext.checkCallingPermission("android.permission.DUMP") != 0)
            paramPrintWriter.println("Permission Denial: can't dump UsageStats from from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid() + " without permission " + "android.permission.DUMP");
        while (true)
        {
            return;
            boolean bool1 = scanArgs(paramArrayOfString, "--checkin");
            boolean bool2;
            if ((bool1) || (scanArgs(paramArrayOfString, "-c")))
            {
                bool2 = true;
                if ((!bool1) && (!scanArgs(paramArrayOfString, "-d")))
                    break label214;
            }
            HashSet localHashSet;
            label214: for (boolean bool3 = true; ; bool3 = false)
            {
                String str1 = scanArgsData(paramArrayOfString, "--packages");
                if (!bool3)
                    writeStatsToFile(true, false);
                localHashSet = null;
                if (str1 == null)
                    break label220;
                if ("*".equals(str1))
                    break label237;
                for (String str2 : str1.split(","))
                {
                    if (localHashSet == null)
                        localHashSet = new HashSet();
                    localHashSet.add(str2);
                }
                bool2 = false;
                break;
            }
            label220: if (bool1)
            {
                Slog.w("UsageStats", "Checkin without packages");
                continue;
            }
            label237: synchronized (this.mFileLock)
            {
                collectDumpInfoFLOCK(paramPrintWriter, bool2, bool3, localHashSet);
            }
        }
    }

    public void enforceCallingPermission()
    {
        if (Binder.getCallingPid() == Process.myPid());
        while (true)
        {
            return;
            this.mContext.enforcePermission("android.permission.UPDATE_DEVICE_STATS", Binder.getCallingPid(), Binder.getCallingUid(), null);
        }
    }

    public PkgUsageStats[] getAllPkgUsageStats()
    {
        PkgUsageStats[] arrayOfPkgUsageStats = null;
        this.mContext.enforceCallingOrSelfPermission("android.permission.PACKAGE_USAGE_STATS", null);
        synchronized (this.mStatsLock)
        {
            int i = this.mLastResumeTimes.size();
            if (i > 0)
            {
                arrayOfPkgUsageStats = new PkgUsageStats[i];
                int j = 0;
                Iterator localIterator = this.mLastResumeTimes.entrySet().iterator();
                while (localIterator.hasNext())
                {
                    Map.Entry localEntry = (Map.Entry)localIterator.next();
                    String str = (String)localEntry.getKey();
                    long l = 0L;
                    int k = 0;
                    PkgUsageStatsExtended localPkgUsageStatsExtended = (PkgUsageStatsExtended)this.mStats.get(str);
                    if (localPkgUsageStatsExtended != null)
                    {
                        l = localPkgUsageStatsExtended.mUsageTime;
                        k = localPkgUsageStatsExtended.mLaunchCount;
                    }
                    arrayOfPkgUsageStats[j] = new PkgUsageStats(str, k, l, (Map)localEntry.getValue());
                    j++;
                }
            }
        }
        return arrayOfPkgUsageStats;
    }

    // ERROR //
    public PkgUsageStats getPkgUsageStats(ComponentName paramComponentName)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: aload_0
        //     3: getfield 422	com/android/server/am/UsageStatsService:mContext	Landroid/content/Context;
        //     6: ldc_w 791
        //     9: aconst_null
        //     10: invokevirtual 795	android/content/Context:enforceCallingOrSelfPermission	(Ljava/lang/String;Ljava/lang/String;)V
        //     13: aload_1
        //     14: ifnull +12 -> 26
        //     17: aload_1
        //     18: invokevirtual 808	android/content/ComponentName:getPackageName	()Ljava/lang/String;
        //     21: astore_3
        //     22: aload_3
        //     23: ifnonnull +5 -> 28
        //     26: aload_2
        //     27: areturn
        //     28: aload_0
        //     29: getfield 116	com/android/server/am/UsageStatsService:mStatsLock	Ljava/lang/Object;
        //     32: astore 4
        //     34: aload 4
        //     36: monitorenter
        //     37: aload_0
        //     38: getfield 109	com/android/server/am/UsageStatsService:mStats	Ljava/util/Map;
        //     41: aload_3
        //     42: invokeinterface 445 2 0
        //     47: checkcast 10	com/android/server/am/UsageStatsService$PkgUsageStatsExtended
        //     50: astore 6
        //     52: aload_0
        //     53: getfield 111	com/android/server/am/UsageStatsService:mLastResumeTimes	Ljava/util/Map;
        //     56: aload_3
        //     57: invokeinterface 445 2 0
        //     62: checkcast 417	java/util/Map
        //     65: astore 7
        //     67: aload 6
        //     69: ifnonnull +22 -> 91
        //     72: aload 7
        //     74: ifnonnull +17 -> 91
        //     77: aload 4
        //     79: monitorexit
        //     80: goto -54 -> 26
        //     83: astore 5
        //     85: aload 4
        //     87: monitorexit
        //     88: aload 5
        //     90: athrow
        //     91: aload 6
        //     93: ifnull +43 -> 136
        //     96: aload 6
        //     98: getfield 353	com/android/server/am/UsageStatsService$PkgUsageStatsExtended:mLaunchCount	I
        //     101: istore 8
        //     103: aload 6
        //     105: ifnull +37 -> 142
        //     108: aload 6
        //     110: getfield 357	com/android/server/am/UsageStatsService$PkgUsageStatsExtended:mUsageTime	J
        //     113: lstore 9
        //     115: new 798	com/android/internal/os/PkgUsageStats
        //     118: dup
        //     119: aload_3
        //     120: iload 8
        //     122: lload 9
        //     124: aload 7
        //     126: invokespecial 801	com/android/internal/os/PkgUsageStats:<init>	(Ljava/lang/String;IJLjava/util/Map;)V
        //     129: astore_2
        //     130: aload 4
        //     132: monitorexit
        //     133: goto -107 -> 26
        //     136: iconst_0
        //     137: istore 8
        //     139: goto -36 -> 103
        //     142: lconst_0
        //     143: lstore 9
        //     145: goto -30 -> 115
        //
        // Exception table:
        //     from	to	target	type
        //     37	88	83	finally
        //     96	133	83	finally
    }

    public void monitorPackages()
    {
        this.mPackageMonitor = new PackageMonitor()
        {
            public void onPackageRemoved(String paramAnonymousString, int paramAnonymousInt)
            {
                synchronized (UsageStatsService.this.mStatsLock)
                {
                    UsageStatsService.this.mLastResumeTimes.remove(paramAnonymousString);
                    return;
                }
            }
        };
        this.mPackageMonitor.register(this.mContext, null, true);
        filterHistoryStats();
    }

    public void noteLaunchTime(ComponentName paramComponentName, int paramInt)
    {
        enforceCallingPermission();
        String str;
        if (paramComponentName != null)
        {
            str = paramComponentName.getPackageName();
            if (str != null)
                break label18;
        }
        while (true)
        {
            return;
            label18: writeStatsToFile(false, false);
            synchronized (this.mStatsLock)
            {
                PkgUsageStatsExtended localPkgUsageStatsExtended = (PkgUsageStatsExtended)this.mStats.get(str);
                if (localPkgUsageStatsExtended != null)
                    localPkgUsageStatsExtended.addLaunchTime(paramComponentName.getClassName(), paramInt);
            }
        }
    }

    // ERROR //
    public void notePauseComponent(ComponentName paramComponentName)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokevirtual 826	com/android/server/am/UsageStatsService:enforceCallingPermission	()V
        //     4: aload_0
        //     5: getfield 116	com/android/server/am/UsageStatsService:mStatsLock	Ljava/lang/Object;
        //     8: astore_2
        //     9: aload_2
        //     10: monitorenter
        //     11: aload_1
        //     12: ifnull +14 -> 26
        //     15: aload_1
        //     16: invokevirtual 808	android/content/ComponentName:getPackageName	()Ljava/lang/String;
        //     19: astore 4
        //     21: aload 4
        //     23: ifnonnull +8 -> 31
        //     26: aload_2
        //     27: monitorexit
        //     28: goto +91 -> 119
        //     31: aload_0
        //     32: getfield 837	com/android/server/am/UsageStatsService:mIsResumed	Z
        //     35: ifne +13 -> 48
        //     38: aload_2
        //     39: monitorexit
        //     40: goto +79 -> 119
        //     43: astore_3
        //     44: aload_2
        //     45: monitorexit
        //     46: aload_3
        //     47: athrow
        //     48: aload_0
        //     49: iconst_0
        //     50: putfield 837	com/android/server/am/UsageStatsService:mIsResumed	Z
        //     53: aload_0
        //     54: getfield 109	com/android/server/am/UsageStatsService:mStats	Ljava/util/Map;
        //     57: aload 4
        //     59: invokeinterface 445 2 0
        //     64: checkcast 10	com/android/server/am/UsageStatsService$PkgUsageStatsExtended
        //     67: astore 5
        //     69: aload 5
        //     71: ifnonnull +35 -> 106
        //     74: ldc 41
        //     76: new 154	java/lang/StringBuilder
        //     79: dup
        //     80: invokespecial 155	java/lang/StringBuilder:<init>	()V
        //     83: ldc_w 839
        //     86: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     89: aload 4
        //     91: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     94: invokevirtual 168	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     97: invokestatic 182	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     100: pop
        //     101: aload_2
        //     102: monitorexit
        //     103: goto +16 -> 119
        //     106: aload 5
        //     108: invokevirtual 842	com/android/server/am/UsageStatsService$PkgUsageStatsExtended:updatePause	()V
        //     111: aload_2
        //     112: monitorexit
        //     113: aload_0
        //     114: iconst_0
        //     115: iconst_0
        //     116: invokespecial 232	com/android/server/am/UsageStatsService:writeStatsToFile	(ZZ)V
        //     119: return
        //
        // Exception table:
        //     from	to	target	type
        //     15	46	43	finally
        //     48	113	43	finally
    }

    public void noteResumeComponent(ComponentName paramComponentName)
    {
        boolean bool1 = true;
        enforceCallingPermission();
        Object localObject1 = this.mStatsLock;
        if (paramComponentName != null);
        while (true)
        {
            try
            {
                String str1 = paramComponentName.getPackageName();
                if (str1 == null)
                    break;
                boolean bool2 = str1.equals(this.mLastResumedPkg);
                if ((this.mIsResumed) && (this.mLastResumedPkg != null))
                {
                    PkgUsageStatsExtended localPkgUsageStatsExtended2 = (PkgUsageStatsExtended)this.mStats.get(this.mLastResumedPkg);
                    if (localPkgUsageStatsExtended2 != null)
                        localPkgUsageStatsExtended2.updatePause();
                }
                if ((bool2) && (paramComponentName.getClassName().equals(this.mLastResumedComp)))
                {
                    bool3 = bool1;
                    this.mIsResumed = true;
                    this.mLastResumedPkg = str1;
                    this.mLastResumedComp = paramComponentName.getClassName();
                    PkgUsageStatsExtended localPkgUsageStatsExtended1 = (PkgUsageStatsExtended)this.mStats.get(str1);
                    if (localPkgUsageStatsExtended1 == null)
                    {
                        localPkgUsageStatsExtended1 = new PkgUsageStatsExtended();
                        this.mStats.put(str1, localPkgUsageStatsExtended1);
                    }
                    String str2 = this.mLastResumedComp;
                    if (bool2)
                        break label285;
                    localPkgUsageStatsExtended1.updateResume(str2, bool1);
                    if (!bool3)
                        localPkgUsageStatsExtended1.addLaunchCount(this.mLastResumedComp);
                    Object localObject3 = (Map)this.mLastResumeTimes.get(str1);
                    if (localObject3 == null)
                    {
                        localObject3 = new HashMap();
                        this.mLastResumeTimes.put(str1, localObject3);
                    }
                    ((Map)localObject3).put(this.mLastResumedComp, Long.valueOf(System.currentTimeMillis()));
                    break;
                }
            }
            finally
            {
                localObject2 = finally;
                throw localObject2;
            }
            boolean bool3 = false;
            continue;
            label285: bool1 = false;
        }
    }

    public void publish(Context paramContext)
    {
        this.mContext = paramContext;
        ServiceManager.addService("usagestats", asBinder());
    }

    public void shutdown()
    {
        if (this.mPackageMonitor != null)
            this.mPackageMonitor.unregister();
        Slog.i("UsageStats", "Writing usage stats before shutdown...");
        writeStatsToFile(true, true);
    }

    private class PkgUsageStatsExtended
    {
        int mLaunchCount;
        final HashMap<String, UsageStatsService.TimeStats> mLaunchTimes = new HashMap();
        long mPausedTime;
        long mResumedTime;
        long mUsageTime;

        PkgUsageStatsExtended()
        {
            this.mLaunchCount = 0;
            this.mUsageTime = 0L;
        }

        PkgUsageStatsExtended(Parcel arg2)
        {
            Parcel localParcel;
            this.mLaunchCount = localParcel.readInt();
            this.mUsageTime = localParcel.readLong();
            int i = localParcel.readInt();
            for (int j = 0; j < i; j++)
            {
                String str = localParcel.readString();
                UsageStatsService.TimeStats localTimeStats = new UsageStatsService.TimeStats(localParcel);
                this.mLaunchTimes.put(str, localTimeStats);
            }
        }

        void addLaunchCount(String paramString)
        {
            UsageStatsService.TimeStats localTimeStats = (UsageStatsService.TimeStats)this.mLaunchTimes.get(paramString);
            if (localTimeStats == null)
            {
                localTimeStats = new UsageStatsService.TimeStats();
                this.mLaunchTimes.put(paramString, localTimeStats);
            }
            localTimeStats.incCount();
        }

        void addLaunchTime(String paramString, int paramInt)
        {
            UsageStatsService.TimeStats localTimeStats = (UsageStatsService.TimeStats)this.mLaunchTimes.get(paramString);
            if (localTimeStats == null)
            {
                localTimeStats = new UsageStatsService.TimeStats();
                this.mLaunchTimes.put(paramString, localTimeStats);
            }
            localTimeStats.add(paramInt);
        }

        void clear()
        {
            this.mLaunchTimes.clear();
            this.mLaunchCount = 0;
            this.mUsageTime = 0L;
        }

        void updatePause()
        {
            this.mPausedTime = SystemClock.elapsedRealtime();
            this.mUsageTime += this.mPausedTime - this.mResumedTime;
        }

        void updateResume(String paramString, boolean paramBoolean)
        {
            if (paramBoolean)
                this.mLaunchCount = (1 + this.mLaunchCount);
            this.mResumedTime = SystemClock.elapsedRealtime();
        }

        void writeToParcel(Parcel paramParcel)
        {
            paramParcel.writeInt(this.mLaunchCount);
            paramParcel.writeLong(this.mUsageTime);
            int i = this.mLaunchTimes.size();
            paramParcel.writeInt(i);
            if (i > 0)
            {
                Iterator localIterator = this.mLaunchTimes.entrySet().iterator();
                while (localIterator.hasNext())
                {
                    Map.Entry localEntry = (Map.Entry)localIterator.next();
                    paramParcel.writeString((String)localEntry.getKey());
                    ((UsageStatsService.TimeStats)localEntry.getValue()).writeToParcel(paramParcel);
                }
            }
        }
    }

    static class TimeStats
    {
        int count;
        int[] times = new int[10];

        TimeStats()
        {
        }

        TimeStats(Parcel paramParcel)
        {
            this.count = paramParcel.readInt();
            int[] arrayOfInt = this.times;
            for (int i = 0; i < 10; i++)
                arrayOfInt[i] = paramParcel.readInt();
        }

        void add(int paramInt)
        {
            int[] arrayOfInt1 = UsageStatsService.LAUNCH_TIME_BINS;
            int i = 0;
            if (i < 9)
                if (paramInt < arrayOfInt1[i])
                {
                    int[] arrayOfInt3 = this.times;
                    arrayOfInt3[i] = (1 + arrayOfInt3[i]);
                }
            while (true)
            {
                return;
                i++;
                break;
                int[] arrayOfInt2 = this.times;
                arrayOfInt2[9] = (1 + arrayOfInt2[9]);
            }
        }

        void incCount()
        {
            this.count = (1 + this.count);
        }

        void writeToParcel(Parcel paramParcel)
        {
            paramParcel.writeInt(this.count);
            int[] arrayOfInt = this.times;
            for (int i = 0; i < 10; i++)
                paramParcel.writeInt(arrayOfInt[i]);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.UsageStatsService
 * JD-Core Version:        0.6.2
 */