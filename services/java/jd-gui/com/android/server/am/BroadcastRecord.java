package com.android.server.am;

import android.content.ComponentName;
import android.content.IIntentReceiver;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ResolveInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.PrintWriterPrinter;
import android.util.TimeUtils;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

class BroadcastRecord extends Binder
{
    static final int APP_RECEIVE = 1;
    static final int CALL_DONE_RECEIVE = 3;
    static final int CALL_IN_RECEIVE = 2;
    static final int IDLE;
    int anrCount;
    final ProcessRecord callerApp;
    final String callerPackage;
    final int callingPid;
    final int callingUid;
    ProcessRecord curApp;
    ComponentName curComponent;
    BroadcastFilter curFilter;
    ActivityInfo curReceiver;
    long dispatchClockTime;
    long dispatchTime;
    long finishTime;
    final boolean initialSticky;
    final Intent intent;
    int nextReceiver;
    final boolean ordered;
    BroadcastQueue queue;
    IBinder receiver;
    long receiverTime;
    final List receivers;
    final String requiredPermission;
    boolean resultAbort;
    int resultCode;
    String resultData;
    Bundle resultExtras;
    IIntentReceiver resultTo;
    int state;
    final boolean sticky;

    BroadcastRecord(BroadcastQueue paramBroadcastQueue, Intent paramIntent, ProcessRecord paramProcessRecord, String paramString1, int paramInt1, int paramInt2, String paramString2, List paramList, IIntentReceiver paramIIntentReceiver, int paramInt3, String paramString3, Bundle paramBundle, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
    {
        this.queue = paramBroadcastQueue;
        this.intent = paramIntent;
        this.callerApp = paramProcessRecord;
        this.callerPackage = paramString1;
        this.callingPid = paramInt1;
        this.callingUid = paramInt2;
        this.requiredPermission = paramString2;
        this.receivers = paramList;
        this.resultTo = paramIIntentReceiver;
        this.resultCode = paramInt3;
        this.resultData = paramString3;
        this.resultExtras = paramBundle;
        this.ordered = paramBoolean1;
        this.sticky = paramBoolean2;
        this.initialSticky = paramBoolean3;
        this.nextReceiver = 0;
        this.state = 0;
    }

    void dump(PrintWriter paramPrintWriter, String paramString)
    {
        long l = SystemClock.uptimeMillis();
        paramPrintWriter.print(paramString);
        paramPrintWriter.println(this);
        paramPrintWriter.print(paramString);
        paramPrintWriter.println(this.intent);
        if (this.sticky)
        {
            Bundle localBundle = this.intent.getExtras();
            if (localBundle != null)
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("extras: ");
                paramPrintWriter.println(localBundle.toString());
            }
        }
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("caller=");
        paramPrintWriter.print(this.callerPackage);
        paramPrintWriter.print(" ");
        String str1;
        label239: String str4;
        label608: String str2;
        label696: int i;
        label739: String str3;
        PrintWriterPrinter localPrintWriterPrinter;
        int j;
        label774: Object localObject;
        if (this.callerApp != null)
        {
            str1 = this.callerApp.toShortString();
            paramPrintWriter.print(str1);
            paramPrintWriter.print(" pid=");
            paramPrintWriter.print(this.callingPid);
            paramPrintWriter.print(" uid=");
            paramPrintWriter.println(this.callingUid);
            if (this.requiredPermission != null)
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("requiredPermission=");
                paramPrintWriter.println(this.requiredPermission);
            }
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("dispatchClockTime=");
            paramPrintWriter.println(new Date(this.dispatchClockTime));
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("dispatchTime=");
            TimeUtils.formatDuration(this.dispatchTime, l, paramPrintWriter);
            if (this.finishTime == 0L)
                break label858;
            paramPrintWriter.print(" finishTime=");
            TimeUtils.formatDuration(this.finishTime, l, paramPrintWriter);
            paramPrintWriter.println("");
            if (this.anrCount != 0)
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("anrCount=");
                paramPrintWriter.println(this.anrCount);
            }
            if ((this.resultTo != null) || (this.resultCode != -1) || (this.resultData != null))
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("resultTo=");
                paramPrintWriter.print(this.resultTo);
                paramPrintWriter.print(" resultCode=");
                paramPrintWriter.print(this.resultCode);
                paramPrintWriter.print(" resultData=");
                paramPrintWriter.println(this.resultData);
            }
            if (this.resultExtras != null)
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("resultExtras=");
                paramPrintWriter.println(this.resultExtras);
            }
            if ((this.resultAbort) || (this.ordered) || (this.sticky) || (this.initialSticky))
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("resultAbort=");
                paramPrintWriter.print(this.resultAbort);
                paramPrintWriter.print(" ordered=");
                paramPrintWriter.print(this.ordered);
                paramPrintWriter.print(" sticky=");
                paramPrintWriter.print(this.sticky);
                paramPrintWriter.print(" initialSticky=");
                paramPrintWriter.println(this.initialSticky);
            }
            if ((this.nextReceiver != 0) || (this.receiver != null))
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("nextReceiver=");
                paramPrintWriter.print(this.nextReceiver);
                paramPrintWriter.print(" receiver=");
                paramPrintWriter.println(this.receiver);
            }
            if (this.curFilter != null)
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("curFilter=");
                paramPrintWriter.println(this.curFilter);
            }
            if (this.curReceiver != null)
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("curReceiver=");
                paramPrintWriter.println(this.curReceiver);
            }
            if (this.curApp != null)
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("curApp=");
                paramPrintWriter.println(this.curApp);
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("curComponent=");
                if (this.curComponent == null)
                    break label877;
                str4 = this.curComponent.toShortString();
                paramPrintWriter.println(str4);
                if ((this.curReceiver != null) && (this.curReceiver.applicationInfo != null))
                {
                    paramPrintWriter.print(paramString);
                    paramPrintWriter.print("curSourceDir=");
                    paramPrintWriter.println(this.curReceiver.applicationInfo.sourceDir);
                }
            }
            str2 = " (?)";
            switch (this.state)
            {
            default:
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("state=");
                paramPrintWriter.print(this.state);
                paramPrintWriter.println(str2);
                if (this.receivers != null)
                {
                    i = this.receivers.size();
                    str3 = paramString + "    ";
                    localPrintWriterPrinter = new PrintWriterPrinter(paramPrintWriter);
                    j = 0;
                    if (j >= i)
                        return;
                    localObject = this.receivers.get(j);
                    paramPrintWriter.print(paramString);
                    paramPrintWriter.print("Receiver #");
                    paramPrintWriter.print(j);
                    paramPrintWriter.print(": ");
                    paramPrintWriter.println(localObject);
                    if (!(localObject instanceof BroadcastFilter))
                        break label923;
                    ((BroadcastFilter)localObject).dumpBrief(paramPrintWriter, str3);
                }
                break;
            case 0:
            case 1:
            case 2:
            case 3:
            }
        }
        while (true)
        {
            j++;
            break label774;
            str1 = "null";
            break;
            label858: paramPrintWriter.print(" receiverTime=");
            TimeUtils.formatDuration(this.receiverTime, l, paramPrintWriter);
            break label239;
            label877: str4 = "--";
            break label608;
            str2 = " (IDLE)";
            break label696;
            str2 = " (APP_RECEIVE)";
            break label696;
            str2 = " (CALL_IN_RECEIVE)";
            break label696;
            str2 = " (CALL_DONE_RECEIVE)";
            break label696;
            i = 0;
            break label739;
            label923: if ((localObject instanceof ResolveInfo))
                ((ResolveInfo)localObject).dump(localPrintWriterPrinter, str3);
        }
    }

    public String toString()
    {
        return "BroadcastRecord{" + Integer.toHexString(System.identityHashCode(this)) + " " + this.intent.getAction() + "}";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.BroadcastRecord
 * JD-Core Version:        0.6.2
 */