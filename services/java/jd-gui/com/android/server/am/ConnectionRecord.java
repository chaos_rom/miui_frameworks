package com.android.server.am;

import android.app.IServiceConnection;
import android.app.PendingIntent;
import java.io.PrintWriter;

class ConnectionRecord
{
    final ActivityRecord activity;
    final AppBindRecord binding;
    final PendingIntent clientIntent;
    final int clientLabel;
    final IServiceConnection conn;
    final int flags;
    boolean serviceDead;
    String stringName;

    ConnectionRecord(AppBindRecord paramAppBindRecord, ActivityRecord paramActivityRecord, IServiceConnection paramIServiceConnection, int paramInt1, int paramInt2, PendingIntent paramPendingIntent)
    {
        this.binding = paramAppBindRecord;
        this.activity = paramActivityRecord;
        this.conn = paramIServiceConnection;
        this.flags = paramInt1;
        this.clientLabel = paramInt2;
        this.clientIntent = paramPendingIntent;
    }

    void dump(PrintWriter paramPrintWriter, String paramString)
    {
        paramPrintWriter.println(paramString + "binding=" + this.binding);
        if (this.activity != null)
            paramPrintWriter.println(paramString + "activity=" + this.activity);
        paramPrintWriter.println(paramString + "conn=" + this.conn.asBinder() + " flags=0x" + Integer.toHexString(this.flags));
    }

    public String toString()
    {
        String str;
        if (this.stringName != null)
            str = this.stringName;
        while (true)
        {
            return str;
            StringBuilder localStringBuilder = new StringBuilder(128);
            localStringBuilder.append("ConnectionRecord{");
            localStringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
            localStringBuilder.append(' ');
            if (this.serviceDead)
                localStringBuilder.append("DEAD ");
            localStringBuilder.append(this.binding.service.shortName);
            localStringBuilder.append(":@");
            localStringBuilder.append(Integer.toHexString(System.identityHashCode(this.conn.asBinder())));
            localStringBuilder.append('}');
            str = localStringBuilder.toString();
            this.stringName = str;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.ConnectionRecord
 * JD-Core Version:        0.6.2
 */