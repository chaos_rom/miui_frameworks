package com.android.server.am;

import android.content.IIntentReceiver;
import android.os.Binder;
import android.os.IBinder.DeathRecipient;
import android.util.PrintWriterPrinter;
import java.io.PrintWriter;
import java.util.ArrayList;

class ReceiverList extends ArrayList<BroadcastFilter>
    implements IBinder.DeathRecipient
{
    public final ProcessRecord app;
    BroadcastRecord curBroadcast = null;
    boolean linkedToDeath = false;
    final ActivityManagerService owner;
    public final int pid;
    public final IIntentReceiver receiver;
    String stringName;
    public final int uid;

    ReceiverList(ActivityManagerService paramActivityManagerService, ProcessRecord paramProcessRecord, int paramInt1, int paramInt2, IIntentReceiver paramIIntentReceiver)
    {
        this.owner = paramActivityManagerService;
        this.receiver = paramIIntentReceiver;
        this.app = paramProcessRecord;
        this.pid = paramInt1;
        this.uid = paramInt2;
    }

    public void binderDied()
    {
        this.linkedToDeath = false;
        this.owner.unregisterReceiver(this.receiver);
    }

    void dump(PrintWriter paramPrintWriter, String paramString)
    {
        PrintWriterPrinter localPrintWriterPrinter = new PrintWriterPrinter(paramPrintWriter);
        dumpLocal(paramPrintWriter, paramString);
        String str = paramString + "    ";
        int i = size();
        for (int j = 0; j < i; j++)
        {
            BroadcastFilter localBroadcastFilter = (BroadcastFilter)get(j);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("Filter #");
            paramPrintWriter.print(j);
            paramPrintWriter.print(": BroadcastFilter{");
            paramPrintWriter.print(Integer.toHexString(System.identityHashCode(localBroadcastFilter)));
            paramPrintWriter.println('}');
            localBroadcastFilter.dumpInReceiverList(paramPrintWriter, localPrintWriterPrinter, str);
        }
    }

    void dumpLocal(PrintWriter paramPrintWriter, String paramString)
    {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("app=");
        paramPrintWriter.print(this.app);
        paramPrintWriter.print(" pid=");
        paramPrintWriter.print(this.pid);
        paramPrintWriter.print(" uid=");
        paramPrintWriter.println(this.uid);
        if ((this.curBroadcast != null) || (this.linkedToDeath))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("curBroadcast=");
            paramPrintWriter.print(this.curBroadcast);
            paramPrintWriter.print(" linkedToDeath=");
            paramPrintWriter.println(this.linkedToDeath);
        }
    }

    public boolean equals(Object paramObject)
    {
        if (this == paramObject);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public int hashCode()
    {
        return System.identityHashCode(this);
    }

    public String toString()
    {
        String str3;
        if (this.stringName != null)
        {
            str3 = this.stringName;
            return str3;
        }
        StringBuilder localStringBuilder = new StringBuilder(128);
        localStringBuilder.append("ReceiverList{");
        localStringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
        localStringBuilder.append(' ');
        localStringBuilder.append(this.pid);
        localStringBuilder.append(' ');
        String str1;
        if (this.app != null)
        {
            str1 = this.app.processName;
            label85: localStringBuilder.append(str1);
            localStringBuilder.append('/');
            localStringBuilder.append(this.uid);
            if (!(this.receiver.asBinder() instanceof Binder))
                break label183;
        }
        label183: for (String str2 = " local:"; ; str2 = " remote:")
        {
            localStringBuilder.append(str2);
            localStringBuilder.append(Integer.toHexString(System.identityHashCode(this.receiver.asBinder())));
            localStringBuilder.append('}');
            str3 = localStringBuilder.toString();
            this.stringName = str3;
            break;
            str1 = "(unknown name)";
            break label85;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.ReceiverList
 * JD-Core Version:        0.6.2
 */