package com.android.server.am;

import android.app.ActivityOptions;
import android.app.IApplicationThread;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import android.content.res.CompatibilityInfo;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Process;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.UserId;
import android.util.EventLog;
import android.util.Log;
import android.util.Slog;
import android.util.TimeUtils;
import android.view.IApplicationToken.Stub;
import com.android.internal.R.styleable;
import com.android.internal.app.ResolverActivity;
import com.android.server.AttributeCache;
import com.android.server.AttributeCache.Entry;
import com.android.server.wm.WindowManagerService;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

final class ActivityRecord
{
    ProcessRecord app;
    final IApplicationToken.Stub appToken;
    final String baseDir;
    CompatibilityInfo compat;
    final boolean componentSpecified;
    int configChangeFlags;
    boolean configDestroy;
    Configuration configuration;
    HashSet<ConnectionRecord> connections;
    long cpuTimeAtResume;
    final String dataDir;
    boolean delayedResume;
    boolean finishing;
    boolean forceNewConfig;
    boolean frontOfTask;
    boolean frozenBeforeDestroy;
    final boolean fullscreen;
    boolean hasBeenLaunched;
    boolean haveState;
    Bundle icicle;
    int icon;
    boolean idle;
    boolean immersive;
    private boolean inHistory;
    final ActivityInfo info;
    final Intent intent;
    final boolean isHomeActivity;
    boolean keysPaused;
    int labelRes;
    long lastVisibleTime;
    boolean launchFailed;
    int launchMode;
    long launchTickTime;
    long launchTime;
    final int launchedFromUid;
    ArrayList newIntents;
    final boolean noDisplay;
    CharSequence nonLocalizedLabel;
    boolean nowVisible;
    final String packageName;
    long pauseTime;
    ActivityOptions pendingOptions;
    HashSet<WeakReference<PendingIntentRecord>> pendingResults;
    final String processName;
    final ComponentName realActivity;
    int realTheme;
    final int requestCode;
    final String resDir;
    final String resolvedType;
    ActivityRecord resultTo;
    final String resultWho;
    ArrayList results;
    final ActivityManagerService service;
    final String shortComponentName;
    boolean sleeping;
    final ActivityStack stack;
    long startTime;
    ActivityStack.ActivityState state;
    final boolean stateNotNeeded;
    boolean stopped;
    String stringName;
    TaskRecord task;
    final String taskAffinity;
    int theme;
    ThumbnailHolder thumbHolder;
    boolean thumbnailNeeded;
    UriPermissionOwner uriPermissions;
    final int userId;
    boolean visible;
    boolean waitingVisible;
    int windowFlags;

    ActivityRecord(ActivityManagerService paramActivityManagerService, ActivityStack paramActivityStack, ProcessRecord paramProcessRecord, int paramInt1, Intent paramIntent, String paramString1, ActivityInfo paramActivityInfo, Configuration paramConfiguration, ActivityRecord paramActivityRecord, String paramString2, int paramInt2, boolean paramBoolean)
    {
        this.service = paramActivityManagerService;
        this.stack = paramActivityStack;
        this.appToken = new Token(this);
        this.info = paramActivityInfo;
        this.launchedFromUid = paramInt1;
        this.userId = UserId.getUserId(paramActivityInfo.applicationInfo.uid);
        this.intent = paramIntent;
        this.shortComponentName = paramIntent.getComponent().flattenToShortString();
        this.resolvedType = paramString1;
        this.componentSpecified = paramBoolean;
        this.configuration = paramConfiguration;
        this.resultTo = paramActivityRecord;
        this.resultWho = paramString2;
        this.requestCode = paramInt2;
        this.state = ActivityStack.ActivityState.INITIALIZING;
        this.frontOfTask = false;
        this.launchFailed = false;
        this.haveState = false;
        this.stopped = false;
        this.delayedResume = false;
        this.finishing = false;
        this.configDestroy = false;
        this.keysPaused = false;
        this.inHistory = false;
        this.visible = true;
        this.waitingVisible = false;
        this.nowVisible = false;
        this.thumbnailNeeded = false;
        this.idle = false;
        this.hasBeenLaunched = false;
        boolean bool1;
        label251: int i;
        label401: label485: boolean bool2;
        label588: boolean bool3;
        label616: boolean bool4;
        if (paramActivityInfo != null)
            if ((paramActivityInfo.targetActivity == null) || (paramActivityInfo.launchMode == 0) || (paramActivityInfo.launchMode == 1))
            {
                this.realActivity = paramIntent.getComponent();
                this.taskAffinity = paramActivityInfo.taskAffinity;
                if ((0x10 & paramActivityInfo.flags) == 0)
                    break label779;
                bool1 = true;
                this.stateNotNeeded = bool1;
                this.baseDir = paramActivityInfo.applicationInfo.sourceDir;
                this.resDir = paramActivityInfo.applicationInfo.publicSourceDir;
                this.dataDir = paramActivityInfo.applicationInfo.dataDir;
                this.nonLocalizedLabel = paramActivityInfo.nonLocalizedLabel;
                this.labelRes = paramActivityInfo.labelRes;
                if ((this.nonLocalizedLabel == null) && (this.labelRes == 0))
                {
                    ApplicationInfo localApplicationInfo = paramActivityInfo.applicationInfo;
                    this.nonLocalizedLabel = localApplicationInfo.nonLocalizedLabel;
                    this.labelRes = localApplicationInfo.labelRes;
                }
                this.icon = paramActivityInfo.getIconResource();
                this.theme = paramActivityInfo.getThemeResource();
                this.realTheme = this.theme;
                if (this.realTheme == 0)
                {
                    if (paramActivityInfo.applicationInfo.targetSdkVersion >= 11)
                        break label785;
                    i = 16973829;
                    this.realTheme = i;
                }
                if ((0x200 & paramActivityInfo.flags) != 0)
                    this.windowFlags = (0x1000000 | this.windowFlags);
                if (((0x1 & paramActivityInfo.flags) == 0) || (paramProcessRecord == null) || ((paramActivityInfo.applicationInfo.uid != 1000) && (paramActivityInfo.applicationInfo.uid != paramProcessRecord.info.uid)))
                    break label793;
                this.processName = paramProcessRecord.processName;
                if ((this.intent != null) && ((0x20 & paramActivityInfo.flags) != 0))
                    this.intent.addFlags(8388608);
                this.packageName = paramActivityInfo.applicationInfo.packageName;
                this.launchMode = paramActivityInfo.launchMode;
                AttributeCache.Entry localEntry = AttributeCache.instance().get(this.packageName, this.realTheme, R.styleable.Window);
                if ((localEntry == null) || (localEntry.array.getBoolean(4, false)) || (localEntry.array.getBoolean(5, false)))
                    break label805;
                bool2 = true;
                this.fullscreen = bool2;
                if ((localEntry == null) || (!localEntry.array.getBoolean(10, false)))
                    break label811;
                bool3 = true;
                this.noDisplay = bool3;
                if ((paramBoolean) && (paramInt1 != Process.myUid()) && (paramInt1 != 0))
                    break label825;
                if ((!"android.intent.action.MAIN".equals(paramIntent.getAction())) || (!paramIntent.hasCategory("android.intent.category.HOME")) || (paramIntent.getCategories().size() != 1) || (paramIntent.getData() != null) || (paramIntent.getType() != null) || ((0x10000000 & this.intent.getFlags()) == 0) || (ResolverActivity.class.getName().equals(this.realActivity.getClassName())))
                    break label817;
                this.isHomeActivity = true;
                label733: if ((0x400 & paramActivityInfo.flags) == 0)
                    break label833;
                bool4 = true;
            }
        label748: for (this.immersive = bool4; ; this.immersive = false)
        {
            return;
            this.realActivity = new ComponentName(paramActivityInfo.packageName, paramActivityInfo.targetActivity);
            break;
            bool1 = false;
            break label251;
            i = 16973931;
            break label401;
            this.processName = paramActivityInfo.processName;
            break label485;
            bool2 = false;
            break label588;
            bool3 = false;
            break label616;
            this.isHomeActivity = false;
            break label733;
            this.isHomeActivity = false;
            break label733;
            bool4 = false;
            break label748;
            this.realActivity = null;
            this.taskAffinity = null;
            this.stateNotNeeded = false;
            this.baseDir = null;
            this.resDir = null;
            this.dataDir = null;
            this.processName = null;
            this.packageName = null;
            this.fullscreen = true;
            this.noDisplay = false;
            this.isHomeActivity = false;
        }
    }

    static ActivityRecord forToken(IBinder paramIBinder)
    {
        Object localObject = null;
        if (paramIBinder != null);
        try
        {
            for (ActivityRecord localActivityRecord = (ActivityRecord)((Token)paramIBinder).weakActivity.get(); ; localActivityRecord = null)
            {
                localObject = localActivityRecord;
                return localObject;
            }
        }
        catch (ClassCastException localClassCastException)
        {
            while (true)
                Slog.w("ActivityManager", "Bad activity token: " + paramIBinder, localClassCastException);
        }
    }

    private ActivityRecord getWaitingHistoryRecordLocked()
    {
        ActivityRecord localActivityRecord = this;
        if (localActivityRecord.waitingVisible)
        {
            localActivityRecord = this.stack.mResumedActivity;
            if (localActivityRecord == null)
                localActivityRecord = this.stack.mPausingActivity;
            if (localActivityRecord == null)
                localActivityRecord = this;
        }
        return localActivityRecord;
    }

    void addNewIntentLocked(Intent paramIntent)
    {
        if (this.newIntents == null)
            this.newIntents = new ArrayList();
        this.newIntents.add(paramIntent);
    }

    void addResultLocked(ActivityRecord paramActivityRecord, String paramString, int paramInt1, int paramInt2, Intent paramIntent)
    {
        ActivityResult localActivityResult = new ActivityResult(paramActivityRecord, paramString, paramInt1, paramInt2, paramIntent);
        if (this.results == null)
            this.results = new ArrayList();
        this.results.add(localActivityResult);
    }

    void applyOptionsLocked()
    {
        int i;
        if (this.pendingOptions != null)
            i = this.pendingOptions.getAnimationType();
        switch (i)
        {
        default:
        case 1:
        case 2:
            while (true)
            {
                this.pendingOptions = null;
                return;
                this.service.mWindowManager.overridePendingAppTransition(this.pendingOptions.getPackageName(), this.pendingOptions.getCustomEnterResId(), this.pendingOptions.getCustomExitResId(), this.pendingOptions.getOnAnimationStartListener());
                continue;
                this.service.mWindowManager.overridePendingAppTransitionScaleUp(this.pendingOptions.getStartX(), this.pendingOptions.getStartY(), this.pendingOptions.getStartWidth(), this.pendingOptions.getStartHeight());
                if (this.intent.getSourceBounds() == null)
                    this.intent.setSourceBounds(new Rect(this.pendingOptions.getStartX(), this.pendingOptions.getStartY(), this.pendingOptions.getStartX() + this.pendingOptions.getStartWidth(), this.pendingOptions.getStartY() + this.pendingOptions.getStartHeight()));
            }
        case 3:
        case 4:
        }
        if (i == 4);
        for (boolean bool = true; ; bool = false)
        {
            this.service.mWindowManager.overridePendingAppTransitionThumb(this.pendingOptions.getThumbnail(), this.pendingOptions.getStartX(), this.pendingOptions.getStartY(), this.pendingOptions.getOnAnimationStartListener(), bool);
            if (this.intent.getSourceBounds() != null)
                break;
            this.intent.setSourceBounds(new Rect(this.pendingOptions.getStartX(), this.pendingOptions.getStartY(), this.pendingOptions.getStartX() + this.pendingOptions.getThumbnail().getWidth(), this.pendingOptions.getStartY() + this.pendingOptions.getThumbnail().getHeight()));
            break;
        }
    }

    void clearOptionsLocked()
    {
        if (this.pendingOptions != null)
        {
            this.pendingOptions.abort();
            this.pendingOptions = null;
        }
    }

    void clearThumbnail()
    {
        if (this.thumbHolder != null)
        {
            this.thumbHolder.lastThumbnail = null;
            this.thumbHolder.lastDescription = null;
        }
    }

    boolean continueLaunchTickingLocked()
    {
        if (this.launchTickTime != 0L)
        {
            Message localMessage = this.stack.mHandler.obtainMessage(107);
            localMessage.obj = this;
            this.stack.mHandler.removeMessages(107);
            this.stack.mHandler.sendMessageDelayed(localMessage, 500L);
        }
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    // ERROR //
    final void deliverNewIntentLocked(int paramInt, Intent paramIntent)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore_3
        //     2: aload_0
        //     3: getfield 175	com/android/server/am/ActivityRecord:state	Lcom/android/server/am/ActivityStack$ActivityState;
        //     6: getstatic 569	com/android/server/am/ActivityStack$ActivityState:RESUMED	Lcom/android/server/am/ActivityStack$ActivityState;
        //     9: if_acmpeq +25 -> 34
        //     12: aload_0
        //     13: getfield 110	com/android/server/am/ActivityRecord:service	Lcom/android/server/am/ActivityManagerService;
        //     16: getfield 572	com/android/server/am/ActivityManagerService:mSleeping	Z
        //     19: ifeq +100 -> 119
        //     22: aload_0
        //     23: getfield 112	com/android/server/am/ActivityRecord:stack	Lcom/android/server/am/ActivityStack;
        //     26: aconst_null
        //     27: invokevirtual 576	com/android/server/am/ActivityStack:topRunningActivityLocked	(Lcom/android/server/am/ActivityRecord;)Lcom/android/server/am/ActivityRecord;
        //     30: aload_0
        //     31: if_acmpne +88 -> 119
        //     34: aload_0
        //     35: getfield 578	com/android/server/am/ActivityRecord:app	Lcom/android/server/am/ProcessRecord;
        //     38: ifnull +81 -> 119
        //     41: aload_0
        //     42: getfield 578	com/android/server/am/ActivityRecord:app	Lcom/android/server/am/ProcessRecord;
        //     45: getfield 582	com/android/server/am/ProcessRecord:thread	Landroid/app/IApplicationThread;
        //     48: ifnull +71 -> 119
        //     51: new 425	java/util/ArrayList
        //     54: dup
        //     55: invokespecial 426	java/util/ArrayList:<init>	()V
        //     58: astore 4
        //     60: new 144	android/content/Intent
        //     63: dup
        //     64: aload_2
        //     65: invokespecial 584	android/content/Intent:<init>	(Landroid/content/Intent;)V
        //     68: astore 5
        //     70: aload 4
        //     72: aload 5
        //     74: invokevirtual 429	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     77: pop
        //     78: aload_0
        //     79: getfield 110	com/android/server/am/ActivityRecord:service	Lcom/android/server/am/ActivityManagerService;
        //     82: iload_1
        //     83: aload_0
        //     84: getfield 276	com/android/server/am/ActivityRecord:packageName	Ljava/lang/String;
        //     87: aload 5
        //     89: aload_0
        //     90: invokevirtual 588	com/android/server/am/ActivityRecord:getUriPermissionsLocked	()Lcom/android/server/am/UriPermissionOwner;
        //     93: invokevirtual 592	com/android/server/am/ActivityManagerService:grantUriPermissionFromIntentLocked	(ILjava/lang/String;Landroid/content/Intent;Lcom/android/server/am/UriPermissionOwner;)V
        //     96: aload_0
        //     97: getfield 578	com/android/server/am/ActivityRecord:app	Lcom/android/server/am/ProcessRecord;
        //     100: getfield 582	com/android/server/am/ProcessRecord:thread	Landroid/app/IApplicationThread;
        //     103: aload 4
        //     105: aload_0
        //     106: getfield 117	com/android/server/am/ActivityRecord:appToken	Landroid/view/IApplicationToken$Stub;
        //     109: invokeinterface 598 3 0
        //     114: iconst_1
        //     115: istore_3
        //     116: aload 5
        //     118: astore_2
        //     119: iload_3
        //     120: ifne +15 -> 135
        //     123: aload_0
        //     124: new 144	android/content/Intent
        //     127: dup
        //     128: aload_2
        //     129: invokespecial 584	android/content/Intent:<init>	(Landroid/content/Intent;)V
        //     132: invokevirtual 600	com/android/server/am/ActivityRecord:addNewIntentLocked	(Landroid/content/Intent;)V
        //     135: return
        //     136: astore 6
        //     138: ldc_w 388
        //     141: new 390	java/lang/StringBuilder
        //     144: dup
        //     145: invokespecial 391	java/lang/StringBuilder:<init>	()V
        //     148: ldc_w 602
        //     151: invokevirtual 397	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     154: aload_0
        //     155: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     158: invokevirtual 403	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     161: aload 6
        //     163: invokestatic 409	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     166: pop
        //     167: goto -48 -> 119
        //     170: astore 8
        //     172: ldc_w 388
        //     175: new 390	java/lang/StringBuilder
        //     178: dup
        //     179: invokespecial 391	java/lang/StringBuilder:<init>	()V
        //     182: ldc_w 602
        //     185: invokevirtual 397	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     188: aload_0
        //     189: invokevirtual 400	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     192: invokevirtual 403	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     195: aload 8
        //     197: invokestatic 409	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     200: pop
        //     201: goto -82 -> 119
        //     204: astore 8
        //     206: aload 5
        //     208: astore_2
        //     209: goto -37 -> 172
        //     212: astore 6
        //     214: aload 5
        //     216: astore_2
        //     217: goto -79 -> 138
        //
        // Exception table:
        //     from	to	target	type
        //     51	70	136	android/os/RemoteException
        //     51	70	170	java/lang/NullPointerException
        //     70	114	204	java/lang/NullPointerException
        //     70	114	212	android/os/RemoteException
    }

    void dump(PrintWriter paramPrintWriter, String paramString)
    {
        long l = SystemClock.uptimeMillis();
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("packageName=");
        paramPrintWriter.print(this.packageName);
        paramPrintWriter.print(" processName=");
        paramPrintWriter.println(this.processName);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("launchedFromUid=");
        paramPrintWriter.print(this.launchedFromUid);
        paramPrintWriter.print(" userId=");
        paramPrintWriter.println(this.userId);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("app=");
        paramPrintWriter.println(this.app);
        paramPrintWriter.print(paramString);
        paramPrintWriter.println(this.intent.toInsecureStringWithClip());
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("frontOfTask=");
        paramPrintWriter.print(this.frontOfTask);
        paramPrintWriter.print(" task=");
        paramPrintWriter.println(this.task);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("taskAffinity=");
        paramPrintWriter.println(this.taskAffinity);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("realActivity=");
        paramPrintWriter.println(this.realActivity.flattenToShortString());
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("baseDir=");
        paramPrintWriter.println(this.baseDir);
        if (!this.resDir.equals(this.baseDir))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("resDir=");
            paramPrintWriter.println(this.resDir);
        }
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("dataDir=");
        paramPrintWriter.println(this.dataDir);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("stateNotNeeded=");
        paramPrintWriter.print(this.stateNotNeeded);
        paramPrintWriter.print(" componentSpecified=");
        paramPrintWriter.print(this.componentSpecified);
        paramPrintWriter.print(" isHomeActivity=");
        paramPrintWriter.println(this.isHomeActivity);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("compat=");
        paramPrintWriter.print(this.compat);
        paramPrintWriter.print(" labelRes=0x");
        paramPrintWriter.print(Integer.toHexString(this.labelRes));
        paramPrintWriter.print(" icon=0x");
        paramPrintWriter.print(Integer.toHexString(this.icon));
        paramPrintWriter.print(" theme=0x");
        paramPrintWriter.println(Integer.toHexString(this.theme));
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("config=");
        paramPrintWriter.println(this.configuration);
        if ((this.resultTo != null) || (this.resultWho != null))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("resultTo=");
            paramPrintWriter.print(this.resultTo);
            paramPrintWriter.print(" resultWho=");
            paramPrintWriter.print(this.resultWho);
            paramPrintWriter.print(" resultCode=");
            paramPrintWriter.println(this.requestCode);
        }
        if (this.results != null)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("results=");
            paramPrintWriter.println(this.results);
        }
        if ((this.pendingResults != null) && (this.pendingResults.size() > 0))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.println("Pending Results:");
            Iterator localIterator = this.pendingResults.iterator();
            while (localIterator.hasNext())
            {
                WeakReference localWeakReference = (WeakReference)localIterator.next();
                if (localWeakReference != null);
                for (PendingIntentRecord localPendingIntentRecord = (PendingIntentRecord)localWeakReference.get(); ; localPendingIntentRecord = null)
                {
                    paramPrintWriter.print(paramString);
                    paramPrintWriter.print("    - ");
                    if (localPendingIntentRecord != null)
                        break label605;
                    paramPrintWriter.println("null");
                    break;
                }
                label605: paramPrintWriter.println(localPendingIntentRecord);
                localPendingIntentRecord.dump(paramPrintWriter, paramString + "        ");
            }
        }
        if ((this.newIntents != null) && (this.newIntents.size() > 0))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.println("Pending New Intents:");
            int i = 0;
            if (i < this.newIntents.size())
            {
                Intent localIntent = (Intent)this.newIntents.get(i);
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("    - ");
                if (localIntent == null)
                    paramPrintWriter.println("null");
                while (true)
                {
                    i++;
                    break;
                    paramPrintWriter.println(localIntent.toShortString(false, true, false, true));
                }
            }
        }
        if (this.pendingOptions != null)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("pendingOptions=");
            paramPrintWriter.println(this.pendingOptions);
        }
        if (this.uriPermissions != null)
        {
            if (this.uriPermissions.readUriPermissions != null)
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("readUriPermissions=");
                paramPrintWriter.println(this.uriPermissions.readUriPermissions);
            }
            if (this.uriPermissions.writeUriPermissions != null)
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("writeUriPermissions=");
                paramPrintWriter.println(this.uriPermissions.writeUriPermissions);
            }
        }
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("launchFailed=");
        paramPrintWriter.print(this.launchFailed);
        paramPrintWriter.print(" haveState=");
        paramPrintWriter.print(this.haveState);
        paramPrintWriter.print(" icicle=");
        paramPrintWriter.println(this.icicle);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("state=");
        paramPrintWriter.print(this.state);
        paramPrintWriter.print(" stopped=");
        paramPrintWriter.print(this.stopped);
        paramPrintWriter.print(" delayedResume=");
        paramPrintWriter.print(this.delayedResume);
        paramPrintWriter.print(" finishing=");
        paramPrintWriter.println(this.finishing);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("keysPaused=");
        paramPrintWriter.print(this.keysPaused);
        paramPrintWriter.print(" inHistory=");
        paramPrintWriter.print(this.inHistory);
        paramPrintWriter.print(" visible=");
        paramPrintWriter.print(this.visible);
        paramPrintWriter.print(" sleeping=");
        paramPrintWriter.print(this.sleeping);
        paramPrintWriter.print(" idle=");
        paramPrintWriter.println(this.idle);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("fullscreen=");
        paramPrintWriter.print(this.fullscreen);
        paramPrintWriter.print(" noDisplay=");
        paramPrintWriter.print(this.noDisplay);
        paramPrintWriter.print(" immersive=");
        paramPrintWriter.print(this.immersive);
        paramPrintWriter.print(" launchMode=");
        paramPrintWriter.println(this.launchMode);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("frozenBeforeDestroy=");
        paramPrintWriter.print(this.frozenBeforeDestroy);
        paramPrintWriter.print(" thumbnailNeeded=");
        paramPrintWriter.print(this.thumbnailNeeded);
        paramPrintWriter.print(" forceNewConfig=");
        paramPrintWriter.println(this.forceNewConfig);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("thumbHolder=");
        paramPrintWriter.println(this.thumbHolder);
        if ((this.launchTime != 0L) || (this.startTime != 0L))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("launchTime=");
            if (this.launchTime == 0L)
            {
                paramPrintWriter.print("0");
                paramPrintWriter.print(" startTime=");
                if (this.startTime != 0L)
                    break label1424;
                paramPrintWriter.print("0");
                label1243: paramPrintWriter.println();
            }
        }
        else if ((this.lastVisibleTime != 0L) || (this.waitingVisible) || (this.nowVisible))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("waitingVisible=");
            paramPrintWriter.print(this.waitingVisible);
            paramPrintWriter.print(" nowVisible=");
            paramPrintWriter.print(this.nowVisible);
            paramPrintWriter.print(" lastVisibleTime=");
            if (this.lastVisibleTime != 0L)
                break label1436;
            paramPrintWriter.print("0");
        }
        while (true)
        {
            paramPrintWriter.println();
            if ((this.configDestroy) || (this.configChangeFlags != 0))
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("configDestroy=");
                paramPrintWriter.print(this.configDestroy);
                paramPrintWriter.print(" configChangeFlags=");
                paramPrintWriter.println(Integer.toHexString(this.configChangeFlags));
            }
            if (this.connections != null)
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("connections=");
                paramPrintWriter.println(this.connections);
            }
            return;
            TimeUtils.formatDuration(this.launchTime, l, paramPrintWriter);
            break;
            label1424: TimeUtils.formatDuration(this.startTime, l, paramPrintWriter);
            break label1243;
            label1436: TimeUtils.formatDuration(this.lastVisibleTime, l, paramPrintWriter);
        }
    }

    void finishLaunchTickingLocked()
    {
        this.launchTickTime = 0L;
        this.stack.mHandler.removeMessages(107);
    }

    public long getKeyDispatchingTimeout()
    {
        long l;
        synchronized (this.service)
        {
            ActivityRecord localActivityRecord = getWaitingHistoryRecordLocked();
            if ((localActivityRecord != null) && (localActivityRecord.app != null) && ((localActivityRecord.app.instrumentationClass != null) || (localActivityRecord.app.usingWrapper)))
                l = 60000L;
            else
                l = 5000L;
        }
        return l;
    }

    UriPermissionOwner getUriPermissionsLocked()
    {
        if (this.uriPermissions == null)
            this.uriPermissions = new UriPermissionOwner(this.service, this);
        return this.uriPermissions;
    }

    boolean isInHistory()
    {
        return this.inHistory;
    }

    public boolean isInterestingToUserLocked()
    {
        if ((this.visible) || (this.nowVisible) || (this.state == ActivityStack.ActivityState.PAUSING) || (this.state == ActivityStack.ActivityState.RESUMED));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    // ERROR //
    public boolean keyDispatchingTimedOut()
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore_1
        //     2: aconst_null
        //     3: astore_2
        //     4: aload_0
        //     5: getfield 110	com/android/server/am/ActivityRecord:service	Lcom/android/server/am/ActivityManagerService;
        //     8: astore_3
        //     9: aload_3
        //     10: monitorenter
        //     11: aload_0
        //     12: invokespecial 837	com/android/server/am/ActivityRecord:getWaitingHistoryRecordLocked	()Lcom/android/server/am/ActivityRecord;
        //     15: astore 5
        //     17: aload 5
        //     19: ifnull +74 -> 93
        //     22: aload 5
        //     24: getfield 578	com/android/server/am/ActivityRecord:app	Lcom/android/server/am/ProcessRecord;
        //     27: ifnull +66 -> 93
        //     30: aload 5
        //     32: getfield 578	com/android/server/am/ActivityRecord:app	Lcom/android/server/am/ProcessRecord;
        //     35: getfield 859	com/android/server/am/ProcessRecord:debugging	Z
        //     38: ifeq +8 -> 46
        //     41: aload_3
        //     42: monitorexit
        //     43: goto +124 -> 167
        //     46: aload_0
        //     47: getfield 110	com/android/server/am/ActivityRecord:service	Lcom/android/server/am/ActivityManagerService;
        //     50: getfield 862	com/android/server/am/ActivityManagerService:mDidDexOpt	Z
        //     53: ifeq +23 -> 76
        //     56: aload_0
        //     57: getfield 110	com/android/server/am/ActivityRecord:service	Lcom/android/server/am/ActivityManagerService;
        //     60: iconst_0
        //     61: putfield 862	com/android/server/am/ActivityManagerService:mDidDexOpt	Z
        //     64: aload_3
        //     65: monitorexit
        //     66: goto +101 -> 167
        //     69: astore 4
        //     71: aload_3
        //     72: monitorexit
        //     73: aload 4
        //     75: athrow
        //     76: aload 5
        //     78: getfield 578	com/android/server/am/ActivityRecord:app	Lcom/android/server/am/ProcessRecord;
        //     81: getfield 840	com/android/server/am/ProcessRecord:instrumentationClass	Landroid/content/ComponentName;
        //     84: ifnonnull +34 -> 118
        //     87: aload 5
        //     89: getfield 578	com/android/server/am/ActivityRecord:app	Lcom/android/server/am/ProcessRecord;
        //     92: astore_2
        //     93: aload_3
        //     94: monitorexit
        //     95: aload_2
        //     96: ifnull +17 -> 113
        //     99: aload_0
        //     100: getfield 110	com/android/server/am/ActivityRecord:service	Lcom/android/server/am/ActivityManagerService;
        //     103: aload_2
        //     104: aload 5
        //     106: aload_0
        //     107: ldc_w 863
        //     110: invokevirtual 867	com/android/server/am/ActivityManagerService:appNotResponding	(Lcom/android/server/am/ProcessRecord;Lcom/android/server/am/ActivityRecord;Lcom/android/server/am/ActivityRecord;Ljava/lang/String;)V
        //     113: iconst_1
        //     114: istore_1
        //     115: goto +52 -> 167
        //     118: new 869	android/os/Bundle
        //     121: dup
        //     122: invokespecial 870	android/os/Bundle:<init>	()V
        //     125: astore 6
        //     127: aload 6
        //     129: ldc_w 872
        //     132: ldc_w 863
        //     135: invokevirtual 875	android/os/Bundle:putString	(Ljava/lang/String;Ljava/lang/String;)V
        //     138: aload 6
        //     140: ldc_w 877
        //     143: ldc_w 879
        //     146: invokevirtual 875	android/os/Bundle:putString	(Ljava/lang/String;Ljava/lang/String;)V
        //     149: aload_0
        //     150: getfield 110	com/android/server/am/ActivityRecord:service	Lcom/android/server/am/ActivityManagerService;
        //     153: aload 5
        //     155: getfield 578	com/android/server/am/ActivityRecord:app	Lcom/android/server/am/ProcessRecord;
        //     158: iconst_0
        //     159: aload 6
        //     161: invokevirtual 883	com/android/server/am/ActivityManagerService:finishInstrumentationLocked	(Lcom/android/server/am/ProcessRecord;ILandroid/os/Bundle;)V
        //     164: goto -71 -> 93
        //     167: iload_1
        //     168: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     11	73	69	finally
        //     76	95	69	finally
        //     118	164	69	finally
    }

    void makeFinishing()
    {
        if (!this.finishing)
        {
            this.finishing = true;
            if ((this.task != null) && (this.inHistory))
            {
                TaskRecord localTaskRecord = this.task;
                localTaskRecord.numActivities = (-1 + localTaskRecord.numActivities);
            }
            if (this.stopped)
                clearOptionsLocked();
        }
    }

    public boolean mayFreezeScreenLocked(ProcessRecord paramProcessRecord)
    {
        if ((paramProcessRecord != null) && (!paramProcessRecord.crashing) && (!paramProcessRecord.notResponding));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    void pauseKeyDispatchingLocked()
    {
        if (!this.keysPaused)
        {
            this.keysPaused = true;
            this.service.mWindowManager.pauseKeyDispatching(this.appToken);
        }
    }

    void putInHistory()
    {
        if (!this.inHistory)
        {
            this.inHistory = true;
            if ((this.task != null) && (!this.finishing))
            {
                TaskRecord localTaskRecord = this.task;
                localTaskRecord.numActivities = (1 + localTaskRecord.numActivities);
            }
        }
    }

    void removeResultsLocked(ActivityRecord paramActivityRecord, String paramString, int paramInt)
    {
        if (this.results != null)
        {
            int i = -1 + this.results.size();
            if (i >= 0)
            {
                ActivityResult localActivityResult = (ActivityResult)this.results.get(i);
                if (localActivityResult.mFrom != paramActivityRecord)
                    label46: i--;
                while (true)
                {
                    break;
                    if (localActivityResult.mResultWho == null)
                    {
                        if (paramString == null)
                            if (localActivityResult.mRequestCode == paramInt)
                                this.results.remove(i);
                    }
                    else
                        if (localActivityResult.mResultWho.equals(paramString))
                            break label46;
                }
            }
        }
    }

    void removeUriPermissionsLocked()
    {
        if (this.uriPermissions != null)
        {
            this.uriPermissions.removeUriPermissionsLocked();
            this.uriPermissions = null;
        }
    }

    void resumeKeyDispatchingLocked()
    {
        if (this.keysPaused)
        {
            this.keysPaused = false;
            this.service.mWindowManager.resumeKeyDispatching(this.appToken);
        }
    }

    public void setSleeping(boolean paramBoolean)
    {
        if (this.sleeping == paramBoolean);
        while (true)
        {
            return;
            if ((this.app != null) && (this.app.thread != null))
                try
                {
                    this.app.thread.scheduleSleeping(this.appToken, paramBoolean);
                    if ((this.sleeping) && (!this.stack.mGoingToSleepActivities.contains(this)))
                        this.stack.mGoingToSleepActivities.add(this);
                    this.sleeping = paramBoolean;
                }
                catch (RemoteException localRemoteException)
                {
                    Slog.w("ActivityManager", "Exception thrown when sleeping: " + this.intent.getComponent(), localRemoteException);
                }
        }
    }

    void setTask(TaskRecord paramTaskRecord, ThumbnailHolder paramThumbnailHolder, boolean paramBoolean)
    {
        if ((this.inHistory) && (!this.finishing))
        {
            if (this.task != null)
            {
                TaskRecord localTaskRecord = this.task;
                localTaskRecord.numActivities = (-1 + localTaskRecord.numActivities);
            }
            if (paramTaskRecord != null)
                paramTaskRecord.numActivities = (1 + paramTaskRecord.numActivities);
        }
        if (paramThumbnailHolder == null)
            paramThumbnailHolder = paramTaskRecord;
        this.task = paramTaskRecord;
        if ((!paramBoolean) && ((0x80000 & this.intent.getFlags()) != 0))
            if (this.thumbHolder != null);
        for (this.thumbHolder = new ThumbnailHolder(); ; this.thumbHolder = paramThumbnailHolder)
            return;
    }

    public void startFreezingScreenLocked(ProcessRecord paramProcessRecord, int paramInt)
    {
        if (mayFreezeScreenLocked(paramProcessRecord))
            this.service.mWindowManager.startAppFreezingScreen(this.appToken, paramInt);
    }

    void startLaunchTickingLocked()
    {
        if (ActivityManagerService.IS_USER_BUILD);
        while (true)
        {
            return;
            if (this.launchTickTime == 0L)
            {
                this.launchTickTime = SystemClock.uptimeMillis();
                continueLaunchTickingLocked();
            }
        }
    }

    public void stopFreezingScreenLocked(boolean paramBoolean)
    {
        if ((paramBoolean) || (this.frozenBeforeDestroy))
        {
            this.frozenBeforeDestroy = false;
            this.service.mWindowManager.stopAppFreezingScreen(this.appToken, paramBoolean);
        }
    }

    void takeFromHistory()
    {
        if (this.inHistory)
        {
            this.inHistory = false;
            if ((this.task != null) && (!this.finishing))
            {
                TaskRecord localTaskRecord = this.task;
                localTaskRecord.numActivities = (-1 + localTaskRecord.numActivities);
            }
            clearOptionsLocked();
        }
    }

    public String toString()
    {
        String str;
        if (this.stringName != null)
            str = this.stringName;
        while (true)
        {
            return str;
            StringBuilder localStringBuilder = new StringBuilder(128);
            localStringBuilder.append("ActivityRecord{");
            localStringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
            localStringBuilder.append(' ');
            localStringBuilder.append(this.intent.getComponent().flattenToShortString());
            localStringBuilder.append('}');
            str = localStringBuilder.toString();
            this.stringName = str;
        }
    }

    void updateOptionsLocked(Bundle paramBundle)
    {
        if (paramBundle != null)
        {
            if (this.pendingOptions != null)
                this.pendingOptions.abort();
            this.pendingOptions = new ActivityOptions(paramBundle);
        }
    }

    void updateThumbnail(Bitmap paramBitmap, CharSequence paramCharSequence)
    {
        if (((0x80000 & this.intent.getFlags()) == 0) || (this.thumbHolder != null))
        {
            if (paramBitmap != null)
                this.thumbHolder.lastThumbnail = paramBitmap;
            this.thumbHolder.lastDescription = paramCharSequence;
        }
    }

    public void windowsDrawn()
    {
        while (true)
        {
            long l2;
            synchronized (this.service)
            {
                if (this.launchTime != 0L)
                {
                    long l1 = SystemClock.uptimeMillis();
                    l2 = l1 - this.launchTime;
                    if (this.stack.mInitialStartTime != 0L)
                    {
                        l3 = l1 - this.stack.mInitialStartTime;
                        Object[] arrayOfObject = new Object[4];
                        arrayOfObject[0] = Integer.valueOf(System.identityHashCode(this));
                        arrayOfObject[1] = this.shortComponentName;
                        arrayOfObject[2] = Long.valueOf(l2);
                        arrayOfObject[3] = Long.valueOf(l3);
                        EventLog.writeEvent(30009, arrayOfObject);
                        StringBuilder localStringBuilder = this.service.mStringBuilder;
                        localStringBuilder.setLength(0);
                        localStringBuilder.append("Displayed ");
                        localStringBuilder.append(this.shortComponentName);
                        localStringBuilder.append(": ");
                        TimeUtils.formatDuration(l2, localStringBuilder);
                        if (l2 != l3)
                        {
                            localStringBuilder.append(" (total ");
                            TimeUtils.formatDuration(l3, localStringBuilder);
                            localStringBuilder.append(")");
                        }
                        Log.i("ActivityManager", localStringBuilder.toString());
                        this.stack.reportActivityLaunchedLocked(false, this, l2, l3);
                        if (l3 > 0L)
                            this.service.mUsageStatsService.noteLaunchTime(this.realActivity, (int)l3);
                        this.launchTime = 0L;
                        this.stack.mInitialStartTime = 0L;
                    }
                }
                else
                {
                    this.startTime = 0L;
                    finishLaunchTickingLocked();
                    return;
                }
            }
            long l3 = l2;
        }
    }

    public void windowsGone()
    {
        this.nowVisible = false;
    }

    public void windowsVisible()
    {
        synchronized (this.service)
        {
            this.stack.reportActivityVisibleLocked(this);
            if (!this.nowVisible)
            {
                this.nowVisible = true;
                this.lastVisibleTime = SystemClock.uptimeMillis();
                if (this.idle)
                    break label60;
                this.stack.processStoppingActivitiesLocked(false);
            }
            label60: int i;
            do
            {
                this.service.scheduleAppGcsLocked();
                return;
                i = this.stack.mWaitingVisibleActivities.size();
            }
            while (i <= 0);
            for (int j = 0; j < i; j++)
                ((ActivityRecord)this.stack.mWaitingVisibleActivities.get(j)).waitingVisible = false;
            this.stack.mWaitingVisibleActivities.clear();
            Message localMessage = Message.obtain();
            localMessage.what = 103;
            this.stack.mHandler.sendMessage(localMessage);
        }
    }

    static class Token extends IApplicationToken.Stub
    {
        final WeakReference<ActivityRecord> weakActivity;

        Token(ActivityRecord paramActivityRecord)
        {
            this.weakActivity = new WeakReference(paramActivityRecord);
        }

        public long getKeyDispatchingTimeout()
            throws RemoteException
        {
            ActivityRecord localActivityRecord = (ActivityRecord)this.weakActivity.get();
            if (localActivityRecord != null);
            for (long l = localActivityRecord.getKeyDispatchingTimeout(); ; l = 0L)
                return l;
        }

        public boolean keyDispatchingTimedOut()
            throws RemoteException
        {
            ActivityRecord localActivityRecord = (ActivityRecord)this.weakActivity.get();
            if (localActivityRecord != null);
            for (boolean bool = localActivityRecord.keyDispatchingTimedOut(); ; bool = false)
                return bool;
        }

        public String toString()
        {
            StringBuilder localStringBuilder = new StringBuilder(128);
            localStringBuilder.append("Token{");
            localStringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
            localStringBuilder.append(' ');
            localStringBuilder.append(this.weakActivity.get());
            localStringBuilder.append('}');
            return localStringBuilder.toString();
        }

        public void windowsDrawn()
            throws RemoteException
        {
            ActivityRecord localActivityRecord = (ActivityRecord)this.weakActivity.get();
            if (localActivityRecord != null)
                localActivityRecord.windowsDrawn();
        }

        public void windowsGone()
            throws RemoteException
        {
            ActivityRecord localActivityRecord = (ActivityRecord)this.weakActivity.get();
            if (localActivityRecord != null)
                localActivityRecord.windowsGone();
        }

        public void windowsVisible()
            throws RemoteException
        {
            ActivityRecord localActivityRecord = (ActivityRecord)this.weakActivity.get();
            if (localActivityRecord != null)
                localActivityRecord.windowsVisible();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.ActivityRecord
 * JD-Core Version:        0.6.2
 */