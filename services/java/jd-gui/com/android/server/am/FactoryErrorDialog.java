package com.android.server.am;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.Window;

class FactoryErrorDialog extends BaseErrorDialog
{
    private final Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            throw new RuntimeException("Rebooting from failed factory test");
        }
    };

    public FactoryErrorDialog(Context paramContext, CharSequence paramCharSequence)
    {
        super(paramContext);
        setCancelable(false);
        setTitle(paramContext.getText(17040183));
        setMessage(paramCharSequence);
        setButton(-1, paramContext.getText(17040186), this.mHandler.obtainMessage(0));
        getWindow().setTitle("Factory Error");
    }

    public void onStop()
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.FactoryErrorDialog
 * JD-Core Version:        0.6.2
 */