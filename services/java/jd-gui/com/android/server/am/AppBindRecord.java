package com.android.server.am;

import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Iterator;

class AppBindRecord
{
    final ProcessRecord client;
    final HashSet<ConnectionRecord> connections = new HashSet();
    final IntentBindRecord intent;
    final ServiceRecord service;

    AppBindRecord(ServiceRecord paramServiceRecord, IntentBindRecord paramIntentBindRecord, ProcessRecord paramProcessRecord)
    {
        this.service = paramServiceRecord;
        this.intent = paramIntentBindRecord;
        this.client = paramProcessRecord;
    }

    void dump(PrintWriter paramPrintWriter, String paramString)
    {
        paramPrintWriter.println(paramString + "service=" + this.service);
        paramPrintWriter.println(paramString + "client=" + this.client);
        dumpInIntentBind(paramPrintWriter, paramString);
    }

    void dumpInIntentBind(PrintWriter paramPrintWriter, String paramString)
    {
        if (this.connections.size() > 0)
        {
            paramPrintWriter.println(paramString + "Per-process Connections:");
            Iterator localIterator = this.connections.iterator();
            while (localIterator.hasNext())
            {
                ConnectionRecord localConnectionRecord = (ConnectionRecord)localIterator.next();
                paramPrintWriter.println(paramString + "    " + localConnectionRecord);
            }
        }
    }

    public String toString()
    {
        return "AppBindRecord{" + Integer.toHexString(System.identityHashCode(this)) + " " + this.service.shortName + ":" + this.client.processName + "}";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.am.AppBindRecord
 * JD-Core Version:        0.6.2
 */