package com.android.server.accessibility;

import android.content.Context;
import android.content.res.Resources;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GesturePoint;
import android.gesture.GestureStroke;
import android.gesture.Prediction;
import android.graphics.Rect;
import android.os.Handler;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Slog;
import android.view.InputEvent;
import android.view.MotionEvent;
import android.view.MotionEvent.PointerCoords;
import android.view.MotionEvent.PointerProperties;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;
import android.view.accessibility.AccessibilityEvent;
import com.android.server.input.InputFilter;
import java.util.ArrayList;
import java.util.Arrays;

public class TouchExplorer
{
    private static final int ALL_POINTER_ID_BITS = -1;
    private static final boolean DEBUG = false;
    private static final int EXIT_GESTURE_DETECTION_TIMEOUT = 2000;
    private static final int GESTURE_DETECTION_VELOCITY_DIP = 1000;
    private static final int INVALID_POINTER_ID = -1;
    private static final String LOG_TAG = "TouchExplorer";
    private static final float MAX_DRAGGING_ANGLE_COS = 0.525322F;
    private static final int MAX_POINTER_COUNT = 32;
    private static final int MIN_POINTER_DISTANCE_TO_USE_MIDDLE_LOCATION_DIP = 200;
    private static final float MIN_PREDICTION_SCORE = 2.0F;
    private static final int STATE_DELEGATING = 4;
    private static final int STATE_DRAGGING = 2;
    private static final int STATE_GESTURE_DETECTING = 5;
    private static final int STATE_TOUCH_EXPLORING = 1;
    private static final int TOUCH_TOLERANCE = 3;
    private final AccessibilityManagerService mAms;
    private int mCurrentState = 1;
    private final int mDetermineUserIntentTimeout;
    private final DoubleTapDetector mDoubleTapDetector;
    private final int mDoubleTapSlop;
    private final int mDoubleTapTimeout;
    private int mDraggingPointerId;
    private final ExitGestureDetectionModeDelayed mExitGestureDetectionModeDelayed;
    private GestureLibrary mGestureLibrary;
    private final Handler mHandler;
    private final InjectedPointerTracker mInjectedPointerTracker;
    private final InputFilter mInputFilter;
    private int mLastTouchedWindowId;
    private int mLongPressingPointerDeltaX;
    private int mLongPressingPointerDeltaY;
    private int mLongPressingPointerId;
    private final PerformLongPressDelayed mPerformLongPressDelayed;
    private float mPreviousX;
    private float mPreviousY;
    private final ReceivedPointerTracker mReceivedPointerTracker;
    private final int mScaledGestureDetectionVelocity;
    private final int mScaledMinPointerDistanceToUseMiddleLocation;
    private final SendHoverDelayed mSendHoverEnterDelayed;
    private final SendHoverDelayed mSendHoverExitDelayed;
    private final ArrayList<GesturePoint> mStrokeBuffer = new ArrayList(100);
    private final int mTapTimeout;
    private final int[] mTempPointerIds = new int[32];
    private final Rect mTempRect = new Rect();
    private final int mTouchSlop;
    private VelocityTracker mVelocityTracker;

    public TouchExplorer(InputFilter paramInputFilter, Context paramContext, AccessibilityManagerService paramAccessibilityManagerService)
    {
        this.mAms = paramAccessibilityManagerService;
        this.mReceivedPointerTracker = new ReceivedPointerTracker(paramContext);
        this.mInjectedPointerTracker = new InjectedPointerTracker();
        this.mInputFilter = paramInputFilter;
        this.mTapTimeout = ViewConfiguration.getTapTimeout();
        this.mDetermineUserIntentTimeout = ((int)(1.5F * this.mTapTimeout));
        this.mDoubleTapTimeout = ViewConfiguration.getDoubleTapTimeout();
        this.mTouchSlop = ViewConfiguration.get(paramContext).getScaledTouchSlop();
        this.mDoubleTapSlop = ViewConfiguration.get(paramContext).getScaledDoubleTapSlop();
        this.mHandler = new Handler(paramContext.getMainLooper());
        this.mPerformLongPressDelayed = new PerformLongPressDelayed(null);
        this.mExitGestureDetectionModeDelayed = new ExitGestureDetectionModeDelayed(null);
        this.mGestureLibrary = GestureLibraries.fromRawResource(paramContext, 17825792);
        this.mGestureLibrary.setOrientationStyle(8);
        this.mGestureLibrary.setSequenceType(2);
        this.mGestureLibrary.load();
        this.mSendHoverEnterDelayed = new SendHoverDelayed(9, true);
        this.mSendHoverExitDelayed = new SendHoverDelayed(10, false);
        this.mDoubleTapDetector = new DoubleTapDetector(null);
        float f = paramContext.getResources().getDisplayMetrics().density;
        this.mScaledMinPointerDistanceToUseMiddleLocation = ((int)(200.0F * f));
        this.mScaledGestureDetectionVelocity = ((int)(1000.0F * f));
    }

    private int computeInjectionAction(int paramInt1, int paramInt2)
    {
        switch (paramInt1)
        {
        default:
        case 0:
        case 5:
        case 6:
        }
        while (true)
        {
            return paramInt1;
            if (this.mInjectedPointerTracker.getInjectedPointerDownCount() == 0)
            {
                paramInt1 = 0;
            }
            else
            {
                paramInt1 = 0x5 | paramInt2 << 8;
                continue;
                if (this.mInjectedPointerTracker.getInjectedPointerDownCount() == 1)
                    paramInt1 = 1;
                else
                    paramInt1 = 0x6 | paramInt2 << 8;
            }
        }
    }

    private int getNotInjectedActivePointerCount(ReceivedPointerTracker paramReceivedPointerTracker, InjectedPointerTracker paramInjectedPointerTracker)
    {
        return Integer.bitCount(paramReceivedPointerTracker.getActivePointers() & (0xFFFFFFFF ^ paramInjectedPointerTracker.getInjectedPointersDown()));
    }

    private static String getStateSymbolicName(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        case 3:
        default:
            throw new IllegalArgumentException("Unknown state: " + paramInt);
        case 1:
            str = "STATE_TOUCH_EXPLORING";
        case 2:
        case 4:
        case 5:
        }
        while (true)
        {
            return str;
            str = "STATE_DRAGGING";
            continue;
            str = "STATE_DELEGATING";
            continue;
            str = "STATE_GESTURE_DETECTING";
        }
    }

    private void handleMotionEventGestureDetecting(MotionEvent paramMotionEvent, int paramInt)
    {
        switch (paramMotionEvent.getActionMasked())
        {
        default:
        case 0:
        case 2:
        case 1:
        case 3:
        }
        while (true)
        {
            return;
            float f7 = paramMotionEvent.getX();
            float f8 = paramMotionEvent.getY();
            this.mPreviousX = f7;
            this.mPreviousY = f8;
            this.mStrokeBuffer.add(new GesturePoint(f7, f8, paramMotionEvent.getEventTime()));
            continue;
            float f3 = paramMotionEvent.getX();
            float f4 = paramMotionEvent.getY();
            float f5 = Math.abs(f3 - this.mPreviousX);
            float f6 = Math.abs(f4 - this.mPreviousY);
            if ((f5 >= 3.0F) || (f6 >= 3.0F))
            {
                this.mPreviousX = f3;
                this.mPreviousY = f4;
                this.mStrokeBuffer.add(new GesturePoint(f3, f4, paramMotionEvent.getEventTime()));
                continue;
                float f1 = paramMotionEvent.getX();
                float f2 = paramMotionEvent.getY();
                this.mStrokeBuffer.add(new GesturePoint(f1, f2, paramMotionEvent.getEventTime()));
                Gesture localGesture = new Gesture();
                localGesture.addStroke(new GestureStroke(this.mStrokeBuffer));
                ArrayList localArrayList = this.mGestureLibrary.recognize(localGesture);
                Prediction localPrediction;
                if (!localArrayList.isEmpty())
                {
                    localPrediction = (Prediction)localArrayList.get(0);
                    if (localPrediction.score < 2.0D);
                }
                try
                {
                    int i = Integer.parseInt(localPrediction.name);
                    this.mAms.onGesture(i);
                    this.mStrokeBuffer.clear();
                    this.mExitGestureDetectionModeDelayed.remove();
                    this.mCurrentState = 1;
                }
                catch (NumberFormatException localNumberFormatException)
                {
                    while (true)
                        Slog.w("TouchExplorer", "Non numeric gesture id:" + localPrediction.name);
                }
                clear(paramMotionEvent, paramInt);
            }
        }
    }

    private void handleMotionEventStateDelegating(MotionEvent paramMotionEvent, int paramInt)
    {
        switch (paramMotionEvent.getActionMasked())
        {
        case 4:
        case 5:
        default:
        case 0:
        case 2:
        case 1:
        case 6:
        case 3:
        }
        while (true)
        {
            sendMotionEventStripInactivePointers(paramMotionEvent, paramInt);
            return;
            throw new IllegalStateException("Delegating state can only be reached if there is at least one pointer down!");
            if (getNotInjectedActivePointerCount(this.mReceivedPointerTracker, this.mInjectedPointerTracker) > 0)
            {
                sendDownForAllActiveNotInjectedPointers(MotionEvent.obtain(paramMotionEvent), paramInt);
                continue;
                this.mLongPressingPointerId = -1;
                this.mLongPressingPointerDeltaX = 0;
                this.mLongPressingPointerDeltaY = 0;
                if (this.mReceivedPointerTracker.getActivePointerCount() == 0)
                {
                    this.mCurrentState = 1;
                    continue;
                    clear(paramMotionEvent, paramInt);
                }
            }
        }
    }

    private void handleMotionEventStateDragging(MotionEvent paramMotionEvent, int paramInt)
    {
        int i = 1 << this.mDraggingPointerId;
        switch (paramMotionEvent.getActionMasked())
        {
        case 4:
        default:
        case 0:
        case 5:
        case 2:
        case 6:
        case 1:
        case 3:
        }
        while (true)
        {
            return;
            throw new IllegalStateException("Dragging state can be reached only if two pointers are already down");
            this.mCurrentState = 4;
            sendMotionEvent(paramMotionEvent, 1, i, paramInt);
            sendDownForAllActiveNotInjectedPointers(paramMotionEvent, paramInt);
            continue;
            switch (this.mReceivedPointerTracker.getActivePointerCount())
            {
            case 1:
            default:
                this.mCurrentState = 4;
                sendMotionEvent(paramMotionEvent, 1, i, paramInt);
                sendDownForAllActiveNotInjectedPointers(paramMotionEvent, paramInt);
                break;
            case 2:
                if (isDraggingGesture(paramMotionEvent))
                {
                    int[] arrayOfInt = this.mTempPointerIds;
                    this.mReceivedPointerTracker.populateActivePointerIds(arrayOfInt);
                    int j = paramMotionEvent.findPointerIndex(arrayOfInt[0]);
                    int k = paramMotionEvent.findPointerIndex(arrayOfInt[1]);
                    float f1 = paramMotionEvent.getX(j);
                    float f2 = paramMotionEvent.getY(j);
                    float f3 = paramMotionEvent.getX(k);
                    float f4 = paramMotionEvent.getY(k);
                    float f5 = f1 - f3;
                    float f6 = f2 - f4;
                    if (Math.hypot(f5, f6) > this.mScaledMinPointerDistanceToUseMiddleLocation)
                        paramMotionEvent.setLocation(f5 / 2.0F, f6 / 2.0F);
                    sendMotionEvent(paramMotionEvent, 2, i, paramInt);
                }
                else
                {
                    this.mCurrentState = 4;
                    sendMotionEvent(paramMotionEvent, 1, i, paramInt);
                    sendDownForAllActiveNotInjectedPointers(paramMotionEvent, paramInt);
                    continue;
                    switch (this.mReceivedPointerTracker.getActivePointerCount())
                    {
                    default:
                        this.mCurrentState = 1;
                        break;
                    case 1:
                        sendMotionEvent(paramMotionEvent, 1, i, paramInt);
                        continue;
                        this.mCurrentState = 1;
                        continue;
                        clear(paramMotionEvent, paramInt);
                    }
                }
                break;
            }
        }
    }

    private void handleMotionEventStateTouchExploring(MotionEvent paramMotionEvent, int paramInt)
    {
        ReceivedPointerTracker localReceivedPointerTracker = this.mReceivedPointerTracker;
        int i = localReceivedPointerTracker.getActivePointerCount();
        if (this.mVelocityTracker == null)
            this.mVelocityTracker = VelocityTracker.obtain();
        this.mVelocityTracker.addMovement(paramMotionEvent);
        this.mDoubleTapDetector.onMotionEvent(paramMotionEvent, paramInt);
        switch (paramMotionEvent.getActionMasked())
        {
        case 4:
        default:
        case 0:
        case 5:
        case 2:
        case 1:
        case 6:
        case 3:
        }
        while (true)
        {
            return;
            handleMotionEventGestureDetecting(paramMotionEvent, paramInt);
            switch (i)
            {
            default:
                break;
            case 0:
                throw new IllegalStateException("The must always be one active pointer intouch exploring state!");
            case 1:
                if (this.mSendHoverEnterDelayed.isPending())
                {
                    this.mSendHoverEnterDelayed.remove();
                    this.mSendHoverExitDelayed.remove();
                }
                this.mPerformLongPressDelayed.remove();
                if (this.mDoubleTapDetector.firstTapDetected())
                {
                    this.mPerformLongPressDelayed.post(paramMotionEvent, paramInt);
                }
                else
                {
                    int i1 = 1 << localReceivedPointerTracker.getPrimaryActivePointerId();
                    this.mSendHoverEnterDelayed.post(paramMotionEvent, i1, paramInt);
                    continue;
                    int k = localReceivedPointerTracker.getPrimaryActivePointerId();
                    int m = paramMotionEvent.findPointerIndex(k);
                    int n = 1 << k;
                    switch (i)
                    {
                    case 0:
                    default:
                        if (this.mSendHoverEnterDelayed.isPending())
                        {
                            this.mSendHoverEnterDelayed.remove();
                            this.mSendHoverExitDelayed.remove();
                            this.mPerformLongPressDelayed.remove();
                        }
                        break;
                    case 1:
                    case 2:
                    }
                    while (true)
                    {
                        this.mCurrentState = 4;
                        sendDownForAllActiveNotInjectedPointers(paramMotionEvent, paramInt);
                        break;
                        if (this.mSendHoverEnterDelayed.isPending())
                        {
                            handleMotionEventGestureDetecting(paramMotionEvent, paramInt);
                            float f3 = localReceivedPointerTracker.getReceivedPointerDownX(k) - paramMotionEvent.getX(m);
                            float f4 = localReceivedPointerTracker.getReceivedPointerDownY(k) - paramMotionEvent.getY(m);
                            if (Math.hypot(f3, f4) <= this.mDoubleTapSlop)
                                break;
                            this.mVelocityTracker.computeCurrentVelocity(1000);
                            if (Math.max(Math.abs(this.mVelocityTracker.getXVelocity(k)), Math.abs(this.mVelocityTracker.getYVelocity(k))) > this.mScaledGestureDetectionVelocity)
                            {
                                this.mCurrentState = 5;
                                this.mSendHoverEnterDelayed.remove();
                                this.mSendHoverExitDelayed.remove();
                                this.mPerformLongPressDelayed.remove();
                                this.mExitGestureDetectionModeDelayed.post();
                                break;
                            }
                            this.mSendHoverEnterDelayed.forceSendAndRemove();
                            this.mSendHoverExitDelayed.remove();
                            this.mPerformLongPressDelayed.remove();
                            sendMotionEvent(paramMotionEvent, 7, n, paramInt);
                            break;
                        }
                        if (this.mDoubleTapDetector.firstTapDetected())
                            break;
                        sendEnterEventsIfNeeded(paramInt);
                        sendMotionEvent(paramMotionEvent, 7, n, paramInt);
                        break;
                        if (this.mSendHoverEnterDelayed.isPending())
                        {
                            this.mSendHoverEnterDelayed.remove();
                            this.mSendHoverExitDelayed.remove();
                            this.mPerformLongPressDelayed.remove();
                        }
                        while (true)
                        {
                            this.mStrokeBuffer.clear();
                            if (!isDraggingGesture(paramMotionEvent))
                                break label641;
                            this.mCurrentState = 2;
                            this.mDraggingPointerId = k;
                            sendMotionEvent(paramMotionEvent, 0, n, paramInt);
                            break;
                            this.mPerformLongPressDelayed.remove();
                            float f1 = localReceivedPointerTracker.getReceivedPointerDownX(k) - paramMotionEvent.getX(m);
                            float f2 = localReceivedPointerTracker.getReceivedPointerDownY(k) - paramMotionEvent.getY(m);
                            if (Math.hypot(f1, f2) < this.mDoubleTapSlop)
                                break;
                            sendExitEventsIfNeeded(paramInt);
                        }
                        label641: this.mCurrentState = 4;
                        sendDownForAllActiveNotInjectedPointers(paramMotionEvent, paramInt);
                        break;
                        this.mPerformLongPressDelayed.remove();
                        sendExitEventsIfNeeded(paramInt);
                    }
                    this.mStrokeBuffer.clear();
                    int j = 1 << localReceivedPointerTracker.getLastReceivedUpPointerId();
                    switch (i)
                    {
                    default:
                    case 0:
                    }
                    while (true)
                    {
                        if (this.mVelocityTracker == null)
                            break label770;
                        this.mVelocityTracker.clear();
                        this.mVelocityTracker = null;
                        break;
                        if (localReceivedPointerTracker.wasLastReceivedUpPointerActive())
                        {
                            this.mPerformLongPressDelayed.remove();
                            if (this.mSendHoverEnterDelayed.isPending())
                                this.mSendHoverExitDelayed.post(paramMotionEvent, j, paramInt);
                            else
                                sendExitEventsIfNeeded(paramInt);
                        }
                    }
                    label770: continue;
                    clear(paramMotionEvent, paramInt);
                }
                break;
            }
        }
    }

    private boolean isDraggingGesture(MotionEvent paramMotionEvent)
    {
        ReceivedPointerTracker localReceivedPointerTracker = this.mReceivedPointerTracker;
        int[] arrayOfInt = this.mTempPointerIds;
        localReceivedPointerTracker.populateActivePointerIds(arrayOfInt);
        int i = paramMotionEvent.findPointerIndex(arrayOfInt[0]);
        int j = paramMotionEvent.findPointerIndex(arrayOfInt[1]);
        float f1 = paramMotionEvent.getX(i);
        float f2 = paramMotionEvent.getY(i);
        float f3 = paramMotionEvent.getX(j);
        float f4 = paramMotionEvent.getY(j);
        float f5 = f1 - localReceivedPointerTracker.getReceivedPointerDownX(i);
        float f6 = f2 - localReceivedPointerTracker.getReceivedPointerDownY(i);
        boolean bool;
        if ((f5 == 0.0F) && (f6 == 0.0F))
            bool = true;
        while (true)
        {
            return bool;
            float f7 = (float)Math.sqrt(f5 * f5 + f6 * f6);
            float f8;
            if (f7 > 0.0F)
            {
                f8 = f5 / f7;
                label139: if (f7 <= 0.0F)
                    break label202;
            }
            float f10;
            float f11;
            label202: for (float f9 = f6 / f7; ; f9 = f6)
            {
                f10 = f3 - localReceivedPointerTracker.getReceivedPointerDownX(j);
                f11 = f4 - localReceivedPointerTracker.getReceivedPointerDownY(j);
                if ((f10 != 0.0F) || (f11 != 0.0F))
                    break label209;
                bool = true;
                break;
                f8 = f5;
                break label139;
            }
            label209: float f12 = (float)Math.sqrt(f10 * f10 + f11 * f11);
            float f13;
            if (f12 > 0.0F)
            {
                f13 = f10 / f12;
                label241: if (f12 <= 0.0F)
                    break label285;
            }
            label285: for (float f14 = f11 / f12; ; f14 = f11)
            {
                if (f8 * f13 + f9 * f14 >= 0.525322F)
                    break label292;
                bool = false;
                break;
                f13 = f10;
                break label241;
            }
            label292: bool = true;
        }
    }

    private void sendActionDownAndUp(MotionEvent paramMotionEvent, int paramInt)
    {
        int i = 1 << paramMotionEvent.getPointerId(paramMotionEvent.getActionIndex());
        sendMotionEvent(paramMotionEvent, 0, i, paramInt);
        sendMotionEvent(paramMotionEvent, 1, i, paramInt);
    }

    private void sendDownForAllActiveNotInjectedPointers(MotionEvent paramMotionEvent, int paramInt)
    {
        ReceivedPointerTracker localReceivedPointerTracker = this.mReceivedPointerTracker;
        InjectedPointerTracker localInjectedPointerTracker = this.mInjectedPointerTracker;
        int i = 0;
        int j = paramMotionEvent.getPointerCount();
        for (int k = 0; k < j; k++)
        {
            int i1 = paramMotionEvent.getPointerId(k);
            if (localInjectedPointerTracker.isInjectedPointerDown(i1))
                i |= 1 << i1;
        }
        int m = 0;
        if (m < j)
        {
            int n = paramMotionEvent.getPointerId(m);
            if (!localReceivedPointerTracker.isActivePointer(n));
            while (true)
            {
                m++;
                break;
                if (!localInjectedPointerTracker.isInjectedPointerDown(n))
                {
                    i |= 1 << n;
                    sendMotionEvent(paramMotionEvent, computeInjectionAction(0, m), i, paramInt);
                }
            }
        }
    }

    private void sendEnterEventsIfNeeded(int paramInt)
    {
        MotionEvent localMotionEvent = this.mInjectedPointerTracker.getLastInjectedHoverEvent();
        if ((localMotionEvent != null) && (localMotionEvent.getActionMasked() == 10))
        {
            int i = localMotionEvent.getPointerIdBits();
            this.mAms.touchExplorationGestureStarted();
            sendMotionEvent(localMotionEvent, 9, i, paramInt);
        }
    }

    private void sendExitEventsIfNeeded(int paramInt)
    {
        MotionEvent localMotionEvent = this.mInjectedPointerTracker.getLastInjectedHoverEvent();
        if ((localMotionEvent != null) && (localMotionEvent.getActionMasked() != 10))
        {
            int i = localMotionEvent.getPointerIdBits();
            this.mAms.touchExplorationGestureEnded();
            sendMotionEvent(localMotionEvent, 10, i, paramInt);
        }
    }

    private void sendMotionEvent(MotionEvent paramMotionEvent, int paramInt1, int paramInt2, int paramInt3)
    {
        paramMotionEvent.setAction(paramInt1);
        Object localObject;
        if (paramInt2 == -1)
        {
            localObject = paramMotionEvent;
            if (paramInt1 != 0)
                break label186;
            long l2 = ((MotionEvent)localObject).getEventTime();
            ((MotionEvent)localObject).setDownTime(l2);
        }
        while (true)
            label32: if (this.mLongPressingPointerId >= 0)
            {
                int j = this.mLongPressingPointerId;
                int k = ((MotionEvent)localObject).findPointerIndex(j);
                int m = ((MotionEvent)localObject).getPointerCount();
                MotionEvent.PointerProperties[] arrayOfPointerProperties = MotionEvent.PointerProperties.createArray(m);
                MotionEvent.PointerCoords[] arrayOfPointerCoords = MotionEvent.PointerCoords.createArray(m);
                int n = 0;
                while (true)
                    if (n < m)
                    {
                        MotionEvent.PointerProperties localPointerProperties = arrayOfPointerProperties[n];
                        ((MotionEvent)localObject).getPointerProperties(n, localPointerProperties);
                        MotionEvent.PointerCoords localPointerCoords1 = arrayOfPointerCoords[n];
                        ((MotionEvent)localObject).getPointerCoords(n, localPointerCoords1);
                        if (n == k)
                        {
                            MotionEvent.PointerCoords localPointerCoords2 = arrayOfPointerCoords[n];
                            localPointerCoords2.x -= this.mLongPressingPointerDeltaX;
                            MotionEvent.PointerCoords localPointerCoords3 = arrayOfPointerCoords[n];
                            localPointerCoords3.y -= this.mLongPressingPointerDeltaY;
                        }
                        n++;
                        continue;
                        localObject = paramMotionEvent.split(paramInt2);
                        break;
                        label186: long l1 = this.mInjectedPointerTracker.getLastInjectedDownEventTime();
                        ((MotionEvent)localObject).setDownTime(l1);
                        break label32;
                    }
                MotionEvent localMotionEvent = MotionEvent.obtain(((MotionEvent)localObject).getDownTime(), ((MotionEvent)localObject).getEventTime(), ((MotionEvent)localObject).getAction(), ((MotionEvent)localObject).getPointerCount(), arrayOfPointerProperties, arrayOfPointerCoords, ((MotionEvent)localObject).getMetaState(), ((MotionEvent)localObject).getButtonState(), 1.0F, 1.0F, ((MotionEvent)localObject).getDeviceId(), ((MotionEvent)localObject).getEdgeFlags(), ((MotionEvent)localObject).getSource(), ((MotionEvent)localObject).getFlags());
                if (localObject != paramMotionEvent)
                    ((MotionEvent)localObject).recycle();
                localObject = localMotionEvent;
            }
        int i = paramInt3 | 0x40000000;
        this.mInputFilter.sendInputEvent((InputEvent)localObject, i);
        this.mInjectedPointerTracker.onMotionEvent((MotionEvent)localObject);
        if (localObject != paramMotionEvent)
            ((MotionEvent)localObject).recycle();
    }

    private void sendMotionEventStripInactivePointers(MotionEvent paramMotionEvent, int paramInt)
    {
        ReceivedPointerTracker localReceivedPointerTracker = this.mReceivedPointerTracker;
        if (paramMotionEvent.getPointerCount() == localReceivedPointerTracker.getActivePointerCount())
            sendMotionEvent(paramMotionEvent, paramMotionEvent.getAction(), -1, paramInt);
        while (true)
        {
            return;
            if ((localReceivedPointerTracker.getActivePointerCount() != 0) || (localReceivedPointerTracker.wasLastReceivedUpPointerActive()))
            {
                int i = paramMotionEvent.getActionMasked();
                int j = paramMotionEvent.getPointerId(paramMotionEvent.getActionIndex());
                if ((i == 2) || (localReceivedPointerTracker.isActiveOrWasLastActiveUpPointer(j)))
                {
                    int k = 0;
                    int m = paramMotionEvent.getPointerCount();
                    for (int n = 0; n < m; n++)
                    {
                        int i1 = paramMotionEvent.getPointerId(n);
                        if (localReceivedPointerTracker.isActiveOrWasLastActiveUpPointer(i1))
                            k |= 1 << i1;
                    }
                    sendMotionEvent(paramMotionEvent, paramMotionEvent.getAction(), k, paramInt);
                }
            }
        }
    }

    private void sendUpForInjectedDownPointers(MotionEvent paramMotionEvent, int paramInt)
    {
        InjectedPointerTracker localInjectedPointerTracker = this.mInjectedPointerTracker;
        int i = 0;
        int j = paramMotionEvent.getPointerCount();
        int k = 0;
        if (k < j)
        {
            int m = paramMotionEvent.getPointerId(k);
            if (!localInjectedPointerTracker.isInjectedPointerDown(m));
            while (true)
            {
                k++;
                break;
                i |= 1 << m;
                sendMotionEvent(paramMotionEvent, computeInjectionAction(1, k), i, paramInt);
            }
        }
    }

    public void clear()
    {
        if (this.mReceivedPointerTracker.getLastReceivedEvent() != null)
            clear(this.mReceivedPointerTracker.getLastReceivedEvent(), 33554432);
    }

    public void clear(MotionEvent paramMotionEvent, int paramInt)
    {
        switch (this.mCurrentState)
        {
        case 3:
        default:
        case 1:
        case 2:
        case 4:
        case 5:
        }
        while (true)
        {
            this.mSendHoverEnterDelayed.remove();
            this.mSendHoverExitDelayed.remove();
            this.mPerformLongPressDelayed.remove();
            this.mExitGestureDetectionModeDelayed.remove();
            this.mReceivedPointerTracker.clear();
            this.mInjectedPointerTracker.clear();
            this.mDoubleTapDetector.clear();
            this.mLongPressingPointerId = -1;
            this.mLongPressingPointerDeltaX = 0;
            this.mLongPressingPointerDeltaY = 0;
            this.mCurrentState = 1;
            return;
            sendExitEventsIfNeeded(paramInt);
            continue;
            this.mDraggingPointerId = -1;
            sendUpForInjectedDownPointers(paramMotionEvent, paramInt);
            continue;
            sendUpForInjectedDownPointers(paramMotionEvent, paramInt);
            continue;
            this.mStrokeBuffer.clear();
        }
    }

    public void onAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        switch (paramAccessibilityEvent.getEventType())
        {
        default:
        case 32:
        case 32768:
        case 128:
        case 256:
        }
        while (true)
        {
            return;
            if (this.mInjectedPointerTracker.mLastInjectedHoverEventForClick != null)
            {
                this.mInjectedPointerTracker.mLastInjectedHoverEventForClick.recycle();
                InjectedPointerTracker.access$302(this.mInjectedPointerTracker, null);
            }
            this.mLastTouchedWindowId = -1;
            continue;
            this.mLastTouchedWindowId = paramAccessibilityEvent.getWindowId();
        }
    }

    public void onMotionEvent(MotionEvent paramMotionEvent, int paramInt)
    {
        this.mReceivedPointerTracker.onMotionEvent(paramMotionEvent);
        switch (this.mCurrentState)
        {
        case 3:
        default:
            throw new IllegalStateException("Illegal state: " + this.mCurrentState);
        case 1:
            handleMotionEventStateTouchExploring(paramMotionEvent, paramInt);
        case 2:
        case 4:
        case 5:
        }
        while (true)
        {
            return;
            handleMotionEventStateDragging(paramMotionEvent, paramInt);
            continue;
            handleMotionEventStateDelegating(paramMotionEvent, paramInt);
            continue;
            handleMotionEventGestureDetecting(paramMotionEvent, paramInt);
        }
    }

    public String toString()
    {
        return "TouchExplorer";
    }

    class ReceivedPointerTracker
    {
        private static final int COEFFICIENT_ACTIVE_POINTER = 2;
        private static final String LOG_TAG_RECEIVED_POINTER_TRACKER = "ReceivedPointerTracker";
        private int mActivePointers;
        private boolean mHasMovingActivePointer;
        private MotionEvent mLastReceivedEvent;
        private boolean mLastReceivedUpPointerActive;
        private long mLastReceivedUpPointerDownTime;
        private float mLastReceivedUpPointerDownX;
        private float mLastReceivedUpPointerDownY;
        private int mLastReceivedUpPointerId;
        private int mPrimaryActivePointerId;
        private final long[] mReceivedPointerDownTime = new long[32];
        private final float[] mReceivedPointerDownX = new float[32];
        private final float[] mReceivedPointerDownY = new float[32];
        private int mReceivedPointersDown;
        private final double mThresholdActivePointer;

        public ReceivedPointerTracker(Context arg2)
        {
            Context localContext;
            this.mThresholdActivePointer = (2 * ViewConfiguration.get(localContext).getScaledTouchSlop());
        }

        private float computePointerDeltaMove(int paramInt, MotionEvent paramMotionEvent)
        {
            int i = paramMotionEvent.getPointerId(paramInt);
            float f1 = paramMotionEvent.getX(paramInt) - this.mReceivedPointerDownX[i];
            float f2 = paramMotionEvent.getY(paramInt) - this.mReceivedPointerDownY[i];
            return (float)Math.hypot(f1, f2);
        }

        private void detectActivePointers(MotionEvent paramMotionEvent)
        {
            int i = 0;
            int j = paramMotionEvent.getPointerCount();
            if (i < j)
            {
                int k = paramMotionEvent.getPointerId(i);
                if ((this.mHasMovingActivePointer) && (isActivePointer(k)));
                while (true)
                {
                    i++;
                    break;
                    if (computePointerDeltaMove(i, paramMotionEvent) > this.mThresholdActivePointer)
                    {
                        this.mActivePointers = (1 << k | this.mActivePointers);
                        this.mHasMovingActivePointer = true;
                    }
                }
            }
        }

        private int findPrimaryActivePointer()
        {
            int i = -1;
            long l1 = 9223372036854775807L;
            int j = 0;
            int k = this.mReceivedPointerDownTime.length;
            while (j < k)
            {
                if (isActivePointer(j))
                {
                    long l2 = this.mReceivedPointerDownTime[j];
                    if (l2 < l1)
                    {
                        l1 = l2;
                        i = j;
                    }
                }
                j++;
            }
            return i;
        }

        private void handleReceivedPointerDown(int paramInt, MotionEvent paramMotionEvent)
        {
            int i = paramMotionEvent.getPointerId(paramInt);
            int j = 1 << i;
            this.mLastReceivedUpPointerId = 0;
            this.mLastReceivedUpPointerDownTime = 0L;
            this.mLastReceivedUpPointerActive = false;
            this.mLastReceivedUpPointerDownX = 0.0F;
            this.mLastReceivedUpPointerDownX = 0.0F;
            this.mReceivedPointersDown = (j | this.mReceivedPointersDown);
            this.mReceivedPointerDownX[i] = paramMotionEvent.getX(paramInt);
            this.mReceivedPointerDownY[i] = paramMotionEvent.getY(paramInt);
            this.mReceivedPointerDownTime[i] = paramMotionEvent.getEventTime();
            if (!this.mHasMovingActivePointer)
            {
                this.mActivePointers = j;
                this.mPrimaryActivePointerId = i;
            }
            while (true)
            {
                return;
                this.mActivePointers = (j | this.mActivePointers);
            }
        }

        private void handleReceivedPointerMove(MotionEvent paramMotionEvent)
        {
            detectActivePointers(paramMotionEvent);
        }

        private void handleReceivedPointerUp(int paramInt, MotionEvent paramMotionEvent)
        {
            int i = paramMotionEvent.getPointerId(paramInt);
            int j = 1 << i;
            this.mLastReceivedUpPointerId = i;
            this.mLastReceivedUpPointerDownTime = getReceivedPointerDownTime(i);
            this.mLastReceivedUpPointerActive = isActivePointer(i);
            this.mLastReceivedUpPointerDownX = this.mReceivedPointerDownX[i];
            this.mLastReceivedUpPointerDownY = this.mReceivedPointerDownY[i];
            this.mReceivedPointersDown &= (j ^ 0xFFFFFFFF);
            this.mActivePointers &= (j ^ 0xFFFFFFFF);
            this.mReceivedPointerDownX[i] = 0.0F;
            this.mReceivedPointerDownY[i] = 0.0F;
            this.mReceivedPointerDownTime[i] = 0L;
            if (this.mActivePointers == 0)
                this.mHasMovingActivePointer = false;
            if (this.mPrimaryActivePointerId == i)
                this.mPrimaryActivePointerId = -1;
        }

        public void clear()
        {
            Arrays.fill(this.mReceivedPointerDownX, 0.0F);
            Arrays.fill(this.mReceivedPointerDownY, 0.0F);
            Arrays.fill(this.mReceivedPointerDownTime, 0L);
            this.mReceivedPointersDown = 0;
            this.mActivePointers = 0;
            this.mPrimaryActivePointerId = 0;
            this.mHasMovingActivePointer = false;
            this.mLastReceivedUpPointerDownTime = 0L;
            this.mLastReceivedUpPointerId = 0;
            this.mLastReceivedUpPointerActive = false;
            this.mLastReceivedUpPointerDownX = 0.0F;
            this.mLastReceivedUpPointerDownY = 0.0F;
        }

        public int getActivePointerCount()
        {
            return Integer.bitCount(this.mActivePointers);
        }

        public int getActivePointers()
        {
            return this.mActivePointers;
        }

        public MotionEvent getLastReceivedEvent()
        {
            return this.mLastReceivedEvent;
        }

        public long getLastReceivedUpPointerDownTime()
        {
            return this.mLastReceivedUpPointerDownTime;
        }

        public float getLastReceivedUpPointerDownX()
        {
            return this.mLastReceivedUpPointerDownX;
        }

        public float getLastReceivedUpPointerDownY()
        {
            return this.mLastReceivedUpPointerDownY;
        }

        public int getLastReceivedUpPointerId()
        {
            return this.mLastReceivedUpPointerId;
        }

        public int getPrimaryActivePointerId()
        {
            if (this.mPrimaryActivePointerId == -1)
                this.mPrimaryActivePointerId = findPrimaryActivePointer();
            return this.mPrimaryActivePointerId;
        }

        public int getReceivedPointerDownCount()
        {
            return Integer.bitCount(this.mReceivedPointersDown);
        }

        public long getReceivedPointerDownTime(int paramInt)
        {
            return this.mReceivedPointerDownTime[paramInt];
        }

        public float getReceivedPointerDownX(int paramInt)
        {
            return this.mReceivedPointerDownX[paramInt];
        }

        public float getReceivedPointerDownY(int paramInt)
        {
            return this.mReceivedPointerDownY[paramInt];
        }

        public boolean isActiveOrWasLastActiveUpPointer(int paramInt)
        {
            if ((isActivePointer(paramInt)) || ((this.mLastReceivedUpPointerId == paramInt) && (this.mLastReceivedUpPointerActive)));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public boolean isActivePointer(int paramInt)
        {
            int i = 1;
            if ((i << paramInt & this.mActivePointers) != 0);
            while (true)
            {
                return i;
                i = 0;
            }
        }

        public boolean isReceivedPointerDown(int paramInt)
        {
            int i = 1;
            if ((i << paramInt & this.mReceivedPointersDown) != 0);
            while (true)
            {
                return i;
                i = 0;
            }
        }

        public void onMotionEvent(MotionEvent paramMotionEvent)
        {
            if (this.mLastReceivedEvent != null)
                this.mLastReceivedEvent.recycle();
            this.mLastReceivedEvent = MotionEvent.obtain(paramMotionEvent);
            switch (paramMotionEvent.getActionMasked())
            {
            case 3:
            case 4:
            default:
            case 0:
            case 5:
            case 2:
            case 1:
            case 6:
            }
            while (true)
            {
                return;
                handleReceivedPointerDown(paramMotionEvent.getActionIndex(), paramMotionEvent);
                continue;
                handleReceivedPointerDown(paramMotionEvent.getActionIndex(), paramMotionEvent);
                continue;
                handleReceivedPointerMove(paramMotionEvent);
                continue;
                handleReceivedPointerUp(paramMotionEvent.getActionIndex(), paramMotionEvent);
                continue;
                handleReceivedPointerUp(paramMotionEvent.getActionIndex(), paramMotionEvent);
            }
        }

        public void populateActivePointerIds(int[] paramArrayOfInt)
        {
            int i = 0;
            int j = this.mActivePointers;
            while (j != 0)
            {
                int k = Integer.numberOfTrailingZeros(j);
                j &= (0xFFFFFFFF ^ 1 << k);
                paramArrayOfInt[i] = k;
                i++;
            }
        }

        public String toString()
        {
            StringBuilder localStringBuilder = new StringBuilder();
            localStringBuilder.append("=========================");
            localStringBuilder.append("\nDown pointers #");
            localStringBuilder.append(getReceivedPointerDownCount());
            localStringBuilder.append(" [ ");
            for (int i = 0; i < 32; i++)
                if (isReceivedPointerDown(i))
                {
                    localStringBuilder.append(i);
                    localStringBuilder.append(" ");
                }
            localStringBuilder.append("]");
            localStringBuilder.append("\nActive pointers #");
            localStringBuilder.append(getActivePointerCount());
            localStringBuilder.append(" [ ");
            for (int j = 0; j < 32; j++)
                if (isActivePointer(j))
                {
                    localStringBuilder.append(j);
                    localStringBuilder.append(" ");
                }
            localStringBuilder.append("]");
            localStringBuilder.append("\nPrimary active pointer id [ ");
            localStringBuilder.append(getPrimaryActivePointerId());
            localStringBuilder.append(" ]");
            localStringBuilder.append("\n=========================");
            return localStringBuilder.toString();
        }

        public boolean wasLastReceivedUpPointerActive()
        {
            return this.mLastReceivedUpPointerActive;
        }
    }

    class InjectedPointerTracker
    {
        private static final String LOG_TAG_INJECTED_POINTER_TRACKER = "InjectedPointerTracker";
        private int mInjectedPointersDown;
        private long mLastInjectedDownEventTime;
        private MotionEvent mLastInjectedHoverEvent;
        private MotionEvent mLastInjectedHoverEventForClick;

        InjectedPointerTracker()
        {
        }

        public void clear()
        {
            this.mInjectedPointersDown = 0;
        }

        public int getInjectedPointerDownCount()
        {
            return Integer.bitCount(this.mInjectedPointersDown);
        }

        public int getInjectedPointersDown()
        {
            return this.mInjectedPointersDown;
        }

        public long getLastInjectedDownEventTime()
        {
            return this.mLastInjectedDownEventTime;
        }

        public MotionEvent getLastInjectedHoverEvent()
        {
            return this.mLastInjectedHoverEvent;
        }

        public MotionEvent getLastInjectedHoverEventForClick()
        {
            return this.mLastInjectedHoverEventForClick;
        }

        public boolean isInjectedPointerDown(int paramInt)
        {
            int i = 1;
            if ((i << paramInt & this.mInjectedPointersDown) != 0);
            while (true)
            {
                return i;
                i = 0;
            }
        }

        public void onMotionEvent(MotionEvent paramMotionEvent)
        {
            switch (paramMotionEvent.getActionMasked())
            {
            case 2:
            case 3:
            case 4:
            case 8:
            default:
            case 0:
            case 5:
            case 1:
            case 6:
            case 7:
            case 9:
            case 10:
            }
            while (true)
            {
                return;
                this.mInjectedPointersDown = (1 << paramMotionEvent.getPointerId(paramMotionEvent.getActionIndex()) | this.mInjectedPointersDown);
                this.mLastInjectedDownEventTime = paramMotionEvent.getDownTime();
                continue;
                int i = 1 << paramMotionEvent.getPointerId(paramMotionEvent.getActionIndex());
                this.mInjectedPointersDown &= (i ^ 0xFFFFFFFF);
                if (this.mInjectedPointersDown == 0)
                {
                    this.mLastInjectedDownEventTime = 0L;
                    continue;
                    if (this.mLastInjectedHoverEvent != null)
                        this.mLastInjectedHoverEvent.recycle();
                    this.mLastInjectedHoverEvent = MotionEvent.obtain(paramMotionEvent);
                    if (this.mLastInjectedHoverEventForClick != null)
                        this.mLastInjectedHoverEventForClick.recycle();
                    this.mLastInjectedHoverEventForClick = MotionEvent.obtain(paramMotionEvent);
                }
            }
        }

        public String toString()
        {
            StringBuilder localStringBuilder = new StringBuilder();
            localStringBuilder.append("=========================");
            localStringBuilder.append("\nDown pointers #");
            localStringBuilder.append(Integer.bitCount(this.mInjectedPointersDown));
            localStringBuilder.append(" [ ");
            for (int i = 0; i < 32; i++)
                if ((i & this.mInjectedPointersDown) != 0)
                {
                    localStringBuilder.append(i);
                    localStringBuilder.append(" ");
                }
            localStringBuilder.append("]");
            localStringBuilder.append("\n=========================");
            return localStringBuilder.toString();
        }
    }

    class SendHoverDelayed
        implements Runnable
    {
        private final String LOG_TAG_SEND_HOVER_DELAYED = SendHoverDelayed.class.getName();
        private final boolean mGestureStarted;
        private final int mHoverAction;
        private int mPointerIdBits;
        private int mPolicyFlags;
        private MotionEvent mPrototype;

        public SendHoverDelayed(int paramBoolean, boolean arg3)
        {
            this.mHoverAction = paramBoolean;
            boolean bool;
            this.mGestureStarted = bool;
        }

        private void clear()
        {
            if (!isPending());
            while (true)
            {
                return;
                this.mPrototype.recycle();
                this.mPrototype = null;
                this.mPointerIdBits = -1;
                this.mPolicyFlags = 0;
            }
        }

        private boolean isPending()
        {
            if (this.mPrototype != null);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void forceSendAndRemove()
        {
            if (isPending())
            {
                run();
                remove();
            }
        }

        public float getX()
        {
            if (isPending());
            for (float f = this.mPrototype.getX(); ; f = 0.0F)
                return f;
        }

        public float getY()
        {
            if (isPending());
            for (float f = this.mPrototype.getY(); ; f = 0.0F)
                return f;
        }

        public void post(MotionEvent paramMotionEvent, int paramInt1, int paramInt2)
        {
            remove();
            this.mPrototype = MotionEvent.obtain(paramMotionEvent);
            this.mPointerIdBits = paramInt1;
            this.mPolicyFlags = paramInt2;
            TouchExplorer.this.mHandler.postDelayed(this, TouchExplorer.this.mDetermineUserIntentTimeout);
        }

        public void remove()
        {
            TouchExplorer.this.mHandler.removeCallbacks(this);
            clear();
        }

        public void run()
        {
            if (this.mGestureStarted)
                TouchExplorer.this.mAms.touchExplorationGestureStarted();
            while (true)
            {
                TouchExplorer.this.sendMotionEvent(this.mPrototype, this.mHoverAction, this.mPointerIdBits, this.mPolicyFlags);
                clear();
                return;
                TouchExplorer.this.mAms.touchExplorationGestureEnded();
            }
        }
    }

    private final class PerformLongPressDelayed
        implements Runnable
    {
        private MotionEvent mEvent;
        private int mPolicyFlags;

        private PerformLongPressDelayed()
        {
        }

        private void clear()
        {
            if (!isPenidng());
            while (true)
            {
                return;
                this.mEvent.recycle();
                this.mEvent = null;
                this.mPolicyFlags = 0;
            }
        }

        private boolean isPenidng()
        {
            if (this.mEvent != null);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void post(MotionEvent paramMotionEvent, int paramInt)
        {
            this.mEvent = MotionEvent.obtain(paramMotionEvent);
            this.mPolicyFlags = paramInt;
            TouchExplorer.this.mHandler.postDelayed(this, ViewConfiguration.getLongPressTimeout());
        }

        public void remove()
        {
            if (isPenidng())
            {
                TouchExplorer.this.mHandler.removeCallbacks(this);
                clear();
            }
        }

        public void run()
        {
            if (TouchExplorer.this.mReceivedPointerTracker.getActivePointerCount() == 0);
            int i;
            int j;
            MotionEvent localMotionEvent;
            Rect localRect3;
            do
            {
                return;
                i = this.mEvent.getPointerId(this.mEvent.getActionIndex());
                j = this.mEvent.findPointerIndex(i);
                localMotionEvent = TouchExplorer.this.mInjectedPointerTracker.getLastInjectedHoverEventForClick();
                if (localMotionEvent != null)
                    break;
                localRect3 = TouchExplorer.this.mTempRect;
            }
            while (!TouchExplorer.this.mAms.getAccessibilityFocusBoundsInActiveWindow(localRect3));
            int m = localRect3.centerX();
            int n = localRect3.centerY();
            while (true)
            {
                TouchExplorer.access$2002(TouchExplorer.this, i);
                TouchExplorer.access$2102(TouchExplorer.this, (int)this.mEvent.getX(j) - m);
                TouchExplorer.access$2202(TouchExplorer.this, (int)this.mEvent.getY(j) - n);
                TouchExplorer.this.sendExitEventsIfNeeded(this.mPolicyFlags);
                TouchExplorer.access$2302(TouchExplorer.this, 4);
                TouchExplorer.this.sendDownForAllActiveNotInjectedPointers(this.mEvent, this.mPolicyFlags);
                clear();
                break;
                int k = localMotionEvent.getActionIndex();
                m = (int)localMotionEvent.getX(k);
                n = (int)localMotionEvent.getY(k);
                Rect localRect1 = TouchExplorer.this.mTempRect;
                if (TouchExplorer.this.mLastTouchedWindowId == TouchExplorer.this.mAms.getActiveWindowId())
                {
                    TouchExplorer.this.mAms.getActiveWindowBounds(localRect1);
                    if (localRect1.contains(m, n))
                    {
                        Rect localRect2 = TouchExplorer.this.mTempRect;
                        if ((TouchExplorer.this.mAms.getAccessibilityFocusBoundsInActiveWindow(localRect2)) && (!localRect2.contains(m, n)))
                        {
                            m = localRect2.centerX();
                            n = localRect2.centerY();
                        }
                    }
                }
            }
        }
    }

    private final class ExitGestureDetectionModeDelayed
        implements Runnable
    {
        private ExitGestureDetectionModeDelayed()
        {
        }

        public void post()
        {
            TouchExplorer.this.mHandler.postDelayed(this, 2000L);
        }

        public void remove()
        {
            TouchExplorer.this.mHandler.removeCallbacks(this);
        }

        public void run()
        {
            TouchExplorer.this.clear();
        }
    }

    private class DoubleTapDetector
    {
        private MotionEvent mDownEvent;
        private MotionEvent mFirstTapEvent;

        private DoubleTapDetector()
        {
        }

        private boolean eventsWithinTimeoutAndDistance(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, int paramInt1, int paramInt2)
        {
            boolean bool = false;
            if (isTimedOut(paramMotionEvent1, paramMotionEvent2, paramInt1));
            while (true)
            {
                return bool;
                int i = paramMotionEvent1.getActionIndex();
                int j = paramMotionEvent2.getActionIndex();
                float f1 = paramMotionEvent2.getX(j) - paramMotionEvent1.getX(i);
                float f2 = paramMotionEvent2.getY(j) - paramMotionEvent1.getY(i);
                if (Math.hypot(f1, f2) < paramInt2)
                    bool = true;
            }
        }

        private boolean isDoubleTap(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2)
        {
            return eventsWithinTimeoutAndDistance(paramMotionEvent1, paramMotionEvent2, TouchExplorer.this.mDoubleTapTimeout, TouchExplorer.this.mDoubleTapSlop);
        }

        private boolean isSamePointerContext(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2)
        {
            if ((paramMotionEvent1.getPointerIdBits() == paramMotionEvent2.getPointerIdBits()) && (paramMotionEvent1.getPointerId(paramMotionEvent1.getActionIndex()) == paramMotionEvent2.getPointerId(paramMotionEvent2.getActionIndex())));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        private boolean isTimedOut(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, int paramInt)
        {
            if (paramMotionEvent2.getEventTime() - paramMotionEvent1.getEventTime() >= paramInt);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void clear()
        {
            if (this.mDownEvent != null)
            {
                this.mDownEvent.recycle();
                this.mDownEvent = null;
            }
            if (this.mFirstTapEvent != null)
            {
                this.mFirstTapEvent.recycle();
                this.mFirstTapEvent = null;
            }
        }

        public boolean firstTapDetected()
        {
            if ((this.mFirstTapEvent != null) && (SystemClock.uptimeMillis() - this.mFirstTapEvent.getEventTime() < TouchExplorer.this.mDoubleTapTimeout));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public boolean isTap(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2)
        {
            return eventsWithinTimeoutAndDistance(paramMotionEvent1, paramMotionEvent2, TouchExplorer.this.mTapTimeout, TouchExplorer.this.mTouchSlop);
        }

        public void onDoubleTap(MotionEvent paramMotionEvent, int paramInt)
        {
            if (paramMotionEvent.getPointerCount() > 2);
            int i;
            MotionEvent localMotionEvent1;
            Rect localRect3;
            do
            {
                return;
                TouchExplorer.this.mSendHoverEnterDelayed.remove();
                TouchExplorer.this.mSendHoverExitDelayed.remove();
                TouchExplorer.this.mPerformLongPressDelayed.remove();
                TouchExplorer.this.sendExitEventsIfNeeded(paramInt);
                i = paramMotionEvent.findPointerIndex(paramMotionEvent.getPointerId(paramMotionEvent.getActionIndex()));
                localMotionEvent1 = TouchExplorer.this.mInjectedPointerTracker.getLastInjectedHoverEventForClick();
                if (localMotionEvent1 != null)
                    break;
                localRect3 = TouchExplorer.this.mTempRect;
            }
            while (!TouchExplorer.this.mAms.getAccessibilityFocusBoundsInActiveWindow(localRect3));
            int k = localRect3.centerX();
            int m = localRect3.centerY();
            while (true)
            {
                MotionEvent.PointerProperties[] arrayOfPointerProperties = new MotionEvent.PointerProperties[1];
                arrayOfPointerProperties[0] = new MotionEvent.PointerProperties();
                paramMotionEvent.getPointerProperties(i, arrayOfPointerProperties[0]);
                MotionEvent.PointerCoords[] arrayOfPointerCoords = new MotionEvent.PointerCoords[1];
                arrayOfPointerCoords[0] = new MotionEvent.PointerCoords();
                arrayOfPointerCoords[0].x = k;
                arrayOfPointerCoords[0].y = m;
                MotionEvent localMotionEvent2 = MotionEvent.obtain(paramMotionEvent.getDownTime(), paramMotionEvent.getEventTime(), 0, 1, arrayOfPointerProperties, arrayOfPointerCoords, 0, 0, 1.0F, 1.0F, paramMotionEvent.getDeviceId(), 0, paramMotionEvent.getSource(), paramMotionEvent.getFlags());
                TouchExplorer.this.sendActionDownAndUp(localMotionEvent2, paramInt);
                localMotionEvent2.recycle();
                break;
                int j = localMotionEvent1.getActionIndex();
                k = (int)localMotionEvent1.getX(j);
                m = (int)localMotionEvent1.getY(j);
                Rect localRect1 = TouchExplorer.this.mTempRect;
                if (TouchExplorer.this.mLastTouchedWindowId == TouchExplorer.this.mAms.getActiveWindowId())
                {
                    TouchExplorer.this.mAms.getActiveWindowBounds(localRect1);
                    if (localRect1.contains(k, m))
                    {
                        Rect localRect2 = TouchExplorer.this.mTempRect;
                        if ((TouchExplorer.this.mAms.getAccessibilityFocusBoundsInActiveWindow(localRect2)) && (!localRect2.contains(k, m)))
                        {
                            k = localRect2.centerX();
                            m = localRect2.centerY();
                        }
                    }
                }
            }
        }

        public void onMotionEvent(MotionEvent paramMotionEvent, int paramInt)
        {
            switch (paramMotionEvent.getActionMasked())
            {
            case 2:
            case 3:
            case 4:
            default:
            case 0:
            case 5:
            case 1:
            case 6:
            }
            while (true)
            {
                return;
                if ((this.mFirstTapEvent != null) && (!isSamePointerContext(this.mFirstTapEvent, paramMotionEvent)))
                    clear();
                this.mDownEvent = MotionEvent.obtain(paramMotionEvent);
                continue;
                if (this.mDownEvent != null)
                    if (!isSamePointerContext(this.mDownEvent, paramMotionEvent))
                    {
                        clear();
                    }
                    else
                    {
                        if (!isTap(this.mDownEvent, paramMotionEvent))
                            break label242;
                        if ((this.mFirstTapEvent == null) || (isTimedOut(this.mFirstTapEvent, paramMotionEvent, TouchExplorer.this.mDoubleTapTimeout)))
                        {
                            this.mFirstTapEvent = MotionEvent.obtain(paramMotionEvent);
                            this.mDownEvent.recycle();
                            this.mDownEvent = null;
                        }
                        else
                        {
                            if (!isDoubleTap(this.mFirstTapEvent, paramMotionEvent))
                                break;
                            onDoubleTap(paramMotionEvent, paramInt);
                            this.mFirstTapEvent.recycle();
                            this.mFirstTapEvent = null;
                            this.mDownEvent.recycle();
                            this.mDownEvent = null;
                        }
                    }
            }
            this.mFirstTapEvent.recycle();
            this.mFirstTapEvent = null;
            while (true)
            {
                this.mDownEvent.recycle();
                this.mDownEvent = null;
                break;
                label242: if (this.mFirstTapEvent != null)
                {
                    this.mFirstTapEvent.recycle();
                    this.mFirstTapEvent = null;
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.accessibility.TouchExplorer
 * JD-Core Version:        0.6.2
 */