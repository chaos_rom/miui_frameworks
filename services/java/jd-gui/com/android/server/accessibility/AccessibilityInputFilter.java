package com.android.server.accessibility;

import android.content.Context;
import android.os.PowerManager;
import android.view.InputEvent;
import android.view.MotionEvent;
import android.view.accessibility.AccessibilityEvent;
import com.android.server.input.InputFilter;

public class AccessibilityInputFilter extends InputFilter
{
    private static final boolean DEBUG = false;
    private static final String TAG = "AccessibilityInputFilter";
    private final AccessibilityManagerService mAms;
    private final Context mContext;
    private final PowerManager mPm;
    private TouchExplorer mTouchExplorer;
    private int mTouchscreenSourceDeviceId;

    public AccessibilityInputFilter(Context paramContext, AccessibilityManagerService paramAccessibilityManagerService)
    {
        super(paramContext.getMainLooper());
        this.mContext = paramContext;
        this.mAms = paramAccessibilityManagerService;
        this.mPm = ((PowerManager)paramContext.getSystemService("power"));
    }

    public void onAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        if (this.mTouchExplorer != null)
            this.mTouchExplorer.onAccessibilityEvent(paramAccessibilityEvent);
    }

    public void onInputEvent(InputEvent paramInputEvent, int paramInt)
    {
        MotionEvent localMotionEvent;
        if (paramInputEvent.getSource() == 4098)
        {
            localMotionEvent = (MotionEvent)paramInputEvent;
            int i = paramInputEvent.getDeviceId();
            if (this.mTouchscreenSourceDeviceId != i)
            {
                this.mTouchscreenSourceDeviceId = i;
                this.mTouchExplorer.clear(localMotionEvent, paramInt);
            }
            if ((0x40000000 & paramInt) != 0)
            {
                this.mPm.userActivity(paramInputEvent.getEventTime(), false);
                this.mTouchExplorer.onMotionEvent(localMotionEvent, paramInt);
            }
        }
        while (true)
        {
            return;
            this.mTouchExplorer.clear(localMotionEvent, paramInt);
            continue;
            super.onInputEvent(paramInputEvent, paramInt);
        }
    }

    public void onInstalled()
    {
        this.mTouchExplorer = new TouchExplorer(this, this.mContext, this.mAms);
        super.onInstalled();
    }

    public void onUninstalled()
    {
        this.mTouchExplorer.clear();
        super.onUninstalled();
    }

    public static abstract interface Explorer
    {
        public abstract void clear();

        public abstract void clear(MotionEvent paramMotionEvent, int paramInt);

        public abstract void onMotionEvent(MotionEvent paramMotionEvent, int paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.accessibility.AccessibilityInputFilter
 * JD-Core Version:        0.6.2
 */