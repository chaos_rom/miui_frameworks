package com.android.server.accessibility;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.accessibilityservice.IAccessibilityServiceClient;
import android.accessibilityservice.IAccessibilityServiceClient.Stub;
import android.accessibilityservice.IAccessibilityServiceConnection.Stub;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.StatusBarManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.database.ContentObserver;
import android.graphics.Rect;
import android.hardware.input.InputManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.provider.Settings.Secure;
import android.text.TextUtils.SimpleStringSplitter;
import android.util.Slog;
import android.util.SparseArray;
import android.view.IWindow;
import android.view.KeyEvent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityInteractionClient;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.IAccessibilityInteractionConnection;
import android.view.accessibility.IAccessibilityManager.Stub;
import android.view.accessibility.IAccessibilityManagerClient;
import com.android.internal.content.PackageMonitor;
import com.android.internal.statusbar.IStatusBarService;
import com.android.internal.statusbar.IStatusBarService.Stub;
import com.android.server.wm.WindowManagerService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.xmlpull.v1.XmlPullParserException;

public class AccessibilityManagerService extends IAccessibilityManager.Stub
{
    private static final char COMPONENT_NAME_SEPARATOR = ':';
    private static final boolean DEBUG = false;
    private static final String FUNCTION_REGISTER_UI_TEST_AUTOMATION_SERVICE = "registerUiTestAutomationService";
    private static final String LOG_TAG = "AccessibilityManagerService";
    private static final int MSG_SEND_ACCESSIBILITY_EVENT_TO_INPUT_FILTER = 3;
    private static final int MSG_SHOW_ENABLE_TOUCH_EXPLORATION_DIALOG = 1;
    private static final int MSG_TOGGLE_TOUCH_EXPLORATION = 2;
    private static final int OWN_PROCESS_ID = Process.myPid();
    private static int sIdCounter = 0;
    private static int sNextWindowId;
    final List<IAccessibilityManagerClient> mClients = new ArrayList();
    final Map<ComponentName, Service> mComponentNameToServiceMap = new HashMap();
    final Context mContext;
    private AlertDialog mEnableTouchExplorationDialog;
    private final Set<ComponentName> mEnabledServices = new HashSet();
    private final List<AccessibilityServiceInfo> mEnabledServicesForFeedbackTempList = new ArrayList();
    private int mHandledFeedbackTypes = 0;
    private boolean mHasInputFilter;
    private AccessibilityInputFilter mInputFilter;
    private final List<AccessibilityServiceInfo> mInstalledServices = new ArrayList();
    private boolean mIsAccessibilityEnabled;
    private boolean mIsTouchExplorationEnabled;
    final Object mLock = new Object();
    private final MainHanler mMainHandler;
    private PackageManager mPackageManager;
    private Service mQueryBridge;
    private final SecurityPolicy mSecurityPolicy;
    final List<Service> mServices = new ArrayList();
    private final TextUtils.SimpleStringSplitter mStringColonSplitter = new TextUtils.SimpleStringSplitter(':');
    private boolean mTouchExplorationGestureEnded;
    private boolean mTouchExplorationGestureStarted;
    private final Set<ComponentName> mTouchExplorationGrantedServices = new HashSet();
    private Service mUiAutomationService;
    private final SparseArray<AccessibilityConnectionWrapper> mWindowIdToInteractionConnectionWrapperMap = new SparseArray();
    private final SparseArray<IBinder> mWindowIdToWindowTokenMap = new SparseArray();
    private final WindowManagerService mWindowManagerService;

    public AccessibilityManagerService(Context paramContext)
    {
        this.mContext = paramContext;
        this.mPackageManager = this.mContext.getPackageManager();
        this.mWindowManagerService = ((WindowManagerService)ServiceManager.getService("window"));
        this.mSecurityPolicy = new SecurityPolicy();
        this.mMainHandler = new MainHanler(null);
        registerPackageChangeAndBootCompletedBroadcastReceiver();
        registerSettingsContentObservers();
    }

    private boolean canDispathEventLocked(Service paramService, AccessibilityEvent paramAccessibilityEvent, int paramInt)
    {
        boolean bool = false;
        if (!paramService.isConfigured());
        while (true)
        {
            return bool;
            if ((paramAccessibilityEvent.isImportantForAccessibility()) || (paramService.mIncludeNotImportantViews))
            {
                int i = paramAccessibilityEvent.getEventType();
                if ((i & paramService.mEventTypes) == i)
                {
                    Set localSet = paramService.mPackageNames;
                    CharSequence localCharSequence = paramAccessibilityEvent.getPackageName();
                    if ((localSet.isEmpty()) || (localSet.contains(localCharSequence)))
                    {
                        int j = paramService.mFeedbackType;
                        if (((paramInt & j) != j) || (j == 16))
                            bool = true;
                    }
                }
            }
        }
    }

    private Service getQueryBridge()
    {
        if (this.mQueryBridge == null)
            this.mQueryBridge = new Service(null, new AccessibilityServiceInfo(), true);
        return this.mQueryBridge;
    }

    private int getState()
    {
        int i = 0;
        if (this.mIsAccessibilityEnabled)
            i = 0x0 | 0x1;
        if ((this.mIsAccessibilityEnabled) && (this.mIsTouchExplorationEnabled))
            i |= 2;
        return i;
    }

    private void handleAccessibilityEnabledSettingChangedLocked()
    {
        int i = 1;
        if (Settings.Secure.getInt(this.mContext.getContentResolver(), "accessibility_enabled", 0) == i)
        {
            this.mIsAccessibilityEnabled = i;
            if (!this.mIsAccessibilityEnabled)
                break label42;
            manageServicesLocked();
        }
        while (true)
        {
            return;
            i = 0;
            break;
            label42: unbindAllServicesLocked();
        }
    }

    private void handleTouchExplorationEnabledSettingChangedLocked()
    {
        int i = 1;
        if (Settings.Secure.getInt(this.mContext.getContentResolver(), "touch_exploration_enabled", 0) == i);
        while (true)
        {
            this.mIsTouchExplorationEnabled = i;
            return;
            i = 0;
        }
    }

    private void manageServicesLocked()
    {
        int i = updateServicesStateLocked(this.mInstalledServices, this.mEnabledServices);
        if ((this.mIsAccessibilityEnabled) && (i == 0))
            Settings.Secure.putInt(this.mContext.getContentResolver(), "accessibility_enabled", 0);
    }

    private void notifyAccessibilityServicesDelayedLocked(AccessibilityEvent paramAccessibilityEvent, boolean paramBoolean)
    {
        int i = 0;
        try
        {
            int j = this.mServices.size();
            while (i < j)
            {
                Service localService = (Service)this.mServices.get(i);
                if ((localService.mIsDefault == paramBoolean) && (canDispathEventLocked(localService, paramAccessibilityEvent, this.mHandledFeedbackTypes)))
                {
                    this.mHandledFeedbackTypes |= localService.mFeedbackType;
                    localService.notifyAccessibilityEvent(paramAccessibilityEvent);
                }
                i++;
            }
        }
        catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
        {
        }
    }

    private boolean notifyGestureLocked(int paramInt, boolean paramBoolean)
    {
        int i = -1 + this.mServices.size();
        if (i >= 0)
        {
            Service localService = (Service)this.mServices.get(i);
            if ((localService.mReqeustTouchExplorationMode) && (localService.mIsDefault == paramBoolean))
                localService.notifyGesture(paramInt);
        }
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            i--;
            break;
        }
    }

    private void persistComponentNamesToSettingLocked(String paramString, Set<ComponentName> paramSet)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        Iterator localIterator = paramSet.iterator();
        while (localIterator.hasNext())
        {
            ComponentName localComponentName = (ComponentName)localIterator.next();
            if (localStringBuilder.length() > 0)
                localStringBuilder.append(':');
            localStringBuilder.append(localComponentName.flattenToShortString());
        }
        Settings.Secure.putString(this.mContext.getContentResolver(), paramString, localStringBuilder.toString());
    }

    private void populateAccessibilityServiceListLocked()
    {
        this.mInstalledServices.clear();
        List localList = this.mPackageManager.queryIntentServices(new Intent("android.accessibilityservice.AccessibilityService"), 132);
        int i = 0;
        int j = localList.size();
        if (i < j)
        {
            ResolveInfo localResolveInfo = (ResolveInfo)localList.get(i);
            ServiceInfo localServiceInfo = localResolveInfo.serviceInfo;
            if ((localServiceInfo.applicationInfo.targetSdkVersion >= 16) && (!"android.permission.BIND_ACCESSIBILITY_SERVICE".equals(localServiceInfo.permission)))
                Slog.w("AccessibilityManagerService", "Skipping accessibilty service " + new ComponentName(localServiceInfo.packageName, localServiceInfo.name).flattenToShortString() + ": it does not require the permission " + "android.permission.BIND_ACCESSIBILITY_SERVICE");
            while (true)
            {
                i++;
                break;
                try
                {
                    AccessibilityServiceInfo localAccessibilityServiceInfo = new AccessibilityServiceInfo(localResolveInfo, this.mContext);
                    this.mInstalledServices.add(localAccessibilityServiceInfo);
                }
                catch (XmlPullParserException localXmlPullParserException)
                {
                    Slog.e("AccessibilityManagerService", "Error while initializing AccessibilityServiceInfo", localXmlPullParserException);
                }
                catch (IOException localIOException)
                {
                    Slog.e("AccessibilityManagerService", "Error while initializing AccessibilityServiceInfo", localIOException);
                }
            }
        }
    }

    private void populateComponentNamesFromSettingLocked(String paramString, Set<ComponentName> paramSet)
    {
        paramSet.clear();
        String str1 = Settings.Secure.getString(this.mContext.getContentResolver(), paramString);
        if (str1 != null)
        {
            TextUtils.SimpleStringSplitter localSimpleStringSplitter = this.mStringColonSplitter;
            localSimpleStringSplitter.setString(str1);
            while (localSimpleStringSplitter.hasNext())
            {
                String str2 = localSimpleStringSplitter.next();
                if ((str2 != null) && (str2.length() > 0))
                {
                    ComponentName localComponentName = ComponentName.unflattenFromString(str2);
                    if (localComponentName != null)
                        paramSet.add(localComponentName);
                }
            }
        }
    }

    private void populateEnabledAccessibilityServicesLocked()
    {
        populateComponentNamesFromSettingLocked("enabled_accessibility_services", this.mEnabledServices);
    }

    private void populateTouchExplorationGrantedAccessibilityServicesLocked()
    {
        populateComponentNamesFromSettingLocked("touch_exploration_granted_accessibility_services", this.mTouchExplorationGrantedServices);
    }

    private void registerPackageChangeAndBootCompletedBroadcastReceiver()
    {
        Context localContext = this.mContext;
        PackageMonitor local1 = new PackageMonitor()
        {
            public boolean onHandleForceStop(Intent paramAnonymousIntent, String[] paramAnonymousArrayOfString, int paramAnonymousInt, boolean paramAnonymousBoolean)
            {
                while (true)
                {
                    int j;
                    boolean bool;
                    synchronized (AccessibilityManagerService.this.mLock)
                    {
                        Iterator localIterator = AccessibilityManagerService.this.mEnabledServices.iterator();
                        if (localIterator.hasNext())
                        {
                            String str = ((ComponentName)localIterator.next()).getPackageName();
                            int i = paramAnonymousArrayOfString.length;
                            j = 0;
                            if (j >= i)
                                continue;
                            if (!str.equals(paramAnonymousArrayOfString[j]))
                                break label137;
                            if (!paramAnonymousBoolean)
                            {
                                bool = true;
                                break label134;
                            }
                            localIterator.remove();
                            AccessibilityManagerService.this.persistComponentNamesToSettingLocked("enabled_accessibility_services", AccessibilityManagerService.this.mEnabledServices);
                            break label137;
                        }
                        bool = false;
                    }
                    label134: return bool;
                    label137: j++;
                }
            }

            public void onPackageRemoved(String paramAnonymousString, int paramAnonymousInt)
            {
                synchronized (AccessibilityManagerService.this.mLock)
                {
                    Iterator localIterator = AccessibilityManagerService.this.mEnabledServices.iterator();
                    while (localIterator.hasNext())
                    {
                        ComponentName localComponentName = (ComponentName)localIterator.next();
                        if (localComponentName.getPackageName().equals(paramAnonymousString))
                        {
                            localIterator.remove();
                            AccessibilityManagerService.this.persistComponentNamesToSettingLocked("enabled_accessibility_services", AccessibilityManagerService.this.mEnabledServices);
                            AccessibilityManagerService.this.mTouchExplorationGrantedServices.remove(localComponentName);
                            AccessibilityManagerService.this.persistComponentNamesToSettingLocked("touch_exploration_granted_accessibility_services", AccessibilityManagerService.this.mEnabledServices);
                            return;
                        }
                    }
                }
            }

            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                if (paramAnonymousIntent.getAction() == "android.intent.action.BOOT_COMPLETED")
                    synchronized (AccessibilityManagerService.this.mLock)
                    {
                        if (AccessibilityManagerService.this.mUiAutomationService == null)
                        {
                            AccessibilityManagerService.this.populateAccessibilityServiceListLocked();
                            AccessibilityManagerService.this.populateEnabledAccessibilityServicesLocked();
                            AccessibilityManagerService.this.populateTouchExplorationGrantedAccessibilityServicesLocked();
                            AccessibilityManagerService.this.handleAccessibilityEnabledSettingChangedLocked();
                            AccessibilityManagerService.this.handleTouchExplorationEnabledSettingChangedLocked();
                            AccessibilityManagerService.this.updateInputFilterLocked();
                            AccessibilityManagerService.this.sendStateToClientsLocked();
                        }
                    }
                super.onReceive(paramAnonymousContext, paramAnonymousIntent);
            }

            public void onSomePackagesChanged()
            {
                synchronized (AccessibilityManagerService.this.mLock)
                {
                    if (AccessibilityManagerService.this.mUiAutomationService == null)
                    {
                        AccessibilityManagerService.this.populateAccessibilityServiceListLocked();
                        AccessibilityManagerService.this.manageServicesLocked();
                    }
                    return;
                }
            }
        };
        local1.register(localContext, null, true);
        IntentFilter localIntentFilter = new IntentFilter("android.intent.action.BOOT_COMPLETED");
        this.mContext.registerReceiver(local1, localIntentFilter, null, local1.getRegisteredHandler());
    }

    private void registerSettingsContentObservers()
    {
        ContentResolver localContentResolver = this.mContext.getContentResolver();
        localContentResolver.registerContentObserver(Settings.Secure.getUriFor("accessibility_enabled"), false, new ContentObserver(new Handler())
        {
            public void onChange(boolean paramAnonymousBoolean)
            {
                super.onChange(paramAnonymousBoolean);
                synchronized (AccessibilityManagerService.this.mLock)
                {
                    if (AccessibilityManagerService.this.mUiAutomationService == null)
                    {
                        AccessibilityManagerService.this.handleAccessibilityEnabledSettingChangedLocked();
                        AccessibilityManagerService.this.updateInputFilterLocked();
                        AccessibilityManagerService.this.sendStateToClientsLocked();
                    }
                    return;
                }
            }
        });
        localContentResolver.registerContentObserver(Settings.Secure.getUriFor("touch_exploration_enabled"), false, new ContentObserver(new Handler())
        {
            public void onChange(boolean paramAnonymousBoolean)
            {
                super.onChange(paramAnonymousBoolean);
                synchronized (AccessibilityManagerService.this.mLock)
                {
                    if (AccessibilityManagerService.this.mUiAutomationService == null)
                    {
                        AccessibilityManagerService.this.handleTouchExplorationEnabledSettingChangedLocked();
                        AccessibilityManagerService.this.updateInputFilterLocked();
                        AccessibilityManagerService.this.sendStateToClientsLocked();
                    }
                    return;
                }
            }
        });
        localContentResolver.registerContentObserver(Settings.Secure.getUriFor("enabled_accessibility_services"), false, new ContentObserver(new Handler())
        {
            public void onChange(boolean paramAnonymousBoolean)
            {
                super.onChange(paramAnonymousBoolean);
                synchronized (AccessibilityManagerService.this.mLock)
                {
                    if (AccessibilityManagerService.this.mUiAutomationService == null)
                    {
                        AccessibilityManagerService.this.populateEnabledAccessibilityServicesLocked();
                        AccessibilityManagerService.this.manageServicesLocked();
                    }
                    return;
                }
            }
        });
        localContentResolver.registerContentObserver(Settings.Secure.getUriFor("touch_exploration_granted_accessibility_services"), false, new ContentObserver(new Handler())
        {
            public void onChange(boolean paramAnonymousBoolean)
            {
                super.onChange(paramAnonymousBoolean);
                synchronized (AccessibilityManagerService.this.mLock)
                {
                    if (AccessibilityManagerService.this.mUiAutomationService == null)
                    {
                        AccessibilityManagerService.this.populateTouchExplorationGrantedAccessibilityServicesLocked();
                        AccessibilityManagerService.this.unbindAllServicesLocked();
                        AccessibilityManagerService.this.manageServicesLocked();
                    }
                    return;
                }
            }
        });
    }

    private void removeAccessibilityInteractionConnectionLocked(int paramInt)
    {
        this.mWindowIdToWindowTokenMap.remove(paramInt);
        this.mWindowIdToInteractionConnectionWrapperMap.remove(paramInt);
    }

    private void sendStateToClientsLocked()
    {
        int i = getState();
        int j = 0;
        int k = this.mClients.size();
        while (true)
            if (j < k)
                try
                {
                    ((IAccessibilityManagerClient)this.mClients.get(j)).setState(i);
                    j++;
                }
                catch (RemoteException localRemoteException)
                {
                    while (true)
                    {
                        this.mClients.remove(j);
                        k--;
                        j--;
                    }
                }
    }

    private void tryAddServiceLocked(Service paramService)
    {
        try
        {
            if ((!this.mServices.contains(paramService)) && (paramService.isConfigured()))
            {
                paramService.linkToOwnDeath();
                this.mServices.add(paramService);
                this.mComponentNameToServiceMap.put(paramService.mComponentName, paramService);
                updateInputFilterLocked();
                tryEnableTouchExplorationLocked(paramService);
            }
        }
        catch (RemoteException localRemoteException)
        {
        }
    }

    private void tryDisableTouchExplorationLocked(Service paramService)
    {
        if (this.mIsTouchExplorationEnabled);
        while (true)
        {
            int j;
            synchronized (this.mLock)
            {
                int i = this.mServices.size();
                j = 0;
                if (j < i)
                {
                    Service localService = (Service)this.mServices.get(j);
                    if ((localService == paramService) || (!localService.mRequestTouchExplorationMode))
                        break label94;
                }
                else
                {
                    this.mMainHandler.obtainMessage(2, 0, 0).sendToTarget();
                }
            }
            return;
            label94: j++;
        }
    }

    private void tryEnableTouchExplorationLocked(Service paramService)
    {
        if ((!this.mIsTouchExplorationEnabled) && (paramService.mRequestTouchExplorationMode))
        {
            boolean bool = this.mTouchExplorationGrantedServices.contains(paramService.mComponentName);
            if ((paramService.mIsAutomation) || (bool))
                break label52;
            this.mMainHandler.obtainMessage(1, paramService).sendToTarget();
        }
        while (true)
        {
            return;
            label52: this.mMainHandler.obtainMessage(2, 1, 0).sendToTarget();
        }
    }

    private boolean tryRemoveServiceLocked(Service paramService)
    {
        boolean bool = this.mServices.remove(paramService);
        if (!bool)
            bool = false;
        while (true)
        {
            return bool;
            this.mComponentNameToServiceMap.remove(paramService.mComponentName);
            paramService.unlinkToOwnDeath();
            paramService.dispose();
            updateInputFilterLocked();
            tryDisableTouchExplorationLocked(paramService);
        }
    }

    private void unbindAllServicesLocked()
    {
        List localList = this.mServices;
        int i = 0;
        int j = localList.size();
        while (i < j)
        {
            if (((Service)localList.get(i)).unbind())
            {
                i--;
                j--;
            }
            i++;
        }
    }

    private void updateInputFilterLocked()
    {
        if ((this.mIsAccessibilityEnabled) && (this.mIsTouchExplorationEnabled))
            if (!this.mHasInputFilter)
            {
                this.mHasInputFilter = true;
                if (this.mInputFilter == null)
                    this.mInputFilter = new AccessibilityInputFilter(this.mContext, this);
                this.mWindowManagerService.setInputFilter(this.mInputFilter);
            }
        while (true)
        {
            return;
            if (this.mHasInputFilter)
            {
                this.mHasInputFilter = false;
                this.mWindowManagerService.setInputFilter(null);
            }
        }
    }

    private int updateServicesStateLocked(List<AccessibilityServiceInfo> paramList, Set<ComponentName> paramSet)
    {
        Map localMap = this.mComponentNameToServiceMap;
        boolean bool = this.mIsAccessibilityEnabled;
        int i = 0;
        int j = 0;
        int k = paramList.size();
        if (j < k)
        {
            AccessibilityServiceInfo localAccessibilityServiceInfo = (AccessibilityServiceInfo)paramList.get(j);
            ComponentName localComponentName = ComponentName.unflattenFromString(localAccessibilityServiceInfo.getId());
            Service localService = (Service)localMap.get(localComponentName);
            if (bool)
                if (paramSet.contains(localComponentName))
                {
                    if (localService == null)
                        localService = new Service(localComponentName, localAccessibilityServiceInfo, false);
                    localService.bind();
                    i++;
                }
            while (true)
            {
                j++;
                break;
                if (localService != null)
                {
                    localService.unbind();
                    continue;
                    if (localService != null)
                        localService.unbind();
                }
            }
        }
        return i;
    }

    public int addAccessibilityInteractionConnection(IWindow paramIWindow, IAccessibilityInteractionConnection paramIAccessibilityInteractionConnection)
        throws RemoteException
    {
        synchronized (this.mLock)
        {
            int i = sNextWindowId;
            sNextWindowId = i + 1;
            AccessibilityConnectionWrapper localAccessibilityConnectionWrapper = new AccessibilityConnectionWrapper(i, paramIAccessibilityInteractionConnection);
            localAccessibilityConnectionWrapper.linkToDeath();
            this.mWindowIdToWindowTokenMap.put(i, paramIWindow.asBinder());
            this.mWindowIdToInteractionConnectionWrapperMap.put(i, localAccessibilityConnectionWrapper);
            return i;
        }
    }

    public int addClient(final IAccessibilityManagerClient paramIAccessibilityManagerClient)
        throws RemoteException
    {
        synchronized (this.mLock)
        {
            this.mClients.add(paramIAccessibilityManagerClient);
            paramIAccessibilityManagerClient.asBinder().linkToDeath(new IBinder.DeathRecipient()
            {
                public void binderDied()
                {
                    synchronized (AccessibilityManagerService.this.mLock)
                    {
                        paramIAccessibilityManagerClient.asBinder().unlinkToDeath(this, 0);
                        AccessibilityManagerService.this.mClients.remove(paramIAccessibilityManagerClient);
                        return;
                    }
                }
            }
            , 0);
            int i = getState();
            return i;
        }
    }

    boolean getAccessibilityFocusBoundsInActiveWindow(Rect paramRect)
    {
        boolean bool = false;
        Service localService = getQueryBridge();
        int i = localService.mId;
        AccessibilityInteractionClient localAccessibilityInteractionClient = AccessibilityInteractionClient.getInstance();
        localAccessibilityInteractionClient.addConnection(i, localService);
        try
        {
            AccessibilityNodeInfo localAccessibilityNodeInfo1 = AccessibilityInteractionClient.getInstance().getRootInActiveWindow(i);
            if (localAccessibilityNodeInfo1 == null);
            while (true)
            {
                return bool;
                AccessibilityNodeInfo localAccessibilityNodeInfo2 = localAccessibilityNodeInfo1.findFocus(2);
                if (localAccessibilityNodeInfo2 == null)
                {
                    localAccessibilityInteractionClient.removeConnection(i);
                }
                else
                {
                    localAccessibilityNodeInfo2.getBoundsInScreen(paramRect);
                    bool = true;
                    localAccessibilityInteractionClient.removeConnection(i);
                }
            }
        }
        finally
        {
            localAccessibilityInteractionClient.removeConnection(i);
        }
    }

    void getActiveWindowBounds(Rect paramRect)
    {
        synchronized (this.mLock)
        {
            int i = this.mSecurityPolicy.mActiveWindowId;
            IBinder localIBinder = (IBinder)this.mWindowIdToWindowTokenMap.get(i);
            this.mWindowManagerService.getWindowFrame(localIBinder, paramRect);
            return;
        }
    }

    int getActiveWindowId()
    {
        return this.mSecurityPolicy.mActiveWindowId;
    }

    public List<AccessibilityServiceInfo> getEnabledAccessibilityServiceList(int paramInt)
    {
        List localList1 = this.mEnabledServicesForFeedbackTempList;
        localList1.clear();
        List localList2 = this.mServices;
        Object localObject1 = this.mLock;
        if (paramInt != 0);
        while (true)
        {
            int k;
            try
            {
                int i = 1 << Integer.numberOfTrailingZeros(paramInt);
                paramInt &= (i ^ 0xFFFFFFFF);
                int j = localList2.size();
                k = 0;
                if (k >= j)
                    break;
                Service localService = (Service)localList2.get(k);
                if ((i & localService.mFeedbackType) != 0)
                {
                    localList1.add(localService.mAccessibilityServiceInfo);
                    break label115;
                    return localList1;
                }
            }
            finally
            {
                localObject2 = finally;
                throw localObject2;
            }
            label115: k++;
        }
    }

    public List<AccessibilityServiceInfo> getInstalledAccessibilityServiceList()
    {
        synchronized (this.mLock)
        {
            List localList = this.mInstalledServices;
            return localList;
        }
    }

    public void interrupt()
    {
        Object localObject1 = this.mLock;
        int i = 0;
        try
        {
            int j = this.mServices.size();
            while (true)
                if (i < j)
                {
                    Service localService = (Service)this.mServices.get(i);
                    try
                    {
                        localService.mServiceInterface.onInterrupt();
                        i++;
                    }
                    catch (RemoteException localRemoteException)
                    {
                        while (true)
                            Slog.e("AccessibilityManagerService", "Error during sending interrupt request to " + localService.mService, localRemoteException);
                    }
                }
        }
        finally
        {
        }
    }

    boolean onGesture(int paramInt)
    {
        synchronized (this.mLock)
        {
            boolean bool = notifyGestureLocked(paramInt, false);
            if (!bool)
                bool = notifyGestureLocked(paramInt, true);
            return bool;
        }
    }

    public void registerUiTestAutomationService(IAccessibilityServiceClient paramIAccessibilityServiceClient, AccessibilityServiceInfo paramAccessibilityServiceInfo)
    {
        this.mSecurityPolicy.enforceCallingPermission("android.permission.RETRIEVE_WINDOW_CONTENT", "registerUiTestAutomationService");
        ComponentName localComponentName = new ComponentName("foo.bar", "AutomationAccessibilityService");
        synchronized (this.mLock)
        {
            int i = this.mServices.size();
            for (int j = 0; j < i; j++)
                ((Service)this.mServices.get(j)).unbind();
            if (!this.mIsAccessibilityEnabled)
            {
                this.mIsAccessibilityEnabled = true;
                sendStateToClientsLocked();
            }
            this.mUiAutomationService = new Service(localComponentName, paramAccessibilityServiceInfo, true);
            this.mUiAutomationService.onServiceConnected(localComponentName, paramIAccessibilityServiceClient.asBinder());
            return;
        }
    }

    public void removeAccessibilityInteractionConnection(IWindow paramIWindow)
    {
        while (true)
        {
            int j;
            synchronized (this.mLock)
            {
                int i = this.mWindowIdToWindowTokenMap.size();
                j = 0;
                if (j < i)
                {
                    if (this.mWindowIdToWindowTokenMap.valueAt(j) != paramIWindow.asBinder())
                        break label92;
                    int k = this.mWindowIdToWindowTokenMap.keyAt(j);
                    ((AccessibilityConnectionWrapper)this.mWindowIdToInteractionConnectionWrapperMap.get(k)).unlinkToDeath();
                    removeAccessibilityInteractionConnectionLocked(k);
                }
            }
            return;
            label92: j++;
        }
    }

    public boolean sendAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
    {
        boolean bool = true;
        int i = paramAccessibilityEvent.getEventType();
        if ((i == 128) && (this.mTouchExplorationGestureStarted))
        {
            this.mTouchExplorationGestureStarted = false;
            sendAccessibilityEvent(AccessibilityEvent.obtain(512));
        }
        while (true)
        {
            synchronized (this.mLock)
            {
                if (this.mSecurityPolicy.canDispatchAccessibilityEvent(paramAccessibilityEvent))
                {
                    this.mSecurityPolicy.updateActiveWindowAndEventSourceLocked(paramAccessibilityEvent);
                    notifyAccessibilityServicesDelayedLocked(paramAccessibilityEvent, false);
                    notifyAccessibilityServicesDelayedLocked(paramAccessibilityEvent, true);
                }
                if ((this.mHasInputFilter) && (this.mInputFilter != null))
                    this.mMainHandler.obtainMessage(3, AccessibilityEvent.obtain(paramAccessibilityEvent)).sendToTarget();
                paramAccessibilityEvent.recycle();
                this.mHandledFeedbackTypes = 0;
                if ((i == 256) && (this.mTouchExplorationGestureEnded))
                {
                    this.mTouchExplorationGestureEnded = false;
                    sendAccessibilityEvent(AccessibilityEvent.obtain(1024));
                }
                if (OWN_PROCESS_ID != Binder.getCallingPid())
                    return bool;
            }
            bool = false;
        }
    }

    public void touchExplorationGestureEnded()
    {
        this.mTouchExplorationGestureEnded = true;
    }

    public void touchExplorationGestureStarted()
    {
        this.mTouchExplorationGestureStarted = true;
    }

    public void unregisterUiTestAutomationService(IAccessibilityServiceClient paramIAccessibilityServiceClient)
    {
        synchronized (this.mLock)
        {
            if ((this.mUiAutomationService != null) && (this.mUiAutomationService.mServiceInterface == paramIAccessibilityServiceClient))
                this.mUiAutomationService.binderDied();
            return;
        }
    }

    final class SecurityPolicy
    {
        private static final int RETRIEVAL_ALLOWING_EVENT_TYPES = 113087;
        private static final int VALID_ACTIONS = 16383;
        private int mActiveWindowId;

        SecurityPolicy()
        {
        }

        private boolean canDispatchAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
        {
            if ((paramAccessibilityEvent.getEventType() != 2048) || (paramAccessibilityEvent.getWindowId() == this.mActiveWindowId));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        private void enforceCallingPermission(String paramString1, String paramString2)
        {
            if (AccessibilityManagerService.OWN_PROCESS_ID == Binder.getCallingPid());
            while (AccessibilityManagerService.this.mContext.checkCallingPermission(paramString1) == 0)
                return;
            throw new SecurityException("You do not have " + paramString1 + " required to call " + paramString2);
        }

        private int getFocusedWindowId()
        {
            IBinder localIBinder = AccessibilityManagerService.this.mWindowManagerService.getFocusedWindowClientToken();
            SparseArray localSparseArray;
            int k;
            if (localIBinder != null)
            {
                localSparseArray = AccessibilityManagerService.this.mWindowIdToWindowTokenMap;
                int j = localSparseArray.size();
                k = 0;
                if (k < j)
                    if (localSparseArray.valueAt(k) != localIBinder);
            }
            for (int i = localSparseArray.keyAt(k); ; i = -1)
            {
                return i;
                k++;
                break;
            }
        }

        private boolean isActionPermitted(int paramInt)
        {
            if ((paramInt & 0x3FFF) != 0);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        private boolean isRetrievalAllowingWindow(int paramInt)
        {
            if (this.mActiveWindowId == paramInt);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public boolean canGetAccessibilityNodeInfoLocked(AccessibilityManagerService.Service paramService, int paramInt)
        {
            if ((canRetrieveWindowContent(paramService)) && (isRetrievalAllowingWindow(paramInt)));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public boolean canPerformActionLocked(AccessibilityManagerService.Service paramService, int paramInt1, int paramInt2, Bundle paramBundle)
        {
            if ((canRetrieveWindowContent(paramService)) && (isRetrievalAllowingWindow(paramInt1)) && (isActionPermitted(paramInt2)));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public boolean canRetrieveWindowContent(AccessibilityManagerService.Service paramService)
        {
            return paramService.mCanRetrieveScreenContent;
        }

        public void enforceCanRetrieveWindowContent(AccessibilityManagerService.Service paramService)
            throws RemoteException
        {
            if (!canRetrieveWindowContent(paramService))
            {
                Slog.e("AccessibilityManagerService", "Accessibility serivce " + paramService.mComponentName + " does not " + "declare android:canRetrieveWindowContent.");
                throw new RemoteException();
            }
        }

        public int getRetrievalAllowingWindowLocked()
        {
            return this.mActiveWindowId;
        }

        public void updateActiveWindowAndEventSourceLocked(AccessibilityEvent paramAccessibilityEvent)
        {
            int i = paramAccessibilityEvent.getWindowId();
            int j = paramAccessibilityEvent.getEventType();
            switch (j)
            {
            default:
            case 32:
            case 128:
            case 256:
            case 1024:
            }
            while (true)
            {
                if ((0x1B9BF & j) == 0)
                    paramAccessibilityEvent.setSource(null);
                return;
                if (getFocusedWindowId() == i)
                {
                    this.mActiveWindowId = i;
                    continue;
                    this.mActiveWindowId = i;
                    continue;
                    this.mActiveWindowId = getFocusedWindowId();
                }
            }
        }
    }

    class Service extends IAccessibilityServiceConnection.Stub
        implements ServiceConnection, IBinder.DeathRecipient
    {
        private static final int MSG_ON_GESTURE = -2147483648;
        AccessibilityServiceInfo mAccessibilityServiceInfo;
        boolean mCanRetrieveScreenContent;
        ComponentName mComponentName;
        int mEventTypes;
        int mFeedbackType;
        public Handler mHandler = new Handler(AccessibilityManagerService.this.mMainHandler.getLooper())
        {
            public void handleMessage(Message paramAnonymousMessage)
            {
                int i = paramAnonymousMessage.what;
                switch (i)
                {
                default:
                    AccessibilityManagerService.Service.this.notifyAccessibilityEventInternal(i);
                case -2147483648:
                }
                while (true)
                {
                    return;
                    int j = paramAnonymousMessage.arg1;
                    AccessibilityManagerService.Service.this.notifyGestureInternal(j);
                }
            }
        };
        int mId = 0;
        boolean mIncludeNotImportantViews;
        Intent mIntent;
        boolean mIsAutomation;
        boolean mIsDefault;
        long mNotificationTimeout;
        Set<String> mPackageNames = new HashSet();
        final SparseArray<AccessibilityEvent> mPendingEvents = new SparseArray();
        boolean mReqeustTouchExplorationMode;
        boolean mRequestTouchExplorationMode;
        final ResolveInfo mResolveInfo;
        IBinder mService;
        IAccessibilityServiceClient mServiceInterface;
        final Rect mTempBounds = new Rect();

        public Service(ComponentName paramAccessibilityServiceInfo, AccessibilityServiceInfo paramBoolean, boolean arg4)
        {
            this.mResolveInfo = paramBoolean.getResolveInfo();
            this.mId = AccessibilityManagerService.access$2508();
            this.mComponentName = paramAccessibilityServiceInfo;
            this.mAccessibilityServiceInfo = paramBoolean;
            boolean bool1;
            this.mIsAutomation = bool1;
            if (!bool1)
            {
                this.mCanRetrieveScreenContent = paramBoolean.getCanRetrieveWindowContent();
                if ((0x4 & paramBoolean.flags) != 0)
                {
                    this.mReqeustTouchExplorationMode = bool2;
                    this.mIntent = new Intent().setComponent(this.mComponentName);
                    this.mIntent.putExtra("android.intent.extra.client_label", 17040503);
                    this.mIntent.putExtra("android.intent.extra.client_intent", PendingIntent.getActivity(AccessibilityManagerService.this.mContext, 0, new Intent("android.settings.ACCESSIBILITY_SETTINGS"), 0));
                }
            }
            while (true)
            {
                setDynamicallyConfigurableProperties(paramBoolean);
                return;
                bool2 = false;
                break;
                this.mCanRetrieveScreenContent = bool2;
            }
        }

        private void expandStatusBar()
        {
            long l = Binder.clearCallingIdentity();
            ((StatusBarManager)AccessibilityManagerService.this.mContext.getSystemService("statusbar")).expand();
            Binder.restoreCallingIdentity(l);
        }

        private float getCompatibilityScale(int paramInt)
        {
            IBinder localIBinder = (IBinder)AccessibilityManagerService.this.mWindowIdToWindowTokenMap.get(paramInt);
            return AccessibilityManagerService.this.mWindowManagerService.getWindowCompatibilityScale(localIBinder);
        }

        private IAccessibilityInteractionConnection getConnectionLocked(int paramInt)
        {
            AccessibilityManagerService.AccessibilityConnectionWrapper localAccessibilityConnectionWrapper = (AccessibilityManagerService.AccessibilityConnectionWrapper)AccessibilityManagerService.this.mWindowIdToInteractionConnectionWrapperMap.get(paramInt);
            if ((localAccessibilityConnectionWrapper != null) && (AccessibilityManagerService.AccessibilityConnectionWrapper.access$3400(localAccessibilityConnectionWrapper) != null));
            for (IAccessibilityInteractionConnection localIAccessibilityInteractionConnection = AccessibilityManagerService.AccessibilityConnectionWrapper.access$3400(localAccessibilityConnectionWrapper); ; localIAccessibilityInteractionConnection = null)
                return localIAccessibilityInteractionConnection;
        }

        // ERROR //
        private void notifyAccessibilityEventInternal(int paramInt)
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     4: getfield 233	com/android/server/accessibility/AccessibilityManagerService:mLock	Ljava/lang/Object;
            //     7: astore_2
            //     8: aload_2
            //     9: monitorenter
            //     10: aload_0
            //     11: getfield 235	com/android/server/accessibility/AccessibilityManagerService$Service:mServiceInterface	Landroid/accessibilityservice/IAccessibilityServiceClient;
            //     14: astore 4
            //     16: aload 4
            //     18: ifnonnull +8 -> 26
            //     21: aload_2
            //     22: monitorexit
            //     23: goto +157 -> 180
            //     26: aload_0
            //     27: getfield 76	com/android/server/accessibility/AccessibilityManagerService$Service:mPendingEvents	Landroid/util/SparseArray;
            //     30: iload_1
            //     31: invokevirtual 204	android/util/SparseArray:get	(I)Ljava/lang/Object;
            //     34: checkcast 237	android/view/accessibility/AccessibilityEvent
            //     37: astore 5
            //     39: aload 5
            //     41: ifnonnull +13 -> 54
            //     44: aload_2
            //     45: monitorexit
            //     46: goto +134 -> 180
            //     49: astore_3
            //     50: aload_2
            //     51: monitorexit
            //     52: aload_3
            //     53: athrow
            //     54: aload_0
            //     55: getfield 76	com/android/server/accessibility/AccessibilityManagerService$Service:mPendingEvents	Landroid/util/SparseArray;
            //     58: iload_1
            //     59: invokevirtual 240	android/util/SparseArray:remove	(I)V
            //     62: aload_0
            //     63: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     66: invokestatic 244	com/android/server/accessibility/AccessibilityManagerService:access$3000	(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;
            //     69: aload_0
            //     70: invokevirtual 250	com/android/server/accessibility/AccessibilityManagerService$SecurityPolicy:canRetrieveWindowContent	(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)Z
            //     73: ifeq +37 -> 110
            //     76: aload 5
            //     78: aload_0
            //     79: getfield 61	com/android/server/accessibility/AccessibilityManagerService$Service:mId	I
            //     82: invokevirtual 253	android/view/accessibility/AccessibilityEvent:setConnectionId	(I)V
            //     85: aload 5
            //     87: iconst_1
            //     88: invokevirtual 257	android/view/accessibility/AccessibilityEvent:setSealed	(Z)V
            //     91: aload_2
            //     92: monitorexit
            //     93: aload 4
            //     95: aload 5
            //     97: invokeinterface 263 2 0
            //     102: aload 5
            //     104: invokevirtual 266	android/view/accessibility/AccessibilityEvent:recycle	()V
            //     107: goto +73 -> 180
            //     110: aload 5
            //     112: aconst_null
            //     113: invokevirtual 270	android/view/accessibility/AccessibilityEvent:setSource	(Landroid/view/View;)V
            //     116: goto -31 -> 85
            //     119: astore 7
            //     121: ldc_w 272
            //     124: new 274	java/lang/StringBuilder
            //     127: dup
            //     128: invokespecial 275	java/lang/StringBuilder:<init>	()V
            //     131: ldc_w 277
            //     134: invokevirtual 281	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     137: aload 5
            //     139: invokevirtual 284	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     142: ldc_w 286
            //     145: invokevirtual 281	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     148: aload 4
            //     150: invokevirtual 284	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     153: invokevirtual 290	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     156: aload 7
            //     158: invokestatic 296	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     161: pop
            //     162: aload 5
            //     164: invokevirtual 266	android/view/accessibility/AccessibilityEvent:recycle	()V
            //     167: goto +13 -> 180
            //     170: astore 6
            //     172: aload 5
            //     174: invokevirtual 266	android/view/accessibility/AccessibilityEvent:recycle	()V
            //     177: aload 6
            //     179: athrow
            //     180: return
            //
            // Exception table:
            //     from	to	target	type
            //     10	52	49	finally
            //     54	93	49	finally
            //     110	116	49	finally
            //     93	102	119	android/os/RemoteException
            //     93	102	170	finally
            //     121	162	170	finally
        }

        private void notifyGestureInternal(int paramInt)
        {
            IAccessibilityServiceClient localIAccessibilityServiceClient = this.mServiceInterface;
            if (localIAccessibilityServiceClient != null);
            try
            {
                localIAccessibilityServiceClient.onGesture(paramInt);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Slog.e("AccessibilityManagerService", "Error during sending gesture " + paramInt + " to " + this.mService, localRemoteException);
            }
        }

        private void openRecents()
        {
            long l = Binder.clearCallingIdentity();
            IStatusBarService localIStatusBarService = IStatusBarService.Stub.asInterface(ServiceManager.getService("statusbar"));
            try
            {
                localIStatusBarService.toggleRecentApps();
                Binder.restoreCallingIdentity(l);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Slog.e("AccessibilityManagerService", "Error toggling recent apps.");
            }
        }

        private int resolveAccessibilityWindowId(int paramInt)
        {
            if (paramInt == -1)
                paramInt = AccessibilityManagerService.access$3000(AccessibilityManagerService.this).mActiveWindowId;
            return paramInt;
        }

        private void sendDownAndUpKeyEvents(int paramInt)
        {
            long l1 = Binder.clearCallingIdentity();
            long l2 = SystemClock.uptimeMillis();
            KeyEvent localKeyEvent1 = KeyEvent.obtain(l2, l2, 0, paramInt, 0, 0, -1, 0, 8, 257, null);
            InputManager.getInstance().injectInputEvent(localKeyEvent1, 0);
            localKeyEvent1.recycle();
            KeyEvent localKeyEvent2 = KeyEvent.obtain(l2, SystemClock.uptimeMillis(), 1, paramInt, 0, 0, -1, 0, 8, 257, null);
            InputManager.getInstance().injectInputEvent(localKeyEvent2, 0);
            localKeyEvent2.recycle();
            Binder.restoreCallingIdentity(l1);
        }

        public boolean bind()
        {
            if ((!this.mIsAutomation) && (this.mService == null));
            for (boolean bool = AccessibilityManagerService.this.mContext.bindService(this.mIntent, this, 1); ; bool = false)
                return bool;
        }

        public void binderDied()
        {
            synchronized (AccessibilityManagerService.this.mLock)
            {
                AccessibilityManagerService.this.tryRemoveServiceLocked(this);
                if (this.mIsAutomation)
                {
                    AccessibilityManagerService.access$102(AccessibilityManagerService.this, null);
                    AccessibilityManagerService.this.populateEnabledAccessibilityServicesLocked();
                    AccessibilityManagerService.this.populateTouchExplorationGrantedAccessibilityServicesLocked();
                    AccessibilityManagerService.this.handleAccessibilityEnabledSettingChangedLocked();
                    AccessibilityManagerService.this.sendStateToClientsLocked();
                    AccessibilityManagerService.this.handleTouchExplorationEnabledSettingChangedLocked();
                    AccessibilityManagerService.this.updateInputFilterLocked();
                    AccessibilityManagerService.this.populateAccessibilityServiceListLocked();
                    AccessibilityManagerService.this.manageServicesLocked();
                }
                return;
            }
        }

        public void dispose()
        {
            try
            {
                this.mServiceInterface.setConnection(null, this.mId);
                label14: this.mService = null;
                this.mServiceInterface = null;
                return;
            }
            catch (RemoteException localRemoteException)
            {
                break label14;
            }
        }

        // ERROR //
        public float findAccessibilityNodeInfoByAccessibilityId(int paramInt1, long paramLong1, int paramInt2, android.view.accessibility.IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, int paramInt3, long paramLong2)
            throws RemoteException
        {
            // Byte code:
            //     0: aload_0
            //     1: iload_1
            //     2: invokespecial 406	com/android/server/accessibility/AccessibilityManagerService$Service:resolveAccessibilityWindowId	(I)I
            //     5: istore 9
            //     7: aload_0
            //     8: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     11: getfield 233	com/android/server/accessibility/AccessibilityManagerService:mLock	Ljava/lang/Object;
            //     14: astore 10
            //     16: aload 10
            //     18: monitorenter
            //     19: aload_0
            //     20: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     23: invokestatic 244	com/android/server/accessibility/AccessibilityManagerService:access$3000	(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;
            //     26: aload_0
            //     27: invokevirtual 410	com/android/server/accessibility/AccessibilityManagerService$SecurityPolicy:enforceCanRetrieveWindowContent	(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V
            //     30: aload_0
            //     31: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     34: invokestatic 244	com/android/server/accessibility/AccessibilityManagerService:access$3000	(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;
            //     37: aload_0
            //     38: iload 9
            //     40: invokevirtual 414	com/android/server/accessibility/AccessibilityManagerService$SecurityPolicy:canGetAccessibilityNodeInfoLocked	(Lcom/android/server/accessibility/AccessibilityManagerService$Service;I)Z
            //     43: ifne +12 -> 55
            //     46: fconst_0
            //     47: fstore 24
            //     49: aload 10
            //     51: monitorexit
            //     52: goto +180 -> 232
            //     55: aload_0
            //     56: iload 9
            //     58: invokespecial 416	com/android/server/accessibility/AccessibilityManagerService$Service:getConnectionLocked	(I)Landroid/view/accessibility/IAccessibilityInteractionConnection;
            //     61: astore 12
            //     63: aload 12
            //     65: ifnonnull +20 -> 85
            //     68: fconst_0
            //     69: fstore 24
            //     71: aload 10
            //     73: monitorexit
            //     74: goto +158 -> 232
            //     77: astore 11
            //     79: aload 10
            //     81: monitorexit
            //     82: aload 11
            //     84: athrow
            //     85: aload_0
            //     86: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     89: invokestatic 200	com/android/server/accessibility/AccessibilityManagerService:access$3100	(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/util/SparseArray;
            //     92: iload 9
            //     94: invokevirtual 204	android/util/SparseArray:get	(I)Ljava/lang/Object;
            //     97: checkcast 206	android/os/IBinder
            //     100: astore 13
            //     102: aload_0
            //     103: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     106: invokestatic 210	com/android/server/accessibility/AccessibilityManagerService:access$3200	(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/wm/WindowManagerService;
            //     109: aload 13
            //     111: aload_0
            //     112: getfield 71	com/android/server/accessibility/AccessibilityManagerService$Service:mTempBounds	Landroid/graphics/Rect;
            //     115: invokevirtual 420	com/android/server/wm/WindowManagerService:getWindowFrame	(Landroid/os/IBinder;Landroid/graphics/Rect;)Z
            //     118: pop
            //     119: aload_0
            //     120: getfield 71	com/android/server/accessibility/AccessibilityManagerService$Service:mTempBounds	Landroid/graphics/Rect;
            //     123: getfield 423	android/graphics/Rect:left	I
            //     126: istore 15
            //     128: aload_0
            //     129: getfield 71	com/android/server/accessibility/AccessibilityManagerService$Service:mTempBounds	Landroid/graphics/Rect;
            //     132: getfield 426	android/graphics/Rect:top	I
            //     135: istore 16
            //     137: aload 10
            //     139: monitorexit
            //     140: aload_0
            //     141: getfield 428	com/android/server/accessibility/AccessibilityManagerService$Service:mIncludeNotImportantViews	Z
            //     144: ifeq +62 -> 206
            //     147: bipush 8
            //     149: istore 17
            //     151: iload 6
            //     153: iload 17
            //     155: ior
            //     156: istore 18
            //     158: invokestatic 431	android/os/Binder:getCallingPid	()I
            //     161: istore 19
            //     163: invokestatic 177	android/os/Binder:clearCallingIdentity	()J
            //     166: lstore 20
            //     168: aload 12
            //     170: lload_2
            //     171: iload 15
            //     173: iload 16
            //     175: iload 4
            //     177: aload 5
            //     179: iload 18
            //     181: iload 19
            //     183: lload 7
            //     185: invokeinterface 436 11 0
            //     190: lload 20
            //     192: invokestatic 194	android/os/Binder:restoreCallingIdentity	(J)V
            //     195: aload_0
            //     196: iload 9
            //     198: invokespecial 438	com/android/server/accessibility/AccessibilityManagerService$Service:getCompatibilityScale	(I)F
            //     201: fstore 24
            //     203: goto +29 -> 232
            //     206: iconst_0
            //     207: istore 17
            //     209: goto -58 -> 151
            //     212: astore 23
            //     214: lload 20
            //     216: invokestatic 194	android/os/Binder:restoreCallingIdentity	(J)V
            //     219: goto -24 -> 195
            //     222: astore 22
            //     224: lload 20
            //     226: invokestatic 194	android/os/Binder:restoreCallingIdentity	(J)V
            //     229: aload 22
            //     231: athrow
            //     232: fload 24
            //     234: freturn
            //
            // Exception table:
            //     from	to	target	type
            //     19	82	77	finally
            //     85	140	77	finally
            //     168	190	212	android/os/RemoteException
            //     168	190	222	finally
        }

        // ERROR //
        public float findAccessibilityNodeInfoByViewId(int paramInt1, long paramLong1, int paramInt2, int paramInt3, android.view.accessibility.IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, long paramLong2)
            throws RemoteException
        {
            // Byte code:
            //     0: aload_0
            //     1: iload_1
            //     2: invokespecial 406	com/android/server/accessibility/AccessibilityManagerService$Service:resolveAccessibilityWindowId	(I)I
            //     5: istore 9
            //     7: aload_0
            //     8: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     11: getfield 233	com/android/server/accessibility/AccessibilityManagerService:mLock	Ljava/lang/Object;
            //     14: astore 10
            //     16: aload 10
            //     18: monitorenter
            //     19: aload_0
            //     20: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     23: invokestatic 244	com/android/server/accessibility/AccessibilityManagerService:access$3000	(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;
            //     26: aload_0
            //     27: invokevirtual 410	com/android/server/accessibility/AccessibilityManagerService$SecurityPolicy:enforceCanRetrieveWindowContent	(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V
            //     30: aload_0
            //     31: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     34: invokestatic 244	com/android/server/accessibility/AccessibilityManagerService:access$3000	(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;
            //     37: aload_0
            //     38: invokevirtual 250	com/android/server/accessibility/AccessibilityManagerService$SecurityPolicy:canRetrieveWindowContent	(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)Z
            //     41: ifne +12 -> 53
            //     44: fconst_0
            //     45: fstore 23
            //     47: aload 10
            //     49: monitorexit
            //     50: goto +175 -> 225
            //     53: aload_0
            //     54: iload 9
            //     56: invokespecial 416	com/android/server/accessibility/AccessibilityManagerService$Service:getConnectionLocked	(I)Landroid/view/accessibility/IAccessibilityInteractionConnection;
            //     59: astore 12
            //     61: aload 12
            //     63: ifnonnull +20 -> 83
            //     66: fconst_0
            //     67: fstore 23
            //     69: aload 10
            //     71: monitorexit
            //     72: goto +153 -> 225
            //     75: astore 11
            //     77: aload 10
            //     79: monitorexit
            //     80: aload 11
            //     82: athrow
            //     83: aload_0
            //     84: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     87: invokestatic 200	com/android/server/accessibility/AccessibilityManagerService:access$3100	(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/util/SparseArray;
            //     90: iload 9
            //     92: invokevirtual 204	android/util/SparseArray:get	(I)Ljava/lang/Object;
            //     95: checkcast 206	android/os/IBinder
            //     98: astore 13
            //     100: aload_0
            //     101: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     104: invokestatic 210	com/android/server/accessibility/AccessibilityManagerService:access$3200	(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/wm/WindowManagerService;
            //     107: aload 13
            //     109: aload_0
            //     110: getfield 71	com/android/server/accessibility/AccessibilityManagerService$Service:mTempBounds	Landroid/graphics/Rect;
            //     113: invokevirtual 420	com/android/server/wm/WindowManagerService:getWindowFrame	(Landroid/os/IBinder;Landroid/graphics/Rect;)Z
            //     116: pop
            //     117: aload_0
            //     118: getfield 71	com/android/server/accessibility/AccessibilityManagerService$Service:mTempBounds	Landroid/graphics/Rect;
            //     121: getfield 423	android/graphics/Rect:left	I
            //     124: istore 15
            //     126: aload_0
            //     127: getfield 71	com/android/server/accessibility/AccessibilityManagerService$Service:mTempBounds	Landroid/graphics/Rect;
            //     130: getfield 426	android/graphics/Rect:top	I
            //     133: istore 16
            //     135: aload 10
            //     137: monitorexit
            //     138: aload_0
            //     139: getfield 428	com/android/server/accessibility/AccessibilityManagerService$Service:mIncludeNotImportantViews	Z
            //     142: ifeq +57 -> 199
            //     145: bipush 8
            //     147: istore 17
            //     149: invokestatic 431	android/os/Binder:getCallingPid	()I
            //     152: istore 18
            //     154: invokestatic 177	android/os/Binder:clearCallingIdentity	()J
            //     157: lstore 19
            //     159: aload 12
            //     161: lload_2
            //     162: iload 4
            //     164: iload 15
            //     166: iload 16
            //     168: iload 5
            //     170: aload 6
            //     172: iload 17
            //     174: iload 18
            //     176: lload 7
            //     178: invokeinterface 443 12 0
            //     183: lload 19
            //     185: invokestatic 194	android/os/Binder:restoreCallingIdentity	(J)V
            //     188: aload_0
            //     189: iload 9
            //     191: invokespecial 438	com/android/server/accessibility/AccessibilityManagerService$Service:getCompatibilityScale	(I)F
            //     194: fstore 23
            //     196: goto +29 -> 225
            //     199: iconst_0
            //     200: istore 17
            //     202: goto -53 -> 149
            //     205: astore 22
            //     207: lload 19
            //     209: invokestatic 194	android/os/Binder:restoreCallingIdentity	(J)V
            //     212: goto -24 -> 188
            //     215: astore 21
            //     217: lload 19
            //     219: invokestatic 194	android/os/Binder:restoreCallingIdentity	(J)V
            //     222: aload 21
            //     224: athrow
            //     225: fload 23
            //     227: freturn
            //
            // Exception table:
            //     from	to	target	type
            //     19	80	75	finally
            //     83	138	75	finally
            //     159	183	205	android/os/RemoteException
            //     159	183	215	finally
        }

        // ERROR //
        public float findAccessibilityNodeInfosByText(int paramInt1, long paramLong1, String paramString, int paramInt2, android.view.accessibility.IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, long paramLong2)
            throws RemoteException
        {
            // Byte code:
            //     0: aload_0
            //     1: iload_1
            //     2: invokespecial 406	com/android/server/accessibility/AccessibilityManagerService$Service:resolveAccessibilityWindowId	(I)I
            //     5: istore 9
            //     7: aload_0
            //     8: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     11: getfield 233	com/android/server/accessibility/AccessibilityManagerService:mLock	Ljava/lang/Object;
            //     14: astore 10
            //     16: aload 10
            //     18: monitorenter
            //     19: aload_0
            //     20: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     23: invokestatic 244	com/android/server/accessibility/AccessibilityManagerService:access$3000	(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;
            //     26: aload_0
            //     27: invokevirtual 410	com/android/server/accessibility/AccessibilityManagerService$SecurityPolicy:enforceCanRetrieveWindowContent	(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V
            //     30: aload_0
            //     31: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     34: invokestatic 244	com/android/server/accessibility/AccessibilityManagerService:access$3000	(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;
            //     37: aload_0
            //     38: iload 9
            //     40: invokevirtual 414	com/android/server/accessibility/AccessibilityManagerService$SecurityPolicy:canGetAccessibilityNodeInfoLocked	(Lcom/android/server/accessibility/AccessibilityManagerService$Service;I)Z
            //     43: ifne +12 -> 55
            //     46: fconst_0
            //     47: fstore 23
            //     49: aload 10
            //     51: monitorexit
            //     52: goto +175 -> 227
            //     55: aload_0
            //     56: iload 9
            //     58: invokespecial 416	com/android/server/accessibility/AccessibilityManagerService$Service:getConnectionLocked	(I)Landroid/view/accessibility/IAccessibilityInteractionConnection;
            //     61: astore 12
            //     63: aload 12
            //     65: ifnonnull +20 -> 85
            //     68: fconst_0
            //     69: fstore 23
            //     71: aload 10
            //     73: monitorexit
            //     74: goto +153 -> 227
            //     77: astore 11
            //     79: aload 10
            //     81: monitorexit
            //     82: aload 11
            //     84: athrow
            //     85: aload_0
            //     86: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     89: invokestatic 200	com/android/server/accessibility/AccessibilityManagerService:access$3100	(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/util/SparseArray;
            //     92: iload 9
            //     94: invokevirtual 204	android/util/SparseArray:get	(I)Ljava/lang/Object;
            //     97: checkcast 206	android/os/IBinder
            //     100: astore 13
            //     102: aload_0
            //     103: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     106: invokestatic 210	com/android/server/accessibility/AccessibilityManagerService:access$3200	(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/wm/WindowManagerService;
            //     109: aload 13
            //     111: aload_0
            //     112: getfield 71	com/android/server/accessibility/AccessibilityManagerService$Service:mTempBounds	Landroid/graphics/Rect;
            //     115: invokevirtual 420	com/android/server/wm/WindowManagerService:getWindowFrame	(Landroid/os/IBinder;Landroid/graphics/Rect;)Z
            //     118: pop
            //     119: aload_0
            //     120: getfield 71	com/android/server/accessibility/AccessibilityManagerService$Service:mTempBounds	Landroid/graphics/Rect;
            //     123: getfield 423	android/graphics/Rect:left	I
            //     126: istore 15
            //     128: aload_0
            //     129: getfield 71	com/android/server/accessibility/AccessibilityManagerService$Service:mTempBounds	Landroid/graphics/Rect;
            //     132: getfield 426	android/graphics/Rect:top	I
            //     135: istore 16
            //     137: aload 10
            //     139: monitorexit
            //     140: aload_0
            //     141: getfield 428	com/android/server/accessibility/AccessibilityManagerService$Service:mIncludeNotImportantViews	Z
            //     144: ifeq +57 -> 201
            //     147: bipush 8
            //     149: istore 17
            //     151: invokestatic 431	android/os/Binder:getCallingPid	()I
            //     154: istore 18
            //     156: invokestatic 177	android/os/Binder:clearCallingIdentity	()J
            //     159: lstore 19
            //     161: aload 12
            //     163: lload_2
            //     164: aload 4
            //     166: iload 15
            //     168: iload 16
            //     170: iload 5
            //     172: aload 6
            //     174: iload 17
            //     176: iload 18
            //     178: lload 7
            //     180: invokeinterface 448 12 0
            //     185: lload 19
            //     187: invokestatic 194	android/os/Binder:restoreCallingIdentity	(J)V
            //     190: aload_0
            //     191: iload 9
            //     193: invokespecial 438	com/android/server/accessibility/AccessibilityManagerService$Service:getCompatibilityScale	(I)F
            //     196: fstore 23
            //     198: goto +29 -> 227
            //     201: iconst_0
            //     202: istore 17
            //     204: goto -53 -> 151
            //     207: astore 22
            //     209: lload 19
            //     211: invokestatic 194	android/os/Binder:restoreCallingIdentity	(J)V
            //     214: goto -24 -> 190
            //     217: astore 21
            //     219: lload 19
            //     221: invokestatic 194	android/os/Binder:restoreCallingIdentity	(J)V
            //     224: aload 21
            //     226: athrow
            //     227: fload 23
            //     229: freturn
            //
            // Exception table:
            //     from	to	target	type
            //     19	82	77	finally
            //     85	140	77	finally
            //     161	185	207	android/os/RemoteException
            //     161	185	217	finally
        }

        // ERROR //
        public float findFocus(int paramInt1, long paramLong1, int paramInt2, int paramInt3, android.view.accessibility.IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, long paramLong2)
            throws RemoteException
        {
            // Byte code:
            //     0: aload_0
            //     1: iload_1
            //     2: invokespecial 406	com/android/server/accessibility/AccessibilityManagerService$Service:resolveAccessibilityWindowId	(I)I
            //     5: istore 9
            //     7: aload_0
            //     8: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     11: getfield 233	com/android/server/accessibility/AccessibilityManagerService:mLock	Ljava/lang/Object;
            //     14: astore 10
            //     16: aload 10
            //     18: monitorenter
            //     19: aload_0
            //     20: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     23: invokestatic 244	com/android/server/accessibility/AccessibilityManagerService:access$3000	(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;
            //     26: aload_0
            //     27: invokevirtual 410	com/android/server/accessibility/AccessibilityManagerService$SecurityPolicy:enforceCanRetrieveWindowContent	(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V
            //     30: aload_0
            //     31: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     34: invokestatic 244	com/android/server/accessibility/AccessibilityManagerService:access$3000	(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;
            //     37: aload_0
            //     38: iload 9
            //     40: invokevirtual 414	com/android/server/accessibility/AccessibilityManagerService$SecurityPolicy:canGetAccessibilityNodeInfoLocked	(Lcom/android/server/accessibility/AccessibilityManagerService$Service;I)Z
            //     43: ifne +12 -> 55
            //     46: fconst_0
            //     47: fstore 23
            //     49: aload 10
            //     51: monitorexit
            //     52: goto +175 -> 227
            //     55: aload_0
            //     56: iload 9
            //     58: invokespecial 416	com/android/server/accessibility/AccessibilityManagerService$Service:getConnectionLocked	(I)Landroid/view/accessibility/IAccessibilityInteractionConnection;
            //     61: astore 12
            //     63: aload 12
            //     65: ifnonnull +20 -> 85
            //     68: fconst_0
            //     69: fstore 23
            //     71: aload 10
            //     73: monitorexit
            //     74: goto +153 -> 227
            //     77: astore 11
            //     79: aload 10
            //     81: monitorexit
            //     82: aload 11
            //     84: athrow
            //     85: aload_0
            //     86: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     89: invokestatic 200	com/android/server/accessibility/AccessibilityManagerService:access$3100	(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/util/SparseArray;
            //     92: iload 9
            //     94: invokevirtual 204	android/util/SparseArray:get	(I)Ljava/lang/Object;
            //     97: checkcast 206	android/os/IBinder
            //     100: astore 13
            //     102: aload_0
            //     103: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     106: invokestatic 210	com/android/server/accessibility/AccessibilityManagerService:access$3200	(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/wm/WindowManagerService;
            //     109: aload 13
            //     111: aload_0
            //     112: getfield 71	com/android/server/accessibility/AccessibilityManagerService$Service:mTempBounds	Landroid/graphics/Rect;
            //     115: invokevirtual 420	com/android/server/wm/WindowManagerService:getWindowFrame	(Landroid/os/IBinder;Landroid/graphics/Rect;)Z
            //     118: pop
            //     119: aload_0
            //     120: getfield 71	com/android/server/accessibility/AccessibilityManagerService$Service:mTempBounds	Landroid/graphics/Rect;
            //     123: getfield 423	android/graphics/Rect:left	I
            //     126: istore 15
            //     128: aload_0
            //     129: getfield 71	com/android/server/accessibility/AccessibilityManagerService$Service:mTempBounds	Landroid/graphics/Rect;
            //     132: getfield 426	android/graphics/Rect:top	I
            //     135: istore 16
            //     137: aload 10
            //     139: monitorexit
            //     140: aload_0
            //     141: getfield 428	com/android/server/accessibility/AccessibilityManagerService$Service:mIncludeNotImportantViews	Z
            //     144: ifeq +57 -> 201
            //     147: bipush 8
            //     149: istore 17
            //     151: invokestatic 431	android/os/Binder:getCallingPid	()I
            //     154: istore 18
            //     156: invokestatic 177	android/os/Binder:clearCallingIdentity	()J
            //     159: lstore 19
            //     161: aload 12
            //     163: lload_2
            //     164: iload 4
            //     166: iload 15
            //     168: iload 16
            //     170: iload 5
            //     172: aload 6
            //     174: iload 17
            //     176: iload 18
            //     178: lload 7
            //     180: invokeinterface 451 12 0
            //     185: lload 19
            //     187: invokestatic 194	android/os/Binder:restoreCallingIdentity	(J)V
            //     190: aload_0
            //     191: iload 9
            //     193: invokespecial 438	com/android/server/accessibility/AccessibilityManagerService$Service:getCompatibilityScale	(I)F
            //     196: fstore 23
            //     198: goto +29 -> 227
            //     201: iconst_0
            //     202: istore 17
            //     204: goto -53 -> 151
            //     207: astore 22
            //     209: lload 19
            //     211: invokestatic 194	android/os/Binder:restoreCallingIdentity	(J)V
            //     214: goto -24 -> 190
            //     217: astore 21
            //     219: lload 19
            //     221: invokestatic 194	android/os/Binder:restoreCallingIdentity	(J)V
            //     224: aload 21
            //     226: athrow
            //     227: fload 23
            //     229: freturn
            //
            // Exception table:
            //     from	to	target	type
            //     19	82	77	finally
            //     85	140	77	finally
            //     161	185	207	android/os/RemoteException
            //     161	185	217	finally
        }

        // ERROR //
        public float focusSearch(int paramInt1, long paramLong1, int paramInt2, int paramInt3, android.view.accessibility.IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, long paramLong2)
            throws RemoteException
        {
            // Byte code:
            //     0: aload_0
            //     1: iload_1
            //     2: invokespecial 406	com/android/server/accessibility/AccessibilityManagerService$Service:resolveAccessibilityWindowId	(I)I
            //     5: istore 9
            //     7: aload_0
            //     8: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     11: getfield 233	com/android/server/accessibility/AccessibilityManagerService:mLock	Ljava/lang/Object;
            //     14: astore 10
            //     16: aload 10
            //     18: monitorenter
            //     19: aload_0
            //     20: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     23: invokestatic 244	com/android/server/accessibility/AccessibilityManagerService:access$3000	(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;
            //     26: aload_0
            //     27: invokevirtual 410	com/android/server/accessibility/AccessibilityManagerService$SecurityPolicy:enforceCanRetrieveWindowContent	(Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V
            //     30: aload_0
            //     31: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     34: invokestatic 244	com/android/server/accessibility/AccessibilityManagerService:access$3000	(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;
            //     37: aload_0
            //     38: iload 9
            //     40: invokevirtual 414	com/android/server/accessibility/AccessibilityManagerService$SecurityPolicy:canGetAccessibilityNodeInfoLocked	(Lcom/android/server/accessibility/AccessibilityManagerService$Service;I)Z
            //     43: ifne +12 -> 55
            //     46: fconst_0
            //     47: fstore 23
            //     49: aload 10
            //     51: monitorexit
            //     52: goto +175 -> 227
            //     55: aload_0
            //     56: iload 9
            //     58: invokespecial 416	com/android/server/accessibility/AccessibilityManagerService$Service:getConnectionLocked	(I)Landroid/view/accessibility/IAccessibilityInteractionConnection;
            //     61: astore 12
            //     63: aload 12
            //     65: ifnonnull +20 -> 85
            //     68: fconst_0
            //     69: fstore 23
            //     71: aload 10
            //     73: monitorexit
            //     74: goto +153 -> 227
            //     77: astore 11
            //     79: aload 10
            //     81: monitorexit
            //     82: aload 11
            //     84: athrow
            //     85: aload_0
            //     86: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     89: invokestatic 200	com/android/server/accessibility/AccessibilityManagerService:access$3100	(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/util/SparseArray;
            //     92: iload 9
            //     94: invokevirtual 204	android/util/SparseArray:get	(I)Ljava/lang/Object;
            //     97: checkcast 206	android/os/IBinder
            //     100: astore 13
            //     102: aload_0
            //     103: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     106: invokestatic 210	com/android/server/accessibility/AccessibilityManagerService:access$3200	(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/wm/WindowManagerService;
            //     109: aload 13
            //     111: aload_0
            //     112: getfield 71	com/android/server/accessibility/AccessibilityManagerService$Service:mTempBounds	Landroid/graphics/Rect;
            //     115: invokevirtual 420	com/android/server/wm/WindowManagerService:getWindowFrame	(Landroid/os/IBinder;Landroid/graphics/Rect;)Z
            //     118: pop
            //     119: aload_0
            //     120: getfield 71	com/android/server/accessibility/AccessibilityManagerService$Service:mTempBounds	Landroid/graphics/Rect;
            //     123: getfield 423	android/graphics/Rect:left	I
            //     126: istore 15
            //     128: aload_0
            //     129: getfield 71	com/android/server/accessibility/AccessibilityManagerService$Service:mTempBounds	Landroid/graphics/Rect;
            //     132: getfield 426	android/graphics/Rect:top	I
            //     135: istore 16
            //     137: aload 10
            //     139: monitorexit
            //     140: aload_0
            //     141: getfield 428	com/android/server/accessibility/AccessibilityManagerService$Service:mIncludeNotImportantViews	Z
            //     144: ifeq +57 -> 201
            //     147: bipush 8
            //     149: istore 17
            //     151: invokestatic 431	android/os/Binder:getCallingPid	()I
            //     154: istore 18
            //     156: invokestatic 177	android/os/Binder:clearCallingIdentity	()J
            //     159: lstore 19
            //     161: aload 12
            //     163: lload_2
            //     164: iload 4
            //     166: iload 15
            //     168: iload 16
            //     170: iload 5
            //     172: aload 6
            //     174: iload 17
            //     176: iload 18
            //     178: lload 7
            //     180: invokeinterface 454 12 0
            //     185: lload 19
            //     187: invokestatic 194	android/os/Binder:restoreCallingIdentity	(J)V
            //     190: aload_0
            //     191: iload 9
            //     193: invokespecial 438	com/android/server/accessibility/AccessibilityManagerService$Service:getCompatibilityScale	(I)F
            //     196: fstore 23
            //     198: goto +29 -> 227
            //     201: iconst_0
            //     202: istore 17
            //     204: goto -53 -> 151
            //     207: astore 22
            //     209: lload 19
            //     211: invokestatic 194	android/os/Binder:restoreCallingIdentity	(J)V
            //     214: goto -24 -> 190
            //     217: astore 21
            //     219: lload 19
            //     221: invokestatic 194	android/os/Binder:restoreCallingIdentity	(J)V
            //     224: aload 21
            //     226: athrow
            //     227: fload 23
            //     229: freturn
            //
            // Exception table:
            //     from	to	target	type
            //     19	82	77	finally
            //     85	140	77	finally
            //     161	185	207	android/os/RemoteException
            //     161	185	217	finally
        }

        public AccessibilityServiceInfo getServiceInfo()
        {
            synchronized (AccessibilityManagerService.this.mLock)
            {
                AccessibilityServiceInfo localAccessibilityServiceInfo = this.mAccessibilityServiceInfo;
                return localAccessibilityServiceInfo;
            }
        }

        public boolean isConfigured()
        {
            if ((this.mEventTypes != 0) && (this.mFeedbackType != 0) && (this.mService != null));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void linkToOwnDeath()
            throws RemoteException
        {
            this.mService.linkToDeath(this, 0);
        }

        public void notifyAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
        {
            synchronized (AccessibilityManagerService.this.mLock)
            {
                int i = paramAccessibilityEvent.getEventType();
                AccessibilityEvent localAccessibilityEvent1 = AccessibilityEvent.obtain(paramAccessibilityEvent);
                AccessibilityEvent localAccessibilityEvent2 = (AccessibilityEvent)this.mPendingEvents.get(i);
                this.mPendingEvents.put(i, localAccessibilityEvent1);
                if (localAccessibilityEvent2 != null)
                {
                    this.mHandler.removeMessages(i);
                    localAccessibilityEvent2.recycle();
                }
                Message localMessage = this.mHandler.obtainMessage(i);
                this.mHandler.sendMessageDelayed(localMessage, this.mNotificationTimeout);
                return;
            }
        }

        public void notifyGesture(int paramInt)
        {
            this.mHandler.obtainMessage(-2147483648, paramInt, 0).sendToTarget();
        }

        public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
        {
            this.mService = paramIBinder;
            this.mServiceInterface = IAccessibilityServiceClient.Stub.asInterface(paramIBinder);
            try
            {
                this.mServiceInterface.setConnection(this, this.mId);
                synchronized (AccessibilityManagerService.this.mLock)
                {
                    AccessibilityManagerService.this.tryAddServiceLocked(this);
                }
            }
            catch (RemoteException localRemoteException)
            {
                Slog.w("AccessibilityManagerService", "Error while setting Controller for service: " + paramIBinder, localRemoteException);
            }
        }

        public void onServiceDisconnected(ComponentName paramComponentName)
        {
        }

        // ERROR //
        public boolean performAccessibilityAction(int paramInt1, long paramLong1, int paramInt2, Bundle paramBundle, int paramInt3, android.view.accessibility.IAccessibilityInteractionConnectionCallback paramIAccessibilityInteractionConnectionCallback, long paramLong2)
        {
            // Byte code:
            //     0: aload_0
            //     1: iload_1
            //     2: invokespecial 406	com/android/server/accessibility/AccessibilityManagerService$Service:resolveAccessibilityWindowId	(I)I
            //     5: istore 10
            //     7: aload_0
            //     8: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     11: getfield 233	com/android/server/accessibility/AccessibilityManagerService:mLock	Ljava/lang/Object;
            //     14: astore 11
            //     16: aload 11
            //     18: monitorenter
            //     19: aload_0
            //     20: getfield 56	com/android/server/accessibility/AccessibilityManagerService$Service:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     23: invokestatic 244	com/android/server/accessibility/AccessibilityManagerService:access$3000	(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityManagerService$SecurityPolicy;
            //     26: aload_0
            //     27: iload 10
            //     29: iload 4
            //     31: aload 5
            //     33: invokevirtual 525	com/android/server/accessibility/AccessibilityManagerService$SecurityPolicy:canPerformActionLocked	(Lcom/android/server/accessibility/AccessibilityManagerService$Service;IILandroid/os/Bundle;)Z
            //     36: ifne +12 -> 48
            //     39: iconst_0
            //     40: istore 20
            //     42: aload 11
            //     44: monitorexit
            //     45: goto +116 -> 161
            //     48: aload_0
            //     49: iload 10
            //     51: invokespecial 416	com/android/server/accessibility/AccessibilityManagerService$Service:getConnectionLocked	(I)Landroid/view/accessibility/IAccessibilityInteractionConnection;
            //     54: astore 13
            //     56: aload 13
            //     58: ifnonnull +20 -> 78
            //     61: iconst_0
            //     62: istore 20
            //     64: aload 11
            //     66: monitorexit
            //     67: goto +94 -> 161
            //     70: astore 12
            //     72: aload 11
            //     74: monitorexit
            //     75: aload 12
            //     77: athrow
            //     78: aload 11
            //     80: monitorexit
            //     81: aload_0
            //     82: getfield 428	com/android/server/accessibility/AccessibilityManagerService$Service:mIncludeNotImportantViews	Z
            //     85: ifeq +50 -> 135
            //     88: bipush 8
            //     90: istore 14
            //     92: invokestatic 431	android/os/Binder:getCallingPid	()I
            //     95: istore 15
            //     97: invokestatic 177	android/os/Binder:clearCallingIdentity	()J
            //     100: lstore 16
            //     102: aload 13
            //     104: lload_2
            //     105: iload 4
            //     107: aload 5
            //     109: iload 6
            //     111: aload 7
            //     113: iload 14
            //     115: iload 15
            //     117: lload 8
            //     119: invokeinterface 528 11 0
            //     124: lload 16
            //     126: invokestatic 194	android/os/Binder:restoreCallingIdentity	(J)V
            //     129: iconst_1
            //     130: istore 20
            //     132: goto +29 -> 161
            //     135: iconst_0
            //     136: istore 14
            //     138: goto -46 -> 92
            //     141: astore 19
            //     143: lload 16
            //     145: invokestatic 194	android/os/Binder:restoreCallingIdentity	(J)V
            //     148: goto -19 -> 129
            //     151: astore 18
            //     153: lload 16
            //     155: invokestatic 194	android/os/Binder:restoreCallingIdentity	(J)V
            //     158: aload 18
            //     160: athrow
            //     161: iload 20
            //     163: ireturn
            //
            // Exception table:
            //     from	to	target	type
            //     19	75	70	finally
            //     78	81	70	finally
            //     102	124	141	android/os/RemoteException
            //     102	124	151	finally
        }

        public boolean performGlobalAction(int paramInt)
        {
            boolean bool = true;
            switch (paramInt)
            {
            default:
                bool = false;
            case 1:
            case 2:
            case 3:
            case 4:
            }
            while (true)
            {
                return bool;
                sendDownAndUpKeyEvents(4);
                continue;
                sendDownAndUpKeyEvents(3);
                continue;
                openRecents();
                continue;
                expandStatusBar();
            }
        }

        public void setDynamicallyConfigurableProperties(AccessibilityServiceInfo paramAccessibilityServiceInfo)
        {
            boolean bool1 = true;
            this.mEventTypes = paramAccessibilityServiceInfo.eventTypes;
            this.mFeedbackType = paramAccessibilityServiceInfo.feedbackType;
            String[] arrayOfString = paramAccessibilityServiceInfo.packageNames;
            if (arrayOfString != null)
                this.mPackageNames.addAll(Arrays.asList(arrayOfString));
            this.mNotificationTimeout = paramAccessibilityServiceInfo.notificationTimeout;
            boolean bool2;
            boolean bool3;
            if ((0x1 & paramAccessibilityServiceInfo.flags) != 0)
            {
                bool2 = bool1;
                this.mIsDefault = bool2;
                if ((this.mIsAutomation) || (paramAccessibilityServiceInfo.getResolveInfo().serviceInfo.applicationInfo.targetSdkVersion >= 16))
                {
                    if ((0x2 & paramAccessibilityServiceInfo.flags) == 0)
                        break label187;
                    bool3 = bool1;
                    label104: this.mIncludeNotImportantViews = bool3;
                }
                if ((0x4 & paramAccessibilityServiceInfo.flags) == 0)
                    break label193;
            }
            while (true)
            {
                this.mRequestTouchExplorationMode = bool1;
                synchronized (AccessibilityManagerService.this.mLock)
                {
                    if (isConfigured())
                    {
                        if (this.mRequestTouchExplorationMode)
                            AccessibilityManagerService.this.tryEnableTouchExplorationLocked(this);
                    }
                    else
                        return;
                    AccessibilityManagerService.this.tryDisableTouchExplorationLocked(this);
                }
                break;
                label187: bool3 = false;
                break label104;
                label193: bool1 = false;
            }
        }

        public void setServiceInfo(AccessibilityServiceInfo paramAccessibilityServiceInfo)
        {
            synchronized (AccessibilityManagerService.this.mLock)
            {
                AccessibilityServiceInfo localAccessibilityServiceInfo = this.mAccessibilityServiceInfo;
                if (localAccessibilityServiceInfo != null)
                {
                    localAccessibilityServiceInfo.updateDynamicallyConfigurableProperties(paramAccessibilityServiceInfo);
                    setDynamicallyConfigurableProperties(localAccessibilityServiceInfo);
                    return;
                }
                setDynamicallyConfigurableProperties(paramAccessibilityServiceInfo);
            }
        }

        public boolean unbind()
        {
            if (this.mService != null);
            while (true)
            {
                synchronized (AccessibilityManagerService.this.mLock)
                {
                    AccessibilityManagerService.this.tryRemoveServiceLocked(this);
                    if (!this.mIsAutomation)
                        AccessibilityManagerService.this.mContext.unbindService(this);
                    bool = true;
                    return bool;
                }
                boolean bool = false;
            }
        }

        public void unlinkToOwnDeath()
        {
            this.mService.unlinkToDeath(this, 0);
        }
    }

    private class MainHanler extends Handler
    {
        private MainHanler()
        {
        }

        // ERROR //
        public void handleMessage(Message paramMessage)
        {
            // Byte code:
            //     0: aload_1
            //     1: getfield 31	android/os/Message:what	I
            //     4: tableswitch	default:+28 -> 32, 1:+56->60, 2:+29->33, 3:+297->301
            //     33: aload_1
            //     34: getfield 34	android/os/Message:arg1	I
            //     37: istore 12
            //     39: aload_0
            //     40: getfield 17	com/android/server/accessibility/AccessibilityManagerService$MainHanler:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     43: getfield 38	com/android/server/accessibility/AccessibilityManagerService:mContext	Landroid/content/Context;
            //     46: invokevirtual 44	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
            //     49: ldc 46
            //     51: iload 12
            //     53: invokestatic 52	android/provider/Settings$Secure:putInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
            //     56: pop
            //     57: goto -25 -> 32
            //     60: aload_1
            //     61: getfield 56	android/os/Message:obj	Ljava/lang/Object;
            //     64: checkcast 58	com/android/server/accessibility/AccessibilityManagerService$Service
            //     67: astore_3
            //     68: aload_3
            //     69: getfield 62	com/android/server/accessibility/AccessibilityManagerService$Service:mResolveInfo	Landroid/content/pm/ResolveInfo;
            //     72: aload_0
            //     73: getfield 17	com/android/server/accessibility/AccessibilityManagerService$MainHanler:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     76: getfield 38	com/android/server/accessibility/AccessibilityManagerService:mContext	Landroid/content/Context;
            //     79: invokevirtual 66	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
            //     82: invokevirtual 72	android/content/pm/ResolveInfo:loadLabel	(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
            //     85: invokevirtual 78	java/lang/Object:toString	()Ljava/lang/String;
            //     88: astore 4
            //     90: aload_0
            //     91: getfield 17	com/android/server/accessibility/AccessibilityManagerService$MainHanler:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     94: getfield 81	com/android/server/accessibility/AccessibilityManagerService:mLock	Ljava/lang/Object;
            //     97: astore 5
            //     99: aload 5
            //     101: monitorenter
            //     102: aload_0
            //     103: getfield 17	com/android/server/accessibility/AccessibilityManagerService$MainHanler:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     106: invokestatic 85	com/android/server/accessibility/AccessibilityManagerService:access$1800	(Lcom/android/server/accessibility/AccessibilityManagerService;)Z
            //     109: ifeq +17 -> 126
            //     112: aload 5
            //     114: monitorexit
            //     115: goto -83 -> 32
            //     118: astore 6
            //     120: aload 5
            //     122: monitorexit
            //     123: aload 6
            //     125: athrow
            //     126: aload_0
            //     127: getfield 17	com/android/server/accessibility/AccessibilityManagerService$MainHanler:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     130: invokestatic 89	com/android/server/accessibility/AccessibilityManagerService:access$1900	(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/app/AlertDialog;
            //     133: ifnull +22 -> 155
            //     136: aload_0
            //     137: getfield 17	com/android/server/accessibility/AccessibilityManagerService$MainHanler:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     140: invokestatic 89	com/android/server/accessibility/AccessibilityManagerService:access$1900	(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/app/AlertDialog;
            //     143: invokevirtual 95	android/app/AlertDialog:isShowing	()Z
            //     146: ifeq +9 -> 155
            //     149: aload 5
            //     151: monitorexit
            //     152: goto -120 -> 32
            //     155: aload_0
            //     156: getfield 17	com/android/server/accessibility/AccessibilityManagerService$MainHanler:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     159: astore 7
            //     161: new 97	android/app/AlertDialog$Builder
            //     164: dup
            //     165: aload_0
            //     166: getfield 17	com/android/server/accessibility/AccessibilityManagerService$MainHanler:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     169: getfield 38	com/android/server/accessibility/AccessibilityManagerService:mContext	Landroid/content/Context;
            //     172: invokespecial 100	android/app/AlertDialog$Builder:<init>	(Landroid/content/Context;)V
            //     175: ldc 101
            //     177: invokevirtual 105	android/app/AlertDialog$Builder:setIcon	(I)Landroid/app/AlertDialog$Builder;
            //     180: ldc 106
            //     182: new 8	com/android/server/accessibility/AccessibilityManagerService$MainHanler$2
            //     185: dup
            //     186: aload_0
            //     187: aload_3
            //     188: invokespecial 109	com/android/server/accessibility/AccessibilityManagerService$MainHanler$2:<init>	(Lcom/android/server/accessibility/AccessibilityManagerService$MainHanler;Lcom/android/server/accessibility/AccessibilityManagerService$Service;)V
            //     191: invokevirtual 113	android/app/AlertDialog$Builder:setPositiveButton	(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
            //     194: ldc 114
            //     196: new 6	com/android/server/accessibility/AccessibilityManagerService$MainHanler$1
            //     199: dup
            //     200: aload_0
            //     201: invokespecial 117	com/android/server/accessibility/AccessibilityManagerService$MainHanler$1:<init>	(Lcom/android/server/accessibility/AccessibilityManagerService$MainHanler;)V
            //     204: invokevirtual 120	android/app/AlertDialog$Builder:setNegativeButton	(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
            //     207: ldc 121
            //     209: invokevirtual 124	android/app/AlertDialog$Builder:setTitle	(I)Landroid/app/AlertDialog$Builder;
            //     212: astore 8
            //     214: aload_0
            //     215: getfield 17	com/android/server/accessibility/AccessibilityManagerService$MainHanler:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     218: getfield 38	com/android/server/accessibility/AccessibilityManagerService:mContext	Landroid/content/Context;
            //     221: astore 9
            //     223: iconst_1
            //     224: anewarray 74	java/lang/Object
            //     227: astore 10
            //     229: aload 10
            //     231: iconst_0
            //     232: aload 4
            //     234: aastore
            //     235: aload 7
            //     237: aload 8
            //     239: aload 9
            //     241: ldc 125
            //     243: aload 10
            //     245: invokevirtual 129	android/content/Context:getString	(I[Ljava/lang/Object;)Ljava/lang/String;
            //     248: invokevirtual 133	android/app/AlertDialog$Builder:setMessage	(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
            //     251: invokevirtual 137	android/app/AlertDialog$Builder:create	()Landroid/app/AlertDialog;
            //     254: invokestatic 141	com/android/server/accessibility/AccessibilityManagerService:access$1902	(Lcom/android/server/accessibility/AccessibilityManagerService;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
            //     257: pop
            //     258: aload_0
            //     259: getfield 17	com/android/server/accessibility/AccessibilityManagerService$MainHanler:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     262: invokestatic 89	com/android/server/accessibility/AccessibilityManagerService:access$1900	(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/app/AlertDialog;
            //     265: invokevirtual 145	android/app/AlertDialog:getWindow	()Landroid/view/Window;
            //     268: sipush 2012
            //     271: invokevirtual 151	android/view/Window:setType	(I)V
            //     274: aload_0
            //     275: getfield 17	com/android/server/accessibility/AccessibilityManagerService$MainHanler:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     278: invokestatic 89	com/android/server/accessibility/AccessibilityManagerService:access$1900	(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/app/AlertDialog;
            //     281: iconst_1
            //     282: invokevirtual 155	android/app/AlertDialog:setCanceledOnTouchOutside	(Z)V
            //     285: aload_0
            //     286: getfield 17	com/android/server/accessibility/AccessibilityManagerService$MainHanler:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     289: invokestatic 89	com/android/server/accessibility/AccessibilityManagerService:access$1900	(Lcom/android/server/accessibility/AccessibilityManagerService;)Landroid/app/AlertDialog;
            //     292: invokevirtual 158	android/app/AlertDialog:show	()V
            //     295: aload 5
            //     297: monitorexit
            //     298: goto -266 -> 32
            //     301: aload_1
            //     302: getfield 56	android/os/Message:obj	Ljava/lang/Object;
            //     305: checkcast 160	android/view/accessibility/AccessibilityEvent
            //     308: astore_2
            //     309: aload_0
            //     310: getfield 17	com/android/server/accessibility/AccessibilityManagerService$MainHanler:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     313: invokestatic 163	com/android/server/accessibility/AccessibilityManagerService:access$2000	(Lcom/android/server/accessibility/AccessibilityManagerService;)Z
            //     316: ifeq +24 -> 340
            //     319: aload_0
            //     320: getfield 17	com/android/server/accessibility/AccessibilityManagerService$MainHanler:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     323: invokestatic 167	com/android/server/accessibility/AccessibilityManagerService:access$2100	(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityInputFilter;
            //     326: ifnull +14 -> 340
            //     329: aload_0
            //     330: getfield 17	com/android/server/accessibility/AccessibilityManagerService$MainHanler:this$0	Lcom/android/server/accessibility/AccessibilityManagerService;
            //     333: invokestatic 167	com/android/server/accessibility/AccessibilityManagerService:access$2100	(Lcom/android/server/accessibility/AccessibilityManagerService;)Lcom/android/server/accessibility/AccessibilityInputFilter;
            //     336: aload_2
            //     337: invokevirtual 173	com/android/server/accessibility/AccessibilityInputFilter:onAccessibilityEvent	(Landroid/view/accessibility/AccessibilityEvent;)V
            //     340: aload_2
            //     341: invokevirtual 176	android/view/accessibility/AccessibilityEvent:recycle	()V
            //     344: goto -312 -> 32
            //
            // Exception table:
            //     from	to	target	type
            //     102	123	118	finally
            //     126	298	118	finally
        }
    }

    private class AccessibilityConnectionWrapper
        implements IBinder.DeathRecipient
    {
        private final IAccessibilityInteractionConnection mConnection;
        private final int mWindowId;

        public AccessibilityConnectionWrapper(int paramIAccessibilityInteractionConnection, IAccessibilityInteractionConnection arg3)
        {
            this.mWindowId = paramIAccessibilityInteractionConnection;
            Object localObject;
            this.mConnection = localObject;
        }

        public void binderDied()
        {
            unlinkToDeath();
            synchronized (AccessibilityManagerService.this.mLock)
            {
                AccessibilityManagerService.this.removeAccessibilityInteractionConnectionLocked(this.mWindowId);
                return;
            }
        }

        public void linkToDeath()
            throws RemoteException
        {
            this.mConnection.asBinder().linkToDeath(this, 0);
        }

        public void unlinkToDeath()
        {
            this.mConnection.asBinder().unlinkToDeath(this, 0);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.accessibility.AccessibilityManagerService
 * JD-Core Version:        0.6.2
 */