package com.android.server;

import android.app.ActivityManagerNative;
import android.app.IActivityManager;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipData.Item;
import android.content.ClipDescription;
import android.content.Context;
import android.content.IClipboard.Stub;
import android.content.IOnPrimaryClipChangedListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.os.UserId;
import android.util.Slog;
import android.util.SparseArray;
import java.util.HashSet;

public class ClipboardService extends IClipboard.Stub
{
    private static final String TAG = "ClipboardService";
    private final IActivityManager mAm;
    private SparseArray<PerUserClipboard> mClipboards = new SparseArray();
    private final Context mContext;
    private final IBinder mPermissionOwner;
    private final PackageManager mPm;

    public ClipboardService(Context paramContext)
    {
        this.mContext = paramContext;
        this.mAm = ActivityManagerNative.getDefault();
        this.mPm = paramContext.getPackageManager();
        Object localObject = null;
        try
        {
            IBinder localIBinder = this.mAm.newUriPermissionOwner("clipboard");
            localObject = localIBinder;
            this.mPermissionOwner = localObject;
            IntentFilter localIntentFilter = new IntentFilter();
            localIntentFilter.addAction("android.intent.action.USER_REMOVED");
            this.mContext.registerReceiver(new BroadcastReceiver()
            {
                public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
                {
                    if ("android.intent.action.USER_REMOVED".equals(paramAnonymousIntent.getAction()))
                        ClipboardService.this.removeClipboard(paramAnonymousIntent.getIntExtra("android.intent.extra.user_id", 0));
                }
            }
            , localIntentFilter);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Slog.w("clipboard", "AM dead", localRemoteException);
        }
    }

    private final void addActiveOwnerLocked(int paramInt, String paramString)
    {
        try
        {
            if (!UserId.isSameApp(this.mPm.getPackageInfo(paramString, 0).applicationInfo.uid, paramInt))
                throw new SecurityException("Calling uid " + paramInt + " does not own package " + paramString);
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            throw new IllegalArgumentException("Unknown package " + paramString, localNameNotFoundException);
        }
        PerUserClipboard localPerUserClipboard = getClipboard();
        if ((localPerUserClipboard.primaryClip != null) && (!localPerUserClipboard.activePermissionOwners.contains(paramString)))
        {
            int i = localPerUserClipboard.primaryClip.getItemCount();
            for (int j = 0; j < i; j++)
                grantItemLocked(localPerUserClipboard.primaryClip.getItemAt(j), paramString);
            localPerUserClipboard.activePermissionOwners.add(paramString);
        }
    }

    private final void checkDataOwnerLocked(ClipData paramClipData, int paramInt)
    {
        int i = paramClipData.getItemCount();
        for (int j = 0; j < i; j++)
            checkItemOwnerLocked(paramClipData.getItemAt(j), paramInt);
    }

    private final void checkItemOwnerLocked(ClipData.Item paramItem, int paramInt)
    {
        if (paramItem.getUri() != null)
            checkUriOwnerLocked(paramItem.getUri(), paramInt);
        Intent localIntent = paramItem.getIntent();
        if ((localIntent != null) && (localIntent.getData() != null))
            checkUriOwnerLocked(localIntent.getData(), paramInt);
    }

    // ERROR //
    private final void checkUriOwnerLocked(android.net.Uri paramUri, int paramInt)
    {
        // Byte code:
        //     0: ldc 213
        //     2: aload_1
        //     3: invokevirtual 218	android/net/Uri:getScheme	()Ljava/lang/String;
        //     6: invokevirtual 223	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     9: ifne +4 -> 13
        //     12: return
        //     13: invokestatic 229	android/os/Binder:clearCallingIdentity	()J
        //     16: lstore_3
        //     17: aload_0
        //     18: getfield 46	com/android/server/ClipboardService:mAm	Landroid/app/IActivityManager;
        //     21: iload_2
        //     22: aconst_null
        //     23: aload_1
        //     24: iconst_1
        //     25: invokeinterface 233 5 0
        //     30: pop
        //     31: lload_3
        //     32: invokestatic 237	android/os/Binder:restoreCallingIdentity	(J)V
        //     35: goto -23 -> 12
        //     38: astore 6
        //     40: lload_3
        //     41: invokestatic 237	android/os/Binder:restoreCallingIdentity	(J)V
        //     44: aload 6
        //     46: athrow
        //     47: astore 5
        //     49: goto -18 -> 31
        //
        // Exception table:
        //     from	to	target	type
        //     17	31	38	finally
        //     17	31	47	android/os/RemoteException
    }

    private final void clearActiveOwnersLocked()
    {
        PerUserClipboard localPerUserClipboard = getClipboard();
        localPerUserClipboard.activePermissionOwners.clear();
        if (localPerUserClipboard.primaryClip == null);
        while (true)
        {
            return;
            int i = localPerUserClipboard.primaryClip.getItemCount();
            for (int j = 0; j < i; j++)
                revokeItemLocked(localPerUserClipboard.primaryClip.getItemAt(j));
        }
    }

    private PerUserClipboard getClipboard()
    {
        return getClipboard(UserId.getCallingUserId());
    }

    private PerUserClipboard getClipboard(int paramInt)
    {
        synchronized (this.mClipboards)
        {
            Slog.i("ClipboardService", "Got clipboard for user=" + paramInt);
            PerUserClipboard localPerUserClipboard = (PerUserClipboard)this.mClipboards.get(paramInt);
            if (localPerUserClipboard == null)
            {
                localPerUserClipboard = new PerUserClipboard(paramInt);
                this.mClipboards.put(paramInt, localPerUserClipboard);
            }
            return localPerUserClipboard;
        }
    }

    private final void grantItemLocked(ClipData.Item paramItem, String paramString)
    {
        if (paramItem.getUri() != null)
            grantUriLocked(paramItem.getUri(), paramString);
        Intent localIntent = paramItem.getIntent();
        if ((localIntent != null) && (localIntent.getData() != null))
            grantUriLocked(localIntent.getData(), paramString);
    }

    // ERROR //
    private final void grantUriLocked(android.net.Uri paramUri, String paramString)
    {
        // Byte code:
        //     0: invokestatic 229	android/os/Binder:clearCallingIdentity	()J
        //     3: lstore_3
        //     4: aload_0
        //     5: getfield 46	com/android/server/ClipboardService:mAm	Landroid/app/IActivityManager;
        //     8: aload_0
        //     9: getfield 64	com/android/server/ClipboardService:mPermissionOwner	Landroid/os/IBinder;
        //     12: invokestatic 276	android/os/Process:myUid	()I
        //     15: aload_2
        //     16: aload_1
        //     17: iconst_1
        //     18: invokeinterface 280 6 0
        //     23: lload_3
        //     24: invokestatic 237	android/os/Binder:restoreCallingIdentity	(J)V
        //     27: return
        //     28: astore 6
        //     30: lload_3
        //     31: invokestatic 237	android/os/Binder:restoreCallingIdentity	(J)V
        //     34: aload 6
        //     36: athrow
        //     37: astore 5
        //     39: goto -16 -> 23
        //
        // Exception table:
        //     from	to	target	type
        //     4	23	28	finally
        //     4	23	37	android/os/RemoteException
    }

    private void removeClipboard(int paramInt)
    {
        synchronized (this.mClipboards)
        {
            this.mClipboards.remove(paramInt);
            return;
        }
    }

    private final void revokeItemLocked(ClipData.Item paramItem)
    {
        if (paramItem.getUri() != null)
            revokeUriLocked(paramItem.getUri());
        Intent localIntent = paramItem.getIntent();
        if ((localIntent != null) && (localIntent.getData() != null))
            revokeUriLocked(localIntent.getData());
    }

    // ERROR //
    private final void revokeUriLocked(android.net.Uri paramUri)
    {
        // Byte code:
        //     0: invokestatic 229	android/os/Binder:clearCallingIdentity	()J
        //     3: lstore_2
        //     4: aload_0
        //     5: getfield 46	com/android/server/ClipboardService:mAm	Landroid/app/IActivityManager;
        //     8: aload_0
        //     9: getfield 64	com/android/server/ClipboardService:mPermissionOwner	Landroid/os/IBinder;
        //     12: aload_1
        //     13: iconst_3
        //     14: invokeinterface 291 4 0
        //     19: lload_2
        //     20: invokestatic 237	android/os/Binder:restoreCallingIdentity	(J)V
        //     23: return
        //     24: astore 5
        //     26: lload_2
        //     27: invokestatic 237	android/os/Binder:restoreCallingIdentity	(J)V
        //     30: aload 5
        //     32: athrow
        //     33: astore 4
        //     35: goto -16 -> 19
        //
        // Exception table:
        //     from	to	target	type
        //     4	19	24	finally
        //     4	19	33	android/os/RemoteException
    }

    public void addPrimaryClipChangedListener(IOnPrimaryClipChangedListener paramIOnPrimaryClipChangedListener)
    {
        try
        {
            getClipboard().primaryClipListeners.register(paramIOnPrimaryClipChangedListener);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public ClipData getPrimaryClip(String paramString)
    {
        try
        {
            addActiveOwnerLocked(Binder.getCallingUid(), paramString);
            ClipData localClipData = getClipboard().primaryClip;
            return localClipData;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public ClipDescription getPrimaryClipDescription()
    {
        while (true)
        {
            try
            {
                PerUserClipboard localPerUserClipboard = getClipboard();
                if (localPerUserClipboard.primaryClip != null)
                {
                    localClipDescription = localPerUserClipboard.primaryClip.getDescription();
                    return localClipDescription;
                }
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
            ClipDescription localClipDescription = null;
        }
    }

    public boolean hasClipboardText()
    {
        boolean bool = false;
        try
        {
            PerUserClipboard localPerUserClipboard = getClipboard();
            if (localPerUserClipboard.primaryClip != null)
            {
                CharSequence localCharSequence = localPerUserClipboard.primaryClip.getItemAt(0).getText();
                if ((localCharSequence != null) && (localCharSequence.length() > 0))
                    bool = true;
            }
            else;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
        return bool;
    }

    public boolean hasPrimaryClip()
    {
        while (true)
        {
            try
            {
                if (getClipboard().primaryClip != null)
                {
                    bool = true;
                    return bool;
                }
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
            boolean bool = false;
        }
    }

    public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
        throws RemoteException
    {
        try
        {
            boolean bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            return bool;
        }
        catch (RuntimeException localRuntimeException)
        {
            Slog.w("clipboard", "Exception: ", localRuntimeException);
            throw localRuntimeException;
        }
    }

    public void removePrimaryClipChangedListener(IOnPrimaryClipChangedListener paramIOnPrimaryClipChangedListener)
    {
        try
        {
            getClipboard().primaryClipListeners.unregister(paramIOnPrimaryClipChangedListener);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    // ERROR //
    public void setPrimaryClip(ClipData paramClipData)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_1
        //     3: ifnull +28 -> 31
        //     6: aload_1
        //     7: invokevirtual 175	android/content/ClipData:getItemCount	()I
        //     10: ifgt +21 -> 31
        //     13: new 146	java/lang/IllegalArgumentException
        //     16: dup
        //     17: ldc_w 343
        //     20: invokespecial 344	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     23: athrow
        //     24: astore 5
        //     26: aload_0
        //     27: monitorexit
        //     28: aload 5
        //     30: athrow
        //     31: aload_0
        //     32: aload_1
        //     33: invokestatic 308	android/os/Binder:getCallingUid	()I
        //     36: invokespecial 346	com/android/server/ClipboardService:checkDataOwnerLocked	(Landroid/content/ClipData;I)V
        //     39: aload_0
        //     40: invokespecial 348	com/android/server/ClipboardService:clearActiveOwnersLocked	()V
        //     43: aload_0
        //     44: invokespecial 155	com/android/server/ClipboardService:getClipboard	()Lcom/android/server/ClipboardService$PerUserClipboard;
        //     47: astore_2
        //     48: aload_2
        //     49: aload_1
        //     50: putfield 159	com/android/server/ClipboardService$PerUserClipboard:primaryClip	Landroid/content/ClipData;
        //     53: aload_2
        //     54: getfield 297	com/android/server/ClipboardService$PerUserClipboard:primaryClipListeners	Landroid/os/RemoteCallbackList;
        //     57: invokevirtual 351	android/os/RemoteCallbackList:beginBroadcast	()I
        //     60: istore_3
        //     61: iconst_0
        //     62: istore 4
        //     64: iload 4
        //     66: iload_3
        //     67: if_icmpge +26 -> 93
        //     70: aload_2
        //     71: getfield 297	com/android/server/ClipboardService$PerUserClipboard:primaryClipListeners	Landroid/os/RemoteCallbackList;
        //     74: iload 4
        //     76: invokevirtual 355	android/os/RemoteCallbackList:getBroadcastItem	(I)Landroid/os/IInterface;
        //     79: checkcast 357	android/content/IOnPrimaryClipChangedListener
        //     82: invokeinterface 360 1 0
        //     87: iinc 4 1
        //     90: goto -26 -> 64
        //     93: aload_2
        //     94: getfield 297	com/android/server/ClipboardService$PerUserClipboard:primaryClipListeners	Landroid/os/RemoteCallbackList;
        //     97: invokevirtual 363	android/os/RemoteCallbackList:finishBroadcast	()V
        //     100: aload_0
        //     101: monitorexit
        //     102: return
        //     103: astore 6
        //     105: goto -18 -> 87
        //
        // Exception table:
        //     from	to	target	type
        //     6	28	24	finally
        //     31	61	24	finally
        //     70	87	24	finally
        //     93	102	24	finally
        //     70	87	103	android/os/RemoteException
    }

    private class PerUserClipboard
    {
        final HashSet<String> activePermissionOwners = new HashSet();
        ClipData primaryClip;
        final RemoteCallbackList<IOnPrimaryClipChangedListener> primaryClipListeners = new RemoteCallbackList();
        final int userId;

        PerUserClipboard(int arg2)
        {
            int i;
            this.userId = i;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.ClipboardService
 * JD-Core Version:        0.6.2
 */