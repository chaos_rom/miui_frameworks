package com.android.server;

import android.content.Context;
import android.net.INetworkManagementEventObserver;
import android.net.InterfaceConfiguration;
import android.net.LinkAddress;
import android.net.NetworkStats;
import android.net.NetworkUtils;
import android.net.RouteInfo;
import android.net.wifi.WifiConfiguration;
import android.os.Handler;
import android.os.INetworkManagementService.Stub;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.util.Log;
import android.util.Slog;
import android.util.SparseBooleanArray;
import com.android.internal.net.NetworkStatsFactory;
import com.google.android.collect.Maps;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class NetworkManagementService extends INetworkManagementService.Stub
    implements Watchdog.Monitor
{
    private static final String ADD = "add";
    private static final boolean DBG = false;
    private static final String DEFAULT = "default";
    public static final String LIMIT_GLOBAL_ALERT = "globalAlert";
    private static final String NETD_TAG = "NetdConnector";
    private static final String REMOVE = "remove";
    private static final String SECONDARY = "secondary";
    private static final String TAG = "NetworkManagementService";
    private HashMap<String, Long> mActiveAlerts = Maps.newHashMap();
    private HashMap<String, Long> mActiveQuotas = Maps.newHashMap();
    private volatile boolean mBandwidthControlEnabled;
    private CountDownLatch mConnectedSignal = new CountDownLatch(1);
    private NativeDaemonConnector mConnector;
    private Context mContext;
    private final Handler mMainHandler = new Handler();
    private final RemoteCallbackList<INetworkManagementEventObserver> mObservers = new RemoteCallbackList();
    private Object mQuotaLock = new Object();
    private final NetworkStatsFactory mStatsFactory = new NetworkStatsFactory();
    private Thread mThread;
    private SparseBooleanArray mUidRejectOnQuota = new SparseBooleanArray();

    private NetworkManagementService(Context paramContext)
    {
        this.mContext = paramContext;
        if ("simulator".equals(SystemProperties.get("ro.product.device")));
        while (true)
        {
            return;
            this.mConnector = new NativeDaemonConnector(new NetdCallbackReceiver(null), "netd", 10, "NetdConnector", 160);
            this.mThread = new Thread(this.mConnector, "NetdConnector");
            Watchdog.getInstance().addMonitor(this);
        }
    }

    public static NetworkManagementService create(Context paramContext)
        throws InterruptedException
    {
        NetworkManagementService localNetworkManagementService = new NetworkManagementService(paramContext);
        CountDownLatch localCountDownLatch = localNetworkManagementService.mConnectedSignal;
        localNetworkManagementService.mThread.start();
        localCountDownLatch.await();
        return localNetworkManagementService;
    }

    // ERROR //
    private int getInterfaceThrottle(String paramString, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 140	com/android/server/NetworkManagementService:mConnector	Lcom/android/server/NativeDaemonConnector;
        //     4: astore 4
        //     6: iconst_3
        //     7: anewarray 92	java/lang/Object
        //     10: astore 5
        //     12: aload 5
        //     14: iconst_0
        //     15: ldc 214
        //     17: aastore
        //     18: aload 5
        //     20: iconst_1
        //     21: aload_1
        //     22: aastore
        //     23: iload_2
        //     24: ifeq +49 -> 73
        //     27: ldc 216
        //     29: astore 6
        //     31: aload 5
        //     33: iconst_2
        //     34: aload 6
        //     36: aastore
        //     37: aload 4
        //     39: ldc 218
        //     41: aload 5
        //     43: invokevirtual 222	com/android/server/NativeDaemonConnector:execute	(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
        //     46: astore 7
        //     48: iload_2
        //     49: ifeq +37 -> 86
        //     52: aload 7
        //     54: sipush 218
        //     57: invokevirtual 227	com/android/server/NativeDaemonEvent:checkCode	(I)V
        //     60: aload 7
        //     62: invokevirtual 231	com/android/server/NativeDaemonEvent:getMessage	()Ljava/lang/String;
        //     65: invokestatic 237	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     68: istore 9
        //     70: iload 9
        //     72: ireturn
        //     73: ldc 239
        //     75: astore 6
        //     77: goto -46 -> 31
        //     80: astore_3
        //     81: aload_3
        //     82: invokevirtual 243	com/android/server/NativeDaemonConnectorException:rethrowAsParcelableException	()Ljava/lang/IllegalArgumentException;
        //     85: athrow
        //     86: aload 7
        //     88: sipush 219
        //     91: invokevirtual 227	com/android/server/NativeDaemonEvent:checkCode	(I)V
        //     94: goto -34 -> 60
        //     97: astore 8
        //     99: new 245	java/lang/IllegalStateException
        //     102: dup
        //     103: new 247	java/lang/StringBuilder
        //     106: dup
        //     107: invokespecial 248	java/lang/StringBuilder:<init>	()V
        //     110: ldc 250
        //     112: invokevirtual 254	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     115: aload 7
        //     117: invokevirtual 257	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     120: invokevirtual 260	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     123: invokespecial 262	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     126: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     0	48	80	com/android/server/NativeDaemonConnectorException
        //     73	77	80	com/android/server/NativeDaemonConnectorException
        //     60	70	97	java/lang/NumberFormatException
    }

    // ERROR //
    private android.net.NetworkStats.Entry getNetworkStatsTethering(String paramString1, String paramString2)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 140	com/android/server/NetworkManagementService:mConnector	Lcom/android/server/NativeDaemonConnector;
        //     4: astore 4
        //     6: iconst_3
        //     7: anewarray 92	java/lang/Object
        //     10: astore 5
        //     12: aload 5
        //     14: iconst_0
        //     15: ldc_w 266
        //     18: aastore
        //     19: aload 5
        //     21: iconst_1
        //     22: aload_1
        //     23: aastore
        //     24: aload 5
        //     26: iconst_2
        //     27: aload_2
        //     28: aastore
        //     29: aload 4
        //     31: ldc_w 268
        //     34: aload 5
        //     36: invokevirtual 222	com/android/server/NativeDaemonConnector:execute	(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
        //     39: astore 6
        //     41: aload 6
        //     43: sipush 221
        //     46: invokevirtual 227	com/android/server/NativeDaemonEvent:checkCode	(I)V
        //     49: new 270	java/util/StringTokenizer
        //     52: dup
        //     53: aload 6
        //     55: invokevirtual 231	com/android/server/NativeDaemonEvent:getMessage	()Ljava/lang/String;
        //     58: invokespecial 271	java/util/StringTokenizer:<init>	(Ljava/lang/String;)V
        //     61: astore 7
        //     63: aload 7
        //     65: invokevirtual 274	java/util/StringTokenizer:nextToken	()Ljava/lang/String;
        //     68: pop
        //     69: aload 7
        //     71: invokevirtual 274	java/util/StringTokenizer:nextToken	()Ljava/lang/String;
        //     74: pop
        //     75: new 276	android/net/NetworkStats$Entry
        //     78: dup
        //     79: invokespecial 277	android/net/NetworkStats$Entry:<init>	()V
        //     82: astore 10
        //     84: aload 10
        //     86: aload_1
        //     87: putfield 280	android/net/NetworkStats$Entry:iface	Ljava/lang/String;
        //     90: aload 10
        //     92: bipush 251
        //     94: putfield 284	android/net/NetworkStats$Entry:uid	I
        //     97: aload 10
        //     99: iconst_0
        //     100: putfield 287	android/net/NetworkStats$Entry:set	I
        //     103: aload 10
        //     105: iconst_0
        //     106: putfield 290	android/net/NetworkStats$Entry:tag	I
        //     109: aload 10
        //     111: aload 7
        //     113: invokevirtual 274	java/util/StringTokenizer:nextToken	()Ljava/lang/String;
        //     116: invokestatic 296	java/lang/Long:parseLong	(Ljava/lang/String;)J
        //     119: putfield 300	android/net/NetworkStats$Entry:rxBytes	J
        //     122: aload 10
        //     124: aload 7
        //     126: invokevirtual 274	java/util/StringTokenizer:nextToken	()Ljava/lang/String;
        //     129: invokestatic 296	java/lang/Long:parseLong	(Ljava/lang/String;)J
        //     132: putfield 303	android/net/NetworkStats$Entry:rxPackets	J
        //     135: aload 10
        //     137: aload 7
        //     139: invokevirtual 274	java/util/StringTokenizer:nextToken	()Ljava/lang/String;
        //     142: invokestatic 296	java/lang/Long:parseLong	(Ljava/lang/String;)J
        //     145: putfield 306	android/net/NetworkStats$Entry:txBytes	J
        //     148: aload 10
        //     150: aload 7
        //     152: invokevirtual 274	java/util/StringTokenizer:nextToken	()Ljava/lang/String;
        //     155: invokestatic 296	java/lang/Long:parseLong	(Ljava/lang/String;)J
        //     158: putfield 309	android/net/NetworkStats$Entry:txPackets	J
        //     161: aload 10
        //     163: areturn
        //     164: astore_3
        //     165: aload_3
        //     166: invokevirtual 243	com/android/server/NativeDaemonConnectorException:rethrowAsParcelableException	()Ljava/lang/IllegalArgumentException;
        //     169: athrow
        //     170: astore 11
        //     172: new 245	java/lang/IllegalStateException
        //     175: dup
        //     176: new 247	java/lang/StringBuilder
        //     179: dup
        //     180: invokespecial 248	java/lang/StringBuilder:<init>	()V
        //     183: ldc_w 311
        //     186: invokevirtual 254	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     189: aload_1
        //     190: invokevirtual 254	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     193: ldc_w 313
        //     196: invokevirtual 254	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     199: aload_2
        //     200: invokevirtual 254	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     203: ldc_w 315
        //     206: invokevirtual 254	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     209: aload 11
        //     211: invokevirtual 257	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     214: invokevirtual 260	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     217: invokespecial 262	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     220: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     0	41	164	com/android/server/NativeDaemonConnectorException
        //     75	161	170	java/lang/NumberFormatException
    }

    private static String getSecurityType(WifiConfiguration paramWifiConfiguration)
    {
        String str;
        switch (paramWifiConfiguration.getAuthType())
        {
        case 2:
        case 3:
        default:
            str = "open";
        case 1:
        case 4:
        }
        while (true)
        {
            return str;
            str = "wpa-psk";
            continue;
            str = "wpa2-psk";
        }
    }

    private void modifyNat(String paramString1, String paramString2, String paramString3)
        throws SocketException
    {
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = paramString1;
        arrayOfObject[1] = paramString2;
        arrayOfObject[2] = paramString3;
        NativeDaemonConnector.Command localCommand = new NativeDaemonConnector.Command("nat", arrayOfObject);
        NetworkInterface localNetworkInterface = NetworkInterface.getByName(paramString2);
        if (localNetworkInterface == null)
            localCommand.appendArg("0");
        try
        {
            this.mConnector.execute(localCommand);
            return;
            List localList = localNetworkInterface.getInterfaceAddresses();
            localCommand.appendArg(Integer.valueOf(localList.size()));
            Iterator localIterator = localList.iterator();
            while (localIterator.hasNext())
            {
                InterfaceAddress localInterfaceAddress = (InterfaceAddress)localIterator.next();
                InetAddress localInetAddress = NetworkUtils.getNetworkPart(localInterfaceAddress.getAddress(), localInterfaceAddress.getNetworkPrefixLength());
                localCommand.appendArg(localInetAddress.getHostAddress() + "/" + localInterfaceAddress.getNetworkPrefixLength());
            }
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    private void modifyRoute(String paramString1, String paramString2, RouteInfo paramRouteInfo, String paramString3)
    {
        Object[] arrayOfObject = new Object[4];
        arrayOfObject[0] = "route";
        arrayOfObject[1] = paramString2;
        arrayOfObject[2] = paramString1;
        arrayOfObject[3] = paramString3;
        NativeDaemonConnector.Command localCommand = new NativeDaemonConnector.Command("interface", arrayOfObject);
        LinkAddress localLinkAddress = paramRouteInfo.getDestination();
        localCommand.appendArg(localLinkAddress.getAddress().getHostAddress());
        localCommand.appendArg(Integer.valueOf(localLinkAddress.getNetworkPrefixLength()));
        if (paramRouteInfo.getGateway() == null)
            if ((localLinkAddress.getAddress() instanceof Inet4Address))
                localCommand.appendArg("0.0.0.0");
        try
        {
            while (true)
            {
                this.mConnector.execute(localCommand);
                return;
                localCommand.appendArg("::0");
                continue;
                localCommand.appendArg(paramRouteInfo.getGateway().getHostAddress());
            }
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    private void notifyInterfaceAdded(String paramString)
    {
        int i = this.mObservers.beginBroadcast();
        int j = 0;
        while (true)
        {
            if (j < i);
            try
            {
                ((INetworkManagementEventObserver)this.mObservers.getBroadcastItem(j)).interfaceAdded(paramString);
                label32: j++;
                continue;
                this.mObservers.finishBroadcast();
                return;
            }
            catch (RemoteException localRemoteException)
            {
                break label32;
            }
        }
    }

    private void notifyInterfaceLinkStateChanged(String paramString, boolean paramBoolean)
    {
        int i = this.mObservers.beginBroadcast();
        int j = 0;
        while (true)
        {
            if (j < i);
            try
            {
                ((INetworkManagementEventObserver)this.mObservers.getBroadcastItem(j)).interfaceLinkStateChanged(paramString, paramBoolean);
                label36: j++;
                continue;
                this.mObservers.finishBroadcast();
                return;
            }
            catch (RemoteException localRemoteException)
            {
                break label36;
            }
        }
    }

    private void notifyInterfaceRemoved(String paramString)
    {
        this.mActiveAlerts.remove(paramString);
        this.mActiveQuotas.remove(paramString);
        int i = this.mObservers.beginBroadcast();
        int j = 0;
        while (true)
        {
            if (j < i);
            try
            {
                ((INetworkManagementEventObserver)this.mObservers.getBroadcastItem(j)).interfaceRemoved(paramString);
                label55: j++;
                continue;
                this.mObservers.finishBroadcast();
                return;
            }
            catch (RemoteException localRemoteException)
            {
                break label55;
            }
        }
    }

    private void notifyInterfaceStatusChanged(String paramString, boolean paramBoolean)
    {
        int i = this.mObservers.beginBroadcast();
        int j = 0;
        while (true)
        {
            if (j < i);
            try
            {
                ((INetworkManagementEventObserver)this.mObservers.getBroadcastItem(j)).interfaceStatusChanged(paramString, paramBoolean);
                label36: j++;
                continue;
                this.mObservers.finishBroadcast();
                return;
            }
            catch (RemoteException localRemoteException)
            {
                break label36;
            }
        }
    }

    private void notifyLimitReached(String paramString1, String paramString2)
    {
        int i = this.mObservers.beginBroadcast();
        int j = 0;
        while (true)
        {
            if (j < i);
            try
            {
                ((INetworkManagementEventObserver)this.mObservers.getBroadcastItem(j)).limitReached(paramString1, paramString2);
                label36: j++;
                continue;
                this.mObservers.finishBroadcast();
                return;
            }
            catch (RemoteException localRemoteException)
            {
                break label36;
            }
        }
    }

    // ERROR //
    private void prepareNativeDaemon()
    {
        // Byte code:
        //     0: aload_0
        //     1: iconst_0
        //     2: putfield 468	com/android/server/NetworkManagementService:mBandwidthControlEnabled	Z
        //     5: new 470	java/io/File
        //     8: dup
        //     9: ldc_w 472
        //     12: invokespecial 473	java/io/File:<init>	(Ljava/lang/String;)V
        //     15: invokevirtual 476	java/io/File:exists	()Z
        //     18: ifeq +219 -> 237
        //     21: ldc 39
        //     23: ldc_w 478
        //     26: invokestatic 484	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     29: pop
        //     30: aload_0
        //     31: getfield 140	com/android/server/NetworkManagementService:mConnector	Lcom/android/server/NativeDaemonConnector;
        //     34: astore 22
        //     36: iconst_1
        //     37: anewarray 92	java/lang/Object
        //     40: astore 23
        //     42: aload 23
        //     44: iconst_0
        //     45: ldc_w 486
        //     48: aastore
        //     49: aload 22
        //     51: ldc_w 268
        //     54: aload 23
        //     56: invokevirtual 222	com/android/server/NativeDaemonConnector:execute	(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
        //     59: pop
        //     60: aload_0
        //     61: iconst_1
        //     62: putfield 468	com/android/server/NetworkManagementService:mBandwidthControlEnabled	Z
        //     65: aload_0
        //     66: getfield 468	com/android/server/NetworkManagementService:mBandwidthControlEnabled	Z
        //     69: ifeq +180 -> 249
        //     72: ldc_w 488
        //     75: astore_2
        //     76: ldc_w 490
        //     79: aload_2
        //     80: invokestatic 492	android/os/SystemProperties:set	(Ljava/lang/String;Ljava/lang/String;)V
        //     83: aload_0
        //     84: getfield 95	com/android/server/NetworkManagementService:mQuotaLock	Ljava/lang/Object;
        //     87: astore_3
        //     88: aload_3
        //     89: monitorenter
        //     90: aload_0
        //     91: getfield 103	com/android/server/NetworkManagementService:mActiveQuotas	Ljava/util/HashMap;
        //     94: invokevirtual 493	java/util/HashMap:size	()I
        //     97: istore 5
        //     99: iload 5
        //     101: ifle +155 -> 256
        //     104: ldc 39
        //     106: new 247	java/lang/StringBuilder
        //     109: dup
        //     110: invokespecial 248	java/lang/StringBuilder:<init>	()V
        //     113: ldc_w 495
        //     116: invokevirtual 254	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     119: iload 5
        //     121: invokevirtual 408	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     124: ldc_w 497
        //     127: invokevirtual 254	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     130: invokevirtual 260	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     133: invokestatic 484	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     136: pop
        //     137: aload_0
        //     138: getfield 103	com/android/server/NetworkManagementService:mActiveQuotas	Ljava/util/HashMap;
        //     141: astore 16
        //     143: aload_0
        //     144: invokestatic 101	com/google/android/collect/Maps:newHashMap	()Ljava/util/HashMap;
        //     147: putfield 103	com/android/server/NetworkManagementService:mActiveQuotas	Ljava/util/HashMap;
        //     150: aload 16
        //     152: invokevirtual 501	java/util/HashMap:entrySet	()Ljava/util/Set;
        //     155: invokeinterface 504 1 0
        //     160: astore 17
        //     162: aload 17
        //     164: invokeinterface 378 1 0
        //     169: ifeq +87 -> 256
        //     172: aload 17
        //     174: invokeinterface 382 1 0
        //     179: checkcast 506	java/util/Map$Entry
        //     182: astore 18
        //     184: aload_0
        //     185: aload 18
        //     187: invokeinterface 509 1 0
        //     192: checkcast 124	java/lang/String
        //     195: aload 18
        //     197: invokeinterface 512 1 0
        //     202: checkcast 292	java/lang/Long
        //     205: invokevirtual 516	java/lang/Long:longValue	()J
        //     208: invokevirtual 520	com/android/server/NetworkManagementService:setInterfaceQuota	(Ljava/lang/String;J)V
        //     211: goto -49 -> 162
        //     214: astore 4
        //     216: aload_3
        //     217: monitorexit
        //     218: aload 4
        //     220: athrow
        //     221: astore 20
        //     223: ldc 39
        //     225: ldc_w 522
        //     228: aload 20
        //     230: invokestatic 528	android/util/Log:wtf	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     233: pop
        //     234: goto -169 -> 65
        //     237: ldc 39
        //     239: ldc_w 530
        //     242: invokestatic 484	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     245: pop
        //     246: goto -181 -> 65
        //     249: ldc_w 348
        //     252: astore_2
        //     253: goto -177 -> 76
        //     256: aload_0
        //     257: getfield 105	com/android/server/NetworkManagementService:mActiveAlerts	Ljava/util/HashMap;
        //     260: invokevirtual 493	java/util/HashMap:size	()I
        //     263: istore 6
        //     265: iload 6
        //     267: ifle +113 -> 380
        //     270: ldc 39
        //     272: new 247	java/lang/StringBuilder
        //     275: dup
        //     276: invokespecial 248	java/lang/StringBuilder:<init>	()V
        //     279: ldc_w 495
        //     282: invokevirtual 254	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     285: iload 6
        //     287: invokevirtual 408	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     290: ldc_w 532
        //     293: invokevirtual 254	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     296: invokevirtual 260	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     299: invokestatic 484	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     302: pop
        //     303: aload_0
        //     304: getfield 105	com/android/server/NetworkManagementService:mActiveAlerts	Ljava/util/HashMap;
        //     307: astore 12
        //     309: aload_0
        //     310: invokestatic 101	com/google/android/collect/Maps:newHashMap	()Ljava/util/HashMap;
        //     313: putfield 105	com/android/server/NetworkManagementService:mActiveAlerts	Ljava/util/HashMap;
        //     316: aload 12
        //     318: invokevirtual 501	java/util/HashMap:entrySet	()Ljava/util/Set;
        //     321: invokeinterface 504 1 0
        //     326: astore 13
        //     328: aload 13
        //     330: invokeinterface 378 1 0
        //     335: ifeq +45 -> 380
        //     338: aload 13
        //     340: invokeinterface 382 1 0
        //     345: checkcast 506	java/util/Map$Entry
        //     348: astore 14
        //     350: aload_0
        //     351: aload 14
        //     353: invokeinterface 509 1 0
        //     358: checkcast 124	java/lang/String
        //     361: aload 14
        //     363: invokeinterface 512 1 0
        //     368: checkcast 292	java/lang/Long
        //     371: invokevirtual 516	java/lang/Long:longValue	()J
        //     374: invokevirtual 535	com/android/server/NetworkManagementService:setInterfaceAlert	(Ljava/lang/String;J)V
        //     377: goto -49 -> 328
        //     380: aload_0
        //     381: getfield 110	com/android/server/NetworkManagementService:mUidRejectOnQuota	Landroid/util/SparseBooleanArray;
        //     384: invokevirtual 536	android/util/SparseBooleanArray:size	()I
        //     387: istore 7
        //     389: iload 7
        //     391: ifle +90 -> 481
        //     394: ldc 39
        //     396: new 247	java/lang/StringBuilder
        //     399: dup
        //     400: invokespecial 248	java/lang/StringBuilder:<init>	()V
        //     403: ldc_w 495
        //     406: invokevirtual 254	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     409: iload 7
        //     411: invokevirtual 408	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     414: ldc_w 538
        //     417: invokevirtual 254	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     420: invokevirtual 260	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     423: invokestatic 484	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     426: pop
        //     427: aload_0
        //     428: getfield 110	com/android/server/NetworkManagementService:mUidRejectOnQuota	Landroid/util/SparseBooleanArray;
        //     431: astore 9
        //     433: aload_0
        //     434: new 107	android/util/SparseBooleanArray
        //     437: dup
        //     438: invokespecial 108	android/util/SparseBooleanArray:<init>	()V
        //     441: putfield 110	com/android/server/NetworkManagementService:mUidRejectOnQuota	Landroid/util/SparseBooleanArray;
        //     444: iconst_0
        //     445: istore 10
        //     447: iload 10
        //     449: aload 9
        //     451: invokevirtual 536	android/util/SparseBooleanArray:size	()I
        //     454: if_icmpge +27 -> 481
        //     457: aload_0
        //     458: aload 9
        //     460: iload 10
        //     462: invokevirtual 542	android/util/SparseBooleanArray:keyAt	(I)I
        //     465: aload 9
        //     467: iload 10
        //     469: invokevirtual 546	android/util/SparseBooleanArray:valueAt	(I)Z
        //     472: invokevirtual 550	com/android/server/NetworkManagementService:setUidNetworkRules	(IZ)V
        //     475: iinc 10 1
        //     478: goto -31 -> 447
        //     481: aload_3
        //     482: monitorexit
        //     483: return
        //
        // Exception table:
        //     from	to	target	type
        //     90	218	214	finally
        //     256	483	214	finally
        //     30	65	221	com/android/server/NativeDaemonConnectorException
    }

    // ERROR //
    private ArrayList<String> readRouteList(String paramString)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: new 556	java/util/ArrayList
        //     5: dup
        //     6: invokespecial 557	java/util/ArrayList:<init>	()V
        //     9: astore_3
        //     10: new 559	java/io/FileInputStream
        //     13: dup
        //     14: aload_1
        //     15: invokespecial 560	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
        //     18: astore 4
        //     20: new 562	java/io/BufferedReader
        //     23: dup
        //     24: new 564	java/io/InputStreamReader
        //     27: dup
        //     28: new 566	java/io/DataInputStream
        //     31: dup
        //     32: aload 4
        //     34: invokespecial 569	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
        //     37: invokespecial 570	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
        //     40: invokespecial 573	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
        //     43: astore 5
        //     45: aload 5
        //     47: invokevirtual 576	java/io/BufferedReader:readLine	()Ljava/lang/String;
        //     50: astore 10
        //     52: aload 10
        //     54: ifnull +36 -> 90
        //     57: aload 10
        //     59: invokevirtual 579	java/lang/String:length	()I
        //     62: ifeq +28 -> 90
        //     65: aload_3
        //     66: aload 10
        //     68: invokevirtual 581	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     71: pop
        //     72: goto -27 -> 45
        //     75: astore 8
        //     77: aload 4
        //     79: astore_2
        //     80: aload_2
        //     81: ifnull +7 -> 88
        //     84: aload_2
        //     85: invokevirtual 584	java/io/FileInputStream:close	()V
        //     88: aload_3
        //     89: areturn
        //     90: aload 4
        //     92: ifnull +8 -> 100
        //     95: aload 4
        //     97: invokevirtual 584	java/io/FileInputStream:close	()V
        //     100: goto -12 -> 88
        //     103: astore 6
        //     105: aload_2
        //     106: ifnull +7 -> 113
        //     109: aload_2
        //     110: invokevirtual 584	java/io/FileInputStream:close	()V
        //     113: aload 6
        //     115: athrow
        //     116: astore 9
        //     118: goto -30 -> 88
        //     121: astore 7
        //     123: goto -10 -> 113
        //     126: astore 11
        //     128: goto -28 -> 100
        //     131: astore 6
        //     133: aload 4
        //     135: astore_2
        //     136: goto -31 -> 105
        //     139: astore 13
        //     141: goto -61 -> 80
        //
        // Exception table:
        //     from	to	target	type
        //     20	72	75	java/io/IOException
        //     10	20	103	finally
        //     84	88	116	java/io/IOException
        //     109	113	121	java/io/IOException
        //     95	100	126	java/io/IOException
        //     20	72	131	finally
        //     10	20	139	java/io/IOException
    }

    public void addRoute(String paramString, RouteInfo paramRouteInfo)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        modifyRoute(paramString, "add", paramRouteInfo, "default");
    }

    public void addSecondaryRoute(String paramString, RouteInfo paramRouteInfo)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        modifyRoute(paramString, "add", paramRouteInfo, "secondary");
    }

    public void attachPppd(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[6];
            arrayOfObject[0] = "attach";
            arrayOfObject[1] = paramString1;
            arrayOfObject[2] = NetworkUtils.numericToInetAddress(paramString2).getHostAddress();
            arrayOfObject[3] = NetworkUtils.numericToInetAddress(paramString3).getHostAddress();
            arrayOfObject[4] = NetworkUtils.numericToInetAddress(paramString4).getHostAddress();
            arrayOfObject[5] = NetworkUtils.numericToInetAddress(paramString5).getHostAddress();
            localNativeDaemonConnector.execute("pppd", arrayOfObject);
            return;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    public void clearInterfaceAddresses(String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = "clearaddrs";
            arrayOfObject[1] = paramString;
            localNativeDaemonConnector.execute("interface", arrayOfObject);
            return;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    public void detachPppd(String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = "detach";
            arrayOfObject[1] = paramString;
            localNativeDaemonConnector.execute("pppd", arrayOfObject);
            return;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    public void disableIpv6(String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[3];
            arrayOfObject[0] = "ipv6";
            arrayOfObject[1] = paramString;
            arrayOfObject[2] = "disable";
            localNativeDaemonConnector.execute("interface", arrayOfObject);
            return;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    public void disableNat(String paramString1, String paramString2)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        try
        {
            modifyNat("disable", paramString1, paramString2);
            return;
        }
        catch (SocketException localSocketException)
        {
            throw new IllegalStateException(localSocketException);
        }
    }

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.DUMP", "NetworkManagementService");
        paramPrintWriter.println("NetworkManagementService NativeDaemonConnector Log:");
        this.mConnector.dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
        paramPrintWriter.println();
        paramPrintWriter.print("Bandwidth control enabled: ");
        paramPrintWriter.println(this.mBandwidthControlEnabled);
        synchronized (this.mQuotaLock)
        {
            paramPrintWriter.print("Active quota ifaces: ");
            paramPrintWriter.println(this.mActiveQuotas.toString());
            paramPrintWriter.print("Active alert ifaces: ");
            paramPrintWriter.println(this.mActiveAlerts.toString());
        }
        synchronized (this.mUidRejectOnQuota)
        {
            paramPrintWriter.print("UID reject on quota ifaces: [");
            int i = this.mUidRejectOnQuota.size();
            int j = 0;
            while (j < i)
            {
                paramPrintWriter.print(this.mUidRejectOnQuota.keyAt(j));
                if (j < i - 1)
                    paramPrintWriter.print(",");
                j++;
                continue;
                localObject2 = finally;
                throw localObject2;
            }
            paramPrintWriter.println("]");
            return;
        }
    }

    public void enableIpv6(String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[3];
            arrayOfObject[0] = "ipv6";
            arrayOfObject[1] = paramString;
            arrayOfObject[2] = "enable";
            localNativeDaemonConnector.execute("interface", arrayOfObject);
            return;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    public void enableNat(String paramString1, String paramString2)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        try
        {
            modifyNat("enable", paramString1, paramString2);
            return;
        }
        catch (SocketException localSocketException)
        {
            throw new IllegalStateException(localSocketException);
        }
    }

    public void flushDefaultDnsCache()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = "flushdefaultif";
            localNativeDaemonConnector.execute("resolver", arrayOfObject);
            return;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    public void flushInterfaceDnsCache(String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = "flushif";
            arrayOfObject[1] = paramString;
            localNativeDaemonConnector.execute("resolver", arrayOfObject);
            return;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    public String[] getDnsForwarders()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = "dns";
            arrayOfObject[1] = "list";
            String[] arrayOfString = NativeDaemonEvent.filterMessageList(localNativeDaemonConnector.executeForList("tether", arrayOfObject), 112);
            return arrayOfString;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    // ERROR //
    public InterfaceConfiguration getInterfaceConfig(String paramString)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 112	com/android/server/NetworkManagementService:mContext	Landroid/content/Context;
        //     4: ldc_w 588
        //     7: ldc 39
        //     9: invokevirtual 593	android/content/Context:enforceCallingOrSelfPermission	(Ljava/lang/String;Ljava/lang/String;)V
        //     12: aload_0
        //     13: getfield 140	com/android/server/NetworkManagementService:mConnector	Lcom/android/server/NativeDaemonConnector;
        //     16: astore_3
        //     17: iconst_2
        //     18: anewarray 92	java/lang/Object
        //     21: astore 4
        //     23: aload 4
        //     25: iconst_0
        //     26: ldc_w 693
        //     29: aastore
        //     30: aload 4
        //     32: iconst_1
        //     33: aload_1
        //     34: aastore
        //     35: aload_3
        //     36: ldc 218
        //     38: aload 4
        //     40: invokevirtual 222	com/android/server/NativeDaemonConnector:execute	(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
        //     43: astore 5
        //     45: aload 5
        //     47: sipush 213
        //     50: invokevirtual 227	com/android/server/NativeDaemonEvent:checkCode	(I)V
        //     53: new 270	java/util/StringTokenizer
        //     56: dup
        //     57: aload 5
        //     59: invokevirtual 231	com/android/server/NativeDaemonEvent:getMessage	()Ljava/lang/String;
        //     62: invokespecial 271	java/util/StringTokenizer:<init>	(Ljava/lang/String;)V
        //     65: astore 6
        //     67: new 695	android/net/InterfaceConfiguration
        //     70: dup
        //     71: invokespecial 696	android/net/InterfaceConfiguration:<init>	()V
        //     74: astore 7
        //     76: aload 7
        //     78: aload 6
        //     80: ldc_w 313
        //     83: invokevirtual 698	java/util/StringTokenizer:nextToken	(Ljava/lang/String;)Ljava/lang/String;
        //     86: invokevirtual 701	android/net/InterfaceConfiguration:setHardwareAddress	(Ljava/lang/String;)V
        //     89: aconst_null
        //     90: astore 9
        //     92: iconst_0
        //     93: istore 10
        //     95: aload 6
        //     97: invokevirtual 274	java/util/StringTokenizer:nextToken	()Ljava/lang/String;
        //     100: invokestatic 604	android/net/NetworkUtils:numericToInetAddress	(Ljava/lang/String;)Ljava/net/InetAddress;
        //     103: astore 16
        //     105: aload 16
        //     107: astore 9
        //     109: aload 6
        //     111: invokevirtual 274	java/util/StringTokenizer:nextToken	()Ljava/lang/String;
        //     114: invokestatic 237	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     117: istore 15
        //     119: iload 15
        //     121: istore 10
        //     123: aload 7
        //     125: new 420	android/net/LinkAddress
        //     128: dup
        //     129: aload 9
        //     131: iload 10
        //     133: invokespecial 704	android/net/LinkAddress:<init>	(Ljava/net/InetAddress;I)V
        //     136: invokevirtual 708	android/net/InterfaceConfiguration:setLinkAddress	(Landroid/net/LinkAddress;)V
        //     139: aload 6
        //     141: invokevirtual 711	java/util/StringTokenizer:hasMoreTokens	()Z
        //     144: ifeq +85 -> 229
        //     147: aload 7
        //     149: aload 6
        //     151: invokevirtual 274	java/util/StringTokenizer:nextToken	()Ljava/lang/String;
        //     154: invokevirtual 714	android/net/InterfaceConfiguration:setFlag	(Ljava/lang/String;)V
        //     157: goto -18 -> 139
        //     160: astore 8
        //     162: new 245	java/lang/IllegalStateException
        //     165: dup
        //     166: new 247	java/lang/StringBuilder
        //     169: dup
        //     170: invokespecial 248	java/lang/StringBuilder:<init>	()V
        //     173: ldc_w 716
        //     176: invokevirtual 254	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     179: aload 5
        //     181: invokevirtual 257	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     184: invokevirtual 260	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     187: invokespecial 262	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     190: athrow
        //     191: astore_2
        //     192: aload_2
        //     193: invokevirtual 243	com/android/server/NativeDaemonConnectorException:rethrowAsParcelableException	()Ljava/lang/IllegalArgumentException;
        //     196: athrow
        //     197: astore 11
        //     199: ldc 39
        //     201: ldc_w 718
        //     204: aload 11
        //     206: invokestatic 721	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     209: pop
        //     210: goto -101 -> 109
        //     213: astore 13
        //     215: ldc 39
        //     217: ldc_w 723
        //     220: aload 13
        //     222: invokestatic 721	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     225: pop
        //     226: goto -103 -> 123
        //     229: aload 7
        //     231: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     67	89	160	java/util/NoSuchElementException
        //     95	105	160	java/util/NoSuchElementException
        //     109	119	160	java/util/NoSuchElementException
        //     123	157	160	java/util/NoSuchElementException
        //     199	226	160	java/util/NoSuchElementException
        //     12	45	191	com/android/server/NativeDaemonConnectorException
        //     95	105	197	java/lang/IllegalArgumentException
        //     109	119	213	java/lang/NumberFormatException
    }

    public int getInterfaceRxThrottle(String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        return getInterfaceThrottle(paramString, true);
    }

    public int getInterfaceTxThrottle(String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        return getInterfaceThrottle(paramString, false);
    }

    public boolean getIpForwardingEnabled()
        throws IllegalStateException
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = "status";
            NativeDaemonEvent localNativeDaemonEvent = localNativeDaemonConnector.execute("ipfwd", arrayOfObject);
            localNativeDaemonEvent.checkCode(211);
            return localNativeDaemonEvent.getMessage().endsWith("enabled");
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    public NetworkStats getNetworkStatsDetail()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        return this.mStatsFactory.readNetworkStatsDetail(-1);
    }

    public NetworkStats getNetworkStatsSummaryDev()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        return this.mStatsFactory.readNetworkStatsSummaryDev();
    }

    public NetworkStats getNetworkStatsSummaryXt()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        return this.mStatsFactory.readNetworkStatsSummaryXt();
    }

    public NetworkStats getNetworkStatsTethering(String[] paramArrayOfString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        if (paramArrayOfString.length % 2 != 0)
            throw new IllegalArgumentException("unexpected ifacePairs; length=" + paramArrayOfString.length);
        NetworkStats localNetworkStats = new NetworkStats(SystemClock.elapsedRealtime(), 1);
        for (int i = 0; i < paramArrayOfString.length; i += 2)
        {
            String str1 = paramArrayOfString[i];
            String str2 = paramArrayOfString[(i + 1)];
            if ((str1 != null) && (str2 != null))
                localNetworkStats.combineValues(getNetworkStatsTethering(str1, str2));
        }
        return localNetworkStats;
    }

    public NetworkStats getNetworkStatsUidDetail(int paramInt)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        return this.mStatsFactory.readNetworkStatsDetail(paramInt);
    }

    public RouteInfo[] getRoutes(String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        ArrayList localArrayList = new ArrayList();
        Iterator localIterator1 = readRouteList("/proc/net/route").iterator();
        while (localIterator1.hasNext())
        {
            String str5 = (String)localIterator1.next();
            String[] arrayOfString2 = str5.split("\t");
            if ((arrayOfString2.length > 7) && (paramString.equals(arrayOfString2[0])))
            {
                String str6 = arrayOfString2[1];
                String str7 = arrayOfString2[2];
                arrayOfString2[3];
                String str8 = arrayOfString2[7];
                try
                {
                    LinkAddress localLinkAddress2 = new LinkAddress(NetworkUtils.intToInetAddress((int)Long.parseLong(str6, 16)), NetworkUtils.netmaskIntToPrefixLength((int)Long.parseLong(str8, 16)));
                    InetAddress localInetAddress2 = NetworkUtils.intToInetAddress((int)Long.parseLong(str7, 16));
                    RouteInfo localRouteInfo2 = new RouteInfo(localLinkAddress2, localInetAddress2);
                    localArrayList.add(localRouteInfo2);
                }
                catch (Exception localException2)
                {
                    Log.e("NetworkManagementService", "Error parsing route " + str5 + " : " + localException2);
                }
            }
        }
        Iterator localIterator2 = readRouteList("/proc/net/ipv6_route").iterator();
        while (localIterator2.hasNext())
        {
            String str1 = (String)localIterator2.next();
            String[] arrayOfString1 = str1.split("\\s+");
            if ((arrayOfString1.length > 9) && (paramString.equals(arrayOfString1[9].trim())))
            {
                String str2 = arrayOfString1[0];
                String str3 = arrayOfString1[1];
                String str4 = arrayOfString1[4];
                try
                {
                    int i = Integer.parseInt(str3, 16);
                    LinkAddress localLinkAddress1 = new LinkAddress(NetworkUtils.hexToInet6Address(str2), i);
                    InetAddress localInetAddress1 = NetworkUtils.hexToInet6Address(str4);
                    RouteInfo localRouteInfo1 = new RouteInfo(localLinkAddress1, localInetAddress1);
                    localArrayList.add(localRouteInfo1);
                }
                catch (Exception localException1)
                {
                    Log.e("NetworkManagementService", "Error parsing route " + str1 + " : " + localException1);
                }
            }
        }
        return (RouteInfo[])localArrayList.toArray(new RouteInfo[localArrayList.size()]);
    }

    public boolean isBandwidthControlEnabled()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        return this.mBandwidthControlEnabled;
    }

    public boolean isTetheringStarted()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = "status";
            NativeDaemonEvent localNativeDaemonEvent = localNativeDaemonConnector.execute("tether", arrayOfObject);
            localNativeDaemonEvent.checkCode(210);
            return localNativeDaemonEvent.getMessage().endsWith("started");
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    public String[] listInterfaces()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = "list";
            String[] arrayOfString = NativeDaemonEvent.filterMessageList(localNativeDaemonConnector.executeForList("interface", arrayOfObject), 110);
            return arrayOfString;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    public String[] listTetheredInterfaces()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = "interface";
            arrayOfObject[1] = "list";
            String[] arrayOfString = NativeDaemonEvent.filterMessageList(localNativeDaemonConnector.executeForList("tether", arrayOfObject), 111);
            return arrayOfString;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    public String[] listTtys()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        try
        {
            String[] arrayOfString = NativeDaemonEvent.filterMessageList(this.mConnector.executeForList("list_ttys", new Object[0]), 113);
            return arrayOfString;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    public void monitor()
    {
        if (this.mConnector != null)
            this.mConnector.monitor();
    }

    public void registerObserver(INetworkManagementEventObserver paramINetworkManagementEventObserver)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        this.mObservers.register(paramINetworkManagementEventObserver);
    }

    // ERROR //
    public void removeInterfaceAlert(String paramString)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 112	com/android/server/NetworkManagementService:mContext	Landroid/content/Context;
        //     4: ldc_w 588
        //     7: ldc 39
        //     9: invokevirtual 593	android/content/Context:enforceCallingOrSelfPermission	(Ljava/lang/String;Ljava/lang/String;)V
        //     12: aload_0
        //     13: getfield 468	com/android/server/NetworkManagementService:mBandwidthControlEnabled	Z
        //     16: ifne +4 -> 20
        //     19: return
        //     20: aload_0
        //     21: getfield 95	com/android/server/NetworkManagementService:mQuotaLock	Ljava/lang/Object;
        //     24: astore_2
        //     25: aload_2
        //     26: monitorenter
        //     27: aload_0
        //     28: getfield 105	com/android/server/NetworkManagementService:mActiveAlerts	Ljava/util/HashMap;
        //     31: aload_1
        //     32: invokevirtual 849	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
        //     35: ifne +13 -> 48
        //     38: aload_2
        //     39: monitorexit
        //     40: goto -21 -> 19
        //     43: astore_3
        //     44: aload_2
        //     45: monitorexit
        //     46: aload_3
        //     47: athrow
        //     48: aload_0
        //     49: getfield 140	com/android/server/NetworkManagementService:mConnector	Lcom/android/server/NativeDaemonConnector;
        //     52: astore 5
        //     54: iconst_2
        //     55: anewarray 92	java/lang/Object
        //     58: astore 6
        //     60: aload 6
        //     62: iconst_0
        //     63: ldc_w 851
        //     66: aastore
        //     67: aload 6
        //     69: iconst_1
        //     70: aload_1
        //     71: aastore
        //     72: aload 5
        //     74: ldc_w 268
        //     77: aload 6
        //     79: invokevirtual 222	com/android/server/NativeDaemonConnector:execute	(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
        //     82: pop
        //     83: aload_0
        //     84: getfield 105	com/android/server/NetworkManagementService:mActiveAlerts	Ljava/util/HashMap;
        //     87: aload_1
        //     88: invokevirtual 457	java/util/HashMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
        //     91: pop
        //     92: aload_2
        //     93: monitorexit
        //     94: goto -75 -> 19
        //     97: astore 4
        //     99: aload 4
        //     101: invokevirtual 243	com/android/server/NativeDaemonConnectorException:rethrowAsParcelableException	()Ljava/lang/IllegalArgumentException;
        //     104: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     27	46	43	finally
        //     48	92	43	finally
        //     92	105	43	finally
        //     48	92	97	com/android/server/NativeDaemonConnectorException
    }

    // ERROR //
    public void removeInterfaceQuota(String paramString)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 112	com/android/server/NetworkManagementService:mContext	Landroid/content/Context;
        //     4: ldc_w 588
        //     7: ldc 39
        //     9: invokevirtual 593	android/content/Context:enforceCallingOrSelfPermission	(Ljava/lang/String;Ljava/lang/String;)V
        //     12: aload_0
        //     13: getfield 468	com/android/server/NetworkManagementService:mBandwidthControlEnabled	Z
        //     16: ifne +4 -> 20
        //     19: return
        //     20: aload_0
        //     21: getfield 95	com/android/server/NetworkManagementService:mQuotaLock	Ljava/lang/Object;
        //     24: astore_2
        //     25: aload_2
        //     26: monitorenter
        //     27: aload_0
        //     28: getfield 103	com/android/server/NetworkManagementService:mActiveQuotas	Ljava/util/HashMap;
        //     31: aload_1
        //     32: invokevirtual 849	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
        //     35: ifne +13 -> 48
        //     38: aload_2
        //     39: monitorexit
        //     40: goto -21 -> 19
        //     43: astore_3
        //     44: aload_2
        //     45: monitorexit
        //     46: aload_3
        //     47: athrow
        //     48: aload_0
        //     49: getfield 103	com/android/server/NetworkManagementService:mActiveQuotas	Ljava/util/HashMap;
        //     52: aload_1
        //     53: invokevirtual 457	java/util/HashMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
        //     56: pop
        //     57: aload_0
        //     58: getfield 105	com/android/server/NetworkManagementService:mActiveAlerts	Ljava/util/HashMap;
        //     61: aload_1
        //     62: invokevirtual 457	java/util/HashMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
        //     65: pop
        //     66: aload_0
        //     67: getfield 140	com/android/server/NetworkManagementService:mConnector	Lcom/android/server/NativeDaemonConnector;
        //     70: astore 7
        //     72: iconst_2
        //     73: anewarray 92	java/lang/Object
        //     76: astore 8
        //     78: aload 8
        //     80: iconst_0
        //     81: ldc_w 854
        //     84: aastore
        //     85: aload 8
        //     87: iconst_1
        //     88: aload_1
        //     89: aastore
        //     90: aload 7
        //     92: ldc_w 268
        //     95: aload 8
        //     97: invokevirtual 222	com/android/server/NativeDaemonConnector:execute	(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
        //     100: pop
        //     101: aload_2
        //     102: monitorexit
        //     103: goto -84 -> 19
        //     106: astore 6
        //     108: aload 6
        //     110: invokevirtual 243	com/android/server/NativeDaemonConnectorException:rethrowAsParcelableException	()Ljava/lang/IllegalArgumentException;
        //     113: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     27	46	43	finally
        //     48	66	43	finally
        //     66	101	43	finally
        //     101	114	43	finally
        //     66	101	106	com/android/server/NativeDaemonConnectorException
    }

    public void removeRoute(String paramString, RouteInfo paramRouteInfo)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        modifyRoute(paramString, "remove", paramRouteInfo, "default");
    }

    public void removeSecondaryRoute(String paramString, RouteInfo paramRouteInfo)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        modifyRoute(paramString, "remove", paramRouteInfo, "secondary");
    }

    public void setAccessPoint(WifiConfiguration paramWifiConfiguration, String paramString1, String paramString2)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        if (paramWifiConfiguration == null);
        try
        {
            NativeDaemonConnector localNativeDaemonConnector2 = this.mConnector;
            Object[] arrayOfObject2 = new Object[3];
            arrayOfObject2[0] = "set";
            arrayOfObject2[1] = paramString1;
            arrayOfObject2[2] = paramString2;
            localNativeDaemonConnector2.execute("softap", arrayOfObject2);
            return;
            NativeDaemonConnector localNativeDaemonConnector1 = this.mConnector;
            Object[] arrayOfObject1 = new Object[6];
            arrayOfObject1[0] = "set";
            arrayOfObject1[1] = paramString1;
            arrayOfObject1[2] = paramString2;
            arrayOfObject1[3] = paramWifiConfiguration.SSID;
            arrayOfObject1[4] = getSecurityType(paramWifiConfiguration);
            arrayOfObject1[5] = paramWifiConfiguration.preSharedKey;
            localNativeDaemonConnector1.execute("softap", arrayOfObject1);
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    public void setDefaultInterfaceForDns(String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = "setdefaultif";
            arrayOfObject[1] = paramString;
            localNativeDaemonConnector.execute("resolver", arrayOfObject);
            return;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    public void setDnsForwarders(String[] paramArrayOfString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = "dns";
        arrayOfObject[1] = "set";
        NativeDaemonConnector.Command localCommand = new NativeDaemonConnector.Command("tether", arrayOfObject);
        int i = paramArrayOfString.length;
        for (int j = 0; j < i; j++)
            localCommand.appendArg(NetworkUtils.numericToInetAddress(paramArrayOfString[j]).getHostAddress());
        try
        {
            this.mConnector.execute(localCommand);
            return;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    public void setDnsServersForInterface(String paramString, String[] paramArrayOfString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = "setifdns";
        arrayOfObject[1] = paramString;
        NativeDaemonConnector.Command localCommand = new NativeDaemonConnector.Command("resolver", arrayOfObject);
        int i = paramArrayOfString.length;
        for (int j = 0; j < i; j++)
        {
            InetAddress localInetAddress = NetworkUtils.numericToInetAddress(paramArrayOfString[j]);
            if (!localInetAddress.isAnyLocalAddress())
                localCommand.appendArg(localInetAddress.getHostAddress());
        }
        try
        {
            this.mConnector.execute(localCommand);
            return;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    public void setGlobalAlert(long paramLong)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        if (!this.mBandwidthControlEnabled);
        while (true)
        {
            return;
            try
            {
                NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
                Object[] arrayOfObject = new Object[2];
                arrayOfObject[0] = "setglobalalert";
                arrayOfObject[1] = Long.valueOf(paramLong);
                localNativeDaemonConnector.execute("bandwidth", arrayOfObject);
            }
            catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
            {
                throw localNativeDaemonConnectorException.rethrowAsParcelableException();
            }
        }
    }

    // ERROR //
    public void setInterfaceAlert(String paramString, long paramLong)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 112	com/android/server/NetworkManagementService:mContext	Landroid/content/Context;
        //     4: ldc_w 588
        //     7: ldc 39
        //     9: invokevirtual 593	android/content/Context:enforceCallingOrSelfPermission	(Ljava/lang/String;Ljava/lang/String;)V
        //     12: aload_0
        //     13: getfield 468	com/android/server/NetworkManagementService:mBandwidthControlEnabled	Z
        //     16: ifne +4 -> 20
        //     19: return
        //     20: aload_0
        //     21: getfield 103	com/android/server/NetworkManagementService:mActiveQuotas	Ljava/util/HashMap;
        //     24: aload_1
        //     25: invokevirtual 849	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
        //     28: ifne +14 -> 42
        //     31: new 245	java/lang/IllegalStateException
        //     34: dup
        //     35: ldc_w 890
        //     38: invokespecial 262	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     41: athrow
        //     42: aload_0
        //     43: getfield 95	com/android/server/NetworkManagementService:mQuotaLock	Ljava/lang/Object;
        //     46: astore 4
        //     48: aload 4
        //     50: monitorenter
        //     51: aload_0
        //     52: getfield 105	com/android/server/NetworkManagementService:mActiveAlerts	Ljava/util/HashMap;
        //     55: aload_1
        //     56: invokevirtual 849	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
        //     59: ifeq +45 -> 104
        //     62: new 245	java/lang/IllegalStateException
        //     65: dup
        //     66: new 247	java/lang/StringBuilder
        //     69: dup
        //     70: invokespecial 248	java/lang/StringBuilder:<init>	()V
        //     73: ldc_w 892
        //     76: invokevirtual 254	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     79: aload_1
        //     80: invokevirtual 254	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     83: ldc_w 894
        //     86: invokevirtual 254	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     89: invokevirtual 260	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     92: invokespecial 262	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     95: athrow
        //     96: astore 5
        //     98: aload 4
        //     100: monitorexit
        //     101: aload 5
        //     103: athrow
        //     104: aload_0
        //     105: getfield 140	com/android/server/NetworkManagementService:mConnector	Lcom/android/server/NativeDaemonConnector;
        //     108: astore 7
        //     110: iconst_3
        //     111: anewarray 92	java/lang/Object
        //     114: astore 8
        //     116: aload 8
        //     118: iconst_0
        //     119: ldc_w 896
        //     122: aastore
        //     123: aload 8
        //     125: iconst_1
        //     126: aload_1
        //     127: aastore
        //     128: aload 8
        //     130: iconst_2
        //     131: lload_2
        //     132: invokestatic 888	java/lang/Long:valueOf	(J)Ljava/lang/Long;
        //     135: aastore
        //     136: aload 7
        //     138: ldc_w 268
        //     141: aload 8
        //     143: invokevirtual 222	com/android/server/NativeDaemonConnector:execute	(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
        //     146: pop
        //     147: aload_0
        //     148: getfield 105	com/android/server/NetworkManagementService:mActiveAlerts	Ljava/util/HashMap;
        //     151: aload_1
        //     152: lload_2
        //     153: invokestatic 888	java/lang/Long:valueOf	(J)Ljava/lang/Long;
        //     156: invokevirtual 900	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     159: pop
        //     160: aload 4
        //     162: monitorexit
        //     163: goto -144 -> 19
        //     166: astore 6
        //     168: aload 6
        //     170: invokevirtual 243	com/android/server/NativeDaemonConnectorException:rethrowAsParcelableException	()Ljava/lang/IllegalArgumentException;
        //     173: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     51	101	96	finally
        //     104	160	96	finally
        //     160	174	96	finally
        //     104	160	166	com/android/server/NativeDaemonConnectorException
    }

    public void setInterfaceConfig(String paramString, InterfaceConfiguration paramInterfaceConfiguration)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        LinkAddress localLinkAddress = paramInterfaceConfiguration.getLinkAddress();
        if ((localLinkAddress == null) || (localLinkAddress.getAddress() == null))
            throw new IllegalStateException("Null LinkAddress given");
        Object[] arrayOfObject = new Object[4];
        arrayOfObject[0] = "setcfg";
        arrayOfObject[1] = paramString;
        arrayOfObject[2] = localLinkAddress.getAddress().getHostAddress();
        arrayOfObject[3] = Integer.valueOf(localLinkAddress.getNetworkPrefixLength());
        NativeDaemonConnector.Command localCommand = new NativeDaemonConnector.Command("interface", arrayOfObject);
        Iterator localIterator = paramInterfaceConfiguration.getFlags().iterator();
        while (localIterator.hasNext())
            localCommand.appendArg((String)localIterator.next());
        try
        {
            this.mConnector.execute(localCommand);
            return;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    public void setInterfaceDown(String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        InterfaceConfiguration localInterfaceConfiguration = getInterfaceConfig(paramString);
        localInterfaceConfiguration.setInterfaceDown();
        setInterfaceConfig(paramString, localInterfaceConfiguration);
    }

    public void setInterfaceIpv6PrivacyExtensions(String paramString, boolean paramBoolean)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[3];
            arrayOfObject[0] = "ipv6privacyextensions";
            arrayOfObject[1] = paramString;
            if (paramBoolean);
            for (String str = "enable"; ; str = "disable")
            {
                arrayOfObject[2] = str;
                localNativeDaemonConnector.execute("interface", arrayOfObject);
                return;
            }
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    // ERROR //
    public void setInterfaceQuota(String paramString, long paramLong)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 112	com/android/server/NetworkManagementService:mContext	Landroid/content/Context;
        //     4: ldc_w 588
        //     7: ldc 39
        //     9: invokevirtual 593	android/content/Context:enforceCallingOrSelfPermission	(Ljava/lang/String;Ljava/lang/String;)V
        //     12: aload_0
        //     13: getfield 468	com/android/server/NetworkManagementService:mBandwidthControlEnabled	Z
        //     16: ifne +4 -> 20
        //     19: return
        //     20: aload_0
        //     21: getfield 95	com/android/server/NetworkManagementService:mQuotaLock	Ljava/lang/Object;
        //     24: astore 4
        //     26: aload 4
        //     28: monitorenter
        //     29: aload_0
        //     30: getfield 103	com/android/server/NetworkManagementService:mActiveQuotas	Ljava/util/HashMap;
        //     33: aload_1
        //     34: invokevirtual 849	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
        //     37: ifeq +45 -> 82
        //     40: new 245	java/lang/IllegalStateException
        //     43: dup
        //     44: new 247	java/lang/StringBuilder
        //     47: dup
        //     48: invokespecial 248	java/lang/StringBuilder:<init>	()V
        //     51: ldc_w 892
        //     54: invokevirtual 254	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     57: aload_1
        //     58: invokevirtual 254	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     61: ldc_w 928
        //     64: invokevirtual 254	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     67: invokevirtual 260	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     70: invokespecial 262	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     73: athrow
        //     74: astore 5
        //     76: aload 4
        //     78: monitorexit
        //     79: aload 5
        //     81: athrow
        //     82: aload_0
        //     83: getfield 140	com/android/server/NetworkManagementService:mConnector	Lcom/android/server/NativeDaemonConnector;
        //     86: astore 7
        //     88: iconst_3
        //     89: anewarray 92	java/lang/Object
        //     92: astore 8
        //     94: aload 8
        //     96: iconst_0
        //     97: ldc_w 930
        //     100: aastore
        //     101: aload 8
        //     103: iconst_1
        //     104: aload_1
        //     105: aastore
        //     106: aload 8
        //     108: iconst_2
        //     109: lload_2
        //     110: invokestatic 888	java/lang/Long:valueOf	(J)Ljava/lang/Long;
        //     113: aastore
        //     114: aload 7
        //     116: ldc_w 268
        //     119: aload 8
        //     121: invokevirtual 222	com/android/server/NativeDaemonConnector:execute	(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
        //     124: pop
        //     125: aload_0
        //     126: getfield 103	com/android/server/NetworkManagementService:mActiveQuotas	Ljava/util/HashMap;
        //     129: aload_1
        //     130: lload_2
        //     131: invokestatic 888	java/lang/Long:valueOf	(J)Ljava/lang/Long;
        //     134: invokevirtual 900	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     137: pop
        //     138: aload 4
        //     140: monitorexit
        //     141: goto -122 -> 19
        //     144: astore 6
        //     146: aload 6
        //     148: invokevirtual 243	com/android/server/NativeDaemonConnectorException:rethrowAsParcelableException	()Ljava/lang/IllegalArgumentException;
        //     151: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     29	79	74	finally
        //     82	138	74	finally
        //     138	152	74	finally
        //     82	138	144	com/android/server/NativeDaemonConnectorException
    }

    public void setInterfaceThrottle(String paramString, int paramInt1, int paramInt2)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[4];
            arrayOfObject[0] = "setthrottle";
            arrayOfObject[1] = paramString;
            arrayOfObject[2] = Integer.valueOf(paramInt1);
            arrayOfObject[3] = Integer.valueOf(paramInt2);
            localNativeDaemonConnector.execute("interface", arrayOfObject);
            return;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    public void setInterfaceUp(String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        InterfaceConfiguration localInterfaceConfiguration = getInterfaceConfig(paramString);
        localInterfaceConfiguration.setInterfaceUp();
        setInterfaceConfig(paramString, localInterfaceConfiguration);
    }

    public void setIpForwardingEnabled(boolean paramBoolean)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[1];
            if (paramBoolean);
            for (String str = "enable"; ; str = "disable")
            {
                arrayOfObject[0] = str;
                localNativeDaemonConnector.execute("ipfwd", arrayOfObject);
                return;
            }
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    // ERROR //
    public void setUidNetworkRules(int paramInt, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 112	com/android/server/NetworkManagementService:mContext	Landroid/content/Context;
        //     4: ldc_w 588
        //     7: ldc 39
        //     9: invokevirtual 593	android/content/Context:enforceCallingOrSelfPermission	(Ljava/lang/String;Ljava/lang/String;)V
        //     12: aload_0
        //     13: getfield 468	com/android/server/NetworkManagementService:mBandwidthControlEnabled	Z
        //     16: ifne +4 -> 20
        //     19: return
        //     20: aload_0
        //     21: getfield 95	com/android/server/NetworkManagementService:mQuotaLock	Ljava/lang/Object;
        //     24: astore_3
        //     25: aload_3
        //     26: monitorenter
        //     27: aload_0
        //     28: getfield 110	com/android/server/NetworkManagementService:mUidRejectOnQuota	Landroid/util/SparseBooleanArray;
        //     31: iload_1
        //     32: iconst_0
        //     33: invokevirtual 941	android/util/SparseBooleanArray:get	(IZ)Z
        //     36: iload_2
        //     37: if_icmpne +15 -> 52
        //     40: aload_3
        //     41: monitorexit
        //     42: goto -23 -> 19
        //     45: astore 4
        //     47: aload_3
        //     48: monitorexit
        //     49: aload 4
        //     51: athrow
        //     52: aload_0
        //     53: getfield 140	com/android/server/NetworkManagementService:mConnector	Lcom/android/server/NativeDaemonConnector;
        //     56: astore 6
        //     58: iconst_2
        //     59: anewarray 92	java/lang/Object
        //     62: astore 7
        //     64: iload_2
        //     65: ifeq +51 -> 116
        //     68: ldc_w 943
        //     71: astore 8
        //     73: aload 7
        //     75: iconst_0
        //     76: aload 8
        //     78: aastore
        //     79: aload 7
        //     81: iconst_1
        //     82: iload_1
        //     83: invokestatic 368	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     86: aastore
        //     87: aload 6
        //     89: ldc_w 268
        //     92: aload 7
        //     94: invokevirtual 222	com/android/server/NativeDaemonConnector:execute	(Ljava/lang/String;[Ljava/lang/Object;)Lcom/android/server/NativeDaemonEvent;
        //     97: pop
        //     98: iload_2
        //     99: ifeq +25 -> 124
        //     102: aload_0
        //     103: getfield 110	com/android/server/NetworkManagementService:mUidRejectOnQuota	Landroid/util/SparseBooleanArray;
        //     106: iload_1
        //     107: iconst_1
        //     108: invokevirtual 945	android/util/SparseBooleanArray:put	(IZ)V
        //     111: aload_3
        //     112: monitorexit
        //     113: goto -94 -> 19
        //     116: ldc_w 947
        //     119: astore 8
        //     121: goto -48 -> 73
        //     124: aload_0
        //     125: getfield 110	com/android/server/NetworkManagementService:mUidRejectOnQuota	Landroid/util/SparseBooleanArray;
        //     128: iload_1
        //     129: invokevirtual 950	android/util/SparseBooleanArray:delete	(I)V
        //     132: goto -21 -> 111
        //     135: astore 5
        //     137: aload 5
        //     139: invokevirtual 243	com/android/server/NativeDaemonConnectorException:rethrowAsParcelableException	()Ljava/lang/IllegalArgumentException;
        //     142: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     27	49	45	finally
        //     52	111	45	finally
        //     111	113	45	finally
        //     116	132	45	finally
        //     137	143	45	finally
        //     52	111	135	com/android/server/NativeDaemonConnectorException
        //     116	132	135	com/android/server/NativeDaemonConnectorException
    }

    public void shutdown()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.SHUTDOWN", "NetworkManagementService");
        Slog.d("NetworkManagementService", "Shutting down");
    }

    public void startAccessPoint(WifiConfiguration paramWifiConfiguration, String paramString1, String paramString2)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        try
        {
            wifiFirmwareReload(paramString1, "AP");
            if (paramWifiConfiguration == null)
            {
                NativeDaemonConnector localNativeDaemonConnector3 = this.mConnector;
                Object[] arrayOfObject3 = new Object[3];
                arrayOfObject3[0] = "set";
                arrayOfObject3[1] = paramString1;
                arrayOfObject3[2] = paramString2;
                localNativeDaemonConnector3.execute("softap", arrayOfObject3);
            }
            while (true)
            {
                NativeDaemonConnector localNativeDaemonConnector2 = this.mConnector;
                Object[] arrayOfObject2 = new Object[1];
                arrayOfObject2[0] = "startap";
                localNativeDaemonConnector2.execute("softap", arrayOfObject2);
                return;
                NativeDaemonConnector localNativeDaemonConnector1 = this.mConnector;
                Object[] arrayOfObject1 = new Object[6];
                arrayOfObject1[0] = "set";
                arrayOfObject1[1] = paramString1;
                arrayOfObject1[2] = paramString2;
                arrayOfObject1[3] = paramWifiConfiguration.SSID;
                arrayOfObject1[4] = getSecurityType(paramWifiConfiguration);
                arrayOfObject1[5] = paramWifiConfiguration.preSharedKey;
                localNativeDaemonConnector1.execute("softap", arrayOfObject1);
            }
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    public void startTethering(String[] paramArrayOfString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = "start";
        NativeDaemonConnector.Command localCommand = new NativeDaemonConnector.Command("tether", arrayOfObject);
        int i = paramArrayOfString.length;
        for (int j = 0; j < i; j++)
            localCommand.appendArg(paramArrayOfString[j]);
        try
        {
            this.mConnector.execute(localCommand);
            return;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    public void stopAccessPoint(String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        try
        {
            NativeDaemonConnector localNativeDaemonConnector1 = this.mConnector;
            Object[] arrayOfObject1 = new Object[1];
            arrayOfObject1[0] = "stopap";
            localNativeDaemonConnector1.execute("softap", arrayOfObject1);
            NativeDaemonConnector localNativeDaemonConnector2 = this.mConnector;
            Object[] arrayOfObject2 = new Object[2];
            arrayOfObject2[0] = "stop";
            arrayOfObject2[1] = paramString;
            localNativeDaemonConnector2.execute("softap", arrayOfObject2);
            wifiFirmwareReload(paramString, "STA");
            return;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    public void stopTethering()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = "stop";
            localNativeDaemonConnector.execute("tether", arrayOfObject);
            return;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    public void systemReady()
    {
        prepareNativeDaemon();
    }

    public void tetherInterface(String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[3];
            arrayOfObject[0] = "interface";
            arrayOfObject[1] = "add";
            arrayOfObject[2] = paramString;
            localNativeDaemonConnector.execute("tether", arrayOfObject);
            return;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    public void unregisterObserver(INetworkManagementEventObserver paramINetworkManagementEventObserver)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        this.mObservers.unregister(paramINetworkManagementEventObserver);
    }

    public void untetherInterface(String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[3];
            arrayOfObject[0] = "interface";
            arrayOfObject[1] = "remove";
            arrayOfObject[2] = paramString;
            localNativeDaemonConnector.execute("tether", arrayOfObject);
            return;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    public void wifiFirmwareReload(String paramString1, String paramString2)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkManagementService");
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[3];
            arrayOfObject[0] = "fwreload";
            arrayOfObject[1] = paramString1;
            arrayOfObject[2] = paramString2;
            localNativeDaemonConnector.execute("softap", arrayOfObject);
            return;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            throw localNativeDaemonConnectorException.rethrowAsParcelableException();
        }
    }

    private class NetdCallbackReceiver
        implements INativeDaemonConnectorCallbacks
    {
        private NetdCallbackReceiver()
        {
        }

        public void onDaemonConnected()
        {
            if (NetworkManagementService.this.mConnectedSignal != null)
            {
                NetworkManagementService.this.mConnectedSignal.countDown();
                NetworkManagementService.access$102(NetworkManagementService.this, null);
            }
            while (true)
            {
                return;
                NetworkManagementService.this.mMainHandler.post(new Runnable()
                {
                    public void run()
                    {
                        NetworkManagementService.this.prepareNativeDaemon();
                    }
                });
            }
        }

        public boolean onEvent(int paramInt, String paramString, String[] paramArrayOfString)
        {
            boolean bool = true;
            switch (paramInt)
            {
            default:
                bool = false;
            case 600:
            case 601:
            }
            while (true)
            {
                return bool;
                if ((paramArrayOfString.length < 4) || (!paramArrayOfString[bool].equals("Iface")))
                {
                    Object[] arrayOfObject3 = new Object[bool];
                    arrayOfObject3[0] = paramString;
                    throw new IllegalStateException(String.format("Invalid event from daemon (%s)", arrayOfObject3));
                }
                if (paramArrayOfString[2].equals("added"))
                {
                    NetworkManagementService.this.notifyInterfaceAdded(paramArrayOfString[3]);
                }
                else if (paramArrayOfString[2].equals("removed"))
                {
                    NetworkManagementService.this.notifyInterfaceRemoved(paramArrayOfString[3]);
                }
                else if ((paramArrayOfString[2].equals("changed")) && (paramArrayOfString.length == 5))
                {
                    NetworkManagementService.this.notifyInterfaceStatusChanged(paramArrayOfString[3], paramArrayOfString[4].equals("up"));
                }
                else if ((paramArrayOfString[2].equals("linkstate")) && (paramArrayOfString.length == 5))
                {
                    NetworkManagementService.this.notifyInterfaceLinkStateChanged(paramArrayOfString[3], paramArrayOfString[4].equals("up"));
                }
                else
                {
                    Object[] arrayOfObject4 = new Object[bool];
                    arrayOfObject4[0] = paramString;
                    throw new IllegalStateException(String.format("Invalid event from daemon (%s)", arrayOfObject4));
                    if ((paramArrayOfString.length < 5) || (!paramArrayOfString[bool].equals("limit")))
                    {
                        Object[] arrayOfObject1 = new Object[bool];
                        arrayOfObject1[0] = paramString;
                        throw new IllegalStateException(String.format("Invalid event from daemon (%s)", arrayOfObject1));
                    }
                    if (!paramArrayOfString[2].equals("alert"))
                        break;
                    NetworkManagementService.this.notifyLimitReached(paramArrayOfString[3], paramArrayOfString[4]);
                }
            }
            Object[] arrayOfObject2 = new Object[bool];
            arrayOfObject2[0] = paramString;
            throw new IllegalStateException(String.format("Invalid event from daemon (%s)", arrayOfObject2));
        }
    }

    class NetdResponseCode
    {
        public static final int BandwidthControl = 601;
        public static final int DnsProxyQueryResult = 222;
        public static final int InterfaceChange = 600;
        public static final int InterfaceGetCfgResult = 213;
        public static final int InterfaceListResult = 110;
        public static final int InterfaceRxCounterResult = 216;
        public static final int InterfaceRxThrottleResult = 218;
        public static final int InterfaceTxCounterResult = 217;
        public static final int InterfaceTxThrottleResult = 219;
        public static final int IpFwdStatusResult = 211;
        public static final int QuotaCounterResult = 220;
        public static final int SoftapStatusResult = 214;
        public static final int TetherDnsFwdTgtListResult = 112;
        public static final int TetherInterfaceListResult = 111;
        public static final int TetherStatusResult = 210;
        public static final int TetheringStatsResult = 221;
        public static final int TtyListResult = 113;

        NetdResponseCode()
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.NetworkManagementService
 * JD-Core Version:        0.6.2
 */