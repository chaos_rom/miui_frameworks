package com.android.server;

import android.app.IWallpaperManager.Stub;
import android.app.IWallpaperManagerCallback;
import android.app.PendingIntent;
import android.app.WallpaperInfo;
import android.app.backup.BackupManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.FileObserver;
import android.os.FileUtils;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.os.UserId;
import android.service.wallpaper.IWallpaperConnection.Stub;
import android.service.wallpaper.IWallpaperEngine;
import android.service.wallpaper.IWallpaperService;
import android.service.wallpaper.IWallpaperService.Stub;
import android.util.Slog;
import android.util.SparseArray;
import android.view.IWindowManager;
import android.view.IWindowManager.Stub;
import com.android.internal.content.PackageMonitor;
import com.android.internal.util.JournaledFile;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import org.xmlpull.v1.XmlPullParserException;

class WallpaperManagerService extends IWallpaperManager.Stub
{
    static final boolean DEBUG = false;
    static final long MIN_WALLPAPER_CRASH_TIME = 10000L;
    static final String TAG = "WallpaperService";
    static final String WALLPAPER = "wallpaper";
    static final File WALLPAPER_BASE_DIR = new File("/data/system/users");
    static final String WALLPAPER_INFO = "wallpaper_info.xml";
    final Context mContext;
    int mCurrentUserId;
    final IWindowManager mIWindowManager;
    WallpaperData mLastWallpaper;
    final Object mLock = new Object[0];
    final MyPackageMonitor mMonitor;
    SparseArray<WallpaperData> mWallpaperMap = new SparseArray();

    public WallpaperManagerService(Context paramContext)
    {
        this.mContext = paramContext;
        this.mIWindowManager = IWindowManager.Stub.asInterface(ServiceManager.getService("window"));
        this.mMonitor = new MyPackageMonitor();
        this.mMonitor.register(paramContext, null, true);
        WALLPAPER_BASE_DIR.mkdirs();
        loadSettingsLocked(0);
    }

    private void checkPermission(String paramString)
    {
        if (this.mContext.checkCallingOrSelfPermission(paramString) != 0)
            throw new SecurityException("Access denied to process: " + Binder.getCallingPid() + ", must have permission " + paramString);
    }

    private static File getWallpaperDir(int paramInt)
    {
        return new File(WALLPAPER_BASE_DIR + "/" + paramInt);
    }

    // ERROR //
    private void loadSettingsLocked(int paramInt)
    {
        // Byte code:
        //     0: iload_1
        //     1: invokestatic 179	com/android/server/WallpaperManagerService:makeJournaledFile	(I)Lcom/android/internal/util/JournaledFile;
        //     4: astore_2
        //     5: aconst_null
        //     6: astore_3
        //     7: aload_2
        //     8: invokevirtual 185	com/android/internal/util/JournaledFile:chooseForRead	()Ljava/io/File;
        //     11: astore 4
        //     13: aload 4
        //     15: invokevirtual 188	java/io/File:exists	()Z
        //     18: ifne +7 -> 25
        //     21: aload_0
        //     22: invokespecial 191	com/android/server/WallpaperManagerService:migrateFromOld	()V
        //     25: aload_0
        //     26: getfield 76	com/android/server/WallpaperManagerService:mWallpaperMap	Landroid/util/SparseArray;
        //     29: iload_1
        //     30: invokevirtual 195	android/util/SparseArray:get	(I)Ljava/lang/Object;
        //     33: checkcast 14	com/android/server/WallpaperManagerService$WallpaperData
        //     36: astore 5
        //     38: aload 5
        //     40: ifnonnull +23 -> 63
        //     43: new 14	com/android/server/WallpaperManagerService$WallpaperData
        //     46: dup
        //     47: iload_1
        //     48: invokespecial 197	com/android/server/WallpaperManagerService$WallpaperData:<init>	(I)V
        //     51: astore 5
        //     53: aload_0
        //     54: getfield 76	com/android/server/WallpaperManagerService:mWallpaperMap	Landroid/util/SparseArray;
        //     57: iload_1
        //     58: aload 5
        //     60: invokevirtual 201	android/util/SparseArray:put	(ILjava/lang/Object;)V
        //     63: iconst_0
        //     64: istore 6
        //     66: new 203	java/io/FileInputStream
        //     69: dup
        //     70: aload 4
        //     72: invokespecial 206	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //     75: astore 7
        //     77: invokestatic 212	android/util/Xml:newPullParser	()Lorg/xmlpull/v1/XmlPullParser;
        //     80: astore 20
        //     82: aload 20
        //     84: aload 7
        //     86: aconst_null
        //     87: invokeinterface 218 3 0
        //     92: aload 20
        //     94: invokeinterface 221 1 0
        //     99: istore 21
        //     101: iload 21
        //     103: iconst_2
        //     104: if_icmpne +135 -> 239
        //     107: ldc 223
        //     109: aload 20
        //     111: invokeinterface 226 1 0
        //     116: invokevirtual 232	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     119: ifeq +120 -> 239
        //     122: aload 5
        //     124: aload 20
        //     126: aconst_null
        //     127: ldc 234
        //     129: invokeinterface 238 3 0
        //     134: invokestatic 243	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     137: putfield 245	com/android/server/WallpaperManagerService$WallpaperData:width	I
        //     140: aload 5
        //     142: aload 20
        //     144: aconst_null
        //     145: ldc 247
        //     147: invokeinterface 238 3 0
        //     152: invokestatic 243	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     155: putfield 249	com/android/server/WallpaperManagerService$WallpaperData:height	I
        //     158: aload 5
        //     160: aload 20
        //     162: aconst_null
        //     163: ldc 251
        //     165: invokeinterface 238 3 0
        //     170: putfield 253	com/android/server/WallpaperManagerService$WallpaperData:name	Ljava/lang/String;
        //     173: aload 20
        //     175: aconst_null
        //     176: ldc 255
        //     178: invokeinterface 238 3 0
        //     183: astore 22
        //     185: aload 22
        //     187: ifnull +156 -> 343
        //     190: aload 22
        //     192: invokestatic 261	android/content/ComponentName:unflattenFromString	(Ljava/lang/String;)Landroid/content/ComponentName;
        //     195: astore 23
        //     197: aload 5
        //     199: aload 23
        //     201: putfield 265	com/android/server/WallpaperManagerService$WallpaperData:nextWallpaperComponent	Landroid/content/ComponentName;
        //     204: aload 5
        //     206: getfield 265	com/android/server/WallpaperManagerService$WallpaperData:nextWallpaperComponent	Landroid/content/ComponentName;
        //     209: ifnull +20 -> 229
        //     212: ldc_w 267
        //     215: aload 5
        //     217: getfield 265	com/android/server/WallpaperManagerService$WallpaperData:nextWallpaperComponent	Landroid/content/ComponentName;
        //     220: invokevirtual 270	android/content/ComponentName:getPackageName	()Ljava/lang/String;
        //     223: invokevirtual 232	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     226: ifeq +13 -> 239
        //     229: aload 5
        //     231: aload 5
        //     233: getfield 273	com/android/server/WallpaperManagerService$WallpaperData:imageWallpaperComponent	Landroid/content/ComponentName;
        //     236: putfield 265	com/android/server/WallpaperManagerService$WallpaperData:nextWallpaperComponent	Landroid/content/ComponentName;
        //     239: iload 21
        //     241: iconst_1
        //     242: if_icmpne -150 -> 92
        //     245: iconst_1
        //     246: istore 6
        //     248: aload 7
        //     250: astore_3
        //     251: aload_3
        //     252: ifnull +7 -> 259
        //     255: aload_3
        //     256: invokevirtual 276	java/io/FileInputStream:close	()V
        //     259: iload 6
        //     261: ifne +25 -> 286
        //     264: aload 5
        //     266: bipush 255
        //     268: putfield 245	com/android/server/WallpaperManagerService$WallpaperData:width	I
        //     271: aload 5
        //     273: bipush 255
        //     275: putfield 249	com/android/server/WallpaperManagerService$WallpaperData:height	I
        //     278: aload 5
        //     280: ldc_w 278
        //     283: putfield 253	com/android/server/WallpaperManagerService$WallpaperData:name	Ljava/lang/String;
        //     286: aload_0
        //     287: getfield 78	com/android/server/WallpaperManagerService:mContext	Landroid/content/Context;
        //     290: ldc 80
        //     292: invokevirtual 282	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
        //     295: checkcast 284	android/view/WindowManager
        //     298: invokeinterface 288 1 0
        //     303: invokevirtual 293	android/view/Display:getMaximumSizeDimension	()I
        //     306: istore 10
        //     308: aload 5
        //     310: getfield 245	com/android/server/WallpaperManagerService$WallpaperData:width	I
        //     313: iload 10
        //     315: if_icmpge +10 -> 325
        //     318: aload 5
        //     320: iload 10
        //     322: putfield 245	com/android/server/WallpaperManagerService$WallpaperData:width	I
        //     325: aload 5
        //     327: getfield 249	com/android/server/WallpaperManagerService$WallpaperData:height	I
        //     330: iload 10
        //     332: if_icmpge +10 -> 342
        //     335: aload 5
        //     337: iload 10
        //     339: putfield 249	com/android/server/WallpaperManagerService$WallpaperData:height	I
        //     342: return
        //     343: aconst_null
        //     344: astore 23
        //     346: goto -149 -> 197
        //     349: astore 8
        //     351: ldc 29
        //     353: new 137	java/lang/StringBuilder
        //     356: dup
        //     357: invokespecial 138	java/lang/StringBuilder:<init>	()V
        //     360: ldc_w 295
        //     363: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     366: aload 4
        //     368: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     371: ldc_w 297
        //     374: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     377: aload 8
        //     379: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     382: invokevirtual 159	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     385: invokestatic 303	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     388: pop
        //     389: goto -138 -> 251
        //     392: astore 12
        //     394: ldc 29
        //     396: new 137	java/lang/StringBuilder
        //     399: dup
        //     400: invokespecial 138	java/lang/StringBuilder:<init>	()V
        //     403: ldc_w 295
        //     406: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     409: aload 4
        //     411: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     414: ldc_w 297
        //     417: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     420: aload 12
        //     422: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     425: invokevirtual 159	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     428: invokestatic 303	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     431: pop
        //     432: goto -181 -> 251
        //     435: astore 14
        //     437: ldc 29
        //     439: new 137	java/lang/StringBuilder
        //     442: dup
        //     443: invokespecial 138	java/lang/StringBuilder:<init>	()V
        //     446: ldc_w 295
        //     449: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     452: aload 4
        //     454: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     457: ldc_w 297
        //     460: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     463: aload 14
        //     465: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     468: invokevirtual 159	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     471: invokestatic 303	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     474: pop
        //     475: goto -224 -> 251
        //     478: astore 16
        //     480: ldc 29
        //     482: new 137	java/lang/StringBuilder
        //     485: dup
        //     486: invokespecial 138	java/lang/StringBuilder:<init>	()V
        //     489: ldc_w 295
        //     492: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     495: aload 4
        //     497: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     500: ldc_w 297
        //     503: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     506: aload 16
        //     508: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     511: invokevirtual 159	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     514: invokestatic 303	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     517: pop
        //     518: goto -267 -> 251
        //     521: astore 18
        //     523: ldc 29
        //     525: new 137	java/lang/StringBuilder
        //     528: dup
        //     529: invokespecial 138	java/lang/StringBuilder:<init>	()V
        //     532: ldc_w 295
        //     535: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     538: aload 4
        //     540: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     543: ldc_w 297
        //     546: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     549: aload 18
        //     551: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     554: invokevirtual 159	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     557: invokestatic 303	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     560: pop
        //     561: goto -310 -> 251
        //     564: astore 11
        //     566: goto -307 -> 259
        //     569: astore 18
        //     571: aload 7
        //     573: astore_3
        //     574: goto -51 -> 523
        //     577: astore 16
        //     579: aload 7
        //     581: astore_3
        //     582: goto -102 -> 480
        //     585: astore 14
        //     587: aload 7
        //     589: astore_3
        //     590: goto -153 -> 437
        //     593: astore 12
        //     595: aload 7
        //     597: astore_3
        //     598: goto -204 -> 394
        //     601: astore 8
        //     603: aload 7
        //     605: astore_3
        //     606: goto -255 -> 351
        //
        // Exception table:
        //     from	to	target	type
        //     66	77	349	java/lang/NullPointerException
        //     66	77	392	java/lang/NumberFormatException
        //     66	77	435	org/xmlpull/v1/XmlPullParserException
        //     66	77	478	java/io/IOException
        //     66	77	521	java/lang/IndexOutOfBoundsException
        //     255	259	564	java/io/IOException
        //     77	239	569	java/lang/IndexOutOfBoundsException
        //     77	239	577	java/io/IOException
        //     77	239	585	org/xmlpull/v1/XmlPullParserException
        //     77	239	593	java/lang/NumberFormatException
        //     77	239	601	java/lang/NullPointerException
    }

    private static JournaledFile makeJournaledFile(int paramInt)
    {
        String str = getWallpaperDir(paramInt) + "/" + "wallpaper_info.xml";
        return new JournaledFile(new File(str), new File(str + ".tmp"));
    }

    private void migrateFromOld()
    {
        File localFile1 = new File("/data/data/com.android.settings/files/wallpaper");
        File localFile2 = new File("/data/system/wallpaper_info.xml");
        if (localFile1.exists())
            localFile1.renameTo(new File(getWallpaperDir(0), "wallpaper"));
        if (localFile2.exists())
            localFile2.renameTo(new File(getWallpaperDir(0), "wallpaper_info.xml"));
    }

    private void notifyCallbacksLocked(WallpaperData paramWallpaperData)
    {
        int i = paramWallpaperData.callbacks.beginBroadcast();
        int j = 0;
        while (true)
        {
            if (j < i);
            try
            {
                ((IWallpaperManagerCallback)paramWallpaperData.callbacks.getBroadcastItem(j)).onWallpaperChanged();
                label31: j++;
                continue;
                paramWallpaperData.callbacks.finishBroadcast();
                Intent localIntent = new Intent("android.intent.action.WALLPAPER_CHANGED");
                this.mContext.sendBroadcast(localIntent);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                break label31;
            }
        }
    }

    // ERROR //
    private void saveSettingsLocked(WallpaperData paramWallpaperData)
    {
        // Byte code:
        //     0: aload_1
        //     1: getfield 354	com/android/server/WallpaperManagerService$WallpaperData:userId	I
        //     4: invokestatic 179	com/android/server/WallpaperManagerService:makeJournaledFile	(I)Lcom/android/internal/util/JournaledFile;
        //     7: astore_2
        //     8: aconst_null
        //     9: astore_3
        //     10: new 356	java/io/FileOutputStream
        //     13: dup
        //     14: aload_2
        //     15: invokevirtual 359	com/android/internal/util/JournaledFile:chooseForWrite	()Ljava/io/File;
        //     18: iconst_0
        //     19: invokespecial 362	java/io/FileOutputStream:<init>	(Ljava/io/File;Z)V
        //     22: astore 4
        //     24: new 364	com/android/internal/util/FastXmlSerializer
        //     27: dup
        //     28: invokespecial 365	com/android/internal/util/FastXmlSerializer:<init>	()V
        //     31: astore 5
        //     33: aload 5
        //     35: aload 4
        //     37: ldc_w 367
        //     40: invokeinterface 373 3 0
        //     45: aload 5
        //     47: aconst_null
        //     48: iconst_1
        //     49: invokestatic 379	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
        //     52: invokeinterface 383 3 0
        //     57: aload 5
        //     59: aconst_null
        //     60: ldc 223
        //     62: invokeinterface 387 3 0
        //     67: pop
        //     68: aload 5
        //     70: aconst_null
        //     71: ldc 234
        //     73: aload_1
        //     74: getfield 245	com/android/server/WallpaperManagerService$WallpaperData:width	I
        //     77: invokestatic 390	java/lang/Integer:toString	(I)Ljava/lang/String;
        //     80: invokeinterface 394 4 0
        //     85: pop
        //     86: aload 5
        //     88: aconst_null
        //     89: ldc 247
        //     91: aload_1
        //     92: getfield 249	com/android/server/WallpaperManagerService$WallpaperData:height	I
        //     95: invokestatic 390	java/lang/Integer:toString	(I)Ljava/lang/String;
        //     98: invokeinterface 394 4 0
        //     103: pop
        //     104: aload 5
        //     106: aconst_null
        //     107: ldc 251
        //     109: aload_1
        //     110: getfield 253	com/android/server/WallpaperManagerService$WallpaperData:name	Ljava/lang/String;
        //     113: invokeinterface 394 4 0
        //     118: pop
        //     119: aload_1
        //     120: getfield 397	com/android/server/WallpaperManagerService$WallpaperData:wallpaperComponent	Landroid/content/ComponentName;
        //     123: ifnull +35 -> 158
        //     126: aload_1
        //     127: getfield 397	com/android/server/WallpaperManagerService$WallpaperData:wallpaperComponent	Landroid/content/ComponentName;
        //     130: aload_1
        //     131: getfield 273	com/android/server/WallpaperManagerService$WallpaperData:imageWallpaperComponent	Landroid/content/ComponentName;
        //     134: invokevirtual 398	android/content/ComponentName:equals	(Ljava/lang/Object;)Z
        //     137: ifne +21 -> 158
        //     140: aload 5
        //     142: aconst_null
        //     143: ldc 255
        //     145: aload_1
        //     146: getfield 397	com/android/server/WallpaperManagerService$WallpaperData:wallpaperComponent	Landroid/content/ComponentName;
        //     149: invokevirtual 401	android/content/ComponentName:flattenToShortString	()Ljava/lang/String;
        //     152: invokeinterface 394 4 0
        //     157: pop
        //     158: aload 5
        //     160: aconst_null
        //     161: ldc 223
        //     163: invokeinterface 404 3 0
        //     168: pop
        //     169: aload 5
        //     171: invokeinterface 407 1 0
        //     176: aload 4
        //     178: invokevirtual 408	java/io/FileOutputStream:close	()V
        //     181: aload_2
        //     182: invokevirtual 411	com/android/internal/util/JournaledFile:commit	()V
        //     185: return
        //     186: astore 14
        //     188: aload_3
        //     189: ifnull +7 -> 196
        //     192: aload_3
        //     193: invokevirtual 408	java/io/FileOutputStream:close	()V
        //     196: aload_2
        //     197: invokevirtual 414	com/android/internal/util/JournaledFile:rollback	()V
        //     200: goto -15 -> 185
        //     203: astore 7
        //     205: goto -9 -> 196
        //     208: astore 6
        //     210: aload 4
        //     212: astore_3
        //     213: goto -25 -> 188
        //
        // Exception table:
        //     from	to	target	type
        //     10	24	186	java/io/IOException
        //     192	196	203	java/io/IOException
        //     24	185	208	java/io/IOException
    }

    void attachServiceLocked(WallpaperConnection paramWallpaperConnection, WallpaperData paramWallpaperData)
    {
        try
        {
            paramWallpaperConnection.mService.attach(paramWallpaperConnection, paramWallpaperConnection.mToken, 2013, false, paramWallpaperData.width, paramWallpaperData.height);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Slog.w("WallpaperService", "Failed attaching wallpaper; clearing", localRemoteException);
                if (!paramWallpaperData.wallpaperUpdating)
                    bindWallpaperComponentLocked(null, false, false, paramWallpaperData);
            }
        }
    }

    boolean bindWallpaperComponentLocked(ComponentName paramComponentName, boolean paramBoolean1, boolean paramBoolean2, WallpaperData paramWallpaperData)
    {
        boolean bool1;
        if ((!paramBoolean1) && (paramWallpaperData.connection != null))
            if (paramWallpaperData.wallpaperComponent == null)
            {
                if (paramComponentName != null)
                    break label48;
                bool1 = true;
            }
        label48: ServiceInfo localServiceInfo1;
        String str1;
        while (true)
        {
            return bool1;
            if (paramWallpaperData.wallpaperComponent.equals(paramComponentName))
            {
                bool1 = true;
            }
            else
            {
                if (paramComponentName == null);
                String str4;
                try
                {
                    String str5 = this.mContext.getString(17039387);
                    if (str5 != null)
                        paramComponentName = ComponentName.unflattenFromString(str5);
                    if (paramComponentName == null)
                        paramComponentName = paramWallpaperData.imageWallpaperComponent;
                    localServiceInfo1 = this.mContext.getPackageManager().getServiceInfo(paramComponentName, 4224);
                    if ("android.permission.BIND_WALLPAPER".equals(localServiceInfo1.permission))
                        break;
                    str4 = "Selected service does not require android.permission.BIND_WALLPAPER: " + paramComponentName;
                    if (!paramBoolean2)
                        break label189;
                    throw new SecurityException(str4);
                }
                catch (PackageManager.NameNotFoundException localNameNotFoundException)
                {
                    str1 = "Unknown component " + paramComponentName;
                    if (!paramBoolean2)
                        break label719;
                }
                throw new IllegalArgumentException(str1);
                label189: Slog.w("WallpaperService", str4);
                bool1 = false;
            }
        }
        WallpaperInfo localWallpaperInfo = null;
        Intent localIntent = new Intent("android.service.wallpaper.WallpaperService");
        List localList;
        if (paramComponentName != null)
        {
            ComponentName localComponentName2 = paramWallpaperData.imageWallpaperComponent;
            if (!paramComponentName.equals(localComponentName2))
                localList = this.mContext.getPackageManager().queryIntentServices(localIntent, 128);
        }
        label449: label463: label738: for (int j = 0; ; j++)
            while (true)
            {
                String str3;
                while (true)
                {
                    if (j < localList.size())
                    {
                        ServiceInfo localServiceInfo2 = ((ResolveInfo)localList.get(j)).serviceInfo;
                        if (!localServiceInfo2.name.equals(localServiceInfo1.name))
                            break label738;
                        boolean bool2 = localServiceInfo2.packageName.equals(localServiceInfo1.packageName);
                        if (!bool2)
                            break label738;
                    }
                    try
                    {
                        localWallpaperInfo = new WallpaperInfo(this.mContext, (ResolveInfo)localList.get(j));
                        if (localWallpaperInfo != null)
                            break label463;
                        str3 = "Selected service is not a wallpaper: " + paramComponentName;
                        if (!paramBoolean2)
                            break label449;
                        throw new SecurityException(str3);
                    }
                    catch (XmlPullParserException localXmlPullParserException)
                    {
                        if (paramBoolean2)
                            throw new IllegalArgumentException(localXmlPullParserException);
                        Slog.w("WallpaperService", localXmlPullParserException);
                        bool1 = false;
                    }
                    catch (IOException localIOException)
                    {
                        if (paramBoolean2)
                            throw new IllegalArgumentException(localIOException);
                        Slog.w("WallpaperService", localIOException);
                        bool1 = false;
                    }
                }
                break;
                Slog.w("WallpaperService", str3);
                bool1 = false;
                break;
                WallpaperConnection localWallpaperConnection = new WallpaperConnection(localWallpaperInfo, paramWallpaperData);
                localIntent.setComponent(paramComponentName);
                int i = paramWallpaperData.userId;
                ComponentName localComponentName1 = paramWallpaperData.imageWallpaperComponent;
                if (paramComponentName.equals(localComponentName1))
                    i = 0;
                localIntent.putExtra("android.intent.extra.client_label", 17040504);
                localIntent.putExtra("android.intent.extra.client_intent", PendingIntent.getActivity(this.mContext, 0, Intent.createChooser(new Intent("android.intent.action.SET_WALLPAPER"), this.mContext.getText(17040505)), 0));
                if (!this.mContext.bindService(localIntent, localWallpaperConnection, 1, i))
                {
                    String str2 = "Unable to bind service: " + paramComponentName;
                    if (paramBoolean2)
                        throw new IllegalArgumentException(str2);
                    Slog.w("WallpaperService", str2);
                    bool1 = false;
                    break;
                }
                if ((paramWallpaperData.userId == this.mCurrentUserId) && (this.mLastWallpaper != null))
                    detachWallpaperLocked(this.mLastWallpaper);
                paramWallpaperData.wallpaperComponent = paramComponentName;
                paramWallpaperData.connection = localWallpaperConnection;
                paramWallpaperData.lastDiedTime = SystemClock.uptimeMillis();
                try
                {
                    if (paramWallpaperData.userId == this.mCurrentUserId)
                    {
                        this.mIWindowManager.addWindowToken(localWallpaperConnection.mToken, 2013);
                        this.mLastWallpaper = paramWallpaperData;
                    }
                    bool1 = true;
                    break;
                    Slog.w("WallpaperService", str1);
                    bool1 = false;
                }
                catch (RemoteException localRemoteException)
                {
                    break label713;
                }
            }
    }

    public void clearWallpaper()
    {
        synchronized (this.mLock)
        {
            clearWallpaperLocked(false, UserId.getCallingUserId());
            return;
        }
    }

    void clearWallpaperComponentLocked(WallpaperData paramWallpaperData)
    {
        paramWallpaperData.wallpaperComponent = null;
        detachWallpaperLocked(paramWallpaperData);
    }

    void clearWallpaperLocked(boolean paramBoolean, int paramInt)
    {
        WallpaperData localWallpaperData = (WallpaperData)this.mWallpaperMap.get(paramInt);
        File localFile = new File(getWallpaperDir(paramInt), "wallpaper");
        if (localFile.exists())
            localFile.delete();
        long l = Binder.clearCallingIdentity();
        Object localObject1 = null;
        try
        {
            localWallpaperData.imageWallpaperPending = false;
            int i = this.mCurrentUserId;
            if (paramInt != i)
                label71: return;
            if (paramBoolean);
            for (ComponentName localComponentName = localWallpaperData.imageWallpaperComponent; ; localComponentName = null)
            {
                boolean bool = bindWallpaperComponentLocked(localComponentName, true, false, localWallpaperData);
                if (bool)
                    break;
                Binder.restoreCallingIdentity(l);
                Slog.e("WallpaperService", "Default wallpaper component not found!", (Throwable)localObject1);
                clearWallpaperComponentLocked(localWallpaperData);
                break label71;
            }
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            while (true)
                localObject1 = localIllegalArgumentException;
        }
        finally
        {
            Binder.restoreCallingIdentity(l);
        }
    }

    void detachWallpaperLocked(WallpaperData paramWallpaperData)
    {
        if ((paramWallpaperData.connection == null) || (paramWallpaperData.connection.mEngine != null));
        try
        {
            paramWallpaperData.connection.mEngine.destroy();
            label29: this.mContext.unbindService(paramWallpaperData.connection);
            try
            {
                this.mIWindowManager.removeWindowToken(paramWallpaperData.connection.mToken);
                label56: paramWallpaperData.connection.mService = null;
                paramWallpaperData.connection.mEngine = null;
                paramWallpaperData.connection = null;
                return;
            }
            catch (RemoteException localRemoteException1)
            {
                break label56;
            }
        }
        catch (RemoteException localRemoteException2)
        {
            break label29;
        }
    }

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.DUMP") != 0)
        {
            paramPrintWriter.println("Permission Denial: can't dump wallpaper service from from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid());
            return;
        }
        while (true)
        {
            int i;
            synchronized (this.mLock)
            {
                paramPrintWriter.println("Current Wallpaper Service state:");
                i = 0;
                if (i < this.mWallpaperMap.size())
                {
                    WallpaperData localWallpaperData = (WallpaperData)this.mWallpaperMap.valueAt(i);
                    paramPrintWriter.println(" User " + localWallpaperData.userId + ":");
                    paramPrintWriter.print("    mWidth=");
                    paramPrintWriter.print(localWallpaperData.width);
                    paramPrintWriter.print(" mHeight=");
                    paramPrintWriter.println(localWallpaperData.height);
                    paramPrintWriter.print("    mName=");
                    paramPrintWriter.println(localWallpaperData.name);
                    paramPrintWriter.print("    mWallpaperComponent=");
                    paramPrintWriter.println(localWallpaperData.wallpaperComponent);
                    if (localWallpaperData.connection == null)
                        break label342;
                    WallpaperConnection localWallpaperConnection = localWallpaperData.connection;
                    paramPrintWriter.print("    Wallpaper connection ");
                    paramPrintWriter.print(localWallpaperConnection);
                    paramPrintWriter.println(":");
                    if (localWallpaperConnection.mInfo != null)
                    {
                        paramPrintWriter.print("        mInfo.component=");
                        paramPrintWriter.println(localWallpaperConnection.mInfo.getComponent());
                    }
                    paramPrintWriter.print("        mToken=");
                    paramPrintWriter.println(localWallpaperConnection.mToken);
                    paramPrintWriter.print("        mService=");
                    paramPrintWriter.println(localWallpaperConnection.mService);
                    paramPrintWriter.print("        mEngine=");
                    paramPrintWriter.println(localWallpaperConnection.mEngine);
                    paramPrintWriter.print("        mLastDiedTime=");
                    paramPrintWriter.println(localWallpaperData.lastDiedTime - SystemClock.uptimeMillis());
                }
            }
            label342: i++;
        }
    }

    protected void finalize()
        throws Throwable
    {
        super.finalize();
        for (int i = 0; i < this.mWallpaperMap.size(); i++)
            ((WallpaperData)this.mWallpaperMap.valueAt(i)).wallpaperObserver.stopWatching();
    }

    public int getHeightHint()
        throws RemoteException
    {
        synchronized (this.mLock)
        {
            int i = ((WallpaperData)this.mWallpaperMap.get(UserId.getCallingUserId())).height;
            return i;
        }
    }

    String getName()
    {
        return ((WallpaperData)this.mWallpaperMap.get(0)).name;
    }

    public ParcelFileDescriptor getWallpaper(IWallpaperManagerCallback paramIWallpaperManagerCallback, Bundle paramBundle)
    {
        Object localObject1 = null;
        while (true)
        {
            int i;
            int k;
            WallpaperData localWallpaperData;
            synchronized (this.mLock)
            {
                i = Binder.getCallingUid();
                if (i == 1000)
                {
                    k = this.mCurrentUserId;
                    localWallpaperData = (WallpaperData)this.mWallpaperMap.get(k);
                    if (paramBundle == null);
                }
            }
            try
            {
                paramBundle.putInt("width", localWallpaperData.width);
                paramBundle.putInt("height", localWallpaperData.height);
                localWallpaperData.callbacks.register(paramIWallpaperManagerCallback);
                File localFile = new File(getWallpaperDir(k), "wallpaper");
                boolean bool = localFile.exists();
                if (!bool)
                {
                    break;
                    int j = UserId.getUserId(i);
                    k = j;
                }
                else
                {
                    ParcelFileDescriptor localParcelFileDescriptor = ParcelFileDescriptor.open(localFile, 268435456);
                    localObject1 = localParcelFileDescriptor;
                    break;
                    localObject3 = finally;
                    throw localObject3;
                }
            }
            catch (FileNotFoundException localFileNotFoundException)
            {
                Slog.w("WallpaperService", "Error getting wallpaper", localFileNotFoundException);
            }
        }
        return localObject1;
    }

    public WallpaperInfo getWallpaperInfo()
    {
        int i = UserId.getCallingUserId();
        WallpaperInfo localWallpaperInfo;
        synchronized (this.mLock)
        {
            WallpaperData localWallpaperData = (WallpaperData)this.mWallpaperMap.get(i);
            if (localWallpaperData.connection != null)
                localWallpaperInfo = localWallpaperData.connection.mInfo;
            else
                localWallpaperInfo = null;
        }
        return localWallpaperInfo;
    }

    public int getWidthHint()
        throws RemoteException
    {
        synchronized (this.mLock)
        {
            int i = ((WallpaperData)this.mWallpaperMap.get(UserId.getCallingUserId())).width;
            return i;
        }
    }

    void removeUser(int paramInt)
    {
        synchronized (this.mLock)
        {
            WallpaperData localWallpaperData = (WallpaperData)this.mWallpaperMap.get(paramInt);
            if (localWallpaperData != null)
            {
                localWallpaperData.wallpaperObserver.stopWatching();
                this.mWallpaperMap.remove(paramInt);
            }
            new File(getWallpaperDir(paramInt), "wallpaper").delete();
            new File(getWallpaperDir(paramInt), "wallpaper_info.xml").delete();
            return;
        }
    }

    // ERROR //
    boolean restoreNamedResourceLocked(WallpaperData paramWallpaperData)
    {
        // Byte code:
        //     0: aload_1
        //     1: getfield 253	com/android/server/WallpaperManagerService$WallpaperData:name	Ljava/lang/String;
        //     4: invokevirtual 746	java/lang/String:length	()I
        //     7: iconst_4
        //     8: if_icmple +379 -> 387
        //     11: ldc_w 748
        //     14: aload_1
        //     15: getfield 253	com/android/server/WallpaperManagerService$WallpaperData:name	Ljava/lang/String;
        //     18: iconst_0
        //     19: iconst_4
        //     20: invokevirtual 752	java/lang/String:substring	(II)Ljava/lang/String;
        //     23: invokevirtual 232	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     26: ifeq +361 -> 387
        //     29: aload_1
        //     30: getfield 253	com/android/server/WallpaperManagerService$WallpaperData:name	Ljava/lang/String;
        //     33: iconst_4
        //     34: invokevirtual 754	java/lang/String:substring	(I)Ljava/lang/String;
        //     37: astore_3
        //     38: aconst_null
        //     39: astore 4
        //     41: aload_3
        //     42: bipush 58
        //     44: invokevirtual 757	java/lang/String:indexOf	(I)I
        //     47: istore 5
        //     49: iload 5
        //     51: ifle +12 -> 63
        //     54: aload_3
        //     55: iconst_0
        //     56: iload 5
        //     58: invokevirtual 752	java/lang/String:substring	(II)Ljava/lang/String;
        //     61: astore 4
        //     63: aconst_null
        //     64: astore 6
        //     66: aload_3
        //     67: bipush 47
        //     69: invokevirtual 760	java/lang/String:lastIndexOf	(I)I
        //     72: istore 7
        //     74: iload 7
        //     76: ifle +13 -> 89
        //     79: aload_3
        //     80: iload 7
        //     82: iconst_1
        //     83: iadd
        //     84: invokevirtual 754	java/lang/String:substring	(I)Ljava/lang/String;
        //     87: astore 6
        //     89: aconst_null
        //     90: astore 8
        //     92: iload 5
        //     94: ifle +29 -> 123
        //     97: iload 7
        //     99: ifle +24 -> 123
        //     102: iload 7
        //     104: iload 5
        //     106: isub
        //     107: iconst_1
        //     108: if_icmple +15 -> 123
        //     111: aload_3
        //     112: iload 5
        //     114: iconst_1
        //     115: iadd
        //     116: iload 7
        //     118: invokevirtual 752	java/lang/String:substring	(II)Ljava/lang/String;
        //     121: astore 8
        //     123: aload 4
        //     125: ifnull +262 -> 387
        //     128: aload 6
        //     130: ifnull +257 -> 387
        //     133: aload 8
        //     135: ifnull +252 -> 387
        //     138: bipush 255
        //     140: istore 9
        //     142: aconst_null
        //     143: astore 10
        //     145: aconst_null
        //     146: astore 11
        //     148: aload_0
        //     149: getfield 78	com/android/server/WallpaperManagerService:mContext	Landroid/content/Context;
        //     152: aload 4
        //     154: iconst_4
        //     155: invokevirtual 764	android/content/Context:createPackageContext	(Ljava/lang/String;I)Landroid/content/Context;
        //     158: invokevirtual 768	android/content/Context:getResources	()Landroid/content/res/Resources;
        //     161: astore 29
        //     163: aload 29
        //     165: aload_3
        //     166: aconst_null
        //     167: aconst_null
        //     168: invokevirtual 774	android/content/res/Resources:getIdentifier	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
        //     171: istore 9
        //     173: iload 9
        //     175: ifne +73 -> 248
        //     178: ldc 29
        //     180: new 137	java/lang/StringBuilder
        //     183: dup
        //     184: invokespecial 138	java/lang/StringBuilder:<init>	()V
        //     187: ldc_w 776
        //     190: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     193: aload 4
        //     195: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     198: ldc_w 778
        //     201: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     204: aload 8
        //     206: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     209: ldc_w 780
        //     212: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     215: aload 6
        //     217: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     220: invokevirtual 159	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     223: invokestatic 782	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     226: pop
        //     227: iconst_0
        //     228: istore_2
        //     229: iconst_0
        //     230: ifeq +5 -> 235
        //     233: aconst_null
        //     234: athrow
        //     235: iconst_0
        //     236: ifeq +10 -> 246
        //     239: aconst_null
        //     240: invokestatic 788	android/os/FileUtils:sync	(Ljava/io/FileOutputStream;)Z
        //     243: pop
        //     244: aconst_null
        //     245: athrow
        //     246: iload_2
        //     247: ireturn
        //     248: aload 29
        //     250: iload 9
        //     252: invokevirtual 792	android/content/res/Resources:openRawResource	(I)Ljava/io/InputStream;
        //     255: astore 10
        //     257: aload_1
        //     258: getfield 795	com/android/server/WallpaperManagerService$WallpaperData:wallpaperFile	Ljava/io/File;
        //     261: invokevirtual 188	java/io/File:exists	()Z
        //     264: ifeq +11 -> 275
        //     267: aload_1
        //     268: getfield 795	com/android/server/WallpaperManagerService$WallpaperData:wallpaperFile	Ljava/io/File;
        //     271: invokevirtual 591	java/io/File:delete	()Z
        //     274: pop
        //     275: new 356	java/io/FileOutputStream
        //     278: dup
        //     279: aload_1
        //     280: getfield 795	com/android/server/WallpaperManagerService$WallpaperData:wallpaperFile	Ljava/io/File;
        //     283: invokespecial 796	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
        //     286: astore 30
        //     288: ldc_w 797
        //     291: newarray byte
        //     293: astore 33
        //     295: aload 10
        //     297: aload 33
        //     299: invokevirtual 803	java/io/InputStream:read	([B)I
        //     302: istore 34
        //     304: iload 34
        //     306: ifle +86 -> 392
        //     309: aload 30
        //     311: aload 33
        //     313: iconst_0
        //     314: iload 34
        //     316: invokevirtual 807	java/io/FileOutputStream:write	([BII)V
        //     319: goto -24 -> 295
        //     322: astore 32
        //     324: aload 30
        //     326: astore 11
        //     328: ldc 29
        //     330: new 137	java/lang/StringBuilder
        //     333: dup
        //     334: invokespecial 138	java/lang/StringBuilder:<init>	()V
        //     337: ldc_w 809
        //     340: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     343: aload 4
        //     345: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     348: ldc_w 811
        //     351: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     354: invokevirtual 159	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     357: invokestatic 782	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     360: pop
        //     361: aload 10
        //     363: ifnull +8 -> 371
        //     366: aload 10
        //     368: invokevirtual 812	java/io/InputStream:close	()V
        //     371: aload 11
        //     373: ifnull +14 -> 387
        //     376: aload 11
        //     378: invokestatic 788	android/os/FileUtils:sync	(Ljava/io/FileOutputStream;)Z
        //     381: pop
        //     382: aload 11
        //     384: invokevirtual 408	java/io/FileOutputStream:close	()V
        //     387: iconst_0
        //     388: istore_2
        //     389: goto -143 -> 246
        //     392: ldc 29
        //     394: new 137	java/lang/StringBuilder
        //     397: dup
        //     398: invokespecial 138	java/lang/StringBuilder:<init>	()V
        //     401: ldc_w 814
        //     404: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     407: aload_3
        //     408: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     411: invokevirtual 159	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     414: invokestatic 817	android/util/Slog:v	(Ljava/lang/String;Ljava/lang/String;)I
        //     417: pop
        //     418: iconst_1
        //     419: istore_2
        //     420: aload 10
        //     422: ifnull +8 -> 430
        //     425: aload 10
        //     427: invokevirtual 812	java/io/InputStream:close	()V
        //     430: aload 30
        //     432: ifnull -186 -> 246
        //     435: aload 30
        //     437: invokestatic 788	android/os/FileUtils:sync	(Ljava/io/FileOutputStream;)Z
        //     440: pop
        //     441: aload 30
        //     443: invokevirtual 408	java/io/FileOutputStream:close	()V
        //     446: goto -200 -> 246
        //     449: astore 37
        //     451: goto -205 -> 246
        //     454: astore 25
        //     456: ldc 29
        //     458: new 137	java/lang/StringBuilder
        //     461: dup
        //     462: invokespecial 138	java/lang/StringBuilder:<init>	()V
        //     465: ldc_w 819
        //     468: invokevirtual 144	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     471: iload 9
        //     473: invokevirtual 153	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     476: invokevirtual 159	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     479: invokestatic 782	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     482: pop
        //     483: aload 10
        //     485: ifnull +8 -> 493
        //     488: aload 10
        //     490: invokevirtual 812	java/io/InputStream:close	()V
        //     493: aload 11
        //     495: ifnull -108 -> 387
        //     498: aload 11
        //     500: invokestatic 788	android/os/FileUtils:sync	(Ljava/io/FileOutputStream;)Z
        //     503: pop
        //     504: aload 11
        //     506: invokevirtual 408	java/io/FileOutputStream:close	()V
        //     509: goto -122 -> 387
        //     512: astore 19
        //     514: goto -127 -> 387
        //     517: astore 21
        //     519: ldc 29
        //     521: ldc_w 821
        //     524: aload 21
        //     526: invokestatic 606	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     529: pop
        //     530: aload 10
        //     532: ifnull +8 -> 540
        //     535: aload 10
        //     537: invokevirtual 812	java/io/InputStream:close	()V
        //     540: aload 11
        //     542: ifnull -155 -> 387
        //     545: aload 11
        //     547: invokestatic 788	android/os/FileUtils:sync	(Ljava/io/FileOutputStream;)Z
        //     550: pop
        //     551: aload 11
        //     553: invokevirtual 408	java/io/FileOutputStream:close	()V
        //     556: goto -169 -> 387
        //     559: astore 13
        //     561: aload 10
        //     563: ifnull +8 -> 571
        //     566: aload 10
        //     568: invokevirtual 812	java/io/InputStream:close	()V
        //     571: aload 11
        //     573: ifnull +14 -> 587
        //     576: aload 11
        //     578: invokestatic 788	android/os/FileUtils:sync	(Ljava/io/FileOutputStream;)Z
        //     581: pop
        //     582: aload 11
        //     584: invokevirtual 408	java/io/FileOutputStream:close	()V
        //     587: aload 13
        //     589: athrow
        //     590: astore 16
        //     592: goto -21 -> 571
        //     595: astore 15
        //     597: goto -10 -> 587
        //     600: astore 20
        //     602: goto -231 -> 371
        //     605: astore 28
        //     607: goto -114 -> 493
        //     610: astore 24
        //     612: goto -72 -> 540
        //     615: astore 42
        //     617: goto -382 -> 235
        //     620: astore 38
        //     622: goto -192 -> 430
        //     625: astore 13
        //     627: aload 30
        //     629: astore 11
        //     631: goto -70 -> 561
        //     634: astore 21
        //     636: aload 30
        //     638: astore 11
        //     640: goto -121 -> 519
        //     643: astore 31
        //     645: aload 30
        //     647: astore 11
        //     649: goto -193 -> 456
        //     652: astore 12
        //     654: goto -326 -> 328
        //
        // Exception table:
        //     from	to	target	type
        //     288	319	322	android/content/pm/PackageManager$NameNotFoundException
        //     392	418	322	android/content/pm/PackageManager$NameNotFoundException
        //     244	246	449	java/io/IOException
        //     441	446	449	java/io/IOException
        //     148	227	454	android/content/res/Resources$NotFoundException
        //     248	288	454	android/content/res/Resources$NotFoundException
        //     382	387	512	java/io/IOException
        //     504	509	512	java/io/IOException
        //     551	556	512	java/io/IOException
        //     148	227	517	java/io/IOException
        //     248	288	517	java/io/IOException
        //     148	227	559	finally
        //     248	288	559	finally
        //     328	361	559	finally
        //     456	483	559	finally
        //     519	530	559	finally
        //     566	571	590	java/io/IOException
        //     582	587	595	java/io/IOException
        //     366	371	600	java/io/IOException
        //     488	493	605	java/io/IOException
        //     535	540	610	java/io/IOException
        //     233	235	615	java/io/IOException
        //     425	430	620	java/io/IOException
        //     288	319	625	finally
        //     392	418	625	finally
        //     288	319	634	java/io/IOException
        //     392	418	634	java/io/IOException
        //     288	319	643	android/content/res/Resources$NotFoundException
        //     392	418	643	android/content/res/Resources$NotFoundException
        //     148	227	652	android/content/pm/PackageManager$NameNotFoundException
        //     248	288	652	android/content/pm/PackageManager$NameNotFoundException
    }

    public void setDimensionHints(int paramInt1, int paramInt2)
        throws RemoteException
    {
        checkPermission("android.permission.SET_WALLPAPER_HINTS");
        int i = UserId.getCallingUserId();
        WallpaperData localWallpaperData = (WallpaperData)this.mWallpaperMap.get(i);
        if (localWallpaperData == null)
            throw new IllegalStateException("Wallpaper not yet initialized for user " + i);
        if ((paramInt1 <= 0) || (paramInt2 <= 0))
            throw new IllegalArgumentException("width and height must be > 0");
        synchronized (this.mLock)
        {
            if ((paramInt1 != localWallpaperData.width) || (paramInt2 != localWallpaperData.height))
            {
                localWallpaperData.width = paramInt1;
                localWallpaperData.height = paramInt2;
                saveSettingsLocked(localWallpaperData);
                if (this.mCurrentUserId != i)
                    return;
                if (localWallpaperData.connection != null)
                {
                    IWallpaperEngine localIWallpaperEngine = localWallpaperData.connection.mEngine;
                    if (localIWallpaperEngine == null);
                }
            }
        }
        try
        {
            localWallpaperData.connection.mEngine.setDesiredSize(paramInt1, paramInt2);
            label173: notifyCallbacksLocked(localWallpaperData);
            return;
            localObject2 = finally;
            throw localObject2;
        }
        catch (RemoteException localRemoteException)
        {
            break label173;
        }
    }

    public ParcelFileDescriptor setWallpaper(String paramString)
    {
        int i = UserId.getCallingUserId();
        WallpaperData localWallpaperData = (WallpaperData)this.mWallpaperMap.get(i);
        if (localWallpaperData == null)
            throw new IllegalStateException("Wallpaper not yet initialized for user " + i);
        checkPermission("android.permission.SET_WALLPAPER");
        synchronized (this.mLock)
        {
            long l = Binder.clearCallingIdentity();
            try
            {
                ParcelFileDescriptor localParcelFileDescriptor = updateWallpaperBitmapLocked(paramString, localWallpaperData);
                if (localParcelFileDescriptor != null)
                    localWallpaperData.imageWallpaperPending = true;
                Binder.restoreCallingIdentity(l);
                return localParcelFileDescriptor;
            }
            finally
            {
                localObject3 = finally;
                Binder.restoreCallingIdentity(l);
                throw localObject3;
            }
        }
    }

    public void setWallpaperComponent(ComponentName paramComponentName)
    {
        int i = UserId.getCallingUserId();
        WallpaperData localWallpaperData = (WallpaperData)this.mWallpaperMap.get(i);
        if (localWallpaperData == null)
            throw new IllegalStateException("Wallpaper not yet initialized for user " + i);
        checkPermission("android.permission.SET_WALLPAPER_COMPONENT");
        synchronized (this.mLock)
        {
            long l = Binder.clearCallingIdentity();
            try
            {
                localWallpaperData.imageWallpaperPending = false;
                bindWallpaperComponentLocked(paramComponentName, false, true, localWallpaperData);
                Binder.restoreCallingIdentity(l);
                return;
            }
            finally
            {
                localObject3 = finally;
                Binder.restoreCallingIdentity(l);
                throw localObject3;
            }
        }
    }

    void settingsRestored()
    {
        while (true)
        {
            WallpaperData localWallpaperData;
            synchronized (this.mLock)
            {
                loadSettingsLocked(0);
                localWallpaperData = (WallpaperData)this.mWallpaperMap.get(0);
                if ((localWallpaperData.nextWallpaperComponent != null) && (!localWallpaperData.nextWallpaperComponent.equals(localWallpaperData.imageWallpaperComponent)))
                {
                    if (bindWallpaperComponentLocked(localWallpaperData.nextWallpaperComponent, false, false, localWallpaperData))
                        break label209;
                    bindWallpaperComponentLocked(null, false, false, localWallpaperData);
                    break label209;
                    if (i == 0)
                    {
                        Slog.e("WallpaperService", "Failed to restore wallpaper: '" + localWallpaperData.name + "'");
                        localWallpaperData.name = "";
                        getWallpaperDir(0).delete();
                    }
                }
            }
            synchronized (this.mLock)
            {
                saveSettingsLocked(localWallpaperData);
                return;
                if ("".equals(localWallpaperData.name))
                {
                    i = 1;
                    if (i == 0)
                        continue;
                    bindWallpaperComponentLocked(localWallpaperData.nextWallpaperComponent, false, false, localWallpaperData);
                    continue;
                    localObject2 = finally;
                    throw localObject2;
                }
                boolean bool = restoreNamedResourceLocked(localWallpaperData);
                i = bool;
            }
            label209: int i = 1;
        }
    }

    void switchUser(int paramInt)
    {
        synchronized (this.mLock)
        {
            this.mCurrentUserId = paramInt;
            WallpaperData localWallpaperData = (WallpaperData)this.mWallpaperMap.get(paramInt);
            if (localWallpaperData == null)
            {
                localWallpaperData = new WallpaperData(paramInt);
                this.mWallpaperMap.put(paramInt, localWallpaperData);
                loadSettingsLocked(paramInt);
                localWallpaperData.wallpaperObserver = new WallpaperObserver(localWallpaperData);
                localWallpaperData.wallpaperObserver.startWatching();
            }
            switchWallpaper(localWallpaperData);
            return;
        }
    }

    void switchWallpaper(WallpaperData paramWallpaperData)
    {
        Object localObject1 = this.mLock;
        Object localObject2 = null;
        try
        {
            if (paramWallpaperData.wallpaperComponent != null);
            for (ComponentName localComponentName = paramWallpaperData.wallpaperComponent; ; localComponentName = paramWallpaperData.nextWallpaperComponent)
            {
                boolean bool = bindWallpaperComponentLocked(localComponentName, true, false, paramWallpaperData);
                if (!bool)
                    break;
                return;
            }
        }
        catch (RuntimeException localRuntimeException)
        {
            while (true)
            {
                localObject2 = localRuntimeException;
                Slog.w("WallpaperService", "Failure starting previous wallpaper", localObject2);
                clearWallpaperLocked(false, paramWallpaperData.userId);
            }
        }
        finally
        {
        }
    }

    public void systemReady()
    {
        WallpaperData localWallpaperData = (WallpaperData)this.mWallpaperMap.get(0);
        switchWallpaper(localWallpaperData);
        localWallpaperData.wallpaperObserver = new WallpaperObserver(localWallpaperData);
        localWallpaperData.wallpaperObserver.startWatching();
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("android.intent.action.USER_SWITCHED");
        localIntentFilter.addAction("android.intent.action.USER_REMOVED");
        this.mContext.registerReceiver(new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                String str = paramAnonymousIntent.getAction();
                if ("android.intent.action.USER_SWITCHED".equals(str))
                    WallpaperManagerService.this.switchUser(paramAnonymousIntent.getIntExtra("android.intent.extra.user_id", 0));
                while (true)
                {
                    return;
                    if ("android.intent.action.USER_REMOVED".equals(str))
                        WallpaperManagerService.this.removeUser(paramAnonymousIntent.getIntExtra("android.intent.extra.user_id", 0));
                }
            }
        }
        , localIntentFilter);
    }

    ParcelFileDescriptor updateWallpaperBitmapLocked(String paramString, WallpaperData paramWallpaperData)
    {
        if (paramString == null)
            paramString = "";
        try
        {
            File localFile = getWallpaperDir(paramWallpaperData.userId);
            if (!localFile.exists())
            {
                localFile.mkdir();
                FileUtils.setPermissions(localFile.getPath(), 505, -1, -1);
            }
            localParcelFileDescriptor = ParcelFileDescriptor.open(new File(localFile, "wallpaper"), 939524096);
            paramWallpaperData.name = paramString;
            return localParcelFileDescriptor;
        }
        catch (FileNotFoundException localFileNotFoundException)
        {
            while (true)
            {
                Slog.w("WallpaperService", "Error setting wallpaper", localFileNotFoundException);
                ParcelFileDescriptor localParcelFileDescriptor = null;
            }
        }
    }

    class MyPackageMonitor extends PackageMonitor
    {
        MyPackageMonitor()
        {
        }

        boolean doPackagesChangedLocked(boolean paramBoolean, WallpaperManagerService.WallpaperData paramWallpaperData)
        {
            boolean bool = false;
            if (paramWallpaperData.wallpaperComponent != null)
            {
                int j = isPackageDisappearing(paramWallpaperData.wallpaperComponent.getPackageName());
                if ((j == 3) || (j == 2))
                {
                    bool = true;
                    if (paramBoolean)
                    {
                        Slog.w("WallpaperService", "Wallpaper uninstalled, removing: " + paramWallpaperData.wallpaperComponent);
                        WallpaperManagerService.this.clearWallpaperLocked(false, paramWallpaperData.userId);
                    }
                }
            }
            if (paramWallpaperData.nextWallpaperComponent != null)
            {
                int i = isPackageDisappearing(paramWallpaperData.nextWallpaperComponent.getPackageName());
                if ((i == 3) || (i == 2))
                    paramWallpaperData.nextWallpaperComponent = null;
            }
            if ((paramWallpaperData.wallpaperComponent != null) && (isPackageModified(paramWallpaperData.wallpaperComponent.getPackageName())));
            try
            {
                WallpaperManagerService.this.mContext.getPackageManager().getServiceInfo(paramWallpaperData.wallpaperComponent, 0);
                if ((paramWallpaperData.nextWallpaperComponent == null) || (!isPackageModified(paramWallpaperData.nextWallpaperComponent.getPackageName())));
            }
            catch (PackageManager.NameNotFoundException localNameNotFoundException2)
            {
                try
                {
                    WallpaperManagerService.this.mContext.getPackageManager().getServiceInfo(paramWallpaperData.nextWallpaperComponent, 0);
                    return bool;
                    localNameNotFoundException2 = localNameNotFoundException2;
                    Slog.w("WallpaperService", "Wallpaper component gone, removing: " + paramWallpaperData.wallpaperComponent);
                    WallpaperManagerService.this.clearWallpaperLocked(false, paramWallpaperData.userId);
                }
                catch (PackageManager.NameNotFoundException localNameNotFoundException1)
                {
                    while (true)
                        paramWallpaperData.nextWallpaperComponent = null;
                }
            }
        }

        public boolean onHandleForceStop(Intent paramIntent, String[] paramArrayOfString, int paramInt, boolean paramBoolean)
        {
            Object localObject1 = WallpaperManagerService.this.mLock;
            boolean bool = false;
            int i = 0;
            try
            {
                while (i < WallpaperManagerService.this.mWallpaperMap.size())
                {
                    bool |= doPackagesChangedLocked(paramBoolean, (WallpaperManagerService.WallpaperData)WallpaperManagerService.this.mWallpaperMap.valueAt(i));
                    i++;
                }
                return bool;
            }
            finally
            {
                localObject2 = finally;
                throw localObject2;
            }
        }

        public void onPackageModified(String paramString)
        {
            Object localObject1 = WallpaperManagerService.this.mLock;
            int i = 0;
            while (true)
                try
                {
                    if (i < WallpaperManagerService.this.mWallpaperMap.size())
                    {
                        WallpaperManagerService.WallpaperData localWallpaperData = (WallpaperManagerService.WallpaperData)WallpaperManagerService.this.mWallpaperMap.valueAt(i);
                        if ((localWallpaperData.wallpaperComponent == null) || (!localWallpaperData.wallpaperComponent.getPackageName().equals(paramString)))
                            continue;
                        doPackagesChangedLocked(true, localWallpaperData);
                    }
                }
                finally
                {
                    throw localObject2;
                }
        }

        public void onPackageUpdateFinished(String paramString, int paramInt)
        {
            Object localObject1 = WallpaperManagerService.this.mLock;
            for (int i = 0; ; i++)
                try
                {
                    if (i < WallpaperManagerService.this.mWallpaperMap.size())
                    {
                        WallpaperManagerService.WallpaperData localWallpaperData = (WallpaperManagerService.WallpaperData)WallpaperManagerService.this.mWallpaperMap.valueAt(i);
                        if ((localWallpaperData.wallpaperComponent != null) && (localWallpaperData.wallpaperComponent.getPackageName().equals(paramString)))
                        {
                            localWallpaperData.wallpaperUpdating = false;
                            ComponentName localComponentName = localWallpaperData.wallpaperComponent;
                            WallpaperManagerService.this.clearWallpaperComponentLocked(localWallpaperData);
                            if ((localWallpaperData.userId == WallpaperManagerService.this.mCurrentUserId) && (!WallpaperManagerService.this.bindWallpaperComponentLocked(localComponentName, false, false, localWallpaperData)))
                            {
                                Slog.w("WallpaperService", "Wallpaper no longer available; reverting to default");
                                WallpaperManagerService.this.clearWallpaperLocked(false, localWallpaperData.userId);
                            }
                        }
                    }
                    else
                    {
                        return;
                    }
                }
                finally
                {
                    localObject2 = finally;
                    throw localObject2;
                }
        }

        public void onPackageUpdateStarted(String paramString, int paramInt)
        {
            Object localObject1 = WallpaperManagerService.this.mLock;
            for (int i = 0; ; i++)
                try
                {
                    if (i < WallpaperManagerService.this.mWallpaperMap.size())
                    {
                        WallpaperManagerService.WallpaperData localWallpaperData = (WallpaperManagerService.WallpaperData)WallpaperManagerService.this.mWallpaperMap.valueAt(i);
                        if ((localWallpaperData.wallpaperComponent != null) && (localWallpaperData.wallpaperComponent.getPackageName().equals(paramString)))
                            localWallpaperData.wallpaperUpdating = true;
                    }
                    else
                    {
                        return;
                    }
                }
                finally
                {
                    localObject2 = finally;
                    throw localObject2;
                }
        }

        public void onSomePackagesChanged()
        {
            Object localObject1 = WallpaperManagerService.this.mLock;
            int i = 0;
            try
            {
                while (i < WallpaperManagerService.this.mWallpaperMap.size())
                {
                    doPackagesChangedLocked(true, (WallpaperManagerService.WallpaperData)WallpaperManagerService.this.mWallpaperMap.valueAt(i));
                    i++;
                }
                return;
            }
            finally
            {
                localObject2 = finally;
                throw localObject2;
            }
        }
    }

    class WallpaperConnection extends IWallpaperConnection.Stub
        implements ServiceConnection
    {
        IWallpaperEngine mEngine;
        final WallpaperInfo mInfo;
        IWallpaperService mService;
        final Binder mToken = new Binder();
        WallpaperManagerService.WallpaperData mWallpaper;

        public WallpaperConnection(WallpaperInfo paramWallpaperData, WallpaperManagerService.WallpaperData arg3)
        {
            this.mInfo = paramWallpaperData;
            Object localObject;
            this.mWallpaper = localObject;
        }

        public void attachEngine(IWallpaperEngine paramIWallpaperEngine)
        {
            this.mEngine = paramIWallpaperEngine;
        }

        public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
        {
            synchronized (WallpaperManagerService.this.mLock)
            {
                if (this.mWallpaper.connection == this)
                {
                    this.mWallpaper.lastDiedTime = SystemClock.uptimeMillis();
                    this.mService = IWallpaperService.Stub.asInterface(paramIBinder);
                    WallpaperManagerService.this.attachServiceLocked(this, this.mWallpaper);
                    WallpaperManagerService.this.saveSettingsLocked(this.mWallpaper);
                }
                return;
            }
        }

        public void onServiceDisconnected(ComponentName paramComponentName)
        {
            synchronized (WallpaperManagerService.this.mLock)
            {
                this.mService = null;
                this.mEngine = null;
                if (this.mWallpaper.connection == this)
                {
                    Slog.w("WallpaperService", "Wallpaper service gone: " + this.mWallpaper.wallpaperComponent);
                    if ((!this.mWallpaper.wallpaperUpdating) && (10000L + this.mWallpaper.lastDiedTime > SystemClock.uptimeMillis()) && (this.mWallpaper.userId == WallpaperManagerService.this.mCurrentUserId))
                    {
                        Slog.w("WallpaperService", "Reverting to built-in wallpaper!");
                        WallpaperManagerService.this.clearWallpaperLocked(true, this.mWallpaper.userId);
                    }
                }
                return;
            }
        }

        public ParcelFileDescriptor setWallpaper(String paramString)
        {
            ParcelFileDescriptor localParcelFileDescriptor;
            synchronized (WallpaperManagerService.this.mLock)
            {
                if (this.mWallpaper.connection == this)
                    localParcelFileDescriptor = WallpaperManagerService.this.updateWallpaperBitmapLocked(paramString, this.mWallpaper);
                else
                    localParcelFileDescriptor = null;
            }
            return localParcelFileDescriptor;
        }
    }

    static class WallpaperData
    {
        private RemoteCallbackList<IWallpaperManagerCallback> callbacks = new RemoteCallbackList();
        WallpaperManagerService.WallpaperConnection connection;
        int height = -1;
        ComponentName imageWallpaperComponent = new ComponentName("com.android.systemui", "com.android.systemui.ImageWallpaper");
        boolean imageWallpaperPending;
        long lastDiedTime;
        String name = "";
        ComponentName nextWallpaperComponent;
        int userId;
        ComponentName wallpaperComponent;
        File wallpaperFile;
        WallpaperManagerService.WallpaperObserver wallpaperObserver;
        boolean wallpaperUpdating;
        int width = -1;

        WallpaperData(int paramInt)
        {
            this.userId = paramInt;
            this.wallpaperFile = new File(WallpaperManagerService.getWallpaperDir(paramInt), "wallpaper");
        }
    }

    private class WallpaperObserver extends FileObserver
    {
        final WallpaperManagerService.WallpaperData mWallpaper;
        final File mWallpaperDir;
        final File mWallpaperFile;

        public WallpaperObserver(WallpaperManagerService.WallpaperData arg2)
        {
            super(1544);
            this.mWallpaperDir = WallpaperManagerService.getWallpaperDir(localObject.userId);
            this.mWallpaper = localObject;
            this.mWallpaperFile = new File(this.mWallpaperDir, "wallpaper");
        }

        public void onEvent(int paramInt, String paramString)
        {
            if (paramString == null);
            while (true)
            {
                return;
                synchronized (WallpaperManagerService.this.mLock)
                {
                    long l = Binder.clearCallingIdentity();
                    new BackupManager(WallpaperManagerService.this.mContext).dataChanged();
                    Binder.restoreCallingIdentity(l);
                    File localFile = new File(this.mWallpaperDir, paramString);
                    if (this.mWallpaperFile.equals(localFile))
                    {
                        WallpaperManagerService.this.notifyCallbacksLocked(this.mWallpaper);
                        if ((this.mWallpaper.wallpaperComponent == null) || (paramInt != 8) || (this.mWallpaper.imageWallpaperPending))
                        {
                            if (paramInt == 8)
                                this.mWallpaper.imageWallpaperPending = false;
                            WallpaperManagerService.this.bindWallpaperComponentLocked(this.mWallpaper.imageWallpaperComponent, true, false, this.mWallpaper);
                            WallpaperManagerService.this.saveSettingsLocked(this.mWallpaper);
                        }
                    }
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.WallpaperManagerService
 * JD-Core Version:        0.6.2
 */