package com.android.server.pm;

import java.io.File;

final class PendingPackage extends PackageSettingBase
{
    final int sharedId;

    PendingPackage(String paramString1, String paramString2, File paramFile1, File paramFile2, String paramString3, int paramInt1, int paramInt2, int paramInt3)
    {
        super(paramString1, paramString2, paramFile1, paramFile2, paramString3, paramInt2, paramInt3);
        this.sharedId = paramInt1;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.pm.PendingPackage
 * JD-Core Version:        0.6.2
 */