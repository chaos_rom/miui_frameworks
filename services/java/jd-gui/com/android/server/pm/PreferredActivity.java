package com.android.server.pm;

import android.content.ComponentName;
import android.content.IntentFilter;
import com.android.internal.util.XmlUtils;
import com.android.server.PreferredComponent;
import com.android.server.PreferredComponent.Callbacks;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

class PreferredActivity extends IntentFilter
    implements PreferredComponent.Callbacks
{
    private static final boolean DEBUG_FILTERS = false;
    private static final String TAG = "PreferredActivity";
    final PreferredComponent mPref;

    PreferredActivity(IntentFilter paramIntentFilter, int paramInt, ComponentName[] paramArrayOfComponentName, ComponentName paramComponentName)
    {
        super(paramIntentFilter);
        this.mPref = new PreferredComponent(this, paramInt, paramArrayOfComponentName, paramComponentName);
    }

    PreferredActivity(XmlPullParser paramXmlPullParser)
        throws XmlPullParserException, IOException
    {
        this.mPref = new PreferredComponent(this, paramXmlPullParser);
    }

    public boolean onReadTag(String paramString, XmlPullParser paramXmlPullParser)
        throws XmlPullParserException, IOException
    {
        if (paramString.equals("filter"))
            readFromXml(paramXmlPullParser);
        while (true)
        {
            return true;
            PackageManagerService.reportSettingsProblem(5, "Unknown element under <preferred-activities>: " + paramXmlPullParser.getName());
            XmlUtils.skipCurrentTag(paramXmlPullParser);
        }
    }

    public void writeToXml(XmlSerializer paramXmlSerializer)
        throws IOException
    {
        this.mPref.writeToXml(paramXmlSerializer);
        paramXmlSerializer.startTag(null, "filter");
        super.writeToXml(paramXmlSerializer);
        paramXmlSerializer.endTag(null, "filter");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.pm.PreferredActivity
 * JD-Core Version:        0.6.2
 */