package com.android.server.pm;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.ActivityManagerNative;
import android.app.IActivityManager;
import android.app.backup.IBackupManager;
import android.app.backup.IBackupManager.Stub;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.IIntentReceiver;
import android.content.IIntentReceiver.Stub;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.IntentSender.SendIntentException;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.ContainerEncryptionParams;
import android.content.pm.FeatureInfo;
import android.content.pm.IPackageDataObserver;
import android.content.pm.IPackageDeleteObserver;
import android.content.pm.IPackageInstallObserver;
import android.content.pm.IPackageManager;
import android.content.pm.IPackageManager.Stub;
import android.content.pm.IPackageMoveObserver;
import android.content.pm.IPackageStatsObserver;
import android.content.pm.InstrumentationInfo;
import android.content.pm.ManifestDigest;
import android.content.pm.PackageInfo;
import android.content.pm.PackageInfoLite;
import android.content.pm.PackageParser;
import android.content.pm.PackageParser.Activity;
import android.content.pm.PackageParser.ActivityIntentInfo;
import android.content.pm.PackageParser.Instrumentation;
import android.content.pm.PackageParser.NewPermissionInfo;
import android.content.pm.PackageParser.Package;
import android.content.pm.PackageParser.Permission;
import android.content.pm.PackageParser.PermissionGroup;
import android.content.pm.PackageParser.Provider;
import android.content.pm.PackageParser.Service;
import android.content.pm.PackageParser.ServiceIntentInfo;
import android.content.pm.PackageStats;
import android.content.pm.ParceledListSlice;
import android.content.pm.PermissionGroupInfo;
import android.content.pm.PermissionInfo;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.pm.Signature;
import android.content.pm.UserInfo;
import android.content.pm.VerifierDeviceIdentity;
import android.content.pm.VerifierInfo;
import android.net.Uri;
import android.os.Binder;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.os.FileObserver;
import android.os.FileUtils;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.UserId;
import android.provider.Settings.Secure;
import android.provider.Settings.System;
import android.security.SystemKeyStore;
import android.util.DisplayMetrics;
import android.util.EventLog;
import android.util.Log;
import android.util.LogPrinter;
import android.util.Slog;
import android.util.SparseArray;
import android.util.Xml;
import android.view.Display;
import android.view.WindowManager;
import com.android.internal.app.IMediaContainerService;
import com.android.internal.app.IMediaContainerService.Stub;
import com.android.internal.content.NativeLibraryHelper;
import com.android.internal.content.PackageHelper;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.XmlUtils;
import com.android.server.DeviceStorageMonitorService;
import com.android.server.IntentResolver;
import com.android.server.PreferredComponent;
import dalvik.system.DexFile;
import dalvik.system.StaleDexCacheError;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import miui.content.pm.ExtraPackageManager;
import miui.provider.ExtraGuard;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class PackageManagerService extends IPackageManager.Stub
{
    private static final int ADD_EVENTS = 136;
    static final int BROADCAST_DELAY = 10000;
    static final int CHECK_PENDING_VERIFICATION = 16;
    private static final boolean DEBUG_APP_DIR_OBSERVER = false;
    private static final boolean DEBUG_INSTALL = false;
    private static final boolean DEBUG_INTENT_MATCHING = false;
    private static final boolean DEBUG_PACKAGE_INFO = false;
    private static final boolean DEBUG_PACKAGE_SCANNING = false;
    private static final boolean DEBUG_PREFERRED = false;
    private static final boolean DEBUG_REMOVE = false;
    static final boolean DEBUG_SD_INSTALL = false;
    static final boolean DEBUG_SETTINGS = false;
    private static final boolean DEBUG_SHOW_INFO = false;
    static final boolean DEBUG_UPGRADE = false;
    private static final boolean DEBUG_VERIFY = false;
    static final ComponentName DEFAULT_CONTAINER_COMPONENT = new ComponentName("com.android.defcontainer", "com.android.defcontainer.DefaultContainerService");
    static final String DEFAULT_CONTAINER_PACKAGE = "com.android.defcontainer";
    private static final long DEFAULT_VERIFICATION_TIMEOUT = 60000L;
    private static final boolean DEFAULT_VERIFY_ENABLE = false;
    static final int DEX_OPT_DEFERRED = 2;
    static final int DEX_OPT_FAILED = -1;
    static final int DEX_OPT_PERFORMED = 1;
    static final int DEX_OPT_SKIPPED = 0;
    static final int END_COPY = 4;
    static final int FIND_INSTALL_LOC = 8;
    private static final boolean GET_CERTIFICATES = true;
    static final int INIT_COPY = 5;
    private static final String INSTALL_PACKAGE_SUFFIX = "-";
    private static final String LIB_DIR_NAME = "lib";
    private static final int LOG_UID = 1007;
    static final int MCS_BOUND = 3;
    static final int MCS_GIVE_UP = 11;
    static final int MCS_RECONNECT = 10;
    static final int MCS_UNBIND = 6;
    private static final int NFC_UID = 1027;
    private static final int OBSERVER_EVENTS = 712;
    private static final String PACKAGE_MIME_TYPE = "application/vnd.android.package-archive";
    static final int PACKAGE_VERIFIED = 15;
    static final int POST_INSTALL = 9;
    private static final int RADIO_UID = 1001;
    static final int REMOVE_CHATTY = 65536;
    private static final int REMOVE_EVENTS = 584;
    static final int SCAN_BOOTING = 256;
    static final int SCAN_DEFER_DEX = 128;
    static final int SCAN_FORCE_DEX = 4;
    static final int SCAN_MONITOR = 1;
    static final int SCAN_NEW_INSTALL = 16;
    static final int SCAN_NO_DEX = 2;
    static final int SCAN_NO_PATHS = 32;
    static final int SCAN_UPDATE_SIGNATURE = 8;
    static final int SCAN_UPDATE_TIME = 64;
    private static final String SD_ENCRYPTION_ALGORITHM = "AES";
    private static final String SD_ENCRYPTION_KEYSTORE_NAME = "AppsOnSD";
    static final int SEND_PENDING_BROADCAST = 1;
    static final int START_CLEANING_PACKAGE = 7;
    static final String TAG = "PackageManager";
    static final int UPDATED_MEDIA_STATUS = 12;
    static final int UPDATE_PERMISSIONS_ALL = 1;
    static final int UPDATE_PERMISSIONS_REPLACE_ALL = 4;
    static final int UPDATE_PERMISSIONS_REPLACE_PKG = 2;
    static final int WRITE_PACKAGE_RESTRICTIONS = 14;
    static final int WRITE_SETTINGS = 13;
    static final int WRITE_SETTINGS_DELAY = 10000;
    private static final Comparator<ProviderInfo> mProviderInitOrderSorter = new Comparator()
    {
        public int compare(ProviderInfo paramAnonymousProviderInfo1, ProviderInfo paramAnonymousProviderInfo2)
        {
            int i = paramAnonymousProviderInfo1.initOrder;
            int j = paramAnonymousProviderInfo2.initOrder;
            int k;
            if (i > j)
                k = -1;
            while (true)
            {
                return k;
                if (i < j)
                    k = 1;
                else
                    k = 0;
            }
        }
    };
    private static final Comparator<ResolveInfo> mResolvePrioritySorter = new Comparator()
    {
        public int compare(ResolveInfo paramAnonymousResolveInfo1, ResolveInfo paramAnonymousResolveInfo2)
        {
            int i = -1;
            int j = paramAnonymousResolveInfo1.priority;
            int k = paramAnonymousResolveInfo2.priority;
            if (j != k)
                if (j <= k);
            while (true)
            {
                return i;
                i = 1;
                continue;
                int m = paramAnonymousResolveInfo1.preferredOrder;
                int n = paramAnonymousResolveInfo2.preferredOrder;
                if (m != n)
                {
                    if (m <= n)
                        i = 1;
                }
                else if (paramAnonymousResolveInfo1.isDefault != paramAnonymousResolveInfo2.isDefault)
                {
                    if (!paramAnonymousResolveInfo1.isDefault)
                        i = 1;
                }
                else
                {
                    int i1 = paramAnonymousResolveInfo1.match;
                    int i2 = paramAnonymousResolveInfo2.match;
                    if (i1 != i2)
                    {
                        if (i1 <= i2)
                            i = 1;
                    }
                    else if (paramAnonymousResolveInfo1.system != paramAnonymousResolveInfo2.system)
                    {
                        if (!paramAnonymousResolveInfo1.system)
                            i = 1;
                    }
                    else
                        i = 0;
                }
            }
        }
    };
    static final String mTempContainerPrefix = "smdl2tmp";
    static UserManager sUserManager;
    final ActivityIntentResolver mActivities;
    ApplicationInfo mAndroidApplication;
    final File mAppDataDir;
    final HashMap<String, PackageParser.Package> mAppDirs;
    final File mAppInstallDir;
    final FileObserver mAppInstallObserver;
    final String mAsecInternalPath;
    final HashMap<String, FeatureInfo> mAvailableFeatures;
    private IMediaContainerService mContainerService;
    final Context mContext;
    final File mDalvikCacheDir;
    private final DefaultContainerConnection mDefContainerConn;
    final int mDefParseFlags;
    final ArrayList<PackageParser.Package> mDeferredDexOpt;
    private HashSet<Integer> mDirtyUsers;
    final FileObserver mDrmAppInstallObserver;
    final File mDrmAppPrivateInstallDir;
    final boolean mFactoryTest;
    final File mFrameworkDir;
    final FileObserver mFrameworkInstallObserver;
    int[] mGlobalGids;
    final PackageHandler mHandler;
    final HandlerThread mHandlerThread = new HandlerThread("PackageManager", 10);
    boolean mHasSystemUidErrors;
    final Object mInstallLock;
    final Installer mInstaller;
    final HashMap<ComponentName, PackageParser.Instrumentation> mInstrumentation;
    int mLastScanError;
    private boolean mMediaMounted;
    final DisplayMetrics mMetrics;
    int mNextInstallToken;
    final boolean mNoDexOpt;
    final boolean mOnlyCore;
    final int[] mOutPermissions;
    final HashMap<String, PackageParser.Package> mPackages;
    final HashMap<String, ArrayList<String>> mPendingBroadcasts;
    final SparseArray<PackageVerificationState> mPendingVerification;
    private int mPendingVerificationToken;
    final HashMap<String, PackageParser.PermissionGroup> mPermissionGroups;
    PackageParser.Package mPlatformPackage;
    final HashSet<String> mProtectedBroadcasts;
    final HashMap<String, PackageParser.Provider> mProviders;
    final HashMap<ComponentName, PackageParser.Provider> mProvidersByComponent;
    final ActivityIntentResolver mReceivers;
    private final String mRequiredVerifierPackage;
    final ActivityInfo mResolveActivity;
    ComponentName mResolveComponentName;
    final ResolveInfo mResolveInfo;
    boolean mRestoredSettings;
    final SparseArray<PostInstallData> mRunningInstalls;
    boolean mSafeMode;
    File mScanningPath;
    final String mSdkCodename;
    final int mSdkVersion = Build.VERSION.SDK_INT;
    final String[] mSeparateProcesses;
    final ServiceIntentResolver mServices;
    final Settings mSettings;
    final HashMap<String, String> mSharedLibraries;
    final File mSystemAppDir;
    final FileObserver mSystemInstallObserver;
    final SparseArray<HashSet<String>> mSystemPermissions;
    boolean mSystemReady;
    String[] mTmpSharedLibraries;
    final HashSet<String> mTransferedPackages;
    final File mUserAppDataDir;
    final File mVendorAppDir;
    final FileObserver mVendorInstallObserver;

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public PackageManagerService(Context paramContext, boolean paramBoolean1, boolean paramBoolean2)
    {
        String str1;
        if ("REL".equals(Build.VERSION.CODENAME))
            str1 = null;
        label534: File localFile1;
        long l;
        int i;
        HashSet localHashSet;
        int j;
        while (true)
        {
            this.mSdkCodename = str1;
            this.mInstallLock = new Object();
            this.mAppDirs = new HashMap();
            this.mOutPermissions = new int[3];
            this.mPackages = new HashMap();
            this.mSystemPermissions = new SparseArray();
            this.mSharedLibraries = new HashMap();
            this.mTmpSharedLibraries = null;
            this.mAvailableFeatures = new HashMap();
            this.mActivities = new ActivityIntentResolver(null);
            this.mReceivers = new ActivityIntentResolver(null);
            this.mServices = new ServiceIntentResolver(null);
            this.mProvidersByComponent = new HashMap();
            this.mProviders = new HashMap();
            this.mInstrumentation = new HashMap();
            this.mPermissionGroups = new HashMap();
            this.mTransferedPackages = new HashSet();
            this.mProtectedBroadcasts = new HashSet();
            this.mPendingVerification = new SparseArray();
            this.mDeferredDexOpt = new ArrayList();
            this.mPendingVerificationToken = 0;
            this.mResolveActivity = new ActivityInfo();
            this.mResolveInfo = new ResolveInfo();
            this.mPendingBroadcasts = new HashMap();
            this.mContainerService = null;
            this.mDirtyUsers = new HashSet();
            this.mDefContainerConn = new DefaultContainerConnection();
            this.mRunningInstalls = new SparseArray();
            this.mNextInstallToken = 1;
            this.mMediaMounted = false;
            EventLog.writeEvent(3060, SystemClock.uptimeMillis());
            if (this.mSdkVersion <= 0)
                Slog.w("PackageManager", "**** ro.build.version.sdk not set!");
            this.mContext = paramContext;
            this.mFactoryTest = paramBoolean1;
            this.mOnlyCore = paramBoolean2;
            this.mNoDexOpt = "eng".equals(SystemProperties.get("ro.build.type"));
            this.mMetrics = new DisplayMetrics();
            this.mSettings = new Settings();
            this.mSettings.addSharedUserLPw("android.uid.system", 1000, 1);
            this.mSettings.addSharedUserLPw("android.uid.phone", 1001, 1);
            this.mSettings.addSharedUserLPw("android.uid.log", 1007, 1);
            this.mSettings.addSharedUserLPw("android.uid.nfc", 1027, 1);
            Injector.addMiuiSharedUids(this);
            String str2 = SystemProperties.get("debug.separate_processes");
            String[] arrayOfString3;
            int i8;
            if ((str2 != null) && (str2.length() > 0))
                if ("*".equals(str2))
                {
                    this.mDefParseFlags = 8;
                    this.mSeparateProcesses = null;
                    Slog.w("PackageManager", "Running with debug.separate_processes: * (ALL)");
                    this.mInstaller = new Installer();
                    ((WindowManager)paramContext.getSystemService("window")).getDefaultDisplay().getMetrics(this.mMetrics);
                    synchronized (this.mInstallLock)
                    {
                        synchronized (this.mPackages)
                        {
                            this.mHandlerThread.start();
                            this.mHandler = new PackageHandler(this.mHandlerThread.getLooper());
                            localFile1 = Environment.getDataDirectory();
                            this.mAppDataDir = new File(localFile1, "data");
                            this.mAsecInternalPath = new File(localFile1, "app-asec").getPath();
                            this.mUserAppDataDir = new File(localFile1, "user");
                            this.mDrmAppPrivateInstallDir = new File(localFile1, "app-private");
                            sUserManager = new UserManager(this.mInstaller, this.mUserAppDataDir);
                            readPermissions();
                            this.mRestoredSettings = this.mSettings.readLPw(getUsers());
                            l = SystemClock.uptimeMillis();
                            EventLog.writeEvent(3070, l);
                            i = 417;
                            if (this.mNoDexOpt)
                            {
                                Slog.w("PackageManager", "Running ENG build: no pre-dexopt!");
                                i |= 2;
                            }
                            localHashSet = new HashSet();
                            this.mFrameworkDir = new File(Environment.getRootDirectory(), "framework");
                            this.mDalvikCacheDir = new File(localFile1, "dalvik-cache");
                            j = 0;
                            String str3 = System.getProperty("java.boot.class.path");
                            if (str3 == null)
                                break label1059;
                            arrayOfString3 = splitString(str3, ':');
                            i8 = 0;
                            label831: int i9 = arrayOfString3.length;
                            if (i8 >= i9)
                                break label1068;
                        }
                    }
                }
            try
            {
                if (DexFile.isDexOptNeeded(arrayOfString3[i8]))
                {
                    localHashSet.add(arrayOfString3[i8]);
                    this.mInstaller.dexopt(arrayOfString3[i8], 1000, true);
                    j = 1;
                }
                i8++;
                break label831;
                str1 = Build.VERSION.CODENAME;
                continue;
                this.mDefParseFlags = 0;
                this.mSeparateProcesses = str2.split(",");
                Slog.w("PackageManager", "Running with debug.separate_processes: " + str2);
                break label534;
                this.mDefParseFlags = 0;
                this.mSeparateProcesses = null;
            }
            catch (FileNotFoundException localFileNotFoundException3)
            {
                while (true)
                    Slog.w("PackageManager", "Boot class path not found: " + arrayOfString3[i8]);
                localObject3 = finally;
                throw localObject3;
                localObject2 = finally;
                throw localObject2;
            }
            catch (IOException localIOException3)
            {
                while (true)
                    Slog.w("PackageManager", "Cannot dexopt " + arrayOfString3[i8] + "; is it an APK or JAR? " + localIOException3.getMessage());
            }
        }
        label1059: Slog.w("PackageManager", "No BOOTCLASSPATH found!");
        label1068: if (this.mSharedLibraries.size() > 0)
        {
            Iterator localIterator4 = this.mSharedLibraries.values().iterator();
            while (localIterator4.hasNext())
            {
                String str8 = (String)localIterator4.next();
                try
                {
                    if (DexFile.isDexOptNeeded(str8))
                    {
                        localHashSet.add(str8);
                        this.mInstaller.dexopt(str8, 1000, true);
                        j = 1;
                    }
                }
                catch (FileNotFoundException localFileNotFoundException2)
                {
                    Slog.w("PackageManager", "Library not found: " + str8);
                }
                catch (IOException localIOException2)
                {
                    Slog.w("PackageManager", "Cannot dexopt " + str8 + "; is it an APK or JAR? " + localIOException2.getMessage());
                }
            }
        }
        localHashSet.add(this.mFrameworkDir.getPath() + "/framework-res.apk");
        Injector.ignoreMiuiFrameworkRes(this, localHashSet);
        String[] arrayOfString1 = this.mFrameworkDir.list();
        int i6;
        if (arrayOfString1 != null)
        {
            i6 = 0;
            int i7 = arrayOfString1.length;
            if (i6 < i7)
            {
                File localFile2 = new File(this.mFrameworkDir, arrayOfString1[i6]);
                String str7 = localFile2.getPath();
                if (localHashSet.contains(str7))
                    break label2752;
                if (!str7.endsWith(".apk"))
                {
                    boolean bool = str7.endsWith(".jar");
                    if (!bool)
                        break label2752;
                }
                try
                {
                    if (!DexFile.isDexOptNeeded(str7))
                        break label2752;
                    this.mInstaller.dexopt(str7, 1000, true);
                    j = 1;
                }
                catch (FileNotFoundException localFileNotFoundException1)
                {
                    Slog.w("PackageManager", "Jar not found: " + str7);
                }
                catch (IOException localIOException1)
                {
                    Slog.w("PackageManager", "Exception reading jar: " + str7, localIOException1);
                }
            }
        }
        int i4;
        if (j != 0)
        {
            String[] arrayOfString2 = this.mDalvikCacheDir.list();
            if (arrayOfString2 != null)
            {
                i4 = 0;
                label1475: int i5 = arrayOfString2.length;
                if (i4 < i5)
                {
                    String str6 = arrayOfString2[i4];
                    if ((!str6.startsWith("data@app@")) && (!str6.startsWith("data@app-private@")))
                        break label2758;
                    Slog.i("PackageManager", "Pruning dalvik file: " + str6);
                    new File(this.mDalvikCacheDir, str6).delete();
                    break label2758;
                }
            }
        }
        this.mFrameworkInstallObserver = new AppDirObserver(this.mFrameworkDir.getPath(), 712, true);
        this.mFrameworkInstallObserver.startWatching();
        scanDirLI(this.mFrameworkDir, 65, i | 0x2, 0L);
        this.mSystemAppDir = new File(Environment.getRootDirectory(), "app");
        this.mSystemInstallObserver = new AppDirObserver(this.mSystemAppDir.getPath(), 712, true);
        this.mSystemInstallObserver.startWatching();
        scanDirLI(this.mSystemAppDir, 65, i, 0L);
        this.mVendorAppDir = new File("/vendor/app");
        this.mVendorInstallObserver = new AppDirObserver(this.mVendorAppDir.getPath(), 712, true);
        this.mVendorInstallObserver.startWatching();
        scanDirLI(this.mVendorAppDir, 65, i, 0L);
        this.mInstaller.moveFiles();
        ArrayList localArrayList1 = new ArrayList();
        if (!this.mOnlyCore)
        {
            Iterator localIterator3 = this.mSettings.mPackages.values().iterator();
            while (localIterator3.hasNext())
            {
                PackageSetting localPackageSetting2 = (PackageSetting)localIterator3.next();
                if ((0x1 & localPackageSetting2.pkgFlags) != 0)
                {
                    PackageParser.Package localPackage2 = (PackageParser.Package)this.mPackages.get(localPackageSetting2.name);
                    if (localPackage2 != null)
                    {
                        if (this.mSettings.isDisabledSystemPackageLPr(localPackageSetting2.name))
                        {
                            Slog.i("PackageManager", "Expecting better updatd system app for " + localPackageSetting2.name + "; removing system app");
                            removePackageLI(localPackage2, true);
                        }
                    }
                    else if (!this.mSettings.isDisabledSystemPackageLPr(localPackageSetting2.name))
                    {
                        localIterator3.remove();
                        reportSettingsProblem(5, "System package " + localPackageSetting2.name + " no longer exists; wiping its data");
                        this.mInstaller.remove(localPackageSetting2.name, 0);
                        sUserManager.removePackageForAllUsers(localPackageSetting2.name);
                    }
                    else
                    {
                        PackageSetting localPackageSetting3 = this.mSettings.getDisabledSystemPkgLPr(localPackageSetting2.name);
                        if ((localPackageSetting3.codePath == null) || (!localPackageSetting3.codePath.exists()))
                            localArrayList1.add(localPackageSetting2.name);
                    }
                }
            }
        }
        this.mAppInstallDir = new File(localFile1, "app");
        ArrayList localArrayList2 = this.mSettings.getListOfIncompleteInstallPackagesLPr();
        for (int k = 0; ; k++)
        {
            int m = localArrayList2.size();
            if (k >= m)
                break;
            cleanupInstallFailedPackage((PackageSetting)localArrayList2.get(k));
        }
        deleteTempPackageFiles();
        ExtraPackageManagerServices.performPreinstallApp(this.mSettings);
        if (!this.mOnlyCore)
        {
            EventLog.writeEvent(3080, SystemClock.uptimeMillis());
            this.mAppInstallObserver = new AppDirObserver(this.mAppInstallDir.getPath(), 712, false);
            this.mAppInstallObserver.startWatching();
            scanDirLI(this.mAppInstallDir, 0, i, 0L);
            this.mDrmAppInstallObserver = new AppDirObserver(this.mDrmAppPrivateInstallDir.getPath(), 712, false);
            this.mDrmAppInstallObserver.startWatching();
            scanDirLI(this.mDrmAppPrivateInstallDir, 16, i, 0L);
            Iterator localIterator2 = localArrayList1.iterator();
            if (localIterator2.hasNext())
            {
                String str4 = (String)localIterator2.next();
                PackageParser.Package localPackage1 = (PackageParser.Package)this.mPackages.get(str4);
                this.mSettings.removeDisabledSystemPackageLPw(str4);
                String str5;
                if (localPackage1 == null)
                {
                    str5 = "Updated system package " + str4 + " no longer exists; wiping its data";
                    this.mInstaller.remove(str4, 0);
                    sUserManager.removePackageForAllUsers(str4);
                }
                while (true)
                {
                    reportSettingsProblem(5, str5);
                    break;
                    str5 = "Updated system app + " + str4 + " no longer present; removing system privileges for " + str4;
                    ApplicationInfo localApplicationInfo = localPackage1.applicationInfo;
                    localApplicationInfo.flags = (0xFFFFFFFE & localApplicationInfo.flags);
                    PackageSetting localPackageSetting1 = (PackageSetting)this.mSettings.mPackages.get(str4);
                    localPackageSetting1.pkgFlags = (0xFFFFFFFE & localPackageSetting1.pkgFlags);
                }
            }
        }
        else
        {
            this.mAppInstallObserver = null;
            this.mDrmAppInstallObserver = null;
        }
        EventLog.writeEvent(3090, SystemClock.uptimeMillis());
        Slog.i("PackageManager", "Time to scan packages: " + (float)(SystemClock.uptimeMillis() - l) / 1000.0F + " seconds");
        int n;
        label2471: int i1;
        label2547: ArrayList localArrayList3;
        if (this.mSettings.mInternalSdkPlatform != this.mSdkVersion)
        {
            n = 1;
            if (n != 0)
                Slog.i("PackageManager", "Platform changed from " + this.mSettings.mInternalSdkPlatform + " to " + this.mSdkVersion + "; regranting permissions for internal storage");
            this.mSettings.mInternalSdkPlatform = this.mSdkVersion;
            if (n == 0)
                break label2770;
            i1 = 6;
            updatePermissionsLPw(null, null, i1 | 0x1);
            localArrayList3 = new ArrayList();
            Iterator localIterator1 = this.mSettings.mPreferredActivities.filterSet().iterator();
            while (localIterator1.hasNext())
            {
                PreferredActivity localPreferredActivity2 = (PreferredActivity)localIterator1.next();
                if (this.mActivities.mActivities.get(localPreferredActivity2.mPref.mComponent) == null)
                    localArrayList3.add(localPreferredActivity2);
            }
        }
        while (true)
        {
            int i3 = localArrayList3.size();
            int i2;
            if (i2 < i3)
            {
                PreferredActivity localPreferredActivity1 = (PreferredActivity)localArrayList3.get(i2);
                Slog.w("PackageManager", "Removing dangling preferred activity: " + localPreferredActivity1.mPref.mComponent);
                this.mSettings.mPreferredActivities.removeFilter(localPreferredActivity1);
                i2++;
            }
            else
            {
                this.mSettings.writeLPr();
                EventLog.writeEvent(3100, SystemClock.uptimeMillis());
                Runtime.getRuntime().gc();
                this.mRequiredVerifierPackage = getRequiredVerifierLPr();
                return;
                label2752: i6++;
                break;
                label2758: i4++;
                break label1475;
                n = 0;
                break label2471;
                label2770: i1 = 0;
                break label2547;
                i2 = 0;
            }
        }
    }

    static int[] appendInts(int[] paramArrayOfInt1, int[] paramArrayOfInt2)
    {
        if (paramArrayOfInt2 == null);
        while (true)
        {
            return paramArrayOfInt1;
            if (paramArrayOfInt1 == null)
            {
                paramArrayOfInt1 = paramArrayOfInt2;
            }
            else
            {
                int i = paramArrayOfInt2.length;
                for (int j = 0; j < i; j++)
                    paramArrayOfInt1 = ArrayUtils.appendInt(paramArrayOfInt1, paramArrayOfInt2[j]);
            }
        }
    }

    static String arrayToString(int[] paramArrayOfInt)
    {
        StringBuffer localStringBuffer = new StringBuffer(128);
        localStringBuffer.append('[');
        if (paramArrayOfInt != null)
            for (int i = 0; i < paramArrayOfInt.length; i++)
            {
                if (i > 0)
                    localStringBuffer.append(", ");
                localStringBuffer.append(paramArrayOfInt[i]);
            }
        localStringBuffer.append(']');
        return localStringBuffer.toString();
    }

    private BasePermission checkPermissionTreeLP(String paramString)
    {
        if (paramString != null)
        {
            BasePermission localBasePermission = findPermissionTreeLP(paramString);
            if (localBasePermission != null)
            {
                if (localBasePermission.uid == UserId.getAppId(Binder.getCallingUid()))
                    return localBasePermission;
                throw new SecurityException("Calling uid " + Binder.getCallingUid() + " is not allowed to add to permission tree " + localBasePermission.name + " owned by uid " + localBasePermission.uid);
            }
        }
        throw new SecurityException("No permission tree found for " + paramString);
    }

    private void checkValidCaller(int paramInt1, int paramInt2)
    {
        if ((UserId.getUserId(paramInt1) == paramInt2) || (paramInt1 == 1000) || (paramInt1 == 0))
            return;
        throw new SecurityException("Caller uid=" + paramInt1 + " is not privileged to communicate with user=" + paramInt2);
    }

    private ResolveInfo chooseBestActivity(Intent paramIntent, String paramString, int paramInt1, List<ResolveInfo> paramList, int paramInt2)
    {
        int i;
        Object localObject;
        if (paramList != null)
        {
            i = paramList.size();
            if (i == 1)
                localObject = (ResolveInfo)paramList.get(0);
        }
        while (true)
        {
            return localObject;
            if (i > 1)
            {
                ResolveInfo localResolveInfo1 = (ResolveInfo)paramList.get(0);
                ResolveInfo localResolveInfo2 = (ResolveInfo)paramList.get(1);
                if ((localResolveInfo1.priority != localResolveInfo2.priority) || (localResolveInfo1.preferredOrder != localResolveInfo2.preferredOrder) || (localResolveInfo1.isDefault != localResolveInfo2.isDefault))
                {
                    localObject = (ResolveInfo)paramList.get(0);
                }
                else
                {
                    ResolveInfo localResolveInfo3 = findPreferredActivity(paramIntent, paramString, paramInt1, paramList, localResolveInfo1.priority, paramInt2);
                    if (localResolveInfo3 != null)
                        localObject = localResolveInfo3;
                    else
                        localObject = this.mResolveInfo;
                }
            }
            else
            {
                localObject = null;
            }
        }
    }

    static String cidFromCodePath(String paramString)
    {
        int i = paramString.lastIndexOf("/");
        String str = paramString.substring(0, i);
        return str.substring(1 + str.lastIndexOf("/"), i);
    }

    // ERROR //
    private void cleanUpUser(int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     11: getfield 783	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
        //     14: invokevirtual 1235	java/util/HashMap:entrySet	()Ljava/util/Set;
        //     17: invokeinterface 923 1 0
        //     22: astore 4
        //     24: aload 4
        //     26: invokeinterface 702 1 0
        //     31: ifeq +33 -> 64
        //     34: aload 4
        //     36: invokeinterface 706 1 0
        //     41: checkcast 1237	java/util/Map$Entry
        //     44: invokeinterface 1240 1 0
        //     49: checkcast 785	com/android/server/pm/PackageSetting
        //     52: iload_1
        //     53: invokevirtual 1243	com/android/server/pm/PackageSetting:removeUser	(I)V
        //     56: goto -32 -> 24
        //     59: astore_3
        //     60: aload_2
        //     61: monitorexit
        //     62: aload_3
        //     63: athrow
        //     64: aload_0
        //     65: getfield 441	com/android/server/pm/PackageManagerService:mDirtyUsers	Ljava/util/HashSet;
        //     68: iload_1
        //     69: invokestatic 1249	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     72: invokevirtual 1251	java/util/HashSet:remove	(Ljava/lang/Object;)Z
        //     75: ifeq +3 -> 78
        //     78: aload_0
        //     79: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     82: iload_1
        //     83: invokevirtual 1254	com/android/server/pm/Settings:removeUserLPr	(I)V
        //     86: aload_2
        //     87: monitorexit
        //     88: return
        //
        // Exception table:
        //     from	to	target	type
        //     7	62	59	finally
        //     64	88	59	finally
    }

    // ERROR //
    private boolean clearApplicationUserDataLI(String paramString, int paramInt)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore_3
        //     2: aload_1
        //     3: ifnonnull +14 -> 17
        //     6: ldc 186
        //     8: ldc_w 1256
        //     11: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     14: pop
        //     15: iload_3
        //     16: ireturn
        //     17: iconst_0
        //     18: istore 4
        //     20: aload_0
        //     21: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     24: astore 5
        //     26: aload 5
        //     28: monitorenter
        //     29: aload_0
        //     30: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     33: aload_1
        //     34: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     37: checkcast 800	android/content/pm/PackageParser$Package
        //     40: astore 7
        //     42: aload 7
        //     44: ifnonnull +88 -> 132
        //     47: iconst_1
        //     48: istore 4
        //     50: aload_0
        //     51: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     54: getfield 783	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
        //     57: aload_1
        //     58: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     61: checkcast 785	com/android/server/pm/PackageSetting
        //     64: astore 11
        //     66: aload 11
        //     68: ifnull +11 -> 79
        //     71: aload 11
        //     73: getfield 1259	com/android/server/pm/PackageSetting:pkg	Landroid/content/pm/PackageParser$Package;
        //     76: ifnonnull +49 -> 125
        //     79: ldc 186
        //     81: new 662	java/lang/StringBuilder
        //     84: dup
        //     85: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     88: ldc_w 1261
        //     91: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     94: aload_1
        //     95: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     98: ldc_w 1263
        //     101: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     104: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     107: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     110: pop
        //     111: aload 5
        //     113: monitorexit
        //     114: goto -99 -> 15
        //     117: astore 6
        //     119: aload 5
        //     121: monitorexit
        //     122: aload 6
        //     124: athrow
        //     125: aload 11
        //     127: getfield 1259	com/android/server/pm/PackageSetting:pkg	Landroid/content/pm/PackageParser$Package;
        //     130: astore 7
        //     132: aload 5
        //     134: monitorexit
        //     135: iload 4
        //     137: ifne +86 -> 223
        //     140: aload 7
        //     142: ifnonnull +38 -> 180
        //     145: ldc 186
        //     147: new 662	java/lang/StringBuilder
        //     150: dup
        //     151: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     154: ldc_w 1261
        //     157: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     160: aload_1
        //     161: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     164: ldc_w 1263
        //     167: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     170: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     173: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     176: pop
        //     177: goto -162 -> 15
        //     180: aload 7
        //     182: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     185: ifnonnull +38 -> 223
        //     188: ldc 186
        //     190: new 662	java/lang/StringBuilder
        //     193: dup
        //     194: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     197: ldc_w 1265
        //     200: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     203: aload_1
        //     204: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     207: ldc_w 1267
        //     210: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     213: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     216: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     219: pop
        //     220: goto -205 -> 15
        //     223: aload_0
        //     224: getfield 534	com/android/server/pm/PackageManagerService:mInstaller	Lcom/android/server/pm/Installer;
        //     227: aload_1
        //     228: iload_2
        //     229: invokevirtual 1270	com/android/server/pm/Installer:clearUserData	(Ljava/lang/String;I)I
        //     232: ifge +32 -> 264
        //     235: ldc 186
        //     237: new 662	java/lang/StringBuilder
        //     240: dup
        //     241: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     244: ldc_w 1272
        //     247: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     250: aload_1
        //     251: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     254: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     257: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     260: pop
        //     261: goto -246 -> 15
        //     264: iconst_1
        //     265: istore_3
        //     266: goto -251 -> 15
        //
        // Exception table:
        //     from	to	target	type
        //     29	122	117	finally
        //     125	135	117	finally
    }

    private void clearExternalStorageDataSync(String paramString, boolean paramBoolean)
    {
        int i;
        if (Environment.isExternalStorageEmulated())
        {
            i = 1;
            if (i != 0)
                break label51;
        }
        label51: Intent localIntent;
        ClearStorageConnection localClearStorageConnection;
        do
        {
            return;
            String str = Environment.getExternalStorageState();
            if ((str.equals("mounted")) || (str.equals("mounted_ro")));
            for (i = 1; ; i = 0)
                break;
            localIntent = new Intent().setComponent(DEFAULT_CONTAINER_COMPONENT);
            localClearStorageConnection = new ClearStorageConnection(null);
        }
        while (!this.mContext.bindService(localIntent, localClearStorageConnection, 1));
        while (true)
        {
            Context localContext;
            try
            {
                while (true)
                {
                    long l1 = 5000L + SystemClock.uptimeMillis();
                    try
                    {
                        long l2 = SystemClock.uptimeMillis();
                        while (true)
                        {
                            IMediaContainerService localIMediaContainerService1 = localClearStorageConnection.mContainerService;
                            if ((localIMediaContainerService1 != null) || (l2 >= l1))
                                break;
                            long l3 = l1 - l2;
                            try
                            {
                                localClearStorageConnection.wait(l3);
                            }
                            catch (InterruptedException localInterruptedException)
                            {
                            }
                        }
                        IMediaContainerService localIMediaContainerService2 = localClearStorageConnection.mContainerService;
                        if (localIMediaContainerService2 == null)
                        {
                            localContext = this.mContext;
                            localContext.unbindService(localClearStorageConnection);
                            break;
                        }
                    }
                    finally
                    {
                    }
                }
            }
            finally
            {
                this.mContext.unbindService(localClearStorageConnection);
            }
            File localFile1 = Environment.getExternalStorageAppCacheDirectory(paramString);
            try
            {
                localClearStorageConnection.mContainerService.clearDirectory(localFile1.toString());
                label225: File localFile2;
                if (paramBoolean)
                    localFile2 = Environment.getExternalStorageAppDataDirectory(paramString);
                try
                {
                    localClearStorageConnection.mContainerService.clearDirectory(localFile2.toString());
                    label250: File localFile3 = Environment.getExternalStorageAppMediaDirectory(paramString);
                    try
                    {
                        localClearStorageConnection.mContainerService.clearDirectory(localFile3.toString());
                        label271: localContext = this.mContext;
                    }
                    catch (RemoteException localRemoteException3)
                    {
                        break label271;
                    }
                }
                catch (RemoteException localRemoteException2)
                {
                    break label250;
                }
            }
            catch (RemoteException localRemoteException1)
            {
                break label225;
            }
        }
    }

    private boolean collectCertificatesLI(PackageParser paramPackageParser, PackageSetting paramPackageSetting, PackageParser.Package paramPackage, File paramFile, int paramInt)
    {
        boolean bool = true;
        if ((paramPackageSetting != null) && (paramPackageSetting.codePath.equals(paramFile)) && (paramPackageSetting.timeStamp == paramFile.lastModified()))
            if ((paramPackageSetting.signatures.mSignatures != null) && (paramPackageSetting.signatures.mSignatures.length != 0))
                paramPackage.mSignatures = paramPackageSetting.signatures.mSignatures;
        label157: 
        while (true)
        {
            return bool;
            Slog.w("PackageManager", "PackageSetting for " + paramPackageSetting.name + " is missing signatures.    Collecting certs again to recover them.");
            while (true)
            {
                if (paramPackageParser.collectCertificates(paramPackage, paramInt))
                    break label157;
                this.mLastScanError = paramPackageParser.getParseError();
                bool = false;
                break;
                Log.i("PackageManager", paramFile.toString() + " changed; collecting certs");
            }
        }
    }

    static boolean comparePermissionInfos(PermissionInfo paramPermissionInfo1, PermissionInfo paramPermissionInfo2)
    {
        boolean bool = false;
        if (paramPermissionInfo1.icon != paramPermissionInfo2.icon);
        while (true)
        {
            return bool;
            if ((paramPermissionInfo1.logo == paramPermissionInfo2.logo) && (paramPermissionInfo1.protectionLevel == paramPermissionInfo2.protectionLevel) && (compareStrings(paramPermissionInfo1.name, paramPermissionInfo2.name)) && (compareStrings(paramPermissionInfo1.nonLocalizedLabel, paramPermissionInfo2.nonLocalizedLabel)) && (compareStrings(paramPermissionInfo1.packageName, paramPermissionInfo2.packageName)))
                bool = true;
        }
    }

    static int compareSignatures(Signature[] paramArrayOfSignature1, Signature[] paramArrayOfSignature2)
    {
        int n;
        if (paramArrayOfSignature1 == null)
            if (paramArrayOfSignature2 == null)
                n = 1;
        while (true)
        {
            return n;
            n = -1;
            continue;
            if (paramArrayOfSignature2 == null)
            {
                n = -2;
            }
            else
            {
                HashSet localHashSet1 = new HashSet();
                int i = paramArrayOfSignature1.length;
                for (int j = 0; j < i; j++)
                    localHashSet1.add(paramArrayOfSignature1[j]);
                HashSet localHashSet2 = new HashSet();
                int k = paramArrayOfSignature2.length;
                for (int m = 0; m < k; m++)
                    localHashSet2.add(paramArrayOfSignature2[m]);
                if (localHashSet1.equals(localHashSet2))
                    n = 0;
                else
                    n = -3;
            }
        }
    }

    static boolean compareStrings(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
    {
        boolean bool = false;
        if (paramCharSequence1 == null)
            if (paramCharSequence2 != null);
        for (bool = true; ; bool = paramCharSequence1.equals(paramCharSequence2))
            do
                return bool;
            while ((paramCharSequence2 == null) || (paramCharSequence1.getClass() != paramCharSequence2.getClass()));
    }

    private InstallArgs createInstallArgs(int paramInt, String paramString1, String paramString2, String paramString3)
    {
        int i;
        if (installOnSd(paramInt))
        {
            i = 1;
            if (i == 0)
                break label73;
        }
        label73: for (Object localObject = new AsecInstallArgs(paramString1, paramString2, paramString3, installOnSd(paramInt), installForwardLocked(paramInt)); ; localObject = new FileInstallArgs(paramString1, paramString2, paramString3))
        {
            return localObject;
            if ((installForwardLocked(paramInt)) && (!paramString1.startsWith(this.mDrmAppPrivateInstallDir.getAbsolutePath())))
            {
                i = 1;
                break;
            }
            i = 0;
            break;
        }
    }

    private InstallArgs createInstallArgs(Uri paramUri, int paramInt, String paramString1, String paramString2)
    {
        if ((installOnSd(paramInt)) || (installForwardLocked(paramInt)));
        for (Object localObject = new AsecInstallArgs(paramUri, getNextCodePath(paramUri.getPath(), paramString1, "/pkg.apk"), installOnSd(paramInt), installForwardLocked(paramInt)); ; localObject = new FileInstallArgs(paramUri, paramString1, paramString2))
            return localObject;
    }

    private InstallArgs createInstallArgs(InstallParams paramInstallParams)
    {
        if ((installOnSd(paramInstallParams.flags)) || (paramInstallParams.isForwardLocked()));
        for (Object localObject = new AsecInstallArgs(paramInstallParams); ; localObject = new FileInstallArgs(paramInstallParams))
            return localObject;
    }

    private File createTempPackageFile(File paramFile)
    {
        try
        {
            File localFile2 = File.createTempFile("vmdl", ".tmp", paramFile);
            localFile1 = localFile2;
        }
        catch (IOException localIOException1)
        {
            try
            {
                FileUtils.setPermissions(localFile1.getCanonicalPath(), 384, -1, -1);
                while (true)
                {
                    return localFile1;
                    localIOException1 = localIOException1;
                    Slog.e("PackageManager", "Couldn't create temp file for downloaded package file.");
                    localFile1 = null;
                }
            }
            catch (IOException localIOException2)
            {
                while (true)
                {
                    Slog.e("PackageManager", "Trouble getting the canoncical path for a temp file.");
                    File localFile1 = null;
                }
            }
        }
    }

    private boolean deleteApplicationCacheFilesLI(String paramString, int paramInt)
    {
        boolean bool = false;
        if (paramString == null)
            Slog.w("PackageManager", "Attempt to delete null packageName.");
        while (true)
        {
            return bool;
            PackageParser.Package localPackage;
            synchronized (this.mPackages)
            {
                localPackage = (PackageParser.Package)this.mPackages.get(paramString);
                if (localPackage == null)
                    Slog.w("PackageManager", "Package named '" + paramString + "' doesn't exist.");
            }
            if (localPackage.applicationInfo == null)
                Slog.w("PackageManager", "Package " + paramString + " has no applicationInfo.");
            else if (this.mInstaller.deleteCacheFiles(paramString) < 0)
                Slog.w("PackageManager", "Couldn't remove cache files for package: " + paramString);
            else
                bool = true;
        }
    }

    private boolean deleteInstalledPackageLI(PackageParser.Package paramPackage, boolean paramBoolean1, int paramInt, PackageRemovedInfo paramPackageRemovedInfo, boolean paramBoolean2)
    {
        int i = 0;
        ApplicationInfo localApplicationInfo = paramPackage.applicationInfo;
        if (localApplicationInfo == null)
        {
            Slog.w("PackageManager", "Package " + paramPackage.packageName + " has no applicationInfo.");
            return i;
        }
        if (paramPackageRemovedInfo != null)
            paramPackageRemovedInfo.uid = localApplicationInfo.uid;
        removePackageDataLI(paramPackage, paramPackageRemovedInfo, paramInt, paramBoolean2);
        if (paramBoolean1)
            if (!isExternal(paramPackage))
                break label137;
        label137: int k;
        for (int j = 8; ; k = 0)
        {
            if (isForwardLocked(paramPackage))
                i = 1;
            paramPackageRemovedInfo.args = createInstallArgs(j | i, localApplicationInfo.sourceDir, localApplicationInfo.publicSourceDir, localApplicationInfo.nativeLibraryDir);
            i = 1;
            break;
        }
    }

    // ERROR //
    private boolean deletePackageLI(String paramString, boolean paramBoolean1, int paramInt, PackageRemovedInfo paramPackageRemovedInfo, boolean paramBoolean2)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore 6
        //     3: aload_1
        //     4: ifnonnull +15 -> 19
        //     7: ldc 186
        //     9: ldc_w 1256
        //     12: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     15: pop
        //     16: iload 6
        //     18: ireturn
        //     19: iconst_0
        //     20: istore 7
        //     22: aload_0
        //     23: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     26: astore 8
        //     28: aload 8
        //     30: monitorenter
        //     31: aload_0
        //     32: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     35: aload_1
        //     36: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     39: checkcast 800	android/content/pm/PackageParser$Package
        //     42: astore 10
        //     44: aload 10
        //     46: ifnonnull +80 -> 126
        //     49: iconst_1
        //     50: istore 7
        //     52: aload_0
        //     53: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     56: getfield 783	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
        //     59: aload_1
        //     60: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     63: checkcast 785	com/android/server/pm/PackageSetting
        //     66: astore 15
        //     68: aload 15
        //     70: ifnonnull +49 -> 119
        //     73: ldc 186
        //     75: new 662	java/lang/StringBuilder
        //     78: dup
        //     79: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     82: ldc_w 1261
        //     85: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     88: aload_1
        //     89: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     92: ldc_w 1263
        //     95: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     98: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     101: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     104: pop
        //     105: aload 8
        //     107: monitorexit
        //     108: goto -92 -> 16
        //     111: astore 9
        //     113: aload 8
        //     115: monitorexit
        //     116: aload 9
        //     118: athrow
        //     119: aload 15
        //     121: getfield 1259	com/android/server/pm/PackageSetting:pkg	Landroid/content/pm/PackageParser$Package;
        //     124: astore 10
        //     126: aload 8
        //     128: monitorexit
        //     129: aload 10
        //     131: ifnonnull +38 -> 169
        //     134: ldc 186
        //     136: new 662	java/lang/StringBuilder
        //     139: dup
        //     140: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     143: ldc_w 1261
        //     146: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     149: aload_1
        //     150: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     153: ldc_w 1263
        //     156: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     159: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     162: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     165: pop
        //     166: goto -150 -> 16
        //     169: iload 7
        //     171: ifeq +20 -> 191
        //     174: aload_0
        //     175: aload 10
        //     177: aload 4
        //     179: iload_3
        //     180: iload 5
        //     182: invokespecial 1470	com/android/server/pm/PackageManagerService:removePackageDataLI	(Landroid/content/pm/PackageParser$Package;Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;IZ)V
        //     185: iconst_1
        //     186: istore 6
        //     188: goto -172 -> 16
        //     191: aload 10
        //     193: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     196: ifnonnull +42 -> 238
        //     199: ldc 186
        //     201: new 662	java/lang/StringBuilder
        //     204: dup
        //     205: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     208: ldc_w 1265
        //     211: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     214: aload 10
        //     216: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     219: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     222: ldc_w 1267
        //     225: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     228: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     231: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     234: pop
        //     235: goto -219 -> 16
        //     238: aload 10
        //     240: invokestatic 1491	com/android/server/pm/PackageManagerService:isSystemApp	(Landroid/content/pm/PackageParser$Package;)Z
        //     243: ifeq +49 -> 292
        //     246: ldc 186
        //     248: new 662	java/lang/StringBuilder
        //     251: dup
        //     252: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     255: ldc_w 1493
        //     258: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     261: aload 10
        //     263: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     266: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     269: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     272: invokestatic 1365	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     275: pop
        //     276: aload_0
        //     277: aload 10
        //     279: iload_3
        //     280: aload 4
        //     282: iload 5
        //     284: invokespecial 1497	com/android/server/pm/PackageManagerService:deleteSystemPackageLI	(Landroid/content/pm/PackageParser$Package;ILcom/android/server/pm/PackageManagerService$PackageRemovedInfo;Z)Z
        //     287: istore 6
        //     289: goto -273 -> 16
        //     292: ldc 186
        //     294: new 662	java/lang/StringBuilder
        //     297: dup
        //     298: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     301: ldc_w 1499
        //     304: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     307: aload 10
        //     309: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     312: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     315: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     318: invokestatic 1365	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     321: pop
        //     322: aload_0
        //     323: aload_1
        //     324: aload 10
        //     326: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     329: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     332: invokespecial 1502	com/android/server/pm/PackageManagerService:killApplication	(Ljava/lang/String;I)V
        //     335: aload_0
        //     336: aload 10
        //     338: iload_2
        //     339: iload_3
        //     340: aload 4
        //     342: iload 5
        //     344: invokespecial 1504	com/android/server/pm/PackageManagerService:deleteInstalledPackageLI	(Landroid/content/pm/PackageParser$Package;ZILcom/android/server/pm/PackageManagerService$PackageRemovedInfo;Z)Z
        //     347: istore 6
        //     349: goto -333 -> 16
        //
        // Exception table:
        //     from	to	target	type
        //     31	116	111	finally
        //     119	129	111	finally
    }

    // ERROR //
    private int deletePackageX(String paramString, boolean paramBoolean1, boolean paramBoolean2, int paramInt)
    {
        // Byte code:
        //     0: new 36	com/android/server/pm/PackageManagerService$PackageRemovedInfo
        //     3: dup
        //     4: invokespecial 1505	com/android/server/pm/PackageManagerService$PackageRemovedInfo:<init>	()V
        //     7: astore 5
        //     9: ldc_w 1507
        //     12: invokestatic 1513	android/os/ServiceManager:getService	(Ljava/lang/String;)Landroid/os/IBinder;
        //     15: invokestatic 1519	android/app/admin/IDevicePolicyManager$Stub:asInterface	(Landroid/os/IBinder;)Landroid/app/admin/IDevicePolicyManager;
        //     18: astore 6
        //     20: aload 6
        //     22: ifnull +55 -> 77
        //     25: aload 6
        //     27: aload_1
        //     28: invokeinterface 1524 2 0
        //     33: ifeq +44 -> 77
        //     36: ldc 186
        //     38: new 662	java/lang/StringBuilder
        //     41: dup
        //     42: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     45: ldc_w 1526
        //     48: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     51: aload_1
        //     52: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     55: ldc_w 1528
        //     58: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     61: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     64: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     67: pop
        //     68: bipush 254
        //     70: istore 11
        //     72: iload 11
        //     74: ireturn
        //     75: astore 18
        //     77: aload_0
        //     78: getfield 371	com/android/server/pm/PackageManagerService:mInstallLock	Ljava/lang/Object;
        //     81: astore 7
        //     83: aload 7
        //     85: monitorenter
        //     86: iload 4
        //     88: ldc 159
        //     90: ior
        //     91: istore 8
        //     93: aload_0
        //     94: aload_1
        //     95: iload_3
        //     96: iload 8
        //     98: aload 5
        //     100: iconst_1
        //     101: invokespecial 1530	com/android/server/pm/PackageManagerService:deletePackageLI	(Ljava/lang/String;ZILcom/android/server/pm/PackageManagerService$PackageRemovedInfo;Z)Z
        //     104: istore 10
        //     106: aload 7
        //     108: monitorexit
        //     109: iload 10
        //     111: ifeq +109 -> 220
        //     114: iload_2
        //     115: ifeq +105 -> 220
        //     118: aload 5
        //     120: getfield 1533	com/android/server/pm/PackageManagerService$PackageRemovedInfo:isRemovedPackageSystemUpdate	Z
        //     123: istore 15
        //     125: aload 5
        //     127: iload_3
        //     128: iload 15
        //     130: invokevirtual 1537	com/android/server/pm/PackageManagerService$PackageRemovedInfo:sendBroadcast	(ZZ)V
        //     133: iload 15
        //     135: ifeq +85 -> 220
        //     138: new 1539	android/os/Bundle
        //     141: dup
        //     142: iconst_1
        //     143: invokespecial 1540	android/os/Bundle:<init>	(I)V
        //     146: astore 16
        //     148: aload 5
        //     150: getfield 1543	com/android/server/pm/PackageManagerService$PackageRemovedInfo:removedUid	I
        //     153: iflt +122 -> 275
        //     156: aload 5
        //     158: getfield 1543	com/android/server/pm/PackageManagerService$PackageRemovedInfo:removedUid	I
        //     161: istore 17
        //     163: aload 16
        //     165: ldc_w 1545
        //     168: iload 17
        //     170: invokevirtual 1548	android/os/Bundle:putInt	(Ljava/lang/String;I)V
        //     173: aload 16
        //     175: ldc_w 1550
        //     178: iconst_1
        //     179: invokevirtual 1553	android/os/Bundle:putBoolean	(Ljava/lang/String;Z)V
        //     182: ldc_w 1555
        //     185: aload_1
        //     186: aload 16
        //     188: aconst_null
        //     189: aconst_null
        //     190: bipush 255
        //     192: invokestatic 1559	com/android/server/pm/PackageManagerService:sendPackageBroadcast	(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/IIntentReceiver;I)V
        //     195: ldc_w 1561
        //     198: aload_1
        //     199: aload 16
        //     201: aconst_null
        //     202: aconst_null
        //     203: bipush 255
        //     205: invokestatic 1559	com/android/server/pm/PackageManagerService:sendPackageBroadcast	(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/IIntentReceiver;I)V
        //     208: ldc_w 1563
        //     211: aconst_null
        //     212: aconst_null
        //     213: aload_1
        //     214: aconst_null
        //     215: bipush 255
        //     217: invokestatic 1559	com/android/server/pm/PackageManagerService:sendPackageBroadcast	(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/IIntentReceiver;I)V
        //     220: invokestatic 957	java/lang/Runtime:getRuntime	()Ljava/lang/Runtime;
        //     223: invokevirtual 960	java/lang/Runtime:gc	()V
        //     226: aload 5
        //     228: getfield 1487	com/android/server/pm/PackageManagerService$PackageRemovedInfo:args	Lcom/android/server/pm/PackageManagerService$InstallArgs;
        //     231: ifnull +25 -> 256
        //     234: aload_0
        //     235: getfield 371	com/android/server/pm/PackageManagerService:mInstallLock	Ljava/lang/Object;
        //     238: astore 12
        //     240: aload 12
        //     242: monitorenter
        //     243: aload 5
        //     245: getfield 1487	com/android/server/pm/PackageManagerService$PackageRemovedInfo:args	Lcom/android/server/pm/PackageManagerService$InstallArgs;
        //     248: iload_3
        //     249: invokevirtual 1567	com/android/server/pm/PackageManagerService$InstallArgs:doPostDeleteLI	(Z)Z
        //     252: pop
        //     253: aload 12
        //     255: monitorexit
        //     256: iload 10
        //     258: ifeq +35 -> 293
        //     261: iconst_1
        //     262: istore 11
        //     264: goto -192 -> 72
        //     267: astore 9
        //     269: aload 7
        //     271: monitorexit
        //     272: aload 9
        //     274: athrow
        //     275: aload 5
        //     277: getfield 1466	com/android/server/pm/PackageManagerService$PackageRemovedInfo:uid	I
        //     280: istore 17
        //     282: goto -119 -> 163
        //     285: astore 13
        //     287: aload 12
        //     289: monitorexit
        //     290: aload 13
        //     292: athrow
        //     293: bipush 255
        //     295: istore 11
        //     297: goto -225 -> 72
        //
        // Exception table:
        //     from	to	target	type
        //     25	68	75	android/os/RemoteException
        //     93	109	267	finally
        //     269	272	267	finally
        //     243	256	285	finally
        //     287	290	285	finally
    }

    private boolean deleteSystemPackageLI(PackageParser.Package paramPackage, int paramInt, PackageRemovedInfo paramPackageRemovedInfo, boolean paramBoolean)
    {
        boolean bool;
        if (paramPackage.applicationInfo == null)
        {
            Slog.w("PackageManager", "Package " + paramPackage.packageName + " has no applicationInfo.");
            bool = false;
        }
        while (true)
        {
            return bool;
            PackageSetting localPackageSetting;
            synchronized (this.mPackages)
            {
                localPackageSetting = this.mSettings.getDisabledSystemPkgLPr(paramPackage.packageName);
                if (localPackageSetting == null)
                {
                    Slog.w("PackageManager", "Attempt to delete unknown system package " + paramPackage.packageName);
                    bool = false;
                }
            }
            Log.i("PackageManager", "Deleting system pkg from data partition");
            paramPackageRemovedInfo.isRemovedPackageSystemUpdate = true;
            if (localPackageSetting.versionCode < paramPackage.mVersionCode);
            for (int i = paramInt & 0xFFFFFFFE; ; i = paramInt | 0x1)
            {
                if (deleteInstalledPackageLI(paramPackage, true, i, paramPackageRemovedInfo, paramBoolean))
                    break label181;
                bool = false;
                break;
            }
            label181: PackageParser.Package localPackage;
            synchronized (this.mPackages)
            {
                this.mSettings.enableSystemPackageLPw(paramPackage.packageName);
                NativeLibraryHelper.removeNativeBinariesLI(paramPackage.applicationInfo.nativeLibraryDir);
                localPackage = scanPackageLI(localPackageSetting.codePath, 5, 33, 0L);
                if (localPackage == null)
                {
                    Slog.w("PackageManager", "Failed to restore system package:" + paramPackage.packageName + " with error:" + this.mLastScanError);
                    bool = false;
                }
            }
            synchronized (this.mPackages)
            {
                updatePermissionsLPw(localPackage.packageName, localPackage, 3);
                if (paramBoolean)
                    this.mSettings.writeLPr();
                bool = true;
            }
        }
    }

    private void deleteTempPackageFiles()
    {
        FilenameFilter local6 = new FilenameFilter()
        {
            public boolean accept(File paramAnonymousFile, String paramAnonymousString)
            {
                if ((paramAnonymousString.startsWith("vmdl")) && (paramAnonymousString.endsWith(".tmp")));
                for (boolean bool = true; ; bool = false)
                    return bool;
            }
        };
        String[] arrayOfString = this.mAppInstallDir.list(local6);
        if (arrayOfString == null);
        while (true)
        {
            return;
            for (int i = 0; i < arrayOfString.length; i++)
                new File(this.mAppInstallDir, arrayOfString[i]).delete();
        }
    }

    private static final void enforceSystemOrRoot(String paramString)
    {
        int i = Binder.getCallingUid();
        if ((i != 1000) && (i != 0))
            throw new SecurityException(paramString);
    }

    private BasePermission findPermissionTreeLP(String paramString)
    {
        Iterator localIterator = this.mSettings.mPermissionTrees.values().iterator();
        BasePermission localBasePermission;
        do
        {
            if (!localIterator.hasNext())
                break;
            localBasePermission = (BasePermission)localIterator.next();
        }
        while ((!paramString.startsWith(localBasePermission.name)) || (paramString.length() <= localBasePermission.name.length()) || (paramString.charAt(localBasePermission.name.length()) != '.'));
        while (true)
        {
            return localBasePermission;
            localBasePermission = null;
        }
    }

    private static String fixProcessName(String paramString1, String paramString2, int paramInt)
    {
        if (paramString2 == null);
        while (true)
        {
            return paramString1;
            paramString1 = paramString2;
        }
    }

    private ApplicationInfo generateApplicationInfoFromSettingsLPw(String paramString, int paramInt1, int paramInt2)
    {
        ApplicationInfo localApplicationInfo = null;
        if (!sUserManager.exists(paramInt2));
        while (true)
        {
            return localApplicationInfo;
            PackageSetting localPackageSetting = (PackageSetting)this.mSettings.mPackages.get(paramString);
            if (localPackageSetting != null)
                if (localPackageSetting.pkg == null)
                {
                    PackageInfo localPackageInfo = generatePackageInfoFromSettingsLPw(paramString, paramInt1, paramInt2);
                    if (localPackageInfo != null)
                        localApplicationInfo = localPackageInfo.applicationInfo;
                }
                else
                {
                    localApplicationInfo = PackageParser.generateApplicationInfo(localPackageSetting.pkg, paramInt1, localPackageSetting.getStopped(paramInt2), localPackageSetting.getEnabled(paramInt2), paramInt2);
                }
        }
    }

    private PackageInfo generatePackageInfoFromSettingsLPw(String paramString, int paramInt1, int paramInt2)
    {
        PackageInfo localPackageInfo = null;
        if (!sUserManager.exists(paramInt2));
        while (true)
        {
            return localPackageInfo;
            PackageSetting localPackageSetting = (PackageSetting)this.mSettings.mPackages.get(paramString);
            if (localPackageSetting != null)
            {
                new PackageParser.Package(paramString);
                if (localPackageSetting.pkg == null)
                {
                    localPackageSetting.pkg = new PackageParser.Package(paramString);
                    localPackageSetting.pkg.applicationInfo.packageName = paramString;
                    localPackageSetting.pkg.applicationInfo.flags = localPackageSetting.pkgFlags;
                    localPackageSetting.pkg.applicationInfo.publicSourceDir = localPackageSetting.resourcePathString;
                    localPackageSetting.pkg.applicationInfo.sourceDir = localPackageSetting.codePathString;
                    localPackageSetting.pkg.applicationInfo.dataDir = getDataPathForPackage(localPackageSetting.pkg.packageName, 0).getPath();
                    localPackageSetting.pkg.applicationInfo.nativeLibraryDir = localPackageSetting.nativeLibraryPathString;
                }
                localPackageInfo = generatePackageInfo(localPackageSetting.pkg, paramInt1, paramInt2);
            }
        }
    }

    static final PermissionInfo generatePermissionInfo(BasePermission paramBasePermission, int paramInt)
    {
        PermissionInfo localPermissionInfo;
        if (paramBasePermission.perm != null)
            localPermissionInfo = PackageParser.generatePermissionInfo(paramBasePermission.perm, paramInt);
        while (true)
        {
            return localPermissionInfo;
            localPermissionInfo = new PermissionInfo();
            localPermissionInfo.name = paramBasePermission.name;
            localPermissionInfo.packageName = paramBasePermission.sourcePackage;
            localPermissionInfo.nonLocalizedLabel = paramBasePermission.name;
            localPermissionInfo.protectionLevel = paramBasePermission.protectionLevel;
        }
    }

    static String getApkName(String paramString)
    {
        String str = null;
        if (paramString == null);
        while (true)
        {
            return str;
            int i = paramString.lastIndexOf("/");
            int j = paramString.lastIndexOf(".");
            if (j == -1)
                j = paramString.length();
            while (j != 0)
            {
                str = paramString.substring(i + 1, j);
                break;
            }
            Slog.w("PackageManager", " Invalid code path, " + paramString + " Not a valid apk name");
        }
    }

    static String getAsecPackageName(String paramString)
    {
        int i = paramString.lastIndexOf("-");
        if (i == -1);
        while (true)
        {
            return paramString;
            paramString = paramString.substring(0, i);
        }
    }

    private static final int getContinuationPoint(String[] paramArrayOfString, String paramString)
    {
        int j;
        if (paramString == null)
            j = 0;
        while (true)
        {
            return j;
            int i = Arrays.binarySearch(paramArrayOfString, paramString);
            if (i < 0)
                j = -i;
            else
                j = i + 1;
        }
    }

    private File getDataPathForPackage(String paramString, int paramInt)
    {
        if (paramInt == 0);
        for (File localFile = new File(this.mAppDataDir, paramString); ; localFile = new File(this.mUserAppDataDir.getAbsolutePath() + File.separator + paramInt + File.separator + paramString))
            return localFile;
    }

    private String getEncryptKey()
    {
        try
        {
            str = SystemKeyStore.getInstance().retrieveKeyHexString("AppsOnSD");
            if (str == null)
            {
                str = SystemKeyStore.getInstance().generateNewKeyHexString(128, "AES", "AppsOnSD");
                if (str == null)
                {
                    Slog.e("PackageManager", "Failed to create encryption keys");
                    str = null;
                }
            }
            return str;
        }
        catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
        {
            while (true)
            {
                Slog.e("PackageManager", "Failed to create encryption keys with exception: " + localNoSuchAlgorithmException);
                str = null;
            }
        }
        catch (IOException localIOException)
        {
            while (true)
            {
                Slog.e("PackageManager", "Failed to retrieve encryption keys with exception: " + localIOException);
                String str = null;
            }
        }
    }

    private static String getNextCodePath(String paramString1, String paramString2, String paramString3)
    {
        int i = 1;
        String str3;
        if (paramString1 != null)
        {
            String str2 = paramString1;
            if (str2.endsWith(paramString3))
                str2 = str2.substring(0, str2.length() - paramString3.length());
            int j = str2.lastIndexOf(paramString2);
            if (j != -1)
            {
                str3 = str2.substring(j + paramString2.length());
                if (str3 != null)
                    if (str3.startsWith("-"))
                        str3 = str3.substring("-".length());
            }
        }
        try
        {
            int k = Integer.parseInt(str3);
            if (k <= 1);
            for (i = k + 1; ; i = k - 1)
            {
                label110: String str1 = "-" + Integer.toString(i);
                return paramString2 + str1;
            }
        }
        catch (NumberFormatException localNumberFormatException)
        {
            break label110;
        }
    }

    // ERROR //
    private boolean getPackageSizeInfoLI(String paramString, PackageStats paramPackageStats)
    {
        // Byte code:
        //     0: aload_1
        //     1: ifnonnull +18 -> 19
        //     4: ldc 186
        //     6: ldc_w 1712
        //     9: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     12: pop
        //     13: iconst_0
        //     14: istore 9
        //     16: iload 9
        //     18: ireturn
        //     19: iconst_0
        //     20: istore_3
        //     21: aconst_null
        //     22: astore 4
        //     24: aload_0
        //     25: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     28: astore 5
        //     30: aload 5
        //     32: monitorenter
        //     33: aload_0
        //     34: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     37: aload_1
        //     38: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     41: checkcast 800	android/content/pm/PackageParser$Package
        //     44: astore 7
        //     46: aload 7
        //     48: ifnonnull +90 -> 138
        //     51: iconst_1
        //     52: istore_3
        //     53: aload_0
        //     54: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     57: getfield 783	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
        //     60: aload_1
        //     61: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     64: checkcast 785	com/android/server/pm/PackageSetting
        //     67: astore 13
        //     69: aload 13
        //     71: ifnull +11 -> 82
        //     74: aload 13
        //     76: getfield 1259	com/android/server/pm/PackageSetting:pkg	Landroid/content/pm/PackageParser$Package;
        //     79: ifnonnull +52 -> 131
        //     82: ldc 186
        //     84: new 662	java/lang/StringBuilder
        //     87: dup
        //     88: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     91: ldc_w 1261
        //     94: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     97: aload_1
        //     98: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     101: ldc_w 1263
        //     104: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     107: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     110: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     113: pop
        //     114: iconst_0
        //     115: istore 9
        //     117: aload 5
        //     119: monitorexit
        //     120: goto -104 -> 16
        //     123: astore 6
        //     125: aload 5
        //     127: monitorexit
        //     128: aload 6
        //     130: athrow
        //     131: aload 13
        //     133: getfield 1259	com/android/server/pm/PackageSetting:pkg	Landroid/content/pm/PackageParser$Package;
        //     136: astore 7
        //     138: aload 7
        //     140: ifnull +44 -> 184
        //     143: aload 7
        //     145: invokestatic 1005	com/android/server/pm/PackageManagerService:isExternal	(Landroid/content/pm/PackageParser$Package;)Z
        //     148: ifne +11 -> 159
        //     151: aload 7
        //     153: invokestatic 1472	com/android/server/pm/PackageManagerService:isForwardLocked	(Landroid/content/pm/PackageParser$Package;)Z
        //     156: ifeq +28 -> 184
        //     159: aload 7
        //     161: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     164: getfield 1475	android/content/pm/ApplicationInfo:sourceDir	Ljava/lang/String;
        //     167: invokestatic 1714	com/android/server/pm/PackageManagerService:cidFromCodePath	(Ljava/lang/String;)Ljava/lang/String;
        //     170: astore 12
        //     172: aload 12
        //     174: ifnull +10 -> 184
        //     177: aload 12
        //     179: invokestatic 1719	com/android/internal/content/PackageHelper:getSdFilesystem	(Ljava/lang/String;)Ljava/lang/String;
        //     182: astore 4
        //     184: aload 5
        //     186: monitorexit
        //     187: aconst_null
        //     188: astore 8
        //     190: iload_3
        //     191: ifne +68 -> 259
        //     194: aload 7
        //     196: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     199: astore 10
        //     201: aload 10
        //     203: ifnonnull +41 -> 244
        //     206: ldc 186
        //     208: new 662	java/lang/StringBuilder
        //     211: dup
        //     212: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     215: ldc_w 1265
        //     218: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     221: aload_1
        //     222: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     225: ldc_w 1267
        //     228: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     231: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     234: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     237: pop
        //     238: iconst_0
        //     239: istore 9
        //     241: goto -225 -> 16
        //     244: aload 7
        //     246: invokestatic 1472	com/android/server/pm/PackageManagerService:isForwardLocked	(Landroid/content/pm/PackageParser$Package;)Z
        //     249: ifeq +10 -> 259
        //     252: aload 10
        //     254: getfield 1478	android/content/pm/ApplicationInfo:publicSourceDir	Ljava/lang/String;
        //     257: astore 8
        //     259: aload_0
        //     260: getfield 534	com/android/server/pm/PackageManagerService:mInstaller	Lcom/android/server/pm/Installer;
        //     263: aload_1
        //     264: aload 7
        //     266: getfield 1722	android/content/pm/PackageParser$Package:mPath	Ljava/lang/String;
        //     269: aload 8
        //     271: aload 4
        //     273: aload_2
        //     274: invokevirtual 1726	com/android/server/pm/Installer:getSizeInfo	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/PackageStats;)I
        //     277: ifge +9 -> 286
        //     280: iconst_0
        //     281: istore 9
        //     283: goto -267 -> 16
        //     286: aload 7
        //     288: invokestatic 1005	com/android/server/pm/PackageManagerService:isExternal	(Landroid/content/pm/PackageParser$Package;)Z
        //     291: ifne +21 -> 312
        //     294: aload_2
        //     295: aload_2
        //     296: getfield 1731	android/content/pm/PackageStats:codeSize	J
        //     299: aload_2
        //     300: getfield 1734	android/content/pm/PackageStats:externalCodeSize	J
        //     303: ladd
        //     304: putfield 1731	android/content/pm/PackageStats:codeSize	J
        //     307: aload_2
        //     308: lconst_0
        //     309: putfield 1734	android/content/pm/PackageStats:externalCodeSize	J
        //     312: iconst_1
        //     313: istore 9
        //     315: goto -299 -> 16
        //
        // Exception table:
        //     from	to	target	type
        //     33	128	123	finally
        //     131	187	123	finally
    }

    private String getRequiredVerifierLPr()
    {
        List localList = queryIntentReceivers(new Intent("android.intent.action.PACKAGE_NEEDS_VERIFICATION"), "application/vnd.android.package-archive", 512, 0);
        Object localObject = null;
        int i = localList.size();
        int j = 0;
        if (j < i)
        {
            ResolveInfo localResolveInfo = (ResolveInfo)localList.get(j);
            if (localResolveInfo.activityInfo == null);
            while (true)
            {
                j++;
                break;
                String str = localResolveInfo.activityInfo.packageName;
                PackageSetting localPackageSetting = (PackageSetting)this.mSettings.mPackages.get(str);
                if ((localPackageSetting != null) && (localPackageSetting.grantedPermissions.contains("android.permission.PACKAGE_VERIFICATION_AGENT")))
                {
                    if (localObject != null)
                        throw new RuntimeException("There can be only one required verifier");
                    localObject = str;
                }
            }
        }
        return localObject;
    }

    private static File getSettingsProblemFile()
    {
        return new File(new File(Environment.getDataDirectory(), "system"), "uiderrors.txt");
    }

    static String getTempContainerId()
    {
        int i = 1;
        String[] arrayOfString = PackageHelper.getSecureContainerList();
        int k;
        String str1;
        if (arrayOfString != null)
        {
            int j = arrayOfString.length;
            k = 0;
            if (k < j)
            {
                str1 = arrayOfString[k];
                if ((str1 != null) && (str1.startsWith("smdl2tmp")));
            }
        }
        while (true)
        {
            k++;
            break;
            String str2 = str1.substring("smdl2tmp".length());
            try
            {
                int m = Integer.parseInt(str2);
                if (m >= i)
                {
                    i = m + 1;
                    continue;
                    return "smdl2tmp" + i;
                }
            }
            catch (NumberFormatException localNumberFormatException)
            {
            }
        }
    }

    // ERROR //
    private int getUidForVerifier(VerifierInfo paramVerifierInfo)
    {
        // Byte code:
        //     0: bipush 255
        //     2: istore_2
        //     3: aload_0
        //     4: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     7: astore_3
        //     8: aload_3
        //     9: monitorenter
        //     10: aload_0
        //     11: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     14: aload_1
        //     15: getfield 1773	android/content/pm/VerifierInfo:packageName	Ljava/lang/String;
        //     18: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     21: checkcast 800	android/content/pm/PackageParser$Package
        //     24: astore 5
        //     26: aload 5
        //     28: ifnonnull +8 -> 36
        //     31: aload_3
        //     32: monitorexit
        //     33: goto +152 -> 185
        //     36: aload 5
        //     38: getfield 1345	android/content/pm/PackageParser$Package:mSignatures	[Landroid/content/pm/Signature;
        //     41: arraylength
        //     42: iconst_1
        //     43: if_icmpeq +50 -> 93
        //     46: ldc 186
        //     48: new 662	java/lang/StringBuilder
        //     51: dup
        //     52: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     55: ldc_w 1775
        //     58: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     61: aload_1
        //     62: getfield 1773	android/content/pm/VerifierInfo:packageName	Ljava/lang/String;
        //     65: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     68: ldc_w 1777
        //     71: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     74: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     77: invokestatic 747	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     80: pop
        //     81: aload_3
        //     82: monitorexit
        //     83: goto +102 -> 185
        //     86: astore 4
        //     88: aload_3
        //     89: monitorexit
        //     90: aload 4
        //     92: athrow
        //     93: aload 5
        //     95: getfield 1345	android/content/pm/PackageParser$Package:mSignatures	[Landroid/content/pm/Signature;
        //     98: iconst_0
        //     99: aaload
        //     100: invokevirtual 1783	android/content/pm/Signature:getPublicKey	()Ljava/security/PublicKey;
        //     103: invokeinterface 1789 1 0
        //     108: astore 7
        //     110: aload_1
        //     111: getfield 1793	android/content/pm/VerifierInfo:publicKey	Ljava/security/PublicKey;
        //     114: invokeinterface 1789 1 0
        //     119: aload 7
        //     121: invokestatic 1796	java/util/Arrays:equals	([B[B)Z
        //     124: ifne +50 -> 174
        //     127: ldc 186
        //     129: new 662	java/lang/StringBuilder
        //     132: dup
        //     133: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     136: ldc_w 1775
        //     139: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     142: aload_1
        //     143: getfield 1773	android/content/pm/VerifierInfo:packageName	Ljava/lang/String;
        //     146: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     149: ldc_w 1798
        //     152: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     155: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     158: invokestatic 747	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     161: pop
        //     162: aload_3
        //     163: monitorexit
        //     164: goto +21 -> 185
        //     167: astore 6
        //     169: aload_3
        //     170: monitorexit
        //     171: goto +14 -> 185
        //     174: aload 5
        //     176: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     179: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     182: istore_2
        //     183: aload_3
        //     184: monitorexit
        //     185: iload_2
        //     186: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     10	90	86	finally
        //     93	110	86	finally
        //     110	185	86	finally
        //     93	110	167	java/security/cert/CertificateException
    }

    private int getUidTargetSdkVersionLockedLPr(int paramInt)
    {
        Object localObject = this.mSettings.getUserIdLPr(paramInt);
        if ((localObject instanceof SharedUserSetting))
        {
            SharedUserSetting localSharedUserSetting = (SharedUserSetting)localObject;
            i = 10000;
            Iterator localIterator = localSharedUserSetting.packages.iterator();
            while (localIterator.hasNext())
            {
                PackageSetting localPackageSetting2 = (PackageSetting)localIterator.next();
                if (localPackageSetting2.pkg != null)
                {
                    int j = localPackageSetting2.pkg.applicationInfo.targetSdkVersion;
                    if (j < i)
                        i = j;
                }
            }
        }
        PackageSetting localPackageSetting1;
        if ((localObject instanceof PackageSetting))
        {
            localPackageSetting1 = (PackageSetting)localObject;
            if (localPackageSetting1.pkg == null);
        }
        for (int i = localPackageSetting1.pkg.applicationInfo.targetSdkVersion; ; i = 10000)
            return i;
    }

    private long getVerificationTimeout()
    {
        return Settings.Secure.getLong(this.mContext.getContentResolver(), "verifier_timeout", 60000L);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private void grantPermissionsLPw(PackageParser.Package paramPackage, boolean paramBoolean)
    {
        PackageSetting localPackageSetting1 = (PackageSetting)paramPackage.mExtras;
        if (localPackageSetting1 == null);
        while (true)
        {
            return;
            Object localObject1;
            HashSet localHashSet;
            int i;
            int k;
            label111: String str1;
            BasePermission localBasePermission;
            String str2;
            int m;
            int n;
            boolean bool;
            label196: int i2;
            if (localPackageSetting1.sharedUser != null)
            {
                localObject1 = localPackageSetting1.sharedUser;
                localHashSet = ((GrantedPermissions)localObject1).grantedPermissions;
                i = 0;
                if (paramBoolean)
                {
                    localPackageSetting1.permissionsFixed = false;
                    if (localObject1 == localPackageSetting1)
                    {
                        localHashSet = new HashSet(((GrantedPermissions)localObject1).grantedPermissions);
                        ((GrantedPermissions)localObject1).grantedPermissions.clear();
                        ((GrantedPermissions)localObject1).gids = this.mGlobalGids;
                    }
                }
                if (((GrantedPermissions)localObject1).gids == null)
                    ((GrantedPermissions)localObject1).gids = this.mGlobalGids;
                int j = paramPackage.requestedPermissions.size();
                k = 0;
                if (k >= j)
                    break label943;
                str1 = (String)paramPackage.requestedPermissions.get(k);
                localBasePermission = (BasePermission)this.mSettings.mPermissions.get(str1);
                if ((localBasePermission == null) || (localBasePermission.packageSetting == null))
                    break label900;
                str2 = localBasePermission.name;
                m = 0;
                n = 0xF & localBasePermission.protectionLevel;
                if ((n != 0) && (n != 1))
                    break label395;
                bool = true;
                if (!bool)
                    break label702;
                if (((0x1 & localPackageSetting1.pkgFlags) == 0) && (localPackageSetting1.permissionsFixed) && (m == 0) && (!((GrantedPermissions)localObject1).grantedPermissions.contains(str2)))
                {
                    bool = false;
                    int i1 = PackageParser.NEW_PERMISSIONS.length;
                    i2 = 0;
                    label247: if (i2 < i1)
                    {
                        PackageParser.NewPermissionInfo localNewPermissionInfo = PackageParser.NEW_PERMISSIONS[i2];
                        if ((!localNewPermissionInfo.name.equals(str2)) || (paramPackage.applicationInfo.targetSdkVersion >= localNewPermissionInfo.sdkVersion))
                            break label619;
                        bool = true;
                        Log.i("PackageManager", "Auto-granting " + str2 + " to old pkg " + paramPackage.packageName);
                    }
                }
                if (!bool)
                    break label653;
                if (((GrantedPermissions)localObject1).grantedPermissions.contains(str2))
                    break label625;
                i = 1;
                ((GrantedPermissions)localObject1).grantedPermissions.add(str2);
                ((GrantedPermissions)localObject1).gids = appendInts(((GrantedPermissions)localObject1).gids, localBasePermission.gids);
            }
            label395: label526: label542: label702: 
            while (true)
            {
                k++;
                break label111;
                localObject1 = localPackageSetting1;
                break;
                if (localBasePermission.packageSetting == null)
                {
                    bool = false;
                    break label196;
                }
                if (n == 2)
                {
                    int i3;
                    label456: PackageSetting localPackageSetting2;
                    Object localObject2;
                    if ((compareSignatures(localBasePermission.packageSetting.signatures.mSignatures, paramPackage.mSignatures) == 0) || (compareSignatures(this.mPlatformPackage.mSignatures, paramPackage.mSignatures) == 0))
                    {
                        i3 = 1;
                        bool = i3 | ExtraPackageManager.isTrustedSystemSignature(paramPackage.mSignatures);
                        if ((!bool) && ((0x10 & localBasePermission.protectionLevel) != 0) && (isSystemApp(paramPackage)))
                        {
                            if (!isUpdatedSystemApp(paramPackage))
                                break label601;
                            localPackageSetting2 = this.mSettings.getDisabledSystemPkgLPr(paramPackage.packageName);
                            if (localPackageSetting2.sharedUser == null)
                                break label588;
                            localObject2 = localPackageSetting2.sharedUser;
                            if (!((GrantedPermissions)localObject2).grantedPermissions.contains(str2))
                                break label595;
                            bool = true;
                        }
                        if ((!bool) && ((0x20 & localBasePermission.protectionLevel) != 0))
                            if (!localHashSet.contains(str2))
                                break label607;
                    }
                    label588: label595: label601: label607: for (bool = true; ; bool = false)
                    {
                        if (!bool)
                            break label611;
                        m = 1;
                        break;
                        i3 = 0;
                        break label456;
                        localObject2 = localPackageSetting2;
                        break label526;
                        bool = false;
                        break label542;
                        bool = true;
                        break label542;
                    }
                    label611: break label196;
                }
                bool = false;
                break label196;
                label619: i2++;
                break label247;
                label625: if (!localPackageSetting1.haveGids)
                {
                    ((GrantedPermissions)localObject1).gids = appendInts(((GrantedPermissions)localObject1).gids, localBasePermission.gids);
                    continue;
                    Slog.w("PackageManager", "Not granting permission " + str2 + " to package " + paramPackage.packageName + " because it was previously installed without");
                    continue;
                    if (((GrantedPermissions)localObject1).grantedPermissions.remove(str2))
                    {
                        i = 1;
                        ((GrantedPermissions)localObject1).gids = removeInts(((GrantedPermissions)localObject1).gids, localBasePermission.gids);
                        Slog.i("PackageManager", "Un-granting permission " + str2 + " from package " + paramPackage.packageName + " (protectionLevel=" + localBasePermission.protectionLevel + " flags=0x" + Integer.toHexString(paramPackage.applicationInfo.flags) + ")");
                    }
                    else
                    {
                        Slog.w("PackageManager", "Not granting permission " + str2 + " to package " + paramPackage.packageName + " (protectionLevel=" + localBasePermission.protectionLevel + " flags=0x" + Integer.toHexString(paramPackage.applicationInfo.flags) + ")");
                        continue;
                        Slog.w("PackageManager", "Unknown permission " + str1 + " in package " + paramPackage.packageName);
                    }
                }
            }
            label653: label943: if (((i == 0) && (!paramBoolean)) || (((!localPackageSetting1.permissionsFixed) && ((0x1 & localPackageSetting1.pkgFlags) == 0)) || ((0x80 & localPackageSetting1.pkgFlags) != 0)))
                localPackageSetting1.permissionsFixed = true;
            label900: localPackageSetting1.haveGids = true;
        }
    }

    private static boolean hasPermission(PackageParser.Package paramPackage, String paramString)
    {
        int i = -1 + paramPackage.permissions.size();
        if (i >= 0)
            if (!((PackageParser.Permission)paramPackage.permissions.get(i)).info.name.equals(paramString));
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            i--;
            break;
        }
    }

    private static boolean ignoreCodePath(String paramString)
    {
        String str1 = getApkName(paramString);
        int i = str1.lastIndexOf("-");
        String str2;
        if ((i != -1) && (i + 1 < str1.length()))
            str2 = str1.substring(i + 1);
        while (true)
        {
            try
            {
                Integer.parseInt(str2);
                bool = true;
                return bool;
            }
            catch (NumberFormatException localNumberFormatException)
            {
            }
            boolean bool = false;
        }
    }

    private static boolean installForwardLocked(int paramInt)
    {
        if ((paramInt & 0x1) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    // ERROR //
    private void installNewPackageLI(PackageParser.Package paramPackage, int paramInt1, int paramInt2, String paramString, PackageInstalledInfo paramPackageInstalledInfo)
    {
        // Byte code:
        //     0: aload_1
        //     1: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     4: astore 6
        //     6: aload_0
        //     7: aload_1
        //     8: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     11: iconst_0
        //     12: invokespecial 1636	com/android/server/pm/PackageManagerService:getDataPathForPackage	(Ljava/lang/String;I)Ljava/io/File;
        //     15: invokevirtual 838	java/io/File:exists	()Z
        //     18: istore 7
        //     20: aload 5
        //     22: aload 6
        //     24: putfield 1930	com/android/server/pm/PackageManagerService$PackageInstalledInfo:name	Ljava/lang/String;
        //     27: aload_0
        //     28: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     31: astore 8
        //     33: aload 8
        //     35: monitorenter
        //     36: aload_0
        //     37: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     40: getfield 1933	com/android/server/pm/Settings:mRenamedPackages	Ljava/util/HashMap;
        //     43: aload 6
        //     45: invokevirtual 1936	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
        //     48: ifeq +67 -> 115
        //     51: ldc 186
        //     53: new 662	java/lang/StringBuilder
        //     56: dup
        //     57: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     60: ldc_w 1938
        //     63: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     66: aload 6
        //     68: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     71: ldc_w 1940
        //     74: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     77: aload_0
        //     78: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     81: getfield 1933	com/android/server/pm/Settings:mRenamedPackages	Ljava/util/HashMap;
        //     84: aload 6
        //     86: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     89: checkcast 360	java/lang/String
        //     92: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     95: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     98: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     101: pop
        //     102: aload 5
        //     104: bipush 255
        //     106: putfield 1943	com/android/server/pm/PackageManagerService$PackageInstalledInfo:returnCode	I
        //     109: aload 8
        //     111: monitorexit
        //     112: goto +218 -> 330
        //     115: aload_0
        //     116: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     119: aload 6
        //     121: invokevirtual 1936	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
        //     124: ifne +17 -> 141
        //     127: aload_0
        //     128: getfield 376	com/android/server/pm/PackageManagerService:mAppDirs	Ljava/util/HashMap;
        //     131: aload_1
        //     132: getfield 1722	android/content/pm/PackageParser$Package:mPath	Ljava/lang/String;
        //     135: invokevirtual 1936	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
        //     138: ifeq +57 -> 195
        //     141: ldc 186
        //     143: new 662	java/lang/StringBuilder
        //     146: dup
        //     147: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     150: ldc_w 1938
        //     153: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     156: aload 6
        //     158: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     161: ldc_w 1945
        //     164: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     167: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     170: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     173: pop
        //     174: aload 5
        //     176: bipush 255
        //     178: putfield 1943	com/android/server/pm/PackageManagerService$PackageInstalledInfo:returnCode	I
        //     181: aload 8
        //     183: monitorexit
        //     184: goto +146 -> 330
        //     187: astore 9
        //     189: aload 8
        //     191: monitorexit
        //     192: aload 9
        //     194: athrow
        //     195: aload 8
        //     197: monitorexit
        //     198: aload_0
        //     199: iconst_1
        //     200: putfield 1360	com/android/server/pm/PackageManagerService:mLastScanError	I
        //     203: aload_0
        //     204: aload_1
        //     205: iload_2
        //     206: iload_3
        //     207: invokestatic 1948	java/lang/System:currentTimeMillis	()J
        //     210: invokespecial 1951	com/android/server/pm/PackageManagerService:scanPackageLI	(Landroid/content/pm/PackageParser$Package;IIJ)Landroid/content/pm/PackageParser$Package;
        //     213: astore 11
        //     215: aload 11
        //     217: ifnonnull +61 -> 278
        //     220: ldc 186
        //     222: new 662	java/lang/StringBuilder
        //     225: dup
        //     226: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     229: ldc_w 1953
        //     232: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     235: aload_1
        //     236: getfield 1722	android/content/pm/PackageParser$Package:mPath	Ljava/lang/String;
        //     239: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     242: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     245: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     248: pop
        //     249: aload_0
        //     250: getfield 1360	com/android/server/pm/PackageManagerService:mLastScanError	I
        //     253: istore 15
        //     255: aload 5
        //     257: iload 15
        //     259: putfield 1943	com/android/server/pm/PackageManagerService$PackageInstalledInfo:returnCode	I
        //     262: iload 15
        //     264: iconst_1
        //     265: if_icmpne +65 -> 330
        //     268: aload 5
        //     270: bipush 254
        //     272: putfield 1943	com/android/server/pm/PackageManagerService$PackageInstalledInfo:returnCode	I
        //     275: goto +55 -> 330
        //     278: aload_0
        //     279: aload 11
        //     281: aload 4
        //     283: aload 5
        //     285: invokespecial 1957	com/android/server/pm/PackageManagerService:updateSettingsLI	(Landroid/content/pm/PackageParser$Package;Ljava/lang/String;Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;)V
        //     288: aload 5
        //     290: getfield 1943	com/android/server/pm/PackageManagerService$PackageInstalledInfo:returnCode	I
        //     293: iconst_1
        //     294: if_icmpeq +36 -> 330
        //     297: iload 7
        //     299: ifeq +25 -> 324
        //     302: iconst_1
        //     303: istore 12
        //     305: aload_0
        //     306: aload 6
        //     308: iconst_0
        //     309: iload 12
        //     311: aload 5
        //     313: getfield 1961	com/android/server/pm/PackageManagerService$PackageInstalledInfo:removedInfo	Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;
        //     316: iconst_1
        //     317: invokespecial 1530	com/android/server/pm/PackageManagerService:deletePackageLI	(Ljava/lang/String;ZILcom/android/server/pm/PackageManagerService$PackageRemovedInfo;Z)Z
        //     320: pop
        //     321: goto +9 -> 330
        //     324: iconst_0
        //     325: istore 12
        //     327: goto -22 -> 305
        //     330: return
        //
        // Exception table:
        //     from	to	target	type
        //     36	192	187	finally
        //     195	198	187	finally
    }

    private static boolean installOnSd(int paramInt)
    {
        boolean bool = false;
        if ((paramInt & 0x10) != 0);
        while (true)
        {
            return bool;
            if ((paramInt & 0x8) != 0)
                bool = true;
        }
    }

    private void installPackageLI(InstallArgs paramInstallArgs, boolean paramBoolean, PackageInstalledInfo paramPackageInstalledInfo)
    {
        int i = paramInstallArgs.flags;
        String str1 = paramInstallArgs.installerPackageName;
        File localFile = new File(paramInstallArgs.getCodePath());
        int j;
        int k;
        label46: int m;
        int n;
        label57: int i2;
        label74: int i3;
        int i5;
        label103: int i7;
        label119: int i8;
        PackageParser localPackageParser;
        PackageParser.Package localPackage;
        if ((i & 0x1) != 0)
        {
            j = 1;
            if ((i & 0x8) == 0)
                break label186;
            k = 1;
            m = 0;
            if (k == 0)
                break label192;
            n = 0;
            int i1 = 0x8 | (n | 0x4);
            if (!paramBoolean)
                break label198;
            i2 = 16;
            i3 = i1 | i2;
            paramPackageInstalledInfo.returnCode = 1;
            int i4 = 0x2 | this.mDefParseFlags;
            if (j == 0)
                break label204;
            i5 = 16;
            int i6 = i4 | i5;
            if (k == 0)
                break label210;
            i7 = 32;
            i8 = i6 | i7;
            localPackageParser = new PackageParser(localFile.getPath());
            localPackageParser.setSeparateProcesses(this.mSeparateProcesses);
            localPackage = localPackageParser.parsePackage(localFile, null, this.mMetrics, i8);
            if (localPackage != null)
                break label216;
            paramPackageInstalledInfo.returnCode = localPackageParser.getParseError();
        }
        label186: label192: label198: label204: label210: label216: String str2;
        while (true)
        {
            return;
            j = 0;
            break;
            k = 0;
            break label46;
            n = 1;
            break label57;
            i2 = 0;
            break label74;
            i5 = 0;
            break label103;
            i7 = 0;
            break label119;
            str2 = localPackage.packageName;
            paramPackageInstalledInfo.name = str2;
            if (((0x100 & localPackage.applicationInfo.flags) != 0) && ((i & 0x4) == 0))
            {
                paramPackageInstalledInfo.returnCode = -15;
            }
            else if (!localPackageParser.collectCertificates(localPackage, i8))
            {
                paramPackageInstalledInfo.returnCode = localPackageParser.getParseError();
            }
            else
            {
                if ((paramInstallArgs.manifestDigest == null) || (paramInstallArgs.manifestDigest.equals(localPackage.manifestDigest)))
                    break label315;
                paramPackageInstalledInfo.returnCode = -23;
            }
        }
        label315: String str3 = null;
        int i9 = 0;
        HashMap localHashMap = this.mPackages;
        if ((i & 0x2) != 0);
        while (true)
        {
            try
            {
                String str4 = (String)this.mSettings.mRenamedPackages.get(str2);
                if ((localPackage.mOriginalPackages != null) && (localPackage.mOriginalPackages.contains(str4)) && (this.mPackages.containsKey(str4)))
                {
                    localPackage.setPackageName(str4);
                    str2 = localPackage.packageName;
                    m = 1;
                    PackageSetting localPackageSetting = (PackageSetting)this.mSettings.mPackages.get(str2);
                    if (localPackageSetting != null)
                    {
                        str3 = ((PackageSetting)this.mSettings.mPackages.get(str2)).codePathString;
                        if ((localPackageSetting.pkg != null) && (localPackageSetting.pkg.applicationInfo != null))
                        {
                            if ((0x1 & localPackageSetting.pkg.applicationInfo.flags) == 0)
                                break label627;
                            i9 = 1;
                        }
                    }
                    if ((i9 == 0) || (k == 0))
                        break label541;
                    Slog.w("PackageManager", "Cannot install updates to system apps on sdcard");
                    paramPackageInstalledInfo.returnCode = -19;
                    break;
                }
                if (!this.mPackages.containsKey(str2))
                    continue;
                m = 1;
                continue;
            }
            finally
            {
            }
            label541: if (!paramInstallArgs.doRename(paramPackageInstalledInfo.returnCode, str2, str3))
            {
                paramPackageInstalledInfo.returnCode = -4;
                break;
            }
            setApplicationInfoPaths(localPackage, paramInstallArgs.getCodePath(), paramInstallArgs.getResourcePath());
            localPackage.applicationInfo.nativeLibraryDir = paramInstallArgs.getNativeLibraryPath();
            if (m != 0)
            {
                replacePackageLI(localPackage, i8, i3, str1, paramPackageInstalledInfo);
                break;
            }
            installNewPackageLI(localPackage, i8, i3, str1, paramPackageInstalledInfo);
            break;
            label627: i9 = 0;
        }
    }

    private boolean isAsecExternal(String paramString)
    {
        if (!PackageHelper.getSdFilesystem(paramString).startsWith(this.mAsecInternalPath));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isExternal(PackageParser.Package paramPackage)
    {
        if ((0x40000 & paramPackage.applicationInfo.flags) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isExternal(PackageSetting paramPackageSetting)
    {
        if ((0x40000 & paramPackageSetting.pkgFlags) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isExternalMediaAvailable()
    {
        if ((this.mMediaMounted) || (Environment.isExternalStorageEmulated()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isForwardLocked(PackageParser.Package paramPackage)
    {
        if ((0x20000000 & paramPackage.applicationInfo.flags) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isForwardLocked(PackageSetting paramPackageSetting)
    {
        if ((0x20000000 & paramPackageSetting.pkgFlags) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static final boolean isPackageFilename(String paramString)
    {
        if ((paramString != null) && (paramString.endsWith(".apk")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isPermissionEnforcedLocked(String paramString)
    {
        boolean bool = true;
        if ("android.permission.READ_EXTERNAL_STORAGE".equals(paramString))
        {
            if (this.mSettings.mReadExternalStorageEnforced == null)
                break label35;
            bool = this.mSettings.mReadExternalStorageEnforced.booleanValue();
        }
        while (true)
        {
            return bool;
            label35: if (Settings.Secure.getInt(this.mContext.getContentResolver(), "read_external_storage_enforced_default", 0) == 0)
                bool = false;
        }
    }

    private static boolean isSystemApp(ApplicationInfo paramApplicationInfo)
    {
        if ((0x1 & paramApplicationInfo.flags) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isSystemApp(PackageParser.Package paramPackage)
    {
        if ((0x1 & paramPackage.applicationInfo.flags) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isSystemApp(PackageSetting paramPackageSetting)
    {
        if ((0x1 & paramPackageSetting.pkgFlags) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isUpdatedSystemApp(PackageParser.Package paramPackage)
    {
        if ((0x80 & paramPackage.applicationInfo.flags) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isVerificationEnabled()
    {
        int i = 1;
        if (Settings.Secure.getInt(this.mContext.getContentResolver(), "verifier_enable", 0) == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private void killApplication(String paramString, int paramInt)
    {
        IActivityManager localIActivityManager = ActivityManagerNative.getDefault();
        if (localIActivityManager != null);
        try
        {
            localIActivityManager.killApplicationWithUid(paramString, paramInt);
            label16: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label16;
        }
    }

    private void loadMediaPackages(HashMap<AsecInstallArgs, String> paramHashMap, int[] paramArrayOfInt, HashSet<String> paramHashSet)
    {
        ArrayList localArrayList = new ArrayList();
        Set localSet = paramHashMap.keySet();
        int i = 0;
        Iterator localIterator1 = localSet.iterator();
        if (localIterator1.hasNext())
        {
            AsecInstallArgs localAsecInstallArgs = (AsecInstallArgs)localIterator1.next();
            String str2 = (String)paramHashMap.get(localAsecInstallArgs);
            int m = -18;
            while (true)
            {
                try
                {
                    while (true)
                    {
                        if (localAsecInstallArgs.doPreInstall(1) != 1)
                        {
                            Slog.e("PackageManager", "Failed to mount cid : " + localAsecInstallArgs.cid + " when installing from sdcard");
                            if (m == 1)
                                break;
                        }
                        for (String str3 = localAsecInstallArgs.cid; ; str3 = localAsecInstallArgs.cid)
                        {
                            label123: paramHashSet.add(str3);
                            break;
                            if ((str2 != null) && (str2.equals(localAsecInstallArgs.getCodePath())))
                                break label222;
                            Slog.e("PackageManager", "Container " + localAsecInstallArgs.cid + " cachepath " + localAsecInstallArgs.getCodePath() + " does not match one in settings " + str2);
                            if (m == 1)
                                break;
                        }
                        label222: int n = this.mDefParseFlags;
                        if (localAsecInstallArgs.isExternal())
                            n |= 32;
                        if (localAsecInstallArgs.isFwdLocked())
                            n |= 16;
                        i = 1;
                        synchronized (this.mInstallLock)
                        {
                            while (true)
                            {
                                PackageParser.Package localPackage = scanPackageLI(new File(str2), n, 0, 0L);
                                if (localPackage != null)
                                {
                                    HashMap localHashMap2 = this.mPackages;
                                    m = 1;
                                    try
                                    {
                                        localArrayList.add(localPackage.packageName);
                                        localAsecInstallArgs.doPostInstall(1, localPackage.applicationInfo.uid);
                                        if (m == 1)
                                            break;
                                        str3 = localAsecInstallArgs.cid;
                                        break label123;
                                    }
                                    finally
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
                finally
                {
                    if (m != 1)
                        paramHashSet.add(localAsecInstallArgs.cid);
                }
                Slog.i("PackageManager", "Failed to install pkg from    " + str2 + " from sdcard");
            }
        }
        while (true)
        {
            String str1;
            synchronized (this.mPackages)
            {
                int j;
                if (this.mSettings.mExternalSdkPlatform != this.mSdkVersion)
                {
                    j = 1;
                    if (j != 0)
                        Slog.i("PackageManager", "Platform changed from " + this.mSettings.mExternalSdkPlatform + " to " + this.mSdkVersion + "; regranting permissions for external storage");
                    this.mSettings.mExternalSdkPlatform = this.mSdkVersion;
                    if (j != 0)
                    {
                        k = 6;
                        updatePermissionsLPw(null, null, k | 0x1);
                        this.mSettings.writeLPr();
                        if (localArrayList.size() > 0)
                            sendResourcesChangedBroadcast(true, localArrayList, paramArrayOfInt, null);
                        if (i != 0)
                            Runtime.getRuntime().gc();
                        if (paramHashSet == null)
                            break;
                        Iterator localIterator2 = paramHashSet.iterator();
                        if (!localIterator2.hasNext())
                            break;
                        str1 = (String)localIterator2.next();
                        if (!str1.startsWith("smdl2tmp"))
                            break label675;
                        Log.i("PackageManager", "Destroying stale temporary container " + str1);
                        PackageHelper.destroySdDir(str1);
                        continue;
                    }
                }
                else
                {
                    j = 0;
                    continue;
                }
                int k = 0;
            }
            label675: Log.w("PackageManager", "Container " + str1 + " is stale");
        }
    }

    public static final IPackageManager main(Context paramContext, boolean paramBoolean1, boolean paramBoolean2)
    {
        PackageManagerService localPackageManagerService = new PackageManagerService(paramContext, paramBoolean1, paramBoolean2);
        ServiceManager.addService("package", localPackageManagerService);
        return localPackageManagerService;
    }

    private ComponentName matchComponentForVerifier(String paramString, List<ResolveInfo> paramList)
    {
        ActivityInfo localActivityInfo = null;
        int i = paramList.size();
        int j = 0;
        if (j < i)
        {
            ResolveInfo localResolveInfo = (ResolveInfo)paramList.get(j);
            if (localResolveInfo.activityInfo == null);
            while (!paramString.equals(localResolveInfo.activityInfo.packageName))
            {
                j++;
                break;
            }
            localActivityInfo = localResolveInfo.activityInfo;
        }
        if (localActivityInfo == null);
        for (ComponentName localComponentName = null; ; localComponentName = new ComponentName(localActivityInfo.packageName, localActivityInfo.name))
            return localComponentName;
    }

    private List<ComponentName> matchVerifiers(PackageInfoLite paramPackageInfoLite, List<ResolveInfo> paramList, PackageVerificationState paramPackageVerificationState)
    {
        if (paramPackageInfoLite.verifiers.length == 0)
        {
            localObject = null;
            return localObject;
        }
        int i = paramPackageInfoLite.verifiers.length;
        Object localObject = new ArrayList(i + 1);
        int j = 0;
        label37: VerifierInfo localVerifierInfo;
        ComponentName localComponentName;
        if (j < i)
        {
            localVerifierInfo = paramPackageInfoLite.verifiers[j];
            localComponentName = matchComponentForVerifier(localVerifierInfo.packageName, paramList);
            if (localComponentName != null)
                break label76;
        }
        while (true)
        {
            j++;
            break label37;
            break;
            label76: int k = getUidForVerifier(localVerifierInfo);
            if (k != -1)
            {
                ((List)localObject).add(localComponentName);
                paramPackageVerificationState.addSufficientVerifier(k);
            }
        }
    }

    private int moveDexFilesLI(PackageParser.Package paramPackage)
    {
        if (((0x4 & paramPackage.applicationInfo.flags) != 0) && (this.mInstaller.movedex(paramPackage.mScanPath, paramPackage.mPath) != 0))
        {
            if (!this.mNoDexOpt)
                break label70;
            Slog.i("PackageManager", "dex file doesn't exist, skipping move: " + paramPackage.mPath);
        }
        for (int i = 1; ; i = -4)
        {
            return i;
            label70: Slog.e("PackageManager", "Couldn't rename dex file: " + paramPackage.mPath);
        }
    }

    private int packageFlagsToInstallFlags(PackageSetting paramPackageSetting)
    {
        int i = 0;
        if (isExternal(paramPackageSetting))
            i = 0x0 | 0x8;
        if (isForwardLocked(paramPackageSetting))
            i |= 1;
        return i;
    }

    private int performDexOptLI(PackageParser.Package paramPackage, boolean paramBoolean1, boolean paramBoolean2)
    {
        int i = 1;
        int j = 0;
        if ((0x4 & paramPackage.applicationInfo.flags) != 0)
        {
            String str = paramPackage.mScanPath;
            int k = 0;
            if (!paramBoolean1);
            try
            {
                Installer localInstaller;
                int m;
                if (DexFile.isDexOptNeeded(str))
                {
                    if ((!paramBoolean1) && (paramBoolean2))
                    {
                        this.mDeferredDexOpt.add(paramPackage);
                        i = 2;
                        break label299;
                    }
                    Log.i("PackageManager", "Running dexopt on: " + paramPackage.applicationInfo.packageName);
                    localInstaller = this.mInstaller;
                    m = paramPackage.applicationInfo.uid;
                    if (isForwardLocked(paramPackage))
                        break label153;
                }
                label153: int i1;
                for (int n = i; ; i1 = 0)
                {
                    k = localInstaller.dexopt(str, m, n);
                    paramPackage.mDidDexOpt = true;
                    j = 1;
                    if (k >= 0)
                        break;
                    i = -1;
                    break label299;
                }
            }
            catch (FileNotFoundException localFileNotFoundException)
            {
                while (true)
                {
                    Slog.w("PackageManager", "Apk not found for dexopt: " + str);
                    k = -1;
                }
            }
            catch (IOException localIOException)
            {
                while (true)
                {
                    Slog.w("PackageManager", "IOException reading apk: " + str, localIOException);
                    k = -1;
                }
            }
            catch (StaleDexCacheError localStaleDexCacheError)
            {
                while (true)
                {
                    Slog.w("PackageManager", "StaleDexCacheError when reading apk: " + str, localStaleDexCacheError);
                    k = -1;
                }
            }
            catch (Exception localException)
            {
                while (true)
                {
                    Slog.w("PackageManager", "Exception when doing dexopt : ", localException);
                    k = -1;
                }
            }
        }
        else if (j == 0)
        {
            i = 0;
        }
        label299: return i;
    }

    private void processPendingInstall(final InstallArgs paramInstallArgs, final int paramInt)
    {
        this.mHandler.post(new Runnable()
        {
            public void run()
            {
                PackageManagerService.this.mHandler.removeCallbacks(this);
                PackageManagerService.PackageInstalledInfo localPackageInstalledInfo = new PackageManagerService.PackageInstalledInfo(PackageManagerService.this);
                localPackageInstalledInfo.returnCode = paramInt;
                localPackageInstalledInfo.uid = -1;
                localPackageInstalledInfo.pkg = null;
                localPackageInstalledInfo.removedInfo = new PackageManagerService.PackageRemovedInfo();
                if (localPackageInstalledInfo.returnCode == 1)
                    paramInstallArgs.doPreInstall(localPackageInstalledInfo.returnCode);
                while (true)
                {
                    int i;
                    int j;
                    int k;
                    IBackupManager localIBackupManager;
                    synchronized (PackageManagerService.this.mInstallLock)
                    {
                        PackageManagerService.this.installPackageLI(paramInstallArgs, true, localPackageInstalledInfo);
                        paramInstallArgs.doPostInstall(localPackageInstalledInfo.returnCode, localPackageInstalledInfo.uid);
                        if (localPackageInstalledInfo.removedInfo.removedPackage != null)
                        {
                            i = 1;
                            if ((i != 0) || (localPackageInstalledInfo.pkg == null) || (localPackageInstalledInfo.pkg.applicationInfo.backupAgentName == null))
                                break label321;
                            j = 1;
                            if (PackageManagerService.this.mNextInstallToken < 0)
                                PackageManagerService.this.mNextInstallToken = 1;
                            PackageManagerService localPackageManagerService = PackageManagerService.this;
                            k = localPackageManagerService.mNextInstallToken;
                            localPackageManagerService.mNextInstallToken = (k + 1);
                            PackageManagerService.PostInstallData localPostInstallData = new PackageManagerService.PostInstallData(PackageManagerService.this, paramInstallArgs, localPackageInstalledInfo);
                            PackageManagerService.this.mRunningInstalls.put(k, localPostInstallData);
                            if ((localPackageInstalledInfo.returnCode == 1) && (j != 0))
                            {
                                localIBackupManager = IBackupManager.Stub.asInterface(ServiceManager.getService("backup"));
                                if (localIBackupManager == null)
                                    break label343;
                            }
                        }
                    }
                    try
                    {
                        localIBackupManager.restoreAtInstall(localPackageInstalledInfo.pkg.applicationInfo.packageName, k);
                        label273: if (j == 0)
                        {
                            Message localMessage = PackageManagerService.this.mHandler.obtainMessage(9, k, 0);
                            PackageManagerService.this.mHandler.sendMessage(localMessage);
                        }
                        return;
                        localObject2 = finally;
                        throw localObject2;
                        i = 0;
                        continue;
                        label321: j = 0;
                    }
                    catch (Exception localException)
                    {
                        while (true)
                        {
                            Slog.e("PackageManager", "Exception trying to enqueue restore", localException);
                            j = 0;
                            continue;
                            Slog.e("PackageManager", "Backup Manager not found!");
                            j = 0;
                        }
                    }
                    catch (RemoteException localRemoteException)
                    {
                        label343: break label273;
                    }
                }
            }
        });
    }

    private void processPendingMove(final MoveParams paramMoveParams, final int paramInt)
    {
        this.mHandler.post(new Runnable()
        {
            public void run()
            {
                PackageManagerService.this.mHandler.removeCallbacks(this);
                int i = paramInt;
                Object localObject4;
                Object localObject5;
                if (paramInt == 1)
                {
                    localObject4 = null;
                    localObject5 = null;
                }
                while (true)
                {
                    PackageParser.Package localPackage2;
                    PackageParser.Package localPackage3;
                    IPackageMoveObserver localIPackageMoveObserver;
                    synchronized (PackageManagerService.this.mPackages)
                    {
                        localPackage2 = (PackageParser.Package)PackageManagerService.this.mPackages.get(paramMoveParams.packageName);
                        if (localPackage2 == null)
                        {
                            Slog.w("PackageManager", " Package " + paramMoveParams.packageName + " doesn't exist. Aborting move");
                            i = -2;
                            if (i == 1)
                                PackageManagerService.this.sendResourcesChangedBroadcast(false, (ArrayList)localObject5, (int[])localObject4, null);
                            synchronized (PackageManagerService.this.mInstallLock)
                            {
                                synchronized (PackageManagerService.this.mPackages)
                                {
                                    localPackage3 = (PackageParser.Package)PackageManagerService.this.mPackages.get(paramMoveParams.packageName);
                                    if (localPackage3 == null)
                                    {
                                        Slog.w("PackageManager", " Package " + paramMoveParams.packageName + " doesn't exist. Aborting move");
                                        i = -2;
                                        PackageManagerService.this.sendResourcesChangedBroadcast(true, (ArrayList)localObject5, (int[])localObject4, null);
                                        if (i == 1)
                                            break label1076;
                                        if (paramMoveParams.targetArgs != null)
                                            paramMoveParams.targetArgs.doPostInstall(-110, -1);
                                        if (i != -7);
                                        synchronized (PackageManagerService.this.mPackages)
                                        {
                                            PackageParser.Package localPackage1 = (PackageParser.Package)PackageManagerService.this.mPackages.get(paramMoveParams.packageName);
                                            if (localPackage1 != null)
                                                localPackage1.mOperationPending = false;
                                            localIPackageMoveObserver = paramMoveParams.observer;
                                            if (localIPackageMoveObserver == null);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    try
                    {
                        localIPackageMoveObserver.packageMoved(paramMoveParams.packageName, i);
                        return;
                        if (!paramMoveParams.srcArgs.getCodePath().equals(localPackage2.applicationInfo.sourceDir))
                        {
                            Slog.w("PackageManager", "Package " + paramMoveParams.packageName + " code path changed from " + paramMoveParams.srcArgs.getCodePath() + " to " + localPackage2.applicationInfo.sourceDir + " Aborting move and returning error");
                            i = -6;
                            continue;
                        }
                        arrayOfInt = new int[1];
                        arrayOfInt[0] = localPackage2.applicationInfo.uid;
                    }
                    catch (RemoteException localObject6)
                    {
                        try
                        {
                            localArrayList = new ArrayList();
                        }
                        finally
                        {
                            try
                            {
                                int[] arrayOfInt;
                                ArrayList localArrayList;
                                localArrayList.add(paramMoveParams.packageName);
                                localObject5 = localArrayList;
                                localObject4 = arrayOfInt;
                                continue;
                                localObject6 = finally;
                                throw localObject6;
                                if (!paramMoveParams.srcArgs.getCodePath().equals(localPackage3.applicationInfo.sourceDir))
                                {
                                    Slog.w("PackageManager", "Package " + paramMoveParams.packageName + " code path changed from " + paramMoveParams.srcArgs.getCodePath() + " to " + localPackage3.applicationInfo.sourceDir + " Aborting move and returning error");
                                    i = -6;
                                    continue;
                                }
                                str1 = localPackage3.mPath;
                                str2 = paramMoveParams.targetArgs.getCodePath();
                                str3 = paramMoveParams.targetArgs.getResourcePath();
                                str4 = paramMoveParams.targetArgs.getNativeLibraryPath();
                            }
                            finally
                            {
                                try
                                {
                                    while (true)
                                    {
                                        while (true)
                                        {
                                            String str1;
                                            String str2;
                                            String str3;
                                            String str4;
                                            File localFile1 = new File(str4);
                                            localFile1.getParentFile().getCanonicalPath();
                                            label723: PackageSetting localPackageSetting;
                                            ApplicationInfo localApplicationInfo2;
                                            if (localFile1.getParentFile().getCanonicalPath().equals(localPackage3.applicationInfo.dataDir))
                                            {
                                                int k = PackageManagerService.this.mInstaller.unlinkNativeLibraryDirectory(localPackage3.applicationInfo.dataDir);
                                                if (k < 0)
                                                {
                                                    i = -1;
                                                    if (i == 1)
                                                    {
                                                        localPackage3.mPath = str2;
                                                        if (PackageManagerService.this.moveDexFilesLI(localPackage3) != 1)
                                                        {
                                                            localPackage3.mPath = localPackage3.mScanPath;
                                                            i = -1;
                                                        }
                                                    }
                                                    if (i != 1)
                                                        break;
                                                    localPackage3.mScanPath = str2;
                                                    localPackage3.applicationInfo.sourceDir = str2;
                                                    localPackage3.applicationInfo.publicSourceDir = str3;
                                                    localPackage3.applicationInfo.nativeLibraryDir = str4;
                                                    localPackageSetting = (PackageSetting)localPackage3.mExtras;
                                                    File localFile2 = new File(localPackage3.applicationInfo.sourceDir);
                                                    localPackageSetting.codePath = localFile2;
                                                    localPackageSetting.codePathString = localPackageSetting.codePath.getPath();
                                                    File localFile3 = new File(localPackage3.applicationInfo.publicSourceDir);
                                                    localPackageSetting.resourcePath = localFile3;
                                                    localPackageSetting.resourcePathString = localPackageSetting.resourcePath.getPath();
                                                    localPackageSetting.nativeLibraryPathString = str4;
                                                    if ((0x8 & paramMoveParams.flags) == 0)
                                                        break label1052;
                                                    localApplicationInfo2 = localPackage3.applicationInfo;
                                                }
                                            }
                                            label1052: ApplicationInfo localApplicationInfo1;
                                            for (localApplicationInfo2.flags = (0x40000 | localApplicationInfo2.flags); ; localApplicationInfo1.flags = (0xFFFBFFFF & localApplicationInfo1.flags))
                                            {
                                                localPackageSetting.setFlags(localPackage3.applicationInfo.flags);
                                                PackageManagerService.this.mAppDirs.remove(str1);
                                                PackageManagerService.this.mAppDirs.put(str2, localPackage3);
                                                PackageManagerService.this.mSettings.writeLPr();
                                                break;
                                                localObject11 = finally;
                                                throw localObject11;
                                                localObject10 = finally;
                                                throw localObject10;
                                                File localFile4 = new File(str2);
                                                NativeLibraryHelper.copyNativeBinariesIfNeededLI(localFile4, localFile1);
                                                break label723;
                                                int j = PackageManagerService.this.mInstaller.linkNativeLibraryDirectory(localPackage3.applicationInfo.dataDir, str4);
                                                if (j >= 0)
                                                    break label723;
                                                i = -1;
                                                break label723;
                                                localApplicationInfo1 = localPackage3.applicationInfo;
                                            }
                                            label1076: Runtime.getRuntime().gc();
                                            synchronized (PackageManagerService.this.mInstallLock)
                                            {
                                                paramMoveParams.srcArgs.doPostDeleteLI(true);
                                            }
                                        }
                                        localObject3 = finally;
                                        throw localObject3;
                                        localRemoteException = localRemoteException;
                                        Log.i("PackageManager", "Observer no longer exists.");
                                        continue;
                                        localObject7 = finally;
                                    }
                                    localObject8 = finally;
                                }
                                catch (IOException localIOException)
                                {
                                    while (true)
                                        i = -5;
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    private void readPermissionsFromXml(File paramFile)
    {
        try
        {
            localFileReader = new FileReader(paramFile);
        }
        catch (FileNotFoundException localFileNotFoundException)
        {
            try
            {
                FileReader localFileReader;
                localXmlPullParser = Xml.newPullParser();
                localXmlPullParser.setInput(localFileReader);
                XmlUtils.beginDocument(localXmlPullParser, "permissions");
                while (true)
                {
                    XmlUtils.nextElement(localXmlPullParser);
                    if (localXmlPullParser.getEventType() == 1)
                    {
                        localFileReader.close();
                        while (true)
                        {
                            return;
                            localFileNotFoundException = localFileNotFoundException;
                            Slog.w("PackageManager", "Couldn't find or open permissions file " + paramFile);
                        }
                    }
                    str1 = localXmlPullParser.getName();
                    if (!"group".equals(str1))
                        break label213;
                    String str9 = localXmlPullParser.getAttributeValue(null, "gid");
                    if (str9 == null)
                        break;
                    int j = Integer.parseInt(str9);
                    this.mGlobalGids = ArrayUtils.appendInt(this.mGlobalGids, j);
                    XmlUtils.skipCurrentTag(localXmlPullParser);
                }
            }
            catch (XmlPullParserException localXmlPullParserException)
            {
                while (true)
                {
                    Slog.w("PackageManager", "Got execption parsing permissions.", localXmlPullParserException);
                    continue;
                    Slog.w("PackageManager", "<group> without gid at " + localXmlPullParser.getPositionDescription());
                }
            }
            catch (IOException localIOException)
            {
                while (true)
                {
                    XmlPullParser localXmlPullParser;
                    String str1;
                    Slog.w("PackageManager", "Got execption parsing permissions.", localIOException);
                    continue;
                    label213: if ("permission".equals(str1))
                    {
                        String str8 = localXmlPullParser.getAttributeValue(null, "name");
                        if (str8 == null)
                        {
                            Slog.w("PackageManager", "<permission> without name at " + localXmlPullParser.getPositionDescription());
                            XmlUtils.skipCurrentTag(localXmlPullParser);
                        }
                        else
                        {
                            readPermission(localXmlPullParser, str8.intern());
                        }
                    }
                    else if ("assign-permission".equals(str1))
                    {
                        String str5 = localXmlPullParser.getAttributeValue(null, "name");
                        if (str5 == null)
                        {
                            Slog.w("PackageManager", "<assign-permission> without name at " + localXmlPullParser.getPositionDescription());
                            XmlUtils.skipCurrentTag(localXmlPullParser);
                        }
                        else
                        {
                            String str6 = localXmlPullParser.getAttributeValue(null, "uid");
                            if (str6 == null)
                            {
                                Slog.w("PackageManager", "<assign-permission> without uid at " + localXmlPullParser.getPositionDescription());
                                XmlUtils.skipCurrentTag(localXmlPullParser);
                            }
                            else
                            {
                                int i = Process.getUidForName(str6);
                                if (i < 0)
                                {
                                    Slog.w("PackageManager", "<assign-permission> with unknown uid \"" + str6 + "\" at " + localXmlPullParser.getPositionDescription());
                                    XmlUtils.skipCurrentTag(localXmlPullParser);
                                }
                                else
                                {
                                    String str7 = str5.intern();
                                    HashSet localHashSet = (HashSet)this.mSystemPermissions.get(i);
                                    if (localHashSet == null)
                                    {
                                        localHashSet = new HashSet();
                                        this.mSystemPermissions.put(i, localHashSet);
                                    }
                                    localHashSet.add(str7);
                                    XmlUtils.skipCurrentTag(localXmlPullParser);
                                }
                            }
                        }
                    }
                    else
                    {
                        if ("library".equals(str1))
                        {
                            String str3 = localXmlPullParser.getAttributeValue(null, "name");
                            String str4 = localXmlPullParser.getAttributeValue(null, "file");
                            if (str3 == null)
                                Slog.w("PackageManager", "<library> without name at " + localXmlPullParser.getPositionDescription());
                            while (true)
                            {
                                XmlUtils.skipCurrentTag(localXmlPullParser);
                                break;
                                if (str4 == null)
                                    Slog.w("PackageManager", "<library> without file at " + localXmlPullParser.getPositionDescription());
                                else
                                    this.mSharedLibraries.put(str3, str4);
                            }
                        }
                        if ("feature".equals(str1))
                        {
                            String str2 = localXmlPullParser.getAttributeValue(null, "name");
                            if (str2 == null)
                                Slog.w("PackageManager", "<feature> without name at " + localXmlPullParser.getPositionDescription());
                            while (true)
                            {
                                XmlUtils.skipCurrentTag(localXmlPullParser);
                                break;
                                FeatureInfo localFeatureInfo = new FeatureInfo();
                                localFeatureInfo.name = str2;
                                this.mAvailableFeatures.put(str2, localFeatureInfo);
                            }
                        }
                        XmlUtils.skipCurrentTag(localXmlPullParser);
                    }
                }
            }
        }
    }

    static int[] removeInts(int[] paramArrayOfInt1, int[] paramArrayOfInt2)
    {
        if (paramArrayOfInt2 == null);
        while (true)
        {
            return paramArrayOfInt1;
            if (paramArrayOfInt1 != null)
            {
                int i = paramArrayOfInt2.length;
                for (int j = 0; j < i; j++)
                    paramArrayOfInt1 = ArrayUtils.removeInt(paramArrayOfInt1, paramArrayOfInt2[j]);
            }
        }
    }

    private void removePackageDataLI(PackageParser.Package paramPackage, PackageRemovedInfo paramPackageRemovedInfo, int paramInt, boolean paramBoolean)
    {
        String str = paramPackage.packageName;
        if (paramPackageRemovedInfo != null)
            paramPackageRemovedInfo.removedPackage = str;
        boolean bool;
        if ((0x10000 & paramInt) != 0)
            bool = true;
        while (true)
        {
            removePackageLI(paramPackage, bool);
            PackageSetting localPackageSetting;
            label123: HashMap localHashMap2;
            synchronized (this.mPackages)
            {
                localPackageSetting = (PackageSetting)this.mSettings.mPackages.get(str);
                if ((paramInt & 0x1) == 0)
                {
                    int i = this.mInstaller.remove(str, 0);
                    if (i < 0)
                    {
                        Slog.w("PackageManager", "Couldn't remove app data or cache directory for package: " + str + ", retcode=" + i);
                        schedulePackageCleaning(str);
                    }
                }
                else
                {
                    localHashMap2 = this.mPackages;
                    if ((localPackageSetting != null) && ((paramInt & 0x1) == 0))
                        if (paramPackageRemovedInfo == null);
                }
            }
            try
            {
                paramPackageRemovedInfo.removedUid = this.mSettings.removePackageLPw(str);
                if (localPackageSetting != null)
                {
                    updatePermissionsLPw(localPackageSetting.name, null, 0);
                    if (localPackageSetting.sharedUser != null)
                        this.mSettings.updateSharedUserPermsLPw(localPackageSetting, this.mGlobalGids);
                }
                clearPackagePreferredActivitiesLPw(localPackageSetting.name);
                if (paramBoolean)
                    this.mSettings.writeLPr();
                return;
                bool = false;
                continue;
                localObject1 = finally;
                throw localObject1;
                sUserManager.removePackageForAllUsers(str);
                break label123;
            }
            finally
            {
            }
        }
    }

    private void replaceNonSystemPackageLI(PackageParser.Package paramPackage1, PackageParser.Package paramPackage2, int paramInt1, int paramInt2, String paramString, PackageInstalledInfo paramPackageInstalledInfo)
    {
        String str = paramPackage1.packageName;
        int i = 1;
        int j = 0;
        long l;
        label58: int m;
        label130: int i1;
        label146: int i3;
        if (paramPackage2.mExtras != null)
        {
            l = ((PackageSetting)paramPackage2.mExtras).lastUpdateTime;
            if (deletePackageLI(str, true, 1, paramPackageInstalledInfo.removedInfo, true))
                break label222;
            paramPackageInstalledInfo.returnCode = -10;
            i = 0;
            if (paramPackageInstalledInfo.returnCode != 1)
            {
                if (j != 0)
                    deletePackageLI(str, true, 1, paramPackageInstalledInfo.removedInfo, true);
                if (i != 0)
                {
                    File localFile = new File(paramPackage1.mPath);
                    boolean bool = isExternal(paramPackage1);
                    int k = 0x2 | this.mDefParseFlags;
                    if (!isForwardLocked(paramPackage1))
                        break label322;
                    m = 16;
                    int n = k | m;
                    if (!bool)
                        break label328;
                    i1 = 32;
                    int i2 = n | i1;
                    if (!bool)
                        break label334;
                    i3 = 0;
                    label161: if (scanPackageLI(localFile, i2, 0x40 | (i3 | 0x8), l) != null)
                        break label340;
                    Slog.e("PackageManager", "Failed to restore package : " + str + " after failed upgrade");
                }
            }
        }
        while (true)
        {
            return;
            l = 0L;
            break;
            label222: this.mLastScanError = 1;
            PackageParser.Package localPackage = scanPackageLI(paramPackage2, paramInt1, paramInt2 | 0x40, System.currentTimeMillis());
            if (localPackage == null)
            {
                Slog.w("PackageManager", "Package couldn't be installed in " + paramPackage2.mPath);
                int i4 = this.mLastScanError;
                paramPackageInstalledInfo.returnCode = i4;
                if (i4 != 1)
                    break label58;
                paramPackageInstalledInfo.returnCode = -2;
                break label58;
            }
            updateSettingsLI(localPackage, paramString, paramPackageInstalledInfo);
            j = 1;
            break label58;
            label322: m = 0;
            break label130;
            label328: i1 = 0;
            break label146;
            label334: i3 = 1;
            break label161;
            label340: synchronized (this.mPackages)
            {
                updatePermissionsLPw(paramPackage1.packageName, paramPackage1, 1);
                this.mSettings.writeLPr();
                Slog.i("PackageManager", "Successfully restored package : " + str + " after failed upgrade");
            }
        }
    }

    private void replacePackageLI(PackageParser.Package paramPackage, int paramInt1, int paramInt2, String paramString, PackageInstalledInfo paramPackageInstalledInfo)
    {
        String str = paramPackage.packageName;
        PackageParser.Package localPackage;
        synchronized (this.mPackages)
        {
            localPackage = (PackageParser.Package)this.mPackages.get(str);
            if (compareSignatures(localPackage.mSignatures, paramPackage.mSignatures) != 0)
            {
                Slog.w("PackageManager", "New package has a different signature: " + str);
                paramPackageInstalledInfo.returnCode = -104;
            }
            else if (isSystemApp(localPackage))
            {
                replaceSystemPackageLI(localPackage, paramPackage, paramInt1, paramInt2, paramString, paramPackageInstalledInfo);
            }
        }
        replaceNonSystemPackageLI(localPackage, paramPackage, paramInt1, paramInt2, paramString, paramPackageInstalledInfo);
    }

    // ERROR //
    private void replaceSystemPackageLI(PackageParser.Package paramPackage1, PackageParser.Package paramPackage2, int paramInt1, int paramInt2, String paramString, PackageInstalledInfo paramPackageInstalledInfo)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore 7
        //     3: iload_3
        //     4: iconst_3
        //     5: ior
        //     6: istore 8
        //     8: aload_1
        //     9: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     12: astore 9
        //     14: aload 6
        //     16: bipush 246
        //     18: putfield 1943	com/android/server/pm/PackageManagerService$PackageInstalledInfo:returnCode	I
        //     21: aload 9
        //     23: ifnonnull +13 -> 36
        //     26: ldc 186
        //     28: ldc_w 1256
        //     31: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     34: pop
        //     35: return
        //     36: aload_0
        //     37: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     40: astore 10
        //     42: aload 10
        //     44: monitorenter
        //     45: aload_0
        //     46: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     49: aload 9
        //     51: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     54: checkcast 800	android/content/pm/PackageParser$Package
        //     57: astore 12
        //     59: aload_0
        //     60: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     63: getfield 783	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
        //     66: aload 9
        //     68: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     71: checkcast 785	com/android/server/pm/PackageSetting
        //     74: astore 13
        //     76: aload 12
        //     78: ifnull +16 -> 94
        //     81: aload 12
        //     83: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     86: ifnull +8 -> 94
        //     89: aload 13
        //     91: ifnonnull +50 -> 141
        //     94: ldc 186
        //     96: new 662	java/lang/StringBuilder
        //     99: dup
        //     100: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     103: ldc_w 2322
        //     106: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     109: aload 9
        //     111: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     114: ldc_w 2324
        //     117: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     120: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     123: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     126: pop
        //     127: aload 10
        //     129: monitorexit
        //     130: goto -95 -> 35
        //     133: astore 11
        //     135: aload 10
        //     137: monitorexit
        //     138: aload 11
        //     140: athrow
        //     141: aload 10
        //     143: monitorexit
        //     144: aload_0
        //     145: aload 9
        //     147: aload 12
        //     149: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     152: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     155: invokespecial 1502	com/android/server/pm/PackageManagerService:killApplication	(Ljava/lang/String;I)V
        //     158: aload 6
        //     160: getfield 1961	com/android/server/pm/PackageManagerService$PackageInstalledInfo:removedInfo	Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;
        //     163: aload 12
        //     165: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     168: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     171: putfield 1466	com/android/server/pm/PackageManagerService$PackageRemovedInfo:uid	I
        //     174: aload 6
        //     176: getfield 1961	com/android/server/pm/PackageManagerService$PackageInstalledInfo:removedInfo	Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;
        //     179: aload 9
        //     181: putfield 2285	com/android/server/pm/PackageManagerService$PackageRemovedInfo:removedPackage	Ljava/lang/String;
        //     184: aload_0
        //     185: aload 12
        //     187: iconst_1
        //     188: invokevirtual 811	com/android/server/pm/PackageManagerService:removePackageLI	(Landroid/content/pm/PackageParser$Package;Z)V
        //     191: aload_0
        //     192: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     195: astore 15
        //     197: aload 15
        //     199: monitorenter
        //     200: aload_0
        //     201: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     204: aload 9
        //     206: invokevirtual 2327	com/android/server/pm/Settings:disableSystemPackageLPw	(Ljava/lang/String;)Z
        //     209: ifne +233 -> 442
        //     212: aload_1
        //     213: ifnull +229 -> 442
        //     216: aload 6
        //     218: getfield 1961	com/android/server/pm/PackageManagerService$PackageInstalledInfo:removedInfo	Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;
        //     221: aload_0
        //     222: iconst_0
        //     223: aload_1
        //     224: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     227: getfield 1475	android/content/pm/ApplicationInfo:sourceDir	Ljava/lang/String;
        //     230: aload_1
        //     231: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     234: getfield 1478	android/content/pm/ApplicationInfo:publicSourceDir	Ljava/lang/String;
        //     237: aload_1
        //     238: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     241: getfield 1481	android/content/pm/ApplicationInfo:nativeLibraryDir	Ljava/lang/String;
        //     244: invokespecial 1483	com/android/server/pm/PackageManagerService:createInstallArgs	(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PackageManagerService$InstallArgs;
        //     247: putfield 1487	com/android/server/pm/PackageManagerService$PackageRemovedInfo:args	Lcom/android/server/pm/PackageManagerService$InstallArgs;
        //     250: aload 15
        //     252: monitorexit
        //     253: aload_0
        //     254: iconst_1
        //     255: putfield 1360	com/android/server/pm/PackageManagerService:mLastScanError	I
        //     258: aload_2
        //     259: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     262: astore 17
        //     264: aload 17
        //     266: sipush 128
        //     269: aload 17
        //     271: getfield 886	android/content/pm/ApplicationInfo:flags	I
        //     274: ior
        //     275: putfield 886	android/content/pm/ApplicationInfo:flags	I
        //     278: aload_0
        //     279: aload_2
        //     280: iload 8
        //     282: iload 4
        //     284: lconst_0
        //     285: invokespecial 1951	com/android/server/pm/PackageManagerService:scanPackageLI	(Landroid/content/pm/PackageParser$Package;IIJ)Landroid/content/pm/PackageParser$Package;
        //     288: astore 18
        //     290: aload 18
        //     292: ifnonnull +170 -> 462
        //     295: ldc 186
        //     297: new 662	java/lang/StringBuilder
        //     300: dup
        //     301: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     304: ldc_w 1953
        //     307: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     310: aload_2
        //     311: getfield 1722	android/content/pm/PackageParser$Package:mPath	Ljava/lang/String;
        //     314: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     317: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     320: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     323: pop
        //     324: aload_0
        //     325: getfield 1360	com/android/server/pm/PackageManagerService:mLastScanError	I
        //     328: istore 25
        //     330: aload 6
        //     332: iload 25
        //     334: putfield 1943	com/android/server/pm/PackageManagerService$PackageInstalledInfo:returnCode	I
        //     337: iload 25
        //     339: iconst_1
        //     340: if_icmpne +10 -> 350
        //     343: aload 6
        //     345: bipush 254
        //     347: putfield 1943	com/android/server/pm/PackageManagerService$PackageInstalledInfo:returnCode	I
        //     350: aload 6
        //     352: getfield 1943	com/android/server/pm/PackageManagerService$PackageInstalledInfo:returnCode	I
        //     355: iconst_1
        //     356: if_icmpeq -321 -> 35
        //     359: aload 18
        //     361: ifnull +10 -> 371
        //     364: aload_0
        //     365: aload 18
        //     367: iconst_1
        //     368: invokevirtual 811	com/android/server/pm/PackageManagerService:removePackageLI	(Landroid/content/pm/PackageParser$Package;Z)V
        //     371: aload_0
        //     372: aload 12
        //     374: iload 8
        //     376: bipush 9
        //     378: lconst_0
        //     379: invokespecial 1951	com/android/server/pm/PackageManagerService:scanPackageLI	(Landroid/content/pm/PackageParser$Package;IIJ)Landroid/content/pm/PackageParser$Package;
        //     382: pop
        //     383: aload_0
        //     384: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     387: astore 20
        //     389: aload 20
        //     391: monitorenter
        //     392: iload 7
        //     394: ifeq +27 -> 421
        //     397: aload_0
        //     398: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     401: aload 9
        //     403: invokevirtual 1580	com/android/server/pm/Settings:enableSystemPackageLPw	(Ljava/lang/String;)Lcom/android/server/pm/PackageSetting;
        //     406: pop
        //     407: aload_0
        //     408: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     411: aload 9
        //     413: aload 13
        //     415: getfield 2328	com/android/server/pm/PackageSettingBase:installerPackageName	Ljava/lang/String;
        //     418: invokevirtual 2331	com/android/server/pm/Settings:setInstallerPackageName	(Ljava/lang/String;Ljava/lang/String;)V
        //     421: aload_0
        //     422: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     425: invokevirtual 951	com/android/server/pm/Settings:writeLPr	()V
        //     428: aload 20
        //     430: monitorexit
        //     431: goto -396 -> 35
        //     434: astore 21
        //     436: aload 20
        //     438: monitorexit
        //     439: aload 21
        //     441: athrow
        //     442: aload 6
        //     444: getfield 1961	com/android/server/pm/PackageManagerService$PackageInstalledInfo:removedInfo	Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;
        //     447: aconst_null
        //     448: putfield 1487	com/android/server/pm/PackageManagerService$PackageRemovedInfo:args	Lcom/android/server/pm/PackageManagerService$InstallArgs;
        //     451: goto -201 -> 250
        //     454: astore 16
        //     456: aload 15
        //     458: monitorexit
        //     459: aload 16
        //     461: athrow
        //     462: aload 18
        //     464: getfield 1827	android/content/pm/PackageParser$Package:mExtras	Ljava/lang/Object;
        //     467: ifnull +31 -> 498
        //     470: aload 18
        //     472: getfield 1827	android/content/pm/PackageParser$Package:mExtras	Ljava/lang/Object;
        //     475: checkcast 785	com/android/server/pm/PackageSetting
        //     478: astore 23
        //     480: aload 23
        //     482: aload 13
        //     484: getfield 2334	com/android/server/pm/PackageSettingBase:firstInstallTime	J
        //     487: putfield 2334	com/android/server/pm/PackageSettingBase:firstInstallTime	J
        //     490: aload 23
        //     492: invokestatic 1948	java/lang/System:currentTimeMillis	()J
        //     495: putfield 2307	com/android/server/pm/PackageSettingBase:lastUpdateTime	J
        //     498: aload_0
        //     499: aload 18
        //     501: aload 5
        //     503: aload 6
        //     505: invokespecial 1957	com/android/server/pm/PackageManagerService:updateSettingsLI	(Landroid/content/pm/PackageParser$Package;Ljava/lang/String;Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;)V
        //     508: iconst_1
        //     509: istore 7
        //     511: goto -161 -> 350
        //
        // Exception table:
        //     from	to	target	type
        //     45	138	133	finally
        //     141	144	133	finally
        //     397	439	434	finally
        //     200	253	454	finally
        //     442	459	454	finally
    }

    static void reportSettingsProblem(int paramInt, String paramString)
    {
        try
        {
            File localFile = getSettingsProblemFile();
            PrintWriter localPrintWriter = new PrintWriter(new FileOutputStream(localFile, true));
            String str = new SimpleDateFormat().format(new Date(System.currentTimeMillis()));
            localPrintWriter.println(str + ": " + paramString);
            localPrintWriter.close();
            FileUtils.setPermissions(localFile.toString(), 508, -1, -1);
            label97: Slog.println(paramInt, "PackageManager", paramString);
            return;
        }
        catch (IOException localIOException)
        {
            break label97;
        }
    }

    private void scanDirLI(File paramFile, int paramInt1, int paramInt2, long paramLong)
    {
        String[] arrayOfString = paramFile.list();
        if (arrayOfString == null)
        {
            Log.d("PackageManager", "No files in app dir " + paramFile);
            return;
        }
        int i = 0;
        label41: File localFile;
        if (i < arrayOfString.length)
        {
            localFile = new File(paramFile, arrayOfString[i]);
            if (isPackageFilename(arrayOfString[i]))
                break label81;
        }
        while (true)
        {
            i++;
            break label41;
            break;
            label81: if ((scanPackageLI(localFile, paramInt1 | 0x4, paramInt2, paramLong) == null) && ((paramInt1 & 0x1) == 0) && (this.mLastScanError == -2))
            {
                Slog.w("PackageManager", "Cleaning up failed install of " + localFile);
                localFile.delete();
            }
        }
    }

    // ERROR //
    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private PackageParser.Package scanPackageLI(PackageParser.Package paramPackage, int paramInt1, int paramInt2, long paramLong)
    {
        // Byte code:
        //     0: new 574	java/io/File
        //     3: dup
        //     4: aload_1
        //     5: getfield 2126	android/content/pm/PackageParser$Package:mScanPath	Ljava/lang/String;
        //     8: invokespecial 775	java/io/File:<init>	(Ljava/lang/String;)V
        //     11: astore 6
        //     13: aload 6
        //     15: ifnull +23 -> 38
        //     18: aload_1
        //     19: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     22: getfield 1475	android/content/pm/ApplicationInfo:sourceDir	Ljava/lang/String;
        //     25: ifnull +13 -> 38
        //     28: aload_1
        //     29: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     32: getfield 1478	android/content/pm/ApplicationInfo:publicSourceDir	Ljava/lang/String;
        //     35: ifnonnull +22 -> 57
        //     38: ldc 186
        //     40: ldc_w 2377
        //     43: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     46: pop
        //     47: aload_0
        //     48: bipush 254
        //     50: putfield 1360	com/android/server/pm/PackageManagerService:mLastScanError	I
        //     53: aconst_null
        //     54: astore_1
        //     55: aload_1
        //     56: areturn
        //     57: aload_0
        //     58: aload 6
        //     60: putfield 2379	com/android/server/pm/PackageManagerService:mScanningPath	Ljava/io/File;
        //     63: iload_2
        //     64: iconst_1
        //     65: iand
        //     66: ifeq +21 -> 87
        //     69: aload_1
        //     70: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     73: astore 190
        //     75: aload 190
        //     77: iconst_1
        //     78: aload 190
        //     80: getfield 886	android/content/pm/ApplicationInfo:flags	I
        //     83: ior
        //     84: putfield 886	android/content/pm/ApplicationInfo:flags	I
        //     87: aload_1
        //     88: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     91: ldc_w 2381
        //     94: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     97: ifeq +280 -> 377
        //     100: aload_0
        //     101: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     104: astore 183
        //     106: aload 183
        //     108: monitorenter
        //     109: aload_0
        //     110: getfield 2383	com/android/server/pm/PackageManagerService:mAndroidApplication	Landroid/content/pm/ApplicationInfo;
        //     113: ifnull +81 -> 194
        //     116: ldc 186
        //     118: ldc_w 2385
        //     121: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     124: pop
        //     125: ldc 186
        //     127: ldc_w 2387
        //     130: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     133: pop
        //     134: ldc 186
        //     136: new 662	java/lang/StringBuilder
        //     139: dup
        //     140: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     143: ldc_w 2389
        //     146: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     149: aload_0
        //     150: getfield 2379	com/android/server/pm/PackageManagerService:mScanningPath	Ljava/io/File;
        //     153: invokevirtual 944	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     156: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     159: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     162: pop
        //     163: ldc 186
        //     165: ldc_w 2385
        //     168: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     171: pop
        //     172: aload_0
        //     173: bipush 251
        //     175: putfield 1360	com/android/server/pm/PackageManagerService:mLastScanError	I
        //     178: aconst_null
        //     179: astore_1
        //     180: aload 183
        //     182: monitorexit
        //     183: goto -128 -> 55
        //     186: astore 184
        //     188: aload 183
        //     190: monitorexit
        //     191: aload 184
        //     193: athrow
        //     194: aload_0
        //     195: aload_1
        //     196: putfield 1876	com/android/server/pm/PackageManagerService:mPlatformPackage	Landroid/content/pm/PackageParser$Package;
        //     199: aload_0
        //     200: getfield 353	com/android/server/pm/PackageManagerService:mSdkVersion	I
        //     203: istore 185
        //     205: aload_1
        //     206: iload 185
        //     208: putfield 1577	android/content/pm/PackageParser$Package:mVersionCode	I
        //     211: aload_0
        //     212: aload_1
        //     213: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     216: putfield 2383	com/android/server/pm/PackageManagerService:mAndroidApplication	Landroid/content/pm/ApplicationInfo;
        //     219: aload_0
        //     220: getfield 430	com/android/server/pm/PackageManagerService:mResolveActivity	Landroid/content/pm/ActivityInfo;
        //     223: aload_0
        //     224: getfield 2383	com/android/server/pm/PackageManagerService:mAndroidApplication	Landroid/content/pm/ApplicationInfo;
        //     227: putfield 2390	android/content/pm/ActivityInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     230: aload_0
        //     231: getfield 430	com/android/server/pm/PackageManagerService:mResolveActivity	Landroid/content/pm/ActivityInfo;
        //     234: ldc_w 2392
        //     237: invokevirtual 2395	java/lang/Class:getName	()Ljava/lang/String;
        //     240: putfield 2109	android/content/pm/ComponentInfo:name	Ljava/lang/String;
        //     243: aload_0
        //     244: getfield 430	com/android/server/pm/PackageManagerService:mResolveActivity	Landroid/content/pm/ActivityInfo;
        //     247: aload_0
        //     248: getfield 2383	com/android/server/pm/PackageManagerService:mAndroidApplication	Landroid/content/pm/ApplicationInfo;
        //     251: getfield 1626	android/content/pm/ApplicationInfo:packageName	Ljava/lang/String;
        //     254: putfield 1747	android/content/pm/ComponentInfo:packageName	Ljava/lang/String;
        //     257: aload_0
        //     258: getfield 430	com/android/server/pm/PackageManagerService:mResolveActivity	Landroid/content/pm/ActivityInfo;
        //     261: aload_0
        //     262: getfield 2383	com/android/server/pm/PackageManagerService:mAndroidApplication	Landroid/content/pm/ApplicationInfo;
        //     265: getfield 2398	android/content/pm/ApplicationInfo:processName	Ljava/lang/String;
        //     268: putfield 2399	android/content/pm/ActivityInfo:processName	Ljava/lang/String;
        //     271: aload_0
        //     272: getfield 430	com/android/server/pm/PackageManagerService:mResolveActivity	Landroid/content/pm/ActivityInfo;
        //     275: iconst_0
        //     276: putfield 2402	android/content/pm/ActivityInfo:launchMode	I
        //     279: aload_0
        //     280: getfield 430	com/android/server/pm/PackageManagerService:mResolveActivity	Landroid/content/pm/ActivityInfo;
        //     283: bipush 32
        //     285: putfield 2403	android/content/pm/ActivityInfo:flags	I
        //     288: aload_0
        //     289: getfield 430	com/android/server/pm/PackageManagerService:mResolveActivity	Landroid/content/pm/ActivityInfo;
        //     292: ldc_w 2404
        //     295: putfield 2407	android/content/pm/ActivityInfo:theme	I
        //     298: aload_0
        //     299: getfield 430	com/android/server/pm/PackageManagerService:mResolveActivity	Landroid/content/pm/ActivityInfo;
        //     302: iconst_1
        //     303: putfield 2410	android/content/pm/ActivityInfo:exported	Z
        //     306: aload_0
        //     307: getfield 430	com/android/server/pm/PackageManagerService:mResolveActivity	Landroid/content/pm/ActivityInfo;
        //     310: iconst_1
        //     311: putfield 2413	android/content/pm/ComponentInfo:enabled	Z
        //     314: aload_0
        //     315: getfield 435	com/android/server/pm/PackageManagerService:mResolveInfo	Landroid/content/pm/ResolveInfo;
        //     318: aload_0
        //     319: getfield 430	com/android/server/pm/PackageManagerService:mResolveActivity	Landroid/content/pm/ActivityInfo;
        //     322: putfield 1744	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     325: aload_0
        //     326: getfield 435	com/android/server/pm/PackageManagerService:mResolveInfo	Landroid/content/pm/ResolveInfo;
        //     329: iconst_0
        //     330: putfield 1210	android/content/pm/ResolveInfo:priority	I
        //     333: aload_0
        //     334: getfield 435	com/android/server/pm/PackageManagerService:mResolveInfo	Landroid/content/pm/ResolveInfo;
        //     337: iconst_0
        //     338: putfield 1213	android/content/pm/ResolveInfo:preferredOrder	I
        //     341: aload_0
        //     342: getfield 435	com/android/server/pm/PackageManagerService:mResolveInfo	Landroid/content/pm/ResolveInfo;
        //     345: iconst_0
        //     346: putfield 2416	android/content/pm/ResolveInfo:match	I
        //     349: aload_0
        //     350: new 314	android/content/ComponentName
        //     353: dup
        //     354: aload_0
        //     355: getfield 2383	com/android/server/pm/PackageManagerService:mAndroidApplication	Landroid/content/pm/ApplicationInfo;
        //     358: getfield 1626	android/content/pm/ApplicationInfo:packageName	Ljava/lang/String;
        //     361: aload_0
        //     362: getfield 430	com/android/server/pm/PackageManagerService:mResolveActivity	Landroid/content/pm/ActivityInfo;
        //     365: getfield 2109	android/content/pm/ComponentInfo:name	Ljava/lang/String;
        //     368: invokespecial 320	android/content/ComponentName:<init>	(Ljava/lang/String;Ljava/lang/String;)V
        //     371: putfield 2418	com/android/server/pm/PackageManagerService:mResolveComponentName	Landroid/content/ComponentName;
        //     374: aload 183
        //     376: monitorexit
        //     377: aload_0
        //     378: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     381: aload_1
        //     382: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     385: invokevirtual 1936	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
        //     388: ifne +17 -> 405
        //     391: aload_0
        //     392: getfield 387	com/android/server/pm/PackageManagerService:mSharedLibraries	Ljava/util/HashMap;
        //     395: aload_1
        //     396: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     399: invokevirtual 1936	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
        //     402: ifeq +49 -> 451
        //     405: ldc 186
        //     407: new 662	java/lang/StringBuilder
        //     410: dup
        //     411: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     414: ldc_w 2420
        //     417: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     420: aload_1
        //     421: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     424: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     427: ldc_w 2422
        //     430: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     433: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     436: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     439: pop
        //     440: aload_0
        //     441: bipush 251
        //     443: putfield 1360	com/android/server/pm/PackageManagerService:mLastScanError	I
        //     446: aconst_null
        //     447: astore_1
        //     448: goto -393 -> 55
        //     451: new 574	java/io/File
        //     454: dup
        //     455: aload_1
        //     456: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     459: getfield 1475	android/content/pm/ApplicationInfo:sourceDir	Ljava/lang/String;
        //     462: invokespecial 775	java/io/File:<init>	(Ljava/lang/String;)V
        //     465: astore 9
        //     467: new 574	java/io/File
        //     470: dup
        //     471: aload_1
        //     472: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     475: getfield 1478	android/content/pm/ApplicationInfo:publicSourceDir	Ljava/lang/String;
        //     478: invokespecial 775	java/io/File:<init>	(Ljava/lang/String;)V
        //     481: astore 10
        //     483: aconst_null
        //     484: astore 11
        //     486: aload_1
        //     487: invokestatic 1491	com/android/server/pm/PackageManagerService:isSystemApp	(Landroid/content/pm/PackageParser$Package;)Z
        //     490: ifne +18 -> 508
        //     493: aload_1
        //     494: aconst_null
        //     495: putfield 1988	android/content/pm/PackageParser$Package:mOriginalPackages	Ljava/util/ArrayList;
        //     498: aload_1
        //     499: aconst_null
        //     500: putfield 2425	android/content/pm/PackageParser$Package:mRealPackage	Ljava/lang/String;
        //     503: aload_1
        //     504: aconst_null
        //     505: putfield 2428	android/content/pm/PackageParser$Package:mAdoptPermissions	Ljava/util/ArrayList;
        //     508: aload_0
        //     509: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     512: astore 12
        //     514: aload 12
        //     516: monitorenter
        //     517: aload_1
        //     518: getfield 2431	android/content/pm/PackageParser$Package:usesLibraries	Ljava/util/ArrayList;
        //     521: ifnonnull +10 -> 531
        //     524: aload_1
        //     525: getfield 2434	android/content/pm/PackageParser$Package:usesOptionalLibraries	Ljava/util/ArrayList;
        //     528: ifnull +355 -> 883
        //     531: aload_0
        //     532: getfield 389	com/android/server/pm/PackageManagerService:mTmpSharedLibraries	[Ljava/lang/String;
        //     535: ifnull +18 -> 553
        //     538: aload_0
        //     539: getfield 389	com/android/server/pm/PackageManagerService:mTmpSharedLibraries	[Ljava/lang/String;
        //     542: arraylength
        //     543: aload_0
        //     544: getfield 387	com/android/server/pm/PackageManagerService:mSharedLibraries	Ljava/util/HashMap;
        //     547: invokevirtual 686	java/util/HashMap:size	()I
        //     550: if_icmpge +17 -> 567
        //     553: aload_0
        //     554: aload_0
        //     555: getfield 387	com/android/server/pm/PackageManagerService:mSharedLibraries	Ljava/util/HashMap;
        //     558: invokevirtual 686	java/util/HashMap:size	()I
        //     561: anewarray 360	java/lang/String
        //     564: putfield 389	com/android/server/pm/PackageManagerService:mTmpSharedLibraries	[Ljava/lang/String;
        //     567: iconst_0
        //     568: istore 14
        //     570: aload_1
        //     571: getfield 2431	android/content/pm/PackageParser$Package:usesLibraries	Ljava/util/ArrayList;
        //     574: ifnull +126 -> 700
        //     577: aload_1
        //     578: getfield 2431	android/content/pm/PackageParser$Package:usesLibraries	Ljava/util/ArrayList;
        //     581: invokevirtual 848	java/util/ArrayList:size	()I
        //     584: istore 15
        //     586: goto +4881 -> 5467
        //     589: iload 16
        //     591: iload 15
        //     593: if_icmpge +131 -> 724
        //     596: aload_0
        //     597: getfield 387	com/android/server/pm/PackageManagerService:mSharedLibraries	Ljava/util/HashMap;
        //     600: aload_1
        //     601: getfield 2431	android/content/pm/PackageParser$Package:usesLibraries	Ljava/util/ArrayList;
        //     604: iload 16
        //     606: invokevirtual 851	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     609: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     612: checkcast 360	java/lang/String
        //     615: astore 181
        //     617: aload 181
        //     619: ifnonnull +87 -> 706
        //     622: ldc 186
        //     624: new 662	java/lang/StringBuilder
        //     627: dup
        //     628: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     631: ldc_w 1265
        //     634: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     637: aload_1
        //     638: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     641: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     644: ldc_w 2436
        //     647: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     650: aload_1
        //     651: getfield 2431	android/content/pm/PackageParser$Package:usesLibraries	Ljava/util/ArrayList;
        //     654: iload 16
        //     656: invokevirtual 851	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     659: checkcast 360	java/lang/String
        //     662: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     665: ldc_w 2438
        //     668: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     671: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     674: invokestatic 1456	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     677: pop
        //     678: aload_0
        //     679: bipush 247
        //     681: putfield 1360	com/android/server/pm/PackageManagerService:mLastScanError	I
        //     684: aconst_null
        //     685: astore_1
        //     686: aload 12
        //     688: monitorexit
        //     689: goto -634 -> 55
        //     692: astore 13
        //     694: aload 12
        //     696: monitorexit
        //     697: aload 13
        //     699: athrow
        //     700: iconst_0
        //     701: istore 15
        //     703: goto +4764 -> 5467
        //     706: aload_0
        //     707: getfield 389	com/android/server/pm/PackageManagerService:mTmpSharedLibraries	[Ljava/lang/String;
        //     710: iload 14
        //     712: aload 181
        //     714: aastore
        //     715: iinc 14 1
        //     718: iinc 16 1
        //     721: goto -132 -> 589
        //     724: aload_1
        //     725: getfield 2434	android/content/pm/PackageParser$Package:usesOptionalLibraries	Ljava/util/ArrayList;
        //     728: ifnull +4757 -> 5485
        //     731: aload_1
        //     732: getfield 2434	android/content/pm/PackageParser$Package:usesOptionalLibraries	Ljava/util/ArrayList;
        //     735: invokevirtual 848	java/util/ArrayList:size	()I
        //     738: istore 17
        //     740: goto +4733 -> 5473
        //     743: iload 18
        //     745: iload 17
        //     747: if_icmpge +103 -> 850
        //     750: aload_0
        //     751: getfield 387	com/android/server/pm/PackageManagerService:mSharedLibraries	Ljava/util/HashMap;
        //     754: aload_1
        //     755: getfield 2434	android/content/pm/PackageParser$Package:usesOptionalLibraries	Ljava/util/ArrayList;
        //     758: iload 18
        //     760: invokevirtual 851	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     763: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     766: checkcast 360	java/lang/String
        //     769: astore 179
        //     771: aload 179
        //     773: ifnonnull +62 -> 835
        //     776: ldc 186
        //     778: new 662	java/lang/StringBuilder
        //     781: dup
        //     782: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     785: ldc_w 1265
        //     788: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     791: aload_1
        //     792: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     795: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     798: ldc_w 2440
        //     801: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     804: aload_1
        //     805: getfield 2434	android/content/pm/PackageParser$Package:usesOptionalLibraries	Ljava/util/ArrayList;
        //     808: iload 18
        //     810: invokevirtual 851	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     813: checkcast 360	java/lang/String
        //     816: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     819: ldc_w 2442
        //     822: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     825: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     828: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     831: pop
        //     832: goto +4647 -> 5479
        //     835: aload_0
        //     836: getfield 389	com/android/server/pm/PackageManagerService:mTmpSharedLibraries	[Ljava/lang/String;
        //     839: iload 14
        //     841: aload 179
        //     843: aastore
        //     844: iinc 14 1
        //     847: goto +4632 -> 5479
        //     850: iload 14
        //     852: ifle +31 -> 883
        //     855: iload 14
        //     857: anewarray 360	java/lang/String
        //     860: astore 178
        //     862: aload_1
        //     863: aload 178
        //     865: putfield 2445	android/content/pm/PackageParser$Package:usesLibraryFiles	[Ljava/lang/String;
        //     868: aload_0
        //     869: getfield 389	com/android/server/pm/PackageManagerService:mTmpSharedLibraries	[Ljava/lang/String;
        //     872: iconst_0
        //     873: aload_1
        //     874: getfield 2445	android/content/pm/PackageParser$Package:usesLibraryFiles	[Ljava/lang/String;
        //     877: iconst_0
        //     878: iload 14
        //     880: invokestatic 2449	java/lang/System:arraycopy	(Ljava/lang/Object;ILjava/lang/Object;II)V
        //     883: aload_1
        //     884: getfield 2452	android/content/pm/PackageParser$Package:mSharedUserId	Ljava/lang/String;
        //     887: ifnull +78 -> 965
        //     890: aload_0
        //     891: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     894: aload_1
        //     895: getfield 2452	android/content/pm/PackageParser$Package:mSharedUserId	Ljava/lang/String;
        //     898: aload_1
        //     899: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     902: getfield 886	android/content/pm/ApplicationInfo:flags	I
        //     905: iconst_1
        //     906: invokevirtual 2456	com/android/server/pm/Settings:getSharedUserLPw	(Ljava/lang/String;IZ)Lcom/android/server/pm/SharedUserSetting;
        //     909: astore 11
        //     911: aload 11
        //     913: ifnonnull +52 -> 965
        //     916: ldc 186
        //     918: new 662	java/lang/StringBuilder
        //     921: dup
        //     922: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     925: ldc_w 2458
        //     928: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     931: aload_1
        //     932: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     935: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     938: ldc_w 2460
        //     941: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     944: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     947: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     950: pop
        //     951: aload_0
        //     952: bipush 252
        //     954: putfield 1360	com/android/server/pm/PackageManagerService:mLastScanError	I
        //     957: aconst_null
        //     958: astore_1
        //     959: aload 12
        //     961: monitorexit
        //     962: goto -907 -> 55
        //     965: aconst_null
        //     966: astore 19
        //     968: aconst_null
        //     969: astore 20
        //     971: aload_1
        //     972: getfield 1988	android/content/pm/PackageParser$Package:mOriginalPackages	Ljava/util/ArrayList;
        //     975: ifnull +58 -> 1033
        //     978: aload_0
        //     979: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     982: getfield 1933	com/android/server/pm/Settings:mRenamedPackages	Ljava/util/HashMap;
        //     985: aload_1
        //     986: getfield 2425	android/content/pm/PackageParser$Package:mRealPackage	Ljava/lang/String;
        //     989: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     992: checkcast 360	java/lang/String
        //     995: astore 174
        //     997: aload_1
        //     998: getfield 1988	android/content/pm/PackageParser$Package:mOriginalPackages	Ljava/util/ArrayList;
        //     1001: aload 174
        //     1003: invokevirtual 1989	java/util/ArrayList:contains	(Ljava/lang/Object;)Z
        //     1006: ifeq +178 -> 1184
        //     1009: aload_1
        //     1010: getfield 2425	android/content/pm/PackageParser$Package:mRealPackage	Ljava/lang/String;
        //     1013: astore 20
        //     1015: aload_1
        //     1016: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     1019: aload 174
        //     1021: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     1024: ifne +9 -> 1033
        //     1027: aload_1
        //     1028: aload 174
        //     1030: invokevirtual 1992	android/content/pm/PackageParser$Package:setPackageName	(Ljava/lang/String;)V
        //     1033: aload_0
        //     1034: getfield 414	com/android/server/pm/PackageManagerService:mTransferedPackages	Ljava/util/HashSet;
        //     1037: aload_1
        //     1038: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     1041: invokevirtual 721	java/util/HashSet:contains	(Ljava/lang/Object;)Z
        //     1044: ifeq +38 -> 1082
        //     1047: ldc 186
        //     1049: new 662	java/lang/StringBuilder
        //     1052: dup
        //     1053: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     1056: ldc_w 1265
        //     1059: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1062: aload_1
        //     1063: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     1066: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1069: ldc_w 2462
        //     1072: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1075: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1078: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     1081: pop
        //     1082: aload_0
        //     1083: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     1086: astore 21
        //     1088: aload_1
        //     1089: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     1092: getfield 1481	android/content/pm/ApplicationInfo:nativeLibraryDir	Ljava/lang/String;
        //     1095: astore 22
        //     1097: aload_1
        //     1098: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     1101: getfield 886	android/content/pm/ApplicationInfo:flags	I
        //     1104: istore 23
        //     1106: aload 21
        //     1108: aload_1
        //     1109: aload 19
        //     1111: aload 20
        //     1113: aload 11
        //     1115: aload 9
        //     1117: aload 10
        //     1119: aload 22
        //     1121: iload 23
        //     1123: iconst_1
        //     1124: iconst_0
        //     1125: invokevirtual 2466	com/android/server/pm/Settings:getPackageLPw	(Landroid/content/pm/PackageParser$Package;Lcom/android/server/pm/PackageSetting;Ljava/lang/String;Lcom/android/server/pm/SharedUserSetting;Ljava/io/File;Ljava/io/File;Ljava/lang/String;IZZ)Lcom/android/server/pm/PackageSetting;
        //     1128: astore 24
        //     1130: aload 24
        //     1132: ifnonnull +216 -> 1348
        //     1135: ldc 186
        //     1137: new 662	java/lang/StringBuilder
        //     1140: dup
        //     1141: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     1144: ldc_w 2458
        //     1147: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1150: aload_1
        //     1151: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     1154: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1157: ldc_w 2468
        //     1160: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1163: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1166: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     1169: pop
        //     1170: aload_0
        //     1171: bipush 252
        //     1173: putfield 1360	com/android/server/pm/PackageManagerService:mLastScanError	I
        //     1176: aconst_null
        //     1177: astore_1
        //     1178: aload 12
        //     1180: monitorexit
        //     1181: goto -1126 -> 55
        //     1184: bipush 255
        //     1186: aload_1
        //     1187: getfield 1988	android/content/pm/PackageParser$Package:mOriginalPackages	Ljava/util/ArrayList;
        //     1190: invokevirtual 848	java/util/ArrayList:size	()I
        //     1193: iadd
        //     1194: istore 175
        //     1196: iload 175
        //     1198: iflt -165 -> 1033
        //     1201: aload_0
        //     1202: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     1205: aload_1
        //     1206: getfield 1988	android/content/pm/PackageParser$Package:mOriginalPackages	Ljava/util/ArrayList;
        //     1209: iload 175
        //     1211: invokevirtual 851	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     1214: checkcast 360	java/lang/String
        //     1217: invokevirtual 2471	com/android/server/pm/Settings:peekPackageLPr	(Ljava/lang/String;)Lcom/android/server/pm/PackageSetting;
        //     1220: astore 19
        //     1222: aload 19
        //     1224: ifnull +4267 -> 5491
        //     1227: aload_0
        //     1228: aload 19
        //     1230: aload_1
        //     1231: invokespecial 2475	com/android/server/pm/PackageManagerService:verifyPackageUpdateLPr	(Lcom/android/server/pm/PackageSetting;Landroid/content/pm/PackageParser$Package;)Z
        //     1234: ifne +9 -> 1243
        //     1237: aconst_null
        //     1238: astore 19
        //     1240: goto +4251 -> 5491
        //     1243: aload 19
        //     1245: getfield 1831	com/android/server/pm/PackageSetting:sharedUser	Lcom/android/server/pm/SharedUserSetting;
        //     1248: ifnull -215 -> 1033
        //     1251: aload 19
        //     1253: getfield 1831	com/android/server/pm/PackageSetting:sharedUser	Lcom/android/server/pm/SharedUserSetting;
        //     1256: getfield 2476	com/android/server/pm/SharedUserSetting:name	Ljava/lang/String;
        //     1259: aload_1
        //     1260: getfield 2452	android/content/pm/PackageParser$Package:mSharedUserId	Ljava/lang/String;
        //     1263: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     1266: ifne -233 -> 1033
        //     1269: ldc 186
        //     1271: new 662	java/lang/StringBuilder
        //     1274: dup
        //     1275: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     1278: ldc_w 2478
        //     1281: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1284: aload 19
        //     1286: getfield 795	com/android/server/pm/PackageSettingBase:name	Ljava/lang/String;
        //     1289: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1292: ldc_w 904
        //     1295: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1298: aload_1
        //     1299: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     1302: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1305: ldc_w 2480
        //     1308: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1311: aload 19
        //     1313: getfield 1831	com/android/server/pm/PackageSetting:sharedUser	Lcom/android/server/pm/SharedUserSetting;
        //     1316: getfield 2476	com/android/server/pm/SharedUserSetting:name	Ljava/lang/String;
        //     1319: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1322: ldc_w 2482
        //     1325: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1328: aload_1
        //     1329: getfield 2452	android/content/pm/PackageParser$Package:mSharedUserId	Ljava/lang/String;
        //     1332: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1335: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1338: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     1341: pop
        //     1342: aconst_null
        //     1343: astore 19
        //     1345: goto +4146 -> 5491
        //     1348: aload_1
        //     1349: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     1352: astore 25
        //     1354: aload 25
        //     1356: aload 25
        //     1358: getfield 886	android/content/pm/ApplicationInfo:flags	I
        //     1361: ldc_w 2483
        //     1364: aload 24
        //     1366: getfield 790	com/android/server/pm/GrantedPermissions:pkgFlags	I
        //     1369: iand
        //     1370: ior
        //     1371: putfield 886	android/content/pm/ApplicationInfo:flags	I
        //     1374: aload 24
        //     1376: getfield 2486	com/android/server/pm/PackageSettingBase:origPackage	Lcom/android/server/pm/PackageSettingBase;
        //     1379: ifnull +77 -> 1456
        //     1382: aload 19
        //     1384: getfield 795	com/android/server/pm/PackageSettingBase:name	Ljava/lang/String;
        //     1387: astore 170
        //     1389: aload_1
        //     1390: aload 170
        //     1392: invokevirtual 1992	android/content/pm/PackageParser$Package:setPackageName	(Ljava/lang/String;)V
        //     1395: iconst_5
        //     1396: new 662	java/lang/StringBuilder
        //     1399: dup
        //     1400: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     1403: ldc_w 2488
        //     1406: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1409: aload 24
        //     1411: getfield 2491	com/android/server/pm/PackageSettingBase:realName	Ljava/lang/String;
        //     1414: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1417: ldc_w 2493
        //     1420: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1423: aload 24
        //     1425: getfield 795	com/android/server/pm/PackageSettingBase:name	Ljava/lang/String;
        //     1428: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1431: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1434: invokestatic 822	com/android/server/pm/PackageManagerService:reportSettingsProblem	(ILjava/lang/String;)V
        //     1437: aload_0
        //     1438: getfield 414	com/android/server/pm/PackageManagerService:mTransferedPackages	Ljava/util/HashSet;
        //     1441: aload 19
        //     1443: getfield 795	com/android/server/pm/PackageSettingBase:name	Ljava/lang/String;
        //     1446: invokevirtual 650	java/util/HashSet:add	(Ljava/lang/Object;)Z
        //     1449: pop
        //     1450: aload 24
        //     1452: aconst_null
        //     1453: putfield 2486	com/android/server/pm/PackageSettingBase:origPackage	Lcom/android/server/pm/PackageSettingBase;
        //     1456: aload 20
        //     1458: ifnull +15 -> 1473
        //     1461: aload_0
        //     1462: getfield 414	com/android/server/pm/PackageManagerService:mTransferedPackages	Ljava/util/HashSet;
        //     1465: aload_1
        //     1466: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     1469: invokevirtual 650	java/util/HashSet:add	(Ljava/lang/Object;)Z
        //     1472: pop
        //     1473: aload_0
        //     1474: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     1477: aload_1
        //     1478: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     1481: invokevirtual 803	com/android/server/pm/Settings:isDisabledSystemPackageLPr	(Ljava/lang/String;)Z
        //     1484: ifeq +23 -> 1507
        //     1487: aload_1
        //     1488: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     1491: astore 168
        //     1493: aload 168
        //     1495: sipush 128
        //     1498: aload 168
        //     1500: getfield 886	android/content/pm/ApplicationInfo:flags	I
        //     1503: ior
        //     1504: putfield 886	android/content/pm/ApplicationInfo:flags	I
        //     1507: aload_1
        //     1508: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     1511: aload 24
        //     1513: getfield 2496	com/android/server/pm/PackageSetting:appId	I
        //     1516: putfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     1519: aload_1
        //     1520: aload 24
        //     1522: putfield 1827	android/content/pm/PackageParser$Package:mExtras	Ljava/lang/Object;
        //     1525: aload_0
        //     1526: aload 24
        //     1528: aload_1
        //     1529: invokespecial 2499	com/android/server/pm/PackageManagerService:verifySignaturesLP	(Lcom/android/server/pm/PackageSetting;Landroid/content/pm/PackageParser$Package;)Z
        //     1532: ifne +136 -> 1668
        //     1535: iload_2
        //     1536: bipush 64
        //     1538: iand
        //     1539: ifne +11 -> 1550
        //     1542: aconst_null
        //     1543: astore_1
        //     1544: aload 12
        //     1546: monitorexit
        //     1547: goto -1492 -> 55
        //     1550: aload 24
        //     1552: getfield 1338	com/android/server/pm/PackageSettingBase:signatures	Lcom/android/server/pm/PackageSignatures;
        //     1555: aload_1
        //     1556: getfield 1345	android/content/pm/PackageParser$Package:mSignatures	[Landroid/content/pm/Signature;
        //     1559: putfield 1344	com/android/server/pm/PackageSignatures:mSignatures	[Landroid/content/pm/Signature;
        //     1562: aload 24
        //     1564: getfield 1831	com/android/server/pm/PackageSetting:sharedUser	Lcom/android/server/pm/SharedUserSetting;
        //     1567: ifnull +68 -> 1635
        //     1570: aload 24
        //     1572: getfield 1831	com/android/server/pm/PackageSetting:sharedUser	Lcom/android/server/pm/SharedUserSetting;
        //     1575: getfield 2500	com/android/server/pm/SharedUserSetting:signatures	Lcom/android/server/pm/PackageSignatures;
        //     1578: getfield 1344	com/android/server/pm/PackageSignatures:mSignatures	[Landroid/content/pm/Signature;
        //     1581: aload_1
        //     1582: getfield 1345	android/content/pm/PackageParser$Package:mSignatures	[Landroid/content/pm/Signature;
        //     1585: invokestatic 1874	com/android/server/pm/PackageManagerService:compareSignatures	([Landroid/content/pm/Signature;[Landroid/content/pm/Signature;)I
        //     1588: ifeq +47 -> 1635
        //     1591: ldc 186
        //     1593: new 662	java/lang/StringBuilder
        //     1596: dup
        //     1597: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     1600: ldc_w 2502
        //     1603: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1606: aload 24
        //     1608: getfield 1831	com/android/server/pm/PackageSetting:sharedUser	Lcom/android/server/pm/SharedUserSetting;
        //     1611: invokevirtual 944	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     1614: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1617: invokestatic 2098	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     1620: pop
        //     1621: aload_0
        //     1622: bipush 152
        //     1624: putfield 1360	com/android/server/pm/PackageManagerService:mLastScanError	I
        //     1627: aconst_null
        //     1628: astore_1
        //     1629: aload 12
        //     1631: monitorexit
        //     1632: goto -1577 -> 55
        //     1635: iconst_5
        //     1636: new 662	java/lang/StringBuilder
        //     1639: dup
        //     1640: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     1643: ldc_w 816
        //     1646: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1649: aload_1
        //     1650: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     1653: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1656: ldc_w 2504
        //     1659: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1662: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1665: invokestatic 822	com/android/server/pm/PackageManagerService:reportSettingsProblem	(ILjava/lang/String;)V
        //     1668: iload_3
        //     1669: bipush 16
        //     1671: iand
        //     1672: ifeq +208 -> 1880
        //     1675: aload_1
        //     1676: getfield 2507	android/content/pm/PackageParser$Package:providers	Ljava/util/ArrayList;
        //     1679: invokevirtual 848	java/util/ArrayList:size	()I
        //     1682: istore 157
        //     1684: iconst_0
        //     1685: istore 158
        //     1687: iload 158
        //     1689: iload 157
        //     1691: if_icmpge +189 -> 1880
        //     1694: aload_1
        //     1695: getfield 2507	android/content/pm/PackageParser$Package:providers	Ljava/util/ArrayList;
        //     1698: iload 158
        //     1700: invokevirtual 851	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     1703: checkcast 2509	android/content/pm/PackageParser$Provider
        //     1706: astore 159
        //     1708: aload 159
        //     1710: getfield 2512	android/content/pm/PackageParser$Provider:info	Landroid/content/pm/ProviderInfo;
        //     1713: getfield 2517	android/content/pm/ProviderInfo:authority	Ljava/lang/String;
        //     1716: ifnull +3795 -> 5511
        //     1719: aload 159
        //     1721: getfield 2512	android/content/pm/PackageParser$Provider:info	Landroid/content/pm/ProviderInfo;
        //     1724: getfield 2517	android/content/pm/ProviderInfo:authority	Ljava/lang/String;
        //     1727: ldc_w 2519
        //     1730: invokevirtual 660	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
        //     1733: astore 160
        //     1735: iconst_0
        //     1736: istore 161
        //     1738: aload 160
        //     1740: arraylength
        //     1741: istore 162
        //     1743: iload 161
        //     1745: iload 162
        //     1747: if_icmpge +3764 -> 5511
        //     1750: aload_0
        //     1751: getfield 405	com/android/server/pm/PackageManagerService:mProviders	Ljava/util/HashMap;
        //     1754: aload 160
        //     1756: iload 161
        //     1758: aaload
        //     1759: invokevirtual 1936	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
        //     1762: ifeq +3743 -> 5505
        //     1765: aload_0
        //     1766: getfield 405	com/android/server/pm/PackageManagerService:mProviders	Ljava/util/HashMap;
        //     1769: aload 160
        //     1771: iload 161
        //     1773: aaload
        //     1774: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     1777: checkcast 2509	android/content/pm/PackageParser$Provider
        //     1780: astore 163
        //     1782: new 662	java/lang/StringBuilder
        //     1785: dup
        //     1786: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     1789: ldc_w 2521
        //     1792: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1795: aload 160
        //     1797: iload 161
        //     1799: aaload
        //     1800: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1803: ldc_w 2523
        //     1806: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1809: aload_1
        //     1810: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     1813: getfield 1626	android/content/pm/ApplicationInfo:packageName	Ljava/lang/String;
        //     1816: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1819: ldc_w 2525
        //     1822: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1825: astore 164
        //     1827: aload 163
        //     1829: ifnull +3668 -> 5497
        //     1832: aload 163
        //     1834: invokevirtual 2529	android/content/pm/PackageParser$Provider:getComponentName	()Landroid/content/ComponentName;
        //     1837: ifnull +3660 -> 5497
        //     1840: aload 163
        //     1842: invokevirtual 2529	android/content/pm/PackageParser$Provider:getComponentName	()Landroid/content/ComponentName;
        //     1845: invokevirtual 2532	android/content/ComponentName:getPackageName	()Ljava/lang/String;
        //     1848: astore 165
        //     1850: ldc 186
        //     1852: aload 164
        //     1854: aload 165
        //     1856: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1859: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1862: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     1865: pop
        //     1866: aload_0
        //     1867: bipush 243
        //     1869: putfield 1360	com/android/server/pm/PackageManagerService:mLastScanError	I
        //     1872: aconst_null
        //     1873: astore_1
        //     1874: aload 12
        //     1876: monitorexit
        //     1877: goto -1822 -> 55
        //     1880: aload_1
        //     1881: getfield 2428	android/content/pm/PackageParser$Package:mAdoptPermissions	Ljava/util/ArrayList;
        //     1884: ifnull +116 -> 2000
        //     1887: bipush 255
        //     1889: aload_1
        //     1890: getfield 2428	android/content/pm/PackageParser$Package:mAdoptPermissions	Ljava/util/ArrayList;
        //     1893: invokevirtual 848	java/util/ArrayList:size	()I
        //     1896: iadd
        //     1897: istore 153
        //     1899: iload 153
        //     1901: iflt +99 -> 2000
        //     1904: aload_1
        //     1905: getfield 2428	android/content/pm/PackageParser$Package:mAdoptPermissions	Ljava/util/ArrayList;
        //     1908: iload 153
        //     1910: invokevirtual 851	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     1913: checkcast 360	java/lang/String
        //     1916: astore 154
        //     1918: aload_0
        //     1919: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     1922: aload 154
        //     1924: invokevirtual 2471	com/android/server/pm/Settings:peekPackageLPr	(Ljava/lang/String;)Lcom/android/server/pm/PackageSetting;
        //     1927: astore 155
        //     1929: aload 155
        //     1931: ifnull +3586 -> 5517
        //     1934: aload_0
        //     1935: aload 155
        //     1937: aload_1
        //     1938: invokespecial 2475	com/android/server/pm/PackageManagerService:verifyPackageUpdateLPr	(Lcom/android/server/pm/PackageSetting;Landroid/content/pm/PackageParser$Package;)Z
        //     1941: ifeq +3576 -> 5517
        //     1944: ldc 186
        //     1946: new 662	java/lang/StringBuilder
        //     1949: dup
        //     1950: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     1953: ldc_w 2534
        //     1956: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1959: aload 154
        //     1961: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1964: ldc_w 904
        //     1967: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1970: aload_1
        //     1971: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     1974: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1977: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1980: invokestatic 747	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     1983: pop
        //     1984: aload_0
        //     1985: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     1988: aload 154
        //     1990: aload_1
        //     1991: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     1994: invokevirtual 2537	com/android/server/pm/Settings:transferPermissionsLPw	(Ljava/lang/String;Ljava/lang/String;)V
        //     1997: goto +3520 -> 5517
        //     2000: aload 12
        //     2002: monitorexit
        //     2003: aload_1
        //     2004: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     2007: astore 26
        //     2009: aload 6
        //     2011: invokevirtual 1334	java/io/File:lastModified	()J
        //     2014: lstore 27
        //     2016: iload_3
        //     2017: iconst_4
        //     2018: iand
        //     2019: ifeq +213 -> 2232
        //     2022: iconst_1
        //     2023: istore 29
        //     2025: aload_1
        //     2026: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2029: aload_1
        //     2030: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2033: getfield 1626	android/content/pm/ApplicationInfo:packageName	Ljava/lang/String;
        //     2036: aload_1
        //     2037: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2040: getfield 2398	android/content/pm/ApplicationInfo:processName	Ljava/lang/String;
        //     2043: aload_1
        //     2044: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2047: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     2050: invokestatic 2539	com/android/server/pm/PackageManagerService:fixProcessName	(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
        //     2053: putfield 2398	android/content/pm/ApplicationInfo:processName	Ljava/lang/String;
        //     2056: aload_0
        //     2057: getfield 1876	com/android/server/pm/PackageManagerService:mPlatformPackage	Landroid/content/pm/PackageParser$Package;
        //     2060: aload_1
        //     2061: if_acmpne +177 -> 2238
        //     2064: invokestatic 572	android/os/Environment:getDataDirectory	()Ljava/io/File;
        //     2067: astore 152
        //     2069: new 574	java/io/File
        //     2072: dup
        //     2073: aload 152
        //     2075: ldc_w 1760
        //     2078: invokespecial 579	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     2081: astore 30
        //     2083: aload_1
        //     2084: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2087: aload 30
        //     2089: invokevirtual 587	java/io/File:getPath	()Ljava/lang/String;
        //     2092: putfield 1639	android/content/pm/ApplicationInfo:dataDir	Ljava/lang/String;
        //     2095: aload 6
        //     2097: invokevirtual 587	java/io/File:getPath	()Ljava/lang/String;
        //     2100: astore 33
        //     2102: aload_1
        //     2103: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2106: getfield 1481	android/content/pm/ApplicationInfo:nativeLibraryDir	Ljava/lang/String;
        //     2109: ifnull +75 -> 2184
        //     2112: new 574	java/io/File
        //     2115: dup
        //     2116: aload_1
        //     2117: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2120: getfield 1481	android/content/pm/ApplicationInfo:nativeLibraryDir	Ljava/lang/String;
        //     2123: invokespecial 775	java/io/File:<init>	(Ljava/lang/String;)V
        //     2126: astore 130
        //     2128: aload 30
        //     2130: invokevirtual 1445	java/io/File:getCanonicalPath	()Ljava/lang/String;
        //     2133: astore 133
        //     2135: aload_1
        //     2136: invokestatic 1491	com/android/server/pm/PackageManagerService:isSystemApp	(Landroid/content/pm/PackageParser$Package;)Z
        //     2139: ifeq +881 -> 3020
        //     2142: aload_1
        //     2143: invokestatic 1885	com/android/server/pm/PackageManagerService:isUpdatedSystemApp	(Landroid/content/pm/PackageParser$Package;)Z
        //     2146: ifne +874 -> 3020
        //     2149: aload 130
        //     2151: invokestatic 2543	com/android/internal/content/NativeLibraryHelper:removeNativeBinariesFromDirLI	(Ljava/io/File;)Z
        //     2154: ifeq +30 -> 2184
        //     2157: ldc 186
        //     2159: new 662	java/lang/StringBuilder
        //     2162: dup
        //     2163: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     2166: ldc_w 2545
        //     2169: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2172: aload 33
        //     2174: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2177: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     2180: invokestatic 1365	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     2183: pop
        //     2184: aload_1
        //     2185: aload 33
        //     2187: putfield 2126	android/content/pm/PackageParser$Package:mScanPath	Ljava/lang/String;
        //     2190: iload_3
        //     2191: iconst_2
        //     2192: iand
        //     2193: ifne +994 -> 3187
        //     2196: iload_3
        //     2197: sipush 128
        //     2200: iand
        //     2201: ifeq +980 -> 3181
        //     2204: iconst_1
        //     2205: istore 129
        //     2207: aload_0
        //     2208: aload_1
        //     2209: iload 29
        //     2211: iload 129
        //     2213: invokespecial 2547	com/android/server/pm/PackageManagerService:performDexOptLI	(Landroid/content/pm/PackageParser$Package;ZZ)I
        //     2216: bipush 255
        //     2218: if_icmpne +969 -> 3187
        //     2221: aload_0
        //     2222: bipush 245
        //     2224: putfield 1360	com/android/server/pm/PackageManagerService:mLastScanError	I
        //     2227: aconst_null
        //     2228: astore_1
        //     2229: goto -2174 -> 55
        //     2232: iconst_0
        //     2233: istore 29
        //     2235: goto -210 -> 2025
        //     2238: aload_0
        //     2239: aload_1
        //     2240: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     2243: iconst_0
        //     2244: invokespecial 1636	com/android/server/pm/PackageManagerService:getDataPathForPackage	(Ljava/lang/String;I)Ljava/io/File;
        //     2247: astore 30
        //     2249: iconst_0
        //     2250: istore 31
        //     2252: aload 30
        //     2254: invokevirtual 838	java/io/File:exists	()Z
        //     2257: ifeq +635 -> 2892
        //     2260: aload_0
        //     2261: getfield 378	com/android/server/pm/PackageManagerService:mOutPermissions	[I
        //     2264: iconst_1
        //     2265: iconst_0
        //     2266: iastore
        //     2267: aload 30
        //     2269: invokevirtual 587	java/io/File:getPath	()Ljava/lang/String;
        //     2272: aload_0
        //     2273: getfield 378	com/android/server/pm/PackageManagerService:mOutPermissions	[I
        //     2276: invokestatic 2551	android/os/FileUtils:getPermissions	(Ljava/lang/String;[I)I
        //     2279: pop
        //     2280: aload_0
        //     2281: getfield 378	com/android/server/pm/PackageManagerService:mOutPermissions	[I
        //     2284: iconst_1
        //     2285: iaload
        //     2286: aload_1
        //     2287: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2290: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     2293: if_icmpeq +493 -> 2786
        //     2296: iconst_0
        //     2297: istore 145
        //     2299: aload_0
        //     2300: getfield 378	com/android/server/pm/PackageManagerService:mOutPermissions	[I
        //     2303: iconst_1
        //     2304: iaload
        //     2305: ifne +75 -> 2380
        //     2308: aload_0
        //     2309: getfield 534	com/android/server/pm/PackageManagerService:mInstaller	Lcom/android/server/pm/Installer;
        //     2312: aload 26
        //     2314: aload_1
        //     2315: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2318: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     2321: aload_1
        //     2322: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2325: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     2328: invokevirtual 2555	com/android/server/pm/Installer:fixUid	(Ljava/lang/String;II)I
        //     2331: iflt +49 -> 2380
        //     2334: iconst_1
        //     2335: istore 145
        //     2337: iconst_5
        //     2338: new 662	java/lang/StringBuilder
        //     2341: dup
        //     2342: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     2345: ldc_w 1265
        //     2348: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2351: aload_1
        //     2352: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     2355: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2358: ldc_w 2557
        //     2361: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2364: aload_1
        //     2365: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2368: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     2371: invokevirtual 902	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     2374: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     2377: invokestatic 822	com/android/server/pm/PackageManagerService:reportSettingsProblem	(ILjava/lang/String;)V
        //     2380: iload 145
        //     2382: ifne +486 -> 2868
        //     2385: iload_2
        //     2386: iconst_1
        //     2387: iand
        //     2388: ifne +11 -> 2399
        //     2391: iload_3
        //     2392: sipush 256
        //     2395: iand
        //     2396: ifeq +472 -> 2868
        //     2399: aload_0
        //     2400: getfield 534	com/android/server/pm/PackageManagerService:mInstaller	Lcom/android/server/pm/Installer;
        //     2403: aload 26
        //     2405: iconst_0
        //     2406: invokevirtual 825	com/android/server/pm/Installer:remove	(Ljava/lang/String;I)I
        //     2409: iflt +182 -> 2591
        //     2412: getstatic 604	com/android/server/pm/PackageManagerService:sUserManager	Lcom/android/server/pm/UserManager;
        //     2415: aload 26
        //     2417: invokevirtual 828	com/android/server/pm/UserManager:removePackageForAllUsers	(Ljava/lang/String;)V
        //     2420: iload_2
        //     2421: iconst_1
        //     2422: iand
        //     2423: ifeq +145 -> 2568
        //     2426: ldc_w 816
        //     2429: astore 151
        //     2431: iconst_5
        //     2432: new 662	java/lang/StringBuilder
        //     2435: dup
        //     2436: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     2439: aload 151
        //     2441: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2444: aload_1
        //     2445: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     2448: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2451: ldc_w 2559
        //     2454: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2457: aload_0
        //     2458: getfield 378	com/android/server/pm/PackageManagerService:mOutPermissions	[I
        //     2461: iconst_1
        //     2462: iaload
        //     2463: invokevirtual 902	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     2466: ldc_w 904
        //     2469: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2472: aload_1
        //     2473: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2476: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     2479: invokevirtual 902	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     2482: ldc_w 2561
        //     2485: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2488: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     2491: invokestatic 822	com/android/server/pm/PackageManagerService:reportSettingsProblem	(ILjava/lang/String;)V
        //     2494: iconst_1
        //     2495: istore 145
        //     2497: aload_0
        //     2498: getfield 534	com/android/server/pm/PackageManagerService:mInstaller	Lcom/android/server/pm/Installer;
        //     2501: aload 26
        //     2503: aload_1
        //     2504: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2507: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     2510: aload_1
        //     2511: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2514: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     2517: invokevirtual 2564	com/android/server/pm/Installer:install	(Ljava/lang/String;II)I
        //     2520: bipush 255
        //     2522: if_icmpne +54 -> 2576
        //     2525: iconst_5
        //     2526: new 662	java/lang/StringBuilder
        //     2529: dup
        //     2530: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     2533: aload 151
        //     2535: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2538: aload_1
        //     2539: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     2542: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2545: ldc_w 2566
        //     2548: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2551: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     2554: invokestatic 822	com/android/server/pm/PackageManagerService:reportSettingsProblem	(ILjava/lang/String;)V
        //     2557: aload_0
        //     2558: bipush 252
        //     2560: putfield 1360	com/android/server/pm/PackageManagerService:mLastScanError	I
        //     2563: aconst_null
        //     2564: astore_1
        //     2565: goto -2510 -> 55
        //     2568: ldc_w 2568
        //     2571: astore 151
        //     2573: goto -142 -> 2431
        //     2576: getstatic 604	com/android/server/pm/PackageManagerService:sUserManager	Lcom/android/server/pm/UserManager;
        //     2579: aload 26
        //     2581: aload_1
        //     2582: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2585: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     2588: invokevirtual 2571	com/android/server/pm/UserManager:installPackageForAllUsers	(Ljava/lang/String;I)V
        //     2591: iload 145
        //     2593: ifne +8 -> 2601
        //     2596: aload_0
        //     2597: iconst_1
        //     2598: putfield 2573	com/android/server/pm/PackageManagerService:mHasSystemUidErrors	Z
        //     2601: iload 145
        //     2603: ifne +183 -> 2786
        //     2606: aload_1
        //     2607: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2610: new 662	java/lang/StringBuilder
        //     2613: dup
        //     2614: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     2617: ldc_w 2575
        //     2620: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2623: aload_1
        //     2624: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2627: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     2630: invokevirtual 902	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     2633: ldc_w 2577
        //     2636: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2639: aload_0
        //     2640: getfield 378	com/android/server/pm/PackageManagerService:mOutPermissions	[I
        //     2643: iconst_1
        //     2644: iaload
        //     2645: invokevirtual 902	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     2648: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     2651: putfield 1639	android/content/pm/ApplicationInfo:dataDir	Ljava/lang/String;
        //     2654: aload_1
        //     2655: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2658: aload_1
        //     2659: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2662: getfield 1639	android/content/pm/ApplicationInfo:dataDir	Ljava/lang/String;
        //     2665: putfield 1481	android/content/pm/ApplicationInfo:nativeLibraryDir	Ljava/lang/String;
        //     2668: new 662	java/lang/StringBuilder
        //     2671: dup
        //     2672: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     2675: ldc_w 1265
        //     2678: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2681: aload_1
        //     2682: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     2685: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2688: ldc_w 2579
        //     2691: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2694: aload_0
        //     2695: getfield 378	com/android/server/pm/PackageManagerService:mOutPermissions	[I
        //     2698: iconst_1
        //     2699: iaload
        //     2700: invokevirtual 902	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     2703: ldc_w 2581
        //     2706: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2709: aload_1
        //     2710: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2713: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     2716: invokevirtual 902	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     2719: ldc_w 2583
        //     2722: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2725: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     2728: astore 146
        //     2730: aload_0
        //     2731: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     2734: astore 147
        //     2736: aload 147
        //     2738: monitorenter
        //     2739: aload_0
        //     2740: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     2743: getfield 2587	com/android/server/pm/Settings:mReadMessages	Ljava/lang/StringBuilder;
        //     2746: aload 146
        //     2748: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2751: pop
        //     2752: aload_0
        //     2753: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     2756: getfield 2587	com/android/server/pm/Settings:mReadMessages	Ljava/lang/StringBuilder;
        //     2759: bipush 10
        //     2761: invokevirtual 2590	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
        //     2764: pop
        //     2765: iconst_1
        //     2766: istore 31
        //     2768: aload 24
        //     2770: getfield 2593	com/android/server/pm/PackageSettingBase:uidError	Z
        //     2773: ifne +10 -> 2783
        //     2776: bipush 6
        //     2778: aload 146
        //     2780: invokestatic 822	com/android/server/pm/PackageManagerService:reportSettingsProblem	(ILjava/lang/String;)V
        //     2783: aload 147
        //     2785: monitorexit
        //     2786: aload_1
        //     2787: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2790: aload 30
        //     2792: invokevirtual 587	java/io/File:getPath	()Ljava/lang/String;
        //     2795: putfield 1639	android/content/pm/ApplicationInfo:dataDir	Ljava/lang/String;
        //     2798: aload_1
        //     2799: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2802: getfield 1481	android/content/pm/ApplicationInfo:nativeLibraryDir	Ljava/lang/String;
        //     2805: ifnonnull +53 -> 2858
        //     2808: aload_1
        //     2809: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2812: getfield 1639	android/content/pm/ApplicationInfo:dataDir	Ljava/lang/String;
        //     2815: ifnull +43 -> 2858
        //     2818: aload 24
        //     2820: getfield 1642	com/android/server/pm/PackageSettingBase:nativeLibraryPathString	Ljava/lang/String;
        //     2823: ifnonnull +182 -> 3005
        //     2826: new 574	java/io/File
        //     2829: dup
        //     2830: aload 30
        //     2832: ldc 134
        //     2834: invokespecial 579	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     2837: invokevirtual 587	java/io/File:getPath	()Ljava/lang/String;
        //     2840: astore 143
        //     2842: aload_1
        //     2843: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2846: aload 143
        //     2848: putfield 1481	android/content/pm/ApplicationInfo:nativeLibraryDir	Ljava/lang/String;
        //     2851: aload 24
        //     2853: aload 143
        //     2855: putfield 1642	com/android/server/pm/PackageSettingBase:nativeLibraryPathString	Ljava/lang/String;
        //     2858: aload 24
        //     2860: iload 31
        //     2862: putfield 2593	com/android/server/pm/PackageSettingBase:uidError	Z
        //     2865: goto -770 -> 2095
        //     2868: iload 145
        //     2870: ifne -269 -> 2601
        //     2873: aload_0
        //     2874: bipush 232
        //     2876: putfield 1360	com/android/server/pm/PackageManagerService:mLastScanError	I
        //     2879: aconst_null
        //     2880: astore_1
        //     2881: goto -2826 -> 55
        //     2884: astore 148
        //     2886: aload 147
        //     2888: monitorexit
        //     2889: aload 148
        //     2891: athrow
        //     2892: aload_0
        //     2893: getfield 534	com/android/server/pm/PackageManagerService:mInstaller	Lcom/android/server/pm/Installer;
        //     2896: aload 26
        //     2898: aload_1
        //     2899: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2902: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     2905: aload_1
        //     2906: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2909: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     2912: invokevirtual 2564	com/android/server/pm/Installer:install	(Ljava/lang/String;II)I
        //     2915: ifge +14 -> 2929
        //     2918: aload_0
        //     2919: bipush 252
        //     2921: putfield 1360	com/android/server/pm/PackageManagerService:mLastScanError	I
        //     2924: aconst_null
        //     2925: astore_1
        //     2926: goto -2871 -> 55
        //     2929: getstatic 604	com/android/server/pm/PackageManagerService:sUserManager	Lcom/android/server/pm/UserManager;
        //     2932: aload 26
        //     2934: aload_1
        //     2935: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2938: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     2941: invokevirtual 2571	com/android/server/pm/UserManager:installPackageForAllUsers	(Ljava/lang/String;I)V
        //     2944: aload 30
        //     2946: invokevirtual 838	java/io/File:exists	()Z
        //     2949: ifeq +18 -> 2967
        //     2952: aload_1
        //     2953: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2956: aload 30
        //     2958: invokevirtual 587	java/io/File:getPath	()Ljava/lang/String;
        //     2961: putfield 1639	android/content/pm/ApplicationInfo:dataDir	Ljava/lang/String;
        //     2964: goto -166 -> 2798
        //     2967: ldc 186
        //     2969: new 662	java/lang/StringBuilder
        //     2972: dup
        //     2973: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     2976: ldc_w 2595
        //     2979: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     2982: aload 30
        //     2984: invokevirtual 944	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     2987: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     2990: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     2993: pop
        //     2994: aload_1
        //     2995: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     2998: aconst_null
        //     2999: putfield 1639	android/content/pm/ApplicationInfo:dataDir	Ljava/lang/String;
        //     3002: goto -204 -> 2798
        //     3005: aload_1
        //     3006: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     3009: aload 24
        //     3011: getfield 1642	com/android/server/pm/PackageSettingBase:nativeLibraryPathString	Ljava/lang/String;
        //     3014: putfield 1481	android/content/pm/ApplicationInfo:nativeLibraryDir	Ljava/lang/String;
        //     3017: goto -159 -> 2858
        //     3020: aload 130
        //     3022: invokevirtual 2598	java/io/File:getParentFile	()Ljava/io/File;
        //     3025: invokevirtual 1445	java/io/File:getCanonicalPath	()Ljava/lang/String;
        //     3028: aload 133
        //     3030: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     3033: istore 134
        //     3035: iload 134
        //     3037: ifeq +97 -> 3134
        //     3040: getstatic 2604	libcore/io/Libcore:os	Llibcore/io/Os;
        //     3043: aload 130
        //     3045: invokevirtual 587	java/io/File:getPath	()Ljava/lang/String;
        //     3048: invokeinterface 2610 2 0
        //     3053: getfield 2615	libcore/io/StructStat:st_mode	I
        //     3056: invokestatic 2620	libcore/io/OsConstants:S_ISLNK	(I)Z
        //     3059: istore 141
        //     3061: iload 141
        //     3063: istore 138
        //     3065: iload 138
        //     3067: ifeq +13 -> 3080
        //     3070: aload_0
        //     3071: getfield 534	com/android/server/pm/PackageManagerService:mInstaller	Lcom/android/server/pm/Installer;
        //     3074: aload 133
        //     3076: invokevirtual 2623	com/android/server/pm/Installer:unlinkNativeLibraryDirectory	(Ljava/lang/String;)I
        //     3079: pop
        //     3080: aload 6
        //     3082: aload 130
        //     3084: invokestatic 2627	com/android/internal/content/NativeLibraryHelper:copyNativeBinariesIfNeededLI	(Ljava/io/File;Ljava/io/File;)I
        //     3087: pop
        //     3088: goto -904 -> 2184
        //     3091: astore 131
        //     3093: ldc 186
        //     3095: new 662	java/lang/StringBuilder
        //     3098: dup
        //     3099: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     3102: ldc_w 2629
        //     3105: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     3108: aload 131
        //     3110: invokevirtual 2630	java/io/IOException:toString	()Ljava/lang/String;
        //     3113: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     3116: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     3119: invokestatic 2631	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     3122: pop
        //     3123: goto -939 -> 2184
        //     3126: astore 137
        //     3128: iconst_1
        //     3129: istore 138
        //     3131: goto -66 -> 3065
        //     3134: ldc 186
        //     3136: new 662	java/lang/StringBuilder
        //     3139: dup
        //     3140: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     3143: ldc_w 2633
        //     3146: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     3149: aload 33
        //     3151: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     3154: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     3157: invokestatic 747	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     3160: pop
        //     3161: aload_0
        //     3162: getfield 534	com/android/server/pm/PackageManagerService:mInstaller	Lcom/android/server/pm/Installer;
        //     3165: aload 133
        //     3167: aload_1
        //     3168: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     3171: getfield 1481	android/content/pm/ApplicationInfo:nativeLibraryDir	Ljava/lang/String;
        //     3174: invokevirtual 2636	com/android/server/pm/Installer:linkNativeLibraryDirectory	(Ljava/lang/String;Ljava/lang/String;)I
        //     3177: pop
        //     3178: goto -994 -> 2184
        //     3181: iconst_0
        //     3182: istore 129
        //     3184: goto -977 -> 2207
        //     3187: aload_0
        //     3188: getfield 476	com/android/server/pm/PackageManagerService:mFactoryTest	Z
        //     3191: ifeq +35 -> 3226
        //     3194: aload_1
        //     3195: getfield 1848	android/content/pm/PackageParser$Package:requestedPermissions	Ljava/util/ArrayList;
        //     3198: ldc_w 2638
        //     3201: invokevirtual 1989	java/util/ArrayList:contains	(Ljava/lang/Object;)Z
        //     3204: ifeq +22 -> 3226
        //     3207: aload_1
        //     3208: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     3211: astore 128
        //     3213: aload 128
        //     3215: bipush 16
        //     3217: aload 128
        //     3219: getfield 886	android/content/pm/ApplicationInfo:flags	I
        //     3222: ior
        //     3223: putfield 886	android/content/pm/ApplicationInfo:flags	I
        //     3226: iload_2
        //     3227: iconst_2
        //     3228: iand
        //     3229: ifeq +21 -> 3250
        //     3232: aload_0
        //     3233: aload_1
        //     3234: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     3237: getfield 1626	android/content/pm/ApplicationInfo:packageName	Ljava/lang/String;
        //     3240: aload_1
        //     3241: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     3244: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     3247: invokespecial 1502	com/android/server/pm/PackageManagerService:killApplication	(Ljava/lang/String;I)V
        //     3250: aload_0
        //     3251: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     3254: astore 34
        //     3256: aload 34
        //     3258: monitorenter
        //     3259: iload_3
        //     3260: iconst_1
        //     3261: iand
        //     3262: ifeq +16 -> 3278
        //     3265: aload_0
        //     3266: getfield 376	com/android/server/pm/PackageManagerService:mAppDirs	Ljava/util/HashMap;
        //     3269: aload_1
        //     3270: getfield 1722	android/content/pm/PackageParser$Package:mPath	Ljava/lang/String;
        //     3273: aload_1
        //     3274: invokevirtual 2271	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     3277: pop
        //     3278: aload_0
        //     3279: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     3282: aload 24
        //     3284: aload_1
        //     3285: invokevirtual 2642	com/android/server/pm/Settings:insertPackageSettingLPw	(Lcom/android/server/pm/PackageSetting;Landroid/content/pm/PackageParser$Package;)V
        //     3288: aload_0
        //     3289: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     3292: aload_1
        //     3293: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     3296: getfield 1626	android/content/pm/ApplicationInfo:packageName	Ljava/lang/String;
        //     3299: aload_1
        //     3300: invokevirtual 2271	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     3303: pop
        //     3304: aload_0
        //     3305: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     3308: getfield 2645	com/android/server/pm/Settings:mPackagesToBeCleaned	Ljava/util/ArrayList;
        //     3311: aload 26
        //     3313: invokevirtual 2646	java/util/ArrayList:remove	(Ljava/lang/Object;)Z
        //     3316: pop
        //     3317: lload 4
        //     3319: lconst_0
        //     3320: lcmp
        //     3321: ifeq +314 -> 3635
        //     3324: aload 24
        //     3326: getfield 2334	com/android/server/pm/PackageSettingBase:firstInstallTime	J
        //     3329: lconst_0
        //     3330: lcmp
        //     3331: ifne +279 -> 3610
        //     3334: aload 24
        //     3336: lload 4
        //     3338: putfield 2307	com/android/server/pm/PackageSettingBase:lastUpdateTime	J
        //     3341: aload 24
        //     3343: lload 4
        //     3345: putfield 2334	com/android/server/pm/PackageSettingBase:firstInstallTime	J
        //     3348: aload_1
        //     3349: getfield 2507	android/content/pm/PackageParser$Package:providers	Ljava/util/ArrayList;
        //     3352: invokevirtual 848	java/util/ArrayList:size	()I
        //     3355: istore 38
        //     3357: aconst_null
        //     3358: astore 39
        //     3360: iconst_0
        //     3361: istore 40
        //     3363: iload 40
        //     3365: iload 38
        //     3367: if_icmpge +2187 -> 5554
        //     3370: aload_1
        //     3371: getfield 2507	android/content/pm/PackageParser$Package:providers	Ljava/util/ArrayList;
        //     3374: iload 40
        //     3376: invokevirtual 851	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     3379: checkcast 2509	android/content/pm/PackageParser$Provider
        //     3382: astore 111
        //     3384: aload 111
        //     3386: getfield 2512	android/content/pm/PackageParser$Provider:info	Landroid/content/pm/ProviderInfo;
        //     3389: aload_1
        //     3390: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     3393: getfield 2398	android/content/pm/ApplicationInfo:processName	Ljava/lang/String;
        //     3396: aload 111
        //     3398: getfield 2512	android/content/pm/PackageParser$Provider:info	Landroid/content/pm/ProviderInfo;
        //     3401: getfield 2647	android/content/pm/ProviderInfo:processName	Ljava/lang/String;
        //     3404: aload_1
        //     3405: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     3408: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     3411: invokestatic 2539	com/android/server/pm/PackageManagerService:fixProcessName	(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
        //     3414: putfield 2647	android/content/pm/ProviderInfo:processName	Ljava/lang/String;
        //     3417: aload_0
        //     3418: getfield 403	com/android/server/pm/PackageManagerService:mProvidersByComponent	Ljava/util/HashMap;
        //     3421: new 314	android/content/ComponentName
        //     3424: dup
        //     3425: aload 111
        //     3427: getfield 2512	android/content/pm/PackageParser$Provider:info	Landroid/content/pm/ProviderInfo;
        //     3430: getfield 1747	android/content/pm/ComponentInfo:packageName	Ljava/lang/String;
        //     3433: aload 111
        //     3435: getfield 2512	android/content/pm/PackageParser$Provider:info	Landroid/content/pm/ProviderInfo;
        //     3438: getfield 2109	android/content/pm/ComponentInfo:name	Ljava/lang/String;
        //     3441: invokespecial 320	android/content/ComponentName:<init>	(Ljava/lang/String;Ljava/lang/String;)V
        //     3444: aload 111
        //     3446: invokevirtual 2271	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     3449: pop
        //     3450: aload 111
        //     3452: getfield 2512	android/content/pm/PackageParser$Provider:info	Landroid/content/pm/ProviderInfo;
        //     3455: getfield 2650	android/content/pm/ProviderInfo:isSyncable	Z
        //     3458: istore 113
        //     3460: aload 111
        //     3462: iload 113
        //     3464: putfield 2653	android/content/pm/PackageParser$Provider:syncable	Z
        //     3467: aload 111
        //     3469: getfield 2512	android/content/pm/PackageParser$Provider:info	Landroid/content/pm/ProviderInfo;
        //     3472: getfield 2517	android/content/pm/ProviderInfo:authority	Ljava/lang/String;
        //     3475: ifnull +365 -> 3840
        //     3478: aload 111
        //     3480: getfield 2512	android/content/pm/PackageParser$Provider:info	Landroid/content/pm/ProviderInfo;
        //     3483: getfield 2517	android/content/pm/ProviderInfo:authority	Ljava/lang/String;
        //     3486: ldc_w 2519
        //     3489: invokevirtual 660	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
        //     3492: astore 117
        //     3494: aload 111
        //     3496: getfield 2512	android/content/pm/PackageParser$Provider:info	Landroid/content/pm/ProviderInfo;
        //     3499: aconst_null
        //     3500: putfield 2517	android/content/pm/ProviderInfo:authority	Ljava/lang/String;
        //     3503: iconst_0
        //     3504: istore 118
        //     3506: aload 111
        //     3508: astore 119
        //     3510: aload 117
        //     3512: arraylength
        //     3513: istore 120
        //     3515: iload 118
        //     3517: iload 120
        //     3519: if_icmpge +2022 -> 5541
        //     3522: iload 118
        //     3524: iconst_1
        //     3525: if_icmpne +1935 -> 5460
        //     3528: aload 119
        //     3530: getfield 2653	android/content/pm/PackageParser$Provider:syncable	Z
        //     3533: ifeq +1927 -> 5460
        //     3536: new 2509	android/content/pm/PackageParser$Provider
        //     3539: dup
        //     3540: aload 119
        //     3542: invokespecial 2656	android/content/pm/PackageParser$Provider:<init>	(Landroid/content/pm/PackageParser$Provider;)V
        //     3545: astore 121
        //     3547: aload 121
        //     3549: iconst_0
        //     3550: putfield 2653	android/content/pm/PackageParser$Provider:syncable	Z
        //     3553: aload_0
        //     3554: getfield 405	com/android/server/pm/PackageManagerService:mProviders	Ljava/util/HashMap;
        //     3557: aload 117
        //     3559: iload 118
        //     3561: aaload
        //     3562: invokevirtual 1936	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
        //     3565: ifne +171 -> 3736
        //     3568: aload_0
        //     3569: getfield 405	com/android/server/pm/PackageManagerService:mProviders	Ljava/util/HashMap;
        //     3572: aload 117
        //     3574: iload 118
        //     3576: aaload
        //     3577: aload 121
        //     3579: invokevirtual 2271	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     3582: pop
        //     3583: aload 121
        //     3585: getfield 2512	android/content/pm/PackageParser$Provider:info	Landroid/content/pm/ProviderInfo;
        //     3588: getfield 2517	android/content/pm/ProviderInfo:authority	Ljava/lang/String;
        //     3591: ifnonnull +99 -> 3690
        //     3594: aload 121
        //     3596: getfield 2512	android/content/pm/PackageParser$Provider:info	Landroid/content/pm/ProviderInfo;
        //     3599: aload 117
        //     3601: iload 118
        //     3603: aaload
        //     3604: putfield 2517	android/content/pm/ProviderInfo:authority	Ljava/lang/String;
        //     3607: goto +1916 -> 5523
        //     3610: iload_3
        //     3611: bipush 64
        //     3613: iand
        //     3614: ifeq -266 -> 3348
        //     3617: aload 24
        //     3619: lload 4
        //     3621: putfield 2307	com/android/server/pm/PackageSettingBase:lastUpdateTime	J
        //     3624: goto -276 -> 3348
        //     3627: astore 35
        //     3629: aload 34
        //     3631: monitorexit
        //     3632: aload 35
        //     3634: athrow
        //     3635: aload 24
        //     3637: getfield 2334	com/android/server/pm/PackageSettingBase:firstInstallTime	J
        //     3640: lconst_0
        //     3641: lcmp
        //     3642: ifne +20 -> 3662
        //     3645: aload 24
        //     3647: lload 27
        //     3649: putfield 2307	com/android/server/pm/PackageSettingBase:lastUpdateTime	J
        //     3652: aload 24
        //     3654: lload 27
        //     3656: putfield 2334	com/android/server/pm/PackageSettingBase:firstInstallTime	J
        //     3659: goto -311 -> 3348
        //     3662: iload_2
        //     3663: bipush 64
        //     3665: iand
        //     3666: ifeq -318 -> 3348
        //     3669: lload 27
        //     3671: aload 24
        //     3673: getfield 1331	com/android/server/pm/PackageSettingBase:timeStamp	J
        //     3676: lcmp
        //     3677: ifeq -329 -> 3348
        //     3680: aload 24
        //     3682: lload 27
        //     3684: putfield 2307	com/android/server/pm/PackageSettingBase:lastUpdateTime	J
        //     3687: goto -339 -> 3348
        //     3690: aload 121
        //     3692: getfield 2512	android/content/pm/PackageParser$Provider:info	Landroid/content/pm/ProviderInfo;
        //     3695: new 662	java/lang/StringBuilder
        //     3698: dup
        //     3699: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     3702: aload 121
        //     3704: getfield 2512	android/content/pm/PackageParser$Provider:info	Landroid/content/pm/ProviderInfo;
        //     3707: getfield 2517	android/content/pm/ProviderInfo:authority	Ljava/lang/String;
        //     3710: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     3713: ldc_w 2519
        //     3716: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     3719: aload 117
        //     3721: iload 118
        //     3723: aaload
        //     3724: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     3727: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     3730: putfield 2517	android/content/pm/ProviderInfo:authority	Ljava/lang/String;
        //     3733: goto +1790 -> 5523
        //     3736: aload_0
        //     3737: getfield 405	com/android/server/pm/PackageManagerService:mProviders	Ljava/util/HashMap;
        //     3740: aload 117
        //     3742: iload 118
        //     3744: aaload
        //     3745: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     3748: checkcast 2509	android/content/pm/PackageParser$Provider
        //     3751: astore 122
        //     3753: new 662	java/lang/StringBuilder
        //     3756: dup
        //     3757: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     3760: ldc_w 2658
        //     3763: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     3766: aload 117
        //     3768: iload 118
        //     3770: aaload
        //     3771: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     3774: ldc_w 2523
        //     3777: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     3780: aload_1
        //     3781: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     3784: getfield 1626	android/content/pm/ApplicationInfo:packageName	Ljava/lang/String;
        //     3787: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     3790: ldc_w 2660
        //     3793: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     3796: astore 123
        //     3798: aload 122
        //     3800: ifnull +1733 -> 5533
        //     3803: aload 122
        //     3805: invokevirtual 2529	android/content/pm/PackageParser$Provider:getComponentName	()Landroid/content/ComponentName;
        //     3808: ifnull +1725 -> 5533
        //     3811: aload 122
        //     3813: invokevirtual 2529	android/content/pm/PackageParser$Provider:getComponentName	()Landroid/content/ComponentName;
        //     3816: invokevirtual 2532	android/content/ComponentName:getPackageName	()Ljava/lang/String;
        //     3819: astore 124
        //     3821: ldc 186
        //     3823: aload 123
        //     3825: aload 124
        //     3827: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     3830: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     3833: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     3836: pop
        //     3837: goto +1686 -> 5523
        //     3840: iload_2
        //     3841: iconst_2
        //     3842: iand
        //     3843: ifeq +1705 -> 5548
        //     3846: aload 39
        //     3848: ifnonnull +36 -> 3884
        //     3851: new 662	java/lang/StringBuilder
        //     3854: dup
        //     3855: sipush 256
        //     3858: invokespecial 2661	java/lang/StringBuilder:<init>	(I)V
        //     3861: astore 39
        //     3863: aload 111
        //     3865: getfield 2512	android/content/pm/PackageParser$Provider:info	Landroid/content/pm/ProviderInfo;
        //     3868: getfield 2109	android/content/pm/ComponentInfo:name	Ljava/lang/String;
        //     3871: astore 114
        //     3873: aload 39
        //     3875: aload 114
        //     3877: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     3880: pop
        //     3881: goto +1667 -> 5548
        //     3884: aload 39
        //     3886: bipush 32
        //     3888: invokevirtual 2590	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
        //     3891: pop
        //     3892: goto -29 -> 3863
        //     3895: aload_1
        //     3896: getfield 2664	android/content/pm/PackageParser$Package:services	Ljava/util/ArrayList;
        //     3899: invokevirtual 848	java/util/ArrayList:size	()I
        //     3902: istore 41
        //     3904: aconst_null
        //     3905: astore 42
        //     3907: iconst_0
        //     3908: istore 43
        //     3910: iload 43
        //     3912: iload 41
        //     3914: if_icmpge +1654 -> 5568
        //     3917: aload_1
        //     3918: getfield 2664	android/content/pm/PackageParser$Package:services	Ljava/util/ArrayList;
        //     3921: iload 43
        //     3923: invokevirtual 851	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     3926: checkcast 2666	android/content/pm/PackageParser$Service
        //     3929: astore 107
        //     3931: aload 107
        //     3933: getfield 2669	android/content/pm/PackageParser$Service:info	Landroid/content/pm/ServiceInfo;
        //     3936: aload_1
        //     3937: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     3940: getfield 2398	android/content/pm/ApplicationInfo:processName	Ljava/lang/String;
        //     3943: aload 107
        //     3945: getfield 2669	android/content/pm/PackageParser$Service:info	Landroid/content/pm/ServiceInfo;
        //     3948: getfield 2672	android/content/pm/ServiceInfo:processName	Ljava/lang/String;
        //     3951: aload_1
        //     3952: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     3955: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     3958: invokestatic 2539	com/android/server/pm/PackageManagerService:fixProcessName	(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
        //     3961: putfield 2672	android/content/pm/ServiceInfo:processName	Ljava/lang/String;
        //     3964: aload_0
        //     3965: getfield 401	com/android/server/pm/PackageManagerService:mServices	Lcom/android/server/pm/PackageManagerService$ServiceIntentResolver;
        //     3968: aload 107
        //     3970: invokevirtual 2675	com/android/server/pm/PackageManagerService$ServiceIntentResolver:addService	(Landroid/content/pm/PackageParser$Service;)V
        //     3973: iload_2
        //     3974: iconst_2
        //     3975: iand
        //     3976: ifeq +1586 -> 5562
        //     3979: aload 42
        //     3981: ifnonnull +36 -> 4017
        //     3984: new 662	java/lang/StringBuilder
        //     3987: dup
        //     3988: sipush 256
        //     3991: invokespecial 2661	java/lang/StringBuilder:<init>	(I)V
        //     3994: astore 42
        //     3996: aload 107
        //     3998: getfield 2669	android/content/pm/PackageParser$Service:info	Landroid/content/pm/ServiceInfo;
        //     4001: getfield 2109	android/content/pm/ComponentInfo:name	Ljava/lang/String;
        //     4004: astore 108
        //     4006: aload 42
        //     4008: aload 108
        //     4010: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     4013: pop
        //     4014: goto +1548 -> 5562
        //     4017: aload 42
        //     4019: bipush 32
        //     4021: invokevirtual 2590	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
        //     4024: pop
        //     4025: goto -29 -> 3996
        //     4028: aload_1
        //     4029: getfield 2678	android/content/pm/PackageParser$Package:receivers	Ljava/util/ArrayList;
        //     4032: invokevirtual 848	java/util/ArrayList:size	()I
        //     4035: istore 44
        //     4037: aconst_null
        //     4038: astore 45
        //     4040: iconst_0
        //     4041: istore 46
        //     4043: iload 46
        //     4045: iload 44
        //     4047: if_icmpge +1535 -> 5582
        //     4050: aload_1
        //     4051: getfield 2678	android/content/pm/PackageParser$Package:receivers	Ljava/util/ArrayList;
        //     4054: iload 46
        //     4056: invokevirtual 851	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     4059: checkcast 2680	android/content/pm/PackageParser$Activity
        //     4062: astore 103
        //     4064: aload 103
        //     4066: getfield 2682	android/content/pm/PackageParser$Activity:info	Landroid/content/pm/ActivityInfo;
        //     4069: aload_1
        //     4070: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     4073: getfield 2398	android/content/pm/ApplicationInfo:processName	Ljava/lang/String;
        //     4076: aload 103
        //     4078: getfield 2682	android/content/pm/PackageParser$Activity:info	Landroid/content/pm/ActivityInfo;
        //     4081: getfield 2399	android/content/pm/ActivityInfo:processName	Ljava/lang/String;
        //     4084: aload_1
        //     4085: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     4088: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     4091: invokestatic 2539	com/android/server/pm/PackageManagerService:fixProcessName	(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
        //     4094: putfield 2399	android/content/pm/ActivityInfo:processName	Ljava/lang/String;
        //     4097: aload_0
        //     4098: getfield 398	com/android/server/pm/PackageManagerService:mReceivers	Lcom/android/server/pm/PackageManagerService$ActivityIntentResolver;
        //     4101: aload 103
        //     4103: ldc_w 2684
        //     4106: invokevirtual 2688	com/android/server/pm/PackageManagerService$ActivityIntentResolver:addActivity	(Landroid/content/pm/PackageParser$Activity;Ljava/lang/String;)V
        //     4109: iload_2
        //     4110: iconst_2
        //     4111: iand
        //     4112: ifeq +1464 -> 5576
        //     4115: aload 45
        //     4117: ifnonnull +36 -> 4153
        //     4120: new 662	java/lang/StringBuilder
        //     4123: dup
        //     4124: sipush 256
        //     4127: invokespecial 2661	java/lang/StringBuilder:<init>	(I)V
        //     4130: astore 45
        //     4132: aload 103
        //     4134: getfield 2682	android/content/pm/PackageParser$Activity:info	Landroid/content/pm/ActivityInfo;
        //     4137: getfield 2109	android/content/pm/ComponentInfo:name	Ljava/lang/String;
        //     4140: astore 104
        //     4142: aload 45
        //     4144: aload 104
        //     4146: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     4149: pop
        //     4150: goto +1426 -> 5576
        //     4153: aload 45
        //     4155: bipush 32
        //     4157: invokevirtual 2590	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
        //     4160: pop
        //     4161: goto -29 -> 4132
        //     4164: aload_1
        //     4165: getfield 2691	android/content/pm/PackageParser$Package:activities	Ljava/util/ArrayList;
        //     4168: invokevirtual 848	java/util/ArrayList:size	()I
        //     4171: istore 47
        //     4173: aconst_null
        //     4174: astore 48
        //     4176: iconst_0
        //     4177: istore 49
        //     4179: iload 49
        //     4181: iload 47
        //     4183: if_icmpge +1413 -> 5596
        //     4186: aload_1
        //     4187: getfield 2691	android/content/pm/PackageParser$Package:activities	Ljava/util/ArrayList;
        //     4190: iload 49
        //     4192: invokevirtual 851	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     4195: checkcast 2680	android/content/pm/PackageParser$Activity
        //     4198: astore 99
        //     4200: aload 99
        //     4202: getfield 2682	android/content/pm/PackageParser$Activity:info	Landroid/content/pm/ActivityInfo;
        //     4205: aload_1
        //     4206: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     4209: getfield 2398	android/content/pm/ApplicationInfo:processName	Ljava/lang/String;
        //     4212: aload 99
        //     4214: getfield 2682	android/content/pm/PackageParser$Activity:info	Landroid/content/pm/ActivityInfo;
        //     4217: getfield 2399	android/content/pm/ActivityInfo:processName	Ljava/lang/String;
        //     4220: aload_1
        //     4221: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     4224: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     4227: invokestatic 2539	com/android/server/pm/PackageManagerService:fixProcessName	(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
        //     4230: putfield 2399	android/content/pm/ActivityInfo:processName	Ljava/lang/String;
        //     4233: aload_0
        //     4234: getfield 396	com/android/server/pm/PackageManagerService:mActivities	Lcom/android/server/pm/PackageManagerService$ActivityIntentResolver;
        //     4237: aload 99
        //     4239: ldc_w 2693
        //     4242: invokevirtual 2688	com/android/server/pm/PackageManagerService$ActivityIntentResolver:addActivity	(Landroid/content/pm/PackageParser$Activity;Ljava/lang/String;)V
        //     4245: iload_2
        //     4246: iconst_2
        //     4247: iand
        //     4248: ifeq +1342 -> 5590
        //     4251: aload 48
        //     4253: ifnonnull +36 -> 4289
        //     4256: new 662	java/lang/StringBuilder
        //     4259: dup
        //     4260: sipush 256
        //     4263: invokespecial 2661	java/lang/StringBuilder:<init>	(I)V
        //     4266: astore 48
        //     4268: aload 99
        //     4270: getfield 2682	android/content/pm/PackageParser$Activity:info	Landroid/content/pm/ActivityInfo;
        //     4273: getfield 2109	android/content/pm/ComponentInfo:name	Ljava/lang/String;
        //     4276: astore 100
        //     4278: aload 48
        //     4280: aload 100
        //     4282: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     4285: pop
        //     4286: goto +1304 -> 5590
        //     4289: aload 48
        //     4291: bipush 32
        //     4293: invokevirtual 2590	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
        //     4296: pop
        //     4297: goto -29 -> 4268
        //     4300: aload_1
        //     4301: getfield 2696	android/content/pm/PackageParser$Package:permissionGroups	Ljava/util/ArrayList;
        //     4304: invokevirtual 848	java/util/ArrayList:size	()I
        //     4307: istore 50
        //     4309: aconst_null
        //     4310: astore 51
        //     4312: iconst_0
        //     4313: istore 52
        //     4315: iload 52
        //     4317: iload 50
        //     4319: if_icmpge +1291 -> 5610
        //     4322: aload_1
        //     4323: getfield 2696	android/content/pm/PackageParser$Package:permissionGroups	Ljava/util/ArrayList;
        //     4326: iload 52
        //     4328: invokevirtual 851	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     4331: checkcast 2698	android/content/pm/PackageParser$PermissionGroup
        //     4334: astore 88
        //     4336: aload_0
        //     4337: getfield 409	com/android/server/pm/PackageManagerService:mPermissionGroups	Ljava/util/HashMap;
        //     4340: aload 88
        //     4342: getfield 2701	android/content/pm/PackageParser$PermissionGroup:info	Landroid/content/pm/PermissionGroupInfo;
        //     4345: getfield 2704	android/content/pm/PermissionGroupInfo:name	Ljava/lang/String;
        //     4348: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     4351: checkcast 2698	android/content/pm/PackageParser$PermissionGroup
        //     4354: astore 89
        //     4356: aload 89
        //     4358: ifnonnull +76 -> 4434
        //     4361: aload_0
        //     4362: getfield 409	com/android/server/pm/PackageManagerService:mPermissionGroups	Ljava/util/HashMap;
        //     4365: aload 88
        //     4367: getfield 2701	android/content/pm/PackageParser$PermissionGroup:info	Landroid/content/pm/PermissionGroupInfo;
        //     4370: getfield 2704	android/content/pm/PermissionGroupInfo:name	Ljava/lang/String;
        //     4373: aload 88
        //     4375: invokevirtual 2271	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     4378: pop
        //     4379: iload_2
        //     4380: iconst_2
        //     4381: iand
        //     4382: ifeq +1222 -> 5604
        //     4385: aload 51
        //     4387: ifnonnull +36 -> 4423
        //     4390: new 662	java/lang/StringBuilder
        //     4393: dup
        //     4394: sipush 256
        //     4397: invokespecial 2661	java/lang/StringBuilder:<init>	(I)V
        //     4400: astore 51
        //     4402: aload 88
        //     4404: getfield 2701	android/content/pm/PackageParser$PermissionGroup:info	Landroid/content/pm/PermissionGroupInfo;
        //     4407: getfield 2704	android/content/pm/PermissionGroupInfo:name	Ljava/lang/String;
        //     4410: astore 96
        //     4412: aload 51
        //     4414: aload 96
        //     4416: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     4419: pop
        //     4420: goto +1184 -> 5604
        //     4423: aload 51
        //     4425: bipush 32
        //     4427: invokevirtual 2590	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
        //     4430: pop
        //     4431: goto -29 -> 4402
        //     4434: ldc 186
        //     4436: new 662	java/lang/StringBuilder
        //     4439: dup
        //     4440: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     4443: ldc_w 2706
        //     4446: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     4449: aload 88
        //     4451: getfield 2701	android/content/pm/PackageParser$PermissionGroup:info	Landroid/content/pm/PermissionGroupInfo;
        //     4454: getfield 2704	android/content/pm/PermissionGroupInfo:name	Ljava/lang/String;
        //     4457: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     4460: ldc_w 1901
        //     4463: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     4466: aload 88
        //     4468: getfield 2701	android/content/pm/PackageParser$PermissionGroup:info	Landroid/content/pm/PermissionGroupInfo;
        //     4471: getfield 2707	android/content/pm/PermissionGroupInfo:packageName	Ljava/lang/String;
        //     4474: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     4477: ldc_w 2709
        //     4480: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     4483: aload 89
        //     4485: getfield 2701	android/content/pm/PackageParser$PermissionGroup:info	Landroid/content/pm/PermissionGroupInfo;
        //     4488: getfield 2707	android/content/pm/PermissionGroupInfo:packageName	Ljava/lang/String;
        //     4491: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     4494: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     4497: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     4500: pop
        //     4501: iload_2
        //     4502: iconst_2
        //     4503: iand
        //     4504: ifeq +1100 -> 5604
        //     4507: aload 51
        //     4509: ifnonnull +45 -> 4554
        //     4512: new 662	java/lang/StringBuilder
        //     4515: dup
        //     4516: sipush 256
        //     4519: invokespecial 2661	java/lang/StringBuilder:<init>	(I)V
        //     4522: astore 51
        //     4524: aload 51
        //     4526: ldc_w 2711
        //     4529: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     4532: pop
        //     4533: aload 88
        //     4535: getfield 2701	android/content/pm/PackageParser$PermissionGroup:info	Landroid/content/pm/PermissionGroupInfo;
        //     4538: getfield 2704	android/content/pm/PermissionGroupInfo:name	Ljava/lang/String;
        //     4541: astore 92
        //     4543: aload 51
        //     4545: aload 92
        //     4547: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     4550: pop
        //     4551: goto +1053 -> 5604
        //     4554: aload 51
        //     4556: bipush 32
        //     4558: invokevirtual 2590	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
        //     4561: pop
        //     4562: goto -38 -> 4524
        //     4565: aload_1
        //     4566: getfield 1919	android/content/pm/PackageParser$Package:permissions	Ljava/util/ArrayList;
        //     4569: invokevirtual 848	java/util/ArrayList:size	()I
        //     4572: istore 53
        //     4574: aconst_null
        //     4575: astore 54
        //     4577: iconst_0
        //     4578: istore 55
        //     4580: iload 55
        //     4582: iload 53
        //     4584: if_icmpge +1040 -> 5624
        //     4587: aload_1
        //     4588: getfield 1919	android/content/pm/PackageParser$Package:permissions	Ljava/util/ArrayList;
        //     4591: iload 55
        //     4593: invokevirtual 851	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     4596: checkcast 1921	android/content/pm/PackageParser$Permission
        //     4599: astore 67
        //     4601: aload 67
        //     4603: getfield 2714	android/content/pm/PackageParser$Permission:tree	Z
        //     4606: ifeq +309 -> 4915
        //     4609: aload_0
        //     4610: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     4613: getfield 1597	com/android/server/pm/Settings:mPermissionTrees	Ljava/util/HashMap;
        //     4616: astore 68
        //     4618: aload 67
        //     4620: aload_0
        //     4621: getfield 409	com/android/server/pm/PackageManagerService:mPermissionGroups	Ljava/util/HashMap;
        //     4624: aload 67
        //     4626: getfield 1925	android/content/pm/PackageParser$Permission:info	Landroid/content/pm/PermissionInfo;
        //     4629: getfield 2716	android/content/pm/PermissionInfo:group	Ljava/lang/String;
        //     4632: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     4635: checkcast 2698	android/content/pm/PackageParser$PermissionGroup
        //     4638: putfield 2719	android/content/pm/PackageParser$Permission:group	Landroid/content/pm/PackageParser$PermissionGroup;
        //     4641: aload 67
        //     4643: getfield 1925	android/content/pm/PackageParser$Permission:info	Landroid/content/pm/PermissionInfo;
        //     4646: getfield 2716	android/content/pm/PermissionInfo:group	Ljava/lang/String;
        //     4649: ifnull +11 -> 4660
        //     4652: aload 67
        //     4654: getfield 2719	android/content/pm/PackageParser$Permission:group	Landroid/content/pm/PackageParser$PermissionGroup;
        //     4657: ifnull +493 -> 5150
        //     4660: aload 67
        //     4662: getfield 1925	android/content/pm/PackageParser$Permission:info	Landroid/content/pm/PermissionInfo;
        //     4665: getfield 1379	android/content/pm/PermissionInfo:name	Ljava/lang/String;
        //     4668: astore 69
        //     4670: aload 68
        //     4672: aload 69
        //     4674: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     4677: checkcast 1168	com/android/server/pm/BasePermission
        //     4680: astore 70
        //     4682: aload 70
        //     4684: ifnonnull +57 -> 4741
        //     4687: aload 67
        //     4689: getfield 1925	android/content/pm/PackageParser$Permission:info	Landroid/content/pm/PermissionInfo;
        //     4692: getfield 1379	android/content/pm/PermissionInfo:name	Ljava/lang/String;
        //     4695: astore 83
        //     4697: aload 67
        //     4699: getfield 1925	android/content/pm/PackageParser$Permission:info	Landroid/content/pm/PermissionInfo;
        //     4702: getfield 1390	android/content/pm/PermissionInfo:packageName	Ljava/lang/String;
        //     4705: astore 84
        //     4707: new 1168	com/android/server/pm/BasePermission
        //     4710: dup
        //     4711: aload 83
        //     4713: aload 84
        //     4715: iconst_0
        //     4716: invokespecial 2722	com/android/server/pm/BasePermission:<init>	(Ljava/lang/String;Ljava/lang/String;I)V
        //     4719: astore 70
        //     4721: aload 67
        //     4723: getfield 1925	android/content/pm/PackageParser$Permission:info	Landroid/content/pm/PermissionInfo;
        //     4726: getfield 1379	android/content/pm/PermissionInfo:name	Ljava/lang/String;
        //     4729: astore 85
        //     4731: aload 68
        //     4733: aload 85
        //     4735: aload 70
        //     4737: invokevirtual 2271	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     4740: pop
        //     4741: aload 70
        //     4743: getfield 1652	com/android/server/pm/BasePermission:perm	Landroid/content/pm/PackageParser$Permission;
        //     4746: ifnonnull +340 -> 5086
        //     4749: aload 70
        //     4751: getfield 1659	com/android/server/pm/BasePermission:sourcePackage	Ljava/lang/String;
        //     4754: ifnull +22 -> 4776
        //     4757: aload 70
        //     4759: getfield 1659	com/android/server/pm/BasePermission:sourcePackage	Ljava/lang/String;
        //     4762: aload 67
        //     4764: getfield 1925	android/content/pm/PackageParser$Permission:info	Landroid/content/pm/PermissionInfo;
        //     4767: getfield 1390	android/content/pm/PermissionInfo:packageName	Ljava/lang/String;
        //     4770: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     4773: ifeq +246 -> 5019
        //     4776: aload_0
        //     4777: aload 67
        //     4779: getfield 1925	android/content/pm/PackageParser$Permission:info	Landroid/content/pm/PermissionInfo;
        //     4782: getfield 1379	android/content/pm/PermissionInfo:name	Ljava/lang/String;
        //     4785: invokespecial 1166	com/android/server/pm/PackageManagerService:findPermissionTreeLP	(Ljava/lang/String;)Lcom/android/server/pm/BasePermission;
        //     4788: astore 76
        //     4790: aload 76
        //     4792: ifnull +22 -> 4814
        //     4795: aload 76
        //     4797: getfield 1659	com/android/server/pm/BasePermission:sourcePackage	Ljava/lang/String;
        //     4800: aload 67
        //     4802: getfield 1925	android/content/pm/PackageParser$Permission:info	Landroid/content/pm/PermissionInfo;
        //     4805: getfield 1390	android/content/pm/PermissionInfo:packageName	Ljava/lang/String;
        //     4808: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     4811: ifeq +127 -> 4938
        //     4814: aload 70
        //     4816: aload 24
        //     4818: putfield 1855	com/android/server/pm/BasePermission:packageSetting	Lcom/android/server/pm/PackageSettingBase;
        //     4821: aload 70
        //     4823: aload 67
        //     4825: putfield 1652	com/android/server/pm/BasePermission:perm	Landroid/content/pm/PackageParser$Permission;
        //     4828: aload_1
        //     4829: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     4832: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     4835: istore 77
        //     4837: aload 70
        //     4839: iload 77
        //     4841: putfield 1171	com/android/server/pm/BasePermission:uid	I
        //     4844: iload_2
        //     4845: iconst_2
        //     4846: iand
        //     4847: ifeq +38 -> 4885
        //     4850: aload 54
        //     4852: ifnonnull +75 -> 4927
        //     4855: new 662	java/lang/StringBuilder
        //     4858: dup
        //     4859: sipush 256
        //     4862: invokespecial 2661	java/lang/StringBuilder:<init>	(I)V
        //     4865: astore 54
        //     4867: aload 67
        //     4869: getfield 1925	android/content/pm/PackageParser$Permission:info	Landroid/content/pm/PermissionInfo;
        //     4872: getfield 1379	android/content/pm/PermissionInfo:name	Ljava/lang/String;
        //     4875: astore 78
        //     4877: aload 54
        //     4879: aload 78
        //     4881: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     4884: pop
        //     4885: aload 70
        //     4887: getfield 1652	com/android/server/pm/BasePermission:perm	Landroid/content/pm/PackageParser$Permission;
        //     4890: aload 67
        //     4892: if_acmpne +726 -> 5618
        //     4895: aload 67
        //     4897: getfield 1925	android/content/pm/PackageParser$Permission:info	Landroid/content/pm/PermissionInfo;
        //     4900: getfield 1378	android/content/pm/PermissionInfo:protectionLevel	I
        //     4903: istore 74
        //     4905: aload 70
        //     4907: iload 74
        //     4909: putfield 1660	com/android/server/pm/BasePermission:protectionLevel	I
        //     4912: goto +706 -> 5618
        //     4915: aload_0
        //     4916: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     4919: getfield 1851	com/android/server/pm/Settings:mPermissions	Ljava/util/HashMap;
        //     4922: astore 68
        //     4924: goto -306 -> 4618
        //     4927: aload 54
        //     4929: bipush 32
        //     4931: invokevirtual 2590	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
        //     4934: pop
        //     4935: goto -68 -> 4867
        //     4938: ldc 186
        //     4940: new 662	java/lang/StringBuilder
        //     4943: dup
        //     4944: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     4947: ldc_w 2724
        //     4950: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     4953: aload 67
        //     4955: getfield 1925	android/content/pm/PackageParser$Permission:info	Landroid/content/pm/PermissionInfo;
        //     4958: getfield 1379	android/content/pm/PermissionInfo:name	Ljava/lang/String;
        //     4961: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     4964: ldc_w 1901
        //     4967: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     4970: aload 67
        //     4972: getfield 1925	android/content/pm/PackageParser$Permission:info	Landroid/content/pm/PermissionInfo;
        //     4975: getfield 1390	android/content/pm/PermissionInfo:packageName	Ljava/lang/String;
        //     4978: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     4981: ldc_w 2726
        //     4984: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     4987: aload 76
        //     4989: getfield 1189	com/android/server/pm/BasePermission:name	Ljava/lang/String;
        //     4992: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     4995: ldc_w 2728
        //     4998: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     5001: aload 76
        //     5003: getfield 1659	com/android/server/pm/BasePermission:sourcePackage	Ljava/lang/String;
        //     5006: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     5009: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     5012: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     5015: pop
        //     5016: goto -131 -> 4885
        //     5019: ldc 186
        //     5021: new 662	java/lang/StringBuilder
        //     5024: dup
        //     5025: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     5028: ldc_w 2724
        //     5031: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     5034: aload 67
        //     5036: getfield 1925	android/content/pm/PackageParser$Permission:info	Landroid/content/pm/PermissionInfo;
        //     5039: getfield 1379	android/content/pm/PermissionInfo:name	Ljava/lang/String;
        //     5042: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     5045: ldc_w 1901
        //     5048: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     5051: aload 67
        //     5053: getfield 1925	android/content/pm/PackageParser$Permission:info	Landroid/content/pm/PermissionInfo;
        //     5056: getfield 1390	android/content/pm/PermissionInfo:packageName	Ljava/lang/String;
        //     5059: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     5062: ldc_w 2709
        //     5065: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     5068: aload 70
        //     5070: getfield 1659	com/android/server/pm/BasePermission:sourcePackage	Ljava/lang/String;
        //     5073: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     5076: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     5079: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     5082: pop
        //     5083: goto -198 -> 4885
        //     5086: iload_2
        //     5087: iconst_2
        //     5088: iand
        //     5089: ifeq -204 -> 4885
        //     5092: aload 54
        //     5094: ifnonnull +45 -> 5139
        //     5097: new 662	java/lang/StringBuilder
        //     5100: dup
        //     5101: sipush 256
        //     5104: invokespecial 2661	java/lang/StringBuilder:<init>	(I)V
        //     5107: astore 54
        //     5109: aload 54
        //     5111: ldc_w 2711
        //     5114: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     5117: pop
        //     5118: aload 67
        //     5120: getfield 1925	android/content/pm/PackageParser$Permission:info	Landroid/content/pm/PermissionInfo;
        //     5123: getfield 1379	android/content/pm/PermissionInfo:name	Ljava/lang/String;
        //     5126: astore 72
        //     5128: aload 54
        //     5130: aload 72
        //     5132: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     5135: pop
        //     5136: goto -251 -> 4885
        //     5139: aload 54
        //     5141: bipush 32
        //     5143: invokevirtual 2590	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
        //     5146: pop
        //     5147: goto -38 -> 5109
        //     5150: ldc 186
        //     5152: new 662	java/lang/StringBuilder
        //     5155: dup
        //     5156: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     5159: ldc_w 2724
        //     5162: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     5165: aload 67
        //     5167: getfield 1925	android/content/pm/PackageParser$Permission:info	Landroid/content/pm/PermissionInfo;
        //     5170: getfield 1379	android/content/pm/PermissionInfo:name	Ljava/lang/String;
        //     5173: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     5176: ldc_w 1901
        //     5179: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     5182: aload 67
        //     5184: getfield 1925	android/content/pm/PackageParser$Permission:info	Landroid/content/pm/PermissionInfo;
        //     5187: getfield 1390	android/content/pm/PermissionInfo:packageName	Ljava/lang/String;
        //     5190: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     5193: ldc_w 2730
        //     5196: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     5199: aload 67
        //     5201: getfield 2719	android/content/pm/PackageParser$Permission:group	Landroid/content/pm/PackageParser$PermissionGroup;
        //     5204: invokevirtual 944	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     5207: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     5210: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     5213: pop
        //     5214: goto +404 -> 5618
        //     5217: aload_1
        //     5218: getfield 2733	android/content/pm/PackageParser$Package:instrumentation	Ljava/util/ArrayList;
        //     5221: invokevirtual 848	java/util/ArrayList:size	()I
        //     5224: istore 56
        //     5226: aconst_null
        //     5227: astore 57
        //     5229: iconst_0
        //     5230: istore 58
        //     5232: iload 58
        //     5234: iload 56
        //     5236: if_icmpge +402 -> 5638
        //     5239: aload_1
        //     5240: getfield 2733	android/content/pm/PackageParser$Package:instrumentation	Ljava/util/ArrayList;
        //     5243: iload 58
        //     5245: invokevirtual 851	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     5248: checkcast 2735	android/content/pm/PackageParser$Instrumentation
        //     5251: astore 62
        //     5253: aload 62
        //     5255: getfield 2738	android/content/pm/PackageParser$Instrumentation:info	Landroid/content/pm/InstrumentationInfo;
        //     5258: aload_1
        //     5259: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     5262: getfield 1626	android/content/pm/ApplicationInfo:packageName	Ljava/lang/String;
        //     5265: putfield 2741	android/content/pm/InstrumentationInfo:packageName	Ljava/lang/String;
        //     5268: aload 62
        //     5270: getfield 2738	android/content/pm/PackageParser$Instrumentation:info	Landroid/content/pm/InstrumentationInfo;
        //     5273: aload_1
        //     5274: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     5277: getfield 1475	android/content/pm/ApplicationInfo:sourceDir	Ljava/lang/String;
        //     5280: putfield 2742	android/content/pm/InstrumentationInfo:sourceDir	Ljava/lang/String;
        //     5283: aload 62
        //     5285: getfield 2738	android/content/pm/PackageParser$Instrumentation:info	Landroid/content/pm/InstrumentationInfo;
        //     5288: aload_1
        //     5289: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     5292: getfield 1478	android/content/pm/ApplicationInfo:publicSourceDir	Ljava/lang/String;
        //     5295: putfield 2743	android/content/pm/InstrumentationInfo:publicSourceDir	Ljava/lang/String;
        //     5298: aload 62
        //     5300: getfield 2738	android/content/pm/PackageParser$Instrumentation:info	Landroid/content/pm/InstrumentationInfo;
        //     5303: aload_1
        //     5304: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     5307: getfield 1639	android/content/pm/ApplicationInfo:dataDir	Ljava/lang/String;
        //     5310: putfield 2744	android/content/pm/InstrumentationInfo:dataDir	Ljava/lang/String;
        //     5313: aload 62
        //     5315: getfield 2738	android/content/pm/PackageParser$Instrumentation:info	Landroid/content/pm/InstrumentationInfo;
        //     5318: aload_1
        //     5319: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     5322: getfield 1481	android/content/pm/ApplicationInfo:nativeLibraryDir	Ljava/lang/String;
        //     5325: putfield 2745	android/content/pm/InstrumentationInfo:nativeLibraryDir	Ljava/lang/String;
        //     5328: aload_0
        //     5329: getfield 407	com/android/server/pm/PackageManagerService:mInstrumentation	Ljava/util/HashMap;
        //     5332: aload 62
        //     5334: invokevirtual 2746	android/content/pm/PackageParser$Instrumentation:getComponentName	()Landroid/content/ComponentName;
        //     5337: aload 62
        //     5339: invokevirtual 2271	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     5342: pop
        //     5343: iload_2
        //     5344: iconst_2
        //     5345: iand
        //     5346: ifeq +286 -> 5632
        //     5349: aload 57
        //     5351: ifnonnull +36 -> 5387
        //     5354: new 662	java/lang/StringBuilder
        //     5357: dup
        //     5358: sipush 256
        //     5361: invokespecial 2661	java/lang/StringBuilder:<init>	(I)V
        //     5364: astore 57
        //     5366: aload 62
        //     5368: getfield 2738	android/content/pm/PackageParser$Instrumentation:info	Landroid/content/pm/InstrumentationInfo;
        //     5371: getfield 2747	android/content/pm/InstrumentationInfo:name	Ljava/lang/String;
        //     5374: astore 64
        //     5376: aload 57
        //     5378: aload 64
        //     5380: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     5383: pop
        //     5384: goto +248 -> 5632
        //     5387: aload 57
        //     5389: bipush 32
        //     5391: invokevirtual 2590	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
        //     5394: pop
        //     5395: goto -29 -> 5366
        //     5398: aload_1
        //     5399: getfield 2750	android/content/pm/PackageParser$Package:protectedBroadcasts	Ljava/util/ArrayList;
        //     5402: ifnull +45 -> 5447
        //     5405: aload_1
        //     5406: getfield 2750	android/content/pm/PackageParser$Package:protectedBroadcasts	Ljava/util/ArrayList;
        //     5409: invokevirtual 848	java/util/ArrayList:size	()I
        //     5412: istore 59
        //     5414: iconst_0
        //     5415: istore 60
        //     5417: iload 60
        //     5419: iload 59
        //     5421: if_icmpge +26 -> 5447
        //     5424: aload_0
        //     5425: getfield 416	com/android/server/pm/PackageManagerService:mProtectedBroadcasts	Ljava/util/HashSet;
        //     5428: aload_1
        //     5429: getfield 2750	android/content/pm/PackageParser$Package:protectedBroadcasts	Ljava/util/ArrayList;
        //     5432: iload 60
        //     5434: invokevirtual 851	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     5437: invokevirtual 650	java/util/HashSet:add	(Ljava/lang/Object;)Z
        //     5440: pop
        //     5441: iinc 60 1
        //     5444: goto -27 -> 5417
        //     5447: aload 24
        //     5449: lload 27
        //     5451: invokevirtual 2753	com/android/server/pm/PackageSetting:setTimeStamp	(J)V
        //     5454: aload 34
        //     5456: monitorexit
        //     5457: goto -5402 -> 55
        //     5460: aload 119
        //     5462: astore 121
        //     5464: goto -1911 -> 3553
        //     5467: iconst_0
        //     5468: istore 16
        //     5470: goto -4881 -> 589
        //     5473: iconst_0
        //     5474: istore 18
        //     5476: goto -4733 -> 743
        //     5479: iinc 18 1
        //     5482: goto -4739 -> 743
        //     5485: iconst_0
        //     5486: istore 17
        //     5488: goto -15 -> 5473
        //     5491: iinc 175 255
        //     5494: goto -4298 -> 1196
        //     5497: ldc_w 2755
        //     5500: astore 165
        //     5502: goto -3652 -> 1850
        //     5505: iinc 161 1
        //     5508: goto -3770 -> 1738
        //     5511: iinc 158 1
        //     5514: goto -3827 -> 1687
        //     5517: iinc 153 255
        //     5520: goto -3621 -> 1899
        //     5523: iinc 118 1
        //     5526: aload 121
        //     5528: astore 119
        //     5530: goto -2020 -> 3510
        //     5533: ldc_w 2755
        //     5536: astore 124
        //     5538: goto -1717 -> 3821
        //     5541: aload 119
        //     5543: astore 111
        //     5545: goto -1705 -> 3840
        //     5548: iinc 40 1
        //     5551: goto -2188 -> 3363
        //     5554: aload 39
        //     5556: ifnull -1661 -> 3895
        //     5559: goto -1664 -> 3895
        //     5562: iinc 43 1
        //     5565: goto -1655 -> 3910
        //     5568: aload 42
        //     5570: ifnull -1542 -> 4028
        //     5573: goto -1545 -> 4028
        //     5576: iinc 46 1
        //     5579: goto -1536 -> 4043
        //     5582: aload 45
        //     5584: ifnull -1420 -> 4164
        //     5587: goto -1423 -> 4164
        //     5590: iinc 49 1
        //     5593: goto -1414 -> 4179
        //     5596: aload 48
        //     5598: ifnull -1298 -> 4300
        //     5601: goto -1301 -> 4300
        //     5604: iinc 52 1
        //     5607: goto -1292 -> 4315
        //     5610: aload 51
        //     5612: ifnull -1047 -> 4565
        //     5615: goto -1050 -> 4565
        //     5618: iinc 55 1
        //     5621: goto -1041 -> 4580
        //     5624: aload 54
        //     5626: ifnull -409 -> 5217
        //     5629: goto -412 -> 5217
        //     5632: iinc 58 1
        //     5635: goto -403 -> 5232
        //     5638: aload 57
        //     5640: ifnull -242 -> 5398
        //     5643: goto -245 -> 5398
        //
        // Exception table:
        //     from	to	target	type
        //     109	191	186	finally
        //     194	377	186	finally
        //     517	697	692	finally
        //     706	2003	692	finally
        //     2739	2786	2884	finally
        //     2886	2889	2884	finally
        //     2112	2184	3091	java/io/IOException
        //     3020	3035	3091	java/io/IOException
        //     3040	3061	3091	java/io/IOException
        //     3070	3088	3091	java/io/IOException
        //     3134	3178	3091	java/io/IOException
        //     3040	3061	3126	libcore/io/ErrnoException
        //     3265	3632	3627	finally
        //     3635	5457	3627	finally
    }

    private PackageParser.Package scanPackageLI(File paramFile, int paramInt1, int paramInt2, long paramLong)
    {
        this.mLastScanError = 1;
        String str1 = paramFile.getPath();
        int i = paramInt1 | this.mDefParseFlags;
        PackageParser localPackageParser = new PackageParser(str1);
        localPackageParser.setSeparateProcesses(this.mSeparateProcesses);
        localPackageParser.setOnlyCoreApps(this.mOnlyCore);
        PackageParser.Package localPackage1 = localPackageParser.parsePackage(paramFile, str1, this.mMetrics, i);
        PackageParser.Package localPackage2;
        if (localPackage1 == null)
        {
            this.mLastScanError = localPackageParser.getParseError();
            localPackage2 = null;
        }
        PackageSetting localPackageSetting1;
        PackageSetting localPackageSetting2;
        while (true)
        {
            return localPackage2;
            localPackageSetting1 = null;
            synchronized (this.mPackages)
            {
                String str2 = (String)this.mSettings.mRenamedPackages.get(localPackage1.packageName);
                if ((localPackage1.mOriginalPackages != null) && (localPackage1.mOriginalPackages.contains(str2)))
                    localPackageSetting1 = this.mSettings.peekPackageLPr(str2);
                if (localPackageSetting1 == null)
                    localPackageSetting1 = this.mSettings.peekPackageLPr(localPackage1.packageName);
                Settings localSettings = this.mSettings;
                if (localPackageSetting1 != null)
                {
                    str3 = localPackageSetting1.name;
                    localPackageSetting2 = localSettings.getDisabledSystemPkgLPr(str3);
                    if ((localPackageSetting2 == null) || ((i & 0x1) == 0) || (localPackageSetting1 == null) || (localPackageSetting1.codePath.equals(paramFile)))
                        break label513;
                    if (localPackage1.mVersionCode >= localPackageSetting1.versionCode)
                        break label337;
                    Log.i("PackageManager", "Package " + localPackageSetting1.name + " at " + paramFile + " ignored: updated version " + localPackageSetting1.versionCode + " better than this " + localPackage1.mVersionCode);
                    this.mLastScanError = -5;
                    localPackage2 = null;
                    continue;
                }
                String str3 = localPackage1.packageName;
            }
            label337: synchronized (this.mPackages)
            {
                this.mPackages.remove(localPackageSetting1.name);
                Slog.w("PackageManager", "Package " + localPackageSetting1.name + " at " + paramFile + "reverting from " + localPackageSetting1.codePathString + ": new version " + localPackage1.mVersionCode + " better than installed " + localPackageSetting1.versionCode);
                InstallArgs localInstallArgs2 = createInstallArgs(packageFlagsToInstallFlags(localPackageSetting1), localPackageSetting1.codePathString, localPackageSetting1.resourcePathString, localPackageSetting1.nativeLibraryPathString);
                synchronized (this.mInstaller)
                {
                    localInstallArgs2.cleanUpResourcesLI();
                }
            }
            synchronized (this.mPackages)
            {
                this.mSettings.enableSystemPackageLPw(localPackageSetting1.name);
                label513: if (localPackageSetting2 != null)
                    i |= 1;
                if (!collectCertificatesLI(localPackageParser, localPackageSetting1, localPackage1, paramFile, i))
                {
                    Slog.w("PackageManager", "Failed verifying certificates for package:" + localPackage1.packageName);
                    localPackage2 = null;
                    continue;
                    localObject4 = finally;
                    throw localObject4;
                    localObject5 = finally;
                    throw localObject5;
                }
            }
        }
        int j = 0;
        label665: String str4;
        if ((localPackageSetting2 == null) && (localPackageSetting1 != null) && ((i & 0x40) != 0) && (!isSystemApp(localPackageSetting1)))
        {
            if (compareSignatures(localPackageSetting1.signatures.mSignatures, localPackage1.mSignatures) != 0)
            {
                deletePackageLI(localPackage1.packageName, true, 0, null, false);
                localPackageSetting1 = null;
            }
        }
        else
        {
            if ((localPackageSetting1 != null) && (!localPackageSetting1.codePath.equals(localPackageSetting1.resourcePath)))
                i |= 16;
            str4 = null;
            if ((i & 0x10) == 0)
                break label993;
            if ((localPackageSetting1 == null) || (localPackageSetting1.resourcePathString == null))
                break label960;
            str4 = localPackageSetting1.resourcePathString;
        }
        while (true)
        {
            while (true)
            {
                while (true)
                {
                    setApplicationInfoPaths(localPackage1, localPackage1.mScanPath, str4);
                    int k = paramInt2 | 0x8;
                    localPackage2 = scanPackageLI(localPackage1, i, k, paramLong);
                    if (j == 0)
                        break;
                    synchronized (this.mPackages)
                    {
                        grantPermissionsLPw(localPackage1, true);
                        this.mSettings.disableSystemPackageLPw(localPackage1.packageName);
                    }
                }
                if (localPackage1.mVersionCode < localPackageSetting1.versionCode)
                {
                    j = 1;
                    break label665;
                }
                Slog.w("PackageManager", "Package " + localPackageSetting1.name + " at " + paramFile + "reverting from " + localPackageSetting1.codePathString + ": new version " + localPackage1.mVersionCode + " better than installed " + localPackageSetting1.versionCode);
                InstallArgs localInstallArgs1 = createInstallArgs(packageFlagsToInstallFlags(localPackageSetting1), localPackageSetting1.codePathString, localPackageSetting1.resourcePathString, localPackageSetting1.nativeLibraryPathString);
                synchronized (this.mInstaller)
                {
                    localInstallArgs1.cleanUpResourcesLI();
                }
            }
            label960: Slog.e("PackageManager", "Resource path not set for pkg : " + localPackage1.packageName);
            continue;
            label993: str4 = localPackage1.mScanPath;
        }
    }

    static final void sendPackageBroadcast(String paramString1, String paramString2, Bundle paramBundle, String paramString3, IIntentReceiver paramIIntentReceiver, int paramInt)
    {
        IActivityManager localIActivityManager = ActivityManagerNative.getDefault();
        if (localIActivityManager != null)
            if (paramInt != -1);
        while (true)
        {
            try
            {
                int[] arrayOfInt1 = sUserManager.getUserIds();
                int[] arrayOfInt2 = arrayOfInt1;
                int i = arrayOfInt2.length;
                int j = 0;
                if (j < i)
                {
                    int k = arrayOfInt2[j];
                    if (paramString2 == null)
                        break label203;
                    localUri = Uri.fromParts("package", paramString2, null);
                    Intent localIntent = new Intent(paramString1, localUri);
                    if (paramBundle != null)
                        localIntent.putExtras(paramBundle);
                    if (paramString3 != null)
                        localIntent.setPackage(paramString3);
                    int m = localIntent.getIntExtra("android.intent.extra.UID", -1);
                    if ((m > 0) && (k > 0))
                        localIntent.putExtra("android.intent.extra.UID", UserId.getUid(k, UserId.getAppId(m)));
                    localIntent.addFlags(134217728);
                    if (paramIIntentReceiver == null)
                        break label209;
                    bool = true;
                    localIActivityManager.broadcastIntent(null, localIntent, null, paramIIntentReceiver, 0, null, null, null, bool, false, k);
                    j++;
                    continue;
                    arrayOfInt1 = new int[1];
                    arrayOfInt1[0] = paramInt;
                    continue;
                }
            }
            catch (RemoteException localRemoteException)
            {
            }
            return;
            label203: Uri localUri = null;
            continue;
            label209: boolean bool = false;
        }
    }

    private void sendPackageChangedBroadcast(String paramString, boolean paramBoolean, ArrayList<String> paramArrayList, int paramInt)
    {
        Bundle localBundle = new Bundle(4);
        localBundle.putString("android.intent.extra.changed_component_name", (String)paramArrayList.get(0));
        String[] arrayOfString = new String[paramArrayList.size()];
        paramArrayList.toArray(arrayOfString);
        localBundle.putStringArray("android.intent.extra.changed_component_name_list", arrayOfString);
        localBundle.putBoolean("android.intent.extra.DONT_KILL_APP", paramBoolean);
        localBundle.putInt("android.intent.extra.UID", paramInt);
        sendPackageBroadcast("android.intent.action.PACKAGE_CHANGED", paramString, localBundle, null, null, UserId.getUserId(paramInt));
    }

    private void sendResourcesChangedBroadcast(boolean paramBoolean, ArrayList<String> paramArrayList, int[] paramArrayOfInt, IIntentReceiver paramIIntentReceiver)
    {
        int i = paramArrayList.size();
        Bundle localBundle;
        if (i > 0)
        {
            localBundle = new Bundle();
            localBundle.putStringArray("android.intent.extra.changed_package_list", (String[])paramArrayList.toArray(new String[i]));
            if (paramArrayOfInt != null)
                localBundle.putIntArray("android.intent.extra.changed_uid_list", paramArrayOfInt);
            if (!paramBoolean)
                break label76;
        }
        label76: for (String str = "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE"; ; str = "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE")
        {
            sendPackageBroadcast(str, null, localBundle, null, paramIIntentReceiver, -1);
            return;
        }
    }

    private static void setApplicationInfoPaths(PackageParser.Package paramPackage, String paramString1, String paramString2)
    {
        paramPackage.mScanPath = paramString1;
        paramPackage.mPath = paramString1;
        paramPackage.applicationInfo.sourceDir = paramString1;
        paramPackage.applicationInfo.publicSourceDir = paramString2;
    }

    // ERROR //
    private void setEnabledSetting(String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3)
    {
        // Byte code:
        //     0: iload_3
        //     1: ifeq +46 -> 47
        //     4: iload_3
        //     5: iconst_1
        //     6: if_icmpeq +41 -> 47
        //     9: iload_3
        //     10: iconst_2
        //     11: if_icmpeq +36 -> 47
        //     14: iload_3
        //     15: iconst_3
        //     16: if_icmpeq +31 -> 47
        //     19: new 2866	java/lang/IllegalArgumentException
        //     22: dup
        //     23: new 662	java/lang/StringBuilder
        //     26: dup
        //     27: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     30: ldc_w 2868
        //     33: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     36: iload_3
        //     37: invokevirtual 902	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     40: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     43: invokespecial 2869	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     46: athrow
        //     47: invokestatic 1176	android/os/Binder:getCallingUid	()I
        //     50: istore 6
        //     52: aload_0
        //     53: getfield 474	com/android/server/pm/PackageManagerService:mContext	Landroid/content/Context;
        //     56: ldc_w 2871
        //     59: invokevirtual 2874	android/content/Context:checkCallingPermission	(Ljava/lang/String;)I
        //     62: istore 7
        //     64: aload_0
        //     65: iload 6
        //     67: iload 5
        //     69: invokespecial 2876	com/android/server/pm/PackageManagerService:checkValidCaller	(II)V
        //     72: iload 7
        //     74: ifne +98 -> 172
        //     77: iconst_1
        //     78: istore 8
        //     80: iconst_0
        //     81: istore 9
        //     83: aload_2
        //     84: ifnonnull +94 -> 178
        //     87: iconst_1
        //     88: istore 10
        //     90: iload 10
        //     92: ifeq +92 -> 184
        //     95: aload_1
        //     96: astore 11
        //     98: aload_0
        //     99: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     102: astore 12
        //     104: aload 12
        //     106: monitorenter
        //     107: aload_0
        //     108: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     111: getfield 783	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
        //     114: aload_1
        //     115: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     118: checkcast 785	com/android/server/pm/PackageSetting
        //     121: astore 14
        //     123: aload 14
        //     125: ifnonnull +107 -> 232
        //     128: aload_2
        //     129: ifnonnull +61 -> 190
        //     132: new 2866	java/lang/IllegalArgumentException
        //     135: dup
        //     136: new 662	java/lang/StringBuilder
        //     139: dup
        //     140: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     143: ldc_w 2878
        //     146: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     149: aload_1
        //     150: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     153: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     156: invokespecial 2869	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     159: astore 31
        //     161: aload 31
        //     163: athrow
        //     164: astore 13
        //     166: aload 12
        //     168: monitorexit
        //     169: aload 13
        //     171: athrow
        //     172: iconst_0
        //     173: istore 8
        //     175: goto -95 -> 80
        //     178: iconst_0
        //     179: istore 10
        //     181: goto -91 -> 90
        //     184: aload_2
        //     185: astore 11
        //     187: goto -89 -> 98
        //     190: new 2866	java/lang/IllegalArgumentException
        //     193: dup
        //     194: new 662	java/lang/StringBuilder
        //     197: dup
        //     198: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     201: ldc_w 2880
        //     204: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     207: aload_1
        //     208: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     211: ldc_w 1223
        //     214: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     217: aload_2
        //     218: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     221: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     224: invokespecial 2869	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     227: astore 32
        //     229: aload 32
        //     231: athrow
        //     232: iload 8
        //     234: ifne +75 -> 309
        //     237: iload 6
        //     239: aload 14
        //     241: getfield 2496	com/android/server/pm/PackageSetting:appId	I
        //     244: invokestatic 2884	android/os/UserId:isSameApp	(II)Z
        //     247: ifne +62 -> 309
        //     250: new 1184	java/lang/SecurityException
        //     253: dup
        //     254: new 662	java/lang/StringBuilder
        //     257: dup
        //     258: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     261: ldc_w 2886
        //     264: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     267: invokestatic 2889	android/os/Binder:getCallingPid	()I
        //     270: invokevirtual 902	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     273: ldc_w 2891
        //     276: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     279: iload 6
        //     281: invokevirtual 902	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     284: ldc_w 2893
        //     287: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     290: aload 14
        //     292: getfield 2496	com/android/server/pm/PackageSetting:appId	I
        //     295: invokevirtual 902	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     298: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     301: invokespecial 1192	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     304: astore 30
        //     306: aload 30
        //     308: athrow
        //     309: aload_2
        //     310: ifnonnull +163 -> 473
        //     313: aload 14
        //     315: iload 5
        //     317: invokevirtual 1620	com/android/server/pm/PackageSetting:getEnabled	(I)I
        //     320: iload_3
        //     321: if_icmpne +9 -> 330
        //     324: aload 12
        //     326: monitorexit
        //     327: goto +402 -> 729
        //     330: aload 14
        //     332: iload_3
        //     333: iload 5
        //     335: invokevirtual 2896	com/android/server/pm/PackageSetting:setEnabled	(II)V
        //     338: aload_0
        //     339: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     342: iload 5
        //     344: invokevirtual 2899	com/android/server/pm/Settings:writePackageRestrictionsLPr	(I)V
        //     347: iload 5
        //     349: aload 14
        //     351: getfield 2496	com/android/server/pm/PackageSetting:appId	I
        //     354: invokestatic 2816	android/os/UserId:getUid	(II)I
        //     357: istore 18
        //     359: aload_0
        //     360: getfield 437	com/android/server/pm/PackageManagerService:mPendingBroadcasts	Ljava/util/HashMap;
        //     363: aload_1
        //     364: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     367: checkcast 420	java/util/ArrayList
        //     370: astore 19
        //     372: aload 19
        //     374: ifnonnull +382 -> 756
        //     377: iconst_1
        //     378: istore 20
        //     380: iload 20
        //     382: ifeq +12 -> 394
        //     385: new 420	java/util/ArrayList
        //     388: dup
        //     389: invokespecial 421	java/util/ArrayList:<init>	()V
        //     392: astore 19
        //     394: aload 19
        //     396: aload 11
        //     398: invokevirtual 1989	java/util/ArrayList:contains	(Ljava/lang/Object;)Z
        //     401: ifne +11 -> 412
        //     404: aload 19
        //     406: aload 11
        //     408: invokevirtual 939	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     411: pop
        //     412: iload 4
        //     414: iconst_1
        //     415: iand
        //     416: ifne +255 -> 671
        //     419: iconst_1
        //     420: istore 9
        //     422: aload_0
        //     423: getfield 437	com/android/server/pm/PackageManagerService:mPendingBroadcasts	Ljava/util/HashMap;
        //     426: aload_1
        //     427: invokevirtual 2767	java/util/HashMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
        //     430: pop
        //     431: aload 12
        //     433: monitorexit
        //     434: invokestatic 2902	android/os/Binder:clearCallingIdentity	()J
        //     437: lstore 22
        //     439: iload 9
        //     441: ifeq +24 -> 465
        //     444: iload 4
        //     446: iconst_1
        //     447: iand
        //     448: ifeq +265 -> 713
        //     451: iconst_1
        //     452: istore 24
        //     454: aload_0
        //     455: aload_1
        //     456: iload 24
        //     458: aload 19
        //     460: iload 18
        //     462: invokespecial 1108	com/android/server/pm/PackageManagerService:sendPackageChangedBroadcast	(Ljava/lang/String;ZLjava/util/ArrayList;I)V
        //     465: lload 22
        //     467: invokestatic 2905	android/os/Binder:restoreCallingIdentity	(J)V
        //     470: goto +259 -> 729
        //     473: aload 14
        //     475: getfield 1259	com/android/server/pm/PackageSetting:pkg	Landroid/content/pm/PackageParser$Package;
        //     478: astore 15
        //     480: aload 15
        //     482: ifnull +12 -> 494
        //     485: aload 15
        //     487: aload_2
        //     488: invokevirtual 2908	android/content/pm/PackageParser$Package:hasComponentClassName	(Ljava/lang/String;)Z
        //     491: ifne +239 -> 730
        //     494: aload 15
        //     496: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     499: getfield 1811	android/content/pm/ApplicationInfo:targetSdkVersion	I
        //     502: bipush 16
        //     504: if_icmplt +45 -> 549
        //     507: new 2866	java/lang/IllegalArgumentException
        //     510: dup
        //     511: new 662	java/lang/StringBuilder
        //     514: dup
        //     515: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     518: ldc_w 2910
        //     521: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     524: aload_2
        //     525: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     528: ldc_w 2912
        //     531: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     534: aload_1
        //     535: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     538: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     541: invokespecial 2869	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     544: astore 16
        //     546: aload 16
        //     548: athrow
        //     549: ldc 186
        //     551: new 662	java/lang/StringBuilder
        //     554: dup
        //     555: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     558: ldc_w 2914
        //     561: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     564: aload_2
        //     565: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     568: ldc_w 2912
        //     571: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     574: aload_1
        //     575: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     578: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     581: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     584: pop
        //     585: goto +145 -> 730
        //     588: ldc 186
        //     590: new 662	java/lang/StringBuilder
        //     593: dup
        //     594: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     597: ldc_w 2868
        //     600: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     603: iload_3
        //     604: invokevirtual 902	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     607: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     610: invokestatic 1456	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     613: pop
        //     614: aload 12
        //     616: monitorexit
        //     617: goto +112 -> 729
        //     620: aload 14
        //     622: aload_2
        //     623: iload 5
        //     625: invokevirtual 2917	com/android/server/pm/PackageSetting:enableComponentLPw	(Ljava/lang/String;I)Z
        //     628: ifne -290 -> 338
        //     631: aload 12
        //     633: monitorexit
        //     634: goto +95 -> 729
        //     637: aload 14
        //     639: aload_2
        //     640: iload 5
        //     642: invokevirtual 2920	com/android/server/pm/PackageSetting:disableComponentLPw	(Ljava/lang/String;I)Z
        //     645: ifne -307 -> 338
        //     648: aload 12
        //     650: monitorexit
        //     651: goto +78 -> 729
        //     654: aload 14
        //     656: aload_2
        //     657: iload 5
        //     659: invokevirtual 2923	com/android/server/pm/PackageSetting:restoreComponentLPw	(Ljava/lang/String;I)Z
        //     662: ifne -324 -> 338
        //     665: aload 12
        //     667: monitorexit
        //     668: goto +61 -> 729
        //     671: iload 20
        //     673: ifeq +14 -> 687
        //     676: aload_0
        //     677: getfield 437	com/android/server/pm/PackageManagerService:mPendingBroadcasts	Ljava/util/HashMap;
        //     680: aload_1
        //     681: aload 19
        //     683: invokevirtual 2271	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     686: pop
        //     687: aload_0
        //     688: getfield 566	com/android/server/pm/PackageManagerService:mHandler	Lcom/android/server/pm/PackageManagerService$PackageHandler;
        //     691: iconst_1
        //     692: invokevirtual 2926	com/android/server/pm/PackageManagerService$PackageHandler:hasMessages	(I)Z
        //     695: ifne -264 -> 431
        //     698: aload_0
        //     699: getfield 566	com/android/server/pm/PackageManagerService:mHandler	Lcom/android/server/pm/PackageManagerService$PackageHandler;
        //     702: iconst_1
        //     703: ldc2_w 2927
        //     706: invokevirtual 2932	com/android/server/pm/PackageManagerService$PackageHandler:sendEmptyMessageDelayed	(IJ)Z
        //     709: pop
        //     710: goto -279 -> 431
        //     713: iconst_0
        //     714: istore 24
        //     716: goto -262 -> 454
        //     719: astore 25
        //     721: lload 22
        //     723: invokestatic 2905	android/os/Binder:restoreCallingIdentity	(J)V
        //     726: aload 25
        //     728: athrow
        //     729: return
        //     730: iload_3
        //     731: tableswitch	default:+-143 -> 588, 0:+-77->654, 1:+-111->620, 2:+-94->637
        //     757: istore 20
        //     759: goto -379 -> 380
        //
        // Exception table:
        //     from	to	target	type
        //     107	169	164	finally
        //     190	434	164	finally
        //     473	710	164	finally
        //     454	465	719	finally
    }

    static String[] splitString(String paramString, char paramChar)
    {
        int i = 1;
        int k;
        for (int j = 0; ; j = k + 1)
        {
            k = paramString.indexOf(paramChar, j);
            if (k < 0)
                break;
            i++;
        }
        String[] arrayOfString = new String[i];
        int m = 0;
        int n = 0;
        for (int i1 = 0; ; i1 = m)
        {
            int i2 = paramString.indexOf(paramChar, m);
            if (i2 < 0)
                break;
            arrayOfString[n] = paramString.substring(i1, i2);
            n++;
            m = i2 + 1;
        }
        arrayOfString[n] = paramString.substring(i1, paramString.length());
        return arrayOfString;
    }

    private void unloadAllContainers(Set<AsecInstallArgs> paramSet)
    {
        Iterator localIterator = paramSet.iterator();
        while (localIterator.hasNext())
        {
            AsecInstallArgs localAsecInstallArgs = (AsecInstallArgs)localIterator.next();
            synchronized (this.mInstallLock)
            {
                localAsecInstallArgs.doPostDeleteLI(false);
            }
        }
    }

    // ERROR //
    private void unloadMediaPackages(HashMap<AsecInstallArgs, String> paramHashMap, int[] paramArrayOfInt, final boolean paramBoolean)
    {
        // Byte code:
        //     0: new 420	java/util/ArrayList
        //     3: dup
        //     4: invokespecial 421	java/util/ArrayList:<init>	()V
        //     7: astore 4
        //     9: new 420	java/util/ArrayList
        //     12: dup
        //     13: invokespecial 421	java/util/ArrayList:<init>	()V
        //     16: astore 5
        //     18: aload_1
        //     19: invokevirtual 2053	java/util/HashMap:keySet	()Ljava/util/Set;
        //     22: astore 6
        //     24: aload 6
        //     26: invokeinterface 923 1 0
        //     31: astore 7
        //     33: aload 7
        //     35: invokeinterface 702 1 0
        //     40: ifeq +114 -> 154
        //     43: aload 7
        //     45: invokeinterface 706 1 0
        //     50: checkcast 42	com/android/server/pm/PackageManagerService$AsecInstallArgs
        //     53: astore 14
        //     55: aload 14
        //     57: invokevirtual 2939	com/android/server/pm/PackageManagerService$AsecInstallArgs:getPackageName	()Ljava/lang/String;
        //     60: astore 15
        //     62: new 36	com/android/server/pm/PackageManagerService$PackageRemovedInfo
        //     65: dup
        //     66: invokespecial 1505	com/android/server/pm/PackageManagerService$PackageRemovedInfo:<init>	()V
        //     69: astore 16
        //     71: aload_0
        //     72: getfield 371	com/android/server/pm/PackageManagerService:mInstallLock	Ljava/lang/Object;
        //     75: astore 17
        //     77: aload 17
        //     79: monitorenter
        //     80: aload_0
        //     81: aload 15
        //     83: iconst_0
        //     84: iconst_1
        //     85: aload 16
        //     87: iconst_0
        //     88: invokespecial 1530	com/android/server/pm/PackageManagerService:deletePackageLI	(Ljava/lang/String;ZILcom/android/server/pm/PackageManagerService$PackageRemovedInfo;Z)Z
        //     91: ifeq +25 -> 116
        //     94: aload 4
        //     96: aload 15
        //     98: invokevirtual 939	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     101: pop
        //     102: aload 17
        //     104: monitorexit
        //     105: goto -72 -> 33
        //     108: astore 18
        //     110: aload 17
        //     112: monitorexit
        //     113: aload 18
        //     115: athrow
        //     116: ldc 186
        //     118: new 662	java/lang/StringBuilder
        //     121: dup
        //     122: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     125: ldc_w 2941
        //     128: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     131: aload 15
        //     133: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     136: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     139: invokestatic 1456	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     142: pop
        //     143: aload 5
        //     145: aload 14
        //     147: invokevirtual 939	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     150: pop
        //     151: goto -49 -> 102
        //     154: aload_0
        //     155: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     158: astore 8
        //     160: aload 8
        //     162: monitorenter
        //     163: aload_0
        //     164: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     167: invokevirtual 951	com/android/server/pm/Settings:writeLPr	()V
        //     170: aload 8
        //     172: monitorexit
        //     173: aload 4
        //     175: invokevirtual 848	java/util/ArrayList:size	()I
        //     178: ifle +31 -> 209
        //     181: aload_0
        //     182: iconst_0
        //     183: aload 4
        //     185: aload_2
        //     186: new 8	com/android/server/pm/PackageManagerService$11
        //     189: dup
        //     190: aload_0
        //     191: iload_3
        //     192: aload 6
        //     194: invokespecial 2944	com/android/server/pm/PackageManagerService$11:<init>	(Lcom/android/server/pm/PackageManagerService;ZLjava/util/Set;)V
        //     197: invokespecial 1114	com/android/server/pm/PackageManagerService:sendResourcesChangedBroadcast	(ZLjava/util/ArrayList;[ILandroid/content/IIntentReceiver;)V
        //     200: return
        //     201: astore 9
        //     203: aload 8
        //     205: monitorexit
        //     206: aload 9
        //     208: athrow
        //     209: aload_0
        //     210: getfield 566	com/android/server/pm/PackageManagerService:mHandler	Lcom/android/server/pm/PackageManagerService$PackageHandler;
        //     213: astore 10
        //     215: iload_3
        //     216: ifeq +34 -> 250
        //     219: iconst_1
        //     220: istore 11
        //     222: aload 10
        //     224: bipush 12
        //     226: iload 11
        //     228: bipush 255
        //     230: aload 6
        //     232: invokevirtual 2948	com/android/server/pm/PackageManagerService$PackageHandler:obtainMessage	(IIILjava/lang/Object;)Landroid/os/Message;
        //     235: astore 12
        //     237: aload_0
        //     238: getfield 566	com/android/server/pm/PackageManagerService:mHandler	Lcom/android/server/pm/PackageManagerService$PackageHandler;
        //     241: aload 12
        //     243: invokevirtual 2952	com/android/server/pm/PackageManagerService$PackageHandler:sendMessage	(Landroid/os/Message;)Z
        //     246: pop
        //     247: goto -47 -> 200
        //     250: iconst_0
        //     251: istore 11
        //     253: goto -31 -> 222
        //
        // Exception table:
        //     from	to	target	type
        //     80	113	108	finally
        //     116	151	108	finally
        //     163	173	201	finally
        //     203	206	201	finally
    }

    private void updateExternalMediaStatusInner(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
    {
        int[] arrayOfInt1 = null;
        HashSet localHashSet = new HashSet();
        HashMap localHashMap1 = new HashMap();
        String[] arrayOfString = PackageHelper.getSecureContainerList();
        if ((arrayOfString == null) || (arrayOfString.length == 0))
            Log.i("PackageManager", "No secure containers on sdcard");
        while (true)
        {
            label64: int[] arrayOfInt2;
            if (paramBoolean1)
            {
                loadMediaPackages(localHashMap1, arrayOfInt1, localHashSet);
                startCleaningPackages();
                return;
                arrayOfInt2 = new int[arrayOfString.length];
            }
            int k;
            int i2;
            label453: label460: label475: synchronized (this.mPackages)
            {
                int i = arrayOfString.length;
                int j = 0;
                k = 0;
                if (j < i);
                try
                {
                    String str1 = arrayOfString[j];
                    String str2 = getAsecPackageName(str1);
                    if (str2 == null)
                    {
                        localHashSet.add(str1);
                        i2 = k;
                    }
                    else
                    {
                        PackageSetting localPackageSetting = (PackageSetting)this.mSettings.mPackages.get(str2);
                        if (localPackageSetting == null)
                        {
                            Log.i("PackageManager", "Deleting container with no matching settings " + str1);
                            localHashSet.add(str1);
                            i2 = k;
                        }
                        else if ((paramBoolean3) && (!paramBoolean1) && (!isExternal(localPackageSetting)))
                        {
                            i2 = k;
                        }
                        else
                        {
                            AsecInstallArgs localAsecInstallArgs = new AsecInstallArgs(str1, isForwardLocked(localPackageSetting));
                            if ((localPackageSetting.codePathString != null) && (localPackageSetting.codePathString.equals(localAsecInstallArgs.getCodePath())))
                            {
                                localHashMap1.put(localAsecInstallArgs, localPackageSetting.codePathString);
                                int i3 = localPackageSetting.appId;
                                if (i3 == -1)
                                    break label475;
                                i2 = k + 1;
                                arrayOfInt2[k] = i3;
                                break label460;
                                Object localObject1;
                                throw localObject1;
                            }
                            else
                            {
                                Log.i("PackageManager", "Deleting stale container for " + str1);
                                localHashSet.add(str1);
                                break label475;
                                if (k <= 0)
                                    continue;
                                Arrays.sort(arrayOfInt2, 0, k);
                                arrayOfInt1 = new int[k];
                                arrayOfInt1[0] = arrayOfInt2[0];
                                int m = 1;
                                for (n = 0; m < k; n = i1)
                                {
                                    if (arrayOfInt2[(m - 1)] == arrayOfInt2[m])
                                        break label453;
                                    i1 = n + 1;
                                    arrayOfInt1[n] = arrayOfInt2[m];
                                    m++;
                                }
                                unloadMediaPackages(localHashMap1, arrayOfInt1, paramBoolean2);
                                break label64;
                            }
                        }
                    }
                }
                finally
                {
                    while (true)
                    {
                        int n;
                        continue;
                        int i1 = n;
                    }
                }
                j++;
                k = i2;
            }
        }
    }

    private void updatePermissionsLPw(String paramString, PackageParser.Package paramPackage, int paramInt)
    {
        boolean bool1 = true;
        Iterator localIterator1 = this.mSettings.mPermissionTrees.values().iterator();
        while (localIterator1.hasNext())
        {
            BasePermission localBasePermission3 = (BasePermission)localIterator1.next();
            if (localBasePermission3.packageSetting == null)
                localBasePermission3.packageSetting = ((PackageSettingBase)this.mSettings.mPackages.get(localBasePermission3.sourcePackage));
            if (localBasePermission3.packageSetting == null)
            {
                Slog.w("PackageManager", "Removing dangling permission tree: " + localBasePermission3.name + " from package " + localBasePermission3.sourcePackage);
                localIterator1.remove();
            }
            else if ((paramString != null) && (paramString.equals(localBasePermission3.sourcePackage)) && ((paramPackage == null) || (!hasPermission(paramPackage, localBasePermission3.name))))
            {
                Slog.i("PackageManager", "Removing old permission tree: " + localBasePermission3.name + " from package " + localBasePermission3.sourcePackage);
                paramInt |= 1;
                localIterator1.remove();
            }
        }
        Iterator localIterator2 = this.mSettings.mPermissions.values().iterator();
        while (localIterator2.hasNext())
        {
            BasePermission localBasePermission1 = (BasePermission)localIterator2.next();
            if ((localBasePermission1.type == 2) && (localBasePermission1.packageSetting == null) && (localBasePermission1.pendingInfo != null))
            {
                BasePermission localBasePermission2 = findPermissionTreeLP(localBasePermission1.name);
                if ((localBasePermission2 != null) && (localBasePermission2.perm != null))
                {
                    localBasePermission1.packageSetting = localBasePermission2.packageSetting;
                    localBasePermission1.perm = new PackageParser.Permission(localBasePermission2.perm.owner, new PermissionInfo(localBasePermission1.pendingInfo));
                    localBasePermission1.perm.info.packageName = localBasePermission2.perm.info.packageName;
                    localBasePermission1.perm.info.name = localBasePermission1.name;
                    localBasePermission1.uid = localBasePermission2.uid;
                }
            }
            if (localBasePermission1.packageSetting == null)
                localBasePermission1.packageSetting = ((PackageSettingBase)this.mSettings.mPackages.get(localBasePermission1.sourcePackage));
            if (localBasePermission1.packageSetting == null)
            {
                Slog.w("PackageManager", "Removing dangling permission: " + localBasePermission1.name + " from package " + localBasePermission1.sourcePackage);
                localIterator2.remove();
            }
            else if ((paramString != null) && (paramString.equals(localBasePermission1.sourcePackage)) && ((paramPackage == null) || (!hasPermission(paramPackage, localBasePermission1.name))))
            {
                Slog.i("PackageManager", "Removing old permission: " + localBasePermission1.name + " from package " + localBasePermission1.sourcePackage);
                paramInt |= 1;
                localIterator2.remove();
            }
        }
        if ((paramInt & 0x1) != 0)
        {
            Iterator localIterator3 = this.mPackages.values().iterator();
            while (localIterator3.hasNext())
            {
                PackageParser.Package localPackage = (PackageParser.Package)localIterator3.next();
                if (localPackage != paramPackage)
                {
                    if ((paramInt & 0x4) != 0);
                    for (boolean bool2 = bool1; ; bool2 = false)
                    {
                        grantPermissionsLPw(localPackage, bool2);
                        break;
                    }
                }
            }
        }
        if (paramPackage != null)
            if ((paramInt & 0x2) == 0)
                break label679;
        while (true)
        {
            grantPermissionsLPw(paramPackage, bool1);
            return;
            label679: bool1 = false;
        }
    }

    private void updateSettingsLI(PackageParser.Package paramPackage, String paramString, PackageInstalledInfo paramPackageInstalledInfo)
    {
        int i = 1;
        String str1 = paramPackage.packageName;
        synchronized (this.mPackages)
        {
            this.mSettings.setInstallStatus(str1, 0);
            this.mSettings.writeLPr();
            int j = moveDexFilesLI(paramPackage);
            paramPackageInstalledInfo.returnCode = j;
            if (j != i)
                return;
        }
        Log.d("PackageManager", "New package installed in " + paramPackage.mPath);
        while (true)
        {
            synchronized (this.mPackages)
            {
                String str2 = paramPackage.packageName;
                if (paramPackage.permissions.size() > 0)
                {
                    updatePermissionsLPw(str2, paramPackage, i | 0x2);
                    paramPackageInstalledInfo.name = str1;
                    paramPackageInstalledInfo.uid = paramPackage.applicationInfo.uid;
                    paramPackageInstalledInfo.pkg = paramPackage;
                    this.mSettings.setInstallStatus(str1, 1);
                    this.mSettings.setInstallerPackageName(str1, paramString);
                    paramPackageInstalledInfo.returnCode = 1;
                    this.mSettings.writeLPr();
                }
            }
            i = 0;
        }
    }

    private boolean verifyPackageUpdateLPr(PackageSetting paramPackageSetting, PackageParser.Package paramPackage)
    {
        boolean bool = false;
        if ((0x1 & paramPackageSetting.pkgFlags) == 0)
            Slog.w("PackageManager", "Unable to update from " + paramPackageSetting.name + " to " + paramPackage.packageName + ": old package not in system partition");
        while (true)
        {
            return bool;
            if (this.mPackages.get(paramPackageSetting.name) != null)
                Slog.w("PackageManager", "Unable to update from " + paramPackageSetting.name + " to " + paramPackage.packageName + ": old package still exists");
            else
                bool = true;
        }
    }

    private boolean verifySignaturesLP(PackageSetting paramPackageSetting, PackageParser.Package paramPackage)
    {
        boolean bool = false;
        if ((paramPackageSetting.signatures.mSignatures != null) && (compareSignatures(paramPackageSetting.signatures.mSignatures, paramPackage.mSignatures) != 0))
        {
            Slog.e("PackageManager", "Package " + paramPackage.packageName + " signatures do not match the previously installed version; ignoring!");
            this.mLastScanError = -7;
        }
        while (true)
        {
            return bool;
            if ((paramPackageSetting.sharedUser != null) && (paramPackageSetting.sharedUser.signatures.mSignatures != null) && (compareSignatures(paramPackageSetting.sharedUser.signatures.mSignatures, paramPackage.mSignatures) != 0))
            {
                Slog.e("PackageManager", "Package " + paramPackage.packageName + " has no signatures that match those in shared user " + paramPackageSetting.sharedUser.name + "; ignoring!");
                this.mLastScanError = -8;
            }
            else
            {
                bool = true;
            }
        }
    }

    public void addPackageToPreferred(String paramString)
    {
        Slog.w("PackageManager", "addPackageToPreferred: this is now a no-op");
    }

    public boolean addPermission(PermissionInfo paramPermissionInfo)
    {
        synchronized (this.mPackages)
        {
            boolean bool = addPermissionLocked(paramPermissionInfo, false);
            return bool;
        }
    }

    public boolean addPermissionAsync(PermissionInfo paramPermissionInfo)
    {
        synchronized (this.mPackages)
        {
            boolean bool = addPermissionLocked(paramPermissionInfo, true);
            return bool;
        }
    }

    boolean addPermissionLocked(PermissionInfo paramPermissionInfo, boolean paramBoolean)
    {
        if ((paramPermissionInfo.labelRes == 0) && (paramPermissionInfo.nonLocalizedLabel == null))
            throw new SecurityException("Label must be specified in permission");
        BasePermission localBasePermission1 = checkPermissionTreeLP(paramPermissionInfo.name);
        BasePermission localBasePermission2 = (BasePermission)this.mSettings.mPermissions.get(paramPermissionInfo.name);
        boolean bool;
        int i;
        int j;
        if (localBasePermission2 == null)
        {
            bool = true;
            i = 1;
            j = PermissionInfo.fixProtectionLevel(paramPermissionInfo.protectionLevel);
            if (!bool)
                break label219;
            localBasePermission2 = new BasePermission(paramPermissionInfo.name, localBasePermission1.sourcePackage, 2);
            label96: localBasePermission2.protectionLevel = j;
            PermissionInfo localPermissionInfo = new PermissionInfo(paramPermissionInfo);
            localPermissionInfo.protectionLevel = j;
            localBasePermission2.perm = new PackageParser.Permission(localBasePermission1.perm.owner, localPermissionInfo);
            localBasePermission2.perm.info.packageName = localBasePermission1.perm.info.packageName;
            localBasePermission2.uid = localBasePermission1.uid;
            if (bool)
                this.mSettings.mPermissions.put(localPermissionInfo.name, localBasePermission2);
            if (i != 0)
            {
                if (paramBoolean)
                    break label323;
                this.mSettings.writeLPr();
            }
        }
        while (true)
        {
            return bool;
            bool = false;
            break;
            label219: if (localBasePermission2.type != 2)
                throw new SecurityException("Not allowed to modify non-dynamic permission " + paramPermissionInfo.name);
            if ((localBasePermission2.protectionLevel != j) || (!localBasePermission2.perm.owner.equals(localBasePermission1.perm.owner)) || (localBasePermission2.uid != localBasePermission1.uid) || (!comparePermissionInfos(localBasePermission2.perm.info, paramPermissionInfo)))
                break label96;
            i = 0;
            break label96;
            label323: scheduleWriteSettingsLocked();
        }
    }

    public void addPreferredActivity(IntentFilter paramIntentFilter, int paramInt, ComponentName[] paramArrayOfComponentName, ComponentName paramComponentName)
    {
        synchronized (this.mPackages)
        {
            if (this.mContext.checkCallingOrSelfPermission("android.permission.SET_PREFERRED_APPLICATIONS") != 0)
            {
                if (getUidTargetSdkVersionLockedLPr(Binder.getCallingUid()) < 8)
                    Slog.w("PackageManager", "Ignoring addPreferredActivity() from uid " + Binder.getCallingUid());
                else
                    this.mContext.enforceCallingOrSelfPermission("android.permission.SET_PREFERRED_APPLICATIONS", null);
            }
            else
            {
                Slog.i("PackageManager", "Adding preferred activity " + paramComponentName + ":");
                paramIntentFilter.dump(new LogPrinter(4, "PackageManager"), "    ");
                this.mSettings.mPreferredActivities.addFilter(new PreferredActivity(paramIntentFilter, paramInt, paramArrayOfComponentName, paramComponentName));
                scheduleWriteSettingsLocked();
            }
        }
    }

    public String[] canonicalToCurrentPackageNames(String[] paramArrayOfString)
    {
        String[] arrayOfString = new String[paramArrayOfString.length];
        synchronized (this.mPackages)
        {
            int i = -1 + paramArrayOfString.length;
            if (i >= 0)
            {
                String str = (String)this.mSettings.mRenamedPackages.get(paramArrayOfString[i]);
                if (str != null);
                while (true)
                {
                    arrayOfString[i] = str;
                    i--;
                    break;
                    str = paramArrayOfString[i];
                }
            }
            return arrayOfString;
        }
    }

    // ERROR //
    public int checkPermission(String paramString1, String paramString2)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore_3
        //     2: aload_0
        //     3: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     6: astore 4
        //     8: aload 4
        //     10: monitorenter
        //     11: aload_0
        //     12: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     15: aload_2
        //     16: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     19: checkcast 800	android/content/pm/PackageParser$Package
        //     22: astore 6
        //     24: aload 6
        //     26: ifnull +76 -> 102
        //     29: aload 6
        //     31: getfield 1827	android/content/pm/PackageParser$Package:mExtras	Ljava/lang/Object;
        //     34: ifnull +68 -> 102
        //     37: aload 6
        //     39: getfield 1827	android/content/pm/PackageParser$Package:mExtras	Ljava/lang/Object;
        //     42: checkcast 785	com/android/server/pm/PackageSetting
        //     45: astore 7
        //     47: aload 7
        //     49: getfield 1831	com/android/server/pm/PackageSetting:sharedUser	Lcom/android/server/pm/SharedUserSetting;
        //     52: ifnull +24 -> 76
        //     55: aload 7
        //     57: getfield 1831	com/android/server/pm/PackageSetting:sharedUser	Lcom/android/server/pm/SharedUserSetting;
        //     60: getfield 1750	com/android/server/pm/GrantedPermissions:grantedPermissions	Ljava/util/HashSet;
        //     63: aload_1
        //     64: invokevirtual 721	java/util/HashSet:contains	(Ljava/lang/Object;)Z
        //     67: ifeq +35 -> 102
        //     70: aload 4
        //     72: monitorexit
        //     73: goto +49 -> 122
        //     76: aload 7
        //     78: getfield 1750	com/android/server/pm/GrantedPermissions:grantedPermissions	Ljava/util/HashSet;
        //     81: aload_1
        //     82: invokevirtual 721	java/util/HashSet:contains	(Ljava/lang/Object;)Z
        //     85: ifeq +17 -> 102
        //     88: aload 4
        //     90: monitorexit
        //     91: goto +31 -> 122
        //     94: astore 5
        //     96: aload 4
        //     98: monitorexit
        //     99: aload 5
        //     101: athrow
        //     102: aload_0
        //     103: aload_1
        //     104: invokespecial 3082	com/android/server/pm/PackageManagerService:isPermissionEnforcedLocked	(Ljava/lang/String;)Z
        //     107: ifne +9 -> 116
        //     110: aload 4
        //     112: monitorexit
        //     113: goto +9 -> 122
        //     116: aload 4
        //     118: monitorexit
        //     119: bipush 255
        //     121: istore_3
        //     122: iload_3
        //     123: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     11	99	94	finally
        //     102	119	94	finally
    }

    public int checkSignatures(String paramString1, String paramString2)
    {
        int i;
        synchronized (this.mPackages)
        {
            PackageParser.Package localPackage1 = (PackageParser.Package)this.mPackages.get(paramString1);
            PackageParser.Package localPackage2 = (PackageParser.Package)this.mPackages.get(paramString2);
            if ((localPackage1 == null) || (localPackage1.mExtras == null) || (localPackage2 == null) || (localPackage2.mExtras == null))
                i = -4;
            else
                i = compareSignatures(localPackage1.mSignatures, localPackage2.mSignatures);
        }
        return i;
    }

    // ERROR //
    public int checkUidPermission(String paramString, int paramInt)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore_3
        //     2: aload_0
        //     3: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     6: astore 4
        //     8: aload 4
        //     10: monitorenter
        //     11: aload_0
        //     12: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     15: iload_2
        //     16: invokestatic 1182	android/os/UserId:getAppId	(I)I
        //     19: invokevirtual 1802	com/android/server/pm/Settings:getUserIdLPr	(I)Ljava/lang/Object;
        //     22: astore 6
        //     24: aload 6
        //     26: ifnull +24 -> 50
        //     29: aload 6
        //     31: checkcast 787	com/android/server/pm/GrantedPermissions
        //     34: getfield 1750	com/android/server/pm/GrantedPermissions:grantedPermissions	Ljava/util/HashSet;
        //     37: aload_1
        //     38: invokevirtual 721	java/util/HashSet:contains	(Ljava/lang/Object;)Z
        //     41: ifeq +50 -> 91
        //     44: aload 4
        //     46: monitorexit
        //     47: goto +64 -> 111
        //     50: aload_0
        //     51: getfield 385	com/android/server/pm/PackageManagerService:mSystemPermissions	Landroid/util/SparseArray;
        //     54: iload_2
        //     55: invokevirtual 2256	android/util/SparseArray:get	(I)Ljava/lang/Object;
        //     58: checkcast 411	java/util/HashSet
        //     61: astore 7
        //     63: aload 7
        //     65: ifnull +26 -> 91
        //     68: aload 7
        //     70: aload_1
        //     71: invokevirtual 721	java/util/HashSet:contains	(Ljava/lang/Object;)Z
        //     74: ifeq +17 -> 91
        //     77: aload 4
        //     79: monitorexit
        //     80: goto +31 -> 111
        //     83: astore 5
        //     85: aload 4
        //     87: monitorexit
        //     88: aload 5
        //     90: athrow
        //     91: aload_0
        //     92: aload_1
        //     93: invokespecial 3082	com/android/server/pm/PackageManagerService:isPermissionEnforcedLocked	(Ljava/lang/String;)Z
        //     96: ifne +9 -> 105
        //     99: aload 4
        //     101: monitorexit
        //     102: goto +9 -> 111
        //     105: aload 4
        //     107: monitorexit
        //     108: bipush 255
        //     110: istore_3
        //     111: iload_3
        //     112: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     11	88	83	finally
        //     91	108	83	finally
    }

    // ERROR //
    public int checkUidSignatures(int paramInt1, int paramInt2)
    {
        // Byte code:
        //     0: bipush 252
        //     2: istore_3
        //     3: iload_1
        //     4: invokestatic 1182	android/os/UserId:getAppId	(I)I
        //     7: istore 4
        //     9: iload_2
        //     10: invokestatic 1182	android/os/UserId:getAppId	(I)I
        //     13: istore 5
        //     15: aload_0
        //     16: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     19: astore 6
        //     21: aload 6
        //     23: monitorenter
        //     24: aload_0
        //     25: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     28: iload 4
        //     30: invokevirtual 1802	com/android/server/pm/Settings:getUserIdLPr	(I)Ljava/lang/Object;
        //     33: astore 8
        //     35: aload 8
        //     37: ifnull +113 -> 150
        //     40: aload 8
        //     42: instanceof 1804
        //     45: ifeq +67 -> 112
        //     48: aload 8
        //     50: checkcast 1804	com/android/server/pm/SharedUserSetting
        //     53: getfield 2500	com/android/server/pm/SharedUserSetting:signatures	Lcom/android/server/pm/PackageSignatures;
        //     56: getfield 1344	com/android/server/pm/PackageSignatures:mSignatures	[Landroid/content/pm/Signature;
        //     59: astore 9
        //     61: aload_0
        //     62: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     65: iload 5
        //     67: invokevirtual 1802	com/android/server/pm/Settings:getUserIdLPr	(I)Ljava/lang/Object;
        //     70: astore 10
        //     72: aload 10
        //     74: ifnull +112 -> 186
        //     77: aload 10
        //     79: instanceof 1804
        //     82: ifeq +74 -> 156
        //     85: aload 10
        //     87: checkcast 1804	com/android/server/pm/SharedUserSetting
        //     90: getfield 2500	com/android/server/pm/SharedUserSetting:signatures	Lcom/android/server/pm/PackageSignatures;
        //     93: getfield 1344	com/android/server/pm/PackageSignatures:mSignatures	[Landroid/content/pm/Signature;
        //     96: astore 11
        //     98: aload 9
        //     100: aload 11
        //     102: invokestatic 1874	com/android/server/pm/PackageManagerService:compareSignatures	([Landroid/content/pm/Signature;[Landroid/content/pm/Signature;)I
        //     105: istore_3
        //     106: aload 6
        //     108: monitorexit
        //     109: goto +80 -> 189
        //     112: aload 8
        //     114: instanceof 785
        //     117: ifeq +19 -> 136
        //     120: aload 8
        //     122: checkcast 785	com/android/server/pm/PackageSetting
        //     125: getfield 1338	com/android/server/pm/PackageSettingBase:signatures	Lcom/android/server/pm/PackageSignatures;
        //     128: getfield 1344	com/android/server/pm/PackageSignatures:mSignatures	[Landroid/content/pm/Signature;
        //     131: astore 9
        //     133: goto -72 -> 61
        //     136: aload 6
        //     138: monitorexit
        //     139: goto +50 -> 189
        //     142: astore 7
        //     144: aload 6
        //     146: monitorexit
        //     147: aload 7
        //     149: athrow
        //     150: aload 6
        //     152: monitorexit
        //     153: goto +36 -> 189
        //     156: aload 10
        //     158: instanceof 785
        //     161: ifeq +19 -> 180
        //     164: aload 10
        //     166: checkcast 785	com/android/server/pm/PackageSetting
        //     169: getfield 1338	com/android/server/pm/PackageSettingBase:signatures	Lcom/android/server/pm/PackageSignatures;
        //     172: getfield 1344	com/android/server/pm/PackageSignatures:mSignatures	[Landroid/content/pm/Signature;
        //     175: astore 11
        //     177: goto -79 -> 98
        //     180: aload 6
        //     182: monitorexit
        //     183: goto +6 -> 189
        //     186: aload 6
        //     188: monitorexit
        //     189: iload_3
        //     190: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     24	147	142	finally
        //     150	189	142	finally
    }

    void cleanupInstallFailedPackage(PackageSetting paramPackageSetting)
    {
        Slog.i("PackageManager", "Cleaning up incompletely installed app: " + paramPackageSetting.name);
        int i = this.mInstaller.remove(paramPackageSetting.name, 0);
        if (i < 0)
            Slog.w("PackageManager", "Couldn't remove app data directory for package: " + paramPackageSetting.name + ", retcode=" + i);
        while (true)
        {
            if ((paramPackageSetting.codePath != null) && (!paramPackageSetting.codePath.delete()))
                Slog.w("PackageManager", "Unable to remove old code file: " + paramPackageSetting.codePath);
            if ((paramPackageSetting.resourcePath != null) && (!paramPackageSetting.resourcePath.delete()) && (!paramPackageSetting.resourcePath.equals(paramPackageSetting.codePath)))
                Slog.w("PackageManager", "Unable to remove old code file: " + paramPackageSetting.resourcePath);
            this.mSettings.removePackageLPw(paramPackageSetting.name);
            return;
            sUserManager.removePackageForAllUsers(paramPackageSetting.name);
        }
    }

    public void clearApplicationUserData(final String paramString, final IPackageDataObserver paramIPackageDataObserver, final int paramInt)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CLEAR_APP_USER_DATA", null);
        checkValidCaller(Binder.getCallingUid(), paramInt);
        this.mHandler.post(new Runnable()
        {
            public void run()
            {
                PackageManagerService.this.mHandler.removeCallbacks(this);
                boolean bool;
                synchronized (PackageManagerService.this.mInstallLock)
                {
                    bool = PackageManagerService.this.clearApplicationUserDataLI(paramString, paramInt);
                    PackageManagerService.this.clearExternalStorageDataSync(paramString, true);
                    if (bool)
                    {
                        DeviceStorageMonitorService localDeviceStorageMonitorService = (DeviceStorageMonitorService)ServiceManager.getService("devicestoragemonitor");
                        if (localDeviceStorageMonitorService != null)
                            localDeviceStorageMonitorService.updateMemory();
                    }
                    if (paramIPackageDataObserver == null);
                }
                try
                {
                    paramIPackageDataObserver.onRemoveCompleted(paramString, bool);
                    return;
                    localObject2 = finally;
                    throw localObject2;
                }
                catch (RemoteException localRemoteException)
                {
                    while (true)
                        Log.i("PackageManager", "Observer no longer exists.");
                }
            }
        });
    }

    public void clearPackagePreferredActivities(String paramString)
    {
        int i = Binder.getCallingUid();
        synchronized (this.mPackages)
        {
            PackageParser.Package localPackage = (PackageParser.Package)this.mPackages.get(paramString);
            if (((localPackage == null) || (localPackage.applicationInfo.uid != i)) && (this.mContext.checkCallingOrSelfPermission("android.permission.SET_PREFERRED_APPLICATIONS") != 0))
            {
                if (getUidTargetSdkVersionLockedLPr(Binder.getCallingUid()) < 8)
                    Slog.w("PackageManager", "Ignoring clearPackagePreferredActivities() from uid " + Binder.getCallingUid());
                else
                    this.mContext.enforceCallingOrSelfPermission("android.permission.SET_PREFERRED_APPLICATIONS", null);
            }
            else if (clearPackagePreferredActivitiesLPw(paramString))
                scheduleWriteSettingsLocked();
        }
    }

    boolean clearPackagePreferredActivitiesLPw(String paramString)
    {
        ArrayList localArrayList = null;
        Iterator localIterator = this.mSettings.mPreferredActivities.filterIterator();
        while (localIterator.hasNext())
        {
            PreferredActivity localPreferredActivity2 = (PreferredActivity)localIterator.next();
            if (localPreferredActivity2.mPref.mComponent.getPackageName().equals(paramString))
            {
                if (localArrayList == null)
                    localArrayList = new ArrayList();
                localArrayList.add(localPreferredActivity2);
            }
        }
        if (localArrayList != null)
            for (int i = 0; i < localArrayList.size(); i++)
            {
                PreferredActivity localPreferredActivity1 = (PreferredActivity)localArrayList.get(i);
                this.mSettings.mPreferredActivities.removeFilter(localPreferredActivity1);
            }
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public UserInfo createUser(String paramString, int paramInt)
    {
        enforceSystemOrRoot("Only the system can create users");
        UserInfo localUserInfo = sUserManager.createUser(paramString, paramInt);
        if (localUserInfo != null)
        {
            Intent localIntent = new Intent("android.intent.action.USER_ADDED");
            localIntent.putExtra("android.intent.extra.user_id", localUserInfo.id);
            this.mContext.sendBroadcast(localIntent, "android.permission.MANAGE_ACCOUNTS");
        }
        return localUserInfo;
    }

    public String[] currentToCanonicalPackageNames(String[] paramArrayOfString)
    {
        String[] arrayOfString = new String[paramArrayOfString.length];
        synchronized (this.mPackages)
        {
            int i = -1 + paramArrayOfString.length;
            if (i >= 0)
            {
                PackageSetting localPackageSetting = (PackageSetting)this.mSettings.mPackages.get(paramArrayOfString[i]);
                if ((localPackageSetting != null) && (localPackageSetting.realName != null));
                for (String str = localPackageSetting.realName; ; str = paramArrayOfString[i])
                {
                    arrayOfString[i] = str;
                    i--;
                    break;
                }
            }
            return arrayOfString;
        }
    }

    public void deleteApplicationCacheFiles(final String paramString, final IPackageDataObserver paramIPackageDataObserver)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.DELETE_CACHE_FILES", null);
        final int i = UserId.getCallingUserId();
        this.mHandler.post(new Runnable()
        {
            public void run()
            {
                PackageManagerService.this.mHandler.removeCallbacks(this);
                boolean bool;
                synchronized (PackageManagerService.this.mInstallLock)
                {
                    bool = PackageManagerService.this.deleteApplicationCacheFilesLI(paramString, i);
                    PackageManagerService.this.clearExternalStorageDataSync(paramString, false);
                    if (paramIPackageDataObserver == null);
                }
                try
                {
                    paramIPackageDataObserver.onRemoveCompleted(paramString, bool);
                    return;
                    localObject2 = finally;
                    throw localObject2;
                }
                catch (RemoteException localRemoteException)
                {
                    while (true)
                        Log.i("PackageManager", "Observer no longer exists.");
                }
            }
        });
    }

    public void deletePackage(final String paramString, final IPackageDeleteObserver paramIPackageDeleteObserver, final int paramInt)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.DELETE_PACKAGES", null);
        this.mHandler.post(new Runnable()
        {
            public void run()
            {
                PackageManagerService.this.mHandler.removeCallbacks(this);
                int i = PackageManagerService.this.deletePackageX(paramString, true, true, paramInt);
                if (paramIPackageDeleteObserver != null);
                try
                {
                    paramIPackageDeleteObserver.packageDeleted(paramString, i);
                    return;
                }
                catch (RemoteException localRemoteException)
                {
                    while (true)
                        Log.i("PackageManager", "Observer no longer exists.");
                }
            }
        });
    }

    // ERROR //
    protected void dump(java.io.FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 474	com/android/server/pm/PackageManagerService:mContext	Landroid/content/Context;
        //     4: ldc_w 3147
        //     7: invokevirtual 3049	android/content/Context:checkCallingOrSelfPermission	(Ljava/lang/String;)I
        //     10: ifeq +54 -> 64
        //     13: aload_2
        //     14: new 662	java/lang/StringBuilder
        //     17: dup
        //     18: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     21: ldc_w 3149
        //     24: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     27: invokestatic 2889	android/os/Binder:getCallingPid	()I
        //     30: invokevirtual 902	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     33: ldc_w 2891
        //     36: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     39: invokestatic 1176	android/os/Binder:getCallingUid	()I
        //     42: invokevirtual 902	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     45: ldc_w 3151
        //     48: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     51: ldc_w 3147
        //     54: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     57: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     60: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     63: return
        //     64: new 30	com/android/server/pm/PackageManagerService$DumpState
        //     67: dup
        //     68: invokespecial 3152	com/android/server/pm/PackageManagerService$DumpState:<init>	()V
        //     71: astore 4
        //     73: aconst_null
        //     74: astore 5
        //     76: iconst_0
        //     77: istore 6
        //     79: aload_3
        //     80: arraylength
        //     81: istore 7
        //     83: iload 6
        //     85: iload 7
        //     87: if_icmpge +33 -> 120
        //     90: aload_3
        //     91: iload 6
        //     93: aaload
        //     94: astore 51
        //     96: aload 51
        //     98: ifnull +22 -> 120
        //     101: aload 51
        //     103: invokevirtual 521	java/lang/String:length	()I
        //     106: ifle +14 -> 120
        //     109: aload 51
        //     111: iconst_0
        //     112: invokevirtual 1601	java/lang/String:charAt	(I)C
        //     115: bipush 45
        //     117: if_icmpeq +259 -> 376
        //     120: aload_3
        //     121: arraylength
        //     122: istore 8
        //     124: iload 6
        //     126: iload 8
        //     128: if_icmpge +40 -> 168
        //     131: aload_3
        //     132: iload 6
        //     134: aaload
        //     135: astore 49
        //     137: iload 6
        //     139: iconst_1
        //     140: iadd
        //     141: pop
        //     142: ldc_w 2381
        //     145: aload 49
        //     147: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     150: ifne +14 -> 164
        //     153: aload 49
        //     155: ldc_w 1663
        //     158: invokevirtual 3155	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
        //     161: ifeq +416 -> 577
        //     164: aload 49
        //     166: astore 5
        //     168: aload_0
        //     169: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     172: astore 9
        //     174: aload 9
        //     176: monitorenter
        //     177: aload 4
        //     179: sipush 256
        //     182: invokevirtual 3158	com/android/server/pm/PackageManagerService$DumpState:isDumping	(I)Z
        //     185: ifeq +72 -> 257
        //     188: aload 5
        //     190: ifnonnull +67 -> 257
        //     193: aload 4
        //     195: invokevirtual 3161	com/android/server/pm/PackageManagerService$DumpState:onTitlePrinted	()Z
        //     198: ifeq +10 -> 208
        //     201: aload_2
        //     202: ldc_w 3163
        //     205: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     208: aload_2
        //     209: ldc_w 3165
        //     212: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     215: aload_2
        //     216: ldc_w 3167
        //     219: invokevirtual 3170	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     222: aload_2
        //     223: aload_0
        //     224: getfield 965	com/android/server/pm/PackageManagerService:mRequiredVerifierPackage	Ljava/lang/String;
        //     227: invokevirtual 3170	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     230: aload_2
        //     231: ldc_w 3172
        //     234: invokevirtual 3170	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     237: aload_2
        //     238: aload_0
        //     239: aload_0
        //     240: getfield 965	com/android/server/pm/PackageManagerService:mRequiredVerifierPackage	Ljava/lang/String;
        //     243: iconst_0
        //     244: invokevirtual 3175	com/android/server/pm/PackageManagerService:getPackageUid	(Ljava/lang/String;I)I
        //     247: invokevirtual 3177	java/io/PrintWriter:print	(I)V
        //     250: aload_2
        //     251: ldc_w 1910
        //     254: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     257: aload 4
        //     259: iconst_1
        //     260: invokevirtual 3158	com/android/server/pm/PackageManagerService$DumpState:isDumping	(I)Z
        //     263: ifeq +656 -> 919
        //     266: aload 5
        //     268: ifnonnull +651 -> 919
        //     271: aload 4
        //     273: invokevirtual 3161	com/android/server/pm/PackageManagerService$DumpState:onTitlePrinted	()Z
        //     276: ifeq +10 -> 286
        //     279: aload_2
        //     280: ldc_w 3163
        //     283: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     286: aload_2
        //     287: ldc_w 3179
        //     290: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     293: aload_0
        //     294: getfield 387	com/android/server/pm/PackageManagerService:mSharedLibraries	Ljava/util/HashMap;
        //     297: invokevirtual 2053	java/util/HashMap:keySet	()Ljava/util/Set;
        //     300: invokeinterface 923 1 0
        //     305: astore 47
        //     307: aload 47
        //     309: invokeinterface 702 1 0
        //     314: ifeq +605 -> 919
        //     317: aload 47
        //     319: invokeinterface 706 1 0
        //     324: checkcast 360	java/lang/String
        //     327: astore 48
        //     329: aload_2
        //     330: ldc_w 3066
        //     333: invokevirtual 3170	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     336: aload_2
        //     337: aload 48
        //     339: invokevirtual 3170	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     342: aload_2
        //     343: ldc_w 3181
        //     346: invokevirtual 3170	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     349: aload_2
        //     350: aload_0
        //     351: getfield 387	com/android/server/pm/PackageManagerService:mSharedLibraries	Ljava/util/HashMap;
        //     354: aload 48
        //     356: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     359: checkcast 360	java/lang/String
        //     362: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     365: goto -58 -> 307
        //     368: astore 10
        //     370: aload 9
        //     372: monitorexit
        //     373: aload 10
        //     375: athrow
        //     376: iinc 6 1
        //     379: ldc_w 3183
        //     382: aload 51
        //     384: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     387: ifne -308 -> 79
        //     390: ldc_w 3185
        //     393: aload 51
        //     395: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     398: ifeq +125 -> 523
        //     401: aload_2
        //     402: ldc_w 3187
        //     405: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     408: aload_2
        //     409: ldc_w 3189
        //     412: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     415: aload_2
        //     416: ldc_w 3191
        //     419: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     422: aload_2
        //     423: ldc_w 3193
        //     426: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     429: aload_2
        //     430: ldc_w 3195
        //     433: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     436: aload_2
        //     437: ldc_w 3197
        //     440: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     443: aload_2
        //     444: ldc_w 3199
        //     447: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     450: aload_2
        //     451: ldc_w 3201
        //     454: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     457: aload_2
        //     458: ldc_w 3203
        //     461: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     464: aload_2
        //     465: ldc_w 3205
        //     468: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     471: aload_2
        //     472: ldc_w 3207
        //     475: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     478: aload_2
        //     479: ldc_w 3209
        //     482: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     485: aload_2
        //     486: ldc_w 3211
        //     489: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     492: aload_2
        //     493: ldc_w 3213
        //     496: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     499: aload_2
        //     500: ldc_w 3215
        //     503: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     506: aload_2
        //     507: ldc_w 3217
        //     510: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     513: aload_2
        //     514: ldc_w 3219
        //     517: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     520: goto -457 -> 63
        //     523: ldc_w 3221
        //     526: aload 51
        //     528: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     531: ifeq +12 -> 543
        //     534: aload 4
        //     536: iconst_1
        //     537: invokevirtual 3224	com/android/server/pm/PackageManagerService$DumpState:setOptionEnabled	(I)V
        //     540: goto -461 -> 79
        //     543: aload_2
        //     544: new 662	java/lang/StringBuilder
        //     547: dup
        //     548: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     551: ldc_w 3226
        //     554: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     557: aload 51
        //     559: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     562: ldc_w 3228
        //     565: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     568: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     571: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     574: goto -495 -> 79
        //     577: ldc_w 3230
        //     580: aload 49
        //     582: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     585: ifne +14 -> 599
        //     588: ldc_w 3232
        //     591: aload 49
        //     593: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     596: ifeq +12 -> 608
        //     599: aload 4
        //     601: iconst_1
        //     602: invokevirtual 3235	com/android/server/pm/PackageManagerService$DumpState:setDump	(I)V
        //     605: goto -437 -> 168
        //     608: ldc_w 3237
        //     611: aload 49
        //     613: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     616: ifne +14 -> 630
        //     619: ldc_w 3239
        //     622: aload 49
        //     624: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     627: ifeq +12 -> 639
        //     630: aload 4
        //     632: iconst_2
        //     633: invokevirtual 3235	com/android/server/pm/PackageManagerService$DumpState:setDump	(I)V
        //     636: goto -468 -> 168
        //     639: ldc_w 3241
        //     642: aload 49
        //     644: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     647: ifne +14 -> 661
        //     650: ldc_w 3243
        //     653: aload 49
        //     655: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     658: ifeq +12 -> 670
        //     661: aload 4
        //     663: iconst_4
        //     664: invokevirtual 3235	com/android/server/pm/PackageManagerService$DumpState:setDump	(I)V
        //     667: goto -499 -> 168
        //     670: ldc_w 3244
        //     673: aload 49
        //     675: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     678: ifne +14 -> 692
        //     681: ldc_w 2189
        //     684: aload 49
        //     686: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     689: ifeq +13 -> 702
        //     692: aload 4
        //     694: bipush 8
        //     696: invokevirtual 3235	com/android/server/pm/PackageManagerService$DumpState:setDump	(I)V
        //     699: goto -531 -> 168
        //     702: ldc_w 3246
        //     705: aload 49
        //     707: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     710: ifne +14 -> 724
        //     713: ldc_w 3248
        //     716: aload 49
        //     718: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     721: ifeq +14 -> 735
        //     724: aload 4
        //     726: sipush 512
        //     729: invokevirtual 3235	com/android/server/pm/PackageManagerService$DumpState:setDump	(I)V
        //     732: goto -564 -> 168
        //     735: ldc_w 3250
        //     738: aload 49
        //     740: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     743: ifeq +14 -> 757
        //     746: aload 4
        //     748: sipush 1024
        //     751: invokevirtual 3235	com/android/server/pm/PackageManagerService$DumpState:setDump	(I)V
        //     754: goto -586 -> 168
        //     757: ldc_w 3252
        //     760: aload 49
        //     762: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     765: ifne +14 -> 779
        //     768: ldc_w 3253
        //     771: aload 49
        //     773: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     776: ifeq +13 -> 789
        //     779: aload 4
        //     781: bipush 16
        //     783: invokevirtual 3235	com/android/server/pm/PackageManagerService$DumpState:setDump	(I)V
        //     786: goto -618 -> 168
        //     789: ldc_w 3255
        //     792: aload 49
        //     794: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     797: ifne +14 -> 811
        //     800: ldc_w 3257
        //     803: aload 49
        //     805: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     808: ifeq +13 -> 821
        //     811: aload 4
        //     813: bipush 32
        //     815: invokevirtual 3235	com/android/server/pm/PackageManagerService$DumpState:setDump	(I)V
        //     818: goto -650 -> 168
        //     821: ldc_w 3259
        //     824: aload 49
        //     826: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     829: ifne +14 -> 843
        //     832: ldc_w 3260
        //     835: aload 49
        //     837: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     840: ifeq +14 -> 854
        //     843: aload 4
        //     845: sipush 128
        //     848: invokevirtual 3235	com/android/server/pm/PackageManagerService$DumpState:setDump	(I)V
        //     851: goto -683 -> 168
        //     854: ldc_w 3262
        //     857: aload 49
        //     859: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     862: ifne +14 -> 876
        //     865: ldc_w 3264
        //     868: aload 49
        //     870: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     873: ifeq +13 -> 886
        //     876: aload 4
        //     878: bipush 64
        //     880: invokevirtual 3235	com/android/server/pm/PackageManagerService$DumpState:setDump	(I)V
        //     883: goto -715 -> 168
        //     886: ldc_w 3266
        //     889: aload 49
        //     891: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     894: ifne +14 -> 908
        //     897: ldc_w 3267
        //     900: aload 49
        //     902: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     905: ifeq -737 -> 168
        //     908: aload 4
        //     910: sipush 256
        //     913: invokevirtual 3235	com/android/server/pm/PackageManagerService$DumpState:setDump	(I)V
        //     916: goto -748 -> 168
        //     919: aload 4
        //     921: iconst_2
        //     922: invokevirtual 3158	com/android/server/pm/PackageManagerService$DumpState:isDumping	(I)Z
        //     925: ifeq +82 -> 1007
        //     928: aload 5
        //     930: ifnonnull +77 -> 1007
        //     933: aload 4
        //     935: invokevirtual 3161	com/android/server/pm/PackageManagerService$DumpState:onTitlePrinted	()Z
        //     938: ifeq +10 -> 948
        //     941: aload_2
        //     942: ldc_w 3163
        //     945: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     948: aload_2
        //     949: ldc_w 3269
        //     952: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     955: aload_0
        //     956: getfield 391	com/android/server/pm/PackageManagerService:mAvailableFeatures	Ljava/util/HashMap;
        //     959: invokevirtual 2053	java/util/HashMap:keySet	()Ljava/util/Set;
        //     962: invokeinterface 923 1 0
        //     967: astore 45
        //     969: aload 45
        //     971: invokeinterface 702 1 0
        //     976: ifeq +31 -> 1007
        //     979: aload 45
        //     981: invokeinterface 706 1 0
        //     986: checkcast 360	java/lang/String
        //     989: astore 46
        //     991: aload_2
        //     992: ldc_w 3066
        //     995: invokevirtual 3170	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     998: aload_2
        //     999: aload 46
        //     1001: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     1004: goto -35 -> 969
        //     1007: aload 4
        //     1009: iconst_4
        //     1010: invokevirtual 3158	com/android/server/pm/PackageManagerService$DumpState:isDumping	(I)Z
        //     1013: ifeq +144 -> 1157
        //     1016: aload_0
        //     1017: getfield 396	com/android/server/pm/PackageManagerService:mActivities	Lcom/android/server/pm/PackageManagerService$ActivityIntentResolver;
        //     1020: astore 39
        //     1022: aload 4
        //     1024: invokevirtual 3272	com/android/server/pm/PackageManagerService$DumpState:getTitlePrinted	()Z
        //     1027: ifeq +981 -> 2008
        //     1030: ldc_w 3274
        //     1033: astore 40
        //     1035: aload 39
        //     1037: aload_2
        //     1038: aload 40
        //     1040: ldc_w 3066
        //     1043: aload 5
        //     1045: aload 4
        //     1047: iconst_1
        //     1048: invokevirtual 3277	com/android/server/pm/PackageManagerService$DumpState:isOptionEnabled	(I)Z
        //     1051: invokevirtual 3280	com/android/server/pm/PackageManagerService$ActivityIntentResolver:dump	(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z
        //     1054: ifeq +9 -> 1063
        //     1057: aload 4
        //     1059: iconst_1
        //     1060: invokevirtual 3283	com/android/server/pm/PackageManagerService$DumpState:setTitlePrinted	(Z)V
        //     1063: aload_0
        //     1064: getfield 398	com/android/server/pm/PackageManagerService:mReceivers	Lcom/android/server/pm/PackageManagerService$ActivityIntentResolver;
        //     1067: astore 41
        //     1069: aload 4
        //     1071: invokevirtual 3272	com/android/server/pm/PackageManagerService$DumpState:getTitlePrinted	()Z
        //     1074: ifeq +942 -> 2016
        //     1077: ldc_w 3285
        //     1080: astore 42
        //     1082: aload 41
        //     1084: aload_2
        //     1085: aload 42
        //     1087: ldc_w 3066
        //     1090: aload 5
        //     1092: aload 4
        //     1094: iconst_1
        //     1095: invokevirtual 3277	com/android/server/pm/PackageManagerService$DumpState:isOptionEnabled	(I)Z
        //     1098: invokevirtual 3280	com/android/server/pm/PackageManagerService$ActivityIntentResolver:dump	(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z
        //     1101: ifeq +9 -> 1110
        //     1104: aload 4
        //     1106: iconst_1
        //     1107: invokevirtual 3283	com/android/server/pm/PackageManagerService$DumpState:setTitlePrinted	(Z)V
        //     1110: aload_0
        //     1111: getfield 401	com/android/server/pm/PackageManagerService:mServices	Lcom/android/server/pm/PackageManagerService$ServiceIntentResolver;
        //     1114: astore 43
        //     1116: aload 4
        //     1118: invokevirtual 3272	com/android/server/pm/PackageManagerService$DumpState:getTitlePrinted	()Z
        //     1121: ifeq +903 -> 2024
        //     1124: ldc_w 3287
        //     1127: astore 44
        //     1129: aload 43
        //     1131: aload_2
        //     1132: aload 44
        //     1134: ldc_w 3066
        //     1137: aload 5
        //     1139: aload 4
        //     1141: iconst_1
        //     1142: invokevirtual 3277	com/android/server/pm/PackageManagerService$DumpState:isOptionEnabled	(I)Z
        //     1145: invokevirtual 3288	com/android/server/pm/PackageManagerService$ServiceIntentResolver:dump	(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z
        //     1148: ifeq +9 -> 1157
        //     1151: aload 4
        //     1153: iconst_1
        //     1154: invokevirtual 3283	com/android/server/pm/PackageManagerService$DumpState:setTitlePrinted	(Z)V
        //     1157: aload 4
        //     1159: sipush 512
        //     1162: invokevirtual 3158	com/android/server/pm/PackageManagerService$DumpState:isDumping	(I)Z
        //     1165: ifeq +53 -> 1218
        //     1168: aload_0
        //     1169: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     1172: getfield 914	com/android/server/pm/Settings:mPreferredActivities	Lcom/android/server/IntentResolver;
        //     1175: astore 37
        //     1177: aload 4
        //     1179: invokevirtual 3272	com/android/server/pm/PackageManagerService$DumpState:getTitlePrinted	()Z
        //     1182: ifeq +850 -> 2032
        //     1185: ldc_w 3290
        //     1188: astore 38
        //     1190: aload 37
        //     1192: aload_2
        //     1193: aload 38
        //     1195: ldc_w 3066
        //     1198: aload 5
        //     1200: aload 4
        //     1202: iconst_1
        //     1203: invokevirtual 3277	com/android/server/pm/PackageManagerService$DumpState:isOptionEnabled	(I)Z
        //     1206: invokevirtual 3291	com/android/server/IntentResolver:dump	(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z
        //     1209: ifeq +9 -> 1218
        //     1212: aload 4
        //     1214: iconst_1
        //     1215: invokevirtual 3283	com/android/server/pm/PackageManagerService$DumpState:setTitlePrinted	(Z)V
        //     1218: aload 4
        //     1220: sipush 1024
        //     1223: invokevirtual 3158	com/android/server/pm/PackageManagerService$DumpState:isDumping	(I)Z
        //     1226: ifeq +95 -> 1321
        //     1229: aload_2
        //     1230: invokevirtual 3294	java/io/PrintWriter:flush	()V
        //     1233: new 2340	java/io/FileOutputStream
        //     1236: dup
        //     1237: aload_1
        //     1238: invokespecial 3297	java/io/FileOutputStream:<init>	(Ljava/io/FileDescriptor;)V
        //     1241: astore 31
        //     1243: new 3299	java/io/BufferedOutputStream
        //     1246: dup
        //     1247: aload 31
        //     1249: invokespecial 3300	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;)V
        //     1252: astore 32
        //     1254: new 3302	com/android/internal/util/FastXmlSerializer
        //     1257: dup
        //     1258: invokespecial 3303	com/android/internal/util/FastXmlSerializer:<init>	()V
        //     1261: astore 33
        //     1263: aload 33
        //     1265: aload 32
        //     1267: ldc_w 3305
        //     1270: invokeinterface 3311 3 0
        //     1275: aload 33
        //     1277: aconst_null
        //     1278: iconst_1
        //     1279: invokestatic 3314	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
        //     1282: invokeinterface 3318 3 0
        //     1287: aload 33
        //     1289: ldc_w 3320
        //     1292: iconst_1
        //     1293: invokeinterface 3323 3 0
        //     1298: aload_0
        //     1299: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     1302: aload 33
        //     1304: invokevirtual 3327	com/android/server/pm/Settings:writePreferredActivitiesLPr	(Lorg/xmlpull/v1/XmlSerializer;)V
        //     1307: aload 33
        //     1309: invokeinterface 3330 1 0
        //     1314: aload 33
        //     1316: invokeinterface 3331 1 0
        //     1321: aload 4
        //     1323: bipush 8
        //     1325: invokevirtual 3158	com/android/server/pm/PackageManagerService$DumpState:isDumping	(I)Z
        //     1328: ifeq +15 -> 1343
        //     1331: aload_0
        //     1332: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     1335: aload_2
        //     1336: aload 5
        //     1338: aload 4
        //     1340: invokevirtual 3335	com/android/server/pm/Settings:dumpPermissionsLPr	(Ljava/io/PrintWriter;Ljava/lang/String;Lcom/android/server/pm/PackageManagerService$DumpState;)V
        //     1343: aload 4
        //     1345: sipush 128
        //     1348: invokevirtual 3158	com/android/server/pm/PackageManagerService$DumpState:isDumping	(I)Z
        //     1351: ifeq +419 -> 1770
        //     1354: iconst_0
        //     1355: istore 23
        //     1357: aload_0
        //     1358: getfield 403	com/android/server/pm/PackageManagerService:mProvidersByComponent	Ljava/util/HashMap;
        //     1361: invokevirtual 690	java/util/HashMap:values	()Ljava/util/Collection;
        //     1364: invokeinterface 696 1 0
        //     1369: astore 24
        //     1371: aload 24
        //     1373: invokeinterface 702 1 0
        //     1378: ifeq +198 -> 1576
        //     1381: aload 24
        //     1383: invokeinterface 706 1 0
        //     1388: checkcast 2509	android/content/pm/PackageParser$Provider
        //     1391: astore 30
        //     1393: aload 5
        //     1395: ifnull +19 -> 1414
        //     1398: aload 5
        //     1400: aload 30
        //     1402: getfield 2512	android/content/pm/PackageParser$Provider:info	Landroid/content/pm/ProviderInfo;
        //     1405: getfield 1747	android/content/pm/ComponentInfo:packageName	Ljava/lang/String;
        //     1408: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     1411: ifeq -40 -> 1371
        //     1414: iload 23
        //     1416: ifne +28 -> 1444
        //     1419: aload 4
        //     1421: invokevirtual 3161	com/android/server/pm/PackageManagerService$DumpState:onTitlePrinted	()Z
        //     1424: ifeq +10 -> 1434
        //     1427: aload_2
        //     1428: ldc_w 3163
        //     1431: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     1434: aload_2
        //     1435: ldc_w 3337
        //     1438: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     1441: iconst_1
        //     1442: istore 23
        //     1444: aload_2
        //     1445: ldc_w 3066
        //     1448: invokevirtual 3170	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     1451: aload_2
        //     1452: aload 30
        //     1454: invokevirtual 3340	android/content/pm/PackageParser$Provider:getComponentShortName	()Ljava/lang/String;
        //     1457: invokevirtual 3170	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     1460: aload_2
        //     1461: ldc_w 3060
        //     1464: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     1467: aload_2
        //     1468: ldc_w 3342
        //     1471: invokevirtual 3170	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     1474: aload_2
        //     1475: aload 30
        //     1477: invokevirtual 3343	android/content/pm/PackageParser$Provider:toString	()Ljava/lang/String;
        //     1480: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     1483: goto -112 -> 1371
        //     1486: astore 36
        //     1488: aload_2
        //     1489: new 662	java/lang/StringBuilder
        //     1492: dup
        //     1493: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     1496: ldc_w 3345
        //     1499: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1502: aload 36
        //     1504: invokevirtual 944	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     1507: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1510: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     1513: goto -192 -> 1321
        //     1516: astore 35
        //     1518: aload_2
        //     1519: new 662	java/lang/StringBuilder
        //     1522: dup
        //     1523: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     1526: ldc_w 3345
        //     1529: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1532: aload 35
        //     1534: invokevirtual 944	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     1537: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1540: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     1543: goto -222 -> 1321
        //     1546: astore 34
        //     1548: aload_2
        //     1549: new 662	java/lang/StringBuilder
        //     1552: dup
        //     1553: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     1556: ldc_w 3345
        //     1559: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1562: aload 34
        //     1564: invokevirtual 944	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     1567: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1570: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     1573: goto -252 -> 1321
        //     1576: iconst_0
        //     1577: istore 25
        //     1579: aload_0
        //     1580: getfield 405	com/android/server/pm/PackageManagerService:mProviders	Ljava/util/HashMap;
        //     1583: invokevirtual 1235	java/util/HashMap:entrySet	()Ljava/util/Set;
        //     1586: invokeinterface 923 1 0
        //     1591: astore 26
        //     1593: aload 26
        //     1595: invokeinterface 702 1 0
        //     1600: ifeq +170 -> 1770
        //     1603: aload 26
        //     1605: invokeinterface 706 1 0
        //     1610: checkcast 1237	java/util/Map$Entry
        //     1613: astore 27
        //     1615: aload 27
        //     1617: invokeinterface 1240 1 0
        //     1622: checkcast 2509	android/content/pm/PackageParser$Provider
        //     1625: astore 28
        //     1627: aload 5
        //     1629: ifnull +19 -> 1648
        //     1632: aload 5
        //     1634: aload 28
        //     1636: getfield 2512	android/content/pm/PackageParser$Provider:info	Landroid/content/pm/ProviderInfo;
        //     1639: getfield 1747	android/content/pm/ComponentInfo:packageName	Ljava/lang/String;
        //     1642: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     1645: ifeq -52 -> 1593
        //     1648: iload 25
        //     1650: ifne +28 -> 1678
        //     1653: aload 4
        //     1655: invokevirtual 3161	com/android/server/pm/PackageManagerService$DumpState:onTitlePrinted	()Z
        //     1658: ifeq +10 -> 1668
        //     1661: aload_2
        //     1662: ldc_w 3163
        //     1665: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     1668: aload_2
        //     1669: ldc_w 3347
        //     1672: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     1675: iconst_1
        //     1676: istore 25
        //     1678: aload_2
        //     1679: ldc_w 3349
        //     1682: invokevirtual 3170	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     1685: aload_2
        //     1686: aload 27
        //     1688: invokeinterface 3352 1 0
        //     1693: checkcast 360	java/lang/String
        //     1696: invokevirtual 3170	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     1699: aload_2
        //     1700: ldc_w 3354
        //     1703: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     1706: aload_2
        //     1707: ldc_w 3342
        //     1710: invokevirtual 3170	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     1713: aload_2
        //     1714: aload 28
        //     1716: invokevirtual 3343	android/content/pm/PackageParser$Provider:toString	()Ljava/lang/String;
        //     1719: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     1722: aload 28
        //     1724: getfield 2512	android/content/pm/PackageParser$Provider:info	Landroid/content/pm/ProviderInfo;
        //     1727: ifnull -134 -> 1593
        //     1730: aload 28
        //     1732: getfield 2512	android/content/pm/PackageParser$Provider:info	Landroid/content/pm/ProviderInfo;
        //     1735: getfield 3355	android/content/pm/ProviderInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     1738: ifnull -145 -> 1593
        //     1741: aload 28
        //     1743: getfield 2512	android/content/pm/PackageParser$Provider:info	Landroid/content/pm/ProviderInfo;
        //     1746: getfield 3355	android/content/pm/ProviderInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     1749: invokevirtual 3356	android/content/pm/ApplicationInfo:toString	()Ljava/lang/String;
        //     1752: astore 29
        //     1754: aload_2
        //     1755: ldc_w 3358
        //     1758: invokevirtual 3170	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     1761: aload_2
        //     1762: aload 29
        //     1764: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     1767: goto -174 -> 1593
        //     1770: aload 4
        //     1772: bipush 16
        //     1774: invokevirtual 3158	com/android/server/pm/PackageManagerService$DumpState:isDumping	(I)Z
        //     1777: ifeq +15 -> 1792
        //     1780: aload_0
        //     1781: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     1784: aload_2
        //     1785: aload 5
        //     1787: aload 4
        //     1789: invokevirtual 3361	com/android/server/pm/Settings:dumpPackagesLPr	(Ljava/io/PrintWriter;Ljava/lang/String;Lcom/android/server/pm/PackageManagerService$DumpState;)V
        //     1792: aload 4
        //     1794: bipush 32
        //     1796: invokevirtual 3158	com/android/server/pm/PackageManagerService$DumpState:isDumping	(I)Z
        //     1799: ifeq +15 -> 1814
        //     1802: aload_0
        //     1803: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     1806: aload_2
        //     1807: aload 5
        //     1809: aload 4
        //     1811: invokevirtual 3364	com/android/server/pm/Settings:dumpSharedUsersLPr	(Ljava/io/PrintWriter;Ljava/lang/String;Lcom/android/server/pm/PackageManagerService$DumpState;)V
        //     1814: aload 4
        //     1816: bipush 64
        //     1818: invokevirtual 3158	com/android/server/pm/PackageManagerService$DumpState:isDumping	(I)Z
        //     1821: ifeq +106 -> 1927
        //     1824: aload 5
        //     1826: ifnonnull +101 -> 1927
        //     1829: aload 4
        //     1831: invokevirtual 3161	com/android/server/pm/PackageManagerService$DumpState:onTitlePrinted	()Z
        //     1834: ifeq +10 -> 1844
        //     1837: aload_2
        //     1838: ldc_w 3163
        //     1841: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     1844: aload_0
        //     1845: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     1848: aload_2
        //     1849: aload 4
        //     1851: invokevirtual 3368	com/android/server/pm/Settings:dumpReadMessagesLPr	(Ljava/io/PrintWriter;Lcom/android/server/pm/PackageManagerService$DumpState;)V
        //     1854: aload_2
        //     1855: ldc_w 3163
        //     1858: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     1861: aload_2
        //     1862: ldc_w 3370
        //     1865: invokevirtual 2362	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     1868: invokestatic 2336	com/android/server/pm/PackageManagerService:getSettingsProblemFile	()Ljava/io/File;
        //     1871: astore 11
        //     1873: aconst_null
        //     1874: astore 12
        //     1876: new 3372	java/io/FileInputStream
        //     1879: dup
        //     1880: aload 11
        //     1882: invokespecial 3373	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //     1885: astore 13
        //     1887: aload 13
        //     1889: invokevirtual 3376	java/io/FileInputStream:available	()I
        //     1892: newarray byte
        //     1894: astore 19
        //     1896: aload 13
        //     1898: aload 19
        //     1900: invokevirtual 3380	java/io/FileInputStream:read	([B)I
        //     1903: pop
        //     1904: aload_2
        //     1905: new 360	java/lang/String
        //     1908: dup
        //     1909: aload 19
        //     1911: invokespecial 3383	java/lang/String:<init>	([B)V
        //     1914: invokevirtual 3170	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     1917: aload 13
        //     1919: ifnull +8 -> 1927
        //     1922: aload 13
        //     1924: invokevirtual 3384	java/io/FileInputStream:close	()V
        //     1927: aload 9
        //     1929: monitorexit
        //     1930: goto -1867 -> 63
        //     1933: astore 17
        //     1935: aload 12
        //     1937: ifnull +8 -> 1945
        //     1940: aload 12
        //     1942: invokevirtual 3384	java/io/FileInputStream:close	()V
        //     1945: aload 17
        //     1947: athrow
        //     1948: astore 22
        //     1950: aload 12
        //     1952: ifnull -25 -> 1927
        //     1955: aload 12
        //     1957: invokevirtual 3384	java/io/FileInputStream:close	()V
        //     1960: goto -33 -> 1927
        //     1963: aload 12
        //     1965: ifnull -38 -> 1927
        //     1968: aload 12
        //     1970: invokevirtual 3384	java/io/FileInputStream:close	()V
        //     1973: goto -46 -> 1927
        //     1976: astore 18
        //     1978: goto -33 -> 1945
        //     1981: astore 17
        //     1983: aload 13
        //     1985: astore 12
        //     1987: goto -52 -> 1935
        //     1990: astore 16
        //     1992: aload 13
        //     1994: astore 12
        //     1996: goto -33 -> 1963
        //     1999: astore 14
        //     2001: aload 13
        //     2003: astore 12
        //     2005: goto -55 -> 1950
        //     2008: ldc_w 3386
        //     2011: astore 40
        //     2013: goto -978 -> 1035
        //     2016: ldc_w 3388
        //     2019: astore 42
        //     2021: goto -939 -> 1082
        //     2024: ldc_w 3390
        //     2027: astore 44
        //     2029: goto -900 -> 1129
        //     2032: ldc_w 3392
        //     2035: astore 38
        //     2037: goto -847 -> 1190
        //     2040: astore 15
        //     2042: goto -115 -> 1927
        //     2045: astore 21
        //     2047: goto -84 -> 1963
        //
        // Exception table:
        //     from	to	target	type
        //     177	373	368	finally
        //     919	1263	368	finally
        //     1263	1321	368	finally
        //     1321	1873	368	finally
        //     1922	1927	368	finally
        //     1927	1930	368	finally
        //     1940	1945	368	finally
        //     1945	1948	368	finally
        //     1955	1973	368	finally
        //     1263	1321	1486	java/lang/IllegalArgumentException
        //     1263	1321	1516	java/lang/IllegalStateException
        //     1263	1321	1546	java/io/IOException
        //     1876	1887	1933	finally
        //     1876	1887	1948	java/io/FileNotFoundException
        //     1940	1945	1976	java/io/IOException
        //     1887	1917	1981	finally
        //     1887	1917	1990	java/io/IOException
        //     1887	1917	1999	java/io/FileNotFoundException
        //     1922	1927	2040	java/io/IOException
        //     1955	1973	2040	java/io/IOException
        //     1876	1887	2045	java/io/IOException
    }

    public void enterSafeMode()
    {
        enforceSystemOrRoot("Only the system can request entering safe mode");
        if (!this.mSystemReady)
            this.mSafeMode = true;
    }

    ResolveInfo findPreferredActivity(Intent paramIntent, String paramString, int paramInt1, List<ResolveInfo> paramList, int paramInt2, int paramInt3)
    {
        ResolveInfo localResolveInfo1;
        if (!sUserManager.exists(paramInt3))
        {
            localResolveInfo1 = null;
            return localResolveInfo1;
        }
        while (true)
        {
            boolean bool;
            int j;
            int k;
            int i1;
            PreferredActivity localPreferredActivity;
            ActivityInfo localActivityInfo;
            int i2;
            synchronized (this.mPackages)
            {
                if (paramIntent.getSelector() != null)
                    paramIntent = paramIntent.getSelector();
                IntentResolver localIntentResolver = this.mSettings.mPreferredActivities;
                if ((0x10000 & paramInt1) != 0)
                {
                    bool = true;
                    List localList = localIntentResolver.queryIntent(paramIntent, paramString, bool, paramInt3);
                    if ((localList != null) && (localList.size() > 0))
                    {
                        int i = 0;
                        j = paramList.size();
                        k = 0;
                        if (k < j)
                        {
                            ResolveInfo localResolveInfo2 = (ResolveInfo)paramList.get(k);
                            if (localResolveInfo2.match <= i)
                                continue;
                            i = localResolveInfo2.match;
                            continue;
                        }
                        int m = i & 0xFFF0000;
                        int n = localList.size();
                        i1 = 0;
                        if (i1 < n)
                        {
                            localPreferredActivity = (PreferredActivity)localList.get(i1);
                            if (localPreferredActivity.mPref.mMatch != m)
                                continue;
                            localActivityInfo = getActivityInfo(localPreferredActivity.mPref.mComponent, paramInt1, paramInt3);
                            if (localActivityInfo == null)
                            {
                                Slog.w("PackageManager", "Removing dangling preferred activity: " + localPreferredActivity.mPref.mComponent);
                                this.mSettings.mPreferredActivities.removeFilter(localPreferredActivity);
                            }
                        }
                    }
                }
            }
            continue;
            i2++;
        }
    }

    public void finishPackageInstall(int paramInt)
    {
        enforceSystemOrRoot("Only the system is allowed to finish installs");
        Message localMessage = this.mHandler.obtainMessage(9, paramInt, 0);
        this.mHandler.sendMessage(localMessage);
    }

    public void freeStorage(final long paramLong, IntentSender paramIntentSender)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CLEAR_APP_CACHE", null);
        this.mHandler.post(new Runnable()
        {
            public void run()
            {
                PackageManagerService.this.mHandler.removeCallbacks(this);
                int i = PackageManagerService.this.mInstaller.freeCache(paramLong);
                if (i < 0)
                    Slog.w("PackageManager", "Couldn't clear application caches");
                int j;
                if (this.val$pi != null)
                {
                    if (i < 0)
                        break label64;
                    j = 1;
                }
                try
                {
                    while (true)
                    {
                        this.val$pi.sendIntent(null, j, null, null, null);
                        return;
                        label64: j = 0;
                    }
                }
                catch (IntentSender.SendIntentException localSendIntentException)
                {
                    while (true)
                        Slog.i("PackageManager", "Failed to send pending intent");
                }
            }
        });
    }

    public void freeStorageAndNotify(final long paramLong, IPackageDataObserver paramIPackageDataObserver)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CLEAR_APP_CACHE", null);
        this.mHandler.post(new Runnable()
        {
            public void run()
            {
                PackageManagerService.this.mHandler.removeCallbacks(this);
                int i = PackageManagerService.this.mInstaller.freeCache(paramLong);
                if (i < 0)
                    Slog.w("PackageManager", "Couldn't clear application caches");
                if (this.val$observer != null);
                try
                {
                    IPackageDataObserver localIPackageDataObserver = this.val$observer;
                    if (i >= 0);
                    for (boolean bool = true; ; bool = false)
                    {
                        localIPackageDataObserver.onRemoveCompleted(null, bool);
                        return;
                    }
                }
                catch (RemoteException localRemoteException)
                {
                    while (true)
                        Slog.w("PackageManager", "RemoveException when invoking call back");
                }
            }
        });
    }

    PackageInfo generatePackageInfo(PackageParser.Package paramPackage, int paramInt1, int paramInt2)
    {
        PackageInfo localPackageInfo;
        if (!sUserManager.exists(paramInt2))
            localPackageInfo = null;
        PackageSetting localPackageSetting;
        while (true)
        {
            return localPackageInfo;
            if ((paramInt1 & 0x2000) != 0)
            {
                localPackageInfo = PackageParser.generatePackageInfo(paramPackage, null, paramInt1, 0L, 0L, null, false, 0, paramInt2);
            }
            else
            {
                localPackageSetting = (PackageSetting)paramPackage.mExtras;
                if (localPackageSetting != null)
                    break;
                localPackageInfo = null;
            }
        }
        Object localObject;
        label76: ApplicationInfo localApplicationInfo;
        if (localPackageSetting.sharedUser != null)
        {
            localObject = localPackageSetting.sharedUser;
            localPackageInfo = PackageParser.generatePackageInfo(paramPackage, ((GrantedPermissions)localObject).gids, paramInt1, localPackageSetting.firstInstallTime, localPackageSetting.lastUpdateTime, ((GrantedPermissions)localObject).grantedPermissions, localPackageSetting.getStopped(paramInt2), localPackageSetting.getEnabled(paramInt2), paramInt2);
            localPackageInfo.applicationInfo.enabledSetting = localPackageSetting.getEnabled(paramInt2);
            localApplicationInfo = localPackageInfo.applicationInfo;
            if ((localPackageInfo.applicationInfo.enabledSetting != 0) && (localPackageInfo.applicationInfo.enabledSetting != 1))
                break label180;
        }
        label180: for (boolean bool = true; ; bool = false)
        {
            localApplicationInfo.enabled = bool;
            break;
            localObject = localPackageSetting;
            break label76;
        }
    }

    // ERROR //
    public ActivityInfo getActivityInfo(ComponentName paramComponentName, int paramInt1, int paramInt2)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore 4
        //     3: getstatic 604	com/android/server/pm/PackageManagerService:sUserManager	Lcom/android/server/pm/UserManager;
        //     6: iload_3
        //     7: invokevirtual 1607	com/android/server/pm/UserManager:exists	(I)Z
        //     10: ifne +6 -> 16
        //     13: aload 4
        //     15: areturn
        //     16: aload_0
        //     17: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     20: astore 5
        //     22: aload 5
        //     24: monitorenter
        //     25: aload_0
        //     26: getfield 396	com/android/server/pm/PackageManagerService:mActivities	Lcom/android/server/pm/PackageManagerService$ActivityIntentResolver;
        //     29: invokestatic 929	com/android/server/pm/PackageManagerService$ActivityIntentResolver:access$800	(Lcom/android/server/pm/PackageManagerService$ActivityIntentResolver;)Ljava/util/HashMap;
        //     32: aload_1
        //     33: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     36: checkcast 2680	android/content/pm/PackageParser$Activity
        //     39: astore 7
        //     41: aload 7
        //     43: ifnull +85 -> 128
        //     46: aload_0
        //     47: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     50: aload 7
        //     52: getfield 2682	android/content/pm/PackageParser$Activity:info	Landroid/content/pm/ActivityInfo;
        //     55: iload_2
        //     56: iload_3
        //     57: invokevirtual 3452	com/android/server/pm/Settings:isEnabledLPr	(Landroid/content/pm/ComponentInfo;II)Z
        //     60: ifeq +68 -> 128
        //     63: aload_0
        //     64: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     67: getfield 783	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
        //     70: aload_1
        //     71: invokevirtual 2532	android/content/ComponentName:getPackageName	()Ljava/lang/String;
        //     74: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     77: checkcast 785	com/android/server/pm/PackageSetting
        //     80: astore 8
        //     82: aload 8
        //     84: ifnonnull +17 -> 101
        //     87: aload 5
        //     89: monitorexit
        //     90: goto -77 -> 13
        //     93: astore 6
        //     95: aload 5
        //     97: monitorexit
        //     98: aload 6
        //     100: athrow
        //     101: aload 7
        //     103: iload_2
        //     104: aload 8
        //     106: iload_3
        //     107: invokevirtual 1617	com/android/server/pm/PackageSetting:getStopped	(I)Z
        //     110: aload 8
        //     112: iload_3
        //     113: invokevirtual 1620	com/android/server/pm/PackageSetting:getEnabled	(I)I
        //     116: iload_3
        //     117: invokestatic 3456	android/content/pm/PackageParser:generateActivityInfo	(Landroid/content/pm/PackageParser$Activity;IZII)Landroid/content/pm/ActivityInfo;
        //     120: astore 4
        //     122: aload 5
        //     124: monitorexit
        //     125: goto -112 -> 13
        //     128: aload_0
        //     129: getfield 2418	com/android/server/pm/PackageManagerService:mResolveComponentName	Landroid/content/ComponentName;
        //     132: aload_1
        //     133: invokevirtual 3457	android/content/ComponentName:equals	(Ljava/lang/Object;)Z
        //     136: ifeq +15 -> 151
        //     139: aload_0
        //     140: getfield 430	com/android/server/pm/PackageManagerService:mResolveActivity	Landroid/content/pm/ActivityInfo;
        //     143: astore 4
        //     145: aload 5
        //     147: monitorexit
        //     148: goto -135 -> 13
        //     151: aload 5
        //     153: monitorexit
        //     154: goto -141 -> 13
        //
        // Exception table:
        //     from	to	target	type
        //     25	98	93	finally
        //     101	154	93	finally
    }

    // ERROR //
    public List<PermissionGroupInfo> getAllPermissionGroups(int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: new 420	java/util/ArrayList
        //     10: dup
        //     11: aload_0
        //     12: getfield 409	com/android/server/pm/PackageManagerService:mPermissionGroups	Ljava/util/HashMap;
        //     15: invokevirtual 686	java/util/HashMap:size	()I
        //     18: invokespecial 2116	java/util/ArrayList:<init>	(I)V
        //     21: astore_3
        //     22: aload_0
        //     23: getfield 409	com/android/server/pm/PackageManagerService:mPermissionGroups	Ljava/util/HashMap;
        //     26: invokevirtual 690	java/util/HashMap:values	()Ljava/util/Collection;
        //     29: invokeinterface 696 1 0
        //     34: astore 5
        //     36: aload 5
        //     38: invokeinterface 702 1 0
        //     43: ifeq +32 -> 75
        //     46: aload_3
        //     47: aload 5
        //     49: invokeinterface 706 1 0
        //     54: checkcast 2698	android/content/pm/PackageParser$PermissionGroup
        //     57: iload_1
        //     58: invokestatic 3463	android/content/pm/PackageParser:generatePermissionGroupInfo	(Landroid/content/pm/PackageParser$PermissionGroup;I)Landroid/content/pm/PermissionGroupInfo;
        //     61: invokevirtual 939	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     64: pop
        //     65: goto -29 -> 36
        //     68: astore 4
        //     70: aload_2
        //     71: monitorexit
        //     72: aload 4
        //     74: athrow
        //     75: aload_2
        //     76: monitorexit
        //     77: aload_3
        //     78: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     7	72	68	finally
        //     75	77	68	finally
    }

    public int getApplicationEnabledSetting(String paramString, int paramInt)
    {
        int i;
        if (!sUserManager.exists(paramInt))
            i = 2;
        while (true)
        {
            return i;
            checkValidCaller(Binder.getCallingUid(), paramInt);
            synchronized (this.mPackages)
            {
                i = this.mSettings.getApplicationEnabledSettingLPr(paramString, paramInt);
            }
        }
    }

    // ERROR //
    public ApplicationInfo getApplicationInfo(String paramString, int paramInt1, int paramInt2)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore 4
        //     3: getstatic 604	com/android/server/pm/PackageManagerService:sUserManager	Lcom/android/server/pm/UserManager;
        //     6: iload_3
        //     7: invokevirtual 1607	com/android/server/pm/UserManager:exists	(I)Z
        //     10: ifne +6 -> 16
        //     13: aload 4
        //     15: areturn
        //     16: aload_0
        //     17: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     20: astore 5
        //     22: aload 5
        //     24: monitorenter
        //     25: aload_0
        //     26: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     29: aload_1
        //     30: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     33: checkcast 800	android/content/pm/PackageParser$Package
        //     36: astore 7
        //     38: aload 7
        //     40: ifnull +64 -> 104
        //     43: aload_0
        //     44: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     47: getfield 783	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
        //     50: aload_1
        //     51: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     54: checkcast 785	com/android/server/pm/PackageSetting
        //     57: astore 8
        //     59: aload 8
        //     61: ifnonnull +17 -> 78
        //     64: aload 5
        //     66: monitorexit
        //     67: goto -54 -> 13
        //     70: astore 6
        //     72: aload 5
        //     74: monitorexit
        //     75: aload 6
        //     77: athrow
        //     78: aload 7
        //     80: iload_2
        //     81: aload 8
        //     83: iload_3
        //     84: invokevirtual 1617	com/android/server/pm/PackageSetting:getStopped	(I)Z
        //     87: aload 8
        //     89: iload_3
        //     90: invokevirtual 1620	com/android/server/pm/PackageSetting:getEnabled	(I)I
        //     93: invokestatic 3471	android/content/pm/PackageParser:generateApplicationInfo	(Landroid/content/pm/PackageParser$Package;IZI)Landroid/content/pm/ApplicationInfo;
        //     96: astore 4
        //     98: aload 5
        //     100: monitorexit
        //     101: goto -88 -> 13
        //     104: ldc_w 2381
        //     107: aload_1
        //     108: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     111: ifne +13 -> 124
        //     114: ldc_w 1760
        //     117: aload_1
        //     118: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     121: ifeq +15 -> 136
        //     124: aload_0
        //     125: getfield 2383	com/android/server/pm/PackageManagerService:mAndroidApplication	Landroid/content/pm/ApplicationInfo;
        //     128: astore 4
        //     130: aload 5
        //     132: monitorexit
        //     133: goto -120 -> 13
        //     136: iload_2
        //     137: sipush 8192
        //     140: iand
        //     141: ifeq +18 -> 159
        //     144: aload_0
        //     145: aload_1
        //     146: iload_2
        //     147: iload_3
        //     148: invokespecial 3473	com/android/server/pm/PackageManagerService:generateApplicationInfoFromSettingsLPw	(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
        //     151: astore 4
        //     153: aload 5
        //     155: monitorexit
        //     156: goto -143 -> 13
        //     159: aload 5
        //     161: monitorexit
        //     162: goto -149 -> 13
        //
        // Exception table:
        //     from	to	target	type
        //     25	75	70	finally
        //     78	162	70	finally
    }

    public int getComponentEnabledSetting(ComponentName paramComponentName, int paramInt)
    {
        int i;
        if (!sUserManager.exists(paramInt))
            i = 2;
        while (true)
        {
            return i;
            checkValidCaller(Binder.getCallingUid(), paramInt);
            synchronized (this.mPackages)
            {
                i = this.mSettings.getComponentEnabledSettingLPr(paramComponentName, paramInt);
            }
        }
    }

    File getDataPathForUser(int paramInt)
    {
        return new File(this.mUserAppDataDir.getAbsolutePath() + File.separator + paramInt);
    }

    public int getInstallLocation()
    {
        return Settings.System.getInt(this.mContext.getContentResolver(), "default_install_location", 0);
    }

    // ERROR //
    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public ParceledListSlice<ApplicationInfo> getInstalledApplications(int paramInt1, String paramString, int paramInt2)
    {
        // Byte code:
        //     0: getstatic 604	com/android/server/pm/PackageManagerService:sUserManager	Lcom/android/server/pm/UserManager;
        //     3: iload_3
        //     4: invokevirtual 1607	com/android/server/pm/UserManager:exists	(I)Z
        //     7: ifne +9 -> 16
        //     10: aconst_null
        //     11: astore 4
        //     13: aload 4
        //     15: areturn
        //     16: new 3490	android/content/pm/ParceledListSlice
        //     19: dup
        //     20: invokespecial 3491	android/content/pm/ParceledListSlice:<init>	()V
        //     23: astore 4
        //     25: iload_1
        //     26: sipush 8192
        //     29: iand
        //     30: ifeq +180 -> 210
        //     33: iconst_1
        //     34: istore 5
        //     36: aload_0
        //     37: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     40: astore 6
        //     42: aload 6
        //     44: monitorenter
        //     45: iload 5
        //     47: ifeq +169 -> 216
        //     50: aload_0
        //     51: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     54: getfield 783	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
        //     57: invokevirtual 2053	java/util/HashMap:keySet	()Ljava/util/Set;
        //     60: aload_0
        //     61: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     64: getfield 783	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
        //     67: invokevirtual 686	java/util/HashMap:size	()I
        //     70: anewarray 360	java/lang/String
        //     73: invokeinterface 3492 2 0
        //     78: checkcast 2852	[Ljava/lang/String;
        //     81: astore 7
        //     83: aload 7
        //     85: invokestatic 3495	java/util/Arrays:sort	([Ljava/lang/Object;)V
        //     88: aload 7
        //     90: aload_2
        //     91: invokestatic 3497	com/android/server/pm/PackageManagerService:getContinuationPoint	([Ljava/lang/String;Ljava/lang/String;)I
        //     94: istore 9
        //     96: aload 7
        //     98: arraylength
        //     99: istore 10
        //     101: iload 9
        //     103: istore 11
        //     105: iload 11
        //     107: iload 10
        //     109: if_icmpge +196 -> 305
        //     112: iload 11
        //     114: iconst_1
        //     115: iadd
        //     116: istore 12
        //     118: aload 7
        //     120: iload 11
        //     122: aaload
        //     123: astore 13
        //     125: aconst_null
        //     126: astore 14
        //     128: aload_0
        //     129: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     132: getfield 783	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
        //     135: aload 13
        //     137: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     140: checkcast 785	com/android/server/pm/PackageSetting
        //     143: astore 15
        //     145: iload 5
        //     147: ifeq +99 -> 246
        //     150: aload 15
        //     152: ifnull +16 -> 168
        //     155: aload_0
        //     156: aload 15
        //     158: getfield 795	com/android/server/pm/PackageSettingBase:name	Ljava/lang/String;
        //     161: iload_1
        //     162: iload_3
        //     163: invokespecial 3473	com/android/server/pm/PackageManagerService:generateApplicationInfoFromSettingsLPw	(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
        //     166: astore 14
        //     168: aload 14
        //     170: ifnull +128 -> 298
        //     173: aload 4
        //     175: aload 14
        //     177: invokevirtual 3500	android/content/pm/ParceledListSlice:append	(Landroid/os/Parcelable;)Z
        //     180: ifeq +118 -> 298
        //     183: iload 12
        //     185: iload 10
        //     187: if_icmpne +9 -> 196
        //     190: aload 4
        //     192: iconst_1
        //     193: invokevirtual 3503	android/content/pm/ParceledListSlice:setLastSlice	(Z)V
        //     196: aload 6
        //     198: monitorexit
        //     199: goto -186 -> 13
        //     202: astore 8
        //     204: aload 6
        //     206: monitorexit
        //     207: aload 8
        //     209: athrow
        //     210: iconst_0
        //     211: istore 5
        //     213: goto -177 -> 36
        //     216: aload_0
        //     217: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     220: invokevirtual 2053	java/util/HashMap:keySet	()Ljava/util/Set;
        //     223: aload_0
        //     224: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     227: invokevirtual 686	java/util/HashMap:size	()I
        //     230: anewarray 360	java/lang/String
        //     233: invokeinterface 3492 2 0
        //     238: checkcast 2852	[Ljava/lang/String;
        //     241: astore 7
        //     243: goto -160 -> 83
        //     246: aload_0
        //     247: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     250: aload 13
        //     252: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     255: checkcast 800	android/content/pm/PackageParser$Package
        //     258: astore 16
        //     260: aload 16
        //     262: ifnull -94 -> 168
        //     265: aload 15
        //     267: ifnull -99 -> 168
        //     270: aload 16
        //     272: iload_1
        //     273: aload 15
        //     275: iload_3
        //     276: invokevirtual 1617	com/android/server/pm/PackageSetting:getStopped	(I)Z
        //     279: aload 15
        //     281: iload_3
        //     282: invokevirtual 1620	com/android/server/pm/PackageSetting:getEnabled	(I)I
        //     285: iload_3
        //     286: invokestatic 1624	android/content/pm/PackageParser:generateApplicationInfo	(Landroid/content/pm/PackageParser$Package;IZII)Landroid/content/pm/ApplicationInfo;
        //     289: astore 17
        //     291: aload 17
        //     293: astore 14
        //     295: goto -127 -> 168
        //     298: iload 12
        //     300: istore 11
        //     302: goto -197 -> 105
        //     305: iload 11
        //     307: istore 12
        //     309: goto -126 -> 183
        //
        // Exception table:
        //     from	to	target	type
        //     50	207	202	finally
        //     216	291	202	finally
    }

    public ParceledListSlice<PackageInfo> getInstalledPackages(int paramInt, String paramString)
    {
        ParceledListSlice localParceledListSlice = new ParceledListSlice();
        int i;
        int j;
        HashMap localHashMap;
        if ((paramInt & 0x2000) != 0)
        {
            i = 1;
            j = UserId.getCallingUserId();
            localHashMap = this.mPackages;
            if (i == 0);
        }
        while (true)
        {
            int i1;
            try
            {
                String[] arrayOfString = (String[])this.mSettings.mPackages.keySet().toArray(new String[this.mSettings.mPackages.size()]);
                Arrays.sort(arrayOfString);
                int k = getContinuationPoint(arrayOfString, paramString);
                int m = arrayOfString.length;
                n = k;
                if (n < m)
                {
                    i1 = n + 1;
                    String str = arrayOfString[n];
                    PackageInfo localPackageInfo = null;
                    if (i != 0)
                    {
                        PackageSetting localPackageSetting = (PackageSetting)this.mSettings.mPackages.get(str);
                        if (localPackageSetting != null)
                            localPackageInfo = generatePackageInfoFromSettingsLPw(localPackageSetting.name, paramInt, j);
                        if ((localPackageInfo == null) || (!Injector.addPackageToSlice(localParceledListSlice, localPackageInfo, paramInt)))
                            break label273;
                        if (i1 == m)
                            localParceledListSlice.setLastSlice(true);
                        return localParceledListSlice;
                        arrayOfString = (String[])this.mPackages.keySet().toArray(new String[this.mPackages.size()]);
                        continue;
                    }
                    PackageParser.Package localPackage = (PackageParser.Package)this.mPackages.get(str);
                    if (localPackage == null)
                        continue;
                    localPackageInfo = generatePackageInfo(localPackage, paramInt, j);
                }
            }
            finally
            {
            }
            continue;
            i = 0;
            break;
            label273: int n = i1;
        }
    }

    public String getInstallerPackageName(String paramString)
    {
        synchronized (this.mPackages)
        {
            String str = this.mSettings.getInstallerPackageNameLPr(paramString);
            return str;
        }
    }

    public InstrumentationInfo getInstrumentationInfo(ComponentName paramComponentName, int paramInt)
    {
        synchronized (this.mPackages)
        {
            InstrumentationInfo localInstrumentationInfo = PackageParser.generateInstrumentationInfo((PackageParser.Instrumentation)this.mInstrumentation.get(paramComponentName), paramInt);
            return localInstrumentationInfo;
        }
    }

    // ERROR //
    public String getNameForUid(int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     11: iload_1
        //     12: invokestatic 1182	android/os/UserId:getAppId	(I)I
        //     15: invokevirtual 1802	com/android/server/pm/Settings:getUserIdLPr	(I)Ljava/lang/Object;
        //     18: astore 4
        //     20: aload 4
        //     22: instanceof 1804
        //     25: ifeq +49 -> 74
        //     28: aload 4
        //     30: checkcast 1804	com/android/server/pm/SharedUserSetting
        //     33: astore 6
        //     35: new 662	java/lang/StringBuilder
        //     38: dup
        //     39: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     42: aload 6
        //     44: getfield 2476	com/android/server/pm/SharedUserSetting:name	Ljava/lang/String;
        //     47: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     50: ldc_w 3060
        //     53: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     56: aload 6
        //     58: getfield 3523	com/android/server/pm/SharedUserSetting:userId	I
        //     61: invokevirtual 902	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     64: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     67: astore 5
        //     69: aload_2
        //     70: monitorexit
        //     71: goto +36 -> 107
        //     74: aload 4
        //     76: instanceof 785
        //     79: ifeq +23 -> 102
        //     82: aload 4
        //     84: checkcast 785	com/android/server/pm/PackageSetting
        //     87: getfield 795	com/android/server/pm/PackageSettingBase:name	Ljava/lang/String;
        //     90: astore 5
        //     92: aload_2
        //     93: monitorexit
        //     94: goto +13 -> 107
        //     97: astore_3
        //     98: aload_2
        //     99: monitorexit
        //     100: aload_3
        //     101: athrow
        //     102: aload_2
        //     103: monitorexit
        //     104: aconst_null
        //     105: astore 5
        //     107: aload 5
        //     109: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     7	100	97	finally
        //     102	104	97	finally
    }

    public int[] getPackageGids(String paramString)
    {
        int[] arrayOfInt;
        synchronized (this.mPackages)
        {
            PackageParser.Package localPackage = (PackageParser.Package)this.mPackages.get(paramString);
            if (localPackage != null)
            {
                PackageSetting localPackageSetting = (PackageSetting)localPackage.mExtras;
                SharedUserSetting localSharedUserSetting = localPackageSetting.sharedUser;
                if (localSharedUserSetting != null);
                for (arrayOfInt = localSharedUserSetting.gids; ; arrayOfInt = localPackageSetting.gids)
                {
                    if (!isPermissionEnforcedLocked("android.permission.READ_EXTERNAL_STORAGE"))
                        arrayOfInt = appendInts(arrayOfInt, ((BasePermission)this.mSettings.mPermissions.get("android.permission.READ_EXTERNAL_STORAGE")).gids);
                    break;
                }
            }
            arrayOfInt = new int[0];
        }
        return arrayOfInt;
    }

    // ERROR //
    public PackageInfo getPackageInfo(String paramString, int paramInt1, int paramInt2)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore 4
        //     3: getstatic 604	com/android/server/pm/PackageManagerService:sUserManager	Lcom/android/server/pm/UserManager;
        //     6: iload_3
        //     7: invokevirtual 1607	com/android/server/pm/UserManager:exists	(I)Z
        //     10: ifne +6 -> 16
        //     13: aload 4
        //     15: areturn
        //     16: aload_0
        //     17: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     20: astore 5
        //     22: aload 5
        //     24: monitorenter
        //     25: aload_0
        //     26: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     29: aload_1
        //     30: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     33: checkcast 800	android/content/pm/PackageParser$Package
        //     36: astore 7
        //     38: aload 7
        //     40: ifnull +27 -> 67
        //     43: aload_0
        //     44: aload 7
        //     46: iload_2
        //     47: iload_3
        //     48: invokevirtual 1646	com/android/server/pm/PackageManagerService:generatePackageInfo	(Landroid/content/pm/PackageParser$Package;II)Landroid/content/pm/PackageInfo;
        //     51: astore 4
        //     53: aload 5
        //     55: monitorexit
        //     56: goto -43 -> 13
        //     59: astore 6
        //     61: aload 5
        //     63: monitorexit
        //     64: aload 6
        //     66: athrow
        //     67: iload_2
        //     68: sipush 8192
        //     71: iand
        //     72: ifeq +18 -> 90
        //     75: aload_0
        //     76: aload_1
        //     77: iload_2
        //     78: iload_3
        //     79: invokespecial 1611	com/android/server/pm/PackageManagerService:generatePackageInfoFromSettingsLPw	(Ljava/lang/String;II)Landroid/content/pm/PackageInfo;
        //     82: astore 4
        //     84: aload 5
        //     86: monitorexit
        //     87: goto -74 -> 13
        //     90: aload 5
        //     92: monitorexit
        //     93: goto -80 -> 13
        //
        // Exception table:
        //     from	to	target	type
        //     25	64	59	finally
        //     75	93	59	finally
    }

    public void getPackageSizeInfo(String paramString, IPackageStatsObserver paramIPackageStatsObserver)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.GET_PACKAGE_SIZE", null);
        PackageStats localPackageStats = new PackageStats(paramString);
        Message localMessage = this.mHandler.obtainMessage(5);
        localMessage.obj = new MeasureParams(localPackageStats, paramIPackageStatsObserver);
        this.mHandler.sendMessage(localMessage);
    }

    // ERROR //
    public int getPackageUid(String paramString, int paramInt)
    {
        // Byte code:
        //     0: bipush 255
        //     2: istore_3
        //     3: getstatic 604	com/android/server/pm/PackageManagerService:sUserManager	Lcom/android/server/pm/UserManager;
        //     6: iload_2
        //     7: invokevirtual 1607	com/android/server/pm/UserManager:exists	(I)Z
        //     10: ifne +5 -> 15
        //     13: iload_3
        //     14: ireturn
        //     15: aload_0
        //     16: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     19: astore 4
        //     21: aload 4
        //     23: monitorenter
        //     24: aload_0
        //     25: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     28: aload_1
        //     29: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     32: checkcast 800	android/content/pm/PackageParser$Package
        //     35: astore 6
        //     37: aload 6
        //     39: ifnull +30 -> 69
        //     42: iload_2
        //     43: aload 6
        //     45: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     48: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     51: invokestatic 2816	android/os/UserId:getUid	(II)I
        //     54: istore_3
        //     55: aload 4
        //     57: monitorexit
        //     58: goto -45 -> 13
        //     61: astore 5
        //     63: aload 4
        //     65: monitorexit
        //     66: aload 5
        //     68: athrow
        //     69: aload_0
        //     70: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     73: getfield 783	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
        //     76: aload_1
        //     77: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     80: checkcast 785	com/android/server/pm/PackageSetting
        //     83: astore 7
        //     85: aload 7
        //     87: ifnull +22 -> 109
        //     90: aload 7
        //     92: getfield 1259	com/android/server/pm/PackageSetting:pkg	Landroid/content/pm/PackageParser$Package;
        //     95: ifnull +14 -> 109
        //     98: aload 7
        //     100: getfield 1259	com/android/server/pm/PackageSetting:pkg	Landroid/content/pm/PackageParser$Package;
        //     103: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     106: ifnonnull +9 -> 115
        //     109: aload 4
        //     111: monitorexit
        //     112: goto -99 -> 13
        //     115: aload 7
        //     117: getfield 1259	com/android/server/pm/PackageSetting:pkg	Landroid/content/pm/PackageParser$Package;
        //     120: astore 8
        //     122: aload 8
        //     124: ifnull +16 -> 140
        //     127: iload_2
        //     128: aload 8
        //     130: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     133: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     136: invokestatic 2816	android/os/UserId:getUid	(II)I
        //     139: istore_3
        //     140: aload 4
        //     142: monitorexit
        //     143: goto -130 -> 13
        //
        // Exception table:
        //     from	to	target	type
        //     24	66	61	finally
        //     69	143	61	finally
    }

    // ERROR //
    public String[] getPackagesForUid(int paramInt)
    {
        // Byte code:
        //     0: iload_1
        //     1: invokestatic 1182	android/os/UserId:getAppId	(I)I
        //     4: istore_2
        //     5: aload_0
        //     6: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     9: astore_3
        //     10: aload_3
        //     11: monitorenter
        //     12: aload_0
        //     13: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     16: iload_2
        //     17: invokevirtual 1802	com/android/server/pm/Settings:getUserIdLPr	(I)Ljava/lang/Object;
        //     20: astore 5
        //     22: aload 5
        //     24: instanceof 1804
        //     27: ifeq +82 -> 109
        //     30: aload 5
        //     32: checkcast 1804	com/android/server/pm/SharedUserSetting
        //     35: astore 8
        //     37: aload 8
        //     39: getfield 1807	com/android/server/pm/SharedUserSetting:packages	Ljava/util/HashSet;
        //     42: invokevirtual 3545	java/util/HashSet:size	()I
        //     45: anewarray 360	java/lang/String
        //     48: astore 6
        //     50: aload 8
        //     52: getfield 1807	com/android/server/pm/SharedUserSetting:packages	Ljava/util/HashSet;
        //     55: invokevirtual 1808	java/util/HashSet:iterator	()Ljava/util/Iterator;
        //     58: astore 9
        //     60: iconst_0
        //     61: istore 10
        //     63: aload 9
        //     65: invokeinterface 702 1 0
        //     70: ifeq +34 -> 104
        //     73: iload 10
        //     75: iconst_1
        //     76: iadd
        //     77: istore 11
        //     79: aload 6
        //     81: iload 10
        //     83: aload 9
        //     85: invokeinterface 706 1 0
        //     90: checkcast 785	com/android/server/pm/PackageSetting
        //     93: getfield 795	com/android/server/pm/PackageSettingBase:name	Ljava/lang/String;
        //     96: aastore
        //     97: iload 11
        //     99: istore 10
        //     101: goto -38 -> 63
        //     104: aload_3
        //     105: monitorexit
        //     106: goto +50 -> 156
        //     109: aload 5
        //     111: instanceof 785
        //     114: ifeq +37 -> 151
        //     117: aload 5
        //     119: checkcast 785	com/android/server/pm/PackageSetting
        //     122: astore 7
        //     124: iconst_1
        //     125: anewarray 360	java/lang/String
        //     128: astore 6
        //     130: aload 6
        //     132: iconst_0
        //     133: aload 7
        //     135: getfield 795	com/android/server/pm/PackageSettingBase:name	Ljava/lang/String;
        //     138: aastore
        //     139: aload_3
        //     140: monitorexit
        //     141: goto +15 -> 156
        //     144: astore 4
        //     146: aload_3
        //     147: monitorexit
        //     148: aload 4
        //     150: athrow
        //     151: aload_3
        //     152: monitorexit
        //     153: aconst_null
        //     154: astore 6
        //     156: aload 6
        //     158: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     12	148	144	finally
        //     151	153	144	finally
    }

    public PermissionGroupInfo getPermissionGroupInfo(String paramString, int paramInt)
    {
        synchronized (this.mPackages)
        {
            PermissionGroupInfo localPermissionGroupInfo = PackageParser.generatePermissionGroupInfo((PackageParser.PermissionGroup)this.mPermissionGroups.get(paramString), paramInt);
            return localPermissionGroupInfo;
        }
    }

    public PermissionInfo getPermissionInfo(String paramString, int paramInt)
    {
        PermissionInfo localPermissionInfo;
        synchronized (this.mPackages)
        {
            BasePermission localBasePermission = (BasePermission)this.mSettings.mPermissions.get(paramString);
            if (localBasePermission != null)
                localPermissionInfo = generatePermissionInfo(localBasePermission, paramInt);
            else
                localPermissionInfo = null;
        }
        return localPermissionInfo;
    }

    // ERROR //
    public List<ApplicationInfo> getPersistentApplications(int paramInt)
    {
        // Byte code:
        //     0: new 420	java/util/ArrayList
        //     3: dup
        //     4: invokespecial 421	java/util/ArrayList:<init>	()V
        //     7: astore_2
        //     8: aload_0
        //     9: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     12: astore_3
        //     13: aload_3
        //     14: monitorenter
        //     15: aload_0
        //     16: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     19: invokevirtual 690	java/util/HashMap:values	()Ljava/util/Collection;
        //     22: invokeinterface 696 1 0
        //     27: astore 5
        //     29: invokestatic 3134	android/os/UserId:getCallingUserId	()I
        //     32: istore 6
        //     34: aload 5
        //     36: invokeinterface 702 1 0
        //     41: ifeq +139 -> 180
        //     44: aload 5
        //     46: invokeinterface 706 1 0
        //     51: checkcast 800	android/content/pm/PackageParser$Package
        //     54: astore 7
        //     56: aload 7
        //     58: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     61: ifnull -27 -> 34
        //     64: bipush 8
        //     66: aload 7
        //     68: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     71: getfield 886	android/content/pm/ApplicationInfo:flags	I
        //     74: iand
        //     75: ifeq -41 -> 34
        //     78: aload_0
        //     79: getfield 3399	com/android/server/pm/PackageManagerService:mSafeMode	Z
        //     82: ifeq +11 -> 93
        //     85: aload 7
        //     87: invokestatic 1491	com/android/server/pm/PackageManagerService:isSystemApp	(Landroid/content/pm/PackageParser$Package;)Z
        //     90: ifeq -56 -> 34
        //     93: aload_0
        //     94: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     97: getfield 783	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
        //     100: aload 7
        //     102: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     105: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     108: checkcast 785	com/android/server/pm/PackageSetting
        //     111: astore 8
        //     113: aload 8
        //     115: ifnull +53 -> 168
        //     118: aload 8
        //     120: iload 6
        //     122: invokevirtual 1617	com/android/server/pm/PackageSetting:getStopped	(I)Z
        //     125: istore 9
        //     127: aload 8
        //     129: ifnull +45 -> 174
        //     132: aload 8
        //     134: iload 6
        //     136: invokevirtual 1620	com/android/server/pm/PackageSetting:getEnabled	(I)I
        //     139: istore 10
        //     141: aload_2
        //     142: aload 7
        //     144: iload_1
        //     145: iload 9
        //     147: iload 10
        //     149: iload 6
        //     151: invokestatic 1624	android/content/pm/PackageParser:generateApplicationInfo	(Landroid/content/pm/PackageParser$Package;IZII)Landroid/content/pm/ApplicationInfo;
        //     154: invokevirtual 939	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     157: pop
        //     158: goto -124 -> 34
        //     161: astore 4
        //     163: aload_3
        //     164: monitorexit
        //     165: aload 4
        //     167: athrow
        //     168: iconst_0
        //     169: istore 9
        //     171: goto -44 -> 127
        //     174: iconst_0
        //     175: istore 10
        //     177: goto -36 -> 141
        //     180: aload_3
        //     181: monitorexit
        //     182: aload_2
        //     183: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     15	165	161	finally
        //     180	182	161	finally
    }

    // ERROR //
    public int getPreferredActivities(List<IntentFilter> paramList, List<ComponentName> paramList1, String paramString)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     4: astore 4
        //     6: aload 4
        //     8: monitorenter
        //     9: aload_0
        //     10: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     13: getfield 914	com/android/server/pm/Settings:mPreferredActivities	Lcom/android/server/IntentResolver;
        //     16: invokevirtual 3104	com/android/server/IntentResolver:filterIterator	()Ljava/util/Iterator;
        //     19: astore 6
        //     21: aload 6
        //     23: invokeinterface 702 1 0
        //     28: ifeq +87 -> 115
        //     31: aload 6
        //     33: invokeinterface 706 1 0
        //     38: checkcast 925	com/android/server/pm/PreferredActivity
        //     41: astore 7
        //     43: aload_3
        //     44: ifnull +21 -> 65
        //     47: aload 7
        //     49: getfield 933	com/android/server/pm/PreferredActivity:mPref	Lcom/android/server/PreferredComponent;
        //     52: getfield 938	com/android/server/PreferredComponent:mComponent	Landroid/content/ComponentName;
        //     55: invokevirtual 2532	android/content/ComponentName:getPackageName	()Ljava/lang/String;
        //     58: aload_3
        //     59: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     62: ifeq -41 -> 21
        //     65: aload_1
        //     66: ifnull +19 -> 85
        //     69: aload_1
        //     70: new 3068	android/content/IntentFilter
        //     73: dup
        //     74: aload 7
        //     76: invokespecial 3556	android/content/IntentFilter:<init>	(Landroid/content/IntentFilter;)V
        //     79: invokeinterface 841 2 0
        //     84: pop
        //     85: aload_2
        //     86: ifnull -65 -> 21
        //     89: aload_2
        //     90: aload 7
        //     92: getfield 933	com/android/server/pm/PreferredActivity:mPref	Lcom/android/server/PreferredComponent;
        //     95: getfield 938	com/android/server/PreferredComponent:mComponent	Landroid/content/ComponentName;
        //     98: invokeinterface 841 2 0
        //     103: pop
        //     104: goto -83 -> 21
        //     107: astore 5
        //     109: aload 4
        //     111: monitorexit
        //     112: aload 5
        //     114: athrow
        //     115: aload 4
        //     117: monitorexit
        //     118: iconst_0
        //     119: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     9	112	107	finally
        //     115	118	107	finally
    }

    public List<PackageInfo> getPreferredPackages(int paramInt)
    {
        return new ArrayList();
    }

    // ERROR //
    public ProviderInfo getProviderInfo(ComponentName paramComponentName, int paramInt1, int paramInt2)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore 4
        //     3: getstatic 604	com/android/server/pm/PackageManagerService:sUserManager	Lcom/android/server/pm/UserManager;
        //     6: iload_3
        //     7: invokevirtual 1607	com/android/server/pm/UserManager:exists	(I)Z
        //     10: ifne +6 -> 16
        //     13: aload 4
        //     15: areturn
        //     16: aload_0
        //     17: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     20: astore 5
        //     22: aload 5
        //     24: monitorenter
        //     25: aload_0
        //     26: getfield 403	com/android/server/pm/PackageManagerService:mProvidersByComponent	Ljava/util/HashMap;
        //     29: aload_1
        //     30: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     33: checkcast 2509	android/content/pm/PackageParser$Provider
        //     36: astore 7
        //     38: aload 7
        //     40: ifnull +85 -> 125
        //     43: aload_0
        //     44: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     47: aload 7
        //     49: getfield 2512	android/content/pm/PackageParser$Provider:info	Landroid/content/pm/ProviderInfo;
        //     52: iload_2
        //     53: iload_3
        //     54: invokevirtual 3452	com/android/server/pm/Settings:isEnabledLPr	(Landroid/content/pm/ComponentInfo;II)Z
        //     57: ifeq +68 -> 125
        //     60: aload_0
        //     61: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     64: getfield 783	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
        //     67: aload_1
        //     68: invokevirtual 2532	android/content/ComponentName:getPackageName	()Ljava/lang/String;
        //     71: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     74: checkcast 785	com/android/server/pm/PackageSetting
        //     77: astore 8
        //     79: aload 8
        //     81: ifnonnull +17 -> 98
        //     84: aload 5
        //     86: monitorexit
        //     87: goto -74 -> 13
        //     90: astore 6
        //     92: aload 5
        //     94: monitorexit
        //     95: aload 6
        //     97: athrow
        //     98: aload 7
        //     100: iload_2
        //     101: aload 8
        //     103: iload_3
        //     104: invokevirtual 1617	com/android/server/pm/PackageSetting:getStopped	(I)Z
        //     107: aload 8
        //     109: iload_3
        //     110: invokevirtual 1620	com/android/server/pm/PackageSetting:getEnabled	(I)I
        //     113: iload_3
        //     114: invokestatic 3563	android/content/pm/PackageParser:generateProviderInfo	(Landroid/content/pm/PackageParser$Provider;IZII)Landroid/content/pm/ProviderInfo;
        //     117: astore 4
        //     119: aload 5
        //     121: monitorexit
        //     122: goto -109 -> 13
        //     125: aload 5
        //     127: monitorexit
        //     128: goto -115 -> 13
        //
        // Exception table:
        //     from	to	target	type
        //     25	95	90	finally
        //     98	128	90	finally
    }

    // ERROR //
    public ActivityInfo getReceiverInfo(ComponentName paramComponentName, int paramInt1, int paramInt2)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore 4
        //     3: getstatic 604	com/android/server/pm/PackageManagerService:sUserManager	Lcom/android/server/pm/UserManager;
        //     6: iload_3
        //     7: invokevirtual 1607	com/android/server/pm/UserManager:exists	(I)Z
        //     10: ifne +6 -> 16
        //     13: aload 4
        //     15: areturn
        //     16: aload_0
        //     17: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     20: astore 5
        //     22: aload 5
        //     24: monitorenter
        //     25: aload_0
        //     26: getfield 398	com/android/server/pm/PackageManagerService:mReceivers	Lcom/android/server/pm/PackageManagerService$ActivityIntentResolver;
        //     29: invokestatic 929	com/android/server/pm/PackageManagerService$ActivityIntentResolver:access$800	(Lcom/android/server/pm/PackageManagerService$ActivityIntentResolver;)Ljava/util/HashMap;
        //     32: aload_1
        //     33: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     36: checkcast 2680	android/content/pm/PackageParser$Activity
        //     39: astore 7
        //     41: aload 7
        //     43: ifnull +85 -> 128
        //     46: aload_0
        //     47: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     50: aload 7
        //     52: getfield 2682	android/content/pm/PackageParser$Activity:info	Landroid/content/pm/ActivityInfo;
        //     55: iload_2
        //     56: iload_3
        //     57: invokevirtual 3452	com/android/server/pm/Settings:isEnabledLPr	(Landroid/content/pm/ComponentInfo;II)Z
        //     60: ifeq +68 -> 128
        //     63: aload_0
        //     64: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     67: getfield 783	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
        //     70: aload_1
        //     71: invokevirtual 2532	android/content/ComponentName:getPackageName	()Ljava/lang/String;
        //     74: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     77: checkcast 785	com/android/server/pm/PackageSetting
        //     80: astore 8
        //     82: aload 8
        //     84: ifnonnull +17 -> 101
        //     87: aload 5
        //     89: monitorexit
        //     90: goto -77 -> 13
        //     93: astore 6
        //     95: aload 5
        //     97: monitorexit
        //     98: aload 6
        //     100: athrow
        //     101: aload 7
        //     103: iload_2
        //     104: aload 8
        //     106: iload_3
        //     107: invokevirtual 1617	com/android/server/pm/PackageSetting:getStopped	(I)Z
        //     110: aload 8
        //     112: iload_3
        //     113: invokevirtual 1620	com/android/server/pm/PackageSetting:getEnabled	(I)I
        //     116: iload_3
        //     117: invokestatic 3456	android/content/pm/PackageParser:generateActivityInfo	(Landroid/content/pm/PackageParser$Activity;IZII)Landroid/content/pm/ActivityInfo;
        //     120: astore 4
        //     122: aload 5
        //     124: monitorexit
        //     125: goto -112 -> 13
        //     128: aload 5
        //     130: monitorexit
        //     131: goto -118 -> 13
        //
        // Exception table:
        //     from	to	target	type
        //     25	98	93	finally
        //     101	131	93	finally
    }

    // ERROR //
    public ServiceInfo getServiceInfo(ComponentName paramComponentName, int paramInt1, int paramInt2)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore 4
        //     3: getstatic 604	com/android/server/pm/PackageManagerService:sUserManager	Lcom/android/server/pm/UserManager;
        //     6: iload_3
        //     7: invokevirtual 1607	com/android/server/pm/UserManager:exists	(I)Z
        //     10: ifne +6 -> 16
        //     13: aload 4
        //     15: areturn
        //     16: aload_0
        //     17: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     20: astore 5
        //     22: aload 5
        //     24: monitorenter
        //     25: aload_0
        //     26: getfield 401	com/android/server/pm/PackageManagerService:mServices	Lcom/android/server/pm/PackageManagerService$ServiceIntentResolver;
        //     29: invokestatic 3570	com/android/server/pm/PackageManagerService$ServiceIntentResolver:access$900	(Lcom/android/server/pm/PackageManagerService$ServiceIntentResolver;)Ljava/util/HashMap;
        //     32: aload_1
        //     33: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     36: checkcast 2666	android/content/pm/PackageParser$Service
        //     39: astore 7
        //     41: aload 7
        //     43: ifnull +85 -> 128
        //     46: aload_0
        //     47: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     50: aload 7
        //     52: getfield 2669	android/content/pm/PackageParser$Service:info	Landroid/content/pm/ServiceInfo;
        //     55: iload_2
        //     56: iload_3
        //     57: invokevirtual 3452	com/android/server/pm/Settings:isEnabledLPr	(Landroid/content/pm/ComponentInfo;II)Z
        //     60: ifeq +68 -> 128
        //     63: aload_0
        //     64: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     67: getfield 783	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
        //     70: aload_1
        //     71: invokevirtual 2532	android/content/ComponentName:getPackageName	()Ljava/lang/String;
        //     74: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     77: checkcast 785	com/android/server/pm/PackageSetting
        //     80: astore 8
        //     82: aload 8
        //     84: ifnonnull +17 -> 101
        //     87: aload 5
        //     89: monitorexit
        //     90: goto -77 -> 13
        //     93: astore 6
        //     95: aload 5
        //     97: monitorexit
        //     98: aload 6
        //     100: athrow
        //     101: aload 7
        //     103: iload_2
        //     104: aload 8
        //     106: iload_3
        //     107: invokevirtual 1617	com/android/server/pm/PackageSetting:getStopped	(I)Z
        //     110: aload 8
        //     112: iload_3
        //     113: invokevirtual 1620	com/android/server/pm/PackageSetting:getEnabled	(I)I
        //     116: iload_3
        //     117: invokestatic 3574	android/content/pm/PackageParser:generateServiceInfo	(Landroid/content/pm/PackageParser$Service;IZII)Landroid/content/pm/ServiceInfo;
        //     120: astore 4
        //     122: aload 5
        //     124: monitorexit
        //     125: goto -112 -> 13
        //     128: aload 5
        //     130: monitorexit
        //     131: goto -118 -> 13
        //
        // Exception table:
        //     from	to	target	type
        //     25	98	93	finally
        //     101	131	93	finally
    }

    public FeatureInfo[] getSystemAvailableFeatures()
    {
        FeatureInfo[] arrayOfFeatureInfo;
        synchronized (this.mPackages)
        {
            Collection localCollection = this.mAvailableFeatures.values();
            int i = localCollection.size();
            if (i > 0)
            {
                arrayOfFeatureInfo = new FeatureInfo[i + 1];
                localCollection.toArray(arrayOfFeatureInfo);
                FeatureInfo localFeatureInfo = new FeatureInfo();
                localFeatureInfo.reqGlEsVersion = SystemProperties.getInt("ro.opengles.version", 0);
                arrayOfFeatureInfo[i] = localFeatureInfo;
            }
            else
            {
                arrayOfFeatureInfo = null;
            }
        }
        return arrayOfFeatureInfo;
    }

    public String[] getSystemSharedLibraryNames()
    {
        String[] arrayOfString;
        synchronized (this.mPackages)
        {
            Set localSet = this.mSharedLibraries.keySet();
            int i = localSet.size();
            if (i > 0)
            {
                arrayOfString = new String[i];
                localSet.toArray(arrayOfString);
            }
            else
            {
                arrayOfString = null;
            }
        }
        return arrayOfString;
    }

    // ERROR //
    public int getUidForSharedUser(String paramString)
    {
        // Byte code:
        //     0: bipush 255
        //     2: istore_2
        //     3: aload_1
        //     4: ifnonnull +5 -> 9
        //     7: iload_2
        //     8: ireturn
        //     9: aload_0
        //     10: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     13: astore_3
        //     14: aload_3
        //     15: monitorenter
        //     16: aload_0
        //     17: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     20: aload_1
        //     21: iconst_0
        //     22: iconst_0
        //     23: invokevirtual 2456	com/android/server/pm/Settings:getSharedUserLPw	(Ljava/lang/String;IZ)Lcom/android/server/pm/SharedUserSetting;
        //     26: astore 5
        //     28: aload 5
        //     30: ifnonnull +15 -> 45
        //     33: aload_3
        //     34: monitorexit
        //     35: goto -28 -> 7
        //     38: astore 4
        //     40: aload_3
        //     41: monitorexit
        //     42: aload 4
        //     44: athrow
        //     45: aload 5
        //     47: getfield 3523	com/android/server/pm/SharedUserSetting:userId	I
        //     50: istore_2
        //     51: aload_3
        //     52: monitorexit
        //     53: goto -46 -> 7
        //
        // Exception table:
        //     from	to	target	type
        //     16	42	38	finally
        //     45	53	38	finally
    }

    public UserInfo getUser(int paramInt)
    {
        enforceSystemOrRoot("Only the system can remove users");
        return sUserManager.getUser(paramInt);
    }

    public List<UserInfo> getUsers()
    {
        enforceSystemOrRoot("Only the system can query users");
        return sUserManager.getUsers();
    }

    public VerifierDeviceIdentity getVerifierDeviceIdentity()
        throws RemoteException
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.PACKAGE_VERIFICATION_AGENT", "Only package verification agents can read the verifier device identity");
        synchronized (this.mPackages)
        {
            VerifierDeviceIdentity localVerifierDeviceIdentity = this.mSettings.getVerifierDeviceIdentityLPw();
            return localVerifierDeviceIdentity;
        }
    }

    // ERROR //
    public void grantPermission(String paramString1, String paramString2)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 474	com/android/server/pm/PackageManagerService:mContext	Landroid/content/Context;
        //     4: ldc_w 3607
        //     7: aconst_null
        //     8: invokevirtual 3056	android/content/Context:enforceCallingOrSelfPermission	(Ljava/lang/String;Ljava/lang/String;)V
        //     11: aload_0
        //     12: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     15: astore_3
        //     16: aload_3
        //     17: monitorenter
        //     18: aload_0
        //     19: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     22: aload_1
        //     23: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     26: checkcast 800	android/content/pm/PackageParser$Package
        //     29: astore 5
        //     31: aload 5
        //     33: ifnonnull +38 -> 71
        //     36: new 2866	java/lang/IllegalArgumentException
        //     39: dup
        //     40: new 662	java/lang/StringBuilder
        //     43: dup
        //     44: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     47: ldc_w 2878
        //     50: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     53: aload_1
        //     54: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     57: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     60: invokespecial 2869	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     63: athrow
        //     64: astore 4
        //     66: aload_3
        //     67: monitorexit
        //     68: aload 4
        //     70: athrow
        //     71: aload_0
        //     72: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     75: getfield 1851	com/android/server/pm/Settings:mPermissions	Ljava/util/HashMap;
        //     78: aload_2
        //     79: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     82: checkcast 1168	com/android/server/pm/BasePermission
        //     85: astore 6
        //     87: aload 6
        //     89: ifnonnull +31 -> 120
        //     92: new 2866	java/lang/IllegalArgumentException
        //     95: dup
        //     96: new 662	java/lang/StringBuilder
        //     99: dup
        //     100: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     103: ldc_w 3609
        //     106: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     109: aload_1
        //     110: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     113: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     116: invokespecial 2869	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     119: athrow
        //     120: aload 5
        //     122: getfield 1848	android/content/pm/PackageParser$Package:requestedPermissions	Ljava/util/ArrayList;
        //     125: aload_2
        //     126: invokevirtual 1989	java/util/ArrayList:contains	(Ljava/lang/Object;)Z
        //     129: ifne +41 -> 170
        //     132: new 1184	java/lang/SecurityException
        //     135: dup
        //     136: new 662	java/lang/StringBuilder
        //     139: dup
        //     140: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     143: ldc_w 1265
        //     146: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     149: aload_1
        //     150: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     153: ldc_w 3611
        //     156: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     159: aload_2
        //     160: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     163: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     166: invokespecial 1192	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     169: athrow
        //     170: bipush 32
        //     172: aload 6
        //     174: getfield 1660	com/android/server/pm/BasePermission:protectionLevel	I
        //     177: iand
        //     178: ifne +37 -> 215
        //     181: new 1184	java/lang/SecurityException
        //     184: dup
        //     185: new 662	java/lang/StringBuilder
        //     188: dup
        //     189: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     192: ldc_w 2724
        //     195: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     198: aload_2
        //     199: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     202: ldc_w 3613
        //     205: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     208: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     211: invokespecial 1192	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     214: athrow
        //     215: aload 5
        //     217: getfield 1827	android/content/pm/PackageParser$Package:mExtras	Ljava/lang/Object;
        //     220: checkcast 785	com/android/server/pm/PackageSetting
        //     223: astore 7
        //     225: aload 7
        //     227: ifnonnull +8 -> 235
        //     230: aload_3
        //     231: monitorexit
        //     232: goto +75 -> 307
        //     235: aload 7
        //     237: getfield 1831	com/android/server/pm/PackageSetting:sharedUser	Lcom/android/server/pm/SharedUserSetting;
        //     240: ifnull +60 -> 300
        //     243: aload 7
        //     245: getfield 1831	com/android/server/pm/PackageSetting:sharedUser	Lcom/android/server/pm/SharedUserSetting;
        //     248: astore 8
        //     250: aload 8
        //     252: getfield 1750	com/android/server/pm/GrantedPermissions:grantedPermissions	Ljava/util/HashSet;
        //     255: aload_2
        //     256: invokevirtual 650	java/util/HashSet:add	(Ljava/lang/Object;)Z
        //     259: ifeq +36 -> 295
        //     262: aload 7
        //     264: getfield 1888	com/android/server/pm/PackageSettingBase:haveGids	Z
        //     267: ifeq +21 -> 288
        //     270: aload 8
        //     272: aload 8
        //     274: getfield 1845	com/android/server/pm/GrantedPermissions:gids	[I
        //     277: aload 6
        //     279: getfield 1870	com/android/server/pm/BasePermission:gids	[I
        //     282: invokestatic 1872	com/android/server/pm/PackageManagerService:appendInts	([I[I)[I
        //     285: putfield 1845	com/android/server/pm/GrantedPermissions:gids	[I
        //     288: aload_0
        //     289: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     292: invokevirtual 951	com/android/server/pm/Settings:writeLPr	()V
        //     295: aload_3
        //     296: monitorexit
        //     297: goto +10 -> 307
        //     300: aload 7
        //     302: astore 8
        //     304: goto -54 -> 250
        //     307: return
        //
        // Exception table:
        //     from	to	target	type
        //     18	68	64	finally
        //     71	297	64	finally
    }

    public boolean hasSystemFeature(String paramString)
    {
        synchronized (this.mPackages)
        {
            boolean bool = this.mAvailableFeatures.containsKey(paramString);
            return bool;
        }
    }

    public boolean hasSystemUidErrors()
    {
        return this.mHasSystemUidErrors;
    }

    public void installPackage(Uri paramUri, IPackageInstallObserver paramIPackageInstallObserver, int paramInt)
    {
        installPackage(paramUri, paramIPackageInstallObserver, paramInt, null);
    }

    public void installPackage(Uri paramUri, IPackageInstallObserver paramIPackageInstallObserver, int paramInt, String paramString)
    {
        installPackageWithVerification(paramUri, paramIPackageInstallObserver, paramInt, paramString, null, null, null);
    }

    public void installPackageWithVerification(Uri paramUri1, IPackageInstallObserver paramIPackageInstallObserver, int paramInt, String paramString, Uri paramUri2, ManifestDigest paramManifestDigest, ContainerEncryptionParams paramContainerEncryptionParams)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.INSTALL_PACKAGES", null);
        int i = Binder.getCallingUid();
        if ((i == 2000) || (i == 0));
        for (int j = paramInt | 0x20; ; j = paramInt & 0xFFFFFFDF)
        {
            Message localMessage = this.mHandler.obtainMessage(5);
            localMessage.obj = new InstallParams(paramUri1, paramIPackageInstallObserver, j, paramString, paramUri2, paramManifestDigest, paramContainerEncryptionParams);
            this.mHandler.sendMessage(localMessage);
            return;
        }
    }

    public boolean isFirstBoot()
    {
        if (!this.mRestoredSettings);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isPermissionEnforced(String paramString)
    {
        synchronized (this.mPackages)
        {
            boolean bool = isPermissionEnforcedLocked(paramString);
            return bool;
        }
    }

    public boolean isProtectedBroadcast(String paramString)
    {
        synchronized (this.mPackages)
        {
            boolean bool = this.mProtectedBroadcasts.contains(paramString);
            return bool;
        }
    }

    public boolean isSafeMode()
    {
        return this.mSafeMode;
    }

    public boolean isStorageLow()
    {
        long l = Binder.clearCallingIdentity();
        try
        {
            boolean bool = ((DeviceStorageMonitorService)ServiceManager.getService("devicestoragemonitor")).isMemoryLow();
            return bool;
        }
        finally
        {
            Binder.restoreCallingIdentity(l);
        }
    }

    // ERROR //
    public void movePackage(String paramString, IPackageMoveObserver paramIPackageMoveObserver, int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 474	com/android/server/pm/PackageManagerService:mContext	Landroid/content/Context;
        //     4: ldc_w 3645
        //     7: aconst_null
        //     8: invokevirtual 3056	android/content/Context:enforceCallingOrSelfPermission	(Ljava/lang/String;Ljava/lang/String;)V
        //     11: iconst_1
        //     12: istore 4
        //     14: iconst_0
        //     15: istore 5
        //     17: iconst_0
        //     18: istore 6
        //     20: aload_0
        //     21: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     24: astore 7
        //     26: aload 7
        //     28: monitorenter
        //     29: aload_0
        //     30: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     33: aload_1
        //     34: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     37: checkcast 800	android/content/pm/PackageParser$Package
        //     40: astore 9
        //     42: aload 9
        //     44: ifnonnull +38 -> 82
        //     47: bipush 254
        //     49: istore 4
        //     51: iload 4
        //     53: iconst_1
        //     54: if_icmpeq +200 -> 254
        //     57: aload_0
        //     58: new 51	com/android/server/pm/PackageManagerService$MoveParams
        //     61: dup
        //     62: aload_0
        //     63: aconst_null
        //     64: aload_2
        //     65: iconst_0
        //     66: aload_1
        //     67: aconst_null
        //     68: bipush 255
        //     70: invokespecial 3648	com/android/server/pm/PackageManagerService$MoveParams:<init>	(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageManagerService$InstallArgs;Landroid/content/pm/IPackageMoveObserver;ILjava/lang/String;Ljava/lang/String;I)V
        //     73: iload 4
        //     75: invokespecial 1056	com/android/server/pm/PackageManagerService:processPendingMove	(Lcom/android/server/pm/PackageManagerService$MoveParams;I)V
        //     78: aload 7
        //     80: monitorexit
        //     81: return
        //     82: aload 9
        //     84: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     87: ifnull +27 -> 114
        //     90: aload 9
        //     92: invokestatic 1491	com/android/server/pm/PackageManagerService:isSystemApp	(Landroid/content/pm/PackageParser$Package;)Z
        //     95: ifeq +19 -> 114
        //     98: ldc 186
        //     100: ldc_w 3650
        //     103: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     106: pop
        //     107: bipush 253
        //     109: istore 4
        //     111: goto -60 -> 51
        //     114: aload 9
        //     116: getfield 3653	android/content/pm/PackageParser$Package:mOperationPending	Z
        //     119: ifeq +19 -> 138
        //     122: ldc 186
        //     124: ldc_w 3655
        //     127: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     130: pop
        //     131: bipush 249
        //     133: istore 4
        //     135: goto -84 -> 51
        //     138: iload_3
        //     139: iconst_2
        //     140: iand
        //     141: ifeq +45 -> 186
        //     144: iload_3
        //     145: iconst_1
        //     146: iand
        //     147: ifeq +39 -> 186
        //     150: ldc 186
        //     152: ldc_w 3657
        //     155: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     158: pop
        //     159: bipush 251
        //     161: istore 4
        //     163: iload 4
        //     165: iconst_1
        //     166: if_icmpne -115 -> 51
        //     169: aload 9
        //     171: iconst_1
        //     172: putfield 3653	android/content/pm/PackageParser$Package:mOperationPending	Z
        //     175: goto -124 -> 51
        //     178: astore 8
        //     180: aload 7
        //     182: monitorexit
        //     183: aload 8
        //     185: athrow
        //     186: iload_3
        //     187: iconst_2
        //     188: iand
        //     189: ifeq +163 -> 352
        //     192: bipush 8
        //     194: istore 6
        //     196: aload 9
        //     198: invokestatic 1005	com/android/server/pm/PackageManagerService:isExternal	(Landroid/content/pm/PackageParser$Package;)Z
        //     201: ifeq +158 -> 359
        //     204: bipush 8
        //     206: istore 5
        //     208: iload 6
        //     210: iload 5
        //     212: if_icmpne +19 -> 231
        //     215: ldc 186
        //     217: ldc_w 3659
        //     220: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     223: pop
        //     224: bipush 251
        //     226: istore 4
        //     228: goto -65 -> 163
        //     231: aload 9
        //     233: invokestatic 1472	com/android/server/pm/PackageManagerService:isForwardLocked	(Landroid/content/pm/PackageParser$Package;)Z
        //     236: ifeq -73 -> 163
        //     239: iload 5
        //     241: iconst_1
        //     242: ior
        //     243: istore 5
        //     245: iload 6
        //     247: iconst_1
        //     248: ior
        //     249: istore 6
        //     251: goto -88 -> 163
        //     254: aload_0
        //     255: getfield 566	com/android/server/pm/PackageManagerService:mHandler	Lcom/android/server/pm/PackageManagerService$PackageHandler;
        //     258: iconst_5
        //     259: invokevirtual 3534	com/android/server/pm/PackageManagerService$PackageHandler:obtainMessage	(I)Landroid/os/Message;
        //     262: astore 10
        //     264: aload_0
        //     265: iload 5
        //     267: aload 9
        //     269: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     272: getfield 1475	android/content/pm/ApplicationInfo:sourceDir	Ljava/lang/String;
        //     275: aload 9
        //     277: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     280: getfield 1478	android/content/pm/ApplicationInfo:publicSourceDir	Ljava/lang/String;
        //     283: aload 9
        //     285: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     288: getfield 1481	android/content/pm/ApplicationInfo:nativeLibraryDir	Ljava/lang/String;
        //     291: invokespecial 1483	com/android/server/pm/PackageManagerService:createInstallArgs	(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/android/server/pm/PackageManagerService$InstallArgs;
        //     294: astore 11
        //     296: aload 9
        //     298: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     301: getfield 1639	android/content/pm/ApplicationInfo:dataDir	Ljava/lang/String;
        //     304: astore 12
        //     306: aload 9
        //     308: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     311: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     314: istore 13
        //     316: aload 10
        //     318: new 51	com/android/server/pm/PackageManagerService$MoveParams
        //     321: dup
        //     322: aload_0
        //     323: aload 11
        //     325: aload_2
        //     326: iload 6
        //     328: aload_1
        //     329: aload 12
        //     331: iload 13
        //     333: invokespecial 3648	com/android/server/pm/PackageManagerService$MoveParams:<init>	(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageManagerService$InstallArgs;Landroid/content/pm/IPackageMoveObserver;ILjava/lang/String;Ljava/lang/String;I)V
        //     336: putfield 3542	android/os/Message:obj	Ljava/lang/Object;
        //     339: aload_0
        //     340: getfield 566	com/android/server/pm/PackageManagerService:mHandler	Lcom/android/server/pm/PackageManagerService$PackageHandler;
        //     343: aload 10
        //     345: invokevirtual 2952	com/android/server/pm/PackageManagerService$PackageHandler:sendMessage	(Landroid/os/Message;)Z
        //     348: pop
        //     349: goto -271 -> 78
        //     352: bipush 16
        //     354: istore 6
        //     356: goto -160 -> 196
        //     359: bipush 16
        //     361: istore 5
        //     363: goto -155 -> 208
        //
        // Exception table:
        //     from	to	target	type
        //     29	183	178	finally
        //     196	349	178	finally
    }

    public String nextPackageToClean(String paramString)
    {
        String str = null;
        synchronized (this.mPackages)
        {
            if (isExternalMediaAvailable())
            {
                if (paramString != null)
                    this.mSettings.mPackagesToBeCleaned.remove(paramString);
                if (this.mSettings.mPackagesToBeCleaned.size() > 0)
                    str = (String)this.mSettings.mPackagesToBeCleaned.get(0);
            }
        }
        return str;
    }

    public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
        throws RemoteException
    {
        try
        {
            boolean bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            return bool;
        }
        catch (RuntimeException localRuntimeException)
        {
            if ((!(localRuntimeException instanceof SecurityException)) && (!(localRuntimeException instanceof IllegalArgumentException)))
                Slog.e("PackageManager", "Package Manager Crash", localRuntimeException);
            throw localRuntimeException;
        }
    }

    // ERROR //
    public void performBootDexOpt()
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_1
        //     2: aload_0
        //     3: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     6: astore_2
        //     7: aload_2
        //     8: monitorenter
        //     9: aload_0
        //     10: getfield 423	com/android/server/pm/PackageManagerService:mDeferredDexOpt	Ljava/util/ArrayList;
        //     13: invokevirtual 848	java/util/ArrayList:size	()I
        //     16: ifle +26 -> 42
        //     19: new 420	java/util/ArrayList
        //     22: dup
        //     23: aload_0
        //     24: getfield 423	com/android/server/pm/PackageManagerService:mDeferredDexOpt	Ljava/util/ArrayList;
        //     27: invokespecial 3672	java/util/ArrayList:<init>	(Ljava/util/Collection;)V
        //     30: astore 4
        //     32: aload_0
        //     33: getfield 423	com/android/server/pm/PackageManagerService:mDeferredDexOpt	Ljava/util/ArrayList;
        //     36: invokevirtual 3673	java/util/ArrayList:clear	()V
        //     39: aload 4
        //     41: astore_1
        //     42: aload_2
        //     43: monitorexit
        //     44: aload_1
        //     45: ifnull +141 -> 186
        //     48: iconst_0
        //     49: istore 5
        //     51: iload 5
        //     53: aload_1
        //     54: invokevirtual 848	java/util/ArrayList:size	()I
        //     57: if_icmpge +129 -> 186
        //     60: aload_0
        //     61: invokevirtual 3675	com/android/server/pm/PackageManagerService:isFirstBoot	()Z
        //     64: ifne +63 -> 127
        //     67: invokestatic 2043	android/app/ActivityManagerNative:getDefault	()Landroid/app/IActivityManager;
        //     70: astore 11
        //     72: aload_0
        //     73: getfield 474	com/android/server/pm/PackageManagerService:mContext	Landroid/content/Context;
        //     76: invokevirtual 3679	android/content/Context:getResources	()Landroid/content/res/Resources;
        //     79: astore 12
        //     81: iconst_2
        //     82: anewarray 368	java/lang/Object
        //     85: astore 13
        //     87: aload 13
        //     89: iconst_0
        //     90: iload 5
        //     92: iconst_1
        //     93: iadd
        //     94: invokestatic 1249	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     97: aastore
        //     98: aload 13
        //     100: iconst_1
        //     101: aload_1
        //     102: invokevirtual 848	java/util/ArrayList:size	()I
        //     105: invokestatic 1249	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     108: aastore
        //     109: aload 11
        //     111: aload 12
        //     113: ldc_w 3680
        //     116: aload 13
        //     118: invokevirtual 3686	android/content/res/Resources:getString	(I[Ljava/lang/Object;)Ljava/lang/String;
        //     121: iconst_1
        //     122: invokeinterface 3690 3 0
        //     127: aload_1
        //     128: iload 5
        //     130: invokevirtual 851	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     133: checkcast 800	android/content/pm/PackageParser$Package
        //     136: astore 6
        //     138: aload_0
        //     139: getfield 371	com/android/server/pm/PackageManagerService:mInstallLock	Ljava/lang/Object;
        //     142: astore 7
        //     144: aload 7
        //     146: monitorenter
        //     147: aload 6
        //     149: getfield 2150	android/content/pm/PackageParser$Package:mDidDexOpt	Z
        //     152: ifne +12 -> 164
        //     155: aload_0
        //     156: aload 6
        //     158: iconst_0
        //     159: iconst_0
        //     160: invokespecial 2547	com/android/server/pm/PackageManagerService:performDexOptLI	(Landroid/content/pm/PackageParser$Package;ZZ)I
        //     163: pop
        //     164: aload 7
        //     166: monitorexit
        //     167: iinc 5 1
        //     170: goto -119 -> 51
        //     173: astore_3
        //     174: aload_2
        //     175: monitorexit
        //     176: aload_3
        //     177: athrow
        //     178: astore 8
        //     180: aload 7
        //     182: monitorexit
        //     183: aload 8
        //     185: athrow
        //     186: return
        //     187: astore 10
        //     189: goto -62 -> 127
        //     192: astore_3
        //     193: goto -19 -> 174
        //
        // Exception table:
        //     from	to	target	type
        //     9	32	173	finally
        //     42	44	173	finally
        //     174	176	173	finally
        //     147	167	178	finally
        //     180	183	178	finally
        //     67	127	187	android/os/RemoteException
        //     32	39	192	finally
    }

    // ERROR //
    public boolean performDexOpt(String paramString)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore_2
        //     2: ldc_w 3693
        //     5: invokestatic 3110	com/android/server/pm/PackageManagerService:enforceSystemOrRoot	(Ljava/lang/String;)V
        //     8: aload_0
        //     9: getfield 490	com/android/server/pm/PackageManagerService:mNoDexOpt	Z
        //     12: ifne +5 -> 17
        //     15: iload_2
        //     16: ireturn
        //     17: aload_0
        //     18: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     21: astore_3
        //     22: aload_3
        //     23: monitorenter
        //     24: aload_0
        //     25: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     28: aload_1
        //     29: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     32: checkcast 800	android/content/pm/PackageParser$Package
        //     35: astore 5
        //     37: aload 5
        //     39: ifnull +11 -> 50
        //     42: aload 5
        //     44: getfield 2150	android/content/pm/PackageParser$Package:mDidDexOpt	Z
        //     47: ifeq +15 -> 62
        //     50: aload_3
        //     51: monitorexit
        //     52: goto -37 -> 15
        //     55: astore 4
        //     57: aload_3
        //     58: monitorexit
        //     59: aload 4
        //     61: athrow
        //     62: aload_3
        //     63: monitorexit
        //     64: aload_0
        //     65: getfield 371	com/android/server/pm/PackageManagerService:mInstallLock	Ljava/lang/Object;
        //     68: astore 6
        //     70: aload 6
        //     72: monitorenter
        //     73: aload_0
        //     74: aload 5
        //     76: iconst_0
        //     77: iconst_0
        //     78: invokespecial 2547	com/android/server/pm/PackageManagerService:performDexOptLI	(Landroid/content/pm/PackageParser$Package;ZZ)I
        //     81: iconst_1
        //     82: if_icmpne +5 -> 87
        //     85: iconst_1
        //     86: istore_2
        //     87: aload 6
        //     89: monitorexit
        //     90: goto -75 -> 15
        //     93: astore 7
        //     95: aload 6
        //     97: monitorexit
        //     98: aload 7
        //     100: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     24	59	55	finally
        //     62	64	55	finally
        //     73	98	93	finally
    }

    public List<ProviderInfo> queryContentProviders(String paramString, int paramInt1, int paramInt2)
    {
        Iterator localIterator;
        int j;
        Object localObject3;
        synchronized (this.mPackages)
        {
            localIterator = this.mProvidersByComponent.values().iterator();
            if (paramString != null)
            {
                int m = UserId.getUserId(paramInt1);
                j = m;
                localObject3 = null;
            }
        }
        while (true)
            try
            {
                if (localIterator.hasNext())
                {
                    PackageParser.Provider localProvider = (PackageParser.Provider)localIterator.next();
                    PackageSetting localPackageSetting = (PackageSetting)this.mSettings.mPackages.get(localProvider.owner.packageName);
                    if ((localProvider.info.authority == null) || ((paramString != null) && ((!localProvider.info.processName.equals(paramString)) || (!UserId.isSameApp(localProvider.info.applicationInfo.uid, paramInt1)))) || (!this.mSettings.isEnabledLPr(localProvider.info, paramInt2, j)) || ((this.mSafeMode) && ((0x1 & localProvider.info.applicationInfo.flags) == 0)))
                        continue;
                    if (localObject3 != null)
                        continue;
                    localObject4 = new ArrayList(3);
                    if (localPackageSetting != null)
                    {
                        bool = localPackageSetting.getStopped(j);
                        if (localPackageSetting == null)
                            continue;
                        k = localPackageSetting.getEnabled(j);
                        ((ArrayList)localObject4).add(PackageParser.generateProviderInfo(localProvider, paramInt2, bool, k, j));
                        continue;
                        int i = UserId.getCallingUserId();
                        j = i;
                        break;
                    }
                    boolean bool = false;
                    continue;
                    int k = 0;
                    continue;
                }
                if (localObject3 != null)
                    Collections.sort((List)localObject3, mProviderInitOrderSorter);
                return localObject3;
                localObject1 = finally;
                throw localObject1;
            }
            finally
            {
                continue;
                Object localObject4 = localObject3;
                continue;
                localObject4 = localObject3;
                localObject3 = localObject4;
            }
    }

    // ERROR //
    public List<InstrumentationInfo> queryInstrumentation(String paramString, int paramInt)
    {
        // Byte code:
        //     0: new 420	java/util/ArrayList
        //     3: dup
        //     4: invokespecial 421	java/util/ArrayList:<init>	()V
        //     7: astore_3
        //     8: aload_0
        //     9: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     12: astore 4
        //     14: aload 4
        //     16: monitorenter
        //     17: aload_0
        //     18: getfield 407	com/android/server/pm/PackageManagerService:mInstrumentation	Ljava/util/HashMap;
        //     21: invokevirtual 690	java/util/HashMap:values	()Ljava/util/Collection;
        //     24: invokeinterface 696 1 0
        //     29: astore 6
        //     31: aload 6
        //     33: invokeinterface 702 1 0
        //     38: ifeq +56 -> 94
        //     41: aload 6
        //     43: invokeinterface 706 1 0
        //     48: checkcast 2735	android/content/pm/PackageParser$Instrumentation
        //     51: astore 7
        //     53: aload_1
        //     54: ifnull +18 -> 72
        //     57: aload_1
        //     58: aload 7
        //     60: getfield 2738	android/content/pm/PackageParser$Instrumentation:info	Landroid/content/pm/InstrumentationInfo;
        //     63: getfield 3706	android/content/pm/InstrumentationInfo:targetPackage	Ljava/lang/String;
        //     66: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     69: ifeq -38 -> 31
        //     72: aload_3
        //     73: aload 7
        //     75: iload_2
        //     76: invokestatic 3519	android/content/pm/PackageParser:generateInstrumentationInfo	(Landroid/content/pm/PackageParser$Instrumentation;I)Landroid/content/pm/InstrumentationInfo;
        //     79: invokevirtual 939	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     82: pop
        //     83: goto -52 -> 31
        //     86: astore 5
        //     88: aload 4
        //     90: monitorexit
        //     91: aload 5
        //     93: athrow
        //     94: aload 4
        //     96: monitorexit
        //     97: aload_3
        //     98: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     17	91	86	finally
        //     94	97	86	finally
    }

    // ERROR //
    public List<ResolveInfo> queryIntentActivities(Intent paramIntent, String paramString, int paramInt1, int paramInt2)
    {
        // Byte code:
        //     0: getstatic 604	com/android/server/pm/PackageManagerService:sUserManager	Lcom/android/server/pm/UserManager;
        //     3: iload 4
        //     5: invokevirtual 1607	com/android/server/pm/UserManager:exists	(I)Z
        //     8: ifne +9 -> 17
        //     11: aconst_null
        //     12: astore 6
        //     14: aload 6
        //     16: areturn
        //     17: aload_1
        //     18: invokevirtual 3710	android/content/Intent:getComponent	()Landroid/content/ComponentName;
        //     21: astore 5
        //     23: aload 5
        //     25: ifnonnull +21 -> 46
        //     28: aload_1
        //     29: invokevirtual 3403	android/content/Intent:getSelector	()Landroid/content/Intent;
        //     32: ifnull +14 -> 46
        //     35: aload_1
        //     36: invokevirtual 3403	android/content/Intent:getSelector	()Landroid/content/Intent;
        //     39: astore_1
        //     40: aload_1
        //     41: invokevirtual 3710	android/content/Intent:getComponent	()Landroid/content/ComponentName;
        //     44: astore 5
        //     46: aload 5
        //     48: ifnull +58 -> 106
        //     51: new 420	java/util/ArrayList
        //     54: dup
        //     55: iconst_1
        //     56: invokespecial 2116	java/util/ArrayList:<init>	(I)V
        //     59: astore 6
        //     61: aload_0
        //     62: aload 5
        //     64: iload_3
        //     65: iload 4
        //     67: invokevirtual 3415	com/android/server/pm/PackageManagerService:getActivityInfo	(Landroid/content/ComponentName;II)Landroid/content/pm/ActivityInfo;
        //     70: astore 7
        //     72: aload 7
        //     74: ifnull -60 -> 14
        //     77: new 432	android/content/pm/ResolveInfo
        //     80: dup
        //     81: invokespecial 433	android/content/pm/ResolveInfo:<init>	()V
        //     84: astore 8
        //     86: aload 8
        //     88: aload 7
        //     90: putfield 1744	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     93: aload 6
        //     95: aload 8
        //     97: invokeinterface 841 2 0
        //     102: pop
        //     103: goto -89 -> 14
        //     106: aload_0
        //     107: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     110: astore 10
        //     112: aload 10
        //     114: monitorenter
        //     115: aload_1
        //     116: invokevirtual 3713	android/content/Intent:getPackage	()Ljava/lang/String;
        //     119: astore 12
        //     121: aload 12
        //     123: ifnonnull +31 -> 154
        //     126: aload_0
        //     127: getfield 396	com/android/server/pm/PackageManagerService:mActivities	Lcom/android/server/pm/PackageManagerService$ActivityIntentResolver;
        //     130: aload_1
        //     131: aload_2
        //     132: iload_3
        //     133: iload 4
        //     135: invokevirtual 3715	com/android/server/pm/PackageManagerService$ActivityIntentResolver:queryIntent	(Landroid/content/Intent;Ljava/lang/String;II)Ljava/util/List;
        //     138: astore 6
        //     140: aload 10
        //     142: monitorexit
        //     143: goto -129 -> 14
        //     146: astore 11
        //     148: aload 10
        //     150: monitorexit
        //     151: aload 11
        //     153: athrow
        //     154: aload_0
        //     155: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     158: aload 12
        //     160: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     163: checkcast 800	android/content/pm/PackageParser$Package
        //     166: astore 13
        //     168: aload 13
        //     170: ifnull +36 -> 206
        //     173: aload_0
        //     174: getfield 396	com/android/server/pm/PackageManagerService:mActivities	Lcom/android/server/pm/PackageManagerService$ActivityIntentResolver;
        //     177: astore 14
        //     179: aload 13
        //     181: getfield 2691	android/content/pm/PackageParser$Package:activities	Ljava/util/ArrayList;
        //     184: astore 15
        //     186: aload 14
        //     188: aload_1
        //     189: aload_2
        //     190: iload_3
        //     191: aload 15
        //     193: iload 4
        //     195: invokevirtual 3719	com/android/server/pm/PackageManagerService$ActivityIntentResolver:queryIntentForPackage	(Landroid/content/Intent;Ljava/lang/String;ILjava/util/ArrayList;I)Ljava/util/List;
        //     198: astore 6
        //     200: aload 10
        //     202: monitorexit
        //     203: goto -189 -> 14
        //     206: new 420	java/util/ArrayList
        //     209: dup
        //     210: invokespecial 421	java/util/ArrayList:<init>	()V
        //     213: astore 6
        //     215: aload 10
        //     217: monitorexit
        //     218: goto -204 -> 14
        //
        // Exception table:
        //     from	to	target	type
        //     115	151	146	finally
        //     154	218	146	finally
    }

    public List<ResolveInfo> queryIntentActivityOptions(ComponentName paramComponentName, Intent[] paramArrayOfIntent, String[] paramArrayOfString, Intent paramIntent, String paramString, int paramInt1, int paramInt2)
    {
        if (!sUserManager.exists(paramInt2))
        {
            localObject1 = null;
            return localObject1;
        }
        String str1 = paramIntent.getAction();
        Object localObject1 = queryIntentActivities(paramIntent, paramString, paramInt1 | 0x40, paramInt2);
        int i = 0;
        if (paramArrayOfIntent != null)
        {
            int i4 = 0;
            if (i4 < paramArrayOfIntent.length)
            {
                Intent localIntent = paramArrayOfIntent[i4];
                if (localIntent == null);
                while (true)
                {
                    i4++;
                    break;
                    String str3 = localIntent.getAction();
                    if ((str1 != null) && (str1.equals(str3)))
                        str3 = null;
                    Object localObject2 = null;
                    ComponentName localComponentName = localIntent.getComponent();
                    String str4;
                    ActivityInfo localActivityInfo2;
                    if (localComponentName == null)
                    {
                        if (paramArrayOfString != null)
                        {
                            str4 = paramArrayOfString[i4];
                            localObject2 = resolveIntent(localIntent, str4, paramInt1, paramInt2);
                            if (localObject2 == null)
                                continue;
                            if (localObject2 == this.mResolveInfo);
                            localActivityInfo2 = ((ResolveInfo)localObject2).activityInfo;
                            localComponentName = new ComponentName(localActivityInfo2.applicationInfo.packageName, localActivityInfo2.name);
                        }
                    }
                    else
                    {
                        do
                        {
                            int i5 = ((List)localObject1).size();
                            for (int i6 = i; i6 < i5; i6++)
                            {
                                ResolveInfo localResolveInfo3 = (ResolveInfo)((List)localObject1).get(i6);
                                if (((localResolveInfo3.activityInfo.name.equals(localComponentName.getClassName())) && (localResolveInfo3.activityInfo.applicationInfo.packageName.equals(localComponentName.getPackageName()))) || ((str3 != null) && (localResolveInfo3.filter.matchAction(str3))))
                                {
                                    ((List)localObject1).remove(i6);
                                    if (localObject2 == null)
                                        localObject2 = localResolveInfo3;
                                    i6--;
                                    i5--;
                                }
                            }
                            str4 = null;
                            break;
                            localActivityInfo2 = getActivityInfo(localComponentName, paramInt1, paramInt2);
                        }
                        while (localActivityInfo2 != null);
                        continue;
                        if (localObject2 == null)
                        {
                            localObject2 = new ResolveInfo();
                            ((ResolveInfo)localObject2).activityInfo = localActivityInfo2;
                        }
                        ((List)localObject1).add(i, localObject2);
                        ((ResolveInfo)localObject2).specificIndex = i4;
                        i++;
                    }
                }
            }
        }
        int j = ((List)localObject1).size();
        int k = i;
        if (k < j - 1)
        {
            ResolveInfo localResolveInfo1 = (ResolveInfo)((List)localObject1).get(k);
            if (localResolveInfo1.filter == null);
            while (true)
            {
                k++;
                break;
                Iterator localIterator = localResolveInfo1.filter.actionsIterator();
                if (localIterator != null)
                {
                    while (localIterator.hasNext())
                    {
                        String str2 = (String)localIterator.next();
                        if ((str1 == null) || (!str1.equals(str2)))
                            for (int i3 = k + 1; i3 < j; i3++)
                            {
                                ResolveInfo localResolveInfo2 = (ResolveInfo)((List)localObject1).get(i3);
                                if ((localResolveInfo2.filter != null) && (localResolveInfo2.filter.hasAction(str2)))
                                {
                                    ((List)localObject1).remove(i3);
                                    i3--;
                                    j--;
                                }
                            }
                    }
                    if ((paramInt1 & 0x40) == 0)
                        localResolveInfo1.filter = null;
                }
            }
        }
        int i1;
        if (paramComponentName != null)
            i1 = ((List)localObject1).size();
        for (int i2 = 0; ; i2++)
            if (i2 < i1)
            {
                ActivityInfo localActivityInfo1 = ((ResolveInfo)((List)localObject1).get(i2)).activityInfo;
                if ((paramComponentName.getPackageName().equals(localActivityInfo1.applicationInfo.packageName)) && (paramComponentName.getClassName().equals(localActivityInfo1.name)))
                    ((List)localObject1).remove(i2);
            }
            else
            {
                if ((paramInt1 & 0x40) != 0)
                    break;
                int m = ((List)localObject1).size();
                for (int n = 0; n < m; n++)
                    ((ResolveInfo)((List)localObject1).get(n)).filter = null;
                break;
            }
    }

    // ERROR //
    public List<ResolveInfo> queryIntentReceivers(Intent paramIntent, String paramString, int paramInt1, int paramInt2)
    {
        // Byte code:
        //     0: getstatic 604	com/android/server/pm/PackageManagerService:sUserManager	Lcom/android/server/pm/UserManager;
        //     3: iload 4
        //     5: invokevirtual 1607	com/android/server/pm/UserManager:exists	(I)Z
        //     8: ifne +9 -> 17
        //     11: aconst_null
        //     12: astore 6
        //     14: aload 6
        //     16: areturn
        //     17: aload_1
        //     18: invokevirtual 3710	android/content/Intent:getComponent	()Landroid/content/ComponentName;
        //     21: astore 5
        //     23: aload 5
        //     25: ifnonnull +21 -> 46
        //     28: aload_1
        //     29: invokevirtual 3403	android/content/Intent:getSelector	()Landroid/content/Intent;
        //     32: ifnull +14 -> 46
        //     35: aload_1
        //     36: invokevirtual 3403	android/content/Intent:getSelector	()Landroid/content/Intent;
        //     39: astore_1
        //     40: aload_1
        //     41: invokevirtual 3710	android/content/Intent:getComponent	()Landroid/content/ComponentName;
        //     44: astore 5
        //     46: aload 5
        //     48: ifnull +58 -> 106
        //     51: new 420	java/util/ArrayList
        //     54: dup
        //     55: iconst_1
        //     56: invokespecial 2116	java/util/ArrayList:<init>	(I)V
        //     59: astore 6
        //     61: aload_0
        //     62: aload 5
        //     64: iload_3
        //     65: iload 4
        //     67: invokevirtual 3755	com/android/server/pm/PackageManagerService:getReceiverInfo	(Landroid/content/ComponentName;II)Landroid/content/pm/ActivityInfo;
        //     70: astore 7
        //     72: aload 7
        //     74: ifnull -60 -> 14
        //     77: new 432	android/content/pm/ResolveInfo
        //     80: dup
        //     81: invokespecial 433	android/content/pm/ResolveInfo:<init>	()V
        //     84: astore 8
        //     86: aload 8
        //     88: aload 7
        //     90: putfield 1744	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
        //     93: aload 6
        //     95: aload 8
        //     97: invokeinterface 841 2 0
        //     102: pop
        //     103: goto -89 -> 14
        //     106: aload_0
        //     107: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     110: astore 10
        //     112: aload 10
        //     114: monitorenter
        //     115: aload_1
        //     116: invokevirtual 3713	android/content/Intent:getPackage	()Ljava/lang/String;
        //     119: astore 12
        //     121: aload 12
        //     123: ifnonnull +31 -> 154
        //     126: aload_0
        //     127: getfield 398	com/android/server/pm/PackageManagerService:mReceivers	Lcom/android/server/pm/PackageManagerService$ActivityIntentResolver;
        //     130: aload_1
        //     131: aload_2
        //     132: iload_3
        //     133: iload 4
        //     135: invokevirtual 3715	com/android/server/pm/PackageManagerService$ActivityIntentResolver:queryIntent	(Landroid/content/Intent;Ljava/lang/String;II)Ljava/util/List;
        //     138: astore 6
        //     140: aload 10
        //     142: monitorexit
        //     143: goto -129 -> 14
        //     146: astore 11
        //     148: aload 10
        //     150: monitorexit
        //     151: aload 11
        //     153: athrow
        //     154: aload_0
        //     155: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     158: aload 12
        //     160: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     163: checkcast 800	android/content/pm/PackageParser$Package
        //     166: astore 13
        //     168: aload 13
        //     170: ifnull +36 -> 206
        //     173: aload_0
        //     174: getfield 398	com/android/server/pm/PackageManagerService:mReceivers	Lcom/android/server/pm/PackageManagerService$ActivityIntentResolver;
        //     177: astore 14
        //     179: aload 13
        //     181: getfield 2678	android/content/pm/PackageParser$Package:receivers	Ljava/util/ArrayList;
        //     184: astore 15
        //     186: aload 14
        //     188: aload_1
        //     189: aload_2
        //     190: iload_3
        //     191: aload 15
        //     193: iload 4
        //     195: invokevirtual 3719	com/android/server/pm/PackageManagerService$ActivityIntentResolver:queryIntentForPackage	(Landroid/content/Intent;Ljava/lang/String;ILjava/util/ArrayList;I)Ljava/util/List;
        //     198: astore 6
        //     200: aload 10
        //     202: monitorexit
        //     203: goto -189 -> 14
        //     206: aconst_null
        //     207: astore 6
        //     209: aload 10
        //     211: monitorexit
        //     212: goto -198 -> 14
        //
        // Exception table:
        //     from	to	target	type
        //     115	151	146	finally
        //     154	212	146	finally
    }

    // ERROR //
    public List<ResolveInfo> queryIntentServices(Intent paramIntent, String paramString, int paramInt1, int paramInt2)
    {
        // Byte code:
        //     0: getstatic 604	com/android/server/pm/PackageManagerService:sUserManager	Lcom/android/server/pm/UserManager;
        //     3: iload 4
        //     5: invokevirtual 1607	com/android/server/pm/UserManager:exists	(I)Z
        //     8: ifne +9 -> 17
        //     11: aconst_null
        //     12: astore 6
        //     14: aload 6
        //     16: areturn
        //     17: aload_1
        //     18: invokevirtual 3710	android/content/Intent:getComponent	()Landroid/content/ComponentName;
        //     21: astore 5
        //     23: aload 5
        //     25: ifnonnull +21 -> 46
        //     28: aload_1
        //     29: invokevirtual 3403	android/content/Intent:getSelector	()Landroid/content/Intent;
        //     32: ifnull +14 -> 46
        //     35: aload_1
        //     36: invokevirtual 3403	android/content/Intent:getSelector	()Landroid/content/Intent;
        //     39: astore_1
        //     40: aload_1
        //     41: invokevirtual 3710	android/content/Intent:getComponent	()Landroid/content/ComponentName;
        //     44: astore 5
        //     46: aload 5
        //     48: ifnull +58 -> 106
        //     51: new 420	java/util/ArrayList
        //     54: dup
        //     55: iconst_1
        //     56: invokespecial 2116	java/util/ArrayList:<init>	(I)V
        //     59: astore 6
        //     61: aload_0
        //     62: aload 5
        //     64: iload_3
        //     65: iload 4
        //     67: invokevirtual 3758	com/android/server/pm/PackageManagerService:getServiceInfo	(Landroid/content/ComponentName;II)Landroid/content/pm/ServiceInfo;
        //     70: astore 7
        //     72: aload 7
        //     74: ifnull -60 -> 14
        //     77: new 432	android/content/pm/ResolveInfo
        //     80: dup
        //     81: invokespecial 433	android/content/pm/ResolveInfo:<init>	()V
        //     84: astore 8
        //     86: aload 8
        //     88: aload 7
        //     90: putfield 3761	android/content/pm/ResolveInfo:serviceInfo	Landroid/content/pm/ServiceInfo;
        //     93: aload 6
        //     95: aload 8
        //     97: invokeinterface 841 2 0
        //     102: pop
        //     103: goto -89 -> 14
        //     106: aload_0
        //     107: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     110: astore 10
        //     112: aload 10
        //     114: monitorenter
        //     115: aload_1
        //     116: invokevirtual 3713	android/content/Intent:getPackage	()Ljava/lang/String;
        //     119: astore 12
        //     121: aload 12
        //     123: ifnonnull +31 -> 154
        //     126: aload_0
        //     127: getfield 401	com/android/server/pm/PackageManagerService:mServices	Lcom/android/server/pm/PackageManagerService$ServiceIntentResolver;
        //     130: aload_1
        //     131: aload_2
        //     132: iload_3
        //     133: iload 4
        //     135: invokevirtual 3762	com/android/server/pm/PackageManagerService$ServiceIntentResolver:queryIntent	(Landroid/content/Intent;Ljava/lang/String;II)Ljava/util/List;
        //     138: astore 6
        //     140: aload 10
        //     142: monitorexit
        //     143: goto -129 -> 14
        //     146: astore 11
        //     148: aload 10
        //     150: monitorexit
        //     151: aload 11
        //     153: athrow
        //     154: aload_0
        //     155: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     158: aload 12
        //     160: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     163: checkcast 800	android/content/pm/PackageParser$Package
        //     166: astore 13
        //     168: aload 13
        //     170: ifnull +36 -> 206
        //     173: aload_0
        //     174: getfield 401	com/android/server/pm/PackageManagerService:mServices	Lcom/android/server/pm/PackageManagerService$ServiceIntentResolver;
        //     177: astore 14
        //     179: aload 13
        //     181: getfield 2664	android/content/pm/PackageParser$Package:services	Ljava/util/ArrayList;
        //     184: astore 15
        //     186: aload 14
        //     188: aload_1
        //     189: aload_2
        //     190: iload_3
        //     191: aload 15
        //     193: iload 4
        //     195: invokevirtual 3763	com/android/server/pm/PackageManagerService$ServiceIntentResolver:queryIntentForPackage	(Landroid/content/Intent;Ljava/lang/String;ILjava/util/ArrayList;I)Ljava/util/List;
        //     198: astore 6
        //     200: aload 10
        //     202: monitorexit
        //     203: goto -189 -> 14
        //     206: aconst_null
        //     207: astore 6
        //     209: aload 10
        //     211: monitorexit
        //     212: goto -198 -> 14
        //
        // Exception table:
        //     from	to	target	type
        //     115	151	146	finally
        //     154	212	146	finally
    }

    // ERROR //
    public List<PermissionInfo> queryPermissionsByGroup(String paramString, int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     4: astore_3
        //     5: aload_3
        //     6: monitorenter
        //     7: new 420	java/util/ArrayList
        //     10: dup
        //     11: bipush 10
        //     13: invokespecial 2116	java/util/ArrayList:<init>	(I)V
        //     16: astore 4
        //     18: aload_0
        //     19: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     22: getfield 1851	com/android/server/pm/Settings:mPermissions	Ljava/util/HashMap;
        //     25: invokevirtual 690	java/util/HashMap:values	()Ljava/util/Collection;
        //     28: invokeinterface 696 1 0
        //     33: astore 6
        //     35: aload 6
        //     37: invokeinterface 702 1 0
        //     42: ifeq +107 -> 149
        //     45: aload 6
        //     47: invokeinterface 706 1 0
        //     52: checkcast 1168	com/android/server/pm/BasePermission
        //     55: astore 7
        //     57: aload_1
        //     58: ifnonnull +47 -> 105
        //     61: aload 7
        //     63: getfield 1652	com/android/server/pm/BasePermission:perm	Landroid/content/pm/PackageParser$Permission;
        //     66: ifnull +17 -> 83
        //     69: aload 7
        //     71: getfield 1652	com/android/server/pm/BasePermission:perm	Landroid/content/pm/PackageParser$Permission;
        //     74: getfield 1925	android/content/pm/PackageParser$Permission:info	Landroid/content/pm/PermissionInfo;
        //     77: getfield 2716	android/content/pm/PermissionInfo:group	Ljava/lang/String;
        //     80: ifnonnull -45 -> 35
        //     83: aload 4
        //     85: aload 7
        //     87: iload_2
        //     88: invokestatic 3551	com/android/server/pm/PackageManagerService:generatePermissionInfo	(Lcom/android/server/pm/BasePermission;I)Landroid/content/pm/PermissionInfo;
        //     91: invokevirtual 939	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     94: pop
        //     95: goto -60 -> 35
        //     98: astore 5
        //     100: aload_3
        //     101: monitorexit
        //     102: aload 5
        //     104: athrow
        //     105: aload 7
        //     107: getfield 1652	com/android/server/pm/BasePermission:perm	Landroid/content/pm/PackageParser$Permission;
        //     110: ifnull -75 -> 35
        //     113: aload_1
        //     114: aload 7
        //     116: getfield 1652	com/android/server/pm/BasePermission:perm	Landroid/content/pm/PackageParser$Permission;
        //     119: getfield 1925	android/content/pm/PackageParser$Permission:info	Landroid/content/pm/PermissionInfo;
        //     122: getfield 2716	android/content/pm/PermissionInfo:group	Ljava/lang/String;
        //     125: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     128: ifeq -93 -> 35
        //     131: aload 4
        //     133: aload 7
        //     135: getfield 1652	com/android/server/pm/BasePermission:perm	Landroid/content/pm/PackageParser$Permission;
        //     138: iload_2
        //     139: invokestatic 1655	android/content/pm/PackageParser:generatePermissionInfo	(Landroid/content/pm/PackageParser$Permission;I)Landroid/content/pm/PermissionInfo;
        //     142: invokevirtual 939	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     145: pop
        //     146: goto -111 -> 35
        //     149: aload 4
        //     151: invokevirtual 848	java/util/ArrayList:size	()I
        //     154: ifle +8 -> 162
        //     157: aload_3
        //     158: monitorexit
        //     159: goto +25 -> 184
        //     162: aload_0
        //     163: getfield 409	com/android/server/pm/PackageManagerService:mPermissionGroups	Ljava/util/HashMap;
        //     166: aload_1
        //     167: invokevirtual 1936	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
        //     170: ifeq +8 -> 178
        //     173: aload_3
        //     174: monitorexit
        //     175: goto +9 -> 184
        //     178: aconst_null
        //     179: astore 4
        //     181: goto -8 -> 173
        //     184: aload 4
        //     186: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     7	102	98	finally
        //     105	175	98	finally
    }

    // ERROR //
    @java.lang.Deprecated
    public void querySyncProviders(List<String> paramList, List<ProviderInfo> paramList1)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     4: astore_3
        //     5: aload_3
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 405	com/android/server/pm/PackageManagerService:mProviders	Ljava/util/HashMap;
        //     11: invokevirtual 1235	java/util/HashMap:entrySet	()Ljava/util/Set;
        //     14: invokeinterface 923 1 0
        //     19: astore 5
        //     21: invokestatic 3134	android/os/UserId:getCallingUserId	()I
        //     24: istore 6
        //     26: aload 5
        //     28: invokeinterface 702 1 0
        //     33: ifeq +164 -> 197
        //     36: aload 5
        //     38: invokeinterface 706 1 0
        //     43: checkcast 1237	java/util/Map$Entry
        //     46: astore 7
        //     48: aload 7
        //     50: invokeinterface 1240 1 0
        //     55: checkcast 2509	android/content/pm/PackageParser$Provider
        //     58: astore 8
        //     60: aload_0
        //     61: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     64: getfield 783	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
        //     67: aload 8
        //     69: getfield 3696	android/content/pm/PackageParser$Provider:owner	Landroid/content/pm/PackageParser$Package;
        //     72: getfield 1464	android/content/pm/PackageParser$Package:packageName	Ljava/lang/String;
        //     75: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     78: checkcast 785	com/android/server/pm/PackageSetting
        //     81: astore 9
        //     83: aload 8
        //     85: getfield 2653	android/content/pm/PackageParser$Provider:syncable	Z
        //     88: ifeq -62 -> 26
        //     91: aload_0
        //     92: getfield 3399	com/android/server/pm/PackageManagerService:mSafeMode	Z
        //     95: ifeq +19 -> 114
        //     98: iconst_1
        //     99: aload 8
        //     101: getfield 2512	android/content/pm/PackageParser$Provider:info	Landroid/content/pm/ProviderInfo;
        //     104: getfield 3355	android/content/pm/ProviderInfo:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     107: getfield 886	android/content/pm/ApplicationInfo:flags	I
        //     110: iand
        //     111: ifeq -85 -> 26
        //     114: aload_1
        //     115: aload 7
        //     117: invokeinterface 3352 1 0
        //     122: invokeinterface 841 2 0
        //     127: pop
        //     128: aload 9
        //     130: ifnull +55 -> 185
        //     133: aload 9
        //     135: iload 6
        //     137: invokevirtual 1617	com/android/server/pm/PackageSetting:getStopped	(I)Z
        //     140: istore 11
        //     142: aload 9
        //     144: ifnull +47 -> 191
        //     147: aload 9
        //     149: iload 6
        //     151: invokevirtual 1620	com/android/server/pm/PackageSetting:getEnabled	(I)I
        //     154: istore 12
        //     156: aload_2
        //     157: aload 8
        //     159: iconst_0
        //     160: iload 11
        //     162: iload 12
        //     164: iload 6
        //     166: invokestatic 3563	android/content/pm/PackageParser:generateProviderInfo	(Landroid/content/pm/PackageParser$Provider;IZII)Landroid/content/pm/ProviderInfo;
        //     169: invokeinterface 841 2 0
        //     174: pop
        //     175: goto -149 -> 26
        //     178: astore 4
        //     180: aload_3
        //     181: monitorexit
        //     182: aload 4
        //     184: athrow
        //     185: iconst_0
        //     186: istore 11
        //     188: goto -46 -> 142
        //     191: iconst_0
        //     192: istore 12
        //     194: goto -38 -> 156
        //     197: aload_3
        //     198: monitorexit
        //     199: return
        //
        // Exception table:
        //     from	to	target	type
        //     7	182	178	finally
        //     197	199	178	finally
    }

    void readPermission(XmlPullParser paramXmlPullParser, String paramString)
        throws IOException, XmlPullParserException
    {
        String str1 = paramString.intern();
        BasePermission localBasePermission = (BasePermission)this.mSettings.mPermissions.get(str1);
        if (localBasePermission == null)
        {
            localBasePermission = new BasePermission(str1, null, 1);
            this.mSettings.mPermissions.put(str1, localBasePermission);
        }
        int i = paramXmlPullParser.getDepth();
        int j;
        do
        {
            j = paramXmlPullParser.next();
            if ((j == 1) || ((j == 3) && (paramXmlPullParser.getDepth() <= i)))
                break;
        }
        while ((j == 3) || (j == 4));
        if ("group".equals(paramXmlPullParser.getName()))
        {
            String str2 = paramXmlPullParser.getAttributeValue(null, "gid");
            if (str2 == null)
                break label164;
            int k = Process.getGidForName(str2);
            localBasePermission.gids = ArrayUtils.appendInt(localBasePermission.gids, k);
        }
        while (true)
        {
            XmlUtils.skipCurrentTag(paramXmlPullParser);
            break;
            label164: Slog.w("PackageManager", "<group> without gid at " + paramXmlPullParser.getPositionDescription());
        }
    }

    void readPermissions()
    {
        File localFile1 = new File(Environment.getRootDirectory(), "etc/permissions");
        if ((!localFile1.exists()) || (!localFile1.isDirectory()))
            Slog.w("PackageManager", "No directory " + localFile1 + ", skipping");
        while (true)
        {
            return;
            if (!localFile1.canRead())
            {
                Slog.w("PackageManager", "Directory " + localFile1 + " cannot be read");
            }
            else
            {
                File[] arrayOfFile = localFile1.listFiles();
                int i = arrayOfFile.length;
                int j = 0;
                if (j < i)
                {
                    File localFile2 = arrayOfFile[j];
                    if (localFile2.getPath().endsWith("etc/permissions/platform.xml"));
                    while (true)
                    {
                        j++;
                        break;
                        if (!localFile2.getPath().endsWith(".xml"))
                            Slog.i("PackageManager", "Non-xml file " + localFile2 + " in " + localFile1 + " directory, ignoring");
                        else if (!localFile2.canRead())
                            Slog.w("PackageManager", "Permissions library file " + localFile2 + " cannot be read");
                        else
                            readPermissionsFromXml(localFile2);
                    }
                }
                readPermissionsFromXml(new File(Environment.getRootDirectory(), "etc/permissions/platform.xml"));
            }
        }
    }

    public void removePackageFromPreferred(String paramString)
    {
        Slog.w("PackageManager", "removePackageFromPreferred: this is now a no-op");
    }

    void removePackageLI(PackageParser.Package paramPackage, boolean paramBoolean)
    {
        while (true)
        {
            StringBuilder localStringBuilder1;
            int j;
            int i8;
            int k;
            StringBuilder localStringBuilder2;
            int m;
            PackageParser.Service localService;
            int n;
            StringBuilder localStringBuilder3;
            int i1;
            PackageParser.Activity localActivity2;
            int i2;
            StringBuilder localStringBuilder4;
            int i3;
            PackageParser.Activity localActivity1;
            int i4;
            StringBuilder localStringBuilder5;
            int i5;
            PackageParser.Permission localPermission;
            BasePermission localBasePermission;
            int i6;
            StringBuilder localStringBuilder6;
            int i7;
            PackageParser.Instrumentation localInstrumentation;
            synchronized (this.mPackages)
            {
                this.mPackages.remove(paramPackage.applicationInfo.packageName);
                if (paramPackage.mPath != null)
                    this.mAppDirs.remove(paramPackage.mPath);
                int i = paramPackage.providers.size();
                localStringBuilder1 = null;
                j = 0;
                if (j >= i)
                    break label796;
                PackageParser.Provider localProvider = (PackageParser.Provider)paramPackage.providers.get(j);
                this.mProvidersByComponent.remove(new ComponentName(localProvider.info.packageName, localProvider.info.name));
                if (localProvider.info.authority != null)
                {
                    String[] arrayOfString = localProvider.info.authority.split(";");
                    i8 = 0;
                    if (i8 < arrayOfString.length)
                    {
                        if (this.mProviders.get(arrayOfString[i8]) != localProvider)
                            break label790;
                        this.mProviders.remove(arrayOfString[i8]);
                        break label790;
                    }
                    if (paramBoolean)
                        if (localStringBuilder1 == null)
                        {
                            localStringBuilder1 = new StringBuilder(256);
                            localStringBuilder1.append(localProvider.info.name);
                        }
                }
            }
            continue;
            label790: i8++;
            continue;
            label796: if (localStringBuilder1 != null)
            {
                continue;
                m++;
                continue;
                if (localStringBuilder2 != null)
                {
                    continue;
                    i1++;
                    continue;
                    if (localStringBuilder3 != null)
                    {
                        continue;
                        i3++;
                        continue;
                        if (localStringBuilder4 != null)
                        {
                            continue;
                            i5++;
                            continue;
                            if (localStringBuilder5 != null)
                            {
                                continue;
                                i7++;
                                continue;
                                if (localStringBuilder6 == null);
                            }
                        }
                    }
                }
            }
        }
    }

    // ERROR //
    public void removePermission(String paramString)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_0
        //     8: aload_1
        //     9: invokespecial 3032	com/android/server/pm/PackageManagerService:checkPermissionTreeLP	(Ljava/lang/String;)Lcom/android/server/pm/BasePermission;
        //     12: pop
        //     13: aload_0
        //     14: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     17: getfield 1851	com/android/server/pm/Settings:mPermissions	Ljava/util/HashMap;
        //     20: aload_1
        //     21: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     24: checkcast 1168	com/android/server/pm/BasePermission
        //     27: astore 5
        //     29: aload 5
        //     31: ifnull +64 -> 95
        //     34: aload 5
        //     36: getfield 2982	com/android/server/pm/BasePermission:type	I
        //     39: iconst_2
        //     40: if_icmpeq +36 -> 76
        //     43: new 1184	java/lang/SecurityException
        //     46: dup
        //     47: new 662	java/lang/StringBuilder
        //     50: dup
        //     51: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     54: ldc_w 3037
        //     57: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     60: aload_1
        //     61: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     64: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     67: invokespecial 1192	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     70: athrow
        //     71: astore_3
        //     72: aload_2
        //     73: monitorexit
        //     74: aload_3
        //     75: athrow
        //     76: aload_0
        //     77: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     80: getfield 1851	com/android/server/pm/Settings:mPermissions	Ljava/util/HashMap;
        //     83: aload_1
        //     84: invokevirtual 2767	java/util/HashMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
        //     87: pop
        //     88: aload_0
        //     89: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     92: invokevirtual 951	com/android/server/pm/Settings:writeLPr	()V
        //     95: aload_2
        //     96: monitorexit
        //     97: return
        //
        // Exception table:
        //     from	to	target	type
        //     7	74	71	finally
        //     76	97	71	finally
    }

    public boolean removeUser(int paramInt)
    {
        enforceSystemOrRoot("Only the system can remove users");
        if ((paramInt == 0) || (!sUserManager.exists(paramInt)));
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            cleanUpUser(paramInt);
            if (sUserManager.removeUser(paramInt))
            {
                Intent localIntent = new Intent("android.intent.action.USER_REMOVED");
                localIntent.putExtra("android.intent.extra.user_id", paramInt);
                this.mContext.sendBroadcast(localIntent, "android.permission.MANAGE_ACCOUNTS");
            }
            sUserManager.removePackageFolders(paramInt);
        }
    }

    // ERROR //
    public void replacePreferredActivity(IntentFilter paramIntentFilter, int paramInt, ComponentName[] paramArrayOfComponentName, ComponentName paramComponentName)
    {
        // Byte code:
        //     0: aload_1
        //     1: invokevirtual 3832	android/content/IntentFilter:countActions	()I
        //     4: iconst_1
        //     5: if_icmpeq +14 -> 19
        //     8: new 2866	java/lang/IllegalArgumentException
        //     11: dup
        //     12: ldc_w 3834
        //     15: invokespecial 2869	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     18: athrow
        //     19: aload_1
        //     20: invokevirtual 3837	android/content/IntentFilter:countCategories	()I
        //     23: iconst_1
        //     24: if_icmpeq +14 -> 38
        //     27: new 2866	java/lang/IllegalArgumentException
        //     30: dup
        //     31: ldc_w 3839
        //     34: invokespecial 2869	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     37: athrow
        //     38: aload_1
        //     39: invokevirtual 3842	android/content/IntentFilter:countDataAuthorities	()I
        //     42: ifne +24 -> 66
        //     45: aload_1
        //     46: invokevirtual 3845	android/content/IntentFilter:countDataPaths	()I
        //     49: ifne +17 -> 66
        //     52: aload_1
        //     53: invokevirtual 3848	android/content/IntentFilter:countDataSchemes	()I
        //     56: ifne +10 -> 66
        //     59: aload_1
        //     60: invokevirtual 3851	android/content/IntentFilter:countDataTypes	()I
        //     63: ifeq +14 -> 77
        //     66: new 2866	java/lang/IllegalArgumentException
        //     69: dup
        //     70: ldc_w 3853
        //     73: invokespecial 2869	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     76: athrow
        //     77: aload_0
        //     78: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     81: astore 5
        //     83: aload 5
        //     85: monitorenter
        //     86: aload_0
        //     87: getfield 474	com/android/server/pm/PackageManagerService:mContext	Landroid/content/Context;
        //     90: ldc_w 3046
        //     93: invokevirtual 3049	android/content/Context:checkCallingOrSelfPermission	(Ljava/lang/String;)I
        //     96: ifeq +60 -> 156
        //     99: aload_0
        //     100: invokestatic 1176	android/os/Binder:getCallingUid	()I
        //     103: invokespecial 3051	com/android/server/pm/PackageManagerService:getUidTargetSdkVersionLockedLPr	(I)I
        //     106: bipush 8
        //     108: if_icmpge +37 -> 145
        //     111: ldc 186
        //     113: new 662	java/lang/StringBuilder
        //     116: dup
        //     117: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     120: ldc_w 3855
        //     123: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     126: invokestatic 1176	android/os/Binder:getCallingUid	()I
        //     129: invokevirtual 902	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     132: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     135: invokestatic 472	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     138: pop
        //     139: aload 5
        //     141: monitorexit
        //     142: goto +242 -> 384
        //     145: aload_0
        //     146: getfield 474	com/android/server/pm/PackageManagerService:mContext	Landroid/content/Context;
        //     149: ldc_w 3046
        //     152: aconst_null
        //     153: invokevirtual 3056	android/content/Context:enforceCallingOrSelfPermission	(Ljava/lang/String;Ljava/lang/String;)V
        //     156: aconst_null
        //     157: astore 7
        //     159: aload_0
        //     160: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     163: getfield 914	com/android/server/pm/Settings:mPreferredActivities	Lcom/android/server/IntentResolver;
        //     166: invokevirtual 3104	com/android/server/IntentResolver:filterIterator	()Ljava/util/Iterator;
        //     169: astore 8
        //     171: aload_1
        //     172: iconst_0
        //     173: invokevirtual 3857	android/content/IntentFilter:getAction	(I)Ljava/lang/String;
        //     176: astore 9
        //     178: aload_1
        //     179: iconst_0
        //     180: invokevirtual 3860	android/content/IntentFilter:getCategory	(I)Ljava/lang/String;
        //     183: astore 10
        //     185: aload 8
        //     187: invokeinterface 702 1 0
        //     192: ifeq +132 -> 324
        //     195: aload 8
        //     197: invokeinterface 706 1 0
        //     202: checkcast 925	com/android/server/pm/PreferredActivity
        //     205: astore 13
        //     207: aload 13
        //     209: iconst_0
        //     210: invokevirtual 3861	com/android/server/pm/PreferredActivity:getAction	(I)Ljava/lang/String;
        //     213: aload 9
        //     215: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     218: ifeq -33 -> 185
        //     221: aload 13
        //     223: iconst_0
        //     224: invokevirtual 3862	com/android/server/pm/PreferredActivity:getCategory	(I)Ljava/lang/String;
        //     227: aload 10
        //     229: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     232: ifeq -47 -> 185
        //     235: aload 7
        //     237: ifnonnull +12 -> 249
        //     240: new 420	java/util/ArrayList
        //     243: dup
        //     244: invokespecial 421	java/util/ArrayList:<init>	()V
        //     247: astore 7
        //     249: aload 7
        //     251: aload 13
        //     253: invokevirtual 939	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     256: pop
        //     257: ldc 186
        //     259: new 662	java/lang/StringBuilder
        //     262: dup
        //     263: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     266: ldc_w 3864
        //     269: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     272: aload 13
        //     274: getfield 933	com/android/server/pm/PreferredActivity:mPref	Lcom/android/server/PreferredComponent;
        //     277: getfield 938	com/android/server/PreferredComponent:mComponent	Landroid/content/ComponentName;
        //     280: invokevirtual 944	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     283: ldc_w 3060
        //     286: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     289: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     292: invokestatic 1365	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     295: pop
        //     296: aload_1
        //     297: new 3062	android/util/LogPrinter
        //     300: dup
        //     301: iconst_4
        //     302: ldc 186
        //     304: invokespecial 3064	android/util/LogPrinter:<init>	(ILjava/lang/String;)V
        //     307: ldc_w 3066
        //     310: invokevirtual 3072	android/content/IntentFilter:dump	(Landroid/util/Printer;Ljava/lang/String;)V
        //     313: goto -128 -> 185
        //     316: astore 6
        //     318: aload 5
        //     320: monitorexit
        //     321: aload 6
        //     323: athrow
        //     324: aload 7
        //     326: ifnull +46 -> 372
        //     329: iconst_0
        //     330: istore 11
        //     332: iload 11
        //     334: aload 7
        //     336: invokevirtual 848	java/util/ArrayList:size	()I
        //     339: if_icmpge +33 -> 372
        //     342: aload 7
        //     344: iload 11
        //     346: invokevirtual 851	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     349: checkcast 925	com/android/server/pm/PreferredActivity
        //     352: astore 12
        //     354: aload_0
        //     355: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     358: getfield 914	com/android/server/pm/Settings:mPreferredActivities	Lcom/android/server/IntentResolver;
        //     361: aload 12
        //     363: invokevirtual 948	com/android/server/IntentResolver:removeFilter	(Landroid/content/IntentFilter;)V
        //     366: iinc 11 1
        //     369: goto -37 -> 332
        //     372: aload_0
        //     373: aload_1
        //     374: iload_2
        //     375: aload_3
        //     376: aload 4
        //     378: invokevirtual 3866	com/android/server/pm/PackageManagerService:addPreferredActivity	(Landroid/content/IntentFilter;I[Landroid/content/ComponentName;Landroid/content/ComponentName;)V
        //     381: aload 5
        //     383: monitorexit
        //     384: return
        //
        // Exception table:
        //     from	to	target	type
        //     86	321	316	finally
        //     332	384	316	finally
    }

    public ProviderInfo resolveContentProvider(String paramString, int paramInt1, int paramInt2)
    {
        ProviderInfo localProviderInfo;
        if (!sUserManager.exists(paramInt2))
        {
            localProviderInfo = null;
            return localProviderInfo;
        }
        while (true)
        {
            synchronized (this.mPackages)
            {
                PackageParser.Provider localProvider = (PackageParser.Provider)this.mProviders.get(paramString);
                if (localProvider != null)
                {
                    localPackageSetting = (PackageSetting)this.mSettings.mPackages.get(localProvider.owner.packageName);
                    if ((localProvider == null) || (!this.mSettings.isEnabledLPr(localProvider.info, paramInt1, paramInt2)) || ((this.mSafeMode) && ((0x1 & localProvider.info.applicationInfo.flags) == 0)))
                        break label182;
                    if (localPackageSetting == null)
                        break label170;
                    bool = localPackageSetting.getStopped(paramInt2);
                    if (localPackageSetting == null)
                        break label176;
                    i = localPackageSetting.getEnabled(paramInt2);
                    localProviderInfo = PackageParser.generateProviderInfo(localProvider, paramInt1, bool, i, paramInt2);
                }
            }
            PackageSetting localPackageSetting = null;
            continue;
            label170: boolean bool = false;
            continue;
            label176: int i = 0;
            continue;
            label182: localProviderInfo = null;
        }
    }

    public ResolveInfo resolveIntent(Intent paramIntent, String paramString, int paramInt1, int paramInt2)
    {
        if (!sUserManager.exists(paramInt2));
        for (ResolveInfo localResolveInfo = null; ; localResolveInfo = chooseBestActivity(paramIntent, paramString, paramInt1, queryIntentActivities(paramIntent, paramString, paramInt1, paramInt2), paramInt2))
            return localResolveInfo;
    }

    public ResolveInfo resolveService(Intent paramIntent, String paramString, int paramInt1, int paramInt2)
    {
        ResolveInfo localResolveInfo = null;
        List localList = queryIntentServices(paramIntent, paramString, paramInt1, paramInt2);
        if (!sUserManager.exists(paramInt2));
        while (true)
        {
            return localResolveInfo;
            if ((localList != null) && (localList.size() >= 1))
                localResolveInfo = (ResolveInfo)localList.get(0);
        }
    }

    // ERROR //
    public void revokePermission(String paramString1, String paramString2)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     4: astore_3
        //     5: aload_3
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     11: aload_1
        //     12: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     15: checkcast 800	android/content/pm/PackageParser$Package
        //     18: astore 5
        //     20: aload 5
        //     22: ifnonnull +38 -> 60
        //     25: new 2866	java/lang/IllegalArgumentException
        //     28: dup
        //     29: new 662	java/lang/StringBuilder
        //     32: dup
        //     33: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     36: ldc_w 2878
        //     39: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     42: aload_1
        //     43: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     46: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     49: invokespecial 2869	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     52: athrow
        //     53: astore 4
        //     55: aload_3
        //     56: monitorexit
        //     57: aload 4
        //     59: athrow
        //     60: aload 5
        //     62: getfield 881	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
        //     65: getfield 1465	android/content/pm/ApplicationInfo:uid	I
        //     68: invokestatic 1176	android/os/Binder:getCallingUid	()I
        //     71: if_icmpeq +14 -> 85
        //     74: aload_0
        //     75: getfield 474	com/android/server/pm/PackageManagerService:mContext	Landroid/content/Context;
        //     78: ldc_w 3607
        //     81: aconst_null
        //     82: invokevirtual 3056	android/content/Context:enforceCallingOrSelfPermission	(Ljava/lang/String;Ljava/lang/String;)V
        //     85: aload_0
        //     86: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     89: getfield 1851	com/android/server/pm/Settings:mPermissions	Ljava/util/HashMap;
        //     92: aload_2
        //     93: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     96: checkcast 1168	com/android/server/pm/BasePermission
        //     99: astore 6
        //     101: aload 6
        //     103: ifnonnull +31 -> 134
        //     106: new 2866	java/lang/IllegalArgumentException
        //     109: dup
        //     110: new 662	java/lang/StringBuilder
        //     113: dup
        //     114: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     117: ldc_w 3609
        //     120: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     123: aload_1
        //     124: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     127: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     130: invokespecial 2869	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     133: athrow
        //     134: aload 5
        //     136: getfield 1848	android/content/pm/PackageParser$Package:requestedPermissions	Ljava/util/ArrayList;
        //     139: aload_2
        //     140: invokevirtual 1989	java/util/ArrayList:contains	(Ljava/lang/Object;)Z
        //     143: ifne +41 -> 184
        //     146: new 1184	java/lang/SecurityException
        //     149: dup
        //     150: new 662	java/lang/StringBuilder
        //     153: dup
        //     154: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     157: ldc_w 1265
        //     160: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     163: aload_1
        //     164: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     167: ldc_w 3611
        //     170: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     173: aload_2
        //     174: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     177: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     180: invokespecial 1192	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     183: athrow
        //     184: bipush 32
        //     186: aload 6
        //     188: getfield 1660	com/android/server/pm/BasePermission:protectionLevel	I
        //     191: iand
        //     192: ifne +37 -> 229
        //     195: new 1184	java/lang/SecurityException
        //     198: dup
        //     199: new 662	java/lang/StringBuilder
        //     202: dup
        //     203: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     206: ldc_w 2724
        //     209: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     212: aload_2
        //     213: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     216: ldc_w 3613
        //     219: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     222: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     225: invokespecial 1192	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     228: athrow
        //     229: aload 5
        //     231: getfield 1827	android/content/pm/PackageParser$Package:mExtras	Ljava/lang/Object;
        //     234: checkcast 785	com/android/server/pm/PackageSetting
        //     237: astore 7
        //     239: aload 7
        //     241: ifnonnull +8 -> 249
        //     244: aload_3
        //     245: monitorexit
        //     246: goto +85 -> 331
        //     249: aload 7
        //     251: getfield 1831	com/android/server/pm/PackageSetting:sharedUser	Lcom/android/server/pm/SharedUserSetting;
        //     254: ifnull +70 -> 324
        //     257: aload 7
        //     259: getfield 1831	com/android/server/pm/PackageSetting:sharedUser	Lcom/android/server/pm/SharedUserSetting;
        //     262: astore 8
        //     264: aload 8
        //     266: getfield 1750	com/android/server/pm/GrantedPermissions:grantedPermissions	Ljava/util/HashSet;
        //     269: aload_2
        //     270: invokevirtual 1251	java/util/HashSet:remove	(Ljava/lang/Object;)Z
        //     273: ifeq +46 -> 319
        //     276: aload 8
        //     278: getfield 1750	com/android/server/pm/GrantedPermissions:grantedPermissions	Ljava/util/HashSet;
        //     281: aload_2
        //     282: invokevirtual 1251	java/util/HashSet:remove	(Ljava/lang/Object;)Z
        //     285: pop
        //     286: aload 7
        //     288: getfield 1888	com/android/server/pm/PackageSettingBase:haveGids	Z
        //     291: ifeq +21 -> 312
        //     294: aload 8
        //     296: aload 8
        //     298: getfield 1845	com/android/server/pm/GrantedPermissions:gids	[I
        //     301: aload 6
        //     303: getfield 1870	com/android/server/pm/BasePermission:gids	[I
        //     306: invokestatic 1897	com/android/server/pm/PackageManagerService:removeInts	([I[I)[I
        //     309: putfield 1845	com/android/server/pm/GrantedPermissions:gids	[I
        //     312: aload_0
        //     313: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     316: invokevirtual 951	com/android/server/pm/Settings:writeLPr	()V
        //     319: aload_3
        //     320: monitorexit
        //     321: goto +10 -> 331
        //     324: aload 7
        //     326: astore 8
        //     328: goto -64 -> 264
        //     331: return
        //
        // Exception table:
        //     from	to	target	type
        //     7	57	53	finally
        //     60	321	53	finally
    }

    public void scanAvailableAsecs()
    {
        updateExternalMediaStatusInner(true, false, false);
    }

    void schedulePackageCleaning(String paramString)
    {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(7, paramString));
    }

    void scheduleWritePackageRestrictionsLocked(int paramInt)
    {
        if (!sUserManager.exists(paramInt));
        while (true)
        {
            return;
            this.mDirtyUsers.add(Integer.valueOf(paramInt));
            if (!this.mHandler.hasMessages(14))
                this.mHandler.sendEmptyMessageDelayed(14, 10000L);
        }
    }

    void scheduleWriteSettingsLocked()
    {
        if (!this.mHandler.hasMessages(13))
            this.mHandler.sendEmptyMessageDelayed(13, 10000L);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public void setApplicationEnabledSetting(String paramString, int paramInt1, int paramInt2, int paramInt3)
    {
        if (!sUserManager.exists(paramInt3));
        while (true)
        {
            return;
            if (!Injector.setAccessControl(this, paramString, paramInt1, paramInt2))
                setEnabledSetting(paramString, null, paramInt1, paramInt2, paramInt3);
        }
    }

    public void setComponentEnabledSetting(ComponentName paramComponentName, int paramInt1, int paramInt2, int paramInt3)
    {
        if (!sUserManager.exists(paramInt3));
        while (true)
        {
            return;
            setEnabledSetting(paramComponentName.getPackageName(), paramComponentName.getClassName(), paramInt1, paramInt2, paramInt3);
        }
    }

    public boolean setInstallLocation(int paramInt)
    {
        int i = 1;
        this.mContext.enforceCallingOrSelfPermission("android.permission.WRITE_SECURE_SETTINGS", null);
        if (getInstallLocation() == paramInt);
        while (true)
        {
            return i;
            if ((paramInt == 0) || (paramInt == i) || (paramInt == 2))
                Settings.System.putInt(this.mContext.getContentResolver(), "default_install_location", paramInt);
            else
                i = 0;
        }
    }

    // ERROR //
    public void setInstallerPackageName(String paramString1, String paramString2)
    {
        // Byte code:
        //     0: invokestatic 1176	android/os/Binder:getCallingUid	()I
        //     3: istore_3
        //     4: aload_0
        //     5: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     8: astore 4
        //     10: aload 4
        //     12: monitorenter
        //     13: aload_0
        //     14: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     17: getfield 783	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
        //     20: aload_1
        //     21: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     24: checkcast 785	com/android/server/pm/PackageSetting
        //     27: astore 6
        //     29: aload 6
        //     31: ifnonnull +39 -> 70
        //     34: new 2866	java/lang/IllegalArgumentException
        //     37: dup
        //     38: new 662	java/lang/StringBuilder
        //     41: dup
        //     42: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     45: ldc_w 3899
        //     48: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     51: aload_1
        //     52: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     55: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     58: invokespecial 2869	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     61: athrow
        //     62: astore 5
        //     64: aload 4
        //     66: monitorexit
        //     67: aload 5
        //     69: athrow
        //     70: aload_2
        //     71: ifnull +323 -> 394
        //     74: aload_0
        //     75: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     78: getfield 783	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
        //     81: aload_2
        //     82: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     85: checkcast 785	com/android/server/pm/PackageSetting
        //     88: astore 7
        //     90: aload 7
        //     92: ifnonnull +31 -> 123
        //     95: new 2866	java/lang/IllegalArgumentException
        //     98: dup
        //     99: new 662	java/lang/StringBuilder
        //     102: dup
        //     103: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     106: ldc_w 3901
        //     109: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     112: aload_2
        //     113: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     116: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     119: invokespecial 2869	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     122: athrow
        //     123: aload_0
        //     124: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     127: iload_3
        //     128: invokevirtual 1802	com/android/server/pm/Settings:getUserIdLPr	(I)Ljava/lang/Object;
        //     131: astore 8
        //     133: aload 8
        //     135: ifnull +136 -> 271
        //     138: aload 8
        //     140: instanceof 1804
        //     143: ifeq +65 -> 208
        //     146: aload 8
        //     148: checkcast 1804	com/android/server/pm/SharedUserSetting
        //     151: getfield 2500	com/android/server/pm/SharedUserSetting:signatures	Lcom/android/server/pm/PackageSignatures;
        //     154: getfield 1344	com/android/server/pm/PackageSignatures:mSignatures	[Landroid/content/pm/Signature;
        //     157: astore 9
        //     159: aload 7
        //     161: ifnull +138 -> 299
        //     164: aload 9
        //     166: aload 7
        //     168: getfield 1338	com/android/server/pm/PackageSettingBase:signatures	Lcom/android/server/pm/PackageSignatures;
        //     171: getfield 1344	com/android/server/pm/PackageSignatures:mSignatures	[Landroid/content/pm/Signature;
        //     174: invokestatic 1874	com/android/server/pm/PackageManagerService:compareSignatures	([Landroid/content/pm/Signature;[Landroid/content/pm/Signature;)I
        //     177: ifeq +122 -> 299
        //     180: new 1184	java/lang/SecurityException
        //     183: dup
        //     184: new 662	java/lang/StringBuilder
        //     187: dup
        //     188: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     191: ldc_w 3903
        //     194: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     197: aload_2
        //     198: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     201: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     204: invokespecial 1192	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     207: athrow
        //     208: aload 8
        //     210: instanceof 785
        //     213: ifeq +19 -> 232
        //     216: aload 8
        //     218: checkcast 785	com/android/server/pm/PackageSetting
        //     221: getfield 1338	com/android/server/pm/PackageSettingBase:signatures	Lcom/android/server/pm/PackageSignatures;
        //     224: getfield 1344	com/android/server/pm/PackageSignatures:mSignatures	[Landroid/content/pm/Signature;
        //     227: astore 9
        //     229: goto -70 -> 159
        //     232: new 1184	java/lang/SecurityException
        //     235: dup
        //     236: new 662	java/lang/StringBuilder
        //     239: dup
        //     240: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     243: ldc_w 3905
        //     246: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     249: aload 8
        //     251: invokevirtual 944	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     254: ldc_w 3907
        //     257: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     260: iload_3
        //     261: invokevirtual 902	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     264: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     267: invokespecial 1192	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     270: athrow
        //     271: new 1184	java/lang/SecurityException
        //     274: dup
        //     275: new 662	java/lang/StringBuilder
        //     278: dup
        //     279: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     282: ldc_w 3909
        //     285: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     288: iload_3
        //     289: invokevirtual 902	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     292: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     295: invokespecial 1192	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     298: athrow
        //     299: aload 6
        //     301: getfield 2328	com/android/server/pm/PackageSettingBase:installerPackageName	Ljava/lang/String;
        //     304: ifnull +76 -> 380
        //     307: aload_0
        //     308: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     311: getfield 783	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
        //     314: aload 6
        //     316: getfield 2328	com/android/server/pm/PackageSettingBase:installerPackageName	Ljava/lang/String;
        //     319: invokevirtual 798	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     322: checkcast 785	com/android/server/pm/PackageSetting
        //     325: astore 10
        //     327: aload 10
        //     329: ifnull +51 -> 380
        //     332: aload 9
        //     334: aload 10
        //     336: getfield 1338	com/android/server/pm/PackageSettingBase:signatures	Lcom/android/server/pm/PackageSignatures;
        //     339: getfield 1344	com/android/server/pm/PackageSignatures:mSignatures	[Landroid/content/pm/Signature;
        //     342: invokestatic 1874	com/android/server/pm/PackageManagerService:compareSignatures	([Landroid/content/pm/Signature;[Landroid/content/pm/Signature;)I
        //     345: ifeq +35 -> 380
        //     348: new 1184	java/lang/SecurityException
        //     351: dup
        //     352: new 662	java/lang/StringBuilder
        //     355: dup
        //     356: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     359: ldc_w 3911
        //     362: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     365: aload 6
        //     367: getfield 2328	com/android/server/pm/PackageSettingBase:installerPackageName	Ljava/lang/String;
        //     370: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     373: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     376: invokespecial 1192	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     379: athrow
        //     380: aload 6
        //     382: aload_2
        //     383: putfield 2328	com/android/server/pm/PackageSettingBase:installerPackageName	Ljava/lang/String;
        //     386: aload_0
        //     387: invokevirtual 3042	com/android/server/pm/PackageManagerService:scheduleWriteSettingsLocked	()V
        //     390: aload 4
        //     392: monitorexit
        //     393: return
        //     394: aconst_null
        //     395: astore 7
        //     397: goto -274 -> 123
        //
        // Exception table:
        //     from	to	target	type
        //     13	67	62	finally
        //     74	393	62	finally
    }

    public void setPackageStoppedState(String paramString, boolean paramBoolean, int paramInt)
    {
        if (!sUserManager.exists(paramInt))
            return;
        int i = Binder.getCallingUid();
        if (this.mContext.checkCallingOrSelfPermission("android.permission.CHANGE_COMPONENT_ENABLED_STATE") == 0);
        for (boolean bool = true; ; bool = false)
            while (true)
            {
                checkValidCaller(i, paramInt);
                synchronized (this.mPackages)
                {
                    if (this.mSettings.setPackageStoppedStateLPw(paramString, paramBoolean, bool, i, paramInt))
                        scheduleWritePackageRestrictionsLocked(paramInt);
                }
            }
    }

    // ERROR //
    public void setPermissionEnforced(String paramString, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 474	com/android/server/pm/PackageManagerService:mContext	Landroid/content/Context;
        //     4: ldc_w 3607
        //     7: aconst_null
        //     8: invokevirtual 3056	android/content/Context:enforceCallingOrSelfPermission	(Ljava/lang/String;Ljava/lang/String;)V
        //     11: ldc_w 2020
        //     14: aload_1
        //     15: invokevirtual 364	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     18: ifeq +103 -> 121
        //     21: aload_0
        //     22: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     25: astore_3
        //     26: aload_3
        //     27: monitorenter
        //     28: aload_0
        //     29: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     32: getfield 2024	com/android/server/pm/Settings:mReadExternalStorageEnforced	Ljava/lang/Boolean;
        //     35: ifnull +17 -> 52
        //     38: aload_0
        //     39: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     42: getfield 2024	com/android/server/pm/Settings:mReadExternalStorageEnforced	Ljava/lang/Boolean;
        //     45: invokevirtual 2029	java/lang/Boolean:booleanValue	()Z
        //     48: iload_2
        //     49: if_icmpeq +52 -> 101
        //     52: aload_0
        //     53: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     56: iload_2
        //     57: invokestatic 3314	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
        //     60: putfield 2024	com/android/server/pm/Settings:mReadExternalStorageEnforced	Ljava/lang/Boolean;
        //     63: aload_0
        //     64: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     67: invokevirtual 951	com/android/server/pm/Settings:writeLPr	()V
        //     70: invokestatic 2043	android/app/ActivityManagerNative:getDefault	()Landroid/app/IActivityManager;
        //     73: astore 5
        //     75: aload 5
        //     77: ifnull +24 -> 101
        //     80: invokestatic 2902	android/os/Binder:clearCallingIdentity	()J
        //     83: lstore 6
        //     85: aload 5
        //     87: ldc_w 3922
        //     90: invokeinterface 3925 2 0
        //     95: pop
        //     96: lload 6
        //     98: invokestatic 2905	android/os/Binder:restoreCallingIdentity	(J)V
        //     101: aload_3
        //     102: monitorexit
        //     103: return
        //     104: astore 9
        //     106: lload 6
        //     108: invokestatic 2905	android/os/Binder:restoreCallingIdentity	(J)V
        //     111: aload 9
        //     113: athrow
        //     114: astore 4
        //     116: aload_3
        //     117: monitorexit
        //     118: aload 4
        //     120: athrow
        //     121: new 2866	java/lang/IllegalArgumentException
        //     124: dup
        //     125: new 662	java/lang/StringBuilder
        //     128: dup
        //     129: invokespecial 663	java/lang/StringBuilder:<init>	()V
        //     132: ldc_w 3927
        //     135: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     138: aload_1
        //     139: invokevirtual 669	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     142: invokevirtual 672	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     145: invokespecial 2869	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     148: athrow
        //     149: astore 8
        //     151: lload 6
        //     153: invokestatic 2905	android/os/Binder:restoreCallingIdentity	(J)V
        //     156: goto -55 -> 101
        //
        // Exception table:
        //     from	to	target	type
        //     85	96	104	finally
        //     28	85	114	finally
        //     96	118	114	finally
        //     151	156	114	finally
        //     85	96	149	android/os/RemoteException
    }

    // ERROR //
    void startCleaningPackages()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 380	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
        //     4: astore_1
        //     5: aload_1
        //     6: monitorenter
        //     7: aload_0
        //     8: invokespecial 3662	com/android/server/pm/PackageManagerService:isExternalMediaAvailable	()Z
        //     11: ifne +8 -> 19
        //     14: aload_1
        //     15: monitorexit
        //     16: goto +73 -> 89
        //     19: aload_0
        //     20: getfield 500	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
        //     23: getfield 2645	com/android/server/pm/Settings:mPackagesToBeCleaned	Ljava/util/ArrayList;
        //     26: invokevirtual 848	java/util/ArrayList:size	()I
        //     29: ifgt +13 -> 42
        //     32: aload_1
        //     33: monitorexit
        //     34: goto +55 -> 89
        //     37: astore_2
        //     38: aload_1
        //     39: monitorexit
        //     40: aload_2
        //     41: athrow
        //     42: aload_1
        //     43: monitorexit
        //     44: new 1288	android/content/Intent
        //     47: dup
        //     48: ldc_w 3929
        //     51: invokespecial 1737	android/content/Intent:<init>	(Ljava/lang/String;)V
        //     54: astore_3
        //     55: aload_3
        //     56: getstatic 322	com/android/server/pm/PackageManagerService:DEFAULT_CONTAINER_COMPONENT	Landroid/content/ComponentName;
        //     59: invokevirtual 1293	android/content/Intent:setComponent	(Landroid/content/ComponentName;)Landroid/content/Intent;
        //     62: pop
        //     63: invokestatic 2043	android/app/ActivityManagerNative:getDefault	()Landroid/app/IActivityManager;
        //     66: astore 5
        //     68: aload 5
        //     70: ifnull +19 -> 89
        //     73: aload 5
        //     75: aconst_null
        //     76: aload_3
        //     77: aconst_null
        //     78: invokeinterface 3933 4 0
        //     83: pop
        //     84: goto +5 -> 89
        //     87: astore 6
        //     89: return
        //
        // Exception table:
        //     from	to	target	type
        //     7	40	37	finally
        //     42	44	37	finally
        //     73	84	87	android/os/RemoteException
    }

    public void systemReady()
    {
        int i = 1;
        this.mSystemReady = i;
        if (Settings.System.getInt(this.mContext.getContentResolver(), "compatibility_mode", i) == i);
        while (true)
        {
            PackageParser.setCompatibilityModeEnabled(i);
            return;
            int j = 0;
        }
    }

    public void updateExternalMediaStatus(final boolean paramBoolean1, final boolean paramBoolean2)
    {
        int i = Binder.getCallingUid();
        if ((i != 0) && (i != 1000))
            throw new SecurityException("Media status can only be updated by the system");
        while (true)
        {
            synchronized (this.mPackages)
            {
                StringBuilder localStringBuilder1 = new StringBuilder().append("Updating external media status from ");
                if (!this.mMediaMounted)
                    break label190;
                str1 = "mounted";
                StringBuilder localStringBuilder2 = localStringBuilder1.append(str1).append(" to ");
                if (!paramBoolean1)
                    break label198;
                str2 = "mounted";
                Log.i("PackageManager", str2);
                if (paramBoolean1 == this.mMediaMounted)
                {
                    PackageHandler localPackageHandler = this.mHandler;
                    if (!paramBoolean2)
                        break label206;
                    j = 1;
                    Message localMessage = localPackageHandler.obtainMessage(12, j, -1);
                    this.mHandler.sendMessage(localMessage);
                }
                else
                {
                    this.mMediaMounted = paramBoolean1;
                    this.mHandler.post(new Runnable()
                    {
                        public void run()
                        {
                            PackageManagerService.this.updateExternalMediaStatusInner(paramBoolean1, paramBoolean2, true);
                        }
                    });
                }
            }
            return;
            label190: String str1 = "unmounted";
            continue;
            label198: String str2 = "unmounted";
            continue;
            label206: int j = 0;
        }
    }

    public void updateUserName(int paramInt, String paramString)
    {
        enforceSystemOrRoot("Only the system can rename users");
        sUserManager.updateUserName(paramInt, paramString);
    }

    public void verifyPendingInstall(int paramInt1, int paramInt2)
        throws RemoteException
    {
        Message localMessage = this.mHandler.obtainMessage(15);
        PackageVerificationResponse localPackageVerificationResponse = new PackageVerificationResponse(paramInt2, Binder.getCallingUid());
        localMessage.arg1 = paramInt1;
        localMessage.obj = localPackageVerificationResponse;
        this.mHandler.sendMessage(localMessage);
    }

    static class DumpState
    {
        public static final int DUMP_FEATURES = 2;
        public static final int DUMP_LIBS = 1;
        public static final int DUMP_MESSAGES = 64;
        public static final int DUMP_PACKAGES = 16;
        public static final int DUMP_PERMISSIONS = 8;
        public static final int DUMP_PREFERRED = 512;
        public static final int DUMP_PREFERRED_XML = 1024;
        public static final int DUMP_PROVIDERS = 128;
        public static final int DUMP_RESOLVERS = 4;
        public static final int DUMP_SHARED_USERS = 32;
        public static final int DUMP_VERIFIERS = 256;
        public static final int OPTION_SHOW_FILTERS = 1;
        private int mOptions;
        private SharedUserSetting mSharedUser;
        private boolean mTitlePrinted;
        private int mTypes;

        public SharedUserSetting getSharedUser()
        {
            return this.mSharedUser;
        }

        public boolean getTitlePrinted()
        {
            return this.mTitlePrinted;
        }

        public boolean isDumping(int paramInt)
        {
            boolean bool = true;
            if ((this.mTypes == 0) && (paramInt != 1024));
            while (true)
            {
                return bool;
                if ((paramInt & this.mTypes) == 0)
                    bool = false;
            }
        }

        public boolean isOptionEnabled(int paramInt)
        {
            if ((paramInt & this.mOptions) != 0);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public boolean onTitlePrinted()
        {
            boolean bool = this.mTitlePrinted;
            this.mTitlePrinted = true;
            return bool;
        }

        public void setDump(int paramInt)
        {
            this.mTypes = (paramInt | this.mTypes);
        }

        public void setOptionEnabled(int paramInt)
        {
            this.mOptions = (paramInt | this.mOptions);
        }

        public void setSharedUser(SharedUserSetting paramSharedUserSetting)
        {
            this.mSharedUser = paramSharedUserSetting;
        }

        public void setTitlePrinted(boolean paramBoolean)
        {
            this.mTitlePrinted = paramBoolean;
        }
    }

    private final class ClearStorageConnection
        implements ServiceConnection
    {
        IMediaContainerService mContainerService;

        private ClearStorageConnection()
        {
        }

        public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
        {
            try
            {
                this.mContainerService = IMediaContainerService.Stub.asInterface(paramIBinder);
                notifyAll();
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public void onServiceDisconnected(ComponentName paramComponentName)
        {
        }
    }

    static class PackageRemovedInfo
    {
        PackageManagerService.InstallArgs args = null;
        boolean isRemovedPackageSystemUpdate = false;
        String removedPackage;
        int removedUid = -1;
        int uid = -1;

        void sendBroadcast(boolean paramBoolean1, boolean paramBoolean2)
        {
            Bundle localBundle = new Bundle(1);
            if (this.removedUid >= 0);
            for (int i = this.removedUid; ; i = this.uid)
            {
                localBundle.putInt("android.intent.extra.UID", i);
                localBundle.putBoolean("android.intent.extra.DATA_REMOVED", paramBoolean1);
                if (paramBoolean2)
                    localBundle.putBoolean("android.intent.extra.REPLACING", true);
                if (this.removedPackage != null)
                {
                    PackageManagerService.sendPackageBroadcast("android.intent.action.PACKAGE_REMOVED", this.removedPackage, localBundle, null, null, -1);
                    if ((paramBoolean1) && (!paramBoolean2))
                        PackageManagerService.sendPackageBroadcast("android.intent.action.PACKAGE_FULLY_REMOVED", this.removedPackage, localBundle, null, null, -1);
                }
                if (this.removedUid >= 0)
                    PackageManagerService.sendPackageBroadcast("android.intent.action.UID_REMOVED", null, localBundle, null, null, UserId.getUserId(this.removedUid));
                return;
            }
        }
    }

    class PackageInstalledInfo
    {
        String name;
        PackageParser.Package pkg;
        PackageManagerService.PackageRemovedInfo removedInfo;
        int returnCode;
        int uid;

        PackageInstalledInfo()
        {
        }
    }

    class AsecInstallArgs extends PackageManagerService.InstallArgs
    {
        static final String PUBLIC_RES_FILE_NAME = "res.zip";
        static final String RES_FILE_NAME = "pkg.apk";
        String cid;
        String libraryPath;
        String packagePath;
        String resourcePath;

        AsecInstallArgs(Uri paramString, String paramBoolean1, boolean paramBoolean2, boolean arg5)
        {
        }

        AsecInstallArgs(PackageManagerService.InstallParams arg2)
        {
            super(localObject.observer, localObject.flags, localObject.installerPackageName, localObject.manifestDigest);
        }

        AsecInstallArgs(String paramString1, String paramString2, String paramBoolean1, boolean paramBoolean2, boolean arg6)
        {
        }

        AsecInstallArgs(String paramBoolean, boolean arg3)
        {
        }

        private void cleanUp()
        {
            PackageHelper.destroySdDir(this.cid);
        }

        private final boolean isExternal()
        {
            if ((0x8 & this.flags) != 0);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        private void setCachePath(String paramString)
        {
            File localFile = new File(paramString);
            this.libraryPath = new File(localFile, "lib").getPath();
            this.packagePath = new File(localFile, "pkg.apk").getPath();
            if (isFwdLocked());
            for (this.resourcePath = new File(localFile, "res.zip").getPath(); ; this.resourcePath = this.packagePath)
                return;
        }

        boolean checkFreeStorage(IMediaContainerService paramIMediaContainerService)
            throws RemoteException
        {
            try
            {
                PackageManagerService.this.mContext.grantUriPermission("com.android.defcontainer", this.packageURI, 1);
                boolean bool = paramIMediaContainerService.checkExternalFreeStorage(this.packageURI, isFwdLocked());
                return bool;
            }
            finally
            {
                PackageManagerService.this.mContext.revokeUriPermission(this.packageURI, 1);
            }
        }

        void cleanUpResourcesLI()
        {
            String str = getCodePath();
            int i = PackageManagerService.this.mInstaller.rmdex(str);
            if (i < 0)
                Slog.w("PackageManager", "Couldn't remove dex file for package:    at location " + str.toString() + ", retcode=" + i);
            cleanUp();
        }

        int copyApk(IMediaContainerService paramIMediaContainerService, boolean paramBoolean)
            throws RemoteException
        {
            if (paramBoolean)
                createCopyFile();
            while (true)
            {
                int i;
                try
                {
                    PackageManagerService.this.mContext.grantUriPermission("com.android.defcontainer", this.packageURI, 1);
                    String str = paramIMediaContainerService.copyResourceToContainer(this.packageURI, this.cid, PackageManagerService.this.getEncryptKey(), "pkg.apk", "res.zip", isExternal(), isFwdLocked());
                    PackageManagerService.this.mContext.revokeUriPermission(this.packageURI, 1);
                    if (str != null)
                    {
                        setCachePath(str);
                        return i;
                        PackageHelper.destroySdDir(this.cid);
                    }
                }
                finally
                {
                    PackageManagerService.this.mContext.revokeUriPermission(this.packageURI, 1);
                }
            }
        }

        void createCopyFile()
        {
            this.cid = PackageManagerService.getTempContainerId();
        }

        int doPostCopy(int paramInt)
        {
            if ((isFwdLocked()) && ((paramInt < 10000) || (!PackageHelper.fixSdPermissions(this.cid, paramInt, "pkg.apk"))))
            {
                Slog.e("PackageManager", "Failed to finalize " + this.cid);
                PackageHelper.destroySdDir(this.cid);
            }
            for (int i = -18; ; i = 1)
                return i;
        }

        boolean doPostDeleteLI(boolean paramBoolean)
        {
            boolean bool = false;
            if (PackageHelper.isContainerMounted(this.cid))
                bool = PackageHelper.unMountSdDir(this.cid);
            if ((bool) && (paramBoolean))
                cleanUpResourcesLI();
            return bool;
        }

        int doPostInstall(int paramInt1, int paramInt2)
        {
            if (paramInt1 != 1)
                cleanUp();
            while (true)
            {
                return paramInt1;
                int i;
                if (isFwdLocked())
                    i = paramInt2;
                for (String str = "pkg.apk"; ; str = null)
                {
                    if ((paramInt2 >= 10000) && (PackageHelper.fixSdPermissions(this.cid, i, str)))
                        break label95;
                    Slog.e("PackageManager", "Failed to finalize " + this.cid);
                    PackageHelper.destroySdDir(this.cid);
                    paramInt1 = -18;
                    break;
                    i = -1;
                }
                label95: if (!PackageHelper.isContainerMounted(this.cid))
                    PackageHelper.mountSdDir(this.cid, PackageManagerService.this.getEncryptKey(), Process.myUid());
            }
        }

        int doPreCopy()
        {
            if ((isFwdLocked()) && (!PackageHelper.fixSdPermissions(this.cid, PackageManagerService.this.getPackageUid("com.android.defcontainer", 0), "pkg.apk")));
            for (int i = -18; ; i = 1)
                return i;
        }

        int doPreInstall(int paramInt)
        {
            if (paramInt != 1)
                PackageHelper.destroySdDir(this.cid);
            while (true)
            {
                return paramInt;
                if (!PackageHelper.isContainerMounted(this.cid))
                {
                    String str = PackageHelper.mountSdDir(this.cid, PackageManagerService.this.getEncryptKey(), 1000);
                    if (str != null)
                        setCachePath(str);
                    else
                        paramInt = -18;
                }
            }
        }

        boolean doRename(int paramInt, String paramString1, String paramString2)
        {
            boolean bool = false;
            String str1 = PackageManagerService.getNextCodePath(paramString2, paramString1, "/pkg.apk");
            if ((PackageHelper.isContainerMounted(this.cid)) && (!PackageHelper.unMountSdDir(this.cid)))
                Slog.i("PackageManager", "Failed to unmount " + this.cid + " before renaming");
            while (true)
            {
                return bool;
                if (!PackageHelper.renameSdDir(this.cid, str1))
                {
                    Slog.e("PackageManager", "Failed to rename " + this.cid + " to " + str1 + " which might be stale. Will try to clean up.");
                    if (!PackageHelper.destroySdDir(str1))
                        Slog.e("PackageManager", "Very strange. Cannot clean up stale container " + str1);
                    else if (!PackageHelper.renameSdDir(this.cid, str1))
                        Slog.e("PackageManager", "Failed to rename " + this.cid + " to " + str1 + " inspite of cleaning it up.");
                }
                else
                {
                    if (!PackageHelper.isContainerMounted(str1))
                        Slog.w("PackageManager", "Mounting container " + str1);
                    for (String str2 = PackageHelper.mountSdDir(str1, PackageManagerService.this.getEncryptKey(), 1000); ; str2 = PackageHelper.getSdDir(str1))
                    {
                        if (str2 != null)
                            break label322;
                        Slog.w("PackageManager", "Failed to get cache path for    " + str1);
                        break;
                    }
                    label322: Log.i("PackageManager", "Succesfully renamed " + this.cid + " to " + str1 + " at new path: " + str2);
                    this.cid = str1;
                    setCachePath(str2);
                    bool = true;
                }
            }
        }

        String getCodePath()
        {
            return this.packagePath;
        }

        String getNativeLibraryPath()
        {
            return this.libraryPath;
        }

        String getPackageName()
        {
            return PackageManagerService.getAsecPackageName(this.cid);
        }

        String getResourcePath()
        {
            return this.resourcePath;
        }

        boolean matchContainer(String paramString)
        {
            if (this.cid.startsWith(paramString));
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }

    class FileInstallArgs extends PackageManagerService.InstallArgs
    {
        String codeFileName;
        boolean created = false;
        File installDir;
        String libraryPath;
        String resourceFileName;

        FileInstallArgs(Uri paramString1, String paramString2, String arg4)
        {
            super(null, 0, null, null);
            if (isFwdLocked());
            for (File localFile = PackageManagerService.this.mDrmAppPrivateInstallDir; ; localFile = PackageManagerService.this.mAppInstallDir)
            {
                this.installDir = localFile;
                String str2 = PackageManagerService.getNextCodePath(null, paramString2, ".apk");
                this.codeFileName = new File(this.installDir, str2 + ".apk").getPath();
                this.resourceFileName = getResourcePathFromCodePath();
                String str1;
                this.libraryPath = new File(str1, "lib").getPath();
                return;
            }
        }

        FileInstallArgs(PackageManagerService.InstallParams arg2)
        {
            super(localObject.observer, localObject.flags, localObject.installerPackageName, localObject.manifestDigest);
        }

        FileInstallArgs(String paramString1, String paramString2, String arg4)
        {
            super(null, 0, null, null);
            this.installDir = new File(paramString1).getParentFile();
            this.codeFileName = paramString1;
            this.resourceFileName = paramString2;
            Object localObject;
            this.libraryPath = localObject;
        }

        private boolean cleanUp()
        {
            boolean bool = true;
            String str1 = getCodePath();
            String str2 = getResourcePath();
            if (str1 != null)
            {
                File localFile1 = new File(str1);
                if (!localFile1.exists())
                {
                    Slog.w("PackageManager", "Package source " + str1 + " does not exist.");
                    bool = false;
                }
                localFile1.delete();
            }
            if ((str2 != null) && (!str2.equals(str1)))
            {
                File localFile2 = new File(str2);
                if (!localFile2.exists())
                    Slog.w("PackageManager", "Package public source " + localFile2 + " does not exist.");
                if (localFile2.exists())
                    localFile2.delete();
            }
            return bool;
        }

        private String getResourcePathFromCodePath()
        {
            String str = getCodePath();
            if (isFwdLocked())
            {
                StringBuilder localStringBuilder = new StringBuilder();
                localStringBuilder.append(PackageManagerService.this.mAppInstallDir.getPath());
                localStringBuilder.append('/');
                localStringBuilder.append(PackageManagerService.getApkName(str));
                localStringBuilder.append(".zip");
                if (str.endsWith(".tmp"))
                    localStringBuilder.append(".tmp");
                str = localStringBuilder.toString();
            }
            return str;
        }

        private boolean setPermissions()
        {
            boolean bool = true;
            if (!isFwdLocked())
            {
                int i = FileUtils.setPermissions(getCodePath(), 420, -1, -1);
                if (i != 0)
                {
                    Slog.e("PackageManager", "Couldn't set new package file permissions for " + getCodePath() + ". The return code was: " + i);
                    bool = false;
                }
            }
            return bool;
        }

        boolean checkFreeStorage(IMediaContainerService paramIMediaContainerService)
            throws RemoteException
        {
            DeviceStorageMonitorService localDeviceStorageMonitorService = (DeviceStorageMonitorService)ServiceManager.getService("devicestoragemonitor");
            long l;
            if (localDeviceStorageMonitorService == null)
            {
                Log.w("PackageManager", "Couldn't get low memory threshold; no free limit imposed");
                l = 0L;
            }
            try
            {
                PackageManagerService.this.mContext.grantUriPermission("com.android.defcontainer", this.packageURI, 1);
                boolean bool1 = paramIMediaContainerService.checkInternalFreeStorage(this.packageURI, isFwdLocked(), l);
                for (boolean bool2 = bool1; ; bool2 = false)
                {
                    return bool2;
                    if (!localDeviceStorageMonitorService.isMemoryLow())
                        break;
                    Log.w("PackageManager", "Memory is reported as being too low; aborting package install");
                }
                l = localDeviceStorageMonitorService.getMemoryLowThreshold();
            }
            finally
            {
                PackageManagerService.this.mContext.revokeUriPermission(this.packageURI, 1);
            }
        }

        void cleanUpResourcesLI()
        {
            String str = getCodePath();
            if (cleanUp())
            {
                int i = PackageManagerService.this.mInstaller.rmdex(str);
                if (i < 0)
                    Slog.w("PackageManager", "Couldn't remove dex file for package:    at location " + str + ", retcode=" + i);
            }
        }

        // ERROR //
        int copyApk(IMediaContainerService paramIMediaContainerService, boolean paramBoolean)
            throws RemoteException
        {
            // Byte code:
            //     0: iload_2
            //     1: ifeq +7 -> 8
            //     4: aload_0
            //     5: invokevirtual 256	com/android/server/pm/PackageManagerService$FileInstallArgs:createCopyFile	()V
            //     8: new 43	java/io/File
            //     11: dup
            //     12: aload_0
            //     13: getfield 64	com/android/server/pm/PackageManagerService$FileInstallArgs:codeFileName	Ljava/lang/String;
            //     16: invokespecial 105	java/io/File:<init>	(Ljava/lang/String;)V
            //     19: astore_3
            //     20: aload_0
            //     21: getfield 26	com/android/server/pm/PackageManagerService$FileInstallArgs:created	Z
            //     24: ifne +61 -> 85
            //     27: aload_3
            //     28: invokevirtual 259	java/io/File:createNewFile	()Z
            //     31: pop
            //     32: aload_0
            //     33: invokespecial 261	com/android/server/pm/PackageManagerService$FileInstallArgs:setPermissions	()Z
            //     36: istore 18
            //     38: iload 18
            //     40: ifne +45 -> 85
            //     43: bipush 252
            //     45: istore 6
            //     47: iload 6
            //     49: ireturn
            //     50: astore 15
            //     52: ldc 121
            //     54: new 45	java/lang/StringBuilder
            //     57: dup
            //     58: invokespecial 48	java/lang/StringBuilder:<init>	()V
            //     61: ldc_w 263
            //     64: invokevirtual 52	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     67: aload_3
            //     68: invokevirtual 145	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     71: invokevirtual 56	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     74: invokestatic 131	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     77: pop
            //     78: bipush 252
            //     80: istore 6
            //     82: goto -35 -> 47
            //     85: aload_3
            //     86: ldc_w 264
            //     89: invokestatic 270	android/os/ParcelFileDescriptor:open	(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
            //     92: astore 7
            //     94: aload_0
            //     95: getfield 21	com/android/server/pm/PackageManagerService$FileInstallArgs:this$0	Lcom/android/server/pm/PackageManagerService;
            //     98: getfield 199	com/android/server/pm/PackageManagerService:mContext	Landroid/content/Context;
            //     101: ldc 201
            //     103: aload_0
            //     104: getfield 205	com/android/server/pm/PackageManagerService$InstallArgs:packageURI	Landroid/net/Uri;
            //     107: iconst_1
            //     108: invokevirtual 211	android/content/Context:grantUriPermission	(Ljava/lang/String;Landroid/net/Uri;I)V
            //     111: aload_1
            //     112: aload_0
            //     113: getfield 205	com/android/server/pm/PackageManagerService$InstallArgs:packageURI	Landroid/net/Uri;
            //     116: aconst_null
            //     117: aload 7
            //     119: invokeinterface 274 4 0
            //     124: istore 9
            //     126: iload 9
            //     128: istore 6
            //     130: aload 7
            //     132: invokestatic 280	libcore/io/IoUtils:closeQuietly	(Ljava/lang/AutoCloseable;)V
            //     135: aload_0
            //     136: getfield 21	com/android/server/pm/PackageManagerService$FileInstallArgs:this$0	Lcom/android/server/pm/PackageManagerService;
            //     139: getfield 199	com/android/server/pm/PackageManagerService:mContext	Landroid/content/Context;
            //     142: aload_0
            //     143: getfield 205	com/android/server/pm/PackageManagerService$InstallArgs:packageURI	Landroid/net/Uri;
            //     146: iconst_1
            //     147: invokevirtual 221	android/content/Context:revokeUriPermission	(Landroid/net/Uri;I)V
            //     150: aload_0
            //     151: invokevirtual 30	com/android/server/pm/PackageManagerService$FileInstallArgs:isFwdLocked	()Z
            //     154: ifeq -107 -> 47
            //     157: new 43	java/io/File
            //     160: dup
            //     161: aload_0
            //     162: invokevirtual 116	com/android/server/pm/PackageManagerService$FileInstallArgs:getResourcePath	()Ljava/lang/String;
            //     165: invokespecial 105	java/io/File:<init>	(Ljava/lang/String;)V
            //     168: astore 10
            //     170: aload_0
            //     171: getfield 64	com/android/server/pm/PackageManagerService$FileInstallArgs:codeFileName	Ljava/lang/String;
            //     174: aload 10
            //     176: invokestatic 286	com/android/internal/content/PackageHelper:extractPublicFiles	(Ljava/lang/String;Ljava/io/File;)I
            //     179: pop
            //     180: goto -133 -> 47
            //     183: astore 11
            //     185: ldc 121
            //     187: ldc_w 288
            //     190: invokestatic 176	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     193: pop
            //     194: aload 10
            //     196: invokevirtual 134	java/io/File:delete	()Z
            //     199: pop
            //     200: bipush 252
            //     202: istore 6
            //     204: goto -157 -> 47
            //     207: astore 4
            //     209: ldc 121
            //     211: new 45	java/lang/StringBuilder
            //     214: dup
            //     215: invokespecial 48	java/lang/StringBuilder:<init>	()V
            //     218: ldc_w 290
            //     221: invokevirtual 52	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     224: aload_0
            //     225: getfield 64	com/android/server/pm/PackageManagerService$FileInstallArgs:codeFileName	Ljava/lang/String;
            //     228: invokevirtual 52	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     231: invokevirtual 56	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     234: invokestatic 176	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     237: pop
            //     238: bipush 252
            //     240: istore 6
            //     242: goto -195 -> 47
            //     245: astore 8
            //     247: aload 7
            //     249: invokestatic 280	libcore/io/IoUtils:closeQuietly	(Ljava/lang/AutoCloseable;)V
            //     252: aload_0
            //     253: getfield 21	com/android/server/pm/PackageManagerService$FileInstallArgs:this$0	Lcom/android/server/pm/PackageManagerService;
            //     256: getfield 199	com/android/server/pm/PackageManagerService:mContext	Landroid/content/Context;
            //     259: aload_0
            //     260: getfield 205	com/android/server/pm/PackageManagerService$InstallArgs:packageURI	Landroid/net/Uri;
            //     263: iconst_1
            //     264: invokevirtual 221	android/content/Context:revokeUriPermission	(Landroid/net/Uri;I)V
            //     267: aload 8
            //     269: athrow
            //
            // Exception table:
            //     from	to	target	type
            //     27	38	50	java/io/IOException
            //     170	180	183	java/io/IOException
            //     85	94	207	java/io/FileNotFoundException
            //     94	126	245	finally
        }

        void createCopyFile()
        {
            if (isFwdLocked());
            for (File localFile = PackageManagerService.this.mDrmAppPrivateInstallDir; ; localFile = PackageManagerService.this.mAppInstallDir)
            {
                this.installDir = localFile;
                this.codeFileName = PackageManagerService.this.createTempPackageFile(this.installDir).getPath();
                this.resourceFileName = getResourcePathFromCodePath();
                this.created = true;
                return;
            }
        }

        boolean doPostDeleteLI(boolean paramBoolean)
        {
            cleanUpResourcesLI();
            return true;
        }

        int doPostInstall(int paramInt1, int paramInt2)
        {
            if (paramInt1 != 1)
                cleanUp();
            return paramInt1;
        }

        int doPreInstall(int paramInt)
        {
            if (paramInt != 1)
                cleanUp();
            return paramInt;
        }

        boolean doRename(int paramInt, String paramString1, String paramString2)
        {
            boolean bool = false;
            if (paramInt != 1)
                cleanUp();
            while (true)
            {
                return bool;
                File localFile1 = new File(getCodePath());
                File localFile2 = new File(getResourcePath());
                String str = PackageManagerService.getNextCodePath(paramString2, paramString1, ".apk");
                File localFile3 = new File(this.installDir, str + ".apk");
                if (localFile1.renameTo(localFile3))
                {
                    this.codeFileName = localFile3.getPath();
                    File localFile4 = new File(getResourcePathFromCodePath());
                    if ((!isFwdLocked()) || (localFile2.renameTo(localFile4)))
                    {
                        this.resourceFileName = getResourcePathFromCodePath();
                        if (setPermissions())
                            bool = true;
                    }
                }
            }
        }

        String getCodePath()
        {
            return this.codeFileName;
        }

        String getNativeLibraryPath()
        {
            return this.libraryPath;
        }

        String getResourcePath()
        {
            return this.resourceFileName;
        }
    }

    static abstract class InstallArgs
    {
        final int flags;
        final String installerPackageName;
        final ManifestDigest manifestDigest;
        final IPackageInstallObserver observer;
        final Uri packageURI;

        InstallArgs(Uri paramUri, IPackageInstallObserver paramIPackageInstallObserver, int paramInt, String paramString, ManifestDigest paramManifestDigest)
        {
            this.packageURI = paramUri;
            this.flags = paramInt;
            this.observer = paramIPackageInstallObserver;
            this.installerPackageName = paramString;
            this.manifestDigest = paramManifestDigest;
        }

        abstract boolean checkFreeStorage(IMediaContainerService paramIMediaContainerService)
            throws RemoteException;

        abstract void cleanUpResourcesLI();

        abstract int copyApk(IMediaContainerService paramIMediaContainerService, boolean paramBoolean)
            throws RemoteException;

        abstract void createCopyFile();

        int doPostCopy(int paramInt)
        {
            return 1;
        }

        abstract boolean doPostDeleteLI(boolean paramBoolean);

        abstract int doPostInstall(int paramInt1, int paramInt2);

        int doPreCopy()
        {
            return 1;
        }

        abstract int doPreInstall(int paramInt);

        abstract boolean doRename(int paramInt, String paramString1, String paramString2);

        abstract String getCodePath();

        abstract String getNativeLibraryPath();

        abstract String getResourcePath();

        protected boolean isFwdLocked()
        {
            if ((0x1 & this.flags) != 0);
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }

    class MoveParams extends PackageManagerService.HandlerParams
    {
        final int flags;
        int mRet;
        final IPackageMoveObserver observer;
        final String packageName;
        final PackageManagerService.InstallArgs srcArgs;
        final PackageManagerService.InstallArgs targetArgs;
        int uid;

        MoveParams(PackageManagerService.InstallArgs paramIPackageMoveObserver, IPackageMoveObserver paramInt1, int paramString1, String paramString2, String paramInt2, int arg7)
        {
            super(null);
            this.srcArgs = paramIPackageMoveObserver;
            this.observer = paramInt1;
            this.flags = paramString1;
            this.packageName = paramString2;
            int i;
            this.uid = i;
            if (paramIPackageMoveObserver != null);
            for (this.targetArgs = PackageManagerService.this.createInstallArgs(Uri.fromFile(new File(paramIPackageMoveObserver.getCodePath())), paramString1, paramString2, paramInt2); ; this.targetArgs = null)
                return;
        }

        void handleReturnCode()
        {
            this.targetArgs.doPostInstall(this.mRet, this.uid);
            int i = -6;
            if (this.mRet == 1)
                i = 1;
            while (true)
            {
                PackageManagerService.this.processPendingMove(this, i);
                return;
                if (this.mRet == -4)
                    i = -1;
            }
        }

        void handleServiceError()
        {
            this.mRet = -110;
        }

        public void handleStartCopy()
            throws RemoteException
        {
            this.mRet = -4;
            if (!this.targetArgs.checkFreeStorage(PackageManagerService.this.mContainerService))
                Log.w("PackageManager", "Insufficient storage to install");
            while (true)
            {
                return;
                this.mRet = this.srcArgs.doPreCopy();
                if (this.mRet == 1)
                {
                    this.mRet = this.targetArgs.copyApk(PackageManagerService.this.mContainerService, false);
                    if (this.mRet != 1)
                    {
                        this.srcArgs.doPostCopy(this.uid);
                    }
                    else
                    {
                        this.mRet = this.srcArgs.doPostCopy(this.uid);
                        if (this.mRet == 1)
                        {
                            this.mRet = this.targetArgs.doPreInstall(this.mRet);
                            if (this.mRet == 1);
                        }
                    }
                }
            }
        }
    }

    class InstallParams extends PackageManagerService.HandlerParams
    {
        final ContainerEncryptionParams encryptionParams;
        int flags;
        final String installerPackageName;
        private PackageManagerService.InstallArgs mArgs;
        private final Uri mPackageURI;
        private int mRet;
        private File mTempPackage;
        final ManifestDigest manifestDigest;
        final IPackageInstallObserver observer;
        final Uri verificationURI;

        InstallParams(Uri paramIPackageInstallObserver, IPackageInstallObserver paramInt, int paramString, String paramUri1, Uri paramManifestDigest, ManifestDigest paramContainerEncryptionParams, ContainerEncryptionParams arg8)
        {
            super(null);
            this.mPackageURI = paramIPackageInstallObserver;
            this.flags = paramString;
            this.observer = paramInt;
            this.installerPackageName = paramUri1;
            this.verificationURI = paramManifestDigest;
            this.manifestDigest = paramContainerEncryptionParams;
            Object localObject;
            this.encryptionParams = localObject;
        }

        private int installLocationPolicy(PackageInfoLite paramPackageInfoLite, int paramInt)
        {
            int i = 1;
            String str = paramPackageInfoLite.packageName;
            int j = paramPackageInfoLite.installLocation;
            int k;
            if ((paramInt & 0x8) != 0)
                k = i;
            while (true)
            {
                PackageParser.Package localPackage;
                synchronized (PackageManagerService.this.mPackages)
                {
                    localPackage = (PackageParser.Package)PackageManagerService.this.mPackages.get(str);
                    if (localPackage != null)
                    {
                        if ((paramInt & 0x2) == 0)
                            continue;
                        if ((0x1 & localPackage.applicationInfo.flags) != 0)
                            if (k != 0)
                            {
                                Slog.w("PackageManager", "Cannot install update to system app on sdcard");
                                i = -3;
                                continue;
                            }
                    }
                }
                k = 0;
                continue;
                if (j != 2);
            }
        }

        public Uri getPackageUri()
        {
            if (this.mTempPackage != null);
            for (Uri localUri = Uri.fromFile(this.mTempPackage); ; localUri = this.mPackageURI)
                return localUri;
        }

        void handleReturnCode()
        {
            if (this.mArgs != null)
                PackageManagerService.this.processPendingInstall(this.mArgs, this.mRet);
            if ((this.mTempPackage != null) && (!this.mTempPackage.delete()))
                Slog.w("PackageManager", "Couldn't delete temporary file: " + this.mTempPackage.getAbsolutePath());
        }

        void handleServiceError()
        {
            this.mArgs = PackageManagerService.this.createInstallArgs(this);
            this.mRet = -110;
        }

        public void handleStartCopy()
            throws RemoteException
        {
            int i = 1;
            int j;
            int k;
            label26: Object localObject1;
            int i3;
            label70: PackageManagerService.InstallArgs localInstallArgs;
            int m;
            label105: Intent localIntent1;
            List localList2;
            int i1;
            if ((0x8 & this.flags) != 0)
            {
                j = 1;
                if ((0x10 & this.flags) == 0)
                    break label387;
                k = 1;
                localObject1 = null;
                if ((k == 0) || (j == 0))
                    break label392;
                Slog.w("PackageManager", "Conflicting flags specified for installing on both internal and external");
                i = -19;
                if (i == 1)
                {
                    i3 = ((PackageInfoLite)localObject1).recommendedInstallLocation;
                    if (i3 != -3)
                        break label699;
                    i = -19;
                }
                localInstallArgs = PackageManagerService.this.createInstallArgs(this);
                this.mArgs = localInstallArgs;
                if (i == 1)
                {
                    if (PackageManagerService.this.mRequiredVerifierPackage != null)
                        break label844;
                    m = -1;
                    if ((m == -1) || (!PackageManagerService.this.isVerificationEnabled()))
                        break label925;
                    localIntent1 = new Intent("android.intent.action.PACKAGE_NEEDS_VERIFICATION");
                    localIntent1.setDataAndType(getPackageUri(), "application/vnd.android.package-archive");
                    localIntent1.addFlags(1);
                    List localList1 = PackageManagerService.this.queryIntentReceivers(localIntent1, null, 512, 0);
                    final int n = PackageManagerService.access$2408(PackageManagerService.this);
                    localIntent1.putExtra("android.content.pm.extra.VERIFICATION_ID", n);
                    localIntent1.putExtra("android.content.pm.extra.VERIFICATION_INSTALLER_PACKAGE", this.installerPackageName);
                    localIntent1.putExtra("android.content.pm.extra.VERIFICATION_INSTALL_FLAGS", this.flags);
                    if (this.verificationURI != null)
                        localIntent1.putExtra("android.content.pm.extra.VERIFICATION_URI", this.verificationURI);
                    PackageVerificationState localPackageVerificationState = new PackageVerificationState(m, localInstallArgs);
                    PackageManagerService.this.mPendingVerification.append(n, localPackageVerificationState);
                    localList2 = PackageManagerService.this.matchVerifiers((PackageInfoLite)localObject1, localList1, localPackageVerificationState);
                    if (localList2 != null)
                    {
                        i1 = localList2.size();
                        if (i1 != 0)
                            break label864;
                        Slog.i("PackageManager", "Additional verifiers required, but none installed.");
                        i = -22;
                    }
                    label302: ComponentName localComponentName1 = PackageManagerService.this.matchComponentForVerifier(PackageManagerService.this.mRequiredVerifierPackage, localList1);
                    if ((i == 1) && (PackageManagerService.this.mRequiredVerifierPackage != null))
                    {
                        localIntent1.setComponent(localComponentName1);
                        PackageManagerService.this.mContext.sendOrderedBroadcast(localIntent1, "android.permission.PACKAGE_VERIFICATION_AGENT", new BroadcastReceiver()
                        {
                            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
                            {
                                Message localMessage = PackageManagerService.this.mHandler.obtainMessage(16);
                                localMessage.arg1 = n;
                                PackageManagerService.this.mHandler.sendMessageDelayed(localMessage, PackageManagerService.this.getVerificationTimeout());
                            }
                        }
                        , null, 0, null, null);
                        this.mArgs = null;
                    }
                }
            }
            while (true)
            {
                this.mRet = i;
                return;
                j = 0;
                break;
                label387: k = 0;
                break label26;
                label392: DeviceStorageMonitorService localDeviceStorageMonitorService = (DeviceStorageMonitorService)ServiceManager.getService("devicestoragemonitor");
                long l;
                if (localDeviceStorageMonitorService == null)
                {
                    Log.w("PackageManager", "Couldn't get low memory threshold; no free limit imposed");
                    l = 0L;
                }
                while (true)
                {
                    try
                    {
                        while (true)
                        {
                            PackageManagerService.this.mContext.grantUriPermission("com.android.defcontainer", this.mPackageURI, 1);
                            if ((this.encryptionParams == null) && ("file".equals(this.mPackageURI.getScheme())))
                                break label676;
                            Object localObject3 = null;
                            this.mTempPackage = PackageManagerService.this.createTempPackageFile(PackageManagerService.this.mDrmAppPrivateInstallDir);
                            File localFile1 = this.mTempPackage;
                            if (localFile1 != null)
                                try
                                {
                                    ParcelFileDescriptor localParcelFileDescriptor = ParcelFileDescriptor.open(this.mTempPackage, 805306368);
                                    localObject3 = localParcelFileDescriptor;
                                    i = PackageManagerService.this.mContainerService.copyResource(this.mPackageURI, this.encryptionParams, localObject3);
                                    localFile2 = this.mTempPackage;
                                    FileUtils.setPermissions(localFile2.getAbsolutePath(), 388, -1, -1);
                                    if (localFile2 != null)
                                    {
                                        PackageInfoLite localPackageInfoLite = PackageManagerService.this.mContainerService.getMinimalPackageInfo(localFile2.getAbsolutePath(), this.flags, l);
                                        localObject1 = localPackageInfoLite;
                                    }
                                    PackageManagerService.this.mContext.revokeUriPermission(this.mPackageURI, 1);
                                    break;
                                    l = localDeviceStorageMonitorService.getMemoryLowThreshold();
                                }
                                catch (FileNotFoundException localFileNotFoundException)
                                {
                                    while (true)
                                        Slog.e("PackageManager", "Failed to create temporary file for : " + this.mPackageURI);
                                }
                        }
                    }
                    finally
                    {
                        PackageManagerService.this.mContext.revokeUriPermission(this.mPackageURI, 1);
                    }
                    File localFile2 = null;
                    continue;
                    label676: String str = this.mPackageURI.getPath();
                    localFile2 = new File(str);
                }
                label699: if (i3 == -4)
                {
                    i = -1;
                    break label70;
                }
                if (i3 == -1)
                {
                    i = -4;
                    break label70;
                }
                if (i3 == -2)
                {
                    i = -2;
                    break label70;
                }
                if (i3 == -6)
                {
                    i = -3;
                    break label70;
                }
                if (i3 == -5)
                {
                    i = -20;
                    break label70;
                }
                int i4 = this.flags;
                int i5 = installLocationPolicy((PackageInfoLite)localObject1, i4);
                if ((j != 0) || (k != 0))
                    break label70;
                if (i5 == 2)
                {
                    this.flags = (0x8 | this.flags);
                    this.flags = (0xFFFFFFEF & this.flags);
                    break label70;
                }
                this.flags = (0x10 | this.flags);
                this.flags = (0xFFFFFFF7 & this.flags);
                break label70;
                label844: m = PackageManagerService.this.getPackageUid(PackageManagerService.this.mRequiredVerifierPackage, 0);
                break label105;
                label864: for (int i2 = 0; i2 < i1; i2++)
                {
                    ComponentName localComponentName2 = (ComponentName)localList2.get(i2);
                    Intent localIntent2 = new Intent(localIntent1);
                    localIntent2.setComponent(localComponentName2);
                    PackageManagerService.this.mContext.sendBroadcast(localIntent2);
                }
                break label302;
                label925: i = localInstallArgs.copyApk(PackageManagerService.this.mContainerService, true);
            }
        }

        public boolean isForwardLocked()
        {
            if ((0x1 & this.flags) != 0);
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }

    class MeasureParams extends PackageManagerService.HandlerParams
    {
        private final IPackageStatsObserver mObserver;
        private final PackageStats mStats;
        private boolean mSuccess;

        public MeasureParams(PackageStats paramIPackageStatsObserver, IPackageStatsObserver arg3)
        {
            super(null);
            Object localObject;
            this.mObserver = localObject;
            this.mStats = paramIPackageStatsObserver;
        }

        void handleReturnCode()
        {
            if (this.mObserver != null);
            try
            {
                this.mObserver.onGetStatsCompleted(this.mStats, this.mSuccess);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Slog.i("PackageManager", "Observer no longer exists.");
            }
        }

        void handleServiceError()
        {
            Slog.e("PackageManager", "Could not measure application " + this.mStats.packageName + " external storage");
        }

        void handleStartCopy()
            throws RemoteException
        {
            synchronized (PackageManagerService.this.mInstallLock)
            {
                this.mSuccess = PackageManagerService.this.getPackageSizeInfoLI(this.mStats.packageName, this.mStats);
                if (Environment.isExternalStorageEmulated())
                {
                    i = 1;
                    if (i != 0)
                    {
                        File localFile1 = Environment.getExternalStorageAppCacheDirectory(this.mStats.packageName);
                        long l1 = PackageManagerService.this.mContainerService.calculateDirectorySize(localFile1.getPath());
                        this.mStats.externalCacheSize = l1;
                        File localFile2 = Environment.getExternalStorageAppDataDirectory(this.mStats.packageName);
                        long l2 = PackageManagerService.this.mContainerService.calculateDirectorySize(localFile2.getPath());
                        if (localFile1.getParentFile().equals(localFile2))
                            l2 -= l1;
                        this.mStats.externalDataSize = l2;
                        File localFile3 = Environment.getExternalStorageAppMediaDirectory(this.mStats.packageName);
                        this.mStats.externalMediaSize = PackageManagerService.this.mContainerService.calculateDirectorySize(localFile3.getPath());
                        File localFile4 = Environment.getExternalStorageAppObbDirectory(this.mStats.packageName);
                        this.mStats.externalObbSize = PackageManagerService.this.mContainerService.calculateDirectorySize(localFile4.getPath());
                    }
                    return;
                }
            }
            String str = Environment.getExternalStorageState();
            if ((str.equals("mounted")) || (str.equals("mounted_ro")));
            for (int i = 1; ; i = 0)
                break;
        }
    }

    private abstract class HandlerParams
    {
        private static final int MAX_RETRIES = 4;
        private int mRetries = 0;

        private HandlerParams()
        {
        }

        abstract void handleReturnCode();

        abstract void handleServiceError();

        abstract void handleStartCopy()
            throws RemoteException;

        final void serviceError()
        {
            handleServiceError();
            handleReturnCode();
        }

        final boolean startCopy()
        {
            boolean bool;
            try
            {
                int i = 1 + this.mRetries;
                this.mRetries = i;
                if (i > 4)
                {
                    Slog.w("PackageManager", "Failed to invoke remote methods on default container service. Giving up");
                    PackageManagerService.this.mHandler.sendEmptyMessage(11);
                    handleServiceError();
                    bool = false;
                }
                else
                {
                    handleStartCopy();
                    bool = true;
                    handleReturnCode();
                }
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                {
                    PackageManagerService.this.mHandler.sendEmptyMessage(10);
                    bool = false;
                }
            }
            return bool;
        }
    }

    private final class AppDirObserver extends FileObserver
    {
        private final boolean mIsRom;
        private final String mRootDir;

        public AppDirObserver(String paramInt, int paramBoolean, boolean arg4)
        {
            super(paramBoolean);
            this.mRootDir = paramInt;
            boolean bool;
            this.mIsRom = bool;
        }

        public void onEvent(int paramInt, String paramString)
        {
            String str1 = null;
            int i = -1;
            String str2 = null;
            int j = -1;
            Object localObject1 = PackageManagerService.this.mInstallLock;
            Object localObject2 = null;
            Object localObject3 = null;
            if (paramString != null);
            try
            {
                File localFile = new File(this.mRootDir, paramString);
                try
                {
                    String str4 = localFile.getPath();
                    localObject2 = str4;
                    localObject3 = localFile;
                    if (!PackageManagerService.isPackageFilename(paramString))
                        break label451;
                    if (PackageManagerService.ignoreCodePath(localObject2))
                    {
                        break label451;
                        label91: Object localObject4;
                        throw localObject4;
                    }
                    else
                    {
                        while (true)
                        {
                            int k;
                            int m;
                            synchronized (PackageManagerService.this.mPackages)
                            {
                                PackageParser.Package localPackage1 = (PackageParser.Package)PackageManagerService.this.mAppDirs.get(localObject2);
                                if (((paramInt & 0x248) != 0) && (localPackage1 != null))
                                {
                                    PackageManagerService.this.removePackageLI(localPackage1, true);
                                    str1 = localPackage1.applicationInfo.packageName;
                                    i = localPackage1.applicationInfo.uid;
                                }
                                if (((paramInt & 0x88) != 0) && (localPackage1 == null))
                                {
                                    PackageManagerService localPackageManagerService1 = PackageManagerService.this;
                                    if (!this.mIsRom)
                                        break label418;
                                    k = 65;
                                    PackageParser.Package localPackage2 = localPackageManagerService1.scanPackageLI(localObject3, 0x4 | (k | 0x2), 97, System.currentTimeMillis());
                                    if (localPackage2 != null)
                                        synchronized (PackageManagerService.this.mPackages)
                                        {
                                            PackageManagerService localPackageManagerService2 = PackageManagerService.this;
                                            String str3 = localPackage2.packageName;
                                            if (localPackage2.permissions.size() > 0)
                                            {
                                                m = 1;
                                                localPackageManagerService2.updatePermissionsLPw(str3, localPackage2, m);
                                                str2 = localPackage2.applicationInfo.packageName;
                                                j = localPackage2.applicationInfo.uid;
                                            }
                                        }
                                }
                            }
                            synchronized (PackageManagerService.this.mPackages)
                            {
                                PackageManagerService.this.mSettings.writeLPr();
                                if (str1 != null)
                                {
                                    Bundle localBundle1 = new Bundle(1);
                                    localBundle1.putInt("android.intent.extra.UID", i);
                                    localBundle1.putBoolean("android.intent.extra.DATA_REMOVED", false);
                                    PackageManagerService.sendPackageBroadcast("android.intent.action.PACKAGE_REMOVED", str1, localBundle1, null, null, -1);
                                }
                                if (str2 != null)
                                {
                                    Bundle localBundle2 = new Bundle(1);
                                    localBundle2.putInt("android.intent.extra.UID", j);
                                    PackageManagerService.sendPackageBroadcast("android.intent.action.PACKAGE_ADDED", str2, localBundle2, null, null, -1);
                                    break;
                                    localObject7 = finally;
                                    throw localObject7;
                                    label418: k = 0;
                                    continue;
                                    m = 0;
                                    continue;
                                    localObject9 = finally;
                                    throw localObject9;
                                }
                            }
                        }
                    }
                }
                finally
                {
                }
                label451: return;
            }
            finally
            {
                break label91;
            }
        }
    }

    private final class ServiceIntentResolver extends IntentResolver<PackageParser.ServiceIntentInfo, ResolveInfo>
    {
        private int mFlags;
        private final HashMap<ComponentName, PackageParser.Service> mServices = new HashMap();

        private ServiceIntentResolver()
        {
        }

        public final void addService(PackageParser.Service paramService)
        {
            this.mServices.put(paramService.getComponentName(), paramService);
            int i = paramService.intents.size();
            for (int j = 0; j < i; j++)
            {
                PackageParser.ServiceIntentInfo localServiceIntentInfo = (PackageParser.ServiceIntentInfo)paramService.intents.get(j);
                if (!localServiceIntentInfo.debugCheck())
                    Log.w("PackageManager", "==> For Service " + paramService.info.name);
                addFilter(localServiceIntentInfo);
            }
        }

        protected boolean allowFilterResult(PackageParser.ServiceIntentInfo paramServiceIntentInfo, List<ResolveInfo> paramList)
        {
            ServiceInfo localServiceInfo1 = paramServiceIntentInfo.service.info;
            int i = -1 + paramList.size();
            if (i >= 0)
            {
                ServiceInfo localServiceInfo2 = ((ResolveInfo)paramList.get(i)).serviceInfo;
                if ((localServiceInfo2.name != localServiceInfo1.name) || (localServiceInfo2.packageName != localServiceInfo1.packageName));
            }
            for (boolean bool = false; ; bool = true)
            {
                return bool;
                i--;
                break;
            }
        }

        protected void dumpFilter(PrintWriter paramPrintWriter, String paramString, PackageParser.ServiceIntentInfo paramServiceIntentInfo)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print(Integer.toHexString(System.identityHashCode(paramServiceIntentInfo.service)));
            paramPrintWriter.print(' ');
            paramPrintWriter.print(paramServiceIntentInfo.service.getComponentShortName());
            paramPrintWriter.print(" filter ");
            paramPrintWriter.println(Integer.toHexString(System.identityHashCode(paramServiceIntentInfo)));
        }

        protected boolean isFilterStopped(PackageParser.ServiceIntentInfo paramServiceIntentInfo, int paramInt)
        {
            boolean bool = true;
            if (!PackageManagerService.sUserManager.exists(paramInt));
            while (true)
            {
                return bool;
                PackageParser.Package localPackage = paramServiceIntentInfo.service.owner;
                if (localPackage != null)
                {
                    PackageSetting localPackageSetting = (PackageSetting)localPackage.mExtras;
                    if (localPackageSetting != null)
                    {
                        if ((localPackageSetting.getStopped(paramInt)) && ((0x1 & localPackageSetting.pkgFlags) == 0))
                            continue;
                        bool = false;
                    }
                }
                else
                {
                    bool = false;
                }
            }
        }

        protected ResolveInfo newResult(PackageParser.ServiceIntentInfo paramServiceIntentInfo, int paramInt1, int paramInt2)
        {
            int i = 0;
            ResolveInfo localResolveInfo = null;
            if (!PackageManagerService.sUserManager.exists(paramInt2));
            PackageParser.Service localService;
            do
            {
                do
                    return localResolveInfo;
                while (!PackageManagerService.this.mSettings.isEnabledLPr(paramServiceIntentInfo.service.info, this.mFlags, paramInt2));
                localService = paramServiceIntentInfo.service;
            }
            while ((PackageManagerService.this.mSafeMode) && ((0x1 & localService.info.applicationInfo.flags) == 0));
            localResolveInfo = new ResolveInfo();
            PackageSetting localPackageSetting = (PackageSetting)localService.owner.mExtras;
            int j = this.mFlags;
            if (localPackageSetting != null);
            for (boolean bool = localPackageSetting.getStopped(paramInt2); ; bool = false)
            {
                if (localPackageSetting != null)
                    i = localPackageSetting.getEnabled(paramInt2);
                localResolveInfo.serviceInfo = PackageParser.generateServiceInfo(localService, j, bool, i, paramInt2);
                if ((0x40 & this.mFlags) != 0)
                    localResolveInfo.filter = paramServiceIntentInfo;
                localResolveInfo.priority = paramServiceIntentInfo.getPriority();
                localResolveInfo.preferredOrder = localService.owner.mPreferredOrder;
                localResolveInfo.match = paramInt1;
                localResolveInfo.isDefault = paramServiceIntentInfo.hasDefault;
                localResolveInfo.labelRes = paramServiceIntentInfo.labelRes;
                localResolveInfo.nonLocalizedLabel = paramServiceIntentInfo.nonLocalizedLabel;
                localResolveInfo.icon = paramServiceIntentInfo.icon;
                localResolveInfo.system = PackageManagerService.isSystemApp(localResolveInfo.serviceInfo.applicationInfo);
                break;
            }
        }

        protected String packageForFilter(PackageParser.ServiceIntentInfo paramServiceIntentInfo)
        {
            return paramServiceIntentInfo.service.owner.packageName;
        }

        public List<ResolveInfo> queryIntent(Intent paramIntent, String paramString, int paramInt1, int paramInt2)
        {
            Object localObject;
            if (!PackageManagerService.sUserManager.exists(paramInt2))
            {
                localObject = null;
                return localObject;
            }
            this.mFlags = paramInt1;
            if ((0x10000 & paramInt1) != 0);
            for (boolean bool = true; ; bool = false)
            {
                localObject = super.queryIntent(paramIntent, paramString, bool, paramInt2);
                break;
            }
        }

        public List<ResolveInfo> queryIntent(Intent paramIntent, String paramString, boolean paramBoolean, int paramInt)
        {
            if (paramBoolean);
            for (int i = 65536; ; i = 0)
            {
                this.mFlags = i;
                return super.queryIntent(paramIntent, paramString, paramBoolean, paramInt);
            }
        }

        public List<ResolveInfo> queryIntentForPackage(Intent paramIntent, String paramString, int paramInt1, ArrayList<PackageParser.Service> paramArrayList, int paramInt2)
        {
            Object localObject = null;
            if (!PackageManagerService.sUserManager.exists(paramInt2));
            while (true)
            {
                return localObject;
                if (paramArrayList != null)
                {
                    this.mFlags = paramInt1;
                    if ((0x10000 & paramInt1) != 0);
                    ArrayList localArrayList1;
                    for (boolean bool = true; ; bool = false)
                    {
                        int i = paramArrayList.size();
                        localArrayList1 = new ArrayList(i);
                        for (int j = 0; j < i; j++)
                        {
                            ArrayList localArrayList2 = ((PackageParser.Service)paramArrayList.get(j)).intents;
                            if ((localArrayList2 != null) && (localArrayList2.size() > 0))
                                localArrayList1.add(localArrayList2);
                        }
                    }
                    localObject = super.queryIntentFromList(paramIntent, paramString, bool, localArrayList1, paramInt2);
                }
            }
        }

        public final void removeService(PackageParser.Service paramService)
        {
            this.mServices.remove(paramService.getComponentName());
            int i = paramService.intents.size();
            for (int j = 0; j < i; j++)
                removeFilter((PackageParser.ServiceIntentInfo)paramService.intents.get(j));
        }

        protected void sortResults(List<ResolveInfo> paramList)
        {
            Collections.sort(paramList, PackageManagerService.mResolvePrioritySorter);
        }
    }

    private final class ActivityIntentResolver extends IntentResolver<PackageParser.ActivityIntentInfo, ResolveInfo>
    {
        private final HashMap<ComponentName, PackageParser.Activity> mActivities = new HashMap();
        private int mFlags;

        private ActivityIntentResolver()
        {
        }

        public final void addActivity(PackageParser.Activity paramActivity, String paramString)
        {
            boolean bool = PackageManagerService.isSystemApp(paramActivity.info.applicationInfo);
            this.mActivities.put(paramActivity.getComponentName(), paramActivity);
            int i = paramActivity.intents.size();
            for (int j = 0; j < i; j++)
            {
                PackageParser.ActivityIntentInfo localActivityIntentInfo = (PackageParser.ActivityIntentInfo)paramActivity.intents.get(j);
                if ((!bool) && (localActivityIntentInfo.getPriority() > 0) && ("activity".equals(paramString)))
                {
                    localActivityIntentInfo.setPriority(0);
                    Log.w("PackageManager", "Package " + paramActivity.info.applicationInfo.packageName + " has activity " + paramActivity.className + " with priority > 0, forcing to 0");
                }
                if (!localActivityIntentInfo.debugCheck())
                    Log.w("PackageManager", "==> For Activity " + paramActivity.info.name);
                addFilter(localActivityIntentInfo);
            }
        }

        protected boolean allowFilterResult(PackageParser.ActivityIntentInfo paramActivityIntentInfo, List<ResolveInfo> paramList)
        {
            ActivityInfo localActivityInfo1 = paramActivityIntentInfo.activity.info;
            int i = -1 + paramList.size();
            if (i >= 0)
            {
                ActivityInfo localActivityInfo2 = ((ResolveInfo)paramList.get(i)).activityInfo;
                if ((localActivityInfo2.name != localActivityInfo1.name) || (localActivityInfo2.packageName != localActivityInfo1.packageName));
            }
            for (boolean bool = false; ; bool = true)
            {
                return bool;
                i--;
                break;
            }
        }

        protected void dumpFilter(PrintWriter paramPrintWriter, String paramString, PackageParser.ActivityIntentInfo paramActivityIntentInfo)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print(Integer.toHexString(System.identityHashCode(paramActivityIntentInfo.activity)));
            paramPrintWriter.print(' ');
            paramPrintWriter.print(paramActivityIntentInfo.activity.getComponentShortName());
            paramPrintWriter.print(" filter ");
            paramPrintWriter.println(Integer.toHexString(System.identityHashCode(paramActivityIntentInfo)));
        }

        protected boolean isFilterStopped(PackageParser.ActivityIntentInfo paramActivityIntentInfo, int paramInt)
        {
            boolean bool = true;
            if (!PackageManagerService.sUserManager.exists(paramInt));
            while (true)
            {
                return bool;
                PackageParser.Package localPackage = paramActivityIntentInfo.activity.owner;
                if (localPackage != null)
                {
                    PackageSetting localPackageSetting = (PackageSetting)localPackage.mExtras;
                    if (localPackageSetting != null)
                    {
                        if ((localPackageSetting.getStopped(paramInt)) && ((0x1 & localPackageSetting.pkgFlags) == 0))
                            continue;
                        bool = false;
                    }
                }
                else
                {
                    bool = false;
                }
            }
        }

        protected ResolveInfo newResult(PackageParser.ActivityIntentInfo paramActivityIntentInfo, int paramInt1, int paramInt2)
        {
            int i = 0;
            ResolveInfo localResolveInfo = null;
            if (!PackageManagerService.sUserManager.exists(paramInt2));
            PackageParser.Activity localActivity;
            do
            {
                do
                    return localResolveInfo;
                while (!PackageManagerService.this.mSettings.isEnabledLPr(paramActivityIntentInfo.activity.info, this.mFlags, paramInt2));
                localActivity = paramActivityIntentInfo.activity;
            }
            while ((PackageManagerService.this.mSafeMode) && ((0x1 & localActivity.info.applicationInfo.flags) == 0));
            localResolveInfo = new ResolveInfo();
            PackageSetting localPackageSetting = (PackageSetting)localActivity.owner.mExtras;
            int j = this.mFlags;
            if (localPackageSetting != null);
            for (boolean bool = localPackageSetting.getStopped(paramInt2); ; bool = false)
            {
                if (localPackageSetting != null)
                    i = localPackageSetting.getEnabled(paramInt2);
                localResolveInfo.activityInfo = PackageParser.generateActivityInfo(localActivity, j, bool, i, paramInt2);
                if ((0x40 & this.mFlags) != 0)
                    localResolveInfo.filter = paramActivityIntentInfo;
                localResolveInfo.priority = paramActivityIntentInfo.getPriority();
                localResolveInfo.preferredOrder = localActivity.owner.mPreferredOrder;
                localResolveInfo.match = paramInt1;
                localResolveInfo.isDefault = paramActivityIntentInfo.hasDefault;
                localResolveInfo.labelRes = paramActivityIntentInfo.labelRes;
                localResolveInfo.nonLocalizedLabel = paramActivityIntentInfo.nonLocalizedLabel;
                localResolveInfo.icon = paramActivityIntentInfo.icon;
                localResolveInfo.system = PackageManagerService.isSystemApp(localResolveInfo.activityInfo.applicationInfo);
                break;
            }
        }

        protected String packageForFilter(PackageParser.ActivityIntentInfo paramActivityIntentInfo)
        {
            return paramActivityIntentInfo.activity.owner.packageName;
        }

        public List<ResolveInfo> queryIntent(Intent paramIntent, String paramString, int paramInt1, int paramInt2)
        {
            Object localObject;
            if (!PackageManagerService.sUserManager.exists(paramInt2))
            {
                localObject = null;
                return localObject;
            }
            this.mFlags = paramInt1;
            if ((0x10000 & paramInt1) != 0);
            for (boolean bool = true; ; bool = false)
            {
                localObject = super.queryIntent(paramIntent, paramString, bool, paramInt2);
                break;
            }
        }

        public List<ResolveInfo> queryIntent(Intent paramIntent, String paramString, boolean paramBoolean, int paramInt)
        {
            Object localObject;
            if (!PackageManagerService.sUserManager.exists(paramInt))
            {
                localObject = null;
                return localObject;
            }
            if (paramBoolean);
            for (int i = 65536; ; i = 0)
            {
                this.mFlags = i;
                localObject = super.queryIntent(paramIntent, paramString, paramBoolean, paramInt);
                break;
            }
        }

        public List<ResolveInfo> queryIntentForPackage(Intent paramIntent, String paramString, int paramInt1, ArrayList<PackageParser.Activity> paramArrayList, int paramInt2)
        {
            Object localObject = null;
            if (!PackageManagerService.sUserManager.exists(paramInt2));
            while (true)
            {
                return localObject;
                if (paramArrayList != null)
                {
                    this.mFlags = paramInt1;
                    if ((0x10000 & paramInt1) != 0);
                    ArrayList localArrayList1;
                    for (boolean bool = true; ; bool = false)
                    {
                        int i = paramArrayList.size();
                        localArrayList1 = new ArrayList(i);
                        for (int j = 0; j < i; j++)
                        {
                            ArrayList localArrayList2 = ((PackageParser.Activity)paramArrayList.get(j)).intents;
                            if ((localArrayList2 != null) && (localArrayList2.size() > 0))
                                localArrayList1.add(localArrayList2);
                        }
                    }
                    localObject = super.queryIntentFromList(paramIntent, paramString, bool, localArrayList1, paramInt2);
                }
            }
        }

        public final void removeActivity(PackageParser.Activity paramActivity, String paramString)
        {
            this.mActivities.remove(paramActivity.getComponentName());
            int i = paramActivity.intents.size();
            for (int j = 0; j < i; j++)
                removeFilter((PackageParser.ActivityIntentInfo)paramActivity.intents.get(j));
        }

        protected void sortResults(List<ResolveInfo> paramList)
        {
            Collections.sort(paramList, PackageManagerService.mResolvePrioritySorter);
        }
    }

    class PackageHandler extends Handler
    {
        private boolean mBound = false;
        final ArrayList<PackageManagerService.HandlerParams> mPendingInstalls = new ArrayList();

        PackageHandler(Looper arg2)
        {
            super();
        }

        private boolean connectToService()
        {
            int i = 1;
            Intent localIntent = new Intent().setComponent(PackageManagerService.DEFAULT_CONTAINER_COMPONENT);
            Process.setThreadPriority(0);
            if (PackageManagerService.this.mContext.bindService(localIntent, PackageManagerService.this.mDefContainerConn, i))
            {
                Process.setThreadPriority(10);
                this.mBound = i;
            }
            while (true)
            {
                return i;
                Process.setThreadPriority(10);
                int j = 0;
            }
        }

        private void disconnectService()
        {
            PackageManagerService.access$302(PackageManagerService.this, null);
            this.mBound = false;
            Process.setThreadPriority(0);
            PackageManagerService.this.mContext.unbindService(PackageManagerService.this.mDefContainerConn);
            Process.setThreadPriority(10);
        }

        // ERROR //
        void doHandleMessage(Message paramMessage)
        {
            // Byte code:
            //     0: aload_1
            //     1: getfield 82	android/os/Message:what	I
            //     4: tableswitch	default:+80 -> 84, 1:+535->539, 2:+80->84, 3:+174->178, 4:+80->84, 5:+81->85, 6:+461->465, 7:+811->815, 8:+80->84, 9:+896->900, 10:+380->384, 11:+523->527, 12:+1238->1242, 13:+1338->1342, 14:+1405->1409, 15:+1615->1619, 16:+1508->1512
            //     85: aload_1
            //     86: getfield 86	android/os/Message:obj	Ljava/lang/Object;
            //     89: checkcast 88	com/android/server/pm/PackageManagerService$HandlerParams
            //     92: astore 70
            //     94: aload_0
            //     95: getfield 30	com/android/server/pm/PackageManagerService$PackageHandler:mPendingInstalls	Ljava/util/ArrayList;
            //     98: invokevirtual 92	java/util/ArrayList:size	()I
            //     101: istore 71
            //     103: aload_0
            //     104: getfield 23	com/android/server/pm/PackageManagerService$PackageHandler:mBound	Z
            //     107: ifne +40 -> 147
            //     110: aload_0
            //     111: invokespecial 94	com/android/server/pm/PackageManagerService$PackageHandler:connectToService	()Z
            //     114: ifne +19 -> 133
            //     117: ldc 96
            //     119: ldc 98
            //     121: invokestatic 104	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     124: pop
            //     125: aload 70
            //     127: invokevirtual 107	com/android/server/pm/PackageManagerService$HandlerParams:serviceError	()V
            //     130: goto -46 -> 84
            //     133: aload_0
            //     134: getfield 30	com/android/server/pm/PackageManagerService$PackageHandler:mPendingInstalls	Ljava/util/ArrayList;
            //     137: iload 71
            //     139: aload 70
            //     141: invokevirtual 111	java/util/ArrayList:add	(ILjava/lang/Object;)V
            //     144: goto -60 -> 84
            //     147: aload_0
            //     148: getfield 30	com/android/server/pm/PackageManagerService$PackageHandler:mPendingInstalls	Ljava/util/ArrayList;
            //     151: iload 71
            //     153: aload 70
            //     155: invokevirtual 111	java/util/ArrayList:add	(ILjava/lang/Object;)V
            //     158: iload 71
            //     160: ifne -76 -> 84
            //     163: aload_0
            //     164: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     167: getfield 115	com/android/server/pm/PackageManagerService:mHandler	Lcom/android/server/pm/PackageManagerService$PackageHandler;
            //     170: iconst_3
            //     171: invokevirtual 119	com/android/server/pm/PackageManagerService$PackageHandler:sendEmptyMessage	(I)Z
            //     174: pop
            //     175: goto -91 -> 84
            //     178: aload_1
            //     179: getfield 86	android/os/Message:obj	Ljava/lang/Object;
            //     182: ifnull +18 -> 200
            //     185: aload_0
            //     186: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     189: aload_1
            //     190: getfield 86	android/os/Message:obj	Ljava/lang/Object;
            //     193: checkcast 121	com/android/internal/app/IMediaContainerService
            //     196: invokestatic 68	com/android/server/pm/PackageManagerService:access$302	(Lcom/android/server/pm/PackageManagerService;Lcom/android/internal/app/IMediaContainerService;)Lcom/android/internal/app/IMediaContainerService;
            //     199: pop
            //     200: aload_0
            //     201: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     204: invokestatic 125	com/android/server/pm/PackageManagerService:access$300	(Lcom/android/server/pm/PackageManagerService;)Lcom/android/internal/app/IMediaContainerService;
            //     207: ifnonnull +56 -> 263
            //     210: ldc 96
            //     212: ldc 127
            //     214: invokestatic 104	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     217: pop
            //     218: aload_0
            //     219: getfield 30	com/android/server/pm/PackageManagerService$PackageHandler:mPendingInstalls	Ljava/util/ArrayList;
            //     222: invokevirtual 131	java/util/ArrayList:iterator	()Ljava/util/Iterator;
            //     225: astore 68
            //     227: aload 68
            //     229: invokeinterface 136 1 0
            //     234: ifeq +19 -> 253
            //     237: aload 68
            //     239: invokeinterface 140 1 0
            //     244: checkcast 88	com/android/server/pm/PackageManagerService$HandlerParams
            //     247: invokevirtual 107	com/android/server/pm/PackageManagerService$HandlerParams:serviceError	()V
            //     250: goto -23 -> 227
            //     253: aload_0
            //     254: getfield 30	com/android/server/pm/PackageManagerService$PackageHandler:mPendingInstalls	Ljava/util/ArrayList;
            //     257: invokevirtual 143	java/util/ArrayList:clear	()V
            //     260: goto -176 -> 84
            //     263: aload_0
            //     264: getfield 30	com/android/server/pm/PackageManagerService$PackageHandler:mPendingInstalls	Ljava/util/ArrayList;
            //     267: invokevirtual 92	java/util/ArrayList:size	()I
            //     270: ifle +103 -> 373
            //     273: aload_0
            //     274: getfield 30	com/android/server/pm/PackageManagerService$PackageHandler:mPendingInstalls	Ljava/util/ArrayList;
            //     277: iconst_0
            //     278: invokevirtual 147	java/util/ArrayList:get	(I)Ljava/lang/Object;
            //     281: checkcast 88	com/android/server/pm/PackageManagerService$HandlerParams
            //     284: astore 63
            //     286: aload 63
            //     288: ifnull -204 -> 84
            //     291: aload 63
            //     293: invokevirtual 150	com/android/server/pm/PackageManagerService$HandlerParams:startCopy	()Z
            //     296: ifeq -212 -> 84
            //     299: aload_0
            //     300: getfield 30	com/android/server/pm/PackageManagerService$PackageHandler:mPendingInstalls	Ljava/util/ArrayList;
            //     303: invokevirtual 92	java/util/ArrayList:size	()I
            //     306: ifle +12 -> 318
            //     309: aload_0
            //     310: getfield 30	com/android/server/pm/PackageManagerService$PackageHandler:mPendingInstalls	Ljava/util/ArrayList;
            //     313: iconst_0
            //     314: invokevirtual 153	java/util/ArrayList:remove	(I)Ljava/lang/Object;
            //     317: pop
            //     318: aload_0
            //     319: getfield 30	com/android/server/pm/PackageManagerService$PackageHandler:mPendingInstalls	Ljava/util/ArrayList;
            //     322: invokevirtual 92	java/util/ArrayList:size	()I
            //     325: ifne +33 -> 358
            //     328: aload_0
            //     329: getfield 23	com/android/server/pm/PackageManagerService$PackageHandler:mBound	Z
            //     332: ifeq -248 -> 84
            //     335: aload_0
            //     336: bipush 6
            //     338: invokevirtual 156	com/android/server/pm/PackageManagerService$PackageHandler:removeMessages	(I)V
            //     341: aload_0
            //     342: aload_0
            //     343: bipush 6
            //     345: invokevirtual 160	com/android/server/pm/PackageManagerService$PackageHandler:obtainMessage	(I)Landroid/os/Message;
            //     348: ldc2_w 161
            //     351: invokevirtual 166	com/android/server/pm/PackageManagerService$PackageHandler:sendMessageDelayed	(Landroid/os/Message;J)Z
            //     354: pop
            //     355: goto -271 -> 84
            //     358: aload_0
            //     359: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     362: getfield 115	com/android/server/pm/PackageManagerService:mHandler	Lcom/android/server/pm/PackageManagerService$PackageHandler;
            //     365: iconst_3
            //     366: invokevirtual 119	com/android/server/pm/PackageManagerService$PackageHandler:sendEmptyMessage	(I)Z
            //     369: pop
            //     370: goto -286 -> 84
            //     373: ldc 96
            //     375: ldc 168
            //     377: invokestatic 171	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     380: pop
            //     381: goto -297 -> 84
            //     384: aload_0
            //     385: getfield 30	com/android/server/pm/PackageManagerService$PackageHandler:mPendingInstalls	Ljava/util/ArrayList;
            //     388: invokevirtual 92	java/util/ArrayList:size	()I
            //     391: ifle -307 -> 84
            //     394: aload_0
            //     395: getfield 23	com/android/server/pm/PackageManagerService$PackageHandler:mBound	Z
            //     398: ifeq +7 -> 405
            //     401: aload_0
            //     402: invokespecial 173	com/android/server/pm/PackageManagerService$PackageHandler:disconnectService	()V
            //     405: aload_0
            //     406: invokespecial 94	com/android/server/pm/PackageManagerService$PackageHandler:connectToService	()Z
            //     409: ifne -325 -> 84
            //     412: ldc 96
            //     414: ldc 98
            //     416: invokestatic 104	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     419: pop
            //     420: aload_0
            //     421: getfield 30	com/android/server/pm/PackageManagerService$PackageHandler:mPendingInstalls	Ljava/util/ArrayList;
            //     424: invokevirtual 131	java/util/ArrayList:iterator	()Ljava/util/Iterator;
            //     427: astore 61
            //     429: aload 61
            //     431: invokeinterface 136 1 0
            //     436: ifeq +19 -> 455
            //     439: aload 61
            //     441: invokeinterface 140 1 0
            //     446: checkcast 88	com/android/server/pm/PackageManagerService$HandlerParams
            //     449: invokevirtual 107	com/android/server/pm/PackageManagerService$HandlerParams:serviceError	()V
            //     452: goto -23 -> 429
            //     455: aload_0
            //     456: getfield 30	com/android/server/pm/PackageManagerService$PackageHandler:mPendingInstalls	Ljava/util/ArrayList;
            //     459: invokevirtual 143	java/util/ArrayList:clear	()V
            //     462: goto -378 -> 84
            //     465: aload_0
            //     466: getfield 30	com/android/server/pm/PackageManagerService$PackageHandler:mPendingInstalls	Ljava/util/ArrayList;
            //     469: invokevirtual 92	java/util/ArrayList:size	()I
            //     472: ifne +30 -> 502
            //     475: aload_0
            //     476: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     479: getfield 177	com/android/server/pm/PackageManagerService:mPendingVerification	Landroid/util/SparseArray;
            //     482: invokevirtual 180	android/util/SparseArray:size	()I
            //     485: ifne +17 -> 502
            //     488: aload_0
            //     489: getfield 23	com/android/server/pm/PackageManagerService$PackageHandler:mBound	Z
            //     492: ifeq -408 -> 84
            //     495: aload_0
            //     496: invokespecial 173	com/android/server/pm/PackageManagerService$PackageHandler:disconnectService	()V
            //     499: goto -415 -> 84
            //     502: aload_0
            //     503: getfield 30	com/android/server/pm/PackageManagerService$PackageHandler:mPendingInstalls	Ljava/util/ArrayList;
            //     506: invokevirtual 92	java/util/ArrayList:size	()I
            //     509: ifle -425 -> 84
            //     512: aload_0
            //     513: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     516: getfield 115	com/android/server/pm/PackageManagerService:mHandler	Lcom/android/server/pm/PackageManagerService$PackageHandler;
            //     519: iconst_3
            //     520: invokevirtual 119	com/android/server/pm/PackageManagerService$PackageHandler:sendEmptyMessage	(I)Z
            //     523: pop
            //     524: goto -440 -> 84
            //     527: aload_0
            //     528: getfield 30	com/android/server/pm/PackageManagerService$PackageHandler:mPendingInstalls	Ljava/util/ArrayList;
            //     531: iconst_0
            //     532: invokevirtual 153	java/util/ArrayList:remove	(I)Ljava/lang/Object;
            //     535: pop
            //     536: goto -452 -> 84
            //     539: iconst_0
            //     540: invokestatic 49	android/os/Process:setThreadPriority	(I)V
            //     543: aload_0
            //     544: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     547: getfield 184	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
            //     550: astore 45
            //     552: aload 45
            //     554: monitorenter
            //     555: aload_0
            //     556: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     559: getfield 187	com/android/server/pm/PackageManagerService:mPendingBroadcasts	Ljava/util/HashMap;
            //     562: ifnonnull +17 -> 579
            //     565: aload 45
            //     567: monitorexit
            //     568: goto -484 -> 84
            //     571: astore 46
            //     573: aload 45
            //     575: monitorexit
            //     576: aload 46
            //     578: athrow
            //     579: aload_0
            //     580: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     583: getfield 187	com/android/server/pm/PackageManagerService:mPendingBroadcasts	Ljava/util/HashMap;
            //     586: invokevirtual 190	java/util/HashMap:size	()I
            //     589: istore 47
            //     591: iload 47
            //     593: ifgt +9 -> 602
            //     596: aload 45
            //     598: monitorexit
            //     599: goto -515 -> 84
            //     602: iload 47
            //     604: anewarray 192	java/lang/String
            //     607: astore 48
            //     609: iload 47
            //     611: anewarray 25	java/util/ArrayList
            //     614: astore 49
            //     616: iload 47
            //     618: newarray int
            //     620: astore 50
            //     622: aload_0
            //     623: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     626: getfield 187	com/android/server/pm/PackageManagerService:mPendingBroadcasts	Ljava/util/HashMap;
            //     629: invokevirtual 196	java/util/HashMap:entrySet	()Ljava/util/Set;
            //     632: invokeinterface 199 1 0
            //     637: astore 51
            //     639: iconst_0
            //     640: istore 52
            //     642: aload 51
            //     644: invokeinterface 136 1 0
            //     649: ifeq +102 -> 751
            //     652: iload 52
            //     654: iload 47
            //     656: if_icmpge +95 -> 751
            //     659: aload 51
            //     661: invokeinterface 140 1 0
            //     666: checkcast 201	java/util/Map$Entry
            //     669: astore 55
            //     671: aload 48
            //     673: iload 52
            //     675: aload 55
            //     677: invokeinterface 204 1 0
            //     682: checkcast 192	java/lang/String
            //     685: aastore
            //     686: aload 49
            //     688: iload 52
            //     690: aload 55
            //     692: invokeinterface 207 1 0
            //     697: checkcast 25	java/util/ArrayList
            //     700: aastore
            //     701: aload_0
            //     702: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     705: getfield 211	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
            //     708: getfield 214	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
            //     711: aload 55
            //     713: invokeinterface 204 1 0
            //     718: invokevirtual 217	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
            //     721: checkcast 219	com/android/server/pm/PackageSetting
            //     724: astore 56
            //     726: aload 56
            //     728: ifnull +1076 -> 1804
            //     731: aload 56
            //     733: getfield 222	com/android/server/pm/PackageSetting:appId	I
            //     736: istore 57
            //     738: aload 50
            //     740: iload 52
            //     742: iload 57
            //     744: iastore
            //     745: iinc 52 1
            //     748: goto -106 -> 642
            //     751: iload 52
            //     753: istore 53
            //     755: aload_0
            //     756: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     759: getfield 187	com/android/server/pm/PackageManagerService:mPendingBroadcasts	Ljava/util/HashMap;
            //     762: invokevirtual 223	java/util/HashMap:clear	()V
            //     765: aload 45
            //     767: monitorexit
            //     768: iconst_0
            //     769: istore 54
            //     771: iload 54
            //     773: iload 53
            //     775: if_icmpge +32 -> 807
            //     778: aload_0
            //     779: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     782: aload 48
            //     784: iload 54
            //     786: aaload
            //     787: iconst_1
            //     788: aload 49
            //     790: iload 54
            //     792: aaload
            //     793: aload 50
            //     795: iload 54
            //     797: iaload
            //     798: invokestatic 227	com/android/server/pm/PackageManagerService:access$400	(Lcom/android/server/pm/PackageManagerService;Ljava/lang/String;ZLjava/util/ArrayList;I)V
            //     801: iinc 54 1
            //     804: goto -33 -> 771
            //     807: bipush 10
            //     809: invokestatic 49	android/os/Process:setThreadPriority	(I)V
            //     812: goto -728 -> 84
            //     815: aload_1
            //     816: getfield 86	android/os/Message:obj	Ljava/lang/Object;
            //     819: checkcast 192	java/lang/String
            //     822: astore 41
            //     824: iconst_0
            //     825: invokestatic 49	android/os/Process:setThreadPriority	(I)V
            //     828: aload_0
            //     829: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     832: getfield 184	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
            //     835: astore 42
            //     837: aload 42
            //     839: monitorenter
            //     840: aload_0
            //     841: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     844: getfield 211	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
            //     847: getfield 230	com/android/server/pm/Settings:mPackagesToBeCleaned	Ljava/util/ArrayList;
            //     850: aload 41
            //     852: invokevirtual 234	java/util/ArrayList:contains	(Ljava/lang/Object;)Z
            //     855: ifne +19 -> 874
            //     858: aload_0
            //     859: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     862: getfield 211	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
            //     865: getfield 230	com/android/server/pm/Settings:mPackagesToBeCleaned	Ljava/util/ArrayList;
            //     868: aload 41
            //     870: invokevirtual 236	java/util/ArrayList:add	(Ljava/lang/Object;)Z
            //     873: pop
            //     874: aload 42
            //     876: monitorexit
            //     877: bipush 10
            //     879: invokestatic 49	android/os/Process:setThreadPriority	(I)V
            //     882: aload_0
            //     883: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     886: invokevirtual 239	com/android/server/pm/PackageManagerService:startCleaningPackages	()V
            //     889: goto -805 -> 84
            //     892: astore 43
            //     894: aload 42
            //     896: monitorexit
            //     897: aload 43
            //     899: athrow
            //     900: aload_0
            //     901: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     904: getfield 242	com/android/server/pm/PackageManagerService:mRunningInstalls	Landroid/util/SparseArray;
            //     907: aload_1
            //     908: getfield 245	android/os/Message:arg1	I
            //     911: invokevirtual 246	android/util/SparseArray:get	(I)Ljava/lang/Object;
            //     914: checkcast 248	com/android/server/pm/PackageManagerService$PostInstallData
            //     917: astore 29
            //     919: aload_0
            //     920: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     923: getfield 242	com/android/server/pm/PackageManagerService:mRunningInstalls	Landroid/util/SparseArray;
            //     926: aload_1
            //     927: getfield 245	android/os/Message:arg1	I
            //     930: invokevirtual 251	android/util/SparseArray:delete	(I)V
            //     933: iconst_0
            //     934: istore 30
            //     936: aload 29
            //     938: ifnull +272 -> 1210
            //     941: aload 29
            //     943: getfield 255	com/android/server/pm/PackageManagerService$PostInstallData:args	Lcom/android/server/pm/PackageManagerService$InstallArgs;
            //     946: astore 32
            //     948: aload 29
            //     950: getfield 259	com/android/server/pm/PackageManagerService$PostInstallData:res	Lcom/android/server/pm/PackageManagerService$PackageInstalledInfo;
            //     953: astore 33
            //     955: aload 33
            //     957: getfield 264	com/android/server/pm/PackageManagerService$PackageInstalledInfo:returnCode	I
            //     960: iconst_1
            //     961: if_icmpne +151 -> 1112
            //     964: aload 33
            //     966: getfield 268	com/android/server/pm/PackageManagerService$PackageInstalledInfo:removedInfo	Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;
            //     969: iconst_0
            //     970: iconst_1
            //     971: invokevirtual 274	com/android/server/pm/PackageManagerService$PackageRemovedInfo:sendBroadcast	(ZZ)V
            //     974: new 276	android/os/Bundle
            //     977: dup
            //     978: iconst_1
            //     979: invokespecial 278	android/os/Bundle:<init>	(I)V
            //     982: astore 39
            //     984: aload 39
            //     986: ldc_w 280
            //     989: aload 33
            //     991: getfield 283	com/android/server/pm/PackageManagerService$PackageInstalledInfo:uid	I
            //     994: invokevirtual 287	android/os/Bundle:putInt	(Ljava/lang/String;I)V
            //     997: aload 33
            //     999: getfield 268	com/android/server/pm/PackageManagerService$PackageInstalledInfo:removedInfo	Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;
            //     1002: getfield 291	com/android/server/pm/PackageManagerService$PackageRemovedInfo:removedPackage	Ljava/lang/String;
            //     1005: ifnull +191 -> 1196
            //     1008: iconst_1
            //     1009: istore 40
            //     1011: iload 40
            //     1013: ifeq +12 -> 1025
            //     1016: aload 39
            //     1018: ldc_w 293
            //     1021: iconst_1
            //     1022: invokevirtual 297	android/os/Bundle:putBoolean	(Ljava/lang/String;Z)V
            //     1025: ldc_w 299
            //     1028: aload 33
            //     1030: getfield 303	com/android/server/pm/PackageManagerService$PackageInstalledInfo:pkg	Landroid/content/pm/PackageParser$Package;
            //     1033: getfield 309	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
            //     1036: getfield 314	android/content/pm/ApplicationInfo:packageName	Ljava/lang/String;
            //     1039: aload 39
            //     1041: aconst_null
            //     1042: aconst_null
            //     1043: bipush 255
            //     1045: invokestatic 318	com/android/server/pm/PackageManagerService:sendPackageBroadcast	(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/IIntentReceiver;I)V
            //     1048: iload 40
            //     1050: ifeq +48 -> 1098
            //     1053: ldc_w 320
            //     1056: aload 33
            //     1058: getfield 303	com/android/server/pm/PackageManagerService$PackageInstalledInfo:pkg	Landroid/content/pm/PackageParser$Package;
            //     1061: getfield 309	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
            //     1064: getfield 314	android/content/pm/ApplicationInfo:packageName	Ljava/lang/String;
            //     1067: aload 39
            //     1069: aconst_null
            //     1070: aconst_null
            //     1071: bipush 255
            //     1073: invokestatic 318	com/android/server/pm/PackageManagerService:sendPackageBroadcast	(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/IIntentReceiver;I)V
            //     1076: ldc_w 322
            //     1079: aconst_null
            //     1080: aconst_null
            //     1081: aload 33
            //     1083: getfield 303	com/android/server/pm/PackageManagerService$PackageInstalledInfo:pkg	Landroid/content/pm/PackageParser$Package;
            //     1086: getfield 309	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
            //     1089: getfield 314	android/content/pm/ApplicationInfo:packageName	Ljava/lang/String;
            //     1092: aconst_null
            //     1093: bipush 255
            //     1095: invokestatic 318	com/android/server/pm/PackageManagerService:sendPackageBroadcast	(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/IIntentReceiver;I)V
            //     1098: aload 33
            //     1100: getfield 268	com/android/server/pm/PackageManagerService$PackageInstalledInfo:removedInfo	Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;
            //     1103: getfield 323	com/android/server/pm/PackageManagerService$PackageRemovedInfo:args	Lcom/android/server/pm/PackageManagerService$InstallArgs;
            //     1106: ifnull +6 -> 1112
            //     1109: iconst_1
            //     1110: istore 30
            //     1112: invokestatic 329	java/lang/Runtime:getRuntime	()Ljava/lang/Runtime;
            //     1115: invokevirtual 332	java/lang/Runtime:gc	()V
            //     1118: iload 30
            //     1120: ifeq +31 -> 1151
            //     1123: aload_0
            //     1124: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     1127: getfield 335	com/android/server/pm/PackageManagerService:mInstallLock	Ljava/lang/Object;
            //     1130: astore 36
            //     1132: aload 36
            //     1134: monitorenter
            //     1135: aload 33
            //     1137: getfield 268	com/android/server/pm/PackageManagerService$PackageInstalledInfo:removedInfo	Lcom/android/server/pm/PackageManagerService$PackageRemovedInfo;
            //     1140: getfield 323	com/android/server/pm/PackageManagerService$PackageRemovedInfo:args	Lcom/android/server/pm/PackageManagerService$InstallArgs;
            //     1143: iconst_1
            //     1144: invokevirtual 341	com/android/server/pm/PackageManagerService$InstallArgs:doPostDeleteLI	(Z)Z
            //     1147: pop
            //     1148: aload 36
            //     1150: monitorexit
            //     1151: aload 32
            //     1153: getfield 345	com/android/server/pm/PackageManagerService$InstallArgs:observer	Landroid/content/pm/IPackageInstallObserver;
            //     1156: ifnull -1072 -> 84
            //     1159: aload 32
            //     1161: getfield 345	com/android/server/pm/PackageManagerService$InstallArgs:observer	Landroid/content/pm/IPackageInstallObserver;
            //     1164: aload 33
            //     1166: getfield 348	com/android/server/pm/PackageManagerService$PackageInstalledInfo:name	Ljava/lang/String;
            //     1169: aload 33
            //     1171: getfield 264	com/android/server/pm/PackageManagerService$PackageInstalledInfo:returnCode	I
            //     1174: invokeinterface 353 3 0
            //     1179: goto -1095 -> 84
            //     1182: astore 34
            //     1184: ldc 96
            //     1186: ldc_w 355
            //     1189: invokestatic 358	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
            //     1192: pop
            //     1193: goto -1109 -> 84
            //     1196: iconst_0
            //     1197: istore 40
            //     1199: goto -188 -> 1011
            //     1202: astore 37
            //     1204: aload 36
            //     1206: monitorexit
            //     1207: aload 37
            //     1209: athrow
            //     1210: ldc 96
            //     1212: new 360	java/lang/StringBuilder
            //     1215: dup
            //     1216: invokespecial 361	java/lang/StringBuilder:<init>	()V
            //     1219: ldc_w 363
            //     1222: invokevirtual 367	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     1225: aload_1
            //     1226: getfield 245	android/os/Message:arg1	I
            //     1229: invokevirtual 370	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     1232: invokevirtual 374	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     1235: invokestatic 104	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     1238: pop
            //     1239: goto -1155 -> 84
            //     1242: aload_1
            //     1243: getfield 245	android/os/Message:arg1	I
            //     1246: iconst_1
            //     1247: if_icmpne +83 -> 1330
            //     1250: iconst_1
            //     1251: istore 24
            //     1253: aload_1
            //     1254: getfield 377	android/os/Message:arg2	I
            //     1257: iconst_1
            //     1258: if_icmpne +78 -> 1336
            //     1261: iconst_1
            //     1262: istore 25
            //     1264: iload 25
            //     1266: ifeq +9 -> 1275
            //     1269: invokestatic 329	java/lang/Runtime:getRuntime	()Ljava/lang/Runtime;
            //     1272: invokevirtual 332	java/lang/Runtime:gc	()V
            //     1275: aload_1
            //     1276: getfield 86	android/os/Message:obj	Ljava/lang/Object;
            //     1279: ifnull +21 -> 1300
            //     1282: aload_1
            //     1283: getfield 86	android/os/Message:obj	Ljava/lang/Object;
            //     1286: checkcast 198	java/util/Set
            //     1289: astore 28
            //     1291: aload_0
            //     1292: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     1295: aload 28
            //     1297: invokestatic 381	com/android/server/pm/PackageManagerService:access$500	(Lcom/android/server/pm/PackageManagerService;Ljava/util/Set;)V
            //     1300: iload 24
            //     1302: ifeq -1218 -> 84
            //     1305: invokestatic 387	com/android/internal/content/PackageHelper:getMountService	()Landroid/os/storage/IMountService;
            //     1308: invokeinterface 392 1 0
            //     1313: goto -1229 -> 84
            //     1316: astore 26
            //     1318: ldc 96
            //     1320: ldc_w 394
            //     1323: invokestatic 397	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     1326: pop
            //     1327: goto -1243 -> 84
            //     1330: iconst_0
            //     1331: istore 24
            //     1333: goto -80 -> 1253
            //     1336: iconst_0
            //     1337: istore 25
            //     1339: goto -75 -> 1264
            //     1342: iconst_0
            //     1343: invokestatic 49	android/os/Process:setThreadPriority	(I)V
            //     1346: aload_0
            //     1347: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     1350: getfield 184	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
            //     1353: astore 22
            //     1355: aload 22
            //     1357: monitorenter
            //     1358: aload_0
            //     1359: bipush 13
            //     1361: invokevirtual 156	com/android/server/pm/PackageManagerService$PackageHandler:removeMessages	(I)V
            //     1364: aload_0
            //     1365: bipush 14
            //     1367: invokevirtual 156	com/android/server/pm/PackageManagerService$PackageHandler:removeMessages	(I)V
            //     1370: aload_0
            //     1371: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     1374: getfield 211	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
            //     1377: invokevirtual 400	com/android/server/pm/Settings:writeLPr	()V
            //     1380: aload_0
            //     1381: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     1384: invokestatic 404	com/android/server/pm/PackageManagerService:access$600	(Lcom/android/server/pm/PackageManagerService;)Ljava/util/HashSet;
            //     1387: invokevirtual 407	java/util/HashSet:clear	()V
            //     1390: aload 22
            //     1392: monitorexit
            //     1393: bipush 10
            //     1395: invokestatic 49	android/os/Process:setThreadPriority	(I)V
            //     1398: goto -1314 -> 84
            //     1401: astore 23
            //     1403: aload 22
            //     1405: monitorexit
            //     1406: aload 23
            //     1408: athrow
            //     1409: iconst_0
            //     1410: invokestatic 49	android/os/Process:setThreadPriority	(I)V
            //     1413: aload_0
            //     1414: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     1417: getfield 184	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
            //     1420: astore 18
            //     1422: aload 18
            //     1424: monitorenter
            //     1425: aload_0
            //     1426: bipush 14
            //     1428: invokevirtual 156	com/android/server/pm/PackageManagerService$PackageHandler:removeMessages	(I)V
            //     1431: aload_0
            //     1432: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     1435: invokestatic 404	com/android/server/pm/PackageManagerService:access$600	(Lcom/android/server/pm/PackageManagerService;)Ljava/util/HashSet;
            //     1438: invokevirtual 408	java/util/HashSet:iterator	()Ljava/util/Iterator;
            //     1441: astore 20
            //     1443: aload 20
            //     1445: invokeinterface 136 1 0
            //     1450: ifeq +41 -> 1491
            //     1453: aload 20
            //     1455: invokeinterface 140 1 0
            //     1460: checkcast 410	java/lang/Integer
            //     1463: invokevirtual 413	java/lang/Integer:intValue	()I
            //     1466: istore 21
            //     1468: aload_0
            //     1469: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     1472: getfield 211	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
            //     1475: iload 21
            //     1477: invokevirtual 416	com/android/server/pm/Settings:writePackageRestrictionsLPr	(I)V
            //     1480: goto -37 -> 1443
            //     1483: astore 19
            //     1485: aload 18
            //     1487: monitorexit
            //     1488: aload 19
            //     1490: athrow
            //     1491: aload_0
            //     1492: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     1495: invokestatic 404	com/android/server/pm/PackageManagerService:access$600	(Lcom/android/server/pm/PackageManagerService;)Ljava/util/HashSet;
            //     1498: invokevirtual 407	java/util/HashSet:clear	()V
            //     1501: aload 18
            //     1503: monitorexit
            //     1504: bipush 10
            //     1506: invokestatic 49	android/os/Process:setThreadPriority	(I)V
            //     1509: goto -1425 -> 84
            //     1512: aload_1
            //     1513: getfield 245	android/os/Message:arg1	I
            //     1516: istore 13
            //     1518: aload_0
            //     1519: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     1522: getfield 177	com/android/server/pm/PackageManagerService:mPendingVerification	Landroid/util/SparseArray;
            //     1525: iload 13
            //     1527: invokevirtual 246	android/util/SparseArray:get	(I)Ljava/lang/Object;
            //     1530: checkcast 418	com/android/server/pm/PackageVerificationState
            //     1533: astore 14
            //     1535: aload 14
            //     1537: ifnull -1453 -> 84
            //     1540: aload 14
            //     1542: invokevirtual 422	com/android/server/pm/PackageVerificationState:getInstallArgs	()Lcom/android/server/pm/PackageManagerService$InstallArgs;
            //     1545: astore 15
            //     1547: ldc 96
            //     1549: new 360	java/lang/StringBuilder
            //     1552: dup
            //     1553: invokespecial 361	java/lang/StringBuilder:<init>	()V
            //     1556: ldc_w 424
            //     1559: invokevirtual 367	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     1562: aload 15
            //     1564: getfield 428	com/android/server/pm/PackageManagerService$InstallArgs:packageURI	Landroid/net/Uri;
            //     1567: invokevirtual 431	android/net/Uri:toString	()Ljava/lang/String;
            //     1570: invokevirtual 367	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     1573: invokevirtual 374	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     1576: invokestatic 358	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
            //     1579: pop
            //     1580: aload_0
            //     1581: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     1584: getfield 177	com/android/server/pm/PackageManagerService:mPendingVerification	Landroid/util/SparseArray;
            //     1587: iload 13
            //     1589: invokevirtual 433	android/util/SparseArray:remove	(I)V
            //     1592: aload_0
            //     1593: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     1596: aload 15
            //     1598: bipush 235
            //     1600: invokestatic 437	com/android/server/pm/PackageManagerService:access$700	(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageManagerService$InstallArgs;I)V
            //     1603: aload_0
            //     1604: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     1607: getfield 115	com/android/server/pm/PackageManagerService:mHandler	Lcom/android/server/pm/PackageManagerService$PackageHandler;
            //     1610: bipush 6
            //     1612: invokevirtual 119	com/android/server/pm/PackageManagerService$PackageHandler:sendEmptyMessage	(I)Z
            //     1615: pop
            //     1616: goto -1532 -> 84
            //     1619: aload_1
            //     1620: getfield 245	android/os/Message:arg1	I
            //     1623: istore_2
            //     1624: aload_0
            //     1625: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     1628: getfield 177	com/android/server/pm/PackageManagerService:mPendingVerification	Landroid/util/SparseArray;
            //     1631: iload_2
            //     1632: invokevirtual 246	android/util/SparseArray:get	(I)Ljava/lang/Object;
            //     1635: checkcast 418	com/android/server/pm/PackageVerificationState
            //     1638: astore_3
            //     1639: aload_3
            //     1640: ifnonnull +38 -> 1678
            //     1643: ldc 96
            //     1645: new 360	java/lang/StringBuilder
            //     1648: dup
            //     1649: invokespecial 361	java/lang/StringBuilder:<init>	()V
            //     1652: ldc_w 439
            //     1655: invokevirtual 367	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     1658: iload_2
            //     1659: invokevirtual 370	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
            //     1662: ldc_w 441
            //     1665: invokevirtual 367	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     1668: invokevirtual 374	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     1671: invokestatic 171	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     1674: pop
            //     1675: goto -1591 -> 84
            //     1678: aload_1
            //     1679: getfield 86	android/os/Message:obj	Ljava/lang/Object;
            //     1682: checkcast 443	com/android/server/pm/PackageVerificationResponse
            //     1685: astore 4
            //     1687: aload_3
            //     1688: aload 4
            //     1690: getfield 446	com/android/server/pm/PackageVerificationResponse:callerUid	I
            //     1693: aload 4
            //     1695: getfield 449	com/android/server/pm/PackageVerificationResponse:code	I
            //     1698: invokevirtual 453	com/android/server/pm/PackageVerificationState:setVerifierResponse	(II)Z
            //     1701: pop
            //     1702: aload_3
            //     1703: invokevirtual 456	com/android/server/pm/PackageVerificationState:isVerificationComplete	()Z
            //     1706: ifeq -1622 -> 84
            //     1709: aload_0
            //     1710: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     1713: getfield 177	com/android/server/pm/PackageManagerService:mPendingVerification	Landroid/util/SparseArray;
            //     1716: iload_2
            //     1717: invokevirtual 433	android/util/SparseArray:remove	(I)V
            //     1720: aload_3
            //     1721: invokevirtual 422	com/android/server/pm/PackageVerificationState:getInstallArgs	()Lcom/android/server/pm/PackageManagerService$InstallArgs;
            //     1724: astore 6
            //     1726: aload_3
            //     1727: invokevirtual 459	com/android/server/pm/PackageVerificationState:isInstallAllowed	()Z
            //     1730: ifeq +67 -> 1797
            //     1733: bipush 146
            //     1735: istore 7
            //     1737: aload 6
            //     1739: aload_0
            //     1740: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     1743: invokestatic 125	com/android/server/pm/PackageManagerService:access$300	(Lcom/android/server/pm/PackageManagerService;)Lcom/android/internal/app/IMediaContainerService;
            //     1746: iconst_1
            //     1747: invokevirtual 463	com/android/server/pm/PackageManagerService$InstallArgs:copyApk	(Lcom/android/internal/app/IMediaContainerService;Z)I
            //     1750: istore 11
            //     1752: iload 11
            //     1754: istore 7
            //     1756: aload_0
            //     1757: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     1760: aload 6
            //     1762: iload 7
            //     1764: invokestatic 437	com/android/server/pm/PackageManagerService:access$700	(Lcom/android/server/pm/PackageManagerService;Lcom/android/server/pm/PackageManagerService$InstallArgs;I)V
            //     1767: aload_0
            //     1768: getfield 18	com/android/server/pm/PackageManagerService$PackageHandler:this$0	Lcom/android/server/pm/PackageManagerService;
            //     1771: getfield 115	com/android/server/pm/PackageManagerService:mHandler	Lcom/android/server/pm/PackageManagerService$PackageHandler;
            //     1774: bipush 6
            //     1776: invokevirtual 119	com/android/server/pm/PackageManagerService$PackageHandler:sendEmptyMessage	(I)Z
            //     1779: pop
            //     1780: goto -1696 -> 84
            //     1783: astore 9
            //     1785: ldc 96
            //     1787: ldc_w 465
            //     1790: invokestatic 104	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     1793: pop
            //     1794: goto -38 -> 1756
            //     1797: bipush 234
            //     1799: istore 7
            //     1801: goto -45 -> 1756
            //     1804: bipush 255
            //     1806: istore 57
            //     1808: goto -1070 -> 738
            //
            // Exception table:
            //     from	to	target	type
            //     555	576	571	finally
            //     579	768	571	finally
            //     840	877	892	finally
            //     894	897	892	finally
            //     1159	1179	1182	android/os/RemoteException
            //     1135	1151	1202	finally
            //     1204	1207	1202	finally
            //     1305	1313	1316	android/os/RemoteException
            //     1358	1393	1401	finally
            //     1403	1406	1401	finally
            //     1425	1488	1483	finally
            //     1491	1504	1483	finally
            //     1737	1752	1783	android/os/RemoteException
        }

        @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
        public void handleMessage(Message paramMessage)
        {
            try
            {
                PackageManagerService.Injector.doHandleMessage(PackageManagerService.this, this, paramMessage);
                return;
            }
            finally
            {
                Process.setThreadPriority(10);
            }
        }
    }

    class PostInstallData
    {
        public PackageManagerService.InstallArgs args;
        public PackageManagerService.PackageInstalledInfo res;

        PostInstallData(PackageManagerService.InstallArgs paramPackageInstalledInfo, PackageManagerService.PackageInstalledInfo arg3)
        {
            this.args = paramPackageInstalledInfo;
            Object localObject;
            this.res = localObject;
        }
    }

    class DefaultContainerConnection
        implements ServiceConnection
    {
        DefaultContainerConnection()
        {
        }

        public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
        {
            IMediaContainerService localIMediaContainerService = IMediaContainerService.Stub.asInterface(paramIBinder);
            PackageManagerService.this.mHandler.sendMessage(PackageManagerService.this.mHandler.obtainMessage(3, localIMediaContainerService));
        }

        public void onServiceDisconnected(ComponentName paramComponentName)
        {
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static void addMiuiSharedUids(PackageManagerService paramPackageManagerService)
        {
            MiuiSharedUids.add(paramPackageManagerService.mSettings, true);
        }

        static boolean addPackageToSlice(ParceledListSlice<PackageInfo> paramParceledListSlice, PackageInfo paramPackageInfo, int paramInt)
        {
            if ((0x20000 & paramInt) != 0)
            {
                if ((paramPackageInfo.activities != null) && (paramPackageInfo.activities.length > 0))
                    paramPackageInfo.activities = null;
            }
            else
            {
                if ((0x40000 & paramInt) != 0)
                {
                    if (((paramPackageInfo.activities == null) || (paramPackageInfo.activities.length <= 0)) && ((paramPackageInfo.services == null) || (paramPackageInfo.services.length <= 0)))
                        break label91;
                    paramPackageInfo.activities = null;
                    paramPackageInfo.services = null;
                }
                label74: if (paramPackageInfo == null)
                    break label96;
            }
            label91: label96: for (boolean bool = paramParceledListSlice.append(paramPackageInfo); ; bool = false)
            {
                return bool;
                paramPackageInfo = null;
                break;
                paramPackageInfo = null;
                break label74;
            }
        }

        static boolean checkApk(PackageManagerService paramPackageManagerService, Message paramMessage)
        {
            PackageManagerService.HandlerParams localHandlerParams = (PackageManagerService.HandlerParams)paramMessage.obj;
            PackageManagerService.InstallParams localInstallParams;
            if ((localHandlerParams instanceof PackageManagerService.InstallParams))
            {
                localInstallParams = (PackageManagerService.InstallParams)localHandlerParams;
                if ((!ExtraGuard.checkApk(paramPackageManagerService.mContext, localInstallParams.getPackageUri())) && (localInstallParams.observer == null));
            }
            try
            {
                localInstallParams.observer.packageInstalled(null, -22);
                label57: for (boolean bool = false; ; bool = true)
                    return bool;
            }
            catch (RemoteException localRemoteException)
            {
                break label57;
            }
        }

        static void doHandleMessage(PackageManagerService paramPackageManagerService, PackageManagerService.PackageHandler paramPackageHandler, Message paramMessage)
        {
            if ((paramMessage.what == 5) && (!checkApk(paramPackageManagerService, paramMessage)));
            while (true)
            {
                return;
                paramPackageHandler.doHandleMessage(paramMessage);
            }
        }

        static void ignoreMiuiFrameworkRes(PackageManagerService paramPackageManagerService, HashSet<String> paramHashSet)
        {
            paramHashSet.add(paramPackageManagerService.mFrameworkDir.getPath() + "/framework-miui-res.apk");
        }

        // ERROR //
        static boolean setAccessControl(PackageManagerService paramPackageManagerService, String paramString, int paramInt1, int paramInt2)
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 131	com/android/server/pm/PackageManagerService:mPackages	Ljava/util/HashMap;
            //     4: astore 4
            //     6: aload_0
            //     7: getfield 21	com/android/server/pm/PackageManagerService:mSettings	Lcom/android/server/pm/Settings;
            //     10: astore 5
            //     12: aload 4
            //     14: monitorenter
            //     15: iload_2
            //     16: ldc 132
            //     18: if_icmpeq +12 -> 30
            //     21: iconst_0
            //     22: istore 9
            //     24: aload 4
            //     26: monitorexit
            //     27: goto +135 -> 162
            //     30: aload 4
            //     32: aload_1
            //     33: invokevirtual 138	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
            //     36: checkcast 140	android/content/pm/PackageParser$Package
            //     39: astore 7
            //     41: aload 5
            //     43: getfield 143	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
            //     46: aload_1
            //     47: invokevirtual 138	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
            //     50: checkcast 145	com/android/server/pm/PackageSetting
            //     53: astore 8
            //     55: aload 7
            //     57: ifnull +52 -> 109
            //     60: aload 8
            //     62: ifnull +47 -> 109
            //     65: iload_3
            //     66: ldc 132
            //     68: if_icmpne +58 -> 126
            //     71: aload 8
            //     73: ldc 132
            //     75: aload 8
            //     77: getfield 150	com/android/server/pm/GrantedPermissions:pkgFlags	I
            //     80: ior
            //     81: putfield 150	com/android/server/pm/GrantedPermissions:pkgFlags	I
            //     84: aload 7
            //     86: getfield 154	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
            //     89: astore 11
            //     91: aload 11
            //     93: ldc 132
            //     95: aload 11
            //     97: getfield 159	android/content/pm/ApplicationInfo:flags	I
            //     100: ior
            //     101: putfield 159	android/content/pm/ApplicationInfo:flags	I
            //     104: aload 5
            //     106: invokevirtual 162	com/android/server/pm/Settings:writeLPr	()V
            //     109: iconst_1
            //     110: istore 9
            //     112: aload 4
            //     114: monitorexit
            //     115: goto +47 -> 162
            //     118: astore 6
            //     120: aload 4
            //     122: monitorexit
            //     123: aload 6
            //     125: athrow
            //     126: aload 8
            //     128: ldc 163
            //     130: aload 8
            //     132: getfield 150	com/android/server/pm/GrantedPermissions:pkgFlags	I
            //     135: iand
            //     136: putfield 150	com/android/server/pm/GrantedPermissions:pkgFlags	I
            //     139: aload 7
            //     141: getfield 154	android/content/pm/PackageParser$Package:applicationInfo	Landroid/content/pm/ApplicationInfo;
            //     144: astore 10
            //     146: aload 10
            //     148: ldc 163
            //     150: aload 10
            //     152: getfield 159	android/content/pm/ApplicationInfo:flags	I
            //     155: iand
            //     156: putfield 159	android/content/pm/ApplicationInfo:flags	I
            //     159: goto -55 -> 104
            //     162: iload 9
            //     164: ireturn
            //
            // Exception table:
            //     from	to	target	type
            //     24	123	118	finally
            //     126	159	118	finally
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.pm.PackageManagerService
 * JD-Core Version:        0.6.2
 */