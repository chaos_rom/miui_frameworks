package com.android.server.pm;

import android.content.pm.UserInfo;
import android.os.Environment;
import android.os.FileUtils;
import android.os.UserId;
import android.util.SparseArray;
import com.android.internal.util.ArrayUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class UserManager
{
    private static final String ATTR_FLAGS = "flags";
    private static final String ATTR_ID = "id";
    private static final String LOG_TAG = "UserManager";
    private static final String TAG_NAME = "name";
    private static final String TAG_USER = "user";
    private static final String TAG_USERS = "users";
    private static final String USER_INFO_DIR = "system" + File.separator + "users";
    private static final String USER_LIST_FILENAME = "userlist.xml";
    private File mBaseUserPath;
    private Installer mInstaller;
    private int[] mUserIds;
    private final File mUserListFile;
    private SparseArray<UserInfo> mUsers = new SparseArray();
    private final File mUsersDir;

    public UserManager(Installer paramInstaller, File paramFile)
    {
        this(Environment.getDataDirectory(), paramFile);
        this.mInstaller = paramInstaller;
    }

    UserManager(File paramFile1, File paramFile2)
    {
        this.mUsersDir = new File(paramFile1, USER_INFO_DIR);
        this.mUsersDir.mkdirs();
        new File(this.mUsersDir, "0").mkdirs();
        this.mBaseUserPath = paramFile2;
        FileUtils.setPermissions(this.mUsersDir.toString(), 509, -1, -1);
        this.mUserListFile = new File(this.mUsersDir, "userlist.xml");
        readUserList();
    }

    private boolean createPackageFolders(int paramInt, File paramFile)
    {
        if (this.mInstaller == null);
        while (true)
        {
            return true;
            paramFile.mkdir();
            FileUtils.setPermissions(paramFile.toString(), 505, -1, -1);
            this.mInstaller.cloneUserData(0, paramInt, false);
        }
    }

    private void fallbackToSingleUserLocked()
    {
        UserInfo localUserInfo = new UserInfo(0, "Primary", 3);
        this.mUsers.put(0, localUserInfo);
        updateUserIdsLocked();
        writeUserListLocked();
        writeUserLocked(localUserInfo);
    }

    private int getNextAvailableId()
    {
        for (int i = 0; ; i++)
            if ((i >= 2147483647) || (this.mUsers.indexOfKey(i) < 0))
                return i;
    }

    // ERROR //
    private UserInfo readUser(int paramInt)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore_2
        //     2: aconst_null
        //     3: astore_3
        //     4: aconst_null
        //     5: astore 4
        //     7: new 153	java/io/FileInputStream
        //     10: dup
        //     11: new 53	java/io/File
        //     14: dup
        //     15: aload_0
        //     16: getfield 85	com/android/server/pm/UserManager:mUsersDir	Ljava/io/File;
        //     19: new 42	java/lang/StringBuilder
        //     22: dup
        //     23: invokespecial 45	java/lang/StringBuilder:<init>	()V
        //     26: iload_1
        //     27: invokestatic 158	java/lang/Integer:toString	(I)Ljava/lang/String;
        //     30: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     33: ldc 160
        //     35: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     38: invokevirtual 60	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     41: invokespecial 83	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     44: invokespecial 163	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //     47: astore 5
        //     49: invokestatic 169	android/util/Xml:newPullParser	()Lorg/xmlpull/v1/XmlPullParser;
        //     52: astore 12
        //     54: aload 12
        //     56: aload 5
        //     58: aconst_null
        //     59: invokeinterface 175 3 0
        //     64: aload 12
        //     66: invokeinterface 178 1 0
        //     71: istore 13
        //     73: iload 13
        //     75: iconst_2
        //     76: if_icmpeq +9 -> 85
        //     79: iload 13
        //     81: iconst_1
        //     82: if_icmpne -18 -> 64
        //     85: iload 13
        //     87: iconst_2
        //     88: if_icmpeq +44 -> 132
        //     91: ldc 14
        //     93: new 42	java/lang/StringBuilder
        //     96: dup
        //     97: invokespecial 45	java/lang/StringBuilder:<init>	()V
        //     100: ldc 180
        //     102: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     105: iload_1
        //     106: invokevirtual 183	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     109: invokevirtual 60	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     112: invokestatic 189	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     115: pop
        //     116: aconst_null
        //     117: astore 7
        //     119: aload 5
        //     121: ifnull +8 -> 129
        //     124: aload 5
        //     126: invokevirtual 192	java/io/FileInputStream:close	()V
        //     129: aload 7
        //     131: areturn
        //     132: iload 13
        //     134: iconst_2
        //     135: if_icmpne +134 -> 269
        //     138: aload 12
        //     140: invokeinterface 195 1 0
        //     145: ldc 20
        //     147: invokevirtual 201	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     150: ifeq +119 -> 269
        //     153: aload 12
        //     155: aconst_null
        //     156: ldc 11
        //     158: invokeinterface 205 3 0
        //     163: invokestatic 209	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     166: iload_1
        //     167: if_icmpeq +27 -> 194
        //     170: ldc 14
        //     172: ldc 211
        //     174: invokestatic 189	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     177: pop
        //     178: aconst_null
        //     179: astore 7
        //     181: aload 5
        //     183: ifnull +8 -> 191
        //     186: aload 5
        //     188: invokevirtual 192	java/io/FileInputStream:close	()V
        //     191: goto -62 -> 129
        //     194: aload 12
        //     196: aconst_null
        //     197: ldc 8
        //     199: invokeinterface 205 3 0
        //     204: invokestatic 209	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     207: istore_2
        //     208: aload 12
        //     210: invokeinterface 178 1 0
        //     215: istore 15
        //     217: iload 15
        //     219: iconst_2
        //     220: if_icmpeq +9 -> 229
        //     223: iload 15
        //     225: iconst_1
        //     226: if_icmpne -18 -> 208
        //     229: iload 15
        //     231: iconst_2
        //     232: if_icmpne +37 -> 269
        //     235: aload 12
        //     237: invokeinterface 195 1 0
        //     242: ldc 17
        //     244: invokevirtual 201	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     247: ifeq +22 -> 269
        //     250: aload 12
        //     252: invokeinterface 178 1 0
        //     257: iconst_4
        //     258: if_icmpne +11 -> 269
        //     261: aload 12
        //     263: invokeinterface 214 1 0
        //     268: astore_3
        //     269: new 119	android/content/pm/UserInfo
        //     272: dup
        //     273: iload_1
        //     274: aload_3
        //     275: iload_2
        //     276: invokespecial 124	android/content/pm/UserInfo:<init>	(ILjava/lang/String;I)V
        //     279: astore 7
        //     281: aload 5
        //     283: ifnull +8 -> 291
        //     286: aload 5
        //     288: invokevirtual 192	java/io/FileInputStream:close	()V
        //     291: goto -162 -> 129
        //     294: astore 10
        //     296: aload 4
        //     298: ifnull +8 -> 306
        //     301: aload 4
        //     303: invokevirtual 192	java/io/FileInputStream:close	()V
        //     306: aload 10
        //     308: athrow
        //     309: astore 21
        //     311: aload 4
        //     313: ifnull +71 -> 384
        //     316: aload 4
        //     318: invokevirtual 192	java/io/FileInputStream:close	()V
        //     321: goto +63 -> 384
        //     324: aload 4
        //     326: ifnull +58 -> 384
        //     329: aload 4
        //     331: invokevirtual 192	java/io/FileInputStream:close	()V
        //     334: goto +50 -> 384
        //     337: astore 11
        //     339: goto -33 -> 306
        //     342: astore 19
        //     344: goto -215 -> 129
        //     347: astore 17
        //     349: goto -158 -> 191
        //     352: astore 14
        //     354: goto -63 -> 291
        //     357: astore 10
        //     359: aload 5
        //     361: astore 4
        //     363: goto -67 -> 296
        //     366: astore 9
        //     368: aload 5
        //     370: astore 4
        //     372: goto -48 -> 324
        //     375: astore 6
        //     377: aload 5
        //     379: astore 4
        //     381: goto -70 -> 311
        //     384: aconst_null
        //     385: astore 7
        //     387: goto -258 -> 129
        //     390: astore 8
        //     392: goto -8 -> 384
        //     395: astore 20
        //     397: goto -73 -> 324
        //
        // Exception table:
        //     from	to	target	type
        //     7	49	294	finally
        //     7	49	309	java/io/IOException
        //     301	306	337	java/io/IOException
        //     124	129	342	java/io/IOException
        //     186	191	347	java/io/IOException
        //     286	291	352	java/io/IOException
        //     49	116	357	finally
        //     138	178	357	finally
        //     194	281	357	finally
        //     49	116	366	org/xmlpull/v1/XmlPullParserException
        //     138	178	366	org/xmlpull/v1/XmlPullParserException
        //     194	281	366	org/xmlpull/v1/XmlPullParserException
        //     49	116	375	java/io/IOException
        //     138	178	375	java/io/IOException
        //     194	281	375	java/io/IOException
        //     316	334	390	java/io/IOException
        //     7	49	395	org/xmlpull/v1/XmlPullParserException
    }

    private void readUserList()
    {
        synchronized (this.mUsers)
        {
            readUserListLocked();
            return;
        }
    }

    // ERROR //
    private void readUserListLocked()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 102	com/android/server/pm/UserManager:mUserListFile	Ljava/io/File;
        //     4: invokevirtual 220	java/io/File:exists	()Z
        //     7: ifne +8 -> 15
        //     10: aload_0
        //     11: invokespecial 222	com/android/server/pm/UserManager:fallbackToSingleUserLocked	()V
        //     14: return
        //     15: aconst_null
        //     16: astore_1
        //     17: new 153	java/io/FileInputStream
        //     20: dup
        //     21: aload_0
        //     22: getfield 102	com/android/server/pm/UserManager:mUserListFile	Ljava/io/File;
        //     25: invokespecial 163	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //     28: astore_2
        //     29: invokestatic 169	android/util/Xml:newPullParser	()Lorg/xmlpull/v1/XmlPullParser;
        //     32: astore 8
        //     34: aload 8
        //     36: aload_2
        //     37: aconst_null
        //     38: invokeinterface 175 3 0
        //     43: aload 8
        //     45: invokeinterface 178 1 0
        //     50: istore 9
        //     52: iload 9
        //     54: iconst_2
        //     55: if_icmpeq +9 -> 64
        //     58: iload 9
        //     60: iconst_1
        //     61: if_icmpne -18 -> 43
        //     64: iload 9
        //     66: iconst_2
        //     67: if_icmpeq +31 -> 98
        //     70: ldc 14
        //     72: ldc 224
        //     74: invokestatic 189	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     77: pop
        //     78: aload_0
        //     79: invokespecial 222	com/android/server/pm/UserManager:fallbackToSingleUserLocked	()V
        //     82: aload_2
        //     83: ifnull -69 -> 14
        //     86: aload_2
        //     87: invokevirtual 192	java/io/FileInputStream:close	()V
        //     90: goto -76 -> 14
        //     93: astore 6
        //     95: goto -81 -> 14
        //     98: aload 8
        //     100: invokeinterface 178 1 0
        //     105: istore 10
        //     107: iload 10
        //     109: iconst_1
        //     110: if_icmpeq +84 -> 194
        //     113: iload 10
        //     115: iconst_2
        //     116: if_icmpne -18 -> 98
        //     119: aload 8
        //     121: invokeinterface 195 1 0
        //     126: ldc 20
        //     128: invokevirtual 201	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     131: ifeq -33 -> 98
        //     134: aload_0
        //     135: aload 8
        //     137: aconst_null
        //     138: ldc 11
        //     140: invokeinterface 205 3 0
        //     145: invokestatic 209	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     148: invokespecial 226	com/android/server/pm/UserManager:readUser	(I)Landroid/content/pm/UserInfo;
        //     151: astore 12
        //     153: aload 12
        //     155: ifnull -57 -> 98
        //     158: aload_0
        //     159: getfield 80	com/android/server/pm/UserManager:mUsers	Landroid/util/SparseArray;
        //     162: aload 12
        //     164: getfield 229	android/content/pm/UserInfo:id	I
        //     167: aload 12
        //     169: invokevirtual 128	android/util/SparseArray:put	(ILjava/lang/Object;)V
        //     172: goto -74 -> 98
        //     175: astore 7
        //     177: aload_2
        //     178: astore_1
        //     179: aload_0
        //     180: invokespecial 222	com/android/server/pm/UserManager:fallbackToSingleUserLocked	()V
        //     183: aload_1
        //     184: ifnull -170 -> 14
        //     187: aload_1
        //     188: invokevirtual 192	java/io/FileInputStream:close	()V
        //     191: goto -177 -> 14
        //     194: aload_0
        //     195: invokespecial 131	com/android/server/pm/UserManager:updateUserIdsLocked	()V
        //     198: aload_2
        //     199: ifnull +7 -> 206
        //     202: aload_2
        //     203: invokevirtual 192	java/io/FileInputStream:close	()V
        //     206: goto -192 -> 14
        //     209: astore 15
        //     211: aload_0
        //     212: invokespecial 222	com/android/server/pm/UserManager:fallbackToSingleUserLocked	()V
        //     215: aload_1
        //     216: ifnull -202 -> 14
        //     219: aload_1
        //     220: invokevirtual 192	java/io/FileInputStream:close	()V
        //     223: goto -209 -> 14
        //     226: astore 4
        //     228: aload_1
        //     229: ifnull +7 -> 236
        //     232: aload_1
        //     233: invokevirtual 192	java/io/FileInputStream:close	()V
        //     236: aload 4
        //     238: athrow
        //     239: astore 5
        //     241: goto -5 -> 236
        //     244: astore 11
        //     246: goto -40 -> 206
        //     249: astore 4
        //     251: aload_2
        //     252: astore_1
        //     253: goto -25 -> 228
        //     256: astore_3
        //     257: aload_2
        //     258: astore_1
        //     259: goto -48 -> 211
        //     262: astore 14
        //     264: goto -85 -> 179
        //
        // Exception table:
        //     from	to	target	type
        //     86	90	93	java/io/IOException
        //     187	191	93	java/io/IOException
        //     219	223	93	java/io/IOException
        //     29	82	175	java/io/IOException
        //     98	172	175	java/io/IOException
        //     194	198	175	java/io/IOException
        //     17	29	209	org/xmlpull/v1/XmlPullParserException
        //     17	29	226	finally
        //     179	183	226	finally
        //     211	215	226	finally
        //     232	236	239	java/io/IOException
        //     202	206	244	java/io/IOException
        //     29	82	249	finally
        //     98	172	249	finally
        //     194	198	249	finally
        //     29	82	256	org/xmlpull/v1/XmlPullParserException
        //     98	172	256	org/xmlpull/v1/XmlPullParserException
        //     194	198	256	org/xmlpull/v1/XmlPullParserException
        //     17	29	262	java/io/IOException
    }

    private boolean removeUserLocked(int paramInt)
    {
        if ((UserInfo)this.mUsers.get(paramInt) != null)
        {
            this.mUsers.remove(paramInt);
            new File(this.mUsersDir, paramInt + ".xml").delete();
            writeUserListLocked();
            updateUserIdsLocked();
        }
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void updateUserIdsLocked()
    {
        if ((this.mUserIds == null) || (this.mUserIds.length != this.mUsers.size()))
            this.mUserIds = new int[this.mUsers.size()];
        for (int i = 0; i < this.mUsers.size(); i++)
            this.mUserIds[i] = this.mUsers.keyAt(i);
    }

    // ERROR //
    private void writeUserListLocked()
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_1
        //     2: new 252	java/io/FileOutputStream
        //     5: dup
        //     6: aload_0
        //     7: getfield 102	com/android/server/pm/UserManager:mUserListFile	Ljava/io/File;
        //     10: invokespecial 253	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
        //     13: astore_2
        //     14: new 255	java/io/BufferedOutputStream
        //     17: dup
        //     18: aload_2
        //     19: invokespecial 258	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;)V
        //     22: astore_3
        //     23: new 260	com/android/internal/util/FastXmlSerializer
        //     26: dup
        //     27: invokespecial 261	com/android/internal/util/FastXmlSerializer:<init>	()V
        //     30: astore 4
        //     32: aload 4
        //     34: aload_3
        //     35: ldc_w 263
        //     38: invokeinterface 269 3 0
        //     43: aload 4
        //     45: aconst_null
        //     46: iconst_1
        //     47: invokestatic 275	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
        //     50: invokeinterface 279 3 0
        //     55: aload 4
        //     57: ldc_w 281
        //     60: iconst_1
        //     61: invokeinterface 285 3 0
        //     66: aload 4
        //     68: aconst_null
        //     69: ldc 23
        //     71: invokeinterface 289 3 0
        //     76: pop
        //     77: iconst_0
        //     78: istore 11
        //     80: iload 11
        //     82: aload_0
        //     83: getfield 80	com/android/server/pm/UserManager:mUsers	Landroid/util/SparseArray;
        //     86: invokevirtual 247	android/util/SparseArray:size	()I
        //     89: if_icmpge +64 -> 153
        //     92: aload_0
        //     93: getfield 80	com/android/server/pm/UserManager:mUsers	Landroid/util/SparseArray;
        //     96: iload 11
        //     98: invokevirtual 292	android/util/SparseArray:valueAt	(I)Ljava/lang/Object;
        //     101: checkcast 119	android/content/pm/UserInfo
        //     104: astore 14
        //     106: aload 4
        //     108: aconst_null
        //     109: ldc 20
        //     111: invokeinterface 289 3 0
        //     116: pop
        //     117: aload 4
        //     119: aconst_null
        //     120: ldc 11
        //     122: aload 14
        //     124: getfield 229	android/content/pm/UserInfo:id	I
        //     127: invokestatic 158	java/lang/Integer:toString	(I)Ljava/lang/String;
        //     130: invokeinterface 296 4 0
        //     135: pop
        //     136: aload 4
        //     138: aconst_null
        //     139: ldc 20
        //     141: invokeinterface 299 3 0
        //     146: pop
        //     147: iinc 11 1
        //     150: goto -70 -> 80
        //     153: aload 4
        //     155: aconst_null
        //     156: ldc 23
        //     158: invokeinterface 299 3 0
        //     163: pop
        //     164: aload 4
        //     166: invokeinterface 302 1 0
        //     171: aload_2
        //     172: ifnull +7 -> 179
        //     175: aload_2
        //     176: invokevirtual 303	java/io/FileOutputStream:close	()V
        //     179: return
        //     180: astore 18
        //     182: ldc 14
        //     184: ldc_w 305
        //     187: invokestatic 189	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     190: pop
        //     191: aload_1
        //     192: ifnull -13 -> 179
        //     195: aload_1
        //     196: invokevirtual 303	java/io/FileOutputStream:close	()V
        //     199: goto -20 -> 179
        //     202: astore 9
        //     204: goto -25 -> 179
        //     207: astore 6
        //     209: aload_1
        //     210: ifnull +7 -> 217
        //     213: aload_1
        //     214: invokevirtual 303	java/io/FileOutputStream:close	()V
        //     217: aload 6
        //     219: athrow
        //     220: astore 7
        //     222: goto -5 -> 217
        //     225: astore 13
        //     227: goto -48 -> 179
        //     230: astore 6
        //     232: aload_2
        //     233: astore_1
        //     234: goto -25 -> 209
        //     237: astore 5
        //     239: aload_2
        //     240: astore_1
        //     241: goto -59 -> 182
        //
        // Exception table:
        //     from	to	target	type
        //     2	14	180	java/io/IOException
        //     195	199	202	java/io/IOException
        //     2	14	207	finally
        //     182	191	207	finally
        //     213	217	220	java/io/IOException
        //     175	179	225	java/io/IOException
        //     14	171	230	finally
        //     14	171	237	java/io/IOException
    }

    // ERROR //
    private void writeUserLocked(UserInfo paramUserInfo)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: new 252	java/io/FileOutputStream
        //     5: dup
        //     6: new 53	java/io/File
        //     9: dup
        //     10: aload_0
        //     11: getfield 85	com/android/server/pm/UserManager:mUsersDir	Ljava/io/File;
        //     14: new 42	java/lang/StringBuilder
        //     17: dup
        //     18: invokespecial 45	java/lang/StringBuilder:<init>	()V
        //     21: aload_1
        //     22: getfield 229	android/content/pm/UserInfo:id	I
        //     25: invokevirtual 183	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     28: ldc 160
        //     30: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     33: invokevirtual 60	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     36: invokespecial 83	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     39: invokespecial 253	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
        //     42: astore_3
        //     43: new 255	java/io/BufferedOutputStream
        //     46: dup
        //     47: aload_3
        //     48: invokespecial 258	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;)V
        //     51: astore 4
        //     53: new 260	com/android/internal/util/FastXmlSerializer
        //     56: dup
        //     57: invokespecial 261	com/android/internal/util/FastXmlSerializer:<init>	()V
        //     60: astore 5
        //     62: aload 5
        //     64: aload 4
        //     66: ldc_w 263
        //     69: invokeinterface 269 3 0
        //     74: aload 5
        //     76: aconst_null
        //     77: iconst_1
        //     78: invokestatic 275	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
        //     81: invokeinterface 279 3 0
        //     86: aload 5
        //     88: ldc_w 281
        //     91: iconst_1
        //     92: invokeinterface 285 3 0
        //     97: aload 5
        //     99: aconst_null
        //     100: ldc 20
        //     102: invokeinterface 289 3 0
        //     107: pop
        //     108: aload 5
        //     110: aconst_null
        //     111: ldc 11
        //     113: aload_1
        //     114: getfield 229	android/content/pm/UserInfo:id	I
        //     117: invokestatic 158	java/lang/Integer:toString	(I)Ljava/lang/String;
        //     120: invokeinterface 296 4 0
        //     125: pop
        //     126: aload 5
        //     128: aconst_null
        //     129: ldc 8
        //     131: aload_1
        //     132: getfield 307	android/content/pm/UserInfo:flags	I
        //     135: invokestatic 158	java/lang/Integer:toString	(I)Ljava/lang/String;
        //     138: invokeinterface 296 4 0
        //     143: pop
        //     144: aload 5
        //     146: aconst_null
        //     147: ldc 17
        //     149: invokeinterface 289 3 0
        //     154: pop
        //     155: aload 5
        //     157: aload_1
        //     158: getfield 309	android/content/pm/UserInfo:name	Ljava/lang/String;
        //     161: invokeinterface 313 2 0
        //     166: pop
        //     167: aload 5
        //     169: aconst_null
        //     170: ldc 17
        //     172: invokeinterface 299 3 0
        //     177: pop
        //     178: aload 5
        //     180: aconst_null
        //     181: ldc 20
        //     183: invokeinterface 299 3 0
        //     188: pop
        //     189: aload 5
        //     191: invokeinterface 302 1 0
        //     196: aload_3
        //     197: ifnull +7 -> 204
        //     200: aload_3
        //     201: invokevirtual 303	java/io/FileOutputStream:close	()V
        //     204: return
        //     205: astore 6
        //     207: ldc 14
        //     209: new 42	java/lang/StringBuilder
        //     212: dup
        //     213: invokespecial 45	java/lang/StringBuilder:<init>	()V
        //     216: ldc_w 315
        //     219: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     222: aload_1
        //     223: getfield 229	android/content/pm/UserInfo:id	I
        //     226: invokevirtual 183	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     229: ldc_w 317
        //     232: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     235: aload 6
        //     237: invokevirtual 320	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     240: invokevirtual 60	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     243: invokestatic 189	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     246: pop
        //     247: aload_2
        //     248: ifnull -44 -> 204
        //     251: aload_2
        //     252: invokevirtual 303	java/io/FileOutputStream:close	()V
        //     255: goto -51 -> 204
        //     258: astore 10
        //     260: goto -56 -> 204
        //     263: astore 7
        //     265: aload_2
        //     266: ifnull +7 -> 273
        //     269: aload_2
        //     270: invokevirtual 303	java/io/FileOutputStream:close	()V
        //     273: aload 7
        //     275: athrow
        //     276: astore 8
        //     278: goto -5 -> 273
        //     281: astore 18
        //     283: goto -79 -> 204
        //     286: astore 7
        //     288: aload_3
        //     289: astore_2
        //     290: goto -25 -> 265
        //     293: astore 6
        //     295: aload_3
        //     296: astore_2
        //     297: goto -90 -> 207
        //
        // Exception table:
        //     from	to	target	type
        //     2	43	205	java/io/IOException
        //     251	255	258	java/io/IOException
        //     2	43	263	finally
        //     207	247	263	finally
        //     269	273	276	java/io/IOException
        //     200	204	281	java/io/IOException
        //     43	196	286	finally
        //     43	196	293	java/io/IOException
    }

    public void clearUserDataForAllUsers(String paramString)
    {
        int[] arrayOfInt = this.mUserIds;
        int i = arrayOfInt.length;
        int j = 0;
        if (j < i)
        {
            int k = arrayOfInt[j];
            if (k == 0);
            while (true)
            {
                j++;
                break;
                this.mInstaller.clearUserData(paramString, k);
            }
        }
    }

    public UserInfo createUser(String paramString, int paramInt)
    {
        int i = getNextAvailableId();
        UserInfo localUserInfo = new UserInfo(i, paramString, paramInt);
        if (!createPackageFolders(i, new File(this.mBaseUserPath, Integer.toString(i))))
            localUserInfo = null;
        while (true)
        {
            return localUserInfo;
            synchronized (this.mUsers)
            {
                this.mUsers.put(i, localUserInfo);
                writeUserListLocked();
                writeUserLocked(localUserInfo);
                updateUserIdsLocked();
            }
        }
    }

    public boolean exists(int paramInt)
    {
        synchronized (this.mUsers)
        {
            boolean bool = ArrayUtils.contains(this.mUserIds, paramInt);
            return bool;
        }
    }

    public UserInfo getUser(int paramInt)
    {
        synchronized (this.mUsers)
        {
            UserInfo localUserInfo = (UserInfo)this.mUsers.get(paramInt);
            return localUserInfo;
        }
    }

    int[] getUserIds()
    {
        return this.mUserIds;
    }

    public List<UserInfo> getUsers()
    {
        synchronized (this.mUsers)
        {
            ArrayList localArrayList = new ArrayList(this.mUsers.size());
            for (int i = 0; i < this.mUsers.size(); i++)
                localArrayList.add(this.mUsers.valueAt(i));
            return localArrayList;
        }
    }

    public void installPackageForAllUsers(String paramString, int paramInt)
    {
        int[] arrayOfInt = this.mUserIds;
        int i = arrayOfInt.length;
        int j = 0;
        if (j < i)
        {
            int k = arrayOfInt[j];
            if (k == 0);
            while (true)
            {
                j++;
                break;
                this.mInstaller.createUserData(paramString, UserId.getUid(k, paramInt), k);
            }
        }
    }

    boolean removePackageFolders(int paramInt)
    {
        if (this.mInstaller == null);
        while (true)
        {
            return true;
            this.mInstaller.removeUserDataDirs(paramInt);
        }
    }

    public void removePackageForAllUsers(String paramString)
    {
        int[] arrayOfInt = this.mUserIds;
        int i = arrayOfInt.length;
        int j = 0;
        if (j < i)
        {
            int k = arrayOfInt[j];
            if (k == 0);
            while (true)
            {
                j++;
                break;
                this.mInstaller.remove(paramString, k);
            }
        }
    }

    public boolean removeUser(int paramInt)
    {
        synchronized (this.mUsers)
        {
            boolean bool = removeUserLocked(paramInt);
            return bool;
        }
    }

    public void updateUserName(int paramInt, String paramString)
    {
        synchronized (this.mUsers)
        {
            UserInfo localUserInfo = (UserInfo)this.mUsers.get(paramInt);
            if ((paramString != null) && (!paramString.equals(localUserInfo.name)))
            {
                localUserInfo.name = paramString;
                writeUserLocked(localUserInfo);
            }
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.pm.UserManager
 * JD-Core Version:        0.6.2
 */