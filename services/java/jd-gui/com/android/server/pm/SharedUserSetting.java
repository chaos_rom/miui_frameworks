package com.android.server.pm;

import java.util.HashSet;

final class SharedUserSetting extends GrantedPermissions
{
    final String name;
    final HashSet<PackageSetting> packages = new HashSet();
    final PackageSignatures signatures = new PackageSignatures();
    int userId;

    SharedUserSetting(String paramString, int paramInt)
    {
        super(paramInt);
        this.name = paramString;
    }

    public String toString()
    {
        return "SharedUserSetting{" + Integer.toHexString(System.identityHashCode(this)) + " " + this.name + "/" + this.userId + "}";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.pm.SharedUserSetting
 * JD-Core Version:        0.6.2
 */