package com.android.server.pm;

import android.content.pm.PackageParser;
import android.content.pm.PackageParser.Package;
import android.os.FileUtils;
import android.util.Log;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import miui.os.Build;

class ExtraPackageManagerServices
{
    private static final String INSTALL_DIR = "/data/app/";
    private static final String PREINSTALL_HISTORY_FILE = "/data/system/preinstall_history";
    private static final String REINSTALL_MARK_FILE = "reinstall_apps";
    private static final String TAG = "ExtraPackageManagerServices";
    private static final String THIRD_PART_DEV_PREINSTALL_DIR = "/data/preinstall_apps/";
    private static final String XIAOMI_PREINSTALL_DIR = "/data/media/preinstall_apps/";

    private static void deleteOdexFile(String paramString)
    {
        String str = paramString.replace(".apk", ".odex");
        File localFile = new File("/data/app/" + str);
        if (localFile.exists())
            localFile.delete();
    }

    private static void installPreinstallApp(File paramFile)
    {
        String str = paramFile.getName();
        File localFile = new File("/data/app/" + str);
        FileUtils.copyFile(paramFile, localFile);
        localFile.setReadable(true, false);
        deleteOdexFile(str);
    }

    private static final boolean isPackageFilename(String paramString)
    {
        if ((paramString != null) && (paramString.endsWith(".apk")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static void packagePermissionsUpdate(Settings paramSettings, PackageSetting paramPackageSetting, PackageParser.Package paramPackage)
    {
        Object localObject;
        int j;
        label24: BasePermission localBasePermission;
        if (paramPackageSetting.sharedUser != null)
        {
            localObject = paramPackageSetting.sharedUser;
            int i = paramPackage.requestedPermissions.size();
            j = 0;
            if (j >= i)
                return;
            String str1 = (String)paramPackage.requestedPermissions.get(j);
            localBasePermission = (BasePermission)paramSettings.mPermissions.get(str1);
            if (localBasePermission != null)
            {
                String str2 = localBasePermission.name;
                if (((GrantedPermissions)localObject).grantedPermissions.contains(str2))
                    break label120;
                ((GrantedPermissions)localObject).grantedPermissions.add(str2);
                ((GrantedPermissions)localObject).gids = PackageManagerService.appendInts(((GrantedPermissions)localObject).gids, localBasePermission.gids);
            }
        }
        while (true)
        {
            j++;
            break label24;
            localObject = paramPackageSetting;
            break;
            label120: if (!paramPackageSetting.haveGids)
                ((GrantedPermissions)localObject).gids = PackageManagerService.appendInts(((GrantedPermissions)localObject).gids, localBasePermission.gids);
        }
    }

    private static PackageParser.Package parsePackage(File paramFile)
    {
        String str = paramFile.getPath();
        return new PackageParser(str).parsePackage(paramFile, str, null, 4);
    }

    public static void performPreinstallApp(Settings paramSettings)
    {
        String str1;
        File localFile1;
        String[] arrayOfString;
        if (Build.IS_XIAOMI)
        {
            str1 = "/data/media/preinstall_apps/";
            localFile1 = new File(str1);
            arrayOfString = localFile1.list();
            if (arrayOfString != null)
                break label59;
            Log.d("ExtraPackageManagerServices", "No files in preinstall app dir " + localFile1);
        }
        while (true)
        {
            return;
            str1 = "/data/preinstall_apps/";
            break;
            label59: ArrayList localArrayList = readPreinstallAppHistory("/data/system/preinstall_history");
            File localFile2 = new File(str1 + "reinstall_apps");
            boolean bool1 = localFile2.exists();
            int i = 0;
            if (i < arrayOfString.length)
            {
                String str2 = arrayOfString[i];
                if (!isPackageFilename(str2));
                label302: 
                while (true)
                {
                    i++;
                    break;
                    boolean bool2 = localArrayList.contains(str2);
                    File localFile3 = new File(localFile1, str2);
                    PackageParser.Package localPackage = parsePackage(localFile3);
                    if (localPackage == null)
                    {
                        Log.d("ExtraPackageManagerServices", "preinstall app " + str2 + " package parser fail!");
                    }
                    else
                    {
                        PackageSetting localPackageSetting = paramSettings.peekPackageLPr(localPackage.packageName);
                        if (localPackageSetting != null)
                            if (localPackage.mVersionCode > localPackageSetting.versionCode)
                            {
                                if ((0x1 & localPackageSetting.pkgFlags) == 0)
                                {
                                    File localFile4 = localPackageSetting.codePath;
                                    deleteOdexFile(localFile4.getName());
                                    localFile4.delete();
                                }
                                installPreinstallApp(localFile3);
                                packagePermissionsUpdate(paramSettings, localPackageSetting, localPackage);
                            }
                        while (true)
                        {
                            if (bool2)
                                break label302;
                            writePreinstallAppHistory("/data/system/preinstall_history", str2);
                            break;
                            if ((!bool2) || (bool1))
                                installPreinstallApp(localFile3);
                        }
                    }
                }
            }
            if (bool1)
                localFile2.delete();
        }
    }

    private static ArrayList<String> readPreinstallAppHistory(String paramString)
    {
        ArrayList localArrayList = new ArrayList();
        try
        {
            File localFile = new File(paramString);
            if (localFile.exists())
            {
                FileReader localFileReader = new FileReader(localFile);
                BufferedReader localBufferedReader = new BufferedReader(localFileReader);
                while (true)
                {
                    String str = localBufferedReader.readLine();
                    if (str == null)
                        break;
                    localArrayList.add(str);
                }
                localBufferedReader.close();
                localFileReader.close();
            }
            label80: return localArrayList;
        }
        catch (IOException localIOException)
        {
            break label80;
        }
    }

    private static void writePreinstallAppHistory(String paramString1, String paramString2)
    {
        try
        {
            File localFile = new File(paramString1);
            if (!localFile.exists())
                localFile.createNewFile();
            FileWriter localFileWriter = new FileWriter(localFile, true);
            BufferedWriter localBufferedWriter = new BufferedWriter(localFileWriter);
            localBufferedWriter.write(paramString2);
            localBufferedWriter.write("\n");
            localBufferedWriter.close();
            localFileWriter.close();
            label67: return;
        }
        catch (IOException localIOException)
        {
            break label67;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.pm.ExtraPackageManagerServices
 * JD-Core Version:        0.6.2
 */