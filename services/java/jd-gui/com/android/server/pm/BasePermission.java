package com.android.server.pm;

import android.content.pm.PackageParser.Permission;
import android.content.pm.PermissionInfo;

final class BasePermission
{
    static final int TYPE_BUILTIN = 1;
    static final int TYPE_DYNAMIC = 2;
    static final int TYPE_NORMAL;
    int[] gids;
    final String name;
    PackageSettingBase packageSetting;
    PermissionInfo pendingInfo;
    PackageParser.Permission perm;
    int protectionLevel;
    String sourcePackage;
    final int type;
    int uid;

    BasePermission(String paramString1, String paramString2, int paramInt)
    {
        this.name = paramString1;
        this.sourcePackage = paramString2;
        this.type = paramInt;
        this.protectionLevel = 2;
    }

    public String toString()
    {
        return "BasePermission{" + Integer.toHexString(System.identityHashCode(this)) + " " + this.name + "}";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.pm.BasePermission
 * JD-Core Version:        0.6.2
 */