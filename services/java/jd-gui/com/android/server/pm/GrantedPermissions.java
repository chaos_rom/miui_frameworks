package com.android.server.pm;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import java.util.HashSet;

class GrantedPermissions
{
    int[] gids;
    HashSet<String> grantedPermissions = new HashSet();
    int pkgFlags;

    GrantedPermissions(int paramInt)
    {
        setFlags(paramInt);
    }

    GrantedPermissions(GrantedPermissions paramGrantedPermissions)
    {
        this.pkgFlags = paramGrantedPermissions.pkgFlags;
        this.grantedPermissions = ((HashSet)paramGrantedPermissions.grantedPermissions.clone());
        if (paramGrantedPermissions.gids != null)
            this.gids = ((int[])paramGrantedPermissions.gids.clone());
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    void setFlags(int paramInt)
    {
        this.pkgFlags = (0xA0040001 & paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.pm.GrantedPermissions
 * JD-Core Version:        0.6.2
 */