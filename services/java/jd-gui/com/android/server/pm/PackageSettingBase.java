package com.android.server.pm;

import android.util.SparseArray;
import android.util.SparseIntArray;
import java.io.File;
import java.util.HashSet;

class PackageSettingBase extends GrantedPermissions
{
    static final int PKG_INSTALL_COMPLETE = 1;
    static final int PKG_INSTALL_INCOMPLETE;
    File codePath;
    String codePathString;
    private SparseArray<HashSet<String>> disabledComponents = new SparseArray();
    private SparseIntArray enabled = new SparseIntArray();
    private SparseArray<HashSet<String>> enabledComponents = new SparseArray();
    long firstInstallTime;
    boolean haveGids;
    int installStatus = 1;
    String installerPackageName;
    long lastUpdateTime;
    final String name;
    String nativeLibraryPathString;
    private SparseArray<Boolean> notLaunched = new SparseArray();
    PackageSettingBase origPackage;
    boolean permissionsFixed;
    final String realName;
    File resourcePath;
    String resourcePathString;
    PackageSignatures signatures = new PackageSignatures();
    private SparseArray<Boolean> stopped = new SparseArray();
    long timeStamp;
    boolean uidError;
    int versionCode;

    PackageSettingBase(PackageSettingBase paramPackageSettingBase)
    {
        super(paramPackageSettingBase);
        this.name = paramPackageSettingBase.name;
        this.realName = paramPackageSettingBase.realName;
        this.codePath = paramPackageSettingBase.codePath;
        this.codePathString = paramPackageSettingBase.codePathString;
        this.resourcePath = paramPackageSettingBase.resourcePath;
        this.resourcePathString = paramPackageSettingBase.resourcePathString;
        this.nativeLibraryPathString = paramPackageSettingBase.nativeLibraryPathString;
        this.timeStamp = paramPackageSettingBase.timeStamp;
        this.firstInstallTime = paramPackageSettingBase.firstInstallTime;
        this.lastUpdateTime = paramPackageSettingBase.lastUpdateTime;
        this.versionCode = paramPackageSettingBase.versionCode;
        this.uidError = paramPackageSettingBase.uidError;
        this.signatures = new PackageSignatures(paramPackageSettingBase.signatures);
        this.permissionsFixed = paramPackageSettingBase.permissionsFixed;
        this.haveGids = paramPackageSettingBase.haveGids;
        this.notLaunched = paramPackageSettingBase.notLaunched;
        this.disabledComponents = paramPackageSettingBase.disabledComponents.clone();
        this.enabledComponents = paramPackageSettingBase.enabledComponents.clone();
        this.enabled = paramPackageSettingBase.enabled.clone();
        this.stopped = paramPackageSettingBase.stopped.clone();
        this.installStatus = paramPackageSettingBase.installStatus;
        this.origPackage = paramPackageSettingBase.origPackage;
        this.installerPackageName = paramPackageSettingBase.installerPackageName;
    }

    PackageSettingBase(String paramString1, String paramString2, File paramFile1, File paramFile2, String paramString3, int paramInt1, int paramInt2)
    {
        super(paramInt2);
        this.name = paramString1;
        this.realName = paramString2;
        init(paramFile1, paramFile2, paramString3, paramInt1);
    }

    private HashSet<String> getComponentHashSet(SparseArray<HashSet<String>> paramSparseArray, int paramInt)
    {
        HashSet localHashSet = (HashSet)paramSparseArray.get(paramInt);
        if (localHashSet == null)
        {
            localHashSet = new HashSet(1);
            paramSparseArray.put(paramInt, localHashSet);
        }
        return localHashSet;
    }

    void addDisabledComponent(String paramString, int paramInt)
    {
        getComponentHashSet(this.disabledComponents, paramInt).add(paramString);
    }

    void addEnabledComponent(String paramString, int paramInt)
    {
        getComponentHashSet(this.enabledComponents, paramInt).add(paramString);
    }

    public void copyFrom(PackageSettingBase paramPackageSettingBase)
    {
        this.grantedPermissions = paramPackageSettingBase.grantedPermissions;
        this.gids = paramPackageSettingBase.gids;
        this.timeStamp = paramPackageSettingBase.timeStamp;
        this.firstInstallTime = paramPackageSettingBase.firstInstallTime;
        this.lastUpdateTime = paramPackageSettingBase.lastUpdateTime;
        this.signatures = paramPackageSettingBase.signatures;
        this.permissionsFixed = paramPackageSettingBase.permissionsFixed;
        this.haveGids = paramPackageSettingBase.haveGids;
        this.stopped = paramPackageSettingBase.stopped;
        this.notLaunched = paramPackageSettingBase.notLaunched;
        this.disabledComponents = paramPackageSettingBase.disabledComponents;
        this.enabledComponents = paramPackageSettingBase.enabledComponents;
        this.enabled = paramPackageSettingBase.enabled;
        this.installStatus = paramPackageSettingBase.installStatus;
    }

    boolean disableComponentLPw(String paramString, int paramInt)
    {
        HashSet localHashSet = getComponentHashSet(this.disabledComponents, paramInt);
        return getComponentHashSet(this.enabledComponents, paramInt).remove(paramString) | localHashSet.add(paramString);
    }

    boolean enableComponentLPw(String paramString, int paramInt)
    {
        HashSet localHashSet1 = getComponentHashSet(this.disabledComponents, paramInt);
        HashSet localHashSet2 = getComponentHashSet(this.enabledComponents, paramInt);
        return localHashSet1.remove(paramString) | localHashSet2.add(paramString);
    }

    int getCurrentEnabledStateLPr(String paramString, int paramInt)
    {
        HashSet localHashSet = getComponentHashSet(this.disabledComponents, paramInt);
        int i;
        if (getComponentHashSet(this.enabledComponents, paramInt).contains(paramString))
            i = 1;
        while (true)
        {
            return i;
            if (localHashSet.contains(paramString))
                i = 2;
            else
                i = 0;
        }
    }

    HashSet<String> getDisabledComponents(int paramInt)
    {
        return getComponentHashSet(this.disabledComponents, paramInt);
    }

    int getEnabled(int paramInt)
    {
        return this.enabled.get(paramInt, 0);
    }

    HashSet<String> getEnabledComponents(int paramInt)
    {
        return getComponentHashSet(this.enabledComponents, paramInt);
    }

    public int getInstallStatus()
    {
        return this.installStatus;
    }

    String getInstallerPackageName()
    {
        return this.installerPackageName;
    }

    boolean getNotLaunched(int paramInt)
    {
        return ((Boolean)this.notLaunched.get(paramInt, Boolean.valueOf(false))).booleanValue();
    }

    boolean getStopped(int paramInt)
    {
        return ((Boolean)this.stopped.get(paramInt, Boolean.valueOf(false))).booleanValue();
    }

    void init(File paramFile1, File paramFile2, String paramString, int paramInt)
    {
        this.codePath = paramFile1;
        this.codePathString = paramFile1.toString();
        this.resourcePath = paramFile2;
        this.resourcePathString = paramFile2.toString();
        this.nativeLibraryPathString = paramString;
        this.versionCode = paramInt;
    }

    void removeUser(int paramInt)
    {
        this.enabled.delete(paramInt);
        this.stopped.delete(paramInt);
        this.enabledComponents.delete(paramInt);
        this.disabledComponents.delete(paramInt);
        this.notLaunched.delete(paramInt);
    }

    boolean restoreComponentLPw(String paramString, int paramInt)
    {
        HashSet localHashSet = getComponentHashSet(this.disabledComponents, paramInt);
        return getComponentHashSet(this.enabledComponents, paramInt).remove(paramString) | localHashSet.remove(paramString);
    }

    void setDisabledComponents(HashSet<String> paramHashSet, int paramInt)
    {
        this.disabledComponents.put(paramInt, paramHashSet);
    }

    void setEnabled(int paramInt1, int paramInt2)
    {
        this.enabled.put(paramInt2, paramInt1);
    }

    void setEnabledComponents(HashSet<String> paramHashSet, int paramInt)
    {
        this.enabledComponents.put(paramInt, paramHashSet);
    }

    public void setInstallStatus(int paramInt)
    {
        this.installStatus = paramInt;
    }

    public void setInstallerPackageName(String paramString)
    {
        this.installerPackageName = paramString;
    }

    void setNotLaunched(boolean paramBoolean, int paramInt)
    {
        this.notLaunched.put(paramInt, Boolean.valueOf(paramBoolean));
    }

    void setStopped(boolean paramBoolean, int paramInt)
    {
        this.stopped.put(paramInt, Boolean.valueOf(paramBoolean));
    }

    public void setTimeStamp(long paramLong)
    {
        this.timeStamp = paramLong;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.pm.PackageSettingBase
 * JD-Core Version:        0.6.2
 */