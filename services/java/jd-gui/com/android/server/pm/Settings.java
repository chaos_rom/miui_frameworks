package com.android.server.pm;

import android.content.ComponentName;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageParser.Package;
import android.content.pm.PackageParser.Permission;
import android.content.pm.PermissionInfo;
import android.content.pm.Signature;
import android.content.pm.UserInfo;
import android.content.pm.VerifierDeviceIdentity;
import android.os.Binder;
import android.os.Environment;
import android.os.FileUtils;
import android.os.UserId;
import android.util.Log;
import android.util.Slog;
import android.util.SparseArray;
import android.util.Xml;
import com.android.internal.util.FastXmlSerializer;
import com.android.internal.util.JournaledFile;
import com.android.internal.util.XmlUtils;
import com.android.server.IntentResolver;
import com.android.server.PreferredComponent;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import libcore.io.IoUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

final class Settings
{
    private static final String ATTR_ENABLED = "enabled";
    private static final String ATTR_ENFORCEMENT = "enforcement";
    private static final String ATTR_NAME = "name";
    private static final String ATTR_NOT_LAUNCHED = "nl";
    private static final String ATTR_STOPPED = "stopped";
    private static final boolean DEBUG_STOPPED = false;
    static final Object[] FLAG_DUMP_SPEC = arrayOfObject;
    private static final String TAG = "PackageSettings";
    private static final String TAG_DISABLED_COMPONENTS = "disabled-components";
    private static final String TAG_ENABLED_COMPONENTS = "enabled-components";
    private static final String TAG_ITEM = "item";
    private static final String TAG_PACKAGE = "pkg";
    private static final String TAG_PACKAGE_RESTRICTIONS = "package-restrictions";
    private static final String TAG_READ_EXTERNAL_STORAGE = "read-external-storage";
    private final File mBackupSettingsFilename;
    private final File mBackupStoppedPackagesFilename;
    private final HashMap<String, PackageSetting> mDisabledSysPackages = new HashMap();
    int mExternalSdkPlatform;
    int mInternalSdkPlatform;
    private final SparseArray<Object> mOtherUserIds = new SparseArray();
    private final File mPackageListFilename;
    final HashMap<String, PackageSetting> mPackages = new HashMap();
    final ArrayList<String> mPackagesToBeCleaned = new ArrayList();
    private final ArrayList<Signature> mPastSignatures = new ArrayList();
    private final ArrayList<PendingPackage> mPendingPackages = new ArrayList();
    final HashMap<String, BasePermission> mPermissionTrees = new HashMap();
    final HashMap<String, BasePermission> mPermissions = new HashMap();
    final IntentResolver<PreferredActivity, PreferredActivity> mPreferredActivities = new IntentResolver()
    {
        protected void dumpFilter(PrintWriter paramAnonymousPrintWriter, String paramAnonymousString, PreferredActivity paramAnonymousPreferredActivity)
        {
            paramAnonymousPreferredActivity.mPref.dump(paramAnonymousPrintWriter, paramAnonymousString, paramAnonymousPreferredActivity);
        }

        protected String packageForFilter(PreferredActivity paramAnonymousPreferredActivity)
        {
            return paramAnonymousPreferredActivity.mPref.mComponent.getPackageName();
        }
    };
    Boolean mReadExternalStorageEnforced;
    final StringBuilder mReadMessages = new StringBuilder();
    final HashMap<String, String> mRenamedPackages = new HashMap();
    private final File mSettingsFilename;
    final HashMap<String, SharedUserSetting> mSharedUsers = new HashMap();
    private final File mStoppedPackagesFilename;
    private final File mSystemDir;
    private final ArrayList<Object> mUserIds = new ArrayList();
    private VerifierDeviceIdentity mVerifierDeviceIdentity;

    static
    {
        Object[] arrayOfObject = new Object[36];
        arrayOfObject[0] = Integer.valueOf(1);
        arrayOfObject[1] = "SYSTEM";
        arrayOfObject[2] = Integer.valueOf(2);
        arrayOfObject[3] = "DEBUGGABLE";
        arrayOfObject[4] = Integer.valueOf(4);
        arrayOfObject[5] = "HAS_CODE";
        arrayOfObject[6] = Integer.valueOf(8);
        arrayOfObject[7] = "PERSISTENT";
        arrayOfObject[8] = Integer.valueOf(16);
        arrayOfObject[9] = "FACTORY_TEST";
        arrayOfObject[10] = Integer.valueOf(32);
        arrayOfObject[11] = "ALLOW_TASK_REPARENTING";
        arrayOfObject[12] = Integer.valueOf(64);
        arrayOfObject[13] = "ALLOW_CLEAR_USER_DATA";
        arrayOfObject[14] = Integer.valueOf(128);
        arrayOfObject[15] = "UPDATED_SYSTEM_APP";
        arrayOfObject[16] = Integer.valueOf(256);
        arrayOfObject[17] = "TEST_ONLY";
        arrayOfObject[18] = Integer.valueOf(16384);
        arrayOfObject[19] = "VM_SAFE_MODE";
        arrayOfObject[20] = Integer.valueOf(32768);
        arrayOfObject[21] = "ALLOW_BACKUP";
        arrayOfObject[22] = Integer.valueOf(65536);
        arrayOfObject[23] = "KILL_AFTER_RESTORE";
        arrayOfObject[24] = Integer.valueOf(131072);
        arrayOfObject[25] = "RESTORE_ANY_VERSION";
        arrayOfObject[26] = Integer.valueOf(262144);
        arrayOfObject[27] = "EXTERNAL_STORAGE";
        arrayOfObject[28] = Integer.valueOf(1048576);
        arrayOfObject[29] = "LARGE_HEAP";
        arrayOfObject[30] = Integer.valueOf(2097152);
        arrayOfObject[31] = "STOPPED";
        arrayOfObject[32] = Integer.valueOf(536870912);
        arrayOfObject[33] = "FORWARD_LOCK";
        arrayOfObject[34] = Integer.valueOf(268435456);
        arrayOfObject[35] = "CANT_SAVE_STATE";
    }

    Settings()
    {
        this(Environment.getDataDirectory());
    }

    Settings(File paramFile)
    {
        this.mSystemDir = new File(paramFile, "system");
        this.mSystemDir.mkdirs();
        FileUtils.setPermissions(this.mSystemDir.toString(), 509, -1, -1);
        this.mSettingsFilename = new File(this.mSystemDir, "packages.xml");
        this.mBackupSettingsFilename = new File(this.mSystemDir, "packages-backup.xml");
        this.mPackageListFilename = new File(this.mSystemDir, "packages.list");
        this.mStoppedPackagesFilename = new File(this.mSystemDir, "packages-stopped.xml");
        this.mBackupStoppedPackagesFilename = new File(this.mSystemDir, "packages-stopped-backup.xml");
    }

    private void addPackageSettingLPw(PackageSetting paramPackageSetting, String paramString, SharedUserSetting paramSharedUserSetting)
    {
        this.mPackages.put(paramString, paramPackageSetting);
        if (paramSharedUserSetting != null)
        {
            if ((paramPackageSetting.sharedUser == null) || (paramPackageSetting.sharedUser == paramSharedUserSetting))
                break label120;
            PackageManagerService.reportSettingsProblem(6, "Package " + paramPackageSetting.name + " was user " + paramPackageSetting.sharedUser + " but is now " + paramSharedUserSetting + "; I am not changing its files so it will probably fail!");
            paramPackageSetting.sharedUser.packages.remove(paramPackageSetting);
        }
        while (true)
        {
            paramSharedUserSetting.packages.add(paramPackageSetting);
            paramPackageSetting.sharedUser = paramSharedUserSetting;
            paramPackageSetting.appId = paramSharedUserSetting.userId;
            return;
            label120: if (paramPackageSetting.appId != paramSharedUserSetting.userId)
                PackageManagerService.reportSettingsProblem(6, "Package " + paramPackageSetting.name + " was user id " + paramPackageSetting.appId + " but is now user " + paramSharedUserSetting + " with id " + paramSharedUserSetting.userId + "; I am not changing its files so it will probably fail!");
        }
    }

    private boolean addUserIdLPw(int paramInt, Object paramObject1, Object paramObject2)
    {
        boolean bool = false;
        if (paramInt > 19999);
        int j;
        while (true)
        {
            return bool;
            if (paramInt < 10000)
                break label125;
            int i = this.mUserIds.size();
            j = paramInt - 10000;
            while (j >= i)
            {
                this.mUserIds.add(null);
                i++;
            }
            if (this.mUserIds.get(j) == null)
                break;
            PackageManagerService.reportSettingsProblem(6, "Adding duplicate user id: " + paramInt + " name=" + paramObject2);
        }
        this.mUserIds.set(j, paramObject1);
        while (true)
        {
            bool = true;
            break;
            label125: if (this.mOtherUserIds.get(paramInt) != null)
            {
                PackageManagerService.reportSettingsProblem(6, "Adding duplicate shared id: " + paramInt + " name=" + paramObject2);
                break;
            }
            this.mOtherUserIds.put(paramInt, paramObject1);
        }
    }

    // ERROR //
    private List<UserInfo> getAllUsers()
    {
        // Byte code:
        //     0: invokestatic 345	android/os/Binder:clearCallingIdentity	()J
        //     3: lstore_1
        //     4: invokestatic 351	android/app/AppGlobals:getPackageManager	()Landroid/content/pm/IPackageManager;
        //     7: astore 7
        //     9: aload 7
        //     11: invokeinterface 356 1 0
        //     16: astore 9
        //     18: aload 9
        //     20: astore 4
        //     22: lload_1
        //     23: invokestatic 360	android/os/Binder:restoreCallingIdentity	(J)V
        //     26: aload 4
        //     28: areturn
        //     29: astore 6
        //     31: lload_1
        //     32: invokestatic 360	android/os/Binder:restoreCallingIdentity	(J)V
        //     35: aload 6
        //     37: athrow
        //     38: astore 5
        //     40: lload_1
        //     41: invokestatic 360	android/os/Binder:restoreCallingIdentity	(J)V
        //     44: aconst_null
        //     45: astore 4
        //     47: goto -21 -> 26
        //     50: astore 8
        //     52: goto -12 -> 40
        //     55: astore_3
        //     56: goto -16 -> 40
        //
        // Exception table:
        //     from	to	target	type
        //     4	9	29	finally
        //     9	18	29	finally
        //     4	9	38	java/lang/NullPointerException
        //     9	18	50	java/lang/NullPointerException
        //     4	9	55	android/os/RemoteException
        //     9	18	55	android/os/RemoteException
    }

    private PackageSetting getPackageLPw(String paramString1, PackageSetting paramPackageSetting, String paramString2, SharedUserSetting paramSharedUserSetting, File paramFile1, File paramFile2, String paramString3, int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
    {
        PackageSetting localPackageSetting1 = (PackageSetting)this.mPackages.get(paramString1);
        String str2;
        label138: String str3;
        label165: label188: PackageSetting localPackageSetting2;
        if (localPackageSetting1 != null)
        {
            if (!localPackageSetting1.codePath.equals(paramFile1))
            {
                if ((0x1 & localPackageSetting1.pkgFlags) != 0)
                    Slog.w("PackageManager", "Trying to update system app code path from " + localPackageSetting1.codePathString + " to " + paramFile1.toString());
            }
            else
            {
                if (localPackageSetting1.sharedUser == paramSharedUserSetting)
                    break label287;
                StringBuilder localStringBuilder1 = new StringBuilder().append("Package ").append(paramString1).append(" shared user changed from ");
                if (localPackageSetting1.sharedUser == null)
                    break label271;
                str2 = localPackageSetting1.sharedUser.name;
                StringBuilder localStringBuilder2 = localStringBuilder1.append(str2).append(" to ");
                if (paramSharedUserSetting == null)
                    break label279;
                str3 = paramSharedUserSetting.name;
                PackageManagerService.reportSettingsProblem(5, str3 + "; replacing with new");
                localPackageSetting1 = null;
            }
        }
        else
        {
            if (localPackageSetting1 != null)
                break label811;
            if (paramBoolean1)
                break label309;
            localPackageSetting2 = null;
        }
        while (true)
        {
            return localPackageSetting2;
            Slog.i("PackageManager", "Package " + paramString1 + " codePath changed from " + localPackageSetting1.codePath + " to " + paramFile1 + "; Retaining data and using new");
            localPackageSetting1.nativeLibraryPathString = paramString3;
            break;
            label271: str2 = "<nothing>";
            break label138;
            label279: str3 = "<nothing>";
            break label165;
            label287: if ((paramInt2 & 0x1) == 0)
                break label188;
            localPackageSetting1.pkgFlags = (0x1 | localPackageSetting1.pkgFlags);
            break label188;
            label309: if (paramPackageSetting != null)
            {
                localPackageSetting1 = new PackageSetting(paramPackageSetting.name, paramString1, paramFile1, paramFile2, paramString3, paramInt1, paramInt2);
                PackageSignatures localPackageSignatures = localPackageSetting1.signatures;
                localPackageSetting1.copyFrom(paramPackageSetting);
                localPackageSetting1.signatures = localPackageSignatures;
                localPackageSetting1.sharedUser = paramPackageSetting.sharedUser;
                localPackageSetting1.appId = paramPackageSetting.appId;
                localPackageSetting1.origPackage = paramPackageSetting;
                HashMap localHashMap = this.mRenamedPackages;
                String str1 = paramPackageSetting.name;
                localHashMap.put(paramString1, str1);
                paramString1 = paramPackageSetting.name;
                localPackageSetting1.setTimeStamp(paramFile1.lastModified());
            }
            while (true)
            {
                if (localPackageSetting1.appId >= 0)
                    break label797;
                PackageManagerService.reportSettingsProblem(5, "Package " + paramString1 + " could not be assigned a valid uid");
                localPackageSetting2 = null;
                break;
                localPackageSetting1 = new PackageSetting(paramString1, paramString2, paramFile1, paramFile2, paramString3, paramInt1, paramInt2);
                localPackageSetting1.setTimeStamp(paramFile1.lastModified());
                localPackageSetting1.sharedUser = paramSharedUserSetting;
                if ((paramInt2 & 0x1) == 0)
                {
                    List localList2 = getAllUsers();
                    if (localList2 != null)
                    {
                        Iterator localIterator2 = localList2.iterator();
                        while (localIterator2.hasNext())
                        {
                            UserInfo localUserInfo = (UserInfo)localIterator2.next();
                            localPackageSetting1.setStopped(true, localUserInfo.id);
                            localPackageSetting1.setNotLaunched(true, localUserInfo.id);
                            writePackageRestrictionsLPr(localUserInfo.id);
                        }
                    }
                }
                if (paramSharedUserSetting != null)
                {
                    localPackageSetting1.appId = paramSharedUserSetting.userId;
                }
                else
                {
                    PackageSetting localPackageSetting3 = (PackageSetting)this.mDisabledSysPackages.get(paramString1);
                    if (localPackageSetting3 != null)
                    {
                        if (localPackageSetting3.signatures.mSignatures != null)
                            localPackageSetting1.signatures.mSignatures = ((Signature[])localPackageSetting3.signatures.mSignatures.clone());
                        localPackageSetting1.appId = localPackageSetting3.appId;
                        localPackageSetting1.grantedPermissions = new HashSet(localPackageSetting3.grantedPermissions);
                        List localList1 = getAllUsers();
                        if (localList1 != null)
                        {
                            Iterator localIterator1 = localList1.iterator();
                            while (localIterator1.hasNext())
                            {
                                int i = ((UserInfo)localIterator1.next()).id;
                                localPackageSetting1.setDisabledComponents(new HashSet(localPackageSetting3.getDisabledComponents(i)), i);
                                localPackageSetting1.setEnabledComponents(new HashSet(localPackageSetting3.getEnabledComponents(i)), i);
                            }
                        }
                        addUserIdLPw(localPackageSetting1.appId, localPackageSetting1, paramString1);
                    }
                    else
                    {
                        localPackageSetting1.appId = newUserIdLPw(localPackageSetting1);
                    }
                }
            }
            label797: if (paramBoolean2)
                addPackageSettingLPw(localPackageSetting1, paramString1, paramSharedUserSetting);
            label811: localPackageSetting2 = localPackageSetting1;
        }
    }

    private File getUserPackagesStateBackupFile(int paramInt)
    {
        return new File(this.mSystemDir, "users/" + paramInt + "/package-restrictions-backup.xml");
    }

    private File getUserPackagesStateFile(int paramInt)
    {
        return new File(this.mSystemDir, "users/" + paramInt + "/package-restrictions.xml");
    }

    private int newUserIdLPw(Object paramObject)
    {
        int i = this.mUserIds.size();
        int j = 0;
        int k;
        if (j < i)
            if (this.mUserIds.get(j) == null)
            {
                this.mUserIds.set(j, paramObject);
                k = j + 10000;
            }
        while (true)
        {
            return k;
            j++;
            break;
            if (i > 9999)
            {
                k = -1;
            }
            else
            {
                this.mUserIds.add(paramObject);
                k = i + 10000;
            }
        }
    }

    static final void printFlags(PrintWriter paramPrintWriter, int paramInt, Object[] paramArrayOfObject)
    {
        paramPrintWriter.print("[ ");
        for (int i = 0; i < paramArrayOfObject.length; i += 2)
            if ((paramInt & ((Integer)paramArrayOfObject[i]).intValue()) != 0)
            {
                paramPrintWriter.print(paramArrayOfObject[(i + 1)]);
                paramPrintWriter.print(" ");
            }
        paramPrintWriter.print("]");
    }

    private HashSet<String> readComponentsLPr(XmlPullParser paramXmlPullParser)
        throws IOException, XmlPullParserException
    {
        HashSet localHashSet = new HashSet();
        int i = paramXmlPullParser.getDepth();
        while (true)
        {
            int j = paramXmlPullParser.next();
            if ((j == 1) || ((j == 3) && (paramXmlPullParser.getDepth() <= i)))
                break;
            if ((j != 3) && (j != 4) && (paramXmlPullParser.getName().equals("item")))
            {
                String str = paramXmlPullParser.getAttributeValue(null, "name");
                if (str != null)
                    localHashSet.add(str);
            }
        }
        return localHashSet;
    }

    // ERROR //
    private void readDefaultPreferredAppsLPw()
    {
        // Byte code:
        //     0: new 199	java/io/File
        //     3: dup
        //     4: invokestatic 559	android/os/Environment:getRootDirectory	()Ljava/io/File;
        //     7: ldc_w 561
        //     10: invokespecial 204	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
        //     13: astore_1
        //     14: aload_1
        //     15: invokevirtual 564	java/io/File:exists	()Z
        //     18: ifeq +10 -> 28
        //     21: aload_1
        //     22: invokevirtual 567	java/io/File:isDirectory	()Z
        //     25: ifne +4 -> 29
        //     28: return
        //     29: aload_1
        //     30: invokevirtual 570	java/io/File:canRead	()Z
        //     33: ifne +38 -> 71
        //     36: ldc 30
        //     38: new 192	java/lang/StringBuilder
        //     41: dup
        //     42: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     45: ldc_w 572
        //     48: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     51: aload_1
        //     52: invokevirtual 267	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     55: ldc_w 574
        //     58: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     61: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     64: invokestatic 391	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     67: pop
        //     68: goto -40 -> 28
        //     71: aload_1
        //     72: invokevirtual 578	java/io/File:listFiles	()[Ljava/io/File;
        //     75: astore_2
        //     76: aload_2
        //     77: arraylength
        //     78: istore_3
        //     79: iconst_0
        //     80: istore 4
        //     82: iload 4
        //     84: iload_3
        //     85: if_icmpge -57 -> 28
        //     88: aload_2
        //     89: iload 4
        //     91: aaload
        //     92: astore 5
        //     94: aload 5
        //     96: invokevirtual 581	java/io/File:getPath	()Ljava/lang/String;
        //     99: ldc_w 583
        //     102: invokevirtual 587	java/lang/String:endsWith	(Ljava/lang/String;)Z
        //     105: ifne +52 -> 157
        //     108: ldc 30
        //     110: new 192	java/lang/StringBuilder
        //     113: dup
        //     114: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     117: ldc_w 589
        //     120: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     123: aload 5
        //     125: invokevirtual 267	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     128: ldc_w 591
        //     131: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     134: aload_1
        //     135: invokevirtual 267	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     138: ldc_w 593
        //     141: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     144: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     147: invokestatic 403	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     150: pop
        //     151: iinc 4 1
        //     154: goto -72 -> 82
        //     157: aload 5
        //     159: invokevirtual 570	java/io/File:canRead	()Z
        //     162: ifne +39 -> 201
        //     165: ldc 30
        //     167: new 192	java/lang/StringBuilder
        //     170: dup
        //     171: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     174: ldc_w 595
        //     177: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     180: aload 5
        //     182: invokevirtual 267	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     185: ldc_w 574
        //     188: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     191: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     194: invokestatic 391	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     197: pop
        //     198: goto -47 -> 151
        //     201: aconst_null
        //     202: astore 6
        //     204: new 597	java/io/FileInputStream
        //     207: dup
        //     208: aload 5
        //     210: invokespecial 598	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //     213: astore 7
        //     215: invokestatic 604	android/util/Xml:newPullParser	()Lorg/xmlpull/v1/XmlPullParser;
        //     218: astore 15
        //     220: aload 15
        //     222: aload 7
        //     224: aconst_null
        //     225: invokeinterface 608 3 0
        //     230: aload 15
        //     232: invokeinterface 545 1 0
        //     237: istore 16
        //     239: iload 16
        //     241: iconst_2
        //     242: if_icmpeq +9 -> 251
        //     245: iload 16
        //     247: iconst_1
        //     248: if_icmpne -18 -> 230
        //     251: iload 16
        //     253: iconst_2
        //     254: if_icmpeq +54 -> 308
        //     257: ldc 30
        //     259: new 192	java/lang/StringBuilder
        //     262: dup
        //     263: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     266: ldc_w 595
        //     269: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     272: aload 5
        //     274: invokevirtual 267	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     277: ldc_w 610
        //     280: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     283: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     286: invokestatic 391	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     289: pop
        //     290: aload 7
        //     292: ifnull -141 -> 151
        //     295: aload 7
        //     297: invokevirtual 613	java/io/FileInputStream:close	()V
        //     300: goto -149 -> 151
        //     303: astore 12
        //     305: goto -154 -> 151
        //     308: ldc_w 615
        //     311: aload 15
        //     313: invokeinterface 548 1 0
        //     318: invokevirtual 551	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     321: ifne +49 -> 370
        //     324: ldc 30
        //     326: new 192	java/lang/StringBuilder
        //     329: dup
        //     330: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     333: ldc_w 595
        //     336: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     339: aload 5
        //     341: invokevirtual 267	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     344: ldc_w 617
        //     347: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     350: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     353: invokestatic 391	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     356: pop
        //     357: aload 7
        //     359: ifnull -208 -> 151
        //     362: aload 7
        //     364: invokevirtual 613	java/io/FileInputStream:close	()V
        //     367: goto -216 -> 151
        //     370: aload_0
        //     371: aload 15
        //     373: invokespecial 621	com/android/server/pm/Settings:readPreferredActivitiesLPw	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     376: aload 7
        //     378: ifnull -227 -> 151
        //     381: aload 7
        //     383: invokevirtual 613	java/io/FileInputStream:close	()V
        //     386: goto -235 -> 151
        //     389: astore 8
        //     391: ldc 30
        //     393: new 192	java/lang/StringBuilder
        //     396: dup
        //     397: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     400: ldc_w 623
        //     403: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     406: aload 5
        //     408: invokevirtual 267	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     411: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     414: aload 8
        //     416: invokestatic 626	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     419: pop
        //     420: aload 6
        //     422: ifnull -271 -> 151
        //     425: aload 6
        //     427: invokevirtual 613	java/io/FileInputStream:close	()V
        //     430: goto -279 -> 151
        //     433: astore 13
        //     435: ldc 30
        //     437: new 192	java/lang/StringBuilder
        //     440: dup
        //     441: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     444: ldc_w 623
        //     447: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     450: aload 5
        //     452: invokevirtual 267	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     455: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     458: aload 13
        //     460: invokestatic 626	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     463: pop
        //     464: aload 6
        //     466: ifnull -315 -> 151
        //     469: aload 6
        //     471: invokevirtual 613	java/io/FileInputStream:close	()V
        //     474: goto -323 -> 151
        //     477: astore 9
        //     479: aload 6
        //     481: ifnull +8 -> 489
        //     484: aload 6
        //     486: invokevirtual 613	java/io/FileInputStream:close	()V
        //     489: aload 9
        //     491: athrow
        //     492: astore 10
        //     494: goto -5 -> 489
        //     497: astore 9
        //     499: aload 7
        //     501: astore 6
        //     503: goto -24 -> 479
        //     506: astore 13
        //     508: aload 7
        //     510: astore 6
        //     512: goto -77 -> 435
        //     515: astore 8
        //     517: aload 7
        //     519: astore 6
        //     521: goto -130 -> 391
        //
        // Exception table:
        //     from	to	target	type
        //     295	300	303	java/io/IOException
        //     362	367	303	java/io/IOException
        //     381	386	303	java/io/IOException
        //     425	430	303	java/io/IOException
        //     469	474	303	java/io/IOException
        //     204	215	389	org/xmlpull/v1/XmlPullParserException
        //     204	215	433	java/io/IOException
        //     204	215	477	finally
        //     391	420	477	finally
        //     435	464	477	finally
        //     484	489	492	java/io/IOException
        //     215	290	497	finally
        //     308	357	497	finally
        //     370	376	497	finally
        //     215	290	506	java/io/IOException
        //     308	357	506	java/io/IOException
        //     370	376	506	java/io/IOException
        //     215	290	515	org/xmlpull/v1/XmlPullParserException
        //     308	357	515	org/xmlpull/v1/XmlPullParserException
        //     370	376	515	org/xmlpull/v1/XmlPullParserException
    }

    private void readDisabledComponentsLPw(PackageSettingBase paramPackageSettingBase, XmlPullParser paramXmlPullParser, int paramInt)
        throws IOException, XmlPullParserException
    {
        int i = paramXmlPullParser.getDepth();
        int j;
        do
        {
            j = paramXmlPullParser.next();
            if ((j == 1) || ((j == 3) && (paramXmlPullParser.getDepth() <= i)))
                break;
        }
        while ((j == 3) || (j == 4));
        if (paramXmlPullParser.getName().equals("item"))
        {
            String str = paramXmlPullParser.getAttributeValue(null, "name");
            if (str != null)
                paramPackageSettingBase.addDisabledComponent(str.intern(), paramInt);
        }
        while (true)
        {
            XmlUtils.skipCurrentTag(paramXmlPullParser);
            break;
            PackageManagerService.reportSettingsProblem(5, "Error in package manager settings: <disabled-components> has no name at " + paramXmlPullParser.getPositionDescription());
            continue;
            PackageManagerService.reportSettingsProblem(5, "Unknown element under <disabled-components>: " + paramXmlPullParser.getName());
        }
    }

    private void readDisabledSysPackageLPw(XmlPullParser paramXmlPullParser)
        throws XmlPullParserException, IOException
    {
        String str1 = paramXmlPullParser.getAttributeValue(null, "name");
        String str2 = paramXmlPullParser.getAttributeValue(null, "realName");
        String str3 = paramXmlPullParser.getAttributeValue(null, "codePath");
        String str4 = paramXmlPullParser.getAttributeValue(null, "resourcePath");
        String str5 = paramXmlPullParser.getAttributeValue(null, "nativeLibraryPath");
        if (str4 == null)
            str4 = str3;
        String str6 = paramXmlPullParser.getAttributeValue(null, "version");
        int i = 0;
        if (str6 != null);
        try
        {
            int i2 = Integer.parseInt(str6);
            i = i2;
            int j = 0x0 | 0x1;
            localPackageSetting = new PackageSetting(str1, str2, new File(str3), new File(str4), str5, i, j);
            str7 = paramXmlPullParser.getAttributeValue(null, "ft");
            if (str7 == null);
        }
        catch (NumberFormatException localNumberFormatException5)
        {
            try
            {
                while (true)
                {
                    PackageSetting localPackageSetting;
                    String str7;
                    localPackageSetting.setTimeStamp(Long.parseLong(str7, 16));
                    label166: String str9 = paramXmlPullParser.getAttributeValue(null, "it");
                    if (str9 != null);
                    try
                    {
                        localPackageSetting.firstInstallTime = Long.parseLong(str9, 16);
                        label195: String str10 = paramXmlPullParser.getAttributeValue(null, "ut");
                        if (str10 != null);
                        try
                        {
                            localPackageSetting.lastUpdateTime = Long.parseLong(str10, 16);
                            label224: String str11 = paramXmlPullParser.getAttributeValue(null, "userId");
                            int k;
                            label248: int i1;
                            label287: int m;
                            if (str11 != null)
                            {
                                k = Integer.parseInt(str11);
                                localPackageSetting.appId = k;
                                if (localPackageSetting.appId <= 0)
                                {
                                    String str12 = paramXmlPullParser.getAttributeValue(null, "sharedUserId");
                                    if (str12 == null)
                                        break label414;
                                    i1 = Integer.parseInt(str12);
                                    localPackageSetting.appId = i1;
                                }
                                m = paramXmlPullParser.getDepth();
                            }
                            while (true)
                            {
                                while (true)
                                {
                                    int n = paramXmlPullParser.next();
                                    if ((n == 1) || ((n == 3) && (paramXmlPullParser.getDepth() <= m)))
                                        break label456;
                                    if ((n != 3) && (n != 4))
                                    {
                                        if (!paramXmlPullParser.getName().equals("perms"))
                                            break label420;
                                        readGrantedPermissionsLPw(paramXmlPullParser, localPackageSetting.grantedPermissions);
                                        continue;
                                        String str8 = paramXmlPullParser.getAttributeValue(null, "ts");
                                        if (str8 == null)
                                            break;
                                        try
                                        {
                                            localPackageSetting.setTimeStamp(Long.parseLong(str8));
                                        }
                                        catch (NumberFormatException localNumberFormatException1)
                                        {
                                        }
                                    }
                                }
                                break;
                                k = 0;
                                break label248;
                                label414: i1 = 0;
                                break label287;
                                label420: PackageManagerService.reportSettingsProblem(5, "Unknown element under <updated-package>: " + paramXmlPullParser.getName());
                                XmlUtils.skipCurrentTag(paramXmlPullParser);
                            }
                            label456: this.mDisabledSysPackages.put(str1, localPackageSetting);
                            return;
                            localNumberFormatException5 = localNumberFormatException5;
                        }
                        catch (NumberFormatException localNumberFormatException2)
                        {
                            break label224;
                        }
                    }
                    catch (NumberFormatException localNumberFormatException3)
                    {
                        break label195;
                    }
                }
            }
            catch (NumberFormatException localNumberFormatException4)
            {
                break label166;
            }
        }
    }

    private void readEnabledComponentsLPw(PackageSettingBase paramPackageSettingBase, XmlPullParser paramXmlPullParser, int paramInt)
        throws IOException, XmlPullParserException
    {
        int i = paramXmlPullParser.getDepth();
        int j;
        do
        {
            j = paramXmlPullParser.next();
            if ((j == 1) || ((j == 3) && (paramXmlPullParser.getDepth() <= i)))
                break;
        }
        while ((j == 3) || (j == 4));
        if (paramXmlPullParser.getName().equals("item"))
        {
            String str = paramXmlPullParser.getAttributeValue(null, "name");
            if (str != null)
                paramPackageSettingBase.addEnabledComponent(str.intern(), paramInt);
        }
        while (true)
        {
            XmlUtils.skipCurrentTag(paramXmlPullParser);
            break;
            PackageManagerService.reportSettingsProblem(5, "Error in package manager settings: <enabled-components> has no name at " + paramXmlPullParser.getPositionDescription());
            continue;
            PackageManagerService.reportSettingsProblem(5, "Unknown element under <enabled-components>: " + paramXmlPullParser.getName());
        }
    }

    private void readGrantedPermissionsLPw(XmlPullParser paramXmlPullParser, HashSet<String> paramHashSet)
        throws IOException, XmlPullParserException
    {
        int i = paramXmlPullParser.getDepth();
        int j;
        do
        {
            j = paramXmlPullParser.next();
            if ((j == 1) || ((j == 3) && (paramXmlPullParser.getDepth() <= i)))
                break;
        }
        while ((j == 3) || (j == 4));
        if (paramXmlPullParser.getName().equals("item"))
        {
            String str = paramXmlPullParser.getAttributeValue(null, "name");
            if (str != null)
                paramHashSet.add(str.intern());
        }
        while (true)
        {
            XmlUtils.skipCurrentTag(paramXmlPullParser);
            break;
            PackageManagerService.reportSettingsProblem(5, "Error in package manager settings: <perms> has no name at " + paramXmlPullParser.getPositionDescription());
            continue;
            PackageManagerService.reportSettingsProblem(5, "Unknown element under <perms>: " + paramXmlPullParser.getName());
        }
    }

    private int readInt(XmlPullParser paramXmlPullParser, String paramString1, String paramString2, int paramInt)
    {
        String str = paramXmlPullParser.getAttributeValue(paramString1, paramString2);
        if (str == null);
        while (true)
        {
            return paramInt;
            try
            {
                int i = Integer.parseInt(str);
                paramInt = i;
            }
            catch (NumberFormatException localNumberFormatException)
            {
                PackageManagerService.reportSettingsProblem(5, "Error in package manager settings: attribute " + paramString2 + " has bad integer value " + str + " at " + paramXmlPullParser.getPositionDescription());
            }
        }
    }

    // ERROR //
    private void readPackageLPw(XmlPullParser paramXmlPullParser)
        throws XmlPullParserException, IOException
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: aconst_null
        //     3: astore_3
        //     4: aconst_null
        //     5: astore 4
        //     7: aconst_null
        //     8: astore 5
        //     10: aconst_null
        //     11: astore 6
        //     13: iconst_0
        //     14: istore 7
        //     16: lconst_0
        //     17: lstore 8
        //     19: lconst_0
        //     20: lstore 10
        //     22: lconst_0
        //     23: lstore 12
        //     25: iconst_0
        //     26: istore 14
        //     28: aload_1
        //     29: aconst_null
        //     30: ldc 16
        //     32: invokeinterface 555 3 0
        //     37: astore_2
        //     38: aload_1
        //     39: aconst_null
        //     40: ldc_w 652
        //     43: invokeinterface 555 3 0
        //     48: astore 23
        //     50: aload_1
        //     51: aconst_null
        //     52: ldc_w 685
        //     55: invokeinterface 555 3 0
        //     60: astore_3
        //     61: aload_1
        //     62: aconst_null
        //     63: ldc_w 723
        //     66: invokeinterface 555 3 0
        //     71: astore 6
        //     73: aload_1
        //     74: aconst_null
        //     75: ldc_w 687
        //     78: invokeinterface 555 3 0
        //     83: astore 24
        //     85: aload_1
        //     86: aconst_null
        //     87: ldc_w 653
        //     90: invokeinterface 555 3 0
        //     95: astore 25
        //     97: aload_1
        //     98: aconst_null
        //     99: ldc_w 655
        //     102: invokeinterface 555 3 0
        //     107: astore 26
        //     109: aload_1
        //     110: aconst_null
        //     111: ldc_w 657
        //     114: invokeinterface 555 3 0
        //     119: astore 4
        //     121: aload_1
        //     122: aconst_null
        //     123: ldc_w 659
        //     126: invokeinterface 555 3 0
        //     131: astore 27
        //     133: aload 27
        //     135: ifnull +14 -> 149
        //     138: aload 27
        //     140: invokestatic 663	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     143: istore 55
        //     145: iload 55
        //     147: istore 14
        //     149: aload_1
        //     150: aconst_null
        //     151: ldc_w 725
        //     154: invokeinterface 555 3 0
        //     159: astore 5
        //     161: aload_1
        //     162: aconst_null
        //     163: ldc_w 727
        //     166: invokeinterface 555 3 0
        //     171: astore 28
        //     173: aload 28
        //     175: ifnull +338 -> 513
        //     178: aload 28
        //     180: invokestatic 663	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     183: istore 53
        //     185: iload 53
        //     187: istore 7
        //     189: aload_1
        //     190: aconst_null
        //     191: ldc_w 667
        //     194: invokeinterface 555 3 0
        //     199: astore 30
        //     201: aload 30
        //     203: ifnull +343 -> 546
        //     206: aload 30
        //     208: bipush 16
        //     210: invokestatic 673	java/lang/Long:parseLong	(Ljava/lang/String;I)J
        //     213: lstore 49
        //     215: lload 49
        //     217: lstore 8
        //     219: aload_1
        //     220: aconst_null
        //     221: ldc_w 675
        //     224: invokeinterface 555 3 0
        //     229: astore 33
        //     231: aload 33
        //     233: ifnull +16 -> 249
        //     236: aload 33
        //     238: bipush 16
        //     240: invokestatic 673	java/lang/Long:parseLong	(Ljava/lang/String;I)J
        //     243: lstore 44
        //     245: lload 44
        //     247: lstore 10
        //     249: aload_1
        //     250: aconst_null
        //     251: ldc_w 681
        //     254: invokeinterface 555 3 0
        //     259: astore 34
        //     261: aload 34
        //     263: ifnull +16 -> 279
        //     266: aload 34
        //     268: bipush 16
        //     270: invokestatic 673	java/lang/Long:parseLong	(Ljava/lang/String;I)J
        //     273: lstore 41
        //     275: lload 41
        //     277: lstore 12
        //     279: aload_3
        //     280: ifnull +297 -> 577
        //     283: aload_3
        //     284: invokestatic 663	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     287: istore 35
        //     289: goto +1018 -> 1307
        //     292: aload 23
        //     294: ifnull +10 -> 304
        //     297: aload 23
        //     299: invokevirtual 631	java/lang/String:intern	()Ljava/lang/String;
        //     302: astore 23
        //     304: aload_2
        //     305: ifnonnull +278 -> 583
        //     308: iconst_5
        //     309: new 192	java/lang/StringBuilder
        //     312: dup
        //     313: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     316: ldc_w 729
        //     319: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     322: aload_1
        //     323: invokeinterface 645 1 0
        //     328: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     331: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     334: invokestatic 278	com/android/server/pm/PackageManagerService:reportSettingsProblem	(ILjava/lang/String;)V
        //     337: aconst_null
        //     338: astore 16
        //     340: aload 16
        //     342: ifnull +922 -> 1264
        //     345: aload 16
        //     347: ldc_w 731
        //     350: aload 6
        //     352: invokevirtual 551	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     355: putfield 733	com/android/server/pm/PackageSettingBase:uidError	Z
        //     358: aload 16
        //     360: aload 5
        //     362: putfield 736	com/android/server/pm/PackageSettingBase:installerPackageName	Ljava/lang/String;
        //     365: aload 16
        //     367: aload 4
        //     369: putfield 406	com/android/server/pm/PackageSettingBase:nativeLibraryPathString	Ljava/lang/String;
        //     372: aload_1
        //     373: aconst_null
        //     374: ldc 10
        //     376: invokeinterface 555 3 0
        //     381: astore 17
        //     383: aload 17
        //     385: ifnull +746 -> 1131
        //     388: aload 16
        //     390: aload 17
        //     392: invokestatic 663	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     395: iconst_0
        //     396: invokevirtual 740	com/android/server/pm/PackageSettingBase:setEnabled	(II)V
        //     399: aload_1
        //     400: aconst_null
        //     401: ldc_w 742
        //     404: invokeinterface 555 3 0
        //     409: astore 18
        //     411: aload 18
        //     413: ifnull +20 -> 433
        //     416: aload 18
        //     418: ldc_w 744
        //     421: invokevirtual 747	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
        //     424: ifeq +717 -> 1141
        //     427: aload 16
        //     429: iconst_0
        //     430: putfield 749	com/android/server/pm/PackageSettingBase:installStatus	I
        //     433: aload_1
        //     434: invokeinterface 543 1 0
        //     439: istore 19
        //     441: aload_1
        //     442: invokeinterface 545 1 0
        //     447: istore 20
        //     449: iload 20
        //     451: iconst_1
        //     452: if_icmpeq +816 -> 1268
        //     455: iload 20
        //     457: iconst_3
        //     458: if_icmpne +14 -> 472
        //     461: aload_1
        //     462: invokeinterface 543 1 0
        //     467: iload 19
        //     469: if_icmple +799 -> 1268
        //     472: iload 20
        //     474: iconst_3
        //     475: if_icmpeq -34 -> 441
        //     478: iload 20
        //     480: iconst_4
        //     481: if_icmpeq -40 -> 441
        //     484: aload_1
        //     485: invokeinterface 548 1 0
        //     490: astore 21
        //     492: aload 21
        //     494: ldc 33
        //     496: invokevirtual 551	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     499: ifeq +651 -> 1150
        //     502: aload_0
        //     503: aload 16
        //     505: aload_1
        //     506: iconst_0
        //     507: invokespecial 751	com/android/server/pm/Settings:readDisabledComponentsLPw	(Lcom/android/server/pm/PackageSettingBase;Lorg/xmlpull/v1/XmlPullParser;I)V
        //     510: goto -69 -> 441
        //     513: aload_1
        //     514: aconst_null
        //     515: ldc 201
        //     517: invokeinterface 555 3 0
        //     522: astore 29
        //     524: aload 29
        //     526: ifnull +808 -> 1334
        //     529: ldc_w 731
        //     532: aload 29
        //     534: invokevirtual 747	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
        //     537: ifeq +791 -> 1328
        //     540: iconst_1
        //     541: istore 51
        //     543: goto +776 -> 1319
        //     546: aload_1
        //     547: aconst_null
        //     548: ldc_w 695
        //     551: invokeinterface 555 3 0
        //     556: astore 31
        //     558: aload 31
        //     560: ifnull -341 -> 219
        //     563: aload 31
        //     565: invokestatic 698	java/lang/Long:parseLong	(Ljava/lang/String;)J
        //     568: lstore 46
        //     570: lload 46
        //     572: lstore 8
        //     574: goto -355 -> 219
        //     577: iconst_0
        //     578: istore 35
        //     580: goto +727 -> 1307
        //     583: aload 25
        //     585: ifnonnull +38 -> 623
        //     588: iconst_5
        //     589: new 192	java/lang/StringBuilder
        //     592: dup
        //     593: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     596: ldc_w 753
        //     599: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     602: aload_1
        //     603: invokeinterface 645 1 0
        //     608: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     611: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     614: invokestatic 278	com/android/server/pm/PackageManagerService:reportSettingsProblem	(ILjava/lang/String;)V
        //     617: aconst_null
        //     618: astore 16
        //     620: goto -280 -> 340
        //     623: iload 35
        //     625: ifle +172 -> 797
        //     628: aload_0
        //     629: aload_2
        //     630: invokevirtual 631	java/lang/String:intern	()Ljava/lang/String;
        //     633: aload 23
        //     635: new 199	java/io/File
        //     638: dup
        //     639: aload 25
        //     641: invokespecial 665	java/io/File:<init>	(Ljava/lang/String;)V
        //     644: new 199	java/io/File
        //     647: dup
        //     648: aload 26
        //     650: invokespecial 665	java/io/File:<init>	(Ljava/lang/String;)V
        //     653: aload 4
        //     655: iload 35
        //     657: iload 14
        //     659: iload 7
        //     661: invokevirtual 757	com/android/server/pm/Settings:addPackageLPw	(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/io/File;Ljava/lang/String;III)Lcom/android/server/pm/PackageSetting;
        //     664: astore 39
        //     666: aload 39
        //     668: astore 16
        //     670: aload 16
        //     672: ifnonnull +101 -> 773
        //     675: bipush 6
        //     677: new 192	java/lang/StringBuilder
        //     680: dup
        //     681: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     684: ldc_w 759
        //     687: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     690: iload 35
        //     692: invokevirtual 304	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     695: ldc_w 761
        //     698: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     701: aload_1
        //     702: invokeinterface 645 1 0
        //     707: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     710: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     713: invokestatic 278	com/android/server/pm/PackageManagerService:reportSettingsProblem	(ILjava/lang/String;)V
        //     716: goto -376 -> 340
        //     719: astore 38
        //     721: iconst_5
        //     722: new 192	java/lang/StringBuilder
        //     725: dup
        //     726: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     729: ldc_w 763
        //     732: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     735: aload_2
        //     736: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     739: ldc_w 765
        //     742: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     745: aload_3
        //     746: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     749: ldc_w 720
        //     752: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     755: aload_1
        //     756: invokeinterface 645 1 0
        //     761: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     764: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     767: invokestatic 278	com/android/server/pm/PackageManagerService:reportSettingsProblem	(ILjava/lang/String;)V
        //     770: goto -430 -> 340
        //     773: aload 16
        //     775: lload 8
        //     777: invokevirtual 429	com/android/server/pm/PackageSetting:setTimeStamp	(J)V
        //     780: aload 16
        //     782: lload 10
        //     784: putfield 679	com/android/server/pm/PackageSettingBase:firstInstallTime	J
        //     787: aload 16
        //     789: lload 12
        //     791: putfield 684	com/android/server/pm/PackageSettingBase:lastUpdateTime	J
        //     794: goto -454 -> 340
        //     797: aload 24
        //     799: ifnull +160 -> 959
        //     802: aload 24
        //     804: ifnull +93 -> 897
        //     807: aload 24
        //     809: invokestatic 663	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     812: istore 36
        //     814: iload 36
        //     816: ifle +87 -> 903
        //     819: new 767	com/android/server/pm/PendingPackage
        //     822: dup
        //     823: aload_2
        //     824: invokevirtual 631	java/lang/String:intern	()Ljava/lang/String;
        //     827: aload 23
        //     829: new 199	java/io/File
        //     832: dup
        //     833: aload 25
        //     835: invokespecial 665	java/io/File:<init>	(Ljava/lang/String;)V
        //     838: new 199	java/io/File
        //     841: dup
        //     842: aload 26
        //     844: invokespecial 665	java/io/File:<init>	(Ljava/lang/String;)V
        //     847: aload 4
        //     849: iload 36
        //     851: iload 14
        //     853: iload 7
        //     855: invokespecial 770	com/android/server/pm/PendingPackage:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/io/File;Ljava/lang/String;III)V
        //     858: astore 16
        //     860: aload 16
        //     862: lload 8
        //     864: invokevirtual 771	com/android/server/pm/PackageSettingBase:setTimeStamp	(J)V
        //     867: aload 16
        //     869: lload 10
        //     871: putfield 679	com/android/server/pm/PackageSettingBase:firstInstallTime	J
        //     874: aload 16
        //     876: lload 12
        //     878: putfield 684	com/android/server/pm/PackageSettingBase:lastUpdateTime	J
        //     881: aload_0
        //     882: getfield 197	com/android/server/pm/Settings:mPendingPackages	Ljava/util/ArrayList;
        //     885: aload 16
        //     887: checkcast 767	com/android/server/pm/PendingPackage
        //     890: invokevirtual 315	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     893: pop
        //     894: goto -554 -> 340
        //     897: iconst_0
        //     898: istore 36
        //     900: goto -86 -> 814
        //     903: iconst_5
        //     904: new 192	java/lang/StringBuilder
        //     907: dup
        //     908: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     911: ldc_w 763
        //     914: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     917: aload_2
        //     918: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     921: ldc_w 773
        //     924: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     927: aload 24
        //     929: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     932: ldc_w 720
        //     935: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     938: aload_1
        //     939: invokeinterface 645 1 0
        //     944: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     947: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     950: invokestatic 278	com/android/server/pm/PackageManagerService:reportSettingsProblem	(ILjava/lang/String;)V
        //     953: aconst_null
        //     954: astore 16
        //     956: goto -616 -> 340
        //     959: iconst_5
        //     960: new 192	java/lang/StringBuilder
        //     963: dup
        //     964: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     967: ldc_w 763
        //     970: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     973: aload_2
        //     974: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     977: ldc_w 765
        //     980: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     983: aload_3
        //     984: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     987: ldc_w 720
        //     990: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     993: aload_1
        //     994: invokeinterface 645 1 0
        //     999: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1002: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1005: invokestatic 278	com/android/server/pm/PackageManagerService:reportSettingsProblem	(ILjava/lang/String;)V
        //     1008: aconst_null
        //     1009: astore 16
        //     1011: goto -671 -> 340
        //     1014: astore 22
        //     1016: aload 17
        //     1018: ldc_w 731
        //     1021: invokevirtual 747	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
        //     1024: ifeq +13 -> 1037
        //     1027: aload 16
        //     1029: iconst_1
        //     1030: iconst_0
        //     1031: invokevirtual 740	com/android/server/pm/PackageSettingBase:setEnabled	(II)V
        //     1034: goto -635 -> 399
        //     1037: aload 17
        //     1039: ldc_w 744
        //     1042: invokevirtual 747	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
        //     1045: ifeq +13 -> 1058
        //     1048: aload 16
        //     1050: iconst_2
        //     1051: iconst_0
        //     1052: invokevirtual 740	com/android/server/pm/PackageSettingBase:setEnabled	(II)V
        //     1055: goto -656 -> 399
        //     1058: aload 17
        //     1060: ldc_w 775
        //     1063: invokevirtual 747	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
        //     1066: ifeq +13 -> 1079
        //     1069: aload 16
        //     1071: iconst_0
        //     1072: iconst_0
        //     1073: invokevirtual 740	com/android/server/pm/PackageSettingBase:setEnabled	(II)V
        //     1076: goto -677 -> 399
        //     1079: iconst_5
        //     1080: new 192	java/lang/StringBuilder
        //     1083: dup
        //     1084: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     1087: ldc_w 763
        //     1090: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1093: aload_2
        //     1094: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1097: ldc_w 777
        //     1100: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1103: aload_3
        //     1104: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1107: ldc_w 720
        //     1110: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1113: aload_1
        //     1114: invokeinterface 645 1 0
        //     1119: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1122: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1125: invokestatic 278	com/android/server/pm/PackageManagerService:reportSettingsProblem	(ILjava/lang/String;)V
        //     1128: goto -729 -> 399
        //     1131: aload 16
        //     1133: iconst_0
        //     1134: iconst_0
        //     1135: invokevirtual 740	com/android/server/pm/PackageSettingBase:setEnabled	(II)V
        //     1138: goto -739 -> 399
        //     1141: aload 16
        //     1143: iconst_1
        //     1144: putfield 749	com/android/server/pm/PackageSettingBase:installStatus	I
        //     1147: goto -714 -> 433
        //     1150: aload 21
        //     1152: ldc 36
        //     1154: invokevirtual 551	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     1157: ifeq +14 -> 1171
        //     1160: aload_0
        //     1161: aload 16
        //     1163: aload_1
        //     1164: iconst_0
        //     1165: invokespecial 779	com/android/server/pm/Settings:readEnabledComponentsLPw	(Lcom/android/server/pm/PackageSettingBase;Lorg/xmlpull/v1/XmlPullParser;I)V
        //     1168: goto -727 -> 441
        //     1171: aload 21
        //     1173: ldc_w 781
        //     1176: invokevirtual 551	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     1179: ifeq +19 -> 1198
        //     1182: aload 16
        //     1184: getfield 415	com/android/server/pm/PackageSettingBase:signatures	Lcom/android/server/pm/PackageSignatures;
        //     1187: aload_1
        //     1188: aload_0
        //     1189: getfield 182	com/android/server/pm/Settings:mPastSignatures	Ljava/util/ArrayList;
        //     1192: invokevirtual 785	com/android/server/pm/PackageSignatures:readXml	(Lorg/xmlpull/v1/XmlPullParser;Ljava/util/ArrayList;)V
        //     1195: goto -754 -> 441
        //     1198: aload 21
        //     1200: ldc_w 689
        //     1203: invokevirtual 551	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     1206: ifeq +22 -> 1228
        //     1209: aload_0
        //     1210: aload_1
        //     1211: aload 16
        //     1213: getfield 477	com/android/server/pm/GrantedPermissions:grantedPermissions	Ljava/util/HashSet;
        //     1216: invokespecial 693	com/android/server/pm/Settings:readGrantedPermissionsLPw	(Lorg/xmlpull/v1/XmlPullParser;Ljava/util/HashSet;)V
        //     1219: aload 16
        //     1221: iconst_1
        //     1222: putfield 788	com/android/server/pm/PackageSettingBase:permissionsFixed	Z
        //     1225: goto -784 -> 441
        //     1228: iconst_5
        //     1229: new 192	java/lang/StringBuilder
        //     1232: dup
        //     1233: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     1236: ldc_w 790
        //     1239: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1242: aload_1
        //     1243: invokeinterface 548 1 0
        //     1248: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1251: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1254: invokestatic 278	com/android/server/pm/PackageManagerService:reportSettingsProblem	(ILjava/lang/String;)V
        //     1257: aload_1
        //     1258: invokestatic 640	com/android/internal/util/XmlUtils:skipCurrentTag	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     1261: goto -820 -> 441
        //     1264: aload_1
        //     1265: invokestatic 640	com/android/internal/util/XmlUtils:skipCurrentTag	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     1268: return
        //     1269: astore 54
        //     1271: goto -1122 -> 149
        //     1274: astore 52
        //     1276: goto -1087 -> 189
        //     1279: astore 48
        //     1281: goto -1062 -> 219
        //     1284: astore 32
        //     1286: goto -1067 -> 219
        //     1289: astore 43
        //     1291: goto -1042 -> 249
        //     1294: astore 40
        //     1296: goto -1017 -> 279
        //     1299: astore 15
        //     1301: aconst_null
        //     1302: astore 16
        //     1304: goto -583 -> 721
        //     1307: aload 26
        //     1309: ifnonnull -1017 -> 292
        //     1312: aload 25
        //     1314: astore 26
        //     1316: goto -1024 -> 292
        //     1319: iconst_0
        //     1320: iload 51
        //     1322: ior
        //     1323: istore 7
        //     1325: goto -1136 -> 189
        //     1328: iconst_0
        //     1329: istore 51
        //     1331: goto -12 -> 1319
        //     1334: iconst_0
        //     1335: iconst_1
        //     1336: ior
        //     1337: istore 7
        //     1339: goto -1150 -> 189
        //
        // Exception table:
        //     from	to	target	type
        //     675	716	719	java/lang/NumberFormatException
        //     773	794	719	java/lang/NumberFormatException
        //     860	894	719	java/lang/NumberFormatException
        //     388	399	1014	java/lang/NumberFormatException
        //     138	145	1269	java/lang/NumberFormatException
        //     178	185	1274	java/lang/NumberFormatException
        //     206	215	1279	java/lang/NumberFormatException
        //     563	570	1284	java/lang/NumberFormatException
        //     236	245	1289	java/lang/NumberFormatException
        //     266	275	1294	java/lang/NumberFormatException
        //     28	133	1299	java/lang/NumberFormatException
        //     149	173	1299	java/lang/NumberFormatException
        //     189	201	1299	java/lang/NumberFormatException
        //     219	231	1299	java/lang/NumberFormatException
        //     249	261	1299	java/lang/NumberFormatException
        //     283	337	1299	java/lang/NumberFormatException
        //     513	558	1299	java/lang/NumberFormatException
        //     588	666	1299	java/lang/NumberFormatException
        //     807	860	1299	java/lang/NumberFormatException
        //     903	1008	1299	java/lang/NumberFormatException
    }

    private void readPermissionsLPw(HashMap<String, BasePermission> paramHashMap, XmlPullParser paramXmlPullParser)
        throws IOException, XmlPullParserException
    {
        int i = paramXmlPullParser.getDepth();
        int j;
        do
        {
            j = paramXmlPullParser.next();
            if ((j == 1) || ((j == 3) && (paramXmlPullParser.getDepth() <= i)))
                break;
        }
        while ((j == 3) || (j == 4));
        int k;
        if (paramXmlPullParser.getName().equals("item"))
        {
            String str1 = paramXmlPullParser.getAttributeValue(null, "name");
            String str2 = paramXmlPullParser.getAttributeValue(null, "package");
            String str3 = paramXmlPullParser.getAttributeValue(null, "type");
            if ((str1 != null) && (str2 != null))
            {
                boolean bool = "dynamic".equals(str3);
                if (bool)
                {
                    k = 2;
                    label126: BasePermission localBasePermission = new BasePermission(str1, str2, k);
                    localBasePermission.protectionLevel = readInt(paramXmlPullParser, null, "protection", 0);
                    localBasePermission.protectionLevel = PermissionInfo.fixProtectionLevel(localBasePermission.protectionLevel);
                    if (bool)
                    {
                        PermissionInfo localPermissionInfo = new PermissionInfo();
                        localPermissionInfo.packageName = str2.intern();
                        localPermissionInfo.name = str1.intern();
                        localPermissionInfo.icon = readInt(paramXmlPullParser, null, "icon", 0);
                        localPermissionInfo.nonLocalizedLabel = paramXmlPullParser.getAttributeValue(null, "label");
                        localPermissionInfo.protectionLevel = localBasePermission.protectionLevel;
                        localBasePermission.pendingInfo = localPermissionInfo;
                    }
                    paramHashMap.put(localBasePermission.name, localBasePermission);
                }
            }
        }
        while (true)
        {
            XmlUtils.skipCurrentTag(paramXmlPullParser);
            break;
            k = 0;
            break label126;
            PackageManagerService.reportSettingsProblem(5, "Error in package manager settings: permissions has no name at " + paramXmlPullParser.getPositionDescription());
            continue;
            PackageManagerService.reportSettingsProblem(5, "Unknown element reading permissions: " + paramXmlPullParser.getName() + " at " + paramXmlPullParser.getPositionDescription());
        }
    }

    private void readPreferredActivitiesLPw(XmlPullParser paramXmlPullParser)
        throws XmlPullParserException, IOException
    {
        int i = paramXmlPullParser.getDepth();
        while (true)
        {
            int j = paramXmlPullParser.next();
            if ((j == 1) || ((j == 3) && (paramXmlPullParser.getDepth() <= i)))
                break;
            if ((j != 3) && (j != 4))
                if (paramXmlPullParser.getName().equals("item"))
                {
                    PreferredActivity localPreferredActivity = new PreferredActivity(paramXmlPullParser);
                    if (localPreferredActivity.mPref.getParseError() == null)
                        this.mPreferredActivities.addFilter(localPreferredActivity);
                    else
                        PackageManagerService.reportSettingsProblem(5, "Error in package manager settings: <preferred-activity> " + localPreferredActivity.mPref.getParseError() + " at " + paramXmlPullParser.getPositionDescription());
                }
                else
                {
                    PackageManagerService.reportSettingsProblem(5, "Unknown element under <preferred-activities>: " + paramXmlPullParser.getName());
                    XmlUtils.skipCurrentTag(paramXmlPullParser);
                }
        }
    }

    private void readSharedUserLPw(XmlPullParser paramXmlPullParser)
        throws XmlPullParserException, IOException
    {
        String str1 = null;
        String str2 = null;
        int i = 0;
        SharedUserSetting localSharedUserSetting = null;
        try
        {
            str1 = paramXmlPullParser.getAttributeValue(null, "name");
            str2 = paramXmlPullParser.getAttributeValue(null, "userId");
            if (str2 != null)
            {
                m = Integer.parseInt(str2);
                if ("true".equals(paramXmlPullParser.getAttributeValue(null, "system")))
                    i = 0x0 | 0x1;
                if (str1 != null)
                    break label194;
                PackageManagerService.reportSettingsProblem(5, "Error in package manager settings: <shared-user> has no name at " + paramXmlPullParser.getPositionDescription());
            }
            while (true)
            {
                if (localSharedUserSetting == null)
                    break label417;
                int j = paramXmlPullParser.getDepth();
                while (true)
                {
                    int k = paramXmlPullParser.next();
                    if ((k == 1) || ((k == 3) && (paramXmlPullParser.getDepth() <= j)))
                        return;
                    if ((k != 3) && (k != 4))
                    {
                        str3 = paramXmlPullParser.getName();
                        if (!str3.equals("sigs"))
                            break;
                        localSharedUserSetting.signatures.readXml(paramXmlPullParser, this.mPastSignatures);
                    }
                }
                m = 0;
                break;
                label194: if (m != 0)
                    break label305;
                PackageManagerService.reportSettingsProblem(5, "Error in package manager settings: shared-user " + str1 + " has bad userId " + str2 + " at " + paramXmlPullParser.getPositionDescription());
            }
        }
        catch (NumberFormatException localNumberFormatException)
        {
            while (true)
            {
                int m;
                String str3;
                PackageManagerService.reportSettingsProblem(5, "Error in package manager settings: package " + str1 + " has bad userId " + str2 + " at " + paramXmlPullParser.getPositionDescription());
                continue;
                label305: localSharedUserSetting = addSharedUserLPw(str1.intern(), m, i);
                if (localSharedUserSetting == null)
                {
                    PackageManagerService.reportSettingsProblem(6, "Occurred while parsing settings at " + paramXmlPullParser.getPositionDescription());
                    continue;
                    if (str3.equals("perms"))
                    {
                        readGrantedPermissionsLPw(paramXmlPullParser, localSharedUserSetting.grantedPermissions);
                    }
                    else
                    {
                        PackageManagerService.reportSettingsProblem(5, "Unknown element under <shared-user>: " + paramXmlPullParser.getName());
                        XmlUtils.skipCurrentTag(paramXmlPullParser);
                    }
                }
            }
            label417: XmlUtils.skipCurrentTag(paramXmlPullParser);
        }
    }

    private void removeUserIdLPw(int paramInt)
    {
        if (paramInt >= 10000)
        {
            int i = this.mUserIds.size();
            int j = paramInt - 10000;
            if (j < i)
                this.mUserIds.set(j, null);
        }
        while (true)
        {
            return;
            this.mOtherUserIds.remove(paramInt);
        }
    }

    private void replacePackageLPw(String paramString, PackageSetting paramPackageSetting)
    {
        PackageSetting localPackageSetting = (PackageSetting)this.mPackages.get(paramString);
        if (localPackageSetting != null)
        {
            if (localPackageSetting.sharedUser == null)
                break label58;
            localPackageSetting.sharedUser.packages.remove(localPackageSetting);
            localPackageSetting.sharedUser.packages.add(paramPackageSetting);
        }
        while (true)
        {
            this.mPackages.put(paramString, paramPackageSetting);
            return;
            label58: replaceUserIdLPw(localPackageSetting.appId, paramPackageSetting);
        }
    }

    private void replaceUserIdLPw(int paramInt, Object paramObject)
    {
        if (paramInt >= 10000)
        {
            int i = this.mUserIds.size();
            int j = paramInt - 10000;
            if (j < i)
                this.mUserIds.set(j, paramObject);
        }
        while (true)
        {
            return;
            this.mOtherUserIds.put(paramInt, paramObject);
        }
    }

    PackageSetting addPackageLPw(String paramString1, String paramString2, File paramFile1, File paramFile2, String paramString3, int paramInt1, int paramInt2, int paramInt3)
    {
        PackageSetting localPackageSetting1 = (PackageSetting)this.mPackages.get(paramString1);
        Object localObject;
        if (localPackageSetting1 != null)
            if (localPackageSetting1.appId == paramInt1)
                localObject = localPackageSetting1;
        while (true)
        {
            return localObject;
            PackageManagerService.reportSettingsProblem(6, "Adding duplicate package, keeping first: " + paramString1);
            localObject = null;
            continue;
            PackageSetting localPackageSetting2 = new PackageSetting(paramString1, paramString2, paramFile1, paramFile2, paramString3, paramInt2, paramInt3);
            localPackageSetting2.appId = paramInt1;
            if (addUserIdLPw(paramInt1, localPackageSetting2, paramString1))
            {
                this.mPackages.put(paramString1, localPackageSetting2);
                localObject = localPackageSetting2;
            }
            else
            {
                localObject = null;
            }
        }
    }

    SharedUserSetting addSharedUserLPw(String paramString, int paramInt1, int paramInt2)
    {
        Object localObject = null;
        SharedUserSetting localSharedUserSetting1 = (SharedUserSetting)this.mSharedUsers.get(paramString);
        if (localSharedUserSetting1 != null)
            if (localSharedUserSetting1.userId == paramInt1)
                localObject = localSharedUserSetting1;
        while (true)
        {
            return localObject;
            PackageManagerService.reportSettingsProblem(6, "Adding duplicate shared user, keeping first: " + paramString);
            continue;
            SharedUserSetting localSharedUserSetting2 = new SharedUserSetting(paramString, paramInt2);
            localSharedUserSetting2.userId = paramInt1;
            if (addUserIdLPw(paramInt1, localSharedUserSetting2, paramString))
            {
                this.mSharedUsers.put(paramString, localSharedUserSetting2);
                localObject = localSharedUserSetting2;
            }
        }
    }

    boolean disableSystemPackageLPw(String paramString)
    {
        boolean bool = false;
        PackageSetting localPackageSetting = (PackageSetting)this.mPackages.get(paramString);
        if (localPackageSetting == null)
            Log.w("PackageManager", "Package:" + paramString + " is not an installed package");
        while (true)
        {
            return bool;
            if ((PackageSetting)this.mDisabledSysPackages.get(paramString) == null)
            {
                if ((localPackageSetting.pkg != null) && (localPackageSetting.pkg.applicationInfo != null))
                {
                    ApplicationInfo localApplicationInfo = localPackageSetting.pkg.applicationInfo;
                    localApplicationInfo.flags = (0x80 | localApplicationInfo.flags);
                }
                this.mDisabledSysPackages.put(paramString, localPackageSetting);
                replacePackageLPw(paramString, new PackageSetting(localPackageSetting));
                bool = true;
            }
        }
    }

    void dumpPackagesLPr(PrintWriter paramPrintWriter, String paramString, PackageManagerService.DumpState paramDumpState)
    {
        SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date localDate = new Date();
        int i = 0;
        List localList = getAllUsers();
        Iterator localIterator1 = this.mPackages.values().iterator();
        while (localIterator1.hasNext())
        {
            PackageSetting localPackageSetting2 = (PackageSetting)localIterator1.next();
            if ((paramString == null) || (paramString.equals(localPackageSetting2.realName)) || (paramString.equals(localPackageSetting2.name)))
            {
                if (paramString != null)
                    paramDumpState.setSharedUser(localPackageSetting2.sharedUser);
                if (i == 0)
                {
                    if (paramDumpState.onTitlePrinted())
                        paramPrintWriter.println(" ");
                    paramPrintWriter.println("Packages:");
                    i = 1;
                }
                paramPrintWriter.print("    Package [");
                String str2;
                Iterator localIterator4;
                if (localPackageSetting2.realName != null)
                {
                    str2 = localPackageSetting2.realName;
                    paramPrintWriter.print(str2);
                    paramPrintWriter.print("] (");
                    paramPrintWriter.print(Integer.toHexString(System.identityHashCode(localPackageSetting2)));
                    paramPrintWriter.println("):");
                    if (localPackageSetting2.realName != null)
                    {
                        paramPrintWriter.print("        compat name=");
                        paramPrintWriter.println(localPackageSetting2.name);
                    }
                    paramPrintWriter.print("        userId=");
                    paramPrintWriter.print(localPackageSetting2.appId);
                    paramPrintWriter.print(" gids=");
                    paramPrintWriter.println(PackageManagerService.arrayToString(localPackageSetting2.gids));
                    paramPrintWriter.print("        sharedUser=");
                    paramPrintWriter.println(localPackageSetting2.sharedUser);
                    paramPrintWriter.print("        pkg=");
                    paramPrintWriter.println(localPackageSetting2.pkg);
                    paramPrintWriter.print("        codePath=");
                    paramPrintWriter.println(localPackageSetting2.codePathString);
                    paramPrintWriter.print("        resourcePath=");
                    paramPrintWriter.println(localPackageSetting2.resourcePathString);
                    paramPrintWriter.print("        nativeLibraryPath=");
                    paramPrintWriter.println(localPackageSetting2.nativeLibraryPathString);
                    paramPrintWriter.print("        versionCode=");
                    paramPrintWriter.println(localPackageSetting2.versionCode);
                    if (localPackageSetting2.pkg != null)
                    {
                        paramPrintWriter.print("        applicationInfo=");
                        paramPrintWriter.println(localPackageSetting2.pkg.applicationInfo.toString());
                        paramPrintWriter.print("        flags=");
                        printFlags(paramPrintWriter, localPackageSetting2.pkg.applicationInfo.flags, FLAG_DUMP_SPEC);
                        paramPrintWriter.println();
                        paramPrintWriter.print("        versionName=");
                        paramPrintWriter.println(localPackageSetting2.pkg.mVersionName);
                        paramPrintWriter.print("        dataDir=");
                        paramPrintWriter.println(localPackageSetting2.pkg.applicationInfo.dataDir);
                        paramPrintWriter.print("        targetSdk=");
                        paramPrintWriter.println(localPackageSetting2.pkg.applicationInfo.targetSdkVersion);
                        if (localPackageSetting2.pkg.mOperationPending)
                            paramPrintWriter.println("        mOperationPending=true");
                        paramPrintWriter.print("        supportsScreens=[");
                        int m = 1;
                        if ((0x200 & localPackageSetting2.pkg.applicationInfo.flags) != 0)
                        {
                            if (m == 0)
                                paramPrintWriter.print(", ");
                            m = 0;
                            paramPrintWriter.print("small");
                        }
                        if ((0x400 & localPackageSetting2.pkg.applicationInfo.flags) != 0)
                        {
                            if (m == 0)
                                paramPrintWriter.print(", ");
                            m = 0;
                            paramPrintWriter.print("medium");
                        }
                        if ((0x800 & localPackageSetting2.pkg.applicationInfo.flags) != 0)
                        {
                            if (m == 0)
                                paramPrintWriter.print(", ");
                            m = 0;
                            paramPrintWriter.print("large");
                        }
                        if ((0x80000 & localPackageSetting2.pkg.applicationInfo.flags) != 0)
                        {
                            if (m == 0)
                                paramPrintWriter.print(", ");
                            m = 0;
                            paramPrintWriter.print("xlarge");
                        }
                        if ((0x1000 & localPackageSetting2.pkg.applicationInfo.flags) != 0)
                        {
                            if (m == 0)
                                paramPrintWriter.print(", ");
                            m = 0;
                            paramPrintWriter.print("resizeable");
                        }
                        if ((0x2000 & localPackageSetting2.pkg.applicationInfo.flags) != 0)
                        {
                            if (m == 0)
                                paramPrintWriter.print(", ");
                            paramPrintWriter.print("anyDensity");
                        }
                    }
                    paramPrintWriter.println("]");
                    paramPrintWriter.print("        timeStamp=");
                    localDate.setTime(localPackageSetting2.timeStamp);
                    paramPrintWriter.println(localSimpleDateFormat.format(localDate));
                    paramPrintWriter.print("        firstInstallTime=");
                    localDate.setTime(localPackageSetting2.firstInstallTime);
                    paramPrintWriter.println(localSimpleDateFormat.format(localDate));
                    paramPrintWriter.print("        lastUpdateTime=");
                    localDate.setTime(localPackageSetting2.lastUpdateTime);
                    paramPrintWriter.println(localSimpleDateFormat.format(localDate));
                    if (localPackageSetting2.installerPackageName != null)
                    {
                        paramPrintWriter.print("        installerPackageName=");
                        paramPrintWriter.println(localPackageSetting2.installerPackageName);
                    }
                    paramPrintWriter.print("        signatures=");
                    paramPrintWriter.println(localPackageSetting2.signatures);
                    paramPrintWriter.print("        permissionsFixed=");
                    paramPrintWriter.print(localPackageSetting2.permissionsFixed);
                    paramPrintWriter.print(" haveGids=");
                    paramPrintWriter.println(localPackageSetting2.haveGids);
                    paramPrintWriter.print("        pkgFlags=0x");
                    paramPrintWriter.print(Integer.toHexString(localPackageSetting2.pkgFlags));
                    paramPrintWriter.print(" installStatus=");
                    paramPrintWriter.print(localPackageSetting2.installStatus);
                    localIterator4 = localList.iterator();
                }
                while (true)
                {
                    if (!localIterator4.hasNext())
                        break label1188;
                    UserInfo localUserInfo = (UserInfo)localIterator4.next();
                    paramPrintWriter.print(" User ");
                    paramPrintWriter.print(localUserInfo.id);
                    paramPrintWriter.print(": ");
                    paramPrintWriter.print(" stopped=");
                    paramPrintWriter.print(localPackageSetting2.getStopped(localUserInfo.id));
                    paramPrintWriter.print(" enabled=");
                    paramPrintWriter.println(localPackageSetting2.getEnabled(localUserInfo.id));
                    if (localPackageSetting2.getDisabledComponents(localUserInfo.id).size() > 0)
                    {
                        paramPrintWriter.println("        disabledComponents:");
                        Iterator localIterator7 = localPackageSetting2.getDisabledComponents(localUserInfo.id).iterator();
                        while (true)
                            if (localIterator7.hasNext())
                            {
                                String str5 = (String)localIterator7.next();
                                paramPrintWriter.print("            ");
                                paramPrintWriter.println(str5);
                                continue;
                                str2 = localPackageSetting2.name;
                                break;
                            }
                    }
                    if (localPackageSetting2.getEnabledComponents(localUserInfo.id).size() > 0)
                    {
                        paramPrintWriter.println("        enabledComponents:");
                        Iterator localIterator6 = localPackageSetting2.getEnabledComponents(localUserInfo.id).iterator();
                        while (localIterator6.hasNext())
                        {
                            String str4 = (String)localIterator6.next();
                            paramPrintWriter.print("            ");
                            paramPrintWriter.println(str4);
                        }
                    }
                }
                label1188: if (localPackageSetting2.grantedPermissions.size() > 0)
                {
                    paramPrintWriter.println("        grantedPermissions:");
                    Iterator localIterator5 = localPackageSetting2.grantedPermissions.iterator();
                    while (localIterator5.hasNext())
                    {
                        String str3 = (String)localIterator5.next();
                        paramPrintWriter.print("            ");
                        paramPrintWriter.println(str3);
                    }
                }
            }
        }
        int j = 0;
        if (this.mRenamedPackages.size() > 0)
        {
            Iterator localIterator3 = this.mRenamedPackages.entrySet().iterator();
            while (localIterator3.hasNext())
            {
                Map.Entry localEntry = (Map.Entry)localIterator3.next();
                if ((paramString == null) || (paramString.equals(localEntry.getKey())) || (paramString.equals(localEntry.getValue())))
                {
                    if (j == 0)
                    {
                        if (paramDumpState.onTitlePrinted())
                            paramPrintWriter.println(" ");
                        paramPrintWriter.println("Renamed packages:");
                        j = 1;
                    }
                    paramPrintWriter.print("    ");
                    paramPrintWriter.print((String)localEntry.getKey());
                    paramPrintWriter.print(" -> ");
                    paramPrintWriter.println((String)localEntry.getValue());
                }
            }
        }
        int k = 0;
        if (this.mDisabledSysPackages.size() > 0)
        {
            Iterator localIterator2 = this.mDisabledSysPackages.values().iterator();
            while (localIterator2.hasNext())
            {
                PackageSetting localPackageSetting1 = (PackageSetting)localIterator2.next();
                if ((paramString == null) || (paramString.equals(localPackageSetting1.realName)) || (paramString.equals(localPackageSetting1.name)))
                {
                    if (k == 0)
                    {
                        if (paramDumpState.onTitlePrinted())
                            paramPrintWriter.println(" ");
                        paramPrintWriter.println("Hidden system packages:");
                        k = 1;
                    }
                    paramPrintWriter.print("    Package [");
                    if (localPackageSetting1.realName != null);
                    for (String str1 = localPackageSetting1.realName; ; str1 = localPackageSetting1.name)
                    {
                        paramPrintWriter.print(str1);
                        paramPrintWriter.print("] (");
                        paramPrintWriter.print(Integer.toHexString(System.identityHashCode(localPackageSetting1)));
                        paramPrintWriter.println("):");
                        if (localPackageSetting1.realName != null)
                        {
                            paramPrintWriter.print("        compat name=");
                            paramPrintWriter.println(localPackageSetting1.name);
                        }
                        if ((localPackageSetting1.pkg != null) && (localPackageSetting1.pkg.applicationInfo != null))
                        {
                            paramPrintWriter.print("        applicationInfo=");
                            paramPrintWriter.println(localPackageSetting1.pkg.applicationInfo.toString());
                        }
                        paramPrintWriter.print("        userId=");
                        paramPrintWriter.println(localPackageSetting1.appId);
                        paramPrintWriter.print("        sharedUser=");
                        paramPrintWriter.println(localPackageSetting1.sharedUser);
                        paramPrintWriter.print("        codePath=");
                        paramPrintWriter.println(localPackageSetting1.codePathString);
                        paramPrintWriter.print("        resourcePath=");
                        paramPrintWriter.println(localPackageSetting1.resourcePathString);
                        break;
                    }
                }
            }
        }
    }

    void dumpPermissionsLPr(PrintWriter paramPrintWriter, String paramString, PackageManagerService.DumpState paramDumpState)
    {
        int i = 0;
        Iterator localIterator = this.mPermissions.values().iterator();
        while (localIterator.hasNext())
        {
            BasePermission localBasePermission = (BasePermission)localIterator.next();
            if ((paramString == null) || (paramString.equals(localBasePermission.sourcePackage)))
            {
                if (i == 0)
                {
                    if (paramDumpState.onTitlePrinted())
                        paramPrintWriter.println(" ");
                    paramPrintWriter.println("Permissions:");
                    i = 1;
                }
                paramPrintWriter.print("    Permission [");
                paramPrintWriter.print(localBasePermission.name);
                paramPrintWriter.print("] (");
                paramPrintWriter.print(Integer.toHexString(System.identityHashCode(localBasePermission)));
                paramPrintWriter.println("):");
                paramPrintWriter.print("        sourcePackage=");
                paramPrintWriter.println(localBasePermission.sourcePackage);
                paramPrintWriter.print("        uid=");
                paramPrintWriter.print(localBasePermission.uid);
                paramPrintWriter.print(" gids=");
                paramPrintWriter.print(PackageManagerService.arrayToString(localBasePermission.gids));
                paramPrintWriter.print(" type=");
                paramPrintWriter.print(localBasePermission.type);
                paramPrintWriter.print(" prot=");
                paramPrintWriter.println(PermissionInfo.protectionToString(localBasePermission.protectionLevel));
                if (localBasePermission.packageSetting != null)
                {
                    paramPrintWriter.print("        packageSetting=");
                    paramPrintWriter.println(localBasePermission.packageSetting);
                }
                if (localBasePermission.perm != null)
                {
                    paramPrintWriter.print("        perm=");
                    paramPrintWriter.println(localBasePermission.perm);
                }
                if ("android.permission.READ_EXTERNAL_STORAGE".equals(localBasePermission.name))
                {
                    paramPrintWriter.print("        enforced=");
                    paramPrintWriter.println(this.mReadExternalStorageEnforced);
                }
            }
        }
    }

    void dumpReadMessagesLPr(PrintWriter paramPrintWriter, PackageManagerService.DumpState paramDumpState)
    {
        paramPrintWriter.println("Settings parse messages:");
        paramPrintWriter.print(this.mReadMessages.toString());
    }

    void dumpSharedUsersLPr(PrintWriter paramPrintWriter, String paramString, PackageManagerService.DumpState paramDumpState)
    {
        int i = 0;
        Iterator localIterator1 = this.mSharedUsers.values().iterator();
        while (localIterator1.hasNext())
        {
            SharedUserSetting localSharedUserSetting = (SharedUserSetting)localIterator1.next();
            if ((paramString == null) || (localSharedUserSetting == paramDumpState.getSharedUser()))
            {
                if (i == 0)
                {
                    if (paramDumpState.onTitlePrinted())
                        paramPrintWriter.println(" ");
                    paramPrintWriter.println("Shared users:");
                    i = 1;
                }
                paramPrintWriter.print("    SharedUser [");
                paramPrintWriter.print(localSharedUserSetting.name);
                paramPrintWriter.print("] (");
                paramPrintWriter.print(Integer.toHexString(System.identityHashCode(localSharedUserSetting)));
                paramPrintWriter.println("):");
                paramPrintWriter.print("        userId=");
                paramPrintWriter.print(localSharedUserSetting.userId);
                paramPrintWriter.print(" gids=");
                paramPrintWriter.println(PackageManagerService.arrayToString(localSharedUserSetting.gids));
                paramPrintWriter.println("        grantedPermissions:");
                Iterator localIterator2 = localSharedUserSetting.grantedPermissions.iterator();
                while (localIterator2.hasNext())
                {
                    String str = (String)localIterator2.next();
                    paramPrintWriter.print("            ");
                    paramPrintWriter.println(str);
                }
            }
        }
    }

    PackageSetting enableSystemPackageLPw(String paramString)
    {
        PackageSetting localPackageSetting1 = (PackageSetting)this.mDisabledSysPackages.get(paramString);
        PackageSetting localPackageSetting2;
        if (localPackageSetting1 == null)
        {
            Log.w("PackageManager", "Package:" + paramString + " is not disabled");
            localPackageSetting2 = null;
        }
        while (true)
        {
            return localPackageSetting2;
            if ((localPackageSetting1.pkg != null) && (localPackageSetting1.pkg.applicationInfo != null))
            {
                ApplicationInfo localApplicationInfo = localPackageSetting1.pkg.applicationInfo;
                localApplicationInfo.flags = (0xFFFFFF7F & localApplicationInfo.flags);
            }
            localPackageSetting2 = addPackageLPw(paramString, localPackageSetting1.realName, localPackageSetting1.codePath, localPackageSetting1.resourcePath, localPackageSetting1.nativeLibraryPathString, localPackageSetting1.appId, localPackageSetting1.versionCode, localPackageSetting1.pkgFlags);
            this.mDisabledSysPackages.remove(paramString);
        }
    }

    int getApplicationEnabledSettingLPr(String paramString, int paramInt)
    {
        PackageSetting localPackageSetting = (PackageSetting)this.mPackages.get(paramString);
        if (localPackageSetting == null)
            throw new IllegalArgumentException("Unknown package: " + paramString);
        return localPackageSetting.getEnabled(paramInt);
    }

    int getComponentEnabledSettingLPr(ComponentName paramComponentName, int paramInt)
    {
        String str = paramComponentName.getPackageName();
        PackageSetting localPackageSetting = (PackageSetting)this.mPackages.get(str);
        if (localPackageSetting == null)
            throw new IllegalArgumentException("Unknown component: " + paramComponentName);
        return localPackageSetting.getCurrentEnabledStateLPr(paramComponentName.getClassName(), paramInt);
    }

    public PackageSetting getDisabledSystemPkgLPr(String paramString)
    {
        return (PackageSetting)this.mDisabledSysPackages.get(paramString);
    }

    String getInstallerPackageNameLPr(String paramString)
    {
        PackageSetting localPackageSetting = (PackageSetting)this.mPackages.get(paramString);
        if (localPackageSetting == null)
            throw new IllegalArgumentException("Unknown package: " + paramString);
        return localPackageSetting.installerPackageName;
    }

    ArrayList<PackageSetting> getListOfIncompleteInstallPackagesLPr()
    {
        Iterator localIterator = new HashSet(this.mPackages.keySet()).iterator();
        ArrayList localArrayList = new ArrayList();
        while (localIterator.hasNext())
        {
            String str = (String)localIterator.next();
            PackageSetting localPackageSetting = (PackageSetting)this.mPackages.get(str);
            if (localPackageSetting.getInstallStatus() == 0)
                localArrayList.add(localPackageSetting);
        }
        return localArrayList;
    }

    PackageSetting getPackageLPw(PackageParser.Package paramPackage, PackageSetting paramPackageSetting, String paramString1, SharedUserSetting paramSharedUserSetting, File paramFile1, File paramFile2, String paramString2, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
    {
        return getPackageLPw(paramPackage.packageName, paramPackageSetting, paramString1, paramSharedUserSetting, paramFile1, paramFile2, paramString2, paramPackage.mVersionCode, paramInt, paramBoolean1, paramBoolean2);
    }

    SharedUserSetting getSharedUserLPw(String paramString, int paramInt, boolean paramBoolean)
    {
        SharedUserSetting localSharedUserSetting1 = (SharedUserSetting)this.mSharedUsers.get(paramString);
        if (localSharedUserSetting1 == null)
            if (paramBoolean);
        for (SharedUserSetting localSharedUserSetting2 = null; ; localSharedUserSetting2 = localSharedUserSetting1)
        {
            return localSharedUserSetting2;
            localSharedUserSetting1 = new SharedUserSetting(paramString, paramInt);
            localSharedUserSetting1.userId = newUserIdLPw(localSharedUserSetting1);
            Log.i("PackageManager", "New shared user " + paramString + ": id=" + localSharedUserSetting1.userId);
            if (localSharedUserSetting1.userId >= 0)
                this.mSharedUsers.put(paramString, localSharedUserSetting1);
        }
    }

    public Object getUserIdLPr(int paramInt)
    {
        Object localObject;
        if (paramInt >= 10000)
        {
            int i = this.mUserIds.size();
            int j = paramInt - 10000;
            if (j < i)
                localObject = this.mUserIds.get(j);
        }
        while (true)
        {
            return localObject;
            localObject = null;
            continue;
            localObject = this.mOtherUserIds.get(paramInt);
        }
    }

    public VerifierDeviceIdentity getVerifierDeviceIdentityLPw()
    {
        if (this.mVerifierDeviceIdentity == null)
        {
            this.mVerifierDeviceIdentity = VerifierDeviceIdentity.generate();
            writeLPr();
        }
        return this.mVerifierDeviceIdentity;
    }

    void insertPackageSettingLPw(PackageSetting paramPackageSetting, PackageParser.Package paramPackage)
    {
        paramPackageSetting.pkg = paramPackage;
        String str1 = paramPackage.applicationInfo.sourceDir;
        String str2 = paramPackage.applicationInfo.publicSourceDir;
        if (!str1.equalsIgnoreCase(paramPackageSetting.codePathString))
        {
            Slog.w("PackageManager", "Code path for pkg : " + paramPackageSetting.pkg.packageName + " changing from " + paramPackageSetting.codePathString + " to " + str1);
            paramPackageSetting.codePath = new File(str1);
            paramPackageSetting.codePathString = str1;
        }
        if (!str2.equalsIgnoreCase(paramPackageSetting.resourcePathString))
        {
            Slog.w("PackageManager", "Resource path for pkg : " + paramPackageSetting.pkg.packageName + " changing from " + paramPackageSetting.resourcePathString + " to " + str2);
            paramPackageSetting.resourcePath = new File(str2);
            paramPackageSetting.resourcePathString = str2;
        }
        String str3 = paramPackage.applicationInfo.nativeLibraryDir;
        if ((str3 != null) && (!str3.equalsIgnoreCase(paramPackageSetting.nativeLibraryPathString)))
            paramPackageSetting.nativeLibraryPathString = str3;
        if (paramPackage.mVersionCode != paramPackageSetting.versionCode)
            paramPackageSetting.versionCode = paramPackage.mVersionCode;
        if (paramPackageSetting.signatures.mSignatures == null)
            paramPackageSetting.signatures.assignSignatures(paramPackage.mSignatures);
        if ((paramPackageSetting.sharedUser != null) && (paramPackageSetting.sharedUser.signatures.mSignatures == null))
            paramPackageSetting.sharedUser.signatures.assignSignatures(paramPackage.mSignatures);
        addPackageSettingLPw(paramPackageSetting, paramPackage.packageName, paramPackageSetting.sharedUser);
    }

    boolean isDisabledSystemPackageLPr(String paramString)
    {
        return this.mDisabledSysPackages.containsKey(paramString);
    }

    boolean isEnabledLPr(ComponentInfo paramComponentInfo, int paramInt1, int paramInt2)
    {
        boolean bool = true;
        if ((paramInt1 & 0x200) != 0);
        while (true)
        {
            return bool;
            String str = paramComponentInfo.packageName;
            PackageSetting localPackageSetting = (PackageSetting)this.mPackages.get(str);
            if (localPackageSetting == null)
            {
                bool = false;
            }
            else
            {
                int i = localPackageSetting.getEnabled(paramInt2);
                if ((i == 2) || (i == 3) || ((localPackageSetting.pkg != null) && (!localPackageSetting.pkg.applicationInfo.enabled) && (i == 0)))
                    bool = false;
                else if (!localPackageSetting.getEnabledComponents(paramInt2).contains(paramComponentInfo.name))
                    if (localPackageSetting.getDisabledComponents(paramInt2).contains(paramComponentInfo.name))
                        bool = false;
                    else
                        bool = paramComponentInfo.enabled;
            }
        }
    }

    PackageSetting peekPackageLPr(String paramString)
    {
        return (PackageSetting)this.mPackages.get(paramString);
    }

    void readAllUsersPackageRestrictionsLPr()
    {
        List localList = getAllUsers();
        if (localList == null)
            readPackageRestrictionsLPr(0);
        while (true)
        {
            return;
            Iterator localIterator = localList.iterator();
            while (localIterator.hasNext())
                readPackageRestrictionsLPr(((UserInfo)localIterator.next()).id);
        }
    }

    boolean readLPw(List<UserInfo> paramList)
    {
        Object localObject1 = null;
        if (this.mBackupSettingsFilename.exists());
        boolean bool;
        try
        {
            localFileInputStream2 = new FileInputStream(this.mBackupSettingsFilename);
        }
        catch (IOException localIOException3)
        {
            try
            {
                this.mReadMessages.append("Reading from backup settings file\n");
                PackageManagerService.reportSettingsProblem(4, "Need to read from backup settings file");
                if (this.mSettingsFilename.exists())
                {
                    Slog.w("PackageManager", "Cleaning up settings file " + this.mSettingsFilename);
                    this.mSettingsFilename.delete();
                }
                localObject1 = localFileInputStream2;
                while (true)
                {
                    this.mPendingPackages.clear();
                    this.mPastSignatures.clear();
                    if (localObject1 == null);
                    try
                    {
                        if (!this.mSettingsFilename.exists())
                        {
                            this.mReadMessages.append("No settings file found\n");
                            PackageManagerService.reportSettingsProblem(4, "No settings file; creating initial state");
                            readDefaultPreferredAppsLPw();
                            bool = false;
                            break;
                        }
                        FileInputStream localFileInputStream1 = new FileInputStream(this.mSettingsFilename);
                        localObject1 = localFileInputStream1;
                        localXmlPullParser = Xml.newPullParser();
                        localXmlPullParser.setInput(localObject1, null);
                        int k;
                        do
                            k = localXmlPullParser.next();
                        while ((k != 2) && (k != 1));
                        if (k != 2)
                        {
                            this.mReadMessages.append("No start tag found in settings file\n");
                            PackageManagerService.reportSettingsProblem(5, "No start tag found in package manager settings");
                            Log.wtf("PackageManager", "No start tag found in package manager settings");
                            bool = false;
                            break;
                        }
                        int m = localXmlPullParser.getDepth();
                        while (true)
                        {
                            int n = localXmlPullParser.next();
                            if ((n == 1) || ((n == 3) && (localXmlPullParser.getDepth() <= m)))
                                break label1066;
                            if ((n != 3) && (n != 4))
                            {
                                str3 = localXmlPullParser.getName();
                                if (!str3.equals("package"))
                                    break;
                                readPackageLPw(localXmlPullParser);
                            }
                        }
                    }
                    catch (XmlPullParserException localXmlPullParserException)
                    {
                        while (true)
                        {
                            this.mReadMessages.append("Error reading: " + localXmlPullParserException.toString());
                            PackageManagerService.reportSettingsProblem(6, "Error reading settings: " + localXmlPullParserException);
                            Log.wtf("PackageManager", "Error reading package manager settings", localXmlPullParserException);
                            int i = this.mPendingPackages.size();
                            for (int j = 0; ; j++)
                            {
                                if (j >= i)
                                    break label1220;
                                localPendingPackage = (PendingPackage)this.mPendingPackages.get(j);
                                localObject3 = getUserIdLPr(localPendingPackage.sharedId);
                                if ((localObject3 == null) || (!(localObject3 instanceof SharedUserSetting)))
                                    break label1083;
                                localPackageSetting2 = getPackageLPw(localPendingPackage.name, null, localPendingPackage.realName, (SharedUserSetting)localObject3, localPendingPackage.codePath, localPendingPackage.resourcePath, localPendingPackage.nativeLibraryPathString, localPendingPackage.versionCode, localPendingPackage.pkgFlags, true, true);
                                if (localPackageSetting2 != null)
                                    break;
                                PackageManagerService.reportSettingsProblem(5, "Unable to create application package for " + localPendingPackage.name);
                            }
                            if (!str3.equals("permissions"))
                                break;
                            readPermissionsLPw(this.mPermissions, localXmlPullParser);
                        }
                    }
                    catch (IOException localIOException1)
                    {
                        while (true)
                        {
                            XmlPullParser localXmlPullParser;
                            String str3;
                            PendingPackage localPendingPackage;
                            Object localObject3;
                            PackageSetting localPackageSetting2;
                            this.mReadMessages.append("Error reading: " + localIOException1.toString());
                            PackageManagerService.reportSettingsProblem(6, "Error reading settings: " + localIOException1);
                            Log.wtf("PackageManager", "Error reading package manager settings", localIOException1);
                            continue;
                            if (str3.equals("permission-trees"))
                                readPermissionsLPw(this.mPermissionTrees, localXmlPullParser);
                            else if (str3.equals("shared-user"))
                                readSharedUserLPw(localXmlPullParser);
                            else if (!str3.equals("preferred-packages"))
                                if (str3.equals("preferred-activities"))
                                {
                                    readPreferredActivitiesLPw(localXmlPullParser);
                                }
                                else if (str3.equals("updated-package"))
                                {
                                    readDisabledSysPackageLPw(localXmlPullParser);
                                }
                                else if (str3.equals("cleaning-package"))
                                {
                                    String str9 = localXmlPullParser.getAttributeValue(null, "name");
                                    if (str9 != null)
                                        this.mPackagesToBeCleaned.add(str9);
                                }
                                else if (str3.equals("renamed-package"))
                                {
                                    String str7 = localXmlPullParser.getAttributeValue(null, "new");
                                    String str8 = localXmlPullParser.getAttributeValue(null, "old");
                                    if ((str7 != null) && (str8 != null))
                                        this.mRenamedPackages.put(str7, str8);
                                }
                                else if (str3.equals("last-platform-version"))
                                {
                                    this.mExternalSdkPlatform = 0;
                                    this.mInternalSdkPlatform = 0;
                                    try
                                    {
                                        String str5 = localXmlPullParser.getAttributeValue(null, "internal");
                                        if (str5 != null)
                                            this.mInternalSdkPlatform = Integer.parseInt(str5);
                                        String str6 = localXmlPullParser.getAttributeValue(null, "external");
                                        if (str6 == null)
                                            continue;
                                        this.mExternalSdkPlatform = Integer.parseInt(str6);
                                    }
                                    catch (NumberFormatException localNumberFormatException)
                                    {
                                    }
                                }
                                else if (str3.equals("verifier"))
                                {
                                    String str4 = localXmlPullParser.getAttributeValue(null, "device");
                                    try
                                    {
                                        this.mVerifierDeviceIdentity = VerifierDeviceIdentity.parse(str4);
                                    }
                                    catch (IllegalArgumentException localIllegalArgumentException)
                                    {
                                        Slog.w("PackageManager", "Discard invalid verifier device id: " + localIllegalArgumentException.getMessage());
                                    }
                                }
                                else if ("read-external-storage".equals(str3))
                                {
                                    this.mReadExternalStorageEnforced = Boolean.valueOf("1".equals(localXmlPullParser.getAttributeValue(null, "enforcement")));
                                }
                                else
                                {
                                    Slog.w("PackageManager", "Unknown element under <packages>: " + localXmlPullParser.getName());
                                    XmlUtils.skipCurrentTag(localXmlPullParser);
                                    continue;
                                    label1066: localObject1.close();
                                    continue;
                                    localPackageSetting2.copyFrom(localPendingPackage);
                                    continue;
                                    label1083: if (localObject3 != null)
                                    {
                                        String str2 = "Bad package setting: package " + localPendingPackage.name + " has shared uid " + localPendingPackage.sharedId + " that is not a shared uid\n";
                                        this.mReadMessages.append(str2);
                                        PackageManagerService.reportSettingsProblem(6, str2);
                                    }
                                    else
                                    {
                                        String str1 = "Bad package setting: package " + localPendingPackage.name + " has shared uid " + localPendingPackage.sharedId + " that is not defined\n";
                                        this.mReadMessages.append(str1);
                                        PackageManagerService.reportSettingsProblem(6, str1);
                                    }
                                }
                        }
                        label1220: this.mPendingPackages.clear();
                        Iterator localIterator1 = this.mDisabledSysPackages.values().iterator();
                        while (localIterator1.hasNext())
                        {
                            PackageSetting localPackageSetting1 = (PackageSetting)localIterator1.next();
                            Object localObject2 = getUserIdLPr(localPackageSetting1.appId);
                            if ((localObject2 != null) && ((localObject2 instanceof SharedUserSetting)))
                                localPackageSetting1.sharedUser = ((SharedUserSetting)localObject2);
                        }
                        if ((this.mBackupStoppedPackagesFilename.exists()) || (this.mStoppedPackagesFilename.exists()))
                        {
                            readStoppedLPw();
                            this.mBackupStoppedPackagesFilename.delete();
                            this.mStoppedPackagesFilename.delete();
                            writePackageRestrictionsLPr(0);
                        }
                        while (true)
                        {
                            this.mReadMessages.append("Read completed successfully: " + this.mPackages.size() + " packages, " + this.mSharedUsers.size() + " shared uids\n");
                            bool = true;
                            break;
                            if (paramList == null)
                            {
                                readPackageRestrictionsLPr(0);
                            }
                            else
                            {
                                Iterator localIterator2 = paramList.iterator();
                                while (localIterator2.hasNext())
                                    readPackageRestrictionsLPr(((UserInfo)localIterator2.next()).id);
                            }
                        }
                    }
                    localIOException3 = localIOException3;
                }
            }
            catch (IOException localIOException2)
            {
                while (true)
                {
                    FileInputStream localFileInputStream2;
                    localObject1 = localFileInputStream2;
                }
            }
        }
        return bool;
    }

    // ERROR //
    void readPackageRestrictionsLPr(int paramInt)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: aload_0
        //     3: iload_1
        //     4: invokespecial 1422	com/android/server/pm/Settings:getUserPackagesStateFile	(I)Ljava/io/File;
        //     7: astore_3
        //     8: aload_0
        //     9: iload_1
        //     10: invokespecial 1424	com/android/server/pm/Settings:getUserPackagesStateBackupFile	(I)Ljava/io/File;
        //     13: astore 4
        //     15: aload 4
        //     17: invokevirtual 564	java/io/File:exists	()Z
        //     20: ifeq +827 -> 847
        //     23: new 597	java/io/FileInputStream
        //     26: dup
        //     27: aload 4
        //     29: invokespecial 598	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //     32: astore 5
        //     34: aload_0
        //     35: getfield 195	com/android/server/pm/Settings:mReadMessages	Ljava/lang/StringBuilder;
        //     38: ldc_w 1426
        //     41: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     44: pop
        //     45: iconst_4
        //     46: ldc_w 1428
        //     49: invokestatic 278	com/android/server/pm/PackageManagerService:reportSettingsProblem	(ILjava/lang/String;)V
        //     52: aload_3
        //     53: invokevirtual 564	java/io/File:exists	()Z
        //     56: ifeq +35 -> 91
        //     59: ldc_w 378
        //     62: new 192	java/lang/StringBuilder
        //     65: dup
        //     66: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     69: ldc_w 1430
        //     72: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     75: aload_3
        //     76: invokevirtual 267	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     79: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     82: invokestatic 391	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     85: pop
        //     86: aload_3
        //     87: invokevirtual 1312	java/io/File:delete	()Z
        //     90: pop
        //     91: aload 5
        //     93: ifnonnull +747 -> 840
        //     96: aload_3
        //     97: invokevirtual 564	java/io/File:exists	()Z
        //     100: ifne +164 -> 264
        //     103: aload_0
        //     104: getfield 195	com/android/server/pm/Settings:mReadMessages	Ljava/lang/StringBuilder;
        //     107: ldc_w 1432
        //     110: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     113: pop
        //     114: iconst_4
        //     115: ldc_w 1434
        //     118: invokestatic 278	com/android/server/pm/PackageManagerService:reportSettingsProblem	(ILjava/lang/String;)V
        //     121: aload_0
        //     122: getfield 161	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
        //     125: invokevirtual 932	java/util/HashMap:values	()Ljava/util/Collection;
        //     128: invokeinterface 935 1 0
        //     133: astore 34
        //     135: aload 34
        //     137: invokeinterface 444 1 0
        //     142: ifeq +116 -> 258
        //     145: aload 34
        //     147: invokeinterface 448 1 0
        //     152: checkcast 248	com/android/server/pm/PackageSetting
        //     155: astore 36
        //     157: aload 36
        //     159: iconst_0
        //     160: iload_1
        //     161: invokevirtual 457	com/android/server/pm/PackageSetting:setStopped	(ZI)V
        //     164: aload 36
        //     166: iconst_0
        //     167: iload_1
        //     168: invokevirtual 460	com/android/server/pm/PackageSetting:setNotLaunched	(ZI)V
        //     171: goto -36 -> 135
        //     174: astore 10
        //     176: aload 5
        //     178: pop
        //     179: aload_0
        //     180: getfield 195	com/android/server/pm/Settings:mReadMessages	Ljava/lang/StringBuilder;
        //     183: new 192	java/lang/StringBuilder
        //     186: dup
        //     187: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     190: ldc_w 1332
        //     193: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     196: aload 10
        //     198: invokevirtual 1333	org/xmlpull/v1/XmlPullParserException:toString	()Ljava/lang/String;
        //     201: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     204: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     207: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     210: pop
        //     211: bipush 6
        //     213: new 192	java/lang/StringBuilder
        //     216: dup
        //     217: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     220: ldc_w 1436
        //     223: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     226: aload 10
        //     228: invokevirtual 267	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     231: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     234: invokestatic 278	com/android/server/pm/PackageManagerService:reportSettingsProblem	(ILjava/lang/String;)V
        //     237: ldc_w 378
        //     240: ldc_w 1438
        //     243: aload 10
        //     245: invokestatic 1339	android/util/Log:wtf	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     248: pop
        //     249: return
        //     250: astore 41
        //     252: aload_2
        //     253: astore 5
        //     255: goto -164 -> 91
        //     258: aload 5
        //     260: pop
        //     261: goto -12 -> 249
        //     264: new 597	java/io/FileInputStream
        //     267: dup
        //     268: aload_3
        //     269: invokespecial 598	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //     272: astore 6
        //     274: invokestatic 604	android/util/Xml:newPullParser	()Lorg/xmlpull/v1/XmlPullParser;
        //     277: astore 13
        //     279: aload 13
        //     281: aload 6
        //     283: aconst_null
        //     284: invokeinterface 608 3 0
        //     289: aload 13
        //     291: invokeinterface 545 1 0
        //     296: istore 14
        //     298: iload 14
        //     300: iconst_2
        //     301: if_icmpeq +9 -> 310
        //     304: iload 14
        //     306: iconst_1
        //     307: if_icmpne -18 -> 289
        //     310: iload 14
        //     312: iconst_2
        //     313: if_icmpeq +24 -> 337
        //     316: aload_0
        //     317: getfield 195	com/android/server/pm/Settings:mReadMessages	Ljava/lang/StringBuilder;
        //     320: ldc_w 1440
        //     323: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     326: pop
        //     327: iconst_5
        //     328: ldc_w 1442
        //     331: invokestatic 278	com/android/server/pm/PackageManagerService:reportSettingsProblem	(ILjava/lang/String;)V
        //     334: goto -85 -> 249
        //     337: aload 13
        //     339: invokeinterface 543 1 0
        //     344: istore 15
        //     346: aload 13
        //     348: invokeinterface 545 1 0
        //     353: istore 16
        //     355: iload 16
        //     357: iconst_1
        //     358: if_icmpeq +458 -> 816
        //     361: iload 16
        //     363: iconst_3
        //     364: if_icmpne +15 -> 379
        //     367: aload 13
        //     369: invokeinterface 543 1 0
        //     374: iload 15
        //     376: if_icmple +440 -> 816
        //     379: iload 16
        //     381: iconst_3
        //     382: if_icmpeq -36 -> 346
        //     385: iload 16
        //     387: iconst_4
        //     388: if_icmpeq -42 -> 346
        //     391: aload 13
        //     393: invokeinterface 548 1 0
        //     398: ldc 42
        //     400: invokevirtual 551	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     403: ifeq +372 -> 775
        //     406: aload 13
        //     408: aconst_null
        //     409: ldc 16
        //     411: invokeinterface 555 3 0
        //     416: astore 18
        //     418: aload_0
        //     419: getfield 161	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
        //     422: aload 18
        //     424: invokevirtual 365	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     427: checkcast 248	com/android/server/pm/PackageSetting
        //     430: astore 19
        //     432: aload 19
        //     434: ifnonnull +114 -> 548
        //     437: ldc_w 378
        //     440: new 192	java/lang/StringBuilder
        //     443: dup
        //     444: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     447: ldc_w 1444
        //     450: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     453: aload 18
        //     455: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     458: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     461: invokestatic 391	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     464: pop
        //     465: aload 13
        //     467: invokestatic 640	com/android/internal/util/XmlUtils:skipCurrentTag	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     470: goto -124 -> 346
        //     473: astore 7
        //     475: aload_0
        //     476: getfield 195	com/android/server/pm/Settings:mReadMessages	Ljava/lang/StringBuilder;
        //     479: new 192	java/lang/StringBuilder
        //     482: dup
        //     483: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     486: ldc_w 1332
        //     489: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     492: aload 7
        //     494: invokevirtual 1351	java/io/IOException:toString	()Ljava/lang/String;
        //     497: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     500: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     503: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     506: pop
        //     507: bipush 6
        //     509: new 192	java/lang/StringBuilder
        //     512: dup
        //     513: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     516: ldc_w 1335
        //     519: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     522: aload 7
        //     524: invokevirtual 267	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     527: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     530: invokestatic 278	com/android/server/pm/PackageManagerService:reportSettingsProblem	(ILjava/lang/String;)V
        //     533: ldc_w 378
        //     536: ldc_w 1438
        //     539: aload 7
        //     541: invokestatic 1339	android/util/Log:wtf	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     544: pop
        //     545: goto -296 -> 249
        //     548: aload 13
        //     550: aconst_null
        //     551: ldc 10
        //     553: invokeinterface 555 3 0
        //     558: astore 20
        //     560: aload 20
        //     562: ifnonnull +158 -> 720
        //     565: iconst_0
        //     566: istore 21
        //     568: aload 19
        //     570: iload 21
        //     572: iload_1
        //     573: invokevirtual 1445	com/android/server/pm/PackageSetting:setEnabled	(II)V
        //     576: aload 13
        //     578: aconst_null
        //     579: ldc 22
        //     581: invokeinterface 555 3 0
        //     586: astore 22
        //     588: aload 22
        //     590: ifnonnull +140 -> 730
        //     593: iconst_0
        //     594: istore 23
        //     596: aload 19
        //     598: iload 23
        //     600: iload_1
        //     601: invokevirtual 457	com/android/server/pm/PackageSetting:setStopped	(ZI)V
        //     604: aload 13
        //     606: aconst_null
        //     607: ldc 19
        //     609: invokeinterface 555 3 0
        //     614: astore 24
        //     616: aload 22
        //     618: ifnonnull +122 -> 740
        //     621: iconst_0
        //     622: istore 25
        //     624: aload 19
        //     626: iload 25
        //     628: iload_1
        //     629: invokevirtual 460	com/android/server/pm/PackageSetting:setNotLaunched	(ZI)V
        //     632: aload 13
        //     634: invokeinterface 543 1 0
        //     639: istore 26
        //     641: aload 13
        //     643: invokeinterface 545 1 0
        //     648: istore 27
        //     650: iload 27
        //     652: iconst_1
        //     653: if_icmpeq -307 -> 346
        //     656: iload 27
        //     658: iconst_3
        //     659: if_icmpne +15 -> 674
        //     662: aload 13
        //     664: invokeinterface 543 1 0
        //     669: iload 26
        //     671: if_icmple -325 -> 346
        //     674: iload 27
        //     676: iconst_3
        //     677: if_icmpeq -36 -> 641
        //     680: iload 27
        //     682: iconst_4
        //     683: if_icmpeq -42 -> 641
        //     686: aload 13
        //     688: invokeinterface 548 1 0
        //     693: astore 28
        //     695: aload 28
        //     697: ldc 36
        //     699: invokevirtual 551	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     702: ifeq +48 -> 750
        //     705: aload 19
        //     707: aload_0
        //     708: aload 13
        //     710: invokespecial 1447	com/android/server/pm/Settings:readComponentsLPr	(Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/HashSet;
        //     713: iload_1
        //     714: invokevirtual 494	com/android/server/pm/PackageSetting:setEnabledComponents	(Ljava/util/HashSet;I)V
        //     717: goto -76 -> 641
        //     720: aload 20
        //     722: invokestatic 663	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     725: istore 21
        //     727: goto -159 -> 568
        //     730: aload 22
        //     732: invokestatic 1450	java/lang/Boolean:parseBoolean	(Ljava/lang/String;)Z
        //     735: istore 23
        //     737: goto -141 -> 596
        //     740: aload 24
        //     742: invokestatic 1450	java/lang/Boolean:parseBoolean	(Ljava/lang/String;)Z
        //     745: istore 25
        //     747: goto -123 -> 624
        //     750: aload 28
        //     752: ldc 33
        //     754: invokevirtual 551	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     757: ifeq -116 -> 641
        //     760: aload 19
        //     762: aload_0
        //     763: aload 13
        //     765: invokespecial 1447	com/android/server/pm/Settings:readComponentsLPr	(Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/HashSet;
        //     768: iload_1
        //     769: invokevirtual 488	com/android/server/pm/PackageSetting:setDisabledComponents	(Ljava/util/HashSet;I)V
        //     772: goto -131 -> 641
        //     775: ldc_w 378
        //     778: new 192	java/lang/StringBuilder
        //     781: dup
        //     782: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     785: ldc_w 1452
        //     788: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     791: aload 13
        //     793: invokeinterface 548 1 0
        //     798: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     801: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     804: invokestatic 391	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     807: pop
        //     808: aload 13
        //     810: invokestatic 640	com/android/internal/util/XmlUtils:skipCurrentTag	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     813: goto -467 -> 346
        //     816: aload 6
        //     818: invokevirtual 613	java/io/FileInputStream:close	()V
        //     821: goto -572 -> 249
        //     824: astore 7
        //     826: aload 5
        //     828: pop
        //     829: goto -354 -> 475
        //     832: astore 37
        //     834: aload 5
        //     836: astore_2
        //     837: goto -585 -> 252
        //     840: aload 5
        //     842: astore 6
        //     844: goto -570 -> 274
        //     847: aconst_null
        //     848: astore 5
        //     850: goto -759 -> 91
        //     853: astore 10
        //     855: goto -676 -> 179
        //
        // Exception table:
        //     from	to	target	type
        //     96	171	174	org/xmlpull/v1/XmlPullParserException
        //     264	274	174	org/xmlpull/v1/XmlPullParserException
        //     23	34	250	java/io/IOException
        //     274	470	473	java/io/IOException
        //     548	821	473	java/io/IOException
        //     96	171	824	java/io/IOException
        //     264	274	824	java/io/IOException
        //     34	91	832	java/io/IOException
        //     274	470	853	org/xmlpull/v1/XmlPullParserException
        //     548	821	853	org/xmlpull/v1/XmlPullParserException
    }

    // ERROR //
    void readStoppedLPw()
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_1
        //     2: aload_0
        //     3: getfield 240	com/android/server/pm/Settings:mBackupStoppedPackagesFilename	Ljava/io/File;
        //     6: invokevirtual 564	java/io/File:exists	()Z
        //     9: ifeq +641 -> 650
        //     12: new 597	java/io/FileInputStream
        //     15: dup
        //     16: aload_0
        //     17: getfield 240	com/android/server/pm/Settings:mBackupStoppedPackagesFilename	Ljava/io/File;
        //     20: invokespecial 598	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //     23: astore_2
        //     24: aload_0
        //     25: getfield 195	com/android/server/pm/Settings:mReadMessages	Ljava/lang/StringBuilder;
        //     28: ldc_w 1426
        //     31: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     34: pop
        //     35: iconst_4
        //     36: ldc_w 1428
        //     39: invokestatic 278	com/android/server/pm/PackageManagerService:reportSettingsProblem	(ILjava/lang/String;)V
        //     42: aload_0
        //     43: getfield 224	com/android/server/pm/Settings:mSettingsFilename	Ljava/io/File;
        //     46: invokevirtual 564	java/io/File:exists	()Z
        //     49: ifeq +41 -> 90
        //     52: ldc_w 378
        //     55: new 192	java/lang/StringBuilder
        //     58: dup
        //     59: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     62: ldc_w 1430
        //     65: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     68: aload_0
        //     69: getfield 236	com/android/server/pm/Settings:mStoppedPackagesFilename	Ljava/io/File;
        //     72: invokevirtual 267	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     75: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     78: invokestatic 391	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     81: pop
        //     82: aload_0
        //     83: getfield 236	com/android/server/pm/Settings:mStoppedPackagesFilename	Ljava/io/File;
        //     86: invokevirtual 1312	java/io/File:delete	()Z
        //     89: pop
        //     90: aload_2
        //     91: ifnonnull +554 -> 645
        //     94: aload_0
        //     95: getfield 236	com/android/server/pm/Settings:mStoppedPackagesFilename	Ljava/io/File;
        //     98: invokevirtual 564	java/io/File:exists	()Z
        //     101: ifne +161 -> 262
        //     104: aload_0
        //     105: getfield 195	com/android/server/pm/Settings:mReadMessages	Ljava/lang/StringBuilder;
        //     108: ldc_w 1432
        //     111: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     114: pop
        //     115: iconst_4
        //     116: ldc_w 1454
        //     119: invokestatic 278	com/android/server/pm/PackageManagerService:reportSettingsProblem	(ILjava/lang/String;)V
        //     122: aload_0
        //     123: getfield 161	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
        //     126: invokevirtual 932	java/util/HashMap:values	()Ljava/util/Collection;
        //     129: invokeinterface 935 1 0
        //     134: astore 22
        //     136: aload 22
        //     138: invokeinterface 444 1 0
        //     143: ifeq +114 -> 257
        //     146: aload 22
        //     148: invokeinterface 448 1 0
        //     153: checkcast 248	com/android/server/pm/PackageSetting
        //     156: astore 24
        //     158: aload 24
        //     160: iconst_0
        //     161: iconst_0
        //     162: invokevirtual 457	com/android/server/pm/PackageSetting:setStopped	(ZI)V
        //     165: aload 24
        //     167: iconst_0
        //     168: iconst_0
        //     169: invokevirtual 460	com/android/server/pm/PackageSetting:setNotLaunched	(ZI)V
        //     172: goto -36 -> 136
        //     175: astore 7
        //     177: aload_2
        //     178: pop
        //     179: aload_0
        //     180: getfield 195	com/android/server/pm/Settings:mReadMessages	Ljava/lang/StringBuilder;
        //     183: new 192	java/lang/StringBuilder
        //     186: dup
        //     187: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     190: ldc_w 1332
        //     193: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     196: aload 7
        //     198: invokevirtual 1333	org/xmlpull/v1/XmlPullParserException:toString	()Ljava/lang/String;
        //     201: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     204: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     207: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     210: pop
        //     211: bipush 6
        //     213: new 192	java/lang/StringBuilder
        //     216: dup
        //     217: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     220: ldc_w 1436
        //     223: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     226: aload 7
        //     228: invokevirtual 267	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     231: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     234: invokestatic 278	com/android/server/pm/PackageManagerService:reportSettingsProblem	(ILjava/lang/String;)V
        //     237: ldc_w 378
        //     240: ldc_w 1438
        //     243: aload 7
        //     245: invokestatic 1339	android/util/Log:wtf	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     248: pop
        //     249: return
        //     250: astore 29
        //     252: aload_1
        //     253: astore_2
        //     254: goto -164 -> 90
        //     257: aload_2
        //     258: pop
        //     259: goto -10 -> 249
        //     262: new 597	java/io/FileInputStream
        //     265: dup
        //     266: aload_0
        //     267: getfield 236	com/android/server/pm/Settings:mStoppedPackagesFilename	Ljava/io/File;
        //     270: invokespecial 598	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //     273: astore_3
        //     274: invokestatic 604	android/util/Xml:newPullParser	()Lorg/xmlpull/v1/XmlPullParser;
        //     277: astore 10
        //     279: aload 10
        //     281: aload_3
        //     282: aconst_null
        //     283: invokeinterface 608 3 0
        //     288: aload 10
        //     290: invokeinterface 545 1 0
        //     295: istore 11
        //     297: iload 11
        //     299: iconst_2
        //     300: if_icmpeq +9 -> 309
        //     303: iload 11
        //     305: iconst_1
        //     306: if_icmpne -18 -> 288
        //     309: iload 11
        //     311: iconst_2
        //     312: if_icmpeq +24 -> 336
        //     315: aload_0
        //     316: getfield 195	com/android/server/pm/Settings:mReadMessages	Ljava/lang/StringBuilder;
        //     319: ldc_w 1456
        //     322: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     325: pop
        //     326: iconst_5
        //     327: ldc_w 1442
        //     330: invokestatic 278	com/android/server/pm/PackageManagerService:reportSettingsProblem	(ILjava/lang/String;)V
        //     333: goto -84 -> 249
        //     336: aload 10
        //     338: invokeinterface 543 1 0
        //     343: istore 12
        //     345: aload 10
        //     347: invokeinterface 545 1 0
        //     352: istore 13
        //     354: iload 13
        //     356: iconst_1
        //     357: if_icmpeq +267 -> 624
        //     360: iload 13
        //     362: iconst_3
        //     363: if_icmpne +15 -> 378
        //     366: aload 10
        //     368: invokeinterface 543 1 0
        //     373: iload 12
        //     375: if_icmple +249 -> 624
        //     378: iload 13
        //     380: iconst_3
        //     381: if_icmpeq -36 -> 345
        //     384: iload 13
        //     386: iconst_4
        //     387: if_icmpeq -42 -> 345
        //     390: aload 10
        //     392: invokeinterface 548 1 0
        //     397: ldc 42
        //     399: invokevirtual 551	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     402: ifeq +181 -> 583
        //     405: aload 10
        //     407: aconst_null
        //     408: ldc 16
        //     410: invokeinterface 555 3 0
        //     415: astore 15
        //     417: aload_0
        //     418: getfield 161	com/android/server/pm/Settings:mPackages	Ljava/util/HashMap;
        //     421: aload 15
        //     423: invokevirtual 365	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     426: checkcast 248	com/android/server/pm/PackageSetting
        //     429: astore 16
        //     431: aload 16
        //     433: ifnull +119 -> 552
        //     436: aload 16
        //     438: iconst_1
        //     439: iconst_0
        //     440: invokevirtual 457	com/android/server/pm/PackageSetting:setStopped	(ZI)V
        //     443: ldc_w 1396
        //     446: aload 10
        //     448: aconst_null
        //     449: ldc 19
        //     451: invokeinterface 555 3 0
        //     456: invokevirtual 551	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     459: ifeq +10 -> 469
        //     462: aload 16
        //     464: iconst_1
        //     465: iconst_0
        //     466: invokevirtual 460	com/android/server/pm/PackageSetting:setNotLaunched	(ZI)V
        //     469: aload 10
        //     471: invokestatic 640	com/android/internal/util/XmlUtils:skipCurrentTag	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     474: goto -129 -> 345
        //     477: astore 4
        //     479: aload_0
        //     480: getfield 195	com/android/server/pm/Settings:mReadMessages	Ljava/lang/StringBuilder;
        //     483: new 192	java/lang/StringBuilder
        //     486: dup
        //     487: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     490: ldc_w 1332
        //     493: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     496: aload 4
        //     498: invokevirtual 1351	java/io/IOException:toString	()Ljava/lang/String;
        //     501: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     504: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     507: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     510: pop
        //     511: bipush 6
        //     513: new 192	java/lang/StringBuilder
        //     516: dup
        //     517: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     520: ldc_w 1335
        //     523: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     526: aload 4
        //     528: invokevirtual 267	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     531: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     534: invokestatic 278	com/android/server/pm/PackageManagerService:reportSettingsProblem	(ILjava/lang/String;)V
        //     537: ldc_w 378
        //     540: ldc_w 1438
        //     543: aload 4
        //     545: invokestatic 1339	android/util/Log:wtf	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     548: pop
        //     549: goto -300 -> 249
        //     552: ldc_w 378
        //     555: new 192	java/lang/StringBuilder
        //     558: dup
        //     559: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     562: ldc_w 1444
        //     565: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     568: aload 15
        //     570: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     573: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     576: invokestatic 391	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     579: pop
        //     580: goto -111 -> 469
        //     583: ldc_w 378
        //     586: new 192	java/lang/StringBuilder
        //     589: dup
        //     590: invokespecial 193	java/lang/StringBuilder:<init>	()V
        //     593: ldc_w 1452
        //     596: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     599: aload 10
        //     601: invokeinterface 548 1 0
        //     606: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     609: invokevirtual 272	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     612: invokestatic 391	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     615: pop
        //     616: aload 10
        //     618: invokestatic 640	com/android/internal/util/XmlUtils:skipCurrentTag	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     621: goto -276 -> 345
        //     624: aload_3
        //     625: invokevirtual 613	java/io/FileInputStream:close	()V
        //     628: goto -379 -> 249
        //     631: astore 4
        //     633: aload_2
        //     634: pop
        //     635: goto -156 -> 479
        //     638: astore 25
        //     640: aload_2
        //     641: astore_1
        //     642: goto -390 -> 252
        //     645: aload_2
        //     646: astore_3
        //     647: goto -373 -> 274
        //     650: aconst_null
        //     651: astore_2
        //     652: goto -562 -> 90
        //     655: astore 7
        //     657: goto -478 -> 179
        //
        // Exception table:
        //     from	to	target	type
        //     94	172	175	org/xmlpull/v1/XmlPullParserException
        //     262	274	175	org/xmlpull/v1/XmlPullParserException
        //     12	24	250	java/io/IOException
        //     274	474	477	java/io/IOException
        //     552	628	477	java/io/IOException
        //     94	172	631	java/io/IOException
        //     262	274	631	java/io/IOException
        //     24	90	638	java/io/IOException
        //     274	474	655	org/xmlpull/v1/XmlPullParserException
        //     552	628	655	org/xmlpull/v1/XmlPullParserException
    }

    void removeDisabledSystemPackageLPw(String paramString)
    {
        this.mDisabledSysPackages.remove(paramString);
    }

    int removePackageLPw(String paramString)
    {
        PackageSetting localPackageSetting = (PackageSetting)this.mPackages.get(paramString);
        int i;
        if (localPackageSetting != null)
        {
            this.mPackages.remove(paramString);
            if (localPackageSetting.sharedUser != null)
            {
                localPackageSetting.sharedUser.packages.remove(localPackageSetting);
                if (localPackageSetting.sharedUser.packages.size() != 0)
                    break label109;
                this.mSharedUsers.remove(localPackageSetting.sharedUser.name);
                removeUserIdLPw(localPackageSetting.sharedUser.userId);
                i = localPackageSetting.sharedUser.userId;
            }
        }
        while (true)
        {
            return i;
            removeUserIdLPw(localPackageSetting.appId);
            i = localPackageSetting.appId;
            continue;
            label109: i = -1;
        }
    }

    void removeUserLPr(int paramInt)
    {
        getUserPackagesStateFile(paramInt).delete();
        getUserPackagesStateBackupFile(paramInt).delete();
    }

    void setInstallStatus(String paramString, int paramInt)
    {
        PackageSetting localPackageSetting = (PackageSetting)this.mPackages.get(paramString);
        if ((localPackageSetting != null) && (localPackageSetting.getInstallStatus() != paramInt))
            localPackageSetting.setInstallStatus(paramInt);
    }

    void setInstallerPackageName(String paramString1, String paramString2)
    {
        PackageSetting localPackageSetting = (PackageSetting)this.mPackages.get(paramString1);
        if (localPackageSetting != null)
            localPackageSetting.setInstallerPackageName(paramString2);
    }

    boolean setPackageStoppedStateLPw(String paramString, boolean paramBoolean1, boolean paramBoolean2, int paramInt1, int paramInt2)
    {
        int i = UserId.getAppId(paramInt1);
        PackageSetting localPackageSetting = (PackageSetting)this.mPackages.get(paramString);
        if (localPackageSetting == null)
            throw new IllegalArgumentException("Unknown package: " + paramString);
        if ((!paramBoolean2) && (i != localPackageSetting.appId))
            throw new SecurityException("Permission Denial: attempt to change stopped state from pid=" + Binder.getCallingPid() + ", uid=" + paramInt1 + ", package uid=" + localPackageSetting.appId);
        if (localPackageSetting.getStopped(paramInt2) != paramBoolean1)
        {
            localPackageSetting.setStopped(paramBoolean1, paramInt2);
            if (localPackageSetting.getNotLaunched(paramInt2))
            {
                if (localPackageSetting.installerPackageName != null)
                    PackageManagerService.sendPackageBroadcast("android.intent.action.PACKAGE_FIRST_LAUNCH", localPackageSetting.name, null, localPackageSetting.installerPackageName, null, paramInt2);
                localPackageSetting.setNotLaunched(false, paramInt2);
            }
        }
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    void transferPermissionsLPw(String paramString1, String paramString2)
    {
        for (int i = 0; i < 2; i++)
        {
            if (i == 0);
            for (HashMap localHashMap = this.mPermissionTrees; ; localHashMap = this.mPermissions)
            {
                Iterator localIterator = localHashMap.values().iterator();
                while (localIterator.hasNext())
                {
                    BasePermission localBasePermission = (BasePermission)localIterator.next();
                    if (paramString1.equals(localBasePermission.sourcePackage))
                    {
                        localBasePermission.sourcePackage = paramString2;
                        localBasePermission.packageSetting = null;
                        localBasePermission.perm = null;
                        if (localBasePermission.pendingInfo != null)
                            localBasePermission.pendingInfo.packageName = paramString2;
                        localBasePermission.uid = 0;
                        localBasePermission.gids = null;
                    }
                }
            }
        }
    }

    void updateSharedUserPermsLPw(PackageSetting paramPackageSetting, int[] paramArrayOfInt)
    {
        if ((paramPackageSetting == null) || (paramPackageSetting.pkg == null))
            Slog.i("PackageManager", "Trying to update info for null package. Just ignoring");
        while (true)
        {
            return;
            if (paramPackageSetting.sharedUser != null)
            {
                SharedUserSetting localSharedUserSetting = paramPackageSetting.sharedUser;
                Iterator localIterator1 = paramPackageSetting.pkg.requestedPermissions.iterator();
                while (localIterator1.hasNext())
                {
                    String str2 = (String)localIterator1.next();
                    int i = 0;
                    if (localSharedUserSetting.grantedPermissions.contains(str2))
                    {
                        Iterator localIterator3 = localSharedUserSetting.packages.iterator();
                        while (localIterator3.hasNext())
                        {
                            PackageSetting localPackageSetting = (PackageSetting)localIterator3.next();
                            if ((localPackageSetting.pkg != null) && (!localPackageSetting.pkg.packageName.equals(paramPackageSetting.pkg.packageName)) && (localPackageSetting.pkg.requestedPermissions.contains(str2)))
                                i = 1;
                        }
                        if (i == 0)
                            localSharedUserSetting.grantedPermissions.remove(str2);
                    }
                }
                int[] arrayOfInt = paramArrayOfInt;
                Iterator localIterator2 = localSharedUserSetting.grantedPermissions.iterator();
                while (localIterator2.hasNext())
                {
                    String str1 = (String)localIterator2.next();
                    BasePermission localBasePermission = (BasePermission)this.mPermissions.get(str1);
                    if (localBasePermission != null)
                        arrayOfInt = PackageManagerService.appendInts(arrayOfInt, localBasePermission.gids);
                }
                localSharedUserSetting.gids = arrayOfInt;
            }
        }
    }

    void writeAllUsersPackageRestrictionsLPr()
    {
        List localList = getAllUsers();
        if (localList == null);
        while (true)
        {
            return;
            Iterator localIterator = localList.iterator();
            while (localIterator.hasNext())
                writePackageRestrictionsLPr(((UserInfo)localIterator.next()).id);
        }
    }

    void writeDisabledSysPackageLPr(XmlSerializer paramXmlSerializer, PackageSetting paramPackageSetting)
        throws IOException
    {
        paramXmlSerializer.startTag(null, "updated-package");
        paramXmlSerializer.attribute(null, "name", paramPackageSetting.name);
        if (paramPackageSetting.realName != null)
            paramXmlSerializer.attribute(null, "realName", paramPackageSetting.realName);
        paramXmlSerializer.attribute(null, "codePath", paramPackageSetting.codePathString);
        paramXmlSerializer.attribute(null, "ft", Long.toHexString(paramPackageSetting.timeStamp));
        paramXmlSerializer.attribute(null, "it", Long.toHexString(paramPackageSetting.firstInstallTime));
        paramXmlSerializer.attribute(null, "ut", Long.toHexString(paramPackageSetting.lastUpdateTime));
        paramXmlSerializer.attribute(null, "version", String.valueOf(paramPackageSetting.versionCode));
        if (!paramPackageSetting.resourcePathString.equals(paramPackageSetting.codePathString))
            paramXmlSerializer.attribute(null, "resourcePath", paramPackageSetting.resourcePathString);
        if (paramPackageSetting.nativeLibraryPathString != null)
            paramXmlSerializer.attribute(null, "nativeLibraryPath", paramPackageSetting.nativeLibraryPathString);
        if (paramPackageSetting.sharedUser == null)
            paramXmlSerializer.attribute(null, "userId", Integer.toString(paramPackageSetting.appId));
        while (true)
        {
            paramXmlSerializer.startTag(null, "perms");
            if (paramPackageSetting.sharedUser != null)
                break;
            Iterator localIterator = paramPackageSetting.grantedPermissions.iterator();
            while (localIterator.hasNext())
            {
                String str = (String)localIterator.next();
                if ((BasePermission)this.mPermissions.get(str) != null)
                {
                    paramXmlSerializer.startTag(null, "item");
                    paramXmlSerializer.attribute(null, "name", str);
                    paramXmlSerializer.endTag(null, "item");
                }
            }
            paramXmlSerializer.attribute(null, "sharedUserId", Integer.toString(paramPackageSetting.appId));
        }
        paramXmlSerializer.endTag(null, "perms");
        paramXmlSerializer.endTag(null, "updated-package");
    }

    void writeLPr()
    {
        if (this.mSettingsFilename.exists())
            if (!this.mBackupSettingsFilename.exists())
            {
                if (this.mSettingsFilename.renameTo(this.mBackupSettingsFilename))
                    break label63;
                Log.wtf("PackageManager", "Unable to backup package manager settings,    current changes will be lost at reboot");
            }
        label63: JournaledFile localJournaledFile;
        FileOutputStream localFileOutputStream2;
        BufferedOutputStream localBufferedOutputStream2;
        while (true)
        {
            return;
            this.mSettingsFilename.delete();
            Slog.w("PackageManager", "Preserving older settings backup");
            this.mPastSignatures.clear();
            try
            {
                localFileOutputStream1 = new FileOutputStream(this.mSettingsFilename);
                localBufferedOutputStream1 = new BufferedOutputStream(localFileOutputStream1);
                localFastXmlSerializer = new FastXmlSerializer();
                localFastXmlSerializer.setOutput(localBufferedOutputStream1, "utf-8");
                localFastXmlSerializer.startDocument(null, Boolean.valueOf(true));
                localFastXmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
                localFastXmlSerializer.startTag(null, "packages");
                localFastXmlSerializer.startTag(null, "last-platform-version");
                localFastXmlSerializer.attribute(null, "internal", Integer.toString(this.mInternalSdkPlatform));
                localFastXmlSerializer.attribute(null, "external", Integer.toString(this.mExternalSdkPlatform));
                localFastXmlSerializer.endTag(null, "last-platform-version");
                if (this.mVerifierDeviceIdentity != null)
                {
                    localFastXmlSerializer.startTag(null, "verifier");
                    localFastXmlSerializer.attribute(null, "device", this.mVerifierDeviceIdentity.toString());
                    localFastXmlSerializer.endTag(null, "verifier");
                }
                if (this.mReadExternalStorageEnforced != null)
                {
                    localFastXmlSerializer.startTag(null, "read-external-storage");
                    if (this.mReadExternalStorageEnforced.booleanValue())
                    {
                        str4 = "1";
                        localFastXmlSerializer.attribute(null, "enforcement", str4);
                        localFastXmlSerializer.endTag(null, "read-external-storage");
                    }
                }
                else
                {
                    localFastXmlSerializer.startTag(null, "permission-trees");
                    Iterator localIterator1 = this.mPermissionTrees.values().iterator();
                    while (localIterator1.hasNext())
                        writePermissionLPr(localFastXmlSerializer, (BasePermission)localIterator1.next());
                }
            }
            catch (XmlPullParserException localXmlPullParserException)
            {
                while (true)
                {
                    Log.wtf("PackageManager", "Unable to write package manager settings, current changes will be lost at reboot", localXmlPullParserException);
                    if ((!this.mSettingsFilename.exists()) || (this.mSettingsFilename.delete()))
                        break;
                    Log.wtf("PackageManager", "Failed to clean up mangled file: " + this.mSettingsFilename);
                    break;
                    String str4 = "0";
                }
                localFastXmlSerializer.endTag(null, "permission-trees");
                localFastXmlSerializer.startTag(null, "permissions");
                Iterator localIterator2 = this.mPermissions.values().iterator();
                while (localIterator2.hasNext())
                    writePermissionLPr(localFastXmlSerializer, (BasePermission)localIterator2.next());
            }
            catch (IOException localIOException)
            {
                FileOutputStream localFileOutputStream1;
                BufferedOutputStream localBufferedOutputStream1;
                FastXmlSerializer localFastXmlSerializer;
                while (true)
                    Log.wtf("PackageManager", "Unable to write package manager settings, current changes will be lost at reboot", localIOException);
                localFastXmlSerializer.endTag(null, "permissions");
                Iterator localIterator3 = this.mPackages.values().iterator();
                while (localIterator3.hasNext())
                    writePackageLPr(localFastXmlSerializer, (PackageSetting)localIterator3.next());
                Iterator localIterator4 = this.mDisabledSysPackages.values().iterator();
                while (localIterator4.hasNext())
                    writeDisabledSysPackageLPr(localFastXmlSerializer, (PackageSetting)localIterator4.next());
                writePreferredActivitiesLPr(localFastXmlSerializer);
                Iterator localIterator5 = this.mSharedUsers.values().iterator();
                while (localIterator5.hasNext())
                {
                    SharedUserSetting localSharedUserSetting = (SharedUserSetting)localIterator5.next();
                    localFastXmlSerializer.startTag(null, "shared-user");
                    localFastXmlSerializer.attribute(null, "name", localSharedUserSetting.name);
                    localFastXmlSerializer.attribute(null, "userId", Integer.toString(localSharedUserSetting.userId));
                    localSharedUserSetting.signatures.writeXml(localFastXmlSerializer, "sigs", this.mPastSignatures);
                    localFastXmlSerializer.startTag(null, "perms");
                    Iterator localIterator8 = localSharedUserSetting.grantedPermissions.iterator();
                    while (localIterator8.hasNext())
                    {
                        String str3 = (String)localIterator8.next();
                        localFastXmlSerializer.startTag(null, "item");
                        localFastXmlSerializer.attribute(null, "name", str3);
                        localFastXmlSerializer.endTag(null, "item");
                    }
                    localFastXmlSerializer.endTag(null, "perms");
                    localFastXmlSerializer.endTag(null, "shared-user");
                }
                if (this.mPackagesToBeCleaned.size() > 0)
                    for (int j = 0; j < this.mPackagesToBeCleaned.size(); j++)
                    {
                        localFastXmlSerializer.startTag(null, "cleaning-package");
                        localFastXmlSerializer.attribute(null, "name", (String)this.mPackagesToBeCleaned.get(j));
                        localFastXmlSerializer.endTag(null, "cleaning-package");
                    }
                if (this.mRenamedPackages.size() > 0)
                {
                    Iterator localIterator7 = this.mRenamedPackages.entrySet().iterator();
                    while (localIterator7.hasNext())
                    {
                        Map.Entry localEntry = (Map.Entry)localIterator7.next();
                        localFastXmlSerializer.startTag(null, "renamed-package");
                        localFastXmlSerializer.attribute(null, "new", (String)localEntry.getKey());
                        localFastXmlSerializer.attribute(null, "old", (String)localEntry.getValue());
                        localFastXmlSerializer.endTag(null, "renamed-package");
                    }
                }
                localFastXmlSerializer.endTag(null, "packages");
                localFastXmlSerializer.endDocument();
                localBufferedOutputStream1.flush();
                FileUtils.sync(localFileOutputStream1);
                localBufferedOutputStream1.close();
                this.mBackupSettingsFilename.delete();
                FileUtils.setPermissions(this.mSettingsFilename.toString(), 432, -1, -1);
                File localFile = new File(this.mPackageListFilename.toString() + ".tmp");
                localJournaledFile = new JournaledFile(this.mPackageListFilename, localFile);
                localFileOutputStream2 = new FileOutputStream(localJournaledFile.chooseForWrite());
                localBufferedOutputStream2 = new BufferedOutputStream(localFileOutputStream2);
            }
        }
        int i;
        label1199: String str2;
        try
        {
            StringBuilder localStringBuilder = new StringBuilder();
            Iterator localIterator6 = this.mPackages.values().iterator();
            while (true)
            {
                if (!localIterator6.hasNext())
                    break label1360;
                ApplicationInfo localApplicationInfo = ((PackageSetting)localIterator6.next()).pkg.applicationInfo;
                String str1 = localApplicationInfo.dataDir;
                if ((0x2 & localApplicationInfo.flags) == 0)
                    break;
                i = 1;
                if ((str1.indexOf(" ") < 0) && (localApplicationInfo.uid > 10000))
                {
                    localStringBuilder.setLength(0);
                    localStringBuilder.append(localApplicationInfo.packageName);
                    localStringBuilder.append(" ");
                    localStringBuilder.append(localApplicationInfo.uid);
                    if (i == 0)
                        break label1352;
                    str2 = " 1 ";
                    label1268: localStringBuilder.append(str2);
                    localStringBuilder.append(str1);
                    localStringBuilder.append("\n");
                    localBufferedOutputStream2.write(localStringBuilder.toString().getBytes());
                }
            }
        }
        catch (Exception localException)
        {
            IoUtils.closeQuietly(localBufferedOutputStream2);
            localJournaledFile.rollback();
        }
        while (true)
        {
            FileUtils.setPermissions(this.mPackageListFilename.toString(), 432, -1, -1);
            writeAllUsersPackageRestrictionsLPr();
            break;
            i = 0;
            break label1199;
            label1352: str2 = " 0 ";
            break label1268;
            label1360: localBufferedOutputStream2.flush();
            FileUtils.sync(localFileOutputStream2);
            localBufferedOutputStream2.close();
            localJournaledFile.commit();
        }
    }

    void writePackageLPr(XmlSerializer paramXmlSerializer, PackageSetting paramPackageSetting)
        throws IOException
    {
        paramXmlSerializer.startTag(null, "package");
        paramXmlSerializer.attribute(null, "name", paramPackageSetting.name);
        if (paramPackageSetting.realName != null)
            paramXmlSerializer.attribute(null, "realName", paramPackageSetting.realName);
        paramXmlSerializer.attribute(null, "codePath", paramPackageSetting.codePathString);
        if (!paramPackageSetting.resourcePathString.equals(paramPackageSetting.codePathString))
            paramXmlSerializer.attribute(null, "resourcePath", paramPackageSetting.resourcePathString);
        if (paramPackageSetting.nativeLibraryPathString != null)
            paramXmlSerializer.attribute(null, "nativeLibraryPath", paramPackageSetting.nativeLibraryPathString);
        paramXmlSerializer.attribute(null, "flags", Integer.toString(paramPackageSetting.pkgFlags));
        paramXmlSerializer.attribute(null, "ft", Long.toHexString(paramPackageSetting.timeStamp));
        paramXmlSerializer.attribute(null, "it", Long.toHexString(paramPackageSetting.firstInstallTime));
        paramXmlSerializer.attribute(null, "ut", Long.toHexString(paramPackageSetting.lastUpdateTime));
        paramXmlSerializer.attribute(null, "version", String.valueOf(paramPackageSetting.versionCode));
        if (paramPackageSetting.sharedUser == null)
            paramXmlSerializer.attribute(null, "userId", Integer.toString(paramPackageSetting.appId));
        while (true)
        {
            if (paramPackageSetting.uidError)
                paramXmlSerializer.attribute(null, "uidError", "true");
            if (paramPackageSetting.installStatus == 0)
                paramXmlSerializer.attribute(null, "installStatus", "false");
            if (paramPackageSetting.installerPackageName != null)
                paramXmlSerializer.attribute(null, "installer", paramPackageSetting.installerPackageName);
            paramPackageSetting.signatures.writeXml(paramXmlSerializer, "sigs", this.mPastSignatures);
            if ((0x1 & paramPackageSetting.pkgFlags) != 0)
                break label432;
            paramXmlSerializer.startTag(null, "perms");
            if (paramPackageSetting.sharedUser != null)
                break;
            Iterator localIterator = paramPackageSetting.grantedPermissions.iterator();
            while (localIterator.hasNext())
            {
                String str = (String)localIterator.next();
                paramXmlSerializer.startTag(null, "item");
                paramXmlSerializer.attribute(null, "name", str);
                paramXmlSerializer.endTag(null, "item");
            }
            paramXmlSerializer.attribute(null, "sharedUserId", Integer.toString(paramPackageSetting.appId));
        }
        paramXmlSerializer.endTag(null, "perms");
        label432: paramXmlSerializer.endTag(null, "package");
    }

    void writePackageRestrictionsLPr(int paramInt)
    {
        File localFile1 = getUserPackagesStateFile(paramInt);
        File localFile2 = getUserPackagesStateBackupFile(paramInt);
        new File(localFile1.getParent()).mkdirs();
        if (localFile1.exists())
            if (!localFile2.exists())
            {
                if (localFile1.renameTo(localFile2))
                    break label75;
                Log.wtf("PackageManager", "Unable to backup user packages state file, current changes will be lost at reboot");
            }
        while (true)
        {
            return;
            localFile1.delete();
            Slog.w("PackageManager", "Preserving older stopped packages backup");
            label75: FileOutputStream localFileOutputStream;
            BufferedOutputStream localBufferedOutputStream;
            FastXmlSerializer localFastXmlSerializer;
            while (true)
            {
                HashSet localHashSet2;
                try
                {
                    localFileOutputStream = new FileOutputStream(localFile1);
                    localBufferedOutputStream = new BufferedOutputStream(localFileOutputStream);
                    localFastXmlSerializer = new FastXmlSerializer();
                    localFastXmlSerializer.setOutput(localBufferedOutputStream, "utf-8");
                    localFastXmlSerializer.startDocument(null, Boolean.valueOf(true));
                    localFastXmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
                    localFastXmlSerializer.startTag(null, "package-restrictions");
                    Iterator localIterator1 = this.mPackages.values().iterator();
                    if (!localIterator1.hasNext())
                        break label633;
                    PackageSetting localPackageSetting = (PackageSetting)localIterator1.next();
                    if ((!localPackageSetting.getStopped(paramInt)) && (!localPackageSetting.getNotLaunched(paramInt)) && (localPackageSetting.getEnabled(paramInt) == 0) && (localPackageSetting.getEnabledComponents(paramInt).size() <= 0) && (localPackageSetting.getDisabledComponents(paramInt).size() <= 0))
                        continue;
                    localFastXmlSerializer.startTag(null, "pkg");
                    localFastXmlSerializer.attribute(null, "name", localPackageSetting.name);
                    boolean bool1 = localPackageSetting.getStopped(paramInt);
                    boolean bool2 = localPackageSetting.getNotLaunched(paramInt);
                    int i = localPackageSetting.getEnabled(paramInt);
                    HashSet localHashSet1 = localPackageSetting.getEnabledComponents(paramInt);
                    localHashSet2 = localPackageSetting.getDisabledComponents(paramInt);
                    if (bool1)
                        localFastXmlSerializer.attribute(null, "stopped", "true");
                    if (bool2)
                        localFastXmlSerializer.attribute(null, "nl", "true");
                    if (i != 0)
                        localFastXmlSerializer.attribute(null, "enabled", Integer.toString(i));
                    if (localHashSet1.size() <= 0)
                        break label522;
                    localFastXmlSerializer.startTag(null, "enabled-components");
                    Iterator localIterator3 = localHashSet1.iterator();
                    if (!localIterator3.hasNext())
                        break label511;
                    String str2 = (String)localIterator3.next();
                    localFastXmlSerializer.startTag(null, "item");
                    localFastXmlSerializer.attribute(null, "name", str2);
                    localFastXmlSerializer.endTag(null, "item");
                    continue;
                }
                catch (IOException localIOException)
                {
                    Log.wtf("PackageManager", "Unable to write package manager user packages state,    current changes will be lost at reboot", localIOException);
                }
                if ((!localFile1.exists()) || (localFile1.delete()))
                    break;
                Log.i("PackageManager", "Failed to clean up mangled file: " + this.mStoppedPackagesFilename);
                break;
                label511: localFastXmlSerializer.endTag(null, "enabled-components");
                label522: if (localHashSet2.size() > 0)
                {
                    localFastXmlSerializer.startTag(null, "disabled-components");
                    Iterator localIterator2 = localHashSet2.iterator();
                    while (localIterator2.hasNext())
                    {
                        String str1 = (String)localIterator2.next();
                        localFastXmlSerializer.startTag(null, "item");
                        localFastXmlSerializer.attribute(null, "name", str1);
                        localFastXmlSerializer.endTag(null, "item");
                    }
                    localFastXmlSerializer.endTag(null, "disabled-components");
                }
                localFastXmlSerializer.endTag(null, "pkg");
            }
            label633: localFastXmlSerializer.endTag(null, "package-restrictions");
            localFastXmlSerializer.endDocument();
            localBufferedOutputStream.flush();
            FileUtils.sync(localFileOutputStream);
            localBufferedOutputStream.close();
            localFile2.delete();
            FileUtils.setPermissions(localFile1.toString(), 432, -1, -1);
        }
    }

    void writePermissionLPr(XmlSerializer paramXmlSerializer, BasePermission paramBasePermission)
        throws XmlPullParserException, IOException
    {
        if ((paramBasePermission.type != 1) && (paramBasePermission.sourcePackage != null))
        {
            paramXmlSerializer.startTag(null, "item");
            paramXmlSerializer.attribute(null, "name", paramBasePermission.name);
            paramXmlSerializer.attribute(null, "package", paramBasePermission.sourcePackage);
            if (paramBasePermission.protectionLevel != 0)
                paramXmlSerializer.attribute(null, "protection", Integer.toString(paramBasePermission.protectionLevel));
            if (paramBasePermission.type == 2)
                if (paramBasePermission.perm == null)
                    break label187;
        }
        label187: for (PermissionInfo localPermissionInfo = paramBasePermission.perm.info; ; localPermissionInfo = paramBasePermission.pendingInfo)
        {
            if (localPermissionInfo != null)
            {
                paramXmlSerializer.attribute(null, "type", "dynamic");
                if (localPermissionInfo.icon != 0)
                    paramXmlSerializer.attribute(null, "icon", Integer.toString(localPermissionInfo.icon));
                if (localPermissionInfo.nonLocalizedLabel != null)
                    paramXmlSerializer.attribute(null, "label", localPermissionInfo.nonLocalizedLabel.toString());
            }
            paramXmlSerializer.endTag(null, "item");
            return;
        }
    }

    void writePreferredActivitiesLPr(XmlSerializer paramXmlSerializer)
        throws IllegalArgumentException, IllegalStateException, IOException
    {
        paramXmlSerializer.startTag(null, "preferred-activities");
        Iterator localIterator = this.mPreferredActivities.filterSet().iterator();
        while (localIterator.hasNext())
        {
            PreferredActivity localPreferredActivity = (PreferredActivity)localIterator.next();
            paramXmlSerializer.startTag(null, "item");
            localPreferredActivity.writeToXml(paramXmlSerializer);
            paramXmlSerializer.endTag(null, "item");
        }
        paramXmlSerializer.endTag(null, "preferred-activities");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.pm.Settings
 * JD-Core Version:        0.6.2
 */