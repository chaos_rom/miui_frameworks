package com.android.server.pm;

import android.content.pm.PackageStats;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.net.LocalSocketAddress.Namespace;
import android.util.Slog;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

class Installer
{
    private static final boolean LOCAL_DEBUG = false;
    private static final String TAG = "Installer";
    byte[] buf = new byte[1024];
    int buflen = 0;
    InputStream mIn;
    OutputStream mOut;
    LocalSocket mSocket;

    private boolean connect()
    {
        boolean bool = true;
        if (this.mSocket != null);
        while (true)
        {
            return bool;
            Slog.i("Installer", "connecting...");
            try
            {
                this.mSocket = new LocalSocket();
                LocalSocketAddress localLocalSocketAddress = new LocalSocketAddress("installd", LocalSocketAddress.Namespace.RESERVED);
                this.mSocket.connect(localLocalSocketAddress);
                this.mIn = this.mSocket.getInputStream();
                this.mOut = this.mSocket.getOutputStream();
            }
            catch (IOException localIOException)
            {
                disconnect();
                bool = false;
            }
        }
    }

    private void disconnect()
    {
        Slog.i("Installer", "disconnecting...");
        try
        {
            if (this.mSocket != null)
                this.mSocket.close();
            try
            {
                label22: if (this.mIn != null)
                    this.mIn.close();
                try
                {
                    label36: if (this.mOut != null)
                        this.mOut.close();
                    label50: this.mSocket = null;
                    this.mIn = null;
                    this.mOut = null;
                    return;
                }
                catch (IOException localIOException3)
                {
                    break label50;
                }
            }
            catch (IOException localIOException2)
            {
                break label36;
            }
        }
        catch (IOException localIOException1)
        {
            break label22;
        }
    }

    private int execute(String paramString)
    {
        String str = transaction(paramString);
        try
        {
            int j = Integer.parseInt(str);
            i = j;
            return i;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            while (true)
                int i = -1;
        }
    }

    private boolean readBytes(byte[] paramArrayOfByte, int paramInt)
    {
        boolean bool = false;
        int i = 0;
        if (paramInt < 0);
        while (true)
        {
            return bool;
            label11: int j;
            i += j;
            if (i != paramInt);
            try
            {
                j = this.mIn.read(paramArrayOfByte, i, paramInt - i);
                if (j > 0)
                    break label11;
                Slog.e("Installer", "read error " + j);
                if (i == paramInt)
                    bool = true;
            }
            catch (IOException localIOException)
            {
                while (true)
                    Slog.e("Installer", "read exception");
                disconnect();
            }
        }
    }

    private boolean readReply()
    {
        boolean bool = false;
        this.buflen = 0;
        if (!readBytes(this.buf, 2));
        while (true)
        {
            return bool;
            int i = 0xFF & this.buf[0] | (0xFF & this.buf[1]) << 8;
            if ((i < 1) || (i > 1024))
            {
                Slog.e("Installer", "invalid reply length (" + i + ")");
                disconnect();
            }
            else if (readBytes(this.buf, i))
            {
                this.buflen = i;
                bool = true;
            }
        }
    }

    /** @deprecated */
    private String transaction(String paramString)
    {
        while (true)
        {
            try
            {
                if (!connect())
                {
                    Slog.e("Installer", "connection failed");
                    str = "-1";
                    return str;
                }
                if (!writeCommand(paramString))
                {
                    Slog.e("Installer", "write command failed? reconnect!");
                    if ((!connect()) || (!writeCommand(paramString)));
                }
                else
                {
                    if (readReply())
                    {
                        str = new String(this.buf, 0, this.buflen);
                        continue;
                    }
                    str = "-1";
                    continue;
                }
            }
            finally
            {
            }
            String str = "-1";
        }
    }

    private boolean writeCommand(String paramString)
    {
        int i = 1;
        byte[] arrayOfByte = paramString.getBytes();
        int j = arrayOfByte.length;
        if ((j < i) || (j > 1024))
            i = 0;
        while (true)
        {
            return i;
            this.buf[0] = ((byte)(j & 0xFF));
            this.buf[i] = ((byte)(0xFF & j >> 8));
            try
            {
                this.mOut.write(this.buf, 0, 2);
                this.mOut.write(arrayOfByte, 0, j);
            }
            catch (IOException localIOException)
            {
                Slog.e("Installer", "write error");
                disconnect();
                i = 0;
            }
        }
    }

    public int clearUserData(String paramString, int paramInt)
    {
        StringBuilder localStringBuilder = new StringBuilder("rmuserdata");
        localStringBuilder.append(' ');
        localStringBuilder.append(paramString);
        localStringBuilder.append(' ');
        localStringBuilder.append(paramInt);
        return execute(localStringBuilder.toString());
    }

    public int cloneUserData(int paramInt1, int paramInt2, boolean paramBoolean)
    {
        StringBuilder localStringBuilder = new StringBuilder("cloneuserdata");
        localStringBuilder.append(' ');
        localStringBuilder.append(paramInt1);
        localStringBuilder.append(' ');
        localStringBuilder.append(paramInt2);
        localStringBuilder.append(' ');
        if (paramBoolean);
        for (char c = '1'; ; c = '0')
        {
            localStringBuilder.append(c);
            return execute(localStringBuilder.toString());
        }
    }

    public int createUserData(String paramString, int paramInt1, int paramInt2)
    {
        StringBuilder localStringBuilder = new StringBuilder("mkuserdata");
        localStringBuilder.append(' ');
        localStringBuilder.append(paramString);
        localStringBuilder.append(' ');
        localStringBuilder.append(paramInt1);
        localStringBuilder.append(' ');
        localStringBuilder.append(paramInt2);
        return execute(localStringBuilder.toString());
    }

    public int deleteCacheFiles(String paramString)
    {
        StringBuilder localStringBuilder = new StringBuilder("rmcache");
        localStringBuilder.append(' ');
        localStringBuilder.append(paramString);
        return execute(localStringBuilder.toString());
    }

    public int dexopt(String paramString, int paramInt, boolean paramBoolean)
    {
        StringBuilder localStringBuilder = new StringBuilder("dexopt");
        localStringBuilder.append(' ');
        localStringBuilder.append(paramString);
        localStringBuilder.append(' ');
        localStringBuilder.append(paramInt);
        if (paramBoolean);
        for (String str = " 1"; ; str = " 0")
        {
            localStringBuilder.append(str);
            return execute(localStringBuilder.toString());
        }
    }

    public int fixUid(String paramString, int paramInt1, int paramInt2)
    {
        StringBuilder localStringBuilder = new StringBuilder("fixuid");
        localStringBuilder.append(' ');
        localStringBuilder.append(paramString);
        localStringBuilder.append(' ');
        localStringBuilder.append(paramInt1);
        localStringBuilder.append(' ');
        localStringBuilder.append(paramInt2);
        return execute(localStringBuilder.toString());
    }

    public int freeCache(long paramLong)
    {
        StringBuilder localStringBuilder = new StringBuilder("freecache");
        localStringBuilder.append(' ');
        localStringBuilder.append(String.valueOf(paramLong));
        return execute(localStringBuilder.toString());
    }

    public int getSizeInfo(String paramString1, String paramString2, String paramString3, String paramString4, PackageStats paramPackageStats)
    {
        int i = -1;
        StringBuilder localStringBuilder = new StringBuilder("getsize");
        localStringBuilder.append(' ');
        localStringBuilder.append(paramString1);
        localStringBuilder.append(' ');
        localStringBuilder.append(paramString2);
        localStringBuilder.append(' ');
        label77: String[] arrayOfString;
        if (paramString3 != null)
        {
            localStringBuilder.append(paramString3);
            localStringBuilder.append(' ');
            if (paramString4 == null)
                break label122;
            localStringBuilder.append(paramString4);
            arrayOfString = transaction(localStringBuilder.toString()).split(" ");
            if ((arrayOfString != null) && (arrayOfString.length == 5))
                break label129;
        }
        while (true)
        {
            return i;
            paramString3 = "!";
            break;
            label122: paramString4 = "!";
            break label77;
            try
            {
                label129: paramPackageStats.codeSize = Long.parseLong(arrayOfString[1]);
                paramPackageStats.dataSize = Long.parseLong(arrayOfString[2]);
                paramPackageStats.cacheSize = Long.parseLong(arrayOfString[3]);
                paramPackageStats.externalCodeSize = Long.parseLong(arrayOfString[4]);
                int j = Integer.parseInt(arrayOfString[0]);
                i = j;
            }
            catch (NumberFormatException localNumberFormatException)
            {
            }
        }
    }

    public int install(String paramString, int paramInt1, int paramInt2)
    {
        StringBuilder localStringBuilder = new StringBuilder("install");
        localStringBuilder.append(' ');
        localStringBuilder.append(paramString);
        localStringBuilder.append(' ');
        localStringBuilder.append(paramInt1);
        localStringBuilder.append(' ');
        localStringBuilder.append(paramInt2);
        return execute(localStringBuilder.toString());
    }

    public int linkNativeLibraryDirectory(String paramString1, String paramString2)
    {
        int i = -1;
        if (paramString1 == null)
            Slog.e("Installer", "unlinkNativeLibraryDirectory dataPath is null");
        while (true)
        {
            return i;
            if (paramString2 == null)
            {
                Slog.e("Installer", "unlinkNativeLibraryDirectory nativeLibPath is null");
            }
            else
            {
                StringBuilder localStringBuilder = new StringBuilder("linklib ");
                localStringBuilder.append(paramString1);
                localStringBuilder.append(' ');
                localStringBuilder.append(paramString2);
                i = execute(localStringBuilder.toString());
            }
        }
    }

    public int moveFiles()
    {
        return execute("movefiles");
    }

    public int movedex(String paramString1, String paramString2)
    {
        StringBuilder localStringBuilder = new StringBuilder("movedex");
        localStringBuilder.append(' ');
        localStringBuilder.append(paramString1);
        localStringBuilder.append(' ');
        localStringBuilder.append(paramString2);
        return execute(localStringBuilder.toString());
    }

    public boolean ping()
    {
        if (execute("ping") < 0);
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    public int remove(String paramString, int paramInt)
    {
        StringBuilder localStringBuilder = new StringBuilder("remove");
        localStringBuilder.append(' ');
        localStringBuilder.append(paramString);
        localStringBuilder.append(' ');
        localStringBuilder.append(paramInt);
        return execute(localStringBuilder.toString());
    }

    public int removeUserDataDirs(int paramInt)
    {
        StringBuilder localStringBuilder = new StringBuilder("rmuser");
        localStringBuilder.append(' ');
        localStringBuilder.append(paramInt);
        return execute(localStringBuilder.toString());
    }

    public int rename(String paramString1, String paramString2)
    {
        StringBuilder localStringBuilder = new StringBuilder("rename");
        localStringBuilder.append(' ');
        localStringBuilder.append(paramString1);
        localStringBuilder.append(' ');
        localStringBuilder.append(paramString2);
        return execute(localStringBuilder.toString());
    }

    public int rmdex(String paramString)
    {
        StringBuilder localStringBuilder = new StringBuilder("rmdex");
        localStringBuilder.append(' ');
        localStringBuilder.append(paramString);
        return execute(localStringBuilder.toString());
    }

    public int setForwardLockPerm(String paramString, int paramInt)
    {
        StringBuilder localStringBuilder = new StringBuilder("protect");
        localStringBuilder.append(' ');
        localStringBuilder.append(paramString);
        localStringBuilder.append(' ');
        localStringBuilder.append(paramInt);
        return execute(localStringBuilder.toString());
    }

    public int unlinkNativeLibraryDirectory(String paramString)
    {
        if (paramString == null)
            Slog.e("Installer", "unlinkNativeLibraryDirectory dataPath is null");
        StringBuilder localStringBuilder;
        for (int i = -1; ; i = execute(localStringBuilder.toString()))
        {
            return i;
            localStringBuilder = new StringBuilder("unlinklib ");
            localStringBuilder.append(paramString);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.pm.Installer
 * JD-Core Version:        0.6.2
 */