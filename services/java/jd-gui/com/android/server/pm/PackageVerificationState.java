package com.android.server.pm;

import android.util.SparseBooleanArray;

class PackageVerificationState
{
    private final PackageManagerService.InstallArgs mArgs;
    private boolean mRequiredVerificationComplete;
    private boolean mRequiredVerificationPassed;
    private final int mRequiredVerifierUid;
    private boolean mSufficientVerificationComplete;
    private boolean mSufficientVerificationPassed;
    private final SparseBooleanArray mSufficientVerifierUids;

    public PackageVerificationState(int paramInt, PackageManagerService.InstallArgs paramInstallArgs)
    {
        this.mRequiredVerifierUid = paramInt;
        this.mArgs = paramInstallArgs;
        this.mSufficientVerifierUids = new SparseBooleanArray();
    }

    public void addSufficientVerifier(int paramInt)
    {
        this.mSufficientVerifierUids.put(paramInt, true);
    }

    public PackageManagerService.InstallArgs getInstallArgs()
    {
        return this.mArgs;
    }

    public boolean isInstallAllowed()
    {
        boolean bool;
        if (!this.mRequiredVerificationPassed)
            bool = false;
        while (true)
        {
            return bool;
            if (this.mSufficientVerificationComplete)
                bool = this.mSufficientVerificationPassed;
            else
                bool = true;
        }
    }

    public boolean isVerificationComplete()
    {
        boolean bool;
        if (!this.mRequiredVerificationComplete)
            bool = false;
        while (true)
        {
            return bool;
            if (this.mSufficientVerifierUids.size() == 0)
                bool = true;
            else
                bool = this.mSufficientVerificationComplete;
        }
    }

    public boolean setVerifierResponse(int paramInt1, int paramInt2)
    {
        int i = 1;
        if (paramInt1 == this.mRequiredVerifierUid)
        {
            this.mRequiredVerificationComplete = i;
            switch (paramInt2)
            {
            default:
                this.mRequiredVerificationPassed = false;
            case 2:
            case 1:
            }
        }
        while (true)
        {
            return i;
            this.mSufficientVerifierUids.clear();
            this.mRequiredVerificationPassed = i;
            continue;
            if (this.mSufficientVerifierUids.get(paramInt1))
            {
                if (paramInt2 == i)
                {
                    this.mSufficientVerificationComplete = i;
                    this.mSufficientVerificationPassed = i;
                }
                this.mSufficientVerifierUids.delete(paramInt1);
                if (this.mSufficientVerifierUids.size() == 0)
                    this.mSufficientVerificationComplete = i;
            }
            else
            {
                i = 0;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.pm.PackageVerificationState
 * JD-Core Version:        0.6.2
 */