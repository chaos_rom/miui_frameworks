package com.android.server.pm;

import android.content.pm.PackageParser.Package;
import java.io.File;

final class PackageSetting extends PackageSettingBase
{
    int appId;
    PackageParser.Package pkg;
    SharedUserSetting sharedUser;

    PackageSetting(PackageSetting paramPackageSetting)
    {
        super(paramPackageSetting);
        this.appId = paramPackageSetting.appId;
        this.pkg = paramPackageSetting.pkg;
        this.sharedUser = paramPackageSetting.sharedUser;
    }

    PackageSetting(String paramString1, String paramString2, File paramFile1, File paramFile2, String paramString3, int paramInt1, int paramInt2)
    {
        super(paramString1, paramString2, paramFile1, paramFile2, paramString3, paramInt1, paramInt2);
    }

    public String toString()
    {
        return "PackageSetting{" + Integer.toHexString(System.identityHashCode(this)) + " " + this.name + "/" + this.appId + "}";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.pm.PackageSetting
 * JD-Core Version:        0.6.2
 */