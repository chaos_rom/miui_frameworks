package com.android.server.pm;

public class MiuiSharedUids
{
    public static void add(Settings paramSettings, boolean paramBoolean)
    {
        int i = 10000;
        int j;
        if (paramBoolean)
        {
            j = 9800;
            paramSettings.addSharedUserLPw("android.uid.backup", j, 1);
            if (!paramBoolean)
                break label63;
        }
        label63: for (int k = 9801; ; k = i)
        {
            paramSettings.addSharedUserLPw("android.uid.theme", k, 1);
            if (paramBoolean)
                i = 9802;
            paramSettings.addSharedUserLPw("android.uid.updater", i, 1);
            return;
            j = i;
            break;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.pm.MiuiSharedUids
 * JD-Core Version:        0.6.2
 */