package com.android.server.pm;

public class PackageVerificationResponse
{
    public final int callerUid;
    public final int code;

    public PackageVerificationResponse(int paramInt1, int paramInt2)
    {
        this.code = paramInt1;
        this.callerUid = paramInt2;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.pm.PackageVerificationResponse
 * JD-Core Version:        0.6.2
 */