package com.android.server.pm;

import android.content.pm.Signature;
import com.android.internal.util.XmlUtils;
import java.io.IOException;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

class PackageSignatures
{
    Signature[] mSignatures;

    PackageSignatures()
    {
    }

    PackageSignatures(PackageSignatures paramPackageSignatures)
    {
        if ((paramPackageSignatures != null) && (paramPackageSignatures.mSignatures != null))
            this.mSignatures = ((Signature[])paramPackageSignatures.mSignatures.clone());
    }

    PackageSignatures(Signature[] paramArrayOfSignature)
    {
        assignSignatures(paramArrayOfSignature);
    }

    void assignSignatures(Signature[] paramArrayOfSignature)
    {
        if (paramArrayOfSignature == null)
            this.mSignatures = null;
        while (true)
        {
            return;
            this.mSignatures = new Signature[paramArrayOfSignature.length];
            for (int i = 0; i < paramArrayOfSignature.length; i++)
                this.mSignatures[i] = paramArrayOfSignature[i];
        }
    }

    void readXml(XmlPullParser paramXmlPullParser, ArrayList<Signature> paramArrayList)
        throws IOException, XmlPullParserException
    {
        String str1 = paramXmlPullParser.getAttributeValue(null, "count");
        if (str1 == null)
        {
            PackageManagerService.reportSettingsProblem(5, "Error in package manager settings: <signatures> has no count at " + paramXmlPullParser.getPositionDescription());
            XmlUtils.skipCurrentTag(paramXmlPullParser);
        }
        int i = Integer.parseInt(str1);
        this.mSignatures = new Signature[i];
        int j = 0;
        int k = paramXmlPullParser.getDepth();
        int m;
        do
        {
            m = paramXmlPullParser.next();
            if ((m == 1) || ((m == 3) && (paramXmlPullParser.getDepth() <= k)))
                break;
        }
        while ((m == 3) || (m == 4));
        String str2;
        if (paramXmlPullParser.getName().equals("cert"))
            if (j < i)
            {
                str2 = paramXmlPullParser.getAttributeValue(null, "index");
                if (str2 == null);
            }
        while (true)
        {
            try
            {
                n = Integer.parseInt(str2);
                str3 = paramXmlPullParser.getAttributeValue(null, "key");
                if (str3 == null)
                    if ((n >= 0) && (n < paramArrayList.size()))
                    {
                        if ((Signature)paramArrayList.get(n) != null)
                        {
                            this.mSignatures[j] = ((Signature)paramArrayList.get(n));
                            j++;
                            XmlUtils.skipCurrentTag(paramXmlPullParser);
                            break;
                        }
                        PackageManagerService.reportSettingsProblem(5, "Error in package manager settings: <cert> index " + str2 + " is not defined at " + paramXmlPullParser.getPositionDescription());
                        continue;
                    }
            }
            catch (NumberFormatException localNumberFormatException)
            {
                PackageManagerService.reportSettingsProblem(5, "Error in package manager settings: <cert> index " + str2 + " is not a number at " + paramXmlPullParser.getPositionDescription());
                continue;
                PackageManagerService.reportSettingsProblem(5, "Error in package manager settings: <cert> index " + str2 + " is out of bounds at " + paramXmlPullParser.getPositionDescription());
                continue;
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
                int n;
                String str3;
                PackageManagerService.reportSettingsProblem(5, "Error in package manager settings: <cert> index " + str2 + " has an invalid signature at " + paramXmlPullParser.getPositionDescription() + ": " + localIllegalArgumentException.getMessage());
                continue;
                if (paramArrayList.size() <= n)
                {
                    paramArrayList.add(null);
                    continue;
                }
                Signature localSignature = new Signature(str3);
                paramArrayList.set(n, localSignature);
                this.mSignatures[j] = localSignature;
                j++;
                continue;
            }
            PackageManagerService.reportSettingsProblem(5, "Error in package manager settings: <cert> has no index at " + paramXmlPullParser.getPositionDescription());
            continue;
            PackageManagerService.reportSettingsProblem(5, "Error in package manager settings: too many <cert> tags, expected " + i + " at " + paramXmlPullParser.getPositionDescription());
            continue;
            PackageManagerService.reportSettingsProblem(5, "Unknown element under <cert>: " + paramXmlPullParser.getName());
        }
        if (j < i)
        {
            Signature[] arrayOfSignature = new Signature[j];
            System.arraycopy(this.mSignatures, 0, arrayOfSignature, 0, j);
            this.mSignatures = arrayOfSignature;
        }
    }

    public String toString()
    {
        StringBuffer localStringBuffer = new StringBuffer(128);
        localStringBuffer.append("PackageSignatures{");
        localStringBuffer.append(Integer.toHexString(System.identityHashCode(this)));
        localStringBuffer.append(" [");
        if (this.mSignatures != null)
            for (int i = 0; i < this.mSignatures.length; i++)
            {
                if (i > 0)
                    localStringBuffer.append(", ");
                localStringBuffer.append(Integer.toHexString(System.identityHashCode(this.mSignatures[i])));
            }
        localStringBuffer.append("]}");
        return localStringBuffer.toString();
    }

    void writeXml(XmlSerializer paramXmlSerializer, String paramString, ArrayList<Signature> paramArrayList)
        throws IOException
    {
        if (this.mSignatures == null);
        while (true)
        {
            return;
            paramXmlSerializer.startTag(null, paramString);
            paramXmlSerializer.attribute(null, "count", Integer.toString(this.mSignatures.length));
            int i = 0;
            if (i < this.mSignatures.length)
            {
                paramXmlSerializer.startTag(null, "cert");
                Signature localSignature1 = this.mSignatures[i];
                int j = localSignature1.hashCode();
                int k = paramArrayList.size();
                for (int m = 0; ; m++)
                    if (m < k)
                    {
                        Signature localSignature2 = (Signature)paramArrayList.get(m);
                        if ((localSignature2.hashCode() == j) && (localSignature2.equals(localSignature1)))
                            paramXmlSerializer.attribute(null, "index", Integer.toString(m));
                    }
                    else
                    {
                        if (m >= k)
                        {
                            paramArrayList.add(localSignature1);
                            paramXmlSerializer.attribute(null, "index", Integer.toString(k));
                            paramXmlSerializer.attribute(null, "key", localSignature1.toCharsString());
                        }
                        paramXmlSerializer.endTag(null, "cert");
                        i++;
                        break;
                    }
            }
            paramXmlSerializer.endTag(null, paramString);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.pm.PackageSignatures
 * JD-Core Version:        0.6.2
 */