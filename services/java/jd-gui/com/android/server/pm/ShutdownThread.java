package com.android.server.pm;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.ActivityManagerNative;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.IActivityManager;
import android.app.ProgressDialog;
import android.bluetooth.IBluetooth;
import android.bluetooth.IBluetooth.Stub;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.drawable.AnimationDrawable;
import android.nfc.INfcAdapter;
import android.nfc.INfcAdapter.Stub;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.SystemVibrator;
import android.os.Vibrator;
import android.os.storage.IMountService;
import android.os.storage.IMountService.Stub;
import android.os.storage.IMountShutdownObserver.Stub;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.ITelephony.Stub;
import com.android.server.PowerManagerService;

public final class ShutdownThread extends Thread
{
    private static final int MAX_BROADCAST_TIME = 10000;
    private static final int MAX_RADIO_WAIT_TIME = 12000;
    private static final int MAX_SHUTDOWN_WAIT_TIME = 20000;
    private static final int PHONE_STATE_POLL_SLEEP_MSEC = 500;
    public static final String REBOOT_SAFEMODE_PROPERTY = "persist.sys.safemode";
    public static final String SHUTDOWN_ACTION_PROPERTY = "sys.shutdown.requested";
    private static final int SHUTDOWN_VIBRATE_MS = 500;
    private static final String TAG = "ShutdownThread";
    private static boolean mReboot;
    private static String mRebootReason;
    private static boolean mRebootSafeMode;
    private static final ShutdownThread sInstance = new ShutdownThread();
    private static boolean sIsStarted;
    private static Object sIsStartedGuard = new Object();
    private boolean mActionDone;
    private final Object mActionDoneSync = new Object();
    private Context mContext;
    private PowerManager.WakeLock mCpuWakeLock;
    private Handler mHandler;
    private PowerManager mPowerManager;
    private PowerManager.WakeLock mScreenWakeLock;

    static
    {
        sIsStarted = false;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private static void beginShutdownSequence(Context paramContext)
    {
        synchronized (sIsStartedGuard)
        {
            if (sIsStarted)
            {
                Log.d("ShutdownThread", "Shutdown sequence already running, returning.");
                return;
            }
            sIsStarted = true;
            ProgressDialog localProgressDialog = new ProgressDialog(paramContext);
            localProgressDialog.setTitle(paramContext.getText(101450129));
            localProgressDialog.setMessage(paramContext.getText(101450154));
            localProgressDialog.setIndeterminate(true);
            localProgressDialog.setCancelable(false);
            localProgressDialog.getWindow().setType(2009);
            Injector.createShutDownDialog(paramContext);
            sInstance.mContext = paramContext;
            sInstance.mPowerManager = ((PowerManager)paramContext.getSystemService("power"));
            sInstance.mCpuWakeLock = null;
        }
        try
        {
            sInstance.mCpuWakeLock = sInstance.mPowerManager.newWakeLock(1, "ShutdownThread-cpu");
            sInstance.mCpuWakeLock.setReferenceCounted(false);
            sInstance.mCpuWakeLock.acquire();
            sInstance.mScreenWakeLock = null;
            if (!sInstance.mPowerManager.isScreenOn());
        }
        catch (SecurityException localSecurityException1)
        {
            try
            {
                sInstance.mScreenWakeLock = sInstance.mPowerManager.newWakeLock(26, "ShutdownThread-screen");
                sInstance.mScreenWakeLock.setReferenceCounted(false);
                sInstance.mScreenWakeLock.acquire();
                sInstance.mHandler = new Handler()
                {
                };
                sInstance.start();
                return;
                localObject2 = finally;
                throw localObject2;
                localSecurityException1 = localSecurityException1;
                Log.w("ShutdownThread", "No permission to acquire wake lock", localSecurityException1);
                sInstance.mCpuWakeLock = null;
            }
            catch (SecurityException localSecurityException2)
            {
                while (true)
                {
                    Log.w("ShutdownThread", "No permission to acquire wake lock", localSecurityException2);
                    sInstance.mScreenWakeLock = null;
                }
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    static boolean getIsStarted()
    {
        return sIsStarted;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    static Object getIsStartedGuard()
    {
        return sIsStartedGuard;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    static boolean getReboot()
    {
        return mReboot;
    }

    public static void reboot(Context paramContext, String paramString, boolean paramBoolean)
    {
        mReboot = true;
        mRebootSafeMode = false;
        mRebootReason = paramString;
        shutdownInner(paramContext, paramBoolean);
    }

    public static void rebootOrShutdown(boolean paramBoolean, String paramString)
    {
        if (paramBoolean)
            Log.i("ShutdownThread", "Rebooting, reason: " + paramString);
        while (true)
        {
            try
            {
                PowerManagerService.lowLevelReboot(paramString);
                Log.i("ShutdownThread", "Performing low-level shutdown...");
                PowerManagerService.lowLevelShutdown();
                return;
            }
            catch (Exception localException2)
            {
                Log.e("ShutdownThread", "Reboot failed, will attempt shutdown instead", localException2);
                continue;
            }
            SystemVibrator localSystemVibrator = new SystemVibrator();
            try
            {
                localSystemVibrator.vibrate(500L);
                try
                {
                    Thread.sleep(500L);
                }
                catch (InterruptedException localInterruptedException)
                {
                }
            }
            catch (Exception localException1)
            {
                while (true)
                    Log.w("ShutdownThread", "Failed to vibrate during shutdown.", localException1);
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public static void rebootSafeMode(Context paramContext, boolean paramBoolean)
    {
        mReboot = true;
        mRebootSafeMode = false;
        mRebootReason = null;
        shutdownInner(paramContext, paramBoolean);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    static void setReboot(boolean paramBoolean)
    {
        mReboot = paramBoolean;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    static void setRebootReason(String paramString)
    {
        mRebootReason = paramString;
    }

    public static void shutdown(Context paramContext, boolean paramBoolean)
    {
        mReboot = false;
        mRebootSafeMode = false;
        shutdownInner(paramContext, paramBoolean);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    static void shutdownInner(Context paramContext, boolean paramBoolean)
    {
        while (true)
        {
            int i;
            int j;
            int m;
            synchronized (sIsStartedGuard)
            {
                if (sIsStarted)
                {
                    Log.d("ShutdownThread", "Request to shutdown already running, returning.");
                }
                else
                {
                    i = paramContext.getResources().getInteger(17694742);
                    if (mRebootSafeMode)
                    {
                        j = 17039666;
                        int k = Injector.getResourceId(j);
                        Log.d("ShutdownThread", "Notifying thread to start shutdown longPressBehavior=" + i);
                        if (!paramBoolean)
                            break;
                        CloseDialogReceiver localCloseDialogReceiver = new CloseDialogReceiver(paramContext);
                        AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramContext);
                        if (!mRebootSafeMode)
                            break label223;
                        m = 17039665;
                        AlertDialog localAlertDialog = localBuilder.setTitle(m).setMessage(k).setPositiveButton(17039379, new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
                            {
                                ShutdownThread.beginShutdownSequence(ShutdownThread.this);
                            }
                        }).setNegativeButton(17039369, null).create();
                        Injector.setDialogTitle(localAlertDialog);
                        localCloseDialogReceiver.dialog = localAlertDialog;
                        localAlertDialog.setOnDismissListener(localCloseDialogReceiver);
                        localAlertDialog.getWindow().setType(2009);
                        localAlertDialog.show();
                    }
                }
            }
            if (i == 2)
            {
                j = 17039664;
            }
            else
            {
                j = 17039663;
                continue;
                label223: m = 17039658;
            }
        }
        beginShutdownSequence(paramContext);
    }

    private void shutdownRadios(int paramInt)
    {
        final long l1 = SystemClock.elapsedRealtime() + paramInt;
        boolean[] arrayOfBoolean = new boolean[1];
        Thread local5 = new Thread()
        {
            public void run()
            {
                INfcAdapter localINfcAdapter = INfcAdapter.Stub.asInterface(ServiceManager.checkService("nfc"));
                ITelephony localITelephony = ITelephony.Stub.asInterface(ServiceManager.checkService("phone"));
                IBluetooth localIBluetooth = IBluetooth.Stub.asInterface(ServiceManager.checkService("bluetooth"));
                if (localINfcAdapter != null);
                while (true)
                {
                    try
                    {
                        if (localINfcAdapter.getState() == 1)
                        {
                            break label449;
                            if (i == 0)
                            {
                                Log.w("ShutdownThread", "Turning off NFC...");
                                localINfcAdapter.disable(false);
                            }
                            if (localIBluetooth == null)
                                break label455;
                        }
                    }
                    catch (RemoteException localRemoteException5)
                    {
                        try
                        {
                            if (localIBluetooth.getBluetoothState() == 10)
                            {
                                break label455;
                                if (j == 0)
                                {
                                    Log.w("ShutdownThread", "Disabling Bluetooth...");
                                    localIBluetooth.disable(false);
                                }
                                if (localITelephony == null)
                                    break label461;
                            }
                        }
                        catch (RemoteException localRemoteException5)
                        {
                            try
                            {
                                if (!localITelephony.isRadioOn())
                                {
                                    break label461;
                                    if (k == 0)
                                    {
                                        Log.w("ShutdownThread", "Turning off radio...");
                                        localITelephony.setRadio(false);
                                    }
                                    Log.i("ShutdownThread", "Waiting for NFC, Bluetooth and Radio...");
                                    if (SystemClock.elapsedRealtime() < l1)
                                        if (j != 0);
                                }
                            }
                            catch (RemoteException localRemoteException5)
                            {
                                try
                                {
                                    int n = localIBluetooth.getBluetoothState();
                                    if (n == 10)
                                    {
                                        j = 1;
                                        if (j != 0)
                                            Log.i("ShutdownThread", "Bluetooth turned off.");
                                        if (k != 0);
                                    }
                                }
                                catch (RemoteException localRemoteException5)
                                {
                                    try
                                    {
                                        boolean bool = localITelephony.isRadioOn();
                                        if (!bool)
                                        {
                                            k = 1;
                                            if (k != 0)
                                                Log.i("ShutdownThread", "Radio turned off.");
                                            if (i != 0);
                                        }
                                    }
                                    catch (RemoteException localRemoteException5)
                                    {
                                        try
                                        {
                                            int m = localINfcAdapter.getState();
                                            if (m == 1)
                                            {
                                                i = 1;
                                                if (k != 0)
                                                    Log.i("ShutdownThread", "NFC turned off.");
                                                if ((k == 0) || (j == 0) || (i == 0))
                                                    continue;
                                                Log.i("ShutdownThread", "NFC, Radio and Bluetooth shutdown complete.");
                                                this.val$done[0] = true;
                                                return;
                                                i = 0;
                                                continue;
                                                localRemoteException1 = localRemoteException1;
                                                Log.e("ShutdownThread", "RemoteException during NFC shutdown", localRemoteException1);
                                                i = 1;
                                                continue;
                                                j = 0;
                                                continue;
                                                localRemoteException2 = localRemoteException2;
                                                Log.e("ShutdownThread", "RemoteException during bluetooth shutdown", localRemoteException2);
                                                j = 1;
                                                continue;
                                                k = 0;
                                                continue;
                                                localRemoteException3 = localRemoteException3;
                                                Log.e("ShutdownThread", "RemoteException during radio shutdown", localRemoteException3);
                                                k = 1;
                                                continue;
                                                j = 0;
                                                continue;
                                                localRemoteException6 = localRemoteException6;
                                                Log.e("ShutdownThread", "RemoteException during bluetooth shutdown", localRemoteException6);
                                                j = 1;
                                                continue;
                                                k = 0;
                                                continue;
                                                localRemoteException5 = localRemoteException5;
                                                Log.e("ShutdownThread", "RemoteException during radio shutdown", localRemoteException5);
                                                k = 1;
                                                continue;
                                            }
                                            i = 0;
                                            continue;
                                        }
                                        catch (RemoteException localRemoteException4)
                                        {
                                            Log.e("ShutdownThread", "RemoteException during NFC shutdown", localRemoteException4);
                                            i = 1;
                                            continue;
                                            SystemClock.sleep(500L);
                                        }
                                    }
                                }
                                continue;
                            }
                        }
                    }
                    label449: int i = 1;
                    continue;
                    label455: int j = 1;
                    continue;
                    label461: int k = 1;
                }
            }
        };
        local5.start();
        long l2 = paramInt;
        try
        {
            local5.join(l2);
            label41: if (arrayOfBoolean[0] == 0)
                Log.w("ShutdownThread", "Timed out waiting for NFC, Radio and Bluetooth shutdown.");
            return;
        }
        catch (InterruptedException localInterruptedException)
        {
            break label41;
        }
    }

    void actionDone()
    {
        synchronized (this.mActionDoneSync)
        {
            this.mActionDone = true;
            this.mActionDoneSync.notifyAll();
            return;
        }
    }

    public void run()
    {
        BroadcastReceiver local3 = new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                ShutdownThread.this.actionDone();
            }
        };
        StringBuilder localStringBuilder1 = new StringBuilder();
        String str1;
        if (mReboot)
            str1 = "1";
        while (true)
        {
            StringBuilder localStringBuilder2 = localStringBuilder1.append(str1);
            String str2;
            label45: long l4;
            label132: IActivityManager localIActivityManager;
            if (mRebootReason != null)
            {
                str2 = mRebootReason;
                SystemProperties.set("sys.shutdown.requested", str2);
                if (mRebootSafeMode)
                    SystemProperties.set("persist.sys.safemode", "1");
                Log.i("ShutdownThread", "Sending shutdown broadcast...");
                this.mActionDone = false;
                this.mContext.sendOrderedBroadcast(new Intent("android.intent.action.ACTION_SHUTDOWN"), null, local3, this.mHandler, 0, null, null);
                long l1 = 10000L + SystemClock.elapsedRealtime();
                synchronized (this.mActionDoneSync)
                {
                    if (!this.mActionDone)
                    {
                        l4 = l1 - SystemClock.elapsedRealtime();
                        if (l4 <= 0L)
                            Log.w("ShutdownThread", "Shutdown broadcast timed out");
                    }
                    else
                    {
                        Log.i("ShutdownThread", "Shutting down activity manager...");
                        localIActivityManager = ActivityManagerNative.asInterface(ServiceManager.checkService("activity"));
                        if (localIActivityManager == null);
                    }
                }
            }
            try
            {
                localIActivityManager.shutdown(10000);
                label202: shutdownRadios(12000);
                IMountShutdownObserver.Stub local4 = new IMountShutdownObserver.Stub()
                {
                    public void onShutDownComplete(int paramAnonymousInt)
                        throws RemoteException
                    {
                        Log.w("ShutdownThread", "Result code " + paramAnonymousInt + " from MountService.shutdown");
                        ShutdownThread.this.actionDone();
                    }
                };
                Log.i("ShutdownThread", "Shutting down MountService");
                this.mActionDone = false;
                long l2 = 20000L + SystemClock.elapsedRealtime();
                while (true)
                {
                    long l3;
                    synchronized (this.mActionDoneSync)
                    {
                        try
                        {
                            while (true)
                            {
                                IMountService localIMountService = IMountService.Stub.asInterface(ServiceManager.checkService("mount"));
                                if (localIMountService == null)
                                    break label360;
                                localIMountService.shutdown(local4);
                                if (!this.mActionDone)
                                {
                                    l3 = l2 - SystemClock.elapsedRealtime();
                                    if (l3 > 0L)
                                        break label396;
                                    Log.w("ShutdownThread", "Shutdown wait timed out");
                                }
                                rebootOrShutdown(mReboot, mRebootReason);
                                return;
                                str1 = "0";
                                break;
                                str2 = "";
                                break label45;
                                try
                                {
                                    this.mActionDoneSync.wait(l4);
                                }
                                catch (InterruptedException localInterruptedException2)
                                {
                                }
                            }
                            break label132;
                            localObject2 = finally;
                            throw localObject2;
                            label360: Log.w("ShutdownThread", "MountService unavailable for shutdown");
                            continue;
                        }
                        catch (Exception localException)
                        {
                            Log.e("ShutdownThread", "Exception during MountService shutdown", localException);
                            continue;
                        }
                    }
                    try
                    {
                        label396: this.mActionDoneSync.wait(l3);
                    }
                    catch (InterruptedException localInterruptedException1)
                    {
                    }
                }
            }
            catch (RemoteException localRemoteException)
            {
                break label202;
            }
        }
    }

    private static class CloseDialogReceiver extends BroadcastReceiver
        implements DialogInterface.OnDismissListener
    {
        public Dialog dialog;
        private Context mContext;

        CloseDialogReceiver(Context paramContext)
        {
            this.mContext = paramContext;
            paramContext.registerReceiver(this, new IntentFilter("android.intent.action.CLOSE_SYSTEM_DIALOGS"));
        }

        @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
        public void onDismiss(DialogInterface paramDialogInterface)
        {
            this.mContext.unregisterReceiver(this);
            ShutdownThread.Injector.onDismiss(paramDialogInterface);
        }

        public void onReceive(Context paramContext, Intent paramIntent)
        {
            this.dialog.cancel();
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static void createShutDownDialog(Context paramContext)
        {
            Dialog localDialog = new Dialog(paramContext, 16974065);
            View localView = LayoutInflater.from(localDialog.getContext()).inflate(100859950, null);
            TextView localTextView = (TextView)localView.findViewById(101384225);
            ImageView localImageView = (ImageView)localView.findViewById(101384226);
            if (ShutdownThread.getReboot())
                localTextView.setText(101449729);
            while (true)
            {
                localDialog.setContentView(localView);
                localDialog.setCancelable(false);
                localDialog.getWindow().setType(2021);
                localDialog.getWindow().setBackgroundDrawableResource(100794420);
                localDialog.show();
                ((AnimationDrawable)localImageView.getDrawable()).start();
                return;
                localTextView.setText(101450154);
            }
        }

        static int getResourceId(int paramInt)
        {
            if (ShutdownThread.getReboot())
                paramInt = 101449728;
            return paramInt;
        }

        static void onDismiss(DialogInterface paramDialogInterface)
        {
            synchronized (ShutdownThread.getIsStartedGuard())
            {
                if (!ShutdownThread.getIsStarted())
                {
                    ShutdownThread.setReboot(false);
                    ShutdownThread.setRebootReason(null);
                }
                return;
            }
        }

        static void setDialogTitle(Dialog paramDialog)
        {
            if (ShutdownThread.getReboot());
            for (int i = 101450124; ; i = 101450129)
            {
                paramDialog.setTitle(i);
                return;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.pm.ShutdownThread
 * JD-Core Version:        0.6.2
 */