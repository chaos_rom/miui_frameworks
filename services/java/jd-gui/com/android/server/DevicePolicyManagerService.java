package com.android.server;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.admin.DeviceAdminInfo;
import android.app.admin.DeviceAdminInfo.PolicyInfo;
import android.app.admin.IDevicePolicyManager.Stub;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.os.Binder;
import android.os.Environment;
import android.os.Handler;
import android.os.IPowerManager;
import android.os.IPowerManager.Stub;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.RecoverySystem;
import android.os.RemoteCallback;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.provider.Settings.Secure;
import android.util.PrintWriterPrinter;
import android.util.Printer;
import android.util.Slog;
import android.view.IWindowManager;
import android.view.IWindowManager.Stub;
import com.android.internal.content.PackageMonitor;
import com.android.internal.os.storage.ExternalStorageFormatter;
import com.android.internal.util.FastXmlSerializer;
import com.android.internal.util.JournaledFile;
import com.android.internal.util.XmlUtils;
import com.android.internal.widget.LockPatternUtils;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class DevicePolicyManagerService extends IDevicePolicyManager.Stub
{
    protected static final String ACTION_EXPIRED_PASSWORD_NOTIFICATION = "com.android.server.ACTION_EXPIRED_PASSWORD_NOTIFICATION";
    private static final long EXPIRATION_GRACE_PERIOD_MS = 432000000L;
    private static final long MS_PER_DAY = 86400000L;
    private static final int REQUEST_EXPIRE_PASSWORD = 5571;
    public static final String SYSTEM_PROP_DISABLE_CAMERA = "sys.secpolicy.camera.disabled";
    private static final String TAG = "DevicePolicyManagerService";
    int mActivePasswordLength = 0;
    int mActivePasswordLetters = 0;
    int mActivePasswordLowerCase = 0;
    int mActivePasswordNonLetter = 0;
    int mActivePasswordNumeric = 0;
    int mActivePasswordQuality = 0;
    int mActivePasswordSymbols = 0;
    int mActivePasswordUpperCase = 0;
    final ArrayList<ActiveAdmin> mAdminList = new ArrayList();
    final HashMap<ComponentName, ActiveAdmin> mAdminMap = new HashMap();
    final Context mContext;
    int mFailedPasswordAttempts = 0;
    Handler mHandler = new Handler();
    IPowerManager mIPowerManager;
    IWindowManager mIWindowManager;
    long mLastMaximumTimeToLock = -1L;
    final MyPackageMonitor mMonitor;
    int mPasswordOwner = -1;
    BroadcastReceiver mReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            String str = paramAnonymousIntent.getAction();
            if (("android.intent.action.BOOT_COMPLETED".equals(str)) || ("com.android.server.ACTION_EXPIRED_PASSWORD_NOTIFICATION".equals(str)))
            {
                Slog.v("DevicePolicyManagerService", "Sending password expiration notifications for action " + str);
                DevicePolicyManagerService.this.mHandler.post(new Runnable()
                {
                    public void run()
                    {
                        DevicePolicyManagerService.this.handlePasswordExpirationNotification();
                    }
                });
            }
        }
    };
    final PowerManager.WakeLock mWakeLock;

    public DevicePolicyManagerService(Context paramContext)
    {
        this.mContext = paramContext;
        this.mMonitor = new MyPackageMonitor();
        this.mMonitor.register(paramContext, null, true);
        this.mWakeLock = ((PowerManager)paramContext.getSystemService("power")).newWakeLock(1, "DPM");
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("android.intent.action.BOOT_COMPLETED");
        localIntentFilter.addAction("com.android.server.ACTION_EXPIRED_PASSWORD_NOTIFICATION");
        paramContext.registerReceiver(this.mReceiver, localIntentFilter);
    }

    private int getEncryptionStatus()
    {
        String str = SystemProperties.get("ro.crypto.state", "unsupported");
        int i;
        if ("encrypted".equalsIgnoreCase(str))
            i = 3;
        while (true)
        {
            return i;
            if ("unencrypted".equalsIgnoreCase(str))
                i = 1;
            else
                i = 0;
        }
    }

    private IPowerManager getIPowerManager()
    {
        if (this.mIPowerManager == null)
            this.mIPowerManager = IPowerManager.Stub.asInterface(ServiceManager.getService("power"));
        return this.mIPowerManager;
    }

    private long getPasswordExpirationLocked(ComponentName paramComponentName)
    {
        long l1 = 0L;
        ActiveAdmin localActiveAdmin2;
        if (paramComponentName != null)
        {
            localActiveAdmin2 = getActiveAdminUncheckedLocked(paramComponentName);
            if (localActiveAdmin2 == null);
        }
        long l2;
        for (l1 = localActiveAdmin2.passwordExpirationDate; ; l1 = l2)
        {
            return l1;
            l2 = 0L;
            int i = this.mAdminList.size();
            for (int j = 0; j < i; j++)
            {
                ActiveAdmin localActiveAdmin1 = (ActiveAdmin)this.mAdminList.get(j);
                if ((l2 == l1) || ((localActiveAdmin1.passwordExpirationDate != l1) && (l2 > localActiveAdmin1.passwordExpirationDate)))
                    l2 = localActiveAdmin1.passwordExpirationDate;
            }
        }
    }

    private IWindowManager getWindowManager()
    {
        if (this.mIWindowManager == null)
            this.mIWindowManager = IWindowManager.Stub.asInterface(ServiceManager.getService("window"));
        return this.mIWindowManager;
    }

    private void handlePasswordExpirationNotification()
    {
        while (true)
        {
            try
            {
                long l = System.currentTimeMillis();
                int i = this.mAdminList.size();
                if (i > 0)
                    break label123;
                break label122;
                if (j < i)
                {
                    ActiveAdmin localActiveAdmin = (ActiveAdmin)this.mAdminList.get(j);
                    if ((!localActiveAdmin.info.usesPolicy(6)) || (localActiveAdmin.passwordExpirationTimeout <= 0L) || (localActiveAdmin.passwordExpirationDate <= 0L) || (l < localActiveAdmin.passwordExpirationDate - 432000000L))
                        break label129;
                    sendAdminCommandLocked(localActiveAdmin, "android.app.action.ACTION_PASSWORD_EXPIRING");
                    break label129;
                }
                setExpirationAlarmCheckLocked(this.mContext);
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
            label122: return;
            label123: int j = 0;
            continue;
            label129: j++;
        }
    }

    private boolean isEncryptionSupported()
    {
        if (getEncryptionStatus() != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isExtStorageEncrypted()
    {
        if (!"".equals(SystemProperties.get("vold.decrypt")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    // ERROR //
    private void loadSettingsLocked()
    {
        // Byte code:
        //     0: invokestatic 295	com/android/server/DevicePolicyManagerService:makeJournaledFile	()Lcom/android/internal/util/JournaledFile;
        //     3: astore_1
        //     4: aconst_null
        //     5: astore_2
        //     6: aload_1
        //     7: invokevirtual 301	com/android/internal/util/JournaledFile:chooseForRead	()Ljava/io/File;
        //     10: astore_3
        //     11: new 303	java/io/FileInputStream
        //     14: dup
        //     15: aload_3
        //     16: invokespecial 306	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //     19: astore 4
        //     21: invokestatic 312	android/util/Xml:newPullParser	()Lorg/xmlpull/v1/XmlPullParser;
        //     24: astore 19
        //     26: aload 19
        //     28: aload 4
        //     30: aconst_null
        //     31: invokeinterface 318 3 0
        //     36: aload 19
        //     38: invokeinterface 321 1 0
        //     43: istore 20
        //     45: iload 20
        //     47: iconst_1
        //     48: if_icmpeq +9 -> 57
        //     51: iload 20
        //     53: iconst_2
        //     54: if_icmpne -18 -> 36
        //     57: aload 19
        //     59: invokeinterface 325 1 0
        //     64: astore 21
        //     66: ldc_w 327
        //     69: aload 21
        //     71: invokevirtual 276	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     74: ifne +209 -> 283
        //     77: new 283	org/xmlpull/v1/XmlPullParserException
        //     80: dup
        //     81: new 329	java/lang/StringBuilder
        //     84: dup
        //     85: invokespecial 330	java/lang/StringBuilder:<init>	()V
        //     88: ldc_w 332
        //     91: invokevirtual 336	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     94: aload 21
        //     96: invokevirtual 336	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     99: invokevirtual 339	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     102: invokespecial 341	org/xmlpull/v1/XmlPullParserException:<init>	(Ljava/lang/String;)V
        //     105: athrow
        //     106: astore 17
        //     108: aload 4
        //     110: astore_2
        //     111: ldc 36
        //     113: new 329	java/lang/StringBuilder
        //     116: dup
        //     117: invokespecial 330	java/lang/StringBuilder:<init>	()V
        //     120: ldc_w 343
        //     123: invokevirtual 336	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     126: aload_3
        //     127: invokevirtual 346	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     130: ldc_w 348
        //     133: invokevirtual 336	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     136: aload 17
        //     138: invokevirtual 346	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     141: invokevirtual 339	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     144: invokestatic 354	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     147: pop
        //     148: aload_2
        //     149: ifnull +7 -> 156
        //     152: aload_2
        //     153: invokevirtual 357	java/io/FileInputStream:close	()V
        //     156: new 359	com/android/internal/widget/LockPatternUtils
        //     159: dup
        //     160: aload_0
        //     161: getfield 118	com/android/server/DevicePolicyManagerService:mContext	Landroid/content/Context;
        //     164: invokespecial 361	com/android/internal/widget/LockPatternUtils:<init>	(Landroid/content/Context;)V
        //     167: astore 7
        //     169: aload 7
        //     171: invokevirtual 364	com/android/internal/widget/LockPatternUtils:getActivePasswordQuality	()I
        //     174: aload_0
        //     175: getfield 74	com/android/server/DevicePolicyManagerService:mActivePasswordQuality	I
        //     178: if_icmpge +92 -> 270
        //     181: ldc 36
        //     183: new 329	java/lang/StringBuilder
        //     186: dup
        //     187: invokespecial 330	java/lang/StringBuilder:<init>	()V
        //     190: ldc_w 366
        //     193: invokevirtual 336	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     196: aload_0
        //     197: getfield 74	com/android/server/DevicePolicyManagerService:mActivePasswordQuality	I
        //     200: invokestatic 372	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     203: invokevirtual 336	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     206: ldc_w 374
        //     209: invokevirtual 336	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     212: aload 7
        //     214: invokevirtual 364	com/android/internal/widget/LockPatternUtils:getActivePasswordQuality	()I
        //     217: invokestatic 372	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     220: invokevirtual 336	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     223: invokevirtual 339	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     226: invokestatic 354	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     229: pop
        //     230: aload_0
        //     231: iconst_0
        //     232: putfield 74	com/android/server/DevicePolicyManagerService:mActivePasswordQuality	I
        //     235: aload_0
        //     236: iconst_0
        //     237: putfield 76	com/android/server/DevicePolicyManagerService:mActivePasswordLength	I
        //     240: aload_0
        //     241: iconst_0
        //     242: putfield 78	com/android/server/DevicePolicyManagerService:mActivePasswordUpperCase	I
        //     245: aload_0
        //     246: iconst_0
        //     247: putfield 80	com/android/server/DevicePolicyManagerService:mActivePasswordLowerCase	I
        //     250: aload_0
        //     251: iconst_0
        //     252: putfield 82	com/android/server/DevicePolicyManagerService:mActivePasswordLetters	I
        //     255: aload_0
        //     256: iconst_0
        //     257: putfield 84	com/android/server/DevicePolicyManagerService:mActivePasswordNumeric	I
        //     260: aload_0
        //     261: iconst_0
        //     262: putfield 86	com/android/server/DevicePolicyManagerService:mActivePasswordSymbols	I
        //     265: aload_0
        //     266: iconst_0
        //     267: putfield 88	com/android/server/DevicePolicyManagerService:mActivePasswordNonLetter	I
        //     270: aload_0
        //     271: invokevirtual 377	com/android/server/DevicePolicyManagerService:validatePasswordOwnerLocked	()V
        //     274: aload_0
        //     275: invokevirtual 380	com/android/server/DevicePolicyManagerService:syncDeviceCapabilitiesLocked	()V
        //     278: aload_0
        //     279: invokevirtual 383	com/android/server/DevicePolicyManagerService:updateMaximumTimeToLockLocked	()V
        //     282: return
        //     283: aload 19
        //     285: invokeinterface 321 1 0
        //     290: pop
        //     291: aload 19
        //     293: invokeinterface 386 1 0
        //     298: istore 23
        //     300: aload 19
        //     302: invokeinterface 321 1 0
        //     307: istore 24
        //     309: iload 24
        //     311: iconst_1
        //     312: if_icmpeq +617 -> 929
        //     315: iload 24
        //     317: iconst_3
        //     318: if_icmpne +15 -> 333
        //     321: aload 19
        //     323: invokeinterface 386 1 0
        //     328: iload 23
        //     330: if_icmple +599 -> 929
        //     333: iload 24
        //     335: iconst_3
        //     336: if_icmpeq -36 -> 300
        //     339: iload 24
        //     341: iconst_4
        //     342: if_icmpeq -42 -> 300
        //     345: aload 19
        //     347: invokeinterface 325 1 0
        //     352: astore 25
        //     354: ldc_w 388
        //     357: aload 25
        //     359: invokevirtual 276	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     362: ifeq +160 -> 522
        //     365: aload 19
        //     367: aconst_null
        //     368: ldc_w 390
        //     371: invokeinterface 393 3 0
        //     376: astore 27
        //     378: aload_0
        //     379: aload 27
        //     381: invokestatic 399	android/content/ComponentName:unflattenFromString	(Ljava/lang/String;)Landroid/content/ComponentName;
        //     384: invokevirtual 403	com/android/server/DevicePolicyManagerService:findAdmin	(Landroid/content/ComponentName;)Landroid/app/admin/DeviceAdminInfo;
        //     387: astore 30
        //     389: aload 30
        //     391: ifnull -91 -> 300
        //     394: new 15	com/android/server/DevicePolicyManagerService$ActiveAdmin
        //     397: dup
        //     398: aload 30
        //     400: invokespecial 406	com/android/server/DevicePolicyManagerService$ActiveAdmin:<init>	(Landroid/app/admin/DeviceAdminInfo;)V
        //     403: astore 31
        //     405: aload 31
        //     407: aload 19
        //     409: invokevirtual 410	com/android/server/DevicePolicyManagerService$ActiveAdmin:readFromXml	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     412: aload_0
        //     413: getfield 106	com/android/server/DevicePolicyManagerService:mAdminMap	Ljava/util/HashMap;
        //     416: aload 31
        //     418: getfield 242	com/android/server/DevicePolicyManagerService$ActiveAdmin:info	Landroid/app/admin/DeviceAdminInfo;
        //     421: invokevirtual 414	android/app/admin/DeviceAdminInfo:getComponent	()Landroid/content/ComponentName;
        //     424: aload 31
        //     426: invokevirtual 418	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     429: pop
        //     430: aload_0
        //     431: getfield 111	com/android/server/DevicePolicyManagerService:mAdminList	Ljava/util/ArrayList;
        //     434: aload 31
        //     436: invokevirtual 421	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     439: pop
        //     440: goto -140 -> 300
        //     443: astore 28
        //     445: ldc 36
        //     447: new 329	java/lang/StringBuilder
        //     450: dup
        //     451: invokespecial 330	java/lang/StringBuilder:<init>	()V
        //     454: ldc_w 423
        //     457: invokevirtual 336	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     460: aload 27
        //     462: invokevirtual 336	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     465: invokevirtual 339	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     468: aload 28
        //     470: invokestatic 426	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     473: pop
        //     474: goto -174 -> 300
        //     477: astore 15
        //     479: aload 4
        //     481: astore_2
        //     482: ldc 36
        //     484: new 329	java/lang/StringBuilder
        //     487: dup
        //     488: invokespecial 330	java/lang/StringBuilder:<init>	()V
        //     491: ldc_w 343
        //     494: invokevirtual 336	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     497: aload_3
        //     498: invokevirtual 346	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     501: ldc_w 348
        //     504: invokevirtual 336	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     507: aload 15
        //     509: invokevirtual 346	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     512: invokevirtual 339	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     515: invokestatic 354	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     518: pop
        //     519: goto -371 -> 148
        //     522: ldc_w 428
        //     525: aload 25
        //     527: invokevirtual 276	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     530: ifeq +74 -> 604
        //     533: aload_0
        //     534: aload 19
        //     536: aconst_null
        //     537: ldc_w 430
        //     540: invokeinterface 393 3 0
        //     545: invokestatic 434	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     548: putfield 90	com/android/server/DevicePolicyManagerService:mFailedPasswordAttempts	I
        //     551: aload 19
        //     553: invokestatic 439	com/android/internal/util/XmlUtils:skipCurrentTag	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     556: goto -256 -> 300
        //     559: astore 13
        //     561: aload 4
        //     563: astore_2
        //     564: ldc 36
        //     566: new 329	java/lang/StringBuilder
        //     569: dup
        //     570: invokespecial 330	java/lang/StringBuilder:<init>	()V
        //     573: ldc_w 343
        //     576: invokevirtual 336	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     579: aload_3
        //     580: invokevirtual 346	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     583: ldc_w 348
        //     586: invokevirtual 336	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     589: aload 13
        //     591: invokevirtual 346	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     594: invokevirtual 339	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     597: invokestatic 354	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     600: pop
        //     601: goto -453 -> 148
        //     604: ldc_w 441
        //     607: aload 25
        //     609: invokevirtual 276	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     612: ifeq +29 -> 641
        //     615: aload_0
        //     616: aload 19
        //     618: aconst_null
        //     619: ldc_w 430
        //     622: invokeinterface 393 3 0
        //     627: invokestatic 434	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     630: putfield 92	com/android/server/DevicePolicyManagerService:mPasswordOwner	I
        //     633: aload 19
        //     635: invokestatic 439	com/android/internal/util/XmlUtils:skipCurrentTag	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     638: goto -338 -> 300
        //     641: ldc_w 443
        //     644: aload 25
        //     646: invokevirtual 276	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     649: ifeq +200 -> 849
        //     652: aload_0
        //     653: aload 19
        //     655: aconst_null
        //     656: ldc_w 445
        //     659: invokeinterface 393 3 0
        //     664: invokestatic 434	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     667: putfield 74	com/android/server/DevicePolicyManagerService:mActivePasswordQuality	I
        //     670: aload_0
        //     671: aload 19
        //     673: aconst_null
        //     674: ldc_w 447
        //     677: invokeinterface 393 3 0
        //     682: invokestatic 434	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     685: putfield 76	com/android/server/DevicePolicyManagerService:mActivePasswordLength	I
        //     688: aload_0
        //     689: aload 19
        //     691: aconst_null
        //     692: ldc_w 449
        //     695: invokeinterface 393 3 0
        //     700: invokestatic 434	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     703: putfield 78	com/android/server/DevicePolicyManagerService:mActivePasswordUpperCase	I
        //     706: aload_0
        //     707: aload 19
        //     709: aconst_null
        //     710: ldc_w 451
        //     713: invokeinterface 393 3 0
        //     718: invokestatic 434	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     721: putfield 80	com/android/server/DevicePolicyManagerService:mActivePasswordLowerCase	I
        //     724: aload_0
        //     725: aload 19
        //     727: aconst_null
        //     728: ldc_w 453
        //     731: invokeinterface 393 3 0
        //     736: invokestatic 434	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     739: putfield 82	com/android/server/DevicePolicyManagerService:mActivePasswordLetters	I
        //     742: aload_0
        //     743: aload 19
        //     745: aconst_null
        //     746: ldc_w 455
        //     749: invokeinterface 393 3 0
        //     754: invokestatic 434	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     757: putfield 84	com/android/server/DevicePolicyManagerService:mActivePasswordNumeric	I
        //     760: aload_0
        //     761: aload 19
        //     763: aconst_null
        //     764: ldc_w 457
        //     767: invokeinterface 393 3 0
        //     772: invokestatic 434	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     775: putfield 86	com/android/server/DevicePolicyManagerService:mActivePasswordSymbols	I
        //     778: aload_0
        //     779: aload 19
        //     781: aconst_null
        //     782: ldc_w 459
        //     785: invokeinterface 393 3 0
        //     790: invokestatic 434	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     793: putfield 88	com/android/server/DevicePolicyManagerService:mActivePasswordNonLetter	I
        //     796: aload 19
        //     798: invokestatic 439	com/android/internal/util/XmlUtils:skipCurrentTag	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     801: goto -501 -> 300
        //     804: astore 10
        //     806: aload 4
        //     808: astore_2
        //     809: ldc 36
        //     811: new 329	java/lang/StringBuilder
        //     814: dup
        //     815: invokespecial 330	java/lang/StringBuilder:<init>	()V
        //     818: ldc_w 343
        //     821: invokevirtual 336	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     824: aload_3
        //     825: invokevirtual 346	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     828: ldc_w 348
        //     831: invokevirtual 336	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     834: aload 10
        //     836: invokevirtual 346	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     839: invokevirtual 339	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     842: invokestatic 354	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     845: pop
        //     846: goto -698 -> 148
        //     849: ldc 36
        //     851: new 329	java/lang/StringBuilder
        //     854: dup
        //     855: invokespecial 330	java/lang/StringBuilder:<init>	()V
        //     858: ldc_w 461
        //     861: invokevirtual 336	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     864: aload 25
        //     866: invokevirtual 336	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     869: invokevirtual 339	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     872: invokestatic 354	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     875: pop
        //     876: aload 19
        //     878: invokestatic 439	com/android/internal/util/XmlUtils:skipCurrentTag	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     881: goto -581 -> 300
        //     884: astore 5
        //     886: aload 4
        //     888: astore_2
        //     889: ldc 36
        //     891: new 329	java/lang/StringBuilder
        //     894: dup
        //     895: invokespecial 330	java/lang/StringBuilder:<init>	()V
        //     898: ldc_w 343
        //     901: invokevirtual 336	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     904: aload_3
        //     905: invokevirtual 346	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     908: ldc_w 348
        //     911: invokevirtual 336	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     914: aload 5
        //     916: invokevirtual 346	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     919: invokevirtual 339	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     922: invokestatic 354	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     925: pop
        //     926: goto -778 -> 148
        //     929: aload 4
        //     931: astore_2
        //     932: goto -784 -> 148
        //     935: astore 9
        //     937: goto -781 -> 156
        //     940: astore 5
        //     942: goto -53 -> 889
        //     945: astore 10
        //     947: goto -138 -> 809
        //     950: astore 34
        //     952: goto -804 -> 148
        //     955: astore 13
        //     957: goto -393 -> 564
        //     960: astore 15
        //     962: goto -480 -> 482
        //     965: astore 17
        //     967: goto -856 -> 111
        //     970: astore 12
        //     972: aload 4
        //     974: astore_2
        //     975: goto -827 -> 148
        //
        // Exception table:
        //     from	to	target	type
        //     21	106	106	java/lang/NullPointerException
        //     283	378	106	java/lang/NullPointerException
        //     378	440	106	java/lang/NullPointerException
        //     445	474	106	java/lang/NullPointerException
        //     522	556	106	java/lang/NullPointerException
        //     604	801	106	java/lang/NullPointerException
        //     849	881	106	java/lang/NullPointerException
        //     378	440	443	java/lang/RuntimeException
        //     21	106	477	java/lang/NumberFormatException
        //     283	378	477	java/lang/NumberFormatException
        //     378	440	477	java/lang/NumberFormatException
        //     445	474	477	java/lang/NumberFormatException
        //     522	556	477	java/lang/NumberFormatException
        //     604	801	477	java/lang/NumberFormatException
        //     849	881	477	java/lang/NumberFormatException
        //     21	106	559	org/xmlpull/v1/XmlPullParserException
        //     283	378	559	org/xmlpull/v1/XmlPullParserException
        //     378	440	559	org/xmlpull/v1/XmlPullParserException
        //     445	474	559	org/xmlpull/v1/XmlPullParserException
        //     522	556	559	org/xmlpull/v1/XmlPullParserException
        //     604	801	559	org/xmlpull/v1/XmlPullParserException
        //     849	881	559	org/xmlpull/v1/XmlPullParserException
        //     21	106	804	java/io/IOException
        //     283	378	804	java/io/IOException
        //     378	440	804	java/io/IOException
        //     445	474	804	java/io/IOException
        //     522	556	804	java/io/IOException
        //     604	801	804	java/io/IOException
        //     849	881	804	java/io/IOException
        //     21	106	884	java/lang/IndexOutOfBoundsException
        //     283	378	884	java/lang/IndexOutOfBoundsException
        //     378	440	884	java/lang/IndexOutOfBoundsException
        //     445	474	884	java/lang/IndexOutOfBoundsException
        //     522	556	884	java/lang/IndexOutOfBoundsException
        //     604	801	884	java/lang/IndexOutOfBoundsException
        //     849	881	884	java/lang/IndexOutOfBoundsException
        //     152	156	935	java/io/IOException
        //     11	21	940	java/lang/IndexOutOfBoundsException
        //     11	21	945	java/io/IOException
        //     11	21	950	java/io/FileNotFoundException
        //     11	21	955	org/xmlpull/v1/XmlPullParserException
        //     11	21	960	java/lang/NumberFormatException
        //     11	21	965	java/lang/NullPointerException
        //     21	106	970	java/io/FileNotFoundException
        //     283	378	970	java/io/FileNotFoundException
        //     378	440	970	java/io/FileNotFoundException
        //     445	474	970	java/io/FileNotFoundException
        //     522	556	970	java/io/FileNotFoundException
        //     604	801	970	java/io/FileNotFoundException
        //     849	881	970	java/io/FileNotFoundException
    }

    private static JournaledFile makeJournaledFile()
    {
        return new JournaledFile(new File("/data/system/device_policies.xml"), new File("/data/system/device_policies.xml.tmp"));
    }

    private void resetGlobalProxyLocked()
    {
        int i = this.mAdminList.size();
        int j = 0;
        if (j < i)
        {
            ActiveAdmin localActiveAdmin = (ActiveAdmin)this.mAdminList.get(j);
            if (localActiveAdmin.specifiesGlobalProxy)
                saveGlobalProxyLocked(localActiveAdmin.globalProxySpec, localActiveAdmin.globalProxyExclusionList);
        }
        while (true)
        {
            return;
            j++;
            break;
            saveGlobalProxyLocked(null, null);
        }
    }

    private void saveGlobalProxyLocked(String paramString1, String paramString2)
    {
        if (paramString2 == null)
            paramString2 = "";
        if (paramString1 == null)
            paramString1 = "";
        String[] arrayOfString = paramString1.trim().split(":");
        int i = 8080;
        if (arrayOfString.length > 1);
        try
        {
            int j = Integer.parseInt(arrayOfString[1]);
            i = j;
            label50: String str = paramString2.trim();
            ContentResolver localContentResolver = this.mContext.getContentResolver();
            Settings.Secure.putString(localContentResolver, "global_http_proxy_host", arrayOfString[0]);
            Settings.Secure.putInt(localContentResolver, "global_http_proxy_port", i);
            Settings.Secure.putString(localContentResolver, "global_http_proxy_exclusion_list", str);
            return;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            break label50;
        }
    }

    private void saveSettingsLocked()
    {
        JournaledFile localJournaledFile = makeJournaledFile();
        Object localObject = null;
        try
        {
            localFileOutputStream = new FileOutputStream(localJournaledFile.chooseForWrite(), false);
        }
        catch (IOException localIOException3)
        {
            try
            {
                FastXmlSerializer localFastXmlSerializer = new FastXmlSerializer();
                localFastXmlSerializer.setOutput(localFileOutputStream, "utf-8");
                localFastXmlSerializer.startDocument(null, Boolean.valueOf(true));
                localFastXmlSerializer.startTag(null, "policies");
                int i = this.mAdminList.size();
                j = 0;
                if (j < i)
                {
                    ActiveAdmin localActiveAdmin = (ActiveAdmin)this.mAdminList.get(j);
                    if (localActiveAdmin != null)
                    {
                        localFastXmlSerializer.startTag(null, "admin");
                        localFastXmlSerializer.attribute(null, "name", localActiveAdmin.info.getComponent().flattenToString());
                        localActiveAdmin.writeToXml(localFastXmlSerializer);
                        localFastXmlSerializer.endTag(null, "admin");
                    }
                }
                else
                {
                    if (this.mPasswordOwner >= 0)
                    {
                        localFastXmlSerializer.startTag(null, "password-owner");
                        localFastXmlSerializer.attribute(null, "value", Integer.toString(this.mPasswordOwner));
                        localFastXmlSerializer.endTag(null, "password-owner");
                    }
                    if (this.mFailedPasswordAttempts != 0)
                    {
                        localFastXmlSerializer.startTag(null, "failed-password-attempts");
                        localFastXmlSerializer.attribute(null, "value", Integer.toString(this.mFailedPasswordAttempts));
                        localFastXmlSerializer.endTag(null, "failed-password-attempts");
                    }
                    if ((this.mActivePasswordQuality != 0) || (this.mActivePasswordLength != 0) || (this.mActivePasswordUpperCase != 0) || (this.mActivePasswordLowerCase != 0) || (this.mActivePasswordLetters != 0) || (this.mActivePasswordNumeric != 0) || (this.mActivePasswordSymbols != 0) || (this.mActivePasswordNonLetter != 0))
                    {
                        localFastXmlSerializer.startTag(null, "active-password");
                        localFastXmlSerializer.attribute(null, "quality", Integer.toString(this.mActivePasswordQuality));
                        localFastXmlSerializer.attribute(null, "length", Integer.toString(this.mActivePasswordLength));
                        localFastXmlSerializer.attribute(null, "uppercase", Integer.toString(this.mActivePasswordUpperCase));
                        localFastXmlSerializer.attribute(null, "lowercase", Integer.toString(this.mActivePasswordLowerCase));
                        localFastXmlSerializer.attribute(null, "letters", Integer.toString(this.mActivePasswordLetters));
                        localFastXmlSerializer.attribute(null, "numeric", Integer.toString(this.mActivePasswordNumeric));
                        localFastXmlSerializer.attribute(null, "symbols", Integer.toString(this.mActivePasswordSymbols));
                        localFastXmlSerializer.attribute(null, "nonletter", Integer.toString(this.mActivePasswordNonLetter));
                        localFastXmlSerializer.endTag(null, "active-password");
                    }
                    localFastXmlSerializer.endTag(null, "policies");
                    localFastXmlSerializer.endDocument();
                    localFileOutputStream.close();
                    localJournaledFile.commit();
                    sendChangedNotification();
                    while (true)
                    {
                        return;
                        localIOException3 = localIOException3;
                        if (localObject != null);
                        try
                        {
                            localObject.close();
                            label532: localJournaledFile.rollback();
                        }
                        catch (IOException localIOException2)
                        {
                            break label532;
                        }
                    }
                }
            }
            catch (IOException localIOException1)
            {
                while (true)
                {
                    FileOutputStream localFileOutputStream;
                    int j;
                    localObject = localFileOutputStream;
                    continue;
                    j++;
                }
            }
        }
    }

    private void sendChangedNotification()
    {
        Intent localIntent = new Intent("android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED");
        localIntent.setFlags(1073741824);
        this.mContext.sendBroadcast(localIntent);
    }

    private void setEncryptionRequested(boolean paramBoolean)
    {
    }

    private void updatePasswordExpirationsLocked()
    {
        int i = this.mAdminList.size();
        if (i > 0)
        {
            int j = 0;
            if (j < i)
            {
                ActiveAdmin localActiveAdmin = (ActiveAdmin)this.mAdminList.get(j);
                long l1;
                if (localActiveAdmin.info.usesPolicy(6))
                {
                    l1 = localActiveAdmin.passwordExpirationTimeout;
                    if (l1 <= 0L)
                        break label76;
                }
                label76: for (long l2 = l1 + System.currentTimeMillis(); ; l2 = 0L)
                {
                    localActiveAdmin.passwordExpirationDate = l2;
                    j++;
                    break;
                }
            }
            saveSettingsLocked();
        }
    }

    static void validateQualityConstant(int paramInt)
    {
        switch (paramInt)
        {
        default:
            throw new IllegalArgumentException("Invalid quality constant: 0x" + Integer.toHexString(paramInt));
        case 0:
        case 32768:
        case 65536:
        case 131072:
        case 262144:
        case 327680:
        case 393216:
        }
    }

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.DUMP") != 0)
        {
            paramPrintWriter.println("Permission Denial: can't dump DevicePolicyManagerService from from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid());
            return;
        }
        PrintWriterPrinter localPrintWriterPrinter = new PrintWriterPrinter(paramPrintWriter);
        while (true)
        {
            int j;
            try
            {
                localPrintWriterPrinter.println("Current Device Policy Manager state:");
                localPrintWriterPrinter.println("    Enabled Device Admins:");
                int i = this.mAdminList.size();
                j = 0;
                if (j < i)
                {
                    ActiveAdmin localActiveAdmin = (ActiveAdmin)this.mAdminList.get(j);
                    if (localActiveAdmin != null)
                    {
                        paramPrintWriter.print("    ");
                        paramPrintWriter.print(localActiveAdmin.info.getComponent().flattenToShortString());
                        paramPrintWriter.println(":");
                        localActiveAdmin.dump("        ", paramPrintWriter);
                    }
                }
                else
                {
                    paramPrintWriter.println(" ");
                    paramPrintWriter.print("    mPasswordOwner=");
                    paramPrintWriter.println(this.mPasswordOwner);
                    break;
                }
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
            j++;
        }
    }

    public DeviceAdminInfo findAdmin(ComponentName paramComponentName)
    {
        Intent localIntent = new Intent();
        localIntent.setComponent(paramComponentName);
        List localList = this.mContext.getPackageManager().queryBroadcastReceivers(localIntent, 128);
        if ((localList == null) || (localList.size() <= 0))
            throw new IllegalArgumentException("Unknown admin: " + paramComponentName);
        try
        {
            DeviceAdminInfo localDeviceAdminInfo1 = new DeviceAdminInfo(this.mContext, (ResolveInfo)localList.get(0));
            localDeviceAdminInfo2 = localDeviceAdminInfo1;
            return localDeviceAdminInfo2;
        }
        catch (XmlPullParserException localXmlPullParserException)
        {
            while (true)
            {
                Slog.w("DevicePolicyManagerService", "Bad device admin requested: " + paramComponentName, localXmlPullParserException);
                localDeviceAdminInfo2 = null;
            }
        }
        catch (IOException localIOException)
        {
            while (true)
            {
                Slog.w("DevicePolicyManagerService", "Bad device admin requested: " + paramComponentName, localIOException);
                DeviceAdminInfo localDeviceAdminInfo2 = null;
            }
        }
    }

    ActiveAdmin getActiveAdminForCallerLocked(ComponentName paramComponentName, int paramInt)
        throws SecurityException
    {
        int i = Binder.getCallingUid();
        ActiveAdmin localActiveAdmin;
        int j;
        if (paramComponentName != null)
        {
            localActiveAdmin = (ActiveAdmin)this.mAdminMap.get(paramComponentName);
            if (localActiveAdmin == null)
                throw new SecurityException("No active admin " + paramComponentName);
            if (localActiveAdmin.getUid() != i)
                throw new SecurityException("Admin " + paramComponentName + " is not owned by uid " + Binder.getCallingUid());
            if (!localActiveAdmin.info.usesPolicy(paramInt))
                throw new SecurityException("Admin " + localActiveAdmin.info.getComponent() + " did not specify uses-policy for: " + localActiveAdmin.info.getTagForPolicy(paramInt));
        }
        else
        {
            j = this.mAdminList.size();
        }
        for (int k = 0; k < j; k++)
        {
            localActiveAdmin = (ActiveAdmin)this.mAdminList.get(k);
            if ((localActiveAdmin.getUid() == i) && (localActiveAdmin.info.usesPolicy(paramInt)))
                return localActiveAdmin;
        }
        throw new SecurityException("No active admin owned by uid " + Binder.getCallingUid() + " for policy #" + paramInt);
    }

    ActiveAdmin getActiveAdminUncheckedLocked(ComponentName paramComponentName)
    {
        ActiveAdmin localActiveAdmin = (ActiveAdmin)this.mAdminMap.get(paramComponentName);
        if ((localActiveAdmin != null) && (paramComponentName.getPackageName().equals(localActiveAdmin.info.getActivityInfo().packageName)) && (paramComponentName.getClassName().equals(localActiveAdmin.info.getActivityInfo().name)));
        while (true)
        {
            return localActiveAdmin;
            localActiveAdmin = null;
        }
    }

    public List<ComponentName> getActiveAdmins()
    {
        ArrayList localArrayList;
        try
        {
            int i = this.mAdminList.size();
            if (i <= 0)
            {
                localArrayList = null;
            }
            else
            {
                localArrayList = new ArrayList(i);
                for (int j = 0; j < i; j++)
                    localArrayList.add(((ActiveAdmin)this.mAdminList.get(j)).info.getComponent());
            }
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
        return localArrayList;
    }

    // ERROR //
    public boolean getCameraDisabled(ComponentName paramComponentName)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore_2
        //     2: aload_0
        //     3: monitorenter
        //     4: aload_1
        //     5: ifnull +26 -> 31
        //     8: aload_0
        //     9: aload_1
        //     10: invokevirtual 212	com/android/server/DevicePolicyManagerService:getActiveAdminUncheckedLocked	(Landroid/content/ComponentName;)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
        //     13: astore 6
        //     15: aload 6
        //     17: ifnull +9 -> 26
        //     20: aload 6
        //     22: getfield 736	com/android/server/DevicePolicyManagerService$ActiveAdmin:disableCamera	Z
        //     25: istore_2
        //     26: aload_0
        //     27: monitorexit
        //     28: goto +60 -> 88
        //     31: aload_0
        //     32: getfield 111	com/android/server/DevicePolicyManagerService:mAdminList	Ljava/util/ArrayList;
        //     35: invokevirtual 218	java/util/ArrayList:size	()I
        //     38: istore 4
        //     40: iconst_0
        //     41: istore 5
        //     43: iload 5
        //     45: iload 4
        //     47: if_icmpge +39 -> 86
        //     50: aload_0
        //     51: getfield 111	com/android/server/DevicePolicyManagerService:mAdminList	Ljava/util/ArrayList;
        //     54: iload 5
        //     56: invokevirtual 221	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     59: checkcast 15	com/android/server/DevicePolicyManagerService$ActiveAdmin
        //     62: getfield 736	com/android/server/DevicePolicyManagerService$ActiveAdmin:disableCamera	Z
        //     65: ifeq +15 -> 80
        //     68: iconst_1
        //     69: istore_2
        //     70: aload_0
        //     71: monitorexit
        //     72: goto +16 -> 88
        //     75: astore_3
        //     76: aload_0
        //     77: monitorexit
        //     78: aload_3
        //     79: athrow
        //     80: iinc 5 1
        //     83: goto -40 -> 43
        //     86: aload_0
        //     87: monitorexit
        //     88: iload_2
        //     89: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     8	78	75	finally
        //     86	88	75	finally
    }

    public int getCurrentFailedPasswordAttempts()
    {
        try
        {
            getActiveAdminForCallerLocked(null, 1);
            int i = this.mFailedPasswordAttempts;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public ComponentName getGlobalProxyAdmin()
    {
        while (true)
        {
            int j;
            ComponentName localComponentName;
            try
            {
                int i = this.mAdminList.size();
                j = 0;
                if (j < i)
                {
                    ActiveAdmin localActiveAdmin = (ActiveAdmin)this.mAdminList.get(j);
                    if (!localActiveAdmin.specifiesGlobalProxy)
                        break label69;
                    localComponentName = localActiveAdmin.info.getComponent();
                }
                else
                {
                    localComponentName = null;
                }
            }
            finally
            {
            }
            label69: j++;
        }
    }

    public int getMaximumFailedPasswordsForWipe(ComponentName paramComponentName)
    {
        int i = 0;
        if (paramComponentName != null);
        while (true)
        {
            int k;
            try
            {
                ActiveAdmin localActiveAdmin2 = getActiveAdminUncheckedLocked(paramComponentName);
                if (localActiveAdmin2 != null)
                {
                    m = localActiveAdmin2.maximumFailedPasswordsForWipe;
                    continue;
                    int j = this.mAdminList.size();
                    k = 0;
                    if (k < j)
                    {
                        ActiveAdmin localActiveAdmin1 = (ActiveAdmin)this.mAdminList.get(k);
                        if (i == 0)
                        {
                            i = localActiveAdmin1.maximumFailedPasswordsForWipe;
                            break label126;
                        }
                        if ((localActiveAdmin1.maximumFailedPasswordsForWipe == 0) || (i <= localActiveAdmin1.maximumFailedPasswordsForWipe))
                            break label126;
                        i = localActiveAdmin1.maximumFailedPasswordsForWipe;
                        break label126;
                    }
                    m = i;
                }
            }
            finally
            {
            }
            int m = 0;
            continue;
            label126: k++;
        }
    }

    public long getMaximumTimeToLock(ComponentName paramComponentName)
    {
        long l1 = 0L;
        if (paramComponentName != null);
        while (true)
        {
            int j;
            try
            {
                ActiveAdmin localActiveAdmin2 = getActiveAdminUncheckedLocked(paramComponentName);
                if (localActiveAdmin2 != null)
                {
                    l2 = localActiveAdmin2.maximumTimeToUnlock;
                    continue;
                    int i = this.mAdminList.size();
                    j = 0;
                    if (j < i)
                    {
                        ActiveAdmin localActiveAdmin1 = (ActiveAdmin)this.mAdminList.get(j);
                        if (l1 == 0L)
                        {
                            l1 = localActiveAdmin1.maximumTimeToUnlock;
                            break label133;
                        }
                        if ((localActiveAdmin1.maximumTimeToUnlock == 0L) || (l1 <= localActiveAdmin1.maximumTimeToUnlock))
                            break label133;
                        l1 = localActiveAdmin1.maximumTimeToUnlock;
                        break label133;
                    }
                    l2 = l1;
                }
            }
            finally
            {
            }
            long l2 = l1;
            continue;
            label133: j++;
        }
    }

    public long getPasswordExpiration(ComponentName paramComponentName)
    {
        try
        {
            long l = getPasswordExpirationLocked(paramComponentName);
            return l;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public long getPasswordExpirationTimeout(ComponentName paramComponentName)
    {
        if (paramComponentName != null);
        while (true)
        {
            int j;
            try
            {
                ActiveAdmin localActiveAdmin2 = getActiveAdminUncheckedLocked(paramComponentName);
                if (localActiveAdmin2 == null)
                    break label113;
                l = localActiveAdmin2.passwordExpirationTimeout;
                break label111;
                l = 0L;
                int i = this.mAdminList.size();
                j = 0;
                if (j < i)
                {
                    ActiveAdmin localActiveAdmin1 = (ActiveAdmin)this.mAdminList.get(j);
                    if ((l != 0L) && ((localActiveAdmin1.passwordExpirationTimeout == 0L) || (l <= localActiveAdmin1.passwordExpirationTimeout)))
                        break label118;
                    l = localActiveAdmin1.passwordExpirationTimeout;
                    break label118;
                }
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
            label111: return l;
            label113: long l = 0L;
            continue;
            label118: j++;
        }
    }

    public int getPasswordHistoryLength(ComponentName paramComponentName)
    {
        int i = 0;
        if (paramComponentName != null);
        while (true)
        {
            int k;
            try
            {
                ActiveAdmin localActiveAdmin2 = getActiveAdminUncheckedLocked(paramComponentName);
                if (localActiveAdmin2 != null)
                {
                    m = localActiveAdmin2.passwordHistoryLength;
                    continue;
                    int j = this.mAdminList.size();
                    k = 0;
                    if (k < j)
                    {
                        ActiveAdmin localActiveAdmin1 = (ActiveAdmin)this.mAdminList.get(k);
                        if (i >= localActiveAdmin1.passwordHistoryLength)
                            break label105;
                        i = localActiveAdmin1.passwordHistoryLength;
                        break label105;
                    }
                    m = i;
                }
            }
            finally
            {
            }
            int m = 0;
            continue;
            label105: k++;
        }
    }

    public int getPasswordMinimumLength(ComponentName paramComponentName)
    {
        int i = 0;
        if (paramComponentName != null);
        while (true)
        {
            int k;
            try
            {
                ActiveAdmin localActiveAdmin2 = getActiveAdminUncheckedLocked(paramComponentName);
                if (localActiveAdmin2 != null)
                {
                    m = localActiveAdmin2.minimumPasswordLength;
                    continue;
                    int j = this.mAdminList.size();
                    k = 0;
                    if (k < j)
                    {
                        ActiveAdmin localActiveAdmin1 = (ActiveAdmin)this.mAdminList.get(k);
                        if (i >= localActiveAdmin1.minimumPasswordLength)
                            break label105;
                        i = localActiveAdmin1.minimumPasswordLength;
                        break label105;
                    }
                    m = i;
                }
            }
            finally
            {
            }
            int m = 0;
            continue;
            label105: k++;
        }
    }

    public int getPasswordMinimumLetters(ComponentName paramComponentName)
    {
        int i = 0;
        if (paramComponentName != null);
        while (true)
        {
            int k;
            try
            {
                ActiveAdmin localActiveAdmin2 = getActiveAdminUncheckedLocked(paramComponentName);
                if (localActiveAdmin2 != null)
                {
                    m = localActiveAdmin2.minimumPasswordLetters;
                    continue;
                    int j = this.mAdminList.size();
                    k = 0;
                    if (k < j)
                    {
                        ActiveAdmin localActiveAdmin1 = (ActiveAdmin)this.mAdminList.get(k);
                        if (i >= localActiveAdmin1.minimumPasswordLetters)
                            break label105;
                        i = localActiveAdmin1.minimumPasswordLetters;
                        break label105;
                    }
                    m = i;
                }
            }
            finally
            {
            }
            int m = 0;
            continue;
            label105: k++;
        }
    }

    public int getPasswordMinimumLowerCase(ComponentName paramComponentName)
    {
        int i = 0;
        if (paramComponentName != null);
        while (true)
        {
            int k;
            try
            {
                ActiveAdmin localActiveAdmin2 = getActiveAdminUncheckedLocked(paramComponentName);
                if (localActiveAdmin2 != null)
                {
                    m = localActiveAdmin2.minimumPasswordLowerCase;
                    continue;
                    int j = this.mAdminList.size();
                    k = 0;
                    if (k < j)
                    {
                        ActiveAdmin localActiveAdmin1 = (ActiveAdmin)this.mAdminList.get(k);
                        if (i >= localActiveAdmin1.minimumPasswordLowerCase)
                            break label105;
                        i = localActiveAdmin1.minimumPasswordLowerCase;
                        break label105;
                    }
                    m = i;
                }
            }
            finally
            {
            }
            int m = 0;
            continue;
            label105: k++;
        }
    }

    public int getPasswordMinimumNonLetter(ComponentName paramComponentName)
    {
        int i = 0;
        if (paramComponentName != null);
        while (true)
        {
            int k;
            try
            {
                ActiveAdmin localActiveAdmin2 = getActiveAdminUncheckedLocked(paramComponentName);
                if (localActiveAdmin2 != null)
                {
                    m = localActiveAdmin2.minimumPasswordNonLetter;
                    continue;
                    int j = this.mAdminList.size();
                    k = 0;
                    if (k < j)
                    {
                        ActiveAdmin localActiveAdmin1 = (ActiveAdmin)this.mAdminList.get(k);
                        if (i >= localActiveAdmin1.minimumPasswordNonLetter)
                            break label105;
                        i = localActiveAdmin1.minimumPasswordNonLetter;
                        break label105;
                    }
                    m = i;
                }
            }
            finally
            {
            }
            int m = 0;
            continue;
            label105: k++;
        }
    }

    public int getPasswordMinimumNumeric(ComponentName paramComponentName)
    {
        int i = 0;
        if (paramComponentName != null);
        while (true)
        {
            int k;
            try
            {
                ActiveAdmin localActiveAdmin2 = getActiveAdminUncheckedLocked(paramComponentName);
                if (localActiveAdmin2 != null)
                {
                    m = localActiveAdmin2.minimumPasswordNumeric;
                    continue;
                    int j = this.mAdminList.size();
                    k = 0;
                    if (k < j)
                    {
                        ActiveAdmin localActiveAdmin1 = (ActiveAdmin)this.mAdminList.get(k);
                        if (i >= localActiveAdmin1.minimumPasswordNumeric)
                            break label105;
                        i = localActiveAdmin1.minimumPasswordNumeric;
                        break label105;
                    }
                    m = i;
                }
            }
            finally
            {
            }
            int m = 0;
            continue;
            label105: k++;
        }
    }

    public int getPasswordMinimumSymbols(ComponentName paramComponentName)
    {
        int i = 0;
        if (paramComponentName != null);
        while (true)
        {
            int k;
            try
            {
                ActiveAdmin localActiveAdmin2 = getActiveAdminUncheckedLocked(paramComponentName);
                if (localActiveAdmin2 != null)
                {
                    m = localActiveAdmin2.minimumPasswordSymbols;
                    continue;
                    int j = this.mAdminList.size();
                    k = 0;
                    if (k < j)
                    {
                        ActiveAdmin localActiveAdmin1 = (ActiveAdmin)this.mAdminList.get(k);
                        if (i >= localActiveAdmin1.minimumPasswordSymbols)
                            break label105;
                        i = localActiveAdmin1.minimumPasswordSymbols;
                        break label105;
                    }
                    m = i;
                }
            }
            finally
            {
            }
            int m = 0;
            continue;
            label105: k++;
        }
    }

    public int getPasswordMinimumUpperCase(ComponentName paramComponentName)
    {
        int i = 0;
        if (paramComponentName != null);
        while (true)
        {
            int k;
            try
            {
                ActiveAdmin localActiveAdmin2 = getActiveAdminUncheckedLocked(paramComponentName);
                if (localActiveAdmin2 != null)
                {
                    m = localActiveAdmin2.minimumPasswordUpperCase;
                    continue;
                    int j = this.mAdminList.size();
                    k = 0;
                    if (k < j)
                    {
                        ActiveAdmin localActiveAdmin1 = (ActiveAdmin)this.mAdminList.get(k);
                        if (i >= localActiveAdmin1.minimumPasswordUpperCase)
                            break label105;
                        i = localActiveAdmin1.minimumPasswordUpperCase;
                        break label105;
                    }
                    m = i;
                }
            }
            finally
            {
            }
            int m = 0;
            continue;
            label105: k++;
        }
    }

    public int getPasswordQuality(ComponentName paramComponentName)
    {
        int i = 0;
        if (paramComponentName != null);
        while (true)
        {
            int k;
            try
            {
                ActiveAdmin localActiveAdmin2 = getActiveAdminUncheckedLocked(paramComponentName);
                if (localActiveAdmin2 != null)
                {
                    m = localActiveAdmin2.passwordQuality;
                    continue;
                    int j = this.mAdminList.size();
                    k = 0;
                    if (k < j)
                    {
                        ActiveAdmin localActiveAdmin1 = (ActiveAdmin)this.mAdminList.get(k);
                        if (i >= localActiveAdmin1.passwordQuality)
                            break label105;
                        i = localActiveAdmin1.passwordQuality;
                        break label105;
                    }
                    m = i;
                }
            }
            finally
            {
            }
            int m = 0;
            continue;
            label105: k++;
        }
    }

    // ERROR //
    public void getRemoveWarning(ComponentName paramComponentName, final RemoteCallback paramRemoteCallback)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 118	com/android/server/DevicePolicyManagerService:mContext	Landroid/content/Context;
        //     4: ldc_w 795
        //     7: aconst_null
        //     8: invokevirtual 798	android/content/Context:enforceCallingOrSelfPermission	(Ljava/lang/String;Ljava/lang/String;)V
        //     11: aload_0
        //     12: monitorenter
        //     13: aload_0
        //     14: aload_1
        //     15: invokevirtual 212	com/android/server/DevicePolicyManagerService:getActiveAdminUncheckedLocked	(Landroid/content/ComponentName;)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
        //     18: astore 4
        //     20: aload 4
        //     22: ifnonnull +13 -> 35
        //     25: aload_2
        //     26: aconst_null
        //     27: invokevirtual 804	android/os/RemoteCallback:sendResult	(Landroid/os/Bundle;)V
        //     30: aload_0
        //     31: monitorexit
        //     32: goto +68 -> 100
        //     35: new 578	android/content/Intent
        //     38: dup
        //     39: ldc_w 806
        //     42: invokespecial 581	android/content/Intent:<init>	(Ljava/lang/String;)V
        //     45: astore 5
        //     47: aload 5
        //     49: aload 4
        //     51: getfield 242	com/android/server/DevicePolicyManagerService$ActiveAdmin:info	Landroid/app/admin/DeviceAdminInfo;
        //     54: invokevirtual 414	android/app/admin/DeviceAdminInfo:getComponent	()Landroid/content/ComponentName;
        //     57: invokevirtual 661	android/content/Intent:setComponent	(Landroid/content/ComponentName;)Landroid/content/Intent;
        //     60: pop
        //     61: aload_0
        //     62: getfield 118	com/android/server/DevicePolicyManagerService:mContext	Landroid/content/Context;
        //     65: aload 5
        //     67: aconst_null
        //     68: new 10	com/android/server/DevicePolicyManagerService$3
        //     71: dup
        //     72: aload_0
        //     73: aload_2
        //     74: invokespecial 809	com/android/server/DevicePolicyManagerService$3:<init>	(Lcom/android/server/DevicePolicyManagerService;Landroid/os/RemoteCallback;)V
        //     77: aconst_null
        //     78: bipush 255
        //     80: aconst_null
        //     81: aconst_null
        //     82: invokevirtual 813	android/content/Context:sendOrderedBroadcast	(Landroid/content/Intent;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V
        //     85: aload_0
        //     86: monitorexit
        //     87: goto +13 -> 100
        //     90: astore_3
        //     91: aload_0
        //     92: monitorexit
        //     93: aload_3
        //     94: athrow
        //     95: astore 7
        //     97: goto -67 -> 30
        //     100: return
        //
        // Exception table:
        //     from	to	target	type
        //     13	20	90	finally
        //     25	30	90	finally
        //     30	93	90	finally
        //     25	30	95	android/os/RemoteException
    }

    public boolean getStorageEncryption(ComponentName paramComponentName)
    {
        if (paramComponentName != null);
        while (true)
        {
            try
            {
                ActiveAdmin localActiveAdmin = getActiveAdminUncheckedLocked(paramComponentName);
                if (localActiveAdmin != null)
                {
                    bool = localActiveAdmin.encryptionRequested;
                    continue;
                    int i = this.mAdminList.size();
                    j = 0;
                    if (j < i)
                        if (((ActiveAdmin)this.mAdminList.get(j)).encryptionRequested)
                            bool = true;
                }
            }
            finally
            {
                int j;
                throw localObject;
                j++;
                continue;
            }
            boolean bool = false;
        }
    }

    public int getStorageEncryptionStatus()
    {
        return getEncryptionStatus();
    }

    // ERROR //
    public boolean hasGrantedPolicy(ComponentName paramComponentName, int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: aload_1
        //     4: invokevirtual 212	com/android/server/DevicePolicyManagerService:getActiveAdminUncheckedLocked	(Landroid/content/ComponentName;)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
        //     7: astore 4
        //     9: aload 4
        //     11: ifnonnull +36 -> 47
        //     14: new 688	java/lang/SecurityException
        //     17: dup
        //     18: new 329	java/lang/StringBuilder
        //     21: dup
        //     22: invokespecial 330	java/lang/StringBuilder:<init>	()V
        //     25: ldc_w 693
        //     28: invokevirtual 336	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     31: aload_1
        //     32: invokevirtual 346	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     35: invokevirtual 339	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     38: invokespecial 694	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     41: athrow
        //     42: astore_3
        //     43: aload_0
        //     44: monitorexit
        //     45: aload_3
        //     46: athrow
        //     47: aload 4
        //     49: getfield 242	com/android/server/DevicePolicyManagerService$ActiveAdmin:info	Landroid/app/admin/DeviceAdminInfo;
        //     52: iload_2
        //     53: invokevirtual 248	android/app/admin/DeviceAdminInfo:usesPolicy	(I)Z
        //     56: istore 5
        //     58: aload_0
        //     59: monitorexit
        //     60: iload 5
        //     62: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     2	45	42	finally
        //     47	60	42	finally
    }

    // ERROR //
    public boolean isActivePasswordSufficient()
    {
        // Byte code:
        //     0: iconst_1
        //     1: istore_1
        //     2: aload_0
        //     3: monitorenter
        //     4: aload_0
        //     5: aconst_null
        //     6: iconst_0
        //     7: invokevirtual 739	com/android/server/DevicePolicyManagerService:getActiveAdminForCallerLocked	(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
        //     10: pop
        //     11: aload_0
        //     12: getfield 74	com/android/server/DevicePolicyManagerService:mActivePasswordQuality	I
        //     15: aload_0
        //     16: aconst_null
        //     17: invokevirtual 823	com/android/server/DevicePolicyManagerService:getPasswordQuality	(Landroid/content/ComponentName;)I
        //     20: if_icmplt +15 -> 35
        //     23: aload_0
        //     24: getfield 76	com/android/server/DevicePolicyManagerService:mActivePasswordLength	I
        //     27: aload_0
        //     28: aconst_null
        //     29: invokevirtual 825	com/android/server/DevicePolicyManagerService:getPasswordMinimumLength	(Landroid/content/ComponentName;)I
        //     32: if_icmpge +10 -> 42
        //     35: aload_0
        //     36: monitorexit
        //     37: iconst_0
        //     38: istore_1
        //     39: goto +105 -> 144
        //     42: aload_0
        //     43: getfield 74	com/android/server/DevicePolicyManagerService:mActivePasswordQuality	I
        //     46: ldc_w 826
        //     49: if_icmpeq +13 -> 62
        //     52: aload_0
        //     53: monitorexit
        //     54: goto +90 -> 144
        //     57: astore_2
        //     58: aload_0
        //     59: monitorexit
        //     60: aload_2
        //     61: athrow
        //     62: aload_0
        //     63: getfield 78	com/android/server/DevicePolicyManagerService:mActivePasswordUpperCase	I
        //     66: aload_0
        //     67: aconst_null
        //     68: invokevirtual 828	com/android/server/DevicePolicyManagerService:getPasswordMinimumUpperCase	(Landroid/content/ComponentName;)I
        //     71: if_icmplt +68 -> 139
        //     74: aload_0
        //     75: getfield 80	com/android/server/DevicePolicyManagerService:mActivePasswordLowerCase	I
        //     78: aload_0
        //     79: aconst_null
        //     80: invokevirtual 830	com/android/server/DevicePolicyManagerService:getPasswordMinimumLowerCase	(Landroid/content/ComponentName;)I
        //     83: if_icmplt +56 -> 139
        //     86: aload_0
        //     87: getfield 82	com/android/server/DevicePolicyManagerService:mActivePasswordLetters	I
        //     90: aload_0
        //     91: aconst_null
        //     92: invokevirtual 832	com/android/server/DevicePolicyManagerService:getPasswordMinimumLetters	(Landroid/content/ComponentName;)I
        //     95: if_icmplt +44 -> 139
        //     98: aload_0
        //     99: getfield 84	com/android/server/DevicePolicyManagerService:mActivePasswordNumeric	I
        //     102: aload_0
        //     103: aconst_null
        //     104: invokevirtual 834	com/android/server/DevicePolicyManagerService:getPasswordMinimumNumeric	(Landroid/content/ComponentName;)I
        //     107: if_icmplt +32 -> 139
        //     110: aload_0
        //     111: getfield 86	com/android/server/DevicePolicyManagerService:mActivePasswordSymbols	I
        //     114: aload_0
        //     115: aconst_null
        //     116: invokevirtual 836	com/android/server/DevicePolicyManagerService:getPasswordMinimumSymbols	(Landroid/content/ComponentName;)I
        //     119: if_icmplt +20 -> 139
        //     122: aload_0
        //     123: getfield 88	com/android/server/DevicePolicyManagerService:mActivePasswordNonLetter	I
        //     126: aload_0
        //     127: aconst_null
        //     128: invokevirtual 838	com/android/server/DevicePolicyManagerService:getPasswordMinimumNonLetter	(Landroid/content/ComponentName;)I
        //     131: if_icmplt +8 -> 139
        //     134: aload_0
        //     135: monitorexit
        //     136: goto +8 -> 144
        //     139: iconst_0
        //     140: istore_1
        //     141: goto -7 -> 134
        //     144: iload_1
        //     145: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     4	60	57	finally
        //     62	136	57	finally
    }

    public boolean isAdminActive(ComponentName paramComponentName)
    {
        while (true)
        {
            try
            {
                if (getActiveAdminUncheckedLocked(paramComponentName) != null)
                {
                    bool = true;
                    return bool;
                }
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
            boolean bool = false;
        }
    }

    // ERROR //
    public void lockNow()
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: aconst_null
        //     4: iconst_3
        //     5: invokevirtual 739	com/android/server/DevicePolicyManagerService:getActiveAdminForCallerLocked	(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
        //     8: pop
        //     9: invokestatic 843	android/os/Binder:clearCallingIdentity	()J
        //     12: lstore_3
        //     13: aload_0
        //     14: getfield 194	com/android/server/DevicePolicyManagerService:mIPowerManager	Landroid/os/IPowerManager;
        //     17: invokestatic 848	android/os/SystemClock:uptimeMillis	()J
        //     20: iconst_1
        //     21: invokeinterface 854 4 0
        //     26: aload_0
        //     27: invokespecial 856	com/android/server/DevicePolicyManagerService:getWindowManager	()Landroid/view/IWindowManager;
        //     30: invokeinterface 860 1 0
        //     35: lload_3
        //     36: invokestatic 864	android/os/Binder:restoreCallingIdentity	(J)V
        //     39: aload_0
        //     40: monitorexit
        //     41: return
        //     42: astore 6
        //     44: lload_3
        //     45: invokestatic 864	android/os/Binder:restoreCallingIdentity	(J)V
        //     48: aload 6
        //     50: athrow
        //     51: astore_1
        //     52: aload_0
        //     53: monitorexit
        //     54: aload_1
        //     55: athrow
        //     56: astore 5
        //     58: lload_3
        //     59: invokestatic 864	android/os/Binder:restoreCallingIdentity	(J)V
        //     62: goto -23 -> 39
        //
        // Exception table:
        //     from	to	target	type
        //     13	35	42	finally
        //     2	13	51	finally
        //     35	54	51	finally
        //     58	62	51	finally
        //     13	35	56	android/os/RemoteException
    }

    public boolean packageHasActiveAdmins(String paramString)
    {
        while (true)
        {
            int j;
            boolean bool;
            try
            {
                int i = this.mAdminList.size();
                j = 0;
                if (j < i)
                {
                    if (!((ActiveAdmin)this.mAdminList.get(j)).info.getPackageName().equals(paramString))
                        break label68;
                    bool = true;
                }
                else
                {
                    bool = false;
                }
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
            return bool;
            label68: j++;
        }
    }

    // ERROR //
    public void removeActiveAdmin(ComponentName paramComponentName)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: aload_1
        //     4: invokevirtual 212	com/android/server/DevicePolicyManagerService:getActiveAdminUncheckedLocked	(Landroid/content/ComponentName;)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
        //     7: astore_3
        //     8: aload_3
        //     9: ifnonnull +8 -> 17
        //     12: aload_0
        //     13: monitorexit
        //     14: goto +59 -> 73
        //     17: aload_3
        //     18: invokevirtual 697	com/android/server/DevicePolicyManagerService$ActiveAdmin:getUid	()I
        //     21: invokestatic 622	android/os/Binder:getCallingUid	()I
        //     24: if_icmpeq +14 -> 38
        //     27: aload_0
        //     28: getfield 118	com/android/server/DevicePolicyManagerService:mContext	Landroid/content/Context;
        //     31: ldc_w 795
        //     34: aconst_null
        //     35: invokevirtual 798	android/content/Context:enforceCallingOrSelfPermission	(Ljava/lang/String;Ljava/lang/String;)V
        //     38: invokestatic 843	android/os/Binder:clearCallingIdentity	()J
        //     41: lstore 4
        //     43: aload_0
        //     44: aload_1
        //     45: invokevirtual 871	com/android/server/DevicePolicyManagerService:removeActiveAdminLocked	(Landroid/content/ComponentName;)V
        //     48: lload 4
        //     50: invokestatic 864	android/os/Binder:restoreCallingIdentity	(J)V
        //     53: aload_0
        //     54: monitorexit
        //     55: goto +18 -> 73
        //     58: astore_2
        //     59: aload_0
        //     60: monitorexit
        //     61: aload_2
        //     62: athrow
        //     63: astore 6
        //     65: lload 4
        //     67: invokestatic 864	android/os/Binder:restoreCallingIdentity	(J)V
        //     70: aload 6
        //     72: athrow
        //     73: return
        //
        // Exception table:
        //     from	to	target	type
        //     2	43	58	finally
        //     48	61	58	finally
        //     65	73	58	finally
        //     43	48	63	finally
    }

    void removeActiveAdminLocked(final ComponentName paramComponentName)
    {
        final ActiveAdmin localActiveAdmin = getActiveAdminUncheckedLocked(paramComponentName);
        if (localActiveAdmin != null)
            sendAdminCommandLocked(localActiveAdmin, "android.app.action.DEVICE_ADMIN_DISABLED", new BroadcastReceiver()
            {
                public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
                {
                    synchronized (DevicePolicyManagerService.this)
                    {
                        boolean bool = localActiveAdmin.info.usesPolicy(5);
                        DevicePolicyManagerService.this.mAdminList.remove(localActiveAdmin);
                        DevicePolicyManagerService.this.mAdminMap.remove(paramComponentName);
                        DevicePolicyManagerService.this.validatePasswordOwnerLocked();
                        DevicePolicyManagerService.this.syncDeviceCapabilitiesLocked();
                        if (bool)
                            DevicePolicyManagerService.this.resetGlobalProxyLocked();
                        DevicePolicyManagerService.this.saveSettingsLocked();
                        DevicePolicyManagerService.this.updateMaximumTimeToLockLocked();
                        return;
                    }
                }
            });
    }

    public void reportFailedPasswordAttempt()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BIND_DEVICE_ADMIN", null);
        try
        {
            long l = Binder.clearCallingIdentity();
            try
            {
                this.mFailedPasswordAttempts = (1 + this.mFailedPasswordAttempts);
                saveSettingsLocked();
                int i = getMaximumFailedPasswordsForWipe(null);
                if ((i > 0) && (this.mFailedPasswordAttempts >= i))
                    wipeDataLocked(0);
                sendAdminCommandLocked("android.app.action.ACTION_PASSWORD_FAILED", 1);
                Binder.restoreCallingIdentity(l);
                return;
            }
            finally
            {
                localObject2 = finally;
                Binder.restoreCallingIdentity(l);
                throw localObject2;
            }
        }
        finally
        {
        }
    }

    public void reportSuccessfulPasswordAttempt()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BIND_DEVICE_ADMIN", null);
        try
        {
            long l;
            if ((this.mFailedPasswordAttempts != 0) || (this.mPasswordOwner >= 0))
                l = Binder.clearCallingIdentity();
            try
            {
                this.mFailedPasswordAttempts = 0;
                this.mPasswordOwner = -1;
                saveSettingsLocked();
                sendAdminCommandLocked("android.app.action.ACTION_PASSWORD_SUCCEEDED", 1);
                Binder.restoreCallingIdentity(l);
                return;
            }
            finally
            {
                localObject2 = finally;
                Binder.restoreCallingIdentity(l);
                throw localObject2;
            }
        }
        finally
        {
        }
    }

    public boolean resetPassword(String paramString, int paramInt)
    {
        int i;
        boolean bool;
        int n;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i13;
        int k;
        long l;
        int m;
        label729: 
        try
        {
            getActiveAdminForCallerLocked(null, 2);
            i = getPasswordQuality(null);
            if (i != 0)
            {
                int i14 = LockPatternUtils.computePasswordQuality(paramString);
                if ((i14 < i) && (i != 393216))
                {
                    Slog.w("DevicePolicyManagerService", "resetPassword: password quality 0x" + Integer.toHexString(i) + " does not meet required quality 0x" + Integer.toHexString(i));
                    bool = false;
                }
                else
                {
                    i = Math.max(i14, i);
                }
            }
            else
            {
                int j = getPasswordMinimumLength(null);
                if (paramString.length() < j)
                {
                    Slog.w("DevicePolicyManagerService", "resetPassword: password length " + paramString.length() + " does not meet required length " + j);
                    bool = false;
                }
            }
        }
        finally
        {
            throw localObject1;
            if (i == 393216)
            {
                n = 0;
                int i1 = 0;
                i2 = 0;
                i3 = 0;
                i4 = 0;
                i5 = 0;
                i6 = 0;
                if (i6 < paramString.length())
                {
                    i13 = paramString.charAt(i6);
                    if ((i13 < 65) || (i13 > 90))
                        break label738;
                    n++;
                    i1++;
                    break label732;
                }
                int i7 = getPasswordMinimumLetters(null);
                if (n < i7)
                {
                    Slog.w("DevicePolicyManagerService", "resetPassword: number of letters " + n + " does not meet required number of letters " + i7);
                    bool = false;
                    break label729;
                }
                int i8 = getPasswordMinimumNumeric(null);
                if (i3 < i8)
                {
                    Slog.w("DevicePolicyManagerService", "resetPassword: number of numerical digits " + i3 + " does not meet required number of numerical digits " + i8);
                    bool = false;
                    break label729;
                }
                int i9 = getPasswordMinimumLowerCase(null);
                if (i2 < i9)
                {
                    Slog.w("DevicePolicyManagerService", "resetPassword: number of lowercase letters " + i2 + " does not meet required number of lowercase letters " + i9);
                    bool = false;
                    break label729;
                }
                int i10 = getPasswordMinimumUpperCase(null);
                if (i1 < i10)
                {
                    Slog.w("DevicePolicyManagerService", "resetPassword: number of uppercase letters " + i1 + " does not meet required number of uppercase letters " + i10);
                    bool = false;
                    break label729;
                }
                int i11 = getPasswordMinimumSymbols(null);
                if (i4 < i11)
                {
                    Slog.w("DevicePolicyManagerService", "resetPassword: number of special symbols " + i4 + " does not meet required number of special symbols " + i11);
                    bool = false;
                    break label729;
                }
                int i12 = getPasswordMinimumNonLetter(null);
                if (i5 < i12)
                {
                    Slog.w("DevicePolicyManagerService", "resetPassword: number of non-letter characters " + i5 + " does not meet required number of non-letter characters " + i12);
                    bool = false;
                    break label729;
                }
            }
            k = Binder.getCallingUid();
            if ((this.mPasswordOwner >= 0) && (this.mPasswordOwner != k))
            {
                Slog.w("DevicePolicyManagerService", "resetPassword: already set by another uid and not entered by user");
                bool = false;
            }
            else
            {
                l = Binder.clearCallingIdentity();
            }
        }
        while (true)
        {
            label732: i6++;
            break;
            label738: if ((i13 >= 97) && (i13 <= 122))
            {
                n++;
                i2++;
            }
            else if ((i13 >= 48) && (i13 <= 57))
            {
                i3++;
                i5++;
            }
            else
            {
                i4++;
                i5++;
            }
        }
    }

    void sendAdminCommandLocked(ActiveAdmin paramActiveAdmin, String paramString)
    {
        sendAdminCommandLocked(paramActiveAdmin, paramString, null);
    }

    void sendAdminCommandLocked(ActiveAdmin paramActiveAdmin, String paramString, BroadcastReceiver paramBroadcastReceiver)
    {
        Intent localIntent = new Intent(paramString);
        localIntent.setComponent(paramActiveAdmin.info.getComponent());
        if (paramString.equals("android.app.action.ACTION_PASSWORD_EXPIRING"))
            localIntent.putExtra("expiration", paramActiveAdmin.passwordExpirationDate);
        if (paramBroadcastReceiver != null)
            this.mContext.sendOrderedBroadcast(localIntent, null, paramBroadcastReceiver, this.mHandler, -1, null, null);
        while (true)
        {
            return;
            this.mContext.sendBroadcast(localIntent);
        }
    }

    void sendAdminCommandLocked(String paramString, int paramInt)
    {
        int i = this.mAdminList.size();
        if (i > 0)
            for (int j = 0; j < i; j++)
            {
                ActiveAdmin localActiveAdmin = (ActiveAdmin)this.mAdminList.get(j);
                if (localActiveAdmin.info.usesPolicy(paramInt))
                    sendAdminCommandLocked(localActiveAdmin, paramString);
            }
    }

    public void setActiveAdmin(ComponentName paramComponentName, boolean paramBoolean)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BIND_DEVICE_ADMIN", null);
        DeviceAdminInfo localDeviceAdminInfo = findAdmin(paramComponentName);
        if (localDeviceAdminInfo == null)
            throw new IllegalArgumentException("Bad admin: " + paramComponentName);
        long l;
        try
        {
            l = Binder.clearCallingIdentity();
            if (!paramBoolean)
                try
                {
                    if (getActiveAdminUncheckedLocked(paramComponentName) != null)
                        throw new IllegalArgumentException("Admin is already added");
                }
                finally
                {
                    Binder.restoreCallingIdentity(l);
                }
        }
        finally
        {
        }
        ActiveAdmin localActiveAdmin = new ActiveAdmin(localDeviceAdminInfo);
        this.mAdminMap.put(paramComponentName, localActiveAdmin);
        int i = -1;
        int k;
        if (paramBoolean)
        {
            int j = this.mAdminList.size();
            k = 0;
            if (k < j)
            {
                if (!((ActiveAdmin)this.mAdminList.get(k)).info.getComponent().equals(paramComponentName))
                    break label211;
                i = k;
            }
        }
        if (i == -1)
            this.mAdminList.add(localActiveAdmin);
        while (true)
        {
            saveSettingsLocked();
            sendAdminCommandLocked(localActiveAdmin, "android.app.action.DEVICE_ADMIN_ENABLED");
            Binder.restoreCallingIdentity(l);
            return;
            label211: k++;
            break;
            this.mAdminList.set(i, localActiveAdmin);
        }
    }

    public void setActivePasswordState(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.BIND_DEVICE_ADMIN", null);
        validateQualityConstant(paramInt1);
        try
        {
            long l;
            if ((this.mActivePasswordQuality != paramInt1) || (this.mActivePasswordLength != paramInt2) || (this.mFailedPasswordAttempts != 0) || (this.mActivePasswordLetters != paramInt3) || (this.mActivePasswordUpperCase != paramInt4) || (this.mActivePasswordLowerCase != paramInt5) || (this.mActivePasswordNumeric != paramInt6) || (this.mActivePasswordSymbols != paramInt7) || (this.mActivePasswordNonLetter != paramInt8))
                l = Binder.clearCallingIdentity();
            try
            {
                this.mActivePasswordQuality = paramInt1;
                this.mActivePasswordLength = paramInt2;
                this.mActivePasswordLetters = paramInt3;
                this.mActivePasswordLowerCase = paramInt5;
                this.mActivePasswordUpperCase = paramInt4;
                this.mActivePasswordNumeric = paramInt6;
                this.mActivePasswordSymbols = paramInt7;
                this.mActivePasswordNonLetter = paramInt8;
                this.mFailedPasswordAttempts = 0;
                saveSettingsLocked();
                updatePasswordExpirationsLocked();
                setExpirationAlarmCheckLocked(this.mContext);
                sendAdminCommandLocked("android.app.action.ACTION_PASSWORD_CHANGED", 0);
                Binder.restoreCallingIdentity(l);
                return;
            }
            finally
            {
                localObject2 = finally;
                Binder.restoreCallingIdentity(l);
                throw localObject2;
            }
        }
        finally
        {
        }
    }

    // ERROR //
    public void setCameraDisabled(ComponentName paramComponentName, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_1
        //     3: ifnonnull +21 -> 24
        //     6: new 279	java/lang/NullPointerException
        //     9: dup
        //     10: ldc_w 977
        //     13: invokespecial 978	java/lang/NullPointerException:<init>	(Ljava/lang/String;)V
        //     16: athrow
        //     17: astore 4
        //     19: aload_0
        //     20: monitorexit
        //     21: aload 4
        //     23: athrow
        //     24: aload_0
        //     25: aload_1
        //     26: bipush 8
        //     28: invokevirtual 739	com/android/server/DevicePolicyManagerService:getActiveAdminForCallerLocked	(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
        //     31: astore_3
        //     32: aload_3
        //     33: getfield 736	com/android/server/DevicePolicyManagerService$ActiveAdmin:disableCamera	Z
        //     36: iload_2
        //     37: if_icmpeq +12 -> 49
        //     40: aload_3
        //     41: iload_2
        //     42: putfield 736	com/android/server/DevicePolicyManagerService$ActiveAdmin:disableCamera	Z
        //     45: aload_0
        //     46: invokespecial 164	com/android/server/DevicePolicyManagerService:saveSettingsLocked	()V
        //     49: aload_0
        //     50: invokevirtual 380	com/android/server/DevicePolicyManagerService:syncDeviceCapabilitiesLocked	()V
        //     53: aload_0
        //     54: monitorexit
        //     55: return
        //
        // Exception table:
        //     from	to	target	type
        //     6	21	17	finally
        //     24	55	17	finally
    }

    protected void setExpirationAlarmCheckLocked(Context paramContext)
    {
        long l1 = getPasswordExpirationLocked(null);
        long l2 = System.currentTimeMillis();
        long l3 = l1 - l2;
        long l5;
        if (l1 == 0L)
            l5 = 0L;
        while (true)
        {
            long l6 = Binder.clearCallingIdentity();
            try
            {
                AlarmManager localAlarmManager = (AlarmManager)paramContext.getSystemService("alarm");
                PendingIntent localPendingIntent = PendingIntent.getBroadcast(paramContext, 5571, new Intent("com.android.server.ACTION_EXPIRED_PASSWORD_NOTIFICATION"), 1207959552);
                localAlarmManager.cancel(localPendingIntent);
                if (l5 != 0L)
                    localAlarmManager.set(1, l5, localPendingIntent);
                return;
                if (l3 <= 0L)
                {
                    l5 = l2 + 86400000L;
                    continue;
                }
                long l4 = l3 % 86400000L;
                if (l4 == 0L)
                    l4 = 86400000L;
                l5 = l2 + l4;
            }
            finally
            {
                Binder.restoreCallingIdentity(l6);
            }
        }
    }

    // ERROR //
    public ComponentName setGlobalProxy(ComponentName paramComponentName, String paramString1, String paramString2)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_1
        //     3: ifnonnull +21 -> 24
        //     6: new 279	java/lang/NullPointerException
        //     9: dup
        //     10: ldc_w 977
        //     13: invokespecial 978	java/lang/NullPointerException:<init>	(Ljava/lang/String;)V
        //     16: athrow
        //     17: astore 9
        //     19: aload_0
        //     20: monitorexit
        //     21: aload 9
        //     23: athrow
        //     24: aload_0
        //     25: aload_1
        //     26: iconst_5
        //     27: invokevirtual 739	com/android/server/DevicePolicyManagerService:getActiveAdminForCallerLocked	(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
        //     30: astore 4
        //     32: aload_0
        //     33: getfield 106	com/android/server/DevicePolicyManagerService:mAdminMap	Ljava/util/HashMap;
        //     36: invokevirtual 1002	java/util/HashMap:keySet	()Ljava/util/Set;
        //     39: invokeinterface 1008 1 0
        //     44: astore 5
        //     46: aload 5
        //     48: invokeinterface 1013 1 0
        //     53: ifeq +47 -> 100
        //     56: aload 5
        //     58: invokeinterface 1016 1 0
        //     63: checkcast 395	android/content/ComponentName
        //     66: astore 8
        //     68: aload_0
        //     69: getfield 106	com/android/server/DevicePolicyManagerService:mAdminMap	Ljava/util/HashMap;
        //     72: aload 8
        //     74: invokevirtual 691	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     77: checkcast 15	com/android/server/DevicePolicyManagerService$ActiveAdmin
        //     80: getfield 475	com/android/server/DevicePolicyManagerService$ActiveAdmin:specifiesGlobalProxy	Z
        //     83: ifeq -37 -> 46
        //     86: aload 8
        //     88: aload_1
        //     89: invokevirtual 960	android/content/ComponentName:equals	(Ljava/lang/Object;)Z
        //     92: ifne -46 -> 46
        //     95: aload_0
        //     96: monitorexit
        //     97: goto +68 -> 165
        //     100: aload_2
        //     101: ifnonnull +43 -> 144
        //     104: aload 4
        //     106: iconst_0
        //     107: putfield 475	com/android/server/DevicePolicyManagerService$ActiveAdmin:specifiesGlobalProxy	Z
        //     110: aload 4
        //     112: aconst_null
        //     113: putfield 478	com/android/server/DevicePolicyManagerService$ActiveAdmin:globalProxySpec	Ljava/lang/String;
        //     116: aload 4
        //     118: aconst_null
        //     119: putfield 481	com/android/server/DevicePolicyManagerService$ActiveAdmin:globalProxyExclusionList	Ljava/lang/String;
        //     122: invokestatic 843	android/os/Binder:clearCallingIdentity	()J
        //     125: lstore 6
        //     127: aload_0
        //     128: invokespecial 168	com/android/server/DevicePolicyManagerService:resetGlobalProxyLocked	()V
        //     131: lload 6
        //     133: invokestatic 864	android/os/Binder:restoreCallingIdentity	(J)V
        //     136: aload_0
        //     137: monitorexit
        //     138: aconst_null
        //     139: astore 8
        //     141: goto +24 -> 165
        //     144: aload 4
        //     146: iconst_1
        //     147: putfield 475	com/android/server/DevicePolicyManagerService$ActiveAdmin:specifiesGlobalProxy	Z
        //     150: aload 4
        //     152: aload_2
        //     153: putfield 478	com/android/server/DevicePolicyManagerService$ActiveAdmin:globalProxySpec	Ljava/lang/String;
        //     156: aload 4
        //     158: aload_3
        //     159: putfield 481	com/android/server/DevicePolicyManagerService$ActiveAdmin:globalProxyExclusionList	Ljava/lang/String;
        //     162: goto -40 -> 122
        //     165: aload 8
        //     167: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     6	21	17	finally
        //     24	162	17	finally
    }

    public void setMaximumFailedPasswordsForWipe(ComponentName paramComponentName, int paramInt)
    {
        try
        {
            getActiveAdminForCallerLocked(paramComponentName, 4);
            ActiveAdmin localActiveAdmin = getActiveAdminForCallerLocked(paramComponentName, 1);
            if (localActiveAdmin.maximumFailedPasswordsForWipe != paramInt)
            {
                localActiveAdmin.maximumFailedPasswordsForWipe = paramInt;
                saveSettingsLocked();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    // ERROR //
    public void setMaximumTimeToLock(ComponentName paramComponentName, long paramLong)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_1
        //     3: ifnonnull +21 -> 24
        //     6: new 279	java/lang/NullPointerException
        //     9: dup
        //     10: ldc_w 977
        //     13: invokespecial 978	java/lang/NullPointerException:<init>	(Ljava/lang/String;)V
        //     16: athrow
        //     17: astore 5
        //     19: aload_0
        //     20: monitorexit
        //     21: aload 5
        //     23: athrow
        //     24: aload_0
        //     25: aload_1
        //     26: iconst_3
        //     27: invokevirtual 739	com/android/server/DevicePolicyManagerService:getActiveAdminForCallerLocked	(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
        //     30: astore 4
        //     32: aload 4
        //     34: getfield 749	com/android/server/DevicePolicyManagerService$ActiveAdmin:maximumTimeToUnlock	J
        //     37: lload_2
        //     38: lcmp
        //     39: ifeq +17 -> 56
        //     42: aload 4
        //     44: lload_2
        //     45: putfield 749	com/android/server/DevicePolicyManagerService$ActiveAdmin:maximumTimeToUnlock	J
        //     48: aload_0
        //     49: invokespecial 164	com/android/server/DevicePolicyManagerService:saveSettingsLocked	()V
        //     52: aload_0
        //     53: invokevirtual 383	com/android/server/DevicePolicyManagerService:updateMaximumTimeToLockLocked	()V
        //     56: aload_0
        //     57: monitorexit
        //     58: return
        //
        // Exception table:
        //     from	to	target	type
        //     6	21	17	finally
        //     24	58	17	finally
    }

    // ERROR //
    public void setPasswordExpirationTimeout(ComponentName paramComponentName, long paramLong)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_1
        //     3: ifnonnull +21 -> 24
        //     6: new 279	java/lang/NullPointerException
        //     9: dup
        //     10: ldc_w 977
        //     13: invokespecial 978	java/lang/NullPointerException:<init>	(Ljava/lang/String;)V
        //     16: athrow
        //     17: astore 7
        //     19: aload_0
        //     20: monitorexit
        //     21: aload 7
        //     23: athrow
        //     24: lload_2
        //     25: lconst_0
        //     26: lcmp
        //     27: ifge +14 -> 41
        //     30: new 597	java/lang/IllegalArgumentException
        //     33: dup
        //     34: ldc_w 1023
        //     37: invokespecial 600	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     40: athrow
        //     41: aload_0
        //     42: aload_1
        //     43: bipush 6
        //     45: invokevirtual 739	com/android/server/DevicePolicyManagerService:getActiveAdminForCallerLocked	(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
        //     48: astore 4
        //     50: lload_2
        //     51: lconst_0
        //     52: lcmp
        //     53: ifle +86 -> 139
        //     56: lload_2
        //     57: invokestatic 238	java/lang/System:currentTimeMillis	()J
        //     60: ladd
        //     61: lstore 5
        //     63: aload 4
        //     65: lload 5
        //     67: putfield 215	com/android/server/DevicePolicyManagerService$ActiveAdmin:passwordExpirationDate	J
        //     70: aload 4
        //     72: lload_2
        //     73: putfield 251	com/android/server/DevicePolicyManagerService$ActiveAdmin:passwordExpirationTimeout	J
        //     76: lload_2
        //     77: lconst_0
        //     78: lcmp
        //     79: ifle +45 -> 124
        //     82: ldc 36
        //     84: new 329	java/lang/StringBuilder
        //     87: dup
        //     88: invokespecial 330	java/lang/StringBuilder:<init>	()V
        //     91: ldc_w 1025
        //     94: invokevirtual 336	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     97: iconst_2
        //     98: iconst_2
        //     99: invokestatic 1031	java/text/DateFormat:getDateTimeInstance	(II)Ljava/text/DateFormat;
        //     102: new 1033	java/util/Date
        //     105: dup
        //     106: lload 5
        //     108: invokespecial 1035	java/util/Date:<init>	(J)V
        //     111: invokevirtual 1039	java/text/DateFormat:format	(Ljava/util/Date;)Ljava/lang/String;
        //     114: invokevirtual 336	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     117: invokevirtual 339	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     120: invokestatic 354	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     123: pop
        //     124: aload_0
        //     125: invokespecial 164	com/android/server/DevicePolicyManagerService:saveSettingsLocked	()V
        //     128: aload_0
        //     129: aload_0
        //     130: getfield 118	com/android/server/DevicePolicyManagerService:mContext	Landroid/content/Context;
        //     133: invokevirtual 260	com/android/server/DevicePolicyManagerService:setExpirationAlarmCheckLocked	(Landroid/content/Context;)V
        //     136: aload_0
        //     137: monitorexit
        //     138: return
        //     139: lconst_0
        //     140: lstore 5
        //     142: goto -79 -> 63
        //
        // Exception table:
        //     from	to	target	type
        //     6	21	17	finally
        //     30	138	17	finally
    }

    // ERROR //
    public void setPasswordHistoryLength(ComponentName paramComponentName, int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_1
        //     3: ifnonnull +21 -> 24
        //     6: new 279	java/lang/NullPointerException
        //     9: dup
        //     10: ldc_w 977
        //     13: invokespecial 978	java/lang/NullPointerException:<init>	(Ljava/lang/String;)V
        //     16: athrow
        //     17: astore 4
        //     19: aload_0
        //     20: monitorexit
        //     21: aload 4
        //     23: athrow
        //     24: aload_0
        //     25: aload_1
        //     26: iconst_0
        //     27: invokevirtual 739	com/android/server/DevicePolicyManagerService:getActiveAdminForCallerLocked	(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
        //     30: astore_3
        //     31: aload_3
        //     32: getfield 757	com/android/server/DevicePolicyManagerService$ActiveAdmin:passwordHistoryLength	I
        //     35: iload_2
        //     36: if_icmpeq +12 -> 48
        //     39: aload_3
        //     40: iload_2
        //     41: putfield 757	com/android/server/DevicePolicyManagerService$ActiveAdmin:passwordHistoryLength	I
        //     44: aload_0
        //     45: invokespecial 164	com/android/server/DevicePolicyManagerService:saveSettingsLocked	()V
        //     48: aload_0
        //     49: monitorexit
        //     50: return
        //
        // Exception table:
        //     from	to	target	type
        //     6	21	17	finally
        //     24	50	17	finally
    }

    // ERROR //
    public void setPasswordMinimumLength(ComponentName paramComponentName, int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_1
        //     3: ifnonnull +21 -> 24
        //     6: new 279	java/lang/NullPointerException
        //     9: dup
        //     10: ldc_w 977
        //     13: invokespecial 978	java/lang/NullPointerException:<init>	(Ljava/lang/String;)V
        //     16: athrow
        //     17: astore 4
        //     19: aload_0
        //     20: monitorexit
        //     21: aload 4
        //     23: athrow
        //     24: aload_0
        //     25: aload_1
        //     26: iconst_0
        //     27: invokevirtual 739	com/android/server/DevicePolicyManagerService:getActiveAdminForCallerLocked	(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
        //     30: astore_3
        //     31: aload_3
        //     32: getfield 761	com/android/server/DevicePolicyManagerService$ActiveAdmin:minimumPasswordLength	I
        //     35: iload_2
        //     36: if_icmpeq +12 -> 48
        //     39: aload_3
        //     40: iload_2
        //     41: putfield 761	com/android/server/DevicePolicyManagerService$ActiveAdmin:minimumPasswordLength	I
        //     44: aload_0
        //     45: invokespecial 164	com/android/server/DevicePolicyManagerService:saveSettingsLocked	()V
        //     48: aload_0
        //     49: monitorexit
        //     50: return
        //
        // Exception table:
        //     from	to	target	type
        //     6	21	17	finally
        //     24	50	17	finally
    }

    // ERROR //
    public void setPasswordMinimumLetters(ComponentName paramComponentName, int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_1
        //     3: ifnonnull +21 -> 24
        //     6: new 279	java/lang/NullPointerException
        //     9: dup
        //     10: ldc_w 977
        //     13: invokespecial 978	java/lang/NullPointerException:<init>	(Ljava/lang/String;)V
        //     16: athrow
        //     17: astore 4
        //     19: aload_0
        //     20: monitorexit
        //     21: aload 4
        //     23: athrow
        //     24: aload_0
        //     25: aload_1
        //     26: iconst_0
        //     27: invokevirtual 739	com/android/server/DevicePolicyManagerService:getActiveAdminForCallerLocked	(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
        //     30: astore_3
        //     31: aload_3
        //     32: getfield 765	com/android/server/DevicePolicyManagerService$ActiveAdmin:minimumPasswordLetters	I
        //     35: iload_2
        //     36: if_icmpeq +12 -> 48
        //     39: aload_3
        //     40: iload_2
        //     41: putfield 765	com/android/server/DevicePolicyManagerService$ActiveAdmin:minimumPasswordLetters	I
        //     44: aload_0
        //     45: invokespecial 164	com/android/server/DevicePolicyManagerService:saveSettingsLocked	()V
        //     48: aload_0
        //     49: monitorexit
        //     50: return
        //
        // Exception table:
        //     from	to	target	type
        //     6	21	17	finally
        //     24	50	17	finally
    }

    // ERROR //
    public void setPasswordMinimumLowerCase(ComponentName paramComponentName, int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_1
        //     3: ifnonnull +21 -> 24
        //     6: new 279	java/lang/NullPointerException
        //     9: dup
        //     10: ldc_w 977
        //     13: invokespecial 978	java/lang/NullPointerException:<init>	(Ljava/lang/String;)V
        //     16: athrow
        //     17: astore 4
        //     19: aload_0
        //     20: monitorexit
        //     21: aload 4
        //     23: athrow
        //     24: aload_0
        //     25: aload_1
        //     26: iconst_0
        //     27: invokevirtual 739	com/android/server/DevicePolicyManagerService:getActiveAdminForCallerLocked	(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
        //     30: astore_3
        //     31: aload_3
        //     32: getfield 769	com/android/server/DevicePolicyManagerService$ActiveAdmin:minimumPasswordLowerCase	I
        //     35: iload_2
        //     36: if_icmpeq +12 -> 48
        //     39: aload_3
        //     40: iload_2
        //     41: putfield 769	com/android/server/DevicePolicyManagerService$ActiveAdmin:minimumPasswordLowerCase	I
        //     44: aload_0
        //     45: invokespecial 164	com/android/server/DevicePolicyManagerService:saveSettingsLocked	()V
        //     48: aload_0
        //     49: monitorexit
        //     50: return
        //
        // Exception table:
        //     from	to	target	type
        //     6	21	17	finally
        //     24	50	17	finally
    }

    // ERROR //
    public void setPasswordMinimumNonLetter(ComponentName paramComponentName, int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_1
        //     3: ifnonnull +21 -> 24
        //     6: new 279	java/lang/NullPointerException
        //     9: dup
        //     10: ldc_w 977
        //     13: invokespecial 978	java/lang/NullPointerException:<init>	(Ljava/lang/String;)V
        //     16: athrow
        //     17: astore 4
        //     19: aload_0
        //     20: monitorexit
        //     21: aload 4
        //     23: athrow
        //     24: aload_0
        //     25: aload_1
        //     26: iconst_0
        //     27: invokevirtual 739	com/android/server/DevicePolicyManagerService:getActiveAdminForCallerLocked	(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
        //     30: astore_3
        //     31: aload_3
        //     32: getfield 773	com/android/server/DevicePolicyManagerService$ActiveAdmin:minimumPasswordNonLetter	I
        //     35: iload_2
        //     36: if_icmpeq +12 -> 48
        //     39: aload_3
        //     40: iload_2
        //     41: putfield 773	com/android/server/DevicePolicyManagerService$ActiveAdmin:minimumPasswordNonLetter	I
        //     44: aload_0
        //     45: invokespecial 164	com/android/server/DevicePolicyManagerService:saveSettingsLocked	()V
        //     48: aload_0
        //     49: monitorexit
        //     50: return
        //
        // Exception table:
        //     from	to	target	type
        //     6	21	17	finally
        //     24	50	17	finally
    }

    // ERROR //
    public void setPasswordMinimumNumeric(ComponentName paramComponentName, int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_1
        //     3: ifnonnull +21 -> 24
        //     6: new 279	java/lang/NullPointerException
        //     9: dup
        //     10: ldc_w 977
        //     13: invokespecial 978	java/lang/NullPointerException:<init>	(Ljava/lang/String;)V
        //     16: athrow
        //     17: astore 4
        //     19: aload_0
        //     20: monitorexit
        //     21: aload 4
        //     23: athrow
        //     24: aload_0
        //     25: aload_1
        //     26: iconst_0
        //     27: invokevirtual 739	com/android/server/DevicePolicyManagerService:getActiveAdminForCallerLocked	(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
        //     30: astore_3
        //     31: aload_3
        //     32: getfield 777	com/android/server/DevicePolicyManagerService$ActiveAdmin:minimumPasswordNumeric	I
        //     35: iload_2
        //     36: if_icmpeq +12 -> 48
        //     39: aload_3
        //     40: iload_2
        //     41: putfield 777	com/android/server/DevicePolicyManagerService$ActiveAdmin:minimumPasswordNumeric	I
        //     44: aload_0
        //     45: invokespecial 164	com/android/server/DevicePolicyManagerService:saveSettingsLocked	()V
        //     48: aload_0
        //     49: monitorexit
        //     50: return
        //
        // Exception table:
        //     from	to	target	type
        //     6	21	17	finally
        //     24	50	17	finally
    }

    // ERROR //
    public void setPasswordMinimumSymbols(ComponentName paramComponentName, int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_1
        //     3: ifnonnull +21 -> 24
        //     6: new 279	java/lang/NullPointerException
        //     9: dup
        //     10: ldc_w 977
        //     13: invokespecial 978	java/lang/NullPointerException:<init>	(Ljava/lang/String;)V
        //     16: athrow
        //     17: astore 4
        //     19: aload_0
        //     20: monitorexit
        //     21: aload 4
        //     23: athrow
        //     24: aload_0
        //     25: aload_1
        //     26: iconst_0
        //     27: invokevirtual 739	com/android/server/DevicePolicyManagerService:getActiveAdminForCallerLocked	(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
        //     30: astore_3
        //     31: aload_3
        //     32: getfield 781	com/android/server/DevicePolicyManagerService$ActiveAdmin:minimumPasswordSymbols	I
        //     35: iload_2
        //     36: if_icmpeq +12 -> 48
        //     39: aload_3
        //     40: iload_2
        //     41: putfield 781	com/android/server/DevicePolicyManagerService$ActiveAdmin:minimumPasswordSymbols	I
        //     44: aload_0
        //     45: invokespecial 164	com/android/server/DevicePolicyManagerService:saveSettingsLocked	()V
        //     48: aload_0
        //     49: monitorexit
        //     50: return
        //
        // Exception table:
        //     from	to	target	type
        //     6	21	17	finally
        //     24	50	17	finally
    }

    // ERROR //
    public void setPasswordMinimumUpperCase(ComponentName paramComponentName, int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_1
        //     3: ifnonnull +21 -> 24
        //     6: new 279	java/lang/NullPointerException
        //     9: dup
        //     10: ldc_w 977
        //     13: invokespecial 978	java/lang/NullPointerException:<init>	(Ljava/lang/String;)V
        //     16: athrow
        //     17: astore 4
        //     19: aload_0
        //     20: monitorexit
        //     21: aload 4
        //     23: athrow
        //     24: aload_0
        //     25: aload_1
        //     26: iconst_0
        //     27: invokevirtual 739	com/android/server/DevicePolicyManagerService:getActiveAdminForCallerLocked	(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
        //     30: astore_3
        //     31: aload_3
        //     32: getfield 785	com/android/server/DevicePolicyManagerService$ActiveAdmin:minimumPasswordUpperCase	I
        //     35: iload_2
        //     36: if_icmpeq +12 -> 48
        //     39: aload_3
        //     40: iload_2
        //     41: putfield 785	com/android/server/DevicePolicyManagerService$ActiveAdmin:minimumPasswordUpperCase	I
        //     44: aload_0
        //     45: invokespecial 164	com/android/server/DevicePolicyManagerService:saveSettingsLocked	()V
        //     48: aload_0
        //     49: monitorexit
        //     50: return
        //
        // Exception table:
        //     from	to	target	type
        //     6	21	17	finally
        //     24	50	17	finally
    }

    // ERROR //
    public void setPasswordQuality(ComponentName paramComponentName, int paramInt)
    {
        // Byte code:
        //     0: iload_2
        //     1: invokestatic 970	com/android/server/DevicePolicyManagerService:validateQualityConstant	(I)V
        //     4: aload_0
        //     5: monitorenter
        //     6: aload_1
        //     7: ifnonnull +21 -> 28
        //     10: new 279	java/lang/NullPointerException
        //     13: dup
        //     14: ldc_w 977
        //     17: invokespecial 978	java/lang/NullPointerException:<init>	(Ljava/lang/String;)V
        //     20: athrow
        //     21: astore 4
        //     23: aload_0
        //     24: monitorexit
        //     25: aload 4
        //     27: athrow
        //     28: aload_0
        //     29: aload_1
        //     30: iconst_0
        //     31: invokevirtual 739	com/android/server/DevicePolicyManagerService:getActiveAdminForCallerLocked	(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
        //     34: astore_3
        //     35: aload_3
        //     36: getfield 789	com/android/server/DevicePolicyManagerService$ActiveAdmin:passwordQuality	I
        //     39: iload_2
        //     40: if_icmpeq +12 -> 52
        //     43: aload_3
        //     44: iload_2
        //     45: putfield 789	com/android/server/DevicePolicyManagerService$ActiveAdmin:passwordQuality	I
        //     48: aload_0
        //     49: invokespecial 164	com/android/server/DevicePolicyManagerService:saveSettingsLocked	()V
        //     52: aload_0
        //     53: monitorexit
        //     54: return
        //
        // Exception table:
        //     from	to	target	type
        //     10	25	21	finally
        //     28	54	21	finally
    }

    // ERROR //
    public int setStorageEncryption(ComponentName paramComponentName, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_1
        //     3: ifnonnull +21 -> 24
        //     6: new 279	java/lang/NullPointerException
        //     9: dup
        //     10: ldc_w 977
        //     13: invokespecial 978	java/lang/NullPointerException:<init>	(Ljava/lang/String;)V
        //     16: athrow
        //     17: astore 8
        //     19: aload_0
        //     20: monitorexit
        //     21: aload 8
        //     23: athrow
        //     24: aload_0
        //     25: aload_1
        //     26: bipush 7
        //     28: invokevirtual 739	com/android/server/DevicePolicyManagerService:getActiveAdminForCallerLocked	(Landroid/content/ComponentName;I)Lcom/android/server/DevicePolicyManagerService$ActiveAdmin;
        //     31: astore_3
        //     32: aload_0
        //     33: invokespecial 1052	com/android/server/DevicePolicyManagerService:isEncryptionSupported	()Z
        //     36: ifne +11 -> 47
        //     39: iconst_0
        //     40: istore 7
        //     42: aload_0
        //     43: monitorexit
        //     44: goto +93 -> 137
        //     47: aload_3
        //     48: getfield 817	com/android/server/DevicePolicyManagerService$ActiveAdmin:encryptionRequested	Z
        //     51: iload_2
        //     52: if_icmpeq +12 -> 64
        //     55: aload_3
        //     56: iload_2
        //     57: putfield 817	com/android/server/DevicePolicyManagerService$ActiveAdmin:encryptionRequested	Z
        //     60: aload_0
        //     61: invokespecial 164	com/android/server/DevicePolicyManagerService:saveSettingsLocked	()V
        //     64: iconst_0
        //     65: istore 4
        //     67: aload_0
        //     68: getfield 111	com/android/server/DevicePolicyManagerService:mAdminList	Ljava/util/ArrayList;
        //     71: invokevirtual 218	java/util/ArrayList:size	()I
        //     74: istore 5
        //     76: iconst_0
        //     77: istore 6
        //     79: iload 6
        //     81: iload 5
        //     83: if_icmpge +29 -> 112
        //     86: iload 4
        //     88: aload_0
        //     89: getfield 111	com/android/server/DevicePolicyManagerService:mAdminList	Ljava/util/ArrayList;
        //     92: iload 6
        //     94: invokevirtual 221	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     97: checkcast 15	com/android/server/DevicePolicyManagerService$ActiveAdmin
        //     100: getfield 817	com/android/server/DevicePolicyManagerService$ActiveAdmin:encryptionRequested	Z
        //     103: ior
        //     104: istore 4
        //     106: iinc 6 1
        //     109: goto -30 -> 79
        //     112: aload_0
        //     113: iload 4
        //     115: invokespecial 1054	com/android/server/DevicePolicyManagerService:setEncryptionRequested	(Z)V
        //     118: iload 4
        //     120: ifeq +11 -> 131
        //     123: iconst_3
        //     124: istore 7
        //     126: aload_0
        //     127: monitorexit
        //     128: goto +9 -> 137
        //     131: iconst_1
        //     132: istore 7
        //     134: goto -8 -> 126
        //     137: iload 7
        //     139: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     6	21	17	finally
        //     24	128	17	finally
    }

    void syncDeviceCapabilitiesLocked()
    {
        boolean bool1 = SystemProperties.getBoolean("sys.secpolicy.camera.disabled", false);
        boolean bool2 = getCameraDisabled(null);
        long l;
        if (bool2 != bool1)
        {
            l = Binder.clearCallingIdentity();
            if (!bool2)
                break label70;
        }
        try
        {
            label70: for (String str = "1"; ; str = "0")
            {
                Slog.v("DevicePolicyManagerService", "Change in camera state [sys.secpolicy.camera.disabled] = " + str);
                SystemProperties.set("sys.secpolicy.camera.disabled", str);
                return;
            }
        }
        finally
        {
            Binder.restoreCallingIdentity(l);
        }
    }

    public void systemReady()
    {
        try
        {
            loadSettingsLocked();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    // ERROR //
    void updateMaximumTimeToLockLocked()
    {
        // Byte code:
        //     0: aload_0
        //     1: aconst_null
        //     2: invokevirtual 1076	com/android/server/DevicePolicyManagerService:getMaximumTimeToLock	(Landroid/content/ComponentName;)J
        //     5: lstore_1
        //     6: aload_0
        //     7: getfield 101	com/android/server/DevicePolicyManagerService:mLastMaximumTimeToLock	J
        //     10: lload_1
        //     11: lcmp
        //     12: ifne +4 -> 16
        //     15: return
        //     16: invokestatic 843	android/os/Binder:clearCallingIdentity	()J
        //     19: lstore_3
        //     20: lload_1
        //     21: lconst_0
        //     22: lcmp
        //     23: ifgt +30 -> 53
        //     26: ldc2_w 1077
        //     29: lstore_1
        //     30: aload_0
        //     31: lload_1
        //     32: putfield 101	com/android/server/DevicePolicyManagerService:mLastMaximumTimeToLock	J
        //     35: aload_0
        //     36: invokespecial 1080	com/android/server/DevicePolicyManagerService:getIPowerManager	()Landroid/os/IPowerManager;
        //     39: lload_1
        //     40: l2i
        //     41: invokeinterface 1083 2 0
        //     46: lload_3
        //     47: invokestatic 864	android/os/Binder:restoreCallingIdentity	(J)V
        //     50: goto -35 -> 15
        //     53: aload_0
        //     54: getfield 118	com/android/server/DevicePolicyManagerService:mContext	Landroid/content/Context;
        //     57: invokevirtual 498	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
        //     60: ldc_w 1085
        //     63: iconst_0
        //     64: invokestatic 1088	android/provider/Settings$System:putInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
        //     67: pop
        //     68: goto -38 -> 30
        //     71: astore 5
        //     73: lload_3
        //     74: invokestatic 864	android/os/Binder:restoreCallingIdentity	(J)V
        //     77: aload 5
        //     79: athrow
        //     80: astore 7
        //     82: ldc 36
        //     84: ldc_w 1090
        //     87: aload 7
        //     89: invokestatic 426	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     92: pop
        //     93: goto -47 -> 46
        //
        // Exception table:
        //     from	to	target	type
        //     30	35	71	finally
        //     35	46	71	finally
        //     53	68	71	finally
        //     82	93	71	finally
        //     35	46	80	android/os/RemoteException
    }

    void validatePasswordOwnerLocked()
    {
        int i;
        if (this.mPasswordOwner >= 0)
            i = 0;
        for (int j = -1 + this.mAdminList.size(); ; j--)
            if (j >= 0)
            {
                if (((ActiveAdmin)this.mAdminList.get(j)).getUid() == this.mPasswordOwner)
                    i = 1;
            }
            else
            {
                if (i == 0)
                {
                    Slog.w("DevicePolicyManagerService", "Previous password owner " + this.mPasswordOwner + " no longer active; disabling");
                    this.mPasswordOwner = -1;
                }
                return;
            }
    }

    public void wipeData(int paramInt)
    {
        try
        {
            getActiveAdminForCallerLocked(null, 4);
            long l = Binder.clearCallingIdentity();
            try
            {
                wipeDataLocked(paramInt);
                Binder.restoreCallingIdentity(l);
                return;
            }
            finally
            {
                localObject2 = finally;
                Binder.restoreCallingIdentity(l);
                throw localObject2;
            }
        }
        finally
        {
        }
    }

    void wipeDataLocked(int paramInt)
    {
        int i = 1;
        int j;
        if ((!Environment.isExternalStorageRemovable()) && (isExtStorageEncrypted()))
        {
            j = i;
            if ((paramInt & 0x1) == 0)
                break label84;
            label23: if (((j == 0) && (i == 0)) || (Environment.isExternalStorageEmulated()))
                break label89;
            Intent localIntent = new Intent("com.android.internal.os.storage.FORMAT_AND_FACTORY_RESET");
            localIntent.setComponent(ExternalStorageFormatter.COMPONENT_NAME);
            this.mWakeLock.acquire(10000L);
            this.mContext.startService(localIntent);
        }
        while (true)
        {
            return;
            j = 0;
            break;
            label84: i = 0;
            break label23;
            try
            {
                label89: RecoverySystem.rebootWipeUserData(this.mContext);
            }
            catch (IOException localIOException)
            {
                Slog.w("DevicePolicyManagerService", "Failed requesting data wipe", localIOException);
            }
        }
    }

    class MyPackageMonitor extends PackageMonitor
    {
        MyPackageMonitor()
        {
        }

        public void onSomePackagesChanged()
        {
            DevicePolicyManagerService localDevicePolicyManagerService = DevicePolicyManagerService.this;
            int i = 0;
            while (true)
            {
                int j;
                try
                {
                    j = -1 + DevicePolicyManagerService.this.mAdminList.size();
                    if (j >= 0)
                    {
                        DevicePolicyManagerService.ActiveAdmin localActiveAdmin = (DevicePolicyManagerService.ActiveAdmin)DevicePolicyManagerService.this.mAdminList.get(j);
                        int k = isPackageDisappearing(localActiveAdmin.info.getPackageName());
                        if ((k == 3) || (k == 2))
                        {
                            Slog.w("DevicePolicyManagerService", "Admin unexpectedly uninstalled: " + localActiveAdmin.info.getComponent());
                            i = 1;
                            DevicePolicyManagerService.this.mAdminList.remove(j);
                            break label252;
                        }
                        boolean bool = isPackageModified(localActiveAdmin.info.getPackageName());
                        if (!bool)
                            break label252;
                        try
                        {
                            DevicePolicyManagerService.this.mContext.getPackageManager().getReceiverInfo(localActiveAdmin.info.getComponent(), 0);
                        }
                        catch (PackageManager.NameNotFoundException localNameNotFoundException)
                        {
                            Slog.w("DevicePolicyManagerService", "Admin package change removed component: " + localActiveAdmin.info.getComponent());
                            i = 1;
                            DevicePolicyManagerService.this.mAdminList.remove(j);
                        }
                    }
                }
                finally
                {
                }
                if (i != 0)
                {
                    DevicePolicyManagerService.this.validatePasswordOwnerLocked();
                    DevicePolicyManagerService.this.syncDeviceCapabilitiesLocked();
                    DevicePolicyManagerService.this.saveSettingsLocked();
                }
                return;
                label252: j--;
            }
        }
    }

    static class ActiveAdmin
    {
        static final int DEF_MAXIMUM_FAILED_PASSWORDS_FOR_WIPE = 0;
        static final long DEF_MAXIMUM_TIME_TO_UNLOCK = 0L;
        static final int DEF_MINIMUM_PASSWORD_LENGTH = 0;
        static final int DEF_MINIMUM_PASSWORD_LETTERS = 1;
        static final int DEF_MINIMUM_PASSWORD_LOWER_CASE = 0;
        static final int DEF_MINIMUM_PASSWORD_NON_LETTER = 0;
        static final int DEF_MINIMUM_PASSWORD_NUMERIC = 1;
        static final int DEF_MINIMUM_PASSWORD_SYMBOLS = 1;
        static final int DEF_MINIMUM_PASSWORD_UPPER_CASE;
        static final long DEF_PASSWORD_EXPIRATION_DATE;
        static final long DEF_PASSWORD_EXPIRATION_TIMEOUT;
        static final int DEF_PASSWORD_HISTORY_LENGTH;
        boolean disableCamera = false;
        boolean encryptionRequested = false;
        String globalProxyExclusionList = null;
        String globalProxySpec = null;
        final DeviceAdminInfo info;
        int maximumFailedPasswordsForWipe = 0;
        long maximumTimeToUnlock = 0L;
        int minimumPasswordLength = 0;
        int minimumPasswordLetters = 1;
        int minimumPasswordLowerCase = 0;
        int minimumPasswordNonLetter = 0;
        int minimumPasswordNumeric = 1;
        int minimumPasswordSymbols = 1;
        int minimumPasswordUpperCase = 0;
        long passwordExpirationDate = 0L;
        long passwordExpirationTimeout = 0L;
        int passwordHistoryLength = 0;
        int passwordQuality = 0;
        boolean specifiesGlobalProxy = false;

        ActiveAdmin(DeviceAdminInfo paramDeviceAdminInfo)
        {
            this.info = paramDeviceAdminInfo;
        }

        void dump(String paramString, PrintWriter paramPrintWriter)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("uid=");
            paramPrintWriter.println(getUid());
            paramPrintWriter.print(paramString);
            paramPrintWriter.println("policies:");
            ArrayList localArrayList = this.info.getUsedPolicies();
            if (localArrayList != null)
                for (int i = 0; i < localArrayList.size(); i++)
                {
                    paramPrintWriter.print(paramString);
                    paramPrintWriter.print("    ");
                    paramPrintWriter.println(((DeviceAdminInfo.PolicyInfo)localArrayList.get(i)).tag);
                }
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("passwordQuality=0x");
            paramPrintWriter.println(Integer.toHexString(this.passwordQuality));
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("minimumPasswordLength=");
            paramPrintWriter.println(this.minimumPasswordLength);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("passwordHistoryLength=");
            paramPrintWriter.println(this.passwordHistoryLength);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("minimumPasswordUpperCase=");
            paramPrintWriter.println(this.minimumPasswordUpperCase);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("minimumPasswordLowerCase=");
            paramPrintWriter.println(this.minimumPasswordLowerCase);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("minimumPasswordLetters=");
            paramPrintWriter.println(this.minimumPasswordLetters);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("minimumPasswordNumeric=");
            paramPrintWriter.println(this.minimumPasswordNumeric);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("minimumPasswordSymbols=");
            paramPrintWriter.println(this.minimumPasswordSymbols);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("minimumPasswordNonLetter=");
            paramPrintWriter.println(this.minimumPasswordNonLetter);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("maximumTimeToUnlock=");
            paramPrintWriter.println(this.maximumTimeToUnlock);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("maximumFailedPasswordsForWipe=");
            paramPrintWriter.println(this.maximumFailedPasswordsForWipe);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("specifiesGlobalProxy=");
            paramPrintWriter.println(this.specifiesGlobalProxy);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("passwordExpirationTimeout=");
            paramPrintWriter.println(this.passwordExpirationTimeout);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("passwordExpirationDate=");
            paramPrintWriter.println(this.passwordExpirationDate);
            if (this.globalProxySpec != null)
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("globalProxySpec=");
                paramPrintWriter.println(this.globalProxySpec);
            }
            if (this.globalProxyExclusionList != null)
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("globalProxyEclusionList=");
                paramPrintWriter.println(this.globalProxyExclusionList);
            }
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("encryptionRequested=");
            paramPrintWriter.println(this.encryptionRequested);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("disableCamera=");
            paramPrintWriter.println(this.disableCamera);
        }

        int getUid()
        {
            return this.info.getActivityInfo().applicationInfo.uid;
        }

        void readFromXml(XmlPullParser paramXmlPullParser)
            throws XmlPullParserException, IOException
        {
            int i = paramXmlPullParser.getDepth();
            int j;
            do
            {
                j = paramXmlPullParser.next();
                if ((j == 1) || ((j == 3) && (paramXmlPullParser.getDepth() <= i)))
                    break;
            }
            while ((j == 3) || (j == 4));
            String str = paramXmlPullParser.getName();
            if ("policies".equals(str))
                this.info.readPoliciesFromXml(paramXmlPullParser);
            while (true)
            {
                XmlUtils.skipCurrentTag(paramXmlPullParser);
                break;
                if ("password-quality".equals(str))
                    this.passwordQuality = Integer.parseInt(paramXmlPullParser.getAttributeValue(null, "value"));
                else if ("min-password-length".equals(str))
                    this.minimumPasswordLength = Integer.parseInt(paramXmlPullParser.getAttributeValue(null, "value"));
                else if ("password-history-length".equals(str))
                    this.passwordHistoryLength = Integer.parseInt(paramXmlPullParser.getAttributeValue(null, "value"));
                else if ("min-password-uppercase".equals(str))
                    this.minimumPasswordUpperCase = Integer.parseInt(paramXmlPullParser.getAttributeValue(null, "value"));
                else if ("min-password-lowercase".equals(str))
                    this.minimumPasswordLowerCase = Integer.parseInt(paramXmlPullParser.getAttributeValue(null, "value"));
                else if ("min-password-letters".equals(str))
                    this.minimumPasswordLetters = Integer.parseInt(paramXmlPullParser.getAttributeValue(null, "value"));
                else if ("min-password-numeric".equals(str))
                    this.minimumPasswordNumeric = Integer.parseInt(paramXmlPullParser.getAttributeValue(null, "value"));
                else if ("min-password-symbols".equals(str))
                    this.minimumPasswordSymbols = Integer.parseInt(paramXmlPullParser.getAttributeValue(null, "value"));
                else if ("min-password-nonletter".equals(str))
                    this.minimumPasswordNonLetter = Integer.parseInt(paramXmlPullParser.getAttributeValue(null, "value"));
                else if ("max-time-to-unlock".equals(str))
                    this.maximumTimeToUnlock = Long.parseLong(paramXmlPullParser.getAttributeValue(null, "value"));
                else if ("max-failed-password-wipe".equals(str))
                    this.maximumFailedPasswordsForWipe = Integer.parseInt(paramXmlPullParser.getAttributeValue(null, "value"));
                else if ("specifies-global-proxy".equals(str))
                    this.specifiesGlobalProxy = Boolean.parseBoolean(paramXmlPullParser.getAttributeValue(null, "value"));
                else if ("global-proxy-spec".equals(str))
                    this.globalProxySpec = paramXmlPullParser.getAttributeValue(null, "value");
                else if ("global-proxy-exclusion-list".equals(str))
                    this.globalProxyExclusionList = paramXmlPullParser.getAttributeValue(null, "value");
                else if ("password-expiration-timeout".equals(str))
                    this.passwordExpirationTimeout = Long.parseLong(paramXmlPullParser.getAttributeValue(null, "value"));
                else if ("password-expiration-date".equals(str))
                    this.passwordExpirationDate = Long.parseLong(paramXmlPullParser.getAttributeValue(null, "value"));
                else if ("encryption-requested".equals(str))
                    this.encryptionRequested = Boolean.parseBoolean(paramXmlPullParser.getAttributeValue(null, "value"));
                else if ("disable-camera".equals(str))
                    this.disableCamera = Boolean.parseBoolean(paramXmlPullParser.getAttributeValue(null, "value"));
                else
                    Slog.w("DevicePolicyManagerService", "Unknown admin tag: " + str);
            }
        }

        void writeToXml(XmlSerializer paramXmlSerializer)
            throws IllegalArgumentException, IllegalStateException, IOException
        {
            paramXmlSerializer.startTag(null, "policies");
            this.info.writePoliciesToXml(paramXmlSerializer);
            paramXmlSerializer.endTag(null, "policies");
            if (this.passwordQuality != 0)
            {
                paramXmlSerializer.startTag(null, "password-quality");
                paramXmlSerializer.attribute(null, "value", Integer.toString(this.passwordQuality));
                paramXmlSerializer.endTag(null, "password-quality");
                if (this.minimumPasswordLength != 0)
                {
                    paramXmlSerializer.startTag(null, "min-password-length");
                    paramXmlSerializer.attribute(null, "value", Integer.toString(this.minimumPasswordLength));
                    paramXmlSerializer.endTag(null, "min-password-length");
                }
                if (this.passwordHistoryLength != 0)
                {
                    paramXmlSerializer.startTag(null, "password-history-length");
                    paramXmlSerializer.attribute(null, "value", Integer.toString(this.passwordHistoryLength));
                    paramXmlSerializer.endTag(null, "password-history-length");
                }
                if (this.minimumPasswordUpperCase != 0)
                {
                    paramXmlSerializer.startTag(null, "min-password-uppercase");
                    paramXmlSerializer.attribute(null, "value", Integer.toString(this.minimumPasswordUpperCase));
                    paramXmlSerializer.endTag(null, "min-password-uppercase");
                }
                if (this.minimumPasswordLowerCase != 0)
                {
                    paramXmlSerializer.startTag(null, "min-password-lowercase");
                    paramXmlSerializer.attribute(null, "value", Integer.toString(this.minimumPasswordLowerCase));
                    paramXmlSerializer.endTag(null, "min-password-lowercase");
                }
                if (this.minimumPasswordLetters != 1)
                {
                    paramXmlSerializer.startTag(null, "min-password-letters");
                    paramXmlSerializer.attribute(null, "value", Integer.toString(this.minimumPasswordLetters));
                    paramXmlSerializer.endTag(null, "min-password-letters");
                }
                if (this.minimumPasswordNumeric != 1)
                {
                    paramXmlSerializer.startTag(null, "min-password-numeric");
                    paramXmlSerializer.attribute(null, "value", Integer.toString(this.minimumPasswordNumeric));
                    paramXmlSerializer.endTag(null, "min-password-numeric");
                }
                if (this.minimumPasswordSymbols != 1)
                {
                    paramXmlSerializer.startTag(null, "min-password-symbols");
                    paramXmlSerializer.attribute(null, "value", Integer.toString(this.minimumPasswordSymbols));
                    paramXmlSerializer.endTag(null, "min-password-symbols");
                }
                if (this.minimumPasswordNonLetter > 0)
                {
                    paramXmlSerializer.startTag(null, "min-password-nonletter");
                    paramXmlSerializer.attribute(null, "value", Integer.toString(this.minimumPasswordNonLetter));
                    paramXmlSerializer.endTag(null, "min-password-nonletter");
                }
            }
            if (this.maximumTimeToUnlock != 0L)
            {
                paramXmlSerializer.startTag(null, "max-time-to-unlock");
                paramXmlSerializer.attribute(null, "value", Long.toString(this.maximumTimeToUnlock));
                paramXmlSerializer.endTag(null, "max-time-to-unlock");
            }
            if (this.maximumFailedPasswordsForWipe != 0)
            {
                paramXmlSerializer.startTag(null, "max-failed-password-wipe");
                paramXmlSerializer.attribute(null, "value", Integer.toString(this.maximumFailedPasswordsForWipe));
                paramXmlSerializer.endTag(null, "max-failed-password-wipe");
            }
            if (this.specifiesGlobalProxy)
            {
                paramXmlSerializer.startTag(null, "specifies-global-proxy");
                paramXmlSerializer.attribute(null, "value", Boolean.toString(this.specifiesGlobalProxy));
                paramXmlSerializer.endTag(null, "specifies_global_proxy");
                if (this.globalProxySpec != null)
                {
                    paramXmlSerializer.startTag(null, "global-proxy-spec");
                    paramXmlSerializer.attribute(null, "value", this.globalProxySpec);
                    paramXmlSerializer.endTag(null, "global-proxy-spec");
                }
                if (this.globalProxyExclusionList != null)
                {
                    paramXmlSerializer.startTag(null, "global-proxy-exclusion-list");
                    paramXmlSerializer.attribute(null, "value", this.globalProxyExclusionList);
                    paramXmlSerializer.endTag(null, "global-proxy-exclusion-list");
                }
            }
            if (this.passwordExpirationTimeout != 0L)
            {
                paramXmlSerializer.startTag(null, "password-expiration-timeout");
                paramXmlSerializer.attribute(null, "value", Long.toString(this.passwordExpirationTimeout));
                paramXmlSerializer.endTag(null, "password-expiration-timeout");
            }
            if (this.passwordExpirationDate != 0L)
            {
                paramXmlSerializer.startTag(null, "password-expiration-date");
                paramXmlSerializer.attribute(null, "value", Long.toString(this.passwordExpirationDate));
                paramXmlSerializer.endTag(null, "password-expiration-date");
            }
            if (this.encryptionRequested)
            {
                paramXmlSerializer.startTag(null, "encryption-requested");
                paramXmlSerializer.attribute(null, "value", Boolean.toString(this.encryptionRequested));
                paramXmlSerializer.endTag(null, "encryption-requested");
            }
            if (this.disableCamera)
            {
                paramXmlSerializer.startTag(null, "disable-camera");
                paramXmlSerializer.attribute(null, "value", Boolean.toString(this.disableCamera));
                paramXmlSerializer.endTag(null, "disable-camera");
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.DevicePolicyManagerService
 * JD-Core Version:        0.6.2
 */