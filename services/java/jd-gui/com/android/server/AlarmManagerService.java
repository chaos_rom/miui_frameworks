package com.android.server;

import android.app.ActivityManagerNative;
import android.app.IActivityManager;
import android.app.IAlarmManager.Stub;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.app.PendingIntent.OnFinished;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.Intent.FilterComparison;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.WorkSource;
import android.text.TextUtils;
import android.util.Slog;
import android.util.TimeUtils;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TimeZone;

class AlarmManagerService extends IAlarmManager.Stub
{
    private static final int ALARM_EVENT = 1;
    private static final String ClockReceiver_TAG = "ClockReceiver";
    private static final int ELAPSED_REALTIME_MASK = 8;
    private static final int ELAPSED_REALTIME_WAKEUP_MASK = 4;
    private static final long LATE_ALARM_THRESHOLD = 10000L;
    private static final long QUANTUM = 900000L;
    private static final int RTC_MASK = 2;
    private static final int RTC_WAKEUP_MASK = 1;
    private static final String TAG = "AlarmManager";
    private static final String TIMEZONE_PROPERTY = "persist.sys.timezone";
    private static final int TIME_CHANGED_MASK = 65536;
    private static final boolean localLOGV;
    private static final Intent mBackgroundIntent = new Intent().addFlags(4);
    private int mBroadcastRefCount = 0;
    private final HashMap<String, BroadcastStats> mBroadcastStats = new HashMap();
    private ClockReceiver mClockReceiver;
    private final Context mContext;
    private final PendingIntent mDateChangeSender;
    private int mDescriptor;
    private final ArrayList<Alarm> mElapsedRealtimeAlarms = new ArrayList();
    private final ArrayList<Alarm> mElapsedRealtimeWakeupAlarms = new ArrayList();
    private final AlarmHandler mHandler = new AlarmHandler();
    private LinkedList<PendingIntent> mInFlight = new LinkedList();
    private final IncreasingTimeOrder mIncreasingTimeOrder = new IncreasingTimeOrder();
    private Object mLock = new Object();
    private final ResultReceiver mResultReceiver = new ResultReceiver();
    private final ArrayList<Alarm> mRtcAlarms = new ArrayList();
    private final ArrayList<Alarm> mRtcWakeupAlarms = new ArrayList();
    private final PendingIntent mTimeTickSender;
    private UninstallReceiver mUninstallReceiver;
    private final AlarmThread mWaitThread = new AlarmThread();
    private PowerManager.WakeLock mWakeLock;

    public AlarmManagerService(Context paramContext)
    {
        this.mContext = paramContext;
        this.mDescriptor = init();
        String str = SystemProperties.get("persist.sys.timezone");
        if (str != null)
            setTimeZone(str);
        this.mWakeLock = ((PowerManager)paramContext.getSystemService("power")).newWakeLock(1, "AlarmManager");
        this.mTimeTickSender = PendingIntent.getBroadcast(paramContext, 0, new Intent("android.intent.action.TIME_TICK").addFlags(1073741824), 0);
        Intent localIntent = new Intent("android.intent.action.DATE_CHANGED");
        localIntent.addFlags(536870912);
        this.mDateChangeSender = PendingIntent.getBroadcast(paramContext, 0, localIntent, 0);
        this.mClockReceiver = new ClockReceiver();
        this.mClockReceiver.scheduleTimeTickEvent();
        this.mClockReceiver.scheduleDateChangedEvent();
        this.mUninstallReceiver = new UninstallReceiver();
        if (this.mDescriptor != -1)
            this.mWaitThread.start();
        while (true)
        {
            return;
            Slog.w("AlarmManager", "Failed to open alarm driver. Falling back to a handler.");
        }
    }

    private int addAlarmLocked(Alarm paramAlarm)
    {
        ArrayList localArrayList = getAlarmList(paramAlarm.type);
        int i = Collections.binarySearch(localArrayList, paramAlarm, this.mIncreasingTimeOrder);
        if (i < 0)
            i = -1 + (0 - i);
        localArrayList.add(i, paramAlarm);
        return i;
    }

    private native void close(int paramInt);

    private static final void dumpAlarmList(PrintWriter paramPrintWriter, ArrayList<Alarm> paramArrayList, String paramString1, String paramString2, long paramLong)
    {
        for (int i = -1 + paramArrayList.size(); i >= 0; i--)
        {
            Alarm localAlarm = (Alarm)paramArrayList.get(i);
            paramPrintWriter.print(paramString1);
            paramPrintWriter.print(paramString2);
            paramPrintWriter.print(" #");
            paramPrintWriter.print(i);
            paramPrintWriter.print(": ");
            paramPrintWriter.println(localAlarm);
            localAlarm.dump(paramPrintWriter, paramString1 + "    ", paramLong);
        }
    }

    private ArrayList<Alarm> getAlarmList(int paramInt)
    {
        Object localObject;
        switch (paramInt)
        {
        default:
            localObject = null;
        case 0:
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            return localObject;
            localObject = this.mRtcWakeupAlarms;
            continue;
            localObject = this.mRtcAlarms;
            continue;
            localObject = this.mElapsedRealtimeWakeupAlarms;
            continue;
            localObject = this.mElapsedRealtimeAlarms;
        }
    }

    private final BroadcastStats getStatsLocked(PendingIntent paramPendingIntent)
    {
        String str = paramPendingIntent.getTargetPackage();
        BroadcastStats localBroadcastStats = (BroadcastStats)this.mBroadcastStats.get(str);
        if (localBroadcastStats == null)
        {
            localBroadcastStats = new BroadcastStats(null);
            this.mBroadcastStats.put(str, localBroadcastStats);
        }
        return localBroadcastStats;
    }

    private native int init();

    private boolean lookForPackageLocked(ArrayList<Alarm> paramArrayList, String paramString)
    {
        int i = -1 + paramArrayList.size();
        if (i >= 0)
            if (!((Alarm)paramArrayList.get(i)).operation.getTargetPackage().equals(paramString));
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            i--;
            break;
        }
    }

    private void removeLocked(ArrayList<Alarm> paramArrayList, PendingIntent paramPendingIntent)
    {
        if (paramArrayList.size() <= 0);
        while (true)
        {
            return;
            Iterator localIterator = paramArrayList.iterator();
            while (localIterator.hasNext())
                if (((Alarm)localIterator.next()).operation.equals(paramPendingIntent))
                    localIterator.remove();
        }
    }

    private void removeLocked(ArrayList<Alarm> paramArrayList, String paramString)
    {
        if (paramArrayList.size() <= 0);
        while (true)
        {
            return;
            Iterator localIterator = paramArrayList.iterator();
            while (localIterator.hasNext())
                if (((Alarm)localIterator.next()).operation.getTargetPackage().equals(paramString))
                    localIterator.remove();
        }
    }

    private native void set(int paramInt1, int paramInt2, long paramLong1, long paramLong2);

    private native int setKernelTimezone(int paramInt1, int paramInt2);

    private void setLocked(Alarm paramAlarm)
    {
        long l1;
        long l2;
        if (this.mDescriptor != -1)
            if (paramAlarm.when < 0L)
            {
                l1 = 0L;
                l2 = 0L;
                set(this.mDescriptor, paramAlarm.type, l1, l2);
            }
        while (true)
        {
            return;
            l1 = paramAlarm.when / 1000L;
            l2 = 1000L * (1000L * (paramAlarm.when % 1000L));
            break;
            Message localMessage = Message.obtain();
            localMessage.what = 1;
            this.mHandler.removeMessages(1);
            this.mHandler.sendMessageAtTime(localMessage, paramAlarm.when);
        }
    }

    private void triggerAlarmsLocked(ArrayList<Alarm> paramArrayList1, ArrayList<Alarm> paramArrayList2, long paramLong)
    {
        Iterator localIterator1 = paramArrayList1.iterator();
        ArrayList localArrayList = new ArrayList();
        while (true)
        {
            Alarm localAlarm2;
            if (localIterator1.hasNext())
            {
                localAlarm2 = (Alarm)localIterator1.next();
                if (localAlarm2.when <= paramLong);
            }
            else
            {
                Iterator localIterator2 = localArrayList.iterator();
                while (localIterator2.hasNext())
                {
                    Alarm localAlarm1 = (Alarm)localIterator2.next();
                    localAlarm1.when += localAlarm1.count * localAlarm1.repeatInterval;
                    addAlarmLocked(localAlarm1);
                }
            }
            localAlarm2.count = 1;
            if (localAlarm2.repeatInterval > 0L)
                localAlarm2.count = ((int)(localAlarm2.count + (paramLong - localAlarm2.when) / localAlarm2.repeatInterval));
            paramArrayList2.add(localAlarm2);
            localIterator1.remove();
            if (localAlarm2.repeatInterval > 0L)
                localArrayList.add(localAlarm2);
        }
        if (paramArrayList1.size() > 0)
            setLocked((Alarm)paramArrayList1.get(0));
    }

    private native int waitForAlarm(int paramInt);

    // ERROR //
    protected void dump(java.io.FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 159	com/android/server/AlarmManagerService:mContext	Landroid/content/Context;
        //     4: ldc_w 435
        //     7: invokevirtual 439	android/content/Context:checkCallingOrSelfPermission	(Ljava/lang/String;)I
        //     10: ifeq +42 -> 52
        //     13: aload_2
        //     14: new 333	java/lang/StringBuilder
        //     17: dup
        //     18: invokespecial 334	java/lang/StringBuilder:<init>	()V
        //     21: ldc_w 441
        //     24: invokevirtual 338	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     27: invokestatic 446	android/os/Binder:getCallingPid	()I
        //     30: invokevirtual 449	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     33: ldc_w 451
        //     36: invokevirtual 338	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     39: invokestatic 454	android/os/Binder:getCallingUid	()I
        //     42: invokevirtual 449	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     45: invokevirtual 344	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     48: invokevirtual 456	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     51: return
        //     52: aload_0
        //     53: getfield 120	com/android/server/AlarmManagerService:mLock	Ljava/lang/Object;
        //     56: astore 4
        //     58: aload 4
        //     60: monitorenter
        //     61: aload_2
        //     62: ldc_w 458
        //     65: invokevirtual 456	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     68: aload_0
        //     69: getfield 125	com/android/server/AlarmManagerService:mRtcWakeupAlarms	Ljava/util/ArrayList;
        //     72: invokevirtual 313	java/util/ArrayList:size	()I
        //     75: ifgt +13 -> 88
        //     78: aload_0
        //     79: getfield 127	com/android/server/AlarmManagerService:mRtcAlarms	Ljava/util/ArrayList;
        //     82: invokevirtual 313	java/util/ArrayList:size	()I
        //     85: ifle +111 -> 196
        //     88: invokestatic 464	java/lang/System:currentTimeMillis	()J
        //     91: lstore 6
        //     93: new 466	java/text/SimpleDateFormat
        //     96: dup
        //     97: ldc_w 468
        //     100: invokespecial 469	java/text/SimpleDateFormat:<init>	(Ljava/lang/String;)V
        //     103: astore 8
        //     105: aload_2
        //     106: ldc_w 471
        //     109: invokevirtual 456	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     112: aload_2
        //     113: ldc_w 473
        //     116: invokevirtual 321	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     119: aload_2
        //     120: aload 8
        //     122: new 475	java/util/Date
        //     125: dup
        //     126: lload 6
        //     128: invokespecial 478	java/util/Date:<init>	(J)V
        //     131: invokevirtual 482	java/text/SimpleDateFormat:format	(Ljava/util/Date;)Ljava/lang/String;
        //     134: invokevirtual 321	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     137: aload_2
        //     138: ldc_w 484
        //     141: invokevirtual 456	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     144: aload_0
        //     145: getfield 125	com/android/server/AlarmManagerService:mRtcWakeupAlarms	Ljava/util/ArrayList;
        //     148: invokevirtual 313	java/util/ArrayList:size	()I
        //     151: ifle +19 -> 170
        //     154: aload_2
        //     155: aload_0
        //     156: getfield 125	com/android/server/AlarmManagerService:mRtcWakeupAlarms	Ljava/util/ArrayList;
        //     159: ldc_w 340
        //     162: ldc_w 486
        //     165: lload 6
        //     167: invokestatic 488	com/android/server/AlarmManagerService:dumpAlarmList	(Ljava/io/PrintWriter;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;J)V
        //     170: aload_0
        //     171: getfield 127	com/android/server/AlarmManagerService:mRtcAlarms	Ljava/util/ArrayList;
        //     174: invokevirtual 313	java/util/ArrayList:size	()I
        //     177: ifle +19 -> 196
        //     180: aload_2
        //     181: aload_0
        //     182: getfield 127	com/android/server/AlarmManagerService:mRtcAlarms	Ljava/util/ArrayList;
        //     185: ldc_w 340
        //     188: ldc_w 490
        //     191: lload 6
        //     193: invokestatic 488	com/android/server/AlarmManagerService:dumpAlarmList	(Ljava/io/PrintWriter;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;J)V
        //     196: aload_0
        //     197: getfield 129	com/android/server/AlarmManagerService:mElapsedRealtimeWakeupAlarms	Ljava/util/ArrayList;
        //     200: invokevirtual 313	java/util/ArrayList:size	()I
        //     203: ifgt +13 -> 216
        //     206: aload_0
        //     207: getfield 131	com/android/server/AlarmManagerService:mElapsedRealtimeAlarms	Ljava/util/ArrayList;
        //     210: invokevirtual 313	java/util/ArrayList:size	()I
        //     213: ifle +87 -> 300
        //     216: invokestatic 495	android/os/SystemClock:elapsedRealtime	()J
        //     219: lstore 9
        //     221: aload_2
        //     222: ldc_w 471
        //     225: invokevirtual 456	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     228: aload_2
        //     229: ldc_w 497
        //     232: invokevirtual 321	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     235: lload 9
        //     237: aload_2
        //     238: invokestatic 503	android/util/TimeUtils:formatDuration	(JLjava/io/PrintWriter;)V
        //     241: aload_2
        //     242: ldc_w 484
        //     245: invokevirtual 456	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     248: aload_0
        //     249: getfield 129	com/android/server/AlarmManagerService:mElapsedRealtimeWakeupAlarms	Ljava/util/ArrayList;
        //     252: invokevirtual 313	java/util/ArrayList:size	()I
        //     255: ifle +19 -> 274
        //     258: aload_2
        //     259: aload_0
        //     260: getfield 129	com/android/server/AlarmManagerService:mElapsedRealtimeWakeupAlarms	Ljava/util/ArrayList;
        //     263: ldc_w 340
        //     266: ldc_w 505
        //     269: lload 9
        //     271: invokestatic 488	com/android/server/AlarmManagerService:dumpAlarmList	(Ljava/io/PrintWriter;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;J)V
        //     274: aload_0
        //     275: getfield 131	com/android/server/AlarmManagerService:mElapsedRealtimeAlarms	Ljava/util/ArrayList;
        //     278: invokevirtual 313	java/util/ArrayList:size	()I
        //     281: ifle +19 -> 300
        //     284: aload_2
        //     285: aload_0
        //     286: getfield 131	com/android/server/AlarmManagerService:mElapsedRealtimeAlarms	Ljava/util/ArrayList;
        //     289: ldc_w 340
        //     292: ldc_w 507
        //     295: lload 9
        //     297: invokestatic 488	com/android/server/AlarmManagerService:dumpAlarmList	(Ljava/io/PrintWriter;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;J)V
        //     300: aload_2
        //     301: ldc_w 471
        //     304: invokevirtual 456	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     307: aload_2
        //     308: ldc_w 509
        //     311: invokevirtual 321	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     314: aload_2
        //     315: aload_0
        //     316: getfield 136	com/android/server/AlarmManagerService:mBroadcastRefCount	I
        //     319: invokevirtual 511	java/io/PrintWriter:println	(I)V
        //     322: aload_2
        //     323: ldc_w 471
        //     326: invokevirtual 456	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     329: aload_2
        //     330: ldc_w 513
        //     333: invokevirtual 456	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     336: aload_0
        //     337: getfield 157	com/android/server/AlarmManagerService:mBroadcastStats	Ljava/util/HashMap;
        //     340: invokevirtual 517	java/util/HashMap:entrySet	()Ljava/util/Set;
        //     343: invokeinterface 520 1 0
        //     348: astore 11
        //     350: aload 11
        //     352: invokeinterface 384 1 0
        //     357: ifeq +190 -> 547
        //     360: aload 11
        //     362: invokeinterface 388 1 0
        //     367: checkcast 522	java/util/Map$Entry
        //     370: astore 12
        //     372: aload 12
        //     374: invokeinterface 525 1 0
        //     379: checkcast 29	com/android/server/AlarmManagerService$BroadcastStats
        //     382: astore 13
        //     384: aload_2
        //     385: ldc_w 340
        //     388: invokevirtual 321	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     391: aload_2
        //     392: aload 12
        //     394: invokeinterface 528 1 0
        //     399: checkcast 368	java/lang/String
        //     402: invokevirtual 456	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     405: aload_2
        //     406: ldc_w 530
        //     409: invokevirtual 321	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     412: aload_2
        //     413: aload 13
        //     415: getfield 533	com/android/server/AlarmManagerService$BroadcastStats:aggregateTime	J
        //     418: invokevirtual 535	java/io/PrintWriter:print	(J)V
        //     421: aload_2
        //     422: ldc_w 537
        //     425: invokevirtual 321	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     428: aload_2
        //     429: aload 13
        //     431: getfield 540	com/android/server/AlarmManagerService$BroadcastStats:numWakeup	I
        //     434: invokevirtual 325	java/io/PrintWriter:print	(I)V
        //     437: aload_2
        //     438: ldc_w 542
        //     441: invokevirtual 456	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     444: aload 13
        //     446: getfield 545	com/android/server/AlarmManagerService$BroadcastStats:filterStats	Ljava/util/HashMap;
        //     449: invokevirtual 517	java/util/HashMap:entrySet	()Ljava/util/Set;
        //     452: invokeinterface 520 1 0
        //     457: astore 14
        //     459: aload 14
        //     461: invokeinterface 384 1 0
        //     466: ifeq -116 -> 350
        //     469: aload 14
        //     471: invokeinterface 388 1 0
        //     476: checkcast 522	java/util/Map$Entry
        //     479: astore 15
        //     481: aload_2
        //     482: ldc_w 530
        //     485: invokevirtual 321	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     488: aload_2
        //     489: aload 15
        //     491: invokeinterface 525 1 0
        //     496: checkcast 32	com/android/server/AlarmManagerService$FilterStats
        //     499: getfield 546	com/android/server/AlarmManagerService$FilterStats:count	I
        //     502: invokevirtual 325	java/io/PrintWriter:print	(I)V
        //     505: aload_2
        //     506: ldc_w 548
        //     509: invokevirtual 321	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     512: aload_2
        //     513: aload 15
        //     515: invokeinterface 528 1 0
        //     520: checkcast 550	android/content/Intent$FilterComparison
        //     523: invokevirtual 553	android/content/Intent$FilterComparison:getIntent	()Landroid/content/Intent;
        //     526: iconst_0
        //     527: iconst_1
        //     528: iconst_0
        //     529: iconst_1
        //     530: invokevirtual 557	android/content/Intent:toShortString	(ZZZZ)Ljava/lang/String;
        //     533: invokevirtual 456	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     536: goto -77 -> 459
        //     539: astore 5
        //     541: aload 4
        //     543: monitorexit
        //     544: aload 5
        //     546: athrow
        //     547: aload 4
        //     549: monitorexit
        //     550: goto -499 -> 51
        //
        // Exception table:
        //     from	to	target	type
        //     61	544	539	finally
        //     547	550	539	finally
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            close(this.mDescriptor);
            return;
        }
        finally
        {
            super.finalize();
        }
    }

    public boolean lookForPackageLocked(String paramString)
    {
        if ((lookForPackageLocked(this.mRtcWakeupAlarms, paramString)) || (lookForPackageLocked(this.mRtcAlarms, paramString)) || (lookForPackageLocked(this.mElapsedRealtimeWakeupAlarms, paramString)) || (lookForPackageLocked(this.mElapsedRealtimeAlarms, paramString)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void remove(PendingIntent paramPendingIntent)
    {
        if (paramPendingIntent == null);
        while (true)
        {
            return;
            synchronized (this.mLock)
            {
                removeLocked(paramPendingIntent);
            }
        }
    }

    public void removeLocked(PendingIntent paramPendingIntent)
    {
        removeLocked(this.mRtcWakeupAlarms, paramPendingIntent);
        removeLocked(this.mRtcAlarms, paramPendingIntent);
        removeLocked(this.mElapsedRealtimeWakeupAlarms, paramPendingIntent);
        removeLocked(this.mElapsedRealtimeAlarms, paramPendingIntent);
    }

    public void removeLocked(String paramString)
    {
        removeLocked(this.mRtcWakeupAlarms, paramString);
        removeLocked(this.mRtcAlarms, paramString);
        removeLocked(this.mElapsedRealtimeWakeupAlarms, paramString);
        removeLocked(this.mElapsedRealtimeAlarms, paramString);
    }

    public void set(int paramInt, long paramLong, PendingIntent paramPendingIntent)
    {
        setRepeating(paramInt, paramLong, 0L, paramPendingIntent);
    }

    public void setInexactRepeating(int paramInt, long paramLong1, long paramLong2, PendingIntent paramPendingIntent)
    {
        if (paramPendingIntent == null)
            Slog.w("AlarmManager", "setInexactRepeating ignored because there is no intent");
        while (true)
        {
            return;
            if (paramLong2 <= 0L)
            {
                Slog.w("AlarmManager", "setInexactRepeating ignored because interval " + paramLong2 + " is invalid");
            }
            else
            {
                if (paramLong2 % 900000L == 0L)
                    break;
                setRepeating(paramInt, paramLong1, paramLong2, paramPendingIntent);
            }
        }
        int i;
        label94: long l1;
        label108: long l2;
        if ((paramInt == 1) || (paramInt == 0))
        {
            i = 1;
            if (i == 0)
                break label155;
            l1 = System.currentTimeMillis() - SystemClock.elapsedRealtime();
            l2 = (paramLong1 - l1) % 900000L;
            if (l2 == 0L)
                break label161;
        }
        label155: label161: for (long l3 = 900000L + (paramLong1 - l2); ; l3 = paramLong1)
        {
            setRepeating(paramInt, l3, paramLong2, paramPendingIntent);
            break;
            i = 0;
            break label94;
            l1 = 0L;
            break label108;
        }
    }

    public void setRepeating(int paramInt, long paramLong1, long paramLong2, PendingIntent paramPendingIntent)
    {
        if (paramPendingIntent == null)
            Slog.w("AlarmManager", "set/setRepeating ignored because there is no intent");
        while (true)
        {
            return;
            synchronized (this.mLock)
            {
                Alarm localAlarm = new Alarm();
                localAlarm.type = paramInt;
                localAlarm.when = paramLong1;
                localAlarm.repeatInterval = paramLong2;
                localAlarm.operation = paramPendingIntent;
                removeLocked(paramPendingIntent);
                if (addAlarmLocked(localAlarm) == 0)
                    setLocked(localAlarm);
            }
        }
    }

    public void setTime(long paramLong)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.SET_TIME", "setTime");
        SystemClock.setCurrentTimeMillis(paramLong);
    }

    public void setTimeZone(String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.SET_TIME_ZONE", "setTimeZone");
        if (TextUtils.isEmpty(paramString));
        while (true)
        {
            return;
            TimeZone localTimeZone = TimeZone.getTimeZone(paramString);
            int i = 0;
            try
            {
                String str = SystemProperties.get("persist.sys.timezone");
                if ((str == null) || (!str.equals(localTimeZone.getID())))
                {
                    i = 1;
                    SystemProperties.set("persist.sys.timezone", localTimeZone.getID());
                }
                int j = localTimeZone.getOffset(System.currentTimeMillis());
                setKernelTimezone(this.mDescriptor, -(j / 60000));
                TimeZone.setDefault(null);
                if (i == 0)
                    continue;
                Intent localIntent = new Intent("android.intent.action.TIMEZONE_CHANGED");
                localIntent.addFlags(536870912);
                localIntent.putExtra("time-zone", localTimeZone.getID());
                this.mContext.sendBroadcast(localIntent);
            }
            finally
            {
            }
        }
    }

    void setWakelockWorkSource(PendingIntent paramPendingIntent)
    {
        try
        {
            int i = ActivityManagerNative.getDefault().getUidForIntentSender(paramPendingIntent.getTarget());
            if (i >= 0)
            {
                this.mWakeLock.setWorkSource(new WorkSource(i));
                return;
            }
        }
        catch (Exception localException)
        {
            while (true)
                this.mWakeLock.setWorkSource(null);
        }
    }

    public long timeToNextAlarm()
    {
        long l = 9223372036854775807L;
        Object localObject1 = this.mLock;
        for (int i = 0; ; i++)
        {
            if (i <= 3);
            try
            {
                ArrayList localArrayList = getAlarmList(i);
                if (localArrayList.size() > 0)
                {
                    Alarm localAlarm = (Alarm)localArrayList.get(0);
                    if (localAlarm.when < l)
                    {
                        l = localAlarm.when;
                        continue;
                        return l;
                    }
                }
            }
            finally
            {
                localObject2 = finally;
                throw localObject2;
            }
        }
    }

    class ResultReceiver
        implements PendingIntent.OnFinished
    {
        ResultReceiver()
        {
        }

        // ERROR //
        public void onSendFinished(PendingIntent paramPendingIntent, Intent paramIntent, int paramInt, String paramString, android.os.Bundle paramBundle)
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 15	com/android/server/AlarmManagerService$ResultReceiver:this$0	Lcom/android/server/AlarmManagerService;
            //     4: invokestatic 24	com/android/server/AlarmManagerService:access$500	(Lcom/android/server/AlarmManagerService;)Ljava/lang/Object;
            //     7: astore 6
            //     9: aload 6
            //     11: monitorenter
            //     12: aload_0
            //     13: getfield 15	com/android/server/AlarmManagerService$ResultReceiver:this$0	Lcom/android/server/AlarmManagerService;
            //     16: aload_1
            //     17: invokestatic 28	com/android/server/AlarmManagerService:access$1700	(Lcom/android/server/AlarmManagerService;Landroid/app/PendingIntent;)Lcom/android/server/AlarmManagerService$BroadcastStats;
            //     20: astore 8
            //     22: aload 8
            //     24: ifnull +115 -> 139
            //     27: aload 8
            //     29: bipush 255
            //     31: aload 8
            //     33: getfield 34	com/android/server/AlarmManagerService$BroadcastStats:nesting	I
            //     36: iadd
            //     37: putfield 34	com/android/server/AlarmManagerService$BroadcastStats:nesting	I
            //     40: aload 8
            //     42: getfield 34	com/android/server/AlarmManagerService$BroadcastStats:nesting	I
            //     45: ifgt +94 -> 139
            //     48: aload 8
            //     50: iconst_0
            //     51: putfield 34	com/android/server/AlarmManagerService$BroadcastStats:nesting	I
            //     54: aload 8
            //     56: aload 8
            //     58: getfield 38	com/android/server/AlarmManagerService$BroadcastStats:aggregateTime	J
            //     61: invokestatic 44	android/os/SystemClock:elapsedRealtime	()J
            //     64: aload 8
            //     66: getfield 47	com/android/server/AlarmManagerService$BroadcastStats:startTime	J
            //     69: lsub
            //     70: ladd
            //     71: putfield 38	com/android/server/AlarmManagerService$BroadcastStats:aggregateTime	J
            //     74: new 49	android/content/Intent$FilterComparison
            //     77: dup
            //     78: aload_2
            //     79: invokespecial 52	android/content/Intent$FilterComparison:<init>	(Landroid/content/Intent;)V
            //     82: astore 13
            //     84: aload 8
            //     86: getfield 56	com/android/server/AlarmManagerService$BroadcastStats:filterStats	Ljava/util/HashMap;
            //     89: aload 13
            //     91: invokevirtual 62	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
            //     94: checkcast 64	com/android/server/AlarmManagerService$FilterStats
            //     97: astore 14
            //     99: aload 14
            //     101: ifnonnull +26 -> 127
            //     104: new 64	com/android/server/AlarmManagerService$FilterStats
            //     107: dup
            //     108: aconst_null
            //     109: invokespecial 67	com/android/server/AlarmManagerService$FilterStats:<init>	(Lcom/android/server/AlarmManagerService$1;)V
            //     112: astore 14
            //     114: aload 8
            //     116: getfield 56	com/android/server/AlarmManagerService$BroadcastStats:filterStats	Ljava/util/HashMap;
            //     119: aload 13
            //     121: aload 14
            //     123: invokevirtual 71	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
            //     126: pop
            //     127: aload 14
            //     129: iconst_1
            //     130: aload 14
            //     132: getfield 74	com/android/server/AlarmManagerService$FilterStats:count	I
            //     135: iadd
            //     136: putfield 74	com/android/server/AlarmManagerService$FilterStats:count	I
            //     139: aload_0
            //     140: getfield 15	com/android/server/AlarmManagerService$ResultReceiver:this$0	Lcom/android/server/AlarmManagerService;
            //     143: invokestatic 78	com/android/server/AlarmManagerService:access$1600	(Lcom/android/server/AlarmManagerService;)Ljava/util/LinkedList;
            //     146: invokevirtual 84	java/util/LinkedList:removeFirst	()Ljava/lang/Object;
            //     149: pop
            //     150: aload_0
            //     151: getfield 15	com/android/server/AlarmManagerService$ResultReceiver:this$0	Lcom/android/server/AlarmManagerService;
            //     154: invokestatic 88	com/android/server/AlarmManagerService:access$1410	(Lcom/android/server/AlarmManagerService;)I
            //     157: pop
            //     158: aload_0
            //     159: getfield 15	com/android/server/AlarmManagerService$ResultReceiver:this$0	Lcom/android/server/AlarmManagerService;
            //     162: invokestatic 91	com/android/server/AlarmManagerService:access$1400	(Lcom/android/server/AlarmManagerService;)I
            //     165: ifne +17 -> 182
            //     168: aload_0
            //     169: getfield 15	com/android/server/AlarmManagerService$ResultReceiver:this$0	Lcom/android/server/AlarmManagerService;
            //     172: invokestatic 95	com/android/server/AlarmManagerService:access$1500	(Lcom/android/server/AlarmManagerService;)Landroid/os/PowerManager$WakeLock;
            //     175: invokevirtual 100	android/os/PowerManager$WakeLock:release	()V
            //     178: aload 6
            //     180: monitorexit
            //     181: return
            //     182: aload_0
            //     183: getfield 15	com/android/server/AlarmManagerService$ResultReceiver:this$0	Lcom/android/server/AlarmManagerService;
            //     186: invokestatic 78	com/android/server/AlarmManagerService:access$1600	(Lcom/android/server/AlarmManagerService;)Ljava/util/LinkedList;
            //     189: invokevirtual 103	java/util/LinkedList:peekFirst	()Ljava/lang/Object;
            //     192: checkcast 105	android/app/PendingIntent
            //     195: astore 11
            //     197: aload 11
            //     199: ifnull +23 -> 222
            //     202: aload_0
            //     203: getfield 15	com/android/server/AlarmManagerService$ResultReceiver:this$0	Lcom/android/server/AlarmManagerService;
            //     206: aload 11
            //     208: invokevirtual 109	com/android/server/AlarmManagerService:setWakelockWorkSource	(Landroid/app/PendingIntent;)V
            //     211: goto -33 -> 178
            //     214: astore 7
            //     216: aload 6
            //     218: monitorexit
            //     219: aload 7
            //     221: athrow
            //     222: ldc 111
            //     224: ldc 113
            //     226: invokestatic 119	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     229: pop
            //     230: aload_0
            //     231: getfield 15	com/android/server/AlarmManagerService$ResultReceiver:this$0	Lcom/android/server/AlarmManagerService;
            //     234: invokestatic 95	com/android/server/AlarmManagerService:access$1500	(Lcom/android/server/AlarmManagerService;)Landroid/os/PowerManager$WakeLock;
            //     237: aconst_null
            //     238: invokevirtual 123	android/os/PowerManager$WakeLock:setWorkSource	(Landroid/os/WorkSource;)V
            //     241: goto -63 -> 178
            //
            // Exception table:
            //     from	to	target	type
            //     12	219	214	finally
            //     222	241	214	finally
        }
    }

    class UninstallReceiver extends BroadcastReceiver
    {
        public UninstallReceiver()
        {
            IntentFilter localIntentFilter1 = new IntentFilter();
            localIntentFilter1.addAction("android.intent.action.PACKAGE_REMOVED");
            localIntentFilter1.addAction("android.intent.action.PACKAGE_RESTARTED");
            localIntentFilter1.addAction("android.intent.action.QUERY_PACKAGE_RESTART");
            localIntentFilter1.addDataScheme("package");
            AlarmManagerService.this.mContext.registerReceiver(this, localIntentFilter1);
            IntentFilter localIntentFilter2 = new IntentFilter();
            localIntentFilter2.addAction("android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE");
            AlarmManagerService.this.mContext.registerReceiver(this, localIntentFilter2);
        }

        public void onReceive(Context paramContext, Intent paramIntent)
        {
            while (true)
            {
                synchronized (AlarmManagerService.this.mLock)
                {
                    String str1 = paramIntent.getAction();
                    String[] arrayOfString1 = null;
                    int m;
                    String[] arrayOfString2;
                    int i;
                    int j;
                    String str3;
                    Uri localUri;
                    String str2;
                    if ("android.intent.action.QUERY_PACKAGE_RESTART".equals(str1))
                    {
                        String[] arrayOfString3 = paramIntent.getStringArrayExtra("android.intent.extra.PACKAGES");
                        int k = arrayOfString3.length;
                        m = 0;
                        if (m < k)
                        {
                            String str4 = arrayOfString3[m];
                            if (!AlarmManagerService.this.lookForPackageLocked(str4))
                                break label243;
                            setResultCode(-1);
                            continue;
                        }
                    }
                }
                label243: m++;
            }
        }
    }

    class ClockReceiver extends BroadcastReceiver
    {
        public ClockReceiver()
        {
            IntentFilter localIntentFilter = new IntentFilter();
            localIntentFilter.addAction("android.intent.action.TIME_TICK");
            localIntentFilter.addAction("android.intent.action.DATE_CHANGED");
            AlarmManagerService.this.mContext.registerReceiver(this, localIntentFilter);
        }

        public void onReceive(Context paramContext, Intent paramIntent)
        {
            if (paramIntent.getAction().equals("android.intent.action.TIME_TICK"))
                scheduleTimeTickEvent();
            while (true)
            {
                return;
                if (paramIntent.getAction().equals("android.intent.action.DATE_CHANGED"))
                {
                    int i = TimeZone.getTimeZone(SystemProperties.get("persist.sys.timezone")).getOffset(System.currentTimeMillis());
                    AlarmManagerService.this.setKernelTimezone(AlarmManagerService.this.mDescriptor, -(i / 60000));
                    scheduleDateChangedEvent();
                }
            }
        }

        public void scheduleDateChangedEvent()
        {
            Calendar localCalendar = Calendar.getInstance();
            localCalendar.setTimeInMillis(System.currentTimeMillis());
            localCalendar.set(10, 0);
            localCalendar.set(12, 0);
            localCalendar.set(13, 0);
            localCalendar.set(14, 0);
            localCalendar.add(5, 1);
            AlarmManagerService.this.set(1, localCalendar.getTimeInMillis(), AlarmManagerService.this.mDateChangeSender);
        }

        public void scheduleTimeTickEvent()
        {
            Calendar localCalendar = Calendar.getInstance();
            long l1 = System.currentTimeMillis();
            localCalendar.setTimeInMillis(l1);
            localCalendar.add(12, 1);
            localCalendar.set(13, 0);
            localCalendar.set(14, 0);
            long l2 = localCalendar.getTimeInMillis() - l1;
            AlarmManagerService.this.set(3, l2 + SystemClock.elapsedRealtime(), AlarmManagerService.this.mTimeTickSender);
        }
    }

    private class AlarmHandler extends Handler
    {
        public static final int ALARM_EVENT = 1;
        public static final int DATE_CHANGE_EVENT = 3;
        public static final int MINUTE_CHANGE_EVENT = 2;

        public AlarmHandler()
        {
        }

        public void handleMessage(Message paramMessage)
        {
            if (paramMessage.what == 1)
            {
                ArrayList localArrayList = new ArrayList();
                synchronized (AlarmManagerService.this.mLock)
                {
                    long l = System.currentTimeMillis();
                    AlarmManagerService.this.triggerAlarmsLocked(AlarmManagerService.this.mRtcWakeupAlarms, localArrayList, l);
                    AlarmManagerService.this.triggerAlarmsLocked(AlarmManagerService.this.mRtcAlarms, localArrayList, l);
                    AlarmManagerService.this.triggerAlarmsLocked(AlarmManagerService.this.mElapsedRealtimeWakeupAlarms, localArrayList, l);
                    AlarmManagerService.this.triggerAlarmsLocked(AlarmManagerService.this.mElapsedRealtimeAlarms, localArrayList, l);
                    Iterator localIterator = localArrayList.iterator();
                    while (localIterator.hasNext())
                    {
                        AlarmManagerService.Alarm localAlarm = (AlarmManagerService.Alarm)localIterator.next();
                        try
                        {
                            localAlarm.operation.send();
                        }
                        catch (PendingIntent.CanceledException localCanceledException)
                        {
                        }
                        if (localAlarm.repeatInterval > 0L)
                            AlarmManagerService.this.remove(localAlarm.operation);
                    }
                }
            }
        }
    }

    private class AlarmThread extends Thread
    {
        public AlarmThread()
        {
            super();
        }

        public void run()
        {
            while (true)
            {
                int i = AlarmManagerService.this.waitForAlarm(AlarmManagerService.this.mDescriptor);
                ArrayList localArrayList = new ArrayList();
                if ((0x10000 & i) != 0)
                {
                    AlarmManagerService.this.remove(AlarmManagerService.this.mTimeTickSender);
                    AlarmManagerService.this.mClockReceiver.scheduleTimeTickEvent();
                    Intent localIntent = new Intent("android.intent.action.TIME_SET");
                    localIntent.addFlags(671088640);
                    AlarmManagerService.this.mContext.sendBroadcast(localIntent);
                }
                while (true)
                {
                    long l2;
                    AlarmManagerService.Alarm localAlarm;
                    synchronized (AlarmManagerService.this.mLock)
                    {
                        long l1 = System.currentTimeMillis();
                        l2 = SystemClock.elapsedRealtime();
                        if ((i & 0x1) != 0)
                            AlarmManagerService.this.triggerAlarmsLocked(AlarmManagerService.this.mRtcWakeupAlarms, localArrayList, l1);
                        if ((i & 0x2) != 0)
                            AlarmManagerService.this.triggerAlarmsLocked(AlarmManagerService.this.mRtcAlarms, localArrayList, l1);
                        if ((i & 0x4) != 0)
                            AlarmManagerService.this.triggerAlarmsLocked(AlarmManagerService.this.mElapsedRealtimeWakeupAlarms, localArrayList, l2);
                        if ((i & 0x8) != 0)
                            AlarmManagerService.this.triggerAlarmsLocked(AlarmManagerService.this.mElapsedRealtimeAlarms, localArrayList, l2);
                        Iterator localIterator = localArrayList.iterator();
                        if (!localIterator.hasNext())
                            break;
                        localAlarm = (AlarmManagerService.Alarm)localIterator.next();
                    }
                    try
                    {
                        localAlarm.operation.send(AlarmManagerService.this.mContext, 0, AlarmManagerService.mBackgroundIntent.putExtra("android.intent.extra.ALARM_COUNT", localAlarm.count), AlarmManagerService.this.mResultReceiver, AlarmManagerService.this.mHandler);
                        if (AlarmManagerService.this.mBroadcastRefCount == 0)
                        {
                            AlarmManagerService.this.setWakelockWorkSource(localAlarm.operation);
                            AlarmManagerService.this.mWakeLock.acquire();
                        }
                        AlarmManagerService.this.mInFlight.add(localAlarm.operation);
                        AlarmManagerService.access$1408(AlarmManagerService.this);
                        localBroadcastStats = AlarmManagerService.this.getStatsLocked(localAlarm.operation);
                        if (localBroadcastStats.nesting == 0)
                        {
                            localBroadcastStats.startTime = l2;
                            if ((localAlarm.type != 2) && (localAlarm.type != 0))
                                continue;
                            localBroadcastStats.numWakeup = (1 + localBroadcastStats.numWakeup);
                            ActivityManagerNative.noteWakeupAlarm(localAlarm.operation);
                        }
                    }
                    catch (PendingIntent.CanceledException localCanceledException)
                    {
                        AlarmManagerService.BroadcastStats localBroadcastStats;
                        while (localAlarm.repeatInterval > 0L)
                        {
                            AlarmManagerService.this.remove(localAlarm.operation);
                            break;
                            localObject2 = finally;
                            throw localObject2;
                            localBroadcastStats.nesting = (1 + localBroadcastStats.nesting);
                        }
                    }
                    catch (RuntimeException localRuntimeException)
                    {
                        Slog.w("AlarmManager", "Failure sending alarm.", localRuntimeException);
                    }
                }
            }
        }
    }

    private static class Alarm
    {
        public int count;
        public PendingIntent operation = null;
        public long repeatInterval = 0L;
        public int type;
        public long when = 0L;

        public void dump(PrintWriter paramPrintWriter, String paramString, long paramLong)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("type=");
            paramPrintWriter.print(this.type);
            paramPrintWriter.print(" when=");
            TimeUtils.formatDuration(this.when, paramLong, paramPrintWriter);
            paramPrintWriter.print(" repeatInterval=");
            paramPrintWriter.print(this.repeatInterval);
            paramPrintWriter.print(" count=");
            paramPrintWriter.println(this.count);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("operation=");
            paramPrintWriter.println(this.operation);
        }

        public String toString()
        {
            StringBuilder localStringBuilder = new StringBuilder(128);
            localStringBuilder.append("Alarm{");
            localStringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
            localStringBuilder.append(" type ");
            localStringBuilder.append(this.type);
            localStringBuilder.append(" ");
            localStringBuilder.append(this.operation.getTargetPackage());
            localStringBuilder.append('}');
            return localStringBuilder.toString();
        }
    }

    public static class IncreasingTimeOrder
        implements Comparator<AlarmManagerService.Alarm>
    {
        public int compare(AlarmManagerService.Alarm paramAlarm1, AlarmManagerService.Alarm paramAlarm2)
        {
            long l1 = paramAlarm1.when;
            long l2 = paramAlarm2.when;
            int i;
            if (l1 - l2 > 0L)
                i = 1;
            while (true)
            {
                return i;
                if (l1 - l2 < 0L)
                    i = -1;
                else
                    i = 0;
            }
        }
    }

    private static final class BroadcastStats
    {
        long aggregateTime;
        HashMap<Intent.FilterComparison, AlarmManagerService.FilterStats> filterStats = new HashMap();
        int nesting;
        int numWakeup;
        long startTime;
    }

    private static final class FilterStats
    {
        int count;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.AlarmManagerService
 * JD-Core Version:        0.6.2
 */