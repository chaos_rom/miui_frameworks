package com.android.server;

import android.app.backup.BackupAgentHelper;
import android.app.backup.BackupDataInput;
import android.app.backup.BackupDataOutput;
import android.app.backup.FullBackup;
import android.app.backup.FullBackupDataOutput;
import android.app.backup.WallpaperBackupHelper;
import android.os.ParcelFileDescriptor;
import android.os.ServiceManager;
import android.util.Slog;
import java.io.File;
import java.io.IOException;

public class SystemBackupAgent extends BackupAgentHelper
{
    private static final String TAG = "SystemBackupAgent";
    private static final String WALLPAPER_IMAGE = "/data/system/users/0/wallpaper";
    private static final String WALLPAPER_IMAGE_DIR = "/data/system/users/0";
    private static final String WALLPAPER_IMAGE_FILENAME = "wallpaper";
    private static final String WALLPAPER_IMAGE_KEY = "/data/data/com.android.settings/files/wallpaper";
    private static final String WALLPAPER_INFO = "/data/system/users/0/wallpaper_info.xml";
    private static final String WALLPAPER_INFO_DIR = "/data/system/users/0";
    private static final String WALLPAPER_INFO_FILENAME = "wallpaper_info.xml";
    private static final String WALLPAPER_INFO_KEY = "/data/system/wallpaper_info.xml";

    private void fullWallpaperBackup(FullBackupDataOutput paramFullBackupDataOutput)
    {
        FullBackup.backupToTar(getPackageName(), "r", null, "/data/system/users/0", "/data/system/users/0/wallpaper_info.xml", paramFullBackupDataOutput.getData());
        FullBackup.backupToTar(getPackageName(), "r", null, "/data/system/users/0", "/data/system/users/0/wallpaper", paramFullBackupDataOutput.getData());
    }

    public void onBackup(ParcelFileDescriptor paramParcelFileDescriptor1, BackupDataOutput paramBackupDataOutput, ParcelFileDescriptor paramParcelFileDescriptor2)
        throws IOException
    {
        WallpaperManagerService localWallpaperManagerService = (WallpaperManagerService)ServiceManager.getService("wallpaper");
        String[] arrayOfString1 = new String[2];
        arrayOfString1[0] = "/data/system/users/0/wallpaper";
        arrayOfString1[1] = "/data/system/users/0/wallpaper_info.xml";
        String[] arrayOfString2 = new String[2];
        arrayOfString2[0] = "/data/data/com.android.settings/files/wallpaper";
        arrayOfString2[1] = "/data/system/wallpaper_info.xml";
        if ((localWallpaperManagerService != null) && (localWallpaperManagerService.getName() != null) && (localWallpaperManagerService.getName().length() > 0))
        {
            arrayOfString1 = new String[1];
            arrayOfString1[0] = "/data/system/users/0/wallpaper_info.xml";
            arrayOfString2 = new String[1];
            arrayOfString2[0] = "/data/system/wallpaper_info.xml";
        }
        addHelper("wallpaper", new WallpaperBackupHelper(this, arrayOfString1, arrayOfString2));
        super.onBackup(paramParcelFileDescriptor1, paramBackupDataOutput, paramParcelFileDescriptor2);
    }

    public void onFullBackup(FullBackupDataOutput paramFullBackupDataOutput)
        throws IOException
    {
        fullWallpaperBackup(paramFullBackupDataOutput);
    }

    public void onRestore(BackupDataInput paramBackupDataInput, int paramInt, ParcelFileDescriptor paramParcelFileDescriptor)
        throws IOException
    {
        String[] arrayOfString1 = new String[2];
        arrayOfString1[0] = "/data/system/users/0/wallpaper";
        arrayOfString1[1] = "/data/system/users/0/wallpaper_info.xml";
        String[] arrayOfString2 = new String[2];
        arrayOfString2[0] = "/data/data/com.android.settings/files/wallpaper";
        arrayOfString2[1] = "/data/system/wallpaper_info.xml";
        addHelper("wallpaper", new WallpaperBackupHelper(this, arrayOfString1, arrayOfString2));
        String[] arrayOfString3 = new String[1];
        arrayOfString3[0] = "/data/system/users/0/wallpaper";
        String[] arrayOfString4 = new String[1];
        arrayOfString4[0] = "/data/data/com.android.settings/files/wallpaper";
        addHelper("system_files", new WallpaperBackupHelper(this, arrayOfString3, arrayOfString4));
        try
        {
            super.onRestore(paramBackupDataInput, paramInt, paramParcelFileDescriptor);
            ((WallpaperManagerService)ServiceManager.getService("wallpaper")).settingsRestored();
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
            {
                Slog.d("SystemBackupAgent", "restore failed", localIOException);
                new File("/data/system/users/0/wallpaper").delete();
                new File("/data/system/users/0/wallpaper_info.xml").delete();
            }
        }
    }

    public void onRestoreFile(ParcelFileDescriptor paramParcelFileDescriptor, long paramLong1, int paramInt, String paramString1, String paramString2, long paramLong2, long paramLong3)
        throws IOException
    {
        Slog.i("SystemBackupAgent", "Restoring file domain=" + paramString1 + " path=" + paramString2);
        int i = 0;
        File localFile = null;
        if (paramString1.equals("r"))
        {
            if (!paramString2.equals("wallpaper_info.xml"))
                break label152;
            localFile = new File("/data/system/users/0/wallpaper_info.xml");
            i = 1;
        }
        while (true)
        {
            if (localFile == null);
            try
            {
                Slog.w("SystemBackupAgent", "Skipping unrecognized system file: [ " + paramString1 + " : " + paramString2 + " ]");
                FullBackup.restoreFile(paramParcelFileDescriptor, paramLong1, paramInt, paramLong2, paramLong3, localFile);
                if (i != 0)
                    ((WallpaperManagerService)ServiceManager.getService("wallpaper")).settingsRestored();
                return;
                label152: if (!paramString2.equals("wallpaper"))
                    continue;
                localFile = new File("/data/system/users/0/wallpaper");
                i = 1;
            }
            catch (IOException localIOException)
            {
                while (true)
                    if (i != 0)
                    {
                        new File("/data/system/users/0/wallpaper").delete();
                        new File("/data/system/users/0/wallpaper_info.xml").delete();
                    }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.SystemBackupAgent
 * JD-Core Version:        0.6.2
 */