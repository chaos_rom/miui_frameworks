package com.android.server;

import android.os.Binder;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Slog;
import java.io.File;
import java.io.IOException;

public class EntropyMixer extends Binder
{
    private static final int ENTROPY_WHAT = 1;
    private static final int ENTROPY_WRITE_PERIOD = 10800000;
    private static final long START_NANOTIME = 0L;
    private static final long START_TIME = 0L;
    private static final String TAG = "EntropyMixer";
    private final String entropyFile;
    private final Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            if (paramAnonymousMessage.what != 1)
                Slog.e("EntropyMixer", "Will not process invalid message");
            while (true)
            {
                return;
                EntropyMixer.this.writeEntropy();
                EntropyMixer.this.scheduleEntropyWriter();
            }
        }
    };
    private final String randomDevice;

    public EntropyMixer()
    {
        this(getSystemDir() + "/entropy.dat", "/dev/urandom");
    }

    public EntropyMixer(String paramString1, String paramString2)
    {
        if (paramString2 == null)
            throw new NullPointerException("randomDevice");
        if (paramString1 == null)
            throw new NullPointerException("entropyFile");
        this.randomDevice = paramString2;
        this.entropyFile = paramString1;
        loadInitialEntropy();
        addDeviceSpecificEntropy();
        writeEntropy();
        scheduleEntropyWriter();
    }

    // ERROR //
    private void addDeviceSpecificEntropy()
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_1
        //     2: new 97	java/io/PrintWriter
        //     5: dup
        //     6: new 99	java/io/FileOutputStream
        //     9: dup
        //     10: aload_0
        //     11: getfield 77	com/android/server/EntropyMixer:randomDevice	Ljava/lang/String;
        //     14: invokespecial 100	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
        //     17: invokespecial 103	java/io/PrintWriter:<init>	(Ljava/io/OutputStream;)V
        //     20: astore_2
        //     21: aload_2
        //     22: ldc 105
        //     24: invokevirtual 108	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     27: aload_2
        //     28: ldc 110
        //     30: invokevirtual 108	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     33: aload_2
        //     34: getstatic 34	com/android/server/EntropyMixer:START_TIME	J
        //     37: invokevirtual 113	java/io/PrintWriter:println	(J)V
        //     40: aload_2
        //     41: getstatic 39	com/android/server/EntropyMixer:START_NANOTIME	J
        //     44: invokevirtual 113	java/io/PrintWriter:println	(J)V
        //     47: aload_2
        //     48: ldc 115
        //     50: invokestatic 121	android/os/SystemProperties:get	(Ljava/lang/String;)Ljava/lang/String;
        //     53: invokevirtual 108	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     56: aload_2
        //     57: ldc 123
        //     59: invokestatic 121	android/os/SystemProperties:get	(Ljava/lang/String;)Ljava/lang/String;
        //     62: invokevirtual 108	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     65: aload_2
        //     66: ldc 125
        //     68: invokestatic 121	android/os/SystemProperties:get	(Ljava/lang/String;)Ljava/lang/String;
        //     71: invokevirtual 108	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     74: aload_2
        //     75: ldc 127
        //     77: invokestatic 121	android/os/SystemProperties:get	(Ljava/lang/String;)Ljava/lang/String;
        //     80: invokevirtual 108	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     83: aload_2
        //     84: ldc 129
        //     86: invokestatic 121	android/os/SystemProperties:get	(Ljava/lang/String;)Ljava/lang/String;
        //     89: invokevirtual 108	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     92: aload_2
        //     93: ldc 131
        //     95: invokestatic 121	android/os/SystemProperties:get	(Ljava/lang/String;)Ljava/lang/String;
        //     98: invokevirtual 108	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     101: aload_2
        //     102: ldc 133
        //     104: invokestatic 121	android/os/SystemProperties:get	(Ljava/lang/String;)Ljava/lang/String;
        //     107: invokevirtual 108	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     110: aload_2
        //     111: new 135	java/lang/Object
        //     114: dup
        //     115: invokespecial 136	java/lang/Object:<init>	()V
        //     118: invokevirtual 140	java/lang/Object:hashCode	()I
        //     121: invokevirtual 143	java/io/PrintWriter:println	(I)V
        //     124: aload_2
        //     125: invokestatic 32	java/lang/System:currentTimeMillis	()J
        //     128: invokevirtual 113	java/io/PrintWriter:println	(J)V
        //     131: aload_2
        //     132: invokestatic 37	java/lang/System:nanoTime	()J
        //     135: invokevirtual 113	java/io/PrintWriter:println	(J)V
        //     138: aload_2
        //     139: ifnull +7 -> 146
        //     142: aload_2
        //     143: invokevirtual 146	java/io/PrintWriter:close	()V
        //     146: return
        //     147: astore_3
        //     148: ldc 20
        //     150: ldc 148
        //     152: aload_3
        //     153: invokestatic 154	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     156: pop
        //     157: aload_1
        //     158: ifnull -12 -> 146
        //     161: aload_1
        //     162: invokevirtual 146	java/io/PrintWriter:close	()V
        //     165: goto -19 -> 146
        //     168: astore 4
        //     170: aload_1
        //     171: ifnull +7 -> 178
        //     174: aload_1
        //     175: invokevirtual 146	java/io/PrintWriter:close	()V
        //     178: aload 4
        //     180: athrow
        //     181: astore 4
        //     183: aload_2
        //     184: astore_1
        //     185: goto -15 -> 170
        //     188: astore_3
        //     189: aload_2
        //     190: astore_1
        //     191: goto -43 -> 148
        //
        // Exception table:
        //     from	to	target	type
        //     2	21	147	java/io/IOException
        //     2	21	168	finally
        //     148	157	168	finally
        //     21	138	181	finally
        //     21	138	188	java/io/IOException
    }

    private static String getSystemDir()
    {
        File localFile = new File(Environment.getDataDirectory(), "system");
        localFile.mkdirs();
        return localFile.toString();
    }

    private void loadInitialEntropy()
    {
        try
        {
            RandomBlock.fromFile(this.entropyFile).toFile(this.randomDevice, false);
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
                Slog.w("EntropyMixer", "unable to load initial entropy (first boot?)", localIOException);
        }
    }

    private void scheduleEntropyWriter()
    {
        this.mHandler.removeMessages(1);
        this.mHandler.sendEmptyMessageDelayed(1, 10800000L);
    }

    private void writeEntropy()
    {
        try
        {
            RandomBlock.fromFile(this.randomDevice).toFile(this.entropyFile, true);
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
                Slog.w("EntropyMixer", "unable to write entropy", localIOException);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.EntropyMixer
 * JD-Core Version:        0.6.2
 */