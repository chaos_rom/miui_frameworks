package com.android.server;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.util.SparseArray;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.WeakHashMap;

public final class AttributeCache
{
    private static AttributeCache sInstance = null;
    private final Configuration mConfiguration = new Configuration();
    private final Context mContext;

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private final WeakHashMap<String, WeakReference<Package>> mPackages = new WeakHashMap();

    public AttributeCache(Context paramContext)
    {
        this.mContext = paramContext;
    }

    public static void init(Context paramContext)
    {
        if (sInstance == null)
            sInstance = new AttributeCache(paramContext);
    }

    public static AttributeCache instance()
    {
        return sInstance;
    }

    // ERROR //
    public Entry get(String paramString, int paramInt, int[] paramArrayOfInt)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore 4
        //     3: aload_0
        //     4: monitorenter
        //     5: aload_0
        //     6: getfield 39	com/android/server/AttributeCache:mPackages	Ljava/util/WeakHashMap;
        //     9: aload_1
        //     10: invokestatic 61	com/android/server/AttributeCache$Injector:getPackage	(Ljava/util/WeakHashMap;Ljava/lang/String;)Lcom/android/server/AttributeCache$Package;
        //     13: astore 6
        //     15: aconst_null
        //     16: astore 7
        //     18: aconst_null
        //     19: astore 8
        //     21: aload 6
        //     23: ifnull +47 -> 70
        //     26: aload 6
        //     28: invokestatic 65	com/android/server/AttributeCache$Package:access$000	(Lcom/android/server/AttributeCache$Package;)Landroid/util/SparseArray;
        //     31: iload_2
        //     32: invokevirtual 70	android/util/SparseArray:get	(I)Ljava/lang/Object;
        //     35: checkcast 72	java/util/HashMap
        //     38: astore 7
        //     40: aload 7
        //     42: ifnull +84 -> 126
        //     45: aload 7
        //     47: aload_3
        //     48: invokevirtual 75	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     51: checkcast 6	com/android/server/AttributeCache$Entry
        //     54: astore 8
        //     56: aload 8
        //     58: ifnull +68 -> 126
        //     61: aload_0
        //     62: monitorexit
        //     63: aload 8
        //     65: astore 4
        //     67: aload 4
        //     69: areturn
        //     70: aload_0
        //     71: getfield 46	com/android/server/AttributeCache:mContext	Landroid/content/Context;
        //     74: aload_1
        //     75: iconst_0
        //     76: invokevirtual 81	android/content/Context:createPackageContext	(Ljava/lang/String;I)Landroid/content/Context;
        //     79: astore 10
        //     81: aload 10
        //     83: ifnonnull +22 -> 105
        //     86: aload_0
        //     87: monitorexit
        //     88: goto -21 -> 67
        //     91: astore 5
        //     93: aload_0
        //     94: monitorexit
        //     95: aload 5
        //     97: athrow
        //     98: astore 9
        //     100: aload_0
        //     101: monitorexit
        //     102: goto -35 -> 67
        //     105: new 9	com/android/server/AttributeCache$Package
        //     108: dup
        //     109: aload 10
        //     111: invokespecial 82	com/android/server/AttributeCache$Package:<init>	(Landroid/content/Context;)V
        //     114: astore 6
        //     116: aload_0
        //     117: getfield 39	com/android/server/AttributeCache:mPackages	Ljava/util/WeakHashMap;
        //     120: aload_1
        //     121: aload 6
        //     123: invokestatic 86	com/android/server/AttributeCache$Injector:putPackage	(Ljava/util/WeakHashMap;Ljava/lang/String;Lcom/android/server/AttributeCache$Package;)V
        //     126: aload 8
        //     128: pop
        //     129: aload 7
        //     131: ifnonnull +23 -> 154
        //     134: new 72	java/util/HashMap
        //     137: dup
        //     138: invokespecial 87	java/util/HashMap:<init>	()V
        //     141: astore 7
        //     143: aload 6
        //     145: invokestatic 65	com/android/server/AttributeCache$Package:access$000	(Lcom/android/server/AttributeCache$Package;)Landroid/util/SparseArray;
        //     148: iload_2
        //     149: aload 7
        //     151: invokevirtual 91	android/util/SparseArray:put	(ILjava/lang/Object;)V
        //     154: new 6	com/android/server/AttributeCache$Entry
        //     157: dup
        //     158: aload 6
        //     160: getfield 94	com/android/server/AttributeCache$Package:context	Landroid/content/Context;
        //     163: aload 6
        //     165: getfield 94	com/android/server/AttributeCache$Package:context	Landroid/content/Context;
        //     168: iload_2
        //     169: aload_3
        //     170: invokevirtual 98	android/content/Context:obtainStyledAttributes	(I[I)Landroid/content/res/TypedArray;
        //     173: invokespecial 101	com/android/server/AttributeCache$Entry:<init>	(Landroid/content/Context;Landroid/content/res/TypedArray;)V
        //     176: astore 12
        //     178: aload 7
        //     180: aload_3
        //     181: aload 12
        //     183: invokevirtual 104	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     186: pop
        //     187: aload_0
        //     188: monitorexit
        //     189: aload 12
        //     191: astore 4
        //     193: goto -126 -> 67
        //     196: aload_0
        //     197: monitorexit
        //     198: goto -131 -> 67
        //     201: astore 13
        //     203: goto -7 -> 196
        //     206: astore 15
        //     208: goto -12 -> 196
        //
        // Exception table:
        //     from	to	target	type
        //     5	63	91	finally
        //     70	81	91	finally
        //     86	95	91	finally
        //     100	154	91	finally
        //     154	178	91	finally
        //     178	187	91	finally
        //     187	198	91	finally
        //     70	81	98	android/content/pm/PackageManager$NameNotFoundException
        //     178	187	201	android/content/res/Resources$NotFoundException
        //     154	178	206	android/content/res/Resources$NotFoundException
    }

    public void removePackage(String paramString)
    {
        try
        {
            this.mPackages.remove(paramString);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void updateConfiguration(Configuration paramConfiguration)
    {
        try
        {
            if ((0xBFFFFF5F & this.mConfiguration.updateFrom(paramConfiguration)) != 0)
                this.mPackages.clear();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public static final class Entry
    {
        public final TypedArray array;
        public final Context context;

        public Entry(Context paramContext, TypedArray paramTypedArray)
        {
            this.context = paramContext;
            this.array = paramTypedArray;
        }
    }

    public static final class Package
    {
        public final Context context;
        private final SparseArray<HashMap<int[], AttributeCache.Entry>> mMap = new SparseArray();

        public Package(Context paramContext)
        {
            this.context = paramContext;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static AttributeCache.Package getPackage(WeakHashMap<String, WeakReference<AttributeCache.Package>> paramWeakHashMap, String paramString)
        {
            WeakReference localWeakReference = (WeakReference)paramWeakHashMap.get(paramString);
            if (localWeakReference != null);
            for (AttributeCache.Package localPackage = (AttributeCache.Package)localWeakReference.get(); ; localPackage = null)
                return localPackage;
        }

        static void putPackage(WeakHashMap<String, WeakReference<AttributeCache.Package>> paramWeakHashMap, String paramString, AttributeCache.Package paramPackage)
        {
            paramWeakHashMap.put(paramString, new WeakReference(paramPackage));
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.AttributeCache
 * JD-Core Version:        0.6.2
 */