package com.android.server;

import android.os.SystemClock;
import android.util.Slog;
import com.android.internal.os.SamplingProfilerIntegration;
import dalvik.system.VMRuntime;
import java.util.Timer;
import java.util.TimerTask;

public class SystemServer
{
    private static final long EARLIEST_SUPPORTED_TIME = 86400000L;
    public static final int FACTORY_TEST_HIGH_LEVEL = 2;
    public static final int FACTORY_TEST_LOW_LEVEL = 1;
    public static final int FACTORY_TEST_OFF = 0;
    static final long SNAPSHOT_INTERVAL = 3600000L;
    private static final String TAG = "SystemServer";
    static Timer timer;

    public static native void init1(String[] paramArrayOfString);

    public static final void init2()
    {
        Slog.i("SystemServer", "Entered the Android system server!");
        ServerThread localServerThread = new ServerThread();
        localServerThread.setName("android.server.ServerThread");
        localServerThread.start();
    }

    public static void main(String[] paramArrayOfString)
    {
        if (System.currentTimeMillis() < 86400000L)
        {
            Slog.w("SystemServer", "System clock is before 1970; setting to 1970.");
            SystemClock.setCurrentTimeMillis(86400000L);
        }
        if (SamplingProfilerIntegration.isEnabled())
        {
            SamplingProfilerIntegration.start();
            timer = new Timer();
            timer.schedule(new TimerTask()
            {
                public void run()
                {
                    SamplingProfilerIntegration.writeSnapshot("system_server", null);
                }
            }
            , 3600000L, 3600000L);
        }
        VMRuntime.getRuntime().clearGrowthLimit();
        VMRuntime.getRuntime().setTargetHeapUtilization(0.8F);
        System.loadLibrary("android_servers");
        init1(paramArrayOfString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.SystemServer
 * JD-Core Version:        0.6.2
 */