package com.android.server;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Slog;
import com.android.server.pm.ShutdownThread;

public class ShutdownActivity extends Activity
{
    private static final String TAG = "ShutdownActivity";
    private boolean mConfirm;
    private boolean mReboot;

    protected void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
        Intent localIntent = getIntent();
        this.mReboot = "android.intent.action.REBOOT".equals(localIntent.getAction());
        this.mConfirm = localIntent.getBooleanExtra("android.intent.extra.KEY_CONFIRM", false);
        Slog.i("ShutdownActivity", "onCreate(): confirm=" + this.mConfirm);
        new Handler().post(new Runnable()
        {
            public void run()
            {
                if (ShutdownActivity.this.mReboot)
                    ShutdownThread.reboot(ShutdownActivity.this, null, ShutdownActivity.this.mConfirm);
                while (true)
                {
                    return;
                    ShutdownThread.shutdown(ShutdownActivity.this, ShutdownActivity.this.mConfirm);
                }
            }
        });
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.ShutdownActivity
 * JD-Core Version:        0.6.2
 */