package com.android.server.connectivity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.net.IConnectivityManager;
import android.net.INetworkManagementEventObserver.Stub;
import android.net.INetworkStatsService;
import android.net.LinkProperties;
import android.net.NetworkInfo;
import android.net.NetworkUtils;
import android.os.Binder;
import android.os.HandlerThread;
import android.os.INetworkManagementService;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.android.internal.util.IState;
import com.android.internal.util.State;
import com.android.internal.util.StateMachine;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

public class Tethering extends INetworkManagementEventObserver.Stub
{
    private static final boolean DBG = true;
    private static final String[] DHCP_DEFAULT_RANGE = arrayOfString;
    private static final String DNS_DEFAULT_SERVER1 = "8.8.8.8";
    private static final String DNS_DEFAULT_SERVER2 = "8.8.4.4";
    private static final Integer DUN_TYPE;
    private static final Integer HIPRI_TYPE;
    private static final Integer MOBILE_TYPE = new Integer(0);
    private static final String TAG = "Tethering";
    private static final String USB_NEAR_IFACE_ADDR = "192.168.42.129";
    private static final int USB_PREFIX_LENGTH = 24;
    private static final boolean VDBG;
    private final IConnectivityManager mConnService;
    private Context mContext;
    private String[] mDefaultDnsServers;
    private String[] mDhcpRange;
    private HashMap<String, TetherInterfaceSM> mIfaces;
    private Looper mLooper;
    private final INetworkManagementService mNMService;
    private int mPreferredUpstreamMobileApn = -1;
    private Object mPublicSync;
    private boolean mRndisEnabled;
    private BroadcastReceiver mStateReceiver;
    private final INetworkStatsService mStatsService;
    private StateMachine mTetherMasterSM;
    private String[] mTetherableBluetoothRegexs;
    private String[] mTetherableUsbRegexs;
    private String[] mTetherableWifiRegexs;
    private Notification mTetheredNotification;
    private HandlerThread mThread;
    private Collection<Integer> mUpstreamIfaceTypes;
    private boolean mUsbTetherRequested;

    static
    {
        HIPRI_TYPE = new Integer(5);
        DUN_TYPE = new Integer(4);
        String[] arrayOfString = new String[14];
        arrayOfString[0] = "192.168.42.2";
        arrayOfString[1] = "192.168.42.254";
        arrayOfString[2] = "192.168.43.2";
        arrayOfString[3] = "192.168.43.254";
        arrayOfString[4] = "192.168.44.2";
        arrayOfString[5] = "192.168.44.254";
        arrayOfString[6] = "192.168.45.2";
        arrayOfString[7] = "192.168.45.254";
        arrayOfString[8] = "192.168.46.2";
        arrayOfString[9] = "192.168.46.254";
        arrayOfString[10] = "192.168.47.2";
        arrayOfString[11] = "192.168.47.254";
        arrayOfString[12] = "192.168.48.2";
        arrayOfString[13] = "192.168.48.254";
    }

    public Tethering(Context paramContext, INetworkManagementService paramINetworkManagementService, INetworkStatsService paramINetworkStatsService, IConnectivityManager paramIConnectivityManager, Looper paramLooper)
    {
        this.mContext = paramContext;
        this.mNMService = paramINetworkManagementService;
        this.mStatsService = paramINetworkStatsService;
        this.mConnService = paramIConnectivityManager;
        this.mLooper = paramLooper;
        this.mPublicSync = new Object();
        this.mIfaces = new HashMap();
        this.mThread = new HandlerThread("Tethering");
        this.mThread.start();
        this.mLooper = this.mThread.getLooper();
        this.mTetherMasterSM = new TetherMasterSM("TetherMaster", this.mLooper);
        this.mTetherMasterSM.start();
        this.mStateReceiver = new StateReceiver(null);
        IntentFilter localIntentFilter1 = new IntentFilter();
        localIntentFilter1.addAction("android.hardware.usb.action.USB_STATE");
        localIntentFilter1.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        this.mContext.registerReceiver(this.mStateReceiver, localIntentFilter1);
        IntentFilter localIntentFilter2 = new IntentFilter();
        localIntentFilter2.addAction("android.intent.action.MEDIA_SHARED");
        localIntentFilter2.addAction("android.intent.action.MEDIA_UNSHARED");
        localIntentFilter2.addDataScheme("file");
        this.mContext.registerReceiver(this.mStateReceiver, localIntentFilter2);
        this.mDhcpRange = paramContext.getResources().getStringArray(17235995);
        if ((this.mDhcpRange.length == 0) || (this.mDhcpRange.length % 2 == 1))
            this.mDhcpRange = DHCP_DEFAULT_RANGE;
        updateConfiguration();
        this.mDefaultDnsServers = new String[2];
        this.mDefaultDnsServers[0] = "8.8.8.8";
        this.mDefaultDnsServers[1] = "8.8.4.4";
    }

    private void clearTetheredNotification()
    {
        NotificationManager localNotificationManager = (NotificationManager)this.mContext.getSystemService("notification");
        if ((localNotificationManager != null) && (this.mTetheredNotification != null))
        {
            localNotificationManager.cancel(this.mTetheredNotification.icon);
            this.mTetheredNotification = null;
        }
    }

    // ERROR //
    private boolean configureUsbIface(boolean paramBoolean)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore_2
        //     2: iconst_0
        //     3: anewarray 91	java/lang/String
        //     6: pop
        //     7: aload_0
        //     8: getfield 130	com/android/server/connectivity/Tethering:mNMService	Landroid/os/INetworkManagementService;
        //     11: invokeinterface 290 1 0
        //     16: astore 6
        //     18: aload 6
        //     20: arraylength
        //     21: istore 7
        //     23: iconst_0
        //     24: istore 8
        //     26: iload 8
        //     28: iload 7
        //     30: if_icmpge +149 -> 179
        //     33: aload 6
        //     35: iload 8
        //     37: aaload
        //     38: astore 9
        //     40: aload_0
        //     41: aload 9
        //     43: invokespecial 294	com/android/server/connectivity/Tethering:isUsb	(Ljava/lang/String;)Z
        //     46: ifeq +70 -> 116
        //     49: aload_0
        //     50: getfield 130	com/android/server/connectivity/Tethering:mNMService	Landroid/os/INetworkManagementService;
        //     53: aload 9
        //     55: invokeinterface 298 2 0
        //     60: astore 12
        //     62: aload 12
        //     64: ifnull +52 -> 116
        //     67: aload 12
        //     69: new 300	android/net/LinkAddress
        //     72: dup
        //     73: ldc 37
        //     75: invokestatic 306	android/net/NetworkUtils:numericToInetAddress	(Ljava/lang/String;)Ljava/net/InetAddress;
        //     78: bipush 24
        //     80: invokespecial 309	android/net/LinkAddress:<init>	(Ljava/net/InetAddress;I)V
        //     83: invokevirtual 315	android/net/InterfaceConfiguration:setLinkAddress	(Landroid/net/LinkAddress;)V
        //     86: iload_1
        //     87: ifeq +50 -> 137
        //     90: aload 12
        //     92: invokevirtual 318	android/net/InterfaceConfiguration:setInterfaceUp	()V
        //     95: aload 12
        //     97: ldc_w 320
        //     100: invokevirtual 323	android/net/InterfaceConfiguration:clearFlag	(Ljava/lang/String;)V
        //     103: aload_0
        //     104: getfield 130	com/android/server/connectivity/Tethering:mNMService	Landroid/os/INetworkManagementService;
        //     107: aload 9
        //     109: aload 12
        //     111: invokeinterface 327 3 0
        //     116: iinc 8 1
        //     119: goto -93 -> 26
        //     122: astore 4
        //     124: ldc 34
        //     126: ldc_w 329
        //     129: aload 4
        //     131: invokestatic 335	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     134: pop
        //     135: iload_2
        //     136: ireturn
        //     137: aload 12
        //     139: invokevirtual 338	android/net/InterfaceConfiguration:setInterfaceDown	()V
        //     142: goto -47 -> 95
        //     145: astore 10
        //     147: ldc 34
        //     149: new 340	java/lang/StringBuilder
        //     152: dup
        //     153: invokespecial 341	java/lang/StringBuilder:<init>	()V
        //     156: ldc_w 343
        //     159: invokevirtual 347	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     162: aload 9
        //     164: invokevirtual 347	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     167: invokevirtual 351	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     170: aload 10
        //     172: invokestatic 335	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     175: pop
        //     176: goto -41 -> 135
        //     179: iconst_1
        //     180: istore_2
        //     181: goto -46 -> 135
        //
        // Exception table:
        //     from	to	target	type
        //     7	18	122	java/lang/Exception
        //     49	116	145	java/lang/Exception
        //     137	142	145	java/lang/Exception
    }

    private boolean isUsb(String paramString)
    {
        while (true)
        {
            int j;
            boolean bool;
            synchronized (this.mPublicSync)
            {
                String[] arrayOfString = this.mTetherableUsbRegexs;
                int i = arrayOfString.length;
                j = 0;
                if (j < i)
                {
                    if (!paramString.matches(arrayOfString[j]))
                        break label64;
                    bool = true;
                }
                else
                {
                    bool = false;
                }
            }
            return bool;
            label64: j++;
        }
    }

    // ERROR //
    private void sendTetherStateChangedBroadcast()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 134	com/android/server/connectivity/Tethering:mConnService	Landroid/net/IConnectivityManager;
        //     4: invokeinterface 364 1 0
        //     9: istore_2
        //     10: iload_2
        //     11: ifne +8 -> 19
        //     14: return
        //     15: astore_1
        //     16: goto -2 -> 14
        //     19: new 366	java/util/ArrayList
        //     22: dup
        //     23: invokespecial 367	java/util/ArrayList:<init>	()V
        //     26: astore_3
        //     27: new 366	java/util/ArrayList
        //     30: dup
        //     31: invokespecial 367	java/util/ArrayList:<init>	()V
        //     34: astore 4
        //     36: new 366	java/util/ArrayList
        //     39: dup
        //     40: invokespecial 367	java/util/ArrayList:<init>	()V
        //     43: astore 5
        //     45: iconst_0
        //     46: istore 6
        //     48: iconst_0
        //     49: istore 7
        //     51: iconst_0
        //     52: istore 8
        //     54: aload_0
        //     55: getfield 141	com/android/server/connectivity/Tethering:mPublicSync	Ljava/lang/Object;
        //     58: astore 9
        //     60: aload 9
        //     62: monitorenter
        //     63: aload_0
        //     64: getfield 146	com/android/server/connectivity/Tethering:mIfaces	Ljava/util/HashMap;
        //     67: invokevirtual 371	java/util/HashMap:keySet	()Ljava/util/Set;
        //     70: invokeinterface 377 1 0
        //     75: astore 11
        //     77: aload 11
        //     79: invokeinterface 382 1 0
        //     84: ifeq +155 -> 239
        //     87: aload 11
        //     89: invokeinterface 386 1 0
        //     94: astore 18
        //     96: aload_0
        //     97: getfield 146	com/android/server/connectivity/Tethering:mIfaces	Ljava/util/HashMap;
        //     100: aload 18
        //     102: invokevirtual 390	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     105: checkcast 11	com/android/server/connectivity/Tethering$TetherInterfaceSM
        //     108: astore 19
        //     110: aload 19
        //     112: ifnull -35 -> 77
        //     115: aload 19
        //     117: invokevirtual 393	com/android/server/connectivity/Tethering$TetherInterfaceSM:isErrored	()Z
        //     120: ifeq +25 -> 145
        //     123: aload 5
        //     125: aload 18
        //     127: checkcast 91	java/lang/String
        //     130: invokevirtual 397	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     133: pop
        //     134: goto -57 -> 77
        //     137: astore 10
        //     139: aload 9
        //     141: monitorexit
        //     142: aload 10
        //     144: athrow
        //     145: aload 19
        //     147: invokevirtual 400	com/android/server/connectivity/Tethering$TetherInterfaceSM:isAvailable	()Z
        //     150: ifeq +16 -> 166
        //     153: aload_3
        //     154: aload 18
        //     156: checkcast 91	java/lang/String
        //     159: invokevirtual 397	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     162: pop
        //     163: goto -86 -> 77
        //     166: aload 19
        //     168: invokevirtual 403	com/android/server/connectivity/Tethering$TetherInterfaceSM:isTethered	()Z
        //     171: ifeq -94 -> 77
        //     174: aload_0
        //     175: aload 18
        //     177: checkcast 91	java/lang/String
        //     180: invokespecial 294	com/android/server/connectivity/Tethering:isUsb	(Ljava/lang/String;)Z
        //     183: ifeq +20 -> 203
        //     186: iconst_1
        //     187: istore 7
        //     189: aload 4
        //     191: aload 18
        //     193: checkcast 91	java/lang/String
        //     196: invokevirtual 397	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     199: pop
        //     200: goto -123 -> 77
        //     203: aload_0
        //     204: aload 18
        //     206: checkcast 91	java/lang/String
        //     209: invokevirtual 406	com/android/server/connectivity/Tethering:isWifi	(Ljava/lang/String;)Z
        //     212: ifeq +9 -> 221
        //     215: iconst_1
        //     216: istore 6
        //     218: goto -29 -> 189
        //     221: aload_0
        //     222: aload 18
        //     224: checkcast 91	java/lang/String
        //     227: invokevirtual 409	com/android/server/connectivity/Tethering:isBluetooth	(Ljava/lang/String;)Z
        //     230: ifeq -41 -> 189
        //     233: iconst_1
        //     234: istore 8
        //     236: goto -47 -> 189
        //     239: aload 9
        //     241: monitorexit
        //     242: new 411	android/content/Intent
        //     245: dup
        //     246: ldc_w 413
        //     249: invokespecial 414	android/content/Intent:<init>	(Ljava/lang/String;)V
        //     252: astore 12
        //     254: aload 12
        //     256: ldc_w 415
        //     259: invokevirtual 419	android/content/Intent:addFlags	(I)Landroid/content/Intent;
        //     262: pop
        //     263: aload 12
        //     265: ldc_w 421
        //     268: aload_3
        //     269: invokevirtual 425	android/content/Intent:putStringArrayListExtra	(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;
        //     272: pop
        //     273: aload 12
        //     275: ldc_w 427
        //     278: aload 4
        //     280: invokevirtual 425	android/content/Intent:putStringArrayListExtra	(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;
        //     283: pop
        //     284: aload 12
        //     286: ldc_w 429
        //     289: aload 5
        //     291: invokevirtual 425	android/content/Intent:putStringArrayListExtra	(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;
        //     294: pop
        //     295: aload_0
        //     296: getfield 128	com/android/server/connectivity/Tethering:mContext	Landroid/content/Context;
        //     299: aload 12
        //     301: invokevirtual 433	android/content/Context:sendStickyBroadcast	(Landroid/content/Intent;)V
        //     304: ldc 34
        //     306: new 340	java/lang/StringBuilder
        //     309: dup
        //     310: invokespecial 341	java/lang/StringBuilder:<init>	()V
        //     313: ldc_w 435
        //     316: invokevirtual 347	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     319: aload_3
        //     320: invokevirtual 439	java/util/ArrayList:size	()I
        //     323: invokevirtual 442	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     326: ldc_w 444
        //     329: invokevirtual 347	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     332: aload 4
        //     334: invokevirtual 439	java/util/ArrayList:size	()I
        //     337: invokevirtual 442	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     340: ldc_w 444
        //     343: invokevirtual 347	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     346: aload 5
        //     348: invokevirtual 439	java/util/ArrayList:size	()I
        //     351: invokevirtual 442	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     354: invokevirtual 351	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     357: invokestatic 448	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     360: pop
        //     361: iload 7
        //     363: ifeq +33 -> 396
        //     366: iload 6
        //     368: ifne +8 -> 376
        //     371: iload 8
        //     373: ifeq +13 -> 386
        //     376: aload_0
        //     377: ldc_w 449
        //     380: invokespecial 452	com/android/server/connectivity/Tethering:showTetheredNotification	(I)V
        //     383: goto -369 -> 14
        //     386: aload_0
        //     387: ldc_w 453
        //     390: invokespecial 452	com/android/server/connectivity/Tethering:showTetheredNotification	(I)V
        //     393: goto -379 -> 14
        //     396: iload 6
        //     398: ifeq +28 -> 426
        //     401: iload 8
        //     403: ifeq +13 -> 416
        //     406: aload_0
        //     407: ldc_w 449
        //     410: invokespecial 452	com/android/server/connectivity/Tethering:showTetheredNotification	(I)V
        //     413: goto -399 -> 14
        //     416: aload_0
        //     417: ldc_w 454
        //     420: invokespecial 452	com/android/server/connectivity/Tethering:showTetheredNotification	(I)V
        //     423: goto -409 -> 14
        //     426: iload 8
        //     428: ifeq +13 -> 441
        //     431: aload_0
        //     432: ldc_w 455
        //     435: invokespecial 452	com/android/server/connectivity/Tethering:showTetheredNotification	(I)V
        //     438: goto -424 -> 14
        //     441: aload_0
        //     442: invokespecial 457	com/android/server/connectivity/Tethering:clearTetheredNotification	()V
        //     445: goto -431 -> 14
        //
        // Exception table:
        //     from	to	target	type
        //     0	10	15	android/os/RemoteException
        //     63	142	137	finally
        //     145	242	137	finally
    }

    private void showTetheredNotification(int paramInt)
    {
        NotificationManager localNotificationManager = (NotificationManager)this.mContext.getSystemService("notification");
        if (localNotificationManager == null);
        while (true)
        {
            return;
            if (this.mTetheredNotification != null)
            {
                if (this.mTetheredNotification.icon != paramInt)
                    localNotificationManager.cancel(this.mTetheredNotification.icon);
            }
            else
            {
                Intent localIntent = new Intent();
                localIntent.setClassName("com.android.settings", "com.android.settings.TetherSettings");
                localIntent.setFlags(1073741824);
                PendingIntent localPendingIntent = PendingIntent.getActivity(this.mContext, 0, localIntent, 0);
                Resources localResources = Resources.getSystem();
                CharSequence localCharSequence1 = localResources.getText(17040517);
                CharSequence localCharSequence2 = localResources.getText(17040518);
                if (this.mTetheredNotification == null)
                {
                    this.mTetheredNotification = new Notification();
                    this.mTetheredNotification.when = 0L;
                }
                this.mTetheredNotification.icon = paramInt;
                Notification localNotification = this.mTetheredNotification;
                localNotification.defaults = (0xFFFFFFFE & localNotification.defaults);
                this.mTetheredNotification.flags = 2;
                this.mTetheredNotification.tickerText = localCharSequence1;
                this.mTetheredNotification.setLatestEventInfo(this.mContext, localCharSequence1, localCharSequence2, localPendingIntent);
                localNotificationManager.notify(this.mTetheredNotification.icon, this.mTetheredNotification);
            }
        }
    }

    private void tetherUsb(boolean paramBoolean)
    {
        new String[0];
        try
        {
            String[] arrayOfString = this.mNMService.listInterfaces();
            int i = arrayOfString.length;
            j = 0;
            if (j < i)
            {
                str = arrayOfString[j];
                if (isUsb(str))
                    if (paramBoolean)
                    {
                        k = tether(str);
                        if (k != 0)
                            break label90;
                    }
            }
        }
        catch (Exception localException)
        {
            while (true)
            {
                int j;
                String str;
                Log.e("Tethering", "Error listing Interfaces", localException);
                continue;
                int k = untether(str);
                continue;
                label90: j++;
                continue;
                Log.e("Tethering", "unable start or stop USB tethering");
            }
        }
    }

    // ERROR //
    public void checkDunRequired()
    {
        // Byte code:
        //     0: iconst_5
        //     1: istore_1
        //     2: aload_0
        //     3: getfield 128	com/android/server/connectivity/Tethering:mContext	Landroid/content/Context;
        //     6: invokevirtual 524	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
        //     9: ldc_w 526
        //     12: iconst_2
        //     13: invokestatic 532	android/provider/Settings$Secure:getInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)I
        //     16: istore_2
        //     17: aload_0
        //     18: getfield 141	com/android/server/connectivity/Tethering:mPublicSync	Ljava/lang/Object;
        //     21: astore_3
        //     22: aload_3
        //     23: monitorenter
        //     24: iload_2
        //     25: iconst_2
        //     26: if_icmpeq +112 -> 138
        //     29: iload_2
        //     30: iconst_1
        //     31: if_icmpne +5 -> 36
        //     34: iconst_4
        //     35: istore_1
        //     36: iload_1
        //     37: iconst_4
        //     38: if_icmpne +123 -> 161
        //     41: aload_0
        //     42: getfield 249	com/android/server/connectivity/Tethering:mUpstreamIfaceTypes	Ljava/util/Collection;
        //     45: getstatic 85	com/android/server/connectivity/Tethering:MOBILE_TYPE	Ljava/lang/Integer;
        //     48: invokeinterface 537 2 0
        //     53: ifeq +26 -> 79
        //     56: aload_0
        //     57: getfield 249	com/android/server/connectivity/Tethering:mUpstreamIfaceTypes	Ljava/util/Collection;
        //     60: getstatic 85	com/android/server/connectivity/Tethering:MOBILE_TYPE	Ljava/lang/Integer;
        //     63: invokeinterface 540 2 0
        //     68: pop
        //     69: goto -28 -> 41
        //     72: astore 4
        //     74: aload_3
        //     75: monitorexit
        //     76: aload 4
        //     78: athrow
        //     79: aload_0
        //     80: getfield 249	com/android/server/connectivity/Tethering:mUpstreamIfaceTypes	Ljava/util/Collection;
        //     83: getstatic 87	com/android/server/connectivity/Tethering:HIPRI_TYPE	Ljava/lang/Integer;
        //     86: invokeinterface 537 2 0
        //     91: ifeq +19 -> 110
        //     94: aload_0
        //     95: getfield 249	com/android/server/connectivity/Tethering:mUpstreamIfaceTypes	Ljava/util/Collection;
        //     98: getstatic 87	com/android/server/connectivity/Tethering:HIPRI_TYPE	Ljava/lang/Integer;
        //     101: invokeinterface 540 2 0
        //     106: pop
        //     107: goto -28 -> 79
        //     110: aload_0
        //     111: getfield 249	com/android/server/connectivity/Tethering:mUpstreamIfaceTypes	Ljava/util/Collection;
        //     114: getstatic 89	com/android/server/connectivity/Tethering:DUN_TYPE	Ljava/lang/Integer;
        //     117: invokeinterface 537 2 0
        //     122: ifne +16 -> 138
        //     125: aload_0
        //     126: getfield 249	com/android/server/connectivity/Tethering:mUpstreamIfaceTypes	Ljava/util/Collection;
        //     129: getstatic 89	com/android/server/connectivity/Tethering:DUN_TYPE	Ljava/lang/Integer;
        //     132: invokeinterface 541 2 0
        //     137: pop
        //     138: aload_0
        //     139: getfield 249	com/android/server/connectivity/Tethering:mUpstreamIfaceTypes	Ljava/util/Collection;
        //     142: getstatic 89	com/android/server/connectivity/Tethering:DUN_TYPE	Ljava/lang/Integer;
        //     145: invokeinterface 537 2 0
        //     150: ifeq +101 -> 251
        //     153: aload_0
        //     154: iconst_4
        //     155: putfield 126	com/android/server/connectivity/Tethering:mPreferredUpstreamMobileApn	I
        //     158: aload_3
        //     159: monitorexit
        //     160: return
        //     161: aload_0
        //     162: getfield 249	com/android/server/connectivity/Tethering:mUpstreamIfaceTypes	Ljava/util/Collection;
        //     165: getstatic 89	com/android/server/connectivity/Tethering:DUN_TYPE	Ljava/lang/Integer;
        //     168: invokeinterface 537 2 0
        //     173: ifeq +19 -> 192
        //     176: aload_0
        //     177: getfield 249	com/android/server/connectivity/Tethering:mUpstreamIfaceTypes	Ljava/util/Collection;
        //     180: getstatic 89	com/android/server/connectivity/Tethering:DUN_TYPE	Ljava/lang/Integer;
        //     183: invokeinterface 540 2 0
        //     188: pop
        //     189: goto -28 -> 161
        //     192: aload_0
        //     193: getfield 249	com/android/server/connectivity/Tethering:mUpstreamIfaceTypes	Ljava/util/Collection;
        //     196: getstatic 85	com/android/server/connectivity/Tethering:MOBILE_TYPE	Ljava/lang/Integer;
        //     199: invokeinterface 537 2 0
        //     204: ifne +16 -> 220
        //     207: aload_0
        //     208: getfield 249	com/android/server/connectivity/Tethering:mUpstreamIfaceTypes	Ljava/util/Collection;
        //     211: getstatic 85	com/android/server/connectivity/Tethering:MOBILE_TYPE	Ljava/lang/Integer;
        //     214: invokeinterface 541 2 0
        //     219: pop
        //     220: aload_0
        //     221: getfield 249	com/android/server/connectivity/Tethering:mUpstreamIfaceTypes	Ljava/util/Collection;
        //     224: getstatic 87	com/android/server/connectivity/Tethering:HIPRI_TYPE	Ljava/lang/Integer;
        //     227: invokeinterface 537 2 0
        //     232: ifne -94 -> 138
        //     235: aload_0
        //     236: getfield 249	com/android/server/connectivity/Tethering:mUpstreamIfaceTypes	Ljava/util/Collection;
        //     239: getstatic 87	com/android/server/connectivity/Tethering:HIPRI_TYPE	Ljava/lang/Integer;
        //     242: invokeinterface 541 2 0
        //     247: pop
        //     248: goto -110 -> 138
        //     251: aload_0
        //     252: iconst_5
        //     253: putfield 126	com/android/server/connectivity/Tethering:mPreferredUpstreamMobileApn	I
        //     256: goto -98 -> 158
        //
        // Exception table:
        //     from	to	target	type
        //     41	76	72	finally
        //     79	256	72	finally
    }

    // ERROR //
    public void dump(java.io.FileDescriptor paramFileDescriptor, java.io.PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 128	com/android/server/connectivity/Tethering:mContext	Landroid/content/Context;
        //     4: ldc_w 545
        //     7: invokevirtual 548	android/content/Context:checkCallingOrSelfPermission	(Ljava/lang/String;)I
        //     10: ifeq +42 -> 52
        //     13: aload_2
        //     14: new 340	java/lang/StringBuilder
        //     17: dup
        //     18: invokespecial 341	java/lang/StringBuilder:<init>	()V
        //     21: ldc_w 550
        //     24: invokevirtual 347	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     27: invokestatic 555	android/os/Binder:getCallingPid	()I
        //     30: invokevirtual 442	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     33: ldc_w 557
        //     36: invokevirtual 347	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     39: invokestatic 560	android/os/Binder:getCallingUid	()I
        //     42: invokevirtual 442	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     45: invokevirtual 351	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     48: invokevirtual 565	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     51: return
        //     52: aload_0
        //     53: getfield 141	com/android/server/connectivity/Tethering:mPublicSync	Ljava/lang/Object;
        //     56: astore 4
        //     58: aload 4
        //     60: monitorenter
        //     61: aload_2
        //     62: ldc_w 567
        //     65: invokevirtual 565	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     68: aload_0
        //     69: getfield 249	com/android/server/connectivity/Tethering:mUpstreamIfaceTypes	Ljava/util/Collection;
        //     72: invokeinterface 568 1 0
        //     77: astore 6
        //     79: aload 6
        //     81: invokeinterface 382 1 0
        //     86: ifeq +51 -> 137
        //     89: aload 6
        //     91: invokeinterface 386 1 0
        //     96: checkcast 79	java/lang/Integer
        //     99: astore 9
        //     101: aload_2
        //     102: new 340	java/lang/StringBuilder
        //     105: dup
        //     106: invokespecial 341	java/lang/StringBuilder:<init>	()V
        //     109: ldc_w 570
        //     112: invokevirtual 347	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     115: aload 9
        //     117: invokevirtual 573	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     120: invokevirtual 351	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     123: invokevirtual 565	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     126: goto -47 -> 79
        //     129: astore 5
        //     131: aload 4
        //     133: monitorexit
        //     134: aload 5
        //     136: athrow
        //     137: aload_2
        //     138: invokevirtual 575	java/io/PrintWriter:println	()V
        //     141: aload_2
        //     142: ldc_w 577
        //     145: invokevirtual 565	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     148: aload_0
        //     149: getfield 146	com/android/server/connectivity/Tethering:mIfaces	Ljava/util/HashMap;
        //     152: invokevirtual 581	java/util/HashMap:values	()Ljava/util/Collection;
        //     155: invokeinterface 568 1 0
        //     160: astore 7
        //     162: aload 7
        //     164: invokeinterface 382 1 0
        //     169: ifeq +46 -> 215
        //     172: aload 7
        //     174: invokeinterface 386 1 0
        //     179: checkcast 11	com/android/server/connectivity/Tethering$TetherInterfaceSM
        //     182: astore 8
        //     184: aload_2
        //     185: new 340	java/lang/StringBuilder
        //     188: dup
        //     189: invokespecial 341	java/lang/StringBuilder:<init>	()V
        //     192: ldc_w 570
        //     195: invokevirtual 347	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     198: aload 8
        //     200: invokevirtual 582	com/android/server/connectivity/Tethering$TetherInterfaceSM:toString	()Ljava/lang/String;
        //     203: invokevirtual 347	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     206: invokevirtual 351	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     209: invokevirtual 565	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     212: goto -50 -> 162
        //     215: aload 4
        //     217: monitorexit
        //     218: aload_2
        //     219: invokevirtual 575	java/io/PrintWriter:println	()V
        //     222: goto -171 -> 51
        //
        // Exception table:
        //     from	to	target	type
        //     61	134	129	finally
        //     137	218	129	finally
    }

    // ERROR //
    public String[] getErroredIfaces()
    {
        // Byte code:
        //     0: new 366	java/util/ArrayList
        //     3: dup
        //     4: invokespecial 367	java/util/ArrayList:<init>	()V
        //     7: astore_1
        //     8: aload_0
        //     9: getfield 141	com/android/server/connectivity/Tethering:mPublicSync	Ljava/lang/Object;
        //     12: astore_2
        //     13: aload_2
        //     14: monitorenter
        //     15: aload_0
        //     16: getfield 146	com/android/server/connectivity/Tethering:mIfaces	Ljava/util/HashMap;
        //     19: invokevirtual 371	java/util/HashMap:keySet	()Ljava/util/Set;
        //     22: invokeinterface 377 1 0
        //     27: astore 4
        //     29: aload 4
        //     31: invokeinterface 382 1 0
        //     36: ifeq +48 -> 84
        //     39: aload 4
        //     41: invokeinterface 386 1 0
        //     46: astore 7
        //     48: aload_0
        //     49: getfield 146	com/android/server/connectivity/Tethering:mIfaces	Ljava/util/HashMap;
        //     52: aload 7
        //     54: invokevirtual 390	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     57: checkcast 11	com/android/server/connectivity/Tethering$TetherInterfaceSM
        //     60: invokevirtual 393	com/android/server/connectivity/Tethering$TetherInterfaceSM:isErrored	()Z
        //     63: ifeq -34 -> 29
        //     66: aload_1
        //     67: aload 7
        //     69: checkcast 91	java/lang/String
        //     72: invokevirtual 397	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     75: pop
        //     76: goto -47 -> 29
        //     79: astore_3
        //     80: aload_2
        //     81: monitorexit
        //     82: aload_3
        //     83: athrow
        //     84: aload_2
        //     85: monitorexit
        //     86: aload_1
        //     87: invokevirtual 439	java/util/ArrayList:size	()I
        //     90: anewarray 91	java/lang/String
        //     93: astore 5
        //     95: iconst_0
        //     96: istore 6
        //     98: iload 6
        //     100: aload_1
        //     101: invokevirtual 439	java/util/ArrayList:size	()I
        //     104: if_icmpge +23 -> 127
        //     107: aload 5
        //     109: iload 6
        //     111: aload_1
        //     112: iload 6
        //     114: invokevirtual 586	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     117: checkcast 91	java/lang/String
        //     120: aastore
        //     121: iinc 6 1
        //     124: goto -26 -> 98
        //     127: aload 5
        //     129: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     15	82	79	finally
        //     84	86	79	finally
    }

    public int getLastTetherError(String paramString)
    {
        int i;
        synchronized (this.mPublicSync)
        {
            TetherInterfaceSM localTetherInterfaceSM = (TetherInterfaceSM)this.mIfaces.get(paramString);
            if (localTetherInterfaceSM == null)
            {
                Log.e("Tethering", "Tried to getLastTetherError on an unknown iface :" + paramString + ", ignoring");
                i = 1;
            }
            else
            {
                i = localTetherInterfaceSM.getLastError();
            }
        }
        return i;
    }

    public String[] getTetherableBluetoothRegexs()
    {
        return this.mTetherableBluetoothRegexs;
    }

    // ERROR //
    public String[] getTetherableIfaces()
    {
        // Byte code:
        //     0: new 366	java/util/ArrayList
        //     3: dup
        //     4: invokespecial 367	java/util/ArrayList:<init>	()V
        //     7: astore_1
        //     8: aload_0
        //     9: getfield 141	com/android/server/connectivity/Tethering:mPublicSync	Ljava/lang/Object;
        //     12: astore_2
        //     13: aload_2
        //     14: monitorenter
        //     15: aload_0
        //     16: getfield 146	com/android/server/connectivity/Tethering:mIfaces	Ljava/util/HashMap;
        //     19: invokevirtual 371	java/util/HashMap:keySet	()Ljava/util/Set;
        //     22: invokeinterface 377 1 0
        //     27: astore 4
        //     29: aload 4
        //     31: invokeinterface 382 1 0
        //     36: ifeq +48 -> 84
        //     39: aload 4
        //     41: invokeinterface 386 1 0
        //     46: astore 7
        //     48: aload_0
        //     49: getfield 146	com/android/server/connectivity/Tethering:mIfaces	Ljava/util/HashMap;
        //     52: aload 7
        //     54: invokevirtual 390	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     57: checkcast 11	com/android/server/connectivity/Tethering$TetherInterfaceSM
        //     60: invokevirtual 400	com/android/server/connectivity/Tethering$TetherInterfaceSM:isAvailable	()Z
        //     63: ifeq -34 -> 29
        //     66: aload_1
        //     67: aload 7
        //     69: checkcast 91	java/lang/String
        //     72: invokevirtual 397	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     75: pop
        //     76: goto -47 -> 29
        //     79: astore_3
        //     80: aload_2
        //     81: monitorexit
        //     82: aload_3
        //     83: athrow
        //     84: aload_2
        //     85: monitorexit
        //     86: aload_1
        //     87: invokevirtual 439	java/util/ArrayList:size	()I
        //     90: anewarray 91	java/lang/String
        //     93: astore 5
        //     95: iconst_0
        //     96: istore 6
        //     98: iload 6
        //     100: aload_1
        //     101: invokevirtual 439	java/util/ArrayList:size	()I
        //     104: if_icmpge +23 -> 127
        //     107: aload 5
        //     109: iload 6
        //     111: aload_1
        //     112: iload 6
        //     114: invokevirtual 586	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     117: checkcast 91	java/lang/String
        //     120: aastore
        //     121: iinc 6 1
        //     124: goto -26 -> 98
        //     127: aload 5
        //     129: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     15	82	79	finally
        //     84	86	79	finally
    }

    public String[] getTetherableUsbRegexs()
    {
        return this.mTetherableUsbRegexs;
    }

    public String[] getTetherableWifiRegexs()
    {
        return this.mTetherableWifiRegexs;
    }

    // ERROR //
    public String[] getTetheredIfacePairs()
    {
        // Byte code:
        //     0: invokestatic 609	com/google/android/collect/Lists:newArrayList	()Ljava/util/ArrayList;
        //     3: astore_1
        //     4: aload_0
        //     5: getfield 141	com/android/server/connectivity/Tethering:mPublicSync	Ljava/lang/Object;
        //     8: astore_2
        //     9: aload_2
        //     10: monitorenter
        //     11: aload_0
        //     12: getfield 146	com/android/server/connectivity/Tethering:mIfaces	Ljava/util/HashMap;
        //     15: invokevirtual 581	java/util/HashMap:values	()Ljava/util/Collection;
        //     18: invokeinterface 568 1 0
        //     23: astore 4
        //     25: aload 4
        //     27: invokeinterface 382 1 0
        //     32: ifeq +51 -> 83
        //     35: aload 4
        //     37: invokeinterface 386 1 0
        //     42: checkcast 11	com/android/server/connectivity/Tethering$TetherInterfaceSM
        //     45: astore 5
        //     47: aload 5
        //     49: invokevirtual 403	com/android/server/connectivity/Tethering$TetherInterfaceSM:isTethered	()Z
        //     52: ifeq -27 -> 25
        //     55: aload_1
        //     56: aload 5
        //     58: getfield 612	com/android/server/connectivity/Tethering$TetherInterfaceSM:mMyUpstreamIfaceName	Ljava/lang/String;
        //     61: invokevirtual 397	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     64: pop
        //     65: aload_1
        //     66: aload 5
        //     68: getfield 615	com/android/server/connectivity/Tethering$TetherInterfaceSM:mIfaceName	Ljava/lang/String;
        //     71: invokevirtual 397	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     74: pop
        //     75: goto -50 -> 25
        //     78: astore_3
        //     79: aload_2
        //     80: monitorexit
        //     81: aload_3
        //     82: athrow
        //     83: aload_2
        //     84: monitorexit
        //     85: aload_1
        //     86: aload_1
        //     87: invokevirtual 439	java/util/ArrayList:size	()I
        //     90: anewarray 91	java/lang/String
        //     93: invokevirtual 619	java/util/ArrayList:toArray	([Ljava/lang/Object;)[Ljava/lang/Object;
        //     96: checkcast 620	[Ljava/lang/String;
        //     99: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     11	81	78	finally
        //     83	85	78	finally
    }

    // ERROR //
    public String[] getTetheredIfaces()
    {
        // Byte code:
        //     0: new 366	java/util/ArrayList
        //     3: dup
        //     4: invokespecial 367	java/util/ArrayList:<init>	()V
        //     7: astore_1
        //     8: aload_0
        //     9: getfield 141	com/android/server/connectivity/Tethering:mPublicSync	Ljava/lang/Object;
        //     12: astore_2
        //     13: aload_2
        //     14: monitorenter
        //     15: aload_0
        //     16: getfield 146	com/android/server/connectivity/Tethering:mIfaces	Ljava/util/HashMap;
        //     19: invokevirtual 371	java/util/HashMap:keySet	()Ljava/util/Set;
        //     22: invokeinterface 377 1 0
        //     27: astore 4
        //     29: aload 4
        //     31: invokeinterface 382 1 0
        //     36: ifeq +48 -> 84
        //     39: aload 4
        //     41: invokeinterface 386 1 0
        //     46: astore 7
        //     48: aload_0
        //     49: getfield 146	com/android/server/connectivity/Tethering:mIfaces	Ljava/util/HashMap;
        //     52: aload 7
        //     54: invokevirtual 390	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     57: checkcast 11	com/android/server/connectivity/Tethering$TetherInterfaceSM
        //     60: invokevirtual 403	com/android/server/connectivity/Tethering$TetherInterfaceSM:isTethered	()Z
        //     63: ifeq -34 -> 29
        //     66: aload_1
        //     67: aload 7
        //     69: checkcast 91	java/lang/String
        //     72: invokevirtual 397	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     75: pop
        //     76: goto -47 -> 29
        //     79: astore_3
        //     80: aload_2
        //     81: monitorexit
        //     82: aload_3
        //     83: athrow
        //     84: aload_2
        //     85: monitorexit
        //     86: aload_1
        //     87: invokevirtual 439	java/util/ArrayList:size	()I
        //     90: anewarray 91	java/lang/String
        //     93: astore 5
        //     95: iconst_0
        //     96: istore 6
        //     98: iload 6
        //     100: aload_1
        //     101: invokevirtual 439	java/util/ArrayList:size	()I
        //     104: if_icmpge +23 -> 127
        //     107: aload 5
        //     109: iload 6
        //     111: aload_1
        //     112: iload 6
        //     114: invokevirtual 586	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     117: checkcast 91	java/lang/String
        //     120: aastore
        //     121: iinc 6 1
        //     124: goto -26 -> 98
        //     127: aload 5
        //     129: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     15	82	79	finally
        //     84	86	79	finally
    }

    public int[] getUpstreamIfaceTypes()
    {
        synchronized (this.mPublicSync)
        {
            updateConfiguration();
            int[] arrayOfInt = new int[this.mUpstreamIfaceTypes.size()];
            Iterator localIterator = this.mUpstreamIfaceTypes.iterator();
            for (int i = 0; i < this.mUpstreamIfaceTypes.size(); i++)
                arrayOfInt[i] = ((Integer)localIterator.next()).intValue();
            return arrayOfInt;
        }
    }

    public void handleTetherIfaceChange()
    {
        this.mTetherMasterSM.sendMessage(3);
    }

    // ERROR //
    public void interfaceAdded(String paramString)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore_2
        //     2: iconst_0
        //     3: istore_3
        //     4: aload_0
        //     5: getfield 141	com/android/server/connectivity/Tethering:mPublicSync	Ljava/lang/Object;
        //     8: astore 4
        //     10: aload 4
        //     12: monitorenter
        //     13: aload_0
        //     14: aload_1
        //     15: invokevirtual 406	com/android/server/connectivity/Tethering:isWifi	(Ljava/lang/String;)Z
        //     18: ifeq +5 -> 23
        //     21: iconst_1
        //     22: istore_2
        //     23: aload_0
        //     24: aload_1
        //     25: invokespecial 294	com/android/server/connectivity/Tethering:isUsb	(Ljava/lang/String;)Z
        //     28: ifeq +7 -> 35
        //     31: iconst_1
        //     32: istore_2
        //     33: iconst_1
        //     34: istore_3
        //     35: aload_0
        //     36: aload_1
        //     37: invokevirtual 409	com/android/server/connectivity/Tethering:isBluetooth	(Ljava/lang/String;)Z
        //     40: ifeq +5 -> 45
        //     43: iconst_1
        //     44: istore_2
        //     45: iload_2
        //     46: ifne +9 -> 55
        //     49: aload 4
        //     51: monitorexit
        //     52: goto +66 -> 118
        //     55: aload_0
        //     56: getfield 146	com/android/server/connectivity/Tethering:mIfaces	Ljava/util/HashMap;
        //     59: aload_1
        //     60: invokevirtual 390	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     63: checkcast 11	com/android/server/connectivity/Tethering$TetherInterfaceSM
        //     66: ifnull +17 -> 83
        //     69: aload 4
        //     71: monitorexit
        //     72: goto +46 -> 118
        //     75: astore 5
        //     77: aload 4
        //     79: monitorexit
        //     80: aload 5
        //     82: athrow
        //     83: new 11	com/android/server/connectivity/Tethering$TetherInterfaceSM
        //     86: dup
        //     87: aload_0
        //     88: aload_1
        //     89: aload_0
        //     90: getfield 136	com/android/server/connectivity/Tethering:mLooper	Landroid/os/Looper;
        //     93: iload_3
        //     94: invokespecial 635	com/android/server/connectivity/Tethering$TetherInterfaceSM:<init>	(Lcom/android/server/connectivity/Tethering;Ljava/lang/String;Landroid/os/Looper;Z)V
        //     97: astore 6
        //     99: aload_0
        //     100: getfield 146	com/android/server/connectivity/Tethering:mIfaces	Ljava/util/HashMap;
        //     103: aload_1
        //     104: aload 6
        //     106: invokevirtual 639	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     109: pop
        //     110: aload 6
        //     112: invokevirtual 640	com/android/server/connectivity/Tethering$TetherInterfaceSM:start	()V
        //     115: aload 4
        //     117: monitorexit
        //     118: return
        //
        // Exception table:
        //     from	to	target	type
        //     13	80	75	finally
        //     83	118	75	finally
    }

    public void interfaceLinkStateChanged(String paramString, boolean paramBoolean)
    {
        interfaceStatusChanged(paramString, paramBoolean);
    }

    public void interfaceRemoved(String paramString)
    {
        synchronized (this.mPublicSync)
        {
            TetherInterfaceSM localTetherInterfaceSM = (TetherInterfaceSM)this.mIfaces.get(paramString);
            if (localTetherInterfaceSM != null)
            {
                localTetherInterfaceSM.sendMessage(4);
                this.mIfaces.remove(paramString);
            }
        }
    }

    // ERROR //
    public void interfaceStatusChanged(String paramString, boolean paramBoolean)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore_3
        //     2: iconst_0
        //     3: istore 4
        //     5: aload_0
        //     6: getfield 141	com/android/server/connectivity/Tethering:mPublicSync	Ljava/lang/Object;
        //     9: astore 5
        //     11: aload 5
        //     13: monitorenter
        //     14: aload_0
        //     15: aload_1
        //     16: invokevirtual 406	com/android/server/connectivity/Tethering:isWifi	(Ljava/lang/String;)Z
        //     19: ifeq +15 -> 34
        //     22: iconst_1
        //     23: istore_3
        //     24: iload_3
        //     25: ifne +38 -> 63
        //     28: aload 5
        //     30: monitorexit
        //     31: goto +132 -> 163
        //     34: aload_0
        //     35: aload_1
        //     36: invokespecial 294	com/android/server/connectivity/Tethering:isUsb	(Ljava/lang/String;)Z
        //     39: ifeq +11 -> 50
        //     42: iconst_1
        //     43: istore_3
        //     44: iconst_1
        //     45: istore 4
        //     47: goto -23 -> 24
        //     50: aload_0
        //     51: aload_1
        //     52: invokevirtual 409	com/android/server/connectivity/Tethering:isBluetooth	(Ljava/lang/String;)Z
        //     55: ifeq -31 -> 24
        //     58: iconst_1
        //     59: istore_3
        //     60: goto -36 -> 24
        //     63: aload_0
        //     64: getfield 146	com/android/server/connectivity/Tethering:mIfaces	Ljava/util/HashMap;
        //     67: aload_1
        //     68: invokevirtual 390	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     71: checkcast 11	com/android/server/connectivity/Tethering$TetherInterfaceSM
        //     74: astore 7
        //     76: iload_2
        //     77: ifeq +55 -> 132
        //     80: aload 7
        //     82: ifnonnull +36 -> 118
        //     85: new 11	com/android/server/connectivity/Tethering$TetherInterfaceSM
        //     88: dup
        //     89: aload_0
        //     90: aload_1
        //     91: aload_0
        //     92: getfield 136	com/android/server/connectivity/Tethering:mLooper	Landroid/os/Looper;
        //     95: iload 4
        //     97: invokespecial 635	com/android/server/connectivity/Tethering$TetherInterfaceSM:<init>	(Lcom/android/server/connectivity/Tethering;Ljava/lang/String;Landroid/os/Looper;Z)V
        //     100: astore 9
        //     102: aload_0
        //     103: getfield 146	com/android/server/connectivity/Tethering:mIfaces	Ljava/util/HashMap;
        //     106: aload_1
        //     107: aload 9
        //     109: invokevirtual 639	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     112: pop
        //     113: aload 9
        //     115: invokevirtual 640	com/android/server/connectivity/Tethering$TetherInterfaceSM:start	()V
        //     118: aload 5
        //     120: monitorexit
        //     121: goto +42 -> 163
        //     124: astore 6
        //     126: aload 5
        //     128: monitorexit
        //     129: aload 6
        //     131: athrow
        //     132: aload_0
        //     133: aload_1
        //     134: invokespecial 294	com/android/server/connectivity/Tethering:isUsb	(Ljava/lang/String;)Z
        //     137: ifne -19 -> 118
        //     140: aload 7
        //     142: ifnull -24 -> 118
        //     145: aload 7
        //     147: iconst_4
        //     148: invokevirtual 647	com/android/server/connectivity/Tethering$TetherInterfaceSM:sendMessage	(I)V
        //     151: aload_0
        //     152: getfield 146	com/android/server/connectivity/Tethering:mIfaces	Ljava/util/HashMap;
        //     155: aload_1
        //     156: invokevirtual 649	java/util/HashMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
        //     159: pop
        //     160: goto -42 -> 118
        //     163: return
        //
        // Exception table:
        //     from	to	target	type
        //     14	129	124	finally
        //     132	160	124	finally
    }

    public boolean isBluetooth(String paramString)
    {
        while (true)
        {
            int j;
            boolean bool;
            synchronized (this.mPublicSync)
            {
                String[] arrayOfString = this.mTetherableBluetoothRegexs;
                int i = arrayOfString.length;
                j = 0;
                if (j < i)
                {
                    if (!paramString.matches(arrayOfString[j]))
                        break label64;
                    bool = true;
                }
                else
                {
                    bool = false;
                }
            }
            return bool;
            label64: j++;
        }
    }

    public boolean isWifi(String paramString)
    {
        while (true)
        {
            int j;
            boolean bool;
            synchronized (this.mPublicSync)
            {
                String[] arrayOfString = this.mTetherableWifiRegexs;
                int i = arrayOfString.length;
                j = 0;
                if (j < i)
                {
                    if (!paramString.matches(arrayOfString[j]))
                        break label64;
                    bool = true;
                }
                else
                {
                    bool = false;
                }
            }
            return bool;
            label64: j++;
        }
    }

    public void limitReached(String paramString1, String paramString2)
    {
    }

    // ERROR //
    public int setUsbTethering(boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 128	com/android/server/connectivity/Tethering:mContext	Landroid/content/Context;
        //     4: ldc_w 655
        //     7: invokevirtual 270	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
        //     10: checkcast 657	android/hardware/usb/UsbManager
        //     13: astore_2
        //     14: aload_0
        //     15: getfield 141	com/android/server/connectivity/Tethering:mPublicSync	Ljava/lang/Object;
        //     18: astore_3
        //     19: aload_3
        //     20: monitorenter
        //     21: iload_1
        //     22: ifeq +42 -> 64
        //     25: aload_0
        //     26: getfield 224	com/android/server/connectivity/Tethering:mRndisEnabled	Z
        //     29: ifeq +12 -> 41
        //     32: aload_0
        //     33: iconst_1
        //     34: invokespecial 245	com/android/server/connectivity/Tethering:tetherUsb	(Z)V
        //     37: aload_3
        //     38: monitorexit
        //     39: iconst_0
        //     40: ireturn
        //     41: aload_0
        //     42: iconst_1
        //     43: putfield 233	com/android/server/connectivity/Tethering:mUsbTetherRequested	Z
        //     46: aload_2
        //     47: ldc_w 659
        //     50: iconst_0
        //     51: invokevirtual 662	android/hardware/usb/UsbManager:setCurrentFunction	(Ljava/lang/String;Z)V
        //     54: goto -17 -> 37
        //     57: astore 4
        //     59: aload_3
        //     60: monitorexit
        //     61: aload 4
        //     63: athrow
        //     64: aload_0
        //     65: iconst_0
        //     66: invokespecial 245	com/android/server/connectivity/Tethering:tetherUsb	(Z)V
        //     69: aload_0
        //     70: getfield 224	com/android/server/connectivity/Tethering:mRndisEnabled	Z
        //     73: ifeq +9 -> 82
        //     76: aload_2
        //     77: aconst_null
        //     78: iconst_0
        //     79: invokevirtual 662	android/hardware/usb/UsbManager:setCurrentFunction	(Ljava/lang/String;Z)V
        //     82: aload_0
        //     83: iconst_0
        //     84: putfield 233	com/android/server/connectivity/Tethering:mUsbTetherRequested	Z
        //     87: goto -50 -> 37
        //
        // Exception table:
        //     from	to	target	type
        //     25	61	57	finally
        //     64	87	57	finally
    }

    public int tether(String paramString)
    {
        Log.d("Tethering", "Tethering " + paramString);
        while (true)
        {
            TetherInterfaceSM localTetherInterfaceSM;
            int i;
            synchronized (this.mPublicSync)
            {
                localTetherInterfaceSM = (TetherInterfaceSM)this.mIfaces.get(paramString);
                if (localTetherInterfaceSM == null)
                {
                    Log.e("Tethering", "Tried to Tether an unknown iface :" + paramString + ", ignoring");
                    i = 1;
                    return i;
                }
            }
            if ((!localTetherInterfaceSM.isAvailable()) && (!localTetherInterfaceSM.isErrored()))
            {
                Log.e("Tethering", "Tried to Tether an unavailable iface :" + paramString + ", ignoring");
                i = 4;
            }
            else
            {
                localTetherInterfaceSM.sendMessage(2);
                i = 0;
            }
        }
    }

    public int untether(String paramString)
    {
        Log.d("Tethering", "Untethering " + paramString);
        while (true)
        {
            TetherInterfaceSM localTetherInterfaceSM;
            int i;
            synchronized (this.mPublicSync)
            {
                localTetherInterfaceSM = (TetherInterfaceSM)this.mIfaces.get(paramString);
                if (localTetherInterfaceSM == null)
                {
                    Log.e("Tethering", "Tried to Untether an unknown iface :" + paramString + ", ignoring");
                    i = 1;
                    return i;
                }
            }
            if (localTetherInterfaceSM.isErrored())
            {
                Log.e("Tethering", "Tried to Untethered an errored iface :" + paramString + ", ignoring");
                i = 4;
            }
            else
            {
                localTetherInterfaceSM.sendMessage(3);
                i = 0;
            }
        }
    }

    void updateConfiguration()
    {
        String[] arrayOfString1 = this.mContext.getResources().getStringArray(17235991);
        String[] arrayOfString2 = this.mContext.getResources().getStringArray(17235992);
        String[] arrayOfString3 = this.mContext.getResources().getStringArray(17235994);
        int[] arrayOfInt = this.mContext.getResources().getIntArray(17235997);
        ArrayList localArrayList = new ArrayList();
        int i = arrayOfInt.length;
        for (int j = 0; j < i; j++)
            localArrayList.add(new Integer(arrayOfInt[j]));
        synchronized (this.mPublicSync)
        {
            this.mTetherableUsbRegexs = arrayOfString1;
            this.mTetherableWifiRegexs = arrayOfString2;
            this.mTetherableBluetoothRegexs = arrayOfString3;
            this.mUpstreamIfaceTypes = localArrayList;
            checkDunRequired();
            return;
        }
    }

    class TetherMasterSM extends StateMachine
    {
        private static final int CELL_CONNECTION_RENEW_MS = 40000;
        static final int CMD_CELL_CONNECTION_RENEW = 4;
        static final int CMD_RETRY_UPSTREAM = 5;
        static final int CMD_TETHER_MODE_REQUESTED = 1;
        static final int CMD_TETHER_MODE_UNREQUESTED = 2;
        static final int CMD_UPSTREAM_CHANGED = 3;
        private static final int UPSTREAM_SETTLE_TIME_MS = 10000;
        private int mCurrentConnectionSequence;
        private State mInitialState = new InitialState();
        private int mMobileApnReserved = -1;
        private ArrayList mNotifyList;
        private int mSequenceNumber;
        private State mSetDnsForwardersErrorState;
        private State mSetIpForwardingDisabledErrorState;
        private State mSetIpForwardingEnabledErrorState;
        private State mStartTetheringErrorState;
        private State mStopTetheringErrorState;
        private State mTetherModeAliveState;
        private String mUpstreamIfaceName = null;

        TetherMasterSM(String paramLooper, Looper arg3)
        {
            super(localLooper);
            addState(this.mInitialState);
            this.mTetherModeAliveState = new TetherModeAliveState();
            addState(this.mTetherModeAliveState);
            this.mSetIpForwardingEnabledErrorState = new SetIpForwardingEnabledErrorState();
            addState(this.mSetIpForwardingEnabledErrorState);
            this.mSetIpForwardingDisabledErrorState = new SetIpForwardingDisabledErrorState();
            addState(this.mSetIpForwardingDisabledErrorState);
            this.mStartTetheringErrorState = new StartTetheringErrorState();
            addState(this.mStartTetheringErrorState);
            this.mStopTetheringErrorState = new StopTetheringErrorState();
            addState(this.mStopTetheringErrorState);
            this.mSetDnsForwardersErrorState = new SetDnsForwardersErrorState();
            addState(this.mSetDnsForwardersErrorState);
            this.mNotifyList = new ArrayList();
            setInitialState(this.mInitialState);
        }

        class SetDnsForwardersErrorState extends Tethering.TetherMasterSM.ErrorState
        {
            SetDnsForwardersErrorState()
            {
                super();
            }

            public void enter()
            {
                Log.e("Tethering", "Error in setDnsForwarders");
                notify(11);
                try
                {
                    Tethering.this.mNMService.stopTethering();
                    try
                    {
                        label29: Tethering.this.mNMService.setIpForwardingEnabled(false);
                        label45: return;
                    }
                    catch (Exception localException2)
                    {
                        break label45;
                    }
                }
                catch (Exception localException1)
                {
                    break label29;
                }
            }
        }

        class StopTetheringErrorState extends Tethering.TetherMasterSM.ErrorState
        {
            StopTetheringErrorState()
            {
                super();
            }

            public void enter()
            {
                Log.e("Tethering", "Error in stopTethering");
                notify(10);
                try
                {
                    Tethering.this.mNMService.setIpForwardingEnabled(false);
                    label30: return;
                }
                catch (Exception localException)
                {
                    break label30;
                }
            }
        }

        class StartTetheringErrorState extends Tethering.TetherMasterSM.ErrorState
        {
            StartTetheringErrorState()
            {
                super();
            }

            public void enter()
            {
                Log.e("Tethering", "Error in startTethering");
                notify(9);
                try
                {
                    Tethering.this.mNMService.setIpForwardingEnabled(false);
                    label30: return;
                }
                catch (Exception localException)
                {
                    break label30;
                }
            }
        }

        class SetIpForwardingDisabledErrorState extends Tethering.TetherMasterSM.ErrorState
        {
            SetIpForwardingDisabledErrorState()
            {
                super();
            }

            public void enter()
            {
                Log.e("Tethering", "Error in setIpForwardingDisabled");
                notify(8);
            }
        }

        class SetIpForwardingEnabledErrorState extends Tethering.TetherMasterSM.ErrorState
        {
            SetIpForwardingEnabledErrorState()
            {
                super();
            }

            public void enter()
            {
                Log.e("Tethering", "Error in setIpForwardingEnabled");
                notify(7);
            }
        }

        class ErrorState extends State
        {
            int mErrorNotification;

            ErrorState()
            {
            }

            void notify(int paramInt)
            {
                this.mErrorNotification = paramInt;
                Iterator localIterator = Tethering.TetherMasterSM.this.mNotifyList.iterator();
                while (localIterator.hasNext())
                    ((Tethering.TetherInterfaceSM)localIterator.next()).sendMessage(paramInt);
            }

            public boolean processMessage(Message paramMessage)
            {
                boolean bool = true;
                switch (paramMessage.what)
                {
                default:
                    bool = false;
                case 1:
                }
                while (true)
                {
                    return bool;
                    ((Tethering.TetherInterfaceSM)paramMessage.obj).sendMessage(this.mErrorNotification);
                }
            }
        }

        class TetherModeAliveState extends Tethering.TetherMasterSM.TetherMasterUtilState
        {
            boolean mTryCell = true;

            TetherModeAliveState()
            {
                super();
            }

            public void enter()
            {
                boolean bool = true;
                turnOnMasterTetherSettings();
                this.mTryCell = bool;
                chooseUpstreamType(this.mTryCell);
                if (!this.mTryCell);
                while (true)
                {
                    this.mTryCell = bool;
                    return;
                    bool = false;
                }
            }

            public void exit()
            {
                turnOffUpstreamMobileConnection();
                notifyTetheredOfNewUpstreamIface(null);
            }

            public boolean processMessage(Message paramMessage)
            {
                boolean bool1 = true;
                Log.d("Tethering", "TetherModeAliveState.processMessage what=" + paramMessage.what);
                boolean bool2 = true;
                switch (paramMessage.what)
                {
                default:
                    bool2 = false;
                case 1:
                case 2:
                case 3:
                case 4:
                    while (true)
                    {
                        return bool2;
                        Tethering.TetherInterfaceSM localTetherInterfaceSM2 = (Tethering.TetherInterfaceSM)paramMessage.obj;
                        Tethering.TetherMasterSM.this.mNotifyList.add(localTetherInterfaceSM2);
                        localTetherInterfaceSM2.sendMessage(12, Tethering.TetherMasterSM.this.mUpstreamIfaceName);
                        continue;
                        Tethering.TetherInterfaceSM localTetherInterfaceSM1 = (Tethering.TetherInterfaceSM)paramMessage.obj;
                        int i = Tethering.TetherMasterSM.this.mNotifyList.indexOf(localTetherInterfaceSM1);
                        if (i != -1)
                        {
                            Tethering.TetherMasterSM.this.mNotifyList.remove(i);
                            if (Tethering.TetherMasterSM.this.mNotifyList.isEmpty())
                            {
                                turnOffMasterTetherSettings();
                                continue;
                                this.mTryCell = bool1;
                                chooseUpstreamType(this.mTryCell);
                                if (!this.mTryCell);
                                while (true)
                                {
                                    this.mTryCell = bool1;
                                    break;
                                    bool1 = false;
                                }
                                if (Tethering.TetherMasterSM.this.mCurrentConnectionSequence == paramMessage.arg1)
                                    turnOnUpstreamMobileConnection(Tethering.TetherMasterSM.this.mMobileApnReserved);
                            }
                        }
                    }
                case 5:
                }
                chooseUpstreamType(this.mTryCell);
                if (!this.mTryCell);
                while (true)
                {
                    this.mTryCell = bool1;
                    break;
                    bool1 = false;
                }
            }
        }

        class InitialState extends Tethering.TetherMasterSM.TetherMasterUtilState
        {
            InitialState()
            {
                super();
            }

            public void enter()
            {
            }

            public boolean processMessage(Message paramMessage)
            {
                Log.d("Tethering", "MasterInitialState.processMessage what=" + paramMessage.what);
                boolean bool = true;
                switch (paramMessage.what)
                {
                default:
                    bool = false;
                case 1:
                case 2:
                }
                while (true)
                {
                    return bool;
                    Tethering.TetherInterfaceSM localTetherInterfaceSM2 = (Tethering.TetherInterfaceSM)paramMessage.obj;
                    Tethering.TetherMasterSM.this.mNotifyList.add(localTetherInterfaceSM2);
                    Tethering.TetherMasterSM.this.transitionTo(Tethering.TetherMasterSM.this.mTetherModeAliveState);
                    continue;
                    Tethering.TetherInterfaceSM localTetherInterfaceSM1 = (Tethering.TetherInterfaceSM)paramMessage.obj;
                    if (Tethering.TetherMasterSM.this.mNotifyList.indexOf(localTetherInterfaceSM1) != -1)
                        Tethering.TetherMasterSM.this.mNotifyList.remove(localTetherInterfaceSM1);
                }
            }
        }

        class TetherMasterUtilState extends State
        {
            protected static final boolean TRY_TO_SETUP_MOBILE_CONNECTION = true;
            protected static final boolean WAIT_FOR_NETWORK_TO_SETTLE;

            TetherMasterUtilState()
            {
            }

            protected void chooseUpstreamType(boolean paramBoolean)
            {
                int i = -1;
                String str = null;
                Tethering.this.updateConfiguration();
                while (true)
                {
                    Integer localInteger;
                    Object localObject4;
                    synchronized (Tethering.this.mPublicSync)
                    {
                        Iterator localIterator1 = Tethering.this.mUpstreamIfaceTypes.iterator();
                        if (localIterator1.hasNext())
                        {
                            localInteger = (Integer)localIterator1.next();
                            localObject4 = null;
                        }
                    }
                    try
                    {
                        NetworkInfo localNetworkInfo = Tethering.this.mConnService.getNetworkInfo(localInteger.intValue());
                        localObject4 = localNetworkInfo;
                        label98: if ((localObject4 == null) || (!localObject4.isConnected()))
                            continue;
                        i = localInteger.intValue();
                        Log.d("Tethering", "chooseUpstreamType(" + paramBoolean + "), preferredApn =" + Tethering.this.mPreferredUpstreamMobileApn + ", got type=" + i);
                        if ((i == 4) || (i == 5))
                        {
                            turnOnUpstreamMobileConnection(i);
                            if (i != -1)
                                break label267;
                            int j = 1;
                            if ((paramBoolean == true) && (turnOnUpstreamMobileConnection(Tethering.this.mPreferredUpstreamMobileApn) == true))
                                j = 0;
                            if (j != 0)
                                Tethering.TetherMasterSM.this.sendMessageDelayed(5, 10000L);
                        }
                        while (true)
                        {
                            notifyTetheredOfNewUpstreamIface(str);
                            return;
                            localObject2 = finally;
                            throw localObject2;
                            if (i == -1)
                                break;
                            turnOffUpstreamMobileConnection();
                            break;
                            label267: Object localObject3 = null;
                            try
                            {
                                LinkProperties localLinkProperties = Tethering.this.mConnService.getLinkProperties(i);
                                localObject3 = localLinkProperties;
                                label292: if (localObject3 == null)
                                    continue;
                                str = localObject3.getInterfaceName();
                                String[] arrayOfString = Tethering.this.mDefaultDnsServers;
                                Collection localCollection = localObject3.getDnses();
                                if (localCollection != null)
                                {
                                    ArrayList localArrayList = new ArrayList(localCollection.size());
                                    Iterator localIterator2 = localCollection.iterator();
                                    while (localIterator2.hasNext())
                                    {
                                        InetAddress localInetAddress = (InetAddress)localIterator2.next();
                                        if ((localInetAddress instanceof Inet4Address))
                                            localArrayList.add(localInetAddress);
                                    }
                                    if (localArrayList.size() > 0)
                                        arrayOfString = NetworkUtils.makeStrings(localArrayList);
                                }
                                try
                                {
                                    Tethering.this.mNMService.setDnsForwarders(arrayOfString);
                                }
                                catch (Exception localException)
                                {
                                    Tethering.TetherMasterSM.this.transitionTo(Tethering.TetherMasterSM.this.mSetDnsForwardersErrorState);
                                }
                            }
                            catch (RemoteException localRemoteException1)
                            {
                                break label292;
                            }
                        }
                    }
                    catch (RemoteException localRemoteException2)
                    {
                        break label98;
                    }
                }
            }

            protected String enableString(int paramInt)
            {
                String str;
                switch (paramInt)
                {
                case 1:
                case 2:
                case 3:
                default:
                    str = null;
                case 4:
                case 0:
                case 5:
                }
                while (true)
                {
                    return str;
                    str = "enableDUNAlways";
                    continue;
                    str = "enableHIPRI";
                }
            }

            protected void notifyTetheredOfNewUpstreamIface(String paramString)
            {
                Log.d("Tethering", "notifying tethered with iface =" + paramString);
                Tethering.TetherMasterSM.access$4902(Tethering.TetherMasterSM.this, paramString);
                Iterator localIterator = Tethering.TetherMasterSM.this.mNotifyList.iterator();
                while (localIterator.hasNext())
                    ((Tethering.TetherInterfaceSM)localIterator.next()).sendMessage(12, paramString);
            }

            public boolean processMessage(Message paramMessage)
            {
                return false;
            }

            protected boolean turnOffMasterTetherSettings()
            {
                boolean bool = false;
                try
                {
                    Tethering.this.mNMService.stopTethering();
                }
                catch (Exception localException1)
                {
                    try
                    {
                        Tethering.this.mNMService.setIpForwardingEnabled(false);
                        Tethering.TetherMasterSM.this.transitionTo(Tethering.TetherMasterSM.this.mInitialState);
                        bool = true;
                        while (true)
                        {
                            return bool;
                            localException1 = localException1;
                            Tethering.TetherMasterSM.this.transitionTo(Tethering.TetherMasterSM.this.mStopTetheringErrorState);
                        }
                    }
                    catch (Exception localException2)
                    {
                        while (true)
                            Tethering.TetherMasterSM.this.transitionTo(Tethering.TetherMasterSM.this.mSetIpForwardingDisabledErrorState);
                    }
                }
            }

            protected boolean turnOffUpstreamMobileConnection()
            {
                boolean bool = false;
                Tethering.TetherMasterSM.access$3104(Tethering.TetherMasterSM.this);
                if (Tethering.TetherMasterSM.this.mMobileApnReserved != -1);
                try
                {
                    Tethering.this.mConnService.stopUsingNetworkFeature(0, enableString(Tethering.TetherMasterSM.this.mMobileApnReserved));
                    Tethering.TetherMasterSM.access$2902(Tethering.TetherMasterSM.this, -1);
                    bool = true;
                    label62: return bool;
                }
                catch (Exception localException)
                {
                    break label62;
                }
            }

            protected boolean turnOnMasterTetherSettings()
            {
                boolean bool = false;
                try
                {
                    Tethering.this.mNMService.setIpForwardingEnabled(true);
                }
                catch (Exception localException2)
                {
                    try
                    {
                        Tethering.this.mNMService.startTethering(Tethering.this.mDhcpRange);
                    }
                    catch (Exception localException2)
                    {
                        try
                        {
                            Tethering.this.mNMService.setDnsForwarders(Tethering.this.mDefaultDnsServers);
                            bool = true;
                            while (true)
                                while (true)
                                {
                                    return bool;
                                    localException1 = localException1;
                                    Tethering.TetherMasterSM.this.transitionTo(Tethering.TetherMasterSM.this.mSetIpForwardingEnabledErrorState);
                                    continue;
                                    localException2 = localException2;
                                    try
                                    {
                                        Tethering.this.mNMService.stopTethering();
                                        Tethering.this.mNMService.startTethering(Tethering.this.mDhcpRange);
                                    }
                                    catch (Exception localException3)
                                    {
                                        Tethering.TetherMasterSM.this.transitionTo(Tethering.TetherMasterSM.this.mStartTetheringErrorState);
                                    }
                                }
                        }
                        catch (Exception localException4)
                        {
                            while (true)
                                Tethering.TetherMasterSM.this.transitionTo(Tethering.TetherMasterSM.this.mSetDnsForwardersErrorState);
                        }
                    }
                }
            }

            protected boolean turnOnUpstreamMobileConnection(int paramInt)
            {
                boolean bool1 = false;
                boolean bool2 = true;
                if (paramInt == -1);
                while (true)
                {
                    return bool1;
                    if (paramInt != Tethering.TetherMasterSM.this.mMobileApnReserved)
                        turnOffUpstreamMobileConnection();
                    int i = 3;
                    String str = enableString(paramInt);
                    if (str == null)
                        continue;
                    try
                    {
                        int j = Tethering.this.mConnService.startUsingNetworkFeature(0, str, new Binder());
                        i = j;
                        switch (i)
                        {
                        default:
                            label74: bool2 = false;
                        case 0:
                        case 1:
                        }
                        while (true)
                        {
                            bool1 = bool2;
                            break;
                            Tethering.TetherMasterSM.access$2902(Tethering.TetherMasterSM.this, paramInt);
                            Message localMessage = Tethering.TetherMasterSM.this.obtainMessage(4);
                            localMessage.arg1 = Tethering.TetherMasterSM.access$3104(Tethering.TetherMasterSM.this);
                            Tethering.TetherMasterSM.this.sendMessageDelayed(localMessage, 40000L);
                        }
                    }
                    catch (Exception localException)
                    {
                        break label74;
                    }
                }
            }
        }
    }

    class TetherInterfaceSM extends StateMachine
    {
        static final int CMD_CELL_DUN_ERROR = 6;
        static final int CMD_INTERFACE_DOWN = 4;
        static final int CMD_INTERFACE_UP = 5;
        static final int CMD_IP_FORWARDING_DISABLE_ERROR = 8;
        static final int CMD_IP_FORWARDING_ENABLE_ERROR = 7;
        static final int CMD_SET_DNS_FORWARDERS_ERROR = 11;
        static final int CMD_START_TETHERING_ERROR = 9;
        static final int CMD_STOP_TETHERING_ERROR = 10;
        static final int CMD_TETHER_CONNECTION_CHANGED = 12;
        static final int CMD_TETHER_MODE_DEAD = 1;
        static final int CMD_TETHER_REQUESTED = 2;
        static final int CMD_TETHER_UNREQUESTED = 3;
        private boolean mAvailable;
        private State mDefaultState;
        String mIfaceName;
        private State mInitialState;
        int mLastError;
        String mMyUpstreamIfaceName;
        private State mStartingState;
        private boolean mTethered;
        private State mTetheredState;
        private State mUnavailableState;
        boolean mUsb;

        TetherInterfaceSM(String paramLooper, Looper paramBoolean, boolean arg4)
        {
            super(paramBoolean);
            this.mIfaceName = paramLooper;
            boolean bool;
            this.mUsb = bool;
            setLastError(0);
            this.mInitialState = new InitialState();
            addState(this.mInitialState);
            this.mStartingState = new StartingState();
            addState(this.mStartingState);
            this.mTetheredState = new TetheredState();
            addState(this.mTetheredState);
            this.mUnavailableState = new UnavailableState();
            addState(this.mUnavailableState);
            setInitialState(this.mInitialState);
        }

        private void setAvailable(boolean paramBoolean)
        {
            synchronized (Tethering.this.mPublicSync)
            {
                this.mAvailable = paramBoolean;
                return;
            }
        }

        private void setLastError(int paramInt)
        {
            synchronized (Tethering.this.mPublicSync)
            {
                this.mLastError = paramInt;
                if ((isErrored()) && (this.mUsb))
                    Tethering.this.configureUsbIface(false);
                return;
            }
        }

        private void setTethered(boolean paramBoolean)
        {
            synchronized (Tethering.this.mPublicSync)
            {
                this.mTethered = paramBoolean;
                return;
            }
        }

        public int getLastError()
        {
            synchronized (Tethering.this.mPublicSync)
            {
                int i = this.mLastError;
                return i;
            }
        }

        public boolean isAvailable()
        {
            synchronized (Tethering.this.mPublicSync)
            {
                boolean bool = this.mAvailable;
                return bool;
            }
        }

        public boolean isErrored()
        {
            while (true)
            {
                synchronized (Tethering.this.mPublicSync)
                {
                    if (this.mLastError != 0)
                    {
                        bool = true;
                        return bool;
                    }
                }
                boolean bool = false;
            }
        }

        public boolean isTethered()
        {
            synchronized (Tethering.this.mPublicSync)
            {
                boolean bool = this.mTethered;
                return bool;
            }
        }

        void setLastErrorAndTransitionToInitialState(int paramInt)
        {
            setLastError(paramInt);
            transitionTo(this.mInitialState);
        }

        public String toString()
        {
            String str1 = new String();
            String str2 = str1 + this.mIfaceName + " - ";
            IState localIState = getCurrentState();
            if (localIState == this.mInitialState)
                str2 = str2 + "InitialState";
            if (localIState == this.mStartingState)
                str2 = str2 + "StartingState";
            if (localIState == this.mTetheredState)
                str2 = str2 + "TetheredState";
            if (localIState == this.mUnavailableState)
                str2 = str2 + "UnavailableState";
            if (this.mAvailable)
                str2 = str2 + " - Available";
            if (this.mTethered)
                str2 = str2 + " - Tethered";
            return str2 + " - lastError =" + this.mLastError;
        }

        class UnavailableState extends State
        {
            UnavailableState()
            {
            }

            public void enter()
            {
                Tethering.TetherInterfaceSM.this.setAvailable(false);
                Tethering.TetherInterfaceSM.this.setLastError(0);
                Tethering.TetherInterfaceSM.this.setTethered(false);
                Tethering.this.sendTetherStateChangedBroadcast();
            }

            public boolean processMessage(Message paramMessage)
            {
                boolean bool = true;
                switch (paramMessage.what)
                {
                default:
                    bool = false;
                case 5:
                }
                while (true)
                {
                    return bool;
                    Tethering.TetherInterfaceSM.this.transitionTo(Tethering.TetherInterfaceSM.this.mInitialState);
                }
            }
        }

        class TetheredState extends State
        {
            TetheredState()
            {
            }

            private void cleanupUpstream()
            {
                if (Tethering.TetherInterfaceSM.this.mMyUpstreamIfaceName != null);
                try
                {
                    Tethering.this.mStatsService.forceUpdate();
                    try
                    {
                        label25: Tethering.this.mNMService.disableNat(Tethering.TetherInterfaceSM.this.mIfaceName, Tethering.TetherInterfaceSM.this.mMyUpstreamIfaceName);
                        label54: Tethering.TetherInterfaceSM.this.mMyUpstreamIfaceName = null;
                        return;
                    }
                    catch (Exception localException2)
                    {
                        break label54;
                    }
                }
                catch (Exception localException1)
                {
                    break label25;
                }
            }

            public void enter()
            {
                try
                {
                    Tethering.this.mNMService.tetherInterface(Tethering.TetherInterfaceSM.this.mIfaceName);
                    Log.d("Tethering", "Tethered " + Tethering.TetherInterfaceSM.this.mIfaceName);
                    Tethering.TetherInterfaceSM.this.setAvailable(false);
                    Tethering.TetherInterfaceSM.this.setTethered(true);
                    Tethering.this.sendTetherStateChangedBroadcast();
                    return;
                }
                catch (Exception localException)
                {
                    while (true)
                    {
                        Log.e("Tethering", "Error Tethering: " + localException.toString());
                        Tethering.TetherInterfaceSM.this.setLastError(6);
                        Tethering.TetherInterfaceSM.this.transitionTo(Tethering.TetherInterfaceSM.this.mInitialState);
                    }
                }
            }

            public boolean processMessage(Message paramMessage)
            {
                Log.d("Tethering", "TetheredState.processMessage what=" + paramMessage.what);
                boolean bool1 = true;
                int i = 0;
                switch (paramMessage.what)
                {
                case 2:
                case 5:
                default:
                    bool1 = false;
                case 3:
                case 4:
                case 12:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 1:
                }
                while (true)
                {
                    boolean bool2 = bool1;
                    label105: return bool2;
                    cleanupUpstream();
                    while (true)
                    {
                        try
                        {
                            Tethering.this.mNMService.untetherInterface(Tethering.TetherInterfaceSM.this.mIfaceName);
                            Tethering.this.mTetherMasterSM.sendMessage(2, Tethering.TetherInterfaceSM.this);
                            if (paramMessage.what != 3)
                                break label255;
                            if ((Tethering.TetherInterfaceSM.this.mUsb) && (!Tethering.this.configureUsbIface(false)))
                                Tethering.TetherInterfaceSM.this.setLastError(10);
                            Tethering.TetherInterfaceSM.this.transitionTo(Tethering.TetherInterfaceSM.this.mInitialState);
                            Log.d("Tethering", "Untethered " + Tethering.TetherInterfaceSM.this.mIfaceName);
                        }
                        catch (Exception localException4)
                        {
                            Tethering.TetherInterfaceSM.this.setLastErrorAndTransitionToInitialState(7);
                        }
                        break;
                        label255: if (paramMessage.what == 4)
                            Tethering.TetherInterfaceSM.this.transitionTo(Tethering.TetherInterfaceSM.this.mUnavailableState);
                    }
                    String str = (String)paramMessage.obj;
                    if (((Tethering.TetherInterfaceSM.this.mMyUpstreamIfaceName == null) && (str == null)) || ((Tethering.TetherInterfaceSM.this.mMyUpstreamIfaceName != null) && (Tethering.TetherInterfaceSM.this.mMyUpstreamIfaceName.equals(str))))
                        continue;
                    cleanupUpstream();
                    if (str != null);
                    try
                    {
                        Tethering.this.mNMService.enableNat(Tethering.TetherInterfaceSM.this.mIfaceName, str);
                        Tethering.TetherInterfaceSM.this.mMyUpstreamIfaceName = str;
                    }
                    catch (Exception localException2)
                    {
                        Log.e("Tethering", "Exception enabling Nat: " + localException2.toString());
                    }
                    try
                    {
                        Tethering.this.mNMService.untetherInterface(Tethering.TetherInterfaceSM.this.mIfaceName);
                        label430: Tethering.TetherInterfaceSM.this.setLastError(8);
                        Tethering.TetherInterfaceSM.this.transitionTo(Tethering.TetherInterfaceSM.this.mInitialState);
                        bool2 = true;
                        break label105;
                        i = 1;
                        cleanupUpstream();
                        try
                        {
                            Tethering.this.mNMService.untetherInterface(Tethering.TetherInterfaceSM.this.mIfaceName);
                            if (i == 0)
                                break label518;
                            Tethering.TetherInterfaceSM.this.setLastErrorAndTransitionToInitialState(5);
                        }
                        catch (Exception localException1)
                        {
                            Tethering.TetherInterfaceSM.this.setLastErrorAndTransitionToInitialState(7);
                        }
                        continue;
                        label518: Log.d("Tethering", "Tether lost upstream connection " + Tethering.TetherInterfaceSM.this.mIfaceName);
                        Tethering.this.sendTetherStateChangedBroadcast();
                        if ((Tethering.TetherInterfaceSM.this.mUsb) && (!Tethering.this.configureUsbIface(false)))
                            Tethering.TetherInterfaceSM.this.setLastError(10);
                        Tethering.TetherInterfaceSM.this.transitionTo(Tethering.TetherInterfaceSM.this.mInitialState);
                    }
                    catch (Exception localException3)
                    {
                        break label430;
                    }
                }
            }
        }

        class StartingState extends State
        {
            StartingState()
            {
            }

            public void enter()
            {
                Tethering.TetherInterfaceSM.this.setAvailable(false);
                if ((Tethering.TetherInterfaceSM.this.mUsb) && (!Tethering.this.configureUsbIface(true)))
                {
                    Tethering.this.mTetherMasterSM.sendMessage(2, Tethering.TetherInterfaceSM.this);
                    Tethering.TetherInterfaceSM.this.setLastError(10);
                    Tethering.TetherInterfaceSM.this.transitionTo(Tethering.TetherInterfaceSM.this.mInitialState);
                }
                while (true)
                {
                    return;
                    Tethering.this.sendTetherStateChangedBroadcast();
                    Tethering.TetherInterfaceSM.this.transitionTo(Tethering.TetherInterfaceSM.this.mTetheredState);
                }
            }

            public boolean processMessage(Message paramMessage)
            {
                Log.d("Tethering", "StartingState.processMessage what=" + paramMessage.what);
                boolean bool = true;
                switch (paramMessage.what)
                {
                case 5:
                default:
                    bool = false;
                case 3:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 4:
                }
                while (true)
                {
                    return bool;
                    Tethering.this.mTetherMasterSM.sendMessage(2, Tethering.TetherInterfaceSM.this);
                    if ((Tethering.TetherInterfaceSM.this.mUsb) && (!Tethering.this.configureUsbIface(false)))
                    {
                        Tethering.TetherInterfaceSM.this.setLastErrorAndTransitionToInitialState(10);
                    }
                    else
                    {
                        Tethering.TetherInterfaceSM.this.transitionTo(Tethering.TetherInterfaceSM.this.mInitialState);
                        continue;
                        Tethering.TetherInterfaceSM.this.setLastErrorAndTransitionToInitialState(5);
                        continue;
                        Tethering.this.mTetherMasterSM.sendMessage(2, Tethering.TetherInterfaceSM.this);
                        Tethering.TetherInterfaceSM.this.transitionTo(Tethering.TetherInterfaceSM.this.mUnavailableState);
                    }
                }
            }
        }

        class InitialState extends State
        {
            InitialState()
            {
            }

            public void enter()
            {
                Tethering.TetherInterfaceSM.this.setAvailable(true);
                Tethering.TetherInterfaceSM.this.setTethered(false);
                Tethering.this.sendTetherStateChangedBroadcast();
            }

            public boolean processMessage(Message paramMessage)
            {
                Log.d("Tethering", "InitialState.processMessage what=" + paramMessage.what);
                boolean bool = true;
                switch (paramMessage.what)
                {
                case 3:
                default:
                    bool = false;
                case 2:
                case 4:
                }
                while (true)
                {
                    return bool;
                    Tethering.TetherInterfaceSM.this.setLastError(0);
                    Tethering.this.mTetherMasterSM.sendMessage(1, Tethering.TetherInterfaceSM.this);
                    Tethering.TetherInterfaceSM.this.transitionTo(Tethering.TetherInterfaceSM.this.mStartingState);
                    continue;
                    Tethering.TetherInterfaceSM.this.transitionTo(Tethering.TetherInterfaceSM.this.mUnavailableState);
                }
            }
        }
    }

    private class StateReceiver extends BroadcastReceiver
    {
        private StateReceiver()
        {
        }

        public void onReceive(Context paramContext, Intent paramIntent)
        {
            String str = paramIntent.getAction();
            if (str.equals("android.hardware.usb.action.USB_STATE"))
                synchronized (Tethering.this.mPublicSync)
                {
                    boolean bool = paramIntent.getBooleanExtra("connected", false);
                    Tethering.access$202(Tethering.this, paramIntent.getBooleanExtra("rndis", false));
                    if ((bool) && (Tethering.this.mRndisEnabled) && (Tethering.this.mUsbTetherRequested))
                        Tethering.this.tetherUsb(true);
                    Tethering.access$302(Tethering.this, false);
                }
            if (str.equals("android.net.conn.CONNECTIVITY_CHANGE"))
                Tethering.this.mTetherMasterSM.sendMessage(3);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.connectivity.Tethering
 * JD-Core Version:        0.6.2
 */