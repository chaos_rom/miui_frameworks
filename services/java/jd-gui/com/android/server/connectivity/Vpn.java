package com.android.server.connectivity;

import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.INetworkManagementEventObserver.Stub;
import android.net.LocalSocket;
import android.os.Binder;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.os.SystemClock;
import android.util.Log;
import com.android.internal.net.LegacyVpnInfo;
import com.android.internal.net.VpnConfig;
import com.android.server.ConnectivityService.VpnCallback;

public class Vpn extends INetworkManagementEventObserver.Stub
{
    private static final String BIND_VPN_SERVICE = "android.permission.BIND_VPN_SERVICE";
    private static final String TAG = "Vpn";
    private final ConnectivityService.VpnCallback mCallback;
    private Connection mConnection;
    private final Context mContext;
    private String mInterface;
    private LegacyVpnRunner mLegacyVpnRunner;
    private String mPackage = "[Legacy VPN]";

    public Vpn(Context paramContext, ConnectivityService.VpnCallback paramVpnCallback)
    {
        this.mContext = paramContext;
        this.mCallback = paramVpnCallback;
    }

    private void enforceControlPermission()
    {
        if (Binder.getCallingUid() == 1000);
        while (true)
        {
            return;
            try
            {
                ApplicationInfo localApplicationInfo = this.mContext.getPackageManager().getApplicationInfo("com.android.vpndialogs", 0);
                int i = Binder.getCallingUid();
                int j = localApplicationInfo.uid;
                if (i == j)
                    continue;
                label40: throw new SecurityException("Unauthorized Caller");
            }
            catch (Exception localException)
            {
                break label40;
            }
        }
    }

    private void hideNotification()
    {
        NotificationManager localNotificationManager = (NotificationManager)this.mContext.getSystemService("notification");
        if (localNotificationManager != null)
            localNotificationManager.cancel(17303037);
    }

    private native int jniCheck(String paramString);

    private native int jniCreate(int paramInt);

    private native String jniGetName(int paramInt);

    private native void jniProtect(int paramInt, String paramString);

    private native void jniReset(String paramString);

    private native int jniSetAddresses(String paramString1, String paramString2);

    private native int jniSetRoutes(String paramString1, String paramString2);

    private void showNotification(VpnConfig paramVpnConfig, String paramString, Bitmap paramBitmap)
    {
        NotificationManager localNotificationManager = (NotificationManager)this.mContext.getSystemService("notification");
        String str1;
        if (localNotificationManager != null)
        {
            if (paramString != null)
                break label119;
            str1 = this.mContext.getString(17040507);
            if (paramVpnConfig.session != null)
                break label150;
        }
        label119: Context localContext2;
        label150: Object[] arrayOfObject2;
        for (String str2 = this.mContext.getString(17040509); ; str2 = localContext2.getString(17040510, arrayOfObject2))
        {
            paramVpnConfig.startTime = SystemClock.elapsedRealtime();
            localNotificationManager.notify(17303037, new Notification.Builder(this.mContext).setSmallIcon(17303037).setLargeIcon(paramBitmap).setContentTitle(str1).setContentText(str2).setContentIntent(VpnConfig.getIntentForStatusPanel(this.mContext, paramVpnConfig)).setDefaults(0).setOngoing(true).getNotification());
            return;
            Context localContext1 = this.mContext;
            Object[] arrayOfObject1 = new Object[1];
            arrayOfObject1[0] = paramString;
            str1 = localContext1.getString(17040508, arrayOfObject1);
            break;
            localContext2 = this.mContext;
            arrayOfObject2 = new Object[1];
            arrayOfObject2[0] = paramVpnConfig.session;
        }
    }

    /** @deprecated */
    public ParcelFileDescriptor establish(VpnConfig paramVpnConfig)
    {
        while (true)
        {
            PackageManager localPackageManager;
            ApplicationInfo localApplicationInfo;
            Intent localIntent;
            ResolveInfo localResolveInfo;
            try
            {
                localPackageManager = this.mContext.getPackageManager();
                try
                {
                    localApplicationInfo = localPackageManager.getApplicationInfo(this.mPackage, 0);
                    int i = Binder.getCallingUid();
                    int j = localApplicationInfo.uid;
                    if (i != j)
                    {
                        localObject2 = null;
                        return localObject2;
                    }
                }
                catch (Exception localException1)
                {
                    localObject2 = null;
                    continue;
                    localIntent = new Intent("android.net.VpnService");
                    localIntent.setClassName(this.mPackage, paramVpnConfig.user);
                    localResolveInfo = localPackageManager.resolveService(localIntent, 0);
                    if (localResolveInfo != null)
                        break label130;
                }
                throw new SecurityException("Cannot find " + paramVpnConfig.user);
            }
            finally
            {
            }
            label130: if (!"android.permission.BIND_VPN_SERVICE".equals(localResolveInfo.serviceInfo.permission))
                throw new SecurityException(paramVpnConfig.user + " does not require " + "android.permission.BIND_VPN_SERVICE");
            String str1 = localApplicationInfo.loadLabel(localPackageManager).toString();
            Drawable localDrawable = localApplicationInfo.loadIcon(localPackageManager);
            Bitmap localBitmap = null;
            if ((localDrawable.getIntrinsicWidth() > 0) && (localDrawable.getIntrinsicHeight() > 0))
            {
                int k = this.mContext.getResources().getDimensionPixelSize(17104901);
                int m = this.mContext.getResources().getDimensionPixelSize(17104902);
                localDrawable.setBounds(0, 0, k, m);
                localBitmap = Bitmap.createBitmap(k, m, Bitmap.Config.ARGB_8888);
                Canvas localCanvas = new Canvas(localBitmap);
                localDrawable.draw(localCanvas);
                localCanvas.setBitmap(null);
            }
            ParcelFileDescriptor localParcelFileDescriptor = ParcelFileDescriptor.adoptFd(jniCreate(paramVpnConfig.mtu));
            Object localObject2 = localParcelFileDescriptor;
            String str2;
            try
            {
                str2 = jniGetName(((ParcelFileDescriptor)localObject2).getFd());
                if (jniSetAddresses(str2, paramVpnConfig.addresses) < 1)
                    throw new IllegalArgumentException("At least one address must be specified");
            }
            catch (RuntimeException localRuntimeException)
            {
            }
            try
            {
                ((ParcelFileDescriptor)localObject2).close();
                label356: throw localRuntimeException;
                if (paramVpnConfig.routes != null)
                    jniSetRoutes(str2, paramVpnConfig.routes);
                Connection localConnection = new Connection(null);
                if (!this.mContext.bindService(localIntent, localConnection, 1))
                    throw new IllegalStateException("Cannot bind " + paramVpnConfig.user);
                if (this.mConnection != null)
                    this.mContext.unbindService(this.mConnection);
                if ((this.mInterface != null) && (!this.mInterface.equals(str2)))
                    jniReset(this.mInterface);
                this.mConnection = localConnection;
                this.mInterface = str2;
                Log.i("Vpn", "Established by " + paramVpnConfig.user + " on " + this.mInterface);
                paramVpnConfig.user = this.mPackage;
                paramVpnConfig.interfaze = this.mInterface;
                long l = Binder.clearCallingIdentity();
                this.mCallback.override(paramVpnConfig.dnsServers, paramVpnConfig.searchDomains);
                showNotification(paramVpnConfig, str1, localBitmap);
                Binder.restoreCallingIdentity(l);
            }
            catch (Exception localException2)
            {
                break label356;
            }
        }
    }

    /** @deprecated */
    public LegacyVpnInfo getLegacyVpnInfo()
    {
        try
        {
            enforceControlPermission();
            LegacyVpnRunner localLegacyVpnRunner = this.mLegacyVpnRunner;
            if (localLegacyVpnRunner == null);
            LegacyVpnInfo localLegacyVpnInfo;
            for (Object localObject2 = null; ; localObject2 = localLegacyVpnInfo)
            {
                return localObject2;
                localLegacyVpnInfo = this.mLegacyVpnRunner.getInfo();
            }
        }
        finally
        {
        }
    }

    public void interfaceAdded(String paramString)
    {
    }

    public void interfaceLinkStateChanged(String paramString, boolean paramBoolean)
    {
    }

    /** @deprecated */
    public void interfaceRemoved(String paramString)
    {
        try
        {
            if ((paramString.equals(this.mInterface)) && (jniCheck(paramString) == 0))
            {
                long l = Binder.clearCallingIdentity();
                this.mCallback.restore();
                hideNotification();
                Binder.restoreCallingIdentity(l);
                this.mInterface = null;
                if (this.mConnection == null)
                    break label71;
                this.mContext.unbindService(this.mConnection);
                this.mConnection = null;
            }
            while (true)
            {
                return;
                label71: if (this.mLegacyVpnRunner != null)
                {
                    this.mLegacyVpnRunner.exit();
                    this.mLegacyVpnRunner = null;
                }
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void interfaceStatusChanged(String paramString, boolean paramBoolean)
    {
        if (!paramBoolean);
        try
        {
            if (this.mLegacyVpnRunner != null)
                this.mLegacyVpnRunner.check(paramString);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void limitReached(String paramString1, String paramString2)
    {
    }

    /** @deprecated */
    // ERROR //
    public boolean prepare(String paramString1, String paramString2)
    {
        // Byte code:
        //     0: iconst_1
        //     1: istore_3
        //     2: aload_0
        //     3: monitorenter
        //     4: aload_1
        //     5: ifnull +24 -> 29
        //     8: aload_1
        //     9: aload_0
        //     10: getfield 38	com/android/server/connectivity/Vpn:mPackage	Ljava/lang/String;
        //     13: invokevirtual 248	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     16: istore 11
        //     18: iload 11
        //     20: ifne +9 -> 29
        //     23: iconst_0
        //     24: istore_3
        //     25: aload_0
        //     26: monitorexit
        //     27: iload_3
        //     28: ireturn
        //     29: aload_2
        //     30: ifnull -5 -> 25
        //     33: aload_2
        //     34: aload_0
        //     35: getfield 38	com/android/server/connectivity/Vpn:mPackage	Ljava/lang/String;
        //     38: invokevirtual 248	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     41: ifeq +12 -> 53
        //     44: aload_2
        //     45: ldc 36
        //     47: invokevirtual 248	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     50: ifeq -25 -> 25
        //     53: aload_0
        //     54: invokespecial 394	com/android/server/connectivity/Vpn:enforceControlPermission	()V
        //     57: aload_0
        //     58: getfield 46	com/android/server/connectivity/Vpn:mInterface	Ljava/lang/String;
        //     61: ifnull +37 -> 98
        //     64: aload_0
        //     65: aload_0
        //     66: getfield 46	com/android/server/connectivity/Vpn:mInterface	Ljava/lang/String;
        //     69: invokespecial 358	com/android/server/connectivity/Vpn:jniReset	(Ljava/lang/String;)V
        //     72: invokestatic 373	android/os/Binder:clearCallingIdentity	()J
        //     75: lstore 9
        //     77: aload_0
        //     78: getfield 42	com/android/server/connectivity/Vpn:mCallback	Lcom/android/server/ConnectivityService$VpnCallback;
        //     81: invokevirtual 406	com/android/server/ConnectivityService$VpnCallback:restore	()V
        //     84: aload_0
        //     85: invokespecial 408	com/android/server/connectivity/Vpn:hideNotification	()V
        //     88: lload 9
        //     90: invokestatic 390	android/os/Binder:restoreCallingIdentity	(J)V
        //     93: aload_0
        //     94: aconst_null
        //     95: putfield 46	com/android/server/connectivity/Vpn:mInterface	Ljava/lang/String;
        //     98: aload_0
        //     99: getfield 352	com/android/server/connectivity/Vpn:mConnection	Lcom/android/server/connectivity/Vpn$Connection;
        //     102: astore 5
        //     104: aload 5
        //     106: ifnull +94 -> 200
        //     109: aload_0
        //     110: getfield 352	com/android/server/connectivity/Vpn:mConnection	Lcom/android/server/connectivity/Vpn$Connection;
        //     113: invokestatic 423	com/android/server/connectivity/Vpn$Connection:access$000	(Lcom/android/server/connectivity/Vpn$Connection;)Landroid/os/IBinder;
        //     116: ldc_w 424
        //     119: invokestatic 430	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //     122: aconst_null
        //     123: iconst_1
        //     124: invokeinterface 436 5 0
        //     129: pop
        //     130: aload_0
        //     131: getfield 40	com/android/server/connectivity/Vpn:mContext	Landroid/content/Context;
        //     134: aload_0
        //     135: getfield 352	com/android/server/connectivity/Vpn:mConnection	Lcom/android/server/connectivity/Vpn$Connection;
        //     138: invokevirtual 356	android/content/Context:unbindService	(Landroid/content/ServiceConnection;)V
        //     141: aload_0
        //     142: aconst_null
        //     143: putfield 352	com/android/server/connectivity/Vpn:mConnection	Lcom/android/server/connectivity/Vpn$Connection;
        //     146: ldc 19
        //     148: new 220	java/lang/StringBuilder
        //     151: dup
        //     152: invokespecial 221	java/lang/StringBuilder:<init>	()V
        //     155: ldc_w 438
        //     158: invokevirtual 227	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     161: aload_0
        //     162: getfield 38	com/android/server/connectivity/Vpn:mPackage	Ljava/lang/String;
        //     165: invokevirtual 227	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     168: ldc_w 440
        //     171: invokevirtual 227	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     174: aload_2
        //     175: invokevirtual 227	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     178: invokevirtual 231	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     181: invokestatic 367	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     184: pop
        //     185: aload_0
        //     186: aload_2
        //     187: putfield 38	com/android/server/connectivity/Vpn:mPackage	Ljava/lang/String;
        //     190: goto -165 -> 25
        //     193: astore 4
        //     195: aload_0
        //     196: monitorexit
        //     197: aload 4
        //     199: athrow
        //     200: aload_0
        //     201: getfield 396	com/android/server/connectivity/Vpn:mLegacyVpnRunner	Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;
        //     204: ifnull -58 -> 146
        //     207: aload_0
        //     208: getfield 396	com/android/server/connectivity/Vpn:mLegacyVpnRunner	Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;
        //     211: invokevirtual 411	com/android/server/connectivity/Vpn$LegacyVpnRunner:exit	()V
        //     214: aload_0
        //     215: aconst_null
        //     216: putfield 396	com/android/server/connectivity/Vpn:mLegacyVpnRunner	Lcom/android/server/connectivity/Vpn$LegacyVpnRunner;
        //     219: goto -73 -> 146
        //     222: astore 7
        //     224: goto -94 -> 130
        //
        // Exception table:
        //     from	to	target	type
        //     8	18	193	finally
        //     33	104	193	finally
        //     109	130	193	finally
        //     130	190	193	finally
        //     200	219	193	finally
        //     109	130	222	java/lang/Exception
    }

    public void protect(ParcelFileDescriptor paramParcelFileDescriptor, String paramString)
        throws Exception
    {
        ApplicationInfo localApplicationInfo = this.mContext.getPackageManager().getApplicationInfo(this.mPackage, 0);
        if (Binder.getCallingUid() != localApplicationInfo.uid)
            throw new SecurityException("Unauthorized Caller");
        jniProtect(paramParcelFileDescriptor.getFd(), paramString);
    }

    /** @deprecated */
    public void startLegacyVpn(VpnConfig paramVpnConfig, String[] paramArrayOfString1, String[] paramArrayOfString2)
    {
        try
        {
            prepare(null, "[Legacy VPN]");
            this.mLegacyVpnRunner = new LegacyVpnRunner(paramVpnConfig, paramArrayOfString1, paramArrayOfString2);
            this.mLegacyVpnRunner.start();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private class LegacyVpnRunner extends Thread
    {
        private static final String TAG = "LegacyVpnRunner";
        private final String[][] mArguments;
        private final VpnConfig mConfig;
        private final String[] mDaemons;
        private final LegacyVpnInfo mInfo;
        private final String mOuterInterface;
        private final LocalSocket[] mSockets;
        private long mTimer = -1L;

        public LegacyVpnRunner(VpnConfig paramArrayOfString1, String[] paramArrayOfString2, String[] arg4)
        {
            super();
            this.mConfig = paramArrayOfString1;
            String[] arrayOfString = new String[2];
            arrayOfString[0] = "racoon";
            arrayOfString[1] = "mtpd";
            this.mDaemons = arrayOfString;
            String[][] arrayOfString; = new String[2][];
            arrayOfString;[0] = paramArrayOfString2;
            Object localObject;
            arrayOfString;[1] = localObject;
            this.mArguments = arrayOfString;;
            this.mSockets = new LocalSocket[this.mDaemons.length];
            this.mInfo = new LegacyVpnInfo();
            this.mOuterInterface = this.mConfig.interfaze;
            this.mInfo.key = this.mConfig.user;
            this.mConfig.user = "[Legacy VPN]";
        }

        private void checkpoint(boolean paramBoolean)
            throws InterruptedException
        {
            long l1 = 1L;
            long l2 = SystemClock.elapsedRealtime();
            if (this.mTimer == -1L)
            {
                this.mTimer = l2;
                Thread.sleep(l1);
            }
            while (true)
            {
                return;
                if (l2 - this.mTimer > 60000L)
                    break;
                if (paramBoolean)
                    l1 = 200L;
                Thread.sleep(l1);
            }
            this.mInfo.state = 4;
            throw new IllegalStateException("Time is up");
        }

        // ERROR //
        private void execute()
        {
            // Byte code:
            //     0: aload_0
            //     1: iconst_0
            //     2: invokespecial 107	com/android/server/connectivity/Vpn$LegacyVpnRunner:checkpoint	(Z)V
            //     5: aload_0
            //     6: getfield 60	com/android/server/connectivity/Vpn$LegacyVpnRunner:mInfo	Lcom/android/internal/net/LegacyVpnInfo;
            //     9: iconst_1
            //     10: putfield 97	com/android/internal/net/LegacyVpnInfo:state	I
            //     13: aload_0
            //     14: getfield 46	com/android/server/connectivity/Vpn$LegacyVpnRunner:mDaemons	[Ljava/lang/String;
            //     17: astore 11
            //     19: aload 11
            //     21: arraylength
            //     22: istore 12
            //     24: iconst_0
            //     25: istore 13
            //     27: iload 13
            //     29: iload 12
            //     31: if_icmpge +125 -> 156
            //     34: aload 11
            //     36: iload 13
            //     38: aaload
            //     39: astore 52
            //     41: new 109	java/lang/StringBuilder
            //     44: dup
            //     45: invokespecial 110	java/lang/StringBuilder:<init>	()V
            //     48: ldc 112
            //     50: invokevirtual 116	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     53: aload 52
            //     55: invokevirtual 116	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     58: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     61: astore 53
            //     63: ldc 122
            //     65: aload 53
            //     67: ldc 122
            //     69: invokestatic 128	android/os/SystemProperties:get	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
            //     72: invokevirtual 132	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     75: ifne +75 -> 150
            //     78: aload_0
            //     79: iconst_1
            //     80: invokespecial 107	com/android/server/connectivity/Vpn$LegacyVpnRunner:checkpoint	(Z)V
            //     83: goto -20 -> 63
            //     86: astore 5
            //     88: ldc 10
            //     90: ldc 134
            //     92: aload 5
            //     94: invokestatic 140	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     97: pop
            //     98: aload_0
            //     99: invokevirtual 143	com/android/server/connectivity/Vpn$LegacyVpnRunner:exit	()V
            //     102: aload_0
            //     103: getfield 60	com/android/server/connectivity/Vpn$LegacyVpnRunner:mInfo	Lcom/android/internal/net/LegacyVpnInfo;
            //     106: getfield 97	com/android/internal/net/LegacyVpnInfo:state	I
            //     109: iconst_1
            //     110: if_icmpne +1150 -> 1260
            //     113: aload_0
            //     114: getfield 46	com/android/server/connectivity/Vpn$LegacyVpnRunner:mDaemons	[Ljava/lang/String;
            //     117: astore 8
            //     119: aload 8
            //     121: arraylength
            //     122: istore 9
            //     124: iconst_0
            //     125: istore 10
            //     127: iload 10
            //     129: iload 9
            //     131: if_icmpge +1129 -> 1260
            //     134: ldc 145
            //     136: aload 8
            //     138: iload 10
            //     140: aaload
            //     141: invokestatic 149	android/os/SystemProperties:set	(Ljava/lang/String;Ljava/lang/String;)V
            //     144: iinc 10 1
            //     147: goto -20 -> 127
            //     150: iinc 13 1
            //     153: goto -126 -> 27
            //     156: new 151	java/io/File
            //     159: dup
            //     160: ldc 153
            //     162: invokespecial 154	java/io/File:<init>	(Ljava/lang/String;)V
            //     165: astore 14
            //     167: aload 14
            //     169: invokevirtual 158	java/io/File:delete	()Z
            //     172: pop
            //     173: aload 14
            //     175: invokevirtual 161	java/io/File:exists	()Z
            //     178: ifeq +57 -> 235
            //     181: new 99	java/lang/IllegalStateException
            //     184: dup
            //     185: ldc 163
            //     187: invokespecial 102	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
            //     190: athrow
            //     191: astore_1
            //     192: aload_0
            //     193: getfield 60	com/android/server/connectivity/Vpn$LegacyVpnRunner:mInfo	Lcom/android/internal/net/LegacyVpnInfo;
            //     196: getfield 97	com/android/internal/net/LegacyVpnInfo:state	I
            //     199: iconst_1
            //     200: if_icmpne +1023 -> 1223
            //     203: aload_0
            //     204: getfield 46	com/android/server/connectivity/Vpn$LegacyVpnRunner:mDaemons	[Ljava/lang/String;
            //     207: astore_2
            //     208: aload_2
            //     209: arraylength
            //     210: istore_3
            //     211: iconst_0
            //     212: istore 4
            //     214: iload 4
            //     216: iload_3
            //     217: if_icmpge +1006 -> 1223
            //     220: ldc 145
            //     222: aload_2
            //     223: iload 4
            //     225: aaload
            //     226: invokestatic 149	android/os/SystemProperties:set	(Ljava/lang/String;Ljava/lang/String;)V
            //     229: iinc 4 1
            //     232: goto -18 -> 214
            //     235: new 151	java/io/File
            //     238: dup
            //     239: ldc 165
            //     241: invokespecial 154	java/io/File:<init>	(Ljava/lang/String;)V
            //     244: invokevirtual 158	java/io/File:delete	()Z
            //     247: pop
            //     248: iconst_0
            //     249: istore 17
            //     251: aload_0
            //     252: getfield 49	com/android/server/connectivity/Vpn$LegacyVpnRunner:mArguments	[[Ljava/lang/String;
            //     255: astore 18
            //     257: aload 18
            //     259: arraylength
            //     260: istore 19
            //     262: iconst_0
            //     263: istore 20
            //     265: iload 20
            //     267: iload 19
            //     269: if_icmpge +23 -> 292
            //     272: aload 18
            //     274: iload 20
            //     276: aaload
            //     277: astore 51
            //     279: iload 17
            //     281: ifne +1076 -> 1357
            //     284: aload 51
            //     286: ifnull +1080 -> 1366
            //     289: goto +1068 -> 1357
            //     292: iload 17
            //     294: ifne +59 -> 353
            //     297: aload_0
            //     298: getfield 60	com/android/server/connectivity/Vpn$LegacyVpnRunner:mInfo	Lcom/android/internal/net/LegacyVpnInfo;
            //     301: iconst_0
            //     302: putfield 97	com/android/internal/net/LegacyVpnInfo:state	I
            //     305: aload_0
            //     306: getfield 60	com/android/server/connectivity/Vpn$LegacyVpnRunner:mInfo	Lcom/android/internal/net/LegacyVpnInfo;
            //     309: getfield 97	com/android/internal/net/LegacyVpnInfo:state	I
            //     312: iconst_1
            //     313: if_icmpne +982 -> 1295
            //     316: aload_0
            //     317: getfield 46	com/android/server/connectivity/Vpn$LegacyVpnRunner:mDaemons	[Ljava/lang/String;
            //     320: astore 48
            //     322: aload 48
            //     324: arraylength
            //     325: istore 49
            //     327: iconst_0
            //     328: istore 50
            //     330: iload 50
            //     332: iload 49
            //     334: if_icmpge +961 -> 1295
            //     337: ldc 145
            //     339: aload 48
            //     341: iload 50
            //     343: aaload
            //     344: invokestatic 149	android/os/SystemProperties:set	(Ljava/lang/String;Ljava/lang/String;)V
            //     347: iinc 50 1
            //     350: goto -20 -> 330
            //     353: aload_0
            //     354: getfield 60	com/android/server/connectivity/Vpn$LegacyVpnRunner:mInfo	Lcom/android/internal/net/LegacyVpnInfo;
            //     357: iconst_2
            //     358: putfield 97	com/android/internal/net/LegacyVpnInfo:state	I
            //     361: iconst_0
            //     362: istore 21
            //     364: iload 21
            //     366: aload_0
            //     367: getfield 46	com/android/server/connectivity/Vpn$LegacyVpnRunner:mDaemons	[Ljava/lang/String;
            //     370: arraylength
            //     371: if_icmpge +297 -> 668
            //     374: aload_0
            //     375: getfield 49	com/android/server/connectivity/Vpn$LegacyVpnRunner:mArguments	[[Ljava/lang/String;
            //     378: iload 21
            //     380: aaload
            //     381: astore 36
            //     383: aload 36
            //     385: ifnonnull +6 -> 391
            //     388: goto +984 -> 1372
            //     391: aload_0
            //     392: getfield 46	com/android/server/connectivity/Vpn$LegacyVpnRunner:mDaemons	[Ljava/lang/String;
            //     395: iload 21
            //     397: aaload
            //     398: astore 37
            //     400: ldc 167
            //     402: aload 37
            //     404: invokestatic 149	android/os/SystemProperties:set	(Ljava/lang/String;Ljava/lang/String;)V
            //     407: new 109	java/lang/StringBuilder
            //     410: dup
            //     411: invokespecial 110	java/lang/StringBuilder:<init>	()V
            //     414: ldc 112
            //     416: invokevirtual 116	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     419: aload 37
            //     421: invokevirtual 116	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     424: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     427: astore 38
            //     429: ldc 169
            //     431: aload 38
            //     433: invokestatic 172	android/os/SystemProperties:get	(Ljava/lang/String;)Ljava/lang/String;
            //     436: invokevirtual 132	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     439: ifne +11 -> 450
            //     442: aload_0
            //     443: iconst_1
            //     444: invokespecial 107	com/android/server/connectivity/Vpn$LegacyVpnRunner:checkpoint	(Z)V
            //     447: goto -18 -> 429
            //     450: aload_0
            //     451: getfield 53	com/android/server/connectivity/Vpn$LegacyVpnRunner:mSockets	[Landroid/net/LocalSocket;
            //     454: iload 21
            //     456: new 51	android/net/LocalSocket
            //     459: dup
            //     460: invokespecial 173	android/net/LocalSocket:<init>	()V
            //     463: aastore
            //     464: new 175	android/net/LocalSocketAddress
            //     467: dup
            //     468: aload 37
            //     470: getstatic 181	android/net/LocalSocketAddress$Namespace:RESERVED	Landroid/net/LocalSocketAddress$Namespace;
            //     473: invokespecial 184	android/net/LocalSocketAddress:<init>	(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V
            //     476: astore 39
            //     478: aload_0
            //     479: getfield 53	com/android/server/connectivity/Vpn$LegacyVpnRunner:mSockets	[Landroid/net/LocalSocket;
            //     482: iload 21
            //     484: aaload
            //     485: aload 39
            //     487: invokevirtual 188	android/net/LocalSocket:connect	(Landroid/net/LocalSocketAddress;)V
            //     490: aload_0
            //     491: getfield 53	com/android/server/connectivity/Vpn$LegacyVpnRunner:mSockets	[Landroid/net/LocalSocket;
            //     494: iload 21
            //     496: aaload
            //     497: sipush 500
            //     500: invokevirtual 192	android/net/LocalSocket:setSoTimeout	(I)V
            //     503: aload_0
            //     504: getfield 53	com/android/server/connectivity/Vpn$LegacyVpnRunner:mSockets	[Landroid/net/LocalSocket;
            //     507: iload 21
            //     509: aaload
            //     510: invokevirtual 196	android/net/LocalSocket:getOutputStream	()Ljava/io/OutputStream;
            //     513: astore 41
            //     515: aload 36
            //     517: arraylength
            //     518: istore 42
            //     520: iconst_0
            //     521: istore 43
            //     523: iload 43
            //     525: iload 42
            //     527: if_icmpge +81 -> 608
            //     530: aload 36
            //     532: iload 43
            //     534: aaload
            //     535: getstatic 202	java/nio/charset/Charsets:UTF_8	Ljava/nio/charset/Charset;
            //     538: invokevirtual 206	java/lang/String:getBytes	(Ljava/nio/charset/Charset;)[B
            //     541: astore 47
            //     543: aload 47
            //     545: arraylength
            //     546: ldc 207
            //     548: if_icmplt +23 -> 571
            //     551: new 209	java/lang/IllegalArgumentException
            //     554: dup
            //     555: ldc 211
            //     557: invokespecial 212	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
            //     560: athrow
            //     561: astore 40
            //     563: aload_0
            //     564: iconst_1
            //     565: invokespecial 107	com/android/server/connectivity/Vpn$LegacyVpnRunner:checkpoint	(Z)V
            //     568: goto -90 -> 478
            //     571: aload 41
            //     573: aload 47
            //     575: arraylength
            //     576: bipush 8
            //     578: ishr
            //     579: invokevirtual 217	java/io/OutputStream:write	(I)V
            //     582: aload 41
            //     584: aload 47
            //     586: arraylength
            //     587: invokevirtual 217	java/io/OutputStream:write	(I)V
            //     590: aload 41
            //     592: aload 47
            //     594: invokevirtual 220	java/io/OutputStream:write	([B)V
            //     597: aload_0
            //     598: iconst_0
            //     599: invokespecial 107	com/android/server/connectivity/Vpn$LegacyVpnRunner:checkpoint	(Z)V
            //     602: iinc 43 1
            //     605: goto -82 -> 523
            //     608: aload 41
            //     610: sipush 255
            //     613: invokevirtual 217	java/io/OutputStream:write	(I)V
            //     616: aload 41
            //     618: sipush 255
            //     621: invokevirtual 217	java/io/OutputStream:write	(I)V
            //     624: aload 41
            //     626: invokevirtual 223	java/io/OutputStream:flush	()V
            //     629: aload_0
            //     630: getfield 53	com/android/server/connectivity/Vpn$LegacyVpnRunner:mSockets	[Landroid/net/LocalSocket;
            //     633: iload 21
            //     635: aaload
            //     636: invokevirtual 227	android/net/LocalSocket:getInputStream	()Ljava/io/InputStream;
            //     639: astore 44
            //     641: aload 44
            //     643: invokevirtual 233	java/io/InputStream:read	()I
            //     646: istore 46
            //     648: iload 46
            //     650: bipush 255
            //     652: if_icmpeq +720 -> 1372
            //     655: aload_0
            //     656: iconst_1
            //     657: invokespecial 107	com/android/server/connectivity/Vpn$LegacyVpnRunner:checkpoint	(Z)V
            //     660: goto -19 -> 641
            //     663: aload_0
            //     664: iconst_1
            //     665: invokespecial 107	com/android/server/connectivity/Vpn$LegacyVpnRunner:checkpoint	(Z)V
            //     668: aload 14
            //     670: invokevirtual 161	java/io/File:exists	()Z
            //     673: ifne +94 -> 767
            //     676: iconst_0
            //     677: istore 34
            //     679: iload 34
            //     681: aload_0
            //     682: getfield 46	com/android/server/connectivity/Vpn$LegacyVpnRunner:mDaemons	[Ljava/lang/String;
            //     685: arraylength
            //     686: if_icmpge -23 -> 663
            //     689: aload_0
            //     690: getfield 46	com/android/server/connectivity/Vpn$LegacyVpnRunner:mDaemons	[Ljava/lang/String;
            //     693: iload 34
            //     695: aaload
            //     696: astore 35
            //     698: aload_0
            //     699: getfield 49	com/android/server/connectivity/Vpn$LegacyVpnRunner:mArguments	[[Ljava/lang/String;
            //     702: iload 34
            //     704: aaload
            //     705: ifnull +673 -> 1378
            //     708: ldc 169
            //     710: new 109	java/lang/StringBuilder
            //     713: dup
            //     714: invokespecial 110	java/lang/StringBuilder:<init>	()V
            //     717: ldc 112
            //     719: invokevirtual 116	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     722: aload 35
            //     724: invokevirtual 116	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     727: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     730: invokestatic 172	android/os/SystemProperties:get	(Ljava/lang/String;)Ljava/lang/String;
            //     733: invokevirtual 132	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     736: ifne +642 -> 1378
            //     739: new 99	java/lang/IllegalStateException
            //     742: dup
            //     743: new 109	java/lang/StringBuilder
            //     746: dup
            //     747: invokespecial 110	java/lang/StringBuilder:<init>	()V
            //     750: aload 35
            //     752: invokevirtual 116	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     755: ldc 235
            //     757: invokevirtual 116	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     760: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     763: invokespecial 102	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
            //     766: athrow
            //     767: aload 14
            //     769: iconst_0
            //     770: aconst_null
            //     771: invokestatic 241	android/os/FileUtils:readTextFile	(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;
            //     774: ldc 243
            //     776: bipush 255
            //     778: invokevirtual 247	java/lang/String:split	(Ljava/lang/String;I)[Ljava/lang/String;
            //     781: astore 22
            //     783: aload 22
            //     785: arraylength
            //     786: bipush 6
            //     788: if_icmpeq +13 -> 801
            //     791: new 99	java/lang/IllegalStateException
            //     794: dup
            //     795: ldc 249
            //     797: invokespecial 102	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
            //     800: athrow
            //     801: aload_0
            //     802: getfield 38	com/android/server/connectivity/Vpn$LegacyVpnRunner:mConfig	Lcom/android/internal/net/VpnConfig;
            //     805: aload 22
            //     807: iconst_0
            //     808: aaload
            //     809: invokevirtual 252	java/lang/String:trim	()Ljava/lang/String;
            //     812: putfield 65	com/android/internal/net/VpnConfig:interfaze	Ljava/lang/String;
            //     815: aload_0
            //     816: getfield 38	com/android/server/connectivity/Vpn$LegacyVpnRunner:mConfig	Lcom/android/internal/net/VpnConfig;
            //     819: aload 22
            //     821: iconst_1
            //     822: aaload
            //     823: invokevirtual 252	java/lang/String:trim	()Ljava/lang/String;
            //     826: putfield 255	com/android/internal/net/VpnConfig:addresses	Ljava/lang/String;
            //     829: aload_0
            //     830: getfield 38	com/android/server/connectivity/Vpn$LegacyVpnRunner:mConfig	Lcom/android/internal/net/VpnConfig;
            //     833: getfield 258	com/android/internal/net/VpnConfig:routes	Ljava/lang/String;
            //     836: ifnull +16 -> 852
            //     839: aload_0
            //     840: getfield 38	com/android/server/connectivity/Vpn$LegacyVpnRunner:mConfig	Lcom/android/internal/net/VpnConfig;
            //     843: getfield 258	com/android/internal/net/VpnConfig:routes	Ljava/lang/String;
            //     846: invokevirtual 261	java/lang/String:isEmpty	()Z
            //     849: ifeq +17 -> 866
            //     852: aload_0
            //     853: getfield 38	com/android/server/connectivity/Vpn$LegacyVpnRunner:mConfig	Lcom/android/internal/net/VpnConfig;
            //     856: aload 22
            //     858: iconst_2
            //     859: aaload
            //     860: invokevirtual 252	java/lang/String:trim	()Ljava/lang/String;
            //     863: putfield 258	com/android/internal/net/VpnConfig:routes	Ljava/lang/String;
            //     866: aload_0
            //     867: getfield 38	com/android/server/connectivity/Vpn$LegacyVpnRunner:mConfig	Lcom/android/internal/net/VpnConfig;
            //     870: getfield 265	com/android/internal/net/VpnConfig:dnsServers	Ljava/util/List;
            //     873: ifnull +18 -> 891
            //     876: aload_0
            //     877: getfield 38	com/android/server/connectivity/Vpn$LegacyVpnRunner:mConfig	Lcom/android/internal/net/VpnConfig;
            //     880: getfield 265	com/android/internal/net/VpnConfig:dnsServers	Ljava/util/List;
            //     883: invokeinterface 270 1 0
            //     888: ifne +38 -> 926
            //     891: aload 22
            //     893: iconst_3
            //     894: aaload
            //     895: invokevirtual 252	java/lang/String:trim	()Ljava/lang/String;
            //     898: astore 23
            //     900: aload 23
            //     902: invokevirtual 261	java/lang/String:isEmpty	()Z
            //     905: ifne +21 -> 926
            //     908: aload_0
            //     909: getfield 38	com/android/server/connectivity/Vpn$LegacyVpnRunner:mConfig	Lcom/android/internal/net/VpnConfig;
            //     912: aload 23
            //     914: ldc_w 272
            //     917: invokevirtual 275	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
            //     920: invokestatic 281	java/util/Arrays:asList	([Ljava/lang/Object;)Ljava/util/List;
            //     923: putfield 265	com/android/internal/net/VpnConfig:dnsServers	Ljava/util/List;
            //     926: aload_0
            //     927: getfield 38	com/android/server/connectivity/Vpn$LegacyVpnRunner:mConfig	Lcom/android/internal/net/VpnConfig;
            //     930: getfield 284	com/android/internal/net/VpnConfig:searchDomains	Ljava/util/List;
            //     933: ifnull +18 -> 951
            //     936: aload_0
            //     937: getfield 38	com/android/server/connectivity/Vpn$LegacyVpnRunner:mConfig	Lcom/android/internal/net/VpnConfig;
            //     940: getfield 284	com/android/internal/net/VpnConfig:searchDomains	Ljava/util/List;
            //     943: invokeinterface 270 1 0
            //     948: ifne +38 -> 986
            //     951: aload 22
            //     953: iconst_4
            //     954: aaload
            //     955: invokevirtual 252	java/lang/String:trim	()Ljava/lang/String;
            //     958: astore 24
            //     960: aload 24
            //     962: invokevirtual 261	java/lang/String:isEmpty	()Z
            //     965: ifne +21 -> 986
            //     968: aload_0
            //     969: getfield 38	com/android/server/connectivity/Vpn$LegacyVpnRunner:mConfig	Lcom/android/internal/net/VpnConfig;
            //     972: aload 24
            //     974: ldc_w 272
            //     977: invokevirtual 275	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
            //     980: invokestatic 281	java/util/Arrays:asList	([Ljava/lang/Object;)Ljava/util/List;
            //     983: putfield 284	com/android/internal/net/VpnConfig:searchDomains	Ljava/util/List;
            //     986: aload_0
            //     987: getfield 29	com/android/server/connectivity/Vpn$LegacyVpnRunner:this$0	Lcom/android/server/connectivity/Vpn;
            //     990: aload_0
            //     991: getfield 38	com/android/server/connectivity/Vpn$LegacyVpnRunner:mConfig	Lcom/android/internal/net/VpnConfig;
            //     994: getfield 65	com/android/internal/net/VpnConfig:interfaze	Ljava/lang/String;
            //     997: aload_0
            //     998: getfield 38	com/android/server/connectivity/Vpn$LegacyVpnRunner:mConfig	Lcom/android/internal/net/VpnConfig;
            //     1001: getfield 258	com/android/internal/net/VpnConfig:routes	Ljava/lang/String;
            //     1004: invokestatic 288	com/android/server/connectivity/Vpn:access$300	(Lcom/android/server/connectivity/Vpn;Ljava/lang/String;Ljava/lang/String;)I
            //     1007: pop
            //     1008: aload_0
            //     1009: getfield 29	com/android/server/connectivity/Vpn$LegacyVpnRunner:this$0	Lcom/android/server/connectivity/Vpn;
            //     1012: astore 26
            //     1014: aload 26
            //     1016: monitorenter
            //     1017: aload_0
            //     1018: iconst_0
            //     1019: invokespecial 107	com/android/server/connectivity/Vpn$LegacyVpnRunner:checkpoint	(Z)V
            //     1022: aload_0
            //     1023: getfield 29	com/android/server/connectivity/Vpn$LegacyVpnRunner:this$0	Lcom/android/server/connectivity/Vpn;
            //     1026: aload_0
            //     1027: getfield 38	com/android/server/connectivity/Vpn$LegacyVpnRunner:mConfig	Lcom/android/internal/net/VpnConfig;
            //     1030: getfield 65	com/android/internal/net/VpnConfig:interfaze	Ljava/lang/String;
            //     1033: invokestatic 292	com/android/server/connectivity/Vpn:access$400	(Lcom/android/server/connectivity/Vpn;Ljava/lang/String;)I
            //     1036: ifne +49 -> 1085
            //     1039: new 99	java/lang/IllegalStateException
            //     1042: dup
            //     1043: new 109	java/lang/StringBuilder
            //     1046: dup
            //     1047: invokespecial 110	java/lang/StringBuilder:<init>	()V
            //     1050: aload_0
            //     1051: getfield 38	com/android/server/connectivity/Vpn$LegacyVpnRunner:mConfig	Lcom/android/internal/net/VpnConfig;
            //     1054: getfield 65	com/android/internal/net/VpnConfig:interfaze	Ljava/lang/String;
            //     1057: invokevirtual 116	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     1060: ldc_w 294
            //     1063: invokevirtual 116	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     1066: invokevirtual 120	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     1069: invokespecial 102	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
            //     1072: astore 28
            //     1074: aload 28
            //     1076: athrow
            //     1077: astore 27
            //     1079: aload 26
            //     1081: monitorexit
            //     1082: aload 27
            //     1084: athrow
            //     1085: aload_0
            //     1086: getfield 29	com/android/server/connectivity/Vpn$LegacyVpnRunner:this$0	Lcom/android/server/connectivity/Vpn;
            //     1089: aload_0
            //     1090: getfield 38	com/android/server/connectivity/Vpn$LegacyVpnRunner:mConfig	Lcom/android/internal/net/VpnConfig;
            //     1093: getfield 65	com/android/internal/net/VpnConfig:interfaze	Ljava/lang/String;
            //     1096: invokestatic 298	com/android/server/connectivity/Vpn:access$202	(Lcom/android/server/connectivity/Vpn;Ljava/lang/String;)Ljava/lang/String;
            //     1099: pop
            //     1100: aload_0
            //     1101: getfield 29	com/android/server/connectivity/Vpn$LegacyVpnRunner:this$0	Lcom/android/server/connectivity/Vpn;
            //     1104: invokestatic 302	com/android/server/connectivity/Vpn:access$500	(Lcom/android/server/connectivity/Vpn;)Lcom/android/server/ConnectivityService$VpnCallback;
            //     1107: aload_0
            //     1108: getfield 38	com/android/server/connectivity/Vpn$LegacyVpnRunner:mConfig	Lcom/android/internal/net/VpnConfig;
            //     1111: getfield 265	com/android/internal/net/VpnConfig:dnsServers	Ljava/util/List;
            //     1114: aload_0
            //     1115: getfield 38	com/android/server/connectivity/Vpn$LegacyVpnRunner:mConfig	Lcom/android/internal/net/VpnConfig;
            //     1118: getfield 284	com/android/internal/net/VpnConfig:searchDomains	Ljava/util/List;
            //     1121: invokevirtual 308	com/android/server/ConnectivityService$VpnCallback:override	(Ljava/util/List;Ljava/util/List;)V
            //     1124: aload_0
            //     1125: getfield 29	com/android/server/connectivity/Vpn$LegacyVpnRunner:this$0	Lcom/android/server/connectivity/Vpn;
            //     1128: aload_0
            //     1129: getfield 38	com/android/server/connectivity/Vpn$LegacyVpnRunner:mConfig	Lcom/android/internal/net/VpnConfig;
            //     1132: aconst_null
            //     1133: aconst_null
            //     1134: invokestatic 312	com/android/server/connectivity/Vpn:access$600	(Lcom/android/server/connectivity/Vpn;Lcom/android/internal/net/VpnConfig;Ljava/lang/String;Landroid/graphics/Bitmap;)V
            //     1137: ldc 10
            //     1139: ldc_w 314
            //     1142: invokestatic 317	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
            //     1145: pop
            //     1146: aload_0
            //     1147: getfield 60	com/android/server/connectivity/Vpn$LegacyVpnRunner:mInfo	Lcom/android/internal/net/LegacyVpnInfo;
            //     1150: iconst_3
            //     1151: putfield 97	com/android/internal/net/LegacyVpnInfo:state	I
            //     1154: aload_0
            //     1155: getfield 60	com/android/server/connectivity/Vpn$LegacyVpnRunner:mInfo	Lcom/android/internal/net/LegacyVpnInfo;
            //     1158: aload_0
            //     1159: getfield 29	com/android/server/connectivity/Vpn$LegacyVpnRunner:this$0	Lcom/android/server/connectivity/Vpn;
            //     1162: invokestatic 321	com/android/server/connectivity/Vpn:access$700	(Lcom/android/server/connectivity/Vpn;)Landroid/content/Context;
            //     1165: aconst_null
            //     1166: invokestatic 325	com/android/internal/net/VpnConfig:getIntentForStatusPanel	(Landroid/content/Context;Lcom/android/internal/net/VpnConfig;)Landroid/app/PendingIntent;
            //     1169: putfield 329	com/android/internal/net/LegacyVpnInfo:intent	Landroid/app/PendingIntent;
            //     1172: aload 26
            //     1174: monitorexit
            //     1175: aload_0
            //     1176: getfield 60	com/android/server/connectivity/Vpn$LegacyVpnRunner:mInfo	Lcom/android/internal/net/LegacyVpnInfo;
            //     1179: getfield 97	com/android/internal/net/LegacyVpnInfo:state	I
            //     1182: iconst_1
            //     1183: if_icmpne +143 -> 1326
            //     1186: aload_0
            //     1187: getfield 46	com/android/server/connectivity/Vpn$LegacyVpnRunner:mDaemons	[Ljava/lang/String;
            //     1190: astore 31
            //     1192: aload 31
            //     1194: arraylength
            //     1195: istore 32
            //     1197: iconst_0
            //     1198: istore 33
            //     1200: iload 33
            //     1202: iload 32
            //     1204: if_icmpge +122 -> 1326
            //     1207: ldc 145
            //     1209: aload 31
            //     1211: iload 33
            //     1213: aaload
            //     1214: invokestatic 149	android/os/SystemProperties:set	(Ljava/lang/String;Ljava/lang/String;)V
            //     1217: iinc 33 1
            //     1220: goto -20 -> 1200
            //     1223: aload_0
            //     1224: getfield 60	com/android/server/connectivity/Vpn$LegacyVpnRunner:mInfo	Lcom/android/internal/net/LegacyVpnInfo;
            //     1227: getfield 97	com/android/internal/net/LegacyVpnInfo:state	I
            //     1230: iconst_1
            //     1231: if_icmpeq +14 -> 1245
            //     1234: aload_0
            //     1235: getfield 60	com/android/server/connectivity/Vpn$LegacyVpnRunner:mInfo	Lcom/android/internal/net/LegacyVpnInfo;
            //     1238: getfield 97	com/android/internal/net/LegacyVpnInfo:state	I
            //     1241: iconst_2
            //     1242: if_icmpne +11 -> 1253
            //     1245: aload_0
            //     1246: getfield 60	com/android/server/connectivity/Vpn$LegacyVpnRunner:mInfo	Lcom/android/internal/net/LegacyVpnInfo;
            //     1249: iconst_5
            //     1250: putfield 97	com/android/internal/net/LegacyVpnInfo:state	I
            //     1253: aload_1
            //     1254: athrow
            //     1255: astore 45
            //     1257: goto -602 -> 655
            //     1260: aload_0
            //     1261: getfield 60	com/android/server/connectivity/Vpn$LegacyVpnRunner:mInfo	Lcom/android/internal/net/LegacyVpnInfo;
            //     1264: getfield 97	com/android/internal/net/LegacyVpnInfo:state	I
            //     1267: iconst_1
            //     1268: if_icmpeq +14 -> 1282
            //     1271: aload_0
            //     1272: getfield 60	com/android/server/connectivity/Vpn$LegacyVpnRunner:mInfo	Lcom/android/internal/net/LegacyVpnInfo;
            //     1275: getfield 97	com/android/internal/net/LegacyVpnInfo:state	I
            //     1278: iconst_2
            //     1279: if_icmpne +15 -> 1294
            //     1282: aload_0
            //     1283: getfield 60	com/android/server/connectivity/Vpn$LegacyVpnRunner:mInfo	Lcom/android/internal/net/LegacyVpnInfo;
            //     1286: astore 7
            //     1288: aload 7
            //     1290: iconst_5
            //     1291: putfield 97	com/android/internal/net/LegacyVpnInfo:state	I
            //     1294: return
            //     1295: aload_0
            //     1296: getfield 60	com/android/server/connectivity/Vpn$LegacyVpnRunner:mInfo	Lcom/android/internal/net/LegacyVpnInfo;
            //     1299: getfield 97	com/android/internal/net/LegacyVpnInfo:state	I
            //     1302: iconst_1
            //     1303: if_icmpeq +14 -> 1317
            //     1306: aload_0
            //     1307: getfield 60	com/android/server/connectivity/Vpn$LegacyVpnRunner:mInfo	Lcom/android/internal/net/LegacyVpnInfo;
            //     1310: getfield 97	com/android/internal/net/LegacyVpnInfo:state	I
            //     1313: iconst_2
            //     1314: if_icmpne -20 -> 1294
            //     1317: aload_0
            //     1318: getfield 60	com/android/server/connectivity/Vpn$LegacyVpnRunner:mInfo	Lcom/android/internal/net/LegacyVpnInfo;
            //     1321: astore 7
            //     1323: goto -35 -> 1288
            //     1326: aload_0
            //     1327: getfield 60	com/android/server/connectivity/Vpn$LegacyVpnRunner:mInfo	Lcom/android/internal/net/LegacyVpnInfo;
            //     1330: getfield 97	com/android/internal/net/LegacyVpnInfo:state	I
            //     1333: iconst_1
            //     1334: if_icmpeq +14 -> 1348
            //     1337: aload_0
            //     1338: getfield 60	com/android/server/connectivity/Vpn$LegacyVpnRunner:mInfo	Lcom/android/internal/net/LegacyVpnInfo;
            //     1341: getfield 97	com/android/internal/net/LegacyVpnInfo:state	I
            //     1344: iconst_2
            //     1345: if_icmpne -51 -> 1294
            //     1348: aload_0
            //     1349: getfield 60	com/android/server/connectivity/Vpn$LegacyVpnRunner:mInfo	Lcom/android/internal/net/LegacyVpnInfo;
            //     1352: astore 7
            //     1354: goto -66 -> 1288
            //     1357: iconst_1
            //     1358: istore 17
            //     1360: iinc 20 1
            //     1363: goto -1098 -> 265
            //     1366: iconst_0
            //     1367: istore 17
            //     1369: goto -9 -> 1360
            //     1372: iinc 21 1
            //     1375: goto -1011 -> 364
            //     1378: iinc 34 1
            //     1381: goto -702 -> 679
            //
            // Exception table:
            //     from	to	target	type
            //     0	83	86	java/lang/Exception
            //     156	191	86	java/lang/Exception
            //     235	305	86	java/lang/Exception
            //     353	478	86	java/lang/Exception
            //     490	641	86	java/lang/Exception
            //     655	1017	86	java/lang/Exception
            //     1082	1085	86	java/lang/Exception
            //     0	83	191	finally
            //     88	102	191	finally
            //     156	191	191	finally
            //     235	305	191	finally
            //     353	478	191	finally
            //     478	490	191	finally
            //     490	641	191	finally
            //     641	648	191	finally
            //     655	1017	191	finally
            //     1082	1085	191	finally
            //     478	490	561	java/lang/Exception
            //     1017	1082	1077	finally
            //     1085	1175	1077	finally
            //     641	648	1255	java/lang/Exception
        }

        public void check(String paramString)
        {
            if (paramString.equals(this.mOuterInterface))
            {
                Log.i("LegacyVpnRunner", "Legacy VPN is going down with " + paramString);
                exit();
            }
        }

        public void exit()
        {
            interrupt();
            LocalSocket[] arrayOfLocalSocket = this.mSockets;
            int i = arrayOfLocalSocket.length;
            int j = 0;
            while (true)
                if (j < i)
                {
                    LocalSocket localLocalSocket = arrayOfLocalSocket[j];
                    try
                    {
                        localLocalSocket.close();
                        label29: j++;
                    }
                    catch (Exception localException)
                    {
                        break label29;
                    }
                }
        }

        public LegacyVpnInfo getInfo()
        {
            if ((this.mInfo.state == 3) && (Vpn.this.mInterface == null))
            {
                this.mInfo.state = 0;
                this.mInfo.intent = null;
            }
            return this.mInfo;
        }

        public void run()
        {
            Log.v("LegacyVpnRunner", "Waiting");
            try
            {
                Log.v("LegacyVpnRunner", "Executing");
                execute();
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }
    }

    private class Connection
        implements ServiceConnection
    {
        private IBinder mService;

        private Connection()
        {
        }

        public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
        {
            this.mService = paramIBinder;
        }

        public void onServiceDisconnected(ComponentName paramComponentName)
        {
            this.mService = null;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.connectivity.Vpn
 * JD-Core Version:        0.6.2
 */