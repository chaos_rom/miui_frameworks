package com.android.server;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ComponentInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Binder;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.util.Slog;
import com.android.internal.content.PackageMonitor;
import java.util.List;

public class RecognitionManagerService extends Binder
{
    static final String TAG = "RecognitionManagerService";
    final Context mContext;
    final MyPackageMonitor mMonitor;

    RecognitionManagerService(Context paramContext)
    {
        this.mContext = paramContext;
        this.mMonitor = new MyPackageMonitor();
        this.mMonitor.register(paramContext, null, true);
    }

    ComponentName findAvailRecognizer(String paramString)
    {
        List localList = this.mContext.getPackageManager().queryIntentServices(new Intent("android.speech.RecognitionService"), 0);
        int i = localList.size();
        ComponentName localComponentName;
        if (i == 0)
        {
            Slog.w("RecognitionManagerService", "no available voice recognition services found");
            localComponentName = null;
        }
        while (true)
        {
            return localComponentName;
            if (paramString != null)
                for (int j = 0; ; j++)
                {
                    if (j >= i)
                        break label115;
                    ServiceInfo localServiceInfo2 = ((ResolveInfo)localList.get(j)).serviceInfo;
                    if (paramString.equals(localServiceInfo2.packageName))
                    {
                        localComponentName = new ComponentName(localServiceInfo2.packageName, localServiceInfo2.name);
                        break;
                    }
                }
            label115: if (i > 1)
                Slog.w("RecognitionManagerService", "more than one voice recognition service found, picking first");
            ServiceInfo localServiceInfo1 = ((ResolveInfo)localList.get(0)).serviceInfo;
            localComponentName = new ComponentName(localServiceInfo1.packageName, localServiceInfo1.name);
        }
    }

    ComponentName getCurRecognizer()
    {
        String str = Settings.Secure.getString(this.mContext.getContentResolver(), "voice_recognition_service");
        if (TextUtils.isEmpty(str));
        for (ComponentName localComponentName = null; ; localComponentName = ComponentName.unflattenFromString(str))
            return localComponentName;
    }

    void setCurRecognizer(ComponentName paramComponentName)
    {
        ContentResolver localContentResolver = this.mContext.getContentResolver();
        if (paramComponentName != null);
        for (String str = paramComponentName.flattenToShortString(); ; str = "")
        {
            Settings.Secure.putString(localContentResolver, "voice_recognition_service", str);
            return;
        }
    }

    public void systemReady()
    {
        ComponentName localComponentName1 = getCurRecognizer();
        if (localComponentName1 != null);
        while (true)
        {
            try
            {
                this.mContext.getPackageManager().getServiceInfo(localComponentName1, 0);
                return;
            }
            catch (PackageManager.NameNotFoundException localNameNotFoundException)
            {
                ComponentName localComponentName3 = findAvailRecognizer(null);
                if (localComponentName3 == null)
                    continue;
                setCurRecognizer(localComponentName3);
                continue;
            }
            ComponentName localComponentName2 = findAvailRecognizer(null);
            if (localComponentName2 != null)
                setCurRecognizer(localComponentName2);
        }
    }

    class MyPackageMonitor extends PackageMonitor
    {
        MyPackageMonitor()
        {
        }

        public void onSomePackagesChanged()
        {
            ComponentName localComponentName1 = RecognitionManagerService.this.getCurRecognizer();
            if (localComponentName1 == null)
                if (anyPackagesAppearing())
                {
                    ComponentName localComponentName2 = RecognitionManagerService.this.findAvailRecognizer(null);
                    if (localComponentName2 != null)
                        RecognitionManagerService.this.setCurRecognizer(localComponentName2);
                }
            while (true)
            {
                return;
                int i = isPackageDisappearing(localComponentName1.getPackageName());
                if ((i == 3) || (i == 2))
                    RecognitionManagerService.this.setCurRecognizer(RecognitionManagerService.this.findAvailRecognizer(null));
                else if (isPackageModified(localComponentName1.getPackageName()))
                    RecognitionManagerService.this.setCurRecognizer(RecognitionManagerService.this.findAvailRecognizer(localComponentName1.getPackageName()));
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.RecognitionManagerService
 * JD-Core Version:        0.6.2
 */