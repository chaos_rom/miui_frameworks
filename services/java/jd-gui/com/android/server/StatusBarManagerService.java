package com.android.server;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.RemoteException;
import android.util.Slog;
import com.android.internal.statusbar.IStatusBar;
import com.android.internal.statusbar.IStatusBarService.Stub;
import com.android.internal.statusbar.StatusBarIconList;
import com.android.internal.statusbar.StatusBarNotification;
import com.android.server.wm.WindowManagerService;
import com.android.server.wm.WindowManagerService.OnHardKeyboardStatusChangeListener;
import java.util.ArrayList;
import java.util.HashMap;

public class StatusBarManagerService extends IStatusBarService.Stub
    implements WindowManagerService.OnHardKeyboardStatusChangeListener
{
    static final boolean SPEW = false;
    static final String TAG = "StatusBarManagerService";
    volatile IStatusBar mBar;
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            String str = paramAnonymousIntent.getAction();
            if (("android.intent.action.CLOSE_SYSTEM_DIALOGS".equals(str)) || ("android.intent.action.SCREEN_OFF".equals(str)))
                StatusBarManagerService.this.collapse();
        }
    };
    final Context mContext;
    ArrayList<DisableRecord> mDisableRecords = new ArrayList();
    int mDisabled = 0;
    Handler mHandler = new Handler();
    StatusBarIconList mIcons = new StatusBarIconList();
    int mImeBackDisposition;
    IBinder mImeToken = null;
    int mImeWindowVis = 0;
    Object mLock = new Object();
    boolean mMenuVisible = false;
    NotificationCallbacks mNotificationCallbacks;
    HashMap<IBinder, StatusBarNotification> mNotifications = new HashMap();
    IBinder mSysUiVisToken = new Binder();
    int mSystemUiVisibility = 0;
    final WindowManagerService mWindowManager;

    public StatusBarManagerService(Context paramContext, WindowManagerService paramWindowManagerService)
    {
        this.mContext = paramContext;
        this.mWindowManager = paramWindowManagerService;
        this.mWindowManager.setOnHardKeyboardStatusChangeListener(this);
        Resources localResources = paramContext.getResources();
        this.mIcons.defineSlots(localResources.getStringArray(17235985));
    }

    private void disableLocked(int paramInt, IBinder paramIBinder, String paramString)
    {
        manageDisableListLocked(paramInt, paramIBinder, paramString);
        final int i = gatherDisableActionsLocked();
        if (i != this.mDisabled)
        {
            this.mDisabled = i;
            this.mHandler.post(new Runnable()
            {
                public void run()
                {
                    StatusBarManagerService.this.mNotificationCallbacks.onSetDisabled(i);
                }
            });
            if (this.mBar == null);
        }
        try
        {
            this.mBar.disable(i);
            label64: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label64;
        }
    }

    private void enforceExpandStatusBar()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.EXPAND_STATUS_BAR", "StatusBarManagerService");
    }

    private void enforceStatusBar()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.STATUS_BAR", "StatusBarManagerService");
    }

    private void enforceStatusBarService()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.STATUS_BAR_SERVICE", "StatusBarManagerService");
    }

    private void updateUiVisibilityLocked(final int paramInt1, final int paramInt2)
    {
        if (this.mSystemUiVisibility != paramInt1)
        {
            this.mSystemUiVisibility = paramInt1;
            this.mHandler.post(new Runnable()
            {
                public void run()
                {
                    if (StatusBarManagerService.this.mBar != null);
                    try
                    {
                        StatusBarManagerService.this.mBar.setSystemUiVisibility(paramInt1, paramInt2);
                        label30: return;
                    }
                    catch (RemoteException localRemoteException)
                    {
                        break label30;
                    }
                }
            });
        }
    }

    public IBinder addNotification(StatusBarNotification paramStatusBarNotification)
    {
        Binder localBinder;
        synchronized (this.mNotifications)
        {
            localBinder = new Binder();
            this.mNotifications.put(localBinder, paramStatusBarNotification);
            IStatusBar localIStatusBar = this.mBar;
            if (localIStatusBar == null);
        }
        try
        {
            this.mBar.addNotification(localBinder, paramStatusBarNotification);
            label47: return localBinder;
            localObject = finally;
            throw localObject;
        }
        catch (RemoteException localRemoteException)
        {
            break label47;
        }
    }

    public void cancelPreloadRecentApps()
    {
        if (this.mBar != null);
        try
        {
            this.mBar.cancelPreloadRecentApps();
            label16: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label16;
        }
    }

    public void collapse()
    {
        enforceExpandStatusBar();
        if (this.mBar != null);
        try
        {
            this.mBar.animateCollapse();
            label20: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label20;
        }
    }

    public void disable(int paramInt, IBinder paramIBinder, String paramString)
    {
        enforceStatusBar();
        synchronized (this.mLock)
        {
            disableLocked(paramInt, paramIBinder, paramString);
            return;
        }
    }

    // ERROR //
    protected void dump(java.io.FileDescriptor paramFileDescriptor, java.io.PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 116	com/android/server/StatusBarManagerService:mContext	Landroid/content/Context;
        //     4: ldc 211
        //     6: invokevirtual 215	android/content/Context:checkCallingOrSelfPermission	(Ljava/lang/String;)I
        //     9: ifeq +40 -> 49
        //     12: aload_2
        //     13: new 217	java/lang/StringBuilder
        //     16: dup
        //     17: invokespecial 218	java/lang/StringBuilder:<init>	()V
        //     20: ldc 220
        //     22: invokevirtual 224	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     25: invokestatic 227	android/os/Binder:getCallingPid	()I
        //     28: invokevirtual 230	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     31: ldc 232
        //     33: invokevirtual 224	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     36: invokestatic 235	android/os/Binder:getCallingUid	()I
        //     39: invokevirtual 230	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     42: invokevirtual 239	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     45: invokevirtual 245	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     48: return
        //     49: aload_0
        //     50: getfield 79	com/android/server/StatusBarManagerService:mIcons	Lcom/android/internal/statusbar/StatusBarIconList;
        //     53: astore 4
        //     55: aload 4
        //     57: monitorenter
        //     58: aload_0
        //     59: getfield 79	com/android/server/StatusBarManagerService:mIcons	Lcom/android/internal/statusbar/StatusBarIconList;
        //     62: aload_2
        //     63: invokevirtual 248	com/android/internal/statusbar/StatusBarIconList:dump	(Ljava/io/PrintWriter;)V
        //     66: aload 4
        //     68: monitorexit
        //     69: aload_0
        //     70: getfield 84	com/android/server/StatusBarManagerService:mNotifications	Ljava/util/HashMap;
        //     73: astore 6
        //     75: aload 6
        //     77: monitorenter
        //     78: iconst_0
        //     79: istore 7
        //     81: aload_2
        //     82: ldc 250
        //     84: invokevirtual 245	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     87: aload_0
        //     88: getfield 84	com/android/server/StatusBarManagerService:mNotifications	Ljava/util/HashMap;
        //     91: invokevirtual 254	java/util/HashMap:entrySet	()Ljava/util/Set;
        //     94: invokeinterface 260 1 0
        //     99: astore 9
        //     101: aload 9
        //     103: invokeinterface 266 1 0
        //     108: ifeq +71 -> 179
        //     111: aload 9
        //     113: invokeinterface 270 1 0
        //     118: checkcast 272	java/util/Map$Entry
        //     121: astore 15
        //     123: iconst_2
        //     124: anewarray 98	java/lang/Object
        //     127: astore 16
        //     129: aload 16
        //     131: iconst_0
        //     132: iload 7
        //     134: invokestatic 278	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     137: aastore
        //     138: aload 16
        //     140: iconst_1
        //     141: aload 15
        //     143: invokeinterface 281 1 0
        //     148: checkcast 283	com/android/internal/statusbar/StatusBarNotification
        //     151: invokevirtual 284	com/android/internal/statusbar/StatusBarNotification:toString	()Ljava/lang/String;
        //     154: aastore
        //     155: aload_2
        //     156: ldc_w 286
        //     159: aload 16
        //     161: invokevirtual 290	java/io/PrintWriter:printf	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;
        //     164: pop
        //     165: iinc 7 1
        //     168: goto -67 -> 101
        //     171: astore 5
        //     173: aload 4
        //     175: monitorexit
        //     176: aload 5
        //     178: athrow
        //     179: aload 6
        //     181: monitorexit
        //     182: aload_0
        //     183: getfield 101	com/android/server/StatusBarManagerService:mLock	Ljava/lang/Object;
        //     186: astore 10
        //     188: aload 10
        //     190: monitorenter
        //     191: aload_0
        //     192: getfield 89	com/android/server/StatusBarManagerService:mDisableRecords	Ljava/util/ArrayList;
        //     195: invokevirtual 293	java/util/ArrayList:size	()I
        //     198: istore 12
        //     200: aload_2
        //     201: new 217	java/lang/StringBuilder
        //     204: dup
        //     205: invokespecial 218	java/lang/StringBuilder:<init>	()V
        //     208: ldc_w 295
        //     211: invokevirtual 224	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     214: iload 12
        //     216: invokevirtual 230	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     219: ldc_w 297
        //     222: invokevirtual 224	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     225: aload_0
        //     226: getfield 96	com/android/server/StatusBarManagerService:mDisabled	I
        //     229: invokestatic 301	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     232: invokevirtual 224	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     235: invokevirtual 239	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     238: invokevirtual 245	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     241: iconst_0
        //     242: istore 13
        //     244: iload 13
        //     246: iload 12
        //     248: if_icmpge +101 -> 349
        //     251: aload_0
        //     252: getfield 89	com/android/server/StatusBarManagerService:mDisableRecords	Ljava/util/ArrayList;
        //     255: iload 13
        //     257: invokevirtual 305	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     260: checkcast 25	com/android/server/StatusBarManagerService$DisableRecord
        //     263: astore 14
        //     265: aload_2
        //     266: new 217	java/lang/StringBuilder
        //     269: dup
        //     270: invokespecial 218	java/lang/StringBuilder:<init>	()V
        //     273: ldc_w 307
        //     276: invokevirtual 224	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     279: iload 13
        //     281: invokevirtual 230	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     284: ldc_w 309
        //     287: invokevirtual 224	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     290: aload 14
        //     292: getfield 312	com/android/server/StatusBarManagerService$DisableRecord:what	I
        //     295: invokestatic 301	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     298: invokevirtual 224	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     301: ldc_w 314
        //     304: invokevirtual 224	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     307: aload 14
        //     309: getfield 317	com/android/server/StatusBarManagerService$DisableRecord:pkg	Ljava/lang/String;
        //     312: invokevirtual 224	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     315: ldc_w 319
        //     318: invokevirtual 224	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     321: aload 14
        //     323: getfield 322	com/android/server/StatusBarManagerService$DisableRecord:token	Landroid/os/IBinder;
        //     326: invokevirtual 325	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     329: invokevirtual 239	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     332: invokevirtual 245	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     335: iinc 13 1
        //     338: goto -94 -> 244
        //     341: astore 8
        //     343: aload 6
        //     345: monitorexit
        //     346: aload 8
        //     348: athrow
        //     349: aload 10
        //     351: monitorexit
        //     352: goto -304 -> 48
        //     355: astore 11
        //     357: aload 10
        //     359: monitorexit
        //     360: aload 11
        //     362: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     58	69	171	finally
        //     173	176	171	finally
        //     81	165	341	finally
        //     179	182	341	finally
        //     343	346	341	finally
        //     191	335	355	finally
        //     349	360	355	finally
    }

    public void expand()
    {
        enforceExpandStatusBar();
        if (this.mBar != null);
        try
        {
            this.mBar.animateExpand();
            label20: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label20;
        }
    }

    int gatherDisableActionsLocked()
    {
        int i = this.mDisableRecords.size();
        int j = 0;
        for (int k = 0; k < i; k++)
            j |= ((DisableRecord)this.mDisableRecords.get(k)).what;
        return j;
    }

    void manageDisableListLocked(int paramInt, IBinder paramIBinder, String paramString)
    {
        int i = this.mDisableRecords.size();
        Object localObject = null;
        int j = 0;
        if (j < i)
        {
            DisableRecord localDisableRecord = (DisableRecord)this.mDisableRecords.get(j);
            if (localDisableRecord.token == paramIBinder)
                localObject = localDisableRecord;
        }
        else
        {
            if ((paramInt != 0) && (paramIBinder.isBinderAlive()))
                break label98;
            if (localObject != null)
            {
                this.mDisableRecords.remove(j);
                ((DisableRecord)localObject).token.unlinkToDeath((IBinder.DeathRecipient)localObject, 0);
            }
        }
        while (true)
        {
            return;
            j++;
            break;
            label98: if (localObject == null)
                localObject = new DisableRecord(null);
            try
            {
                paramIBinder.linkToDeath((IBinder.DeathRecipient)localObject, 0);
                this.mDisableRecords.add(localObject);
                ((DisableRecord)localObject).what = paramInt;
                ((DisableRecord)localObject).token = paramIBinder;
                ((DisableRecord)localObject).pkg = paramString;
            }
            catch (RemoteException localRemoteException)
            {
            }
        }
    }

    public void onClearAllNotifications()
    {
        enforceStatusBarService();
        this.mNotificationCallbacks.onClearAll();
    }

    public void onHardKeyboardStatusChange(final boolean paramBoolean1, final boolean paramBoolean2)
    {
        this.mHandler.post(new Runnable()
        {
            public void run()
            {
                if (StatusBarManagerService.this.mBar != null);
                try
                {
                    StatusBarManagerService.this.mBar.setHardKeyboardStatus(paramBoolean1, paramBoolean2);
                    label30: return;
                }
                catch (RemoteException localRemoteException)
                {
                    break label30;
                }
            }
        });
    }

    public void onNotificationClear(String paramString1, String paramString2, int paramInt)
    {
        enforceStatusBarService();
        this.mNotificationCallbacks.onNotificationClear(paramString1, paramString2, paramInt);
    }

    public void onNotificationClick(String paramString1, String paramString2, int paramInt)
    {
        enforceStatusBarService();
        this.mNotificationCallbacks.onNotificationClick(paramString1, paramString2, paramInt);
    }

    public void onNotificationError(String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3, String paramString3)
    {
        enforceStatusBarService();
        this.mNotificationCallbacks.onNotificationError(paramString1, paramString2, paramInt1, paramInt2, paramInt3, paramString3);
    }

    public void onPanelRevealed()
    {
        enforceStatusBarService();
        this.mNotificationCallbacks.onPanelRevealed();
    }

    public void preloadRecentApps()
    {
        if (this.mBar != null);
        try
        {
            this.mBar.preloadRecentApps();
            label16: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label16;
        }
    }

    // ERROR //
    public void registerStatusBar(IStatusBar paramIStatusBar, StatusBarIconList paramStatusBarIconList, java.util.List<IBinder> paramList1, java.util.List<StatusBarNotification> paramList, int[] paramArrayOfInt, java.util.List<IBinder> paramList2)
    {
        // Byte code:
        //     0: iconst_1
        //     1: istore 7
        //     3: aload_0
        //     4: invokespecial 355	com/android/server/StatusBarManagerService:enforceStatusBarService	()V
        //     7: ldc 33
        //     9: new 217	java/lang/StringBuilder
        //     12: dup
        //     13: invokespecial 218	java/lang/StringBuilder:<init>	()V
        //     16: ldc_w 386
        //     19: invokevirtual 224	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     22: aload_1
        //     23: invokevirtual 325	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     26: invokevirtual 239	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     29: invokestatic 392	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     32: pop
        //     33: aload_0
        //     34: aload_1
        //     35: putfield 161	com/android/server/StatusBarManagerService:mBar	Lcom/android/internal/statusbar/IStatusBar;
        //     38: aload_0
        //     39: getfield 79	com/android/server/StatusBarManagerService:mIcons	Lcom/android/internal/statusbar/StatusBarIconList;
        //     42: astore 9
        //     44: aload 9
        //     46: monitorenter
        //     47: aload_2
        //     48: aload_0
        //     49: getfield 79	com/android/server/StatusBarManagerService:mIcons	Lcom/android/internal/statusbar/StatusBarIconList;
        //     52: invokevirtual 396	com/android/internal/statusbar/StatusBarIconList:copyFrom	(Lcom/android/internal/statusbar/StatusBarIconList;)V
        //     55: aload 9
        //     57: monitorexit
        //     58: aload_0
        //     59: getfield 84	com/android/server/StatusBarManagerService:mNotifications	Ljava/util/HashMap;
        //     62: astore 11
        //     64: aload 11
        //     66: monitorenter
        //     67: aload_0
        //     68: getfield 84	com/android/server/StatusBarManagerService:mNotifications	Ljava/util/HashMap;
        //     71: invokevirtual 254	java/util/HashMap:entrySet	()Ljava/util/Set;
        //     74: invokeinterface 260 1 0
        //     79: astore 13
        //     81: aload 13
        //     83: invokeinterface 266 1 0
        //     88: ifeq +63 -> 151
        //     91: aload 13
        //     93: invokeinterface 270 1 0
        //     98: checkcast 272	java/util/Map$Entry
        //     101: astore 19
        //     103: aload_3
        //     104: aload 19
        //     106: invokeinterface 399 1 0
        //     111: invokeinterface 402 2 0
        //     116: pop
        //     117: aload 4
        //     119: aload 19
        //     121: invokeinterface 281 1 0
        //     126: invokeinterface 402 2 0
        //     131: pop
        //     132: goto -51 -> 81
        //     135: astore 12
        //     137: aload 11
        //     139: monitorexit
        //     140: aload 12
        //     142: athrow
        //     143: astore 10
        //     145: aload 9
        //     147: monitorexit
        //     148: aload 10
        //     150: athrow
        //     151: aload 11
        //     153: monitorexit
        //     154: aload_0
        //     155: getfield 101	com/android/server/StatusBarManagerService:mLock	Ljava/lang/Object;
        //     158: astore 14
        //     160: aload 14
        //     162: monitorenter
        //     163: aload 5
        //     165: iconst_0
        //     166: aload_0
        //     167: invokevirtual 152	com/android/server/StatusBarManagerService:gatherDisableActionsLocked	()I
        //     170: iastore
        //     171: aload 5
        //     173: iconst_1
        //     174: aload_0
        //     175: getfield 103	com/android/server/StatusBarManagerService:mSystemUiVisibility	I
        //     178: iastore
        //     179: aload_0
        //     180: getfield 105	com/android/server/StatusBarManagerService:mMenuVisible	Z
        //     183: ifeq +82 -> 265
        //     186: iload 7
        //     188: istore 16
        //     190: aload 5
        //     192: iconst_2
        //     193: iload 16
        //     195: iastore
        //     196: aload 5
        //     198: iconst_3
        //     199: aload_0
        //     200: getfield 107	com/android/server/StatusBarManagerService:mImeWindowVis	I
        //     203: iastore
        //     204: aload 5
        //     206: iconst_4
        //     207: aload_0
        //     208: getfield 404	com/android/server/StatusBarManagerService:mImeBackDisposition	I
        //     211: iastore
        //     212: aload 6
        //     214: aload_0
        //     215: getfield 109	com/android/server/StatusBarManagerService:mImeToken	Landroid/os/IBinder;
        //     218: invokeinterface 402 2 0
        //     223: pop
        //     224: aload 14
        //     226: monitorexit
        //     227: aload_0
        //     228: getfield 118	com/android/server/StatusBarManagerService:mWindowManager	Lcom/android/server/wm/WindowManagerService;
        //     231: invokevirtual 407	com/android/server/wm/WindowManagerService:isHardKeyboardAvailable	()Z
        //     234: ifeq +45 -> 279
        //     237: iload 7
        //     239: istore 18
        //     241: aload 5
        //     243: iconst_5
        //     244: iload 18
        //     246: iastore
        //     247: aload_0
        //     248: getfield 118	com/android/server/StatusBarManagerService:mWindowManager	Lcom/android/server/wm/WindowManagerService;
        //     251: invokevirtual 410	com/android/server/wm/WindowManagerService:isHardKeyboardEnabled	()Z
        //     254: ifeq +31 -> 285
        //     257: aload 5
        //     259: bipush 6
        //     261: iload 7
        //     263: iastore
        //     264: return
        //     265: iconst_0
        //     266: istore 16
        //     268: goto -78 -> 190
        //     271: astore 15
        //     273: aload 14
        //     275: monitorexit
        //     276: aload 15
        //     278: athrow
        //     279: iconst_0
        //     280: istore 18
        //     282: goto -41 -> 241
        //     285: iconst_0
        //     286: istore 7
        //     288: goto -31 -> 257
        //
        // Exception table:
        //     from	to	target	type
        //     67	140	135	finally
        //     151	154	135	finally
        //     47	58	143	finally
        //     145	148	143	finally
        //     163	227	271	finally
        //     273	276	271	finally
    }

    // ERROR //
    public void removeIcon(String paramString)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 205	com/android/server/StatusBarManagerService:enforceStatusBar	()V
        //     4: aload_0
        //     5: getfield 79	com/android/server/StatusBarManagerService:mIcons	Lcom/android/internal/statusbar/StatusBarIconList;
        //     8: astore_2
        //     9: aload_2
        //     10: monitorenter
        //     11: aload_0
        //     12: getfield 79	com/android/server/StatusBarManagerService:mIcons	Lcom/android/internal/statusbar/StatusBarIconList;
        //     15: aload_1
        //     16: invokevirtual 414	com/android/internal/statusbar/StatusBarIconList:getSlotIndex	(Ljava/lang/String;)I
        //     19: istore 4
        //     21: iload 4
        //     23: ifge +36 -> 59
        //     26: new 416	java/lang/SecurityException
        //     29: dup
        //     30: new 217	java/lang/StringBuilder
        //     33: dup
        //     34: invokespecial 218	java/lang/StringBuilder:<init>	()V
        //     37: ldc_w 418
        //     40: invokevirtual 224	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     43: aload_1
        //     44: invokevirtual 224	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     47: invokevirtual 239	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     50: invokespecial 420	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     53: athrow
        //     54: astore_3
        //     55: aload_2
        //     56: monitorexit
        //     57: aload_3
        //     58: athrow
        //     59: aload_0
        //     60: getfield 79	com/android/server/StatusBarManagerService:mIcons	Lcom/android/internal/statusbar/StatusBarIconList;
        //     63: iload 4
        //     65: invokevirtual 422	com/android/internal/statusbar/StatusBarIconList:removeIcon	(I)V
        //     68: aload_0
        //     69: getfield 161	com/android/server/StatusBarManagerService:mBar	Lcom/android/internal/statusbar/IStatusBar;
        //     72: astore 5
        //     74: aload 5
        //     76: ifnull +14 -> 90
        //     79: aload_0
        //     80: getfield 161	com/android/server/StatusBarManagerService:mBar	Lcom/android/internal/statusbar/IStatusBar;
        //     83: iload 4
        //     85: invokeinterface 423 2 0
        //     90: aload_2
        //     91: monitorexit
        //     92: return
        //     93: astore 6
        //     95: goto -5 -> 90
        //
        // Exception table:
        //     from	to	target	type
        //     11	57	54	finally
        //     59	74	54	finally
        //     79	90	54	finally
        //     90	92	54	finally
        //     79	90	93	android/os/RemoteException
    }

    public void removeNotification(IBinder paramIBinder)
    {
        synchronized (this.mNotifications)
        {
            if ((StatusBarNotification)this.mNotifications.remove(paramIBinder) == null)
            {
                Slog.e("StatusBarManagerService", "removeNotification key not found: " + paramIBinder);
                return;
            }
            IStatusBar localIStatusBar = this.mBar;
            if (localIStatusBar == null);
        }
        try
        {
            this.mBar.removeNotification(paramIBinder);
            label73: return;
            localObject = finally;
            throw localObject;
        }
        catch (RemoteException localRemoteException)
        {
            break label73;
        }
    }

    public void setHardKeyboardEnabled(final boolean paramBoolean)
    {
        this.mHandler.post(new Runnable()
        {
            public void run()
            {
                StatusBarManagerService.this.mWindowManager.setHardKeyboardEnabled(paramBoolean);
            }
        });
    }

    // ERROR //
    public void setIcon(String paramString1, String paramString2, int paramInt1, int paramInt2, String paramString3)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 205	com/android/server/StatusBarManagerService:enforceStatusBar	()V
        //     4: aload_0
        //     5: getfield 79	com/android/server/StatusBarManagerService:mIcons	Lcom/android/internal/statusbar/StatusBarIconList;
        //     8: astore 6
        //     10: aload 6
        //     12: monitorenter
        //     13: aload_0
        //     14: getfield 79	com/android/server/StatusBarManagerService:mIcons	Lcom/android/internal/statusbar/StatusBarIconList;
        //     17: aload_1
        //     18: invokevirtual 414	com/android/internal/statusbar/StatusBarIconList:getSlotIndex	(Ljava/lang/String;)I
        //     21: istore 8
        //     23: iload 8
        //     25: ifge +39 -> 64
        //     28: new 416	java/lang/SecurityException
        //     31: dup
        //     32: new 217	java/lang/StringBuilder
        //     35: dup
        //     36: invokespecial 218	java/lang/StringBuilder:<init>	()V
        //     39: ldc_w 418
        //     42: invokevirtual 224	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     45: aload_1
        //     46: invokevirtual 224	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     49: invokevirtual 239	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     52: invokespecial 420	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     55: athrow
        //     56: astore 7
        //     58: aload 6
        //     60: monitorexit
        //     61: aload 7
        //     63: athrow
        //     64: new 444	com/android/internal/statusbar/StatusBarIcon
        //     67: dup
        //     68: aload_2
        //     69: iload_3
        //     70: iload 4
        //     72: iconst_0
        //     73: aload 5
        //     75: invokespecial 447	com/android/internal/statusbar/StatusBarIcon:<init>	(Ljava/lang/String;IIILjava/lang/CharSequence;)V
        //     78: astore 9
        //     80: aload_0
        //     81: getfield 79	com/android/server/StatusBarManagerService:mIcons	Lcom/android/internal/statusbar/StatusBarIconList;
        //     84: iload 8
        //     86: aload 9
        //     88: invokevirtual 450	com/android/internal/statusbar/StatusBarIconList:setIcon	(ILcom/android/internal/statusbar/StatusBarIcon;)V
        //     91: aload_0
        //     92: getfield 161	com/android/server/StatusBarManagerService:mBar	Lcom/android/internal/statusbar/IStatusBar;
        //     95: astore 10
        //     97: aload 10
        //     99: ifnull +16 -> 115
        //     102: aload_0
        //     103: getfield 161	com/android/server/StatusBarManagerService:mBar	Lcom/android/internal/statusbar/IStatusBar;
        //     106: iload 8
        //     108: aload 9
        //     110: invokeinterface 451 3 0
        //     115: aload 6
        //     117: monitorexit
        //     118: return
        //     119: astore 11
        //     121: goto -6 -> 115
        //
        // Exception table:
        //     from	to	target	type
        //     13	61	56	finally
        //     64	97	56	finally
        //     102	115	56	finally
        //     115	118	56	finally
        //     102	115	119	android/os/RemoteException
    }

    // ERROR //
    public void setIconVisibility(String paramString, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 205	com/android/server/StatusBarManagerService:enforceStatusBar	()V
        //     4: aload_0
        //     5: getfield 79	com/android/server/StatusBarManagerService:mIcons	Lcom/android/internal/statusbar/StatusBarIconList;
        //     8: astore_3
        //     9: aload_3
        //     10: monitorenter
        //     11: aload_0
        //     12: getfield 79	com/android/server/StatusBarManagerService:mIcons	Lcom/android/internal/statusbar/StatusBarIconList;
        //     15: aload_1
        //     16: invokevirtual 414	com/android/internal/statusbar/StatusBarIconList:getSlotIndex	(Ljava/lang/String;)I
        //     19: istore 5
        //     21: iload 5
        //     23: ifge +38 -> 61
        //     26: new 416	java/lang/SecurityException
        //     29: dup
        //     30: new 217	java/lang/StringBuilder
        //     33: dup
        //     34: invokespecial 218	java/lang/StringBuilder:<init>	()V
        //     37: ldc_w 418
        //     40: invokevirtual 224	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     43: aload_1
        //     44: invokevirtual 224	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     47: invokevirtual 239	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     50: invokespecial 420	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     53: athrow
        //     54: astore 4
        //     56: aload_3
        //     57: monitorexit
        //     58: aload 4
        //     60: athrow
        //     61: aload_0
        //     62: getfield 79	com/android/server/StatusBarManagerService:mIcons	Lcom/android/internal/statusbar/StatusBarIconList;
        //     65: iload 5
        //     67: invokevirtual 457	com/android/internal/statusbar/StatusBarIconList:getIcon	(I)Lcom/android/internal/statusbar/StatusBarIcon;
        //     70: astore 6
        //     72: aload 6
        //     74: ifnonnull +8 -> 82
        //     77: aload_3
        //     78: monitorexit
        //     79: goto +52 -> 131
        //     82: aload 6
        //     84: getfield 460	com/android/internal/statusbar/StatusBarIcon:visible	Z
        //     87: iload_2
        //     88: if_icmpeq +33 -> 121
        //     91: aload 6
        //     93: iload_2
        //     94: putfield 460	com/android/internal/statusbar/StatusBarIcon:visible	Z
        //     97: aload_0
        //     98: getfield 161	com/android/server/StatusBarManagerService:mBar	Lcom/android/internal/statusbar/IStatusBar;
        //     101: astore 7
        //     103: aload 7
        //     105: ifnull +16 -> 121
        //     108: aload_0
        //     109: getfield 161	com/android/server/StatusBarManagerService:mBar	Lcom/android/internal/statusbar/IStatusBar;
        //     112: iload 5
        //     114: aload 6
        //     116: invokeinterface 451 3 0
        //     121: aload_3
        //     122: monitorexit
        //     123: goto +8 -> 131
        //     126: astore 8
        //     128: goto -7 -> 121
        //     131: return
        //
        // Exception table:
        //     from	to	target	type
        //     11	58	54	finally
        //     61	103	54	finally
        //     108	121	54	finally
        //     121	123	54	finally
        //     108	121	126	android/os/RemoteException
    }

    public void setImeWindowStatus(final IBinder paramIBinder, final int paramInt1, final int paramInt2)
    {
        enforceStatusBar();
        synchronized (this.mLock)
        {
            this.mImeWindowVis = paramInt1;
            this.mImeBackDisposition = paramInt2;
            this.mImeToken = paramIBinder;
            this.mHandler.post(new Runnable()
            {
                public void run()
                {
                    if (StatusBarManagerService.this.mBar != null);
                    try
                    {
                        StatusBarManagerService.this.mBar.setImeWindowStatus(paramIBinder, paramInt1, paramInt2);
                        label34: return;
                    }
                    catch (RemoteException localRemoteException)
                    {
                        break label34;
                    }
                }
            });
            return;
        }
    }

    public void setNotificationCallbacks(NotificationCallbacks paramNotificationCallbacks)
    {
        this.mNotificationCallbacks = paramNotificationCallbacks;
    }

    public void setSystemUiVisibility(int paramInt1, int paramInt2)
    {
        enforceStatusBarService();
        synchronized (this.mLock)
        {
            updateUiVisibilityLocked(paramInt1, paramInt2);
            disableLocked(0x1FF0000 & paramInt1, this.mSysUiVisToken, "WindowManager.LayoutParams");
            return;
        }
    }

    public void toggleRecentApps()
    {
        if (this.mBar != null);
        try
        {
            this.mBar.toggleRecentApps();
            label16: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label16;
        }
    }

    public void topAppWindowChanged(final boolean paramBoolean)
    {
        enforceStatusBar();
        synchronized (this.mLock)
        {
            this.mMenuVisible = paramBoolean;
            this.mHandler.post(new Runnable()
            {
                public void run()
                {
                    if (StatusBarManagerService.this.mBar != null);
                    try
                    {
                        StatusBarManagerService.this.mBar.topAppWindowChanged(paramBoolean);
                        label26: return;
                    }
                    catch (RemoteException localRemoteException)
                    {
                        break label26;
                    }
                }
            });
            return;
        }
    }

    // ERROR //
    public void updateNotification(IBinder paramIBinder, StatusBarNotification paramStatusBarNotification)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 84	com/android/server/StatusBarManagerService:mNotifications	Ljava/util/HashMap;
        //     4: astore_3
        //     5: aload_3
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 84	com/android/server/StatusBarManagerService:mNotifications	Ljava/util/HashMap;
        //     11: aload_1
        //     12: invokevirtual 482	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
        //     15: ifne +38 -> 53
        //     18: new 484	java/lang/IllegalArgumentException
        //     21: dup
        //     22: new 217	java/lang/StringBuilder
        //     25: dup
        //     26: invokespecial 218	java/lang/StringBuilder:<init>	()V
        //     29: ldc_w 486
        //     32: invokevirtual 224	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     35: aload_1
        //     36: invokevirtual 325	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     39: invokevirtual 239	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     42: invokespecial 487	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     45: athrow
        //     46: astore 4
        //     48: aload_3
        //     49: monitorexit
        //     50: aload 4
        //     52: athrow
        //     53: aload_0
        //     54: getfield 84	com/android/server/StatusBarManagerService:mNotifications	Ljava/util/HashMap;
        //     57: aload_1
        //     58: aload_2
        //     59: invokevirtual 191	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     62: pop
        //     63: aload_0
        //     64: getfield 161	com/android/server/StatusBarManagerService:mBar	Lcom/android/internal/statusbar/IStatusBar;
        //     67: astore 6
        //     69: aload 6
        //     71: ifnull +14 -> 85
        //     74: aload_0
        //     75: getfield 161	com/android/server/StatusBarManagerService:mBar	Lcom/android/internal/statusbar/IStatusBar;
        //     78: aload_1
        //     79: aload_2
        //     80: invokeinterface 489 3 0
        //     85: aload_3
        //     86: monitorexit
        //     87: return
        //     88: astore 7
        //     90: goto -5 -> 85
        //
        // Exception table:
        //     from	to	target	type
        //     7	50	46	finally
        //     53	69	46	finally
        //     74	85	46	finally
        //     85	87	46	finally
        //     74	85	88	android/os/RemoteException
    }

    public static abstract interface NotificationCallbacks
    {
        public abstract void onClearAll();

        public abstract void onNotificationClear(String paramString1, String paramString2, int paramInt);

        public abstract void onNotificationClick(String paramString1, String paramString2, int paramInt);

        public abstract void onNotificationError(String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3, String paramString3);

        public abstract void onPanelRevealed();

        public abstract void onSetDisabled(int paramInt);
    }

    private class DisableRecord
        implements IBinder.DeathRecipient
    {
        String pkg;
        IBinder token;
        int what;

        private DisableRecord()
        {
        }

        public void binderDied()
        {
            Slog.i("StatusBarManagerService", "binder died for pkg=" + this.pkg);
            StatusBarManagerService.this.disable(0, this.token, this.pkg);
            this.token.unlinkToDeath(this, 0);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.StatusBarManagerService
 * JD-Core Version:        0.6.2
 */