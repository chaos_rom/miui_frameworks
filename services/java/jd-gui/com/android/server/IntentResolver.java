package com.android.server;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.util.FastImmutableArraySet;
import android.util.PrintWriterPrinter;
import android.util.Slog;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public abstract class IntentResolver<F extends IntentFilter, R>
{
    private static final boolean DEBUG = false;
    private static final String TAG = "IntentResolver";
    private static final boolean localLOGV;
    private static final Comparator mResolvePrioritySorter = new Comparator()
    {
        public int compare(Object paramAnonymousObject1, Object paramAnonymousObject2)
        {
            int i = ((IntentFilter)paramAnonymousObject1).getPriority();
            int j = ((IntentFilter)paramAnonymousObject2).getPriority();
            int k;
            if (i > j)
                k = -1;
            while (true)
            {
                return k;
                if (i < j)
                    k = 1;
                else
                    k = 0;
            }
        }
    };
    private final HashMap<String, ArrayList<F>> mActionToFilter = new HashMap();
    private final HashMap<String, ArrayList<F>> mBaseTypeToFilter = new HashMap();
    private final HashSet<F> mFilters = new HashSet();
    private final HashMap<String, ArrayList<F>> mSchemeToFilter = new HashMap();
    private final HashMap<String, ArrayList<F>> mTypeToFilter = new HashMap();
    private final HashMap<String, ArrayList<F>> mTypedActionToFilter = new HashMap();
    private final HashMap<String, ArrayList<F>> mWildTypeToFilter = new HashMap();

    private void buildResolveList(Intent paramIntent, FastImmutableArraySet<String> paramFastImmutableArraySet, boolean paramBoolean1, boolean paramBoolean2, String paramString1, String paramString2, List<F> paramList, List<R> paramList1, int paramInt)
    {
        String str1 = paramIntent.getAction();
        Uri localUri = paramIntent.getData();
        String str2 = paramIntent.getPackage();
        boolean bool = paramIntent.isExcludingStopped();
        int i;
        int j;
        int k;
        label44: IntentFilter localIntentFilter;
        if (paramList != null)
        {
            i = paramList.size();
            j = 0;
            k = 0;
            if (k >= i)
                break label421;
            localIntentFilter = (IntentFilter)paramList.get(k);
            if (paramBoolean1)
                Slog.v("IntentResolver", "Matching against filter " + localIntentFilter);
            if ((!bool) || (!isFilterStopped(localIntentFilter, paramInt)))
                break label135;
            if (paramBoolean1)
                Slog.v("IntentResolver", "    Filter's target is stopped; skipping");
        }
        label135: int m;
        label325: 
        do
            while (true)
            {
                k++;
                break label44;
                i = 0;
                break;
                if ((str2 != null) && (!str2.equals(packageForFilter(localIntentFilter))))
                {
                    if (paramBoolean1)
                        Slog.v("IntentResolver", "    Filter is not from package " + str2 + "; skipping");
                }
                else if (!allowFilterResult(localIntentFilter, paramList1))
                {
                    if (paramBoolean1)
                        Slog.v("IntentResolver", "    Filter's target already added");
                }
                else
                {
                    m = localIntentFilter.match(str1, paramString1, paramString2, localUri, paramFastImmutableArraySet, "IntentResolver");
                    if (m < 0)
                        break label325;
                    if (paramBoolean1)
                        Slog.v("IntentResolver", "    Filter matched!    match=0x" + Integer.toHexString(m));
                    if ((!paramBoolean2) || (localIntentFilter.hasCategory("android.intent.category.DEFAULT")))
                    {
                        Object localObject = newResult(localIntentFilter, m, paramInt);
                        if (localObject != null)
                            paramList1.add(localObject);
                    }
                    else
                    {
                        j = 1;
                    }
                }
            }
        while (!paramBoolean1);
        String str3;
        switch (m)
        {
        default:
            str3 = "unknown reason";
        case -3:
        case -4:
        case -2:
        case -1:
        }
        while (true)
        {
            Slog.v("IntentResolver", "    Filter did not match: " + str3);
            break;
            str3 = "action";
            continue;
            str3 = "category";
            continue;
            str3 = "data";
            continue;
            str3 = "type";
        }
        label421: if ((paramList1.size() == 0) && (j != 0))
            Slog.w("IntentResolver", "resolveIntent failed: found match, but none with Intent.CATEGORY_DEFAULT");
    }

    private static FastImmutableArraySet<String> getFastIntentCategories(Intent paramIntent)
    {
        Set localSet = paramIntent.getCategories();
        if (localSet == null);
        for (Object localObject = null; ; localObject = new FastImmutableArraySet(localSet.toArray(new String[localSet.size()])))
            return localObject;
    }

    private final int register_intent_filter(F paramF, Iterator<String> paramIterator, HashMap<String, ArrayList<F>> paramHashMap, String paramString)
    {
        int i;
        if (paramIterator == null)
            i = 0;
        while (true)
        {
            return i;
            i = 0;
            while (paramIterator.hasNext())
            {
                String str = (String)paramIterator.next();
                i++;
                ArrayList localArrayList = (ArrayList)paramHashMap.get(str);
                if (localArrayList == null)
                {
                    localArrayList = new ArrayList();
                    paramHashMap.put(str, localArrayList);
                }
                localArrayList.add(paramF);
            }
        }
    }

    private final int register_mime_types(F paramF, String paramString)
    {
        Iterator localIterator = paramF.typesIterator();
        int i;
        if (localIterator == null)
            i = 0;
        while (true)
        {
            return i;
            i = 0;
            while (localIterator.hasNext())
            {
                String str1 = (String)localIterator.next();
                i++;
                String str2 = str1;
                int j = str1.indexOf('/');
                if (j > 0)
                    str2 = str1.substring(0, j).intern();
                while (true)
                {
                    ArrayList localArrayList1 = (ArrayList)this.mTypeToFilter.get(str1);
                    if (localArrayList1 == null)
                    {
                        localArrayList1 = new ArrayList();
                        this.mTypeToFilter.put(str1, localArrayList1);
                    }
                    localArrayList1.add(paramF);
                    if (j <= 0)
                        break label199;
                    ArrayList localArrayList3 = (ArrayList)this.mBaseTypeToFilter.get(str2);
                    if (localArrayList3 == null)
                    {
                        localArrayList3 = new ArrayList();
                        this.mBaseTypeToFilter.put(str2, localArrayList3);
                    }
                    localArrayList3.add(paramF);
                    break;
                    str1 = str1 + "/*";
                }
                label199: ArrayList localArrayList2 = (ArrayList)this.mWildTypeToFilter.get(str2);
                if (localArrayList2 == null)
                {
                    localArrayList2 = new ArrayList();
                    this.mWildTypeToFilter.put(str2, localArrayList2);
                }
                localArrayList2.add(paramF);
            }
        }
    }

    private final boolean remove_all_objects(List<F> paramList, Object paramObject)
    {
        boolean bool = false;
        if (paramList != null)
        {
            int i = paramList.size();
            for (int j = 0; j < i; j++)
                if (paramList.get(j) == paramObject)
                {
                    paramList.remove(j);
                    j--;
                    i--;
                }
            if (i > 0)
                bool = true;
        }
        return bool;
    }

    private final int unregister_intent_filter(F paramF, Iterator<String> paramIterator, HashMap<String, ArrayList<F>> paramHashMap, String paramString)
    {
        int i;
        if (paramIterator == null)
            i = 0;
        while (true)
        {
            return i;
            i = 0;
            while (paramIterator.hasNext())
            {
                String str = (String)paramIterator.next();
                i++;
                if (!remove_all_objects((List)paramHashMap.get(str), paramF))
                    paramHashMap.remove(str);
            }
        }
    }

    private final int unregister_mime_types(F paramF, String paramString)
    {
        Iterator localIterator = paramF.typesIterator();
        int i;
        if (localIterator == null)
            i = 0;
        while (true)
        {
            return i;
            i = 0;
            while (localIterator.hasNext())
            {
                String str1 = (String)localIterator.next();
                i++;
                String str2 = str1;
                int j = str1.indexOf('/');
                if (j > 0)
                    str2 = str1.substring(0, j).intern();
                while (true)
                {
                    if (!remove_all_objects((List)this.mTypeToFilter.get(str1), paramF))
                        this.mTypeToFilter.remove(str1);
                    if (j <= 0)
                        break label165;
                    if (remove_all_objects((List)this.mBaseTypeToFilter.get(str2), paramF))
                        break;
                    this.mBaseTypeToFilter.remove(str2);
                    break;
                    str1 = str1 + "/*";
                }
                label165: if (!remove_all_objects((List)this.mWildTypeToFilter.get(str2), paramF))
                    this.mWildTypeToFilter.remove(str2);
            }
        }
    }

    public void addFilter(F paramF)
    {
        this.mFilters.add(paramF);
        int i = register_intent_filter(paramF, paramF.schemesIterator(), this.mSchemeToFilter, "            Scheme: ");
        int j = register_mime_types(paramF, "            Type: ");
        if ((i == 0) && (j == 0))
            register_intent_filter(paramF, paramF.actionsIterator(), this.mActionToFilter, "            Action: ");
        if (j != 0)
            register_intent_filter(paramF, paramF.actionsIterator(), this.mTypedActionToFilter, "            TypedAction: ");
    }

    protected boolean allowFilterResult(F paramF, List<R> paramList)
    {
        return true;
    }

    public boolean dump(PrintWriter paramPrintWriter, String paramString1, String paramString2, String paramString3, boolean paramBoolean)
    {
        String str1 = paramString2 + "    ";
        String str2 = "\n" + paramString2;
        String str3 = paramString1 + "\n" + paramString2;
        if (dumpMap(paramPrintWriter, str3, "Full MIME Types:", str1, this.mTypeToFilter, paramString3, paramBoolean))
            str3 = str2;
        if (dumpMap(paramPrintWriter, str3, "Base MIME Types:", str1, this.mBaseTypeToFilter, paramString3, paramBoolean))
            str3 = str2;
        if (dumpMap(paramPrintWriter, str3, "Wild MIME Types:", str1, this.mWildTypeToFilter, paramString3, paramBoolean))
            str3 = str2;
        if (dumpMap(paramPrintWriter, str3, "Schemes:", str1, this.mSchemeToFilter, paramString3, paramBoolean))
            str3 = str2;
        if (dumpMap(paramPrintWriter, str3, "Non-Data Actions:", str1, this.mActionToFilter, paramString3, paramBoolean))
            str3 = str2;
        if (dumpMap(paramPrintWriter, str3, "MIME Typed Actions:", str1, this.mTypedActionToFilter, paramString3, paramBoolean))
            str3 = str2;
        if (str3 == str2);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected void dumpFilter(PrintWriter paramPrintWriter, String paramString, F paramF)
    {
        paramPrintWriter.print(paramString);
        paramPrintWriter.println(paramF);
    }

    boolean dumpMap(PrintWriter paramPrintWriter, String paramString1, String paramString2, String paramString3, Map<String, ArrayList<F>> paramMap, String paramString4, boolean paramBoolean)
    {
        String str1 = paramString3 + "    ";
        String str2 = paramString3 + "        ";
        boolean bool = false;
        PrintWriterPrinter localPrintWriterPrinter = null;
        Iterator localIterator = paramMap.entrySet().iterator();
        if (localIterator.hasNext())
        {
            Map.Entry localEntry = (Map.Entry)localIterator.next();
            ArrayList localArrayList = (ArrayList)localEntry.getValue();
            int i = localArrayList.size();
            int j = 0;
            int k = 0;
            label113: IntentFilter localIntentFilter;
            if (k < i)
            {
                localIntentFilter = (IntentFilter)localArrayList.get(k);
                if ((paramString4 == null) || (paramString4.equals(packageForFilter(localIntentFilter))))
                    break label157;
            }
            while (true)
            {
                k++;
                break label113;
                break;
                label157: if (paramString2 != null)
                {
                    paramPrintWriter.print(paramString1);
                    paramPrintWriter.println(paramString2);
                    paramString2 = null;
                }
                if (j == 0)
                {
                    paramPrintWriter.print(str1);
                    paramPrintWriter.print((String)localEntry.getKey());
                    paramPrintWriter.println(":");
                    j = 1;
                }
                bool = true;
                dumpFilter(paramPrintWriter, str2, localIntentFilter);
                if (paramBoolean)
                {
                    if (localPrintWriterPrinter == null)
                        localPrintWriterPrinter = new PrintWriterPrinter(paramPrintWriter);
                    localIntentFilter.dump(localPrintWriterPrinter, str2 + "    ");
                }
            }
        }
        return bool;
    }

    public Iterator<F> filterIterator()
    {
        return new IteratorWrapper(this.mFilters.iterator());
    }

    public Set<F> filterSet()
    {
        return Collections.unmodifiableSet(this.mFilters);
    }

    protected boolean isFilterStopped(F paramF, int paramInt)
    {
        return false;
    }

    protected R newResult(F paramF, int paramInt1, int paramInt2)
    {
        return paramF;
    }

    protected abstract String packageForFilter(F paramF);

    public List<R> queryIntent(Intent paramIntent, String paramString, boolean paramBoolean, int paramInt)
    {
        String str1 = paramIntent.getScheme();
        ArrayList localArrayList1 = new ArrayList();
        boolean bool;
        ArrayList localArrayList2;
        ArrayList localArrayList3;
        ArrayList localArrayList4;
        ArrayList localArrayList5;
        String str2;
        if ((0x8 & paramIntent.getFlags()) != 0)
        {
            bool = true;
            if (bool)
                Slog.v("IntentResolver", "Resolving type " + paramString + " scheme " + str1 + " of intent " + paramIntent);
            localArrayList2 = null;
            localArrayList3 = null;
            localArrayList4 = null;
            localArrayList5 = null;
            if (paramString != null)
            {
                int i = paramString.indexOf('/');
                if (i > 0)
                {
                    str2 = paramString.substring(0, i);
                    if (str2.equals("*"))
                        break label685;
                    if ((paramString.length() == i + 2) && (paramString.charAt(i + 1) == '*'))
                        break label590;
                    localArrayList2 = (ArrayList)this.mTypeToFilter.get(paramString);
                    if (bool)
                        Slog.v("IntentResolver", "First type cut: " + localArrayList2);
                    localArrayList3 = (ArrayList)this.mWildTypeToFilter.get(str2);
                    if (bool)
                        Slog.v("IntentResolver", "Second type cut: " + localArrayList3);
                    label244: localArrayList4 = (ArrayList)this.mWildTypeToFilter.get("*");
                    if (bool)
                        Slog.v("IntentResolver", "Third type cut: " + localArrayList4);
                }
            }
        }
        while (true)
        {
            if (str1 != null)
            {
                localArrayList5 = (ArrayList)this.mSchemeToFilter.get(str1);
                if (bool)
                    Slog.v("IntentResolver", "Scheme list: " + localArrayList5);
            }
            if ((paramString == null) && (str1 == null) && (paramIntent.getAction() != null))
            {
                localArrayList2 = (ArrayList)this.mActionToFilter.get(paramIntent.getAction());
                if (bool)
                    Slog.v("IntentResolver", "Action list: " + localArrayList2);
            }
            FastImmutableArraySet localFastImmutableArraySet = getFastIntentCategories(paramIntent);
            if (localArrayList2 != null)
                buildResolveList(paramIntent, localFastImmutableArraySet, bool, paramBoolean, paramString, str1, localArrayList2, localArrayList1, paramInt);
            if (localArrayList3 != null)
                buildResolveList(paramIntent, localFastImmutableArraySet, bool, paramBoolean, paramString, str1, localArrayList3, localArrayList1, paramInt);
            if (localArrayList4 != null)
                buildResolveList(paramIntent, localFastImmutableArraySet, bool, paramBoolean, paramString, str1, localArrayList4, localArrayList1, paramInt);
            if (localArrayList5 != null)
                buildResolveList(paramIntent, localFastImmutableArraySet, bool, paramBoolean, paramString, str1, localArrayList5, localArrayList1, paramInt);
            sortResults(localArrayList1);
            if (!bool)
                break label743;
            Slog.v("IntentResolver", "Final result list:");
            Iterator localIterator = localArrayList1.iterator();
            while (localIterator.hasNext())
            {
                Object localObject = localIterator.next();
                Slog.v("IntentResolver", "    " + localObject);
            }
            bool = false;
            break;
            label590: localArrayList2 = (ArrayList)this.mBaseTypeToFilter.get(str2);
            if (bool)
                Slog.v("IntentResolver", "First type cut: " + localArrayList2);
            localArrayList3 = (ArrayList)this.mWildTypeToFilter.get(str2);
            if (!bool)
                break label244;
            Slog.v("IntentResolver", "Second type cut: " + localArrayList3);
            break label244;
            label685: if (paramIntent.getAction() != null)
            {
                localArrayList2 = (ArrayList)this.mTypedActionToFilter.get(paramIntent.getAction());
                if (bool)
                    Slog.v("IntentResolver", "Typed Action list: " + localArrayList2);
            }
        }
        label743: return localArrayList1;
    }

    public List<R> queryIntentFromList(Intent paramIntent, String paramString, boolean paramBoolean, ArrayList<ArrayList<F>> paramArrayList, int paramInt)
    {
        ArrayList localArrayList = new ArrayList();
        if ((0x8 & paramIntent.getFlags()) != 0);
        for (boolean bool = true; ; bool = false)
        {
            FastImmutableArraySet localFastImmutableArraySet = getFastIntentCategories(paramIntent);
            String str = paramIntent.getScheme();
            int i = paramArrayList.size();
            for (int j = 0; j < i; j++)
                buildResolveList(paramIntent, localFastImmutableArraySet, bool, paramBoolean, paramString, str, (List)paramArrayList.get(j), localArrayList, paramInt);
        }
        sortResults(localArrayList);
        return localArrayList;
    }

    public void removeFilter(F paramF)
    {
        removeFilterInternal(paramF);
        this.mFilters.remove(paramF);
    }

    void removeFilterInternal(F paramF)
    {
        int i = unregister_intent_filter(paramF, paramF.schemesIterator(), this.mSchemeToFilter, "            Scheme: ");
        int j = unregister_mime_types(paramF, "            Type: ");
        if ((i == 0) && (j == 0))
            unregister_intent_filter(paramF, paramF.actionsIterator(), this.mActionToFilter, "            Action: ");
        if (j != 0)
            unregister_intent_filter(paramF, paramF.actionsIterator(), this.mTypedActionToFilter, "            TypedAction: ");
    }

    protected void sortResults(List<R> paramList)
    {
        Collections.sort(paramList, mResolvePrioritySorter);
    }

    private class IteratorWrapper
        implements Iterator<F>
    {
        private F mCur;
        private final Iterator<F> mI;

        IteratorWrapper()
        {
            Object localObject;
            this.mI = localObject;
        }

        public boolean hasNext()
        {
            return this.mI.hasNext();
        }

        public F next()
        {
            IntentFilter localIntentFilter = (IntentFilter)this.mI.next();
            this.mCur = localIntentFilter;
            return localIntentFilter;
        }

        public void remove()
        {
            if (this.mCur != null)
                IntentResolver.this.removeFilterInternal(this.mCur);
            this.mI.remove();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.IntentResolver
 * JD-Core Version:        0.6.2
 */