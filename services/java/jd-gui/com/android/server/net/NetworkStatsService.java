package com.android.server.net;

import android.app.IAlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.IConnectivityManager;
import android.net.INetworkManagementEventObserver;
import android.net.INetworkStatsService.Stub;
import android.net.INetworkStatsSession;
import android.net.INetworkStatsSession.Stub;
import android.net.LinkProperties;
import android.net.NetworkIdentity;
import android.net.NetworkInfo;
import android.net.NetworkState;
import android.net.NetworkStats;
import android.net.NetworkStats.Entry;
import android.net.NetworkStats.NonMonotonicObserver;
import android.net.NetworkStatsHistory;
import android.net.NetworkTemplate;
import android.os.Binder;
import android.os.DropBoxManager;
import android.os.Environment;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.INetworkManagementService;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.RemoteException;
import android.os.SystemClock;
import android.provider.Settings.Secure;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.MathUtils;
import android.util.NtpTrustedTime;
import android.util.Slog;
import android.util.SparseIntArray;
import android.util.TrustedTime;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.FileRotator;
import com.android.internal.util.IndentingPrintWriter;
import com.android.internal.util.Preconditions;
import com.android.server.EventLogTags;
import com.android.server.NetworkManagementSocketTagger;
import com.google.android.collect.Maps;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class NetworkStatsService extends INetworkStatsService.Stub
{
    public static final String ACTION_NETWORK_STATS_POLL = "com.android.server.action.NETWORK_STATS_POLL";
    public static final String ACTION_NETWORK_STATS_UPDATED = "com.android.server.action.NETWORK_STATS_UPDATED";
    private static final int FLAG_PERSIST_ALL = 3;
    private static final int FLAG_PERSIST_FORCE = 256;
    private static final int FLAG_PERSIST_NETWORK = 1;
    private static final int FLAG_PERSIST_UID = 2;
    private static final boolean LOGV = false;
    private static final int MSG_PERFORM_POLL = 1;
    private static final int MSG_REGISTER_GLOBAL_ALERT = 3;
    private static final int MSG_UPDATE_IFACES = 2;
    private static final String PREFIX_DEV = "dev";
    private static final String PREFIX_UID = "uid";
    private static final String PREFIX_UID_TAG = "uid_tag";
    private static final String PREFIX_XT = "xt";
    private static final String TAG = "NetworkStats";
    private static final String TAG_NETSTATS_ERROR = "netstats_error";
    private String mActiveIface;
    private HashMap<String, NetworkIdentitySet> mActiveIfaces = Maps.newHashMap();
    private SparseIntArray mActiveUidCounterSet = new SparseIntArray();
    private final IAlarmManager mAlarmManager;
    private INetworkManagementEventObserver mAlertObserver = new NetworkAlertObserver()
    {
        public void limitReached(String paramAnonymousString1, String paramAnonymousString2)
        {
            NetworkStatsService.this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkStats");
            if ("globalAlert".equals(paramAnonymousString1))
            {
                NetworkStatsService.this.mHandler.obtainMessage(1, 1, 0).sendToTarget();
                NetworkStatsService.this.mHandler.obtainMessage(3).sendToTarget();
            }
        }
    };
    private final File mBaseDir;
    private IConnectivityManager mConnManager;
    private BroadcastReceiver mConnReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            NetworkStatsService.this.updateIfaces();
        }
    };
    private final Context mContext;
    private NetworkStatsRecorder mDevRecorder;
    private NetworkStatsCollection mDevStatsCached;
    private long mGlobalAlertBytes;
    private final Handler mHandler;
    private Handler.Callback mHandlerCallback = new Handler.Callback()
    {
        public boolean handleMessage(Message paramAnonymousMessage)
        {
            boolean bool = true;
            switch (paramAnonymousMessage.what)
            {
            default:
                bool = false;
            case 1:
            case 2:
            case 3:
            }
            while (true)
            {
                return bool;
                int i = paramAnonymousMessage.arg1;
                NetworkStatsService.this.performPoll(i);
                continue;
                NetworkStatsService.this.updateIfaces();
                continue;
                NetworkStatsService.this.registerGlobalAlert();
            }
        }
    };
    private final HandlerThread mHandlerThread;
    private int mLastPhoneNetworkType = 0;
    private int mLastPhoneState = -1;
    private String[] mMobileIfaces = new String[0];
    private final INetworkManagementService mNetworkManager;
    private final DropBoxNonMonotonicObserver mNonMonotonicObserver = new DropBoxNonMonotonicObserver(null);
    private long mPersistThreshold = 2097152L;
    private PhoneStateListener mPhoneListener = new PhoneStateListener()
    {
        public void onDataConnectionStateChanged(int paramAnonymousInt1, int paramAnonymousInt2)
        {
            int i;
            if (paramAnonymousInt1 != NetworkStatsService.this.mLastPhoneState)
            {
                i = 1;
                if (paramAnonymousInt2 == NetworkStatsService.this.mLastPhoneNetworkType)
                    break label85;
            }
            label85: for (int j = 1; ; j = 0)
            {
                if ((j != 0) && (i == 0))
                    NetworkStatsService.this.mHandler.sendMessageDelayed(NetworkStatsService.this.mHandler.obtainMessage(2), 1000L);
                NetworkStatsService.access$1402(NetworkStatsService.this, paramAnonymousInt1);
                NetworkStatsService.access$1502(NetworkStatsService.this, paramAnonymousInt2);
                return;
                i = 0;
                break;
            }
        }
    };
    private PendingIntent mPollIntent;
    private BroadcastReceiver mPollReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            NetworkStatsService.this.performPoll(3);
            NetworkStatsService.this.registerGlobalAlert();
        }
    };
    private BroadcastReceiver mRemovedReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            int i = paramAnonymousIntent.getIntExtra("android.intent.extra.UID", 0);
            synchronized (NetworkStatsService.this.mStatsLock)
            {
                NetworkStatsService.this.mWakeLock.acquire();
                try
                {
                    NetworkStatsService.this.removeUidLocked(i);
                    NetworkStatsService.this.mWakeLock.release();
                    return;
                }
                finally
                {
                    localObject3 = finally;
                    NetworkStatsService.this.mWakeLock.release();
                    throw localObject3;
                }
            }
        }
    };
    private final NetworkStatsSettings mSettings;
    private BroadcastReceiver mShutdownReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            synchronized (NetworkStatsService.this.mStatsLock)
            {
                NetworkStatsService.this.shutdownLocked();
                return;
            }
        }
    };
    private final Object mStatsLock = new Object();
    private final File mSystemDir;
    private boolean mSystemReady;
    private final TelephonyManager mTeleManager;
    private BroadcastReceiver mTetherReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            NetworkStatsService.this.performPoll(1);
        }
    };
    private final TrustedTime mTime;
    private NetworkStats mUidOperations = new NetworkStats(0L, 10);
    private NetworkStatsRecorder mUidRecorder;
    private NetworkStatsRecorder mUidTagRecorder;
    private final PowerManager.WakeLock mWakeLock;
    private NetworkStatsRecorder mXtRecorder;
    private NetworkStatsCollection mXtStatsCached;

    public NetworkStatsService(Context paramContext, INetworkManagementService paramINetworkManagementService, IAlarmManager paramIAlarmManager)
    {
        this(paramContext, paramINetworkManagementService, paramIAlarmManager, NtpTrustedTime.getInstance(paramContext), getDefaultSystemDir(), new DefaultNetworkStatsSettings(paramContext));
    }

    public NetworkStatsService(Context paramContext, INetworkManagementService paramINetworkManagementService, IAlarmManager paramIAlarmManager, TrustedTime paramTrustedTime, File paramFile, NetworkStatsSettings paramNetworkStatsSettings)
    {
        this.mContext = ((Context)Preconditions.checkNotNull(paramContext, "missing Context"));
        this.mNetworkManager = ((INetworkManagementService)Preconditions.checkNotNull(paramINetworkManagementService, "missing INetworkManagementService"));
        this.mAlarmManager = ((IAlarmManager)Preconditions.checkNotNull(paramIAlarmManager, "missing IAlarmManager"));
        this.mTime = ((TrustedTime)Preconditions.checkNotNull(paramTrustedTime, "missing TrustedTime"));
        this.mTeleManager = ((TelephonyManager)Preconditions.checkNotNull(TelephonyManager.getDefault(), "missing TelephonyManager"));
        this.mSettings = ((NetworkStatsSettings)Preconditions.checkNotNull(paramNetworkStatsSettings, "missing NetworkStatsSettings"));
        this.mWakeLock = ((PowerManager)paramContext.getSystemService("power")).newWakeLock(1, "NetworkStats");
        this.mHandlerThread = new HandlerThread("NetworkStats");
        this.mHandlerThread.start();
        this.mHandler = new Handler(this.mHandlerThread.getLooper(), this.mHandlerCallback);
        this.mSystemDir = ((File)Preconditions.checkNotNull(paramFile));
        this.mBaseDir = new File(paramFile, "netstats");
        this.mBaseDir.mkdirs();
    }

    private void assertBandwidthControlEnabled()
    {
        if (!isBandwidthControlEnabled())
            throw new IllegalStateException("Bandwidth module disabled");
    }

    private void bootstrapStatsLocked()
    {
        long l;
        if (this.mTime.hasCache())
            l = this.mTime.currentTimeMillis();
        try
        {
            while (true)
            {
                NetworkStats localNetworkStats1 = getNetworkStatsUidDetail();
                NetworkStats localNetworkStats2 = this.mNetworkManager.getNetworkStatsSummaryXt();
                NetworkStats localNetworkStats3 = this.mNetworkManager.getNetworkStatsSummaryDev();
                this.mDevRecorder.recordSnapshotLocked(localNetworkStats3, this.mActiveIfaces, l);
                this.mXtRecorder.recordSnapshotLocked(localNetworkStats2, this.mActiveIfaces, l);
                this.mUidRecorder.recordSnapshotLocked(localNetworkStats1, this.mActiveIfaces, l);
                this.mUidTagRecorder.recordSnapshotLocked(localNetworkStats1, this.mActiveIfaces, l);
                label106: return;
                l = System.currentTimeMillis();
            }
        }
        catch (IllegalStateException localIllegalStateException)
        {
            while (true)
                Slog.w("NetworkStats", "problem reading network stats: " + localIllegalStateException);
        }
        catch (RemoteException localRemoteException)
        {
            break label106;
        }
    }

    private NetworkStatsRecorder buildRecorder(String paramString, NetworkStatsService.NetworkStatsSettings.Config paramConfig, boolean paramBoolean)
    {
        DropBoxManager localDropBoxManager = (DropBoxManager)this.mContext.getSystemService("dropbox");
        NetworkStatsRecorder localNetworkStatsRecorder = new NetworkStatsRecorder(new FileRotator(this.mBaseDir, paramString, paramConfig.rotateAgeMillis, paramConfig.deleteAgeMillis), this.mNonMonotonicObserver, localDropBoxManager, paramString, paramConfig.bucketDuration, paramBoolean);
        return localNetworkStatsRecorder;
    }

    private static File getDefaultSystemDir()
    {
        return new File(Environment.getDataDirectory(), "system");
    }

    private NetworkStats getNetworkStatsTethering()
        throws RemoteException
    {
        try
        {
            String[] arrayOfString = this.mConnManager.getTetheredIfacePairs();
            NetworkStats localNetworkStats2 = this.mNetworkManager.getNetworkStatsTethering(arrayOfString);
            localNetworkStats1 = localNetworkStats2;
            return localNetworkStats1;
        }
        catch (IllegalStateException localIllegalStateException)
        {
            while (true)
            {
                Log.wtf("NetworkStats", "problem reading network stats", localIllegalStateException);
                NetworkStats localNetworkStats1 = new NetworkStats(0L, 10);
            }
        }
    }

    private NetworkStats getNetworkStatsUidDetail()
        throws RemoteException
    {
        NetworkStats localNetworkStats = this.mNetworkManager.getNetworkStatsUidDetail(-1);
        localNetworkStats.combineAllValues(getNetworkStatsTethering());
        localNetworkStats.combineAllValues(this.mUidOperations);
        return localNetworkStats;
    }

    private NetworkStatsHistory internalGetHistoryForNetwork(NetworkTemplate paramNetworkTemplate, int paramInt)
    {
        NetworkStatsHistory localNetworkStatsHistory2;
        if (!this.mSettings.getReportXtOverDev())
            localNetworkStatsHistory2 = this.mDevStatsCached.getHistory(paramNetworkTemplate, -1, -1, 0, paramInt);
        while (true)
        {
            return localNetworkStatsHistory2;
            long l = this.mXtStatsCached.getFirstAtomicBucketMillis();
            NetworkStatsHistory localNetworkStatsHistory1 = this.mDevStatsCached.getHistory(paramNetworkTemplate, -1, -1, 0, paramInt, -9223372036854775808L, l);
            localNetworkStatsHistory2 = this.mXtStatsCached.getHistory(paramNetworkTemplate, -1, -1, 0, paramInt, l, 9223372036854775807L);
            localNetworkStatsHistory2.recordEntireHistory(localNetworkStatsHistory1);
        }
    }

    private NetworkStats internalGetSummaryForNetwork(NetworkTemplate paramNetworkTemplate, long paramLong1, long paramLong2)
    {
        NetworkStats localNetworkStats2;
        if (!this.mSettings.getReportXtOverDev())
            localNetworkStats2 = this.mDevStatsCached.getSummary(paramNetworkTemplate, paramLong1, paramLong2);
        while (true)
        {
            return localNetworkStats2;
            long l = this.mXtStatsCached.getFirstAtomicBucketMillis();
            NetworkStats localNetworkStats1 = this.mDevStatsCached.getSummary(paramNetworkTemplate, Math.min(paramLong1, l), Math.min(paramLong2, l));
            localNetworkStats2 = this.mXtStatsCached.getSummary(paramNetworkTemplate, Math.max(paramLong1, l), Math.max(paramLong2, l));
            localNetworkStats2.combineAllValues(localNetworkStats1);
        }
    }

    // ERROR //
    private boolean isBandwidthControlEnabled()
    {
        // Byte code:
        //     0: invokestatic 548	android/os/Binder:clearCallingIdentity	()J
        //     3: lstore_1
        //     4: aload_0
        //     5: getfield 243	com/android/server/net/NetworkStatsService:mNetworkManager	Landroid/os/INetworkManagementService;
        //     8: invokeinterface 549 1 0
        //     13: istore 6
        //     15: iload 6
        //     17: istore 5
        //     19: lload_1
        //     20: invokestatic 553	android/os/Binder:restoreCallingIdentity	(J)V
        //     23: iload 5
        //     25: ireturn
        //     26: astore 4
        //     28: iconst_0
        //     29: istore 5
        //     31: goto -12 -> 19
        //     34: astore_3
        //     35: lload_1
        //     36: invokestatic 553	android/os/Binder:restoreCallingIdentity	(J)V
        //     39: aload_3
        //     40: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     4	15	26	android/os/RemoteException
        //     4	15	34	finally
    }

    private void maybeUpgradeLegacyStatsLocked()
    {
        try
        {
            File localFile1 = new File(this.mSystemDir, "netstats.bin");
            if (localFile1.exists())
            {
                this.mDevRecorder.importLegacyNetworkLocked(localFile1);
                localFile1.delete();
            }
            File localFile2 = new File(this.mSystemDir, "netstats_xt.bin");
            if (localFile2.exists())
                localFile2.delete();
            File localFile3 = new File(this.mSystemDir, "netstats_uid.bin");
            if (localFile3.exists())
            {
                this.mUidRecorder.importLegacyUidLocked(localFile3);
                this.mUidTagRecorder.importLegacyUidLocked(localFile3);
                localFile3.delete();
            }
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
                Log.wtf("NetworkStats", "problem during legacy upgrade", localIOException);
        }
    }

    private void performPoll(int paramInt)
    {
        synchronized (this.mStatsLock)
        {
            this.mWakeLock.acquire();
            if (this.mTime.getCacheAge() > this.mSettings.getTimeCacheMaxAge())
                this.mTime.forceRefresh();
            try
            {
                performPollLocked(paramInt);
                this.mWakeLock.release();
                return;
            }
            finally
            {
                localObject3 = finally;
                this.mWakeLock.release();
                throw localObject3;
            }
        }
    }

    private void performPollLocked(int paramInt)
    {
        int i = 1;
        if (!this.mSystemReady)
            return;
        SystemClock.elapsedRealtime();
        int j;
        label23: int k;
        label32: label40: long l;
        if ((paramInt & 0x1) != 0)
        {
            j = i;
            if ((paramInt & 0x2) == 0)
                break label248;
            k = i;
            if ((paramInt & 0x100) == 0)
                break label254;
            if (!this.mTime.hasCache())
                break label259;
            l = this.mTime.currentTimeMillis();
        }
        while (true)
        {
            try
            {
                NetworkStats localNetworkStats1 = getNetworkStatsUidDetail();
                NetworkStats localNetworkStats2 = this.mNetworkManager.getNetworkStatsSummaryXt();
                NetworkStats localNetworkStats3 = this.mNetworkManager.getNetworkStatsSummaryDev();
                this.mDevRecorder.recordSnapshotLocked(localNetworkStats3, this.mActiveIfaces, l);
                this.mXtRecorder.recordSnapshotLocked(localNetworkStats2, this.mActiveIfaces, l);
                this.mUidRecorder.recordSnapshotLocked(localNetworkStats1, this.mActiveIfaces, l);
                this.mUidTagRecorder.recordSnapshotLocked(localNetworkStats1, this.mActiveIfaces, l);
                if (i == 0)
                    break label288;
                this.mDevRecorder.forcePersistLocked(l);
                this.mXtRecorder.forcePersistLocked(l);
                this.mUidRecorder.forcePersistLocked(l);
                this.mUidTagRecorder.forcePersistLocked(l);
                if (this.mSettings.getSampleEnabled())
                    performSampleLocked();
                Intent localIntent = new Intent("com.android.server.action.NETWORK_STATS_UPDATED");
                localIntent.setFlags(1073741824);
                this.mContext.sendBroadcast(localIntent, "android.permission.READ_NETWORK_USAGE_HISTORY");
                break;
                j = 0;
                break label23;
                label248: k = 0;
                break label32;
                label254: i = 0;
                break label40;
                label259: l = System.currentTimeMillis();
                continue;
            }
            catch (IllegalStateException localIllegalStateException)
            {
                Log.wtf("NetworkStats", "problem reading network stats", localIllegalStateException);
            }
            catch (RemoteException localRemoteException)
            {
            }
            break;
            label288: if (j != 0)
            {
                this.mDevRecorder.maybePersistLocked(l);
                this.mXtRecorder.maybePersistLocked(l);
            }
            if (k != 0)
            {
                this.mUidRecorder.maybePersistLocked(l);
                this.mUidTagRecorder.maybePersistLocked(l);
            }
        }
    }

    private void performSampleLocked()
    {
        if (this.mTime.hasCache());
        for (long l = this.mTime.currentTimeMillis(); ; l = -1L)
        {
            NetworkTemplate localNetworkTemplate1 = NetworkTemplate.buildTemplateMobileWildcard();
            NetworkStats.Entry localEntry1 = this.mDevRecorder.getTotalSinceBootLocked(localNetworkTemplate1);
            NetworkStats.Entry localEntry2 = this.mXtRecorder.getTotalSinceBootLocked(localNetworkTemplate1);
            NetworkStats.Entry localEntry3 = this.mUidRecorder.getTotalSinceBootLocked(localNetworkTemplate1);
            EventLogTags.writeNetstatsMobileSample(localEntry1.rxBytes, localEntry1.rxPackets, localEntry1.txBytes, localEntry1.txPackets, localEntry2.rxBytes, localEntry2.rxPackets, localEntry2.txBytes, localEntry2.txPackets, localEntry3.rxBytes, localEntry3.rxPackets, localEntry3.txBytes, localEntry3.txPackets, l);
            NetworkTemplate localNetworkTemplate2 = NetworkTemplate.buildTemplateWifiWildcard();
            NetworkStats.Entry localEntry4 = this.mDevRecorder.getTotalSinceBootLocked(localNetworkTemplate2);
            NetworkStats.Entry localEntry5 = this.mXtRecorder.getTotalSinceBootLocked(localNetworkTemplate2);
            NetworkStats.Entry localEntry6 = this.mUidRecorder.getTotalSinceBootLocked(localNetworkTemplate2);
            EventLogTags.writeNetstatsWifiSample(localEntry4.rxBytes, localEntry4.rxPackets, localEntry4.txBytes, localEntry4.txPackets, localEntry5.rxBytes, localEntry5.rxPackets, localEntry5.txBytes, localEntry5.txPackets, localEntry6.rxBytes, localEntry6.rxPackets, localEntry6.txBytes, localEntry6.txPackets, l);
            return;
        }
    }

    private void registerGlobalAlert()
    {
        try
        {
            this.mNetworkManager.setGlobalAlert(this.mGlobalAlertBytes);
            label13: return;
        }
        catch (IllegalStateException localIllegalStateException)
        {
            while (true)
                Slog.w("NetworkStats", "problem registering for global alert: " + localIllegalStateException);
        }
        catch (RemoteException localRemoteException)
        {
            break label13;
        }
    }

    private void registerPollAlarmLocked()
    {
        try
        {
            if (this.mPollIntent != null)
                this.mAlarmManager.remove(this.mPollIntent);
            this.mPollIntent = PendingIntent.getBroadcast(this.mContext, 0, new Intent("com.android.server.action.NETWORK_STATS_POLL"), 0);
            long l = SystemClock.elapsedRealtime();
            this.mAlarmManager.setInexactRepeating(3, l, this.mSettings.getPollInterval(), this.mPollIntent);
            label70: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label70;
        }
    }

    private void removeUidLocked(int paramInt)
    {
        performPollLocked(3);
        this.mUidRecorder.removeUidLocked(paramInt);
        this.mUidTagRecorder.removeUidLocked(paramInt);
        NetworkManagementSocketTagger.resetKernelUidStats(paramInt);
    }

    private void shutdownLocked()
    {
        this.mContext.unregisterReceiver(this.mConnReceiver);
        this.mContext.unregisterReceiver(this.mTetherReceiver);
        this.mContext.unregisterReceiver(this.mPollReceiver);
        this.mContext.unregisterReceiver(this.mRemovedReceiver);
        this.mContext.unregisterReceiver(this.mShutdownReceiver);
        if (this.mTime.hasCache());
        for (long l = this.mTime.currentTimeMillis(); ; l = System.currentTimeMillis())
        {
            this.mDevRecorder.forcePersistLocked(l);
            this.mXtRecorder.forcePersistLocked(l);
            this.mUidRecorder.forcePersistLocked(l);
            this.mUidTagRecorder.forcePersistLocked(l);
            this.mDevRecorder = null;
            this.mXtRecorder = null;
            this.mUidRecorder = null;
            this.mUidTagRecorder = null;
            this.mDevStatsCached = null;
            this.mXtStatsCached = null;
            this.mSystemReady = false;
            return;
        }
    }

    private void updateIfaces()
    {
        synchronized (this.mStatsLock)
        {
            this.mWakeLock.acquire();
            try
            {
                updateIfacesLocked();
                this.mWakeLock.release();
                return;
            }
            finally
            {
                localObject3 = finally;
                this.mWakeLock.release();
                throw localObject3;
            }
        }
    }

    private void updateIfacesLocked()
    {
        if (!this.mSystemReady)
            return;
        performPollLocked(1);
        while (true)
        {
            try
            {
                NetworkState[] arrayOfNetworkState = this.mConnManager.getAllNetworkState();
                LinkProperties localLinkProperties = this.mConnManager.getActiveLinkProperties();
                if (localLinkProperties == null)
                    break label206;
                str1 = localLinkProperties.getInterfaceName();
                this.mActiveIface = str1;
                this.mActiveIfaces.clear();
                int i = arrayOfNetworkState.length;
                int j = 0;
                if (j >= i)
                    break;
                NetworkState localNetworkState = arrayOfNetworkState[j];
                if (localNetworkState.networkInfo.isConnected())
                {
                    String str2 = localNetworkState.linkProperties.getInterfaceName();
                    NetworkIdentitySet localNetworkIdentitySet = (NetworkIdentitySet)this.mActiveIfaces.get(str2);
                    if (localNetworkIdentitySet == null)
                    {
                        localNetworkIdentitySet = new NetworkIdentitySet();
                        this.mActiveIfaces.put(str2, localNetworkIdentitySet);
                    }
                    localNetworkIdentitySet.add(NetworkIdentity.buildNetworkIdentity(this.mContext, localNetworkState));
                    if ((ConnectivityManager.isNetworkTypeMobile(localNetworkState.networkInfo.getType())) && (!ArrayUtils.contains(this.mMobileIfaces, str2)))
                        this.mMobileIfaces = ((String[])ArrayUtils.appendElement(String.class, this.mMobileIfaces, str2));
                }
                j++;
                continue;
            }
            catch (RemoteException localRemoteException)
            {
            }
            break;
            label206: String str1 = null;
        }
    }

    private void updatePersistThresholds()
    {
        this.mDevRecorder.setPersistThreshold(this.mSettings.getDevPersistBytes(this.mPersistThreshold));
        this.mXtRecorder.setPersistThreshold(this.mSettings.getXtPersistBytes(this.mPersistThreshold));
        this.mUidRecorder.setPersistThreshold(this.mSettings.getUidPersistBytes(this.mPersistThreshold));
        this.mUidTagRecorder.setPersistThreshold(this.mSettings.getUidTagPersistBytes(this.mPersistThreshold));
        this.mGlobalAlertBytes = this.mSettings.getGlobalAlertBytes(this.mPersistThreshold);
    }

    public void advisePersistThreshold(long paramLong)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.MODIFY_NETWORK_ACCOUNTING", "NetworkStats");
        assertBandwidthControlEnabled();
        this.mPersistThreshold = MathUtils.constrain(paramLong, 131072L, 2097152L);
        long l;
        if (this.mTime.hasCache())
            l = this.mTime.currentTimeMillis();
        synchronized (this.mStatsLock)
        {
            while (!this.mSystemReady)
            {
                return;
                l = System.currentTimeMillis();
            }
            updatePersistThresholds();
            this.mDevRecorder.maybePersistLocked(l);
            this.mXtRecorder.maybePersistLocked(l);
            this.mUidRecorder.maybePersistLocked(l);
            this.mUidTagRecorder.maybePersistLocked(l);
            registerGlobalAlert();
        }
    }

    public void bindConnectivityManager(IConnectivityManager paramIConnectivityManager)
    {
        this.mConnManager = ((IConnectivityManager)Preconditions.checkNotNull(paramIConnectivityManager, "missing IConnectivityManager"));
    }

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.DUMP", "NetworkStats");
        HashSet localHashSet = new HashSet();
        int i = paramArrayOfString.length;
        for (int j = 0; j < i; j++)
            localHashSet.add(paramArrayOfString[j]);
        int k;
        boolean bool1;
        boolean bool2;
        label111: int m;
        if ((localHashSet.contains("--poll")) || (localHashSet.contains("poll")))
        {
            k = 1;
            bool1 = localHashSet.contains("--checkin");
            if ((!localHashSet.contains("--full")) && (!localHashSet.contains("full")))
                break label526;
            bool2 = true;
            if ((!localHashSet.contains("--uid")) && (!localHashSet.contains("detail")))
                break label532;
            m = 1;
            label136: if ((!localHashSet.contains("--tag")) && (!localHashSet.contains("detail")))
                break label538;
        }
        label519: label526: label532: label538: for (int n = 1; ; n = 0)
        {
            IndentingPrintWriter localIndentingPrintWriter = new IndentingPrintWriter(paramPrintWriter, "    ");
            Object localObject1 = this.mStatsLock;
            if (k != 0);
            try
            {
                performPollLocked(259);
                localIndentingPrintWriter.println("Forced poll");
                break label519;
                if (bool1)
                {
                    localIndentingPrintWriter.println("Current files:");
                    localIndentingPrintWriter.increaseIndent();
                    String[] arrayOfString = this.mBaseDir.list();
                    int i1 = arrayOfString.length;
                    for (int i2 = 0; i2 < i1; i2++)
                        localIndentingPrintWriter.println(arrayOfString[i2]);
                    localIndentingPrintWriter.decreaseIndent();
                }
            }
            finally
            {
                throw localObject2;
                localIndentingPrintWriter.println("Active interfaces:");
                localIndentingPrintWriter.increaseIndent();
                Iterator localIterator = this.mActiveIfaces.keySet().iterator();
                while (localIterator.hasNext())
                {
                    String str = (String)localIterator.next();
                    NetworkIdentitySet localNetworkIdentitySet = (NetworkIdentitySet)this.mActiveIfaces.get(str);
                    localIndentingPrintWriter.print("iface=");
                    localIndentingPrintWriter.print(str);
                    localIndentingPrintWriter.print(" ident=");
                    localIndentingPrintWriter.println(localNetworkIdentitySet.toString());
                }
                localIndentingPrintWriter.decreaseIndent();
                localIndentingPrintWriter.println("Dev stats:");
                localIndentingPrintWriter.increaseIndent();
                this.mDevRecorder.dumpLocked(localIndentingPrintWriter, bool2);
                localIndentingPrintWriter.decreaseIndent();
                localIndentingPrintWriter.println("Xt stats:");
                localIndentingPrintWriter.increaseIndent();
                this.mXtRecorder.dumpLocked(localIndentingPrintWriter, bool2);
                localIndentingPrintWriter.decreaseIndent();
                if (m != 0)
                {
                    localIndentingPrintWriter.println("UID stats:");
                    localIndentingPrintWriter.increaseIndent();
                    this.mUidRecorder.dumpLocked(localIndentingPrintWriter, bool2);
                    localIndentingPrintWriter.decreaseIndent();
                }
                if (n != 0)
                {
                    localIndentingPrintWriter.println("UID tag stats:");
                    localIndentingPrintWriter.increaseIndent();
                    this.mUidTagRecorder.dumpLocked(localIndentingPrintWriter, bool2);
                    localIndentingPrintWriter.decreaseIndent();
                }
            }
            k = 0;
            break;
            bool2 = false;
            break label111;
            m = 0;
            break label136;
        }
    }

    public void forceUpdate()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.READ_NETWORK_USAGE_HISTORY", "NetworkStats");
        assertBandwidthControlEnabled();
        long l = Binder.clearCallingIdentity();
        try
        {
            performPoll(3);
            return;
        }
        finally
        {
            Binder.restoreCallingIdentity(l);
        }
    }

    public NetworkStats getDataLayerSnapshotForUid(int paramInt)
        throws RemoteException
    {
        if (Binder.getCallingUid() != paramInt)
            this.mContext.enforceCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE", "NetworkStats");
        assertBandwidthControlEnabled();
        long l = Binder.clearCallingIdentity();
        NetworkStats localNetworkStats2;
        try
        {
            NetworkStats localNetworkStats1 = this.mNetworkManager.getNetworkStatsUidDetail(paramInt);
            Binder.restoreCallingIdentity(l);
            localNetworkStats1.spliceOperationsFrom(this.mUidOperations);
            localNetworkStats2 = new NetworkStats(localNetworkStats1.getElapsedRealtime(), localNetworkStats1.size());
            NetworkStats.Entry localEntry = null;
            int i = 0;
            if (i < localNetworkStats1.size())
            {
                localEntry = localNetworkStats1.getValues(i, localEntry);
                localEntry.iface = NetworkStats.IFACE_ALL;
                localNetworkStats2.combineValues(localEntry);
                i++;
            }
        }
        finally
        {
            Binder.restoreCallingIdentity(l);
        }
        return localNetworkStats2;
    }

    public String[] getMobileIfaces()
    {
        return this.mMobileIfaces;
    }

    public long getNetworkTotalBytes(NetworkTemplate paramNetworkTemplate, long paramLong1, long paramLong2)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.READ_NETWORK_USAGE_HISTORY", "NetworkStats");
        assertBandwidthControlEnabled();
        return internalGetSummaryForNetwork(paramNetworkTemplate, paramLong1, paramLong2).getTotalBytes();
    }

    public void incrementOperationCount(int paramInt1, int paramInt2, int paramInt3)
    {
        if (Binder.getCallingUid() != paramInt1)
            this.mContext.enforceCallingOrSelfPermission("android.permission.MODIFY_NETWORK_ACCOUNTING", "NetworkStats");
        if (paramInt3 < 0)
            throw new IllegalArgumentException("operation count can only be incremented");
        if (paramInt2 == 0)
            throw new IllegalArgumentException("operation count must have specific tag");
        synchronized (this.mStatsLock)
        {
            int i = this.mActiveUidCounterSet.get(paramInt1, 0);
            this.mUidOperations.combineValues(this.mActiveIface, paramInt1, i, paramInt2, 0L, 0L, 0L, 0L, paramInt3);
            this.mUidOperations.combineValues(this.mActiveIface, paramInt1, i, 0, 0L, 0L, 0L, 0L, paramInt3);
            return;
        }
    }

    public INetworkStatsSession openSession()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.READ_NETWORK_USAGE_HISTORY", "NetworkStats");
        assertBandwidthControlEnabled();
        return new INetworkStatsSession.Stub()
        {
            private NetworkStatsCollection mUidComplete;
            private NetworkStatsCollection mUidTagComplete;

            private NetworkStatsCollection getUidComplete()
            {
                if (this.mUidComplete == null);
                synchronized (NetworkStatsService.this.mStatsLock)
                {
                    this.mUidComplete = NetworkStatsService.this.mUidRecorder.getOrLoadCompleteLocked();
                    return this.mUidComplete;
                }
            }

            private NetworkStatsCollection getUidTagComplete()
            {
                if (this.mUidTagComplete == null);
                synchronized (NetworkStatsService.this.mStatsLock)
                {
                    this.mUidTagComplete = NetworkStatsService.this.mUidTagRecorder.getOrLoadCompleteLocked();
                    return this.mUidTagComplete;
                }
            }

            public void close()
            {
                this.mUidComplete = null;
                this.mUidTagComplete = null;
            }

            public NetworkStatsHistory getHistoryForNetwork(NetworkTemplate paramAnonymousNetworkTemplate, int paramAnonymousInt)
            {
                return NetworkStatsService.this.internalGetHistoryForNetwork(paramAnonymousNetworkTemplate, paramAnonymousInt);
            }

            public NetworkStatsHistory getHistoryForUid(NetworkTemplate paramAnonymousNetworkTemplate, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3, int paramAnonymousInt4)
            {
                if (paramAnonymousInt3 == 0);
                for (NetworkStatsHistory localNetworkStatsHistory = getUidComplete().getHistory(paramAnonymousNetworkTemplate, paramAnonymousInt1, paramAnonymousInt2, paramAnonymousInt3, paramAnonymousInt4); ; localNetworkStatsHistory = getUidTagComplete().getHistory(paramAnonymousNetworkTemplate, paramAnonymousInt1, paramAnonymousInt2, paramAnonymousInt3, paramAnonymousInt4))
                    return localNetworkStatsHistory;
            }

            public NetworkStats getSummaryForAllUid(NetworkTemplate paramAnonymousNetworkTemplate, long paramAnonymousLong1, long paramAnonymousLong2, boolean paramAnonymousBoolean)
            {
                NetworkStats localNetworkStats = getUidComplete().getSummary(paramAnonymousNetworkTemplate, paramAnonymousLong1, paramAnonymousLong2);
                if (paramAnonymousBoolean)
                    localNetworkStats.combineAllValues(getUidTagComplete().getSummary(paramAnonymousNetworkTemplate, paramAnonymousLong1, paramAnonymousLong2));
                return localNetworkStats;
            }

            public NetworkStats getSummaryForNetwork(NetworkTemplate paramAnonymousNetworkTemplate, long paramAnonymousLong1, long paramAnonymousLong2)
            {
                return NetworkStatsService.this.internalGetSummaryForNetwork(paramAnonymousNetworkTemplate, paramAnonymousLong1, paramAnonymousLong2);
            }
        };
    }

    public void setUidForeground(int paramInt, boolean paramBoolean)
    {
        int i = 0;
        this.mContext.enforceCallingOrSelfPermission("android.permission.MODIFY_NETWORK_ACCOUNTING", "NetworkStats");
        Object localObject1 = this.mStatsLock;
        if (paramBoolean)
            i = 1;
        try
        {
            if (this.mActiveUidCounterSet.get(paramInt, 0) != i)
            {
                this.mActiveUidCounterSet.put(paramInt, i);
                NetworkManagementSocketTagger.setKernelCounterSet(paramInt, i);
            }
            return;
        }
        finally
        {
            localObject2 = finally;
            throw localObject2;
        }
    }

    public void systemReady()
    {
        this.mSystemReady = true;
        if (!isBandwidthControlEnabled())
            Slog.w("NetworkStats", "bandwidth controls disabled, unable to track stats");
        while (true)
        {
            return;
            this.mDevRecorder = buildRecorder("dev", this.mSettings.getDevConfig(), false);
            this.mXtRecorder = buildRecorder("xt", this.mSettings.getXtConfig(), false);
            this.mUidRecorder = buildRecorder("uid", this.mSettings.getUidConfig(), false);
            this.mUidTagRecorder = buildRecorder("uid_tag", this.mSettings.getUidTagConfig(), true);
            updatePersistThresholds();
            synchronized (this.mStatsLock)
            {
                maybeUpgradeLegacyStatsLocked();
                this.mDevStatsCached = this.mDevRecorder.getOrLoadCompleteLocked();
                this.mXtStatsCached = this.mXtRecorder.getOrLoadCompleteLocked();
                bootstrapStatsLocked();
                IntentFilter localIntentFilter1 = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE");
                this.mContext.registerReceiver(this.mConnReceiver, localIntentFilter1, "android.permission.CONNECTIVITY_INTERNAL", this.mHandler);
                IntentFilter localIntentFilter2 = new IntentFilter("android.net.conn.TETHER_STATE_CHANGED");
                this.mContext.registerReceiver(this.mTetherReceiver, localIntentFilter2, "android.permission.CONNECTIVITY_INTERNAL", this.mHandler);
                IntentFilter localIntentFilter3 = new IntentFilter("com.android.server.action.NETWORK_STATS_POLL");
                this.mContext.registerReceiver(this.mPollReceiver, localIntentFilter3, "android.permission.READ_NETWORK_USAGE_HISTORY", this.mHandler);
                IntentFilter localIntentFilter4 = new IntentFilter("android.intent.action.UID_REMOVED");
                this.mContext.registerReceiver(this.mRemovedReceiver, localIntentFilter4, null, this.mHandler);
                IntentFilter localIntentFilter5 = new IntentFilter("android.intent.action.ACTION_SHUTDOWN");
                this.mContext.registerReceiver(this.mShutdownReceiver, localIntentFilter5);
            }
            try
            {
                this.mNetworkManager.registerObserver(this.mAlertObserver);
                label311: registerPollAlarmLocked();
                registerGlobalAlert();
                continue;
                localObject2 = finally;
                throw localObject2;
            }
            catch (RemoteException localRemoteException)
            {
                break label311;
            }
        }
    }

    private static class DefaultNetworkStatsSettings
        implements NetworkStatsService.NetworkStatsSettings
    {
        private final ContentResolver mResolver;

        public DefaultNetworkStatsSettings(Context paramContext)
        {
            this.mResolver = ((ContentResolver)Preconditions.checkNotNull(paramContext.getContentResolver()));
        }

        private boolean getSecureBoolean(String paramString, boolean paramBoolean)
        {
            int i = 1;
            if (paramBoolean)
            {
                int j = i;
                if (Settings.Secure.getInt(this.mResolver, paramString, j) == 0)
                    break label30;
            }
            while (true)
            {
                return i;
                int k = 0;
                break;
                label30: i = 0;
            }
        }

        private long getSecureLong(String paramString, long paramLong)
        {
            return Settings.Secure.getLong(this.mResolver, paramString, paramLong);
        }

        public NetworkStatsService.NetworkStatsSettings.Config getDevConfig()
        {
            return new NetworkStatsService.NetworkStatsSettings.Config(getSecureLong("netstats_dev_bucket_duration", 3600000L), getSecureLong("netstats_dev_rotate_age", 1296000000L), getSecureLong("netstats_dev_delete_age", 7776000000L));
        }

        public long getDevPersistBytes(long paramLong)
        {
            return getSecureLong("netstats_dev_persist_bytes", paramLong);
        }

        public long getGlobalAlertBytes(long paramLong)
        {
            return getSecureLong("netstats_global_alert_bytes", paramLong);
        }

        public long getPollInterval()
        {
            return getSecureLong("netstats_poll_interval", 1800000L);
        }

        public boolean getReportXtOverDev()
        {
            return getSecureBoolean("netstats_report_xt_over_dev", true);
        }

        public boolean getSampleEnabled()
        {
            return getSecureBoolean("netstats_sample_enabled", true);
        }

        public long getTimeCacheMaxAge()
        {
            return getSecureLong("netstats_time_cache_max_age", 86400000L);
        }

        public NetworkStatsService.NetworkStatsSettings.Config getUidConfig()
        {
            return new NetworkStatsService.NetworkStatsSettings.Config(getSecureLong("netstats_uid_bucket_duration", 7200000L), getSecureLong("netstats_uid_rotate_age", 1296000000L), getSecureLong("netstats_uid_delete_age", 7776000000L));
        }

        public long getUidPersistBytes(long paramLong)
        {
            return getSecureLong("netstats_uid_persist_bytes", paramLong);
        }

        public NetworkStatsService.NetworkStatsSettings.Config getUidTagConfig()
        {
            return new NetworkStatsService.NetworkStatsSettings.Config(getSecureLong("netstats_uid_tag_bucket_duration", 7200000L), getSecureLong("netstats_uid_tag_rotate_age", 432000000L), getSecureLong("netstats_uid_tag_delete_age", 1296000000L));
        }

        public long getUidTagPersistBytes(long paramLong)
        {
            return getSecureLong("netstats_uid_tag_persist_bytes", paramLong);
        }

        public NetworkStatsService.NetworkStatsSettings.Config getXtConfig()
        {
            return getDevConfig();
        }

        public long getXtPersistBytes(long paramLong)
        {
            return getDevPersistBytes(paramLong);
        }
    }

    private class DropBoxNonMonotonicObserver
        implements NetworkStats.NonMonotonicObserver<String>
    {
        private DropBoxNonMonotonicObserver()
        {
        }

        public void foundNonMonotonic(NetworkStats paramNetworkStats1, int paramInt1, NetworkStats paramNetworkStats2, int paramInt2, String paramString)
        {
            Log.w("NetworkStats", "found non-monotonic values; saving to dropbox");
            StringBuilder localStringBuilder = new StringBuilder();
            localStringBuilder.append("found non-monotonic " + paramString + " values at left[" + paramInt1 + "] - right[" + paramInt2 + "]\n");
            localStringBuilder.append("left=").append(paramNetworkStats1).append('\n');
            localStringBuilder.append("right=").append(paramNetworkStats2).append('\n');
            ((DropBoxManager)NetworkStatsService.this.mContext.getSystemService("dropbox")).addText("netstats_error", localStringBuilder.toString());
        }
    }

    public static abstract interface NetworkStatsSettings
    {
        public abstract Config getDevConfig();

        public abstract long getDevPersistBytes(long paramLong);

        public abstract long getGlobalAlertBytes(long paramLong);

        public abstract long getPollInterval();

        public abstract boolean getReportXtOverDev();

        public abstract boolean getSampleEnabled();

        public abstract long getTimeCacheMaxAge();

        public abstract Config getUidConfig();

        public abstract long getUidPersistBytes(long paramLong);

        public abstract Config getUidTagConfig();

        public abstract long getUidTagPersistBytes(long paramLong);

        public abstract Config getXtConfig();

        public abstract long getXtPersistBytes(long paramLong);

        public static class Config
        {
            public final long bucketDuration;
            public final long deleteAgeMillis;
            public final long rotateAgeMillis;

            public Config(long paramLong1, long paramLong2, long paramLong3)
            {
                this.bucketDuration = paramLong1;
                this.rotateAgeMillis = paramLong2;
                this.deleteAgeMillis = paramLong3;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.net.NetworkStatsService
 * JD-Core Version:        0.6.2
 */