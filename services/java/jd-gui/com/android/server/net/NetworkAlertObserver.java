package com.android.server.net;

import android.net.INetworkManagementEventObserver.Stub;

public abstract class NetworkAlertObserver extends INetworkManagementEventObserver.Stub
{
    public void interfaceAdded(String paramString)
    {
    }

    public void interfaceLinkStateChanged(String paramString, boolean paramBoolean)
    {
    }

    public void interfaceRemoved(String paramString)
    {
    }

    public void interfaceStatusChanged(String paramString, boolean paramBoolean)
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.net.NetworkAlertObserver
 * JD-Core Version:        0.6.2
 */