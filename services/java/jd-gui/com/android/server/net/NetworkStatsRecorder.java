package com.android.server.net;

import android.net.NetworkStats;
import android.net.NetworkStats.Entry;
import android.net.NetworkStats.NonMonotonicObserver;
import android.net.NetworkTemplate;
import android.os.DropBoxManager;
import android.util.Log;
import android.util.MathUtils;
import com.android.internal.util.FileRotator;
import com.android.internal.util.FileRotator.Rewriter;
import com.android.internal.util.IndentingPrintWriter;
import com.android.internal.util.Preconditions;
import com.google.android.collect.Sets;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Map;
import libcore.io.IoUtils;

public class NetworkStatsRecorder
{
    private static final boolean DUMP_BEFORE_DELETE = true;
    private static final boolean LOGD = false;
    private static final boolean LOGV = false;
    private static final String TAG = "NetworkStatsRecorder";
    private static final String TAG_NETSTATS_DUMP = "netstats_dump";
    private final long mBucketDuration;
    private WeakReference<NetworkStatsCollection> mComplete;
    private final String mCookie;
    private final DropBoxManager mDropBox;
    private NetworkStats mLastSnapshot;
    private final NetworkStats.NonMonotonicObserver<String> mObserver;
    private final boolean mOnlyTags;
    private final NetworkStatsCollection mPending;
    private final CombiningRewriter mPendingRewriter;
    private long mPersistThresholdBytes = 2097152L;
    private final FileRotator mRotator;
    private final NetworkStatsCollection mSinceBoot;

    public NetworkStatsRecorder(FileRotator paramFileRotator, NetworkStats.NonMonotonicObserver<String> paramNonMonotonicObserver, DropBoxManager paramDropBoxManager, String paramString, long paramLong, boolean paramBoolean)
    {
        this.mRotator = ((FileRotator)Preconditions.checkNotNull(paramFileRotator, "missing FileRotator"));
        this.mObserver = ((NetworkStats.NonMonotonicObserver)Preconditions.checkNotNull(paramNonMonotonicObserver, "missing NonMonotonicObserver"));
        this.mDropBox = ((DropBoxManager)Preconditions.checkNotNull(paramDropBoxManager, "missing DropBoxManager"));
        this.mCookie = paramString;
        this.mBucketDuration = paramLong;
        this.mOnlyTags = paramBoolean;
        this.mPending = new NetworkStatsCollection(paramLong);
        this.mSinceBoot = new NetworkStatsCollection(paramLong);
        this.mPendingRewriter = new CombiningRewriter(this.mPending);
    }

    private void recoverFromWtf()
    {
        ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
        try
        {
            this.mRotator.dumpAll(localByteArrayOutputStream);
            IoUtils.closeQuietly(localByteArrayOutputStream);
            this.mDropBox.addData("netstats_dump", localByteArrayOutputStream.toByteArray(), 0);
            this.mRotator.deleteAll();
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
                localByteArrayOutputStream.reset();
        }
        finally
        {
            IoUtils.closeQuietly(localByteArrayOutputStream);
        }
    }

    public void dumpLocked(IndentingPrintWriter paramIndentingPrintWriter, boolean paramBoolean)
    {
        paramIndentingPrintWriter.print("Pending bytes: ");
        paramIndentingPrintWriter.println(this.mPending.getTotalBytes());
        if (paramBoolean)
        {
            paramIndentingPrintWriter.println("Complete history:");
            getOrLoadCompleteLocked().dump(paramIndentingPrintWriter);
        }
        while (true)
        {
            return;
            paramIndentingPrintWriter.println("History since boot:");
            this.mSinceBoot.dump(paramIndentingPrintWriter);
        }
    }

    public void forcePersistLocked(long paramLong)
    {
        if (this.mPending.isDirty());
        try
        {
            this.mRotator.rewriteActive(this.mPendingRewriter, paramLong);
            this.mRotator.maybeRotate(paramLong);
            this.mPending.reset();
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
            {
                Log.wtf("NetworkStatsRecorder", "problem persisting pending stats", localIOException);
                recoverFromWtf();
            }
        }
    }

    // ERROR //
    public NetworkStatsCollection getOrLoadCompleteLocked()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 184	com/android/server/net/NetworkStatsRecorder:mComplete	Ljava/lang/ref/WeakReference;
        //     4: ifnull +66 -> 70
        //     7: aload_0
        //     8: getfield 184	com/android/server/net/NetworkStatsRecorder:mComplete	Ljava/lang/ref/WeakReference;
        //     11: invokevirtual 190	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
        //     14: checkcast 86	com/android/server/net/NetworkStatsCollection
        //     17: astore_1
        //     18: aload_1
        //     19: ifnonnull +79 -> 98
        //     22: new 86	com/android/server/net/NetworkStatsCollection
        //     25: dup
        //     26: aload_0
        //     27: getfield 82	com/android/server/net/NetworkStatsRecorder:mBucketDuration	J
        //     30: invokespecial 89	com/android/server/net/NetworkStatsCollection:<init>	(J)V
        //     33: astore_2
        //     34: aload_0
        //     35: getfield 66	com/android/server/net/NetworkStatsRecorder:mRotator	Lcom/android/internal/util/FileRotator;
        //     38: aload_2
        //     39: ldc2_w 191
        //     42: ldc2_w 193
        //     45: invokevirtual 198	com/android/internal/util/FileRotator:readMatching	(Lcom/android/internal/util/FileRotator$Reader;JJ)V
        //     48: aload_2
        //     49: aload_0
        //     50: getfield 91	com/android/server/net/NetworkStatsRecorder:mPending	Lcom/android/server/net/NetworkStatsCollection;
        //     53: invokevirtual 201	com/android/server/net/NetworkStatsCollection:recordCollection	(Lcom/android/server/net/NetworkStatsCollection;)V
        //     56: aload_0
        //     57: new 186	java/lang/ref/WeakReference
        //     60: dup
        //     61: aload_2
        //     62: invokespecial 204	java/lang/ref/WeakReference:<init>	(Ljava/lang/Object;)V
        //     65: putfield 184	com/android/server/net/NetworkStatsRecorder:mComplete	Ljava/lang/ref/WeakReference;
        //     68: aload_2
        //     69: areturn
        //     70: aconst_null
        //     71: astore_1
        //     72: goto -54 -> 18
        //     75: astore_3
        //     76: aload_1
        //     77: astore_2
        //     78: ldc 20
        //     80: ldc 206
        //     82: aload_3
        //     83: invokestatic 180	android/util/Log:wtf	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     86: pop
        //     87: aload_0
        //     88: invokespecial 182	com/android/server/net/NetworkStatsRecorder:recoverFromWtf	()V
        //     91: goto -23 -> 68
        //     94: astore_3
        //     95: goto -17 -> 78
        //     98: aload_1
        //     99: astore_2
        //     100: goto -32 -> 68
        //
        // Exception table:
        //     from	to	target	type
        //     22	34	75	java/io/IOException
        //     34	68	94	java/io/IOException
    }

    public NetworkStats.Entry getTotalSinceBootLocked(NetworkTemplate paramNetworkTemplate)
    {
        return this.mSinceBoot.getSummary(paramNetworkTemplate, -9223372036854775808L, 9223372036854775807L).getTotal(null);
    }

    public void importLegacyNetworkLocked(File paramFile)
        throws IOException
    {
        this.mRotator.deleteAll();
        NetworkStatsCollection localNetworkStatsCollection = new NetworkStatsCollection(this.mBucketDuration);
        localNetworkStatsCollection.readLegacyNetwork(paramFile);
        long l1 = localNetworkStatsCollection.getStartMillis();
        long l2 = localNetworkStatsCollection.getEndMillis();
        if (!localNetworkStatsCollection.isEmpty())
        {
            this.mRotator.rewriteActive(new CombiningRewriter(localNetworkStatsCollection), l1);
            this.mRotator.maybeRotate(l2);
        }
    }

    public void importLegacyUidLocked(File paramFile)
        throws IOException
    {
        this.mRotator.deleteAll();
        NetworkStatsCollection localNetworkStatsCollection = new NetworkStatsCollection(this.mBucketDuration);
        localNetworkStatsCollection.readLegacyUid(paramFile, this.mOnlyTags);
        long l1 = localNetworkStatsCollection.getStartMillis();
        long l2 = localNetworkStatsCollection.getEndMillis();
        if (!localNetworkStatsCollection.isEmpty())
        {
            this.mRotator.rewriteActive(new CombiningRewriter(localNetworkStatsCollection), l1);
            this.mRotator.maybeRotate(l2);
        }
    }

    public void maybePersistLocked(long paramLong)
    {
        if (this.mPending.getTotalBytes() >= this.mPersistThresholdBytes)
            forcePersistLocked(paramLong);
        while (true)
        {
            return;
            this.mRotator.maybeRotate(paramLong);
        }
    }

    public void recordSnapshotLocked(NetworkStats paramNetworkStats, Map<String, NetworkIdentitySet> paramMap, long paramLong)
    {
        HashSet localHashSet = Sets.newHashSet();
        if (paramNetworkStats == null);
        while (true)
        {
            return;
            if (this.mLastSnapshot == null)
            {
                this.mLastSnapshot = paramNetworkStats;
            }
            else
            {
                NetworkStatsCollection localNetworkStatsCollection;
                long l;
                NetworkStats.Entry localEntry;
                int i;
                label77: NetworkIdentitySet localNetworkIdentitySet;
                if (this.mComplete != null)
                {
                    localNetworkStatsCollection = (NetworkStatsCollection)this.mComplete.get();
                    NetworkStats localNetworkStats = NetworkStats.subtract(paramNetworkStats, this.mLastSnapshot, this.mObserver, this.mCookie);
                    l = paramLong - localNetworkStats.getElapsedRealtime();
                    localEntry = null;
                    i = 0;
                    if (i >= localNetworkStats.size())
                        break label288;
                    localEntry = localNetworkStats.getValues(i, localEntry);
                    localNetworkIdentitySet = (NetworkIdentitySet)paramMap.get(localEntry.iface);
                    if (localNetworkIdentitySet != null)
                        break label142;
                    localHashSet.add(localEntry.iface);
                }
                label142: label286: 
                while (true)
                {
                    i++;
                    break label77;
                    localNetworkStatsCollection = null;
                    break;
                    if (!localEntry.isEmpty())
                    {
                        if (localEntry.tag == 0);
                        for (int j = 1; ; j = 0)
                        {
                            if (j == this.mOnlyTags)
                                break label286;
                            this.mPending.recordData(localNetworkIdentitySet, localEntry.uid, localEntry.set, localEntry.tag, l, paramLong, localEntry);
                            if (this.mSinceBoot != null)
                                this.mSinceBoot.recordData(localNetworkIdentitySet, localEntry.uid, localEntry.set, localEntry.tag, l, paramLong, localEntry);
                            if (localNetworkStatsCollection == null)
                                break;
                            int k = localEntry.uid;
                            int m = localEntry.set;
                            int n = localEntry.tag;
                            localNetworkStatsCollection.recordData(localNetworkIdentitySet, k, m, n, l, paramLong, localEntry);
                            break;
                        }
                    }
                }
                label288: this.mLastSnapshot = paramNetworkStats;
            }
        }
    }

    public void removeUidLocked(int paramInt)
    {
        try
        {
            this.mRotator.rewriteAll(new RemoveUidRewriter(this.mBucketDuration, paramInt));
            if (this.mLastSnapshot != null)
                this.mLastSnapshot = this.mLastSnapshot.withoutUid(paramInt);
            if (this.mComplete != null)
            {
                localNetworkStatsCollection = (NetworkStatsCollection)this.mComplete.get();
                if (localNetworkStatsCollection != null)
                    localNetworkStatsCollection.removeUid(paramInt);
                return;
            }
        }
        catch (IOException localIOException)
        {
            while (true)
            {
                Log.wtf("NetworkStatsRecorder", "problem removing UID " + paramInt, localIOException);
                recoverFromWtf();
                continue;
                NetworkStatsCollection localNetworkStatsCollection = null;
            }
        }
    }

    public void resetLocked()
    {
        this.mLastSnapshot = null;
        this.mPending.reset();
        this.mSinceBoot.reset();
        this.mComplete.clear();
    }

    public void setPersistThreshold(long paramLong)
    {
        this.mPersistThresholdBytes = MathUtils.constrain(paramLong, 1024L, 104857600L);
    }

    public static class RemoveUidRewriter
        implements FileRotator.Rewriter
    {
        private final NetworkStatsCollection mTemp;
        private final int mUid;

        public RemoveUidRewriter(long paramLong, int paramInt)
        {
            this.mTemp = new NetworkStatsCollection(paramLong);
            this.mUid = paramInt;
        }

        public void read(InputStream paramInputStream)
            throws IOException
        {
            this.mTemp.read(paramInputStream);
            this.mTemp.clearDirty();
            this.mTemp.removeUid(this.mUid);
        }

        public void reset()
        {
            this.mTemp.reset();
        }

        public boolean shouldWrite()
        {
            return this.mTemp.isDirty();
        }

        public void write(OutputStream paramOutputStream)
            throws IOException
        {
            this.mTemp.write(new DataOutputStream(paramOutputStream));
        }
    }

    private static class CombiningRewriter
        implements FileRotator.Rewriter
    {
        private final NetworkStatsCollection mCollection;

        public CombiningRewriter(NetworkStatsCollection paramNetworkStatsCollection)
        {
            this.mCollection = ((NetworkStatsCollection)Preconditions.checkNotNull(paramNetworkStatsCollection, "missing NetworkStatsCollection"));
        }

        public void read(InputStream paramInputStream)
            throws IOException
        {
            this.mCollection.read(paramInputStream);
        }

        public void reset()
        {
        }

        public boolean shouldWrite()
        {
            return true;
        }

        public void write(OutputStream paramOutputStream)
            throws IOException
        {
            this.mCollection.write(new DataOutputStream(paramOutputStream));
            this.mCollection.reset();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.net.NetworkStatsRecorder
 * JD-Core Version:        0.6.2
 */