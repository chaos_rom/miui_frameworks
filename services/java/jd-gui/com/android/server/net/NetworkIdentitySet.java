package com.android.server.net;

import android.net.NetworkIdentity;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;

public class NetworkIdentitySet extends HashSet<NetworkIdentity>
{
    private static final int VERSION_ADD_NETWORK_ID = 3;
    private static final int VERSION_ADD_ROAMING = 2;
    private static final int VERSION_INIT = 1;

    public NetworkIdentitySet()
    {
    }

    public NetworkIdentitySet(DataInputStream paramDataInputStream)
        throws IOException
    {
        int i = paramDataInputStream.readInt();
        int j = paramDataInputStream.readInt();
        int k = 0;
        if (k < j)
        {
            if (i <= 1)
                paramDataInputStream.readInt();
            int m = paramDataInputStream.readInt();
            int n = paramDataInputStream.readInt();
            String str1 = readOptionalString(paramDataInputStream);
            String str2;
            if (i >= 3)
            {
                str2 = readOptionalString(paramDataInputStream);
                label62: if (i < 2)
                    break label105;
                paramDataInputStream.readBoolean();
            }
            label105: 
            while (true)
            {
                add(new NetworkIdentity(m, n, str1, str2, false));
                k++;
                break;
                str2 = null;
                break label62;
            }
        }
    }

    private static String readOptionalString(DataInputStream paramDataInputStream)
        throws IOException
    {
        if (paramDataInputStream.readByte() != 0);
        for (String str = paramDataInputStream.readUTF(); ; str = null)
            return str;
    }

    private static void writeOptionalString(DataOutputStream paramDataOutputStream, String paramString)
        throws IOException
    {
        if (paramString != null)
        {
            paramDataOutputStream.writeByte(1);
            paramDataOutputStream.writeUTF(paramString);
        }
        while (true)
        {
            return;
            paramDataOutputStream.writeByte(0);
        }
    }

    public void writeToStream(DataOutputStream paramDataOutputStream)
        throws IOException
    {
        paramDataOutputStream.writeInt(3);
        paramDataOutputStream.writeInt(size());
        Iterator localIterator = iterator();
        while (localIterator.hasNext())
        {
            NetworkIdentity localNetworkIdentity = (NetworkIdentity)localIterator.next();
            paramDataOutputStream.writeInt(localNetworkIdentity.getType());
            paramDataOutputStream.writeInt(localNetworkIdentity.getSubType());
            writeOptionalString(paramDataOutputStream, localNetworkIdentity.getSubscriberId());
            writeOptionalString(paramDataOutputStream, localNetworkIdentity.getNetworkId());
            paramDataOutputStream.writeBoolean(localNetworkIdentity.getRoaming());
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.net.NetworkIdentitySet
 * JD-Core Version:        0.6.2
 */