package com.android.server.net;

import android.net.NetworkIdentity;
import android.net.NetworkStats;
import android.net.NetworkStats.Entry;
import android.net.NetworkStatsHistory;
import android.net.NetworkStatsHistory.Entry;
import android.net.NetworkTemplate;
import com.android.internal.util.FileRotator.Reader;
import com.android.internal.util.IndentingPrintWriter;
import com.android.internal.util.Objects;
import com.google.android.collect.Lists;
import com.google.android.collect.Maps;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public class NetworkStatsCollection
    implements FileRotator.Reader
{
    private static final int FILE_MAGIC = 1095648596;
    private static final int VERSION_NETWORK_INIT = 1;
    private static final int VERSION_UID_INIT = 1;
    private static final int VERSION_UID_WITH_IDENT = 2;
    private static final int VERSION_UID_WITH_SET = 4;
    private static final int VERSION_UID_WITH_TAG = 3;
    private static final int VERSION_UNIFIED_INIT = 16;
    private final long mBucketDuration;
    private boolean mDirty;
    private long mEndMillis;
    private long mStartMillis;
    private HashMap<Key, NetworkStatsHistory> mStats = Maps.newHashMap();
    private long mTotalBytes;

    public NetworkStatsCollection(long paramLong)
    {
        this.mBucketDuration = paramLong;
        reset();
    }

    private int estimateBuckets()
    {
        return (int)(Math.min(this.mEndMillis - this.mStartMillis, 3024000000L) / this.mBucketDuration);
    }

    private NetworkStatsHistory findOrCreateHistory(NetworkIdentitySet paramNetworkIdentitySet, int paramInt1, int paramInt2, int paramInt3)
    {
        Key localKey = new Key(paramNetworkIdentitySet, paramInt1, paramInt2, paramInt3);
        NetworkStatsHistory localNetworkStatsHistory1 = (NetworkStatsHistory)this.mStats.get(localKey);
        NetworkStatsHistory localNetworkStatsHistory2 = null;
        if (localNetworkStatsHistory1 == null)
        {
            localNetworkStatsHistory2 = new NetworkStatsHistory(this.mBucketDuration, 10);
            if (localNetworkStatsHistory2 == null)
                break label102;
            this.mStats.put(localKey, localNetworkStatsHistory2);
        }
        while (true)
        {
            return localNetworkStatsHistory2;
            if (localNetworkStatsHistory1.getBucketDuration() == this.mBucketDuration)
                break;
            localNetworkStatsHistory2 = new NetworkStatsHistory(localNetworkStatsHistory1, this.mBucketDuration);
            break;
            label102: localNetworkStatsHistory2 = localNetworkStatsHistory1;
        }
    }

    private void noteRecordedHistory(long paramLong1, long paramLong2, long paramLong3)
    {
        if (paramLong1 < this.mStartMillis)
            this.mStartMillis = paramLong1;
        if (paramLong2 > this.mEndMillis)
            this.mEndMillis = paramLong2;
        this.mTotalBytes = (paramLong3 + this.mTotalBytes);
        this.mDirty = true;
    }

    private void recordHistory(Key paramKey, NetworkStatsHistory paramNetworkStatsHistory)
    {
        if (paramNetworkStatsHistory.size() == 0);
        while (true)
        {
            return;
            noteRecordedHistory(paramNetworkStatsHistory.getStart(), paramNetworkStatsHistory.getEnd(), paramNetworkStatsHistory.getTotalBytes());
            NetworkStatsHistory localNetworkStatsHistory = (NetworkStatsHistory)this.mStats.get(paramKey);
            if (localNetworkStatsHistory == null)
            {
                localNetworkStatsHistory = new NetworkStatsHistory(paramNetworkStatsHistory.getBucketDuration());
                this.mStats.put(paramKey, localNetworkStatsHistory);
            }
            localNetworkStatsHistory.recordEntireHistory(paramNetworkStatsHistory);
        }
    }

    private static boolean templateMatches(NetworkTemplate paramNetworkTemplate, NetworkIdentitySet paramNetworkIdentitySet)
    {
        Iterator localIterator = paramNetworkIdentitySet.iterator();
        do
            if (!localIterator.hasNext())
                break;
        while (!paramNetworkTemplate.matches((NetworkIdentity)localIterator.next()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void clearDirty()
    {
        this.mDirty = false;
    }

    public void dump(IndentingPrintWriter paramIndentingPrintWriter)
    {
        ArrayList localArrayList = Lists.newArrayList();
        localArrayList.addAll(this.mStats.keySet());
        Collections.sort(localArrayList);
        Iterator localIterator = localArrayList.iterator();
        while (localIterator.hasNext())
        {
            Key localKey = (Key)localIterator.next();
            paramIndentingPrintWriter.print("ident=");
            paramIndentingPrintWriter.print(localKey.ident.toString());
            paramIndentingPrintWriter.print(" uid=");
            paramIndentingPrintWriter.print(localKey.uid);
            paramIndentingPrintWriter.print(" set=");
            paramIndentingPrintWriter.print(NetworkStats.setToString(localKey.set));
            paramIndentingPrintWriter.print(" tag=");
            paramIndentingPrintWriter.println(NetworkStats.tagToString(localKey.tag));
            NetworkStatsHistory localNetworkStatsHistory = (NetworkStatsHistory)this.mStats.get(localKey);
            paramIndentingPrintWriter.increaseIndent();
            localNetworkStatsHistory.dump(paramIndentingPrintWriter, true);
            paramIndentingPrintWriter.decreaseIndent();
        }
    }

    public long getEndMillis()
    {
        return this.mEndMillis;
    }

    public long getFirstAtomicBucketMillis()
    {
        long l = 9223372036854775807L;
        if (this.mStartMillis == l);
        while (true)
        {
            return l;
            l = this.mStartMillis + this.mBucketDuration;
        }
    }

    public NetworkStatsHistory getHistory(NetworkTemplate paramNetworkTemplate, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        return getHistory(paramNetworkTemplate, paramInt1, paramInt2, paramInt3, paramInt4, -9223372036854775808L, 9223372036854775807L);
    }

    public NetworkStatsHistory getHistory(NetworkTemplate paramNetworkTemplate, int paramInt1, int paramInt2, int paramInt3, int paramInt4, long paramLong1, long paramLong2)
    {
        NetworkStatsHistory localNetworkStatsHistory = new NetworkStatsHistory(this.mBucketDuration, estimateBuckets(), paramInt4);
        Iterator localIterator = this.mStats.entrySet().iterator();
        label147: 
        while (localIterator.hasNext())
        {
            Map.Entry localEntry = (Map.Entry)localIterator.next();
            Key localKey = (Key)localEntry.getKey();
            if ((paramInt2 == -1) || (localKey.set == paramInt2));
            for (int i = 1; ; i = 0)
            {
                if ((localKey.uid != paramInt1) || (i == 0) || (localKey.tag != paramInt3) || (!templateMatches(paramNetworkTemplate, localKey.ident)))
                    break label147;
                localNetworkStatsHistory.recordHistory((NetworkStatsHistory)localEntry.getValue(), paramLong1, paramLong2);
                break;
            }
        }
        return localNetworkStatsHistory;
    }

    public long getStartMillis()
    {
        return this.mStartMillis;
    }

    public NetworkStats getSummary(NetworkTemplate paramNetworkTemplate, long paramLong1, long paramLong2)
    {
        long l = System.currentTimeMillis();
        NetworkStats localNetworkStats = new NetworkStats(paramLong2 - paramLong1, 24);
        NetworkStats.Entry localEntry = new NetworkStats.Entry();
        NetworkStatsHistory.Entry localEntry1 = null;
        if (paramLong1 == paramLong2);
        while (true)
        {
            return localNetworkStats;
            Iterator localIterator = this.mStats.entrySet().iterator();
            while (localIterator.hasNext())
            {
                Map.Entry localEntry2 = (Map.Entry)localIterator.next();
                Key localKey = (Key)localEntry2.getKey();
                if (templateMatches(paramNetworkTemplate, localKey.ident))
                {
                    localEntry1 = ((NetworkStatsHistory)localEntry2.getValue()).getValues(paramLong1, paramLong2, l, localEntry1);
                    localEntry.iface = NetworkStats.IFACE_ALL;
                    localEntry.uid = localKey.uid;
                    localEntry.set = localKey.set;
                    localEntry.tag = localKey.tag;
                    localEntry.rxBytes = localEntry1.rxBytes;
                    localEntry.rxPackets = localEntry1.rxPackets;
                    localEntry.txBytes = localEntry1.txBytes;
                    localEntry.txPackets = localEntry1.txPackets;
                    localEntry.operations = localEntry1.operations;
                    if (!localEntry.isEmpty())
                        localNetworkStats.combineValues(localEntry);
                }
            }
        }
    }

    public long getTotalBytes()
    {
        return this.mTotalBytes;
    }

    public boolean isDirty()
    {
        return this.mDirty;
    }

    public boolean isEmpty()
    {
        if ((this.mStartMillis == 9223372036854775807L) && (this.mEndMillis == -9223372036854775808L));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void read(DataInputStream paramDataInputStream)
        throws IOException
    {
        int i = paramDataInputStream.readInt();
        if (i != 1095648596)
            throw new ProtocolException("unexpected magic: " + i);
        int j = paramDataInputStream.readInt();
        switch (j)
        {
        default:
            throw new ProtocolException("unexpected version: " + j);
        case 16:
        }
        int k = paramDataInputStream.readInt();
        for (int m = 0; m < k; m++)
        {
            NetworkIdentitySet localNetworkIdentitySet = new NetworkIdentitySet(paramDataInputStream);
            int n = paramDataInputStream.readInt();
            for (int i1 = 0; i1 < n; i1++)
                recordHistory(new Key(localNetworkIdentitySet, paramDataInputStream.readInt(), paramDataInputStream.readInt(), paramDataInputStream.readInt()), new NetworkStatsHistory(paramDataInputStream));
        }
    }

    public void read(InputStream paramInputStream)
        throws IOException
    {
        read(new DataInputStream(paramInputStream));
    }

    // ERROR //
    @java.lang.Deprecated
    public void readLegacyNetwork(java.io.File paramFile)
        throws IOException
    {
        // Byte code:
        //     0: new 363	com/android/internal/os/AtomicFile
        //     3: dup
        //     4: aload_1
        //     5: invokespecial 365	com/android/internal/os/AtomicFile:<init>	(Ljava/io/File;)V
        //     8: astore_2
        //     9: aconst_null
        //     10: astore_3
        //     11: new 324	java/io/DataInputStream
        //     14: dup
        //     15: new 367	java/io/BufferedInputStream
        //     18: dup
        //     19: aload_2
        //     20: invokevirtual 371	com/android/internal/os/AtomicFile:openRead	()Ljava/io/FileInputStream;
        //     23: invokespecial 372	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
        //     26: invokespecial 354	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
        //     29: astore 4
        //     31: aload 4
        //     33: invokevirtual 327	java/io/DataInputStream:readInt	()I
        //     36: istore 7
        //     38: iload 7
        //     40: ldc 12
        //     42: if_icmpeq +42 -> 84
        //     45: new 329	java/net/ProtocolException
        //     48: dup
        //     49: new 331	java/lang/StringBuilder
        //     52: dup
        //     53: invokespecial 332	java/lang/StringBuilder:<init>	()V
        //     56: ldc_w 334
        //     59: invokevirtual 338	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     62: iload 7
        //     64: invokevirtual 341	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     67: invokevirtual 342	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     70: invokespecial 344	java/net/ProtocolException:<init>	(Ljava/lang/String;)V
        //     73: athrow
        //     74: astore 6
        //     76: aload 4
        //     78: astore_3
        //     79: aload_3
        //     80: invokestatic 378	libcore/io/IoUtils:closeQuietly	(Ljava/lang/AutoCloseable;)V
        //     83: return
        //     84: aload 4
        //     86: invokevirtual 327	java/io/DataInputStream:readInt	()I
        //     89: istore 8
        //     91: iload 8
        //     93: tableswitch	default:+19 -> 112, 1:+60->153
        //     113: aconst_null
        //     114: dstore_2
        //     115: dup
        //     116: new 331	java/lang/StringBuilder
        //     119: dup
        //     120: invokespecial 332	java/lang/StringBuilder:<init>	()V
        //     123: ldc_w 346
        //     126: invokevirtual 338	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     129: iload 8
        //     131: invokevirtual 341	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     134: invokevirtual 342	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     137: invokespecial 344	java/net/ProtocolException:<init>	(Ljava/lang/String;)V
        //     140: athrow
        //     141: astore 5
        //     143: aload 4
        //     145: astore_3
        //     146: aload_3
        //     147: invokestatic 378	libcore/io/IoUtils:closeQuietly	(Ljava/lang/AutoCloseable;)V
        //     150: aload 5
        //     152: athrow
        //     153: aload 4
        //     155: invokevirtual 327	java/io/DataInputStream:readInt	()I
        //     158: istore 9
        //     160: iconst_0
        //     161: istore 10
        //     163: iload 10
        //     165: iload 9
        //     167: if_icmpge +51 -> 218
        //     170: new 124	com/android/server/net/NetworkIdentitySet
        //     173: dup
        //     174: aload 4
        //     176: invokespecial 348	com/android/server/net/NetworkIdentitySet:<init>	(Ljava/io/DataInputStream;)V
        //     179: astore 11
        //     181: new 78	android/net/NetworkStatsHistory
        //     184: dup
        //     185: aload 4
        //     187: invokespecial 349	android/net/NetworkStatsHistory:<init>	(Ljava/io/DataInputStream;)V
        //     190: astore 12
        //     192: aload_0
        //     193: new 8	com/android/server/net/NetworkStatsCollection$Key
        //     196: dup
        //     197: aload 11
        //     199: bipush 255
        //     201: bipush 255
        //     203: iconst_0
        //     204: invokespecial 70	com/android/server/net/NetworkStatsCollection$Key:<init>	(Lcom/android/server/net/NetworkIdentitySet;III)V
        //     207: aload 12
        //     209: invokespecial 351	com/android/server/net/NetworkStatsCollection:recordHistory	(Lcom/android/server/net/NetworkStatsCollection$Key;Landroid/net/NetworkStatsHistory;)V
        //     212: iinc 10 1
        //     215: goto -52 -> 163
        //     218: aload 4
        //     220: invokestatic 378	libcore/io/IoUtils:closeQuietly	(Ljava/lang/AutoCloseable;)V
        //     223: goto -140 -> 83
        //     226: astore 5
        //     228: goto -82 -> 146
        //     231: astore 13
        //     233: goto -154 -> 79
        //
        // Exception table:
        //     from	to	target	type
        //     31	74	74	java/io/FileNotFoundException
        //     84	141	74	java/io/FileNotFoundException
        //     153	212	74	java/io/FileNotFoundException
        //     31	74	141	finally
        //     84	141	141	finally
        //     153	212	141	finally
        //     11	31	226	finally
        //     11	31	231	java/io/FileNotFoundException
    }

    // ERROR //
    @java.lang.Deprecated
    public void readLegacyUid(java.io.File paramFile, boolean paramBoolean)
        throws IOException
    {
        // Byte code:
        //     0: new 363	com/android/internal/os/AtomicFile
        //     3: dup
        //     4: aload_1
        //     5: invokespecial 365	com/android/internal/os/AtomicFile:<init>	(Ljava/io/File;)V
        //     8: astore_3
        //     9: aconst_null
        //     10: astore 4
        //     12: new 324	java/io/DataInputStream
        //     15: dup
        //     16: new 367	java/io/BufferedInputStream
        //     19: dup
        //     20: aload_3
        //     21: invokevirtual 371	com/android/internal/os/AtomicFile:openRead	()Ljava/io/FileInputStream;
        //     24: invokespecial 372	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
        //     27: invokespecial 354	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
        //     30: astore 5
        //     32: aload 5
        //     34: invokevirtual 327	java/io/DataInputStream:readInt	()I
        //     37: istore 8
        //     39: iload 8
        //     41: ldc 12
        //     43: if_icmpeq +44 -> 87
        //     46: new 329	java/net/ProtocolException
        //     49: dup
        //     50: new 331	java/lang/StringBuilder
        //     53: dup
        //     54: invokespecial 332	java/lang/StringBuilder:<init>	()V
        //     57: ldc_w 334
        //     60: invokevirtual 338	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     63: iload 8
        //     65: invokevirtual 341	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     68: invokevirtual 342	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     71: invokespecial 344	java/net/ProtocolException:<init>	(Ljava/lang/String;)V
        //     74: athrow
        //     75: astore 7
        //     77: aload 5
        //     79: astore 4
        //     81: aload 4
        //     83: invokestatic 378	libcore/io/IoUtils:closeQuietly	(Ljava/lang/AutoCloseable;)V
        //     86: return
        //     87: aload 5
        //     89: invokevirtual 327	java/io/DataInputStream:readInt	()I
        //     92: istore 9
        //     94: iload 9
        //     96: tableswitch	default:+32 -> 128, 1:+221->317, 2:+221->317, 3:+75->171, 4:+75->171
        //     129: aconst_null
        //     130: dstore_2
        //     131: dup
        //     132: new 331	java/lang/StringBuilder
        //     135: dup
        //     136: invokespecial 332	java/lang/StringBuilder:<init>	()V
        //     139: ldc_w 346
        //     142: invokevirtual 338	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     145: iload 9
        //     147: invokevirtual 341	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     150: invokevirtual 342	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     153: invokespecial 344	java/net/ProtocolException:<init>	(Ljava/lang/String;)V
        //     156: athrow
        //     157: astore 6
        //     159: aload 5
        //     161: astore 4
        //     163: aload 4
        //     165: invokestatic 378	libcore/io/IoUtils:closeQuietly	(Ljava/lang/AutoCloseable;)V
        //     168: aload 6
        //     170: athrow
        //     171: aload 5
        //     173: invokevirtual 327	java/io/DataInputStream:readInt	()I
        //     176: istore 10
        //     178: iconst_0
        //     179: istore 11
        //     181: iload 11
        //     183: iload 10
        //     185: if_icmpge +132 -> 317
        //     188: new 124	com/android/server/net/NetworkIdentitySet
        //     191: dup
        //     192: aload 5
        //     194: invokespecial 348	com/android/server/net/NetworkIdentitySet:<init>	(Ljava/io/DataInputStream;)V
        //     197: astore 12
        //     199: aload 5
        //     201: invokevirtual 327	java/io/DataInputStream:readInt	()I
        //     204: istore 13
        //     206: iconst_0
        //     207: istore 14
        //     209: iload 14
        //     211: iload 13
        //     213: if_icmpge +98 -> 311
        //     216: aload 5
        //     218: invokevirtual 327	java/io/DataInputStream:readInt	()I
        //     221: istore 15
        //     223: iload 9
        //     225: iconst_4
        //     226: if_icmplt +73 -> 299
        //     229: aload 5
        //     231: invokevirtual 327	java/io/DataInputStream:readInt	()I
        //     234: istore 16
        //     236: aload 5
        //     238: invokevirtual 327	java/io/DataInputStream:readInt	()I
        //     241: istore 17
        //     243: new 8	com/android/server/net/NetworkStatsCollection$Key
        //     246: dup
        //     247: aload 12
        //     249: iload 15
        //     251: iload 16
        //     253: iload 17
        //     255: invokespecial 70	com/android/server/net/NetworkStatsCollection$Key:<init>	(Lcom/android/server/net/NetworkIdentitySet;III)V
        //     258: astore 18
        //     260: new 78	android/net/NetworkStatsHistory
        //     263: dup
        //     264: aload 5
        //     266: invokespecial 349	android/net/NetworkStatsHistory:<init>	(Ljava/io/DataInputStream;)V
        //     269: astore 19
        //     271: iload 17
        //     273: ifne +32 -> 305
        //     276: iconst_1
        //     277: istore 20
        //     279: iload 20
        //     281: iload_2
        //     282: if_icmpeq +11 -> 293
        //     285: aload_0
        //     286: aload 18
        //     288: aload 19
        //     290: invokespecial 351	com/android/server/net/NetworkStatsCollection:recordHistory	(Lcom/android/server/net/NetworkStatsCollection$Key;Landroid/net/NetworkStatsHistory;)V
        //     293: iinc 14 1
        //     296: goto -87 -> 209
        //     299: iconst_0
        //     300: istore 16
        //     302: goto -66 -> 236
        //     305: iconst_0
        //     306: istore 20
        //     308: goto -29 -> 279
        //     311: iinc 11 1
        //     314: goto -133 -> 181
        //     317: aload 5
        //     319: invokestatic 378	libcore/io/IoUtils:closeQuietly	(Ljava/lang/AutoCloseable;)V
        //     322: goto -236 -> 86
        //     325: astore 6
        //     327: goto -164 -> 163
        //     330: astore 21
        //     332: goto -251 -> 81
        //
        // Exception table:
        //     from	to	target	type
        //     32	75	75	java/io/FileNotFoundException
        //     87	157	75	java/io/FileNotFoundException
        //     171	293	75	java/io/FileNotFoundException
        //     32	75	157	finally
        //     87	157	157	finally
        //     171	293	157	finally
        //     12	32	325	finally
        //     12	32	330	java/io/FileNotFoundException
    }

    public void recordCollection(NetworkStatsCollection paramNetworkStatsCollection)
    {
        Iterator localIterator = paramNetworkStatsCollection.mStats.entrySet().iterator();
        while (localIterator.hasNext())
        {
            Map.Entry localEntry = (Map.Entry)localIterator.next();
            recordHistory((Key)localEntry.getKey(), (NetworkStatsHistory)localEntry.getValue());
        }
    }

    public void recordData(NetworkIdentitySet paramNetworkIdentitySet, int paramInt1, int paramInt2, int paramInt3, long paramLong1, long paramLong2, NetworkStats.Entry paramEntry)
    {
        NetworkStatsHistory localNetworkStatsHistory = findOrCreateHistory(paramNetworkIdentitySet, paramInt1, paramInt2, paramInt3);
        localNetworkStatsHistory.recordData(paramLong1, paramLong2, paramEntry);
        noteRecordedHistory(localNetworkStatsHistory.getStart(), localNetworkStatsHistory.getEnd(), paramEntry.rxBytes + paramEntry.txBytes);
    }

    public void removeUid(int paramInt)
    {
        ArrayList localArrayList = Lists.newArrayList();
        localArrayList.addAll(this.mStats.keySet());
        Iterator localIterator = localArrayList.iterator();
        while (localIterator.hasNext())
        {
            Key localKey = (Key)localIterator.next();
            if (localKey.uid == paramInt)
            {
                if (localKey.tag == 0)
                {
                    NetworkStatsHistory localNetworkStatsHistory = (NetworkStatsHistory)this.mStats.get(localKey);
                    findOrCreateHistory(localKey.ident, -4, 0, 0).recordEntireHistory(localNetworkStatsHistory);
                }
                this.mStats.remove(localKey);
                this.mDirty = true;
            }
        }
    }

    public void reset()
    {
        this.mStats.clear();
        this.mStartMillis = 9223372036854775807L;
        this.mEndMillis = -9223372036854775808L;
        this.mTotalBytes = 0L;
        this.mDirty = false;
    }

    public void write(DataOutputStream paramDataOutputStream)
        throws IOException
    {
        HashMap localHashMap = Maps.newHashMap();
        Iterator localIterator1 = this.mStats.keySet().iterator();
        while (localIterator1.hasNext())
        {
            Key localKey2 = (Key)localIterator1.next();
            ArrayList localArrayList2 = (ArrayList)localHashMap.get(localKey2.ident);
            if (localArrayList2 == null)
            {
                localArrayList2 = Lists.newArrayList();
                localHashMap.put(localKey2.ident, localArrayList2);
            }
            localArrayList2.add(localKey2);
        }
        paramDataOutputStream.writeInt(1095648596);
        paramDataOutputStream.writeInt(16);
        paramDataOutputStream.writeInt(localHashMap.size());
        Iterator localIterator2 = localHashMap.keySet().iterator();
        while (localIterator2.hasNext())
        {
            NetworkIdentitySet localNetworkIdentitySet = (NetworkIdentitySet)localIterator2.next();
            ArrayList localArrayList1 = (ArrayList)localHashMap.get(localNetworkIdentitySet);
            localNetworkIdentitySet.writeToStream(paramDataOutputStream);
            paramDataOutputStream.writeInt(localArrayList1.size());
            Iterator localIterator3 = localArrayList1.iterator();
            while (localIterator3.hasNext())
            {
                Key localKey1 = (Key)localIterator3.next();
                NetworkStatsHistory localNetworkStatsHistory = (NetworkStatsHistory)this.mStats.get(localKey1);
                paramDataOutputStream.writeInt(localKey1.uid);
                paramDataOutputStream.writeInt(localKey1.set);
                paramDataOutputStream.writeInt(localKey1.tag);
                localNetworkStatsHistory.writeToStream(paramDataOutputStream);
            }
        }
        paramDataOutputStream.flush();
    }

    private static class Key
        implements Comparable<Key>
    {
        private final int hashCode;
        public final NetworkIdentitySet ident;
        public final int set;
        public final int tag;
        public final int uid;

        public Key(NetworkIdentitySet paramNetworkIdentitySet, int paramInt1, int paramInt2, int paramInt3)
        {
            this.ident = paramNetworkIdentitySet;
            this.uid = paramInt1;
            this.set = paramInt2;
            this.tag = paramInt3;
            Object[] arrayOfObject = new Object[4];
            arrayOfObject[0] = paramNetworkIdentitySet;
            arrayOfObject[1] = Integer.valueOf(paramInt1);
            arrayOfObject[2] = Integer.valueOf(paramInt2);
            arrayOfObject[3] = Integer.valueOf(paramInt3);
            this.hashCode = Objects.hashCode(arrayOfObject);
        }

        public int compareTo(Key paramKey)
        {
            return Integer.compare(this.uid, paramKey.uid);
        }

        public boolean equals(Object paramObject)
        {
            boolean bool = false;
            if ((paramObject instanceof Key))
            {
                Key localKey = (Key)paramObject;
                if ((this.uid == localKey.uid) && (this.set == localKey.set) && (this.tag == localKey.tag) && (Objects.equal(this.ident, localKey.ident)))
                    bool = true;
            }
            return bool;
        }

        public int hashCode()
        {
            return this.hashCode;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.net.NetworkStatsCollection
 * JD-Core Version:        0.6.2
 */