package com.android.server.net;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.IActivityManager;
import android.app.INotificationManager;
import android.app.IProcessObserver;
import android.app.IProcessObserver.Stub;
import android.app.Notification.Builder;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.UserInfo;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.IConnectivityManager;
import android.net.INetworkManagementEventObserver;
import android.net.INetworkPolicyListener;
import android.net.INetworkPolicyManager.Stub;
import android.net.INetworkStatsService;
import android.net.LinkProperties;
import android.net.NetworkIdentity;
import android.net.NetworkInfo;
import android.net.NetworkPolicy;
import android.net.NetworkPolicyManager;
import android.net.NetworkQuotaInfo;
import android.net.NetworkState;
import android.net.NetworkTemplate;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.os.Binder;
import android.os.Environment;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.INetworkManagementService;
import android.os.IPowerManager;
import android.os.Looper;
import android.os.Message;
import android.os.MessageQueue;
import android.os.MessageQueue.IdleHandler;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.os.UserId;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.format.Formatter;
import android.text.format.Time;
import android.util.Log;
import android.util.NtpTrustedTime;
import android.util.Slog;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.util.SparseIntArray;
import android.util.TrustedTime;
import com.android.internal.os.AtomicFile;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.FastXmlSerializer;
import com.android.internal.util.Objects;
import com.android.internal.util.Preconditions;
import com.google.android.collect.Lists;
import com.google.android.collect.Maps;
import com.google.android.collect.Sets;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

public class NetworkPolicyManagerService extends INetworkPolicyManager.Stub
{
    public static final String ACTION_ALLOW_BACKGROUND = "com.android.server.net.action.ALLOW_BACKGROUND";
    public static final String ACTION_SNOOZE_WARNING = "com.android.server.net.action.SNOOZE_WARNING";
    private static final String ATTR_APP_ID = "appId";
    private static final String ATTR_CYCLE_DAY = "cycleDay";
    private static final String ATTR_CYCLE_TIMEZONE = "cycleTimezone";
    private static final String ATTR_INFERRED = "inferred";
    private static final String ATTR_LAST_LIMIT_SNOOZE = "lastLimitSnooze";
    private static final String ATTR_LAST_SNOOZE = "lastSnooze";
    private static final String ATTR_LAST_WARNING_SNOOZE = "lastWarningSnooze";
    private static final String ATTR_LIMIT_BYTES = "limitBytes";
    private static final String ATTR_METERED = "metered";
    private static final String ATTR_NETWORK_ID = "networkId";
    private static final String ATTR_NETWORK_TEMPLATE = "networkTemplate";
    private static final String ATTR_POLICY = "policy";
    private static final String ATTR_RESTRICT_BACKGROUND = "restrictBackground";
    private static final String ATTR_SUBSCRIBER_ID = "subscriberId";
    private static final String ATTR_UID = "uid";
    private static final String ATTR_VERSION = "version";
    private static final String ATTR_WARNING_BYTES = "warningBytes";
    private static final boolean LOGD = false;
    private static final boolean LOGV = false;
    private static final int MSG_ADVISE_PERSIST_THRESHOLD = 7;
    private static final int MSG_FOREGROUND_ACTIVITIES_CHANGED = 3;
    private static final int MSG_LIMIT_REACHED = 5;
    private static final int MSG_METERED_IFACES_CHANGED = 2;
    private static final int MSG_PROCESS_DIED = 4;
    private static final int MSG_RESTRICT_BACKGROUND_CHANGED = 6;
    private static final int MSG_RULES_CHANGED = 1;
    private static final int MSG_SCREEN_ON_CHANGED = 8;
    private static final String TAG = "NetworkPolicy";
    private static final String TAG_ALLOW_BACKGROUND = "NetworkPolicy:allowBackground";
    private static final String TAG_APP_POLICY = "app-policy";
    private static final String TAG_NETWORK_POLICY = "network-policy";
    private static final String TAG_POLICY_LIST = "policy-list";
    private static final String TAG_UID_POLICY = "uid-policy";
    private static final long TIME_CACHE_MAX_AGE = 86400000L;
    public static final int TYPE_LIMIT = 2;
    public static final int TYPE_LIMIT_SNOOZED = 3;
    public static final int TYPE_WARNING = 1;
    private static final int VERSION_ADDED_INFERRED = 7;
    private static final int VERSION_ADDED_METERED = 4;
    private static final int VERSION_ADDED_NETWORK_ID = 9;
    private static final int VERSION_ADDED_RESTRICT_BACKGROUND = 3;
    private static final int VERSION_ADDED_SNOOZE = 2;
    private static final int VERSION_ADDED_TIMEZONE = 6;
    private static final int VERSION_INIT = 1;
    private static final int VERSION_LATEST = 9;
    private static final int VERSION_SPLIT_SNOOZE = 5;
    private static final int VERSION_SWITCH_APP_ID = 8;
    private HashSet<String> mActiveNotifs = Sets.newHashSet();
    private final IActivityManager mActivityManager;
    private INetworkManagementEventObserver mAlertObserver = new NetworkAlertObserver()
    {
        public void limitReached(String paramAnonymousString1, String paramAnonymousString2)
        {
            NetworkPolicyManagerService.this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkPolicy");
            if (!"globalAlert".equals(paramAnonymousString1))
                NetworkPolicyManagerService.this.mHandler.obtainMessage(5, paramAnonymousString2).sendToTarget();
        }
    };
    private BroadcastReceiver mAllowReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            NetworkPolicyManagerService.this.setRestrictBackground(false);
        }
    };
    private SparseIntArray mAppPolicy = new SparseIntArray();
    private IConnectivityManager mConnManager;
    private BroadcastReceiver mConnReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            NetworkPolicyManagerService.this.maybeRefreshTrustedTime();
            synchronized (NetworkPolicyManagerService.this.mRulesLock)
            {
                NetworkPolicyManagerService.this.ensureActiveMobilePolicyLocked();
                NetworkPolicyManagerService.this.updateNetworkEnabledLocked();
                NetworkPolicyManagerService.this.updateNetworkRulesLocked();
                NetworkPolicyManagerService.this.updateNotificationsLocked();
                return;
            }
        }
    };
    private final Context mContext;
    private final Handler mHandler;
    private Handler.Callback mHandlerCallback = new Handler.Callback()
    {
        public boolean handleMessage(Message paramAnonymousMessage)
        {
            boolean bool1;
            switch (paramAnonymousMessage.what)
            {
            default:
                bool1 = false;
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            }
            while (true)
            {
                return bool1;
                int i4 = paramAnonymousMessage.arg1;
                int i5 = paramAnonymousMessage.arg2;
                int i6 = NetworkPolicyManagerService.this.mListeners.beginBroadcast();
                int i7 = 0;
                label83: INetworkPolicyListener localINetworkPolicyListener3;
                if (i7 < i6)
                {
                    localINetworkPolicyListener3 = (INetworkPolicyListener)NetworkPolicyManagerService.this.mListeners.getBroadcastItem(i7);
                    if (localINetworkPolicyListener3 == null);
                }
                try
                {
                    localINetworkPolicyListener3.onUidRulesChanged(i4, i5);
                    label123: i7++;
                    break label83;
                    NetworkPolicyManagerService.this.mListeners.finishBroadcast();
                    bool1 = true;
                    continue;
                    arrayOfString = (String[])paramAnonymousMessage.obj;
                    int i2 = NetworkPolicyManagerService.this.mListeners.beginBroadcast();
                    i3 = 0;
                    if (i3 < i2)
                    {
                        localINetworkPolicyListener2 = (INetworkPolicyListener)NetworkPolicyManagerService.this.mListeners.getBroadcastItem(i3);
                        if (localINetworkPolicyListener2 == null);
                    }
                }
                catch (RemoteException localRemoteException2)
                {
                    try
                    {
                        while (true)
                        {
                            String[] arrayOfString;
                            int i3;
                            INetworkPolicyListener localINetworkPolicyListener2;
                            localINetworkPolicyListener2.onMeteredIfacesChanged(arrayOfString);
                            label209: i3++;
                        }
                        NetworkPolicyManagerService.this.mListeners.finishBroadcast();
                        bool1 = true;
                        continue;
                        int n = paramAnonymousMessage.arg1;
                        int i1 = paramAnonymousMessage.arg2;
                        boolean bool4 = ((Boolean)paramAnonymousMessage.obj).booleanValue();
                        int k;
                        synchronized (NetworkPolicyManagerService.this.mRulesLock)
                        {
                            SparseBooleanArray localSparseBooleanArray2 = (SparseBooleanArray)NetworkPolicyManagerService.this.mUidPidForeground.get(i1);
                            if (localSparseBooleanArray2 == null)
                            {
                                localSparseBooleanArray2 = new SparseBooleanArray(2);
                                NetworkPolicyManagerService.this.mUidPidForeground.put(i1, localSparseBooleanArray2);
                            }
                            localSparseBooleanArray2.put(n, bool4);
                            NetworkPolicyManagerService.this.computeUidForegroundLocked(i1);
                            bool1 = true;
                        }
                        int m = paramAnonymousMessage.arg2;
                        String str;
                        synchronized (NetworkPolicyManagerService.this.mRulesLock)
                        {
                            SparseBooleanArray localSparseBooleanArray1 = (SparseBooleanArray)NetworkPolicyManagerService.this.mUidPidForeground.get(m);
                            if (localSparseBooleanArray1 != null)
                            {
                                localSparseBooleanArray1.delete(k);
                                NetworkPolicyManagerService.this.computeUidForegroundLocked(m);
                            }
                            bool1 = true;
                        }
                        NetworkPolicyManagerService.this.maybeRefreshTrustedTime();
                        synchronized (NetworkPolicyManagerService.this.mRulesLock)
                        {
                            boolean bool3 = NetworkPolicyManagerService.this.mMeteredIfaces.contains(str);
                            if (!bool3);
                        }
                    }
                    catch (RemoteException localRemoteException2)
                    {
                        try
                        {
                            NetworkPolicyManagerService.this.mNetworkStats.forceUpdate();
                            label483: NetworkPolicyManagerService.this.updateNetworkEnabledLocked();
                            NetworkPolicyManagerService.this.updateNotificationsLocked();
                            bool1 = true;
                            continue;
                            localObject2 = finally;
                            throw localObject2;
                            boolean bool2;
                            label523: int j;
                            label538: INetworkPolicyListener localINetworkPolicyListener1;
                            if (paramAnonymousMessage.arg1 != 0)
                            {
                                bool2 = true;
                                int i = NetworkPolicyManagerService.this.mListeners.beginBroadcast();
                                j = 0;
                                if (j >= i)
                                    break label588;
                                localINetworkPolicyListener1 = (INetworkPolicyListener)NetworkPolicyManagerService.this.mListeners.getBroadcastItem(j);
                                if (localINetworkPolicyListener1 == null);
                            }
                            try
                            {
                                localINetworkPolicyListener1.onRestrictBackgroundChanged(bool2);
                                j++;
                                break label538;
                                bool2 = false;
                                break label523;
                                label588: NetworkPolicyManagerService.this.mListeners.finishBroadcast();
                                bool1 = true;
                                continue;
                                l1 = ((Long)paramAnonymousMessage.obj).longValue();
                            }
                            catch (RemoteException localRemoteException2)
                            {
                                try
                                {
                                    long l1;
                                    long l2 = l1 / 1000L;
                                    NetworkPolicyManagerService.this.mNetworkStats.advisePersistThreshold(l2);
                                    label635: bool1 = true;
                                    continue;
                                    NetworkPolicyManagerService.this.updateScreenOn();
                                    bool1 = true;
                                    continue;
                                    localRemoteException5 = localRemoteException5;
                                    break label123;
                                    localRemoteException4 = localRemoteException4;
                                    break label209;
                                    localRemoteException2 = localRemoteException2;
                                }
                                catch (RemoteException localRemoteException1)
                                {
                                    break label635;
                                }
                            }
                        }
                        catch (RemoteException localRemoteException3)
                        {
                            break label483;
                        }
                    }
                }
            }
        }
    };
    private final HandlerThread mHandlerThread;
    private final RemoteCallbackList<INetworkPolicyListener> mListeners = new RemoteCallbackList();
    private HashSet<String> mMeteredIfaces = Sets.newHashSet();
    private final INetworkManagementService mNetworkManager;
    private HashMap<NetworkTemplate, NetworkPolicy> mNetworkPolicy = Maps.newHashMap();
    private HashMap<NetworkPolicy, String[]> mNetworkRules = Maps.newHashMap();
    private final INetworkStatsService mNetworkStats;
    private INotificationManager mNotifManager;
    private HashSet<NetworkTemplate> mOverLimitNotified = Sets.newHashSet();
    private BroadcastReceiver mPackageReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            String str = paramAnonymousIntent.getAction();
            int i = UserId.getAppId(paramAnonymousIntent.getIntExtra("android.intent.extra.UID", 0));
            synchronized (NetworkPolicyManagerService.this.mRulesLock)
            {
                if ("android.intent.action.PACKAGE_ADDED".equals(str))
                    NetworkPolicyManagerService.this.updateRulesForAppLocked(i);
                while (!"android.intent.action.UID_REMOVED".equals(str))
                    return;
                NetworkPolicyManagerService.this.mAppPolicy.delete(i);
                NetworkPolicyManagerService.this.updateRulesForAppLocked(i);
                NetworkPolicyManagerService.this.writePolicyLocked();
            }
        }
    };
    private final AtomicFile mPolicyFile;
    private final IPowerManager mPowerManager;
    private IProcessObserver mProcessObserver = new IProcessObserver.Stub()
    {
        public void onForegroundActivitiesChanged(int paramAnonymousInt1, int paramAnonymousInt2, boolean paramAnonymousBoolean)
        {
            NetworkPolicyManagerService.this.mHandler.obtainMessage(3, paramAnonymousInt1, paramAnonymousInt2, Boolean.valueOf(paramAnonymousBoolean)).sendToTarget();
        }

        public void onImportanceChanged(int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
        {
        }

        public void onProcessDied(int paramAnonymousInt1, int paramAnonymousInt2)
        {
            NetworkPolicyManagerService.this.mHandler.obtainMessage(4, paramAnonymousInt1, paramAnonymousInt2).sendToTarget();
        }
    };
    private volatile boolean mRestrictBackground;
    private final Object mRulesLock = new Object();
    private volatile boolean mScreenOn;
    private BroadcastReceiver mScreenReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            synchronized (NetworkPolicyManagerService.this.mRulesLock)
            {
                NetworkPolicyManagerService.this.mHandler.obtainMessage(8).sendToTarget();
                return;
            }
        }
    };
    private BroadcastReceiver mSnoozeWarningReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            NetworkTemplate localNetworkTemplate = (NetworkTemplate)paramAnonymousIntent.getParcelableExtra("android.net.NETWORK_TEMPLATE");
            NetworkPolicyManagerService.this.performSnooze(localNetworkTemplate, 1);
        }
    };
    private BroadcastReceiver mStatsReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            NetworkPolicyManagerService.this.maybeRefreshTrustedTime();
            synchronized (NetworkPolicyManagerService.this.mRulesLock)
            {
                NetworkPolicyManagerService.this.updateNetworkEnabledLocked();
                NetworkPolicyManagerService.this.updateNotificationsLocked();
                return;
            }
        }
    };
    private final boolean mSuppressDefaultPolicy;
    private final TrustedTime mTime;
    private SparseBooleanArray mUidForeground = new SparseBooleanArray();
    private SparseArray<SparseBooleanArray> mUidPidForeground = new SparseArray();
    private SparseIntArray mUidRules = new SparseIntArray();
    private BroadcastReceiver mWifiConfigReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            if (paramAnonymousIntent.getIntExtra("changeReason", 0) == 1)
            {
                WifiConfiguration localWifiConfiguration = (WifiConfiguration)paramAnonymousIntent.getParcelableExtra("wifiConfiguration");
                if (localWifiConfiguration.SSID != null)
                {
                    NetworkTemplate localNetworkTemplate = NetworkTemplate.buildTemplateWifi(WifiInfo.removeDoubleQuotes(localWifiConfiguration.SSID));
                    synchronized (NetworkPolicyManagerService.this.mRulesLock)
                    {
                        if (NetworkPolicyManagerService.this.mNetworkPolicy.containsKey(localNetworkTemplate))
                        {
                            NetworkPolicyManagerService.this.mNetworkPolicy.remove(localNetworkTemplate);
                            NetworkPolicyManagerService.this.writePolicyLocked();
                        }
                    }
                }
            }
        }
    };
    private BroadcastReceiver mWifiStateReceiver = new BroadcastReceiver()
    {
        // ERROR //
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            // Byte code:
            //     0: aload_2
            //     1: ldc 19
            //     3: invokevirtual 25	android/content/Intent:getParcelableExtra	(Ljava/lang/String;)Landroid/os/Parcelable;
            //     6: checkcast 27	android/net/NetworkInfo
            //     9: invokevirtual 31	android/net/NetworkInfo:isConnected	()Z
            //     12: ifne +4 -> 16
            //     15: return
            //     16: aload_2
            //     17: ldc 33
            //     19: invokevirtual 25	android/content/Intent:getParcelableExtra	(Ljava/lang/String;)Landroid/os/Parcelable;
            //     22: checkcast 35	android/net/wifi/WifiInfo
            //     25: astore_3
            //     26: aload_3
            //     27: invokevirtual 38	android/net/wifi/WifiInfo:getMeteredHint	()Z
            //     30: istore 4
            //     32: aload_3
            //     33: invokevirtual 42	android/net/wifi/WifiInfo:getSSID	()Ljava/lang/String;
            //     36: invokestatic 46	android/net/wifi/WifiInfo:removeDoubleQuotes	(Ljava/lang/String;)Ljava/lang/String;
            //     39: invokestatic 52	android/net/NetworkTemplate:buildTemplateWifi	(Ljava/lang/String;)Landroid/net/NetworkTemplate;
            //     42: astore 5
            //     44: aload_0
            //     45: getfield 12	com/android/server/net/NetworkPolicyManagerService$8:this$0	Lcom/android/server/net/NetworkPolicyManagerService;
            //     48: invokestatic 56	com/android/server/net/NetworkPolicyManagerService:access$100	(Lcom/android/server/net/NetworkPolicyManagerService;)Ljava/lang/Object;
            //     51: astore 6
            //     53: aload 6
            //     55: monitorenter
            //     56: aload_0
            //     57: getfield 12	com/android/server/net/NetworkPolicyManagerService$8:this$0	Lcom/android/server/net/NetworkPolicyManagerService;
            //     60: invokestatic 60	com/android/server/net/NetworkPolicyManagerService:access$900	(Lcom/android/server/net/NetworkPolicyManagerService;)Ljava/util/HashMap;
            //     63: aload 5
            //     65: invokevirtual 66	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
            //     68: checkcast 68	android/net/NetworkPolicy
            //     71: astore 8
            //     73: aload 8
            //     75: ifnonnull +61 -> 136
            //     78: iload 4
            //     80: ifeq +56 -> 136
            //     83: new 68	android/net/NetworkPolicy
            //     86: dup
            //     87: aload 5
            //     89: bipush 255
            //     91: ldc 70
            //     93: ldc2_w 71
            //     96: ldc2_w 71
            //     99: ldc2_w 71
            //     102: ldc2_w 71
            //     105: iload 4
            //     107: iconst_1
            //     108: invokespecial 75	android/net/NetworkPolicy:<init>	(Landroid/net/NetworkTemplate;ILjava/lang/String;JJJJZZ)V
            //     111: astore 9
            //     113: aload_0
            //     114: getfield 12	com/android/server/net/NetworkPolicyManagerService$8:this$0	Lcom/android/server/net/NetworkPolicyManagerService;
            //     117: aload 9
            //     119: invokestatic 79	com/android/server/net/NetworkPolicyManagerService:access$1000	(Lcom/android/server/net/NetworkPolicyManagerService;Landroid/net/NetworkPolicy;)V
            //     122: aload 6
            //     124: monitorexit
            //     125: goto -110 -> 15
            //     128: astore 7
            //     130: aload 6
            //     132: monitorexit
            //     133: aload 7
            //     135: athrow
            //     136: aload 8
            //     138: ifnull -16 -> 122
            //     141: aload 8
            //     143: getfield 83	android/net/NetworkPolicy:inferred	Z
            //     146: ifeq -24 -> 122
            //     149: aload 8
            //     151: iload 4
            //     153: putfield 86	android/net/NetworkPolicy:metered	Z
            //     156: aload_0
            //     157: getfield 12	com/android/server/net/NetworkPolicyManagerService$8:this$0	Lcom/android/server/net/NetworkPolicyManagerService;
            //     160: invokestatic 89	com/android/server/net/NetworkPolicyManagerService:access$1100	(Lcom/android/server/net/NetworkPolicyManagerService;)V
            //     163: goto -41 -> 122
            //
            // Exception table:
            //     from	to	target	type
            //     56	133	128	finally
            //     141	163	128	finally
        }
    };

    public NetworkPolicyManagerService(Context paramContext, IActivityManager paramIActivityManager, IPowerManager paramIPowerManager, INetworkStatsService paramINetworkStatsService, INetworkManagementService paramINetworkManagementService)
    {
        this(paramContext, paramIActivityManager, paramIPowerManager, paramINetworkStatsService, paramINetworkManagementService, NtpTrustedTime.getInstance(paramContext), getSystemDir(), false);
    }

    public NetworkPolicyManagerService(Context paramContext, IActivityManager paramIActivityManager, IPowerManager paramIPowerManager, INetworkStatsService paramINetworkStatsService, INetworkManagementService paramINetworkManagementService, TrustedTime paramTrustedTime, File paramFile, boolean paramBoolean)
    {
        this.mContext = ((Context)Preconditions.checkNotNull(paramContext, "missing context"));
        this.mActivityManager = ((IActivityManager)Preconditions.checkNotNull(paramIActivityManager, "missing activityManager"));
        this.mPowerManager = ((IPowerManager)Preconditions.checkNotNull(paramIPowerManager, "missing powerManager"));
        this.mNetworkStats = ((INetworkStatsService)Preconditions.checkNotNull(paramINetworkStatsService, "missing networkStats"));
        this.mNetworkManager = ((INetworkManagementService)Preconditions.checkNotNull(paramINetworkManagementService, "missing networkManagement"));
        this.mTime = ((TrustedTime)Preconditions.checkNotNull(paramTrustedTime, "missing TrustedTime"));
        this.mHandlerThread = new HandlerThread("NetworkPolicy");
        this.mHandlerThread.start();
        this.mHandler = new Handler(this.mHandlerThread.getLooper(), this.mHandlerCallback);
        this.mSuppressDefaultPolicy = paramBoolean;
        this.mPolicyFile = new AtomicFile(new File(paramFile, "netpolicy.xml"));
    }

    private void addNetworkPolicyLocked(NetworkPolicy paramNetworkPolicy)
    {
        this.mNetworkPolicy.put(paramNetworkPolicy.template, paramNetworkPolicy);
        updateNetworkEnabledLocked();
        updateNetworkRulesLocked();
        updateNotificationsLocked();
        writePolicyLocked();
    }

    private static Intent buildAllowBackgroundDataIntent()
    {
        return new Intent("com.android.server.net.action.ALLOW_BACKGROUND");
    }

    private static Intent buildNetworkOverLimitIntent(NetworkTemplate paramNetworkTemplate)
    {
        Intent localIntent = new Intent();
        localIntent.setComponent(new ComponentName("com.android.systemui", "com.android.systemui.net.NetworkOverLimitActivity"));
        localIntent.addFlags(268435456);
        localIntent.putExtra("android.net.NETWORK_TEMPLATE", paramNetworkTemplate);
        return localIntent;
    }

    private String buildNotificationTag(NetworkPolicy paramNetworkPolicy, int paramInt)
    {
        return "NetworkPolicy:" + paramNetworkPolicy.template.hashCode() + ":" + paramInt;
    }

    private static Intent buildSnoozeWarningIntent(NetworkTemplate paramNetworkTemplate)
    {
        Intent localIntent = new Intent("com.android.server.net.action.SNOOZE_WARNING");
        localIntent.putExtra("android.net.NETWORK_TEMPLATE", paramNetworkTemplate);
        return localIntent;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private static Intent buildViewDataUsageIntent(NetworkTemplate paramNetworkTemplate)
    {
        Intent localIntent = new Intent();
        localIntent.setAction("android.intent.action.VIEW_DATA_USAGE_SUMMARY");
        localIntent.addFlags(268435456);
        localIntent.putExtra("android.net.NETWORK_TEMPLATE", paramNetworkTemplate);
        return localIntent;
    }

    private void cancelNotification(String paramString)
    {
        try
        {
            String str = this.mContext.getPackageName();
            this.mNotifManager.cancelNotificationWithTag(str, paramString, 0);
            label20: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label20;
        }
    }

    private static void collectKeys(SparseBooleanArray paramSparseBooleanArray1, SparseBooleanArray paramSparseBooleanArray2)
    {
        int i = paramSparseBooleanArray1.size();
        for (int j = 0; j < i; j++)
            paramSparseBooleanArray2.put(paramSparseBooleanArray1.keyAt(j), true);
    }

    private static void collectKeys(SparseIntArray paramSparseIntArray, SparseBooleanArray paramSparseBooleanArray)
    {
        int i = paramSparseIntArray.size();
        for (int j = 0; j < i; j++)
            paramSparseBooleanArray.put(paramSparseIntArray.keyAt(j), true);
    }

    private void computeUidForegroundLocked(int paramInt)
    {
        SparseBooleanArray localSparseBooleanArray = (SparseBooleanArray)this.mUidPidForeground.get(paramInt);
        boolean bool = false;
        int i = localSparseBooleanArray.size();
        for (int j = 0; ; j++)
            if (j < i)
            {
                if (localSparseBooleanArray.valueAt(j))
                    bool = true;
            }
            else
            {
                if (this.mUidForeground.get(paramInt, false) != bool)
                {
                    this.mUidForeground.put(paramInt, bool);
                    updateRulesForUidLocked(paramInt);
                }
                return;
            }
    }

    private long currentTimeMillis()
    {
        if (this.mTime.hasCache());
        for (long l = this.mTime.currentTimeMillis(); ; l = System.currentTimeMillis())
            return l;
    }

    private static void dumpSparseBooleanArray(PrintWriter paramPrintWriter, SparseBooleanArray paramSparseBooleanArray)
    {
        paramPrintWriter.print("[");
        int i = paramSparseBooleanArray.size();
        for (int j = 0; j < i; j++)
        {
            paramPrintWriter.print(paramSparseBooleanArray.keyAt(j) + "=" + paramSparseBooleanArray.valueAt(j));
            if (j < i - 1)
                paramPrintWriter.print(",");
        }
        paramPrintWriter.print("]");
    }

    private void enqueueNotification(NetworkPolicy paramNetworkPolicy, int paramInt, long paramLong)
    {
        String str1 = buildNotificationTag(paramNetworkPolicy, paramInt);
        Notification.Builder localBuilder = new Notification.Builder(this.mContext);
        localBuilder.setOnlyAlertOnce(true);
        localBuilder.setWhen(0L);
        Resources localResources = this.mContext.getResources();
        switch (paramInt)
        {
        default:
        case 1:
        case 2:
        case 3:
        }
        try
        {
            while (true)
            {
                String str3 = this.mContext.getPackageName();
                int[] arrayOfInt = new int[1];
                this.mNotifManager.enqueueNotificationWithTag(str3, str1, 0, localBuilder.getNotification(), arrayOfInt);
                this.mActiveNotifs.add(str1);
                label117: return;
                CharSequence localCharSequence4 = localResources.getText(17040599);
                String str4 = localResources.getString(17040600);
                localBuilder.setSmallIcon(17301624);
                localBuilder.setTicker(localCharSequence4);
                localBuilder.setContentTitle(localCharSequence4);
                localBuilder.setContentText(str4);
                Intent localIntent3 = buildSnoozeWarningIntent(paramNetworkPolicy.template);
                localBuilder.setDeleteIntent(PendingIntent.getBroadcast(this.mContext, 0, localIntent3, 134217728));
                Intent localIntent4 = buildViewDataUsageIntent(paramNetworkPolicy.template);
                localBuilder.setContentIntent(PendingIntent.getActivity(this.mContext, 0, localIntent4, 134217728));
            }
            CharSequence localCharSequence2 = localResources.getText(17040605);
            CharSequence localCharSequence3;
            switch (paramNetworkPolicy.template.getMatchRule())
            {
            default:
                localCharSequence3 = null;
            case 2:
            case 3:
            case 1:
            case 4:
            }
            while (true)
            {
                localBuilder.setOngoing(true);
                localBuilder.setSmallIcon(17302800);
                localBuilder.setTicker(localCharSequence3);
                localBuilder.setContentTitle(localCharSequence3);
                localBuilder.setContentText(localCharSequence2);
                Intent localIntent2 = buildNetworkOverLimitIntent(paramNetworkPolicy.template);
                localBuilder.setContentIntent(PendingIntent.getActivity(this.mContext, 0, localIntent2, 134217728));
                break;
                localCharSequence3 = localResources.getText(17040601);
                continue;
                localCharSequence3 = localResources.getText(17040602);
                continue;
                localCharSequence3 = localResources.getText(17040603);
                continue;
                localCharSequence3 = localResources.getText(17040604);
            }
            long l = paramLong - paramNetworkPolicy.limitBytes;
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = Formatter.formatFileSize(this.mContext, l);
            String str2 = localResources.getString(17040610, arrayOfObject);
            CharSequence localCharSequence1;
            switch (paramNetworkPolicy.template.getMatchRule())
            {
            default:
                localCharSequence1 = null;
            case 2:
            case 3:
            case 1:
            case 4:
            }
            while (true)
            {
                localBuilder.setOngoing(true);
                localBuilder.setSmallIcon(17301624);
                localBuilder.setTicker(localCharSequence1);
                localBuilder.setContentTitle(localCharSequence1);
                localBuilder.setContentText(str2);
                Intent localIntent1 = buildViewDataUsageIntent(paramNetworkPolicy.template);
                localBuilder.setContentIntent(PendingIntent.getActivity(this.mContext, 0, localIntent1, 134217728));
                break;
                localCharSequence1 = localResources.getText(17040606);
                continue;
                localCharSequence1 = localResources.getText(17040607);
                continue;
                localCharSequence1 = localResources.getText(17040608);
                continue;
                localCharSequence1 = localResources.getText(17040609);
            }
        }
        catch (RemoteException localRemoteException)
        {
            break label117;
        }
    }

    private void enqueueRestrictedNotification(String paramString)
    {
        Resources localResources = this.mContext.getResources();
        Notification.Builder localBuilder = new Notification.Builder(this.mContext);
        CharSequence localCharSequence = localResources.getText(17040611);
        String str1 = localResources.getString(17040612);
        localBuilder.setOnlyAlertOnce(true);
        localBuilder.setOngoing(true);
        localBuilder.setSmallIcon(17301624);
        localBuilder.setTicker(localCharSequence);
        localBuilder.setContentTitle(localCharSequence);
        localBuilder.setContentText(str1);
        Intent localIntent = buildAllowBackgroundDataIntent();
        localBuilder.setContentIntent(PendingIntent.getBroadcast(this.mContext, 0, localIntent, 134217728));
        try
        {
            String str2 = this.mContext.getPackageName();
            int[] arrayOfInt = new int[1];
            this.mNotifManager.enqueueNotificationWithTag(str2, paramString, 0, localBuilder.getNotification(), arrayOfInt);
            this.mActiveNotifs.add(paramString);
            label144: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label144;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    private void enqueueValidNotification(NetworkPolicy paramNetworkPolicy, int paramInt, long paramLong)
    {
        if (Injector.isIntervalValid(paramInt))
        {
            Injector.setInterval(paramInt);
            enqueueNotification(paramNetworkPolicy, paramInt, paramLong);
        }
    }

    private void ensureActiveMobilePolicyLocked()
    {
        if (this.mSuppressDefaultPolicy);
        while (true)
        {
            return;
            TelephonyManager localTelephonyManager = TelephonyManager.from(this.mContext);
            if (localTelephonyManager.getSimState() == 5)
            {
                String str1 = localTelephonyManager.getSubscriberId();
                NetworkIdentity localNetworkIdentity = new NetworkIdentity(0, 0, str1, null, false);
                int i = 0;
                Iterator localIterator = this.mNetworkPolicy.values().iterator();
                while (localIterator.hasNext())
                    if (((NetworkPolicy)localIterator.next()).template.matches(localNetworkIdentity))
                        i = 1;
                if (i == 0)
                {
                    Slog.i("NetworkPolicy", "no policy for active mobile network; generating default policy");
                    long l = 1048576L * this.mContext.getResources().getInteger(17694769);
                    Time localTime = new Time();
                    localTime.setToNow();
                    int j = localTime.monthDay;
                    String str2 = localTime.timezone;
                    addNetworkPolicyLocked(new NetworkPolicy(NetworkTemplate.buildTemplateMobileAll(str1), j, str2, l, -1L, -1L, -1L, true, true));
                }
            }
        }
    }

    private NetworkPolicy findPolicyForNetworkLocked(NetworkIdentity paramNetworkIdentity)
    {
        Iterator localIterator = this.mNetworkPolicy.values().iterator();
        NetworkPolicy localNetworkPolicy;
        do
        {
            if (!localIterator.hasNext())
                break;
            localNetworkPolicy = (NetworkPolicy)localIterator.next();
        }
        while (!localNetworkPolicy.template.matches(paramNetworkIdentity));
        while (true)
        {
            return localNetworkPolicy;
            localNetworkPolicy = null;
        }
    }

    private NetworkQuotaInfo getNetworkQuotaInfoUnchecked(NetworkState paramNetworkState)
    {
        NetworkIdentity localNetworkIdentity = NetworkIdentity.buildNetworkIdentity(this.mContext, paramNetworkState);
        NetworkPolicy localNetworkPolicy;
        NetworkQuotaInfo localNetworkQuotaInfo;
        synchronized (this.mRulesLock)
        {
            localNetworkPolicy = findPolicyForNetworkLocked(localNetworkIdentity);
            if ((localNetworkPolicy == null) || (!localNetworkPolicy.hasCycle()))
            {
                localNetworkQuotaInfo = null;
                return localNetworkQuotaInfo;
            }
        }
        long l1 = currentTimeMillis();
        long l2 = NetworkPolicyManager.computeLastCycleBoundary(l1, localNetworkPolicy);
        long l3 = getTotalBytes(localNetworkPolicy.template, l2, l1);
        long l4;
        if (localNetworkPolicy.warningBytes != -1L)
        {
            l4 = localNetworkPolicy.warningBytes;
            label100: if (localNetworkPolicy.limitBytes == -1L)
                break label145;
        }
        label145: for (long l5 = localNetworkPolicy.limitBytes; ; l5 = -1L)
        {
            localNetworkQuotaInfo = new NetworkQuotaInfo(l3, l4, l5);
            break;
            l4 = -1L;
            break label100;
        }
    }

    private static File getSystemDir()
    {
        return new File(Environment.getDataDirectory(), "system");
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private long getTotalBytes(NetworkTemplate paramNetworkTemplate, long paramLong1, long paramLong2)
    {
        try
        {
            long l2 = this.mNetworkStats.getNetworkTotalBytes(paramNetworkTemplate, paramLong1, paramLong2);
            long l3 = Injector.adjustMobileDataUsage(this, paramNetworkTemplate, paramLong1, paramLong2);
            l1 = l2 + l3;
            return l1;
        }
        catch (RuntimeException localRuntimeException)
        {
            while (true)
            {
                Slog.w("NetworkPolicy", "problem reading network stats: " + localRuntimeException);
                l1 = 0L;
            }
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                long l1 = 0L;
        }
    }

    // ERROR //
    private boolean isBandwidthControlEnabled()
    {
        // Byte code:
        //     0: invokestatic 877	android/os/Binder:clearCallingIdentity	()J
        //     3: lstore_1
        //     4: aload_0
        //     5: getfield 349	com/android/server/net/NetworkPolicyManagerService:mNetworkManager	Landroid/os/INetworkManagementService;
        //     8: invokeinterface 879 1 0
        //     13: istore 6
        //     15: iload 6
        //     17: istore 5
        //     19: lload_1
        //     20: invokestatic 883	android/os/Binder:restoreCallingIdentity	(J)V
        //     23: iload 5
        //     25: ireturn
        //     26: astore 4
        //     28: iconst_0
        //     29: istore 5
        //     31: goto -12 -> 19
        //     34: astore_3
        //     35: lload_1
        //     36: invokestatic 883	android/os/Binder:restoreCallingIdentity	(J)V
        //     39: aload_3
        //     40: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     4	15	26	android/os/RemoteException
        //     4	15	34	finally
    }

    private boolean isTemplateRelevant(NetworkTemplate paramNetworkTemplate)
    {
        TelephonyManager localTelephonyManager = TelephonyManager.from(this.mContext);
        boolean bool;
        switch (paramNetworkTemplate.getMatchRule())
        {
        default:
            bool = true;
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            return bool;
            if (localTelephonyManager.getSimState() == 5)
                bool = Objects.equal(localTelephonyManager.getSubscriberId(), paramNetworkTemplate.getSubscriberId());
            else
                bool = false;
        }
    }

    private static boolean isUidValidForRules(int paramInt)
    {
        if ((paramInt == 1013) || (paramInt == 1019) || (UserId.isApp(paramInt)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void maybeRefreshTrustedTime()
    {
        if (this.mTime.getCacheAge() > 86400000L)
            this.mTime.forceRefresh();
    }

    private void notifyOverLimitLocked(NetworkTemplate paramNetworkTemplate)
    {
        if (!this.mOverLimitNotified.contains(paramNetworkTemplate))
        {
            this.mContext.startActivity(buildNetworkOverLimitIntent(paramNetworkTemplate));
            this.mOverLimitNotified.add(paramNetworkTemplate);
        }
    }

    private void notifyUnderLimitLocked(NetworkTemplate paramNetworkTemplate)
    {
        this.mOverLimitNotified.remove(paramNetworkTemplate);
    }

    // ERROR //
    private void performSnooze(NetworkTemplate paramNetworkTemplate, int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 444	com/android/server/net/NetworkPolicyManagerService:maybeRefreshTrustedTime	()V
        //     4: aload_0
        //     5: invokespecial 830	com/android/server/net/NetworkPolicyManagerService:currentTimeMillis	()J
        //     8: lstore_3
        //     9: aload_0
        //     10: getfield 234	com/android/server/net/NetworkPolicyManagerService:mRulesLock	Ljava/lang/Object;
        //     13: astore 5
        //     15: aload 5
        //     17: monitorenter
        //     18: aload_0
        //     19: getfield 242	com/android/server/net/NetworkPolicyManagerService:mNetworkPolicy	Ljava/util/HashMap;
        //     22: aload_1
        //     23: invokevirtual 920	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     26: checkcast 462	android/net/NetworkPolicy
        //     29: astore 7
        //     31: aload 7
        //     33: ifnonnull +39 -> 72
        //     36: new 922	java/lang/IllegalArgumentException
        //     39: dup
        //     40: new 507	java/lang/StringBuilder
        //     43: dup
        //     44: invokespecial 508	java/lang/StringBuilder:<init>	()V
        //     47: ldc_w 924
        //     50: invokevirtual 514	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     53: aload_1
        //     54: invokevirtual 868	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     57: invokevirtual 529	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     60: invokespecial 925	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     63: athrow
        //     64: astore 6
        //     66: aload 5
        //     68: monitorexit
        //     69: aload 6
        //     71: athrow
        //     72: iload_2
        //     73: tableswitch	default:+23 -> 96, 1:+34->107, 2:+60->133
        //     97: iconst_0
        //     98: ifne +22803 -> 22901
        //     101: iconst_0
        //     102: if_icmpeq -18685 -> -18583
        //     105: ifgt -16615 -> -16510
        //     108: iconst_4
        //     109: lload_3
        //     110: putfield 929	android/net/NetworkPolicy:lastWarningSnooze	J
        //     113: aload_0
        //     114: invokespecial 448	com/android/server/net/NetworkPolicyManagerService:updateNetworkEnabledLocked	()V
        //     117: aload_0
        //     118: invokespecial 406	com/android/server/net/NetworkPolicyManagerService:updateNetworkRulesLocked	()V
        //     121: aload_0
        //     122: invokespecial 452	com/android/server/net/NetworkPolicyManagerService:updateNotificationsLocked	()V
        //     125: aload_0
        //     126: invokespecial 440	com/android/server/net/NetworkPolicyManagerService:writePolicyLocked	()V
        //     129: aload 5
        //     131: monitorexit
        //     132: return
        //     133: aload 7
        //     135: lload_3
        //     136: putfield 931	android/net/NetworkPolicy:lastLimitSnooze	J
        //     139: goto -26 -> 113
        //
        // Exception table:
        //     from	to	target	type
        //     18	69	64	finally
        //     96	139	64	finally
    }

    // ERROR //
    private void readPolicyLocked()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 242	com/android/server/net/NetworkPolicyManagerService:mNetworkPolicy	Ljava/util/HashMap;
        //     4: invokevirtual 941	java/util/HashMap:clear	()V
        //     7: aload_0
        //     8: getfield 249	com/android/server/net/NetworkPolicyManagerService:mAppPolicy	Landroid/util/SparseIntArray;
        //     11: invokevirtual 942	android/util/SparseIntArray:clear	()V
        //     14: aconst_null
        //     15: astore_1
        //     16: aload_0
        //     17: getfield 392	com/android/server/net/NetworkPolicyManagerService:mPolicyFile	Lcom/android/internal/os/AtomicFile;
        //     20: invokevirtual 946	com/android/internal/os/AtomicFile:openRead	()Ljava/io/FileInputStream;
        //     23: astore_1
        //     24: invokestatic 952	android/util/Xml:newPullParser	()Lorg/xmlpull/v1/XmlPullParser;
        //     27: astore 8
        //     29: aload 8
        //     31: aload_1
        //     32: aconst_null
        //     33: invokeinterface 958 3 0
        //     38: iconst_1
        //     39: istore 9
        //     41: aload 8
        //     43: invokeinterface 960 1 0
        //     48: istore 10
        //     50: iload 10
        //     52: iconst_1
        //     53: if_icmpeq +63 -> 116
        //     56: aload 8
        //     58: invokeinterface 963 1 0
        //     63: astore 11
        //     65: iload 10
        //     67: iconst_2
        //     68: if_icmpne -27 -> 41
        //     71: ldc 126
        //     73: aload 11
        //     75: invokevirtual 968	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     78: ifeq +76 -> 154
        //     81: aload 8
        //     83: ldc 87
        //     85: invokestatic 972	com/android/server/net/NetworkPolicyManagerService$XmlUtils:readIntAttribute	(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I
        //     88: istore 9
        //     90: iload 9
        //     92: iconst_3
        //     93: if_icmplt +28 -> 121
        //     96: aload_0
        //     97: aload 8
        //     99: ldc 78
        //     101: invokestatic 976	com/android/server/net/NetworkPolicyManagerService$XmlUtils:readBooleanAttribute	(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z
        //     104: putfield 978	com/android/server/net/NetworkPolicyManagerService:mRestrictBackground	Z
        //     107: goto -66 -> 41
        //     110: astore 7
        //     112: aload_0
        //     113: invokespecial 981	com/android/server/net/NetworkPolicyManagerService:upgradeLegacyBackgroundData	()V
        //     116: aload_1
        //     117: invokestatic 987	libcore/io/IoUtils:closeQuietly	(Ljava/lang/AutoCloseable;)V
        //     120: return
        //     121: aload_0
        //     122: iconst_0
        //     123: putfield 978	com/android/server/net/NetworkPolicyManagerService:mRestrictBackground	Z
        //     126: goto -85 -> 41
        //     129: astore 5
        //     131: ldc 114
        //     133: ldc_w 989
        //     136: aload 5
        //     138: invokestatic 995	android/util/Log:wtf	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     141: pop
        //     142: goto -26 -> 116
        //     145: astore 4
        //     147: aload_1
        //     148: invokestatic 987	libcore/io/IoUtils:closeQuietly	(Ljava/lang/AutoCloseable;)V
        //     151: aload 4
        //     153: athrow
        //     154: ldc 123
        //     156: aload 11
        //     158: invokevirtual 968	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     161: ifeq +249 -> 410
        //     164: aload 8
        //     166: ldc 72
        //     168: invokestatic 972	com/android/server/net/NetworkPolicyManagerService$XmlUtils:readIntAttribute	(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I
        //     171: istore 19
        //     173: aload 8
        //     175: aconst_null
        //     176: ldc 81
        //     178: invokeinterface 999 3 0
        //     183: astore 20
        //     185: iload 9
        //     187: bipush 9
        //     189: if_icmplt +189 -> 378
        //     192: aload 8
        //     194: aconst_null
        //     195: ldc 69
        //     197: invokeinterface 999 3 0
        //     202: astore 21
        //     204: aload 8
        //     206: ldc 45
        //     208: invokestatic 972	com/android/server/net/NetworkPolicyManagerService$XmlUtils:readIntAttribute	(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I
        //     211: istore 22
        //     213: iload 9
        //     215: bipush 6
        //     217: if_icmplt +167 -> 384
        //     220: aload 8
        //     222: aconst_null
        //     223: ldc 48
        //     225: invokeinterface 999 3 0
        //     230: astore 23
        //     232: aload 8
        //     234: ldc 90
        //     236: invokestatic 1003	com/android/server/net/NetworkPolicyManagerService$XmlUtils:readLongAttribute	(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)J
        //     239: lstore 24
        //     241: aload 8
        //     243: ldc 63
        //     245: invokestatic 1003	com/android/server/net/NetworkPolicyManagerService$XmlUtils:readLongAttribute	(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)J
        //     248: lstore 26
        //     250: iload 9
        //     252: iconst_5
        //     253: if_icmplt +139 -> 392
        //     256: aload 8
        //     258: ldc 54
        //     260: invokestatic 1003	com/android/server/net/NetworkPolicyManagerService$XmlUtils:readLongAttribute	(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)J
        //     263: lstore 28
        //     265: iload 9
        //     267: iconst_4
        //     268: if_icmplt +339 -> 607
        //     271: aload 8
        //     273: ldc 66
        //     275: invokestatic 976	com/android/server/net/NetworkPolicyManagerService$XmlUtils:readBooleanAttribute	(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z
        //     278: istore 30
        //     280: iload 9
        //     282: iconst_5
        //     283: if_icmplt +365 -> 648
        //     286: aload 8
        //     288: ldc 60
        //     290: invokestatic 1003	com/android/server/net/NetworkPolicyManagerService$XmlUtils:readLongAttribute	(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)J
        //     293: lstore 31
        //     295: iload 9
        //     297: bipush 7
        //     299: if_icmplt +357 -> 656
        //     302: aload 8
        //     304: ldc 51
        //     306: invokestatic 976	com/android/server/net/NetworkPolicyManagerService$XmlUtils:readBooleanAttribute	(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z
        //     309: istore 33
        //     311: new 516	android/net/NetworkTemplate
        //     314: dup
        //     315: iload 19
        //     317: aload 20
        //     319: aload 21
        //     321: invokespecial 1006	android/net/NetworkTemplate:<init>	(ILjava/lang/String;Ljava/lang/String;)V
        //     324: astore 34
        //     326: aload_0
        //     327: getfield 242	com/android/server/net/NetworkPolicyManagerService:mNetworkPolicy	Ljava/util/HashMap;
        //     330: aload 34
        //     332: new 462	android/net/NetworkPolicy
        //     335: dup
        //     336: aload 34
        //     338: iload 22
        //     340: aload 23
        //     342: lload 24
        //     344: lload 26
        //     346: lload 31
        //     348: lload 28
        //     350: iload 30
        //     352: iload 33
        //     354: invokespecial 816	android/net/NetworkPolicy:<init>	(Landroid/net/NetworkTemplate;ILjava/lang/String;JJJJZZ)V
        //     357: invokevirtual 471	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     360: pop
        //     361: goto -320 -> 41
        //     364: astore_2
        //     365: ldc 114
        //     367: ldc_w 989
        //     370: aload_2
        //     371: invokestatic 995	android/util/Log:wtf	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     374: pop
        //     375: goto -259 -> 116
        //     378: aconst_null
        //     379: astore 21
        //     381: goto -177 -> 204
        //     384: ldc_w 1008
        //     387: astore 23
        //     389: goto -157 -> 232
        //     392: iload 9
        //     394: iconst_2
        //     395: if_icmplt +204 -> 599
        //     398: aload 8
        //     400: ldc 57
        //     402: invokestatic 1003	com/android/server/net/NetworkPolicyManagerService$XmlUtils:readLongAttribute	(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)J
        //     405: lstore 28
        //     407: goto -142 -> 265
        //     410: ldc 129
        //     412: aload 11
        //     414: invokevirtual 968	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     417: ifeq +91 -> 508
        //     420: iload 9
        //     422: bipush 8
        //     424: if_icmpge +84 -> 508
        //     427: aload 8
        //     429: ldc 84
        //     431: invokestatic 972	com/android/server/net/NetworkPolicyManagerService$XmlUtils:readIntAttribute	(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I
        //     434: istore 15
        //     436: aload 8
        //     438: ldc 75
        //     440: invokestatic 972	com/android/server/net/NetworkPolicyManagerService$XmlUtils:readIntAttribute	(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I
        //     443: istore 16
        //     445: iload 15
        //     447: invokestatic 1011	android/os/UserId:getAppId	(I)I
        //     450: istore 17
        //     452: iload 17
        //     454: invokestatic 898	android/os/UserId:isApp	(I)Z
        //     457: ifeq +15 -> 472
        //     460: aload_0
        //     461: iload 17
        //     463: iload 16
        //     465: iconst_0
        //     466: invokespecial 1015	com/android/server/net/NetworkPolicyManagerService:setAppPolicyUnchecked	(IIZ)V
        //     469: goto -428 -> 41
        //     472: ldc 114
        //     474: new 507	java/lang/StringBuilder
        //     477: dup
        //     478: invokespecial 508	java/lang/StringBuilder:<init>	()V
        //     481: ldc_w 1017
        //     484: invokevirtual 514	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     487: iload 15
        //     489: invokevirtual 523	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     492: ldc_w 1019
        //     495: invokevirtual 514	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     498: invokevirtual 529	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     501: invokestatic 871	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     504: pop
        //     505: goto -464 -> 41
        //     508: ldc 120
        //     510: aload 11
        //     512: invokevirtual 968	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     515: ifeq -474 -> 41
        //     518: iload 9
        //     520: bipush 8
        //     522: if_icmplt -481 -> 41
        //     525: aload 8
        //     527: ldc 42
        //     529: invokestatic 972	com/android/server/net/NetworkPolicyManagerService$XmlUtils:readIntAttribute	(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I
        //     532: istore 12
        //     534: aload 8
        //     536: ldc 75
        //     538: invokestatic 972	com/android/server/net/NetworkPolicyManagerService$XmlUtils:readIntAttribute	(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I
        //     541: istore 13
        //     543: iload 12
        //     545: invokestatic 898	android/os/UserId:isApp	(I)Z
        //     548: ifeq +15 -> 563
        //     551: aload_0
        //     552: iload 12
        //     554: iload 13
        //     556: iconst_0
        //     557: invokespecial 1015	com/android/server/net/NetworkPolicyManagerService:setAppPolicyUnchecked	(IIZ)V
        //     560: goto -519 -> 41
        //     563: ldc 114
        //     565: new 507	java/lang/StringBuilder
        //     568: dup
        //     569: invokespecial 508	java/lang/StringBuilder:<init>	()V
        //     572: ldc_w 1021
        //     575: invokevirtual 514	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     578: iload 12
        //     580: invokevirtual 523	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     583: ldc_w 1019
        //     586: invokevirtual 514	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     589: invokevirtual 529	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     592: invokestatic 871	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     595: pop
        //     596: goto -555 -> 41
        //     599: ldc2_w 812
        //     602: lstore 28
        //     604: goto -339 -> 265
        //     607: iload 19
        //     609: tableswitch	default:+27 -> 636, 1:+33->642, 2:+33->642, 3:+33->642
        //     637: istore 30
        //     639: goto -359 -> 280
        //     642: iconst_1
        //     643: istore 30
        //     645: goto -365 -> 280
        //     648: ldc2_w 812
        //     651: lstore 31
        //     653: goto -358 -> 295
        //     656: iconst_0
        //     657: istore 33
        //     659: goto -348 -> 311
        //
        // Exception table:
        //     from	to	target	type
        //     16	107	110	java/io/FileNotFoundException
        //     121	126	110	java/io/FileNotFoundException
        //     154	361	110	java/io/FileNotFoundException
        //     384	596	110	java/io/FileNotFoundException
        //     16	107	129	java/io/IOException
        //     121	126	129	java/io/IOException
        //     154	361	129	java/io/IOException
        //     384	596	129	java/io/IOException
        //     16	107	145	finally
        //     112	116	145	finally
        //     121	126	145	finally
        //     131	142	145	finally
        //     154	361	145	finally
        //     365	375	145	finally
        //     384	596	145	finally
        //     16	107	364	org/xmlpull/v1/XmlPullParserException
        //     121	126	364	org/xmlpull/v1/XmlPullParserException
        //     154	361	364	org/xmlpull/v1/XmlPullParserException
        //     384	596	364	org/xmlpull/v1/XmlPullParserException
    }

    private void removeInterfaceQuota(String paramString)
    {
        try
        {
            this.mNetworkManager.removeInterfaceQuota(paramString);
            label10: return;
        }
        catch (IllegalStateException localIllegalStateException)
        {
            while (true)
                Log.wtf("NetworkPolicy", "problem removing interface quota", localIllegalStateException);
        }
        catch (RemoteException localRemoteException)
        {
            break label10;
        }
    }

    private void setAppPolicyUnchecked(int paramInt1, int paramInt2, boolean paramBoolean)
    {
        synchronized (this.mRulesLock)
        {
            getAppPolicy(paramInt1);
            this.mAppPolicy.put(paramInt1, paramInt2);
            updateRulesForAppLocked(paramInt1);
            if (paramBoolean)
                writePolicyLocked();
            return;
        }
    }

    private void setInterfaceQuota(String paramString, long paramLong)
    {
        try
        {
            this.mNetworkManager.setInterfaceQuota(paramString, paramLong);
            label11: return;
        }
        catch (IllegalStateException localIllegalStateException)
        {
            while (true)
                Log.wtf("NetworkPolicy", "problem setting interface quota", localIllegalStateException);
        }
        catch (RemoteException localRemoteException)
        {
            break label11;
        }
    }

    private void setNetworkTemplateEnabled(NetworkTemplate paramNetworkTemplate, boolean paramBoolean)
    {
        Injector.setNetworkTemplateEnabled(this, paramNetworkTemplate, paramBoolean);
        TelephonyManager localTelephonyManager = TelephonyManager.from(this.mContext);
        switch (paramNetworkTemplate.getMatchRule())
        {
        default:
            throw new IllegalArgumentException("unexpected template");
        case 1:
        case 2:
        case 3:
            if ((localTelephonyManager.getSimState() == 5) && (Objects.equal(localTelephonyManager.getSubscriberId(), paramNetworkTemplate.getSubscriberId())))
            {
                setPolicyDataEnable(0, paramBoolean);
                setPolicyDataEnable(6, paramBoolean);
            }
            break;
        case 4:
        case 5:
        }
        while (true)
        {
            return;
            setPolicyDataEnable(1, paramBoolean);
            continue;
            setPolicyDataEnable(9, paramBoolean);
        }
    }

    private void setPolicyDataEnable(int paramInt, boolean paramBoolean)
    {
        try
        {
            this.mConnManager.setPolicyDataEnable(paramInt, paramBoolean);
            label11: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label11;
        }
    }

    private void setUidNetworkRules(int paramInt, boolean paramBoolean)
    {
        try
        {
            this.mNetworkManager.setUidNetworkRules(paramInt, paramBoolean);
            label11: return;
        }
        catch (IllegalStateException localIllegalStateException)
        {
            while (true)
                Log.wtf("NetworkPolicy", "problem setting uid rules", localIllegalStateException);
        }
        catch (RemoteException localRemoteException)
        {
            break label11;
        }
    }

    private void updateNetworkEnabledLocked()
    {
        long l1 = currentTimeMillis();
        Iterator localIterator = this.mNetworkPolicy.values().iterator();
        while (localIterator.hasNext())
        {
            NetworkPolicy localNetworkPolicy = (NetworkPolicy)localIterator.next();
            if ((localNetworkPolicy.limitBytes == -1L) || (!localNetworkPolicy.hasCycle()))
            {
                setNetworkTemplateEnabled(localNetworkPolicy.template, true);
            }
            else
            {
                long l2 = NetworkPolicyManager.computeLastCycleBoundary(l1, localNetworkPolicy);
                int i;
                if ((localNetworkPolicy.isOverLimit(getTotalBytes(localNetworkPolicy.template, l2, l1))) && (localNetworkPolicy.lastLimitSnooze < l2))
                {
                    i = 1;
                    label113: if (i != 0)
                        break label141;
                }
                label141: for (boolean bool = true; ; bool = false)
                {
                    setNetworkTemplateEnabled(localNetworkPolicy.template, bool);
                    break;
                    i = 0;
                    break label113;
                }
            }
        }
    }

    private void updateNetworkRulesLocked()
    {
        HashMap localHashMap;
        try
        {
            NetworkState[] arrayOfNetworkState = this.mConnManager.getAllNetworkState();
            localHashMap = Maps.newHashMap();
            int i = arrayOfNetworkState.length;
            for (int j = 0; j < i; j++)
            {
                NetworkState localNetworkState = arrayOfNetworkState[j];
                if (localNetworkState.networkInfo.isConnected())
                {
                    String str3 = localNetworkState.linkProperties.getInterfaceName();
                    localHashMap.put(NetworkIdentity.buildNetworkIdentity(this.mContext, localNetworkState), str3);
                }
            }
        }
        catch (RemoteException localRemoteException)
        {
        }
        while (true)
        {
            return;
            this.mNetworkRules.clear();
            ArrayList localArrayList = Lists.newArrayList();
            Iterator localIterator1 = this.mNetworkPolicy.values().iterator();
            while (localIterator1.hasNext())
            {
                NetworkPolicy localNetworkPolicy2 = (NetworkPolicy)localIterator1.next();
                localArrayList.clear();
                Iterator localIterator4 = localHashMap.entrySet().iterator();
                while (localIterator4.hasNext())
                {
                    Map.Entry localEntry = (Map.Entry)localIterator4.next();
                    NetworkIdentity localNetworkIdentity = (NetworkIdentity)localEntry.getKey();
                    if (localNetworkPolicy2.template.matches(localNetworkIdentity))
                        localArrayList.add((String)localEntry.getValue());
                }
                if (localArrayList.size() > 0)
                {
                    String[] arrayOfString3 = (String[])localArrayList.toArray(new String[localArrayList.size()]);
                    this.mNetworkRules.put(localNetworkPolicy2, arrayOfString3);
                }
            }
            long l1 = 9223372036854775807L;
            HashSet localHashSet = Sets.newHashSet();
            long l2 = currentTimeMillis();
            Iterator localIterator2 = this.mNetworkRules.keySet().iterator();
            while (localIterator2.hasNext())
            {
                NetworkPolicy localNetworkPolicy1 = (NetworkPolicy)localIterator2.next();
                String[] arrayOfString2 = (String[])this.mNetworkRules.get(localNetworkPolicy1);
                long l3;
                long l4;
                int k;
                label363: int m;
                label378: long l5;
                if (localNetworkPolicy1.hasCycle())
                {
                    l3 = NetworkPolicyManager.computeLastCycleBoundary(l2, localNetworkPolicy1);
                    l4 = getTotalBytes(localNetworkPolicy1.template, l3, l2);
                    if (localNetworkPolicy1.warningBytes == -1L)
                        break label478;
                    k = 1;
                    if (localNetworkPolicy1.limitBytes == -1L)
                        break label484;
                    m = 1;
                    if ((m == 0) && (!localNetworkPolicy1.metered))
                        break label526;
                    if (m != 0)
                        break label490;
                    l5 = 9223372036854775807L;
                }
                while (true)
                {
                    if (arrayOfString2.length > 1)
                        Slog.w("NetworkPolicy", "shared quota unsupported; generating rule for each iface");
                    int n = arrayOfString2.length;
                    for (int i1 = 0; i1 < n; i1++)
                    {
                        String str2 = arrayOfString2[i1];
                        removeInterfaceQuota(str2);
                        setInterfaceQuota(str2, l5);
                        localHashSet.add(str2);
                    }
                    l3 = 9223372036854775807L;
                    l4 = 0L;
                    break;
                    label478: k = 0;
                    break label363;
                    label484: m = 0;
                    break label378;
                    label490: if (localNetworkPolicy1.lastLimitSnooze >= l3)
                        l5 = 9223372036854775807L;
                    else
                        l5 = Math.max(1L, localNetworkPolicy1.limitBytes - l4);
                }
                label526: if ((k != 0) && (localNetworkPolicy1.warningBytes < l1))
                    l1 = localNetworkPolicy1.warningBytes;
                if ((m != 0) && (localNetworkPolicy1.limitBytes < l1))
                    l1 = localNetworkPolicy1.limitBytes;
            }
            this.mHandler.obtainMessage(7, Long.valueOf(l1)).sendToTarget();
            Iterator localIterator3 = this.mMeteredIfaces.iterator();
            while (localIterator3.hasNext())
            {
                String str1 = (String)localIterator3.next();
                if (!localHashSet.contains(str1))
                    removeInterfaceQuota(str1);
            }
            this.mMeteredIfaces = localHashSet;
            String[] arrayOfString1 = (String[])this.mMeteredIfaces.toArray(new String[this.mMeteredIfaces.size()]);
            this.mHandler.obtainMessage(2, arrayOfString1).sendToTarget();
        }
    }

    private void updateNotificationsLocked()
    {
        HashSet localHashSet = Sets.newHashSet();
        localHashSet.addAll(this.mActiveNotifs);
        this.mActiveNotifs.clear();
        long l1 = currentTimeMillis();
        Iterator localIterator1 = this.mNetworkPolicy.values().iterator();
        while (localIterator1.hasNext())
        {
            NetworkPolicy localNetworkPolicy = (NetworkPolicy)localIterator1.next();
            if ((isTemplateRelevant(localNetworkPolicy.template)) && (localNetworkPolicy.hasCycle()))
            {
                long l2 = NetworkPolicyManager.computeLastCycleBoundary(l1, localNetworkPolicy);
                long l3 = getTotalBytes(localNetworkPolicy.template, l2, l1);
                if (localNetworkPolicy.isOverLimit(l3))
                {
                    if (localNetworkPolicy.lastLimitSnooze >= l2)
                    {
                        enqueueValidNotification(localNetworkPolicy, 3, l3);
                    }
                    else
                    {
                        enqueueValidNotification(localNetworkPolicy, 2, l3);
                        notifyOverLimitLocked(localNetworkPolicy.template);
                    }
                }
                else
                {
                    notifyUnderLimitLocked(localNetworkPolicy.template);
                    if ((localNetworkPolicy.isOverWarning(l3)) && (localNetworkPolicy.lastWarningSnooze < l2))
                        enqueueValidNotification(localNetworkPolicy, 1, l3);
                }
            }
        }
        if (this.mRestrictBackground)
            enqueueRestrictedNotification("NetworkPolicy:allowBackground");
        Iterator localIterator2 = localHashSet.iterator();
        while (localIterator2.hasNext())
        {
            String str = (String)localIterator2.next();
            if (!this.mActiveNotifs.contains(str))
                cancelNotification(str);
        }
    }

    private void updateRulesForAppLocked(int paramInt)
    {
        Iterator localIterator = this.mContext.getPackageManager().getUsers().iterator();
        while (localIterator.hasNext())
            updateRulesForUidLocked(UserId.getUid(((UserInfo)localIterator.next()).id, paramInt));
    }

    private void updateRulesForRestrictBackgroundLocked()
    {
        Iterator localIterator = this.mContext.getPackageManager().getInstalledApplications(0).iterator();
        while (localIterator.hasNext())
            updateRulesForAppLocked(UserId.getAppId(((ApplicationInfo)localIterator.next()).uid));
        updateRulesForUidLocked(1013);
        updateRulesForUidLocked(1019);
    }

    private void updateRulesForScreenLocked()
    {
        int i = this.mUidForeground.size();
        for (int j = 0; j < i; j++)
            if (this.mUidForeground.valueAt(j))
                updateRulesForUidLocked(this.mUidForeground.keyAt(j));
    }

    private void updateRulesForUidLocked(int paramInt)
    {
        if (!isUidValidForRules(paramInt))
            return;
        int i = getAppPolicy(UserId.getAppId(paramInt));
        boolean bool1 = isUidForeground(paramInt);
        int j = 0;
        if ((!bool1) && ((i & 0x1) != 0))
            j = 1;
        if ((!bool1) && (this.mRestrictBackground))
            j = 1;
        if (j == 0)
        {
            this.mUidRules.delete(paramInt);
            label66: if ((j & 0x1) == 0)
                break label129;
        }
        label129: for (boolean bool2 = true; ; bool2 = false)
        {
            while (true)
            {
                setUidNetworkRules(paramInt, bool2);
                this.mHandler.obtainMessage(1, paramInt, j).sendToTarget();
                try
                {
                    this.mNetworkStats.setUidForeground(paramInt, bool1);
                }
                catch (RemoteException localRemoteException)
                {
                }
            }
            break;
            this.mUidRules.put(paramInt, j);
            break label66;
        }
    }

    private void updateScreenOn()
    {
        try
        {
            synchronized (this.mRulesLock)
            {
                this.mScreenOn = this.mPowerManager.isScreenOn();
                label20: updateRulesForScreenLocked();
                return;
            }
        }
        catch (RemoteException localRemoteException)
        {
            break label20;
        }
    }

    private void upgradeLegacyBackgroundData()
    {
        int i = 1;
        if (Settings.Secure.getInt(this.mContext.getContentResolver(), "background_data", i) != i);
        while (true)
        {
            this.mRestrictBackground = i;
            if (this.mRestrictBackground)
            {
                Intent localIntent = new Intent("android.net.conn.BACKGROUND_DATA_SETTING_CHANGED");
                this.mContext.sendBroadcast(localIntent);
            }
            return;
            i = 0;
        }
    }

    private void writePolicyLocked()
    {
        FileOutputStream localFileOutputStream = null;
        FastXmlSerializer localFastXmlSerializer;
        try
        {
            localFileOutputStream = this.mPolicyFile.startWrite();
            localFastXmlSerializer = new FastXmlSerializer();
            localFastXmlSerializer.setOutput(localFileOutputStream, "utf-8");
            localFastXmlSerializer.startDocument(null, Boolean.valueOf(true));
            localFastXmlSerializer.startTag(null, "policy-list");
            XmlUtils.writeIntAttribute(localFastXmlSerializer, "version", 9);
            XmlUtils.writeBooleanAttribute(localFastXmlSerializer, "restrictBackground", this.mRestrictBackground);
            Iterator localIterator = this.mNetworkPolicy.values().iterator();
            while (localIterator.hasNext())
            {
                NetworkPolicy localNetworkPolicy = (NetworkPolicy)localIterator.next();
                NetworkTemplate localNetworkTemplate = localNetworkPolicy.template;
                localFastXmlSerializer.startTag(null, "network-policy");
                XmlUtils.writeIntAttribute(localFastXmlSerializer, "networkTemplate", localNetworkTemplate.getMatchRule());
                String str1 = localNetworkTemplate.getSubscriberId();
                if (str1 != null)
                    localFastXmlSerializer.attribute(null, "subscriberId", str1);
                String str2 = localNetworkTemplate.getNetworkId();
                if (str2 != null)
                    localFastXmlSerializer.attribute(null, "networkId", str2);
                XmlUtils.writeIntAttribute(localFastXmlSerializer, "cycleDay", localNetworkPolicy.cycleDay);
                localFastXmlSerializer.attribute(null, "cycleTimezone", localNetworkPolicy.cycleTimezone);
                XmlUtils.writeLongAttribute(localFastXmlSerializer, "warningBytes", localNetworkPolicy.warningBytes);
                XmlUtils.writeLongAttribute(localFastXmlSerializer, "limitBytes", localNetworkPolicy.limitBytes);
                XmlUtils.writeLongAttribute(localFastXmlSerializer, "lastWarningSnooze", localNetworkPolicy.lastWarningSnooze);
                XmlUtils.writeLongAttribute(localFastXmlSerializer, "lastLimitSnooze", localNetworkPolicy.lastLimitSnooze);
                XmlUtils.writeBooleanAttribute(localFastXmlSerializer, "metered", localNetworkPolicy.metered);
                XmlUtils.writeBooleanAttribute(localFastXmlSerializer, "inferred", localNetworkPolicy.inferred);
                localFastXmlSerializer.endTag(null, "network-policy");
            }
        }
        catch (IOException localIOException)
        {
            if (localFileOutputStream != null)
                this.mPolicyFile.failWrite(localFileOutputStream);
        }
        return;
        for (int i = 0; ; i++)
            if (i < this.mAppPolicy.size())
            {
                int j = this.mAppPolicy.keyAt(i);
                int k = this.mAppPolicy.valueAt(i);
                if (k != 0)
                {
                    localFastXmlSerializer.startTag(null, "app-policy");
                    XmlUtils.writeIntAttribute(localFastXmlSerializer, "appId", j);
                    XmlUtils.writeIntAttribute(localFastXmlSerializer, "policy", k);
                    localFastXmlSerializer.endTag(null, "app-policy");
                }
            }
            else
            {
                localFastXmlSerializer.endTag(null, "policy-list");
                localFastXmlSerializer.endDocument();
                this.mPolicyFile.finishWrite(localFileOutputStream);
                break;
            }
    }

    public void addIdleHandler(MessageQueue.IdleHandler paramIdleHandler)
    {
        this.mHandler.getLooper().getQueue().addIdleHandler(paramIdleHandler);
    }

    public void bindConnectivityManager(IConnectivityManager paramIConnectivityManager)
    {
        this.mConnManager = ((IConnectivityManager)Preconditions.checkNotNull(paramIConnectivityManager, "missing IConnectivityManager"));
    }

    public void bindNotificationManager(INotificationManager paramINotificationManager)
    {
        this.mNotifManager = ((INotificationManager)Preconditions.checkNotNull(paramINotificationManager, "missing INotificationManager"));
    }

    // ERROR //
    protected void dump(java.io.FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 325	com/android/server/net/NetworkPolicyManagerService:mContext	Landroid/content/Context;
        //     4: ldc_w 1341
        //     7: ldc 114
        //     9: invokevirtual 1344	android/content/Context:enforceCallingOrSelfPermission	(Ljava/lang/String;Ljava/lang/String;)V
        //     12: new 1346	com/android/internal/util/IndentingPrintWriter
        //     15: dup
        //     16: aload_2
        //     17: ldc_w 1348
        //     20: invokespecial 1351	com/android/internal/util/IndentingPrintWriter:<init>	(Ljava/io/Writer;Ljava/lang/String;)V
        //     23: astore 4
        //     25: new 644	java/util/HashSet
        //     28: dup
        //     29: invokespecial 1352	java/util/HashSet:<init>	()V
        //     32: astore 5
        //     34: aload_3
        //     35: arraylength
        //     36: istore 6
        //     38: iconst_0
        //     39: istore 7
        //     41: iload 7
        //     43: iload 6
        //     45: if_icmpge +19 -> 64
        //     48: aload 5
        //     50: aload_3
        //     51: iload 7
        //     53: aaload
        //     54: invokevirtual 648	java/util/HashSet:add	(Ljava/lang/Object;)Z
        //     57: pop
        //     58: iinc 7 1
        //     61: goto -20 -> 41
        //     64: aload_0
        //     65: getfield 234	com/android/server/net/NetworkPolicyManagerService:mRulesLock	Ljava/lang/Object;
        //     68: astore 8
        //     70: aload 8
        //     72: monitorenter
        //     73: aload 5
        //     75: ldc_w 1354
        //     78: invokevirtual 909	java/util/HashSet:contains	(Ljava/lang/Object;)Z
        //     81: ifeq +81 -> 162
        //     84: aload_0
        //     85: getfield 242	com/android/server/net/NetworkPolicyManagerService:mNetworkPolicy	Ljava/util/HashMap;
        //     88: invokevirtual 762	java/util/HashMap:values	()Ljava/util/Collection;
        //     91: invokeinterface 768 1 0
        //     96: astore 21
        //     98: aload 21
        //     100: invokeinterface 773 1 0
        //     105: ifeq +27 -> 132
        //     108: aload 21
        //     110: invokeinterface 777 1 0
        //     115: checkcast 462	android/net/NetworkPolicy
        //     118: invokevirtual 1357	android/net/NetworkPolicy:clearSnooze	()V
        //     121: goto -23 -> 98
        //     124: astore 9
        //     126: aload 8
        //     128: monitorexit
        //     129: aload 9
        //     131: athrow
        //     132: aload_0
        //     133: invokespecial 448	com/android/server/net/NetworkPolicyManagerService:updateNetworkEnabledLocked	()V
        //     136: aload_0
        //     137: invokespecial 406	com/android/server/net/NetworkPolicyManagerService:updateNetworkRulesLocked	()V
        //     140: aload_0
        //     141: invokespecial 452	com/android/server/net/NetworkPolicyManagerService:updateNotificationsLocked	()V
        //     144: aload_0
        //     145: invokespecial 440	com/android/server/net/NetworkPolicyManagerService:writePolicyLocked	()V
        //     148: aload 4
        //     150: ldc_w 1359
        //     153: invokevirtual 1362	com/android/internal/util/IndentingPrintWriter:println	(Ljava/lang/String;)V
        //     156: aload 8
        //     158: monitorexit
        //     159: goto +384 -> 543
        //     162: aload 4
        //     164: ldc_w 1364
        //     167: invokevirtual 1365	com/android/internal/util/IndentingPrintWriter:print	(Ljava/lang/String;)V
        //     170: aload 4
        //     172: aload_0
        //     173: getfield 978	com/android/server/net/NetworkPolicyManagerService:mRestrictBackground	Z
        //     176: invokevirtual 1368	com/android/internal/util/IndentingPrintWriter:println	(Z)V
        //     179: aload 4
        //     181: ldc_w 1370
        //     184: invokevirtual 1362	com/android/internal/util/IndentingPrintWriter:println	(Ljava/lang/String;)V
        //     187: aload 4
        //     189: invokevirtual 1373	com/android/internal/util/IndentingPrintWriter:increaseIndent	()V
        //     192: aload_0
        //     193: getfield 242	com/android/server/net/NetworkPolicyManagerService:mNetworkPolicy	Ljava/util/HashMap;
        //     196: invokevirtual 762	java/util/HashMap:values	()Ljava/util/Collection;
        //     199: invokeinterface 768 1 0
        //     204: astore 10
        //     206: aload 10
        //     208: invokeinterface 773 1 0
        //     213: ifeq +24 -> 237
        //     216: aload 4
        //     218: aload 10
        //     220: invokeinterface 777 1 0
        //     225: checkcast 462	android/net/NetworkPolicy
        //     228: invokevirtual 1374	android/net/NetworkPolicy:toString	()Ljava/lang/String;
        //     231: invokevirtual 1362	com/android/internal/util/IndentingPrintWriter:println	(Ljava/lang/String;)V
        //     234: goto -28 -> 206
        //     237: aload 4
        //     239: invokevirtual 1377	com/android/internal/util/IndentingPrintWriter:decreaseIndent	()V
        //     242: aload 4
        //     244: ldc_w 1379
        //     247: invokevirtual 1362	com/android/internal/util/IndentingPrintWriter:println	(Ljava/lang/String;)V
        //     250: aload 4
        //     252: invokevirtual 1373	com/android/internal/util/IndentingPrintWriter:increaseIndent	()V
        //     255: aload_0
        //     256: getfield 249	com/android/server/net/NetworkPolicyManagerService:mAppPolicy	Landroid/util/SparseIntArray;
        //     259: invokevirtual 569	android/util/SparseIntArray:size	()I
        //     262: istore 11
        //     264: iconst_0
        //     265: istore 12
        //     267: iload 12
        //     269: iload 11
        //     271: if_icmpge +66 -> 337
        //     274: aload_0
        //     275: getfield 249	com/android/server/net/NetworkPolicyManagerService:mAppPolicy	Landroid/util/SparseIntArray;
        //     278: iload 12
        //     280: invokevirtual 570	android/util/SparseIntArray:keyAt	(I)I
        //     283: istore 19
        //     285: aload_0
        //     286: getfield 249	com/android/server/net/NetworkPolicyManagerService:mAppPolicy	Landroid/util/SparseIntArray;
        //     289: iload 12
        //     291: invokevirtual 1310	android/util/SparseIntArray:valueAt	(I)I
        //     294: istore 20
        //     296: aload 4
        //     298: ldc_w 1381
        //     301: invokevirtual 1365	com/android/internal/util/IndentingPrintWriter:print	(Ljava/lang/String;)V
        //     304: aload 4
        //     306: iload 19
        //     308: invokevirtual 1383	com/android/internal/util/IndentingPrintWriter:print	(I)V
        //     311: aload 4
        //     313: ldc_w 1385
        //     316: invokevirtual 1365	com/android/internal/util/IndentingPrintWriter:print	(Ljava/lang/String;)V
        //     319: aload 4
        //     321: iload 20
        //     323: invokestatic 1389	android/net/NetworkPolicyManager:dumpPolicy	(Ljava/io/PrintWriter;I)V
        //     326: aload 4
        //     328: invokevirtual 1391	com/android/internal/util/IndentingPrintWriter:println	()V
        //     331: iinc 12 1
        //     334: goto -67 -> 267
        //     337: aload 4
        //     339: invokevirtual 1377	com/android/internal/util/IndentingPrintWriter:decreaseIndent	()V
        //     342: new 265	android/util/SparseBooleanArray
        //     345: dup
        //     346: invokespecial 266	android/util/SparseBooleanArray:<init>	()V
        //     349: astore 13
        //     351: aload_0
        //     352: getfield 268	com/android/server/net/NetworkPolicyManagerService:mUidForeground	Landroid/util/SparseBooleanArray;
        //     355: aload 13
        //     357: invokestatic 1393	com/android/server/net/NetworkPolicyManagerService:collectKeys	(Landroid/util/SparseBooleanArray;Landroid/util/SparseBooleanArray;)V
        //     360: aload_0
        //     361: getfield 251	com/android/server/net/NetworkPolicyManagerService:mUidRules	Landroid/util/SparseIntArray;
        //     364: aload 13
        //     366: invokestatic 1395	com/android/server/net/NetworkPolicyManagerService:collectKeys	(Landroid/util/SparseIntArray;Landroid/util/SparseBooleanArray;)V
        //     369: aload 4
        //     371: ldc_w 1397
        //     374: invokevirtual 1362	com/android/internal/util/IndentingPrintWriter:println	(Ljava/lang/String;)V
        //     377: aload 4
        //     379: invokevirtual 1373	com/android/internal/util/IndentingPrintWriter:increaseIndent	()V
        //     382: aload 13
        //     384: invokevirtual 560	android/util/SparseBooleanArray:size	()I
        //     387: istore 14
        //     389: iconst_0
        //     390: istore 15
        //     392: iload 15
        //     394: iload 14
        //     396: if_icmpge +139 -> 535
        //     399: aload 13
        //     401: iload 15
        //     403: invokevirtual 564	android/util/SparseBooleanArray:keyAt	(I)I
        //     406: istore 16
        //     408: aload 4
        //     410: ldc_w 1399
        //     413: invokevirtual 1365	com/android/internal/util/IndentingPrintWriter:print	(Ljava/lang/String;)V
        //     416: aload 4
        //     418: iload 16
        //     420: invokevirtual 1383	com/android/internal/util/IndentingPrintWriter:print	(I)V
        //     423: aload 4
        //     425: ldc_w 1401
        //     428: invokevirtual 1365	com/android/internal/util/IndentingPrintWriter:print	(Ljava/lang/String;)V
        //     431: aload_0
        //     432: getfield 273	com/android/server/net/NetworkPolicyManagerService:mUidPidForeground	Landroid/util/SparseArray;
        //     435: iload 16
        //     437: invokevirtual 1404	android/util/SparseArray:indexOfKey	(I)I
        //     440: istore 17
        //     442: iload 17
        //     444: ifge +54 -> 498
        //     447: aload 4
        //     449: ldc_w 1406
        //     452: invokevirtual 1365	com/android/internal/util/IndentingPrintWriter:print	(Ljava/lang/String;)V
        //     455: aload 4
        //     457: ldc_w 1408
        //     460: invokevirtual 1365	com/android/internal/util/IndentingPrintWriter:print	(Ljava/lang/String;)V
        //     463: aload_0
        //     464: getfield 251	com/android/server/net/NetworkPolicyManagerService:mUidRules	Landroid/util/SparseIntArray;
        //     467: iload 16
        //     469: invokevirtual 1409	android/util/SparseIntArray:indexOfKey	(I)I
        //     472: istore 18
        //     474: iload 18
        //     476: ifge +42 -> 518
        //     479: aload 4
        //     481: ldc_w 1406
        //     484: invokevirtual 1365	com/android/internal/util/IndentingPrintWriter:print	(Ljava/lang/String;)V
        //     487: aload 4
        //     489: invokevirtual 1391	com/android/internal/util/IndentingPrintWriter:println	()V
        //     492: iinc 15 1
        //     495: goto -103 -> 392
        //     498: aload 4
        //     500: aload_0
        //     501: getfield 273	com/android/server/net/NetworkPolicyManagerService:mUidPidForeground	Landroid/util/SparseArray;
        //     504: iload 17
        //     506: invokevirtual 1411	android/util/SparseArray:valueAt	(I)Ljava/lang/Object;
        //     509: checkcast 265	android/util/SparseBooleanArray
        //     512: invokestatic 1413	com/android/server/net/NetworkPolicyManagerService:dumpSparseBooleanArray	(Ljava/io/PrintWriter;Landroid/util/SparseBooleanArray;)V
        //     515: goto -60 -> 455
        //     518: aload 4
        //     520: aload_0
        //     521: getfield 251	com/android/server/net/NetworkPolicyManagerService:mUidRules	Landroid/util/SparseIntArray;
        //     524: iload 18
        //     526: invokevirtual 1310	android/util/SparseIntArray:valueAt	(I)I
        //     529: invokestatic 1416	android/net/NetworkPolicyManager:dumpRules	(Ljava/io/PrintWriter;I)V
        //     532: goto -45 -> 487
        //     535: aload 4
        //     537: invokevirtual 1377	com/android/internal/util/IndentingPrintWriter:decreaseIndent	()V
        //     540: aload 8
        //     542: monitorexit
        //     543: return
        //
        // Exception table:
        //     from	to	target	type
        //     73	129	124	finally
        //     132	543	124	finally
    }

    public int getAppPolicy(int paramInt)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.MANAGE_NETWORK_POLICY", "NetworkPolicy");
        synchronized (this.mRulesLock)
        {
            int i = this.mAppPolicy.get(paramInt, 0);
            return i;
        }
    }

    public int[] getAppsWithPolicy(int paramInt)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.MANAGE_NETWORK_POLICY", "NetworkPolicy");
        int[] arrayOfInt = new int[0];
        Object localObject1 = this.mRulesLock;
        for (int i = 0; ; i++)
            try
            {
                if (i < this.mAppPolicy.size())
                {
                    int j = this.mAppPolicy.keyAt(i);
                    if (this.mAppPolicy.valueAt(i) == paramInt)
                        arrayOfInt = ArrayUtils.appendInt(arrayOfInt, j);
                }
                else
                {
                    return arrayOfInt;
                }
            }
            finally
            {
                localObject2 = finally;
                throw localObject2;
            }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    Context getContext()
    {
        return this.mContext;
    }

    public NetworkPolicy[] getNetworkPolicies()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.MANAGE_NETWORK_POLICY", "NetworkPolicy");
        this.mContext.enforceCallingOrSelfPermission("android.permission.READ_PHONE_STATE", "NetworkPolicy");
        synchronized (this.mRulesLock)
        {
            NetworkPolicy[] arrayOfNetworkPolicy = (NetworkPolicy[])this.mNetworkPolicy.values().toArray(new NetworkPolicy[this.mNetworkPolicy.size()]);
            return arrayOfNetworkPolicy;
        }
    }

    public NetworkQuotaInfo getNetworkQuotaInfo(NetworkState paramNetworkState)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE", "NetworkPolicy");
        long l = Binder.clearCallingIdentity();
        try
        {
            NetworkQuotaInfo localNetworkQuotaInfo = getNetworkQuotaInfoUnchecked(paramNetworkState);
            return localNetworkQuotaInfo;
        }
        finally
        {
            Binder.restoreCallingIdentity(l);
        }
    }

    public boolean getRestrictBackground()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.MANAGE_NETWORK_POLICY", "NetworkPolicy");
        synchronized (this.mRulesLock)
        {
            boolean bool = this.mRestrictBackground;
            return bool;
        }
    }

    public boolean isNetworkMetered(NetworkState paramNetworkState)
    {
        boolean bool = true;
        NetworkIdentity localNetworkIdentity = NetworkIdentity.buildNetworkIdentity(this.mContext, paramNetworkState);
        if (localNetworkIdentity.getRoaming());
        while (true)
        {
            return bool;
            synchronized (this.mRulesLock)
            {
                NetworkPolicy localNetworkPolicy = findPolicyForNetworkLocked(localNetworkIdentity);
                if (localNetworkPolicy != null)
                    bool = localNetworkPolicy.metered;
            }
            int i = paramNetworkState.networkInfo.getType();
            if ((!ConnectivityManager.isNetworkTypeMobile(i)) && (i != 6))
                bool = false;
        }
    }

    public boolean isUidForeground(int paramInt)
    {
        boolean bool = false;
        this.mContext.enforceCallingOrSelfPermission("android.permission.MANAGE_NETWORK_POLICY", "NetworkPolicy");
        synchronized (this.mRulesLock)
        {
            if ((this.mUidForeground.get(paramInt, false)) && (this.mScreenOn))
                bool = true;
            return bool;
        }
    }

    public void registerListener(INetworkPolicyListener paramINetworkPolicyListener)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkPolicy");
        this.mListeners.register(paramINetworkPolicyListener);
    }

    public void setAppPolicy(int paramInt1, int paramInt2)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.MANAGE_NETWORK_POLICY", "NetworkPolicy");
        if (!UserId.isApp(paramInt1))
            throw new IllegalArgumentException("cannot apply policy to appId " + paramInt1);
        setAppPolicyUnchecked(paramInt1, paramInt2, true);
    }

    public void setNetworkPolicies(NetworkPolicy[] paramArrayOfNetworkPolicy)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.MANAGE_NETWORK_POLICY", "NetworkPolicy");
        maybeRefreshTrustedTime();
        synchronized (this.mRulesLock)
        {
            this.mNetworkPolicy.clear();
            int i = paramArrayOfNetworkPolicy.length;
            for (int j = 0; j < i; j++)
            {
                NetworkPolicy localNetworkPolicy = paramArrayOfNetworkPolicy[j];
                this.mNetworkPolicy.put(localNetworkPolicy.template, localNetworkPolicy);
            }
            updateNetworkEnabledLocked();
            updateNetworkRulesLocked();
            updateNotificationsLocked();
            writePolicyLocked();
            return;
        }
    }

    public void setRestrictBackground(boolean paramBoolean)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.MANAGE_NETWORK_POLICY", "NetworkPolicy");
        maybeRefreshTrustedTime();
        while (true)
        {
            synchronized (this.mRulesLock)
            {
                this.mRestrictBackground = paramBoolean;
                updateRulesForRestrictBackgroundLocked();
                updateNotificationsLocked();
                writePolicyLocked();
                Handler localHandler = this.mHandler;
                if (paramBoolean)
                {
                    i = 1;
                    localHandler.obtainMessage(6, i, 0).sendToTarget();
                    return;
                }
            }
            int i = 0;
        }
    }

    public void snoozeLimit(NetworkTemplate paramNetworkTemplate)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.MANAGE_NETWORK_POLICY", "NetworkPolicy");
        long l = Binder.clearCallingIdentity();
        try
        {
            performSnooze(paramNetworkTemplate, 2);
            return;
        }
        finally
        {
            Binder.restoreCallingIdentity(l);
        }
    }

    public void systemReady()
    {
        if (!isBandwidthControlEnabled())
            Slog.w("NetworkPolicy", "bandwidth controls disabled, unable to enforce policy");
        while (true)
        {
            return;
            synchronized (this.mRulesLock)
            {
                readPolicyLocked();
                if (this.mRestrictBackground)
                {
                    updateRulesForRestrictBackgroundLocked();
                    updateNotificationsLocked();
                }
                updateScreenOn();
            }
            try
            {
                this.mActivityManager.registerProcessObserver(this.mProcessObserver);
                this.mNetworkManager.registerObserver(this.mAlertObserver);
                label75: IntentFilter localIntentFilter1 = new IntentFilter();
                localIntentFilter1.addAction("android.intent.action.SCREEN_ON");
                localIntentFilter1.addAction("android.intent.action.SCREEN_OFF");
                this.mContext.registerReceiver(this.mScreenReceiver, localIntentFilter1);
                IntentFilter localIntentFilter2 = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE");
                this.mContext.registerReceiver(this.mConnReceiver, localIntentFilter2, "android.permission.CONNECTIVITY_INTERNAL", this.mHandler);
                IntentFilter localIntentFilter3 = new IntentFilter();
                localIntentFilter3.addAction("android.intent.action.PACKAGE_ADDED");
                localIntentFilter3.addAction("android.intent.action.UID_REMOVED");
                this.mContext.registerReceiver(this.mPackageReceiver, localIntentFilter3, null, this.mHandler);
                IntentFilter localIntentFilter4 = new IntentFilter("com.android.server.action.NETWORK_STATS_UPDATED");
                this.mContext.registerReceiver(this.mStatsReceiver, localIntentFilter4, "android.permission.READ_NETWORK_USAGE_HISTORY", this.mHandler);
                IntentFilter localIntentFilter5 = new IntentFilter("com.android.server.net.action.ALLOW_BACKGROUND");
                this.mContext.registerReceiver(this.mAllowReceiver, localIntentFilter5, "android.permission.MANAGE_NETWORK_POLICY", this.mHandler);
                IntentFilter localIntentFilter6 = new IntentFilter("com.android.server.net.action.SNOOZE_WARNING");
                this.mContext.registerReceiver(this.mSnoozeWarningReceiver, localIntentFilter6, "android.permission.MANAGE_NETWORK_POLICY", this.mHandler);
                IntentFilter localIntentFilter7 = new IntentFilter("android.net.wifi.CONFIGURED_NETWORKS_CHANGE");
                this.mContext.registerReceiver(this.mWifiConfigReceiver, localIntentFilter7, "android.permission.CONNECTIVITY_INTERNAL", this.mHandler);
                IntentFilter localIntentFilter8 = new IntentFilter("android.net.wifi.STATE_CHANGE");
                this.mContext.registerReceiver(this.mWifiStateReceiver, localIntentFilter8, "android.permission.CONNECTIVITY_INTERNAL", this.mHandler);
                continue;
                localObject2 = finally;
                throw localObject2;
            }
            catch (RemoteException localRemoteException)
            {
                break label75;
            }
        }
    }

    public void unregisterListener(INetworkPolicyListener paramINetworkPolicyListener)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONNECTIVITY_INTERNAL", "NetworkPolicy");
        this.mListeners.unregister(paramINetworkPolicyListener);
    }

    public static class XmlUtils
    {
        public static boolean readBooleanAttribute(XmlPullParser paramXmlPullParser, String paramString)
        {
            return Boolean.parseBoolean(paramXmlPullParser.getAttributeValue(null, paramString));
        }

        public static int readIntAttribute(XmlPullParser paramXmlPullParser, String paramString)
            throws IOException
        {
            String str = paramXmlPullParser.getAttributeValue(null, paramString);
            try
            {
                int i = Integer.parseInt(str);
                return i;
            }
            catch (NumberFormatException localNumberFormatException)
            {
            }
            throw new ProtocolException("problem parsing " + paramString + "=" + str + " as int");
        }

        public static long readLongAttribute(XmlPullParser paramXmlPullParser, String paramString)
            throws IOException
        {
            String str = paramXmlPullParser.getAttributeValue(null, paramString);
            try
            {
                long l = Long.parseLong(str);
                return l;
            }
            catch (NumberFormatException localNumberFormatException)
            {
            }
            throw new ProtocolException("problem parsing " + paramString + "=" + str + " as long");
        }

        public static void writeBooleanAttribute(XmlSerializer paramXmlSerializer, String paramString, boolean paramBoolean)
            throws IOException
        {
            paramXmlSerializer.attribute(null, paramString, Boolean.toString(paramBoolean));
        }

        public static void writeIntAttribute(XmlSerializer paramXmlSerializer, String paramString, int paramInt)
            throws IOException
        {
            paramXmlSerializer.attribute(null, paramString, Integer.toString(paramInt));
        }

        public static void writeLongAttribute(XmlSerializer paramXmlSerializer, String paramString, long paramLong)
            throws IOException
        {
            paramXmlSerializer.attribute(null, paramString, Long.toString(paramLong));
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        private static long[] sLastNotificationTimeArr = arrayOfLong;

        static
        {
            long[] arrayOfLong = new long[4];
            arrayOfLong[0] = 0L;
            arrayOfLong[1] = 0L;
            arrayOfLong[2] = 0L;
            arrayOfLong[3] = 0L;
        }

        static long adjustMobileDataUsage(NetworkPolicyManagerService paramNetworkPolicyManagerService, NetworkTemplate paramNetworkTemplate, long paramLong1, long paramLong2)
        {
            long l1 = 0L;
            Context localContext = paramNetworkPolicyManagerService.getContext();
            if (paramNetworkTemplate.getMatchRule() == 1)
            {
                long l2 = Settings.Secure.getLong(localContext.getContentResolver(), "data_usage_adjusting_time", l1);
                if ((l2 >= paramLong1) && (l2 <= paramLong2))
                    l1 = Math.max(l1, Settings.Secure.getLong(localContext.getContentResolver(), "data_usage_adjustment", l1));
            }
            return l1;
        }

        static boolean isIntervalValid(int paramInt)
        {
            if (System.currentTimeMillis() - sLastNotificationTimeArr[paramInt] > 86400000L);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        static void setInterval(int paramInt)
        {
            sLastNotificationTimeArr[paramInt] = System.currentTimeMillis();
        }

        static void setNetworkTemplateEnabled(NetworkPolicyManagerService paramNetworkPolicyManagerService, NetworkTemplate paramNetworkTemplate, boolean paramBoolean)
        {
            TelephonyManager localTelephonyManager = (TelephonyManager)paramNetworkPolicyManagerService.getContext().getSystemService("phone");
            switch (paramNetworkTemplate.getMatchRule())
            {
            default:
                return;
            case 1:
            case 2:
            case 3:
            }
            ContentResolver localContentResolver;
            if (((paramBoolean) || (isIntervalValid(2))) && (Objects.equal(localTelephonyManager.getSubscriberId(), paramNetworkTemplate.getSubscriberId())))
            {
                localContentResolver = paramNetworkPolicyManagerService.getContext().getContentResolver();
                if (!paramBoolean)
                    break label99;
            }
            label99: for (int i = 1; ; i = 0)
            {
                Settings.Secure.putInt(localContentResolver, "mobile_policy", i);
                break;
                break;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.net.NetworkPolicyManagerService
 * JD-Core Version:        0.6.2
 */