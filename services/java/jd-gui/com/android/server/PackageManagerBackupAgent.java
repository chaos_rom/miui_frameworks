package com.android.server;

import android.app.backup.BackupAgent;
import android.app.backup.BackupDataInput;
import android.app.backup.BackupDataOutput;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build.VERSION;
import android.os.ParcelFileDescriptor;
import android.util.Slog;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class PackageManagerBackupAgent extends BackupAgent
{
    private static final boolean DEBUG = false;
    private static final String GLOBAL_METADATA_KEY = "@meta@";
    private static final String TAG = "PMBA";
    private List<PackageInfo> mAllPackages;
    private final HashSet<String> mExisting = new HashSet();
    private boolean mHasMetadata;
    private PackageManager mPackageManager;
    private HashMap<String, Metadata> mRestoredSignatures;
    private HashMap<String, Metadata> mStateVersions = new HashMap();
    private String mStoredIncrementalVersion;
    private int mStoredSdkVersion;

    PackageManagerBackupAgent(PackageManager paramPackageManager, List<PackageInfo> paramList)
    {
        this.mPackageManager = paramPackageManager;
        this.mAllPackages = paramList;
        this.mRestoredSignatures = null;
        this.mHasMetadata = false;
    }

    private void parseStateFile(ParcelFileDescriptor paramParcelFileDescriptor)
    {
        this.mExisting.clear();
        this.mStateVersions.clear();
        this.mStoredSdkVersion = 0;
        this.mStoredIncrementalVersion = null;
        DataInputStream localDataInputStream = new DataInputStream(new FileInputStream(paramParcelFileDescriptor.getFileDescriptor()));
        new byte[256];
        try
        {
            if (localDataInputStream.readUTF().equals("@meta@"))
            {
                this.mStoredSdkVersion = localDataInputStream.readInt();
                this.mStoredIncrementalVersion = localDataInputStream.readUTF();
                this.mExisting.add("@meta@");
                while (true)
                {
                    String str = localDataInputStream.readUTF();
                    int i = localDataInputStream.readInt();
                    this.mExisting.add(str);
                    this.mStateVersions.put(str, new Metadata(i, null));
                }
            }
            Slog.e("PMBA", "No global metadata in state file!");
        }
        catch (IOException localIOException)
        {
            Slog.e("PMBA", "Unable to read Package Manager state file: " + localIOException);
        }
        catch (EOFException localEOFException)
        {
        }
    }

    private static Signature[] readSignatureArray(DataInputStream paramDataInputStream)
    {
        int i;
        Signature[] arrayOfSignature;
        int j;
        try
        {
            i = paramDataInputStream.readInt();
            if (i > 20)
            {
                Slog.e("PMBA", "Suspiciously large sig count in restore data; aborting");
                throw new IllegalStateException("Bad restore state");
            }
        }
        catch (IOException localIOException)
        {
            Slog.e("PMBA", "Unable to read signatures");
            arrayOfSignature = null;
            return arrayOfSignature;
        }
        catch (EOFException localEOFException)
        {
            while (true)
            {
                Slog.w("PMBA", "Read empty signature block");
                arrayOfSignature = null;
            }
            arrayOfSignature = new Signature[i];
            j = 0;
        }
        while (j < i)
        {
            byte[] arrayOfByte = new byte[paramDataInputStream.readInt()];
            paramDataInputStream.read(arrayOfByte);
            arrayOfSignature[j] = new Signature(arrayOfByte);
            j++;
        }
    }

    private static void writeEntity(BackupDataOutput paramBackupDataOutput, String paramString, byte[] paramArrayOfByte)
        throws IOException
    {
        paramBackupDataOutput.writeEntityHeader(paramString, paramArrayOfByte.length);
        paramBackupDataOutput.writeEntityData(paramArrayOfByte, paramArrayOfByte.length);
    }

    private static void writeSignatureArray(DataOutputStream paramDataOutputStream, Signature[] paramArrayOfSignature)
        throws IOException
    {
        paramDataOutputStream.writeInt(paramArrayOfSignature.length);
        int i = paramArrayOfSignature.length;
        for (int j = 0; j < i; j++)
        {
            byte[] arrayOfByte = paramArrayOfSignature[j].toByteArray();
            paramDataOutputStream.writeInt(arrayOfByte.length);
            paramDataOutputStream.write(arrayOfByte);
        }
    }

    private void writeStateFile(List<PackageInfo> paramList, ParcelFileDescriptor paramParcelFileDescriptor)
    {
        DataOutputStream localDataOutputStream = new DataOutputStream(new FileOutputStream(paramParcelFileDescriptor.getFileDescriptor()));
        try
        {
            localDataOutputStream.writeUTF("@meta@");
            localDataOutputStream.writeInt(Build.VERSION.SDK_INT);
            localDataOutputStream.writeUTF(Build.VERSION.INCREMENTAL);
            Iterator localIterator = paramList.iterator();
            while (localIterator.hasNext())
            {
                PackageInfo localPackageInfo = (PackageInfo)localIterator.next();
                localDataOutputStream.writeUTF(localPackageInfo.packageName);
                localDataOutputStream.writeInt(localPackageInfo.versionCode);
            }
        }
        catch (IOException localIOException)
        {
            Slog.e("PMBA", "Unable to write package manager state file!");
        }
    }

    public Metadata getRestoredMetadata(String paramString)
    {
        if (this.mRestoredSignatures == null)
            Slog.w("PMBA", "getRestoredMetadata() before metadata read!");
        for (Metadata localMetadata = null; ; localMetadata = (Metadata)this.mRestoredSignatures.get(paramString))
            return localMetadata;
    }

    public Set<String> getRestoredPackages()
    {
        if (this.mRestoredSignatures == null)
            Slog.w("PMBA", "getRestoredPackages() before metadata read!");
        for (Object localObject = null; ; localObject = this.mRestoredSignatures.keySet())
            return localObject;
    }

    public boolean hasMetadata()
    {
        return this.mHasMetadata;
    }

    // ERROR //
    public void onBackup(ParcelFileDescriptor paramParcelFileDescriptor1, BackupDataOutput paramBackupDataOutput, ParcelFileDescriptor paramParcelFileDescriptor2)
    {
        // Byte code:
        //     0: new 254	java/io/ByteArrayOutputStream
        //     3: dup
        //     4: invokespecial 255	java/io/ByteArrayOutputStream:<init>	()V
        //     7: astore 4
        //     9: new 176	java/io/DataOutputStream
        //     12: dup
        //     13: aload 4
        //     15: invokespecial 195	java/io/DataOutputStream:<init>	(Ljava/io/OutputStream;)V
        //     18: astore 5
        //     20: aload_0
        //     21: aload_1
        //     22: invokespecial 257	com/android/server/PackageManagerBackupAgent:parseStateFile	(Landroid/os/ParcelFileDescriptor;)V
        //     25: aload_0
        //     26: getfield 70	com/android/server/PackageManagerBackupAgent:mStoredIncrementalVersion	Ljava/lang/String;
        //     29: ifnull +16 -> 45
        //     32: aload_0
        //     33: getfield 70	com/android/server/PackageManagerBackupAgent:mStoredIncrementalVersion	Ljava/lang/String;
        //     36: getstatic 206	android/os/Build$VERSION:INCREMENTAL	Ljava/lang/String;
        //     39: invokevirtual 96	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     42: ifne +57 -> 99
        //     45: ldc 17
        //     47: new 120	java/lang/StringBuilder
        //     50: dup
        //     51: invokespecial 121	java/lang/StringBuilder:<init>	()V
        //     54: ldc_w 259
        //     57: invokevirtual 127	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     60: aload_0
        //     61: getfield 70	com/android/server/PackageManagerBackupAgent:mStoredIncrementalVersion	Ljava/lang/String;
        //     64: invokevirtual 127	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     67: ldc_w 261
        //     70: invokevirtual 127	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     73: getstatic 206	android/os/Build$VERSION:INCREMENTAL	Ljava/lang/String;
        //     76: invokevirtual 127	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     79: ldc_w 263
        //     82: invokevirtual 127	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     85: invokevirtual 133	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     88: invokestatic 266	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     91: pop
        //     92: aload_0
        //     93: getfield 48	com/android/server/PackageManagerBackupAgent:mExisting	Ljava/util/HashSet;
        //     96: invokevirtual 65	java/util/HashSet:clear	()V
        //     99: aload_0
        //     100: getfield 48	com/android/server/PackageManagerBackupAgent:mExisting	Ljava/util/HashSet;
        //     103: ldc 14
        //     105: invokevirtual 269	java/util/HashSet:contains	(Ljava/lang/Object;)Z
        //     108: ifne +203 -> 311
        //     111: aload 5
        //     113: getstatic 203	android/os/Build$VERSION:SDK_INT	I
        //     116: invokevirtual 180	java/io/DataOutputStream:writeInt	(I)V
        //     119: aload 5
        //     121: getstatic 206	android/os/Build$VERSION:INCREMENTAL	Ljava/lang/String;
        //     124: invokevirtual 198	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
        //     127: aload_2
        //     128: ldc 14
        //     130: aload 4
        //     132: invokevirtual 270	java/io/ByteArrayOutputStream:toByteArray	()[B
        //     135: invokestatic 272	com/android/server/PackageManagerBackupAgent:writeEntity	(Landroid/app/backup/BackupDataOutput;Ljava/lang/String;[B)V
        //     138: aload_0
        //     139: getfield 52	com/android/server/PackageManagerBackupAgent:mAllPackages	Ljava/util/List;
        //     142: invokeinterface 212 1 0
        //     147: astore 10
        //     149: aload 10
        //     151: invokeinterface 218 1 0
        //     156: ifeq +222 -> 378
        //     159: aload 10
        //     161: invokeinterface 222 1 0
        //     166: checkcast 224	android/content/pm/PackageInfo
        //     169: getfield 227	android/content/pm/PackageInfo:packageName	Ljava/lang/String;
        //     172: astore 16
        //     174: aload 16
        //     176: ldc 14
        //     178: invokevirtual 96	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     181: istore 17
        //     183: iload 17
        //     185: ifne -36 -> 149
        //     188: aload_0
        //     189: getfield 50	com/android/server/PackageManagerBackupAgent:mPackageManager	Landroid/content/pm/PackageManager;
        //     192: aload 16
        //     194: bipush 64
        //     196: invokevirtual 278	android/content/pm/PackageManager:getPackageInfo	(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
        //     199: astore 20
        //     201: aload_0
        //     202: getfield 48	com/android/server/PackageManagerBackupAgent:mExisting	Ljava/util/HashSet;
        //     205: aload 16
        //     207: invokevirtual 269	java/util/HashSet:contains	(Ljava/lang/Object;)Z
        //     210: ifeq +36 -> 246
        //     213: aload_0
        //     214: getfield 48	com/android/server/PackageManagerBackupAgent:mExisting	Ljava/util/HashSet;
        //     217: aload 16
        //     219: invokevirtual 281	java/util/HashSet:remove	(Ljava/lang/Object;)Z
        //     222: pop
        //     223: aload 20
        //     225: getfield 230	android/content/pm/PackageInfo:versionCode	I
        //     228: aload_0
        //     229: getfield 43	com/android/server/PackageManagerBackupAgent:mStateVersions	Ljava/util/HashMap;
        //     232: aload 16
        //     234: invokevirtual 240	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     237: checkcast 6	com/android/server/PackageManagerBackupAgent$Metadata
        //     240: getfield 282	com/android/server/PackageManagerBackupAgent$Metadata:versionCode	I
        //     243: if_icmpeq -94 -> 149
        //     246: aload 20
        //     248: getfield 286	android/content/pm/PackageInfo:signatures	[Landroid/content/pm/Signature;
        //     251: ifnull +12 -> 263
        //     254: aload 20
        //     256: getfield 286	android/content/pm/PackageInfo:signatures	[Landroid/content/pm/Signature;
        //     259: arraylength
        //     260: ifne +79 -> 339
        //     263: ldc 17
        //     265: new 120	java/lang/StringBuilder
        //     268: dup
        //     269: invokespecial 121	java/lang/StringBuilder:<init>	()V
        //     272: ldc_w 288
        //     275: invokevirtual 127	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     278: aload 16
        //     280: invokevirtual 127	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     283: ldc_w 290
        //     286: invokevirtual 127	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     289: invokevirtual 133	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     292: invokestatic 151	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     295: pop
        //     296: goto -147 -> 149
        //     299: astore 7
        //     301: ldc 17
        //     303: ldc_w 292
        //     306: invokestatic 118	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     309: pop
        //     310: return
        //     311: aload_0
        //     312: getfield 48	com/android/server/PackageManagerBackupAgent:mExisting	Ljava/util/HashSet;
        //     315: ldc 14
        //     317: invokevirtual 281	java/util/HashSet:remove	(Ljava/lang/Object;)Z
        //     320: pop
        //     321: goto -183 -> 138
        //     324: astore 18
        //     326: aload_0
        //     327: getfield 48	com/android/server/PackageManagerBackupAgent:mExisting	Ljava/util/HashSet;
        //     330: aload 16
        //     332: invokevirtual 103	java/util/HashSet:add	(Ljava/lang/Object;)Z
        //     335: pop
        //     336: goto -187 -> 149
        //     339: aload 4
        //     341: invokevirtual 295	java/io/ByteArrayOutputStream:reset	()V
        //     344: aload 5
        //     346: aload 20
        //     348: getfield 230	android/content/pm/PackageInfo:versionCode	I
        //     351: invokevirtual 180	java/io/DataOutputStream:writeInt	(I)V
        //     354: aload 5
        //     356: aload 20
        //     358: getfield 286	android/content/pm/PackageInfo:signatures	[Landroid/content/pm/Signature;
        //     361: invokestatic 297	com/android/server/PackageManagerBackupAgent:writeSignatureArray	(Ljava/io/DataOutputStream;[Landroid/content/pm/Signature;)V
        //     364: aload_2
        //     365: aload 16
        //     367: aload 4
        //     369: invokevirtual 270	java/io/ByteArrayOutputStream:toByteArray	()[B
        //     372: invokestatic 272	com/android/server/PackageManagerBackupAgent:writeEntity	(Landroid/app/backup/BackupDataOutput;Ljava/lang/String;[B)V
        //     375: goto -226 -> 149
        //     378: aload_0
        //     379: getfield 48	com/android/server/PackageManagerBackupAgent:mExisting	Ljava/util/HashSet;
        //     382: invokevirtual 298	java/util/HashSet:iterator	()Ljava/util/Iterator;
        //     385: astore 11
        //     387: aload 11
        //     389: invokeinterface 218 1 0
        //     394: ifeq +41 -> 435
        //     397: aload 11
        //     399: invokeinterface 222 1 0
        //     404: checkcast 92	java/lang/String
        //     407: astore 12
        //     409: aload_2
        //     410: aload 12
        //     412: bipush 255
        //     414: invokevirtual 168	android/app/backup/BackupDataOutput:writeEntityHeader	(Ljava/lang/String;I)I
        //     417: pop
        //     418: goto -31 -> 387
        //     421: astore 13
        //     423: ldc 17
        //     425: ldc_w 300
        //     428: invokestatic 118	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     431: pop
        //     432: goto -122 -> 310
        //     435: aload_0
        //     436: aload_0
        //     437: getfield 52	com/android/server/PackageManagerBackupAgent:mAllPackages	Ljava/util/List;
        //     440: aload_3
        //     441: invokespecial 302	com/android/server/PackageManagerBackupAgent:writeStateFile	(Ljava/util/List;Landroid/os/ParcelFileDescriptor;)V
        //     444: goto -134 -> 310
        //
        // Exception table:
        //     from	to	target	type
        //     99	183	299	java/io/IOException
        //     188	201	299	java/io/IOException
        //     201	296	299	java/io/IOException
        //     311	409	299	java/io/IOException
        //     423	432	299	java/io/IOException
        //     188	201	324	android/content/pm/PackageManager$NameNotFoundException
        //     409	418	421	java/io/IOException
    }

    public void onRestore(BackupDataInput paramBackupDataInput, int paramInt, ParcelFileDescriptor paramParcelFileDescriptor)
        throws IOException
    {
        ArrayList localArrayList = new ArrayList();
        HashMap localHashMap = new HashMap();
        String str;
        DataInputStream localDataInputStream;
        int k;
        if (paramBackupDataInput.readNextHeader())
        {
            str = paramBackupDataInput.getKey();
            int i = paramBackupDataInput.getDataSize();
            byte[] arrayOfByte = new byte[i];
            paramBackupDataInput.readEntityData(arrayOfByte, 0, i);
            localDataInputStream = new DataInputStream(new ByteArrayInputStream(arrayOfByte));
            if (str.equals("@meta@"))
            {
                k = localDataInputStream.readInt();
                if (-1 > Build.VERSION.SDK_INT)
                    Slog.w("PMBA", "Restore set was from a later version of Android; not restoring");
            }
        }
        while (true)
        {
            return;
            this.mStoredSdkVersion = k;
            this.mStoredIncrementalVersion = localDataInputStream.readUTF();
            this.mHasMetadata = true;
            break;
            int j = localDataInputStream.readInt();
            Signature[] arrayOfSignature = readSignatureArray(localDataInputStream);
            if ((arrayOfSignature == null) || (arrayOfSignature.length == 0))
            {
                Slog.w("PMBA", "Not restoring package " + str + " since it appears to have no signatures.");
                break;
            }
            ApplicationInfo localApplicationInfo = new ApplicationInfo();
            localApplicationInfo.packageName = str;
            localArrayList.add(localApplicationInfo);
            localHashMap.put(str, new Metadata(j, arrayOfSignature));
            break;
            this.mRestoredSignatures = localHashMap;
        }
    }

    public class Metadata
    {
        public Signature[] signatures;
        public int versionCode;

        Metadata(int paramArrayOfSignature, Signature[] arg3)
        {
            this.versionCode = paramArrayOfSignature;
            Object localObject;
            this.signatures = localObject;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.PackageManagerBackupAgent
 * JD-Core Version:        0.6.2
 */