package com.android.server.wm;

import android.graphics.Rect;
import android.util.Slog;
import android.view.InputChannel;
import android.view.KeyEvent;
import android.view.WindowManager.LayoutParams;
import android.view.WindowManagerPolicy;
import com.android.server.input.InputApplicationHandle;
import com.android.server.input.InputManagerService;
import com.android.server.input.InputManagerService.Callbacks;
import com.android.server.input.InputWindowHandle;
import java.util.ArrayList;
import java.util.Arrays;

final class InputMonitor
    implements InputManagerService.Callbacks
{
    private boolean mInputDevicesReady;
    private final Object mInputDevicesReadyMonitor = new Object();
    private boolean mInputDispatchEnabled;
    private boolean mInputDispatchFrozen;
    private WindowState mInputFocus;
    private int mInputWindowHandleCount;
    private InputWindowHandle[] mInputWindowHandles;
    private final WindowManagerService mService;
    private boolean mUpdateInputWindowsNeeded = true;

    public InputMonitor(WindowManagerService paramWindowManagerService)
    {
        this.mService = paramWindowManagerService;
    }

    private void addInputWindowHandleLw(InputWindowHandle paramInputWindowHandle)
    {
        if (this.mInputWindowHandles == null)
            this.mInputWindowHandles = new InputWindowHandle[16];
        if (this.mInputWindowHandleCount >= this.mInputWindowHandles.length)
            this.mInputWindowHandles = ((InputWindowHandle[])Arrays.copyOf(this.mInputWindowHandles, 2 * this.mInputWindowHandleCount));
        InputWindowHandle[] arrayOfInputWindowHandle = this.mInputWindowHandles;
        int i = this.mInputWindowHandleCount;
        this.mInputWindowHandleCount = (i + 1);
        arrayOfInputWindowHandle[i] = paramInputWindowHandle;
    }

    private void clearInputWindowHandlesLw()
    {
        while (this.mInputWindowHandleCount != 0)
        {
            InputWindowHandle[] arrayOfInputWindowHandle = this.mInputWindowHandles;
            int i = -1 + this.mInputWindowHandleCount;
            this.mInputWindowHandleCount = i;
            arrayOfInputWindowHandle[i] = null;
        }
    }

    private void updateInputDispatchModeLw()
    {
        this.mService.mInputManager.setInputDispatchMode(this.mInputDispatchEnabled, this.mInputDispatchFrozen);
    }

    public KeyEvent dispatchUnhandledKey(InputWindowHandle paramInputWindowHandle, KeyEvent paramKeyEvent, int paramInt)
    {
        if (paramInputWindowHandle != null);
        for (WindowState localWindowState = (WindowState)paramInputWindowHandle.windowState; ; localWindowState = null)
            return this.mService.mPolicy.dispatchUnhandledKey(localWindowState, paramKeyEvent, paramInt);
    }

    public void freezeInputDispatchingLw()
    {
        if (!this.mInputDispatchFrozen)
        {
            this.mInputDispatchFrozen = true;
            updateInputDispatchModeLw();
        }
    }

    public int getPointerLayer()
    {
        return 1000 + 10000 * this.mService.mPolicy.windowTypeToLayerLw(2018);
    }

    public long interceptKeyBeforeDispatching(InputWindowHandle paramInputWindowHandle, KeyEvent paramKeyEvent, int paramInt)
    {
        if (paramInputWindowHandle != null);
        for (WindowState localWindowState = (WindowState)paramInputWindowHandle.windowState; ; localWindowState = null)
            return this.mService.mPolicy.interceptKeyBeforeDispatching(localWindowState, paramKeyEvent, paramInt);
    }

    public int interceptKeyBeforeQueueing(KeyEvent paramKeyEvent, int paramInt, boolean paramBoolean)
    {
        return this.mService.mPolicy.interceptKeyBeforeQueueing(paramKeyEvent, paramInt, paramBoolean);
    }

    public int interceptMotionBeforeQueueingWhenScreenOff(int paramInt)
    {
        return this.mService.mPolicy.interceptMotionBeforeQueueingWhenScreenOff(paramInt);
    }

    // ERROR //
    public long notifyANR(InputApplicationHandle paramInputApplicationHandle, InputWindowHandle paramInputWindowHandle)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_3
        //     2: aload_0
        //     3: getfield 32	com/android/server/wm/InputMonitor:mService	Lcom/android/server/wm/WindowManagerService;
        //     6: getfield 110	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
        //     9: astore 4
        //     11: aload 4
        //     13: monitorenter
        //     14: aconst_null
        //     15: astore 5
        //     17: aload_2
        //     18: ifnull +23 -> 41
        //     21: aload_2
        //     22: getfield 70	com/android/server/input/InputWindowHandle:windowState	Ljava/lang/Object;
        //     25: checkcast 72	com/android/server/wm/WindowState
        //     28: astore 5
        //     30: aload 5
        //     32: ifnull +9 -> 41
        //     35: aload 5
        //     37: getfield 114	com/android/server/wm/WindowState:mAppToken	Lcom/android/server/wm/AppWindowToken;
        //     40: astore_3
        //     41: aload_3
        //     42: ifnonnull +15 -> 57
        //     45: aload_1
        //     46: ifnull +11 -> 57
        //     49: aload_1
        //     50: getfield 119	com/android/server/input/InputApplicationHandle:appWindowToken	Ljava/lang/Object;
        //     53: checkcast 121	com/android/server/wm/AppWindowToken
        //     56: astore_3
        //     57: aload 5
        //     59: ifnull +80 -> 139
        //     62: ldc 123
        //     64: new 125	java/lang/StringBuilder
        //     67: dup
        //     68: invokespecial 126	java/lang/StringBuilder:<init>	()V
        //     71: ldc 128
        //     73: invokevirtual 132	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     76: aload 5
        //     78: getfield 136	com/android/server/wm/WindowState:mAttrs	Landroid/view/WindowManager$LayoutParams;
        //     81: invokevirtual 142	android/view/WindowManager$LayoutParams:getTitle	()Ljava/lang/CharSequence;
        //     84: invokevirtual 145	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     87: invokevirtual 149	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     90: invokestatic 155	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     93: pop
        //     94: aload_0
        //     95: getfield 32	com/android/server/wm/InputMonitor:mService	Lcom/android/server/wm/WindowManagerService;
        //     98: aload_3
        //     99: aload 5
        //     101: invokevirtual 159	com/android/server/wm/WindowManagerService:saveANRStateLocked	(Lcom/android/server/wm/AppWindowToken;Lcom/android/server/wm/WindowState;)V
        //     104: aload 4
        //     106: monitorexit
        //     107: aload_3
        //     108: ifnull +87 -> 195
        //     111: aload_3
        //     112: getfield 163	com/android/server/wm/AppWindowToken:appToken	Landroid/view/IApplicationToken;
        //     115: ifnull +80 -> 195
        //     118: aload_3
        //     119: getfield 163	com/android/server/wm/AppWindowToken:appToken	Landroid/view/IApplicationToken;
        //     122: invokeinterface 169 1 0
        //     127: ifne +68 -> 195
        //     130: aload_3
        //     131: getfield 173	com/android/server/wm/AppWindowToken:inputDispatchingTimeoutNanos	J
        //     134: lstore 8
        //     136: lload 8
        //     138: lreturn
        //     139: aload_3
        //     140: ifnull +42 -> 182
        //     143: ldc 123
        //     145: new 125	java/lang/StringBuilder
        //     148: dup
        //     149: invokespecial 126	java/lang/StringBuilder:<init>	()V
        //     152: ldc 175
        //     154: invokevirtual 132	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     157: aload_3
        //     158: getfield 181	com/android/server/wm/WindowToken:stringName	Ljava/lang/String;
        //     161: invokevirtual 132	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     164: invokevirtual 149	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     167: invokestatic 155	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     170: pop
        //     171: goto -77 -> 94
        //     174: astore 6
        //     176: aload 4
        //     178: monitorexit
        //     179: aload 6
        //     181: athrow
        //     182: ldc 123
        //     184: ldc 183
        //     186: invokestatic 155	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     189: pop
        //     190: goto -96 -> 94
        //     193: astore 10
        //     195: lconst_0
        //     196: lstore 8
        //     198: goto -62 -> 136
        //
        // Exception table:
        //     from	to	target	type
        //     21	107	174	finally
        //     143	179	174	finally
        //     182	190	174	finally
        //     118	136	193	android/os/RemoteException
    }

    public void notifyConfigurationChanged()
    {
        this.mService.sendNewConfiguration();
        synchronized (this.mInputDevicesReadyMonitor)
        {
            if (!this.mInputDevicesReady)
            {
                this.mInputDevicesReady = true;
                this.mInputDevicesReadyMonitor.notifyAll();
            }
            return;
        }
    }

    public void notifyInputChannelBroken(InputWindowHandle paramInputWindowHandle)
    {
        if (paramInputWindowHandle == null);
        while (true)
        {
            return;
            synchronized (this.mService.mWindowMap)
            {
                WindowState localWindowState = (WindowState)paramInputWindowHandle.windowState;
                if (localWindowState != null)
                {
                    Slog.i("WindowManager", "WINDOW DIED " + localWindowState);
                    this.mService.removeWindowLocked(localWindowState.mSession, localWindowState);
                }
            }
        }
    }

    public void notifyLidSwitchChanged(long paramLong, boolean paramBoolean)
    {
        this.mService.mPolicy.notifyLidSwitchChanged(paramLong, paramBoolean);
    }

    public void pauseDispatchingLw(WindowToken paramWindowToken)
    {
        if (!paramWindowToken.paused)
        {
            paramWindowToken.paused = true;
            updateInputWindowsLw(true);
        }
    }

    public void resumeDispatchingLw(WindowToken paramWindowToken)
    {
        if (paramWindowToken.paused)
        {
            paramWindowToken.paused = false;
            updateInputWindowsLw(true);
        }
    }

    public void setEventDispatchingLw(boolean paramBoolean)
    {
        if (this.mInputDispatchEnabled != paramBoolean)
        {
            this.mInputDispatchEnabled = paramBoolean;
            updateInputDispatchModeLw();
        }
    }

    public void setFocusedAppLw(AppWindowToken paramAppWindowToken)
    {
        if (paramAppWindowToken == null)
            this.mService.mInputManager.setFocusedApplication(null);
        while (true)
        {
            return;
            InputApplicationHandle localInputApplicationHandle = paramAppWindowToken.mInputApplicationHandle;
            localInputApplicationHandle.name = paramAppWindowToken.toString();
            localInputApplicationHandle.dispatchingTimeoutNanos = paramAppWindowToken.inputDispatchingTimeoutNanos;
            this.mService.mInputManager.setFocusedApplication(localInputApplicationHandle);
        }
    }

    public void setInputFocusLw(WindowState paramWindowState, boolean paramBoolean)
    {
        if (paramWindowState != this.mInputFocus)
        {
            if ((paramWindowState != null) && (paramWindowState.canReceiveKeys()))
                paramWindowState.mToken.paused = false;
            this.mInputFocus = paramWindowState;
            setUpdateInputWindowsNeededLw();
            if (paramBoolean)
                updateInputWindowsLw(false);
        }
    }

    public void setUpdateInputWindowsNeededLw()
    {
        this.mUpdateInputWindowsNeeded = true;
    }

    public void thawInputDispatchingLw()
    {
        if (this.mInputDispatchFrozen)
        {
            this.mInputDispatchFrozen = false;
            updateInputDispatchModeLw();
        }
    }

    public void updateInputWindowsLw(boolean paramBoolean)
    {
        if ((!paramBoolean) && (!this.mUpdateInputWindowsNeeded));
        while (true)
        {
            return;
            this.mUpdateInputWindowsNeeded = false;
            ArrayList localArrayList = this.mService.mWindows;
            int i;
            if (this.mService.mDragState != null)
            {
                i = 1;
                if (i != 0)
                {
                    InputWindowHandle localInputWindowHandle2 = this.mService.mDragState.mDragWindowHandle;
                    if (localInputWindowHandle2 == null)
                        break label119;
                    addInputWindowHandleLw(localInputWindowHandle2);
                }
            }
            while (true)
            {
                int j = this.mService.mFakeWindows.size();
                for (int k = 0; k < j; k++)
                    addInputWindowHandleLw(((FakeWindowImpl)this.mService.mFakeWindows.get(k)).mWindowHandle);
                i = 0;
                break;
                label119: Slog.w("WindowManager", "Drag is in progress but there is no drag window handle.");
            }
            int m = -1 + localArrayList.size();
            while (m >= 0)
            {
                WindowState localWindowState = (WindowState)localArrayList.get(m);
                InputChannel localInputChannel = localWindowState.mInputChannel;
                InputWindowHandle localInputWindowHandle1 = localWindowState.mInputWindowHandle;
                if ((localInputChannel == null) || (localInputWindowHandle1 == null) || (localWindowState.mRemoved))
                {
                    m--;
                }
                else
                {
                    int n = localWindowState.mAttrs.flags;
                    int i1 = localWindowState.mAttrs.type;
                    boolean bool1;
                    label226: boolean bool3;
                    label256: boolean bool4;
                    if (localWindowState == this.mInputFocus)
                    {
                        bool1 = true;
                        boolean bool2 = localWindowState.isVisibleLw();
                        if ((localWindowState != this.mService.mWallpaperTarget) || (i1 == 2004))
                            break label510;
                        bool3 = true;
                        if ((i != 0) && (bool2))
                            this.mService.mDragState.sendDragStartedIfNeededLw(localWindowState);
                        localInputWindowHandle1.name = localWindowState.toString();
                        localInputWindowHandle1.layoutParamsFlags = n;
                        localInputWindowHandle1.layoutParamsType = i1;
                        localInputWindowHandle1.dispatchingTimeoutNanos = localWindowState.getInputDispatchingTimeoutNanos();
                        localInputWindowHandle1.visible = bool2;
                        localInputWindowHandle1.canReceiveKeys = localWindowState.canReceiveKeys();
                        localInputWindowHandle1.hasFocus = bool1;
                        localInputWindowHandle1.hasWallpaper = bool3;
                        if (localWindowState.mAppToken == null)
                            break label516;
                        bool4 = localWindowState.mAppToken.paused;
                        label360: localInputWindowHandle1.paused = bool4;
                        localInputWindowHandle1.layer = localWindowState.mLayer;
                        localInputWindowHandle1.ownerPid = localWindowState.mSession.mPid;
                        localInputWindowHandle1.ownerUid = localWindowState.mSession.mUid;
                        localInputWindowHandle1.inputFeatures = localWindowState.mAttrs.inputFeatures;
                        Rect localRect = localWindowState.mFrame;
                        localInputWindowHandle1.frameLeft = localRect.left;
                        localInputWindowHandle1.frameTop = localRect.top;
                        localInputWindowHandle1.frameRight = localRect.right;
                        localInputWindowHandle1.frameBottom = localRect.bottom;
                        if (localWindowState.mGlobalScale == 1.0F)
                            break label522;
                    }
                    label516: label522: for (localInputWindowHandle1.scaleFactor = (1.0F / localWindowState.mGlobalScale); ; localInputWindowHandle1.scaleFactor = 1.0F)
                    {
                        localWindowState.getTouchableRegion(localInputWindowHandle1.touchableRegion);
                        addInputWindowHandleLw(localInputWindowHandle1);
                        break;
                        bool1 = false;
                        break label226;
                        label510: bool3 = false;
                        break label256;
                        bool4 = false;
                        break label360;
                    }
                }
            }
            this.mService.mInputManager.setInputWindows(this.mInputWindowHandles);
            clearInputWindowHandlesLw();
        }
    }

    public boolean waitForInputDevicesReady(long paramLong)
    {
        synchronized (this.mInputDevicesReadyMonitor)
        {
            boolean bool1 = this.mInputDevicesReady;
            if (bool1);
        }
        try
        {
            this.mInputDevicesReadyMonitor.wait(paramLong);
            label26: boolean bool2 = this.mInputDevicesReady;
            return bool2;
            localObject2 = finally;
            throw localObject2;
        }
        catch (InterruptedException localInterruptedException)
        {
            break label26;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.wm.InputMonitor
 * JD-Core Version:        0.6.2
 */