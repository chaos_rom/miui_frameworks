package com.android.server.wm;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.FontMetricsInt;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.Surface;
import android.view.Surface.OutOfResourcesException;
import android.view.SurfaceSession;

class Watermark
{
    final int mDeltaX;
    final int mDeltaY;
    boolean mDrawNeeded;
    int mLastDH;
    int mLastDW;
    Surface mSurface;
    final String mText;
    final int mTextAscent;
    final int mTextDescent;
    final int mTextHeight;
    final Paint mTextPaint;
    final int mTextWidth;
    final String[] mTokens;

    Watermark(DisplayMetrics paramDisplayMetrics, SurfaceSession paramSurfaceSession, String[] paramArrayOfString)
    {
        this.mTokens = paramArrayOfString;
        StringBuilder localStringBuilder = new StringBuilder(32);
        int i = 0xFFFFFFFE & this.mTokens[0].length();
        int j = 0;
        if (j < i)
        {
            int i4 = this.mTokens[0].charAt(j);
            int i5 = this.mTokens[0].charAt(j + 1);
            int i6;
            label96: int i7;
            if ((i4 >= 97) && (i4 <= 102))
            {
                i6 = 10 + (i4 + -97);
                if ((i5 < 97) || (i5 > 102))
                    break label181;
                i7 = 10 + (i5 + -97);
            }
            while (true)
            {
                localStringBuilder.append(255 - (i7 + i6 * 16));
                j += 2;
                break;
                if ((i4 >= 65) && (i4 <= 70))
                {
                    i6 = 10 + (i4 + -65);
                    break label96;
                }
                i6 = i4 + -48;
                break label96;
                label181: if ((i5 >= 65) && (i5 <= 70))
                    i7 = 10 + (i5 + -65);
                else
                    i7 = i5 + -48;
            }
        }
        this.mText = localStringBuilder.toString();
        int k = WindowManagerService.getPropertyInt(paramArrayOfString, 1, 1, 20, paramDisplayMetrics);
        this.mTextPaint = new Paint(1);
        this.mTextPaint.setTextSize(k);
        this.mTextPaint.setTypeface(Typeface.create(Typeface.SANS_SERIF, 1));
        Paint.FontMetricsInt localFontMetricsInt = this.mTextPaint.getFontMetricsInt();
        this.mTextWidth = ((int)this.mTextPaint.measureText(this.mText));
        this.mTextAscent = localFontMetricsInt.ascent;
        this.mTextDescent = localFontMetricsInt.descent;
        this.mTextHeight = (localFontMetricsInt.descent - localFontMetricsInt.ascent);
        this.mDeltaX = WindowManagerService.getPropertyInt(paramArrayOfString, 2, 0, 2 * this.mTextWidth, paramDisplayMetrics);
        this.mDeltaY = WindowManagerService.getPropertyInt(paramArrayOfString, 3, 0, 3 * this.mTextHeight, paramDisplayMetrics);
        int m = WindowManagerService.getPropertyInt(paramArrayOfString, 4, 0, -1342177280, paramDisplayMetrics);
        int n = WindowManagerService.getPropertyInt(paramArrayOfString, 5, 0, 1627389951, paramDisplayMetrics);
        int i1 = WindowManagerService.getPropertyInt(paramArrayOfString, 6, 0, 7, paramDisplayMetrics);
        int i2 = WindowManagerService.getPropertyInt(paramArrayOfString, 8, 0, 0, paramDisplayMetrics);
        int i3 = WindowManagerService.getPropertyInt(paramArrayOfString, 9, 0, 0, paramDisplayMetrics);
        this.mTextPaint.setColor(n);
        this.mTextPaint.setShadowLayer(i1, i2, i3, m);
        try
        {
            this.mSurface = new Surface(paramSurfaceSession, 0, "WatermarkSurface", -1, 1, 1, -3, 0);
            this.mSurface.setLayer(1000000);
            this.mSurface.setPosition(0, 0);
            this.mSurface.show();
            label497: return;
        }
        catch (Surface.OutOfResourcesException localOutOfResourcesException)
        {
            break label497;
        }
    }

    void drawIfNeeded()
    {
        int i;
        int j;
        Rect localRect;
        Object localObject;
        if (this.mDrawNeeded)
        {
            i = this.mLastDW;
            j = this.mLastDH;
            this.mDrawNeeded = false;
            localRect = new Rect(0, 0, i, j);
            localObject = null;
        }
        try
        {
            Canvas localCanvas = this.mSurface.lockCanvas(localRect);
            localObject = localCanvas;
            label51: if (localObject != null)
            {
                localObject.drawColor(0, PorterDuff.Mode.CLEAR);
                int k = this.mDeltaX;
                int m = this.mDeltaY;
                int n = (i + this.mTextWidth) / k;
                int i1 = i + this.mTextWidth - n * k;
                int i2 = k / 4;
                if ((i1 < i2) || (i1 > k - i2))
                    k += k / 3;
                int i3 = -this.mTextHeight;
                int i4 = -this.mTextWidth;
                while (i3 < j + this.mTextHeight)
                {
                    localObject.drawText(this.mText, i4, i3, this.mTextPaint);
                    i4 += k;
                    if (i4 >= i)
                    {
                        i4 -= i + this.mTextWidth;
                        i3 += m;
                    }
                }
                this.mSurface.unlockCanvasAndPost(localObject);
            }
            return;
        }
        catch (Surface.OutOfResourcesException localOutOfResourcesException)
        {
            break label51;
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            break label51;
        }
    }

    void positionSurface(int paramInt1, int paramInt2)
    {
        if ((this.mLastDW != paramInt1) || (this.mLastDH != paramInt2))
        {
            this.mLastDW = paramInt1;
            this.mLastDH = paramInt2;
            this.mSurface.setSize(paramInt1, paramInt2);
            this.mDrawNeeded = true;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.wm.Watermark
 * JD-Core Version:        0.6.2
 */