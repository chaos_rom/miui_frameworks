package com.android.server.wm;

import android.graphics.Matrix;
import android.graphics.Rect;
import android.view.Surface;
import android.view.Surface.OutOfResourcesException;
import android.view.SurfaceSession;
import java.io.PrintWriter;

public class BlackFrame
{
    final BlackSurface[] mBlackSurfaces = new BlackSurface[4];
    final Rect mInnerRect;
    final Rect mOuterRect;
    final float[] mTmpFloats = new float[9];
    final Matrix mTmpMatrix = new Matrix();

    public BlackFrame(SurfaceSession paramSurfaceSession, Rect paramRect1, Rect paramRect2, int paramInt)
        throws Surface.OutOfResourcesException
    {
        this.mOuterRect = new Rect(paramRect1);
        this.mInnerRect = new Rect(paramRect2);
        try
        {
            if (paramRect1.top < paramRect2.top)
                this.mBlackSurfaces[0] = new BlackSurface(paramSurfaceSession, paramInt, paramRect1.left, paramRect1.top, paramRect2.right, paramRect2.top);
            if (paramRect1.left < paramRect2.left)
                this.mBlackSurfaces[1] = new BlackSurface(paramSurfaceSession, paramInt, paramRect1.left, paramRect2.top, paramRect2.left, paramRect1.bottom);
            if (paramRect1.bottom > paramRect2.bottom)
                this.mBlackSurfaces[2] = new BlackSurface(paramSurfaceSession, paramInt, paramRect2.left, paramRect2.bottom, paramRect1.right, paramRect1.bottom);
            if (paramRect1.right > paramRect2.right)
                this.mBlackSurfaces[3] = new BlackSurface(paramSurfaceSession, paramInt, paramRect2.right, paramRect1.top, paramRect1.right, paramRect2.bottom);
            if (1 == 0)
                kill();
            return;
        }
        finally
        {
            if (0 == 0)
                kill();
        }
    }

    public void clearMatrix()
    {
        for (int i = 0; i < this.mBlackSurfaces.length; i++)
            if (this.mBlackSurfaces[i] != null)
                this.mBlackSurfaces[i].clearMatrix();
    }

    public void hide()
    {
        if (this.mBlackSurfaces != null)
            for (int i = 0; i < this.mBlackSurfaces.length; i++)
                if (this.mBlackSurfaces[i] != null)
                    this.mBlackSurfaces[i].surface.hide();
    }

    public void kill()
    {
        if (this.mBlackSurfaces != null)
            for (int i = 0; i < this.mBlackSurfaces.length; i++)
                if (this.mBlackSurfaces[i] != null)
                {
                    this.mBlackSurfaces[i].surface.destroy();
                    this.mBlackSurfaces[i] = null;
                }
    }

    public void printTo(String paramString, PrintWriter paramPrintWriter)
    {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("Outer: ");
        this.mOuterRect.printShortString(paramPrintWriter);
        paramPrintWriter.print(" / Inner: ");
        this.mInnerRect.printShortString(paramPrintWriter);
        paramPrintWriter.println();
        for (int i = 0; i < this.mBlackSurfaces.length; i++)
        {
            BlackSurface localBlackSurface = this.mBlackSurfaces[i];
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("#");
            paramPrintWriter.print(i);
            paramPrintWriter.print(": ");
            paramPrintWriter.print(localBlackSurface.surface);
            paramPrintWriter.print(" left=");
            paramPrintWriter.print(localBlackSurface.left);
            paramPrintWriter.print(" top=");
            paramPrintWriter.println(localBlackSurface.top);
        }
    }

    public void setMatrix(Matrix paramMatrix)
    {
        for (int i = 0; i < this.mBlackSurfaces.length; i++)
            if (this.mBlackSurfaces[i] != null)
                this.mBlackSurfaces[i].setMatrix(paramMatrix);
    }

    class BlackSurface
    {
        final int layer;
        final int left;
        final Surface surface;
        final int top;

        BlackSurface(SurfaceSession paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int arg7)
            throws Surface.OutOfResourcesException
        {
            this.left = paramInt3;
            this.top = paramInt4;
            this.layer = paramInt2;
            int i;
            this.surface = new Surface(paramInt1, 0, "BlackSurface", -1, paramInt5 - paramInt3, i - paramInt4, -1, 131072);
            this.surface.setAlpha(1.0F);
            this.surface.setLayer(paramInt2);
            this.surface.show();
        }

        void clearMatrix()
        {
            this.surface.setMatrix(1.0F, 0.0F, 0.0F, 1.0F);
        }

        void setMatrix(Matrix paramMatrix)
        {
            BlackFrame.this.mTmpMatrix.setTranslate(this.left, this.top);
            BlackFrame.this.mTmpMatrix.postConcat(paramMatrix);
            BlackFrame.this.mTmpMatrix.getValues(BlackFrame.this.mTmpFloats);
            this.surface.setPosition(BlackFrame.this.mTmpFloats[2], BlackFrame.this.mTmpFloats[5]);
            this.surface.setMatrix(BlackFrame.this.mTmpFloats[0], BlackFrame.this.mTmpFloats[3], BlackFrame.this.mTmpFloats[1], BlackFrame.this.mTmpFloats[4]);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.wm.BlackFrame
 * JD-Core Version:        0.6.2
 */