package com.android.server.wm;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.util.Slog;
import android.view.Surface;
import android.view.Surface.OutOfResourcesException;
import android.view.SurfaceSession;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import java.io.PrintWriter;

class ScreenRotationAnimation
{
    static final boolean DEBUG_STATE = false;
    static final boolean DEBUG_TRANSFORMS = false;
    static final int FREEZE_LAYER = 2000000;
    static final String TAG = "ScreenRotationAnimation";
    static final boolean TWO_PHASE_ANIMATION;
    static final boolean USE_CUSTOM_BLACK_FRAME;
    boolean mAnimRunning;
    final Context mContext;
    int mCurRotation;
    BlackFrame mCustomBlackFrame;
    final Transformation mEnterTransformation = new Transformation();
    BlackFrame mEnteringBlackFrame;
    final Matrix mExitFrameFinalMatrix = new Matrix();
    final Transformation mExitTransformation = new Transformation();
    BlackFrame mExitingBlackFrame;
    boolean mFinishAnimReady;
    long mFinishAnimStartTime;
    Animation mFinishEnterAnimation;
    final Transformation mFinishEnterTransformation = new Transformation();
    Animation mFinishExitAnimation;
    final Transformation mFinishExitTransformation = new Transformation();
    Animation mFinishFrameAnimation;
    final Transformation mFinishFrameTransformation = new Transformation();
    final Matrix mFrameInitialMatrix = new Matrix();
    final Transformation mFrameTransformation = new Transformation();
    long mHalfwayPoint;
    int mHeight;
    Animation mLastRotateEnterAnimation;
    final Transformation mLastRotateEnterTransformation = new Transformation();
    Animation mLastRotateExitAnimation;
    final Transformation mLastRotateExitTransformation = new Transformation();
    Animation mLastRotateFrameAnimation;
    final Transformation mLastRotateFrameTransformation = new Transformation();
    private boolean mMoreFinishEnter;
    private boolean mMoreFinishExit;
    private boolean mMoreFinishFrame;
    private boolean mMoreRotateEnter;
    private boolean mMoreRotateExit;
    private boolean mMoreRotateFrame;
    private boolean mMoreStartEnter;
    private boolean mMoreStartExit;
    private boolean mMoreStartFrame;
    int mOriginalHeight;
    int mOriginalRotation;
    int mOriginalWidth;
    Animation mRotateEnterAnimation;
    final Transformation mRotateEnterTransformation = new Transformation();
    Animation mRotateExitAnimation;
    final Transformation mRotateExitTransformation = new Transformation();
    Animation mRotateFrameAnimation;
    final Transformation mRotateFrameTransformation = new Transformation();
    final Matrix mSnapshotFinalMatrix = new Matrix();
    final Matrix mSnapshotInitialMatrix = new Matrix();
    Animation mStartEnterAnimation;
    final Transformation mStartEnterTransformation = new Transformation();
    Animation mStartExitAnimation;
    final Transformation mStartExitTransformation = new Transformation();
    Animation mStartFrameAnimation;
    final Transformation mStartFrameTransformation = new Transformation();
    boolean mStarted;
    Surface mSurface;
    final float[] mTmpFloats = new float[9];
    final Matrix mTmpMatrix = new Matrix();
    int mWidth;

    public ScreenRotationAnimation(Context paramContext, SurfaceSession paramSurfaceSession, boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3)
    {
        this.mContext = paramContext;
        if ((paramInt3 == 1) || (paramInt3 == 3))
        {
            this.mWidth = paramInt2;
            this.mHeight = paramInt1;
        }
        while (true)
        {
            this.mOriginalRotation = paramInt3;
            this.mOriginalWidth = paramInt1;
            this.mOriginalHeight = paramInt2;
            if (!paramBoolean)
                Surface.openTransaction();
            try
            {
                this.mSurface = new Surface(paramSurfaceSession, 0, "FreezeSurface", -1, this.mWidth, this.mHeight, -1, 196612);
                if (!this.mSurface.isValid())
                    this.mSurface = null;
                while (true)
                {
                    return;
                    this.mWidth = paramInt1;
                    this.mHeight = paramInt2;
                    break;
                    this.mSurface.setLayer(2000001);
                    this.mSurface.setAlpha(0.0F);
                    this.mSurface.show();
                    setRotation(paramInt3);
                    if (!paramBoolean)
                        Surface.closeTransaction();
                }
            }
            catch (Surface.OutOfResourcesException localOutOfResourcesException)
            {
                while (true)
                    Slog.w("ScreenRotationAnimation", "Unable to allocate freeze surface", localOutOfResourcesException);
            }
            finally
            {
                if (!paramBoolean)
                    Surface.closeTransaction();
            }
        }
    }

    public static void createRotationMatrix(int paramInt1, int paramInt2, int paramInt3, Matrix paramMatrix)
    {
        switch (paramInt1)
        {
        default:
        case 0:
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            return;
            paramMatrix.reset();
            continue;
            paramMatrix.setRotate(90.0F, 0.0F, 0.0F);
            paramMatrix.postTranslate(paramInt3, 0.0F);
            continue;
            paramMatrix.setRotate(180.0F, 0.0F, 0.0F);
            paramMatrix.postTranslate(paramInt2, paramInt3);
            continue;
            paramMatrix.setRotate(270.0F, 0.0F, 0.0F);
            paramMatrix.postTranslate(0.0F, paramInt2);
        }
    }

    static int deltaRotation(int paramInt1, int paramInt2)
    {
        int i = paramInt2 - paramInt1;
        if (i < 0)
            i += 4;
        return i;
    }

    private boolean hasAnimations()
    {
        if ((this.mRotateEnterAnimation != null) || (this.mRotateExitAnimation != null));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void setRotation(int paramInt)
    {
        this.mCurRotation = paramInt;
        createRotationMatrix(deltaRotation(paramInt, 0), this.mWidth, this.mHeight, this.mSnapshotInitialMatrix);
        setSnapshotTransform(this.mSnapshotInitialMatrix, 1.0F);
    }

    private boolean startAnimation(SurfaceSession paramSurfaceSession, long paramLong, float paramFloat, int paramInt1, int paramInt2, boolean paramBoolean)
    {
        boolean bool;
        if (this.mSurface == null)
            bool = false;
        while (true)
        {
            return bool;
            if (this.mStarted)
            {
                bool = true;
                continue;
            }
            this.mStarted = true;
            int i = deltaRotation(this.mCurRotation, this.mOriginalRotation);
            switch (i)
            {
            default:
                label76: ((paramInt1 + this.mOriginalWidth) / 2);
                ((paramInt2 + this.mOriginalHeight) / 2);
                this.mRotateEnterAnimation.initialize(paramInt1, paramInt2, this.mOriginalWidth, this.mOriginalHeight);
                this.mRotateExitAnimation.initialize(paramInt1, paramInt2, this.mOriginalWidth, this.mOriginalHeight);
                this.mAnimRunning = false;
                this.mFinishAnimReady = false;
                this.mFinishAnimStartTime = -1L;
                this.mRotateExitAnimation.restrictDuration(paramLong);
                this.mRotateExitAnimation.scaleCurrentDuration(paramFloat);
                this.mRotateEnterAnimation.restrictDuration(paramLong);
                this.mRotateEnterAnimation.scaleCurrentDuration(paramFloat);
                if (this.mExitingBlackFrame == null)
                {
                    Surface.openTransaction();
                    createRotationMatrix(i, this.mOriginalWidth, this.mOriginalHeight, this.mFrameInitialMatrix);
                }
                break;
            case 0:
            case 1:
            case 2:
            case 3:
            }
            try
            {
                this.mExitingBlackFrame = new BlackFrame(paramSurfaceSession, new Rect(1 * -this.mOriginalWidth, 1 * -this.mOriginalHeight, 2 * this.mOriginalWidth, 2 * this.mOriginalHeight), new Rect(0, 0, this.mOriginalWidth, this.mOriginalHeight), 2000002);
                this.mExitingBlackFrame.setMatrix(this.mFrameInitialMatrix);
                Surface.closeTransaction();
                bool = true;
                continue;
                this.mRotateExitAnimation = AnimationUtils.loadAnimation(this.mContext, 17432632);
                this.mRotateEnterAnimation = AnimationUtils.loadAnimation(this.mContext, 17432631);
                break label76;
                this.mRotateExitAnimation = AnimationUtils.loadAnimation(this.mContext, 17432644);
                this.mRotateEnterAnimation = AnimationUtils.loadAnimation(this.mContext, 17432643);
                break label76;
                this.mRotateExitAnimation = AnimationUtils.loadAnimation(this.mContext, 17432635);
                this.mRotateEnterAnimation = AnimationUtils.loadAnimation(this.mContext, 17432634);
                this.mRotateFrameAnimation = AnimationUtils.loadAnimation(this.mContext, 17432636);
                break label76;
                this.mRotateExitAnimation = AnimationUtils.loadAnimation(this.mContext, 17432641);
                this.mRotateEnterAnimation = AnimationUtils.loadAnimation(this.mContext, 17432640);
                break label76;
            }
            catch (Surface.OutOfResourcesException localOutOfResourcesException)
            {
                while (true)
                {
                    Slog.w("ScreenRotationAnimation", "Unable to allocate black surface", localOutOfResourcesException);
                    Surface.closeTransaction();
                }
            }
            finally
            {
                Surface.closeTransaction();
            }
        }
    }

    private boolean stepAnimation(long paramLong)
    {
        boolean bool = false;
        if (paramLong > this.mHalfwayPoint)
            this.mHalfwayPoint = 9223372036854775807L;
        if ((this.mFinishAnimReady) && (this.mFinishAnimStartTime < 0L))
            this.mFinishAnimStartTime = paramLong;
        if (this.mFinishAnimReady)
            (paramLong - this.mFinishAnimStartTime);
        this.mMoreRotateExit = false;
        if (this.mRotateExitAnimation != null)
            this.mMoreRotateExit = this.mRotateExitAnimation.getTransformation(paramLong, this.mRotateExitTransformation);
        this.mMoreRotateEnter = false;
        if (this.mRotateEnterAnimation != null)
            this.mMoreRotateEnter = this.mRotateEnterAnimation.getTransformation(paramLong, this.mRotateEnterTransformation);
        if ((!this.mMoreRotateExit) && (this.mRotateExitAnimation != null))
        {
            this.mRotateExitAnimation.cancel();
            this.mRotateExitAnimation = null;
            this.mRotateExitTransformation.clear();
        }
        if ((!this.mMoreRotateEnter) && (this.mRotateEnterAnimation != null))
        {
            this.mRotateEnterAnimation.cancel();
            this.mRotateEnterAnimation = null;
            this.mRotateEnterTransformation.clear();
        }
        this.mExitTransformation.set(this.mRotateExitTransformation);
        this.mEnterTransformation.set(this.mRotateEnterTransformation);
        if ((this.mMoreRotateEnter) || (this.mMoreRotateExit) || (!this.mFinishAnimReady))
            bool = true;
        this.mSnapshotFinalMatrix.setConcat(this.mExitTransformation.getMatrix(), this.mSnapshotInitialMatrix);
        return bool;
    }

    public boolean dismiss(SurfaceSession paramSurfaceSession, long paramLong, float paramFloat, int paramInt1, int paramInt2)
    {
        boolean bool = true;
        if (this.mSurface == null)
            bool = false;
        while (true)
        {
            return bool;
            if (!this.mStarted)
                startAnimation(paramSurfaceSession, paramLong, paramFloat, paramInt1, paramInt2, bool);
            if (!this.mStarted)
                bool = false;
            else
                this.mFinishAnimReady = bool;
        }
    }

    public Transformation getEnterTransformation()
    {
        return this.mEnterTransformation;
    }

    boolean hasScreenshot()
    {
        if (this.mSurface != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isAnimating()
    {
        if (!hasAnimations());
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    public void kill()
    {
        if (this.mSurface != null)
        {
            this.mSurface.destroy();
            this.mSurface = null;
        }
        if (this.mCustomBlackFrame != null)
        {
            this.mCustomBlackFrame.kill();
            this.mCustomBlackFrame = null;
        }
        if (this.mExitingBlackFrame != null)
        {
            this.mExitingBlackFrame.kill();
            this.mExitingBlackFrame = null;
        }
        if (this.mEnteringBlackFrame != null)
        {
            this.mEnteringBlackFrame.kill();
            this.mEnteringBlackFrame = null;
        }
        if (this.mRotateExitAnimation != null)
        {
            this.mRotateExitAnimation.cancel();
            this.mRotateExitAnimation = null;
        }
        if (this.mRotateEnterAnimation != null)
        {
            this.mRotateEnterAnimation.cancel();
            this.mRotateEnterAnimation = null;
        }
    }

    public void printTo(String paramString, PrintWriter paramPrintWriter)
    {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mSurface=");
        paramPrintWriter.print(this.mSurface);
        paramPrintWriter.print(" mWidth=");
        paramPrintWriter.print(this.mWidth);
        paramPrintWriter.print(" mHeight=");
        paramPrintWriter.println(this.mHeight);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mExitingBlackFrame=");
        paramPrintWriter.println(this.mExitingBlackFrame);
        if (this.mExitingBlackFrame != null)
            this.mExitingBlackFrame.printTo(paramString + "    ", paramPrintWriter);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mEnteringBlackFrame=");
        paramPrintWriter.println(this.mEnteringBlackFrame);
        if (this.mEnteringBlackFrame != null)
            this.mEnteringBlackFrame.printTo(paramString + "    ", paramPrintWriter);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mCurRotation=");
        paramPrintWriter.print(this.mCurRotation);
        paramPrintWriter.print(" mOriginalRotation=");
        paramPrintWriter.println(this.mOriginalRotation);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mOriginalWidth=");
        paramPrintWriter.print(this.mOriginalWidth);
        paramPrintWriter.print(" mOriginalHeight=");
        paramPrintWriter.println(this.mOriginalHeight);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mStarted=");
        paramPrintWriter.print(this.mStarted);
        paramPrintWriter.print(" mAnimRunning=");
        paramPrintWriter.print(this.mAnimRunning);
        paramPrintWriter.print(" mFinishAnimReady=");
        paramPrintWriter.print(this.mFinishAnimReady);
        paramPrintWriter.print(" mFinishAnimStartTime=");
        paramPrintWriter.println(this.mFinishAnimStartTime);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mStartExitAnimation=");
        paramPrintWriter.print(this.mStartExitAnimation);
        paramPrintWriter.print(" ");
        this.mStartExitTransformation.printShortString(paramPrintWriter);
        paramPrintWriter.println();
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mStartEnterAnimation=");
        paramPrintWriter.print(this.mStartEnterAnimation);
        paramPrintWriter.print(" ");
        this.mStartEnterTransformation.printShortString(paramPrintWriter);
        paramPrintWriter.println();
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mStartFrameAnimation=");
        paramPrintWriter.print(this.mStartFrameAnimation);
        paramPrintWriter.print(" ");
        this.mStartFrameTransformation.printShortString(paramPrintWriter);
        paramPrintWriter.println();
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mFinishExitAnimation=");
        paramPrintWriter.print(this.mFinishExitAnimation);
        paramPrintWriter.print(" ");
        this.mFinishExitTransformation.printShortString(paramPrintWriter);
        paramPrintWriter.println();
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mFinishEnterAnimation=");
        paramPrintWriter.print(this.mFinishEnterAnimation);
        paramPrintWriter.print(" ");
        this.mFinishEnterTransformation.printShortString(paramPrintWriter);
        paramPrintWriter.println();
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mFinishFrameAnimation=");
        paramPrintWriter.print(this.mFinishFrameAnimation);
        paramPrintWriter.print(" ");
        this.mFinishFrameTransformation.printShortString(paramPrintWriter);
        paramPrintWriter.println();
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mRotateExitAnimation=");
        paramPrintWriter.print(this.mRotateExitAnimation);
        paramPrintWriter.print(" ");
        this.mRotateExitTransformation.printShortString(paramPrintWriter);
        paramPrintWriter.println();
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mRotateEnterAnimation=");
        paramPrintWriter.print(this.mRotateEnterAnimation);
        paramPrintWriter.print(" ");
        this.mRotateEnterTransformation.printShortString(paramPrintWriter);
        paramPrintWriter.println();
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mRotateFrameAnimation=");
        paramPrintWriter.print(this.mRotateFrameAnimation);
        paramPrintWriter.print(" ");
        this.mRotateFrameTransformation.printShortString(paramPrintWriter);
        paramPrintWriter.println();
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mExitTransformation=");
        this.mExitTransformation.printShortString(paramPrintWriter);
        paramPrintWriter.println();
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mEnterTransformation=");
        this.mEnterTransformation.printShortString(paramPrintWriter);
        paramPrintWriter.println();
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mFrameTransformation=");
        this.mEnterTransformation.printShortString(paramPrintWriter);
        paramPrintWriter.println();
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mFrameInitialMatrix=");
        this.mFrameInitialMatrix.printShortString(paramPrintWriter);
        paramPrintWriter.println();
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mSnapshotInitialMatrix=");
        this.mSnapshotInitialMatrix.printShortString(paramPrintWriter);
        paramPrintWriter.print(" mSnapshotFinalMatrix=");
        this.mSnapshotFinalMatrix.printShortString(paramPrintWriter);
        paramPrintWriter.println();
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mExitFrameFinalMatrix=");
        this.mExitFrameFinalMatrix.printShortString(paramPrintWriter);
        paramPrintWriter.println();
    }

    public boolean setRotation(int paramInt1, SurfaceSession paramSurfaceSession, long paramLong, float paramFloat, int paramInt2, int paramInt3)
    {
        setRotation(paramInt1);
        return false;
    }

    void setSnapshotTransform(Matrix paramMatrix, float paramFloat)
    {
        if (this.mSurface != null)
        {
            paramMatrix.getValues(this.mTmpFloats);
            this.mSurface.setPosition(this.mTmpFloats[2], this.mTmpFloats[5]);
            this.mSurface.setMatrix(this.mTmpFloats[0], this.mTmpFloats[3], this.mTmpFloats[1], this.mTmpFloats[4]);
            this.mSurface.setAlpha(paramFloat);
        }
    }

    public boolean stepAnimationLocked(long paramLong)
    {
        boolean bool = false;
        if (!hasAnimations())
            this.mFinishAnimReady = false;
        while (true)
        {
            return bool;
            if (!this.mAnimRunning)
            {
                if (this.mRotateEnterAnimation != null)
                    this.mRotateEnterAnimation.setStartTime(paramLong);
                if (this.mRotateExitAnimation != null)
                    this.mRotateExitAnimation.setStartTime(paramLong);
                this.mAnimRunning = true;
                this.mHalfwayPoint = (paramLong + this.mRotateEnterAnimation.getDuration() / 2L);
            }
            bool = stepAnimation(paramLong);
        }
    }

    void updateSurfaces()
    {
        if (!this.mStarted)
            return;
        if ((this.mSurface != null) && (!this.mMoreStartExit) && (!this.mMoreFinishExit) && (!this.mMoreRotateExit))
            this.mSurface.hide();
        if (this.mCustomBlackFrame != null)
        {
            if ((!this.mMoreStartFrame) && (!this.mMoreFinishFrame) && (!this.mMoreRotateFrame))
                this.mCustomBlackFrame.hide();
        }
        else
        {
            label78: if (this.mExitingBlackFrame != null)
            {
                if ((this.mMoreStartExit) || (this.mMoreFinishExit) || (this.mMoreRotateExit))
                    break label183;
                this.mExitingBlackFrame.hide();
            }
            label113: if (this.mEnteringBlackFrame != null)
            {
                if ((this.mMoreStartEnter) || (this.mMoreFinishEnter) || (this.mMoreRotateEnter))
                    break label216;
                this.mEnteringBlackFrame.hide();
            }
        }
        while (true)
        {
            setSnapshotTransform(this.mSnapshotFinalMatrix, this.mExitTransformation.getAlpha());
            break;
            this.mCustomBlackFrame.setMatrix(this.mFrameTransformation.getMatrix());
            break label78;
            label183: this.mExitFrameFinalMatrix.setConcat(this.mExitTransformation.getMatrix(), this.mFrameInitialMatrix);
            this.mExitingBlackFrame.setMatrix(this.mExitFrameFinalMatrix);
            break label113;
            label216: this.mEnteringBlackFrame.setMatrix(this.mEnterTransformation.getMatrix());
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.wm.ScreenRotationAnimation
 * JD-Core Version:        0.6.2
 */