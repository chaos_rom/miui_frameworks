package com.android.server.wm;

import android.graphics.Matrix;
import android.view.Surface;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import java.io.PrintWriter;
import java.util.ArrayList;

public class AppWindowAnimator
{
    static final String TAG = "AppWindowAnimator";
    static final Animation sDummyAnimation = new DummyAnimation();
    boolean allDrawn;
    boolean animInitialized;
    int animLayerAdjustment;
    boolean animating;
    Animation animation;
    boolean freezingScreen;
    boolean hasTransformation;
    final WindowAnimator mAnimator;
    final AppWindowToken mAppToken;
    final WindowManagerService mService;
    Surface thumbnail;
    Animation thumbnailAnimation;
    int thumbnailLayer;
    int thumbnailTransactionSeq;
    final Transformation thumbnailTransformation = new Transformation();
    int thumbnailX;
    int thumbnailY;
    final Transformation transformation = new Transformation();

    public AppWindowAnimator(WindowManagerService paramWindowManagerService, AppWindowToken paramAppWindowToken)
    {
        this.mService = paramWindowManagerService;
        this.mAppToken = paramAppWindowToken;
        this.mAnimator = paramWindowManagerService.mAnimator;
    }

    private boolean stepAnimation(long paramLong)
    {
        boolean bool;
        if (this.animation == null)
            bool = false;
        while (true)
        {
            return bool;
            this.transformation.clear();
            bool = this.animation.getTransformation(paramLong, this.transformation);
            if (!bool)
            {
                this.animation = null;
                clearThumbnail();
            }
            this.hasTransformation = bool;
        }
    }

    private void stepThumbnailAnimation(long paramLong)
    {
        this.thumbnailTransformation.clear();
        this.thumbnailAnimation.getTransformation(paramLong, this.thumbnailTransformation);
        this.thumbnailTransformation.getMatrix().preTranslate(this.thumbnailX, this.thumbnailY);
        if ((this.mAnimator.mScreenRotationAnimation != null) && (this.mAnimator.mScreenRotationAnimation.isAnimating()));
        for (int i = 1; ; i = 0)
        {
            if (i != 0)
                this.thumbnailTransformation.postCompose(this.mAnimator.mScreenRotationAnimation.getEnterTransformation());
            float[] arrayOfFloat = this.mService.mTmpFloats;
            this.thumbnailTransformation.getMatrix().getValues(arrayOfFloat);
            this.thumbnail.setPosition(arrayOfFloat[2], arrayOfFloat[5]);
            this.thumbnail.setAlpha(this.thumbnailTransformation.getAlpha());
            this.thumbnail.setLayer(-4 + (5 + this.thumbnailLayer));
            this.thumbnail.setMatrix(arrayOfFloat[0], arrayOfFloat[3], arrayOfFloat[1], arrayOfFloat[4]);
            return;
        }
    }

    public void clearAnimation()
    {
        if (this.animation != null)
        {
            this.animation = null;
            this.animating = true;
            this.animInitialized = false;
        }
        clearThumbnail();
    }

    public void clearThumbnail()
    {
        if (this.thumbnail != null)
        {
            this.thumbnail.destroy();
            this.thumbnail = null;
        }
    }

    void dump(PrintWriter paramPrintWriter, String paramString)
    {
        if (this.freezingScreen)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print(" freezingScreen=");
            paramPrintWriter.println(this.freezingScreen);
        }
        if ((this.animating) || (this.animation != null))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("animating=");
            paramPrintWriter.print(this.animating);
            paramPrintWriter.print(" animation=");
            paramPrintWriter.println(this.animation);
        }
        if (this.hasTransformation)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("XForm: ");
            this.transformation.printShortString(paramPrintWriter);
            paramPrintWriter.println();
        }
        if (this.animLayerAdjustment != 0)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("animLayerAdjustment=");
            paramPrintWriter.println(this.animLayerAdjustment);
        }
        if (this.thumbnail != null)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("thumbnail=");
            paramPrintWriter.print(this.thumbnail);
            paramPrintWriter.print(" x=");
            paramPrintWriter.print(this.thumbnailX);
            paramPrintWriter.print(" y=");
            paramPrintWriter.print(this.thumbnailY);
            paramPrintWriter.print(" layer=");
            paramPrintWriter.println(this.thumbnailLayer);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("thumbnailAnimation=");
            paramPrintWriter.println(this.thumbnailAnimation);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("thumbnailTransformation=");
            paramPrintWriter.println(this.thumbnailTransformation.toShortString());
        }
    }

    public void setAnimation(Animation paramAnimation, boolean paramBoolean)
    {
        this.animation = paramAnimation;
        this.animating = false;
        this.animInitialized = paramBoolean;
        paramAnimation.restrictDuration(10000L);
        paramAnimation.scaleCurrentDuration(this.mService.mTransitionAnimationScale);
        int i = paramAnimation.getZAdjustment();
        int j = 0;
        Transformation localTransformation;
        if (i == 1)
        {
            j = 1000;
            if (this.animLayerAdjustment != j)
            {
                this.animLayerAdjustment = j;
                updateLayers();
            }
            this.transformation.clear();
            localTransformation = this.transformation;
            if (!this.mAppToken.reportedVisible)
                break label123;
        }
        label123: for (float f = 1.0F; ; f = 0.0F)
        {
            localTransformation.setAlpha(f);
            this.hasTransformation = true;
            return;
            if (i != -1)
                break;
            j = -1000;
            break;
        }
    }

    public void setDummyAnimation()
    {
        this.animation = sDummyAnimation;
        this.animInitialized = false;
        this.hasTransformation = true;
        this.transformation.clear();
        Transformation localTransformation = this.transformation;
        if (this.mAppToken.reportedVisible);
        for (float f = 1.0F; ; f = 0.0F)
        {
            localTransformation.setAlpha(f);
            return;
        }
    }

    boolean showAllWindowsLocked()
    {
        boolean bool = false;
        int i = this.mAppToken.allAppWindows.size();
        for (int j = 0; j < i; j++)
        {
            WindowStateAnimator localWindowStateAnimator = ((WindowState)this.mAppToken.allAppWindows.get(j)).mWinAnimator;
            localWindowStateAnimator.performShowLocked();
            bool |= localWindowStateAnimator.isAnimating();
        }
        return bool;
    }

    boolean stepAnimationLocked(long paramLong, int paramInt1, int paramInt2)
    {
        boolean bool = true;
        if (this.mService.okToDisplay())
            if (this.animation == sDummyAnimation)
                bool = false;
        while (true)
        {
            return bool;
            if (((this.mAppToken.allDrawn) || (this.animating) || (this.mAppToken.startingDisplayed)) && (this.animation != null))
            {
                if (!this.animating)
                {
                    if (!this.animInitialized)
                        this.animation.initialize(paramInt1, paramInt2, paramInt1, paramInt2);
                    this.animation.setStartTime(paramLong);
                    this.animating = bool;
                    if (this.thumbnail != null)
                    {
                        this.thumbnail.show();
                        this.thumbnailAnimation.setStartTime(paramLong);
                    }
                }
                if (stepAnimation(paramLong))
                {
                    if (this.thumbnail == null)
                        continue;
                    stepThumbnailAnimation(paramLong);
                    continue;
                    if (this.animation != null)
                    {
                        this.animating = bool;
                        this.animation = null;
                    }
                }
            }
            else
            {
                this.hasTransformation = false;
                if ((!this.animating) && (this.animation == null))
                {
                    bool = false;
                }
                else
                {
                    WindowAnimator localWindowAnimator = this.mAnimator;
                    localWindowAnimator.mPendingLayoutChanges = (0x8 | localWindowAnimator.mPendingLayoutChanges);
                    this.mService.debugLayoutRepeats("AppWindowToken", this.mAnimator.mPendingLayoutChanges);
                    clearAnimation();
                    this.animating = false;
                    if (this.animLayerAdjustment != 0)
                    {
                        this.animLayerAdjustment = 0;
                        updateLayers();
                    }
                    if ((this.mService.mInputMethodTarget != null) && (this.mService.mInputMethodTarget.mAppToken == this.mAppToken))
                        this.mService.moveInputMethodWindowsIfNeededLocked(bool);
                    this.transformation.clear();
                    int i = this.mAppToken.windows.size();
                    for (int j = 0; j < i; j++)
                        ((WindowState)this.mAppToken.windows.get(j)).mWinAnimator.finishExit();
                    this.mAppToken.updateReportedVisibilityLocked();
                    bool = false;
                }
            }
        }
    }

    void updateLayers()
    {
        int i = this.mAppToken.allAppWindows.size();
        int j = this.animLayerAdjustment;
        this.thumbnailLayer = -1;
        for (int k = 0; k < i; k++)
        {
            WindowState localWindowState = (WindowState)this.mAppToken.allAppWindows.get(k);
            WindowStateAnimator localWindowStateAnimator = localWindowState.mWinAnimator;
            localWindowStateAnimator.mAnimLayer = (j + localWindowState.mLayer);
            if (localWindowStateAnimator.mAnimLayer > this.thumbnailLayer)
                this.thumbnailLayer = localWindowStateAnimator.mAnimLayer;
            if ((localWindowState == this.mService.mInputMethodTarget) && (!this.mService.mInputMethodTargetWaitingAnim))
                this.mService.setInputMethodAnimLayerAdjustment(j);
            if ((localWindowState == this.mService.mWallpaperTarget) && (this.mService.mLowerWallpaperTarget == null))
                this.mService.setWallpaperAnimLayerAdjustmentLocked(j);
        }
    }

    static final class DummyAnimation extends Animation
    {
        public boolean getTransformation(long paramLong, Transformation paramTransformation)
        {
            return false;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.wm.AppWindowAnimator
 * JD-Core Version:        0.6.2
 */