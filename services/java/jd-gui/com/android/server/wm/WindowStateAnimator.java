package com.android.server.wm;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.os.Debug;
import android.util.Slog;
import android.view.Surface;
import android.view.Surface.OutOfResourcesException;
import android.view.SurfaceSession;
import android.view.WindowManager.LayoutParams;
import android.view.WindowManagerPolicy;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import java.io.PrintWriter;
import java.util.ArrayList;

class WindowStateAnimator
{
    static final int COMMIT_DRAW_PENDING = 2;
    static final boolean DEBUG_ANIM = false;
    static final boolean DEBUG_LAYERS = false;
    static final boolean DEBUG_ORIENTATION = false;
    static final boolean DEBUG_STARTING_WINDOW = false;
    static final boolean DEBUG_SURFACE_TRACE = false;
    static final boolean DEBUG_VISIBILITY = false;
    static final int DRAW_PENDING = 1;
    static final int HAS_DRAWN = 4;
    static final int NO_SURFACE = 0;
    static final int READY_TO_SHOW = 3;
    static final boolean SHOW_LIGHT_TRANSACTIONS = false;
    static final boolean SHOW_SURFACE_ALLOC = false;
    static final boolean SHOW_TRANSACTIONS = false;
    static final String TAG = "WindowStateAnimator";
    static final boolean localLOGV;
    float mAlpha = 0.0F;
    int mAnimDh;
    int mAnimDw;
    int mAnimLayer;
    boolean mAnimating;
    Animation mAnimation;
    boolean mAnimationIsEntrance;
    final WindowAnimator mAnimator;
    final WindowState mAttachedWindow;
    int mAttrFlags;
    int mAttrType;
    final Context mContext;
    int mDrawState;
    float mDsDx = 1.0F;
    float mDsDy = 0.0F;
    float mDtDx = 0.0F;
    float mDtDy = 1.0F;
    boolean mEnterAnimationPending;
    boolean mHasLocalTransformation;
    boolean mHasTransformation;
    boolean mHaveMatrix;
    float mLastAlpha = 0.0F;
    float mLastDsDx = 1.0F;
    float mLastDsDy = 0.0F;
    float mLastDtDx = 0.0F;
    float mLastDtDy = 1.0F;
    boolean mLastHidden;
    int mLastLayer;
    boolean mLocalAnimating;
    Surface mPendingDestroySurface;
    final WindowManagerPolicy mPolicy;
    final WindowManagerService mService;
    final Session mSession;
    float mShownAlpha = 0.0F;
    Surface mSurface;
    float mSurfaceAlpha;
    boolean mSurfaceDestroyDeferred;
    float mSurfaceH;
    int mSurfaceLayer;
    boolean mSurfaceResized;
    boolean mSurfaceShown;
    float mSurfaceW;
    float mSurfaceX;
    float mSurfaceY;
    final Transformation mTransformation = new Transformation();
    boolean mWasAnimating;
    final WindowState mWin;

    public WindowStateAnimator(WindowManagerService paramWindowManagerService, WindowState paramWindowState1, WindowState paramWindowState2)
    {
        this.mService = paramWindowManagerService;
        this.mWin = paramWindowState1;
        this.mAttachedWindow = paramWindowState2;
        this.mAnimator = this.mService.mAnimator;
        this.mSession = paramWindowState1.mSession;
        this.mPolicy = this.mService.mPolicy;
        this.mContext = this.mService.mContext;
        this.mAttrFlags = paramWindowState1.mAttrs.flags;
        this.mAttrType = paramWindowState1.mAttrs.type;
        this.mAnimDw = paramWindowManagerService.mAppDisplayWidth;
        this.mAnimDh = paramWindowManagerService.mAppDisplayHeight;
    }

    static String drawStateToString(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = Integer.toString(paramInt);
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        }
        while (true)
        {
            return str;
            str = "NO_SURFACE";
            continue;
            str = "DRAW_PENDING";
            continue;
            str = "COMMIT_DRAW_PENDING";
            continue;
            str = "READY_TO_SHOW";
            continue;
            str = "HAS_DRAWN";
        }
    }

    private boolean stepAnimation(long paramLong)
    {
        if ((this.mAnimation == null) || (!this.mLocalAnimating));
        for (boolean bool = false; ; bool = this.mAnimation.getTransformation(paramLong, this.mTransformation))
        {
            return bool;
            this.mTransformation.clear();
        }
    }

    boolean applyAnimationLocked(int paramInt, boolean paramBoolean)
    {
        boolean bool = true;
        if ((this.mLocalAnimating) && (this.mAnimationIsEntrance == paramBoolean));
        label188: label195: 
        while (true)
        {
            return bool;
            int j;
            Animation localAnimation;
            if (this.mService.okToDisplay())
            {
                int i = this.mPolicy.selectAnimationLw(this.mWin, paramInt);
                j = -1;
                localAnimation = null;
                if (i != 0)
                {
                    localAnimation = AnimationUtils.loadAnimation(this.mContext, i);
                    label68: if (localAnimation != null)
                    {
                        setAnimation(localAnimation);
                        this.mAnimationIsEntrance = paramBoolean;
                    }
                }
            }
            while (true)
            {
                if (this.mAnimation != null)
                    break label195;
                bool = false;
                break;
                switch (paramInt)
                {
                default:
                case 4097:
                case 8194:
                case 4099:
                case 8196:
                }
                while (true)
                {
                    if (j < 0)
                        break label188;
                    localAnimation = this.mService.loadAnimation(this.mWin.mAttrs, j);
                    break;
                    j = 0;
                    continue;
                    j = 1;
                    continue;
                    j = 2;
                    continue;
                    j = 3;
                }
                break label68;
                clearAnimation();
            }
        }
    }

    void applyEnterAnimationLocked()
    {
        if (this.mEnterAnimationPending)
            this.mEnterAnimationPending = false;
        for (int i = 4097; ; i = 4099)
        {
            applyAnimationLocked(i, true);
            return;
        }
    }

    void cancelExitAnimationForNextAnimationLocked()
    {
        if (this.mAnimation != null)
        {
            this.mAnimation.cancel();
            this.mAnimation = null;
            this.mLocalAnimating = false;
            destroySurfaceLocked();
        }
    }

    public void clearAnimation()
    {
        if (this.mAnimation != null)
        {
            this.mAnimating = true;
            this.mLocalAnimating = false;
            this.mAnimation.cancel();
            this.mAnimation = null;
        }
    }

    boolean commitFinishDrawingLocked(long paramLong)
    {
        boolean bool = false;
        if (this.mDrawState != 2);
        while (true)
        {
            return bool;
            this.mDrawState = 3;
            if (this.mWin.mAttrs.type == 3)
                bool = true;
            AppWindowToken localAppWindowToken = this.mWin.mAppToken;
            if ((localAppWindowToken == null) || (localAppWindowToken.allDrawn) || (bool))
                performShowLocked();
            bool = true;
        }
    }

    void computeShownFrameLocked()
    {
        boolean bool = this.mHasLocalTransformation;
        Transformation localTransformation1;
        AppWindowAnimator localAppWindowAnimator1;
        label48: Transformation localTransformation2;
        label65: AppWindowAnimator localAppWindowAnimator2;
        label182: int i;
        label253: Matrix localMatrix;
        if ((this.mAttachedWindow != null) && (this.mAttachedWindow.mWinAnimator.mHasLocalTransformation))
        {
            localTransformation1 = this.mAttachedWindow.mWinAnimator.mTransformation;
            if (this.mWin.mAppToken != null)
                break label758;
            localAppWindowAnimator1 = null;
            if ((localAppWindowAnimator1 == null) || (!localAppWindowAnimator1.hasTransformation))
                break label772;
            localTransformation2 = localAppWindowAnimator1.transformation;
            if ((this.mWin.mAttrs.type == 2013) && (this.mService.mLowerWallpaperTarget == null) && (this.mService.mWallpaperTarget != null))
            {
                if ((this.mService.mWallpaperTarget.mWinAnimator.mHasLocalTransformation) && (this.mService.mWallpaperTarget.mWinAnimator.mAnimation != null) && (!this.mService.mWallpaperTarget.mWinAnimator.mAnimation.getDetachWallpaper()))
                    localTransformation1 = this.mService.mWallpaperTarget.mWinAnimator.mTransformation;
                if (this.mService.mWallpaperTarget.mAppToken != null)
                    break label778;
                localAppWindowAnimator2 = null;
                if ((localAppWindowAnimator2 != null) && (localAppWindowAnimator2.hasTransformation) && (localAppWindowAnimator2.animation != null) && (!localAppWindowAnimator2.animation.getDetachWallpaper()))
                    localTransformation2 = localAppWindowAnimator2.transformation;
            }
            if ((this.mService.mAnimator.mScreenRotationAnimation == null) || (!this.mService.mAnimator.mScreenRotationAnimation.isAnimating()))
                break label796;
            i = 1;
            if ((!bool) && (localTransformation1 == null) && (localTransformation2 == null) && (i == 0))
                break label818;
            Rect localRect = this.mWin.mFrame;
            float[] arrayOfFloat = this.mService.mTmpFloats;
            localMatrix = this.mWin.mTmpMatrix;
            if (i == 0)
                break label810;
            float f3 = localRect.width();
            float f4 = localRect.height();
            if ((f3 < 1.0F) || (f4 < 1.0F))
                break label802;
            localMatrix.setScale(1.0F + 2.0F / f3, 1.0F + 2.0F / f4, f3 / 2.0F, f4 / 2.0F);
            label358: localMatrix.postScale(this.mWin.mGlobalScale, this.mWin.mGlobalScale);
            if (bool)
                localMatrix.postConcat(this.mTransformation.getMatrix());
            localMatrix.postTranslate(localRect.left + this.mWin.mXOffset, localRect.top + this.mWin.mYOffset);
            if (localTransformation1 != null)
                localMatrix.postConcat(localTransformation1.getMatrix());
            if (localTransformation2 != null)
                localMatrix.postConcat(localTransformation2.getMatrix());
            if (i != 0)
                localMatrix.postConcat(this.mService.mAnimator.mScreenRotationAnimation.getEnterTransformation().getMatrix());
            this.mHaveMatrix = true;
            localMatrix.getValues(arrayOfFloat);
            this.mDsDx = arrayOfFloat[0];
            this.mDtDx = arrayOfFloat[3];
            this.mDsDy = arrayOfFloat[1];
            this.mDtDy = arrayOfFloat[4];
            float f1 = arrayOfFloat[2];
            float f2 = arrayOfFloat[5];
            int j = localRect.width();
            int k = localRect.height();
            this.mWin.mShownFrame.set(f1, f2, f1 + j, f2 + k);
            this.mShownAlpha = this.mAlpha;
            if ((!this.mService.mLimitedAlphaCompositing) || (!PixelFormat.formatHasAlpha(this.mWin.mAttrs.format)) || ((this.mWin.isIdentityMatrix(this.mDsDx, this.mDtDx, this.mDsDy, this.mDtDy)) && (f1 == localRect.left) && (f2 == localRect.top)))
            {
                if (bool)
                    this.mShownAlpha *= this.mTransformation.getAlpha();
                if (localTransformation1 != null)
                    this.mShownAlpha *= localTransformation1.getAlpha();
                if (localTransformation2 != null)
                    this.mShownAlpha *= localTransformation2.getAlpha();
                if (i != 0)
                    this.mShownAlpha *= this.mService.mAnimator.mScreenRotationAnimation.getEnterTransformation().getAlpha();
            }
        }
        while (true)
        {
            return;
            localTransformation1 = null;
            break;
            label758: localAppWindowAnimator1 = this.mWin.mAppToken.mAppAnimator;
            break label48;
            label772: localTransformation2 = null;
            break label65;
            label778: localAppWindowAnimator2 = this.mService.mWallpaperTarget.mAppToken.mAppAnimator;
            break label182;
            label796: i = 0;
            break label253;
            label802: localMatrix.reset();
            break label358;
            label810: localMatrix.reset();
            break label358;
            label818: if ((!this.mWin.mIsWallpaper) || ((0x1 & this.mAnimator.mPendingActions) == 0))
            {
                this.mWin.mShownFrame.set(this.mWin.mFrame);
                if ((this.mWin.mXOffset != 0) || (this.mWin.mYOffset != 0))
                    this.mWin.mShownFrame.offset(this.mWin.mXOffset, this.mWin.mYOffset);
                this.mShownAlpha = this.mAlpha;
                this.mHaveMatrix = false;
                this.mDsDx = this.mWin.mGlobalScale;
                this.mDtDx = 0.0F;
                this.mDsDy = 0.0F;
                this.mDtDy = this.mWin.mGlobalScale;
            }
        }
    }

    // ERROR //
    Surface createSurfaceLocked()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 420	com/android/server/wm/WindowStateAnimator:mSurface	Landroid/view/Surface;
        //     4: ifnonnull +407 -> 411
        //     7: aload_0
        //     8: iconst_1
        //     9: putfield 244	com/android/server/wm/WindowStateAnimator:mDrawState	I
        //     12: aload_0
        //     13: getfield 126	com/android/server/wm/WindowStateAnimator:mWin	Lcom/android/server/wm/WindowState;
        //     16: getfield 248	com/android/server/wm/WindowState:mAppToken	Lcom/android/server/wm/AppWindowToken;
        //     19: ifnull +14 -> 33
        //     22: aload_0
        //     23: getfield 126	com/android/server/wm/WindowStateAnimator:mWin	Lcom/android/server/wm/WindowState;
        //     26: getfield 248	com/android/server/wm/WindowState:mAppToken	Lcom/android/server/wm/AppWindowToken;
        //     29: iconst_0
        //     30: putfield 253	com/android/server/wm/AppWindowToken:allDrawn	Z
        //     33: aload_0
        //     34: getfield 124	com/android/server/wm/WindowStateAnimator:mService	Lcom/android/server/wm/WindowManagerService;
        //     37: aload_0
        //     38: getfield 126	com/android/server/wm/WindowStateAnimator:mWin	Lcom/android/server/wm/WindowState;
        //     41: invokevirtual 424	com/android/server/wm/WindowManagerService:makeWindowFreezingScreenIfNeededLocked	(Lcom/android/server/wm/WindowState;)V
        //     44: iconst_0
        //     45: istore_2
        //     46: aload_0
        //     47: getfield 126	com/android/server/wm/WindowStateAnimator:mWin	Lcom/android/server/wm/WindowState;
        //     50: getfield 148	com/android/server/wm/WindowState:mAttrs	Landroid/view/WindowManager$LayoutParams;
        //     53: astore_3
        //     54: sipush 8192
        //     57: aload_3
        //     58: getfield 153	android/view/WindowManager$LayoutParams:flags	I
        //     61: iand
        //     62: ifeq +9 -> 71
        //     65: iconst_0
        //     66: sipush 128
        //     69: ior
        //     70: istore_2
        //     71: aload_0
        //     72: getfield 126	com/android/server/wm/WindowStateAnimator:mWin	Lcom/android/server/wm/WindowState;
        //     75: getfield 427	com/android/server/wm/WindowState:mCompatFrame	Landroid/graphics/Rect;
        //     78: invokevirtual 312	android/graphics/Rect:width	()I
        //     81: istore 4
        //     83: aload_0
        //     84: getfield 126	com/android/server/wm/WindowStateAnimator:mWin	Lcom/android/server/wm/WindowState;
        //     87: getfield 427	com/android/server/wm/WindowState:mCompatFrame	Landroid/graphics/Rect;
        //     90: invokevirtual 315	android/graphics/Rect:height	()I
        //     93: istore 5
        //     95: sipush 16384
        //     98: aload_3
        //     99: getfield 153	android/view/WindowManager$LayoutParams:flags	I
        //     102: iand
        //     103: ifeq +21 -> 124
        //     106: aload_0
        //     107: getfield 126	com/android/server/wm/WindowStateAnimator:mWin	Lcom/android/server/wm/WindowState;
        //     110: getfield 430	com/android/server/wm/WindowState:mRequestedWidth	I
        //     113: istore 4
        //     115: aload_0
        //     116: getfield 126	com/android/server/wm/WindowStateAnimator:mWin	Lcom/android/server/wm/WindowState;
        //     119: getfield 433	com/android/server/wm/WindowState:mRequestedHeight	I
        //     122: istore 5
        //     124: iload 4
        //     126: ifgt +6 -> 132
        //     129: iconst_1
        //     130: istore 4
        //     132: iload 5
        //     134: ifgt +6 -> 140
        //     137: iconst_1
        //     138: istore 5
        //     140: aload_0
        //     141: iconst_0
        //     142: putfield 435	com/android/server/wm/WindowStateAnimator:mSurfaceShown	Z
        //     145: aload_0
        //     146: iconst_0
        //     147: putfield 437	com/android/server/wm/WindowStateAnimator:mSurfaceLayer	I
        //     150: aload_0
        //     151: fconst_0
        //     152: putfield 439	com/android/server/wm/WindowStateAnimator:mSurfaceAlpha	F
        //     155: aload_0
        //     156: fconst_0
        //     157: putfield 441	com/android/server/wm/WindowStateAnimator:mSurfaceX	F
        //     160: aload_0
        //     161: fconst_0
        //     162: putfield 443	com/android/server/wm/WindowStateAnimator:mSurfaceY	F
        //     165: aload_0
        //     166: iload 4
        //     168: i2f
        //     169: putfield 445	com/android/server/wm/WindowStateAnimator:mSurfaceW	F
        //     172: aload_0
        //     173: iload 5
        //     175: i2f
        //     176: putfield 447	com/android/server/wm/WindowStateAnimator:mSurfaceH	F
        //     179: aload_0
        //     180: getfield 126	com/android/server/wm/WindowStateAnimator:mWin	Lcom/android/server/wm/WindowState;
        //     183: getfield 450	com/android/server/wm/WindowState:mLastSystemDecorRect	Landroid/graphics/Rect;
        //     186: iconst_0
        //     187: iconst_0
        //     188: iconst_0
        //     189: iconst_0
        //     190: invokevirtual 453	android/graphics/Rect:set	(IIII)V
        //     193: ldc_w 454
        //     196: aload_3
        //     197: getfield 153	android/view/WindowManager$LayoutParams:flags	I
        //     200: iand
        //     201: ifeq +217 -> 418
        //     204: iconst_1
        //     205: istore 11
        //     207: goto +354 -> 561
        //     210: aload_3
        //     211: getfield 376	android/view/WindowManager$LayoutParams:format	I
        //     214: invokestatic 382	android/graphics/PixelFormat:formatHasAlpha	(I)Z
        //     217: ifne +9 -> 226
        //     220: iload_2
        //     221: sipush 1024
        //     224: ior
        //     225: istore_2
        //     226: aload_0
        //     227: new 456	android/view/Surface
        //     230: dup
        //     231: aload_0
        //     232: getfield 138	com/android/server/wm/WindowStateAnimator:mSession	Lcom/android/server/wm/Session;
        //     235: getfield 462	com/android/server/wm/Session:mSurfaceSession	Landroid/view/SurfaceSession;
        //     238: aload_0
        //     239: getfield 138	com/android/server/wm/WindowStateAnimator:mSession	Lcom/android/server/wm/Session;
        //     242: getfield 465	com/android/server/wm/Session:mPid	I
        //     245: aload_3
        //     246: invokevirtual 469	android/view/WindowManager$LayoutParams:getTitle	()Ljava/lang/CharSequence;
        //     249: invokevirtual 472	java/lang/Object:toString	()Ljava/lang/String;
        //     252: iconst_0
        //     253: iload 4
        //     255: iload 5
        //     257: iload 12
        //     259: iload_2
        //     260: invokespecial 475	android/view/Surface:<init>	(Landroid/view/SurfaceSession;ILjava/lang/String;IIIII)V
        //     263: putfield 420	com/android/server/wm/WindowStateAnimator:mSurface	Landroid/view/Surface;
        //     266: aload_0
        //     267: getfield 126	com/android/server/wm/WindowStateAnimator:mWin	Lcom/android/server/wm/WindowState;
        //     270: iconst_1
        //     271: putfield 478	com/android/server/wm/WindowState:mHasSurface	Z
        //     274: invokestatic 481	android/view/Surface:openTransaction	()V
        //     277: aload_0
        //     278: aload_0
        //     279: getfield 126	com/android/server/wm/WindowStateAnimator:mWin	Lcom/android/server/wm/WindowState;
        //     282: getfield 298	com/android/server/wm/WindowState:mFrame	Landroid/graphics/Rect;
        //     285: getfield 339	android/graphics/Rect:left	I
        //     288: aload_0
        //     289: getfield 126	com/android/server/wm/WindowStateAnimator:mWin	Lcom/android/server/wm/WindowState;
        //     292: getfield 342	com/android/server/wm/WindowState:mXOffset	I
        //     295: iadd
        //     296: i2f
        //     297: putfield 441	com/android/server/wm/WindowStateAnimator:mSurfaceX	F
        //     300: aload_0
        //     301: aload_0
        //     302: getfield 126	com/android/server/wm/WindowStateAnimator:mWin	Lcom/android/server/wm/WindowState;
        //     305: getfield 298	com/android/server/wm/WindowState:mFrame	Landroid/graphics/Rect;
        //     308: getfield 345	android/graphics/Rect:top	I
        //     311: aload_0
        //     312: getfield 126	com/android/server/wm/WindowStateAnimator:mWin	Lcom/android/server/wm/WindowState;
        //     315: getfield 348	com/android/server/wm/WindowState:mYOffset	I
        //     318: iadd
        //     319: i2f
        //     320: putfield 443	com/android/server/wm/WindowStateAnimator:mSurfaceY	F
        //     323: aload_0
        //     324: getfield 420	com/android/server/wm/WindowStateAnimator:mSurface	Landroid/view/Surface;
        //     327: aload_0
        //     328: getfield 441	com/android/server/wm/WindowStateAnimator:mSurfaceX	F
        //     331: aload_0
        //     332: getfield 443	com/android/server/wm/WindowStateAnimator:mSurfaceY	F
        //     335: invokevirtual 484	android/view/Surface:setPosition	(FF)V
        //     338: aload_0
        //     339: aload_0
        //     340: getfield 486	com/android/server/wm/WindowStateAnimator:mAnimLayer	I
        //     343: putfield 437	com/android/server/wm/WindowStateAnimator:mSurfaceLayer	I
        //     346: aload_0
        //     347: getfield 420	com/android/server/wm/WindowStateAnimator:mSurface	Landroid/view/Surface;
        //     350: aload_0
        //     351: getfield 486	com/android/server/wm/WindowStateAnimator:mAnimLayer	I
        //     354: invokevirtual 490	android/view/Surface:setLayer	(I)V
        //     357: aload_0
        //     358: getfield 420	com/android/server/wm/WindowStateAnimator:mSurface	Landroid/view/Surface;
        //     361: fconst_0
        //     362: invokevirtual 494	android/view/Surface:setAlpha	(F)V
        //     365: aload_0
        //     366: iconst_0
        //     367: putfield 435	com/android/server/wm/WindowStateAnimator:mSurfaceShown	Z
        //     370: aload_0
        //     371: getfield 420	com/android/server/wm/WindowStateAnimator:mSurface	Landroid/view/Surface;
        //     374: invokevirtual 497	android/view/Surface:hide	()V
        //     377: sipush 4096
        //     380: aload_0
        //     381: getfield 126	com/android/server/wm/WindowStateAnimator:mWin	Lcom/android/server/wm/WindowState;
        //     384: getfield 148	com/android/server/wm/WindowState:mAttrs	Landroid/view/WindowManager$LayoutParams;
        //     387: getfield 153	android/view/WindowManager$LayoutParams:flags	I
        //     390: iand
        //     391: ifeq +12 -> 403
        //     394: aload_0
        //     395: getfield 420	com/android/server/wm/WindowStateAnimator:mSurface	Landroid/view/Surface;
        //     398: iconst_4
        //     399: iconst_4
        //     400: invokevirtual 501	android/view/Surface:setFlags	(II)V
        //     403: aload_0
        //     404: iconst_1
        //     405: putfield 503	com/android/server/wm/WindowStateAnimator:mLastHidden	Z
        //     408: invokestatic 506	android/view/Surface:closeTransaction	()V
        //     411: aload_0
        //     412: getfield 420	com/android/server/wm/WindowStateAnimator:mSurface	Landroid/view/Surface;
        //     415: astore_1
        //     416: aload_1
        //     417: areturn
        //     418: iconst_0
        //     419: istore 11
        //     421: goto +140 -> 561
        //     424: aload_3
        //     425: getfield 376	android/view/WindowManager$LayoutParams:format	I
        //     428: istore 12
        //     430: goto -220 -> 210
        //     433: astore 8
        //     435: aload_0
        //     436: getfield 126	com/android/server/wm/WindowStateAnimator:mWin	Lcom/android/server/wm/WindowState;
        //     439: iconst_0
        //     440: putfield 478	com/android/server/wm/WindowState:mHasSurface	Z
        //     443: ldc 32
        //     445: ldc_w 508
        //     448: invokestatic 514	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     451: pop
        //     452: aload_0
        //     453: getfield 124	com/android/server/wm/WindowStateAnimator:mService	Lcom/android/server/wm/WindowManagerService;
        //     456: aload_0
        //     457: ldc_w 516
        //     460: iconst_1
        //     461: invokevirtual 520	com/android/server/wm/WindowManagerService:reclaimSomeSurfaceMemoryLocked	(Lcom/android/server/wm/WindowStateAnimator;Ljava/lang/String;Z)Z
        //     464: pop
        //     465: aload_0
        //     466: iconst_0
        //     467: putfield 244	com/android/server/wm/WindowStateAnimator:mDrawState	I
        //     470: aconst_null
        //     471: astore_1
        //     472: goto -56 -> 416
        //     475: astore 6
        //     477: aload_0
        //     478: getfield 126	com/android/server/wm/WindowStateAnimator:mWin	Lcom/android/server/wm/WindowState;
        //     481: iconst_0
        //     482: putfield 478	com/android/server/wm/WindowState:mHasSurface	Z
        //     485: ldc 32
        //     487: ldc_w 522
        //     490: aload 6
        //     492: invokestatic 526	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     495: pop
        //     496: aload_0
        //     497: iconst_0
        //     498: putfield 244	com/android/server/wm/WindowStateAnimator:mDrawState	I
        //     501: aconst_null
        //     502: astore_1
        //     503: goto -87 -> 416
        //     506: astore 14
        //     508: ldc 32
        //     510: new 528	java/lang/StringBuilder
        //     513: dup
        //     514: invokespecial 529	java/lang/StringBuilder:<init>	()V
        //     517: ldc_w 531
        //     520: invokevirtual 535	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     523: iload 4
        //     525: invokevirtual 538	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     528: invokevirtual 539	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     531: aload 14
        //     533: invokestatic 541	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     536: pop
        //     537: aload_0
        //     538: getfield 124	com/android/server/wm/WindowStateAnimator:mService	Lcom/android/server/wm/WindowManagerService;
        //     541: aload_0
        //     542: ldc_w 543
        //     545: iconst_1
        //     546: invokevirtual 520	com/android/server/wm/WindowManagerService:reclaimSomeSurfaceMemoryLocked	(Lcom/android/server/wm/WindowStateAnimator;Ljava/lang/String;Z)Z
        //     549: pop
        //     550: goto -147 -> 403
        //     553: astore 13
        //     555: invokestatic 506	android/view/Surface:closeTransaction	()V
        //     558: aload 13
        //     560: athrow
        //     561: iload 11
        //     563: ifeq -139 -> 424
        //     566: bipush 253
        //     568: istore 12
        //     570: goto -360 -> 210
        //
        // Exception table:
        //     from	to	target	type
        //     193	274	433	android/view/Surface$OutOfResourcesException
        //     424	430	433	android/view/Surface$OutOfResourcesException
        //     193	274	475	java/lang/Exception
        //     424	430	475	java/lang/Exception
        //     277	403	506	java/lang/RuntimeException
        //     277	403	553	finally
        //     403	408	553	finally
        //     508	550	553	finally
    }

    void destroyDeferredSurfaceLocked()
    {
        try
        {
            if (this.mPendingDestroySurface != null)
            {
                this.mPendingDestroySurface.destroy();
                this.mAnimator.hideWallpapersLocked(this.mWin);
            }
            this.mSurfaceDestroyDeferred = false;
            this.mPendingDestroySurface = null;
            return;
        }
        catch (RuntimeException localRuntimeException)
        {
            while (true)
                Slog.w("WindowStateAnimator", "Exception thrown when destroying Window " + this + " surface " + this.mPendingDestroySurface + " session " + this.mSession + ": " + localRuntimeException.toString());
        }
    }

    void destroySurfaceLocked()
    {
        if ((this.mWin.mAppToken != null) && (this.mWin == this.mWin.mAppToken.startingWindow))
            this.mWin.mAppToken.startingDisplayed = false;
        if (this.mSurface != null)
        {
            int i = this.mWin.mChildWindows.size();
            while (i > 0)
            {
                i--;
                ((WindowState)this.mWin.mChildWindows.get(i)).mAttachedHidden = true;
            }
        }
        try
        {
            if (this.mSurfaceDestroyDeferred)
                if ((this.mSurface != null) && (this.mPendingDestroySurface != this.mSurface))
                {
                    if (this.mPendingDestroySurface != null)
                        this.mPendingDestroySurface.destroy();
                    this.mPendingDestroySurface = this.mSurface;
                }
            while (true)
            {
                this.mAnimator.hideWallpapersLocked(this.mWin);
                this.mSurfaceShown = false;
                this.mSurface = null;
                this.mWin.mHasSurface = false;
                this.mDrawState = 0;
                return;
                this.mSurface.destroy();
            }
        }
        catch (RuntimeException localRuntimeException)
        {
            while (true)
                Slog.w("WindowStateAnimator", "Exception thrown when destroying Window " + this + " surface " + this.mSurface + " session " + this.mSession + ": " + localRuntimeException.toString());
        }
    }

    public void dump(PrintWriter paramPrintWriter, String paramString, boolean paramBoolean)
    {
        if ((this.mAnimating) || (this.mLocalAnimating) || (this.mAnimationIsEntrance) || (this.mAnimation != null))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("mAnimating=");
            paramPrintWriter.print(this.mAnimating);
            paramPrintWriter.print(" mLocalAnimating=");
            paramPrintWriter.print(this.mLocalAnimating);
            paramPrintWriter.print(" mAnimationIsEntrance=");
            paramPrintWriter.print(this.mAnimationIsEntrance);
            paramPrintWriter.print(" mAnimation=");
            paramPrintWriter.println(this.mAnimation);
        }
        if ((this.mHasTransformation) || (this.mHasLocalTransformation))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("XForm: has=");
            paramPrintWriter.print(this.mHasTransformation);
            paramPrintWriter.print(" hasLocal=");
            paramPrintWriter.print(this.mHasLocalTransformation);
            paramPrintWriter.print(" ");
            this.mTransformation.printShortString(paramPrintWriter);
            paramPrintWriter.println();
        }
        if (this.mSurface != null)
        {
            if (paramBoolean)
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("mSurface=");
                paramPrintWriter.println(this.mSurface);
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("mDrawState=");
                paramPrintWriter.print(drawStateToString(this.mDrawState));
                paramPrintWriter.print(" mLastHidden=");
                paramPrintWriter.println(this.mLastHidden);
            }
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("Surface: shown=");
            paramPrintWriter.print(this.mSurfaceShown);
            paramPrintWriter.print(" layer=");
            paramPrintWriter.print(this.mSurfaceLayer);
            paramPrintWriter.print(" alpha=");
            paramPrintWriter.print(this.mSurfaceAlpha);
            paramPrintWriter.print(" rect=(");
            paramPrintWriter.print(this.mSurfaceX);
            paramPrintWriter.print(",");
            paramPrintWriter.print(this.mSurfaceY);
            paramPrintWriter.print(") ");
            paramPrintWriter.print(this.mSurfaceW);
            paramPrintWriter.print(" x ");
            paramPrintWriter.println(this.mSurfaceH);
        }
        if (this.mPendingDestroySurface != null)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("mPendingDestroySurface=");
            paramPrintWriter.println(this.mPendingDestroySurface);
        }
        if ((this.mSurfaceResized) || (this.mSurfaceDestroyDeferred))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("mSurfaceResized=");
            paramPrintWriter.print(this.mSurfaceResized);
            paramPrintWriter.print(" mSurfaceDestroyDeferred=");
            paramPrintWriter.println(this.mSurfaceDestroyDeferred);
        }
        if ((this.mShownAlpha != 1.0F) || (this.mAlpha != 1.0F) || (this.mLastAlpha != 1.0F))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("mShownAlpha=");
            paramPrintWriter.print(this.mShownAlpha);
            paramPrintWriter.print(" mAlpha=");
            paramPrintWriter.print(this.mAlpha);
            paramPrintWriter.print(" mLastAlpha=");
            paramPrintWriter.println(this.mLastAlpha);
        }
        if ((this.mHaveMatrix) || (this.mWin.mGlobalScale != 1.0F))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("mGlobalScale=");
            paramPrintWriter.print(this.mWin.mGlobalScale);
            paramPrintWriter.print(" mDsDx=");
            paramPrintWriter.print(this.mDsDx);
            paramPrintWriter.print(" mDtDx=");
            paramPrintWriter.print(this.mDtDx);
            paramPrintWriter.print(" mDsDy=");
            paramPrintWriter.print(this.mDsDy);
            paramPrintWriter.print(" mDtDy=");
            paramPrintWriter.println(this.mDtDy);
        }
    }

    boolean finishDrawingLocked()
    {
        int i = 1;
        if (this.mDrawState == i)
            this.mDrawState = 2;
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    void finishExit()
    {
        int i = this.mWin.mChildWindows.size();
        for (int j = 0; j < i; j++)
            ((WindowState)this.mWin.mChildWindows.get(j)).mWinAnimator.finishExit();
        if (!this.mWin.mExiting);
        while (true)
        {
            return;
            if (!isWindowAnimating())
            {
                if (this.mSurface != null)
                {
                    this.mService.mDestroySurface.add(this.mWin);
                    this.mWin.mDestroying = true;
                    hide();
                }
                this.mWin.mExiting = false;
                if (this.mWin.mRemoveOnExit)
                {
                    this.mService.mPendingRemove.add(this.mWin);
                    this.mWin.mRemoveOnExit = false;
                }
                this.mAnimator.hideWallpapersLocked(this.mWin);
            }
        }
    }

    void hide()
    {
        if (!this.mLastHidden)
        {
            this.mLastHidden = true;
            if (this.mSurface != null)
                this.mSurfaceShown = false;
        }
        try
        {
            this.mSurface.hide();
            return;
        }
        catch (RuntimeException localRuntimeException)
        {
            while (true)
                Slog.w("WindowStateAnimator", "Exception hiding surface in " + this.mWin);
        }
    }

    boolean isAnimating()
    {
        WindowState localWindowState = this.mAttachedWindow;
        AppWindowToken localAppWindowToken = this.mWin.mAppToken;
        if ((this.mAnimation != null) || ((localWindowState != null) && (localWindowState.mWinAnimator.mAnimation != null)) || ((localAppWindowToken != null) && ((localAppWindowToken.mAppAnimator.animation != null) || (localAppWindowToken.inPendingTransaction))));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean isDummyAnimation()
    {
        AppWindowToken localAppWindowToken = this.mWin.mAppToken;
        if ((localAppWindowToken != null) && (localAppWindowToken.mAppAnimator.animation == AppWindowAnimator.sDummyAnimation));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean isWindowAnimating()
    {
        if (this.mAnimation != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean performShowLocked()
    {
        boolean bool = true;
        if ((this.mDrawState == 3) && (this.mWin.isReadyForDisplayIgnoringKeyguard()))
        {
            this.mService.enableScreenIfNeededLocked();
            applyEnterAnimationLocked();
            this.mLastAlpha = -1.0F;
            this.mDrawState = 4;
            this.mService.scheduleAnimationLocked();
            int i = this.mWin.mChildWindows.size();
            while (i > 0)
            {
                i--;
                WindowState localWindowState = (WindowState)this.mWin.mChildWindows.get(i);
                if (localWindowState.mAttachedHidden)
                {
                    localWindowState.mAttachedHidden = false;
                    if (localWindowState.mWinAnimator.mSurface != null)
                    {
                        localWindowState.mWinAnimator.performShowLocked();
                        this.mService.mLayoutNeeded = bool;
                    }
                }
            }
            if ((this.mWin.mAttrs.type != 3) && (this.mWin.mAppToken != null))
            {
                this.mWin.mAppToken.firstWindowDrawn = bool;
                if (this.mWin.mAppToken.startingData != null)
                {
                    clearAnimation();
                    this.mService.mFinishedStarting.add(this.mWin.mAppToken);
                    this.mService.mH.sendEmptyMessage(7);
                }
                this.mWin.mAppToken.updateReportedVisibilityLocked();
            }
        }
        while (true)
        {
            return bool;
            bool = false;
        }
    }

    public void prepareSurfaceLocked(boolean paramBoolean)
    {
        WindowState localWindowState = this.mWin;
        if (this.mSurface == null)
        {
            if (localWindowState.mOrientationChanging)
                localWindowState.mOrientationChanging = false;
            return;
        }
        int i = 0;
        computeShownFrameLocked();
        setSurfaceBoundaries(paramBoolean);
        if ((this.mWin.mIsWallpaper) && (!this.mWin.mWallpaperVisible))
        {
            hide();
            label60: if (i == 0)
                break label527;
            if (localWindowState.mOrientationChanging)
            {
                if (localWindowState.isDrawnLw())
                    break label529;
                WindowAnimator localWindowAnimator = this.mAnimator;
                localWindowAnimator.mBulkUpdateParams = (0x8 | localWindowAnimator.mBulkUpdateParams);
            }
        }
        while (true)
        {
            localWindowState.mToken.hasVisible = true;
            break;
            if ((localWindowState.mAttachedHidden) || (!localWindowState.isReadyForDisplay()))
            {
                hide();
                this.mAnimator.hideWallpapersLocked(localWindowState);
                if (!localWindowState.mOrientationChanging)
                    break label60;
                localWindowState.mOrientationChanging = false;
                break label60;
            }
            if ((this.mLastLayer != this.mAnimLayer) || (this.mLastAlpha != this.mShownAlpha) || (this.mLastDsDx != this.mDsDx) || (this.mLastDtDx != this.mDtDx) || (this.mLastDsDy != this.mDsDy) || (this.mLastDtDy != this.mDtDy) || (localWindowState.mLastHScale != localWindowState.mHScale) || (localWindowState.mLastVScale != localWindowState.mVScale) || (this.mLastHidden))
            {
                i = 1;
                this.mLastAlpha = this.mShownAlpha;
                this.mLastLayer = this.mAnimLayer;
                this.mLastDsDx = this.mDsDx;
                this.mLastDtDx = this.mDtDx;
                this.mLastDsDy = this.mDsDy;
                this.mLastDtDy = this.mDtDy;
                localWindowState.mLastHScale = localWindowState.mHScale;
                localWindowState.mLastVScale = localWindowState.mVScale;
                if (this.mSurface == null)
                    break label60;
                while (true)
                {
                    try
                    {
                        this.mSurfaceAlpha = this.mShownAlpha;
                        this.mSurface.setAlpha(this.mShownAlpha);
                        this.mSurfaceLayer = this.mAnimLayer;
                        this.mSurface.setLayer(this.mAnimLayer);
                        this.mSurface.setMatrix(this.mDsDx * localWindowState.mHScale, this.mDtDx * localWindowState.mVScale, this.mDsDy * localWindowState.mHScale, this.mDtDy * localWindowState.mVScale);
                        if ((this.mLastHidden) && (this.mDrawState == 4))
                        {
                            if (!showSurfaceRobustlyLocked())
                                break label516;
                            this.mLastHidden = false;
                            if (localWindowState.mIsWallpaper)
                                this.mService.dispatchWallpaperVisibility(localWindowState, true);
                        }
                        if (this.mSurface == null)
                            break;
                        localWindowState.mToken.hasVisible = true;
                    }
                    catch (RuntimeException localRuntimeException)
                    {
                        Slog.w("WindowStateAnimator", "Error updating surface in " + localWindowState, localRuntimeException);
                    }
                    if (paramBoolean)
                        break;
                    this.mService.reclaimSomeSurfaceMemoryLocked(this, "update", true);
                    break;
                    label516: localWindowState.mOrientationChanging = false;
                }
            }
            i = 1;
            break label60;
            label527: break;
            label529: localWindowState.mOrientationChanging = false;
        }
    }

    public void setAnimation(Animation paramAnimation)
    {
        this.mAnimating = false;
        this.mLocalAnimating = false;
        this.mAnimation = paramAnimation;
        this.mAnimation.restrictDuration(10000L);
        this.mAnimation.scaleCurrentDuration(this.mService.mWindowAnimationScale);
        this.mTransformation.clear();
        Transformation localTransformation = this.mTransformation;
        if (this.mLastHidden);
        for (float f = 0.0F; ; f = 1.0F)
        {
            localTransformation.setAlpha(f);
            this.mHasLocalTransformation = true;
            return;
        }
    }

    void setSurfaceBoundaries(boolean paramBoolean)
    {
        WindowState localWindowState = this.mWin;
        int i;
        int j;
        if ((0x4000 & localWindowState.mAttrs.flags) != 0)
        {
            i = localWindowState.mRequestedWidth;
            j = localWindowState.mRequestedHeight;
        }
        while (true)
        {
            if (i < 1)
                i = 1;
            if (j < 1)
                j = 1;
            int k;
            label70: float f1;
            float f2;
            if ((this.mSurfaceW != i) || (this.mSurfaceH != j))
            {
                k = 1;
                if (k != 0)
                {
                    this.mSurfaceW = i;
                    this.mSurfaceH = j;
                }
                f1 = localWindowState.mShownFrame.left;
                f2 = localWindowState.mShownFrame.top;
                if ((this.mSurfaceX == f1) && (this.mSurfaceY == f2));
            }
            try
            {
                this.mSurfaceX = f1;
                this.mSurfaceY = f2;
                this.mSurface.setPosition(f1, f2);
                label149: if (k == 0);
            }
            catch (RuntimeException localRuntimeException1)
            {
                try
                {
                    this.mSurfaceResized = true;
                    this.mSurface.setSize(i, j);
                    WindowAnimator localWindowAnimator1 = this.mAnimator;
                    localWindowAnimator1.mPendingLayoutChanges = (0x4 | localWindowAnimator1.mPendingLayoutChanges);
                    WindowAnimator localWindowAnimator2;
                    if ((0x2 & localWindowState.mAttrs.flags) != 0)
                    {
                        localWindowAnimator2 = this.mAnimator;
                        if (!localWindowState.mExiting)
                            break label347;
                    }
                    label347: for (float f3 = 0.0F; ; f3 = localWindowState.mAttrs.dimAmount)
                    {
                        localWindowAnimator2.startDimming(this, f3, this.mService.mAppDisplayWidth, this.mService.mAppDisplayHeight);
                        updateSurfaceWindowCrop(paramBoolean);
                        return;
                        i = localWindowState.mCompatFrame.width();
                        j = localWindowState.mCompatFrame.height();
                        break;
                        k = 0;
                        break label70;
                        localRuntimeException1 = localRuntimeException1;
                        Slog.w("WindowStateAnimator", "Error positioning surface of " + localWindowState + " pos=(" + f1 + "," + f2 + ")", localRuntimeException1);
                        if (paramBoolean)
                            break label149;
                        this.mService.reclaimSomeSurfaceMemoryLocked(this, "position", true);
                        break label149;
                    }
                }
                catch (RuntimeException localRuntimeException2)
                {
                    while (true)
                    {
                        Slog.e("WindowStateAnimator", "Error resizing surface of " + localWindowState + " size=(" + i + "x" + j + ")", localRuntimeException2);
                        if (!paramBoolean)
                            this.mService.reclaimSomeSurfaceMemoryLocked(this, "size", true);
                    }
                }
            }
        }
    }

    void setTransparentRegionHint(Region paramRegion)
    {
        if (this.mSurface == null)
            Slog.w("WindowStateAnimator", "setTransparentRegionHint: null mSurface after mHasSurface true");
        while (true)
        {
            return;
            Surface.openTransaction();
            try
            {
                this.mSurface.setTransparentRegionHint(paramRegion);
                Surface.closeTransaction();
            }
            finally
            {
                Surface.closeTransaction();
            }
        }
    }

    void setWallpaperOffset(int paramInt1, int paramInt2)
    {
        this.mSurfaceX = paramInt1;
        this.mSurfaceY = paramInt2;
        if (this.mAnimating);
        while (true)
        {
            return;
            Surface.openTransaction();
            try
            {
                this.mSurface.setPosition(paramInt1 + this.mWin.mFrame.left, paramInt2 + this.mWin.mFrame.top);
                updateSurfaceWindowCrop(false);
                Surface.closeTransaction();
            }
            catch (RuntimeException localRuntimeException)
            {
                Slog.w("WindowStateAnimator", "Error positioning surface of " + this.mWin + " pos=(" + paramInt1 + "," + paramInt2 + ")", localRuntimeException);
                Surface.closeTransaction();
            }
            finally
            {
                Surface.closeTransaction();
            }
        }
    }

    boolean showSurfaceRobustlyLocked()
    {
        boolean bool = true;
        try
        {
            if (this.mSurface != null)
            {
                this.mSurfaceShown = true;
                this.mSurface.show();
                if (this.mWin.mTurnOnScreen)
                {
                    this.mWin.mTurnOnScreen = false;
                    WindowAnimator localWindowAnimator = this.mAnimator;
                    localWindowAnimator.mBulkUpdateParams = (0x10 | localWindowAnimator.mBulkUpdateParams);
                }
            }
            return bool;
        }
        catch (RuntimeException localRuntimeException)
        {
            while (true)
            {
                Slog.w("WindowStateAnimator", "Failure showing surface " + this.mSurface + " in " + this.mWin, localRuntimeException);
                this.mService.reclaimSomeSurfaceMemoryLocked(this, "show", bool);
                bool = false;
            }
        }
    }

    boolean stepAnimationLocked(long paramLong)
    {
        boolean bool = true;
        this.mWasAnimating = this.mAnimating;
        if (this.mService.okToDisplay())
        {
            if ((this.mWin.isDrawnLw()) && (this.mAnimation != null))
            {
                this.mHasTransformation = bool;
                this.mHasLocalTransformation = bool;
                if (!this.mLocalAnimating)
                {
                    this.mAnimation.initialize(this.mWin.mFrame.width(), this.mWin.mFrame.height(), this.mAnimDw, this.mAnimDh);
                    this.mAnimDw = this.mService.mAppDisplayWidth;
                    this.mAnimDh = this.mService.mAppDisplayHeight;
                    this.mAnimation.setStartTime(paramLong);
                    this.mLocalAnimating = bool;
                    this.mAnimating = bool;
                }
                if ((this.mAnimation == null) || (!this.mLocalAnimating) || (!stepAnimation(paramLong)));
            }
            while (true)
            {
                return bool;
                this.mHasLocalTransformation = false;
                if (((this.mLocalAnimating) && (!this.mAnimationIsEntrance)) || (this.mWin.mAppToken == null) || (this.mWin.mAppToken.mAppAnimator.animation == null))
                    break;
                this.mAnimating = bool;
                this.mHasTransformation = bool;
                this.mTransformation.clear();
                bool = false;
            }
            if (this.mHasTransformation)
                this.mAnimating = bool;
        }
        while (true)
        {
            if ((this.mAnimating) || (this.mLocalAnimating))
                break label281;
            bool = false;
            break;
            if (isAnimating())
            {
                this.mAnimating = bool;
                continue;
                if (this.mAnimation != null)
                    this.mAnimating = bool;
            }
        }
        label281: this.mAnimating = false;
        this.mLocalAnimating = false;
        if (this.mAnimation != null)
        {
            this.mAnimation.cancel();
            this.mAnimation = null;
        }
        if (this.mAnimator.mWindowDetachedWallpaper == this.mWin)
            this.mAnimator.mWindowDetachedWallpaper = null;
        this.mAnimLayer = this.mWin.mLayer;
        if (this.mWin.mIsImWindow)
            this.mAnimLayer += this.mService.mInputMethodAnimLayerAdjustment;
        while (true)
        {
            this.mHasTransformation = false;
            this.mHasLocalTransformation = false;
            if (this.mWin.mPolicyVisibility != this.mWin.mPolicyVisibilityAfterAnim)
            {
                this.mWin.mPolicyVisibility = this.mWin.mPolicyVisibilityAfterAnim;
                this.mService.mLayoutNeeded = bool;
                if (!this.mWin.mPolicyVisibility)
                {
                    if (this.mService.mCurrentFocus == this.mWin)
                        this.mService.mFocusMayChange = bool;
                    this.mService.enableScreenIfNeededLocked();
                }
            }
            this.mTransformation.clear();
            if ((this.mDrawState == 4) && (this.mWin.mAttrs.type == 3) && (this.mWin.mAppToken != null) && (this.mWin.mAppToken.firstWindowDrawn) && (this.mWin.mAppToken.startingData != null))
            {
                this.mService.mFinishedStarting.add(this.mWin.mAppToken);
                this.mService.mH.sendEmptyMessage(7);
            }
            finishExit();
            WindowAnimator localWindowAnimator = this.mAnimator;
            localWindowAnimator.mPendingLayoutChanges = (0x8 | localWindowAnimator.mPendingLayoutChanges);
            this.mService.debugLayoutRepeats("WindowStateAnimator", this.mAnimator.mPendingLayoutChanges);
            if (this.mWin.mAppToken != null)
                this.mWin.mAppToken.updateReportedVisibilityLocked();
            bool = false;
            break;
            if (this.mWin.mIsWallpaper)
                this.mAnimLayer += this.mService.mWallpaperAnimLayerAdjustment;
        }
    }

    public String toString()
    {
        StringBuffer localStringBuffer = new StringBuffer("WindowStateAnimator (");
        localStringBuffer.append(this.mWin.mLastTitle + "): ");
        localStringBuffer.append("mSurface " + this.mSurface);
        localStringBuffer.append(", mAnimation " + this.mAnimation);
        return localStringBuffer.toString();
    }

    void updateSurfaceWindowCrop(boolean paramBoolean)
    {
        WindowState localWindowState = this.mWin;
        if ((0x4000 & localWindowState.mAttrs.flags) != 0)
            localWindowState.mSystemDecorRect.set(0, 0, localWindowState.mRequestedWidth, localWindowState.mRequestedHeight);
        while (true)
        {
            if (!localWindowState.mSystemDecorRect.equals(localWindowState.mLastSystemDecorRect))
                localWindowState.mLastSystemDecorRect.set(localWindowState.mSystemDecorRect);
            try
            {
                this.mSurface.setWindowCrop(localWindowState.mSystemDecorRect);
                return;
                if (localWindowState.mLayer >= this.mService.mSystemDecorLayer)
                {
                    localWindowState.mSystemDecorRect.set(0, 0, localWindowState.mCompatFrame.width(), localWindowState.mCompatFrame.height());
                    continue;
                }
                Rect localRect = this.mService.mSystemDecorRect;
                int i = localWindowState.mXOffset + localWindowState.mFrame.left;
                int j = localWindowState.mYOffset + localWindowState.mFrame.top;
                localWindowState.mSystemDecorRect.set(0, 0, localWindowState.mFrame.width(), localWindowState.mFrame.height());
                localWindowState.mSystemDecorRect.intersect(localRect.left - i, localRect.top - j, localRect.right - i, localRect.bottom - j);
                if ((!localWindowState.mEnforceSizeCompat) || (localWindowState.mInvGlobalScale == 1.0F))
                    continue;
                float f = localWindowState.mInvGlobalScale;
                localWindowState.mSystemDecorRect.left = ((int)(f * localWindowState.mSystemDecorRect.left - 0.5F));
                localWindowState.mSystemDecorRect.top = ((int)(f * localWindowState.mSystemDecorRect.top - 0.5F));
                localWindowState.mSystemDecorRect.right = ((int)(f * (1 + localWindowState.mSystemDecorRect.right) - 0.5F));
                localWindowState.mSystemDecorRect.bottom = ((int)(f * (1 + localWindowState.mSystemDecorRect.bottom) - 0.5F));
            }
            catch (RuntimeException localRuntimeException)
            {
                while (true)
                {
                    Slog.w("WindowStateAnimator", "Error setting crop surface of " + localWindowState + " crop=" + localWindowState.mSystemDecorRect.toShortString(), localRuntimeException);
                    if (!paramBoolean)
                        this.mService.reclaimSomeSurfaceMemoryLocked(this, "crop", true);
                }
            }
        }
    }

    static class SurfaceTrace extends Surface
    {
        private static final String SURFACE_TAG = "SurfaceTrace";
        static final ArrayList<SurfaceTrace> sSurfaces = new ArrayList();
        private int mLayer;
        private String mName = "Not named";
        private final PointF mPosition = new PointF();
        private boolean mShown = false;
        private final Point mSize = new Point();
        private float mSurfaceTraceAlpha = 0.0F;
        private final Rect mWindowCrop = new Rect();

        public SurfaceTrace(SurfaceSession paramSurfaceSession, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
            throws Surface.OutOfResourcesException
        {
            super(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6);
            this.mSize.set(paramInt3, paramInt4);
            Slog.v("SurfaceTrace", "ctor: " + this + ". Called by " + Debug.getCallers(3));
        }

        public SurfaceTrace(SurfaceSession paramSurfaceSession, int paramInt1, String paramString, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
            throws Surface.OutOfResourcesException
        {
            super(paramInt1, paramString, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6);
            this.mName = paramString;
            this.mSize.set(paramInt3, paramInt4);
            Slog.v("SurfaceTrace", "ctor: " + this + ". Called by " + Debug.getCallers(3));
        }

        static void dumpAllSurfaces()
        {
            int i = sSurfaces.size();
            for (int j = 0; j < i; j++)
                Slog.i("WindowStateAnimator", "SurfaceDump: " + sSurfaces.get(j));
        }

        public void destroy()
        {
            super.destroy();
            Slog.v("SurfaceTrace", "destroy: " + this + ". Called by " + Debug.getCallers(3));
            sSurfaces.remove(this);
        }

        public void hide()
        {
            super.hide();
            this.mShown = false;
            Slog.v("SurfaceTrace", "hide: " + this + ". Called by " + Debug.getCallers(3));
        }

        public void release()
        {
            super.release();
            Slog.v("SurfaceTrace", "release: " + this + ". Called by " + Debug.getCallers(3));
            sSurfaces.remove(this);
        }

        public void setAlpha(float paramFloat)
        {
            super.setAlpha(paramFloat);
            this.mSurfaceTraceAlpha = paramFloat;
            Slog.v("SurfaceTrace", "setAlpha: " + this + ". Called by " + Debug.getCallers(3));
        }

        public void setLayer(int paramInt)
        {
            super.setLayer(paramInt);
            this.mLayer = paramInt;
            Slog.v("SurfaceTrace", "setLayer: " + this + ". Called by " + Debug.getCallers(3));
            sSurfaces.remove(this);
            for (int i = -1 + sSurfaces.size(); ; i--)
                if ((i < 0) || (((SurfaceTrace)sSurfaces.get(i)).mLayer < paramInt))
                {
                    sSurfaces.add(i + 1, this);
                    return;
                }
        }

        public void setPosition(float paramFloat1, float paramFloat2)
        {
            super.setPosition(paramFloat1, paramFloat2);
            this.mPosition.set(paramFloat1, paramFloat2);
            Slog.v("SurfaceTrace", "setPosition: " + this + ". Called by " + Debug.getCallers(3));
        }

        public void setSize(int paramInt1, int paramInt2)
        {
            super.setSize(paramInt1, paramInt2);
            this.mSize.set(paramInt1, paramInt2);
            Slog.v("SurfaceTrace", "setSize: " + this + ". Called by " + Debug.getCallers(3));
        }

        public void setWindowCrop(Rect paramRect)
        {
            super.setWindowCrop(paramRect);
            if (paramRect != null)
                this.mWindowCrop.set(paramRect);
            Slog.v("SurfaceTrace", "setWindowCrop: " + this + ". Called by " + Debug.getCallers(3));
        }

        public void show()
        {
            super.show();
            this.mShown = true;
            Slog.v("SurfaceTrace", "show: " + this + ". Called by " + Debug.getCallers(3));
        }

        public String toString()
        {
            return "Surface " + Integer.toHexString(System.identityHashCode(this)) + " " + this.mName + ": shown=" + this.mShown + " layer=" + this.mLayer + " alpha=" + this.mSurfaceTraceAlpha + " " + this.mPosition.x + "," + this.mPosition.y + " " + this.mSize.x + "x" + this.mSize.y + " crop=" + this.mWindowCrop.toShortString();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.wm.WindowStateAnimator
 * JD-Core Version:        0.6.2
 */