package com.android.server.wm;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Region.Op;
import android.view.Display;
import android.view.Surface;
import android.view.Surface.OutOfResourcesException;
import android.view.SurfaceSession;

class StrictModeFlash
{
    private static final String TAG = "StrictModeFlash";
    boolean mDrawNeeded;
    int mLastDH;
    int mLastDW;
    Surface mSurface;
    final int mThickness = 20;

    public StrictModeFlash(Display paramDisplay, SurfaceSession paramSurfaceSession)
    {
        try
        {
            this.mSurface = new Surface(paramSurfaceSession, 0, "StrictModeFlash", -1, 1, 1, -3, 0);
            this.mSurface.setLayer(1010000);
            this.mSurface.setPosition(0, 0);
            this.mDrawNeeded = true;
            label55: return;
        }
        catch (Surface.OutOfResourcesException localOutOfResourcesException)
        {
            break label55;
        }
    }

    private void drawIfNeeded()
    {
        if (!this.mDrawNeeded);
        while (true)
        {
            return;
            this.mDrawNeeded = false;
            int i = this.mLastDW;
            int j = this.mLastDH;
            Rect localRect = new Rect(0, 0, i, j);
            Object localObject = null;
            try
            {
                Canvas localCanvas = this.mSurface.lockCanvas(localRect);
                localObject = localCanvas;
                label52: if (localObject == null)
                    continue;
                localObject.clipRect(new Rect(0, 0, i, 20), Region.Op.REPLACE);
                localObject.drawColor(-65536);
                localObject.clipRect(new Rect(0, 0, 20, j), Region.Op.REPLACE);
                localObject.drawColor(-65536);
                localObject.clipRect(new Rect(i - 20, 0, i, j), Region.Op.REPLACE);
                localObject.drawColor(-65536);
                localObject.clipRect(new Rect(0, j - 20, i, j), Region.Op.REPLACE);
                localObject.drawColor(-65536);
                this.mSurface.unlockCanvasAndPost(localObject);
            }
            catch (Surface.OutOfResourcesException localOutOfResourcesException)
            {
                break label52;
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
                break label52;
            }
        }
    }

    void positionSurface(int paramInt1, int paramInt2)
    {
        if ((this.mLastDW == paramInt1) && (this.mLastDH == paramInt2));
        while (true)
        {
            return;
            this.mLastDW = paramInt1;
            this.mLastDH = paramInt2;
            this.mSurface.setSize(paramInt1, paramInt2);
            this.mDrawNeeded = true;
        }
    }

    public void setVisibility(boolean paramBoolean)
    {
        if (this.mSurface == null);
        while (true)
        {
            return;
            drawIfNeeded();
            if (paramBoolean)
                this.mSurface.show();
            else
                this.mSurface.hide();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.wm.StrictModeFlash
 * JD-Core Version:        0.6.2
 */