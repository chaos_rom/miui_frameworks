package com.android.server.wm;

import android.util.Slog;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class ViewServer
    implements Runnable
{
    private static final String COMMAND_PROTOCOL_VERSION = "PROTOCOL";
    private static final String COMMAND_SERVER_VERSION = "SERVER";
    private static final String COMMAND_WINDOW_MANAGER_AUTOLIST = "AUTOLIST";
    private static final String COMMAND_WINDOW_MANAGER_GET_FOCUS = "GET_FOCUS";
    private static final String COMMAND_WINDOW_MANAGER_LIST = "LIST";
    private static final String LOG_TAG = "ViewServer";
    private static final String VALUE_PROTOCOL_VERSION = "4";
    private static final String VALUE_SERVER_VERSION = "4";
    public static final int VIEW_SERVER_DEFAULT_PORT = 4939;
    private static final int VIEW_SERVER_MAX_CONNECTIONS = 10;
    private final int mPort;
    private ServerSocket mServer;
    private Thread mThread;
    private ExecutorService mThreadPool;
    private final WindowManagerService mWindowManager;

    ViewServer(WindowManagerService paramWindowManagerService, int paramInt)
    {
        this.mWindowManager = paramWindowManagerService;
        this.mPort = paramInt;
    }

    // ERROR //
    private static boolean writeValue(Socket paramSocket, String paramString)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: new 68	java/io/BufferedWriter
        //     5: dup
        //     6: new 70	java/io/OutputStreamWriter
        //     9: dup
        //     10: aload_0
        //     11: invokevirtual 76	java/net/Socket:getOutputStream	()Ljava/io/OutputStream;
        //     14: invokespecial 79	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;)V
        //     17: sipush 8192
        //     20: invokespecial 82	java/io/BufferedWriter:<init>	(Ljava/io/Writer;I)V
        //     23: astore_3
        //     24: aload_3
        //     25: aload_1
        //     26: invokevirtual 86	java/io/BufferedWriter:write	(Ljava/lang/String;)V
        //     29: aload_3
        //     30: ldc 88
        //     32: invokevirtual 86	java/io/BufferedWriter:write	(Ljava/lang/String;)V
        //     35: aload_3
        //     36: invokevirtual 91	java/io/BufferedWriter:flush	()V
        //     39: iconst_1
        //     40: istore 5
        //     42: aload_3
        //     43: ifnull +74 -> 117
        //     46: aload_3
        //     47: invokevirtual 94	java/io/BufferedWriter:close	()V
        //     50: iload 5
        //     52: ireturn
        //     53: astore 9
        //     55: iconst_0
        //     56: istore 5
        //     58: goto -8 -> 50
        //     61: astore 10
        //     63: iconst_0
        //     64: istore 5
        //     66: aload_2
        //     67: ifnull -17 -> 50
        //     70: aload_2
        //     71: invokevirtual 94	java/io/BufferedWriter:close	()V
        //     74: goto -24 -> 50
        //     77: astore 6
        //     79: iconst_0
        //     80: istore 5
        //     82: goto -32 -> 50
        //     85: astore 7
        //     87: aload_2
        //     88: ifnull +7 -> 95
        //     91: aload_2
        //     92: invokevirtual 94	java/io/BufferedWriter:close	()V
        //     95: aload 7
        //     97: athrow
        //     98: astore 8
        //     100: goto -5 -> 95
        //     103: astore 7
        //     105: aload_3
        //     106: astore_2
        //     107: goto -20 -> 87
        //     110: astore 4
        //     112: aload_3
        //     113: astore_2
        //     114: goto -51 -> 63
        //     117: goto -67 -> 50
        //
        // Exception table:
        //     from	to	target	type
        //     46	50	53	java/io/IOException
        //     2	24	61	java/lang/Exception
        //     70	74	77	java/io/IOException
        //     2	24	85	finally
        //     91	95	98	java/io/IOException
        //     24	39	103	finally
        //     24	39	110	java/lang/Exception
    }

    boolean isRunning()
    {
        if ((this.mThread != null) && (this.mThread.isAlive()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void run()
    {
        while (Thread.currentThread() == this.mThread)
        {
            Socket localSocket;
            try
            {
                localSocket = this.mServer.accept();
                if (this.mThreadPool == null)
                    break label60;
                this.mThreadPool.submit(new ViewServerWorker(localSocket));
            }
            catch (Exception localException)
            {
                Slog.w("ViewServer", "Connection error: ", localException);
            }
            continue;
            try
            {
                label60: localSocket.close();
            }
            catch (IOException localIOException)
            {
                localIOException.printStackTrace();
            }
        }
    }

    boolean start()
        throws IOException
    {
        if (this.mThread != null);
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            this.mServer = new ServerSocket(this.mPort, 10, InetAddress.getLocalHost());
            this.mThread = new Thread(this, "Remote View Server [port=" + this.mPort + "]");
            this.mThreadPool = Executors.newFixedThreadPool(10);
            this.mThread.start();
        }
    }

    boolean stop()
    {
        if (this.mThread != null)
        {
            this.mThread.interrupt();
            if (this.mThreadPool == null);
        }
        while (true)
        {
            try
            {
                this.mThreadPool.shutdownNow();
                this.mThreadPool = null;
                this.mThread = null;
            }
            catch (SecurityException localSecurityException)
            {
                try
                {
                    this.mServer.close();
                    this.mServer = null;
                    bool = true;
                    return bool;
                    localSecurityException = localSecurityException;
                    Slog.w("ViewServer", "Could not stop all view server threads");
                }
                catch (IOException localIOException)
                {
                    Slog.w("ViewServer", "Could not close the view server");
                }
            }
            boolean bool = false;
        }
    }

    class ViewServerWorker
        implements Runnable, WindowManagerService.WindowChangeListener
    {
        private Socket mClient;
        private boolean mNeedFocusedWindowUpdate;
        private boolean mNeedWindowListUpdate;

        public ViewServerWorker(Socket arg2)
        {
            Object localObject;
            this.mClient = localObject;
            this.mNeedWindowListUpdate = false;
            this.mNeedFocusedWindowUpdate = false;
        }

        // ERROR //
        private boolean windowManagerAutolistLoop()
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 22	com/android/server/wm/ViewServer$ViewServerWorker:this$0	Lcom/android/server/wm/ViewServer;
            //     4: invokestatic 41	com/android/server/wm/ViewServer:access$100	(Lcom/android/server/wm/ViewServer;)Lcom/android/server/wm/WindowManagerService;
            //     7: aload_0
            //     8: invokevirtual 47	com/android/server/wm/WindowManagerService:addWindowChangeListener	(Lcom/android/server/wm/WindowManagerService$WindowChangeListener;)V
            //     11: aconst_null
            //     12: astore_1
            //     13: new 49	java/io/BufferedWriter
            //     16: dup
            //     17: new 51	java/io/OutputStreamWriter
            //     20: dup
            //     21: aload_0
            //     22: getfield 27	com/android/server/wm/ViewServer$ViewServerWorker:mClient	Ljava/net/Socket;
            //     25: invokevirtual 57	java/net/Socket:getOutputStream	()Ljava/io/OutputStream;
            //     28: invokespecial 60	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;)V
            //     31: invokespecial 63	java/io/BufferedWriter:<init>	(Ljava/io/Writer;)V
            //     34: astore_2
            //     35: invokestatic 68	java/lang/Thread:interrupted	()Z
            //     38: ifne +153 -> 191
            //     41: iconst_0
            //     42: istore 8
            //     44: iconst_0
            //     45: istore 9
            //     47: aload_0
            //     48: monitorenter
            //     49: aload_0
            //     50: getfield 29	com/android/server/wm/ViewServer$ViewServerWorker:mNeedWindowListUpdate	Z
            //     53: ifne +49 -> 102
            //     56: aload_0
            //     57: getfield 31	com/android/server/wm/ViewServer$ViewServerWorker:mNeedFocusedWindowUpdate	Z
            //     60: ifne +42 -> 102
            //     63: aload_0
            //     64: invokevirtual 71	java/lang/Object:wait	()V
            //     67: goto -18 -> 49
            //     70: astore 10
            //     72: aload_0
            //     73: monitorexit
            //     74: aload 10
            //     76: athrow
            //     77: astore 5
            //     79: aload_2
            //     80: astore_1
            //     81: aload_1
            //     82: ifnull +7 -> 89
            //     85: aload_1
            //     86: invokevirtual 74	java/io/BufferedWriter:close	()V
            //     89: aload_0
            //     90: getfield 22	com/android/server/wm/ViewServer$ViewServerWorker:this$0	Lcom/android/server/wm/ViewServer;
            //     93: invokestatic 41	com/android/server/wm/ViewServer:access$100	(Lcom/android/server/wm/ViewServer;)Lcom/android/server/wm/WindowManagerService;
            //     96: aload_0
            //     97: invokevirtual 77	com/android/server/wm/WindowManagerService:removeWindowChangeListener	(Lcom/android/server/wm/WindowManagerService$WindowChangeListener;)V
            //     100: iconst_1
            //     101: ireturn
            //     102: aload_0
            //     103: getfield 29	com/android/server/wm/ViewServer$ViewServerWorker:mNeedWindowListUpdate	Z
            //     106: ifeq +11 -> 117
            //     109: aload_0
            //     110: iconst_0
            //     111: putfield 29	com/android/server/wm/ViewServer$ViewServerWorker:mNeedWindowListUpdate	Z
            //     114: iconst_1
            //     115: istore 8
            //     117: aload_0
            //     118: getfield 31	com/android/server/wm/ViewServer$ViewServerWorker:mNeedFocusedWindowUpdate	Z
            //     121: ifeq +11 -> 132
            //     124: aload_0
            //     125: iconst_0
            //     126: putfield 31	com/android/server/wm/ViewServer$ViewServerWorker:mNeedFocusedWindowUpdate	Z
            //     129: iconst_1
            //     130: istore 9
            //     132: aload_0
            //     133: monitorexit
            //     134: iload 8
            //     136: ifeq +13 -> 149
            //     139: aload_2
            //     140: ldc 79
            //     142: invokevirtual 83	java/io/BufferedWriter:write	(Ljava/lang/String;)V
            //     145: aload_2
            //     146: invokevirtual 86	java/io/BufferedWriter:flush	()V
            //     149: iload 9
            //     151: ifeq -116 -> 35
            //     154: aload_2
            //     155: ldc 88
            //     157: invokevirtual 83	java/io/BufferedWriter:write	(Ljava/lang/String;)V
            //     160: aload_2
            //     161: invokevirtual 86	java/io/BufferedWriter:flush	()V
            //     164: goto -129 -> 35
            //     167: astore_3
            //     168: aload_2
            //     169: astore_1
            //     170: aload_1
            //     171: ifnull +7 -> 178
            //     174: aload_1
            //     175: invokevirtual 74	java/io/BufferedWriter:close	()V
            //     178: aload_0
            //     179: getfield 22	com/android/server/wm/ViewServer$ViewServerWorker:this$0	Lcom/android/server/wm/ViewServer;
            //     182: invokestatic 41	com/android/server/wm/ViewServer:access$100	(Lcom/android/server/wm/ViewServer;)Lcom/android/server/wm/WindowManagerService;
            //     185: aload_0
            //     186: invokevirtual 77	com/android/server/wm/WindowManagerService:removeWindowChangeListener	(Lcom/android/server/wm/WindowManagerService$WindowChangeListener;)V
            //     189: aload_3
            //     190: athrow
            //     191: aload_2
            //     192: ifnull +7 -> 199
            //     195: aload_2
            //     196: invokevirtual 74	java/io/BufferedWriter:close	()V
            //     199: aload_0
            //     200: getfield 22	com/android/server/wm/ViewServer$ViewServerWorker:this$0	Lcom/android/server/wm/ViewServer;
            //     203: invokestatic 41	com/android/server/wm/ViewServer:access$100	(Lcom/android/server/wm/ViewServer;)Lcom/android/server/wm/WindowManagerService;
            //     206: aload_0
            //     207: invokevirtual 77	com/android/server/wm/WindowManagerService:removeWindowChangeListener	(Lcom/android/server/wm/WindowManagerService$WindowChangeListener;)V
            //     210: goto -110 -> 100
            //     213: astore 7
            //     215: goto -16 -> 199
            //     218: astore 6
            //     220: goto -131 -> 89
            //     223: astore 4
            //     225: goto -47 -> 178
            //     228: astore_3
            //     229: goto -59 -> 170
            //     232: astore 11
            //     234: goto -153 -> 81
            //
            // Exception table:
            //     from	to	target	type
            //     49	74	70	finally
            //     102	134	70	finally
            //     35	49	77	java/lang/Exception
            //     74	77	77	java/lang/Exception
            //     139	164	77	java/lang/Exception
            //     35	49	167	finally
            //     74	77	167	finally
            //     139	164	167	finally
            //     195	199	213	java/io/IOException
            //     85	89	218	java/io/IOException
            //     174	178	223	java/io/IOException
            //     13	35	228	finally
            //     13	35	232	java/lang/Exception
        }

        public void focusChanged()
        {
            try
            {
                this.mNeedFocusedWindowUpdate = true;
                notifyAll();
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        // ERROR //
        public void run()
        {
            // Byte code:
            //     0: aconst_null
            //     1: astore_1
            //     2: new 95	java/io/BufferedReader
            //     5: dup
            //     6: new 97	java/io/InputStreamReader
            //     9: dup
            //     10: aload_0
            //     11: getfield 27	com/android/server/wm/ViewServer$ViewServerWorker:mClient	Ljava/net/Socket;
            //     14: invokevirtual 101	java/net/Socket:getInputStream	()Ljava/io/InputStream;
            //     17: invokespecial 104	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
            //     20: sipush 1024
            //     23: invokespecial 107	java/io/BufferedReader:<init>	(Ljava/io/Reader;I)V
            //     26: astore_2
            //     27: aload_2
            //     28: invokevirtual 111	java/io/BufferedReader:readLine	()Ljava/lang/String;
            //     31: astore 10
            //     33: aload 10
            //     35: bipush 32
            //     37: invokevirtual 117	java/lang/String:indexOf	(I)I
            //     40: istore 11
            //     42: iload 11
            //     44: bipush 255
            //     46: if_icmpne +86 -> 132
            //     49: aload 10
            //     51: astore 12
            //     53: ldc 119
            //     55: astore 13
            //     57: ldc 121
            //     59: aload 12
            //     61: invokevirtual 125	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
            //     64: ifeq +92 -> 156
            //     67: aload_0
            //     68: getfield 27	com/android/server/wm/ViewServer$ViewServerWorker:mClient	Ljava/net/Socket;
            //     71: ldc 127
            //     73: invokestatic 131	com/android/server/wm/ViewServer:access$000	(Ljava/net/Socket;Ljava/lang/String;)Z
            //     76: istore 15
            //     78: iload 15
            //     80: ifne +29 -> 109
            //     83: ldc 133
            //     85: new 135	java/lang/StringBuilder
            //     88: dup
            //     89: invokespecial 136	java/lang/StringBuilder:<init>	()V
            //     92: ldc 138
            //     94: invokevirtual 142	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     97: aload 12
            //     99: invokevirtual 142	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     102: invokevirtual 145	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     105: invokestatic 151	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     108: pop
            //     109: aload_2
            //     110: ifnull +7 -> 117
            //     113: aload_2
            //     114: invokevirtual 152	java/io/BufferedReader:close	()V
            //     117: aload_0
            //     118: getfield 27	com/android/server/wm/ViewServer$ViewServerWorker:mClient	Ljava/net/Socket;
            //     121: ifnull +298 -> 419
            //     124: aload_0
            //     125: getfield 27	com/android/server/wm/ViewServer$ViewServerWorker:mClient	Ljava/net/Socket;
            //     128: invokevirtual 153	java/net/Socket:close	()V
            //     131: return
            //     132: aload 10
            //     134: iconst_0
            //     135: iload 11
            //     137: invokevirtual 157	java/lang/String:substring	(II)Ljava/lang/String;
            //     140: astore 12
            //     142: aload 10
            //     144: iload 11
            //     146: iconst_1
            //     147: iadd
            //     148: invokevirtual 160	java/lang/String:substring	(I)Ljava/lang/String;
            //     151: astore 13
            //     153: goto -96 -> 57
            //     156: ldc 162
            //     158: aload 12
            //     160: invokevirtual 125	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
            //     163: ifeq +17 -> 180
            //     166: aload_0
            //     167: getfield 27	com/android/server/wm/ViewServer$ViewServerWorker:mClient	Ljava/net/Socket;
            //     170: ldc 127
            //     172: invokestatic 131	com/android/server/wm/ViewServer:access$000	(Ljava/net/Socket;Ljava/lang/String;)Z
            //     175: istore 15
            //     177: goto -99 -> 78
            //     180: ldc 164
            //     182: aload 12
            //     184: invokevirtual 125	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
            //     187: ifeq +22 -> 209
            //     190: aload_0
            //     191: getfield 22	com/android/server/wm/ViewServer$ViewServerWorker:this$0	Lcom/android/server/wm/ViewServer;
            //     194: invokestatic 41	com/android/server/wm/ViewServer:access$100	(Lcom/android/server/wm/ViewServer;)Lcom/android/server/wm/WindowManagerService;
            //     197: aload_0
            //     198: getfield 27	com/android/server/wm/ViewServer$ViewServerWorker:mClient	Ljava/net/Socket;
            //     201: invokevirtual 168	com/android/server/wm/WindowManagerService:viewServerListWindows	(Ljava/net/Socket;)Z
            //     204: istore 15
            //     206: goto -128 -> 78
            //     209: ldc 170
            //     211: aload 12
            //     213: invokevirtual 125	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
            //     216: ifeq +22 -> 238
            //     219: aload_0
            //     220: getfield 22	com/android/server/wm/ViewServer$ViewServerWorker:this$0	Lcom/android/server/wm/ViewServer;
            //     223: invokestatic 41	com/android/server/wm/ViewServer:access$100	(Lcom/android/server/wm/ViewServer;)Lcom/android/server/wm/WindowManagerService;
            //     226: aload_0
            //     227: getfield 27	com/android/server/wm/ViewServer$ViewServerWorker:mClient	Ljava/net/Socket;
            //     230: invokevirtual 173	com/android/server/wm/WindowManagerService:viewServerGetFocusedWindow	(Ljava/net/Socket;)Z
            //     233: istore 15
            //     235: goto -157 -> 78
            //     238: ldc 175
            //     240: aload 12
            //     242: invokevirtual 125	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
            //     245: ifeq +12 -> 257
            //     248: aload_0
            //     249: invokespecial 177	com/android/server/wm/ViewServer$ViewServerWorker:windowManagerAutolistLoop	()Z
            //     252: istore 15
            //     254: goto -176 -> 78
            //     257: aload_0
            //     258: getfield 22	com/android/server/wm/ViewServer$ViewServerWorker:this$0	Lcom/android/server/wm/ViewServer;
            //     261: invokestatic 41	com/android/server/wm/ViewServer:access$100	(Lcom/android/server/wm/ViewServer;)Lcom/android/server/wm/WindowManagerService;
            //     264: aload_0
            //     265: getfield 27	com/android/server/wm/ViewServer$ViewServerWorker:mClient	Ljava/net/Socket;
            //     268: aload 12
            //     270: aload 13
            //     272: invokevirtual 181	com/android/server/wm/WindowManagerService:viewServerWindowCommand	(Ljava/net/Socket;Ljava/lang/String;Ljava/lang/String;)Z
            //     275: istore 14
            //     277: iload 14
            //     279: istore 15
            //     281: goto -203 -> 78
            //     284: astore 17
            //     286: aload 17
            //     288: invokevirtual 184	java/io/IOException:printStackTrace	()V
            //     291: goto -174 -> 117
            //     294: astore 16
            //     296: aload 16
            //     298: invokevirtual 184	java/io/IOException:printStackTrace	()V
            //     301: goto -170 -> 131
            //     304: astore_3
            //     305: ldc 133
            //     307: ldc 186
            //     309: aload_3
            //     310: invokestatic 189	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     313: pop
            //     314: aload_1
            //     315: ifnull +7 -> 322
            //     318: aload_1
            //     319: invokevirtual 152	java/io/BufferedReader:close	()V
            //     322: aload_0
            //     323: getfield 27	com/android/server/wm/ViewServer$ViewServerWorker:mClient	Ljava/net/Socket;
            //     326: ifnull -195 -> 131
            //     329: aload_0
            //     330: getfield 27	com/android/server/wm/ViewServer$ViewServerWorker:mClient	Ljava/net/Socket;
            //     333: invokevirtual 153	java/net/Socket:close	()V
            //     336: goto -205 -> 131
            //     339: astore 8
            //     341: aload 8
            //     343: invokevirtual 184	java/io/IOException:printStackTrace	()V
            //     346: goto -215 -> 131
            //     349: astore 9
            //     351: aload 9
            //     353: invokevirtual 184	java/io/IOException:printStackTrace	()V
            //     356: goto -34 -> 322
            //     359: astore 4
            //     361: aload_1
            //     362: ifnull +7 -> 369
            //     365: aload_1
            //     366: invokevirtual 152	java/io/BufferedReader:close	()V
            //     369: aload_0
            //     370: getfield 27	com/android/server/wm/ViewServer$ViewServerWorker:mClient	Ljava/net/Socket;
            //     373: ifnull +10 -> 383
            //     376: aload_0
            //     377: getfield 27	com/android/server/wm/ViewServer$ViewServerWorker:mClient	Ljava/net/Socket;
            //     380: invokevirtual 153	java/net/Socket:close	()V
            //     383: aload 4
            //     385: athrow
            //     386: astore 6
            //     388: aload 6
            //     390: invokevirtual 184	java/io/IOException:printStackTrace	()V
            //     393: goto -24 -> 369
            //     396: astore 5
            //     398: aload 5
            //     400: invokevirtual 184	java/io/IOException:printStackTrace	()V
            //     403: goto -20 -> 383
            //     406: astore 4
            //     408: aload_2
            //     409: astore_1
            //     410: goto -49 -> 361
            //     413: astore_3
            //     414: aload_2
            //     415: astore_1
            //     416: goto -111 -> 305
            //     419: goto -288 -> 131
            //
            // Exception table:
            //     from	to	target	type
            //     113	117	284	java/io/IOException
            //     124	131	294	java/io/IOException
            //     2	27	304	java/io/IOException
            //     329	336	339	java/io/IOException
            //     318	322	349	java/io/IOException
            //     2	27	359	finally
            //     305	314	359	finally
            //     365	369	386	java/io/IOException
            //     376	383	396	java/io/IOException
            //     27	109	406	finally
            //     132	277	406	finally
            //     27	109	413	java/io/IOException
            //     132	277	413	java/io/IOException
        }

        public void windowsChanged()
        {
            try
            {
                this.mNeedWindowListUpdate = true;
                notifyAll();
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.wm.ViewServer
 * JD-Core Version:        0.6.2
 */