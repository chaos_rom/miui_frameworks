package com.android.server.wm;

import android.content.res.CompatibilityInfo;

final class StartingData
{
    final CompatibilityInfo compatInfo;
    final int icon;
    final int labelRes;
    final CharSequence nonLocalizedLabel;
    final String pkg;
    final int theme;
    final int windowFlags;

    StartingData(String paramString, int paramInt1, CompatibilityInfo paramCompatibilityInfo, CharSequence paramCharSequence, int paramInt2, int paramInt3, int paramInt4)
    {
        this.pkg = paramString;
        this.theme = paramInt1;
        this.compatInfo = paramCompatibilityInfo;
        this.nonLocalizedLabel = paramCharSequence;
        this.labelRes = paramInt2;
        this.icon = paramInt3;
        this.windowFlags = paramInt4;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.wm.StartingData
 * JD-Core Version:        0.6.2
 */