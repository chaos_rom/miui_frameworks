package com.android.server.wm;

import android.app.ActivityManagerNative;
import android.app.IActivityManager;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.CompatibilityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.os.Binder;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.IRemoteCallback;
import android.os.Looper;
import android.os.Message;
import android.os.Parcel;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Process;
import android.os.RemoteException;
import android.os.StrictMode;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.TokenWatcher;
import android.os.Trace;
import android.provider.Settings.Secure;
import android.provider.Settings.System;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.util.Slog;
import android.util.TypedValue;
import android.view.Choreographer;
import android.view.Display;
import android.view.IApplicationToken;
import android.view.IOnKeyguardExitResult;
import android.view.IRotationWatcher;
import android.view.IWindow;
import android.view.IWindowManager.Stub;
import android.view.IWindowSession;
import android.view.InputChannel;
import android.view.InputDevice;
import android.view.InputEventReceiver;
import android.view.InputEventReceiver.Factory;
import android.view.Surface;
import android.view.Surface.OutOfResourcesException;
import android.view.SurfaceSession;
import android.view.WindowManager.LayoutParams;
import android.view.WindowManagerPolicy;
import android.view.WindowManagerPolicy.FakeWindow;
import android.view.WindowManagerPolicy.OnKeyguardExitResult;
import android.view.WindowManagerPolicy.WindowManagerFuncs;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import com.android.internal.R.styleable;
import com.android.internal.app.IBatteryStats;
import com.android.internal.policy.PolicyManager;
import com.android.internal.view.IInputContext;
import com.android.internal.view.IInputMethodClient;
import com.android.internal.view.IInputMethodManager;
import com.android.internal.view.WindowManagerPolicyThread;
import com.android.server.AttributeCache;
import com.android.server.AttributeCache.Entry;
import com.android.server.PowerManagerService;
import com.android.server.Watchdog;
import com.android.server.Watchdog.Monitor;
import com.android.server.am.BatteryStatsService;
import com.android.server.input.InputFilter;
import com.android.server.input.InputManagerService;
import com.android.server.pm.ShutdownThread;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class WindowManagerService extends IWindowManager.Stub
    implements Watchdog.Monitor, WindowManagerPolicy.WindowManagerFuncs
{
    static final int ADJUST_WALLPAPER_LAYERS_CHANGED = 2;
    static final int ADJUST_WALLPAPER_VISIBILITY_CHANGED = 4;
    private static final int ALLOW_DISABLE_NO = 0;
    private static final int ALLOW_DISABLE_UNKNOWN = -1;
    private static final int ALLOW_DISABLE_YES = 1;
    static final boolean CUSTOM_SCREEN_ROTATION = true;
    static final boolean DEBUG = false;
    static final boolean DEBUG_ADD_REMOVE = false;
    static final boolean DEBUG_ANIM = false;
    static final boolean DEBUG_APP_ORIENTATION = false;
    static final boolean DEBUG_APP_TRANSITIONS = false;
    static final boolean DEBUG_BOOT = false;
    static final boolean DEBUG_CONFIGURATION = false;
    static final boolean DEBUG_DRAG = false;
    static final boolean DEBUG_FOCUS = false;
    static final boolean DEBUG_INPUT = false;
    static final boolean DEBUG_INPUT_METHOD = false;
    static final boolean DEBUG_LAYERS = false;
    static final boolean DEBUG_LAYOUT = false;
    static final boolean DEBUG_LAYOUT_REPEATS = true;
    static final boolean DEBUG_ORIENTATION = false;
    static final boolean DEBUG_REORDER = false;
    static final boolean DEBUG_RESIZE = false;
    static final boolean DEBUG_SCREENSHOT = false;
    static final boolean DEBUG_SCREEN_ON = false;
    static final boolean DEBUG_STARTING_WINDOW = false;
    static final boolean DEBUG_SURFACE_TRACE = false;
    static final boolean DEBUG_TOKEN_MOVEMENT = false;
    static final boolean DEBUG_VISIBILITY = false;
    static final boolean DEBUG_WALLPAPER = false;
    static final boolean DEBUG_WINDOW_MOVEMENT = false;
    static final boolean DEBUG_WINDOW_TRACE = false;
    static final int DEFAULT_DIM_DURATION = 200;
    static final int DEFAULT_FADE_IN_OUT_DURATION = 400;
    static final long DEFAULT_INPUT_DISPATCHING_TIMEOUT_NANOS = 5000000000L;
    static final int FREEZE_LAYER = 2000001;
    static final boolean HIDE_STACK_CRAWLS = true;
    private static final int INPUT_DEVICES_READY_FOR_SAFE_MODE_DETECTION_TIMEOUT_MILLIS = 1000;
    static final int LAYER_OFFSET_BLUR = 2;
    static final int LAYER_OFFSET_DIM = 1;
    static final int LAYER_OFFSET_THUMBNAIL = 4;
    static final int LAYOUT_REPEAT_THRESHOLD = 4;
    static final int MASK_LAYER = 2000000;
    static final int MAX_ANIMATION_DURATION = 10000;
    static final boolean PROFILE_ORIENTATION = false;
    static final boolean SHOW_LIGHT_TRANSACTIONS = false;
    static final boolean SHOW_SURFACE_ALLOC = false;
    static final boolean SHOW_TRANSACTIONS = false;
    private static final String SYSTEM_DEBUGGABLE = "ro.debuggable";
    private static final String SYSTEM_HEADLESS = "ro.config.headless";
    private static final String SYSTEM_SECURE = "ro.secure";
    static final String TAG = "WindowManager";
    private static final float THUMBNAIL_ANIMATION_DECELERATE_FACTOR = 1.5F;
    static final int TYPE_LAYER_MULTIPLIER = 10000;
    static final int TYPE_LAYER_OFFSET = 1000;
    static final int UPDATE_FOCUS_NORMAL = 0;
    static final int UPDATE_FOCUS_PLACING_SURFACES = 2;
    static final int UPDATE_FOCUS_WILL_ASSIGN_LAYERS = 1;
    static final int UPDATE_FOCUS_WILL_PLACE_SURFACES = 3;
    static final long WALLPAPER_TIMEOUT = 150L;
    static final long WALLPAPER_TIMEOUT_RECOVERY = 10000L;
    static final int WINDOW_LAYER_MULTIPLIER = 5;
    static final boolean localLOGV;
    final IActivityManager mActivityManager;
    final boolean mAllowBootMessages;
    private int mAllowDisableKeyguard = -1;
    boolean mAltOrientation = false;
    ArrayList<AppWindowToken> mAnimatingAppTokens = new ArrayList();
    final AnimationRunnable mAnimationRunnable = new AnimationRunnable(null);
    boolean mAnimationScheduled;
    final WindowAnimator mAnimator;
    float mAnimatorDurationScale = 1.0F;
    int mAppDisplayHeight = 0;
    int mAppDisplayWidth = 0;
    final ArrayList<AppWindowToken> mAppTokens = new ArrayList();
    boolean mAppTransitionReady = false;
    boolean mAppTransitionRunning = false;
    boolean mAppTransitionTimeout = false;
    int mAppsFreezingScreen = 0;
    int mBaseDisplayHeight = 0;
    int mBaseDisplayWidth = 0;
    final IBatteryStats mBatteryStats;
    BlackFrame mBlackFrame;
    final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            WindowManagerService.this.mPolicy.enableKeyguard(true);
            synchronized (WindowManagerService.this.mKeyguardTokenWatcher)
            {
                WindowManagerService.access$202(WindowManagerService.this, -1);
                WindowManagerService.access$102(WindowManagerService.this, false);
                return;
            }
        }
    };
    final Choreographer mChoreographer = Choreographer.getInstance();
    final ArrayList<AppWindowToken> mClosingApps = new ArrayList();
    final DisplayMetrics mCompatDisplayMetrics = new DisplayMetrics();
    float mCompatibleScreenScale;
    final Context mContext;
    Configuration mCurConfiguration = new Configuration();
    int mCurDisplayHeight = 0;
    int mCurDisplayWidth = 0;
    WindowState mCurrentFocus = null;
    int mDeferredRotationPauseCount;
    final ArrayList<WindowState> mDestroySurface = new ArrayList();
    Display mDisplay;
    boolean mDisplayEnabled = false;
    boolean mDisplayFrozen = false;
    final DisplayMetrics mDisplayMetrics = new DisplayMetrics();
    final Object mDisplaySizeLock = new Object();
    DragState mDragState = null;
    private boolean mEventDispatchingEnabled;
    final ArrayList<AppWindowToken> mExitingAppTokens = new ArrayList();
    final ArrayList<WindowToken> mExitingTokens = new ArrayList();
    final ArrayList<FakeWindowImpl> mFakeWindows = new ArrayList();
    final ArrayList<AppWindowToken> mFinishedStarting = new ArrayList();
    boolean mFocusMayChange;
    AppWindowToken mFocusedApp = null;
    boolean mForceDisplayEnabled = false;
    ArrayList<WindowState> mForceRemoves;
    int mForcedAppOrientation = -1;
    final SurfaceSession mFxSession;
    final H mH = new H();
    boolean mHardKeyboardAvailable;
    boolean mHardKeyboardEnabled;
    OnHardKeyboardStatusChangeListener mHardKeyboardStatusChangeListener;
    final boolean mHaveInputMethods;
    private final boolean mHeadless;
    Session mHoldingScreenOn;
    PowerManager.WakeLock mHoldingScreenWakeLock;
    private boolean mInLayout = false;
    boolean mInTouchMode = true;
    int mInitialDisplayHeight = 0;
    int mInitialDisplayWidth = 0;
    LayoutFields mInnerFields = new LayoutFields();
    final InputManagerService mInputManager;
    int mInputMethodAnimLayerAdjustment;
    final ArrayList<WindowState> mInputMethodDialogs = new ArrayList();
    IInputMethodManager mInputMethodManager;
    WindowState mInputMethodTarget = null;
    boolean mInputMethodTargetWaitingAnim;
    WindowState mInputMethodWindow = null;
    final InputMonitor mInputMonitor = new InputMonitor(this);
    boolean mIsTouchDevice;
    private boolean mKeyguardDisabled = false;
    final TokenWatcher mKeyguardTokenWatcher = new TokenWatcher(new Handler(), "WindowManagerService.mKeyguardTokenWatcher")
    {
        public void acquired()
        {
            if (WindowManagerService.this.shouldAllowDisableKeyguard())
            {
                WindowManagerService.this.mPolicy.enableKeyguard(false);
                WindowManagerService.access$102(WindowManagerService.this, true);
            }
            while (true)
            {
                return;
                Log.v("WindowManager", "Not disabling keyguard since device policy is enforced");
            }
        }

        public void released()
        {
            WindowManagerService.this.mPolicy.enableKeyguard(true);
            synchronized (WindowManagerService.this.mKeyguardTokenWatcher)
            {
                WindowManagerService.access$102(WindowManagerService.this, false);
                WindowManagerService.this.mKeyguardTokenWatcher.notifyAll();
                return;
            }
        }
    };
    int mLargestDisplayHeight = 0;
    int mLargestDisplayWidth = 0;
    String mLastANRState;
    WindowState mLastFocus = null;
    int mLastStatusBarVisibility = 0;
    long mLastWallpaperTimeoutTime;
    float mLastWallpaperX = -1.0F;
    float mLastWallpaperXStep = -1.0F;
    float mLastWallpaperY = -1.0F;
    float mLastWallpaperYStep = -1.0F;
    int mLastWindowForcedOrientation = -1;
    boolean mLayoutNeeded = true;
    private int mLayoutRepeatCount;
    int mLayoutSeq = 0;
    final boolean mLimitedAlphaCompositing;
    ArrayList<WindowState> mLosingFocus = new ArrayList();
    WindowState mLowerWallpaperTarget = null;
    int mNextAppTransition = -1;
    IRemoteCallback mNextAppTransitionCallback;
    boolean mNextAppTransitionDelayed;
    int mNextAppTransitionEnter;
    int mNextAppTransitionExit;
    String mNextAppTransitionPackage;
    int mNextAppTransitionStartHeight;
    int mNextAppTransitionStartWidth;
    int mNextAppTransitionStartX;
    int mNextAppTransitionStartY;
    Bitmap mNextAppTransitionThumbnail;
    int mNextAppTransitionType = 0;
    final boolean mOnlyCore;
    final ArrayList<AppWindowToken> mOpeningApps = new ArrayList();
    int mPendingLayoutChanges = 0;
    final ArrayList<WindowState> mPendingRemove = new ArrayList();
    WindowState[] mPendingRemoveTmp = new WindowState[20];
    final WindowManagerPolicy mPolicy = PolicyManager.makeNewWindowManager();
    PowerManagerService mPowerManager;
    final DisplayMetrics mRealDisplayMetrics = new DisplayMetrics();
    WindowState[] mRebuildTmp = new WindowState[20];
    final ArrayList<WindowState> mRelayoutWhileAnimating = new ArrayList();
    final ArrayList<WindowState> mResizingWindows = new ArrayList();
    int mRotation = 0;
    ArrayList<IRotationWatcher> mRotationWatchers = new ArrayList();
    boolean mSafeMode;
    PowerManager.WakeLock mScreenFrozenLock;
    final HashSet<Session> mSessions = new HashSet();
    boolean mShowingBootMessages = false;
    boolean mSkipAppTransitionAnimation = false;
    int mSmallestDisplayHeight = 0;
    int mSmallestDisplayWidth = 0;
    boolean mStartingIconInTransition = false;
    StrictModeFlash mStrictModeFlash;
    boolean mSystemBooted = false;
    int mSystemDecorLayer = 0;
    final Rect mSystemDecorRect = new Rect();
    final Configuration mTempConfiguration = new Configuration();
    final DisplayMetrics mTmpDisplayMetrics = new DisplayMetrics();
    final float[] mTmpFloats = new float[9];
    final HashMap<IBinder, WindowToken> mTokenMap = new HashMap();
    private int mTransactionSequence;
    float mTransitionAnimationScale = 1.0F;
    boolean mTraversalScheduled = false;
    boolean mTurnOnScreen;
    WindowState mUpperWallpaperTarget = null;
    private ViewServer mViewServer;
    boolean mWaitingForConfig = false;
    ArrayList<Pair<WindowState, IRemoteCallback>> mWaitingForDrawn = new ArrayList();
    WindowState mWaitingOnWallpaper;
    int mWallpaperAnimLayerAdjustment;
    WindowState mWallpaperTarget = null;
    final ArrayList<WindowToken> mWallpaperTokens = new ArrayList();
    Watermark mWatermark;
    float mWindowAnimationScale = 1.0F;
    private ArrayList<WindowChangeListener> mWindowChangeListeners = new ArrayList();
    final HashMap<IBinder, WindowState> mWindowMap = new HashMap();
    final ArrayList<WindowState> mWindows = new ArrayList();
    private boolean mWindowsChanged = false;
    boolean mWindowsFreezingScreen = false;

    private WindowManagerService(Context paramContext, PowerManagerService paramPowerManagerService, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
    {
        this.mContext = paramContext;
        this.mHaveInputMethods = paramBoolean1;
        this.mAllowBootMessages = paramBoolean2;
        this.mOnlyCore = paramBoolean3;
        this.mLimitedAlphaCompositing = paramContext.getResources().getBoolean(17891335);
        this.mHeadless = "1".equals(SystemProperties.get("ro.config.headless", "0"));
        this.mPowerManager = paramPowerManagerService;
        this.mPowerManager.setPolicy(this.mPolicy);
        PowerManager localPowerManager = (PowerManager)paramContext.getSystemService("power");
        this.mScreenFrozenLock = localPowerManager.newWakeLock(1, "SCREEN_FROZEN");
        this.mScreenFrozenLock.setReferenceCounted(false);
        this.mActivityManager = ActivityManagerNative.getDefault();
        this.mBatteryStats = BatteryStatsService.getService();
        this.mWindowAnimationScale = Settings.System.getFloat(paramContext.getContentResolver(), "window_animation_scale", this.mWindowAnimationScale);
        this.mTransitionAnimationScale = Settings.System.getFloat(paramContext.getContentResolver(), "transition_animation_scale", this.mTransitionAnimationScale);
        this.mAnimatorDurationScale = Settings.System.getFloat(paramContext.getContentResolver(), "animator_duration_scale", this.mTransitionAnimationScale);
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED");
        this.mContext.registerReceiver(this.mBroadcastReceiver, localIntentFilter);
        this.mHoldingScreenWakeLock = localPowerManager.newWakeLock(536870922, "KEEP_SCREEN_ON_FLAG");
        this.mHoldingScreenWakeLock.setReferenceCounted(false);
        this.mInputManager = new InputManagerService(paramContext, this.mInputMonitor);
        this.mAnimator = new WindowAnimator(this, paramContext, this.mPolicy);
        PolicyThread localPolicyThread = new PolicyThread(this.mPolicy, this, paramContext, paramPowerManagerService);
        localPolicyThread.start();
        try
        {
            while (true)
            {
                boolean bool = localPolicyThread.mRunning;
                if (bool)
                    break;
                try
                {
                    localPolicyThread.wait();
                }
                catch (InterruptedException localInterruptedException)
                {
                }
            }
            this.mInputManager.start();
            Watchdog.getInstance().addMonitor(this);
            this.mFxSession = new SurfaceSession();
            Surface.openTransaction();
            createWatermark();
            Surface.closeTransaction();
            return;
        }
        finally
        {
        }
    }

    private void addAppTokenToAnimating(int paramInt, AppWindowToken paramAppWindowToken)
    {
        if ((paramInt == 0) || (paramInt == this.mAnimatingAppTokens.size()))
            this.mAnimatingAppTokens.add(paramInt, paramAppWindowToken);
        while (true)
        {
            return;
            AppWindowToken localAppWindowToken = (AppWindowToken)this.mAppTokens.get(paramInt + 1);
            this.mAnimatingAppTokens.add(this.mAnimatingAppTokens.indexOf(localAppWindowToken), paramAppWindowToken);
        }
    }

    private void addWindowToListInOrderLocked(WindowState paramWindowState, boolean paramBoolean)
    {
        WindowToken localWindowToken1 = paramWindowState.mToken;
        ArrayList localArrayList = this.mWindows;
        int i = localArrayList.size();
        WindowState localWindowState1 = paramWindowState.mAttachedWindow;
        if (localWindowState1 == null)
        {
            int i2 = localWindowToken1.windows.size();
            if (localWindowToken1.appWindowToken != null)
            {
                int i5 = i2 - 1;
                if (i5 >= 0)
                    if (paramWindowState.mAttrs.type == 1)
                    {
                        placeWindowBefore((WindowState)localWindowToken1.windows.get(0), paramWindowState);
                        i2 = 0;
                    }
                while (true)
                {
                    if (paramBoolean)
                        localWindowToken1.windows.add(i2, paramWindowState);
                    if ((paramWindowState.mAppToken != null) && (paramBoolean))
                        paramWindowState.mAppToken.allAppWindows.add(paramWindowState);
                    return;
                    AppWindowToken localAppWindowToken3 = paramWindowState.mAppToken;
                    if ((localAppWindowToken3 != null) && (localWindowToken1.windows.get(i5) == localAppWindowToken3.startingWindow))
                    {
                        placeWindowBefore((WindowState)localWindowToken1.windows.get(i5), paramWindowState);
                        i2--;
                    }
                    else
                    {
                        int i12 = findIdxBasedOnAppTokens(paramWindowState);
                        if (i12 != -1)
                        {
                            localArrayList.add(i12 + 1, paramWindowState);
                            this.mWindowsChanged = true;
                            continue;
                            int i6 = this.mAnimatingAppTokens.size();
                            Object localObject2 = null;
                            for (int i7 = i6 - 1; ; i7--)
                            {
                                AppWindowToken localAppWindowToken2;
                                if (i7 >= 0)
                                {
                                    localAppWindowToken2 = (AppWindowToken)this.mAnimatingAppTokens.get(i7);
                                    if (localAppWindowToken2 == localWindowToken1)
                                        i7--;
                                }
                                else
                                {
                                    if (localObject2 == null)
                                        break label387;
                                    WindowToken localWindowToken3 = (WindowToken)this.mTokenMap.get(((WindowState)localObject2).mClient.asBinder());
                                    if ((localWindowToken3 != null) && (localWindowToken3.windows.size() > 0))
                                    {
                                        WindowState localWindowState4 = (WindowState)localWindowToken3.windows.get(0);
                                        if (localWindowState4.mSubLayer < 0)
                                            localObject2 = localWindowState4;
                                    }
                                    placeWindowBefore((WindowState)localObject2, paramWindowState);
                                    break;
                                }
                                if ((!localAppWindowToken2.sendingToBottom) && (localAppWindowToken2.windows.size() > 0))
                                    localObject2 = (WindowState)localAppWindowToken2.windows.get(0);
                            }
                            label387: AppWindowToken localAppWindowToken1;
                            int i11;
                            do
                            {
                                i7--;
                                if (i7 < 0)
                                    break;
                                localAppWindowToken1 = (AppWindowToken)this.mAnimatingAppTokens.get(i7);
                                i11 = localAppWindowToken1.windows.size();
                            }
                            while (i11 <= 0);
                            localObject2 = (WindowState)localAppWindowToken1.windows.get(i11 - 1);
                            if (localObject2 == null)
                                break;
                            WindowToken localWindowToken2 = (WindowToken)this.mTokenMap.get(((WindowState)localObject2).mClient.asBinder());
                            if (localWindowToken2 != null)
                            {
                                int i10 = localWindowToken2.windows.size();
                                if (i10 > 0)
                                {
                                    WindowState localWindowState3 = (WindowState)localWindowToken2.windows.get(i10 - 1);
                                    if (localWindowState3.mSubLayer >= 0)
                                        localObject2 = localWindowState3;
                                }
                            }
                            placeWindowAfter((WindowState)localObject2, paramWindowState);
                        }
                    }
                }
                int i8 = paramWindowState.mBaseLayer;
                for (int i9 = 0; ; i9++)
                    if ((i9 >= i) || (((WindowState)localArrayList.get(i9)).mBaseLayer > i8))
                    {
                        localArrayList.add(i9, paramWindowState);
                        this.mWindowsChanged = true;
                        break;
                    }
            }
            int i3 = paramWindowState.mBaseLayer;
            for (int i4 = i - 1; ; i4--)
                if (i4 >= 0)
                {
                    if (((WindowState)localArrayList.get(i4)).mBaseLayer <= i3)
                        i4++;
                }
                else
                {
                    if (i4 < 0)
                        i4 = 0;
                    localArrayList.add(i4, paramWindowState);
                    this.mWindowsChanged = true;
                    break;
                }
        }
        int j = localWindowToken1.windows.size();
        int k = paramWindowState.mSubLayer;
        int m = -2147483648;
        Object localObject1 = null;
        label798: label828: label830: for (int n = 0; ; n++)
        {
            WindowState localWindowState2;
            int i1;
            if (n < j)
            {
                localWindowState2 = (WindowState)localWindowToken1.windows.get(n);
                i1 = localWindowState2.mSubLayer;
                if (i1 >= m)
                {
                    m = i1;
                    localObject1 = localWindowState2;
                }
                if (k >= 0)
                    break label798;
                if (i1 < k)
                    continue;
                if (paramBoolean)
                    localWindowToken1.windows.add(n, paramWindowState);
                if (i1 >= 0)
                    localWindowState2 = localWindowState1;
                placeWindowBefore(localWindowState2, paramWindowState);
            }
            while (true)
            {
                if (n < j)
                    break label828;
                if (paramBoolean)
                    localWindowToken1.windows.add(paramWindowState);
                if (k >= 0)
                    break label836;
                placeWindowBefore(localWindowState1, paramWindowState);
                break;
                if (i1 <= k)
                    break label830;
                if (paramBoolean)
                    localWindowToken1.windows.add(n, paramWindowState);
                placeWindowBefore(localWindowState2, paramWindowState);
            }
            break;
        }
        label836: if (m >= 0);
        while (true)
        {
            placeWindowAfter((WindowState)localObject1, paramWindowState);
            break;
            localObject1 = localWindowState1;
        }
    }

    private void adjustDisplaySizeRanges(int paramInt1, int paramInt2, int paramInt3)
    {
        int i = this.mPolicy.getConfigDisplayWidth(paramInt2, paramInt3, paramInt1);
        if (i < this.mSmallestDisplayWidth)
            this.mSmallestDisplayWidth = i;
        if (i > this.mLargestDisplayWidth)
            this.mLargestDisplayWidth = i;
        int j = this.mPolicy.getConfigDisplayHeight(paramInt2, paramInt3, paramInt1);
        if (j < this.mSmallestDisplayHeight)
            this.mSmallestDisplayHeight = j;
        if (j > this.mLargestDisplayHeight)
            this.mLargestDisplayHeight = j;
    }

    private int animateAwayWallpaperLocked()
    {
        int i = 0;
        if ((this.mLowerWallpaperTarget != null) && (this.mLowerWallpaperTarget.mAppToken != null) && (this.mLowerWallpaperTarget.mAppToken.hidden))
        {
            this.mUpperWallpaperTarget = null;
            this.mLowerWallpaperTarget = null;
            i = 0x0 | 0x8;
        }
        LayoutFields localLayoutFields = this.mInnerFields;
        localLayoutFields.mAdjResult |= adjustWallpaperWindowsLocked();
        return i;
    }

    private boolean applyAnimationLocked(AppWindowToken paramAppWindowToken, WindowManager.LayoutParams paramLayoutParams, int paramInt, boolean paramBoolean)
    {
        int i = 1;
        boolean bool;
        int i1;
        Animation localAnimation;
        if (okToDisplay())
        {
            bool = false;
            if (this.mNextAppTransitionType == i)
            {
                String str = this.mNextAppTransitionPackage;
                if (paramBoolean)
                {
                    i1 = this.mNextAppTransitionEnter;
                    localAnimation = loadAnimation(str, i1);
                    label49: if (localAnimation != null)
                        paramAppWindowToken.mAppAnimator.setAnimation(localAnimation, bool);
                    label65: if (paramAppWindowToken.mAppAnimator.animation == null)
                        break label477;
                }
            }
        }
        while (true)
        {
            return i;
            i1 = this.mNextAppTransitionExit;
            break;
            if (this.mNextAppTransitionType == 2)
            {
                localAnimation = createScaleUpAnimationLocked(paramInt, paramBoolean);
                bool = true;
                break label49;
            }
            if ((this.mNextAppTransitionType == 3) || (this.mNextAppTransitionType == 4))
            {
                if (this.mNextAppTransitionType == 4);
                int m;
                for (int k = i; ; m = 0)
                {
                    localAnimation = createThumbnailAnimationLocked(paramInt, paramBoolean, false, k);
                    bool = true;
                    break;
                }
            }
            int n = 0;
            switch (paramInt)
            {
            default:
                if (n == 0)
                    break;
            case 4102:
            case 8199:
            case 4104:
            case 8201:
            case 4106:
            case 8203:
            case 4109:
            case 8204:
            case 4110:
            case 8207:
            }
            for (localAnimation = loadAnimation(paramLayoutParams, n); ; localAnimation = null)
            {
                break;
                if (paramBoolean);
                for (n = 4; ; n = 5)
                    break;
                if (paramBoolean);
                for (n = 6; ; n = 7)
                    break;
                if (paramBoolean);
                for (n = 8; ; n = 9)
                    break;
                if (paramBoolean);
                for (n = 10; ; n = 11)
                    break;
                if (paramBoolean);
                for (n = 12; ; n = 13)
                    break;
                if (paramBoolean);
                for (n = 14; ; n = 15)
                    break;
                if (paramBoolean);
                for (n = 16; ; n = 17)
                    break;
                if (paramBoolean);
                for (n = 18; ; n = 19)
                    break;
                if (paramBoolean);
                for (n = 20; ; n = 21)
                    break;
                if (paramBoolean);
                for (n = 22; ; n = 23)
                    break;
            }
            paramAppWindowToken.mAppAnimator.clearAnimation();
            break label65;
            label477: int j = 0;
        }
    }

    private final void assignLayersLocked()
    {
        int i = this.mWindows.size();
        int j = 0;
        int k = 0;
        int m = 0;
        if (m < i)
        {
            WindowState localWindowState = (WindowState)this.mWindows.get(m);
            WindowStateAnimator localWindowStateAnimator = localWindowState.mWinAnimator;
            int n = 0;
            int i1 = localWindowState.mLayer;
            label91: int i2;
            if ((localWindowState.mBaseLayer == j) || (localWindowState.mIsImWindow) || ((m > 0) && (localWindowState.mIsWallpaper)))
            {
                k += 5;
                localWindowState.mLayer = k;
                if (localWindowState.mLayer != i1)
                    n = 1;
                i2 = localWindowStateAnimator.mAnimLayer;
                if (localWindowState.mTargetAppToken == null)
                    break label221;
                localWindowStateAnimator.mAnimLayer = (localWindowState.mLayer + localWindowState.mTargetAppToken.mAppAnimator.animLayerAdjustment);
                label141: if (!localWindowState.mIsImWindow)
                    break label267;
                localWindowStateAnimator.mAnimLayer += this.mInputMethodAnimLayerAdjustment;
            }
            while (true)
            {
                if (localWindowStateAnimator.mAnimLayer != i2)
                    n = 1;
                if ((n != 0) && (this.mAnimator.isDimming(localWindowStateAnimator)))
                    scheduleAnimationLocked();
                m++;
                break;
                k = localWindowState.mBaseLayer;
                j = k;
                localWindowState.mLayer = k;
                break label91;
                label221: if (localWindowState.mAppToken != null)
                {
                    localWindowStateAnimator.mAnimLayer = (localWindowState.mLayer + localWindowState.mAppToken.mAppAnimator.animLayerAdjustment);
                    break label141;
                }
                localWindowStateAnimator.mAnimLayer = localWindowState.mLayer;
                break label141;
                label267: if (localWindowState.mIsWallpaper)
                    localWindowStateAnimator.mAnimLayer += this.mWallpaperAnimLayerAdjustment;
            }
        }
    }

    static boolean canBeImeTarget(WindowState paramWindowState)
    {
        int i = 0x20008 & paramWindowState.mAttrs.flags;
        if ((i == 0) || (i == 131080) || (paramWindowState.mAttrs.type == 3));
        for (boolean bool = paramWindowState.isVisibleOrAdding(); ; bool = false)
            return bool;
    }

    private int computeCompatSmallestWidth(boolean paramBoolean, DisplayMetrics paramDisplayMetrics, int paramInt1, int paramInt2)
    {
        this.mTmpDisplayMetrics.setTo(paramDisplayMetrics);
        DisplayMetrics localDisplayMetrics = this.mTmpDisplayMetrics;
        int i;
        if (paramBoolean)
            i = paramInt2;
        for (int j = paramInt1; ; j = paramInt2)
        {
            return reduceCompatConfigWidthSize(reduceCompatConfigWidthSize(reduceCompatConfigWidthSize(reduceCompatConfigWidthSize(0, 0, localDisplayMetrics, i, j), 1, localDisplayMetrics, j, i), 2, localDisplayMetrics, i, j), 3, localDisplayMetrics, j, i);
            i = paramInt1;
        }
    }

    private WindowState computeFocusedWindowLocked()
    {
        Object localObject1 = null;
        Object localObject2 = null;
        int i = -1 + this.mAppTokens.size();
        if (i >= 0);
        WindowState localWindowState;
        AppWindowToken localAppWindowToken;
        for (Object localObject3 = (AppWindowToken)this.mAppTokens.get(i); ; localObject3 = null)
            for (int j = -1 + this.mWindows.size(); ; j--)
            {
                if (j < 0)
                    break label207;
                localWindowState = (WindowState)this.mWindows.get(j);
                localAppWindowToken = localWindowState.mAppToken;
                if ((localAppWindowToken == null) || ((!localAppWindowToken.removed) && (!localAppWindowToken.sendingToBottom)))
                    break;
            }
        int k;
        if ((localAppWindowToken != null) && (localObject3 != null) && (localAppWindowToken != localObject3) && (localWindowState.mAttrs.type != 3))
        {
            k = i;
            label135: if (i > 0)
                if (localObject3 != this.mFocusedApp);
        }
        while (true)
        {
            return localObject1;
            i--;
            localObject3 = (WindowToken)this.mAppTokens.get(i);
            if (localObject3 != localAppWindowToken)
                break label135;
            if (localAppWindowToken != localObject3)
            {
                i = k;
                localObject3 = (WindowToken)this.mAppTokens.get(i);
            }
            if (!localWindowState.canReceiveKeys())
                break;
            localObject2 = localWindowState;
            label207: localObject1 = localObject2;
        }
    }

    private static float computePivot(int paramInt, float paramFloat)
    {
        float f1 = paramFloat - 1.0F;
        if (Math.abs(f1) < 1.0E-04F);
        for (float f2 = paramInt; ; f2 = -paramInt / f1)
            return f2;
    }

    private void computeSizeRangesAndScreenLayout(boolean paramBoolean, int paramInt1, int paramInt2, float paramFloat, Configuration paramConfiguration)
    {
        int i;
        if (paramBoolean)
            i = paramInt2;
        for (int j = paramInt1; ; j = paramInt2)
        {
            this.mSmallestDisplayWidth = 1073741824;
            this.mSmallestDisplayHeight = 1073741824;
            this.mLargestDisplayWidth = 0;
            this.mLargestDisplayHeight = 0;
            adjustDisplaySizeRanges(0, i, j);
            adjustDisplaySizeRanges(1, j, i);
            adjustDisplaySizeRanges(2, i, j);
            adjustDisplaySizeRanges(3, j, i);
            int k = reduceConfigLayout(reduceConfigLayout(reduceConfigLayout(reduceConfigLayout(36, 0, paramFloat, i, j), 1, paramFloat, j, i), 2, paramFloat, i, j), 3, paramFloat, j, i);
            paramConfiguration.smallestScreenWidthDp = ((int)(this.mSmallestDisplayWidth / paramFloat));
            paramConfiguration.screenLayout = k;
            return;
            i = paramInt1;
        }
    }

    private Animation createExitAnimationLocked(int paramInt1, int paramInt2)
    {
        AlphaAnimation localAlphaAnimation1;
        if ((paramInt1 == 4110) || (paramInt1 == 8207))
        {
            localAlphaAnimation1 = new AlphaAnimation(1.0F, 0.0F);
            localAlphaAnimation1.setDetachWallpaper(true);
            localAlphaAnimation1.setDuration(paramInt2);
        }
        AlphaAnimation localAlphaAnimation2;
        for (Object localObject = localAlphaAnimation1; ; localObject = localAlphaAnimation2)
        {
            return localObject;
            localAlphaAnimation2 = new AlphaAnimation(1.0F, 1.0F);
            localAlphaAnimation2.setDuration(paramInt2);
        }
    }

    private Animation createScaleUpAnimationLocked(int paramInt, boolean paramBoolean)
    {
        int i;
        AnimationSet localAnimationSet;
        switch (paramInt)
        {
        default:
            i = 300;
            if (paramBoolean)
            {
                float f1 = this.mNextAppTransitionStartWidth / this.mAppDisplayWidth;
                float f2 = this.mNextAppTransitionStartHeight / this.mAppDisplayHeight;
                ScaleAnimation localScaleAnimation = new ScaleAnimation(f1, 1.0F, f2, 1.0F, computePivot(this.mNextAppTransitionStartX, f1), computePivot(this.mNextAppTransitionStartY, f2));
                localScaleAnimation.setDuration(i);
                localAnimationSet = new AnimationSet(true);
                AlphaAnimation localAlphaAnimation = new AlphaAnimation(0.0F, 1.0F);
                localScaleAnimation.setDuration(i);
                localAnimationSet.addAnimation(localScaleAnimation);
                localAlphaAnimation.setDuration(i);
                localAnimationSet.addAnimation(localAlphaAnimation);
                localAnimationSet.setDetachWallpaper(true);
            }
            break;
        case 4102:
        case 8199:
        }
        for (Object localObject = localAnimationSet; ; localObject = createExitAnimationLocked(paramInt, i))
        {
            ((Animation)localObject).setFillAfter(true);
            ((Animation)localObject).setInterpolator(AnimationUtils.loadInterpolator(this.mContext, 17563651));
            ((Animation)localObject).initialize(this.mAppDisplayWidth, this.mAppDisplayHeight, this.mAppDisplayWidth, this.mAppDisplayHeight);
            return localObject;
            i = this.mContext.getResources().getInteger(17694720);
            break;
        }
    }

    private Animation createThumbnailAnimationLocked(int paramInt, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
    {
        int i = this.mNextAppTransitionThumbnail.getWidth();
        float f1;
        float f2;
        label38: int k;
        label48: int m;
        label86: Object localObject;
        if (i > 0)
        {
            f1 = i;
            int j = this.mNextAppTransitionThumbnail.getHeight();
            if (j <= 0)
                break label286;
            f2 = j;
            if (!paramBoolean3)
                break label292;
            k = 270;
            switch (paramInt)
            {
            default:
                if (paramBoolean3)
                {
                    m = 250;
                    if (!paramBoolean2)
                        break label324;
                    float f5 = this.mAppDisplayWidth / f1;
                    float f6 = this.mAppDisplayHeight / f2;
                    ScaleAnimation localScaleAnimation2 = new ScaleAnimation(1.0F, f5, 1.0F, f6, computePivot(this.mNextAppTransitionStartX, 1.0F / f5), computePivot(this.mNextAppTransitionStartY, 1.0F / f6));
                    AnimationSet localAnimationSet = new AnimationSet(true);
                    AlphaAnimation localAlphaAnimation = new AlphaAnimation(1.0F, 0.0F);
                    localScaleAnimation2.setDuration(m);
                    localScaleAnimation2.setInterpolator(new DecelerateInterpolator(1.5F));
                    localAnimationSet.addAnimation(localScaleAnimation2);
                    localAlphaAnimation.setDuration(m);
                    localAnimationSet.addAnimation(localAlphaAnimation);
                    localAnimationSet.setFillBefore(true);
                    if (k > 0)
                        localAnimationSet.setStartOffset(k);
                    localObject = localAnimationSet;
                }
                break;
            case 4102:
            case 8199:
            }
        }
        while (true)
        {
            ((Animation)localObject).setFillAfter(true);
            ((Animation)localObject).setInterpolator(AnimationUtils.loadInterpolator(this.mContext, 17563649));
            ((Animation)localObject).initialize(this.mAppDisplayWidth, this.mAppDisplayHeight, this.mAppDisplayWidth, this.mAppDisplayHeight);
            return localObject;
            f1 = 1.0F;
            break;
            label286: f2 = 1.0F;
            break label38;
            label292: k = 0;
            break label48;
            m = this.mContext.getResources().getInteger(17694720);
            break label86;
            m = 300;
            break label86;
            label324: if (paramBoolean1)
            {
                float f3 = f1 / this.mAppDisplayWidth;
                float f4 = f2 / this.mAppDisplayHeight;
                ScaleAnimation localScaleAnimation1 = new ScaleAnimation(f3, 1.0F, f4, 1.0F, computePivot(this.mNextAppTransitionStartX, f3), computePivot(this.mNextAppTransitionStartY, f4));
                localScaleAnimation1.setDuration(m);
                localScaleAnimation1.setInterpolator(new DecelerateInterpolator(1.5F));
                localScaleAnimation1.setFillBefore(true);
                if (k > 0)
                    localScaleAnimation1.setStartOffset(k);
                localObject = localScaleAnimation1;
            }
            else if (paramBoolean3)
            {
                localObject = new AlphaAnimation(1.0F, 0.0F);
                ((Animation)localObject).setStartOffset(0L);
                ((Animation)localObject).setDuration(k - 120);
                ((Animation)localObject).setBackgroundColor(-16777216);
            }
            else
            {
                localObject = createExitAnimationLocked(paramInt, m);
            }
        }
    }

    private int findIdxBasedOnAppTokens(WindowState paramWindowState)
    {
        ArrayList localArrayList = this.mWindows;
        int i = localArrayList.size();
        int j;
        if (i == 0)
            j = -1;
        while (true)
        {
            return j;
            for (j = i - 1; ; j--)
            {
                if (j < 0)
                    break label56;
                if (((WindowState)localArrayList.get(j)).mAppToken == paramWindowState.mAppToken)
                    break;
            }
            label56: j = -1;
        }
    }

    // ERROR //
    private WindowState findWindow(int paramInt)
    {
        // Byte code:
        //     0: iload_1
        //     1: bipush 255
        //     3: if_icmpne +12 -> 15
        //     6: aload_0
        //     7: invokespecial 1139	com/android/server/wm/WindowManagerService:getFocusedWindow	()Lcom/android/server/wm/WindowState;
        //     10: astore 7
        //     12: aload 7
        //     14: areturn
        //     15: aload_0
        //     16: getfield 378	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
        //     19: astore_2
        //     20: aload_2
        //     21: monitorenter
        //     22: aload_0
        //     23: getfield 395	com/android/server/wm/WindowManagerService:mWindows	Ljava/util/ArrayList;
        //     26: astore 4
        //     28: aload 4
        //     30: invokevirtual 806	java/util/ArrayList:size	()I
        //     33: istore 5
        //     35: iconst_0
        //     36: istore 6
        //     38: iload 6
        //     40: iload 5
        //     42: if_icmpge +40 -> 82
        //     45: aload 4
        //     47: iload 6
        //     49: invokevirtual 813	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     52: checkcast 403	com/android/server/wm/WindowState
        //     55: astore 7
        //     57: aload 7
        //     59: invokestatic 1144	java/lang/System:identityHashCode	(Ljava/lang/Object;)I
        //     62: iload_1
        //     63: if_icmpne +13 -> 76
        //     66: aload_2
        //     67: monitorexit
        //     68: goto -56 -> 12
        //     71: astore_3
        //     72: aload_2
        //     73: monitorexit
        //     74: aload_3
        //     75: athrow
        //     76: iinc 6 1
        //     79: goto -41 -> 38
        //     82: aload_2
        //     83: monitorexit
        //     84: aconst_null
        //     85: astore 7
        //     87: goto -75 -> 12
        //
        // Exception table:
        //     from	to	target	type
        //     22	74	71	finally
        //     82	84	71	finally
    }

    private int findWindowOffsetLocked(int paramInt)
    {
        int i = this.mWindows.size();
        int j;
        if (paramInt >= this.mAnimatingAppTokens.size())
        {
            int i2 = i;
            while (i2 > 0)
            {
                i2--;
                if (((WindowState)this.mWindows.get(i2)).getAppToken() != null)
                    j = i2 + 1;
            }
        }
        while (true)
        {
            return j;
            while (true)
            {
                if (paramInt <= 0)
                    break label244;
                AppWindowToken localAppWindowToken = (AppWindowToken)this.mAnimatingAppTokens.get(paramInt - 1);
                if (localAppWindowToken.sendingToBottom)
                {
                    paramInt--;
                }
                else
                {
                    int k = localAppWindowToken.windows.size();
                    label198: label200: label236: 
                    while (true)
                    {
                        if (k <= 0)
                            break label238;
                        k--;
                        WindowState localWindowState1 = (WindowState)localAppWindowToken.windows.get(k);
                        int m = localWindowState1.mChildWindows.size();
                        while (true)
                        {
                            if (m <= 0)
                                break label200;
                            m--;
                            WindowState localWindowState2 = (WindowState)localWindowState1.mChildWindows.get(m);
                            if (localWindowState2.mSubLayer >= 0)
                                for (int i1 = i - 1; ; i1--)
                                {
                                    if (i1 < 0)
                                        break label198;
                                    if (this.mWindows.get(i1) == localWindowState2)
                                    {
                                        j = i1 + 1;
                                        break;
                                    }
                                }
                        }
                        for (int n = i - 1; ; n--)
                        {
                            if (n < 0)
                                break label236;
                            if (this.mWindows.get(n) == localWindowState1)
                            {
                                j = n + 1;
                                break;
                            }
                        }
                    }
                    label238: paramInt--;
                }
            }
            label244: j = 0;
        }
    }

    private void finishUpdateFocusedWindowAfterAssignLayersLocked(boolean paramBoolean)
    {
        this.mInputMonitor.setInputFocusLw(this.mCurrentFocus, paramBoolean);
    }

    static float fixScale(float paramFloat)
    {
        if (paramFloat < 0.0F)
            paramFloat = 0.0F;
        while (true)
        {
            return Math.abs(paramFloat);
            if (paramFloat > 20.0F)
                paramFloat = 20.0F;
        }
    }

    private AttributeCache.Entry getCachedAnimations(WindowManager.LayoutParams paramLayoutParams)
    {
        String str;
        int i;
        if ((paramLayoutParams != null) && (paramLayoutParams.windowAnimations != 0))
            if (paramLayoutParams.packageName != null)
            {
                str = paramLayoutParams.packageName;
                i = paramLayoutParams.windowAnimations;
                if ((0xFF000000 & i) == 16777216)
                    str = "android";
            }
        for (AttributeCache.Entry localEntry = AttributeCache.instance().get(str, i, R.styleable.WindowAnimation); ; localEntry = null)
        {
            return localEntry;
            str = "android";
            break;
        }
    }

    private AttributeCache.Entry getCachedAnimations(String paramString, int paramInt)
    {
        if (paramString != null)
            if ((0xFF000000 & paramInt) == 16777216)
                paramString = "android";
        for (AttributeCache.Entry localEntry = AttributeCache.instance().get(paramString, paramInt, R.styleable.WindowAnimation); ; localEntry = null)
            return localEntry;
    }

    private WindowState getFocusedWindow()
    {
        synchronized (this.mWindowMap)
        {
            WindowState localWindowState = getFocusedWindowLocked();
            return localWindowState;
        }
    }

    private WindowState getFocusedWindowLocked()
    {
        return this.mCurrentFocus;
    }

    static int getPropertyInt(String[] paramArrayOfString, int paramInt1, int paramInt2, int paramInt3, DisplayMetrics paramDisplayMetrics)
    {
        String str;
        if (paramInt1 < paramArrayOfString.length)
        {
            str = paramArrayOfString[paramInt1];
            if ((str == null) || (str.length() <= 0));
        }
        while (true)
        {
            int i;
            try
            {
                int j = Integer.parseInt(str);
                i = j;
                return i;
            }
            catch (Exception localException)
            {
            }
            if (paramInt2 == 0)
                i = paramInt3;
            else
                i = (int)TypedValue.applyDimension(paramInt2, paramInt3, paramDisplayMetrics);
        }
    }

    private int handleAnimatingStoppedAndTransitionLocked()
    {
        this.mAppTransitionRunning = false;
        for (int i = -1 + this.mAnimatingAppTokens.size(); i >= 0; i--)
            ((AppWindowToken)this.mAnimatingAppTokens.get(i)).sendingToBottom = false;
        this.mAnimatingAppTokens.clear();
        this.mAnimatingAppTokens.addAll(this.mAppTokens);
        rebuildAppWindowListLocked();
        int j = 0x0 | 0x1;
        LayoutFields localLayoutFields = this.mInnerFields;
        localLayoutFields.mAdjResult = (0x2 | localLayoutFields.mAdjResult);
        moveInputMethodWindowsIfNeededLocked(true);
        this.mInnerFields.mWallpaperMayChange = true;
        this.mFocusMayChange = true;
        return j;
    }

    private void handleNotObscuredLocked(WindowState paramWindowState, long paramLong, int paramInt1, int paramInt2)
    {
        WindowManager.LayoutParams localLayoutParams = paramWindowState.mAttrs;
        int i = localLayoutParams.flags;
        boolean bool = paramWindowState.isDisplayedLw();
        if (paramWindowState.mHasSurface)
        {
            if ((i & 0x80) != 0)
                LayoutFields.access$1102(this.mInnerFields, paramWindowState.mSession);
            if ((!this.mInnerFields.mSyswin) && (paramWindowState.mAttrs.screenBrightness >= 0.0F) && (this.mInnerFields.mScreenBrightness < 0.0F))
                LayoutFields.access$1302(this.mInnerFields, paramWindowState.mAttrs.screenBrightness);
            if ((!this.mInnerFields.mSyswin) && (paramWindowState.mAttrs.buttonBrightness >= 0.0F) && (this.mInnerFields.mButtonBrightness < 0.0F))
                LayoutFields.access$1402(this.mInnerFields, paramWindowState.mAttrs.buttonBrightness);
            if ((bool) && ((localLayoutParams.type == 2008) || (localLayoutParams.type == 2004) || (localLayoutParams.type == 2010)))
                LayoutFields.access$1202(this.mInnerFields, true);
        }
        int j;
        if ((bool) && (paramWindowState.isOpaqueDrawn()))
        {
            j = 1;
            if ((j == 0) || (!paramWindowState.isFullscreen(paramInt1, paramInt2)))
                break label239;
            LayoutFields.access$1502(this.mInnerFields, true);
        }
        label239: WindowStateAnimator localWindowStateAnimator;
        do
        {
            do
            {
                return;
                j = 0;
                break;
            }
            while ((!bool) || ((i & 0x2) == 0) || ((paramWindowState.mAppToken != null) && (paramWindowState.mAppToken.hiddenRequested)) || (paramWindowState.mExiting) || (this.mInnerFields.mDimming));
            this.mInnerFields.mDimming = true;
            localWindowStateAnimator = paramWindowState.mWinAnimator;
        }
        while (this.mAnimator.isDimming(localWindowStateAnimator));
        int k;
        int m;
        label334: WindowAnimator localWindowAnimator;
        if (localLayoutParams.type == 2021)
        {
            k = this.mCurDisplayWidth;
            m = this.mCurDisplayHeight;
            localWindowAnimator = this.mAnimator;
            if (!paramWindowState.mExiting)
                break label377;
        }
        label377: for (float f = 0.0F; ; f = paramWindowState.mAttrs.dimAmount)
        {
            localWindowAnimator.startDimming(localWindowStateAnimator, f, k, m);
            break;
            k = paramInt1;
            m = paramInt2;
            break label334;
        }
    }

    private boolean isSystemSecure()
    {
        if (("1".equals(SystemProperties.get("ro.secure", "1"))) && ("0".equals(SystemProperties.get("ro.debuggable", "0"))));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private Animation loadAnimation(String paramString, int paramInt)
    {
        int i = 0;
        Context localContext = this.mContext;
        if (paramInt >= 0)
        {
            AttributeCache.Entry localEntry = getCachedAnimations(paramString, paramInt);
            if (localEntry != null)
            {
                localContext = localEntry.context;
                i = paramInt;
            }
        }
        if (i != 0);
        for (Animation localAnimation = AnimationUtils.loadAnimation(localContext, i); ; localAnimation = null)
            return localAnimation;
    }

    static void logSurface(Surface paramSurface, String paramString1, String paramString2, RuntimeException paramRuntimeException)
    {
        String str = "    SURFACE " + paramSurface + ": " + paramString2 + " / " + paramString1;
        if (paramRuntimeException != null)
            Slog.i("WindowManager", str, paramRuntimeException);
        while (true)
        {
            return;
            Slog.i("WindowManager", str);
        }
    }

    static void logSurface(WindowState paramWindowState, String paramString, RuntimeException paramRuntimeException)
    {
        String str = "    SURFACE " + paramString + ": " + paramWindowState;
        if (paramRuntimeException != null)
            Slog.i("WindowManager", str, paramRuntimeException);
        while (true)
        {
            return;
            Slog.i("WindowManager", str);
        }
    }

    public static WindowManagerService main(Context paramContext, PowerManagerService paramPowerManagerService, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
    {
        WMThread localWMThread = new WMThread(paramContext, paramPowerManagerService, paramBoolean1, paramBoolean2, paramBoolean3);
        localWMThread.start();
        try
        {
            while (true)
            {
                WindowManagerService localWindowManagerService1 = localWMThread.mService;
                if (localWindowManagerService1 != null)
                    break;
                try
                {
                    localWMThread.wait();
                }
                catch (InterruptedException localInterruptedException)
                {
                }
            }
            WindowManagerService localWindowManagerService2 = localWMThread.mService;
            return localWindowManagerService2;
        }
        finally
        {
        }
    }

    private void moveAppWindowsLocked(AppWindowToken paramAppWindowToken, int paramInt, boolean paramBoolean)
    {
        tmpRemoveAppWindowsLocked(paramAppWindowToken);
        reAddAppWindowsLocked(findWindowOffsetLocked(paramInt), paramAppWindowToken);
        if (paramBoolean)
        {
            this.mInputMonitor.setUpdateInputWindowsNeededLw();
            if (!updateFocusedWindowLocked(3, false))
                assignLayersLocked();
            this.mLayoutNeeded = true;
            if (!this.mInLayout)
                performLayoutAndPlaceSurfacesLocked();
            this.mInputMonitor.updateInputWindowsLw(false);
        }
    }

    private void moveAppWindowsLocked(List<IBinder> paramList, int paramInt)
    {
        int i = paramList.size();
        for (int j = 0; j < i; j++)
        {
            WindowToken localWindowToken2 = (WindowToken)this.mTokenMap.get(paramList.get(j));
            if (localWindowToken2 != null)
                tmpRemoveAppWindowsLocked(localWindowToken2);
        }
        int k = findWindowOffsetLocked(paramInt);
        for (int m = 0; m < i; m++)
        {
            WindowToken localWindowToken1 = (WindowToken)this.mTokenMap.get(paramList.get(m));
            if (localWindowToken1 != null)
                k = reAddAppWindowsLocked(k, localWindowToken1);
        }
        this.mInputMonitor.setUpdateInputWindowsNeededLw();
        if (!updateFocusedWindowLocked(3, false))
            assignLayersLocked();
        this.mLayoutNeeded = true;
        performLayoutAndPlaceSurfacesLocked();
        this.mInputMonitor.updateInputWindowsLw(false);
    }

    private void notifyFocusChanged()
    {
        synchronized (this.mWindowMap)
        {
            if (!this.mWindowChangeListeners.isEmpty())
            {
                WindowChangeListener[] arrayOfWindowChangeListener1 = new WindowChangeListener[this.mWindowChangeListeners.size()];
                WindowChangeListener[] arrayOfWindowChangeListener2 = (WindowChangeListener[])this.mWindowChangeListeners.toArray(arrayOfWindowChangeListener1);
                int i = arrayOfWindowChangeListener2.length;
                int j = 0;
                if (j < i)
                {
                    arrayOfWindowChangeListener2[j].focusChanged();
                    j++;
                }
            }
        }
    }

    private void notifyWindowsChanged()
    {
        synchronized (this.mWindowMap)
        {
            if (!this.mWindowChangeListeners.isEmpty())
            {
                WindowChangeListener[] arrayOfWindowChangeListener1 = new WindowChangeListener[this.mWindowChangeListeners.size()];
                WindowChangeListener[] arrayOfWindowChangeListener2 = (WindowChangeListener[])this.mWindowChangeListeners.toArray(arrayOfWindowChangeListener1);
                int i = arrayOfWindowChangeListener2.length;
                int j = 0;
                if (j < i)
                {
                    arrayOfWindowChangeListener2[j].windowsChanged();
                    j++;
                }
            }
        }
    }

    private final void performLayoutAndPlaceSurfacesLocked()
    {
        if (this.mInLayout)
            Slog.w("WindowManager", "performLayoutAndPlaceSurfacesLocked called while in layout. Callers=" + Debug.getCallers(3));
        while (true)
        {
            return;
            if ((this.mWaitingForConfig) || (this.mDisplay == null))
                continue;
            Trace.traceBegin(32L, "wmLayout");
            this.mInLayout = true;
            boolean bool = false;
            try
            {
                if (this.mForceRemoves != null)
                {
                    bool = true;
                    for (int m = 0; m < this.mForceRemoves.size(); m++)
                    {
                        WindowState localWindowState2 = (WindowState)this.mForceRemoves.get(m);
                        Slog.i("WindowManager", "Force removing: " + localWindowState2);
                        removeWindowInnerLocked(localWindowState2.mSession, localWindowState2);
                    }
                    this.mForceRemoves = null;
                    Slog.w("WindowManager", "Due to memory failure, waiting a bit for next layout");
                }
            }
            catch (RuntimeException localRuntimeException1)
            {
                try
                {
                    label183: synchronized (new Object())
                    {
                        ???.wait(250L);
                    }
                    try
                    {
                        while (true)
                        {
                            performLayoutAndPlaceSurfacesLockedInner(bool);
                            int i = this.mPendingRemove.size();
                            if (i <= 0)
                                break label421;
                            if (this.mPendingRemoveTmp.length < i)
                                this.mPendingRemoveTmp = new WindowState[i + 10];
                            this.mPendingRemove.toArray(this.mPendingRemoveTmp);
                            this.mPendingRemove.clear();
                            for (int k = 0; k < i; k++)
                            {
                                WindowState localWindowState1 = this.mPendingRemoveTmp[k];
                                removeWindowInnerLocked(localWindowState1.mSession, localWindowState1);
                            }
                            localObject2 = finally;
                            throw localObject2;
                            localRuntimeException1 = localRuntimeException1;
                            Log.wtf("WindowManager", "Unhandled exception while force removing for memory", localRuntimeException1);
                        }
                        this.mInLayout = false;
                        assignLayersLocked();
                        this.mLayoutNeeded = true;
                        Trace.traceEnd(32L);
                        performLayoutAndPlaceSurfacesLocked();
                        Trace.traceBegin(32L, "wmLayout");
                        while (true)
                        {
                            if (!this.mLayoutNeeded)
                                break label467;
                            int j = 1 + this.mLayoutRepeatCount;
                            this.mLayoutRepeatCount = j;
                            if (j >= 6)
                                break label450;
                            requestTraversalLocked();
                            if ((this.mWindowsChanged) && (!this.mWindowChangeListeners.isEmpty()))
                            {
                                this.mH.removeMessages(19);
                                this.mH.sendMessage(this.mH.obtainMessage(19));
                            }
                            Trace.traceEnd(32L);
                            break;
                            label421: this.mInLayout = false;
                        }
                    }
                    catch (RuntimeException localRuntimeException2)
                    {
                        while (true)
                        {
                            this.mInLayout = false;
                            Log.wtf("WindowManager", "Unhandled exception while laying out windows", localRuntimeException2);
                            continue;
                            label450: Slog.e("WindowManager", "Performed 6 layouts in a row. Skipping");
                            this.mLayoutRepeatCount = 0;
                            continue;
                            label467: this.mLayoutRepeatCount = 0;
                        }
                    }
                }
                catch (InterruptedException localInterruptedException)
                {
                    break label183;
                }
            }
        }
    }

    // ERROR //
    private final void performLayoutAndPlaceSurfacesLockedInner(boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 1406	com/android/server/wm/WindowManagerService:mDisplay	Landroid/view/Display;
        //     4: ifnonnull +13 -> 17
        //     7: ldc 117
        //     9: ldc_w 1470
        //     12: invokestatic 1338	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     15: pop
        //     16: return
        //     17: invokestatic 1476	android/os/SystemClock:uptimeMillis	()J
        //     20: lstore_2
        //     21: aload_0
        //     22: getfield 440	com/android/server/wm/WindowManagerService:mCurDisplayWidth	I
        //     25: istore 4
        //     27: aload_0
        //     28: getfield 442	com/android/server/wm/WindowManagerService:mCurDisplayHeight	I
        //     31: istore 5
        //     33: aload_0
        //     34: getfield 444	com/android/server/wm/WindowManagerService:mAppDisplayWidth	I
        //     37: istore 6
        //     39: aload_0
        //     40: getfield 446	com/android/server/wm/WindowManagerService:mAppDisplayHeight	I
        //     43: istore 7
        //     45: aload_0
        //     46: getfield 1227	com/android/server/wm/WindowManagerService:mFocusMayChange	Z
        //     49: ifeq +15 -> 64
        //     52: aload_0
        //     53: iconst_0
        //     54: putfield 1227	com/android/server/wm/WindowManagerService:mFocusMayChange	Z
        //     57: aload_0
        //     58: iconst_3
        //     59: iconst_0
        //     60: invokespecial 1366	com/android/server/wm/WindowManagerService:updateFocusedWindowLocked	(IZ)Z
        //     63: pop
        //     64: bipush 255
        //     66: aload_0
        //     67: getfield 385	com/android/server/wm/WindowManagerService:mExitingTokens	Ljava/util/ArrayList;
        //     70: invokevirtual 806	java/util/ArrayList:size	()I
        //     73: iadd
        //     74: istore 8
        //     76: iload 8
        //     78: iflt +25 -> 103
        //     81: aload_0
        //     82: getfield 385	com/android/server/wm/WindowManagerService:mExitingTokens	Ljava/util/ArrayList;
        //     85: iload 8
        //     87: invokevirtual 813	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     90: checkcast 834	com/android/server/wm/WindowToken
        //     93: iconst_0
        //     94: putfield 1479	com/android/server/wm/WindowToken:hasVisible	Z
        //     97: iinc 8 255
        //     100: goto -24 -> 76
        //     103: bipush 255
        //     105: aload_0
        //     106: getfield 391	com/android/server/wm/WindowManagerService:mExitingAppTokens	Ljava/util/ArrayList;
        //     109: invokevirtual 806	java/util/ArrayList:size	()I
        //     112: iadd
        //     113: istore 9
        //     115: iload 9
        //     117: iflt +25 -> 142
        //     120: aload_0
        //     121: getfield 391	com/android/server/wm/WindowManagerService:mExitingAppTokens	Ljava/util/ArrayList;
        //     124: iload 9
        //     126: invokevirtual 813	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     129: checkcast 815	com/android/server/wm/AppWindowToken
        //     132: iconst_0
        //     133: putfield 1479	com/android/server/wm/WindowToken:hasVisible	Z
        //     136: iinc 9 255
        //     139: goto -24 -> 115
        //     142: aload_0
        //     143: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     146: aconst_null
        //     147: invokestatic 1242	com/android/server/wm/WindowManagerService$LayoutFields:access$1102	(Lcom/android/server/wm/WindowManagerService$LayoutFields;Lcom/android/server/wm/Session;)Lcom/android/server/wm/Session;
        //     150: pop
        //     151: aload_0
        //     152: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     155: ldc_w 553
        //     158: invokestatic 1257	com/android/server/wm/WindowManagerService$LayoutFields:access$1302	(Lcom/android/server/wm/WindowManagerService$LayoutFields;F)F
        //     161: pop
        //     162: aload_0
        //     163: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     166: ldc_w 553
        //     169: invokestatic 1266	com/android/server/wm/WindowManagerService$LayoutFields:access$1402	(Lcom/android/server/wm/WindowManagerService$LayoutFields;F)F
        //     172: pop
        //     173: aload_0
        //     174: iconst_1
        //     175: aload_0
        //     176: getfield 1481	com/android/server/wm/WindowManagerService:mTransactionSequence	I
        //     179: iadd
        //     180: putfield 1481	com/android/server/wm/WindowManagerService:mTransactionSequence	I
        //     183: invokestatic 761	android/view/Surface:openTransaction	()V
        //     186: aload_0
        //     187: getfield 1483	com/android/server/wm/WindowManagerService:mWatermark	Lcom/android/server/wm/Watermark;
        //     190: ifnull +14 -> 204
        //     193: aload_0
        //     194: getfield 1483	com/android/server/wm/WindowManagerService:mWatermark	Lcom/android/server/wm/Watermark;
        //     197: iload 4
        //     199: iload 5
        //     201: invokevirtual 1489	com/android/server/wm/Watermark:positionSurface	(II)V
        //     204: aload_0
        //     205: getfield 1491	com/android/server/wm/WindowManagerService:mStrictModeFlash	Lcom/android/server/wm/StrictModeFlash;
        //     208: ifnull +14 -> 222
        //     211: aload_0
        //     212: getfield 1491	com/android/server/wm/WindowManagerService:mStrictModeFlash	Lcom/android/server/wm/StrictModeFlash;
        //     215: iload 4
        //     217: iload 5
        //     219: invokevirtual 1494	com/android/server/wm/StrictModeFlash:positionSurface	(II)V
        //     222: iconst_0
        //     223: istore 13
        //     225: iinc 13 1
        //     228: iload 13
        //     230: bipush 6
        //     232: if_icmple +532 -> 764
        //     235: ldc 117
        //     237: ldc_w 1496
        //     240: invokestatic 1404	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     243: pop
        //     244: aload_0
        //     245: iconst_0
        //     246: putfield 473	com/android/server/wm/WindowManagerService:mLayoutNeeded	Z
        //     249: aload_0
        //     250: getfield 409	com/android/server/wm/WindowManagerService:mLosingFocus	Ljava/util/ArrayList;
        //     253: invokevirtual 1379	java/util/ArrayList:isEmpty	()Z
        //     256: ifne +2148 -> 2404
        //     259: iconst_1
        //     260: istore 53
        //     262: aload_0
        //     263: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     266: iconst_0
        //     267: invokestatic 1280	com/android/server/wm/WindowManagerService$LayoutFields:access$1502	(Lcom/android/server/wm/WindowManagerService$LayoutFields;Z)Z
        //     270: pop
        //     271: aload_0
        //     272: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     275: iconst_0
        //     276: putfield 1289	com/android/server/wm/WindowManagerService$LayoutFields:mDimming	Z
        //     279: aload_0
        //     280: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     283: iconst_0
        //     284: invokestatic 1270	com/android/server/wm/WindowManagerService$LayoutFields:access$1202	(Lcom/android/server/wm/WindowManagerService$LayoutFields;Z)Z
        //     287: pop
        //     288: iconst_0
        //     289: istore 56
        //     291: iconst_0
        //     292: istore 57
        //     294: bipush 255
        //     296: aload_0
        //     297: getfield 395	com/android/server/wm/WindowManagerService:mWindows	Ljava/util/ArrayList;
        //     300: invokevirtual 806	java/util/ArrayList:size	()I
        //     303: iadd
        //     304: istore 58
        //     306: iload 58
        //     308: iflt +1209 -> 1517
        //     311: aload_0
        //     312: getfield 395	com/android/server/wm/WindowManagerService:mWindows	Ljava/util/ArrayList;
        //     315: iload 58
        //     317: invokevirtual 813	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     320: checkcast 403	com/android/server/wm/WindowState
        //     323: astore 60
        //     325: aload 60
        //     327: getfield 1499	com/android/server/wm/WindowState:mObscured	Z
        //     330: aload_0
        //     331: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     334: invokestatic 1502	com/android/server/wm/WindowManagerService$LayoutFields:access$1500	(Lcom/android/server/wm/WindowManagerService$LayoutFields;)Z
        //     337: if_icmpeq +2073 -> 2410
        //     340: iconst_1
        //     341: istore 61
        //     343: aload 60
        //     345: aload_0
        //     346: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     349: invokestatic 1502	com/android/server/wm/WindowManagerService$LayoutFields:access$1500	(Lcom/android/server/wm/WindowManagerService$LayoutFields;)Z
        //     352: putfield 1499	com/android/server/wm/WindowState:mObscured	Z
        //     355: aload_0
        //     356: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     359: invokestatic 1502	com/android/server/wm/WindowManagerService$LayoutFields:access$1500	(Lcom/android/server/wm/WindowManagerService$LayoutFields;)Z
        //     362: ifne +14 -> 376
        //     365: aload_0
        //     366: aload 60
        //     368: lload_2
        //     369: iload 6
        //     371: iload 7
        //     373: invokespecial 1504	com/android/server/wm/WindowManagerService:handleNotObscuredLocked	(Lcom/android/server/wm/WindowState;JII)V
        //     376: iload 61
        //     378: ifeq +24 -> 402
        //     381: aload_0
        //     382: getfield 548	com/android/server/wm/WindowManagerService:mWallpaperTarget	Lcom/android/server/wm/WindowState;
        //     385: aload 60
        //     387: if_acmpne +15 -> 402
        //     390: aload 60
        //     392: invokevirtual 1507	com/android/server/wm/WindowState:isVisibleLw	()Z
        //     395: ifeq +7 -> 402
        //     398: aload_0
        //     399: invokevirtual 1510	com/android/server/wm/WindowManagerService:updateWallpaperVisibilityLocked	()V
        //     402: aload 60
        //     404: getfield 959	com/android/server/wm/WindowState:mWinAnimator	Lcom/android/server/wm/WindowStateAnimator;
        //     407: astore 62
        //     409: aload 60
        //     411: getfield 1235	com/android/server/wm/WindowState:mHasSurface	Z
        //     414: ifeq +70 -> 484
        //     417: aload 60
        //     419: invokevirtual 1513	com/android/server/wm/WindowState:shouldAnimateMove	()Z
        //     422: ifeq +62 -> 484
        //     425: aload 62
        //     427: aload_0
        //     428: getfield 596	com/android/server/wm/WindowManagerService:mContext	Landroid/content/Context;
        //     431: ldc_w 1514
        //     434: invokestatic 1307	android/view/animation/AnimationUtils:loadAnimation	(Landroid/content/Context;I)Landroid/view/animation/Animation;
        //     437: invokevirtual 1516	com/android/server/wm/WindowStateAnimator:setAnimation	(Landroid/view/animation/Animation;)V
        //     440: aload 62
        //     442: aload 60
        //     444: getfield 1519	com/android/server/wm/WindowState:mLastFrame	Landroid/graphics/Rect;
        //     447: getfield 1522	android/graphics/Rect:left	I
        //     450: aload 60
        //     452: getfield 1525	com/android/server/wm/WindowState:mFrame	Landroid/graphics/Rect;
        //     455: getfield 1522	android/graphics/Rect:left	I
        //     458: isub
        //     459: putfield 1528	com/android/server/wm/WindowStateAnimator:mAnimDw	I
        //     462: aload 62
        //     464: aload 60
        //     466: getfield 1519	com/android/server/wm/WindowState:mLastFrame	Landroid/graphics/Rect;
        //     469: getfield 1531	android/graphics/Rect:top	I
        //     472: aload 60
        //     474: getfield 1525	com/android/server/wm/WindowState:mFrame	Landroid/graphics/Rect;
        //     477: getfield 1531	android/graphics/Rect:top	I
        //     480: isub
        //     481: putfield 1534	com/android/server/wm/WindowStateAnimator:mAnimDh	I
        //     484: aload 60
        //     486: iconst_0
        //     487: putfield 1537	com/android/server/wm/WindowState:mContentChanged	Z
        //     490: aload 60
        //     492: getfield 1235	com/android/server/wm/WindowState:mHasSurface	Z
        //     495: ifeq +232 -> 727
        //     498: aload 62
        //     500: lload_2
        //     501: invokevirtual 1541	com/android/server/wm/WindowStateAnimator:commitFinishDrawingLocked	(J)Z
        //     504: ifeq +47 -> 551
        //     507: ldc_w 1542
        //     510: aload 60
        //     512: getfield 844	com/android/server/wm/WindowState:mAttrs	Landroid/view/WindowManager$LayoutParams;
        //     515: getfield 996	android/view/WindowManager$LayoutParams:flags	I
        //     518: iand
        //     519: ifeq +32 -> 551
        //     522: aload_0
        //     523: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     526: iconst_1
        //     527: putfield 1225	com/android/server/wm/WindowManagerService$LayoutFields:mWallpaperMayChange	Z
        //     530: aload_0
        //     531: iconst_4
        //     532: aload_0
        //     533: getfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     536: ior
        //     537: putfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     540: aload_0
        //     541: ldc_w 1544
        //     544: aload_0
        //     545: getfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     548: invokevirtual 1548	com/android/server/wm/WindowManagerService:debugLayoutRepeats	(Ljava/lang/String;I)V
        //     551: aload 62
        //     553: iload_1
        //     554: invokevirtual 1551	com/android/server/wm/WindowStateAnimator:setSurfaceBoundaries	(Z)V
        //     557: aload 60
        //     559: getfield 856	com/android/server/wm/WindowState:mAppToken	Lcom/android/server/wm/AppWindowToken;
        //     562: astore 63
        //     564: aload 63
        //     566: ifnull +161 -> 727
        //     569: aload 63
        //     571: getfield 1554	com/android/server/wm/AppWindowToken:allDrawn	Z
        //     574: ifeq +14 -> 588
        //     577: aload 63
        //     579: getfield 928	com/android/server/wm/AppWindowToken:mAppAnimator	Lcom/android/server/wm/AppWindowAnimator;
        //     582: getfield 1557	com/android/server/wm/AppWindowAnimator:freezingScreen	Z
        //     585: ifeq +142 -> 727
        //     588: aload 63
        //     590: getfield 1560	com/android/server/wm/AppWindowToken:lastTransactionSequence	J
        //     593: aload_0
        //     594: getfield 1481	com/android/server/wm/WindowManagerService:mTransactionSequence	I
        //     597: i2l
        //     598: lcmp
        //     599: ifeq +31 -> 630
        //     602: aload 63
        //     604: aload_0
        //     605: getfield 1481	com/android/server/wm/WindowManagerService:mTransactionSequence	I
        //     608: i2l
        //     609: putfield 1560	com/android/server/wm/AppWindowToken:lastTransactionSequence	J
        //     612: aload 63
        //     614: iconst_0
        //     615: putfield 1563	com/android/server/wm/AppWindowToken:numDrawnWindows	I
        //     618: aload 63
        //     620: iconst_0
        //     621: putfield 1566	com/android/server/wm/AppWindowToken:numInterestingWindows	I
        //     624: aload 63
        //     626: iconst_0
        //     627: putfield 1569	com/android/server/wm/AppWindowToken:startingDisplayed	Z
        //     630: aload 60
        //     632: invokevirtual 1572	com/android/server/wm/WindowState:isOnScreen	()Z
        //     635: ifne +12 -> 647
        //     638: aload 62
        //     640: getfield 1575	com/android/server/wm/WindowStateAnimator:mAttrType	I
        //     643: iconst_1
        //     644: if_icmpne +83 -> 727
        //     647: aload 60
        //     649: getfield 1286	com/android/server/wm/WindowState:mExiting	Z
        //     652: ifne +75 -> 727
        //     655: aload 60
        //     657: getfield 1578	com/android/server/wm/WindowState:mDestroying	Z
        //     660: ifne +67 -> 727
        //     663: aload 60
        //     665: aload 63
        //     667: getfield 864	com/android/server/wm/AppWindowToken:startingWindow	Lcom/android/server/wm/WindowState;
        //     670: if_acmpeq +822 -> 1492
        //     673: aload 63
        //     675: getfield 928	com/android/server/wm/AppWindowToken:mAppAnimator	Lcom/android/server/wm/AppWindowAnimator;
        //     678: getfield 1557	com/android/server/wm/AppWindowAnimator:freezingScreen	Z
        //     681: ifeq +11 -> 692
        //     684: aload 60
        //     686: getfield 1581	com/android/server/wm/WindowState:mAppFreezing	Z
        //     689: ifne +38 -> 727
        //     692: aload 63
        //     694: iconst_1
        //     695: aload 63
        //     697: getfield 1566	com/android/server/wm/AppWindowToken:numInterestingWindows	I
        //     700: iadd
        //     701: putfield 1566	com/android/server/wm/AppWindowToken:numInterestingWindows	I
        //     704: aload 60
        //     706: invokevirtual 1584	com/android/server/wm/WindowState:isDrawnLw	()Z
        //     709: ifeq +18 -> 727
        //     712: aload 63
        //     714: iconst_1
        //     715: aload 63
        //     717: getfield 1563	com/android/server/wm/AppWindowToken:numDrawnWindows	I
        //     720: iadd
        //     721: putfield 1563	com/android/server/wm/AppWindowToken:numDrawnWindows	I
        //     724: iconst_1
        //     725: istore 57
        //     727: iload 53
        //     729: ifeq +23 -> 752
        //     732: aload 60
        //     734: aload_0
        //     735: getfield 536	com/android/server/wm/WindowManagerService:mCurrentFocus	Lcom/android/server/wm/WindowState;
        //     738: if_acmpne +14 -> 752
        //     741: aload 60
        //     743: invokevirtual 1232	com/android/server/wm/WindowState:isDisplayedLw	()Z
        //     746: ifeq +6 -> 752
        //     749: iconst_1
        //     750: istore 56
        //     752: aload_0
        //     753: aload 60
        //     755: invokespecial 1588	com/android/server/wm/WindowManagerService:updateResizingWindows	(Lcom/android/server/wm/WindowState;)V
        //     758: iinc 58 255
        //     761: goto -455 -> 306
        //     764: aload_0
        //     765: ldc_w 1590
        //     768: aload_0
        //     769: getfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     772: invokevirtual 1548	com/android/server/wm/WindowManagerService:debugLayoutRepeats	(Ljava/lang/String;I)V
        //     775: iconst_4
        //     776: aload_0
        //     777: getfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     780: iand
        //     781: ifeq +21 -> 802
        //     784: iconst_2
        //     785: aload_0
        //     786: invokevirtual 911	com/android/server/wm/WindowManagerService:adjustWallpaperWindowsLocked	()I
        //     789: iand
        //     790: ifeq +12 -> 802
        //     793: aload_0
        //     794: invokespecial 1368	com/android/server/wm/WindowManagerService:assignLayersLocked	()V
        //     797: aload_0
        //     798: iconst_1
        //     799: putfield 473	com/android/server/wm/WindowManagerService:mLayoutNeeded	Z
        //     802: iconst_2
        //     803: aload_0
        //     804: getfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     807: iand
        //     808: ifeq +26 -> 834
        //     811: aload_0
        //     812: iconst_1
        //     813: invokevirtual 1593	com/android/server/wm/WindowManagerService:updateOrientationFromAppTokensLocked	(Z)Z
        //     816: ifeq +18 -> 834
        //     819: aload_0
        //     820: iconst_1
        //     821: putfield 473	com/android/server/wm/WindowManagerService:mLayoutNeeded	Z
        //     824: aload_0
        //     825: getfield 526	com/android/server/wm/WindowManagerService:mH	Lcom/android/server/wm/WindowManagerService$H;
        //     828: bipush 18
        //     830: invokevirtual 1596	com/android/server/wm/WindowManagerService$H:sendEmptyMessage	(I)Z
        //     833: pop
        //     834: iconst_1
        //     835: aload_0
        //     836: getfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     839: iand
        //     840: ifeq +1528 -> 2368
        //     843: aload_0
        //     844: iconst_1
        //     845: putfield 473	com/android/server/wm/WindowManagerService:mLayoutNeeded	Z
        //     848: goto +1520 -> 2368
        //     851: aload_0
        //     852: iload 65
        //     854: iconst_0
        //     855: invokespecial 1600	com/android/server/wm/WindowManagerService:performLayoutLockedInner	(ZZ)V
        //     858: aload_0
        //     859: iconst_0
        //     860: putfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     863: aload_0
        //     864: new 1311	java/lang/StringBuilder
        //     867: dup
        //     868: invokespecial 1312	java/lang/StringBuilder:<init>	()V
        //     871: ldc_w 1602
        //     874: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     877: aload_0
        //     878: getfield 1445	com/android/server/wm/WindowManagerService:mLayoutRepeatCount	I
        //     881: invokevirtual 1605	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     884: invokevirtual 1329	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     887: aload_0
        //     888: getfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     891: invokevirtual 1548	com/android/server/wm/WindowManagerService:debugLayoutRepeats	(Ljava/lang/String;I)V
        //     894: aload_0
        //     895: getfield 368	com/android/server/wm/WindowManagerService:mPolicy	Landroid/view/WindowManagerPolicy;
        //     898: iload 4
        //     900: iload 5
        //     902: invokeinterface 1608 3 0
        //     907: bipush 255
        //     909: aload_0
        //     910: getfield 395	com/android/server/wm/WindowManagerService:mWindows	Ljava/util/ArrayList;
        //     913: invokevirtual 806	java/util/ArrayList:size	()I
        //     916: iadd
        //     917: istore 52
        //     919: iload 52
        //     921: iflt +532 -> 1453
        //     924: aload_0
        //     925: getfield 395	com/android/server/wm/WindowManagerService:mWindows	Ljava/util/ArrayList;
        //     928: iload 52
        //     930: invokevirtual 813	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     933: checkcast 403	com/android/server/wm/WindowState
        //     936: astore 64
        //     938: aload 64
        //     940: getfield 1235	com/android/server/wm/WindowState:mHasSurface	Z
        //     943: ifeq +1443 -> 2386
        //     946: aload_0
        //     947: getfield 368	com/android/server/wm/WindowManagerService:mPolicy	Landroid/view/WindowManagerPolicy;
        //     950: aload 64
        //     952: aload 64
        //     954: getfield 844	com/android/server/wm/WindowState:mAttrs	Landroid/view/WindowManager$LayoutParams;
        //     957: invokeinterface 1612 3 0
        //     962: goto +1424 -> 2386
        //     965: ldc 117
        //     967: ldc_w 1614
        //     970: invokestatic 1404	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     973: pop
        //     974: goto -116 -> 858
        //     977: astore 15
        //     979: ldc 117
        //     981: ldc_w 1616
        //     984: aload 15
        //     986: invokestatic 1440	android/util/Log:wtf	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     989: pop
        //     990: invokestatic 767	android/view/Surface:closeTransaction	()V
        //     993: aload_0
        //     994: getfield 500	com/android/server/wm/WindowManagerService:mAppTransitionReady	Z
        //     997: ifeq +27 -> 1024
        //     1000: aload_0
        //     1001: aload_0
        //     1002: getfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     1005: aload_0
        //     1006: invokevirtual 1619	com/android/server/wm/WindowManagerService:handleAppTransitionReadyLocked	()I
        //     1009: ior
        //     1010: putfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     1013: aload_0
        //     1014: ldc_w 1621
        //     1017: aload_0
        //     1018: getfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     1021: invokevirtual 1548	com/android/server/wm/WindowManagerService:debugLayoutRepeats	(Ljava/lang/String;I)V
        //     1024: aload_0
        //     1025: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     1028: iconst_0
        //     1029: putfield 908	com/android/server/wm/WindowManagerService$LayoutFields:mAdjResult	I
        //     1032: aload_0
        //     1033: getfield 729	com/android/server/wm/WindowManagerService:mAnimator	Lcom/android/server/wm/WindowAnimator;
        //     1036: getfield 1624	com/android/server/wm/WindowAnimator:mAnimating	Z
        //     1039: ifne +34 -> 1073
        //     1042: aload_0
        //     1043: getfield 502	com/android/server/wm/WindowManagerService:mAppTransitionRunning	Z
        //     1046: ifeq +27 -> 1073
        //     1049: aload_0
        //     1050: aload_0
        //     1051: getfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     1054: aload_0
        //     1055: invokespecial 1626	com/android/server/wm/WindowManagerService:handleAnimatingStoppedAndTransitionLocked	()I
        //     1058: ior
        //     1059: putfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     1062: aload_0
        //     1063: ldc_w 1628
        //     1066: aload_0
        //     1067: getfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     1070: invokevirtual 1548	com/android/server/wm/WindowManagerService:debugLayoutRepeats	(Ljava/lang/String;I)V
        //     1073: aload_0
        //     1074: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     1077: getfield 1631	com/android/server/wm/WindowManagerService$LayoutFields:mWallpaperForceHidingChanged	Z
        //     1080: ifeq +41 -> 1121
        //     1083: aload_0
        //     1084: getfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     1087: ifne +34 -> 1121
        //     1090: aload_0
        //     1091: getfield 500	com/android/server/wm/WindowManagerService:mAppTransitionReady	Z
        //     1094: ifne +27 -> 1121
        //     1097: aload_0
        //     1098: aload_0
        //     1099: getfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     1102: aload_0
        //     1103: invokespecial 1633	com/android/server/wm/WindowManagerService:animateAwayWallpaperLocked	()I
        //     1106: ior
        //     1107: putfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     1110: aload_0
        //     1111: ldc_w 1635
        //     1114: aload_0
        //     1115: getfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     1118: invokevirtual 1548	com/android/server/wm/WindowManagerService:debugLayoutRepeats	(Ljava/lang/String;I)V
        //     1121: aload_0
        //     1122: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     1125: iconst_0
        //     1126: putfield 1631	com/android/server/wm/WindowManagerService$LayoutFields:mWallpaperForceHidingChanged	Z
        //     1129: aload_0
        //     1130: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     1133: getfield 1225	com/android/server/wm/WindowManagerService$LayoutFields:mWallpaperMayChange	Z
        //     1136: ifeq +24 -> 1160
        //     1139: aload_0
        //     1140: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     1143: astore 50
        //     1145: aload 50
        //     1147: aload 50
        //     1149: getfield 908	com/android/server/wm/WindowManagerService$LayoutFields:mAdjResult	I
        //     1152: aload_0
        //     1153: invokevirtual 911	com/android/server/wm/WindowManagerService:adjustWallpaperWindowsLocked	()I
        //     1156: ior
        //     1157: putfield 908	com/android/server/wm/WindowManagerService$LayoutFields:mAdjResult	I
        //     1160: iconst_2
        //     1161: aload_0
        //     1162: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     1165: getfield 908	com/android/server/wm/WindowManagerService$LayoutFields:mAdjResult	I
        //     1168: iand
        //     1169: ifeq +404 -> 1573
        //     1172: aload_0
        //     1173: iconst_1
        //     1174: aload_0
        //     1175: getfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     1178: ior
        //     1179: putfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     1182: aload_0
        //     1183: invokespecial 1368	com/android/server/wm/WindowManagerService:assignLayersLocked	()V
        //     1186: aload_0
        //     1187: getfield 1227	com/android/server/wm/WindowManagerService:mFocusMayChange	Z
        //     1190: ifeq +36 -> 1226
        //     1193: aload_0
        //     1194: iconst_0
        //     1195: putfield 1227	com/android/server/wm/WindowManagerService:mFocusMayChange	Z
        //     1198: aload_0
        //     1199: iconst_2
        //     1200: iconst_0
        //     1201: invokespecial 1366	com/android/server/wm/WindowManagerService:updateFocusedWindowLocked	(IZ)Z
        //     1204: ifeq +22 -> 1226
        //     1207: aload_0
        //     1208: bipush 8
        //     1210: aload_0
        //     1211: getfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     1214: ior
        //     1215: putfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     1218: aload_0
        //     1219: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     1222: iconst_0
        //     1223: putfield 908	com/android/server/wm/WindowManagerService$LayoutFields:mAdjResult	I
        //     1226: aload_0
        //     1227: getfield 473	com/android/server/wm/WindowManagerService:mLayoutNeeded	Z
        //     1230: ifeq +24 -> 1254
        //     1233: aload_0
        //     1234: iconst_1
        //     1235: aload_0
        //     1236: getfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     1239: ior
        //     1240: putfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     1243: aload_0
        //     1244: ldc_w 1636
        //     1247: aload_0
        //     1248: getfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     1251: invokevirtual 1548	com/android/server/wm/WindowManagerService:debugLayoutRepeats	(Ljava/lang/String;I)V
        //     1254: aload_0
        //     1255: getfield 399	com/android/server/wm/WindowManagerService:mResizingWindows	Ljava/util/ArrayList;
        //     1258: invokevirtual 1379	java/util/ArrayList:isEmpty	()Z
        //     1261: ifne +373 -> 1634
        //     1264: bipush 255
        //     1266: aload_0
        //     1267: getfield 399	com/android/server/wm/WindowManagerService:mResizingWindows	Ljava/util/ArrayList;
        //     1270: invokevirtual 806	java/util/ArrayList:size	()I
        //     1273: iadd
        //     1274: istore 38
        //     1276: iload 38
        //     1278: iflt +349 -> 1627
        //     1281: aload_0
        //     1282: getfield 399	com/android/server/wm/WindowManagerService:mResizingWindows	Ljava/util/ArrayList;
        //     1285: iload 38
        //     1287: invokevirtual 813	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     1290: checkcast 403	com/android/server/wm/WindowState
        //     1293: astore 39
        //     1295: aload 39
        //     1297: getfield 959	com/android/server/wm/WindowState:mWinAnimator	Lcom/android/server/wm/WindowStateAnimator;
        //     1300: astore 40
        //     1302: aload 39
        //     1304: getfield 1639	com/android/server/wm/WindowState:mConfiguration	Landroid/content/res/Configuration;
        //     1307: aload_0
        //     1308: getfield 494	com/android/server/wm/WindowManagerService:mCurConfiguration	Landroid/content/res/Configuration;
        //     1311: if_acmpeq +287 -> 1598
        //     1314: aload 39
        //     1316: getfield 1639	com/android/server/wm/WindowState:mConfiguration	Landroid/content/res/Configuration;
        //     1319: ifnull +1079 -> 2398
        //     1322: aload_0
        //     1323: getfield 494	com/android/server/wm/WindowManagerService:mCurConfiguration	Landroid/content/res/Configuration;
        //     1326: aload 39
        //     1328: getfield 1639	com/android/server/wm/WindowState:mConfiguration	Landroid/content/res/Configuration;
        //     1331: invokevirtual 1643	android/content/res/Configuration:diff	(Landroid/content/res/Configuration;)I
        //     1334: ifeq +264 -> 1598
        //     1337: goto +1061 -> 2398
        //     1340: aload 39
        //     1342: aload_0
        //     1343: getfield 494	com/android/server/wm/WindowManagerService:mCurConfiguration	Landroid/content/res/Configuration;
        //     1346: putfield 1639	com/android/server/wm/WindowState:mConfiguration	Landroid/content/res/Configuration;
        //     1349: aload 39
        //     1351: getfield 825	com/android/server/wm/WindowState:mClient	Landroid/view/IWindow;
        //     1354: astore 43
        //     1356: aload 40
        //     1358: getfield 1646	com/android/server/wm/WindowStateAnimator:mSurfaceW	F
        //     1361: f2i
        //     1362: istore 44
        //     1364: aload 40
        //     1366: getfield 1649	com/android/server/wm/WindowStateAnimator:mSurfaceH	F
        //     1369: f2i
        //     1370: istore 45
        //     1372: aload 39
        //     1374: getfield 1652	com/android/server/wm/WindowState:mLastContentInsets	Landroid/graphics/Rect;
        //     1377: astore 46
        //     1379: aload 39
        //     1381: getfield 1655	com/android/server/wm/WindowState:mLastVisibleInsets	Landroid/graphics/Rect;
        //     1384: astore 47
        //     1386: aload 40
        //     1388: getfield 1658	com/android/server/wm/WindowStateAnimator:mDrawState	I
        //     1391: iconst_1
        //     1392: if_icmpne +212 -> 1604
        //     1395: iconst_1
        //     1396: istore 48
        //     1398: iload 42
        //     1400: ifeq +210 -> 1610
        //     1403: aload 39
        //     1405: getfield 1639	com/android/server/wm/WindowState:mConfiguration	Landroid/content/res/Configuration;
        //     1408: astore 49
        //     1410: aload 43
        //     1412: iload 44
        //     1414: iload 45
        //     1416: aload 46
        //     1418: aload 47
        //     1420: iload 48
        //     1422: aload 49
        //     1424: invokeinterface 1662 7 0
        //     1429: aload 39
        //     1431: iconst_0
        //     1432: putfield 1665	com/android/server/wm/WindowState:mContentInsetsChanged	Z
        //     1435: aload 39
        //     1437: iconst_0
        //     1438: putfield 1668	com/android/server/wm/WindowState:mVisibleInsetsChanged	Z
        //     1441: aload 40
        //     1443: iconst_0
        //     1444: putfield 1671	com/android/server/wm/WindowStateAnimator:mSurfaceResized	Z
        //     1447: iinc 38 255
        //     1450: goto -174 -> 1276
        //     1453: aload_0
        //     1454: aload_0
        //     1455: getfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     1458: aload_0
        //     1459: getfield 368	com/android/server/wm/WindowManagerService:mPolicy	Landroid/view/WindowManagerPolicy;
        //     1462: invokeinterface 1674 1 0
        //     1467: ior
        //     1468: putfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     1471: aload_0
        //     1472: ldc_w 1676
        //     1475: aload_0
        //     1476: getfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     1479: invokevirtual 1548	com/android/server/wm/WindowManagerService:debugLayoutRepeats	(Ljava/lang/String;I)V
        //     1482: aload_0
        //     1483: getfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     1486: ifne -1261 -> 225
        //     1489: goto -1240 -> 249
        //     1492: aload 60
        //     1494: invokevirtual 1584	com/android/server/wm/WindowState:isDrawnLw	()Z
        //     1497: ifeq -770 -> 727
        //     1500: aload 63
        //     1502: iconst_1
        //     1503: putfield 1569	com/android/server/wm/AppWindowToken:startingDisplayed	Z
        //     1506: goto -779 -> 727
        //     1509: astore 14
        //     1511: invokestatic 767	android/view/Surface:closeTransaction	()V
        //     1514: aload 14
        //     1516: athrow
        //     1517: iload 57
        //     1519: ifeq +7 -> 1526
        //     1522: aload_0
        //     1523: invokespecial 1679	com/android/server/wm/WindowManagerService:updateAllDrawnLocked	()V
        //     1526: iload 56
        //     1528: ifeq +12 -> 1540
        //     1531: aload_0
        //     1532: getfield 526	com/android/server/wm/WindowManagerService:mH	Lcom/android/server/wm/WindowManagerService$H;
        //     1535: iconst_3
        //     1536: invokevirtual 1596	com/android/server/wm/WindowManagerService$H:sendEmptyMessage	(I)Z
        //     1539: pop
        //     1540: aload_0
        //     1541: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     1544: getfield 1289	com/android/server/wm/WindowManagerService$LayoutFields:mDimming	Z
        //     1547: ifne +20 -> 1567
        //     1550: aload_0
        //     1551: getfield 729	com/android/server/wm/WindowManagerService:mAnimator	Lcom/android/server/wm/WindowAnimator;
        //     1554: invokevirtual 1681	com/android/server/wm/WindowAnimator:isDimming	()Z
        //     1557: ifeq +10 -> 1567
        //     1560: aload_0
        //     1561: getfield 729	com/android/server/wm/WindowManagerService:mAnimator	Lcom/android/server/wm/WindowAnimator;
        //     1564: invokevirtual 1684	com/android/server/wm/WindowAnimator:stopDimming	()V
        //     1567: invokestatic 767	android/view/Surface:closeTransaction	()V
        //     1570: goto -577 -> 993
        //     1573: iconst_4
        //     1574: aload_0
        //     1575: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     1578: getfield 908	com/android/server/wm/WindowManagerService$LayoutFields:mAdjResult	I
        //     1581: iand
        //     1582: ifeq -396 -> 1186
        //     1585: aload_0
        //     1586: iconst_1
        //     1587: aload_0
        //     1588: getfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     1591: ior
        //     1592: putfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     1595: goto -409 -> 1186
        //     1598: iconst_0
        //     1599: istore 42
        //     1601: goto -261 -> 1340
        //     1604: iconst_0
        //     1605: istore 48
        //     1607: goto -209 -> 1398
        //     1610: aconst_null
        //     1611: astore 49
        //     1613: goto -203 -> 1410
        //     1616: astore 41
        //     1618: aload 39
        //     1620: iconst_0
        //     1621: putfield 1687	com/android/server/wm/WindowState:mOrientationChanging	Z
        //     1624: goto -177 -> 1447
        //     1627: aload_0
        //     1628: getfield 399	com/android/server/wm/WindowManagerService:mResizingWindows	Ljava/util/ArrayList;
        //     1631: invokevirtual 1211	java/util/ArrayList:clear	()V
        //     1634: aload_0
        //     1635: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     1638: getfield 1690	com/android/server/wm/WindowManagerService$LayoutFields:mOrientationChangeComplete	Z
        //     1641: ifeq +28 -> 1669
        //     1644: aload_0
        //     1645: getfield 481	com/android/server/wm/WindowManagerService:mWindowsFreezingScreen	Z
        //     1648: ifeq +17 -> 1665
        //     1651: aload_0
        //     1652: iconst_0
        //     1653: putfield 481	com/android/server/wm/WindowManagerService:mWindowsFreezingScreen	Z
        //     1656: aload_0
        //     1657: getfield 526	com/android/server/wm/WindowManagerService:mH	Lcom/android/server/wm/WindowManagerService$H;
        //     1660: bipush 11
        //     1662: invokevirtual 1451	com/android/server/wm/WindowManagerService$H:removeMessages	(I)V
        //     1665: aload_0
        //     1666: invokespecial 1693	com/android/server/wm/WindowManagerService:stopFreezingDisplayLocked	()V
        //     1669: iconst_0
        //     1670: istore 17
        //     1672: aload_0
        //     1673: getfield 407	com/android/server/wm/WindowManagerService:mDestroySurface	Ljava/util/ArrayList;
        //     1676: invokevirtual 806	java/util/ArrayList:size	()I
        //     1679: istore 18
        //     1681: iload 18
        //     1683: ifle +72 -> 1755
        //     1686: iinc 18 255
        //     1689: aload_0
        //     1690: getfield 407	com/android/server/wm/WindowManagerService:mDestroySurface	Ljava/util/ArrayList;
        //     1693: iload 18
        //     1695: invokevirtual 813	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     1698: checkcast 403	com/android/server/wm/WindowState
        //     1701: astore 37
        //     1703: aload 37
        //     1705: iconst_0
        //     1706: putfield 1578	com/android/server/wm/WindowState:mDestroying	Z
        //     1709: aload_0
        //     1710: getfield 542	com/android/server/wm/WindowManagerService:mInputMethodWindow	Lcom/android/server/wm/WindowState;
        //     1713: aload 37
        //     1715: if_acmpne +8 -> 1723
        //     1718: aload_0
        //     1719: aconst_null
        //     1720: putfield 542	com/android/server/wm/WindowManagerService:mInputMethodWindow	Lcom/android/server/wm/WindowState;
        //     1723: aload 37
        //     1725: aload_0
        //     1726: getfield 548	com/android/server/wm/WindowManagerService:mWallpaperTarget	Lcom/android/server/wm/WindowState;
        //     1729: if_acmpne +6 -> 1735
        //     1732: iconst_1
        //     1733: istore 17
        //     1735: aload 37
        //     1737: getfield 959	com/android/server/wm/WindowState:mWinAnimator	Lcom/android/server/wm/WindowStateAnimator;
        //     1740: invokevirtual 1696	com/android/server/wm/WindowStateAnimator:destroySurfaceLocked	()V
        //     1743: iload 18
        //     1745: ifgt -59 -> 1686
        //     1748: aload_0
        //     1749: getfield 407	com/android/server/wm/WindowManagerService:mDestroySurface	Ljava/util/ArrayList;
        //     1752: invokevirtual 1211	java/util/ArrayList:clear	()V
        //     1755: bipush 255
        //     1757: aload_0
        //     1758: getfield 385	com/android/server/wm/WindowManagerService:mExitingTokens	Ljava/util/ArrayList;
        //     1761: invokevirtual 806	java/util/ArrayList:size	()I
        //     1764: iadd
        //     1765: istore 19
        //     1767: iload 19
        //     1769: iflt +62 -> 1831
        //     1772: aload_0
        //     1773: getfield 385	com/android/server/wm/WindowManagerService:mExitingTokens	Ljava/util/ArrayList;
        //     1776: iload 19
        //     1778: invokevirtual 813	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     1781: checkcast 834	com/android/server/wm/WindowToken
        //     1784: astore 34
        //     1786: aload 34
        //     1788: getfield 1479	com/android/server/wm/WindowToken:hasVisible	Z
        //     1791: ifne +34 -> 1825
        //     1794: aload_0
        //     1795: getfield 385	com/android/server/wm/WindowManagerService:mExitingTokens	Ljava/util/ArrayList;
        //     1798: iload 19
        //     1800: invokevirtual 1699	java/util/ArrayList:remove	(I)Ljava/lang/Object;
        //     1803: pop
        //     1804: aload 34
        //     1806: getfield 1702	com/android/server/wm/WindowToken:windowType	I
        //     1809: sipush 2013
        //     1812: if_icmpne +13 -> 1825
        //     1815: aload_0
        //     1816: getfield 546	com/android/server/wm/WindowManagerService:mWallpaperTokens	Ljava/util/ArrayList;
        //     1819: aload 34
        //     1821: invokevirtual 1704	java/util/ArrayList:remove	(Ljava/lang/Object;)Z
        //     1824: pop
        //     1825: iinc 19 255
        //     1828: goto -61 -> 1767
        //     1831: bipush 255
        //     1833: aload_0
        //     1834: getfield 391	com/android/server/wm/WindowManagerService:mExitingAppTokens	Ljava/util/ArrayList;
        //     1837: invokevirtual 806	java/util/ArrayList:size	()I
        //     1840: iadd
        //     1841: istore 20
        //     1843: iload 20
        //     1845: iflt +90 -> 1935
        //     1848: aload_0
        //     1849: getfield 391	com/android/server/wm/WindowManagerService:mExitingAppTokens	Ljava/util/ArrayList;
        //     1852: iload 20
        //     1854: invokevirtual 813	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     1857: checkcast 815	com/android/server/wm/AppWindowToken
        //     1860: astore 30
        //     1862: aload 30
        //     1864: getfield 1479	com/android/server/wm/WindowToken:hasVisible	Z
        //     1867: ifne +62 -> 1929
        //     1870: aload_0
        //     1871: getfield 512	com/android/server/wm/WindowManagerService:mClosingApps	Ljava/util/ArrayList;
        //     1874: aload 30
        //     1876: invokevirtual 1707	java/util/ArrayList:contains	(Ljava/lang/Object;)Z
        //     1879: ifne +50 -> 1929
        //     1882: aload 30
        //     1884: getfield 928	com/android/server/wm/AppWindowToken:mAppAnimator	Lcom/android/server/wm/AppWindowAnimator;
        //     1887: invokevirtual 954	com/android/server/wm/AppWindowAnimator:clearAnimation	()V
        //     1890: aload 30
        //     1892: getfield 928	com/android/server/wm/AppWindowToken:mAppAnimator	Lcom/android/server/wm/AppWindowAnimator;
        //     1895: iconst_0
        //     1896: putfield 1710	com/android/server/wm/AppWindowAnimator:animating	Z
        //     1899: aload_0
        //     1900: getfield 387	com/android/server/wm/WindowManagerService:mAppTokens	Ljava/util/ArrayList;
        //     1903: aload 30
        //     1905: invokevirtual 1704	java/util/ArrayList:remove	(Ljava/lang/Object;)Z
        //     1908: pop
        //     1909: aload_0
        //     1910: getfield 389	com/android/server/wm/WindowManagerService:mAnimatingAppTokens	Ljava/util/ArrayList;
        //     1913: aload 30
        //     1915: invokevirtual 1704	java/util/ArrayList:remove	(Ljava/lang/Object;)Z
        //     1918: pop
        //     1919: aload_0
        //     1920: getfield 391	com/android/server/wm/WindowManagerService:mExitingAppTokens	Ljava/util/ArrayList;
        //     1923: iload 20
        //     1925: invokevirtual 1699	java/util/ArrayList:remove	(I)Ljava/lang/Object;
        //     1928: pop
        //     1929: iinc 20 255
        //     1932: goto -89 -> 1843
        //     1935: aload_0
        //     1936: getfield 729	com/android/server/wm/WindowManagerService:mAnimator	Lcom/android/server/wm/WindowAnimator;
        //     1939: getfield 1624	com/android/server/wm/WindowAnimator:mAnimating	Z
        //     1942: ifne +63 -> 2005
        //     1945: aload_0
        //     1946: getfield 413	com/android/server/wm/WindowManagerService:mRelayoutWhileAnimating	Ljava/util/ArrayList;
        //     1949: invokevirtual 806	java/util/ArrayList:size	()I
        //     1952: ifle +53 -> 2005
        //     1955: bipush 255
        //     1957: aload_0
        //     1958: getfield 413	com/android/server/wm/WindowManagerService:mRelayoutWhileAnimating	Ljava/util/ArrayList;
        //     1961: invokevirtual 806	java/util/ArrayList:size	()I
        //     1964: iadd
        //     1965: istore 28
        //     1967: iload 28
        //     1969: iflt +29 -> 1998
        //     1972: aload_0
        //     1973: getfield 413	com/android/server/wm/WindowManagerService:mRelayoutWhileAnimating	Ljava/util/ArrayList;
        //     1976: iload 28
        //     1978: invokevirtual 813	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     1981: checkcast 403	com/android/server/wm/WindowState
        //     1984: getfield 825	com/android/server/wm/WindowState:mClient	Landroid/view/IWindow;
        //     1987: invokeinterface 1713 1 0
        //     1992: iinc 28 255
        //     1995: goto -28 -> 1967
        //     1998: aload_0
        //     1999: getfield 413	com/android/server/wm/WindowManagerService:mRelayoutWhileAnimating	Ljava/util/ArrayList;
        //     2002: invokevirtual 1211	java/util/ArrayList:clear	()V
        //     2005: iload 17
        //     2007: ifeq +28 -> 2035
        //     2010: aload_0
        //     2011: getfield 473	com/android/server/wm/WindowManagerService:mLayoutNeeded	Z
        //     2014: istore 26
        //     2016: aload_0
        //     2017: invokevirtual 911	com/android/server/wm/WindowManagerService:adjustWallpaperWindowsLocked	()I
        //     2020: ifeq +275 -> 2295
        //     2023: iconst_1
        //     2024: istore 27
        //     2026: aload_0
        //     2027: iload 27
        //     2029: iload 26
        //     2031: ior
        //     2032: putfield 473	com/android/server/wm/WindowManagerService:mLayoutNeeded	Z
        //     2035: aload_0
        //     2036: getfield 471	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
        //     2039: ifeq +8 -> 2047
        //     2042: aload_0
        //     2043: iconst_1
        //     2044: putfield 473	com/android/server/wm/WindowManagerService:mLayoutNeeded	Z
        //     2047: aload_0
        //     2048: getfield 592	com/android/server/wm/WindowManagerService:mInputMonitor	Lcom/android/server/wm/InputMonitor;
        //     2051: iconst_1
        //     2052: invokevirtual 1371	com/android/server/wm/InputMonitor:updateInputWindowsLw	(Z)V
        //     2055: aload_0
        //     2056: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     2059: invokestatic 1717	com/android/server/wm/WindowManagerService$LayoutFields:access$1100	(Lcom/android/server/wm/WindowManagerService$LayoutFields;)Lcom/android/server/wm/Session;
        //     2062: ifnull +239 -> 2301
        //     2065: iconst_1
        //     2066: istore 21
        //     2068: aload_0
        //     2069: iload 21
        //     2071: invokevirtual 1720	com/android/server/wm/WindowManagerService:setHoldScreenLocked	(Z)V
        //     2074: aload_0
        //     2075: getfield 477	com/android/server/wm/WindowManagerService:mDisplayFrozen	Z
        //     2078: ifne +69 -> 2147
        //     2081: aload_0
        //     2082: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     2085: invokestatic 1253	com/android/server/wm/WindowManagerService$LayoutFields:access$1300	(Lcom/android/server/wm/WindowManagerService$LayoutFields;)F
        //     2088: fconst_0
        //     2089: fcmpg
        //     2090: iflt +15 -> 2105
        //     2093: aload_0
        //     2094: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     2097: invokestatic 1253	com/android/server/wm/WindowManagerService$LayoutFields:access$1300	(Lcom/android/server/wm/WindowManagerService$LayoutFields;)F
        //     2100: fconst_1
        //     2101: fcmpl
        //     2102: ifle +205 -> 2307
        //     2105: aload_0
        //     2106: getfield 637	com/android/server/wm/WindowManagerService:mPowerManager	Lcom/android/server/PowerManagerService;
        //     2109: bipush 255
        //     2111: invokevirtual 1723	com/android/server/PowerManagerService:setScreenBrightnessOverride	(I)V
        //     2114: aload_0
        //     2115: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     2118: invokestatic 1263	com/android/server/wm/WindowManagerService$LayoutFields:access$1400	(Lcom/android/server/wm/WindowManagerService$LayoutFields;)F
        //     2121: fconst_0
        //     2122: fcmpg
        //     2123: iflt +15 -> 2138
        //     2126: aload_0
        //     2127: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     2130: invokestatic 1263	com/android/server/wm/WindowManagerService$LayoutFields:access$1400	(Lcom/android/server/wm/WindowManagerService$LayoutFields;)F
        //     2133: fconst_1
        //     2134: fcmpl
        //     2135: ifle +194 -> 2329
        //     2138: aload_0
        //     2139: getfield 637	com/android/server/wm/WindowManagerService:mPowerManager	Lcom/android/server/PowerManagerService;
        //     2142: bipush 255
        //     2144: invokevirtual 1726	com/android/server/PowerManagerService:setButtonBrightnessOverride	(I)V
        //     2147: aload_0
        //     2148: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     2151: invokestatic 1717	com/android/server/wm/WindowManagerService$LayoutFields:access$1100	(Lcom/android/server/wm/WindowManagerService$LayoutFields;)Lcom/android/server/wm/Session;
        //     2154: aload_0
        //     2155: getfield 1728	com/android/server/wm/WindowManagerService:mHoldingScreenOn	Lcom/android/server/wm/Session;
        //     2158: if_acmpeq +42 -> 2200
        //     2161: aload_0
        //     2162: aload_0
        //     2163: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     2166: invokestatic 1717	com/android/server/wm/WindowManagerService$LayoutFields:access$1100	(Lcom/android/server/wm/WindowManagerService$LayoutFields;)Lcom/android/server/wm/Session;
        //     2169: putfield 1728	com/android/server/wm/WindowManagerService:mHoldingScreenOn	Lcom/android/server/wm/Session;
        //     2172: aload_0
        //     2173: getfield 526	com/android/server/wm/WindowManagerService:mH	Lcom/android/server/wm/WindowManagerService$H;
        //     2176: bipush 12
        //     2178: aload_0
        //     2179: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     2182: invokestatic 1717	com/android/server/wm/WindowManagerService$LayoutFields:access$1100	(Lcom/android/server/wm/WindowManagerService$LayoutFields;)Lcom/android/server/wm/Session;
        //     2185: invokevirtual 1731	com/android/server/wm/WindowManagerService$H:obtainMessage	(ILjava/lang/Object;)Landroid/os/Message;
        //     2188: astore 24
        //     2190: aload_0
        //     2191: getfield 526	com/android/server/wm/WindowManagerService:mH	Lcom/android/server/wm/WindowManagerService$H;
        //     2194: aload 24
        //     2196: invokevirtual 1459	com/android/server/wm/WindowManagerService$H:sendMessage	(Landroid/os/Message;)Z
        //     2199: pop
        //     2200: aload_0
        //     2201: getfield 1733	com/android/server/wm/WindowManagerService:mTurnOnScreen	Z
        //     2204: ifeq +21 -> 2225
        //     2207: aload_0
        //     2208: getfield 637	com/android/server/wm/WindowManagerService:mPowerManager	Lcom/android/server/PowerManagerService;
        //     2211: invokestatic 1476	android/os/SystemClock:uptimeMillis	()J
        //     2214: iconst_0
        //     2215: iconst_1
        //     2216: iconst_1
        //     2217: invokevirtual 1737	com/android/server/PowerManagerService:userActivity	(JZIZ)V
        //     2220: aload_0
        //     2221: iconst_0
        //     2222: putfield 1733	com/android/server/wm/WindowManagerService:mTurnOnScreen	Z
        //     2225: aload_0
        //     2226: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     2229: invokestatic 1740	com/android/server/wm/WindowManagerService$LayoutFields:access$900	(Lcom/android/server/wm/WindowManagerService$LayoutFields;)Z
        //     2232: ifeq +21 -> 2253
        //     2235: aload_0
        //     2236: iconst_0
        //     2237: invokevirtual 1743	com/android/server/wm/WindowManagerService:updateRotationUncheckedLocked	(Z)Z
        //     2240: ifeq +111 -> 2351
        //     2243: aload_0
        //     2244: getfield 526	com/android/server/wm/WindowManagerService:mH	Lcom/android/server/wm/WindowManagerService$H;
        //     2247: bipush 18
        //     2249: invokevirtual 1596	com/android/server/wm/WindowManagerService$H:sendEmptyMessage	(I)Z
        //     2252: pop
        //     2253: aload_0
        //     2254: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     2257: getfield 1690	com/android/server/wm/WindowManagerService$LayoutFields:mOrientationChangeComplete	Z
        //     2260: ifeq +24 -> 2284
        //     2263: aload_0
        //     2264: getfield 473	com/android/server/wm/WindowManagerService:mLayoutNeeded	Z
        //     2267: ifne +17 -> 2284
        //     2270: aload_0
        //     2271: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     2274: invokestatic 1740	com/android/server/wm/WindowManagerService$LayoutFields:access$900	(Lcom/android/server/wm/WindowManagerService$LayoutFields;)Z
        //     2277: ifne +7 -> 2284
        //     2280: aload_0
        //     2281: invokevirtual 1746	com/android/server/wm/WindowManagerService:checkDrawnWindowsLocked	()V
        //     2284: aload_0
        //     2285: invokevirtual 1749	com/android/server/wm/WindowManagerService:enableScreenIfNeededLocked	()V
        //     2288: aload_0
        //     2289: invokevirtual 988	com/android/server/wm/WindowManagerService:scheduleAnimationLocked	()V
        //     2292: goto -2276 -> 16
        //     2295: iconst_0
        //     2296: istore 27
        //     2298: goto -272 -> 2026
        //     2301: iconst_0
        //     2302: istore 21
        //     2304: goto -236 -> 2068
        //     2307: aload_0
        //     2308: getfield 637	com/android/server/wm/WindowManagerService:mPowerManager	Lcom/android/server/PowerManagerService;
        //     2311: ldc_w 1750
        //     2314: aload_0
        //     2315: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     2318: invokestatic 1253	com/android/server/wm/WindowManagerService$LayoutFields:access$1300	(Lcom/android/server/wm/WindowManagerService$LayoutFields;)F
        //     2321: fmul
        //     2322: f2i
        //     2323: invokevirtual 1723	com/android/server/PowerManagerService:setScreenBrightnessOverride	(I)V
        //     2326: goto -212 -> 2114
        //     2329: aload_0
        //     2330: getfield 637	com/android/server/wm/WindowManagerService:mPowerManager	Lcom/android/server/PowerManagerService;
        //     2333: ldc_w 1750
        //     2336: aload_0
        //     2337: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     2340: invokestatic 1263	com/android/server/wm/WindowManagerService$LayoutFields:access$1400	(Lcom/android/server/wm/WindowManagerService$LayoutFields;)F
        //     2343: fmul
        //     2344: f2i
        //     2345: invokevirtual 1726	com/android/server/PowerManagerService:setButtonBrightnessOverride	(I)V
        //     2348: goto -201 -> 2147
        //     2351: aload_0
        //     2352: getfield 574	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
        //     2355: iconst_0
        //     2356: invokestatic 1753	com/android/server/wm/WindowManagerService$LayoutFields:access$902	(Lcom/android/server/wm/WindowManagerService$LayoutFields;Z)Z
        //     2359: pop
        //     2360: goto -107 -> 2253
        //     2363: astore 29
        //     2365: goto -373 -> 1992
        //     2368: iload 13
        //     2370: iconst_4
        //     2371: if_icmpge -1406 -> 965
        //     2374: iload 13
        //     2376: iconst_1
        //     2377: if_icmpne +15 -> 2392
        //     2380: iconst_1
        //     2381: istore 65
        //     2383: goto -1532 -> 851
        //     2386: iinc 52 255
        //     2389: goto -1470 -> 919
        //     2392: iconst_0
        //     2393: istore 65
        //     2395: goto -1544 -> 851
        //     2398: iconst_1
        //     2399: istore 42
        //     2401: goto -1061 -> 1340
        //     2404: iconst_0
        //     2405: istore 53
        //     2407: goto -2145 -> 262
        //     2410: iconst_0
        //     2411: istore 61
        //     2413: goto -2070 -> 343
        //
        // Exception table:
        //     from	to	target	type
        //     235	974	977	java/lang/RuntimeException
        //     1453	1506	977	java/lang/RuntimeException
        //     1522	1567	977	java/lang/RuntimeException
        //     235	974	1509	finally
        //     979	990	1509	finally
        //     1453	1506	1509	finally
        //     1522	1567	1509	finally
        //     1302	1447	1616	android/os/RemoteException
        //     1972	1992	2363	android/os/RemoteException
    }

    private final void performLayoutLockedInner(boolean paramBoolean1, boolean paramBoolean2)
    {
        if (!this.mLayoutNeeded);
        while (true)
        {
            return;
            this.mLayoutNeeded = false;
            int i = this.mCurDisplayWidth;
            int j = this.mCurDisplayHeight;
            int k = this.mFakeWindows.size();
            for (int m = 0; m < k; m++)
                ((FakeWindowImpl)this.mFakeWindows.get(m)).layout(i, j);
            int n = this.mWindows.size();
            this.mPolicy.beginLayoutLw(i, j, this.mRotation);
            this.mSystemDecorLayer = this.mPolicy.getSystemDecorRectLw(this.mSystemDecorRect);
            int i1 = 1 + this.mLayoutSeq;
            if (i1 < 0)
                i1 = 0;
            this.mLayoutSeq = i1;
            int i2 = -1;
            int i3 = n - 1;
            if (i3 >= 0)
            {
                WindowState localWindowState2 = (WindowState)this.mWindows.get(i3);
                if ((!localWindowState2.isGoneForLayoutLw()) || (!localWindowState2.mHaveFrame) || (localWindowState2.mLayoutNeeded))
                {
                    if (localWindowState2.mLayoutAttached)
                        break label243;
                    if (paramBoolean1)
                        localWindowState2.mContentChanged = false;
                    localWindowState2.mLayoutNeeded = false;
                    localWindowState2.prelayout();
                    this.mPolicy.layoutWindowLw(localWindowState2, localWindowState2.mAttrs, null);
                    localWindowState2.mLayoutSeq = i1;
                }
                while (true)
                {
                    i3--;
                    break;
                    label243: if (i2 < 0)
                        i2 = i3;
                }
            }
            for (int i4 = i2; i4 >= 0; i4--)
            {
                WindowState localWindowState1 = (WindowState)this.mWindows.get(i4);
                if ((localWindowState1.mLayoutAttached) && (((localWindowState1.mViewVisibility != 8) && (localWindowState1.mRelayoutCalled)) || (!localWindowState1.mHaveFrame) || (localWindowState1.mLayoutNeeded)))
                {
                    if (paramBoolean1)
                        localWindowState1.mContentChanged = false;
                    localWindowState1.mLayoutNeeded = false;
                    localWindowState1.prelayout();
                    this.mPolicy.layoutWindowLw(localWindowState1, localWindowState1.mAttrs, localWindowState1.mAttachedWindow);
                    localWindowState1.mLayoutSeq = i1;
                }
            }
            this.mInputMonitor.setUpdateInputWindowsNeededLw();
            if (paramBoolean2)
                this.mInputMonitor.updateInputWindowsLw(false);
            this.mPolicy.finishLayoutLw();
        }
    }

    private void placeWindowAfter(WindowState paramWindowState1, WindowState paramWindowState2)
    {
        int i = this.mWindows.indexOf(paramWindowState1);
        this.mWindows.add(i + 1, paramWindowState2);
        this.mWindowsChanged = true;
    }

    private void placeWindowBefore(WindowState paramWindowState1, WindowState paramWindowState2)
    {
        int i = this.mWindows.indexOf(paramWindowState1);
        this.mWindows.add(i, paramWindowState2);
        this.mWindowsChanged = true;
    }

    private final int reAddAppWindowsLocked(int paramInt, WindowToken paramWindowToken)
    {
        int i = paramWindowToken.windows.size();
        for (int j = 0; j < i; j++)
            paramInt = reAddWindowLocked(paramInt, (WindowState)paramWindowToken.windows.get(j));
        return paramInt;
    }

    private final int reAddWindowLocked(int paramInt, WindowState paramWindowState)
    {
        int i = paramWindowState.mChildWindows.size();
        int j = 0;
        for (int k = 0; k < i; k++)
        {
            WindowState localWindowState = (WindowState)paramWindowState.mChildWindows.get(k);
            if ((j == 0) && (localWindowState.mSubLayer >= 0))
            {
                paramWindowState.mRebuilding = false;
                this.mWindows.add(paramInt, paramWindowState);
                paramInt++;
                j = 1;
            }
            localWindowState.mRebuilding = false;
            this.mWindows.add(paramInt, localWindowState);
            paramInt++;
        }
        if (j == 0)
        {
            paramWindowState.mRebuilding = false;
            this.mWindows.add(paramInt, paramWindowState);
            paramInt++;
        }
        this.mWindowsChanged = true;
        return paramInt;
    }

    private void reAddWindowToListInOrderLocked(WindowState paramWindowState)
    {
        addWindowToListInOrderLocked(paramWindowState, false);
        int i = this.mWindows.indexOf(paramWindowState);
        if (i >= 0)
        {
            this.mWindows.remove(i);
            this.mWindowsChanged = true;
            reAddWindowLocked(i, paramWindowState);
        }
    }

    private void readForcedDisplaySizeLocked()
    {
        String str = Settings.Secure.getString(this.mContext.getContentResolver(), "display_size_forced");
        if ((str == null) || (str.length() == 0));
        while (true)
        {
            return;
            int i = str.indexOf(',');
            if ((i > 0) && (str.lastIndexOf(',') == i))
                try
                {
                    int j = Integer.parseInt(str.substring(0, i));
                    int k = Integer.parseInt(str.substring(i + 1));
                    setForcedDisplaySizeLocked(j, k);
                }
                catch (NumberFormatException localNumberFormatException)
                {
                }
        }
    }

    private void rebuildBlackFrame()
    {
        int i = 1;
        if (this.mBlackFrame != null)
        {
            this.mBlackFrame.kill();
            this.mBlackFrame = null;
        }
        if ((this.mBaseDisplayWidth < this.mInitialDisplayWidth) || (this.mBaseDisplayHeight < this.mInitialDisplayHeight))
            if ((this.mRotation != i) && (this.mRotation != 3))
                break label135;
        while (true)
        {
            int j;
            int k;
            int m;
            int n;
            Rect localRect1;
            Rect localRect2;
            if (i != 0)
            {
                j = this.mInitialDisplayHeight;
                k = this.mInitialDisplayWidth;
                m = this.mBaseDisplayHeight;
                n = this.mBaseDisplayWidth;
                localRect1 = new Rect(0, 0, j, k);
                localRect2 = new Rect(0, 0, m, n);
            }
            try
            {
                this.mBlackFrame = new BlackFrame(this.mFxSession, localRect1, localRect2, 2000000);
                label134: return;
                label135: i = 0;
                continue;
                j = this.mInitialDisplayWidth;
                k = this.mInitialDisplayHeight;
                m = this.mBaseDisplayWidth;
                n = this.mBaseDisplayHeight;
            }
            catch (Surface.OutOfResourcesException localOutOfResourcesException)
            {
                break label134;
            }
        }
    }

    private int reduceCompatConfigWidthSize(int paramInt1, int paramInt2, DisplayMetrics paramDisplayMetrics, int paramInt3, int paramInt4)
    {
        paramDisplayMetrics.noncompatWidthPixels = this.mPolicy.getNonDecorDisplayWidth(paramInt3, paramInt4, paramInt2);
        paramDisplayMetrics.noncompatHeightPixels = this.mPolicy.getNonDecorDisplayHeight(paramInt3, paramInt4, paramInt2);
        float f = CompatibilityInfo.computeCompatibleScaling(paramDisplayMetrics, null);
        int i = (int)(0.5F + paramDisplayMetrics.noncompatWidthPixels / f / paramDisplayMetrics.density);
        if ((paramInt1 == 0) || (i < paramInt1))
            paramInt1 = i;
        return paramInt1;
    }

    private int reduceConfigLayout(int paramInt1, int paramInt2, float paramFloat, int paramInt3, int paramInt4)
    {
        int i = this.mPolicy.getNonDecorDisplayWidth(paramInt3, paramInt4, paramInt2);
        int j = this.mPolicy.getNonDecorDisplayHeight(paramInt3, paramInt4, paramInt2);
        int k = i;
        int m = j;
        if (k < m)
        {
            int i5 = k;
            k = m;
            m = i5;
        }
        int n = (int)(k / paramFloat);
        int i1 = (int)(m / paramFloat);
        int i2;
        int i4;
        int i3;
        if (n < 470)
        {
            i2 = 1;
            i4 = 0;
            i3 = 0;
        }
        while (true)
        {
            if (i4 == 0)
                paramInt1 = 0x10 | paramInt1 & 0xFFFFFFCF;
            if (i3 != 0)
                paramInt1 |= 268435456;
            if (i2 < (paramInt1 & 0xF))
                paramInt1 = i2 | paramInt1 & 0xFFFFFFF0;
            return paramInt1;
            if ((n >= 960) && (i1 >= 720))
            {
                i2 = 4;
                label154: if ((i1 <= 321) && (n <= 570))
                    break label220;
            }
            label220: for (i3 = 1; ; i3 = 0)
            {
                if (n * 3 / 5 < i1 - 1)
                    break label226;
                i4 = 1;
                break;
                if ((n >= 640) && (i1 >= 480))
                {
                    i2 = 3;
                    break label154;
                }
                i2 = 2;
                break label154;
            }
            label226: i4 = 0;
        }
    }

    private void removeAppTokensLocked(List<IBinder> paramList)
    {
        int i = paramList.size();
        for (int j = 0; j < i; j++)
        {
            IBinder localIBinder = (IBinder)paramList.get(j);
            AppWindowToken localAppWindowToken = findAppWindowToken(localIBinder);
            if (!this.mAppTokens.remove(localAppWindowToken))
            {
                Slog.w("WindowManager", "Attempting to reorder token that doesn't exist: " + localIBinder + " (" + localAppWindowToken + ")");
                j--;
                i--;
            }
        }
    }

    private void removeWindowInnerLocked(Session paramSession, WindowState paramWindowState)
    {
        if (paramWindowState.mRemoved)
            return;
        for (int i = -1 + paramWindowState.mChildWindows.size(); i >= 0; i--)
        {
            WindowState localWindowState = (WindowState)paramWindowState.mChildWindows.get(i);
            Slog.w("WindowManager", "Force-removing child win " + localWindowState + " from container " + paramWindowState);
            removeWindowInnerLocked(localWindowState.mSession, localWindowState);
        }
        paramWindowState.mRemoved = true;
        if (this.mInputMethodTarget == paramWindowState)
            moveInputMethodWindowsIfNeededLocked(false);
        this.mPolicy.removeWindowLw(paramWindowState);
        paramWindowState.removeLocked();
        this.mWindowMap.remove(paramWindowState.mClient.asBinder());
        this.mWindows.remove(paramWindowState);
        this.mPendingRemove.remove(paramWindowState);
        this.mWindowsChanged = true;
        label176: AppWindowToken localAppWindowToken;
        if (this.mInputMethodWindow == paramWindowState)
        {
            this.mInputMethodWindow = null;
            WindowToken localWindowToken = paramWindowState.mToken;
            localAppWindowToken = paramWindowState.mAppToken;
            localWindowToken.windows.remove(paramWindowState);
            if (localAppWindowToken != null)
                localAppWindowToken.allAppWindows.remove(paramWindowState);
            if (localWindowToken.windows.size() == 0)
            {
                if (localWindowToken.explicit)
                    break label358;
                this.mTokenMap.remove(localWindowToken.token);
            }
            label245: if (localAppWindowToken != null)
            {
                if (localAppWindowToken.startingWindow != paramWindowState)
                    break label372;
                localAppWindowToken.startingWindow = null;
            }
            label265: if (paramWindowState.mAttrs.type != 2013)
                break label446;
            this.mLastWallpaperTimeoutTime = 0L;
            adjustWallpaperWindowsLocked();
        }
        while (true)
        {
            if (!this.mInLayout)
            {
                assignLayersLocked();
                this.mLayoutNeeded = true;
                performLayoutAndPlaceSurfacesLocked();
                if (paramWindowState.mAppToken != null)
                    paramWindowState.mAppToken.updateReportedVisibilityLocked();
            }
            this.mInputMonitor.updateInputWindowsLw(true);
            break;
            if (paramWindowState.mAttrs.type != 2012)
                break label176;
            this.mInputMethodDialogs.remove(paramWindowState);
            break label176;
            label358: if (localAppWindowToken == null)
                break label245;
            localAppWindowToken.firstWindowDrawn = false;
            break label245;
            label372: if ((localAppWindowToken.allAppWindows.size() == 0) && (localAppWindowToken.startingData != null))
            {
                localAppWindowToken.startingData = null;
                break label265;
            }
            if ((localAppWindowToken.allAppWindows.size() != 1) || (localAppWindowToken.startingView == null))
                break label265;
            Message localMessage = this.mH.obtainMessage(6, localAppWindowToken);
            this.mH.sendMessage(localMessage);
            break label265;
            label446: if ((0x100000 & paramWindowState.mAttrs.flags) != 0)
                adjustWallpaperWindowsLocked();
        }
    }

    private void scheduleAnimationCallback(IRemoteCallback paramIRemoteCallback)
    {
        if (paramIRemoteCallback != null)
            this.mH.sendMessage(this.mH.obtainMessage(27, paramIRemoteCallback));
    }

    private void sendScreenStatusToClientsLocked()
    {
        int i = this.mWindows.size();
        boolean bool = this.mPowerManager.isScreenOn();
        int j = i - 1;
        while (true)
        {
            WindowState localWindowState;
            if (j >= 0)
                localWindowState = (WindowState)this.mWindows.get(j);
            try
            {
                localWindowState.mClient.dispatchScreenState(bool);
                label48: j--;
                continue;
                return;
            }
            catch (RemoteException localRemoteException)
            {
                break label48;
            }
        }
    }

    private void setForcedDisplaySizeLocked(int paramInt1, int paramInt2)
    {
        Slog.i("WindowManager", "Using new display size: " + paramInt1 + "x" + paramInt2);
        synchronized (this.mDisplaySizeLock)
        {
            this.mBaseDisplayWidth = paramInt1;
            this.mBaseDisplayHeight = paramInt2;
            this.mPolicy.setInitialDisplaySize(this.mDisplay, this.mBaseDisplayWidth, this.mBaseDisplayHeight);
            this.mLayoutNeeded = true;
            boolean bool = updateOrientationFromAppTokensLocked(false);
            this.mTempConfiguration.setToDefaults();
            this.mTempConfiguration.fontScale = this.mCurConfiguration.fontScale;
            if ((computeScreenConfigurationLocked(this.mTempConfiguration)) && (this.mCurConfiguration.diff(this.mTempConfiguration) != 0))
                bool = true;
            if (bool)
            {
                this.mWaitingForConfig = true;
                startFreezingDisplayLocked(false);
                this.mH.sendEmptyMessage(18);
            }
            rebuildBlackFrame();
            performLayoutAndPlaceSurfacesLocked();
            return;
        }
    }

    private boolean shouldAllowDisableKeyguard()
    {
        int i = 1;
        if (this.mAllowDisableKeyguard == -1)
        {
            DevicePolicyManager localDevicePolicyManager = (DevicePolicyManager)this.mContext.getSystemService("device_policy");
            if (localDevicePolicyManager != null)
            {
                if (localDevicePolicyManager.getPasswordQuality(null) != 0)
                    break label54;
                int k = i;
                this.mAllowDisableKeyguard = k;
            }
        }
        if (this.mAllowDisableKeyguard == i);
        while (true)
        {
            return i;
            label54: int m = 0;
            break;
            int j = 0;
        }
    }

    // ERROR //
    private void showStrictModeViolation(int paramInt)
    {
        // Byte code:
        //     0: iload_1
        //     1: ifeq +153 -> 154
        //     4: iconst_1
        //     5: istore_2
        //     6: invokestatic 1963	android/os/Binder:getCallingPid	()I
        //     9: istore_3
        //     10: aload_0
        //     11: getfield 378	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
        //     14: astore 4
        //     16: aload 4
        //     18: monitorenter
        //     19: iload_2
        //     20: ifeq +71 -> 91
        //     23: iconst_0
        //     24: istore 7
        //     26: bipush 255
        //     28: aload_0
        //     29: getfield 395	com/android/server/wm/WindowManagerService:mWindows	Ljava/util/ArrayList;
        //     32: invokevirtual 806	java/util/ArrayList:size	()I
        //     35: iadd
        //     36: istore 8
        //     38: iload 8
        //     40: iflt +40 -> 80
        //     43: aload_0
        //     44: getfield 395	com/android/server/wm/WindowManagerService:mWindows	Ljava/util/ArrayList;
        //     47: iload 8
        //     49: invokevirtual 813	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     52: checkcast 403	com/android/server/wm/WindowState
        //     55: astore 9
        //     57: aload 9
        //     59: getfield 1238	com/android/server/wm/WindowState:mSession	Lcom/android/server/wm/Session;
        //     62: getfield 1968	com/android/server/wm/Session:mPid	I
        //     65: iload_3
        //     66: if_icmpne +93 -> 159
        //     69: aload 9
        //     71: invokevirtual 1507	com/android/server/wm/WindowState:isVisibleLw	()Z
        //     74: ifeq +85 -> 159
        //     77: iconst_1
        //     78: istore 7
        //     80: iload 7
        //     82: ifne +9 -> 91
        //     85: aload 4
        //     87: monitorexit
        //     88: goto +65 -> 153
        //     91: invokestatic 761	android/view/Surface:openTransaction	()V
        //     94: aload_0
        //     95: getfield 1491	com/android/server/wm/WindowManagerService:mStrictModeFlash	Lcom/android/server/wm/StrictModeFlash;
        //     98: ifnonnull +22 -> 120
        //     101: aload_0
        //     102: new 1493	com/android/server/wm/StrictModeFlash
        //     105: dup
        //     106: aload_0
        //     107: getfield 1406	com/android/server/wm/WindowManagerService:mDisplay	Landroid/view/Display;
        //     110: aload_0
        //     111: getfield 756	com/android/server/wm/WindowManagerService:mFxSession	Landroid/view/SurfaceSession;
        //     114: invokespecial 1971	com/android/server/wm/StrictModeFlash:<init>	(Landroid/view/Display;Landroid/view/SurfaceSession;)V
        //     117: putfield 1491	com/android/server/wm/WindowManagerService:mStrictModeFlash	Lcom/android/server/wm/StrictModeFlash;
        //     120: aload_0
        //     121: getfield 1491	com/android/server/wm/WindowManagerService:mStrictModeFlash	Lcom/android/server/wm/StrictModeFlash;
        //     124: iload_2
        //     125: invokevirtual 1974	com/android/server/wm/StrictModeFlash:setVisibility	(Z)V
        //     128: invokestatic 767	android/view/Surface:closeTransaction	()V
        //     131: aload 4
        //     133: monitorexit
        //     134: goto +19 -> 153
        //     137: astore 5
        //     139: aload 4
        //     141: monitorexit
        //     142: aload 5
        //     144: athrow
        //     145: astore 6
        //     147: invokestatic 767	android/view/Surface:closeTransaction	()V
        //     150: aload 6
        //     152: athrow
        //     153: return
        //     154: iconst_0
        //     155: istore_2
        //     156: goto -150 -> 6
        //     159: iinc 8 255
        //     162: goto -124 -> 38
        //
        // Exception table:
        //     from	to	target	type
        //     26	94	137	finally
        //     128	142	137	finally
        //     147	153	137	finally
        //     94	128	145	finally
    }

    private void startFreezingDisplayLocked(boolean paramBoolean)
    {
        if (this.mDisplayFrozen);
        while (true)
        {
            return;
            if ((this.mDisplay != null) && (this.mPolicy.isScreenOnFully()))
            {
                this.mScreenFrozenLock.acquire();
                this.mDisplayFrozen = true;
                this.mInputMonitor.freezeInputDispatchingLw();
                if (this.mNextAppTransition != -1)
                {
                    this.mNextAppTransition = -1;
                    this.mNextAppTransitionType = 0;
                    this.mNextAppTransitionPackage = null;
                    this.mNextAppTransitionThumbnail = null;
                    this.mAppTransitionReady = true;
                }
                if (this.mAnimator.mScreenRotationAnimation != null)
                {
                    this.mAnimator.mScreenRotationAnimation.kill();
                    this.mAnimator.mScreenRotationAnimation = null;
                }
                this.mAnimator.mScreenRotationAnimation = new ScreenRotationAnimation(this.mContext, this.mFxSession, paramBoolean, this.mCurDisplayWidth, this.mCurDisplayHeight, this.mDisplay.getRotation());
                if (!this.mAnimator.mScreenRotationAnimation.hasScreenshot())
                    Surface.freezeDisplay(0);
            }
        }
    }

    private void stopFreezingDisplayLocked()
    {
        if (!this.mDisplayFrozen)
            return;
        int i;
        if ((!this.mWaitingForConfig) && (this.mAppsFreezingScreen <= 0) && (!this.mWindowsFreezingScreen))
        {
            this.mDisplayFrozen = false;
            this.mH.removeMessages(17);
            i = 0;
            if ((this.mAnimator.mScreenRotationAnimation == null) || (!this.mAnimator.mScreenRotationAnimation.hasScreenshot()))
                break label209;
            if (!this.mAnimator.mScreenRotationAnimation.dismiss(this.mFxSession, 10000L, this.mTransitionAnimationScale, this.mCurDisplayWidth, this.mCurDisplayHeight))
                break label186;
            scheduleAnimationLocked();
        }
        while (true)
        {
            Surface.unfreezeDisplay(0);
            this.mInputMonitor.thawInputDispatchingLw();
            boolean bool = updateOrientationFromAppTokensLocked(false);
            this.mH.removeMessages(15);
            this.mH.sendMessageDelayed(this.mH.obtainMessage(15), 2000L);
            this.mScreenFrozenLock.release();
            if (i != 0)
                bool |= updateRotationUncheckedLocked(false);
            if (!bool)
                break;
            this.mH.sendEmptyMessage(18);
            break;
            break;
            label186: this.mAnimator.mScreenRotationAnimation.kill();
            this.mAnimator.mScreenRotationAnimation = null;
            i = 1;
            continue;
            label209: if (this.mAnimator.mScreenRotationAnimation != null)
            {
                this.mAnimator.mScreenRotationAnimation.kill();
                this.mAnimator.mScreenRotationAnimation = null;
            }
            i = 1;
        }
    }

    private boolean tmpRemoveAppWindowsLocked(WindowToken paramWindowToken)
    {
        boolean bool = true;
        int i = paramWindowToken.windows.size();
        if (i > 0)
            this.mWindowsChanged = bool;
        for (int j = 0; j < i; j++)
        {
            WindowState localWindowState1 = (WindowState)paramWindowToken.windows.get(j);
            this.mWindows.remove(localWindowState1);
            int k = localWindowState1.mChildWindows.size();
            while (k > 0)
            {
                k--;
                WindowState localWindowState2 = (WindowState)localWindowState1.mChildWindows.get(k);
                this.mWindows.remove(localWindowState2);
            }
        }
        if (i > 0);
        while (true)
        {
            return bool;
            bool = false;
        }
    }

    private int tmpRemoveWindowLocked(int paramInt, WindowState paramWindowState)
    {
        int i = this.mWindows.indexOf(paramWindowState);
        if (i >= 0)
        {
            if (i < paramInt)
                paramInt--;
            this.mWindows.remove(i);
            this.mWindowsChanged = true;
            int j = paramWindowState.mChildWindows.size();
            while (j > 0)
            {
                j--;
                WindowState localWindowState = (WindowState)paramWindowState.mChildWindows.get(j);
                int k = this.mWindows.indexOf(localWindowState);
                if (k >= 0)
                {
                    if (k < paramInt)
                        paramInt--;
                    this.mWindows.remove(k);
                }
            }
        }
        return paramInt;
    }

    private void updateAllDrawnLocked()
    {
        ArrayList localArrayList = this.mAnimatingAppTokens;
        int i = localArrayList.size();
        for (int j = 0; j < i; j++)
        {
            AppWindowToken localAppWindowToken = (AppWindowToken)localArrayList.get(j);
            if (!localAppWindowToken.allDrawn)
            {
                int k = localAppWindowToken.numInterestingWindows;
                if ((k > 0) && (localAppWindowToken.numDrawnWindows >= k))
                    localAppWindowToken.allDrawn = true;
            }
        }
    }

    private boolean updateFocusedWindowLocked(int paramInt, boolean paramBoolean)
    {
        boolean bool = false;
        int i = 1;
        WindowState localWindowState1 = computeFocusedWindowLocked();
        if (this.mCurrentFocus != localWindowState1)
        {
            Trace.traceBegin(32L, "wmUpdateFocus");
            this.mH.removeMessages(2);
            this.mH.sendEmptyMessage(2);
            WindowState localWindowState2 = this.mCurrentFocus;
            this.mCurrentFocus = localWindowState1;
            this.mAnimator.setCurrentFocus(localWindowState1);
            this.mLosingFocus.remove(localWindowState1);
            int j = this.mPolicy.focusChangedLw(localWindowState2, localWindowState1);
            WindowState localWindowState3 = this.mInputMethodWindow;
            if ((localWindowState1 != localWindowState3) && (localWindowState2 != localWindowState3))
            {
                if ((paramInt != i) && (paramInt != 3))
                    bool = i;
                if (moveInputMethodWindowsIfNeededLocked(bool))
                    this.mLayoutNeeded = i;
                if (paramInt == 2)
                {
                    performLayoutLockedInner(i, paramBoolean);
                    j &= -2;
                }
            }
            else
            {
                if ((j & 0x1) != 0)
                {
                    this.mLayoutNeeded = i;
                    if (paramInt == 2)
                        performLayoutLockedInner(i, paramBoolean);
                }
                if (paramInt != i)
                    finishUpdateFocusedWindowAfterAssignLayersLocked(paramBoolean);
                Trace.traceEnd(32L);
            }
        }
        while (true)
        {
            return i;
            if (paramInt != 3)
                break;
            assignLayersLocked();
            break;
            i = 0;
        }
    }

    private Configuration updateOrientationFromAppTokensLocked(Configuration paramConfiguration, IBinder paramIBinder)
    {
        Configuration localConfiguration = null;
        if (updateOrientationFromAppTokensLocked(false))
        {
            if (paramIBinder != null)
            {
                AppWindowToken localAppWindowToken = findAppWindowToken(paramIBinder);
                if (localAppWindowToken != null)
                    startAppFreezingScreenLocked(localAppWindowToken, 128);
            }
            localConfiguration = computeNewConfigurationLocked();
        }
        while (true)
        {
            return localConfiguration;
            if (paramConfiguration != null)
            {
                this.mTempConfiguration.setToDefaults();
                this.mTempConfiguration.fontScale = paramConfiguration.fontScale;
                if ((computeScreenConfigurationLocked(this.mTempConfiguration)) && (paramConfiguration.diff(this.mTempConfiguration) != 0))
                {
                    this.mWaitingForConfig = true;
                    this.mLayoutNeeded = true;
                    startFreezingDisplayLocked(false);
                    localConfiguration = new Configuration(this.mTempConfiguration);
                }
            }
        }
    }

    private void updateResizingWindows(WindowState paramWindowState)
    {
        WindowStateAnimator localWindowStateAnimator = paramWindowState.mWinAnimator;
        boolean bool2;
        boolean bool4;
        label83: int i;
        if ((paramWindowState.mHasSurface) && (!paramWindowState.mAppFreezing) && (paramWindowState.mLayoutSeq == this.mLayoutSeq))
        {
            boolean bool1 = paramWindowState.mContentInsetsChanged;
            if (paramWindowState.mLastContentInsets.equals(paramWindowState.mContentInsets))
                break label239;
            bool2 = true;
            paramWindowState.mContentInsetsChanged = (bool2 | bool1);
            boolean bool3 = paramWindowState.mVisibleInsetsChanged;
            if (paramWindowState.mLastVisibleInsets.equals(paramWindowState.mVisibleInsets))
                break label245;
            bool4 = true;
            paramWindowState.mVisibleInsetsChanged = (bool4 | bool3);
            if ((paramWindowState.mConfiguration == this.mCurConfiguration) || ((paramWindowState.mConfiguration != null) && (this.mCurConfiguration.diff(paramWindowState.mConfiguration) == 0)))
                break label251;
            i = 1;
            label127: paramWindowState.mLastFrame.set(paramWindowState.mFrame);
            if ((!paramWindowState.mContentInsetsChanged) && (!paramWindowState.mVisibleInsetsChanged) && (!localWindowStateAnimator.mSurfaceResized) && (i == 0))
                break label257;
            paramWindowState.mLastContentInsets.set(paramWindowState.mContentInsets);
            paramWindowState.mLastVisibleInsets.set(paramWindowState.mVisibleInsets);
            makeWindowFreezingScreenIfNeededLocked(paramWindowState);
            if (paramWindowState.mOrientationChanging)
            {
                localWindowStateAnimator.mDrawState = 1;
                if (paramWindowState.mAppToken != null)
                    paramWindowState.mAppToken.allDrawn = false;
            }
            if (!this.mResizingWindows.contains(paramWindowState))
                this.mResizingWindows.add(paramWindowState);
        }
        while (true)
        {
            return;
            label239: bool2 = false;
            break;
            label245: bool4 = false;
            break label83;
            label251: i = 0;
            break label127;
            label257: if ((paramWindowState.mOrientationChanging) && (paramWindowState.isDrawnLw()))
                paramWindowState.mOrientationChanging = false;
        }
    }

    public void addAppToken(int paramInt1, IApplicationToken paramIApplicationToken, int paramInt2, int paramInt3, boolean paramBoolean)
    {
        if (!checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "addAppToken()"))
            throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
        try
        {
            long l2 = paramIApplicationToken.getKeyDispatchingTimeout();
            l1 = l2 * 1000000L;
        }
        catch (RemoteException localRemoteException)
        {
            synchronized (this.mWindowMap)
            {
                long l1;
                while (findAppWindowToken(paramIApplicationToken.asBinder()) != null)
                {
                    Slog.w("WindowManager", "Attempted to add existing app token: " + paramIApplicationToken);
                    return;
                    localRemoteException = localRemoteException;
                    Slog.w("WindowManager", "Could not get dispatching timeout.", localRemoteException);
                    l1 = 5000000000L;
                }
                AppWindowToken localAppWindowToken = new AppWindowToken(this, paramIApplicationToken);
                localAppWindowToken.inputDispatchingTimeoutNanos = l1;
                localAppWindowToken.groupId = paramInt2;
                localAppWindowToken.appFullscreen = paramBoolean;
                localAppWindowToken.requestedOrientation = paramInt3;
                this.mAppTokens.add(paramInt1, localAppWindowToken);
                addAppTokenToAnimating(paramInt1, localAppWindowToken);
                this.mTokenMap.put(paramIApplicationToken.asBinder(), localAppWindowToken);
                localAppWindowToken.hidden = true;
                localAppWindowToken.hiddenRequested = true;
            }
        }
    }

    public WindowManagerPolicy.FakeWindow addFakeWindow(Looper paramLooper, InputEventReceiver.Factory paramFactory, String paramString, int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
    {
        synchronized (this.mWindowMap)
        {
            FakeWindowImpl localFakeWindowImpl = new FakeWindowImpl(this, paramLooper, paramFactory, paramString, paramInt1, paramInt2, paramBoolean1, paramBoolean2, paramBoolean3);
            while ((this.mFakeWindows.size() < 0) && (((FakeWindowImpl)this.mFakeWindows.get(0)).mWindowLayer > localFakeWindowImpl.mWindowLayer));
            this.mFakeWindows.add(0, localFakeWindowImpl);
            this.mInputMonitor.updateInputWindowsLw(true);
            return localFakeWindowImpl;
        }
    }

    void addInputMethodWindowToListLocked(WindowState paramWindowState)
    {
        int i = findDesiredInputMethodWindowIndexLocked(true);
        if (i >= 0)
        {
            paramWindowState.mTargetAppToken = this.mInputMethodTarget.mAppToken;
            this.mWindows.add(i, paramWindowState);
            this.mWindowsChanged = true;
            moveInputMethodDialogsLocked(i + 1);
        }
        while (true)
        {
            return;
            paramWindowState.mTargetAppToken = null;
            addWindowToListInOrderLocked(paramWindowState, true);
            moveInputMethodDialogsLocked(i);
        }
    }

    // ERROR //
    public int addWindow(Session paramSession, IWindow paramIWindow, int paramInt1, WindowManager.LayoutParams paramLayoutParams, int paramInt2, Rect paramRect, InputChannel paramInputChannel)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 368	com/android/server/wm/WindowManagerService:mPolicy	Landroid/view/WindowManagerPolicy;
        //     4: aload 4
        //     6: invokeinterface 2136 2 0
        //     11: istore 8
        //     13: iload 8
        //     15: ifeq +10 -> 25
        //     18: iload 8
        //     20: istore 16
        //     22: iload 16
        //     24: ireturn
        //     25: iconst_0
        //     26: istore 9
        //     28: aconst_null
        //     29: astore 10
        //     31: aload_0
        //     32: getfield 378	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
        //     35: astore 11
        //     37: aload 11
        //     39: monitorenter
        //     40: aload_0
        //     41: getfield 1406	com/android/server/wm/WindowManagerService:mDisplay	Landroid/view/Display;
        //     44: ifnonnull +22 -> 66
        //     47: new 2138	java/lang/IllegalStateException
        //     50: dup
        //     51: ldc_w 2140
        //     54: invokespecial 2141	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     57: athrow
        //     58: astore 12
        //     60: aload 11
        //     62: monitorexit
        //     63: aload 12
        //     65: athrow
        //     66: aload_0
        //     67: getfield 378	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
        //     70: aload_2
        //     71: invokeinterface 874 1 0
        //     76: invokevirtual 2144	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
        //     79: ifeq +45 -> 124
        //     82: ldc 117
        //     84: new 1311	java/lang/StringBuilder
        //     87: dup
        //     88: invokespecial 1312	java/lang/StringBuilder:<init>	()V
        //     91: ldc_w 2146
        //     94: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     97: aload_2
        //     98: invokevirtual 1321	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     101: ldc_w 2148
        //     104: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     107: invokevirtual 1329	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     110: invokestatic 1404	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     113: pop
        //     114: bipush 251
        //     116: istore 16
        //     118: aload 11
        //     120: monitorexit
        //     121: goto -99 -> 22
        //     124: aload 4
        //     126: getfield 849	android/view/WindowManager$LayoutParams:type	I
        //     129: sipush 1000
        //     132: if_icmplt +152 -> 284
        //     135: aload 4
        //     137: getfield 849	android/view/WindowManager$LayoutParams:type	I
        //     140: sipush 1999
        //     143: if_icmpgt +141 -> 284
        //     146: aload_0
        //     147: aconst_null
        //     148: aload 4
        //     150: getfield 2149	android/view/WindowManager$LayoutParams:token	Landroid/os/IBinder;
        //     153: iconst_0
        //     154: invokevirtual 2153	com/android/server/wm/WindowManagerService:windowForClientLocked	(Lcom/android/server/wm/Session;Landroid/os/IBinder;Z)Lcom/android/server/wm/WindowState;
        //     157: astore 10
        //     159: aload 10
        //     161: ifnonnull +49 -> 210
        //     164: ldc 117
        //     166: new 1311	java/lang/StringBuilder
        //     169: dup
        //     170: invokespecial 1312	java/lang/StringBuilder:<init>	()V
        //     173: ldc_w 2155
        //     176: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     179: aload 4
        //     181: getfield 2149	android/view/WindowManager$LayoutParams:token	Landroid/os/IBinder;
        //     184: invokevirtual 1321	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     187: ldc_w 2157
        //     190: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     193: invokevirtual 1329	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     196: invokestatic 1404	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     199: pop
        //     200: bipush 254
        //     202: istore 16
        //     204: aload 11
        //     206: monitorexit
        //     207: goto -185 -> 22
        //     210: aload 10
        //     212: getfield 844	com/android/server/wm/WindowState:mAttrs	Landroid/view/WindowManager$LayoutParams;
        //     215: getfield 849	android/view/WindowManager$LayoutParams:type	I
        //     218: sipush 1000
        //     221: if_icmplt +63 -> 284
        //     224: aload 10
        //     226: getfield 844	com/android/server/wm/WindowState:mAttrs	Landroid/view/WindowManager$LayoutParams;
        //     229: getfield 849	android/view/WindowManager$LayoutParams:type	I
        //     232: sipush 1999
        //     235: if_icmpgt +49 -> 284
        //     238: ldc 117
        //     240: new 1311	java/lang/StringBuilder
        //     243: dup
        //     244: invokespecial 1312	java/lang/StringBuilder:<init>	()V
        //     247: ldc_w 2159
        //     250: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     253: aload 4
        //     255: getfield 2149	android/view/WindowManager$LayoutParams:token	Landroid/os/IBinder;
        //     258: invokevirtual 1321	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     261: ldc_w 2157
        //     264: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     267: invokevirtual 1329	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     270: invokestatic 1404	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     273: pop
        //     274: bipush 254
        //     276: istore 16
        //     278: aload 11
        //     280: monitorexit
        //     281: goto -259 -> 22
        //     284: iconst_0
        //     285: istore 13
        //     287: aload_0
        //     288: getfield 380	com/android/server/wm/WindowManagerService:mTokenMap	Ljava/util/HashMap;
        //     291: aload 4
        //     293: getfield 2149	android/view/WindowManager$LayoutParams:token	Landroid/os/IBinder;
        //     296: invokevirtual 877	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     299: checkcast 834	com/android/server/wm/WindowToken
        //     302: astore 14
        //     304: aload 14
        //     306: ifnonnull +341 -> 647
        //     309: aload 4
        //     311: getfield 849	android/view/WindowManager$LayoutParams:type	I
        //     314: iconst_1
        //     315: if_icmplt +59 -> 374
        //     318: aload 4
        //     320: getfield 849	android/view/WindowManager$LayoutParams:type	I
        //     323: bipush 99
        //     325: if_icmpgt +49 -> 374
        //     328: ldc 117
        //     330: new 1311	java/lang/StringBuilder
        //     333: dup
        //     334: invokespecial 1312	java/lang/StringBuilder:<init>	()V
        //     337: ldc_w 2161
        //     340: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     343: aload 4
        //     345: getfield 2149	android/view/WindowManager$LayoutParams:token	Landroid/os/IBinder;
        //     348: invokevirtual 1321	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     351: ldc_w 2157
        //     354: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     357: invokevirtual 1329	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     360: invokestatic 1404	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     363: pop
        //     364: bipush 255
        //     366: istore 16
        //     368: aload 11
        //     370: monitorexit
        //     371: goto -349 -> 22
        //     374: aload 4
        //     376: getfield 849	android/view/WindowManager$LayoutParams:type	I
        //     379: sipush 2011
        //     382: if_icmpne +49 -> 431
        //     385: ldc 117
        //     387: new 1311	java/lang/StringBuilder
        //     390: dup
        //     391: invokespecial 1312	java/lang/StringBuilder:<init>	()V
        //     394: ldc_w 2163
        //     397: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     400: aload 4
        //     402: getfield 2149	android/view/WindowManager$LayoutParams:token	Landroid/os/IBinder;
        //     405: invokevirtual 1321	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     408: ldc_w 2157
        //     411: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     414: invokevirtual 1329	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     417: invokestatic 1404	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     420: pop
        //     421: bipush 255
        //     423: istore 16
        //     425: aload 11
        //     427: monitorexit
        //     428: goto -406 -> 22
        //     431: aload 4
        //     433: getfield 849	android/view/WindowManager$LayoutParams:type	I
        //     436: sipush 2013
        //     439: if_icmpne +49 -> 488
        //     442: ldc 117
        //     444: new 1311	java/lang/StringBuilder
        //     447: dup
        //     448: invokespecial 1312	java/lang/StringBuilder:<init>	()V
        //     451: ldc_w 2165
        //     454: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     457: aload 4
        //     459: getfield 2149	android/view/WindowManager$LayoutParams:token	Landroid/os/IBinder;
        //     462: invokevirtual 1321	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     465: ldc_w 2157
        //     468: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     471: invokevirtual 1329	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     474: invokestatic 1404	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     477: pop
        //     478: bipush 255
        //     480: istore 16
        //     482: aload 11
        //     484: monitorexit
        //     485: goto -463 -> 22
        //     488: aload 4
        //     490: getfield 849	android/view/WindowManager$LayoutParams:type	I
        //     493: sipush 2023
        //     496: if_icmpne +49 -> 545
        //     499: ldc 117
        //     501: new 1311	java/lang/StringBuilder
        //     504: dup
        //     505: invokespecial 1312	java/lang/StringBuilder:<init>	()V
        //     508: ldc_w 2167
        //     511: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     514: aload 4
        //     516: getfield 2149	android/view/WindowManager$LayoutParams:token	Landroid/os/IBinder;
        //     519: invokevirtual 1321	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     522: ldc_w 2157
        //     525: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     528: invokevirtual 1329	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     531: invokestatic 1404	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     534: pop
        //     535: bipush 255
        //     537: istore 16
        //     539: aload 11
        //     541: monitorexit
        //     542: goto -520 -> 22
        //     545: new 834	com/android/server/wm/WindowToken
        //     548: dup
        //     549: aload_0
        //     550: aload 4
        //     552: getfield 2149	android/view/WindowManager$LayoutParams:token	Landroid/os/IBinder;
        //     555: bipush 255
        //     557: iconst_0
        //     558: invokespecial 2170	com/android/server/wm/WindowToken:<init>	(Lcom/android/server/wm/WindowManagerService;Landroid/os/IBinder;IZ)V
        //     561: astore 14
        //     563: iconst_1
        //     564: istore 13
        //     566: new 403	com/android/server/wm/WindowState
        //     569: dup
        //     570: aload_0
        //     571: aload_1
        //     572: aload_2
        //     573: aload 14
        //     575: aload 10
        //     577: iload_3
        //     578: aload 4
        //     580: iload 5
        //     582: invokespecial 2173	com/android/server/wm/WindowState:<init>	(Lcom/android/server/wm/WindowManagerService;Lcom/android/server/wm/Session;Landroid/view/IWindow;Lcom/android/server/wm/WindowToken;Lcom/android/server/wm/WindowState;ILandroid/view/WindowManager$LayoutParams;I)V
        //     585: astore 17
        //     587: aload 17
        //     589: getfield 2177	com/android/server/wm/WindowState:mDeathRecipient	Lcom/android/server/wm/WindowState$DeathRecipient;
        //     592: ifnonnull +411 -> 1003
        //     595: ldc 117
        //     597: new 1311	java/lang/StringBuilder
        //     600: dup
        //     601: invokespecial 1312	java/lang/StringBuilder:<init>	()V
        //     604: ldc_w 2179
        //     607: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     610: aload_2
        //     611: invokeinterface 874 1 0
        //     616: invokevirtual 1321	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     619: ldc_w 2181
        //     622: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     625: invokevirtual 1329	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     628: invokestatic 1404	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     631: pop
        //     632: bipush 252
        //     634: istore 16
        //     636: aload 11
        //     638: monitorexit
        //     639: goto -617 -> 22
        //     642: astore 12
        //     644: goto -584 -> 60
        //     647: aload 4
        //     649: getfield 849	android/view/WindowManager$LayoutParams:type	I
        //     652: iconst_1
        //     653: if_icmplt +146 -> 799
        //     656: aload 4
        //     658: getfield 849	android/view/WindowManager$LayoutParams:type	I
        //     661: bipush 99
        //     663: if_icmpgt +136 -> 799
        //     666: aload 14
        //     668: getfield 840	com/android/server/wm/WindowToken:appWindowToken	Lcom/android/server/wm/AppWindowToken;
        //     671: astore 34
        //     673: aload 34
        //     675: ifnonnull +46 -> 721
        //     678: ldc 117
        //     680: new 1311	java/lang/StringBuilder
        //     683: dup
        //     684: invokespecial 1312	java/lang/StringBuilder:<init>	()V
        //     687: ldc_w 2183
        //     690: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     693: aload 14
        //     695: invokevirtual 1321	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     698: ldc_w 2157
        //     701: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     704: invokevirtual 1329	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     707: invokestatic 1404	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     710: pop
        //     711: bipush 253
        //     713: istore 16
        //     715: aload 11
        //     717: monitorexit
        //     718: goto -696 -> 22
        //     721: aload 34
        //     723: getfield 1014	com/android/server/wm/AppWindowToken:removed	Z
        //     726: ifeq +46 -> 772
        //     729: ldc 117
        //     731: new 1311	java/lang/StringBuilder
        //     734: dup
        //     735: invokespecial 1312	java/lang/StringBuilder:<init>	()V
        //     738: ldc_w 2185
        //     741: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     744: aload 14
        //     746: invokevirtual 1321	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     749: ldc_w 2157
        //     752: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     755: invokevirtual 1329	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     758: invokestatic 1404	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     761: pop
        //     762: bipush 252
        //     764: istore 16
        //     766: aload 11
        //     768: monitorexit
        //     769: goto -747 -> 22
        //     772: aload 4
        //     774: getfield 849	android/view/WindowManager$LayoutParams:type	I
        //     777: iconst_3
        //     778: if_icmpne -212 -> 566
        //     781: aload 34
        //     783: getfield 1910	com/android/server/wm/AppWindowToken:firstWindowDrawn	Z
        //     786: ifeq -220 -> 566
        //     789: bipush 250
        //     791: istore 16
        //     793: aload 11
        //     795: monitorexit
        //     796: goto -774 -> 22
        //     799: aload 4
        //     801: getfield 849	android/view/WindowManager$LayoutParams:type	I
        //     804: sipush 2011
        //     807: if_icmpne +60 -> 867
        //     810: aload 14
        //     812: getfield 1702	com/android/server/wm/WindowToken:windowType	I
        //     815: sipush 2011
        //     818: if_icmpeq -252 -> 566
        //     821: ldc 117
        //     823: new 1311	java/lang/StringBuilder
        //     826: dup
        //     827: invokespecial 1312	java/lang/StringBuilder:<init>	()V
        //     830: ldc_w 2187
        //     833: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     836: aload 4
        //     838: getfield 2149	android/view/WindowManager$LayoutParams:token	Landroid/os/IBinder;
        //     841: invokevirtual 1321	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     844: ldc_w 2157
        //     847: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     850: invokevirtual 1329	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     853: invokestatic 1404	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     856: pop
        //     857: bipush 255
        //     859: istore 16
        //     861: aload 11
        //     863: monitorexit
        //     864: goto -842 -> 22
        //     867: aload 4
        //     869: getfield 849	android/view/WindowManager$LayoutParams:type	I
        //     872: sipush 2013
        //     875: if_icmpne +60 -> 935
        //     878: aload 14
        //     880: getfield 1702	com/android/server/wm/WindowToken:windowType	I
        //     883: sipush 2013
        //     886: if_icmpeq -320 -> 566
        //     889: ldc 117
        //     891: new 1311	java/lang/StringBuilder
        //     894: dup
        //     895: invokespecial 1312	java/lang/StringBuilder:<init>	()V
        //     898: ldc_w 2189
        //     901: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     904: aload 4
        //     906: getfield 2149	android/view/WindowManager$LayoutParams:token	Landroid/os/IBinder;
        //     909: invokevirtual 1321	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     912: ldc_w 2157
        //     915: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     918: invokevirtual 1329	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     921: invokestatic 1404	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     924: pop
        //     925: bipush 255
        //     927: istore 16
        //     929: aload 11
        //     931: monitorexit
        //     932: goto -910 -> 22
        //     935: aload 4
        //     937: getfield 849	android/view/WindowManager$LayoutParams:type	I
        //     940: sipush 2023
        //     943: if_icmpne -377 -> 566
        //     946: aload 14
        //     948: getfield 1702	com/android/server/wm/WindowToken:windowType	I
        //     951: sipush 2023
        //     954: if_icmpeq -388 -> 566
        //     957: ldc 117
        //     959: new 1311	java/lang/StringBuilder
        //     962: dup
        //     963: invokespecial 1312	java/lang/StringBuilder:<init>	()V
        //     966: ldc_w 2191
        //     969: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     972: aload 4
        //     974: getfield 2149	android/view/WindowManager$LayoutParams:token	Landroid/os/IBinder;
        //     977: invokevirtual 1321	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     980: ldc_w 2157
        //     983: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     986: invokevirtual 1329	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     989: invokestatic 1404	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     992: pop
        //     993: bipush 255
        //     995: istore 16
        //     997: aload 11
        //     999: monitorexit
        //     1000: goto -978 -> 22
        //     1003: aload_0
        //     1004: getfield 368	com/android/server/wm/WindowManagerService:mPolicy	Landroid/view/WindowManagerPolicy;
        //     1007: aload 17
        //     1009: getfield 844	com/android/server/wm/WindowState:mAttrs	Landroid/view/WindowManager$LayoutParams;
        //     1012: invokeinterface 2195 2 0
        //     1017: aload_0
        //     1018: getfield 368	com/android/server/wm/WindowManagerService:mPolicy	Landroid/view/WindowManagerPolicy;
        //     1021: aload 17
        //     1023: aload 4
        //     1025: invokeinterface 2199 3 0
        //     1030: istore 18
        //     1032: iload 18
        //     1034: ifeq +13 -> 1047
        //     1037: aload 11
        //     1039: monitorexit
        //     1040: iload 18
        //     1042: istore 16
        //     1044: goto -1022 -> 22
        //     1047: aload 7
        //     1049: ifnull +58 -> 1107
        //     1052: iconst_2
        //     1053: aload 4
        //     1055: getfield 2202	android/view/WindowManager$LayoutParams:inputFeatures	I
        //     1058: iand
        //     1059: ifne +48 -> 1107
        //     1062: aload 17
        //     1064: invokevirtual 2205	com/android/server/wm/WindowState:makeInputChannelName	()Ljava/lang/String;
        //     1067: invokestatic 2211	android/view/InputChannel:openInputChannelPair	(Ljava/lang/String;)[Landroid/view/InputChannel;
        //     1070: astore 30
        //     1072: aload 17
        //     1074: aload 30
        //     1076: iconst_0
        //     1077: aaload
        //     1078: invokevirtual 2215	com/android/server/wm/WindowState:setInputChannel	(Landroid/view/InputChannel;)V
        //     1081: aload 30
        //     1083: iconst_1
        //     1084: aaload
        //     1085: aload 7
        //     1087: invokevirtual 2218	android/view/InputChannel:transferTo	(Landroid/view/InputChannel;)V
        //     1090: aload_0
        //     1091: getfield 722	com/android/server/wm/WindowManagerService:mInputManager	Lcom/android/server/input/InputManagerService;
        //     1094: aload 17
        //     1096: getfield 2222	com/android/server/wm/WindowState:mInputChannel	Landroid/view/InputChannel;
        //     1099: aload 17
        //     1101: getfield 2226	com/android/server/wm/WindowState:mInputWindowHandle	Lcom/android/server/input/InputWindowHandle;
        //     1104: invokevirtual 2230	com/android/server/input/InputManagerService:registerInputChannel	(Landroid/view/InputChannel;Lcom/android/server/input/InputWindowHandle;)V
        //     1107: iconst_0
        //     1108: istore 19
        //     1110: invokestatic 2233	android/os/Binder:clearCallingIdentity	()J
        //     1113: lstore 20
        //     1115: iload 13
        //     1117: ifeq +18 -> 1135
        //     1120: aload_0
        //     1121: getfield 380	com/android/server/wm/WindowManagerService:mTokenMap	Ljava/util/HashMap;
        //     1124: aload 4
        //     1126: getfield 2149	android/view/WindowManager$LayoutParams:token	Landroid/os/IBinder;
        //     1129: aload 14
        //     1131: invokevirtual 2114	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     1134: pop
        //     1135: aload 17
        //     1137: invokevirtual 2236	com/android/server/wm/WindowState:attach	()V
        //     1140: aload_0
        //     1141: getfield 378	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
        //     1144: aload_2
        //     1145: invokeinterface 874 1 0
        //     1150: aload 17
        //     1152: invokevirtual 2114	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     1155: pop
        //     1156: aload 4
        //     1158: getfield 849	android/view/WindowManager$LayoutParams:type	I
        //     1161: iconst_3
        //     1162: if_icmpne +21 -> 1183
        //     1165: aload 14
        //     1167: getfield 840	com/android/server/wm/WindowToken:appWindowToken	Lcom/android/server/wm/AppWindowToken;
        //     1170: ifnull +13 -> 1183
        //     1173: aload 14
        //     1175: getfield 840	com/android/server/wm/WindowToken:appWindowToken	Lcom/android/server/wm/AppWindowToken;
        //     1178: aload 17
        //     1180: putfield 864	com/android/server/wm/AppWindowToken:startingWindow	Lcom/android/server/wm/WindowState;
        //     1183: iconst_1
        //     1184: istore 23
        //     1186: aload 4
        //     1188: getfield 849	android/view/WindowManager$LayoutParams:type	I
        //     1191: sipush 2011
        //     1194: if_icmpne +190 -> 1384
        //     1197: aload 17
        //     1199: iconst_1
        //     1200: putfield 2239	com/android/server/wm/WindowState:mGivenInsetsPending	Z
        //     1203: aload_0
        //     1204: aload 17
        //     1206: putfield 542	com/android/server/wm/WindowManagerService:mInputMethodWindow	Lcom/android/server/wm/WindowState;
        //     1209: aload_0
        //     1210: aload 17
        //     1212: invokevirtual 2241	com/android/server/wm/WindowManagerService:addInputMethodWindowToListLocked	(Lcom/android/server/wm/WindowState;)V
        //     1215: iconst_0
        //     1216: istore 23
        //     1218: aload 17
        //     1220: getfield 959	com/android/server/wm/WindowState:mWinAnimator	Lcom/android/server/wm/WindowStateAnimator;
        //     1223: iconst_1
        //     1224: putfield 2244	com/android/server/wm/WindowStateAnimator:mEnterAnimationPending	Z
        //     1227: aload_0
        //     1228: getfield 368	com/android/server/wm/WindowManagerService:mPolicy	Landroid/view/WindowManagerPolicy;
        //     1231: aload 4
        //     1233: aload 6
        //     1235: invokeinterface 2248 3 0
        //     1240: aload_0
        //     1241: getfield 581	com/android/server/wm/WindowManagerService:mInTouchMode	Z
        //     1244: ifeq +8 -> 1252
        //     1247: iconst_0
        //     1248: iconst_1
        //     1249: ior
        //     1250: istore 19
        //     1252: aload 17
        //     1254: getfield 856	com/android/server/wm/WindowState:mAppToken	Lcom/android/server/wm/AppWindowToken;
        //     1257: ifnull +216 -> 1473
        //     1260: aload 17
        //     1262: getfield 856	com/android/server/wm/WindowState:mAppToken	Lcom/android/server/wm/AppWindowToken;
        //     1265: getfield 2251	com/android/server/wm/AppWindowToken:clientHidden	Z
        //     1268: ifne +6 -> 1274
        //     1271: goto +202 -> 1473
        //     1274: aload_0
        //     1275: getfield 592	com/android/server/wm/WindowManagerService:mInputMonitor	Lcom/android/server/wm/InputMonitor;
        //     1278: invokevirtual 1362	com/android/server/wm/InputMonitor:setUpdateInputWindowsNeededLw	()V
        //     1281: iconst_0
        //     1282: istore 25
        //     1284: aload 17
        //     1286: invokevirtual 1017	com/android/server/wm/WindowState:canReceiveKeys	()Z
        //     1289: ifeq +19 -> 1308
        //     1292: aload_0
        //     1293: iconst_1
        //     1294: iconst_0
        //     1295: invokespecial 1366	com/android/server/wm/WindowManagerService:updateFocusedWindowLocked	(IZ)Z
        //     1298: istore 25
        //     1300: iload 25
        //     1302: ifeq +6 -> 1308
        //     1305: iconst_0
        //     1306: istore 23
        //     1308: iload 23
        //     1310: ifeq +9 -> 1319
        //     1313: aload_0
        //     1314: iconst_0
        //     1315: invokevirtual 1222	com/android/server/wm/WindowManagerService:moveInputMethodWindowsIfNeededLocked	(Z)Z
        //     1318: pop
        //     1319: aload_0
        //     1320: invokespecial 1368	com/android/server/wm/WindowManagerService:assignLayersLocked	()V
        //     1323: iload 25
        //     1325: ifeq +8 -> 1333
        //     1328: aload_0
        //     1329: iconst_0
        //     1330: invokespecial 2037	com/android/server/wm/WindowManagerService:finishUpdateFocusedWindowAfterAssignLayersLocked	(Z)V
        //     1333: aload_0
        //     1334: getfield 592	com/android/server/wm/WindowManagerService:mInputMonitor	Lcom/android/server/wm/InputMonitor;
        //     1337: iconst_0
        //     1338: invokevirtual 1371	com/android/server/wm/InputMonitor:updateInputWindowsLw	(Z)V
        //     1341: aload 17
        //     1343: invokevirtual 999	com/android/server/wm/WindowState:isVisibleOrAdding	()Z
        //     1346: ifeq +14 -> 1360
        //     1349: aload_0
        //     1350: iconst_0
        //     1351: invokevirtual 1593	com/android/server/wm/WindowManagerService:updateOrientationFromAppTokensLocked	(Z)Z
        //     1354: ifeq +6 -> 1360
        //     1357: iconst_1
        //     1358: istore 9
        //     1360: aload 11
        //     1362: monitorexit
        //     1363: iload 9
        //     1365: ifeq +7 -> 1372
        //     1368: aload_0
        //     1369: invokevirtual 2254	com/android/server/wm/WindowManagerService:sendNewConfiguration	()V
        //     1372: lload 20
        //     1374: invokestatic 2257	android/os/Binder:restoreCallingIdentity	(J)V
        //     1377: iload 19
        //     1379: istore 16
        //     1381: goto -1359 -> 22
        //     1384: aload 4
        //     1386: getfield 849	android/view/WindowManager$LayoutParams:type	I
        //     1389: sipush 2012
        //     1392: if_icmpne +30 -> 1422
        //     1395: aload_0
        //     1396: getfield 544	com/android/server/wm/WindowManagerService:mInputMethodDialogs	Ljava/util/ArrayList;
        //     1399: aload 17
        //     1401: invokevirtual 861	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     1404: pop
        //     1405: aload_0
        //     1406: aload 17
        //     1408: iconst_1
        //     1409: invokespecial 1802	com/android/server/wm/WindowManagerService:addWindowToListInOrderLocked	(Lcom/android/server/wm/WindowState;Z)V
        //     1412: aload_0
        //     1413: invokevirtual 2260	com/android/server/wm/WindowManagerService:adjustInputMethodDialogsLocked	()V
        //     1416: iconst_0
        //     1417: istore 23
        //     1419: goto -201 -> 1218
        //     1422: aload_0
        //     1423: aload 17
        //     1425: iconst_1
        //     1426: invokespecial 1802	com/android/server/wm/WindowManagerService:addWindowToListInOrderLocked	(Lcom/android/server/wm/WindowState;Z)V
        //     1429: aload 4
        //     1431: getfield 849	android/view/WindowManager$LayoutParams:type	I
        //     1434: sipush 2013
        //     1437: if_icmpne +16 -> 1453
        //     1440: aload_0
        //     1441: lconst_0
        //     1442: putfield 1904	com/android/server/wm/WindowManagerService:mLastWallpaperTimeoutTime	J
        //     1445: aload_0
        //     1446: invokevirtual 911	com/android/server/wm/WindowManagerService:adjustWallpaperWindowsLocked	()I
        //     1449: pop
        //     1450: goto -232 -> 1218
        //     1453: ldc_w 1542
        //     1456: aload 4
        //     1458: getfield 996	android/view/WindowManager$LayoutParams:flags	I
        //     1461: iand
        //     1462: ifeq -244 -> 1218
        //     1465: aload_0
        //     1466: invokevirtual 911	com/android/server/wm/WindowManagerService:adjustWallpaperWindowsLocked	()I
        //     1469: pop
        //     1470: goto -252 -> 1218
        //     1473: iload 19
        //     1475: iconst_2
        //     1476: ior
        //     1477: istore 19
        //     1479: goto -205 -> 1274
        //
        // Exception table:
        //     from	to	target	type
        //     40	58	58	finally
        //     66	587	58	finally
        //     647	1000	58	finally
        //     60	63	642	finally
        //     587	639	642	finally
        //     1003	1363	642	finally
        //     1384	1470	642	finally
    }

    public void addWindowChangeListener(WindowChangeListener paramWindowChangeListener)
    {
        synchronized (this.mWindowMap)
        {
            this.mWindowChangeListeners.add(paramWindowChangeListener);
            return;
        }
    }

    public void addWindowToken(IBinder paramIBinder, int paramInt)
    {
        if (!checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "addWindowToken()"))
            throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
        synchronized (this.mWindowMap)
        {
            if ((WindowToken)this.mTokenMap.get(paramIBinder) != null)
            {
                Slog.w("WindowManager", "Attempted to add existing input method token: " + paramIBinder);
            }
            else
            {
                WindowToken localWindowToken = new WindowToken(this, paramIBinder, paramInt, true);
                this.mTokenMap.put(paramIBinder, localWindowToken);
                if (paramInt == 2013)
                    this.mWallpaperTokens.add(localWindowToken);
            }
        }
    }

    void adjustInputMethodDialogsLocked()
    {
        moveInputMethodDialogsLocked(findDesiredInputMethodWindowIndexLocked(true));
    }

    int adjustWallpaperWindowsLocked()
    {
        this.mInnerFields.mWallpaperMayChange = false;
        int i = 0;
        int j = this.mAppDisplayWidth;
        int k = this.mAppDisplayHeight;
        ArrayList localArrayList = this.mWindows;
        int m = localArrayList.size();
        WindowState localWindowState1 = null;
        Object localObject = null;
        int n = 0;
        WindowState localWindowState2 = null;
        int i1 = 0;
        int i2 = -1;
        int i3 = m;
        WindowState localWindowState9;
        do
        {
            WindowState localWindowState7;
            do
            {
                while (true)
                {
                    if (i3 <= 0)
                        break label230;
                    i3--;
                    localWindowState1 = (WindowState)localArrayList.get(i3);
                    if (localWindowState1.mAttrs.type != 2013)
                        break;
                    if (localWindowState2 == null)
                    {
                        localWindowState2 = localWindowState1;
                        i1 = i3;
                    }
                }
                localWindowState2 = null;
                localWindowState7 = this.mAnimator.mWindowDetachedWallpaper;
            }
            while ((localWindowState1 != localWindowState7) && (localWindowState1.mAppToken != null) && (localWindowState1.mAppToken.hidden) && (localWindowState1.mAppToken.mAppAnimator.animation == null));
            if (((0x100000 & localWindowState1.mAttrs.flags) == 0) || (!localWindowState1.isReadyForDisplay()) || ((this.mWallpaperTarget != localWindowState1) && (!localWindowState1.isDrawnLw())))
                break label280;
            localObject = localWindowState1;
            n = i3;
            localWindowState9 = this.mWallpaperTarget;
        }
        while ((localWindowState1 == localWindowState9) && (localWindowState1.mWinAnimator.isAnimating()));
        label230: if ((localObject == null) && (i2 >= 0))
        {
            localObject = localWindowState1;
            n = i2;
        }
        int i7;
        if (this.mNextAppTransition != -1)
            if ((this.mWallpaperTarget != null) && (this.mWallpaperTarget.mAppToken != null))
                i7 = 0;
        while (true)
        {
            return i7;
            label280: WindowState localWindowState8 = this.mAnimator.mWindowDetachedWallpaper;
            if (localWindowState1 != localWindowState8)
                break;
            i2 = i3;
            break;
            label1029: if ((localObject != null) && (((WindowState)localObject).mAppToken != null))
            {
                i7 = 0;
            }
            else
            {
                WindowState localWindowState6;
                int i12;
                int i13;
                label435: int i14;
                label492: boolean bool1;
                label500: int i10;
                label541: label566: WindowState localWindowState5;
                WindowState localWindowState3;
                int i6;
                if (this.mWallpaperTarget != localObject)
                {
                    this.mLowerWallpaperTarget = null;
                    this.mUpperWallpaperTarget = null;
                    localWindowState6 = this.mWallpaperTarget;
                    this.mWallpaperTarget = ((WindowState)localObject);
                    if ((localObject != null) && (localWindowState6 != null))
                    {
                        if ((localWindowState6.mWinAnimator.mAnimation == null) && ((localWindowState6.mAppToken == null) || (localWindowState6.mAppToken.mAppAnimator.animation == null)))
                            break label923;
                        i12 = 1;
                        if ((((WindowState)localObject).mWinAnimator.mAnimation == null) && ((((WindowState)localObject).mAppToken == null) || (((WindowState)localObject).mAppToken.mAppAnimator.animation == null)))
                            break label929;
                        i13 = 1;
                        if ((i13 != 0) && (i12 != 0))
                        {
                            i14 = localArrayList.indexOf(localWindowState6);
                            if (i14 >= 0)
                            {
                                if ((((WindowState)localObject).mAppToken == null) || (!((WindowState)localObject).mAppToken.hiddenRequested))
                                    break label935;
                                this.mWallpaperTarget = localWindowState6;
                                localObject = localWindowState6;
                                n = i14;
                            }
                        }
                    }
                    if (localObject == null)
                        break label1106;
                    bool1 = true;
                    if (bool1)
                    {
                        bool1 = isWallpaperVisible((WindowState)localObject);
                        if ((this.mLowerWallpaperTarget != null) || (((WindowState)localObject).mAppToken == null))
                            break label1112;
                        i10 = ((WindowState)localObject).mAppToken.mAppAnimator.animLayerAdjustment;
                        this.mWallpaperAnimLayerAdjustment = i10;
                        int i11 = 1000 + 10000 * this.mPolicy.getMaxWallpaperLayer();
                        if (n > 0)
                        {
                            localWindowState5 = (WindowState)localArrayList.get(n - 1);
                            if ((localWindowState5.mBaseLayer >= i11) || (localWindowState5.mAttachedWindow == localObject) || ((((WindowState)localObject).mAttachedWindow != null) && (localWindowState5.mAttachedWindow == ((WindowState)localObject).mAttachedWindow)) || ((localWindowState5.mAttrs.type == 3) && (((WindowState)localObject).mToken != null) && (localWindowState5.mToken == ((WindowState)localObject).mToken)))
                                break label1118;
                        }
                    }
                    if ((localObject != null) || (localWindowState2 == null))
                        break label1128;
                    localWindowState3 = localWindowState2;
                    n = i1 + 1;
                    if (bool1)
                    {
                        if (this.mWallpaperTarget.mWallpaperX >= 0.0F)
                        {
                            this.mLastWallpaperX = this.mWallpaperTarget.mWallpaperX;
                            this.mLastWallpaperXStep = this.mWallpaperTarget.mWallpaperXStep;
                        }
                        if (this.mWallpaperTarget.mWallpaperY >= 0.0F)
                        {
                            this.mLastWallpaperY = this.mWallpaperTarget.mWallpaperY;
                            this.mLastWallpaperYStep = this.mWallpaperTarget.mWallpaperYStep;
                        }
                    }
                    i6 = this.mWallpaperTokens.size();
                }
                label805: label827: label1224: 
                while (true)
                {
                    if (i6 <= 0)
                        break label1226;
                    i6--;
                    WindowToken localWindowToken = (WindowToken)this.mWallpaperTokens.get(i6);
                    boolean bool2;
                    int i8;
                    if (localWindowToken.hidden == bool1)
                    {
                        i |= 4;
                        if (!bool1)
                        {
                            bool2 = true;
                            localWindowToken.hidden = bool2;
                            this.mLayoutNeeded = true;
                        }
                    }
                    else
                    {
                        i8 = localWindowToken.windows.size();
                    }
                    while (true)
                    {
                        if (i8 <= 0)
                            break label1224;
                        i8--;
                        WindowState localWindowState4 = (WindowState)localWindowToken.windows.get(i8);
                        if (bool1)
                            updateWallpaperOffsetLocked(localWindowState4, j, k, false);
                        dispatchWallpaperVisibility(localWindowState4, bool1);
                        localWindowState4.mWinAnimator.mAnimLayer = (localWindowState4.mLayer + this.mWallpaperAnimLayerAdjustment);
                        if (localWindowState4 == localWindowState3)
                        {
                            n--;
                            if (n > 0);
                            for (localWindowState3 = (WindowState)localArrayList.get(n - 1); ; localWindowState3 = null)
                            {
                                break label827;
                                i12 = 0;
                                break;
                                i13 = 0;
                                break label435;
                                if (n > i14)
                                {
                                    this.mUpperWallpaperTarget = ((WindowState)localObject);
                                    this.mLowerWallpaperTarget = localWindowState6;
                                    localObject = localWindowState6;
                                    n = i14;
                                    break label492;
                                }
                                this.mUpperWallpaperTarget = localWindowState6;
                                this.mLowerWallpaperTarget = ((WindowState)localObject);
                                break label492;
                                if (this.mLowerWallpaperTarget == null)
                                    break label492;
                                int i4;
                                if ((this.mLowerWallpaperTarget.mWinAnimator.mAnimation != null) || ((this.mLowerWallpaperTarget.mAppToken != null) && (this.mLowerWallpaperTarget.mAppToken.mAppAnimator.animation != null)))
                                {
                                    i4 = 1;
                                    if ((this.mUpperWallpaperTarget.mWinAnimator.mAnimation == null) && ((this.mUpperWallpaperTarget.mAppToken == null) || (this.mUpperWallpaperTarget.mAppToken.mAppAnimator.animation == null)))
                                        break label1100;
                                }
                                label1100: for (int i5 = 1; ; i5 = 0)
                                {
                                    if ((i4 != 0) && (i5 != 0))
                                        break label1104;
                                    this.mLowerWallpaperTarget = null;
                                    this.mUpperWallpaperTarget = null;
                                    break;
                                    i4 = 0;
                                    break label1029;
                                }
                                label1104: break label492;
                                label1106: bool1 = false;
                                break label500;
                                label1112: i10 = 0;
                                break label541;
                                label1118: localObject = localWindowState5;
                                n--;
                                break label566;
                                label1128: if (n > 0);
                                for (localWindowState3 = (WindowState)localArrayList.get(n - 1); ; localWindowState3 = null)
                                    break;
                                bool2 = false;
                                break label805;
                            }
                        }
                        int i9 = localArrayList.indexOf(localWindowState4);
                        if (i9 >= 0)
                        {
                            localArrayList.remove(i9);
                            this.mWindowsChanged = true;
                            if (i9 < n)
                                n--;
                        }
                        localArrayList.add(n, localWindowState4);
                        this.mWindowsChanged = true;
                        i |= 2;
                    }
                }
                label923: label929: label935: label1226: i7 = i;
            }
        }
    }

    void bulkSetParameters(int paramInt1, int paramInt2)
    {
        this.mH.sendMessage(this.mH.obtainMessage(25, paramInt1, paramInt2));
    }

    boolean checkCallingPermission(String paramString1, String paramString2)
    {
        boolean bool = true;
        if (Binder.getCallingPid() == Process.myPid());
        while (true)
        {
            return bool;
            if (this.mContext.checkCallingPermission(paramString1) != 0)
            {
                Slog.w("WindowManager", "Permission Denial: " + paramString2 + " from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid() + " requires " + paramString1);
                bool = false;
            }
        }
    }

    void checkDrawnWindowsLocked()
    {
        int i;
        if (this.mWaitingForDrawn.size() > 0)
            i = -1 + this.mWaitingForDrawn.size();
        while (true)
        {
            Pair localPair;
            WindowState localWindowState;
            if (i >= 0)
            {
                localPair = (Pair)this.mWaitingForDrawn.get(i);
                localWindowState = (WindowState)localPair.first;
                if ((localWindowState.mRemoved) || (!localWindowState.isVisibleLw()))
                    Slog.w("WindowManager", "Aborted waiting for drawn: " + localPair.first);
            }
            try
            {
                ((IRemoteCallback)localPair.second).sendResult(null);
                label101: this.mWaitingForDrawn.remove(localPair);
                this.mH.removeMessages(24, localPair);
                while (true)
                {
                    i--;
                    break;
                    if (!localWindowState.mWinAnimator.mSurfaceShown)
                        continue;
                    try
                    {
                        ((IRemoteCallback)localPair.second).sendResult(null);
                        label149: this.mWaitingForDrawn.remove(localPair);
                        this.mH.removeMessages(24, localPair);
                        continue;
                        return;
                    }
                    catch (RemoteException localRemoteException2)
                    {
                        break label149;
                    }
                }
            }
            catch (RemoteException localRemoteException1)
            {
                break label101;
            }
        }
    }

    public void clearForcedDisplaySize()
    {
        synchronized (this.mWindowMap)
        {
            setForcedDisplaySizeLocked(this.mInitialDisplayWidth, this.mInitialDisplayHeight);
            Settings.Secure.putString(this.mContext.getContentResolver(), "display_size_forced", "");
            return;
        }
    }

    public void closeSystemDialogs(String paramString)
    {
        while (true)
        {
            int i;
            WindowState localWindowState;
            synchronized (this.mWindowMap)
            {
                i = -1 + this.mWindows.size();
                if (i >= 0)
                {
                    localWindowState = (WindowState)this.mWindows.get(i);
                    boolean bool = localWindowState.mHasSurface;
                    if (!bool);
                }
            }
            try
            {
                localWindowState.mClient.closeSystemDialogs(paramString);
                label61: i--;
                continue;
                return;
                localObject = finally;
                throw localObject;
            }
            catch (RemoteException localRemoteException)
            {
                break label61;
            }
        }
    }

    int computeForcedAppOrientationLocked()
    {
        int i = getOrientationFromWindowsLocked();
        if (i == -1)
            i = getOrientationFromAppTokensLocked();
        return i;
    }

    public Configuration computeNewConfiguration()
    {
        synchronized (this.mWindowMap)
        {
            Configuration localConfiguration = computeNewConfigurationLocked();
            if ((localConfiguration == null) && (this.mWaitingForConfig))
            {
                this.mWaitingForConfig = false;
                performLayoutAndPlaceSurfacesLocked();
            }
            return localConfiguration;
        }
    }

    Configuration computeNewConfigurationLocked()
    {
        Configuration localConfiguration = new Configuration();
        localConfiguration.fontScale = 0.0F;
        if (!computeScreenConfigurationLocked(localConfiguration))
            localConfiguration = null;
        return localConfiguration;
    }

    boolean computeScreenConfigurationLocked(Configuration paramConfiguration)
    {
        boolean bool2;
        if (this.mDisplay == null)
        {
            bool2 = false;
            return bool2;
        }
        boolean bool1;
        int i;
        label40: int j;
        label50: label105: int k;
        int m;
        int i8;
        label137: DisplayMetrics localDisplayMetrics;
        int n;
        int i1;
        if ((this.mRotation == 1) || (this.mRotation == 3))
        {
            bool1 = true;
            if (!bool1)
                break label563;
            i = this.mBaseDisplayHeight;
            if (!bool1)
                break label571;
            j = this.mBaseDisplayWidth;
            synchronized (this.mDisplaySizeLock)
            {
                if (this.mAltOrientation)
                {
                    this.mCurDisplayWidth = i;
                    this.mCurDisplayHeight = j;
                    if (i > j)
                    {
                        int i10 = (int)(j / 1.3F);
                        if (i10 < i)
                            this.mCurDisplayWidth = i10;
                        k = this.mCurDisplayWidth;
                        m = this.mCurDisplayHeight;
                        if (paramConfiguration != null)
                        {
                            i8 = 3;
                            if (k >= m)
                                break label627;
                            i8 = 1;
                            paramConfiguration.orientation = i8;
                        }
                        this.mDisplay.getMetricsWithSize(this.mRealDisplayMetrics, this.mCurDisplayWidth, this.mCurDisplayHeight);
                        localDisplayMetrics = this.mDisplayMetrics;
                        n = this.mPolicy.getNonDecorDisplayWidth(k, m, this.mRotation);
                        i1 = this.mPolicy.getNonDecorDisplayHeight(k, m, this.mRotation);
                    }
                }
            }
        }
        int i2;
        int i3;
        while (true)
        {
            int i6;
            synchronized (this.mDisplaySizeLock)
            {
                this.mAppDisplayWidth = n;
                this.mAppDisplayHeight = i1;
                this.mAnimator.setDisplayDimensions(this.mCurDisplayWidth, this.mCurDisplayHeight, this.mAppDisplayWidth, this.mAppDisplayHeight);
                this.mDisplay.getMetricsWithSize(localDisplayMetrics, this.mAppDisplayWidth, this.mAppDisplayHeight);
                this.mCompatibleScreenScale = CompatibilityInfo.computeCompatibleScaling(localDisplayMetrics, this.mCompatDisplayMetrics);
                if (paramConfiguration == null)
                    break label789;
                paramConfiguration.screenWidthDp = ((int)(this.mPolicy.getConfigDisplayWidth(k, m, this.mRotation) / localDisplayMetrics.density));
                paramConfiguration.screenHeightDp = ((int)(this.mPolicy.getConfigDisplayHeight(k, m, this.mRotation) / localDisplayMetrics.density));
                computeSizeRangesAndScreenLayout(bool1, k, m, localDisplayMetrics.density, paramConfiguration);
                paramConfiguration.compatScreenWidthDp = ((int)(paramConfiguration.screenWidthDp / this.mCompatibleScreenScale));
                paramConfiguration.compatScreenHeightDp = ((int)(paramConfiguration.screenHeightDp / this.mCompatibleScreenScale));
                paramConfiguration.compatSmallestScreenWidthDp = computeCompatSmallestWidth(bool1, localDisplayMetrics, k, m);
                paramConfiguration.touchscreen = 1;
                paramConfiguration.keyboard = 1;
                paramConfiguration.navigation = 1;
                i2 = 0;
                i3 = 0;
                InputDevice[] arrayOfInputDevice = this.mInputManager.getInputDevices();
                int i4 = arrayOfInputDevice.length;
                int i5 = 0;
                if (i5 >= i4)
                    break label697;
                InputDevice localInputDevice = arrayOfInputDevice[i5];
                if (!localInputDevice.isVirtual())
                {
                    i6 = localInputDevice.getSources();
                    if (!localInputDevice.isExternal())
                        break label648;
                    i7 = 2;
                    if (!this.mIsTouchDevice)
                        break label654;
                    if ((i6 & 0x1002) == 4098)
                        paramConfiguration.touchscreen = 3;
                    if ((0x10004 & i6) != 65540)
                        break label662;
                    paramConfiguration.navigation = 3;
                    i3 |= i7;
                    if (localInputDevice.getKeyboardType() == 2)
                    {
                        paramConfiguration.keyboard = 2;
                        i2 |= i7;
                    }
                }
                i5++;
                continue;
                bool1 = false;
                break;
                label563: i = this.mBaseDisplayWidth;
                break label40;
                label571: j = this.mBaseDisplayHeight;
                break label50;
                int i9 = (int)(i / 1.3F);
                if (i9 >= j)
                    break label105;
                this.mCurDisplayHeight = i9;
                break label105;
                localObject2 = finally;
                throw localObject2;
                this.mCurDisplayWidth = i;
                this.mCurDisplayHeight = j;
                break label105;
                label627: if (k <= m)
                    break label137;
                i8 = 2;
            }
            label648: int i7 = 1;
            continue;
            label654: paramConfiguration.touchscreen = 1;
            continue;
            label662: if (((i6 & 0x201) == 513) && (paramConfiguration.navigation == 1))
            {
                paramConfiguration.navigation = 2;
                i3 |= i7;
            }
        }
        label697: if (paramConfiguration.keyboard != 1);
        for (boolean bool3 = true; ; bool3 = false)
        {
            if (bool3 != this.mHardKeyboardAvailable)
            {
                this.mHardKeyboardAvailable = bool3;
                this.mHardKeyboardEnabled = bool3;
                this.mH.removeMessages(22);
                this.mH.sendEmptyMessage(22);
            }
            if (!this.mHardKeyboardEnabled)
                paramConfiguration.keyboard = 1;
            paramConfiguration.keyboardHidden = 1;
            paramConfiguration.hardKeyboardHidden = 1;
            paramConfiguration.navigationHidden = 1;
            this.mPolicy.adjustConfigurationLw(paramConfiguration, i2, i3);
            label789: bool2 = true;
            break;
        }
    }

    // ERROR //
    void createWatermark()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 1483	com/android/server/wm/WindowManagerService:mWatermark	Lcom/android/server/wm/Watermark;
        //     4: ifnull +4 -> 8
        //     7: return
        //     8: new 2451	java/io/File
        //     11: dup
        //     12: ldc_w 2453
        //     15: invokespecial 2454	java/io/File:<init>	(Ljava/lang/String;)V
        //     18: astore_1
        //     19: aconst_null
        //     20: astore_2
        //     21: new 2456	java/io/FileInputStream
        //     24: dup
        //     25: aload_1
        //     26: invokespecial 2459	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //     29: astore_3
        //     30: new 2461	java/io/DataInputStream
        //     33: dup
        //     34: aload_3
        //     35: invokespecial 2464	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
        //     38: invokevirtual 2467	java/io/DataInputStream:readLine	()Ljava/lang/String;
        //     41: astore 10
        //     43: aload 10
        //     45: ifnull +45 -> 90
        //     48: aload 10
        //     50: ldc_w 2469
        //     53: invokevirtual 2473	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
        //     56: astore 12
        //     58: aload 12
        //     60: ifnull +30 -> 90
        //     63: aload 12
        //     65: arraylength
        //     66: ifle +24 -> 90
        //     69: aload_0
        //     70: new 1485	com/android/server/wm/Watermark
        //     73: dup
        //     74: aload_0
        //     75: getfield 519	com/android/server/wm/WindowManagerService:mRealDisplayMetrics	Landroid/util/DisplayMetrics;
        //     78: aload_0
        //     79: getfield 756	com/android/server/wm/WindowManagerService:mFxSession	Landroid/view/SurfaceSession;
        //     82: aload 12
        //     84: invokespecial 2476	com/android/server/wm/Watermark:<init>	(Landroid/util/DisplayMetrics;Landroid/view/SurfaceSession;[Ljava/lang/String;)V
        //     87: putfield 1483	com/android/server/wm/WindowManagerService:mWatermark	Lcom/android/server/wm/Watermark;
        //     90: aload_3
        //     91: ifnull +90 -> 181
        //     94: aload_3
        //     95: invokevirtual 2479	java/io/FileInputStream:close	()V
        //     98: goto -91 -> 7
        //     101: astore 11
        //     103: goto -96 -> 7
        //     106: astore 14
        //     108: aload_2
        //     109: ifnull -102 -> 7
        //     112: aload_2
        //     113: invokevirtual 2479	java/io/FileInputStream:close	()V
        //     116: goto -109 -> 7
        //     119: astore 5
        //     121: goto -114 -> 7
        //     124: astore 13
        //     126: aload_2
        //     127: ifnull -120 -> 7
        //     130: aload_2
        //     131: invokevirtual 2479	java/io/FileInputStream:close	()V
        //     134: goto -127 -> 7
        //     137: astore 7
        //     139: goto -132 -> 7
        //     142: astore 8
        //     144: aload_2
        //     145: ifnull +7 -> 152
        //     148: aload_2
        //     149: invokevirtual 2479	java/io/FileInputStream:close	()V
        //     152: aload 8
        //     154: athrow
        //     155: astore 9
        //     157: goto -5 -> 152
        //     160: astore 8
        //     162: aload_3
        //     163: astore_2
        //     164: goto -20 -> 144
        //     167: astore 6
        //     169: aload_3
        //     170: astore_2
        //     171: goto -45 -> 126
        //     174: astore 4
        //     176: aload_3
        //     177: astore_2
        //     178: goto -70 -> 108
        //     181: goto -174 -> 7
        //
        // Exception table:
        //     from	to	target	type
        //     94	98	101	java/io/IOException
        //     21	30	106	java/io/FileNotFoundException
        //     112	116	119	java/io/IOException
        //     21	30	124	java/io/IOException
        //     130	134	137	java/io/IOException
        //     21	30	142	finally
        //     148	152	155	java/io/IOException
        //     30	90	160	finally
        //     30	90	167	java/io/IOException
        //     30	90	174	java/io/FileNotFoundException
    }

    void debugLayoutRepeats(String paramString, int paramInt)
    {
        if (this.mLayoutRepeatCount >= 4)
            Slog.v("WindowManager", "Layouts looping: " + paramString + ", mPendingLayoutChanges = 0x" + Integer.toHexString(paramInt));
    }

    public boolean detectSafeMode()
    {
        boolean bool = false;
        if (!this.mInputMonitor.waitForInputDevicesReady(1000L))
            Slog.w("WindowManager", "Devices still not ready after waiting 1000 milliseconds before attempting to detect safe mode.");
        int i = this.mInputManager.getKeyCodeState(-1, -256, 82);
        int j = this.mInputManager.getKeyCodeState(-1, -256, 47);
        int k = this.mInputManager.getKeyCodeState(-1, 513, 23);
        int m = this.mInputManager.getScanCodeState(-1, 65540, 272);
        int n = this.mInputManager.getKeyCodeState(-1, -256, 25);
        if ((i > 0) || (j > 0) || (k > 0) || (m > 0) || (n > 0))
            bool = true;
        this.mSafeMode = bool;
        try
        {
            if (SystemProperties.getInt("persist.sys.safemode", 0) != 0)
            {
                this.mSafeMode = true;
                SystemProperties.set("persist.sys.safemode", "");
            }
            label157: if (this.mSafeMode)
                Log.i("WindowManager", "SAFE MODE ENABLED (menu=" + i + " s=" + j + " dpad=" + k + " trackball=" + m + ")");
            while (true)
            {
                this.mPolicy.setSafeMode(this.mSafeMode);
                return this.mSafeMode;
                Log.i("WindowManager", "SAFE MODE not enabled");
            }
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            break label157;
        }
    }

    public void disableKeyguard(IBinder paramIBinder, String paramString)
    {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.DISABLE_KEYGUARD") != 0)
            throw new SecurityException("Requires DISABLE_KEYGUARD permission");
        synchronized (this.mKeyguardTokenWatcher)
        {
            this.mKeyguardTokenWatcher.acquire(paramIBinder, paramString);
            return;
        }
    }

    public void dismissKeyguard()
    {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.DISABLE_KEYGUARD") != 0)
            throw new SecurityException("Requires DISABLE_KEYGUARD permission");
        synchronized (this.mWindowMap)
        {
            this.mPolicy.dismissKeyguardLw();
            return;
        }
    }

    void dispatchWallpaperVisibility(WindowState paramWindowState, boolean paramBoolean)
    {
        if (paramWindowState.mWallpaperVisible != paramBoolean)
            paramWindowState.mWallpaperVisible = paramBoolean;
        try
        {
            paramWindowState.mClient.dispatchAppVisibility(paramBoolean);
            label23: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label23;
        }
    }

    // ERROR //
    public void displayReady()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 378	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
        //     4: astore_1
        //     5: aload_1
        //     6: monitorenter
        //     7: aload_0
        //     8: getfield 1406	com/android/server/wm/WindowManagerService:mDisplay	Landroid/view/Display;
        //     11: ifnull +19 -> 30
        //     14: new 2138	java/lang/IllegalStateException
        //     17: dup
        //     18: ldc_w 2556
        //     21: invokespecial 2141	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     24: athrow
        //     25: astore_2
        //     26: aload_1
        //     27: monitorexit
        //     28: aload_2
        //     29: athrow
        //     30: aload_0
        //     31: aload_0
        //     32: getfield 596	com/android/server/wm/WindowManagerService:mContext	Landroid/content/Context;
        //     35: ldc_w 2558
        //     38: invokevirtual 649	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
        //     41: checkcast 2560	android/view/WindowManager
        //     44: invokeinterface 2564 1 0
        //     49: putfield 1406	com/android/server/wm/WindowManagerService:mDisplay	Landroid/view/Display;
        //     52: aload_0
        //     53: aload_0
        //     54: getfield 596	com/android/server/wm/WindowManagerService:mContext	Landroid/content/Context;
        //     57: invokevirtual 2568	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
        //     60: ldc_w 2570
        //     63: invokevirtual 2576	android/content/pm/PackageManager:hasSystemFeature	(Ljava/lang/String;)Z
        //     66: putfield 2424	com/android/server/wm/WindowManagerService:mIsTouchDevice	Z
        //     69: aload_0
        //     70: getfield 430	com/android/server/wm/WindowManagerService:mDisplaySizeLock	Ljava/lang/Object;
        //     73: astore_3
        //     74: aload_3
        //     75: monitorenter
        //     76: aload_0
        //     77: aload_0
        //     78: getfield 1406	com/android/server/wm/WindowManagerService:mDisplay	Landroid/view/Display;
        //     81: invokevirtual 2579	android/view/Display:getRawWidth	()I
        //     84: putfield 432	com/android/server/wm/WindowManagerService:mInitialDisplayWidth	I
        //     87: aload_0
        //     88: aload_0
        //     89: getfield 1406	com/android/server/wm/WindowManagerService:mDisplay	Landroid/view/Display;
        //     92: invokevirtual 2582	android/view/Display:getRawHeight	()I
        //     95: putfield 434	com/android/server/wm/WindowManagerService:mInitialDisplayHeight	I
        //     98: aload_0
        //     99: getfield 1406	com/android/server/wm/WindowManagerService:mDisplay	Landroid/view/Display;
        //     102: invokevirtual 1995	android/view/Display:getRotation	()I
        //     105: istore 5
        //     107: iload 5
        //     109: iconst_1
        //     110: if_icmpeq +9 -> 119
        //     113: iload 5
        //     115: iconst_3
        //     116: if_icmpne +23 -> 139
        //     119: aload_0
        //     120: getfield 432	com/android/server/wm/WindowManagerService:mInitialDisplayWidth	I
        //     123: istore 6
        //     125: aload_0
        //     126: aload_0
        //     127: getfield 434	com/android/server/wm/WindowManagerService:mInitialDisplayHeight	I
        //     130: putfield 432	com/android/server/wm/WindowManagerService:mInitialDisplayWidth	I
        //     133: aload_0
        //     134: iload 6
        //     136: putfield 434	com/android/server/wm/WindowManagerService:mInitialDisplayHeight	I
        //     139: aload_0
        //     140: getfield 432	com/android/server/wm/WindowManagerService:mInitialDisplayWidth	I
        //     143: istore 7
        //     145: aload_0
        //     146: iload 7
        //     148: putfield 444	com/android/server/wm/WindowManagerService:mAppDisplayWidth	I
        //     151: aload_0
        //     152: iload 7
        //     154: putfield 440	com/android/server/wm/WindowManagerService:mCurDisplayWidth	I
        //     157: aload_0
        //     158: iload 7
        //     160: putfield 436	com/android/server/wm/WindowManagerService:mBaseDisplayWidth	I
        //     163: aload_0
        //     164: getfield 434	com/android/server/wm/WindowManagerService:mInitialDisplayHeight	I
        //     167: istore 8
        //     169: aload_0
        //     170: iload 8
        //     172: putfield 446	com/android/server/wm/WindowManagerService:mAppDisplayHeight	I
        //     175: aload_0
        //     176: iload 8
        //     178: putfield 442	com/android/server/wm/WindowManagerService:mCurDisplayHeight	I
        //     181: aload_0
        //     182: iload 8
        //     184: putfield 438	com/android/server/wm/WindowManagerService:mBaseDisplayHeight	I
        //     187: aload_0
        //     188: getfield 729	com/android/server/wm/WindowManagerService:mAnimator	Lcom/android/server/wm/WindowAnimator;
        //     191: aload_0
        //     192: getfield 440	com/android/server/wm/WindowManagerService:mCurDisplayWidth	I
        //     195: aload_0
        //     196: getfield 442	com/android/server/wm/WindowManagerService:mCurDisplayHeight	I
        //     199: aload_0
        //     200: getfield 444	com/android/server/wm/WindowManagerService:mAppDisplayWidth	I
        //     203: aload_0
        //     204: getfield 446	com/android/server/wm/WindowManagerService:mAppDisplayHeight	I
        //     207: invokevirtual 2377	com/android/server/wm/WindowAnimator:setDisplayDimensions	(IIII)V
        //     210: aload_3
        //     211: monitorexit
        //     212: aload_0
        //     213: getfield 722	com/android/server/wm/WindowManagerService:mInputManager	Lcom/android/server/input/InputManagerService;
        //     216: iconst_0
        //     217: aload_0
        //     218: getfield 1406	com/android/server/wm/WindowManagerService:mDisplay	Landroid/view/Display;
        //     221: invokevirtual 2579	android/view/Display:getRawWidth	()I
        //     224: aload_0
        //     225: getfield 1406	com/android/server/wm/WindowManagerService:mDisplay	Landroid/view/Display;
        //     228: invokevirtual 2582	android/view/Display:getRawHeight	()I
        //     231: aload_0
        //     232: getfield 1406	com/android/server/wm/WindowManagerService:mDisplay	Landroid/view/Display;
        //     235: invokevirtual 2585	android/view/Display:getRawExternalWidth	()I
        //     238: aload_0
        //     239: getfield 1406	com/android/server/wm/WindowManagerService:mDisplay	Landroid/view/Display;
        //     242: invokevirtual 2588	android/view/Display:getRawExternalHeight	()I
        //     245: invokevirtual 2592	com/android/server/input/InputManagerService:setDisplaySize	(IIIII)V
        //     248: aload_0
        //     249: getfield 722	com/android/server/wm/WindowManagerService:mInputManager	Lcom/android/server/input/InputManagerService;
        //     252: iconst_0
        //     253: aload_0
        //     254: getfield 1406	com/android/server/wm/WindowManagerService:mDisplay	Landroid/view/Display;
        //     257: invokevirtual 1995	android/view/Display:getRotation	()I
        //     260: aload_0
        //     261: getfield 1406	com/android/server/wm/WindowManagerService:mDisplay	Landroid/view/Display;
        //     264: invokevirtual 2595	android/view/Display:getExternalRotation	()I
        //     267: invokevirtual 2598	com/android/server/input/InputManagerService:setDisplayOrientation	(III)V
        //     270: aload_0
        //     271: getfield 368	com/android/server/wm/WindowManagerService:mPolicy	Landroid/view/WindowManagerPolicy;
        //     274: aload_0
        //     275: getfield 1406	com/android/server/wm/WindowManagerService:mDisplay	Landroid/view/Display;
        //     278: aload_0
        //     279: getfield 432	com/android/server/wm/WindowManagerService:mInitialDisplayWidth	I
        //     282: aload_0
        //     283: getfield 434	com/android/server/wm/WindowManagerService:mInitialDisplayHeight	I
        //     286: invokeinterface 1935 4 0
        //     291: aload_1
        //     292: monitorexit
        //     293: aload_0
        //     294: getfield 673	com/android/server/wm/WindowManagerService:mActivityManager	Landroid/app/IActivityManager;
        //     297: aconst_null
        //     298: invokeinterface 2603 2 0
        //     303: aload_0
        //     304: getfield 378	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
        //     307: astore 10
        //     309: aload 10
        //     311: monitorenter
        //     312: aload_0
        //     313: invokespecial 2605	com/android/server/wm/WindowManagerService:readForcedDisplaySizeLocked	()V
        //     316: aload 10
        //     318: monitorexit
        //     319: return
        //     320: astore 4
        //     322: aload_3
        //     323: monitorexit
        //     324: aload 4
        //     326: athrow
        //     327: astore 11
        //     329: aload 10
        //     331: monitorexit
        //     332: aload 11
        //     334: athrow
        //     335: astore 9
        //     337: goto -34 -> 303
        //
        // Exception table:
        //     from	to	target	type
        //     7	28	25	finally
        //     30	76	25	finally
        //     212	293	25	finally
        //     324	327	25	finally
        //     76	212	320	finally
        //     322	324	320	finally
        //     312	319	327	finally
        //     329	332	327	finally
        //     293	303	335	android/os/RemoteException
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.DUMP") != 0)
            paramPrintWriter.println("Permission Denial: can't dump WindowManager from from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid());
        while (true)
        {
            return;
            boolean bool = false;
            int i = 0;
            String str1;
            int j;
            while (true)
            {
                String str2;
                while (true)
                    if (i < paramArrayOfString.length)
                    {
                        str2 = paramArrayOfString[i];
                        if ((str2 != null) && (str2.length() > 0) && (str2.charAt(0) == '-'))
                            break;
                    }
                    else
                    {
                        if (i >= paramArrayOfString.length)
                            break label638;
                        str1 = paramArrayOfString[i];
                        j = i + 1;
                        if ((!"lastanr".equals(str1)) && (!"l".equals(str1)))
                            break label330;
                        synchronized (this.mWindowMap)
                        {
                            dumpLastANRLocked(paramPrintWriter);
                        }
                    }
                i++;
                if ("-a".equals(str2))
                {
                    bool = true;
                }
                else
                {
                    if ("-h".equals(str2))
                    {
                        paramPrintWriter.println("Window manager dump options:");
                        paramPrintWriter.println("    [-a] [-h] [cmd] ...");
                        paramPrintWriter.println("    cmd may be one of:");
                        paramPrintWriter.println("        l[astanr]: last ANR information");
                        paramPrintWriter.println("        p[policy]: policy state");
                        paramPrintWriter.println("        s[essions]: active sessions");
                        paramPrintWriter.println("        t[okens]: token list");
                        paramPrintWriter.println("        w[indows]: window list");
                        paramPrintWriter.println("    cmd may also be a NAME to dump windows.    NAME may");
                        paramPrintWriter.println("        be a partial substring in a window name, a");
                        paramPrintWriter.println("        Window hex object identifier, or");
                        paramPrintWriter.println("        \"all\" for all windows, or");
                        paramPrintWriter.println("        \"visible\" for the visible windows.");
                        paramPrintWriter.println("    -a: include all available server state.");
                        break;
                    }
                    paramPrintWriter.println("Unknown argument: " + str2 + "; use -h for help");
                }
            }
            label330: if (("policy".equals(str1)) || ("p".equals(str1)))
                synchronized (this.mWindowMap)
                {
                    dumpPolicyLocked(paramPrintWriter, paramArrayOfString, true);
                }
            if (("sessions".equals(str1)) || ("s".equals(str1)))
                synchronized (this.mWindowMap)
                {
                    dumpSessionsLocked(paramPrintWriter, true);
                }
            if (("tokens".equals(str1)) || ("t".equals(str1)))
                synchronized (this.mWindowMap)
                {
                    dumpTokensLocked(paramPrintWriter, true);
                }
            if (("windows".equals(str1)) || ("w".equals(str1)))
                synchronized (this.mWindowMap)
                {
                    dumpWindowsLocked(paramPrintWriter, true, null);
                }
            if (("all".equals(str1)) || ("a".equals(str1)))
                synchronized (this.mWindowMap)
                {
                    dumpWindowsLocked(paramPrintWriter, true, null);
                }
            if (dumpWindows(paramPrintWriter, str1, paramArrayOfString, j, bool))
                continue;
            paramPrintWriter.println("Bad window command, or no windows match: " + str1);
            paramPrintWriter.println("Use -h for help.");
            continue;
            label638: synchronized (this.mWindowMap)
            {
                paramPrintWriter.println();
                if (bool)
                    paramPrintWriter.println("-------------------------------------------------------------------------------");
                dumpLastANRLocked(paramPrintWriter);
                paramPrintWriter.println();
                if (bool)
                    paramPrintWriter.println("-------------------------------------------------------------------------------");
                dumpPolicyLocked(paramPrintWriter, paramArrayOfString, bool);
                paramPrintWriter.println();
                if (bool)
                    paramPrintWriter.println("-------------------------------------------------------------------------------");
                dumpSessionsLocked(paramPrintWriter, bool);
                paramPrintWriter.println();
                if (bool)
                    paramPrintWriter.println("-------------------------------------------------------------------------------");
                dumpTokensLocked(paramPrintWriter, bool);
                paramPrintWriter.println();
                if (bool)
                    paramPrintWriter.println("-------------------------------------------------------------------------------");
                dumpWindowsLocked(paramPrintWriter, bool, null);
            }
        }
    }

    void dumpAnimatingAppTokensLocked()
    {
        for (int i = -1 + this.mAnimatingAppTokens.size(); i >= 0; i--)
            Slog.v("WindowManager", "    #" + i + ": " + ((AppWindowToken)this.mAnimatingAppTokens.get(i)).token);
    }

    void dumpAppTokensLocked()
    {
        for (int i = -1 + this.mAppTokens.size(); i >= 0; i--)
            Slog.v("WindowManager", "    #" + i + ": " + ((AppWindowToken)this.mAppTokens.get(i)).token);
    }

    void dumpLastANRLocked(PrintWriter paramPrintWriter)
    {
        paramPrintWriter.println("WINDOW MANAGER LAST ANR (dumpsys window lastanr)");
        if (this.mLastANRState == null)
            paramPrintWriter.println("    <no ANR has occurred since boot>");
        while (true)
        {
            return;
            paramPrintWriter.println(this.mLastANRState);
        }
    }

    void dumpPolicyLocked(PrintWriter paramPrintWriter, String[] paramArrayOfString, boolean paramBoolean)
    {
        paramPrintWriter.println("WINDOW MANAGER POLICY STATE (dumpsys window policy)");
        this.mPolicy.dump("        ", paramPrintWriter, paramArrayOfString);
    }

    void dumpSessionsLocked(PrintWriter paramPrintWriter, boolean paramBoolean)
    {
        paramPrintWriter.println("WINDOW MANAGER SESSIONS (dumpsys window sessions)");
        if (this.mSessions.size() > 0)
        {
            Iterator localIterator = this.mSessions.iterator();
            while (localIterator.hasNext())
            {
                Session localSession = (Session)localIterator.next();
                paramPrintWriter.print("    Session ");
                paramPrintWriter.print(localSession);
                paramPrintWriter.println(':');
                localSession.dump(paramPrintWriter, "        ");
            }
        }
    }

    void dumpTokensLocked(PrintWriter paramPrintWriter, boolean paramBoolean)
    {
        paramPrintWriter.println("WINDOW MANAGER TOKENS (dumpsys window tokens)");
        if (this.mTokenMap.size() > 0)
        {
            paramPrintWriter.println("    All tokens:");
            Iterator localIterator = this.mTokenMap.values().iterator();
            while (localIterator.hasNext())
            {
                WindowToken localWindowToken6 = (WindowToken)localIterator.next();
                paramPrintWriter.print("    Token ");
                paramPrintWriter.print(localWindowToken6.token);
                if (paramBoolean)
                {
                    paramPrintWriter.println(':');
                    localWindowToken6.dump(paramPrintWriter, "        ");
                }
                else
                {
                    paramPrintWriter.println();
                }
            }
        }
        if (this.mWallpaperTokens.size() > 0)
        {
            paramPrintWriter.println();
            paramPrintWriter.println("    Wallpaper tokens:");
            int i1 = -1 + this.mWallpaperTokens.size();
            if (i1 >= 0)
            {
                WindowToken localWindowToken5 = (WindowToken)this.mWallpaperTokens.get(i1);
                paramPrintWriter.print("    Wallpaper #");
                paramPrintWriter.print(i1);
                paramPrintWriter.print(' ');
                paramPrintWriter.print(localWindowToken5);
                if (paramBoolean)
                {
                    paramPrintWriter.println(':');
                    localWindowToken5.dump(paramPrintWriter, "        ");
                }
                while (true)
                {
                    i1--;
                    break;
                    paramPrintWriter.println();
                }
            }
        }
        if (this.mAppTokens.size() > 0)
        {
            paramPrintWriter.println();
            paramPrintWriter.println("    Application tokens in Z order:");
            for (int n = -1 + this.mAppTokens.size(); n >= 0; n--)
            {
                paramPrintWriter.print("    App #");
                paramPrintWriter.print(n);
                paramPrintWriter.println(": ");
                ((AppWindowToken)this.mAppTokens.get(n)).dump(paramPrintWriter, "        ");
            }
        }
        if (this.mFinishedStarting.size() > 0)
        {
            paramPrintWriter.println();
            paramPrintWriter.println("    Finishing start of application tokens:");
            int m = -1 + this.mFinishedStarting.size();
            if (m >= 0)
            {
                WindowToken localWindowToken4 = (WindowToken)this.mFinishedStarting.get(m);
                paramPrintWriter.print("    Finished Starting #");
                paramPrintWriter.print(m);
                paramPrintWriter.print(' ');
                paramPrintWriter.print(localWindowToken4);
                if (paramBoolean)
                {
                    paramPrintWriter.println(':');
                    localWindowToken4.dump(paramPrintWriter, "        ");
                }
                while (true)
                {
                    m--;
                    break;
                    paramPrintWriter.println();
                }
            }
        }
        if (this.mExitingTokens.size() > 0)
        {
            paramPrintWriter.println();
            paramPrintWriter.println("    Exiting tokens:");
            int k = -1 + this.mExitingTokens.size();
            if (k >= 0)
            {
                WindowToken localWindowToken3 = (WindowToken)this.mExitingTokens.get(k);
                paramPrintWriter.print("    Exiting #");
                paramPrintWriter.print(k);
                paramPrintWriter.print(' ');
                paramPrintWriter.print(localWindowToken3);
                if (paramBoolean)
                {
                    paramPrintWriter.println(':');
                    localWindowToken3.dump(paramPrintWriter, "        ");
                }
                while (true)
                {
                    k--;
                    break;
                    paramPrintWriter.println();
                }
            }
        }
        if (this.mExitingAppTokens.size() > 0)
        {
            paramPrintWriter.println();
            paramPrintWriter.println("    Exiting application tokens:");
            int j = -1 + this.mExitingAppTokens.size();
            if (j >= 0)
            {
                WindowToken localWindowToken2 = (WindowToken)this.mExitingAppTokens.get(j);
                paramPrintWriter.print("    Exiting App #");
                paramPrintWriter.print(j);
                paramPrintWriter.print(' ');
                paramPrintWriter.print(localWindowToken2);
                if (paramBoolean)
                {
                    paramPrintWriter.println(':');
                    localWindowToken2.dump(paramPrintWriter, "        ");
                }
                while (true)
                {
                    j--;
                    break;
                    paramPrintWriter.println();
                }
            }
        }
        if ((this.mAppTransitionRunning) && (this.mAnimatingAppTokens.size() > 0))
        {
            paramPrintWriter.println();
            paramPrintWriter.println("    Application tokens during animation:");
            int i = -1 + this.mAnimatingAppTokens.size();
            if (i >= 0)
            {
                WindowToken localWindowToken1 = (WindowToken)this.mAnimatingAppTokens.get(i);
                paramPrintWriter.print("    App moving to bottom #");
                paramPrintWriter.print(i);
                paramPrintWriter.print(' ');
                paramPrintWriter.print(localWindowToken1);
                if (paramBoolean)
                {
                    paramPrintWriter.println(':');
                    localWindowToken1.dump(paramPrintWriter, "        ");
                }
                while (true)
                {
                    i--;
                    break;
                    paramPrintWriter.println();
                }
            }
        }
        if ((this.mOpeningApps.size() > 0) || (this.mClosingApps.size() > 0))
        {
            paramPrintWriter.println();
            if (this.mOpeningApps.size() > 0)
            {
                paramPrintWriter.print("    mOpeningApps=");
                paramPrintWriter.println(this.mOpeningApps);
            }
            if (this.mClosingApps.size() > 0)
            {
                paramPrintWriter.print("    mClosingApps=");
                paramPrintWriter.println(this.mClosingApps);
            }
        }
    }

    // ERROR //
    boolean dumpWindows(PrintWriter paramPrintWriter, String paramString, String[] paramArrayOfString, int paramInt, boolean paramBoolean)
    {
        // Byte code:
        //     0: new 382	java/util/ArrayList
        //     3: dup
        //     4: invokespecial 383	java/util/ArrayList:<init>	()V
        //     7: astore 6
        //     9: ldc_w 2808
        //     12: aload_2
        //     13: invokevirtual 633	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     16: ifeq +90 -> 106
        //     19: aload_0
        //     20: getfield 378	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
        //     23: astore 19
        //     25: aload 19
        //     27: monitorenter
        //     28: bipush 255
        //     30: aload_0
        //     31: getfield 395	com/android/server/wm/WindowManagerService:mWindows	Ljava/util/ArrayList;
        //     34: invokevirtual 806	java/util/ArrayList:size	()I
        //     37: iadd
        //     38: istore 21
        //     40: iload 21
        //     42: iflt +39 -> 81
        //     45: aload_0
        //     46: getfield 395	com/android/server/wm/WindowManagerService:mWindows	Ljava/util/ArrayList;
        //     49: iload 21
        //     51: invokevirtual 813	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     54: checkcast 403	com/android/server/wm/WindowState
        //     57: astore 22
        //     59: aload 22
        //     61: getfield 959	com/android/server/wm/WindowState:mWinAnimator	Lcom/android/server/wm/WindowStateAnimator;
        //     64: getfield 2348	com/android/server/wm/WindowStateAnimator:mSurfaceShown	Z
        //     67: ifeq +204 -> 271
        //     70: aload 6
        //     72: aload 22
        //     74: invokevirtual 861	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     77: pop
        //     78: goto +193 -> 271
        //     81: aload 19
        //     83: monitorexit
        //     84: aload 6
        //     86: invokevirtual 806	java/util/ArrayList:size	()I
        //     89: ifgt +142 -> 231
        //     92: iconst_0
        //     93: istore 14
        //     95: iload 14
        //     97: ireturn
        //     98: astore 20
        //     100: aload 19
        //     102: monitorexit
        //     103: aload 20
        //     105: athrow
        //     106: iconst_0
        //     107: istore 7
        //     109: aload_2
        //     110: bipush 16
        //     112: invokestatic 2810	java/lang/Integer:parseInt	(Ljava/lang/String;I)I
        //     115: istore 18
        //     117: iload 18
        //     119: istore 7
        //     121: aconst_null
        //     122: astore_2
        //     123: aload_0
        //     124: getfield 378	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
        //     127: astore 9
        //     129: aload 9
        //     131: monitorenter
        //     132: bipush 255
        //     134: aload_0
        //     135: getfield 395	com/android/server/wm/WindowManagerService:mWindows	Ljava/util/ArrayList;
        //     138: invokevirtual 806	java/util/ArrayList:size	()I
        //     141: iadd
        //     142: istore 11
        //     144: iload 11
        //     146: iflt +79 -> 225
        //     149: aload_0
        //     150: getfield 395	com/android/server/wm/WindowManagerService:mWindows	Ljava/util/ArrayList;
        //     153: iload 11
        //     155: invokevirtual 813	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     158: checkcast 403	com/android/server/wm/WindowState
        //     161: astore 15
        //     163: aload_2
        //     164: ifnull +32 -> 196
        //     167: aload 15
        //     169: getfield 844	com/android/server/wm/WindowState:mAttrs	Landroid/view/WindowManager$LayoutParams;
        //     172: invokevirtual 2814	android/view/WindowManager$LayoutParams:getTitle	()Ljava/lang/CharSequence;
        //     175: invokevirtual 2815	java/lang/Object:toString	()Ljava/lang/String;
        //     178: aload_2
        //     179: invokevirtual 2818	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
        //     182: ifeq +95 -> 277
        //     185: aload 6
        //     187: aload 15
        //     189: invokevirtual 861	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     192: pop
        //     193: goto +84 -> 277
        //     196: aload 15
        //     198: invokestatic 1144	java/lang/System:identityHashCode	(Ljava/lang/Object;)I
        //     201: iload 7
        //     203: if_icmpne +74 -> 277
        //     206: aload 6
        //     208: aload 15
        //     210: invokevirtual 861	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     213: pop
        //     214: goto +63 -> 277
        //     217: astore 10
        //     219: aload 9
        //     221: monitorexit
        //     222: aload 10
        //     224: athrow
        //     225: aload 9
        //     227: monitorexit
        //     228: goto -144 -> 84
        //     231: aload_0
        //     232: getfield 378	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
        //     235: astore 12
        //     237: aload 12
        //     239: monitorenter
        //     240: aload_0
        //     241: aload_1
        //     242: iload 5
        //     244: aload 6
        //     246: invokevirtual 2693	com/android/server/wm/WindowManagerService:dumpWindowsLocked	(Ljava/io/PrintWriter;ZLjava/util/ArrayList;)V
        //     249: aload 12
        //     251: monitorexit
        //     252: iconst_1
        //     253: istore 14
        //     255: goto -160 -> 95
        //     258: astore 13
        //     260: aload 12
        //     262: monitorexit
        //     263: aload 13
        //     265: athrow
        //     266: astore 8
        //     268: goto -145 -> 123
        //     271: iinc 21 255
        //     274: goto -234 -> 40
        //     277: iinc 11 255
        //     280: goto -136 -> 144
        //
        // Exception table:
        //     from	to	target	type
        //     28	84	98	finally
        //     100	103	98	finally
        //     132	222	217	finally
        //     225	228	217	finally
        //     240	263	258	finally
        //     109	117	266	java/lang/RuntimeException
    }

    void dumpWindowsLocked()
    {
        for (int i = -1 + this.mWindows.size(); i >= 0; i--)
            Slog.v("WindowManager", "    #" + i + ": " + this.mWindows.get(i));
    }

    void dumpWindowsLocked(PrintWriter paramPrintWriter, boolean paramBoolean, ArrayList<WindowState> paramArrayList)
    {
        paramPrintWriter.println("WINDOW MANAGER WINDOWS (dumpsys window windows)");
        dumpWindowsNoHeaderLocked(paramPrintWriter, paramBoolean, paramArrayList);
    }

    void dumpWindowsNoHeaderLocked(PrintWriter paramPrintWriter, boolean paramBoolean, ArrayList<WindowState> paramArrayList)
    {
        int i = -1 + this.mWindows.size();
        if (i >= 0)
        {
            WindowState localWindowState7 = (WindowState)this.mWindows.get(i);
            if ((paramArrayList == null) || (paramArrayList.contains(localWindowState7)))
            {
                paramPrintWriter.print("    Window #");
                paramPrintWriter.print(i);
                paramPrintWriter.print(' ');
                paramPrintWriter.print(localWindowState7);
                paramPrintWriter.println(":");
                if ((!paramBoolean) && (paramArrayList == null))
                    break label104;
            }
            label104: for (boolean bool = true; ; bool = false)
            {
                localWindowState7.dump(paramPrintWriter, "        ", bool);
                i--;
                break;
            }
        }
        if (this.mInputMethodDialogs.size() > 0)
        {
            paramPrintWriter.println();
            paramPrintWriter.println("    Input method dialogs:");
            for (int i5 = -1 + this.mInputMethodDialogs.size(); i5 >= 0; i5--)
            {
                WindowState localWindowState6 = (WindowState)this.mInputMethodDialogs.get(i5);
                if ((paramArrayList == null) || (paramArrayList.contains(localWindowState6)))
                {
                    paramPrintWriter.print("    IM Dialog #");
                    paramPrintWriter.print(i5);
                    paramPrintWriter.print(": ");
                    paramPrintWriter.println(localWindowState6);
                }
            }
        }
        if (this.mPendingRemove.size() > 0)
        {
            paramPrintWriter.println();
            paramPrintWriter.println("    Remove pending for:");
            int i4 = -1 + this.mPendingRemove.size();
            if (i4 >= 0)
            {
                WindowState localWindowState5 = (WindowState)this.mPendingRemove.get(i4);
                if ((paramArrayList == null) || (paramArrayList.contains(localWindowState5)))
                {
                    paramPrintWriter.print("    Remove #");
                    paramPrintWriter.print(i4);
                    paramPrintWriter.print(' ');
                    paramPrintWriter.print(localWindowState5);
                    if (!paramBoolean)
                        break label324;
                    paramPrintWriter.println(":");
                    localWindowState5.dump(paramPrintWriter, "        ", true);
                }
                while (true)
                {
                    i4--;
                    break;
                    label324: paramPrintWriter.println();
                }
            }
        }
        if ((this.mForceRemoves != null) && (this.mForceRemoves.size() > 0))
        {
            paramPrintWriter.println();
            paramPrintWriter.println("    Windows force removing:");
            int i3 = -1 + this.mForceRemoves.size();
            if (i3 >= 0)
            {
                WindowState localWindowState4 = (WindowState)this.mForceRemoves.get(i3);
                paramPrintWriter.print("    Removing #");
                paramPrintWriter.print(i3);
                paramPrintWriter.print(' ');
                paramPrintWriter.print(localWindowState4);
                if (paramBoolean)
                {
                    paramPrintWriter.println(":");
                    localWindowState4.dump(paramPrintWriter, "        ", true);
                }
                while (true)
                {
                    i3--;
                    break;
                    paramPrintWriter.println();
                }
            }
        }
        if (this.mDestroySurface.size() > 0)
        {
            paramPrintWriter.println();
            paramPrintWriter.println("    Windows waiting to destroy their surface:");
            int i2 = -1 + this.mDestroySurface.size();
            if (i2 >= 0)
            {
                WindowState localWindowState3 = (WindowState)this.mDestroySurface.get(i2);
                if ((paramArrayList == null) || (paramArrayList.contains(localWindowState3)))
                {
                    paramPrintWriter.print("    Destroy #");
                    paramPrintWriter.print(i2);
                    paramPrintWriter.print(' ');
                    paramPrintWriter.print(localWindowState3);
                    if (!paramBoolean)
                        break label566;
                    paramPrintWriter.println(":");
                    localWindowState3.dump(paramPrintWriter, "        ", true);
                }
                while (true)
                {
                    i2--;
                    break;
                    label566: paramPrintWriter.println();
                }
            }
        }
        if (this.mLosingFocus.size() > 0)
        {
            paramPrintWriter.println();
            paramPrintWriter.println("    Windows losing focus:");
            int i1 = -1 + this.mLosingFocus.size();
            if (i1 >= 0)
            {
                WindowState localWindowState2 = (WindowState)this.mLosingFocus.get(i1);
                if ((paramArrayList == null) || (paramArrayList.contains(localWindowState2)))
                {
                    paramPrintWriter.print("    Losing #");
                    paramPrintWriter.print(i1);
                    paramPrintWriter.print(' ');
                    paramPrintWriter.print(localWindowState2);
                    if (!paramBoolean)
                        break label690;
                    paramPrintWriter.println(":");
                    localWindowState2.dump(paramPrintWriter, "        ", true);
                }
                while (true)
                {
                    i1--;
                    break;
                    label690: paramPrintWriter.println();
                }
            }
        }
        if (this.mResizingWindows.size() > 0)
        {
            paramPrintWriter.println();
            paramPrintWriter.println("    Windows waiting to resize:");
            int n = -1 + this.mResizingWindows.size();
            if (n >= 0)
            {
                WindowState localWindowState1 = (WindowState)this.mResizingWindows.get(n);
                if ((paramArrayList == null) || (paramArrayList.contains(localWindowState1)))
                {
                    paramPrintWriter.print("    Resizing #");
                    paramPrintWriter.print(n);
                    paramPrintWriter.print(' ');
                    paramPrintWriter.print(localWindowState1);
                    if (!paramBoolean)
                        break label814;
                    paramPrintWriter.println(":");
                    localWindowState1.dump(paramPrintWriter, "        ", true);
                }
                while (true)
                {
                    n--;
                    break;
                    label814: paramPrintWriter.println();
                }
            }
        }
        if (this.mWaitingForDrawn.size() > 0)
        {
            paramPrintWriter.println();
            paramPrintWriter.println("    Clients waiting for these windows to be drawn:");
            for (int m = -1 + this.mWaitingForDrawn.size(); m >= 0; m--)
            {
                Pair localPair = (Pair)this.mWaitingForDrawn.get(m);
                paramPrintWriter.print("    Waiting #");
                paramPrintWriter.print(m);
                paramPrintWriter.print(' ');
                paramPrintWriter.print(localPair.first);
                paramPrintWriter.print(": ");
                paramPrintWriter.println(localPair.second);
            }
        }
        paramPrintWriter.println();
        if (this.mDisplay != null)
        {
            paramPrintWriter.print("    Display: init=");
            paramPrintWriter.print(this.mInitialDisplayWidth);
            paramPrintWriter.print("x");
            paramPrintWriter.print(this.mInitialDisplayHeight);
            if ((this.mInitialDisplayWidth != this.mBaseDisplayWidth) || (this.mInitialDisplayHeight != this.mBaseDisplayHeight))
            {
                paramPrintWriter.print(" base=");
                paramPrintWriter.print(this.mBaseDisplayWidth);
                paramPrintWriter.print("x");
                paramPrintWriter.print(this.mBaseDisplayHeight);
            }
            int j = this.mDisplay.getRawWidth();
            int k = this.mDisplay.getRawHeight();
            if ((j != this.mCurDisplayWidth) || (k != this.mCurDisplayHeight))
            {
                paramPrintWriter.print(" raw=");
                paramPrintWriter.print(j);
                paramPrintWriter.print("x");
                paramPrintWriter.print(k);
            }
            paramPrintWriter.print(" cur=");
            paramPrintWriter.print(this.mCurDisplayWidth);
            paramPrintWriter.print("x");
            paramPrintWriter.print(this.mCurDisplayHeight);
            paramPrintWriter.print(" app=");
            paramPrintWriter.print(this.mAppDisplayWidth);
            paramPrintWriter.print("x");
            paramPrintWriter.print(this.mAppDisplayHeight);
            paramPrintWriter.print(" rng=");
            paramPrintWriter.print(this.mSmallestDisplayWidth);
            paramPrintWriter.print("x");
            paramPrintWriter.print(this.mSmallestDisplayHeight);
            paramPrintWriter.print("-");
            paramPrintWriter.print(this.mLargestDisplayWidth);
            paramPrintWriter.print("x");
            paramPrintWriter.println(this.mLargestDisplayHeight);
            paramPrintWriter.print("    mCurConfiguration=");
            paramPrintWriter.println(this.mCurConfiguration);
            paramPrintWriter.print("    mCurrentFocus=");
            paramPrintWriter.println(this.mCurrentFocus);
            if (this.mLastFocus != this.mCurrentFocus)
            {
                paramPrintWriter.print("    mLastFocus=");
                paramPrintWriter.println(this.mLastFocus);
            }
            paramPrintWriter.print("    mFocusedApp=");
            paramPrintWriter.println(this.mFocusedApp);
            if (this.mInputMethodTarget != null)
            {
                paramPrintWriter.print("    mInputMethodTarget=");
                paramPrintWriter.println(this.mInputMethodTarget);
            }
            paramPrintWriter.print("    mInTouchMode=");
            paramPrintWriter.print(this.mInTouchMode);
            paramPrintWriter.print(" mLayoutSeq=");
            paramPrintWriter.println(this.mLayoutSeq);
            if (paramBoolean)
            {
                paramPrintWriter.print("    mSystemDecorRect=");
                paramPrintWriter.print(this.mSystemDecorRect.toShortString());
                paramPrintWriter.print(" mSystemDecorLayer=");
                paramPrintWriter.println(this.mSystemDecorLayer);
                if (this.mLastStatusBarVisibility != 0)
                {
                    paramPrintWriter.print("    mLastStatusBarVisibility=0x");
                    paramPrintWriter.println(Integer.toHexString(this.mLastStatusBarVisibility));
                }
                if (this.mInputMethodWindow != null)
                {
                    paramPrintWriter.print("    mInputMethodWindow=");
                    paramPrintWriter.println(this.mInputMethodWindow);
                }
                paramPrintWriter.print("    mWallpaperTarget=");
                paramPrintWriter.println(this.mWallpaperTarget);
                if ((this.mLowerWallpaperTarget != null) && (this.mUpperWallpaperTarget != null))
                {
                    paramPrintWriter.print("    mLowerWallpaperTarget=");
                    paramPrintWriter.println(this.mLowerWallpaperTarget);
                    paramPrintWriter.print("    mUpperWallpaperTarget=");
                    paramPrintWriter.println(this.mUpperWallpaperTarget);
                }
                paramPrintWriter.print("    mLastWallpaperX=");
                paramPrintWriter.print(this.mLastWallpaperX);
                paramPrintWriter.print(" mLastWallpaperY=");
                paramPrintWriter.println(this.mLastWallpaperY);
                if ((this.mInputMethodAnimLayerAdjustment != 0) || (this.mWallpaperAnimLayerAdjustment != 0))
                {
                    paramPrintWriter.print("    mInputMethodAnimLayerAdjustment=");
                    paramPrintWriter.print(this.mInputMethodAnimLayerAdjustment);
                    paramPrintWriter.print("    mWallpaperAnimLayerAdjustment=");
                    paramPrintWriter.println(this.mWallpaperAnimLayerAdjustment);
                }
                paramPrintWriter.print("    mSystemBooted=");
                paramPrintWriter.print(this.mSystemBooted);
                paramPrintWriter.print(" mDisplayEnabled=");
                paramPrintWriter.println(this.mDisplayEnabled);
                paramPrintWriter.print("    mLayoutNeeded=");
                paramPrintWriter.print(this.mLayoutNeeded);
                paramPrintWriter.print("mTransactionSequence=");
                paramPrintWriter.println(this.mTransactionSequence);
                paramPrintWriter.print("    mDisplayFrozen=");
                paramPrintWriter.print(this.mDisplayFrozen);
                paramPrintWriter.print(" mWindowsFreezingScreen=");
                paramPrintWriter.print(this.mWindowsFreezingScreen);
                paramPrintWriter.print(" mAppsFreezingScreen=");
                paramPrintWriter.print(this.mAppsFreezingScreen);
                paramPrintWriter.print(" mWaitingForConfig=");
                paramPrintWriter.println(this.mWaitingForConfig);
                paramPrintWriter.print("    mRotation=");
                paramPrintWriter.print(this.mRotation);
                paramPrintWriter.print(" mAltOrientation=");
                paramPrintWriter.println(this.mAltOrientation);
                paramPrintWriter.print("    mLastWindowForcedOrientation=");
                paramPrintWriter.print(this.mLastWindowForcedOrientation);
                paramPrintWriter.print(" mForcedAppOrientation=");
                paramPrintWriter.println(this.mForcedAppOrientation);
                paramPrintWriter.print("    mDeferredRotationPauseCount=");
                paramPrintWriter.println(this.mDeferredRotationPauseCount);
                if (this.mAnimator.mScreenRotationAnimation != null)
                {
                    paramPrintWriter.println("    mScreenRotationAnimation:");
                    this.mAnimator.mScreenRotationAnimation.printTo("        ", paramPrintWriter);
                }
                paramPrintWriter.print("    mWindowAnimationScale=");
                paramPrintWriter.print(this.mWindowAnimationScale);
                paramPrintWriter.print(" mTransitionWindowAnimationScale=");
                paramPrintWriter.print(this.mTransitionAnimationScale);
                paramPrintWriter.print(" mAnimatorDurationScale=");
                paramPrintWriter.println(this.mAnimatorDurationScale);
                paramPrintWriter.print("    mTraversalScheduled=");
                paramPrintWriter.print(this.mTraversalScheduled);
                paramPrintWriter.print(" mNextAppTransition=0x");
                paramPrintWriter.print(Integer.toHexString(this.mNextAppTransition));
                paramPrintWriter.print(" mAppTransitionReady=");
                paramPrintWriter.println(this.mAppTransitionReady);
                paramPrintWriter.print("    mAppTransitionRunning=");
                paramPrintWriter.print(this.mAppTransitionRunning);
                paramPrintWriter.print(" mAppTransitionTimeout=");
                paramPrintWriter.println(this.mAppTransitionTimeout);
                if (this.mNextAppTransitionType != 0)
                {
                    paramPrintWriter.print("    mNextAppTransitionType=");
                    paramPrintWriter.println(this.mNextAppTransitionType);
                }
                switch (this.mNextAppTransitionType)
                {
                default:
                case 1:
                case 2:
                case 3:
                case 4:
                }
            }
        }
        while (true)
        {
            if (this.mNextAppTransitionCallback != null)
            {
                paramPrintWriter.print("    mNextAppTransitionCallback=");
                paramPrintWriter.println(this.mNextAppTransitionCallback);
            }
            paramPrintWriter.print("    mStartingIconInTransition=");
            paramPrintWriter.print(this.mStartingIconInTransition);
            paramPrintWriter.print(" mSkipAppTransitionAnimation=");
            paramPrintWriter.println(this.mSkipAppTransitionAnimation);
            paramPrintWriter.println("    Window Animator:");
            this.mAnimator.dump(paramPrintWriter, "        ", paramBoolean);
            return;
            paramPrintWriter.println("    NO DISPLAY");
            break;
            paramPrintWriter.print("    mNextAppTransitionPackage=");
            paramPrintWriter.println(this.mNextAppTransitionPackage);
            paramPrintWriter.print("    mNextAppTransitionEnter=0x");
            paramPrintWriter.print(Integer.toHexString(this.mNextAppTransitionEnter));
            paramPrintWriter.print(" mNextAppTransitionExit=0x");
            paramPrintWriter.println(Integer.toHexString(this.mNextAppTransitionExit));
            continue;
            paramPrintWriter.print("    mNextAppTransitionStartX=");
            paramPrintWriter.print(this.mNextAppTransitionStartX);
            paramPrintWriter.print(" mNextAppTransitionStartY=");
            paramPrintWriter.println(this.mNextAppTransitionStartY);
            paramPrintWriter.print("    mNextAppTransitionStartWidth=");
            paramPrintWriter.print(this.mNextAppTransitionStartWidth);
            paramPrintWriter.print(" mNextAppTransitionStartHeight=");
            paramPrintWriter.println(this.mNextAppTransitionStartHeight);
            continue;
            paramPrintWriter.print("    mNextAppTransitionThumbnail=");
            paramPrintWriter.print(this.mNextAppTransitionThumbnail);
            paramPrintWriter.print(" mNextAppTransitionStartX=");
            paramPrintWriter.print(this.mNextAppTransitionStartX);
            paramPrintWriter.print(" mNextAppTransitionStartY=");
            paramPrintWriter.println(this.mNextAppTransitionStartY);
            paramPrintWriter.print("    mNextAppTransitionDelayed=");
            paramPrintWriter.println(this.mNextAppTransitionDelayed);
        }
    }

    public void enableScreenAfterBoot()
    {
        synchronized (this.mWindowMap)
        {
            if (!this.mSystemBooted)
            {
                this.mSystemBooted = true;
                hideBootMessagesLocked();
                Message localMessage = this.mH.obtainMessage(23);
                this.mH.sendMessageDelayed(localMessage, 30000L);
                this.mPolicy.systemBooted();
                performEnableScreen();
            }
        }
    }

    void enableScreenIfNeededLocked()
    {
        if (this.mDisplayEnabled);
        while (true)
        {
            return;
            if ((this.mSystemBooted) || (this.mShowingBootMessages))
                this.mH.sendMessage(this.mH.obtainMessage(16));
        }
    }

    public void executeAppTransition()
    {
        if (!checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "executeAppTransition()"))
            throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
        synchronized (this.mWindowMap)
        {
            if (this.mNextAppTransition != -1)
            {
                this.mAppTransitionReady = true;
                long l = Binder.clearCallingIdentity();
                performLayoutAndPlaceSurfacesLocked();
                Binder.restoreCallingIdentity(l);
            }
            return;
        }
    }

    public void exitKeyguardSecurely(final IOnKeyguardExitResult paramIOnKeyguardExitResult)
    {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.DISABLE_KEYGUARD") != 0)
            throw new SecurityException("Requires DISABLE_KEYGUARD permission");
        this.mPolicy.exitKeyguardSecurely(new WindowManagerPolicy.OnKeyguardExitResult()
        {
            public void onKeyguardExitResult(boolean paramAnonymousBoolean)
            {
                try
                {
                    paramIOnKeyguardExitResult.onKeyguardExitResult(paramAnonymousBoolean);
                    label10: return;
                }
                catch (RemoteException localRemoteException)
                {
                    break label10;
                }
            }
        });
    }

    AppWindowToken findAppWindowToken(IBinder paramIBinder)
    {
        WindowToken localWindowToken = (WindowToken)this.mTokenMap.get(paramIBinder);
        if (localWindowToken == null);
        for (AppWindowToken localAppWindowToken = null; ; localAppWindowToken = localWindowToken.appWindowToken)
            return localAppWindowToken;
    }

    int findDesiredInputMethodWindowIndexLocked(boolean paramBoolean)
    {
        ArrayList localArrayList = this.mWindows;
        int i = localArrayList.size();
        Object localObject1 = null;
        int j = i;
        while (j > 0)
        {
            j--;
            localObject1 = (WindowState)localArrayList.get(j);
            if (canBeImeTarget((WindowState)localObject1))
                if ((!paramBoolean) && (((WindowState)localObject1).mAttrs.type == 3) && (j > 0))
                {
                    WindowState localWindowState3 = (WindowState)localArrayList.get(j - 1);
                    if ((localWindowState3.mAppToken == ((WindowState)localObject1).mAppToken) && (canBeImeTarget(localWindowState3)))
                    {
                        j--;
                        localObject1 = localWindowState3;
                    }
                }
        }
        if ((this.mInputMethodTarget != null) && (localObject1 != null) && (this.mInputMethodTarget.isDisplayedLw()) && (this.mInputMethodTarget.mExiting) && (this.mInputMethodTarget.mWinAnimator.mAnimLayer > ((WindowState)localObject1).mWinAnimator.mAnimLayer))
        {
            localObject1 = this.mInputMethodTarget;
            j = localArrayList.indexOf(localObject1);
        }
        Object localObject2;
        int m;
        int n;
        WindowState localWindowState2;
        int k;
        if ((paramBoolean) && (localObject1 != null))
        {
            WindowState localWindowState1 = this.mInputMethodTarget;
            if ((localWindowState1 != null) && (localWindowState1.mAppToken != null))
            {
                AppWindowToken localAppWindowToken = localWindowState1.mAppToken;
                localObject2 = null;
                m = 0;
                if ((localAppWindowToken.mAppAnimator.animating) || (localAppWindowToken.mAppAnimator.animation != null))
                {
                    n = localArrayList.indexOf(localWindowState1);
                    if (n >= 0)
                    {
                        localWindowState2 = (WindowState)localArrayList.get(n);
                        if (localWindowState2.mAppToken == localAppWindowToken)
                            break label303;
                    }
                }
                if (localObject2 != null)
                    if (this.mNextAppTransition != -1)
                    {
                        this.mInputMethodTargetWaitingAnim = true;
                        this.mInputMethodTarget = localObject2;
                        k = m + 1;
                    }
            }
        }
        while (true)
        {
            return k;
            label303: if ((!localWindowState2.mRemoved) && ((localObject2 == null) || (localWindowState2.mWinAnimator.mAnimLayer > localObject2.mWinAnimator.mAnimLayer)))
            {
                localObject2 = localWindowState2;
                m = n;
            }
            n--;
            break;
            if ((localObject2.mWinAnimator.isAnimating()) && (localObject2.mWinAnimator.mAnimLayer > ((WindowState)localObject1).mWinAnimator.mAnimLayer))
            {
                this.mInputMethodTargetWaitingAnim = true;
                this.mInputMethodTarget = localObject2;
                k = m + 1;
            }
            else
            {
                if (localObject1 != null)
                {
                    if (paramBoolean)
                    {
                        this.mInputMethodTarget = ((WindowState)localObject1);
                        this.mInputMethodTargetWaitingAnim = false;
                        if (((WindowState)localObject1).mAppToken == null)
                            break label451;
                        setInputMethodAnimLayerAdjustment(((WindowState)localObject1).mAppToken.mAppAnimator.animLayerAdjustment);
                    }
                    while (true)
                    {
                        k = j + 1;
                        break;
                        label451: setInputMethodAnimLayerAdjustment(0);
                    }
                }
                if (paramBoolean)
                {
                    this.mInputMethodTarget = null;
                    setInputMethodAnimLayerAdjustment(0);
                }
                k = -1;
            }
        }
    }

    public void finishDrawingWindow(Session paramSession, IWindow paramIWindow)
    {
        long l = Binder.clearCallingIdentity();
        synchronized (this.mWindowMap)
        {
            WindowState localWindowState = windowForClientLocked(paramSession, paramIWindow, false);
            if ((localWindowState != null) && (localWindowState.mWinAnimator.finishDrawingLocked()))
            {
                if ((0x100000 & localWindowState.mAttrs.flags) != 0)
                    adjustWallpaperWindowsLocked();
                this.mLayoutNeeded = true;
                performLayoutAndPlaceSurfacesLocked();
            }
            Binder.restoreCallingIdentity(l);
            return;
        }
    }

    public void freezeRotation(int paramInt)
    {
        if (!checkCallingPermission("android.permission.SET_ORIENTATION", "freezeRotation()"))
            throw new SecurityException("Requires SET_ORIENTATION permission");
        if ((paramInt < -1) || (paramInt > 3))
            throw new IllegalArgumentException("Rotation argument must be -1 or a valid rotation constant.");
        WindowManagerPolicy localWindowManagerPolicy = this.mPolicy;
        if (paramInt == -1)
            paramInt = this.mRotation;
        localWindowManagerPolicy.setUserRotationMode(1, paramInt);
        updateRotationUnchecked(false, false);
    }

    public float getAnimationScale(int paramInt)
    {
        float f;
        switch (paramInt)
        {
        default:
            f = 0.0F;
        case 0:
        case 1:
        case 2:
        }
        while (true)
        {
            return f;
            f = this.mWindowAnimationScale;
            continue;
            f = this.mTransitionAnimationScale;
            continue;
            f = this.mAnimatorDurationScale;
        }
    }

    public float[] getAnimationScales()
    {
        float[] arrayOfFloat = new float[3];
        arrayOfFloat[0] = this.mWindowAnimationScale;
        arrayOfFloat[1] = this.mTransitionAnimationScale;
        arrayOfFloat[2] = this.mAnimatorDurationScale;
        return arrayOfFloat;
    }

    public int getAppOrientation(IApplicationToken paramIApplicationToken)
    {
        int i;
        synchronized (this.mWindowMap)
        {
            AppWindowToken localAppWindowToken = findAppWindowToken(paramIApplicationToken.asBinder());
            if (localAppWindowToken == null)
                i = -1;
            else
                i = localAppWindowToken.requestedOrientation;
        }
        return i;
    }

    public void getCurrentSizeRange(Point paramPoint1, Point paramPoint2)
    {
        synchronized (this.mDisplaySizeLock)
        {
            paramPoint1.x = this.mSmallestDisplayWidth;
            paramPoint1.y = this.mSmallestDisplayHeight;
            paramPoint2.x = this.mLargestDisplayWidth;
            paramPoint2.y = this.mLargestDisplayHeight;
            return;
        }
    }

    public void getDisplaySize(Point paramPoint)
    {
        synchronized (this.mDisplaySizeLock)
        {
            paramPoint.x = this.mAppDisplayWidth;
            paramPoint.y = this.mAppDisplayHeight;
            return;
        }
    }

    public IBinder getFocusedWindowClientToken()
    {
        IBinder localIBinder;
        synchronized (this.mWindowMap)
        {
            WindowState localWindowState = getFocusedWindowLocked();
            if (localWindowState != null)
                localIBinder = localWindowState.mClient.asBinder();
            else
                localIBinder = null;
        }
        return localIBinder;
    }

    public void getInitialDisplaySize(Point paramPoint)
    {
        synchronized (this.mDisplaySizeLock)
        {
            paramPoint.x = this.mInitialDisplayWidth;
            paramPoint.y = this.mInitialDisplayHeight;
            return;
        }
    }

    public InputManagerService getInputManagerService()
    {
        return this.mInputManager;
    }

    public int getLidState()
    {
        int i = 0;
        int j = this.mInputManager.getSwitchState(-1, -256, 0);
        if (j > 0);
        while (true)
        {
            return i;
            if (j == 0)
                i = 1;
            else
                i = -1;
        }
    }

    public int getMaximumSizeDimension()
    {
        synchronized (this.mDisplaySizeLock)
        {
            if (this.mBaseDisplayWidth > this.mBaseDisplayHeight)
            {
                i = this.mBaseDisplayWidth;
                return i;
            }
            int i = this.mBaseDisplayHeight;
        }
    }

    public int getOrientationFromAppTokensLocked()
    {
        int i = 0;
        int j = -1;
        int k = 0;
        int m = 0;
        boolean bool = false;
        int n = -1 + this.mAppTokens.size();
        AppWindowToken localAppWindowToken;
        while (true)
            if (n >= 0)
            {
                localAppWindowToken = (AppWindowToken)this.mAppTokens.get(n);
                if ((k == 0) && (!localAppWindowToken.hidden) && (localAppWindowToken.hiddenRequested))
                    n--;
                else
                    if ((m != 1) || (i == localAppWindowToken.groupId) || (j == 3) || (!bool))
                        break;
            }
        for (int i1 = j; ; i1 = -1)
        {
            do
            {
                return i1;
                if ((localAppWindowToken.hiddenRequested) || (localAppWindowToken.willBeHidden))
                    break;
                if (m == 0)
                {
                    m = 1;
                    i = localAppWindowToken.groupId;
                    j = localAppWindowToken.requestedOrientation;
                }
                i1 = localAppWindowToken.requestedOrientation;
                bool = localAppWindowToken.appFullscreen;
            }
            while (((bool) && (i1 != 3)) || ((i1 != -1) && (i1 != 3)));
            if (i1 == 3);
            for (int i2 = 1; ; i2 = 0)
            {
                k |= i2;
                break;
            }
        }
    }

    public int getOrientationFromWindowsLocked()
    {
        int i;
        if ((this.mDisplayFrozen) || (this.mOpeningApps.size() > 0) || (this.mClosingApps.size() > 0))
            i = this.mLastWindowForcedOrientation;
        while (true)
        {
            return i;
            int j = -1 + this.mWindows.size();
            while (true)
                if (j >= 0)
                {
                    WindowState localWindowState = (WindowState)this.mWindows.get(j);
                    j--;
                    if (localWindowState.mAppToken != null)
                    {
                        this.mLastWindowForcedOrientation = -1;
                        i = -1;
                        break;
                    }
                    if ((localWindowState.isVisibleLw()) && (localWindowState.mPolicyVisibilityAfterAnim))
                    {
                        i = localWindowState.mAttrs.screenOrientation;
                        if ((i != -1) && (i != 3))
                        {
                            this.mLastWindowForcedOrientation = i;
                            break;
                        }
                    }
                }
            this.mLastWindowForcedOrientation = -1;
            i = -1;
        }
    }

    public int getPendingAppTransition()
    {
        return this.mNextAppTransition;
    }

    public int getPreferredOptionsPanelGravity()
    {
        int i = 81;
        int j;
        synchronized (this.mWindowMap)
        {
            j = getRotation();
            if (this.mInitialDisplayWidth < this.mInitialDisplayHeight)
                switch (j)
                {
                default:
                    break;
                case 1:
                    i = 85;
                case 2:
                case 3:
                }
        }
        switch (j)
        {
        default:
        case 1:
        case 2:
        case 3:
        }
    }

    public void getRealDisplaySize(Point paramPoint)
    {
        synchronized (this.mDisplaySizeLock)
        {
            paramPoint.x = this.mCurDisplayWidth;
            paramPoint.y = this.mCurDisplayHeight;
            return;
        }
    }

    public int getRotation()
    {
        return this.mRotation;
    }

    public float getWindowCompatibilityScale(IBinder paramIBinder)
    {
        while (true)
        {
            synchronized (this.mWindowMap)
            {
                WindowState localWindowState = (WindowState)this.mWindowMap.get(paramIBinder);
                if (localWindowState != null)
                {
                    f = localWindowState.mGlobalScale;
                    return f;
                }
            }
            float f = 1.0F;
        }
    }

    public void getWindowDisplayFrame(Session paramSession, IWindow paramIWindow, Rect paramRect)
    {
        synchronized (this.mWindowMap)
        {
            WindowState localWindowState = windowForClientLocked(paramSession, paramIWindow, false);
            if (localWindowState == null)
                paramRect.setEmpty();
            else
                paramRect.set(localWindowState.mDisplayFrame);
        }
    }

    public boolean getWindowFrame(IBinder paramIBinder, Rect paramRect)
    {
        boolean bool;
        synchronized (this.mWindowMap)
        {
            WindowState localWindowState = (WindowState)this.mWindowMap.get(paramIBinder);
            if (localWindowState != null)
            {
                paramRect.set(localWindowState.getFrameLw());
                bool = true;
            }
            else
            {
                bool = false;
            }
        }
        return bool;
    }

    public int handleAppTransitionReadyLocked()
    {
        int i = 0;
        int j = this.mOpeningApps.size();
        int k = 1;
        if ((!this.mDisplayFrozen) && (!this.mAppTransitionTimeout))
            for (int i15 = 0; (i15 < j) && (k != 0); i15++)
            {
                AppWindowToken localAppWindowToken4 = (AppWindowToken)this.mOpeningApps.get(i15);
                if ((!localAppWindowToken4.allDrawn) && (!localAppWindowToken4.startingDisplayed) && (!localAppWindowToken4.startingMoved))
                    k = 0;
            }
        int m;
        label236: label380: Object localObject;
        label267: label357: int i6;
        label424: Rect localRect;
        if (k != 0)
        {
            m = this.mNextAppTransition;
            if (this.mSkipAppTransitionAnimation)
                m = -1;
            this.mNextAppTransition = -1;
            this.mAppTransitionReady = false;
            this.mAppTransitionRunning = true;
            this.mAppTransitionTimeout = false;
            this.mStartingIconInTransition = false;
            this.mSkipAppTransitionAnimation = false;
            this.mH.removeMessages(13);
            rebuildAppWindowListLocked();
            WindowState localWindowState1;
            WindowManager.LayoutParams localLayoutParams;
            int n;
            int i1;
            int i2;
            int i3;
            int i5;
            AppWindowToken localAppWindowToken3;
            int i14;
            if ((this.mWallpaperTarget != null) && (this.mWallpaperTarget.mWinAnimator.isAnimating()) && (!this.mWallpaperTarget.mWinAnimator.isDummyAnimation()))
            {
                localWindowState1 = null;
                adjustWallpaperWindowsLocked();
                this.mInnerFields.mWallpaperMayChange = false;
                localLayoutParams = null;
                n = -1;
                i1 = 0;
                i2 = 0;
                i3 = this.mClosingApps.size();
                int i4 = i3 + this.mOpeningApps.size();
                i5 = 0;
                if (i5 >= i4)
                    break label424;
                if (i5 >= i3)
                    break label357;
                localAppWindowToken3 = (AppWindowToken)this.mClosingApps.get(i5);
                i14 = 1;
                if ((this.mLowerWallpaperTarget != null) && ((this.mLowerWallpaperTarget.mAppToken == localAppWindowToken3) || (this.mUpperWallpaperTarget.mAppToken == localAppWindowToken3)))
                    i2 |= i14;
                if (!localAppWindowToken3.appFullscreen)
                    break label380;
                WindowState localWindowState4 = localAppWindowToken3.findMainWindow();
                if (localWindowState4 != null)
                {
                    localLayoutParams = localWindowState4.mAttrs;
                    n = localWindowState4.mLayer;
                    i1 = 1;
                }
            }
            while (true)
            {
                i5++;
                break label236;
                localWindowState1 = this.mWallpaperTarget;
                break;
                localAppWindowToken3 = (AppWindowToken)this.mOpeningApps.get(i5 - i3);
                i14 = 2;
                break label267;
                if (i1 == 0)
                {
                    WindowState localWindowState3 = localAppWindowToken3.findMainWindow();
                    if ((localWindowState3 != null) && (localWindowState3.mLayer > n))
                    {
                        localLayoutParams = localWindowState3.mAttrs;
                        n = localWindowState3.mLayer;
                    }
                }
            }
            int i7;
            if (i2 == 3)
                switch (m)
                {
                default:
                    if (!this.mPolicy.allowAppAnimationsLw())
                        localLayoutParams = null;
                    localObject = null;
                    i6 = 0;
                    i7 = this.mOpeningApps.size();
                case 4102:
                case 4104:
                case 4106:
                case 8199:
                case 8201:
                case 8203:
                }
            for (int i8 = 0; ; i8++)
            {
                if (i8 >= i7)
                    break label782;
                AppWindowToken localAppWindowToken2 = (AppWindowToken)this.mOpeningApps.get(i8);
                localAppWindowToken2.mAppAnimator.clearThumbnail();
                localAppWindowToken2.reportedVisible = false;
                localAppWindowToken2.inPendingTransaction = false;
                localAppWindowToken2.mAppAnimator.animation = null;
                setTokenVisibilityLocked(localAppWindowToken2, localLayoutParams, true, m, false);
                localAppWindowToken2.updateReportedVisibilityLocked();
                localAppWindowToken2.waitingToShow = false;
                WindowAnimator localWindowAnimator = this.mAnimator;
                localWindowAnimator.mAnimating |= localAppWindowToken2.mAppAnimator.showAllWindowsLocked();
                if (localLayoutParams != null)
                {
                    int i11 = -1;
                    for (int i12 = 0; ; i12++)
                    {
                        int i13 = localAppWindowToken2.windows.size();
                        if (i12 >= i13)
                            break;
                        WindowState localWindowState2 = (WindowState)localAppWindowToken2.windows.get(i12);
                        if (localWindowState2.mWinAnimator.mAnimLayer > i11)
                            i11 = localWindowState2.mWinAnimator.mAnimLayer;
                    }
                    m = 4110;
                    break;
                    m = 8207;
                    break;
                    if ((localWindowState1 != null) && (!this.mOpeningApps.contains(localWindowState1.mAppToken)))
                    {
                        m = 8204;
                        break;
                    }
                    if (this.mWallpaperTarget == null)
                        break;
                    m = 4109;
                    break;
                    if ((localObject == null) || (i11 > i6))
                    {
                        localObject = localAppWindowToken2;
                        i6 = i11;
                    }
                }
            }
            label782: int i9 = this.mClosingApps.size();
            for (int i10 = 0; i10 < i9; i10++)
            {
                AppWindowToken localAppWindowToken1 = (AppWindowToken)this.mClosingApps.get(i10);
                localAppWindowToken1.mAppAnimator.clearThumbnail();
                localAppWindowToken1.inPendingTransaction = false;
                localAppWindowToken1.mAppAnimator.animation = null;
                setTokenVisibilityLocked(localAppWindowToken1, localLayoutParams, false, m, false);
                localAppWindowToken1.updateReportedVisibilityLocked();
                localAppWindowToken1.waitingToHide = false;
                localAppWindowToken1.allDrawn = true;
            }
            if ((this.mNextAppTransitionThumbnail != null) && (localObject != null) && (localObject.mAppAnimator.animation != null))
                localRect = new Rect(0, 0, this.mNextAppTransitionThumbnail.getWidth(), this.mNextAppTransitionThumbnail.getHeight());
        }
        try
        {
            Surface localSurface1 = new Surface(this.mFxSession, Process.myPid(), "thumbnail anim", 0, localRect.width(), localRect.height(), -3, 4);
            localObject.mAppAnimator.thumbnail = localSurface1;
            Surface localSurface2 = new Surface();
            localSurface2.copyFrom(localSurface1);
            Canvas localCanvas = localSurface2.lockCanvas(localRect);
            localCanvas.drawBitmap(this.mNextAppTransitionThumbnail, 0.0F, 0.0F, null);
            localSurface2.unlockCanvasAndPost(localCanvas);
            localSurface2.release();
            localObject.mAppAnimator.thumbnailLayer = i6;
            Animation localAnimation = createThumbnailAnimationLocked(m, true, true, this.mNextAppTransitionDelayed);
            localObject.mAppAnimator.thumbnailAnimation = localAnimation;
            localAnimation.restrictDuration(10000L);
            localAnimation.scaleCurrentDuration(this.mTransitionAnimationScale);
            localObject.mAppAnimator.thumbnailX = this.mNextAppTransitionStartX;
            localObject.mAppAnimator.thumbnailY = this.mNextAppTransitionStartY;
            this.mNextAppTransitionType = 0;
            this.mNextAppTransitionPackage = null;
            this.mNextAppTransitionThumbnail = null;
            scheduleAnimationCallback(this.mNextAppTransitionCallback);
            this.mNextAppTransitionCallback = null;
            this.mOpeningApps.clear();
            this.mClosingApps.clear();
            i = 0x0 | 0x3;
            this.mLayoutNeeded = true;
            if (!moveInputMethodWindowsIfNeededLocked(true))
                assignLayersLocked();
            updateFocusedWindowLocked(2, false);
            this.mFocusMayChange = false;
            return i;
        }
        catch (Surface.OutOfResourcesException localOutOfResourcesException)
        {
            while (true)
            {
                Slog.e("WindowManager", "Can't allocate thumbnail surface w=" + localRect.width() + " h=" + localRect.height(), localOutOfResourcesException);
                localObject.mAppAnimator.clearThumbnail();
            }
        }
    }

    public boolean hasNavigationBar()
    {
        return this.mPolicy.hasNavigationBar();
    }

    public boolean hasSystemNavBar()
    {
        return this.mPolicy.hasSystemNavBar();
    }

    public void hideBootMessagesLocked()
    {
        if (this.mShowingBootMessages)
        {
            this.mShowingBootMessages = false;
            this.mPolicy.hideBootMessages();
        }
    }

    public boolean inKeyguardRestrictedInputMode()
    {
        return this.mPolicy.inKeyguardRestrictedKeyInputMode();
    }

    public boolean inputMethodClientHasFocus(IInputMethodClient paramIInputMethodClient)
    {
        boolean bool = true;
        while (true)
        {
            int j;
            synchronized (this.mWindowMap)
            {
                int i = findDesiredInputMethodWindowIndexLocked(false);
                if (i > 0)
                {
                    Object localObject2 = (WindowState)this.mWindows.get(i - 1);
                    if (localObject2 != null)
                    {
                        if ((((WindowState)localObject2).mAttrs.type == 3) && (((WindowState)localObject2).mAppToken != null))
                        {
                            j = 0;
                            if (j < ((WindowState)localObject2).mAppToken.windows.size())
                            {
                                WindowState localWindowState = (WindowState)((WindowState)localObject2).mAppToken.windows.get(j);
                                if (localWindowState == localObject2)
                                    break label237;
                                Log.i("WindowManager", "Switching to real app window: " + localWindowState);
                                localObject2 = localWindowState;
                            }
                        }
                        if ((((WindowState)localObject2).mSession.mClient != null) && (((WindowState)localObject2).mSession.mClient.asBinder() == paramIInputMethodClient.asBinder()))
                            continue;
                        if ((this.mCurrentFocus == null) || (this.mCurrentFocus.mSession.mClient == null) || (this.mCurrentFocus.mSession.mClient.asBinder() != paramIInputMethodClient.asBinder()));
                    }
                }
            }
            label237: j++;
        }
    }

    public boolean isHardKeyboardAvailable()
    {
        synchronized (this.mWindowMap)
        {
            boolean bool = this.mHardKeyboardAvailable;
            return bool;
        }
    }

    public boolean isHardKeyboardEnabled()
    {
        synchronized (this.mWindowMap)
        {
            boolean bool = this.mHardKeyboardEnabled;
            return bool;
        }
    }

    public boolean isKeyguardLocked()
    {
        return this.mPolicy.isKeyguardLocked();
    }

    public boolean isKeyguardSecure()
    {
        return this.mPolicy.isKeyguardSecure();
    }

    public boolean isViewServerRunning()
    {
        boolean bool = false;
        if (isSystemSecure());
        while (true)
        {
            return bool;
            if ((checkCallingPermission("android.permission.DUMP", "isViewServerRunning")) && (this.mViewServer != null) && (this.mViewServer.isRunning()))
                bool = true;
        }
    }

    final boolean isWallpaperVisible(WindowState paramWindowState)
    {
        if (((paramWindowState != null) && ((!paramWindowState.mObscured) || ((paramWindowState.mAppToken != null) && (paramWindowState.mAppToken.mAppAnimator.animation != null)))) || (this.mUpperWallpaperTarget != null) || (this.mLowerWallpaperTarget != null));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    Animation loadAnimation(WindowManager.LayoutParams paramLayoutParams, int paramInt)
    {
        int i = 0;
        Context localContext = this.mContext;
        if (paramInt >= 0)
        {
            AttributeCache.Entry localEntry = getCachedAnimations(paramLayoutParams);
            if (localEntry != null)
            {
                localContext = localEntry.context;
                i = localEntry.array.getResourceId(paramInt, 0);
            }
        }
        if (i != 0);
        for (Animation localAnimation = AnimationUtils.loadAnimation(localContext, i); ; localAnimation = null)
            return localAnimation;
    }

    public void lockNow()
    {
        this.mPolicy.lockNow();
    }

    void logWindowList(String paramString)
    {
        int i = this.mWindows.size();
        while (i > 0)
        {
            i--;
            Slog.v("WindowManager", paramString + "#" + i + ": " + this.mWindows.get(i));
        }
    }

    void makeWindowFreezingScreenIfNeededLocked(WindowState paramWindowState)
    {
        if (!okToDisplay())
        {
            paramWindowState.mOrientationChanging = true;
            this.mInnerFields.mOrientationChangeComplete = false;
            if (!this.mWindowsFreezingScreen)
            {
                this.mWindowsFreezingScreen = true;
                this.mH.removeMessages(11);
                this.mH.sendMessageDelayed(this.mH.obtainMessage(11), 2000L);
            }
        }
    }

    public void monitor()
    {
        synchronized (this.mWindowMap)
        {
            synchronized (this.mKeyguardTokenWatcher)
            {
            }
        }
    }

    public InputChannel monitorInput(String paramString)
    {
        return this.mInputManager.monitorInput(paramString);
    }

    public void moveAppToken(int paramInt, IBinder paramIBinder)
    {
        if (!checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "moveAppToken()"))
            throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
        synchronized (this.mWindowMap)
        {
            AppWindowToken localAppWindowToken = findAppWindowToken(paramIBinder);
            if ((this.mAppTokens.indexOf(localAppWindowToken) > paramInt) && (this.mNextAppTransition != -1) && (!this.mAppTransitionRunning))
            {
                this.mAnimatingAppTokens.clear();
                this.mAnimatingAppTokens.addAll(this.mAppTokens);
            }
            if ((localAppWindowToken == null) || (!this.mAppTokens.remove(localAppWindowToken)))
            {
                Slog.w("WindowManager", "Attempting to reorder token that doesn't exist: " + paramIBinder + " (" + localAppWindowToken + ")");
            }
            else
            {
                this.mAppTokens.add(paramInt, localAppWindowToken);
                if ((this.mNextAppTransition == -1) && (!this.mAppTransitionRunning))
                {
                    this.mAnimatingAppTokens.clear();
                    this.mAnimatingAppTokens.addAll(this.mAppTokens);
                    long l = Binder.clearCallingIdentity();
                    if (tmpRemoveAppWindowsLocked(localAppWindowToken))
                    {
                        reAddAppWindowsLocked(findWindowOffsetLocked(paramInt), localAppWindowToken);
                        updateFocusedWindowLocked(3, false);
                        this.mLayoutNeeded = true;
                        this.mInputMonitor.setUpdateInputWindowsNeededLw();
                        performLayoutAndPlaceSurfacesLocked();
                        this.mInputMonitor.updateInputWindowsLw(false);
                    }
                    Binder.restoreCallingIdentity(l);
                }
            }
        }
    }

    public void moveAppTokensToBottom(List<IBinder> paramList)
    {
        if (!checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "moveAppTokensToBottom()"))
            throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
        long l = Binder.clearCallingIdentity();
        while (true)
        {
            int j;
            int k;
            synchronized (this.mWindowMap)
            {
                int i = paramList.size();
                if ((i > 0) && (!this.mAppTransitionRunning))
                {
                    this.mAnimatingAppTokens.clear();
                    this.mAnimatingAppTokens.addAll(this.mAppTokens);
                }
                removeAppTokensLocked(paramList);
                j = 0;
                k = 0;
                if (k < i)
                {
                    AppWindowToken localAppWindowToken = findAppWindowToken((IBinder)paramList.get(k));
                    if (localAppWindowToken == null)
                        break label196;
                    this.mAppTokens.add(j, localAppWindowToken);
                    if (this.mNextAppTransition != -1)
                        localAppWindowToken.sendingToBottom = true;
                }
                else
                {
                    if (!this.mAppTransitionRunning)
                    {
                        this.mAnimatingAppTokens.clear();
                        this.mAnimatingAppTokens.addAll(this.mAppTokens);
                        moveAppWindowsLocked(paramList, 0);
                    }
                    Binder.restoreCallingIdentity(l);
                    return;
                }
            }
            j++;
            label196: k++;
        }
    }

    public void moveAppTokensToTop(List<IBinder> paramList)
    {
        if (!checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "moveAppTokensToTop()"))
            throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
        long l = Binder.clearCallingIdentity();
        while (true)
        {
            int j;
            synchronized (this.mWindowMap)
            {
                removeAppTokensLocked(paramList);
                int i = paramList.size();
                j = 0;
                if (j < i)
                {
                    AppWindowToken localAppWindowToken = findAppWindowToken((IBinder)paramList.get(j));
                    if (localAppWindowToken != null)
                    {
                        this.mAppTokens.add(localAppWindowToken);
                        if (this.mNextAppTransition != -1)
                            localAppWindowToken.sendingToBottom = false;
                    }
                }
                else
                {
                    if (!this.mAppTransitionRunning)
                    {
                        this.mAnimatingAppTokens.clear();
                        this.mAnimatingAppTokens.addAll(this.mAppTokens);
                        moveAppWindowsLocked(paramList, this.mAppTokens.size());
                    }
                    Binder.restoreCallingIdentity(l);
                    return;
                }
            }
            j++;
        }
    }

    void moveInputMethodDialogsLocked(int paramInt)
    {
        ArrayList localArrayList = this.mInputMethodDialogs;
        int i = localArrayList.size();
        for (int j = 0; j < i; j++)
            paramInt = tmpRemoveWindowLocked(paramInt, (WindowState)localArrayList.get(j));
        if (paramInt >= 0)
        {
            AppWindowToken localAppWindowToken = this.mInputMethodTarget.mAppToken;
            if ((paramInt < this.mWindows.size()) && ((WindowState)this.mWindows.get(paramInt) == this.mInputMethodWindow))
                paramInt++;
            for (int m = 0; m < i; m++)
            {
                WindowState localWindowState2 = (WindowState)localArrayList.get(m);
                localWindowState2.mTargetAppToken = localAppWindowToken;
                paramInt = reAddWindowLocked(paramInt, localWindowState2);
            }
        }
        for (int k = 0; k < i; k++)
        {
            WindowState localWindowState1 = (WindowState)localArrayList.get(k);
            localWindowState1.mTargetAppToken = null;
            reAddWindowToListInOrderLocked(localWindowState1);
        }
    }

    boolean moveInputMethodWindowsIfNeededLocked(boolean paramBoolean)
    {
        WindowState localWindowState1 = null;
        WindowState localWindowState2 = this.mInputMethodWindow;
        int i = this.mInputMethodDialogs.size();
        boolean bool;
        if ((localWindowState2 == null) && (i == 0))
        {
            bool = false;
            return bool;
        }
        int j = findDesiredInputMethodWindowIndexLocked(true);
        if (j >= 0)
        {
            int k = this.mWindows.size();
            if (j < k)
                localWindowState1 = (WindowState)this.mWindows.get(j);
            Object localObject;
            label79: int n;
            if (localWindowState2 != null)
            {
                localObject = localWindowState2;
                if (((WindowState)localObject).mChildWindows.size() > 0)
                {
                    WindowState localWindowState3 = (WindowState)((WindowState)localObject).mChildWindows.get(0);
                    if (localWindowState3.mSubLayer < 0)
                        localObject = localWindowState3;
                }
                if (localWindowState1 != localObject)
                    break label225;
                n = j + 1;
                label128: if ((n < k) && (((WindowState)this.mWindows.get(n)).mIsImWindow))
                    break label213;
            }
            for (int i1 = n + 1; ; i1++)
                if ((i1 >= k) || (((WindowState)this.mWindows.get(i1)).mIsImWindow))
                {
                    if (i1 < k)
                        break label225;
                    bool = false;
                    break;
                    localObject = (WindowState)this.mInputMethodDialogs.get(0);
                    break label79;
                    label213: n++;
                    break label128;
                }
            label225: if (localWindowState2 != null)
            {
                int m = tmpRemoveWindowLocked(j, localWindowState2);
                localWindowState2.mTargetAppToken = this.mInputMethodTarget.mAppToken;
                reAddWindowLocked(m, localWindowState2);
                if (i > 0)
                    moveInputMethodDialogsLocked(m + 1);
            }
        }
        while (true)
        {
            if (paramBoolean)
                assignLayersLocked();
            bool = true;
            break;
            moveInputMethodDialogsLocked(j);
            continue;
            if (localWindowState2 != null)
            {
                tmpRemoveWindowLocked(0, localWindowState2);
                localWindowState2.mTargetAppToken = null;
                reAddWindowToListInOrderLocked(localWindowState2);
                if (i > 0)
                    moveInputMethodDialogsLocked(-1);
            }
            else
            {
                moveInputMethodDialogsLocked(-1);
            }
        }
    }

    void notifyHardKeyboardStatusChange()
    {
        synchronized (this.mWindowMap)
        {
            OnHardKeyboardStatusChangeListener localOnHardKeyboardStatusChangeListener = this.mHardKeyboardStatusChangeListener;
            boolean bool1 = this.mHardKeyboardAvailable;
            boolean bool2 = this.mHardKeyboardEnabled;
            if (localOnHardKeyboardStatusChangeListener != null)
                localOnHardKeyboardStatusChangeListener.onHardKeyboardStatusChange(bool1, bool2);
            return;
        }
    }

    boolean okToDisplay()
    {
        if ((!this.mDisplayFrozen) && (this.mDisplayEnabled) && (this.mPolicy.isScreenOnFully()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
        throws RemoteException
    {
        try
        {
            boolean bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            return bool;
        }
        catch (RuntimeException localRuntimeException)
        {
            if (!(localRuntimeException instanceof SecurityException))
                Log.wtf("WindowManager", "Window Manager Crash", localRuntimeException);
            throw localRuntimeException;
        }
    }

    public IWindowSession openSession(IInputMethodClient paramIInputMethodClient, IInputContext paramIInputContext)
    {
        if (paramIInputMethodClient == null)
            throw new IllegalArgumentException("null client");
        if (paramIInputContext == null)
            throw new IllegalArgumentException("null inputContext");
        return new Session(this, paramIInputMethodClient, paramIInputContext);
    }

    public boolean outOfMemoryWindow(Session paramSession, IWindow paramIWindow)
    {
        boolean bool = false;
        long l = Binder.clearCallingIdentity();
        try
        {
            synchronized (this.mWindowMap)
            {
                WindowState localWindowState = windowForClientLocked(paramSession, paramIWindow, false);
                if (localWindowState == null)
                    return bool;
                bool = reclaimSomeSurfaceMemoryLocked(localWindowState.mWinAnimator, "from-client", false);
            }
        }
        finally
        {
            Binder.restoreCallingIdentity(l);
        }
    }

    public void overridePendingAppTransition(String paramString, int paramInt1, int paramInt2, IRemoteCallback paramIRemoteCallback)
    {
        synchronized (this.mWindowMap)
        {
            if (this.mNextAppTransition != -1)
            {
                this.mNextAppTransitionType = 1;
                this.mNextAppTransitionPackage = paramString;
                this.mNextAppTransitionThumbnail = null;
                this.mNextAppTransitionEnter = paramInt1;
                this.mNextAppTransitionExit = paramInt2;
                scheduleAnimationCallback(this.mNextAppTransitionCallback);
                this.mNextAppTransitionCallback = paramIRemoteCallback;
                return;
            }
            scheduleAnimationCallback(paramIRemoteCallback);
        }
    }

    public void overridePendingAppTransitionScaleUp(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        synchronized (this.mWindowMap)
        {
            if (this.mNextAppTransition != -1)
            {
                this.mNextAppTransitionType = 2;
                this.mNextAppTransitionPackage = null;
                this.mNextAppTransitionThumbnail = null;
                this.mNextAppTransitionStartX = paramInt1;
                this.mNextAppTransitionStartY = paramInt2;
                this.mNextAppTransitionStartWidth = paramInt3;
                this.mNextAppTransitionStartHeight = paramInt4;
                scheduleAnimationCallback(this.mNextAppTransitionCallback);
                this.mNextAppTransitionCallback = null;
            }
            return;
        }
    }

    public void overridePendingAppTransitionThumb(Bitmap paramBitmap, int paramInt1, int paramInt2, IRemoteCallback paramIRemoteCallback, boolean paramBoolean)
    {
        while (true)
        {
            int i;
            synchronized (this.mWindowMap)
            {
                if (this.mNextAppTransition != -1)
                {
                    if (paramBoolean)
                    {
                        i = 4;
                        this.mNextAppTransitionType = i;
                        this.mNextAppTransitionPackage = null;
                        this.mNextAppTransitionThumbnail = paramBitmap;
                        this.mNextAppTransitionDelayed = paramBoolean;
                        this.mNextAppTransitionStartX = paramInt1;
                        this.mNextAppTransitionStartY = paramInt2;
                        scheduleAnimationCallback(this.mNextAppTransitionCallback);
                        this.mNextAppTransitionCallback = paramIRemoteCallback;
                    }
                }
                else
                    scheduleAnimationCallback(paramIRemoteCallback);
            }
        }
    }

    public void pauseKeyDispatching(IBinder paramIBinder)
    {
        if (!checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "pauseKeyDispatching()"))
            throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
        synchronized (this.mWindowMap)
        {
            WindowToken localWindowToken = (WindowToken)this.mTokenMap.get(paramIBinder);
            if (localWindowToken != null)
                this.mInputMonitor.pauseDispatchingLw(localWindowToken);
            return;
        }
    }

    void pauseRotationLocked()
    {
        this.mDeferredRotationPauseCount = (1 + this.mDeferredRotationPauseCount);
    }

    public void performBootTimeout()
    {
        synchronized (this.mWindowMap)
        {
            if ((this.mDisplayEnabled) || (!this.mHeadless))
            {
                Slog.w("WindowManager", "***** BOOT TIMEOUT: forcing display enabled");
                this.mForceDisplayEnabled = true;
                performEnableScreen();
            }
        }
    }

    public void performDeferredDestroyWindow(Session paramSession, IWindow paramIWindow)
    {
        long l = Binder.clearCallingIdentity();
        try
        {
            synchronized (this.mWindowMap)
            {
                WindowState localWindowState = windowForClientLocked(paramSession, paramIWindow, false);
                if (localWindowState == null)
                    return;
                localWindowState.mWinAnimator.destroyDeferredSurfaceLocked();
            }
        }
        finally
        {
            Binder.restoreCallingIdentity(l);
        }
    }

    public void performEnableScreen()
    {
        while (true)
        {
            int i;
            int j;
            int k;
            int m;
            int i1;
            int i2;
            WindowState localWindowState;
            int i3;
            label157: IBinder localIBinder;
            Parcel localParcel;
            synchronized (this.mWindowMap)
            {
                if (!this.mDisplayEnabled)
                    if ((this.mSystemBooted) || (this.mShowingBootMessages));
            }
            while (true)
            {
                if (i3 != 0)
                    break label431;
                n = 1;
                break label157;
                m = 0;
                break;
                i3 = 0;
            }
            label431: int n = 0;
            continue;
            i2++;
            continue;
            if ((m == 0) || (k != 0));
        }
    }

    // ERROR //
    public void prepareAppTransition(int paramInt, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: ldc_w 2067
        //     4: ldc_w 3389
        //     7: invokevirtual 2073	com/android/server/wm/WindowManagerService:checkCallingPermission	(Ljava/lang/String;Ljava/lang/String;)Z
        //     10: ifne +14 -> 24
        //     13: new 2075	java/lang/SecurityException
        //     16: dup
        //     17: ldc_w 2077
        //     20: invokespecial 2079	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     23: athrow
        //     24: aload_0
        //     25: getfield 378	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
        //     28: astore_3
        //     29: aload_3
        //     30: monitorenter
        //     31: aload_0
        //     32: invokevirtual 916	com/android/server/wm/WindowManagerService:okToDisplay	()Z
        //     35: ifeq +73 -> 108
        //     38: aload_0
        //     39: getfield 496	com/android/server/wm/WindowManagerService:mNextAppTransition	I
        //     42: bipush 255
        //     44: if_icmpeq +10 -> 54
        //     47: aload_0
        //     48: getfield 496	com/android/server/wm/WindowManagerService:mNextAppTransition	I
        //     51: ifne +60 -> 111
        //     54: aload_0
        //     55: iload_1
        //     56: putfield 496	com/android/server/wm/WindowManagerService:mNextAppTransition	I
        //     59: aload_0
        //     60: iconst_0
        //     61: putfield 500	com/android/server/wm/WindowManagerService:mAppTransitionReady	Z
        //     64: aload_0
        //     65: iconst_0
        //     66: putfield 504	com/android/server/wm/WindowManagerService:mAppTransitionTimeout	Z
        //     69: aload_0
        //     70: iconst_0
        //     71: putfield 506	com/android/server/wm/WindowManagerService:mStartingIconInTransition	Z
        //     74: aload_0
        //     75: iconst_0
        //     76: putfield 508	com/android/server/wm/WindowManagerService:mSkipAppTransitionAnimation	Z
        //     79: aload_0
        //     80: getfield 526	com/android/server/wm/WindowManagerService:mH	Lcom/android/server/wm/WindowManagerService$H;
        //     83: bipush 13
        //     85: invokevirtual 1451	com/android/server/wm/WindowManagerService$H:removeMessages	(I)V
        //     88: aload_0
        //     89: getfield 526	com/android/server/wm/WindowManagerService:mH	Lcom/android/server/wm/WindowManagerService$H;
        //     92: aload_0
        //     93: getfield 526	com/android/server/wm/WindowManagerService:mH	Lcom/android/server/wm/WindowManagerService$H;
        //     96: bipush 13
        //     98: invokevirtual 1455	com/android/server/wm/WindowManagerService$H:obtainMessage	(I)Landroid/os/Message;
        //     101: ldc2_w 3390
        //     104: invokevirtual 2020	com/android/server/wm/WindowManagerService$H:sendMessageDelayed	(Landroid/os/Message;J)Z
        //     107: pop
        //     108: aload_3
        //     109: monitorexit
        //     110: return
        //     111: iload_2
        //     112: ifne -53 -> 59
        //     115: iload_1
        //     116: sipush 4104
        //     119: if_icmpne +28 -> 147
        //     122: aload_0
        //     123: getfield 496	com/android/server/wm/WindowManagerService:mNextAppTransition	I
        //     126: sipush 8201
        //     129: if_icmpne +18 -> 147
        //     132: aload_0
        //     133: iload_1
        //     134: putfield 496	com/android/server/wm/WindowManagerService:mNextAppTransition	I
        //     137: goto -78 -> 59
        //     140: astore 4
        //     142: aload_3
        //     143: monitorexit
        //     144: aload 4
        //     146: athrow
        //     147: iload_1
        //     148: sipush 4102
        //     151: if_icmpne -92 -> 59
        //     154: aload_0
        //     155: getfield 496	com/android/server/wm/WindowManagerService:mNextAppTransition	I
        //     158: sipush 8199
        //     161: if_icmpne -102 -> 59
        //     164: aload_0
        //     165: iload_1
        //     166: putfield 496	com/android/server/wm/WindowManagerService:mNextAppTransition	I
        //     169: goto -110 -> 59
        //
        // Exception table:
        //     from	to	target	type
        //     31	144	140	finally
        //     154	169	140	finally
    }

    // ERROR //
    IBinder prepareDragSurface(IWindow paramIWindow, SurfaceSession paramSurfaceSession, int paramInt1, int paramInt2, int paramInt3, Surface paramSurface)
    {
        // Byte code:
        //     0: invokestatic 1963	android/os/Binder:getCallingPid	()I
        //     3: istore 7
        //     5: invokestatic 2233	android/os/Binder:clearCallingIdentity	()J
        //     8: lstore 8
        //     10: aconst_null
        //     11: astore 10
        //     13: aload_0
        //     14: getfield 378	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
        //     17: astore 12
        //     19: aload 12
        //     21: monitorenter
        //     22: aload_0
        //     23: getfield 571	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
        //     26: ifnonnull +143 -> 169
        //     29: new 758	android/view/Surface
        //     32: dup
        //     33: aload_2
        //     34: iload 7
        //     36: ldc_w 3395
        //     39: iconst_0
        //     40: iload 4
        //     42: iload 5
        //     44: bipush 253
        //     46: iconst_4
        //     47: invokespecial 3165	android/view/Surface:<init>	(Landroid/view/SurfaceSession;ILjava/lang/String;IIIII)V
        //     50: astore 18
        //     52: aload 6
        //     54: aload 18
        //     56: invokevirtual 3174	android/view/Surface:copyFrom	(Landroid/view/Surface;)V
        //     59: aload_1
        //     60: invokeinterface 874 1 0
        //     65: astore 19
        //     67: new 1960	android/os/Binder
        //     70: dup
        //     71: invokespecial 3396	android/os/Binder:<init>	()V
        //     74: astore 16
        //     76: aload_0
        //     77: new 3398	com/android/server/wm/DragState
        //     80: dup
        //     81: aload_0
        //     82: aload 16
        //     84: aload 18
        //     86: iconst_0
        //     87: aload 19
        //     89: invokespecial 3401	com/android/server/wm/DragState:<init>	(Lcom/android/server/wm/WindowManagerService;Landroid/os/IBinder;Landroid/view/Surface;ILandroid/os/IBinder;)V
        //     92: putfield 571	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
        //     95: aload_0
        //     96: getfield 571	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
        //     99: astore 20
        //     101: new 1960	android/os/Binder
        //     104: dup
        //     105: invokespecial 3396	android/os/Binder:<init>	()V
        //     108: astore 10
        //     110: aload 20
        //     112: aload 10
        //     114: putfield 3403	com/android/server/wm/DragState:mToken	Landroid/os/IBinder;
        //     117: aload_0
        //     118: getfield 526	com/android/server/wm/WindowManagerService:mH	Lcom/android/server/wm/WindowManagerService$H;
        //     121: bipush 20
        //     123: aload 19
        //     125: invokevirtual 2345	com/android/server/wm/WindowManagerService$H:removeMessages	(ILjava/lang/Object;)V
        //     128: aload_0
        //     129: getfield 526	com/android/server/wm/WindowManagerService:mH	Lcom/android/server/wm/WindowManagerService$H;
        //     132: bipush 20
        //     134: aload 19
        //     136: invokevirtual 1731	com/android/server/wm/WindowManagerService$H:obtainMessage	(ILjava/lang/Object;)Landroid/os/Message;
        //     139: astore 21
        //     141: aload_0
        //     142: getfield 526	com/android/server/wm/WindowManagerService:mH	Lcom/android/server/wm/WindowManagerService$H;
        //     145: aload 21
        //     147: ldc2_w 3390
        //     150: invokevirtual 2020	com/android/server/wm/WindowManagerService$H:sendMessageDelayed	(Landroid/os/Message;J)Z
        //     153: pop
        //     154: aload 10
        //     156: astore 16
        //     158: aload 12
        //     160: monitorexit
        //     161: lload 8
        //     163: invokestatic 2257	android/os/Binder:restoreCallingIdentity	(J)V
        //     166: aload 16
        //     168: areturn
        //     169: ldc 117
        //     171: ldc_w 3405
        //     174: invokestatic 1404	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     177: pop
        //     178: aconst_null
        //     179: astore 16
        //     181: goto -23 -> 158
        //     184: astore 15
        //     186: aload 10
        //     188: astore 16
        //     190: ldc 117
        //     192: new 1311	java/lang/StringBuilder
        //     195: dup
        //     196: invokespecial 1312	java/lang/StringBuilder:<init>	()V
        //     199: ldc_w 3407
        //     202: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     205: iload 4
        //     207: invokevirtual 1605	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     210: ldc_w 3213
        //     213: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     216: iload 5
        //     218: invokevirtual 1605	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     221: invokevirtual 1329	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     224: aload 15
        //     226: invokestatic 3215	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     229: pop
        //     230: aload_0
        //     231: getfield 571	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
        //     234: ifnull -76 -> 158
        //     237: aload_0
        //     238: getfield 571	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
        //     241: invokevirtual 3410	com/android/server/wm/DragState:reset	()V
        //     244: aload_0
        //     245: aconst_null
        //     246: putfield 571	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
        //     249: goto -91 -> 158
        //     252: aload 12
        //     254: monitorexit
        //     255: aload 13
        //     257: athrow
        //     258: astore 11
        //     260: lload 8
        //     262: invokestatic 2257	android/os/Binder:restoreCallingIdentity	(J)V
        //     265: aload 11
        //     267: athrow
        //     268: astore 11
        //     270: goto -10 -> 260
        //     273: astore 13
        //     275: aload 10
        //     277: pop
        //     278: goto -26 -> 252
        //     281: astore 15
        //     283: goto -93 -> 190
        //     286: astore 13
        //     288: goto -36 -> 252
        //
        // Exception table:
        //     from	to	target	type
        //     22	76	184	android/view/Surface$OutOfResourcesException
        //     117	154	184	android/view/Surface$OutOfResourcesException
        //     169	178	184	android/view/Surface$OutOfResourcesException
        //     255	258	258	finally
        //     13	22	268	finally
        //     22	76	273	finally
        //     117	154	273	finally
        //     169	178	273	finally
        //     76	117	281	android/view/Surface$OutOfResourcesException
        //     76	117	286	finally
        //     158	161	286	finally
        //     190	255	286	finally
    }

    public void rebootSafeMode()
    {
        ShutdownThread.rebootSafeMode(this.mContext, true);
    }

    final void rebuildAppWindowListLocked()
    {
        int i = this.mWindows.size();
        int j = -1;
        int k = 0;
        if (this.mRebuildTmp.length < i)
            this.mRebuildTmp = new WindowState[i + 10];
        int m = 0;
        while (m < i)
        {
            WindowState localWindowState2 = (WindowState)this.mWindows.get(m);
            if (localWindowState2.mAppToken != null)
            {
                WindowState localWindowState3 = (WindowState)this.mWindows.remove(m);
                localWindowState3.mRebuilding = true;
                this.mRebuildTmp[k] = localWindowState3;
                this.mWindowsChanged = true;
                i--;
                k++;
            }
            else
            {
                if ((localWindowState2.mAttrs.type == 2013) && (j == m - 1))
                    j = m;
                m++;
            }
        }
        int n = j + 1;
        int i1 = n;
        int i2 = this.mExitingAppTokens.size();
        for (int i3 = 0; i3 < i2; i3++)
            i1 = reAddAppWindowsLocked(i1, (WindowToken)this.mExitingAppTokens.get(i3));
        int i4 = this.mAnimatingAppTokens.size();
        for (int i5 = 0; i5 < i4; i5++)
            i1 = reAddAppWindowsLocked(i1, (WindowToken)this.mAnimatingAppTokens.get(i5));
        int i6 = i1 - n;
        if (i6 != k)
        {
            Slog.w("WindowManager", "Rebuild removed " + k + " windows but added " + i6);
            for (int i7 = 0; i7 < k; i7++)
            {
                WindowState localWindowState1 = this.mRebuildTmp[i7];
                if (localWindowState1.mRebuilding)
                {
                    StringWriter localStringWriter = new StringWriter();
                    PrintWriter localPrintWriter = new PrintWriter(localStringWriter);
                    localWindowState1.dump(localPrintWriter, "", true);
                    localPrintWriter.flush();
                    Slog.w("WindowManager", "This window was lost: " + localWindowState1);
                    Slog.w("WindowManager", localStringWriter.toString());
                    localWindowState1.mWinAnimator.destroySurfaceLocked();
                }
            }
            Slog.w("WindowManager", "Current app token list:");
            dumpAnimatingAppTokensLocked();
            Slog.w("WindowManager", "Final window list:");
            dumpWindowsLocked();
        }
    }

    // ERROR //
    boolean reclaimSomeSurfaceMemoryLocked(WindowStateAnimator paramWindowStateAnimator, String paramString, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_1
        //     1: getfield 3443	com/android/server/wm/WindowStateAnimator:mSurface	Landroid/view/Surface;
        //     4: astore 4
        //     6: iconst_0
        //     7: istore 5
        //     9: iconst_0
        //     10: istore 6
        //     12: iconst_3
        //     13: anewarray 427	java/lang/Object
        //     16: astore 7
        //     18: aload 7
        //     20: iconst_0
        //     21: aload_1
        //     22: getfield 3446	com/android/server/wm/WindowStateAnimator:mWin	Lcom/android/server/wm/WindowState;
        //     25: invokevirtual 3447	com/android/server/wm/WindowState:toString	()Ljava/lang/String;
        //     28: aastore
        //     29: aload 7
        //     31: iconst_1
        //     32: aload_1
        //     33: getfield 3448	com/android/server/wm/WindowStateAnimator:mSession	Lcom/android/server/wm/Session;
        //     36: getfield 1968	com/android/server/wm/Session:mPid	I
        //     39: invokestatic 3452	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     42: aastore
        //     43: aload 7
        //     45: iconst_2
        //     46: aload_2
        //     47: aastore
        //     48: sipush 31000
        //     51: aload 7
        //     53: invokestatic 3458	android/util/EventLog:writeEvent	(I[Ljava/lang/Object;)I
        //     56: pop
        //     57: aload_0
        //     58: getfield 1418	com/android/server/wm/WindowManagerService:mForceRemoves	Ljava/util/ArrayList;
        //     61: ifnonnull +14 -> 75
        //     64: aload_0
        //     65: new 382	java/util/ArrayList
        //     68: dup
        //     69: invokespecial 383	java/util/ArrayList:<init>	()V
        //     72: putfield 1418	com/android/server/wm/WindowManagerService:mForceRemoves	Ljava/util/ArrayList;
        //     75: invokestatic 2233	android/os/Binder:clearCallingIdentity	()J
        //     78: lstore 9
        //     80: aload_0
        //     81: getfield 395	com/android/server/wm/WindowManagerService:mWindows	Ljava/util/ArrayList;
        //     84: invokevirtual 806	java/util/ArrayList:size	()I
        //     87: istore 12
        //     89: ldc 117
        //     91: ldc_w 3460
        //     94: invokestatic 1338	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     97: pop
        //     98: iconst_0
        //     99: istore 14
        //     101: iload 14
        //     103: iload 12
        //     105: if_icmpge +290 -> 395
        //     108: aload_0
        //     109: getfield 395	com/android/server/wm/WindowManagerService:mWindows	Ljava/util/ArrayList;
        //     112: iload 14
        //     114: invokevirtual 813	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     117: checkcast 403	com/android/server/wm/WindowState
        //     120: astore 26
        //     122: aload 26
        //     124: getfield 959	com/android/server/wm/WindowState:mWinAnimator	Lcom/android/server/wm/WindowStateAnimator;
        //     127: astore 27
        //     129: aload 27
        //     131: getfield 3443	com/android/server/wm/WindowStateAnimator:mSurface	Landroid/view/Surface;
        //     134: ifnull +520 -> 654
        //     137: aload_0
        //     138: getfield 373	com/android/server/wm/WindowManagerService:mSessions	Ljava/util/HashSet;
        //     141: aload 27
        //     143: getfield 3448	com/android/server/wm/WindowStateAnimator:mSession	Lcom/android/server/wm/Session;
        //     146: invokevirtual 3461	java/util/HashSet:contains	(Ljava/lang/Object;)Z
        //     149: ifne +140 -> 289
        //     152: ldc 117
        //     154: new 1311	java/lang/StringBuilder
        //     157: dup
        //     158: invokespecial 1312	java/lang/StringBuilder:<init>	()V
        //     161: ldc_w 3463
        //     164: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     167: aload 26
        //     169: invokevirtual 1321	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     172: ldc_w 3465
        //     175: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     178: aload 27
        //     180: getfield 3443	com/android/server/wm/WindowStateAnimator:mSurface	Landroid/view/Surface;
        //     183: invokevirtual 1321	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     186: ldc_w 3467
        //     189: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     192: aload 26
        //     194: getfield 829	com/android/server/wm/WindowState:mToken	Lcom/android/server/wm/WindowToken;
        //     197: invokevirtual 1321	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     200: ldc_w 3469
        //     203: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     206: aload 26
        //     208: getfield 1238	com/android/server/wm/WindowState:mSession	Lcom/android/server/wm/Session;
        //     211: getfield 1968	com/android/server/wm/Session:mPid	I
        //     214: invokevirtual 1605	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     217: ldc_w 3471
        //     220: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     223: aload 26
        //     225: getfield 1238	com/android/server/wm/WindowState:mSession	Lcom/android/server/wm/Session;
        //     228: getfield 3474	com/android/server/wm/Session:mUid	I
        //     231: invokevirtual 1605	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     234: invokevirtual 1329	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     237: invokestatic 1404	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     240: pop
        //     241: aload 27
        //     243: getfield 3443	com/android/server/wm/WindowStateAnimator:mSurface	Landroid/view/Surface;
        //     246: invokevirtual 3477	android/view/Surface:destroy	()V
        //     249: aload 27
        //     251: iconst_0
        //     252: putfield 2348	com/android/server/wm/WindowStateAnimator:mSurfaceShown	Z
        //     255: aload 27
        //     257: aconst_null
        //     258: putfield 3443	com/android/server/wm/WindowStateAnimator:mSurface	Landroid/view/Surface;
        //     261: aload 26
        //     263: iconst_0
        //     264: putfield 1235	com/android/server/wm/WindowState:mHasSurface	Z
        //     267: aload_0
        //     268: getfield 1418	com/android/server/wm/WindowManagerService:mForceRemoves	Ljava/util/ArrayList;
        //     271: aload 26
        //     273: invokevirtual 861	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     276: pop
        //     277: iinc 14 255
        //     280: iinc 12 255
        //     283: iconst_1
        //     284: istore 5
        //     286: goto +368 -> 654
        //     289: aload 26
        //     291: getfield 856	com/android/server/wm/WindowState:mAppToken	Lcom/android/server/wm/AppWindowToken;
        //     294: ifnull +360 -> 654
        //     297: aload 26
        //     299: getfield 856	com/android/server/wm/WindowState:mAppToken	Lcom/android/server/wm/AppWindowToken;
        //     302: getfield 2251	com/android/server/wm/AppWindowToken:clientHidden	Z
        //     305: ifeq +349 -> 654
        //     308: ldc 117
        //     310: new 1311	java/lang/StringBuilder
        //     313: dup
        //     314: invokespecial 1312	java/lang/StringBuilder:<init>	()V
        //     317: ldc_w 3479
        //     320: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     323: aload 26
        //     325: invokevirtual 1321	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     328: ldc_w 3465
        //     331: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     334: aload 27
        //     336: getfield 3443	com/android/server/wm/WindowStateAnimator:mSurface	Landroid/view/Surface;
        //     339: invokevirtual 1321	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     342: ldc_w 3467
        //     345: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     348: aload 26
        //     350: getfield 856	com/android/server/wm/WindowState:mAppToken	Lcom/android/server/wm/AppWindowToken;
        //     353: invokevirtual 1321	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     356: invokevirtual 1329	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     359: invokestatic 1404	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     362: pop
        //     363: aload 27
        //     365: getfield 3443	com/android/server/wm/WindowStateAnimator:mSurface	Landroid/view/Surface;
        //     368: invokevirtual 3477	android/view/Surface:destroy	()V
        //     371: aload 27
        //     373: iconst_0
        //     374: putfield 2348	com/android/server/wm/WindowStateAnimator:mSurfaceShown	Z
        //     377: aload 27
        //     379: aconst_null
        //     380: putfield 3443	com/android/server/wm/WindowStateAnimator:mSurface	Landroid/view/Surface;
        //     383: aload 26
        //     385: iconst_0
        //     386: putfield 1235	com/android/server/wm/WindowState:mHasSurface	Z
        //     389: iconst_1
        //     390: istore 5
        //     392: goto +262 -> 654
        //     395: iload 5
        //     397: ifne +151 -> 548
        //     400: ldc 117
        //     402: ldc_w 3481
        //     405: invokestatic 1404	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     408: pop
        //     409: new 3483	android/util/SparseIntArray
        //     412: dup
        //     413: invokespecial 3484	android/util/SparseIntArray:<init>	()V
        //     416: astore 19
        //     418: iconst_0
        //     419: istore 20
        //     421: iload 20
        //     423: iload 12
        //     425: if_icmpge +52 -> 477
        //     428: aload_0
        //     429: getfield 395	com/android/server/wm/WindowManagerService:mWindows	Ljava/util/ArrayList;
        //     432: iload 20
        //     434: invokevirtual 813	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     437: checkcast 403	com/android/server/wm/WindowState
        //     440: getfield 959	com/android/server/wm/WindowState:mWinAnimator	Lcom/android/server/wm/WindowStateAnimator;
        //     443: astore 25
        //     445: aload 25
        //     447: getfield 3443	com/android/server/wm/WindowStateAnimator:mSurface	Landroid/view/Surface;
        //     450: ifnull +210 -> 660
        //     453: aload 19
        //     455: aload 25
        //     457: getfield 3448	com/android/server/wm/WindowStateAnimator:mSession	Lcom/android/server/wm/Session;
        //     460: getfield 1968	com/android/server/wm/Session:mPid	I
        //     463: aload 25
        //     465: getfield 3448	com/android/server/wm/WindowStateAnimator:mSession	Lcom/android/server/wm/Session;
        //     468: getfield 1968	com/android/server/wm/Session:mPid	I
        //     471: invokevirtual 3486	android/util/SparseIntArray:append	(II)V
        //     474: goto +186 -> 660
        //     477: aload 19
        //     479: invokevirtual 3487	android/util/SparseIntArray:size	()I
        //     482: ifle +66 -> 548
        //     485: aload 19
        //     487: invokevirtual 3487	android/util/SparseIntArray:size	()I
        //     490: newarray int
        //     492: astore 21
        //     494: iconst_0
        //     495: istore 22
        //     497: iload 22
        //     499: aload 21
        //     501: arraylength
        //     502: if_icmpge +21 -> 523
        //     505: aload 21
        //     507: iload 22
        //     509: aload 19
        //     511: iload 22
        //     513: invokevirtual 3490	android/util/SparseIntArray:keyAt	(I)I
        //     516: iastore
        //     517: iinc 22 1
        //     520: goto -23 -> 497
        //     523: aload_0
        //     524: getfield 673	com/android/server/wm/WindowManagerService:mActivityManager	Landroid/app/IActivityManager;
        //     527: aload 21
        //     529: ldc_w 3492
        //     532: iload_3
        //     533: invokeinterface 3496 4 0
        //     538: istore 24
        //     540: iload 24
        //     542: ifeq +6 -> 548
        //     545: iconst_1
        //     546: istore 6
        //     548: iload 5
        //     550: ifne +8 -> 558
        //     553: iload 6
        //     555: ifeq +52 -> 607
        //     558: ldc 117
        //     560: ldc_w 3498
        //     563: invokestatic 1404	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     566: pop
        //     567: aload 4
        //     569: ifnull +26 -> 595
        //     572: aload 4
        //     574: invokevirtual 3477	android/view/Surface:destroy	()V
        //     577: aload_1
        //     578: iconst_0
        //     579: putfield 2348	com/android/server/wm/WindowStateAnimator:mSurfaceShown	Z
        //     582: aload_1
        //     583: aconst_null
        //     584: putfield 3443	com/android/server/wm/WindowStateAnimator:mSurface	Landroid/view/Surface;
        //     587: aload_1
        //     588: getfield 3446	com/android/server/wm/WindowStateAnimator:mWin	Lcom/android/server/wm/WindowState;
        //     591: iconst_0
        //     592: putfield 1235	com/android/server/wm/WindowState:mHasSurface	Z
        //     595: aload_1
        //     596: getfield 3446	com/android/server/wm/WindowStateAnimator:mWin	Lcom/android/server/wm/WindowState;
        //     599: getfield 825	com/android/server/wm/WindowState:mClient	Landroid/view/IWindow;
        //     602: invokeinterface 3501 1 0
        //     607: lload 9
        //     609: invokestatic 2257	android/os/Binder:restoreCallingIdentity	(J)V
        //     612: iload 5
        //     614: ifne +8 -> 622
        //     617: iload 6
        //     619: ifeq +19 -> 638
        //     622: iconst_1
        //     623: istore 17
        //     625: iload 17
        //     627: ireturn
        //     628: astore 11
        //     630: lload 9
        //     632: invokestatic 2257	android/os/Binder:restoreCallingIdentity	(J)V
        //     635: aload 11
        //     637: athrow
        //     638: iconst_0
        //     639: istore 17
        //     641: goto -16 -> 625
        //     644: astore 16
        //     646: goto -39 -> 607
        //     649: astore 23
        //     651: goto -103 -> 548
        //     654: iinc 14 1
        //     657: goto -556 -> 101
        //     660: iinc 20 1
        //     663: goto -242 -> 421
        //
        // Exception table:
        //     from	to	target	type
        //     80	517	628	finally
        //     523	540	628	finally
        //     558	595	628	finally
        //     595	607	628	finally
        //     595	607	644	android/os/RemoteException
        //     523	540	649	android/os/RemoteException
    }

    public void reenableKeyguard(IBinder paramIBinder)
    {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.DISABLE_KEYGUARD") != 0)
            throw new SecurityException("Requires DISABLE_KEYGUARD permission");
        synchronized (this.mKeyguardTokenWatcher)
        {
            this.mKeyguardTokenWatcher.release(paramIBinder);
            if (!this.mKeyguardTokenWatcher.isAcquired())
                while (true)
                {
                    boolean bool = this.mKeyguardDisabled;
                    if (bool)
                        try
                        {
                            this.mKeyguardTokenWatcher.wait();
                        }
                        catch (InterruptedException localInterruptedException)
                        {
                            Thread.currentThread().interrupt();
                        }
                }
        }
    }

    public void reevaluateStatusBarVisibility()
    {
        synchronized (this.mWindowMap)
        {
            updateStatusBarVisibilityLocked(this.mPolicy.adjustSystemUiVisibilityLw(this.mLastStatusBarVisibility));
            performLayoutAndPlaceSurfacesLocked();
            return;
        }
    }

    // ERROR //
    public int relayoutWindow(Session paramSession, IWindow paramIWindow, int paramInt1, WindowManager.LayoutParams paramLayoutParams, int paramInt2, int paramInt3, int paramInt4, int paramInt5, Rect paramRect1, Rect paramRect2, Rect paramRect3, Configuration paramConfiguration, Surface paramSurface)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore 14
        //     3: iconst_0
        //     4: istore 15
        //     6: iconst_0
        //     7: istore 16
        //     9: aload 4
        //     11: ifnull +46 -> 57
        //     14: aload 4
        //     16: getfield 3528	android/view/WindowManager$LayoutParams:systemUiVisibility	I
        //     19: aload 4
        //     21: getfield 3531	android/view/WindowManager$LayoutParams:subtreeSystemUiVisibility	I
        //     24: ior
        //     25: istore 16
        //     27: ldc_w 3532
        //     30: iload 16
        //     32: iand
        //     33: ifeq +24 -> 57
        //     36: aload_0
        //     37: getfield 596	com/android/server/wm/WindowManagerService:mContext	Landroid/content/Context;
        //     40: ldc_w 3534
        //     43: invokevirtual 2537	android/content/Context:checkCallingOrSelfPermission	(Ljava/lang/String;)I
        //     46: ifeq +11 -> 57
        //     49: iload 16
        //     51: ldc_w 3535
        //     54: iand
        //     55: istore 16
        //     57: invokestatic 2233	android/os/Binder:clearCallingIdentity	()J
        //     60: lstore 17
        //     62: aload_0
        //     63: getfield 378	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
        //     66: astore 19
        //     68: aload 19
        //     70: monitorenter
        //     71: aload_0
        //     72: aload_1
        //     73: aload_2
        //     74: iconst_0
        //     75: invokevirtual 3043	com/android/server/wm/WindowManagerService:windowForClientLocked	(Lcom/android/server/wm/Session;Landroid/view/IWindow;Z)Lcom/android/server/wm/WindowState;
        //     78: astore 21
        //     80: aload 21
        //     82: ifnonnull +12 -> 94
        //     85: iconst_0
        //     86: istore 45
        //     88: aload 19
        //     90: monitorexit
        //     91: goto +1401 -> 1492
        //     94: aload 21
        //     96: getfield 959	com/android/server/wm/WindowState:mWinAnimator	Lcom/android/server/wm/WindowStateAnimator;
        //     99: astore 22
        //     101: aload 21
        //     103: getfield 3538	com/android/server/wm/WindowState:mRequestedWidth	I
        //     106: iload 5
        //     108: if_icmpne +13 -> 121
        //     111: aload 21
        //     113: getfield 3541	com/android/server/wm/WindowState:mRequestedHeight	I
        //     116: iload 6
        //     118: if_icmpeq +23 -> 141
        //     121: aload 21
        //     123: iconst_1
        //     124: putfield 1772	com/android/server/wm/WindowState:mLayoutNeeded	Z
        //     127: aload 21
        //     129: iload 5
        //     131: putfield 3538	com/android/server/wm/WindowState:mRequestedWidth	I
        //     134: aload 21
        //     136: iload 6
        //     138: putfield 3541	com/android/server/wm/WindowState:mRequestedHeight	I
        //     141: aload 4
        //     143: ifnull +19 -> 162
        //     146: iload_3
        //     147: aload 21
        //     149: getfield 3544	com/android/server/wm/WindowState:mSeq	I
        //     152: if_icmpne +10 -> 162
        //     155: aload 21
        //     157: iload 16
        //     159: putfield 3547	com/android/server/wm/WindowState:mSystemUiVisibility	I
        //     162: aload 4
        //     164: ifnull +1331 -> 1495
        //     167: aload_0
        //     168: getfield 368	com/android/server/wm/WindowManagerService:mPolicy	Landroid/view/WindowManagerPolicy;
        //     171: aload 4
        //     173: invokeinterface 2195 2 0
        //     178: goto +1317 -> 1495
        //     181: aload 22
        //     183: iload 23
        //     185: putfield 3550	com/android/server/wm/WindowStateAnimator:mSurfaceDestroyDeferred	Z
        //     188: iconst_0
        //     189: istore 24
        //     191: iconst_0
        //     192: istore 25
        //     194: aload 4
        //     196: ifnull +102 -> 298
        //     199: aload 21
        //     201: getfield 844	com/android/server/wm/WindowState:mAttrs	Landroid/view/WindowManager$LayoutParams;
        //     204: getfield 849	android/view/WindowManager$LayoutParams:type	I
        //     207: aload 4
        //     209: getfield 849	android/view/WindowManager$LayoutParams:type	I
        //     212: if_icmpeq +32 -> 244
        //     215: new 2492	java/lang/IllegalArgumentException
        //     218: dup
        //     219: ldc_w 3552
        //     222: invokespecial 3056	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
        //     225: astore 56
        //     227: aload 56
        //     229: athrow
        //     230: astore 20
        //     232: aload 19
        //     234: monitorexit
        //     235: aload 20
        //     237: athrow
        //     238: iconst_0
        //     239: istore 23
        //     241: goto -60 -> 181
        //     244: aload 21
        //     246: getfield 844	com/android/server/wm/WindowState:mAttrs	Landroid/view/WindowManager$LayoutParams;
        //     249: astore 57
        //     251: aload 57
        //     253: getfield 996	android/view/WindowManager$LayoutParams:flags	I
        //     256: aload 4
        //     258: getfield 996	android/view/WindowManager$LayoutParams:flags	I
        //     261: ixor
        //     262: istore 25
        //     264: aload 57
        //     266: iload 25
        //     268: putfield 996	android/view/WindowManager$LayoutParams:flags	I
        //     271: aload 21
        //     273: getfield 844	com/android/server/wm/WindowState:mAttrs	Landroid/view/WindowManager$LayoutParams;
        //     276: aload 4
        //     278: invokevirtual 3554	android/view/WindowManager$LayoutParams:copyFrom	(Landroid/view/WindowManager$LayoutParams;)I
        //     281: istore 24
        //     283: iload 24
        //     285: sipush 8193
        //     288: iand
        //     289: ifeq +9 -> 298
        //     292: aload 21
        //     294: iconst_1
        //     295: putfield 1772	com/android/server/wm/WindowState:mLayoutNeeded	Z
        //     298: ldc_w 3555
        //     301: aload 21
        //     303: getfield 844	com/android/server/wm/WindowState:mAttrs	Landroid/view/WindowManager$LayoutParams;
        //     306: getfield 996	android/view/WindowManager$LayoutParams:flags	I
        //     309: iand
        //     310: ifeq +850 -> 1160
        //     313: iconst_1
        //     314: istore 26
        //     316: aload 21
        //     318: iload 26
        //     320: putfield 3558	com/android/server/wm/WindowState:mEnforceSizeCompat	Z
        //     323: iload 24
        //     325: sipush 128
        //     328: iand
        //     329: ifeq +13 -> 342
        //     332: aload 22
        //     334: aload 4
        //     336: getfield 3561	android/view/WindowManager$LayoutParams:alpha	F
        //     339: putfield 3564	com/android/server/wm/WindowStateAnimator:mAlpha	F
        //     342: sipush 16384
        //     345: aload 21
        //     347: getfield 844	com/android/server/wm/WindowState:mAttrs	Landroid/view/WindowManager$LayoutParams;
        //     350: getfield 996	android/view/WindowManager$LayoutParams:flags	I
        //     353: iand
        //     354: ifeq +812 -> 1166
        //     357: iconst_1
        //     358: istore 27
        //     360: iload 27
        //     362: ifeq +822 -> 1184
        //     365: aload 4
        //     367: getfield 3566	android/view/WindowManager$LayoutParams:width	I
        //     370: iload 5
        //     372: if_icmpeq +800 -> 1172
        //     375: aload 4
        //     377: getfield 3566	android/view/WindowManager$LayoutParams:width	I
        //     380: i2f
        //     381: iload 5
        //     383: i2f
        //     384: fdiv
        //     385: fstore 54
        //     387: aload 21
        //     389: fload 54
        //     391: putfield 3569	com/android/server/wm/WindowState:mHScale	F
        //     394: aload 4
        //     396: getfield 3571	android/view/WindowManager$LayoutParams:height	I
        //     399: iload 6
        //     401: if_icmpeq +777 -> 1178
        //     404: aload 4
        //     406: getfield 3571	android/view/WindowManager$LayoutParams:height	I
        //     409: i2f
        //     410: iload 6
        //     412: i2f
        //     413: fdiv
        //     414: fstore 55
        //     416: aload 21
        //     418: fload 55
        //     420: putfield 3574	com/android/server/wm/WindowState:mVScale	F
        //     423: goto +1085 -> 1508
        //     426: aload 21
        //     428: getfield 1786	com/android/server/wm/WindowState:mViewVisibility	I
        //     431: iload 7
        //     433: if_icmpne +1090 -> 1523
        //     436: iload 25
        //     438: bipush 8
        //     440: iand
        //     441: ifne +1082 -> 1523
        //     444: aload 21
        //     446: getfield 1789	com/android/server/wm/WindowState:mRelayoutCalled	Z
        //     449: ifne +756 -> 1205
        //     452: goto +1071 -> 1523
        //     455: aload 21
        //     457: getfield 1786	com/android/server/wm/WindowState:mViewVisibility	I
        //     460: iload 7
        //     462: if_icmpeq +749 -> 1211
        //     465: ldc_w 1542
        //     468: aload 21
        //     470: getfield 844	com/android/server/wm/WindowState:mAttrs	Landroid/view/WindowManager$LayoutParams;
        //     473: getfield 996	android/view/WindowManager$LayoutParams:flags	I
        //     476: iand
        //     477: ifeq +734 -> 1211
        //     480: iconst_1
        //     481: istore 30
        //     483: goto +1046 -> 1529
        //     486: iload 30
        //     488: iload 31
        //     490: ior
        //     491: istore 32
        //     493: aload 21
        //     495: iconst_1
        //     496: putfield 1789	com/android/server/wm/WindowState:mRelayoutCalled	Z
        //     499: aload 21
        //     501: getfield 1786	com/android/server/wm/WindowState:mViewVisibility	I
        //     504: istore 33
        //     506: aload 21
        //     508: iload 7
        //     510: putfield 1786	com/android/server/wm/WindowState:mViewVisibility	I
        //     513: iload 7
        //     515: ifne +797 -> 1312
        //     518: aload 21
        //     520: getfield 856	com/android/server/wm/WindowState:mAppToken	Lcom/android/server/wm/AppWindowToken;
        //     523: ifnull +14 -> 537
        //     526: aload 21
        //     528: getfield 856	com/android/server/wm/WindowState:mAppToken	Lcom/android/server/wm/AppWindowToken;
        //     531: getfield 2251	com/android/server/wm/AppWindowToken:clientHidden	Z
        //     534: ifne +778 -> 1312
        //     537: aload 21
        //     539: invokevirtual 1507	com/android/server/wm/WindowState:isVisibleLw	()Z
        //     542: ifne +681 -> 1223
        //     545: iconst_1
        //     546: istore 14
        //     548: aload 21
        //     550: getfield 1286	com/android/server/wm/WindowState:mExiting	Z
        //     553: ifeq +14 -> 567
        //     556: aload 22
        //     558: invokevirtual 3577	com/android/server/wm/WindowStateAnimator:cancelExitAnimationForNextAnimationLocked	()V
        //     561: aload 21
        //     563: iconst_0
        //     564: putfield 1286	com/android/server/wm/WindowState:mExiting	Z
        //     567: aload 21
        //     569: getfield 1578	com/android/server/wm/WindowState:mDestroying	Z
        //     572: ifeq +19 -> 591
        //     575: aload 21
        //     577: iconst_0
        //     578: putfield 1578	com/android/server/wm/WindowState:mDestroying	Z
        //     581: aload_0
        //     582: getfield 407	com/android/server/wm/WindowManagerService:mDestroySurface	Ljava/util/ArrayList;
        //     585: aload 21
        //     587: invokevirtual 1704	java/util/ArrayList:remove	(Ljava/lang/Object;)Z
        //     590: pop
        //     591: iload 33
        //     593: bipush 8
        //     595: if_icmpne +9 -> 604
        //     598: aload 22
        //     600: iconst_1
        //     601: putfield 2244	com/android/server/wm/WindowStateAnimator:mEnterAnimationPending	Z
        //     604: iload 14
        //     606: ifeq +97 -> 703
        //     609: aload 21
        //     611: invokevirtual 1584	com/android/server/wm/WindowState:isDrawnLw	()Z
        //     614: ifeq +15 -> 629
        //     617: aload_0
        //     618: invokevirtual 916	com/android/server/wm/WindowManagerService:okToDisplay	()Z
        //     621: ifeq +8 -> 629
        //     624: aload 22
        //     626: invokevirtual 3580	com/android/server/wm/WindowStateAnimator:applyEnterAnimationLocked	()V
        //     629: ldc_w 3581
        //     632: aload 21
        //     634: getfield 844	com/android/server/wm/WindowState:mAttrs	Landroid/view/WindowManager$LayoutParams;
        //     637: getfield 996	android/view/WindowManager$LayoutParams:flags	I
        //     640: iand
        //     641: ifeq +9 -> 650
        //     644: aload 21
        //     646: iconst_1
        //     647: putfield 3582	com/android/server/wm/WindowState:mTurnOnScreen	Z
        //     650: aload 21
        //     652: getfield 1639	com/android/server/wm/WindowState:mConfiguration	Landroid/content/res/Configuration;
        //     655: aload_0
        //     656: getfield 494	com/android/server/wm/WindowManagerService:mCurConfiguration	Landroid/content/res/Configuration;
        //     659: if_acmpeq +44 -> 703
        //     662: aload 21
        //     664: getfield 1639	com/android/server/wm/WindowState:mConfiguration	Landroid/content/res/Configuration;
        //     667: ifnull +18 -> 685
        //     670: aload_0
        //     671: getfield 494	com/android/server/wm/WindowManagerService:mCurConfiguration	Landroid/content/res/Configuration;
        //     674: aload 21
        //     676: getfield 1639	com/android/server/wm/WindowState:mConfiguration	Landroid/content/res/Configuration;
        //     679: invokevirtual 1643	android/content/res/Configuration:diff	(Landroid/content/res/Configuration;)I
        //     682: ifeq +21 -> 703
        //     685: aload 21
        //     687: aload_0
        //     688: getfield 494	com/android/server/wm/WindowManagerService:mCurConfiguration	Landroid/content/res/Configuration;
        //     691: putfield 1639	com/android/server/wm/WindowState:mConfiguration	Landroid/content/res/Configuration;
        //     694: aload 12
        //     696: aload_0
        //     697: getfield 494	com/android/server/wm/WindowManagerService:mCurConfiguration	Landroid/content/res/Configuration;
        //     700: invokevirtual 3584	android/content/res/Configuration:setTo	(Landroid/content/res/Configuration;)V
        //     703: iload 24
        //     705: bipush 8
        //     707: iand
        //     708: ifeq +14 -> 722
        //     711: aload 22
        //     713: invokevirtual 1696	com/android/server/wm/WindowStateAnimator:destroySurfaceLocked	()V
        //     716: iconst_1
        //     717: istore 14
        //     719: iconst_1
        //     720: istore 15
        //     722: aload 21
        //     724: getfield 1235	com/android/server/wm/WindowState:mHasSurface	Z
        //     727: ifne +6 -> 733
        //     730: iconst_1
        //     731: istore 15
        //     733: aload 22
        //     735: invokevirtual 3588	com/android/server/wm/WindowStateAnimator:createSurfaceLocked	()Landroid/view/Surface;
        //     738: astore 51
        //     740: aload 51
        //     742: ifnull +487 -> 1229
        //     745: aload 13
        //     747: aload 51
        //     749: invokevirtual 3174	android/view/Surface:copyFrom	(Landroid/view/Surface;)V
        //     752: iload 14
        //     754: ifeq +6 -> 760
        //     757: iconst_1
        //     758: istore 29
        //     760: aload 21
        //     762: getfield 844	com/android/server/wm/WindowState:mAttrs	Landroid/view/WindowManager$LayoutParams;
        //     765: getfield 849	android/view/WindowManager$LayoutParams:type	I
        //     768: sipush 2011
        //     771: if_icmpne +19 -> 790
        //     774: aload_0
        //     775: getfield 542	com/android/server/wm/WindowManagerService:mInputMethodWindow	Lcom/android/server/wm/WindowState;
        //     778: ifnonnull +12 -> 790
        //     781: aload_0
        //     782: aload 21
        //     784: putfield 542	com/android/server/wm/WindowManagerService:mInputMethodWindow	Lcom/android/server/wm/WindowState;
        //     787: iconst_1
        //     788: istore 28
        //     790: aload 21
        //     792: getfield 844	com/android/server/wm/WindowState:mAttrs	Landroid/view/WindowManager$LayoutParams;
        //     795: getfield 849	android/view/WindowManager$LayoutParams:type	I
        //     798: iconst_1
        //     799: if_icmpne +62 -> 861
        //     802: aload 21
        //     804: getfield 856	com/android/server/wm/WindowState:mAppToken	Lcom/android/server/wm/AppWindowToken;
        //     807: ifnull +54 -> 861
        //     810: aload 21
        //     812: getfield 856	com/android/server/wm/WindowState:mAppToken	Lcom/android/server/wm/AppWindowToken;
        //     815: getfield 864	com/android/server/wm/AppWindowToken:startingWindow	Lcom/android/server/wm/WindowState;
        //     818: ifnull +43 -> 861
        //     821: aload 21
        //     823: getfield 856	com/android/server/wm/WindowState:mAppToken	Lcom/android/server/wm/AppWindowToken;
        //     826: getfield 864	com/android/server/wm/AppWindowToken:startingWindow	Lcom/android/server/wm/WindowState;
        //     829: getfield 844	com/android/server/wm/WindowState:mAttrs	Landroid/view/WindowManager$LayoutParams;
        //     832: astore 52
        //     834: aload 52
        //     836: ldc_w 3589
        //     839: aload 52
        //     841: getfield 996	android/view/WindowManager$LayoutParams:flags	I
        //     844: iand
        //     845: ldc_w 3590
        //     848: aload 21
        //     850: getfield 844	com/android/server/wm/WindowState:mAttrs	Landroid/view/WindowManager$LayoutParams;
        //     853: getfield 996	android/view/WindowManager$LayoutParams:flags	I
        //     856: iand
        //     857: ior
        //     858: putfield 996	android/view/WindowManager$LayoutParams:flags	I
        //     861: iload 29
        //     863: ifeq +15 -> 878
        //     866: aload_0
        //     867: iconst_3
        //     868: iconst_0
        //     869: invokespecial 1366	com/android/server/wm/WindowManagerService:updateFocusedWindowLocked	(IZ)Z
        //     872: ifeq +6 -> 878
        //     875: iconst_0
        //     876: istore 28
        //     878: iconst_0
        //     879: istore 34
        //     881: iload 28
        //     883: ifeq +19 -> 902
        //     886: aload_0
        //     887: iconst_0
        //     888: invokevirtual 1222	com/android/server/wm/WindowManagerService:moveInputMethodWindowsIfNeededLocked	(Z)Z
        //     891: ifne +653 -> 1544
        //     894: iload 14
        //     896: ifeq +6 -> 902
        //     899: goto +645 -> 1544
        //     902: iload 32
        //     904: ifeq +15 -> 919
        //     907: iconst_2
        //     908: aload_0
        //     909: invokevirtual 911	com/android/server/wm/WindowManagerService:adjustWallpaperWindowsLocked	()I
        //     912: iand
        //     913: ifeq +6 -> 919
        //     916: iconst_1
        //     917: istore 34
        //     919: aload_0
        //     920: iconst_1
        //     921: putfield 473	com/android/server/wm/WindowManagerService:mLayoutNeeded	Z
        //     924: iload 8
        //     926: iconst_1
        //     927: iand
        //     928: ifeq +534 -> 1462
        //     931: iconst_1
        //     932: istore 35
        //     934: aload 21
        //     936: iload 35
        //     938: putfield 2239	com/android/server/wm/WindowState:mGivenInsetsPending	Z
        //     941: iload 34
        //     943: ifeq +7 -> 950
        //     946: aload_0
        //     947: invokespecial 1368	com/android/server/wm/WindowManagerService:assignLayersLocked	()V
        //     950: aload_0
        //     951: iconst_0
        //     952: invokevirtual 1593	com/android/server/wm/WindowManagerService:updateOrientationFromAppTokensLocked	(Z)Z
        //     955: istore 36
        //     957: aload_0
        //     958: invokespecial 794	com/android/server/wm/WindowManagerService:performLayoutAndPlaceSurfacesLocked	()V
        //     961: iload 14
        //     963: ifeq +27 -> 990
        //     966: aload 21
        //     968: getfield 968	com/android/server/wm/WindowState:mIsWallpaper	Z
        //     971: ifeq +19 -> 990
        //     974: aload_0
        //     975: aload 21
        //     977: aload_0
        //     978: getfield 444	com/android/server/wm/WindowManagerService:mAppDisplayWidth	I
        //     981: aload_0
        //     982: getfield 446	com/android/server/wm/WindowManagerService:mAppDisplayHeight	I
        //     985: iconst_0
        //     986: invokevirtual 2302	com/android/server/wm/WindowManagerService:updateWallpaperOffsetLocked	(Lcom/android/server/wm/WindowState;IIZ)Z
        //     989: pop
        //     990: aload 21
        //     992: getfield 856	com/android/server/wm/WindowState:mAppToken	Lcom/android/server/wm/AppWindowToken;
        //     995: ifnull +11 -> 1006
        //     998: aload 21
        //     1000: getfield 856	com/android/server/wm/WindowState:mAppToken	Lcom/android/server/wm/AppWindowToken;
        //     1003: invokevirtual 1907	com/android/server/wm/AppWindowToken:updateReportedVisibilityLocked	()V
        //     1006: aload 9
        //     1008: aload 21
        //     1010: getfield 3593	com/android/server/wm/WindowState:mCompatFrame	Landroid/graphics/Rect;
        //     1013: invokevirtual 2060	android/graphics/Rect:set	(Landroid/graphics/Rect;)V
        //     1016: aload 10
        //     1018: aload 21
        //     1020: getfield 2052	com/android/server/wm/WindowState:mContentInsets	Landroid/graphics/Rect;
        //     1023: invokevirtual 2060	android/graphics/Rect:set	(Landroid/graphics/Rect;)V
        //     1026: aload 11
        //     1028: aload 21
        //     1030: getfield 2056	com/android/server/wm/WindowState:mVisibleInsets	Landroid/graphics/Rect;
        //     1033: invokevirtual 2060	android/graphics/Rect:set	(Landroid/graphics/Rect;)V
        //     1036: aload_0
        //     1037: getfield 581	com/android/server/wm/WindowManagerService:mInTouchMode	Z
        //     1040: istore 37
        //     1042: aload_0
        //     1043: getfield 729	com/android/server/wm/WindowManagerService:mAnimator	Lcom/android/server/wm/WindowAnimator;
        //     1046: getfield 1624	com/android/server/wm/WindowAnimator:mAnimating	Z
        //     1049: istore 38
        //     1051: iload 38
        //     1053: ifeq +25 -> 1078
        //     1056: aload_0
        //     1057: getfield 413	com/android/server/wm/WindowManagerService:mRelayoutWhileAnimating	Ljava/util/ArrayList;
        //     1060: aload 21
        //     1062: invokevirtual 1707	java/util/ArrayList:contains	(Ljava/lang/Object;)Z
        //     1065: ifne +13 -> 1078
        //     1068: aload_0
        //     1069: getfield 413	com/android/server/wm/WindowManagerService:mRelayoutWhileAnimating	Ljava/util/ArrayList;
        //     1072: aload 21
        //     1074: invokevirtual 861	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     1077: pop
        //     1078: aload_0
        //     1079: getfield 592	com/android/server/wm/WindowManagerService:mInputMonitor	Lcom/android/server/wm/InputMonitor;
        //     1082: iconst_1
        //     1083: invokevirtual 1371	com/android/server/wm/InputMonitor:updateInputWindowsLw	(Z)V
        //     1086: aload 19
        //     1088: monitorexit
        //     1089: iload 36
        //     1091: ifeq +7 -> 1098
        //     1094: aload_0
        //     1095: invokevirtual 2254	com/android/server/wm/WindowManagerService:sendNewConfiguration	()V
        //     1098: lload 17
        //     1100: invokestatic 2257	android/os/Binder:restoreCallingIdentity	(J)V
        //     1103: iload 37
        //     1105: ifeq +363 -> 1468
        //     1108: iconst_1
        //     1109: istore 39
        //     1111: iload 14
        //     1113: ifeq +361 -> 1474
        //     1116: iconst_2
        //     1117: istore 40
        //     1119: iload 39
        //     1121: iload 40
        //     1123: ior
        //     1124: istore 41
        //     1126: iload 15
        //     1128: ifeq +352 -> 1480
        //     1131: iconst_4
        //     1132: istore 42
        //     1134: iload 41
        //     1136: iload 42
        //     1138: ior
        //     1139: istore 43
        //     1141: iload 38
        //     1143: ifeq +343 -> 1486
        //     1146: bipush 8
        //     1148: istore 44
        //     1150: iload 44
        //     1152: iload 43
        //     1154: ior
        //     1155: istore 45
        //     1157: goto +335 -> 1492
        //     1160: iconst_0
        //     1161: istore 26
        //     1163: goto -847 -> 316
        //     1166: iconst_0
        //     1167: istore 27
        //     1169: goto -809 -> 360
        //     1172: fconst_1
        //     1173: fstore 54
        //     1175: goto -788 -> 387
        //     1178: fconst_1
        //     1179: fstore 55
        //     1181: goto -765 -> 416
        //     1184: aload 21
        //     1186: fconst_1
        //     1187: putfield 3574	com/android/server/wm/WindowState:mVScale	F
        //     1190: aload 21
        //     1192: fconst_1
        //     1193: putfield 3569	com/android/server/wm/WindowState:mHScale	F
        //     1196: goto +312 -> 1508
        //     1199: iconst_0
        //     1200: istore 28
        //     1202: goto -776 -> 426
        //     1205: iconst_0
        //     1206: istore 29
        //     1208: goto -753 -> 455
        //     1211: iconst_0
        //     1212: istore 30
        //     1214: goto +315 -> 1529
        //     1217: iconst_0
        //     1218: istore 31
        //     1220: goto -734 -> 486
        //     1223: iconst_0
        //     1224: istore 14
        //     1226: goto -678 -> 548
        //     1229: aload 13
        //     1231: invokevirtual 3189	android/view/Surface:release	()V
        //     1234: goto -482 -> 752
        //     1237: astore 49
        //     1239: aload_0
        //     1240: getfield 592	com/android/server/wm/WindowManagerService:mInputMonitor	Lcom/android/server/wm/InputMonitor;
        //     1243: iconst_1
        //     1244: invokevirtual 1371	com/android/server/wm/InputMonitor:updateInputWindowsLw	(Z)V
        //     1247: ldc 117
        //     1249: new 1311	java/lang/StringBuilder
        //     1252: dup
        //     1253: invokespecial 1312	java/lang/StringBuilder:<init>	()V
        //     1256: ldc_w 3595
        //     1259: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1262: aload_2
        //     1263: invokevirtual 1321	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     1266: ldc_w 1877
        //     1269: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1272: aload 21
        //     1274: getfield 844	com/android/server/wm/WindowState:mAttrs	Landroid/view/WindowManager$LayoutParams;
        //     1277: invokevirtual 2814	android/view/WindowManager$LayoutParams:getTitle	()Ljava/lang/CharSequence;
        //     1280: invokevirtual 1321	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     1283: ldc_w 1879
        //     1286: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     1289: invokevirtual 1329	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     1292: aload 49
        //     1294: invokestatic 2093	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     1297: pop
        //     1298: lload 17
        //     1300: invokestatic 2257	android/os/Binder:restoreCallingIdentity	(J)V
        //     1303: iconst_0
        //     1304: istore 45
        //     1306: aload 19
        //     1308: monitorexit
        //     1309: goto +183 -> 1492
        //     1312: aload 22
        //     1314: iconst_0
        //     1315: putfield 2244	com/android/server/wm/WindowStateAnimator:mEnterAnimationPending	Z
        //     1318: aload 22
        //     1320: getfield 3443	com/android/server/wm/WindowStateAnimator:mSurface	Landroid/view/Surface;
        //     1323: ifnull +62 -> 1385
        //     1326: aload 21
        //     1328: getfield 1286	com/android/server/wm/WindowState:mExiting	Z
        //     1331: ifne +54 -> 1385
        //     1334: iconst_1
        //     1335: istore 15
        //     1337: sipush 8194
        //     1340: istore 48
        //     1342: aload 21
        //     1344: getfield 844	com/android/server/wm/WindowState:mAttrs	Landroid/view/WindowManager$LayoutParams;
        //     1347: getfield 849	android/view/WindowManager$LayoutParams:type	I
        //     1350: iconst_3
        //     1351: if_icmpne +6 -> 1357
        //     1354: iconst_5
        //     1355: istore 48
        //     1357: aload 21
        //     1359: invokevirtual 3598	com/android/server/wm/WindowState:isWinVisibleLw	()Z
        //     1362: ifeq +31 -> 1393
        //     1365: aload 22
        //     1367: iload 48
        //     1369: iconst_0
        //     1370: invokevirtual 3600	com/android/server/wm/WindowStateAnimator:applyAnimationLocked	(IZ)Z
        //     1373: ifeq +20 -> 1393
        //     1376: iconst_1
        //     1377: istore 29
        //     1379: aload 21
        //     1381: iconst_1
        //     1382: putfield 1286	com/android/server/wm/WindowState:mExiting	Z
        //     1385: aload 13
        //     1387: invokevirtual 3189	android/view/Surface:release	()V
        //     1390: goto -529 -> 861
        //     1393: aload 21
        //     1395: getfield 959	com/android/server/wm/WindowState:mWinAnimator	Lcom/android/server/wm/WindowStateAnimator;
        //     1398: invokevirtual 2277	com/android/server/wm/WindowStateAnimator:isAnimating	()Z
        //     1401: ifeq +12 -> 1413
        //     1404: aload 21
        //     1406: iconst_1
        //     1407: putfield 1286	com/android/server/wm/WindowState:mExiting	Z
        //     1410: goto -25 -> 1385
        //     1413: aload 21
        //     1415: aload_0
        //     1416: getfield 548	com/android/server/wm/WindowManagerService:mWallpaperTarget	Lcom/android/server/wm/WindowState;
        //     1419: if_acmpne +21 -> 1440
        //     1422: aload 21
        //     1424: iconst_1
        //     1425: putfield 1286	com/android/server/wm/WindowState:mExiting	Z
        //     1428: aload 21
        //     1430: getfield 959	com/android/server/wm/WindowState:mWinAnimator	Lcom/android/server/wm/WindowStateAnimator;
        //     1433: iconst_1
        //     1434: putfield 3601	com/android/server/wm/WindowStateAnimator:mAnimating	Z
        //     1437: goto -52 -> 1385
        //     1440: aload_0
        //     1441: getfield 542	com/android/server/wm/WindowManagerService:mInputMethodWindow	Lcom/android/server/wm/WindowState;
        //     1444: aload 21
        //     1446: if_acmpne +8 -> 1454
        //     1449: aload_0
        //     1450: aconst_null
        //     1451: putfield 542	com/android/server/wm/WindowManagerService:mInputMethodWindow	Lcom/android/server/wm/WindowState;
        //     1454: aload 22
        //     1456: invokevirtual 1696	com/android/server/wm/WindowStateAnimator:destroySurfaceLocked	()V
        //     1459: goto -74 -> 1385
        //     1462: iconst_0
        //     1463: istore 35
        //     1465: goto -531 -> 934
        //     1468: iconst_0
        //     1469: istore 39
        //     1471: goto -360 -> 1111
        //     1474: iconst_0
        //     1475: istore 40
        //     1477: goto -358 -> 1119
        //     1480: iconst_0
        //     1481: istore 42
        //     1483: goto -349 -> 1134
        //     1486: iconst_0
        //     1487: istore 44
        //     1489: goto -339 -> 1150
        //     1492: iload 45
        //     1494: ireturn
        //     1495: iload 8
        //     1497: iconst_2
        //     1498: iand
        //     1499: ifeq -1261 -> 238
        //     1502: iconst_1
        //     1503: istore 23
        //     1505: goto -1324 -> 181
        //     1508: ldc_w 993
        //     1511: iload 25
        //     1513: iand
        //     1514: ifeq -315 -> 1199
        //     1517: iconst_1
        //     1518: istore 28
        //     1520: goto -1094 -> 426
        //     1523: iconst_1
        //     1524: istore 29
        //     1526: goto -1071 -> 455
        //     1529: ldc_w 1542
        //     1532: iload 25
        //     1534: iand
        //     1535: ifeq -318 -> 1217
        //     1538: iconst_1
        //     1539: istore 31
        //     1541: goto -1055 -> 486
        //     1544: iconst_1
        //     1545: istore 34
        //     1547: goto -645 -> 902
        //
        // Exception table:
        //     from	to	target	type
        //     71	235	230	finally
        //     244	716	230	finally
        //     722	752	230	finally
        //     760	1089	230	finally
        //     1184	1196	230	finally
        //     1229	1234	230	finally
        //     1239	1459	230	finally
        //     722	752	1237	java/lang/Exception
        //     1229	1234	1237	java/lang/Exception
    }

    public void removeAppToken(IBinder paramIBinder)
    {
        if (!checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "removeAppToken()"))
            throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
        AppWindowToken localAppWindowToken1 = null;
        AppWindowToken localAppWindowToken2 = null;
        boolean bool = false;
        long l = Binder.clearCallingIdentity();
        while (true)
        {
            synchronized (this.mWindowMap)
            {
                WindowToken localWindowToken = (WindowToken)this.mTokenMap.remove(paramIBinder);
                if (localWindowToken != null)
                {
                    localAppWindowToken1 = localWindowToken.appWindowToken;
                    if (localAppWindowToken1 != null)
                    {
                        bool = setTokenVisibilityLocked(localAppWindowToken1, null, false, -1, true);
                        localAppWindowToken1.inPendingTransaction = false;
                        this.mOpeningApps.remove(localAppWindowToken1);
                        localAppWindowToken1.waitingToShow = false;
                        if (this.mClosingApps.contains(localAppWindowToken1))
                        {
                            bool = true;
                            if (bool)
                            {
                                this.mExitingAppTokens.add(localAppWindowToken1);
                                this.mAppTokens.remove(localAppWindowToken1);
                                this.mAnimatingAppTokens.remove(localAppWindowToken1);
                                localAppWindowToken1.removed = true;
                                if (localAppWindowToken1.startingData != null)
                                    localAppWindowToken2 = localAppWindowToken1;
                                unsetAppFreezingScreenLocked(localAppWindowToken1, true, true);
                                if (this.mFocusedApp == localAppWindowToken1)
                                {
                                    this.mFocusedApp = null;
                                    updateFocusedWindowLocked(0, true);
                                    this.mInputMonitor.setFocusedAppLw(null);
                                }
                                if ((!bool) && (localAppWindowToken1 != null))
                                    localAppWindowToken1.updateReportedVisibilityLocked();
                                Binder.restoreCallingIdentity(l);
                                if (localAppWindowToken2 != null)
                                {
                                    Message localMessage = this.mH.obtainMessage(6, localAppWindowToken2);
                                    this.mH.sendMessage(localMessage);
                                }
                            }
                        }
                        else
                        {
                            if (this.mNextAppTransition == -1)
                                continue;
                            this.mClosingApps.add(localAppWindowToken1);
                            localAppWindowToken1.waitingToHide = true;
                            bool = true;
                            continue;
                        }
                        localAppWindowToken1.mAppAnimator.clearAnimation();
                        localAppWindowToken1.mAppAnimator.animating = false;
                    }
                }
            }
            Slog.w("WindowManager", "Attempted to remove non-existing app token: " + paramIBinder);
        }
    }

    boolean removeFakeWindowLocked(WindowManagerPolicy.FakeWindow paramFakeWindow)
    {
        boolean bool = true;
        synchronized (this.mWindowMap)
        {
            if (this.mFakeWindows.remove(paramFakeWindow))
                this.mInputMonitor.updateInputWindowsLw(true);
            else
                bool = false;
        }
        return bool;
    }

    public void removeWindow(Session paramSession, IWindow paramIWindow)
    {
        synchronized (this.mWindowMap)
        {
            WindowState localWindowState = windowForClientLocked(paramSession, paramIWindow, false);
            if (localWindowState != null)
                removeWindowLocked(paramSession, localWindowState);
        }
    }

    public void removeWindowChangeListener(WindowChangeListener paramWindowChangeListener)
    {
        synchronized (this.mWindowMap)
        {
            this.mWindowChangeListeners.remove(paramWindowChangeListener);
            return;
        }
    }

    public void removeWindowLocked(Session paramSession, WindowState paramWindowState)
    {
        long l = Binder.clearCallingIdentity();
        paramWindowState.disposeInputChannel();
        boolean bool = false;
        if ((paramWindowState.mHasSurface) && (okToDisplay()))
        {
            bool = paramWindowState.isWinVisibleLw();
            if (bool)
            {
                int i = 8194;
                if (paramWindowState.mAttrs.type == 3)
                    i = 5;
                if (paramWindowState.mWinAnimator.applyAnimationLocked(i, false))
                    paramWindowState.mExiting = true;
            }
            if ((paramWindowState.mExiting) || (paramWindowState.mWinAnimator.isAnimating()))
            {
                paramWindowState.mExiting = true;
                paramWindowState.mRemoveOnExit = true;
                this.mLayoutNeeded = true;
                updateFocusedWindowLocked(3, false);
                performLayoutAndPlaceSurfacesLocked();
                this.mInputMonitor.updateInputWindowsLw(false);
                if (paramWindowState.mAppToken != null)
                    paramWindowState.mAppToken.updateReportedVisibilityLocked();
                Binder.restoreCallingIdentity(l);
            }
        }
        while (true)
        {
            return;
            removeWindowInnerLocked(paramSession, paramWindowState);
            if ((bool) && (computeForcedAppOrientationLocked() != this.mForcedAppOrientation) && (updateOrientationFromAppTokensLocked(false)))
                this.mH.sendEmptyMessage(18);
            updateFocusedWindowLocked(0, true);
            Binder.restoreCallingIdentity(l);
        }
    }

    public void removeWindowToken(IBinder paramIBinder)
    {
        if (!checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "removeWindowToken()"))
            throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
        long l = Binder.clearCallingIdentity();
        while (true)
        {
            int m;
            synchronized (this.mWindowMap)
            {
                WindowToken localWindowToken = (WindowToken)this.mTokenMap.remove(paramIBinder);
                if (localWindowToken != null)
                {
                    int i = 0;
                    if (!localWindowToken.hidden)
                    {
                        localWindowToken.hidden = true;
                        int j = localWindowToken.windows.size();
                        int k = 0;
                        m = 0;
                        if (m < j)
                        {
                            WindowState localWindowState = (WindowState)localWindowToken.windows.get(m);
                            if (localWindowState.mWinAnimator.isAnimating())
                                i = 1;
                            if (!localWindowState.isVisibleNow())
                                break label264;
                            localWindowState.mWinAnimator.applyAnimationLocked(8194, false);
                            k = 1;
                            break label264;
                        }
                        if (k != 0)
                        {
                            this.mLayoutNeeded = true;
                            performLayoutAndPlaceSurfacesLocked();
                            updateFocusedWindowLocked(0, false);
                        }
                        if (i != 0)
                            this.mExitingTokens.add(localWindowToken);
                    }
                    else
                    {
                        this.mInputMonitor.updateInputWindowsLw(true);
                        Binder.restoreCallingIdentity(l);
                        return;
                    }
                    if (localWindowToken.windowType != 2013)
                        continue;
                    this.mWallpaperTokens.remove(localWindowToken);
                }
            }
            Slog.w("WindowManager", "Attempted to remove non-existing token: " + paramIBinder);
            continue;
            label264: m++;
        }
    }

    void requestTraversalLocked()
    {
        if (!this.mTraversalScheduled)
        {
            this.mTraversalScheduled = true;
            this.mH.sendEmptyMessage(4);
        }
    }

    public void resumeKeyDispatching(IBinder paramIBinder)
    {
        if (!checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "resumeKeyDispatching()"))
            throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
        synchronized (this.mWindowMap)
        {
            WindowToken localWindowToken = (WindowToken)this.mTokenMap.get(paramIBinder);
            if (localWindowToken != null)
                this.mInputMonitor.resumeDispatchingLw(localWindowToken);
            return;
        }
    }

    void resumeRotationLocked()
    {
        if (this.mDeferredRotationPauseCount > 0)
        {
            this.mDeferredRotationPauseCount = (-1 + this.mDeferredRotationPauseCount);
            if ((this.mDeferredRotationPauseCount == 0) && (updateRotationUncheckedLocked(false)))
                this.mH.sendEmptyMessage(18);
        }
    }

    public void saveANRStateLocked(AppWindowToken paramAppWindowToken, WindowState paramWindowState)
    {
        StringWriter localStringWriter = new StringWriter();
        PrintWriter localPrintWriter = new PrintWriter(localStringWriter);
        localPrintWriter.println("    ANR time: " + DateFormat.getInstance().format(new Date()));
        if (paramAppWindowToken != null)
            localPrintWriter.println("    Application at fault: " + paramAppWindowToken.stringName);
        if (paramWindowState != null)
            localPrintWriter.println("    Window at fault: " + paramWindowState.mAttrs.getTitle());
        localPrintWriter.println();
        dumpWindowsNoHeaderLocked(localPrintWriter, true, null);
        localPrintWriter.close();
        this.mLastANRState = localStringWriter.toString();
    }

    public void saveLastInputMethodWindowForTransition()
    {
        synchronized (this.mWindowMap)
        {
            if (this.mInputMethodWindow != null)
                this.mPolicy.setLastInputMethodWindowLw(this.mInputMethodWindow, this.mInputMethodTarget);
            return;
        }
    }

    void scheduleAnimationLocked()
    {
        if (!this.mAnimationScheduled)
        {
            this.mChoreographer.postCallback(1, this.mAnimationRunnable, null);
            this.mAnimationScheduled = true;
        }
    }

    public Bitmap screenshotApplications(IBinder paramIBinder, int paramInt1, int paramInt2)
    {
        if (!checkCallingPermission("android.permission.READ_FRAME_BUFFER", "screenshotApplications()"))
            throw new SecurityException("Requires READ_FRAME_BUFFER permission");
        int i = 0;
        Rect localRect1 = new Rect();
        while (true)
        {
            long l;
            int j;
            int k;
            int i1;
            int i2;
            Bitmap localBitmap1;
            int i3;
            int i4;
            int i5;
            float f1;
            float f2;
            float f3;
            Bitmap localBitmap2;
            Matrix localMatrix;
            Canvas localCanvas;
            synchronized (this.mWindowMap)
            {
                l = Binder.clearCallingIdentity();
                j = this.mCurDisplayWidth;
                k = this.mCurDisplayHeight;
                int m = 10000 + (1000 + 10000 * this.mPolicy.windowTypeToLayerLw(2));
                if ((this.mInputMethodTarget == null) || (this.mInputMethodTarget.mAppToken == null) || (this.mInputMethodTarget.mAppToken.appToken == null) || (this.mInputMethodTarget.mAppToken.appToken.asBinder() != paramIBinder))
                    break label704;
                n = 1;
                i1 = 0;
                i2 = -1 + this.mWindows.size();
                if (i2 >= 0)
                {
                    WindowState localWindowState = (WindowState)this.mWindows.get(i2);
                    if ((!localWindowState.mHasSurface) || (localWindowState.mLayer >= m) || ((i1 == 0) && (paramIBinder != null) && ((!localWindowState.mIsImWindow) || (n == 0)) && ((localWindowState.mAppToken == null) || (localWindowState.mAppToken.token != paramIBinder))))
                        continue;
                    if ((!localWindowState.mIsImWindow) && (!localWindowState.isFullscreen(j, k)))
                    {
                        i1 = 1;
                        int i9 = localWindowState.mWinAnimator.mSurfaceLayer;
                        if (i < i9)
                            i = localWindowState.mWinAnimator.mSurfaceLayer;
                        if (localWindowState.mIsWallpaper)
                            continue;
                        Rect localRect2 = localWindowState.mFrame;
                        Rect localRect3 = localWindowState.mContentInsets;
                        localRect1.union(localRect2.left + localRect3.left, localRect2.top + localRect3.top, localRect2.right - localRect3.right, localRect2.bottom - localRect3.bottom);
                    }
                }
            }
            continue;
            label704: int n = 0;
            continue;
            return localBitmap1;
            int i6 = (int)(f3 * j);
            int i7 = (int)(f3 * k);
            if ((i3 == 1) || (i3 == 3))
            {
                int i8 = i6;
                i6 = i7;
                i7 = i8;
                if (i3 == 1)
                    i3 = 3;
            }
        }
    }

    void sendNewConfiguration()
    {
        try
        {
            this.mActivityManager.updateConfiguration(null);
            label10: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label10;
        }
    }

    public Bundle sendWindowWallpaperCommandLocked(WindowState paramWindowState, String paramString, int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle, boolean paramBoolean)
    {
        boolean bool;
        int i;
        if ((paramWindowState == this.mWallpaperTarget) || (paramWindowState == this.mLowerWallpaperTarget) || (paramWindowState == this.mUpperWallpaperTarget))
        {
            bool = paramBoolean;
            i = this.mWallpaperTokens.size();
        }
        while (true)
        {
            WindowToken localWindowToken;
            int j;
            if (i > 0)
            {
                i--;
                localWindowToken = (WindowToken)this.mWallpaperTokens.get(i);
                j = localWindowToken.windows.size();
            }
            while (j > 0)
            {
                j--;
                WindowState localWindowState = (WindowState)localWindowToken.windows.get(j);
                try
                {
                    localWindowState.mClient.dispatchWallpaperCommand(paramString, paramInt1, paramInt2, paramInt3, paramBundle, paramBoolean);
                    paramBoolean = false;
                    continue;
                    if (bool);
                    return null;
                }
                catch (RemoteException localRemoteException)
                {
                }
            }
        }
    }

    public void setAnimationScale(int paramInt, float paramFloat)
    {
        if (!checkCallingPermission("android.permission.SET_ANIMATION_SCALE", "setAnimationScale()"))
            throw new SecurityException("Requires SET_ANIMATION_SCALE permission");
        float f;
        if (paramFloat < 0.0F)
        {
            paramFloat = 0.0F;
            f = Math.abs(paramFloat);
            switch (paramInt)
            {
            default:
            case 0:
            case 1:
            case 2:
            }
        }
        while (true)
        {
            this.mH.obtainMessage(14).sendToTarget();
            return;
            if (paramFloat <= 20.0F)
                break;
            paramFloat = 20.0F;
            break;
            this.mWindowAnimationScale = fixScale(f);
            continue;
            this.mTransitionAnimationScale = fixScale(f);
            continue;
            this.mAnimatorDurationScale = fixScale(f);
        }
    }

    public void setAnimationScales(float[] paramArrayOfFloat)
    {
        if (!checkCallingPermission("android.permission.SET_ANIMATION_SCALE", "setAnimationScale()"))
            throw new SecurityException("Requires SET_ANIMATION_SCALE permission");
        if (paramArrayOfFloat != null)
        {
            if (paramArrayOfFloat.length >= 1)
                this.mWindowAnimationScale = fixScale(paramArrayOfFloat[0]);
            if (paramArrayOfFloat.length >= 2)
                this.mTransitionAnimationScale = fixScale(paramArrayOfFloat[1]);
            if (paramArrayOfFloat.length >= 3)
                this.mAnimatorDurationScale = fixScale(paramArrayOfFloat[2]);
        }
        this.mH.obtainMessage(14).sendToTarget();
    }

    public void setAppGroupId(IBinder paramIBinder, int paramInt)
    {
        if (!checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "setAppGroupId()"))
            throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
        synchronized (this.mWindowMap)
        {
            AppWindowToken localAppWindowToken = findAppWindowToken(paramIBinder);
            if (localAppWindowToken == null)
                Slog.w("WindowManager", "Attempted to set group id of non-existing app token: " + paramIBinder);
            else
                localAppWindowToken.groupId = paramInt;
        }
    }

    public void setAppOrientation(IApplicationToken paramIApplicationToken, int paramInt)
    {
        if (!checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "setAppOrientation()"))
            throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
        synchronized (this.mWindowMap)
        {
            AppWindowToken localAppWindowToken = findAppWindowToken(paramIApplicationToken.asBinder());
            if (localAppWindowToken == null)
                Slog.w("WindowManager", "Attempted to set orientation of non-existing app token: " + paramIApplicationToken);
            else
                localAppWindowToken.requestedOrientation = paramInt;
        }
    }

    // ERROR //
    public void setAppStartingWindow(IBinder paramIBinder1, String paramString, int paramInt1, CompatibilityInfo paramCompatibilityInfo, java.lang.CharSequence paramCharSequence, int paramInt2, int paramInt3, int paramInt4, IBinder paramIBinder2, boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: ldc_w 2067
        //     4: ldc_w 3791
        //     7: invokevirtual 2073	com/android/server/wm/WindowManagerService:checkCallingPermission	(Ljava/lang/String;Ljava/lang/String;)Z
        //     10: ifne +14 -> 24
        //     13: new 2075	java/lang/SecurityException
        //     16: dup
        //     17: ldc_w 2077
        //     20: invokespecial 2079	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     23: athrow
        //     24: aload_0
        //     25: getfield 378	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
        //     28: astore 11
        //     30: aload 11
        //     32: monitorenter
        //     33: aload_0
        //     34: aload_1
        //     35: invokevirtual 1873	com/android/server/wm/WindowManagerService:findAppWindowToken	(Landroid/os/IBinder;)Lcom/android/server/wm/AppWindowToken;
        //     38: astore 13
        //     40: aload 13
        //     42: ifnonnull +35 -> 77
        //     45: ldc 117
        //     47: new 1311	java/lang/StringBuilder
        //     50: dup
        //     51: invokespecial 1312	java/lang/StringBuilder:<init>	()V
        //     54: ldc_w 3793
        //     57: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     60: aload_1
        //     61: invokevirtual 1321	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     64: invokevirtual 1329	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     67: invokestatic 1404	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     70: pop
        //     71: aload 11
        //     73: monitorexit
        //     74: goto +722 -> 796
        //     77: aload_0
        //     78: invokevirtual 916	com/android/server/wm/WindowManagerService:okToDisplay	()Z
        //     81: ifne +17 -> 98
        //     84: aload 11
        //     86: monitorexit
        //     87: goto +709 -> 796
        //     90: astore 12
        //     92: aload 11
        //     94: monitorexit
        //     95: aload 12
        //     97: athrow
        //     98: aload 13
        //     100: getfield 1914	com/android/server/wm/AppWindowToken:startingData	Lcom/android/server/wm/StartingData;
        //     103: ifnull +9 -> 112
        //     106: aload 11
        //     108: monitorexit
        //     109: goto +687 -> 796
        //     112: aload 9
        //     114: ifnull +516 -> 630
        //     117: aload_0
        //     118: aload 9
        //     120: invokevirtual 1873	com/android/server/wm/WindowManagerService:findAppWindowToken	(Landroid/os/IBinder;)Lcom/android/server/wm/AppWindowToken;
        //     123: astore 17
        //     125: aload 17
        //     127: ifnull +503 -> 630
        //     130: aload 17
        //     132: getfield 864	com/android/server/wm/AppWindowToken:startingWindow	Lcom/android/server/wm/WindowState;
        //     135: astore 18
        //     137: aload 18
        //     139: ifnull +339 -> 478
        //     142: aload_0
        //     143: getfield 506	com/android/server/wm/WindowManagerService:mStartingIconInTransition	Z
        //     146: ifeq +8 -> 154
        //     149: aload_0
        //     150: iconst_1
        //     151: putfield 508	com/android/server/wm/WindowManagerService:mSkipAppTransitionAnimation	Z
        //     154: invokestatic 2233	android/os/Binder:clearCallingIdentity	()J
        //     157: lstore 23
        //     159: aload 13
        //     161: aload 17
        //     163: getfield 1914	com/android/server/wm/AppWindowToken:startingData	Lcom/android/server/wm/StartingData;
        //     166: putfield 1914	com/android/server/wm/AppWindowToken:startingData	Lcom/android/server/wm/StartingData;
        //     169: aload 13
        //     171: aload 17
        //     173: getfield 1918	com/android/server/wm/AppWindowToken:startingView	Landroid/view/View;
        //     176: putfield 1918	com/android/server/wm/AppWindowToken:startingView	Landroid/view/View;
        //     179: aload 13
        //     181: aload 17
        //     183: getfield 1569	com/android/server/wm/AppWindowToken:startingDisplayed	Z
        //     186: putfield 1569	com/android/server/wm/AppWindowToken:startingDisplayed	Z
        //     189: aload 13
        //     191: aload 18
        //     193: putfield 864	com/android/server/wm/AppWindowToken:startingWindow	Lcom/android/server/wm/WindowState;
        //     196: aload 13
        //     198: aload 17
        //     200: getfield 3138	com/android/server/wm/AppWindowToken:reportedVisible	Z
        //     203: putfield 3138	com/android/server/wm/AppWindowToken:reportedVisible	Z
        //     206: aload 17
        //     208: aconst_null
        //     209: putfield 1914	com/android/server/wm/AppWindowToken:startingData	Lcom/android/server/wm/StartingData;
        //     212: aload 17
        //     214: aconst_null
        //     215: putfield 1918	com/android/server/wm/AppWindowToken:startingView	Landroid/view/View;
        //     218: aload 17
        //     220: aconst_null
        //     221: putfield 864	com/android/server/wm/AppWindowToken:startingWindow	Lcom/android/server/wm/WindowState;
        //     224: aload 17
        //     226: iconst_1
        //     227: putfield 3123	com/android/server/wm/AppWindowToken:startingMoved	Z
        //     230: aload 18
        //     232: aload 13
        //     234: putfield 829	com/android/server/wm/WindowState:mToken	Lcom/android/server/wm/WindowToken;
        //     237: aload 18
        //     239: aload 13
        //     241: putfield 3796	com/android/server/wm/WindowState:mRootToken	Lcom/android/server/wm/WindowToken;
        //     244: aload 18
        //     246: aload 13
        //     248: putfield 856	com/android/server/wm/WindowState:mAppToken	Lcom/android/server/wm/AppWindowToken;
        //     251: aload_0
        //     252: getfield 395	com/android/server/wm/WindowManagerService:mWindows	Ljava/util/ArrayList;
        //     255: aload 18
        //     257: invokevirtual 1704	java/util/ArrayList:remove	(Ljava/lang/Object;)Z
        //     260: pop
        //     261: aload_0
        //     262: iconst_1
        //     263: putfield 585	com/android/server/wm/WindowManagerService:mWindowsChanged	Z
        //     266: aload 17
        //     268: getfield 837	com/android/server/wm/WindowToken:windows	Ljava/util/ArrayList;
        //     271: aload 18
        //     273: invokevirtual 1704	java/util/ArrayList:remove	(Ljava/lang/Object;)Z
        //     276: pop
        //     277: aload 17
        //     279: getfield 859	com/android/server/wm/AppWindowToken:allAppWindows	Ljava/util/ArrayList;
        //     282: aload 18
        //     284: invokevirtual 1704	java/util/ArrayList:remove	(Ljava/lang/Object;)Z
        //     287: pop
        //     288: aload_0
        //     289: aload 18
        //     291: iconst_1
        //     292: invokespecial 1802	com/android/server/wm/WindowManagerService:addWindowToListInOrderLocked	(Lcom/android/server/wm/WindowState;Z)V
        //     295: aload 17
        //     297: getfield 1554	com/android/server/wm/AppWindowToken:allDrawn	Z
        //     300: ifeq +9 -> 309
        //     303: aload 13
        //     305: iconst_1
        //     306: putfield 1554	com/android/server/wm/AppWindowToken:allDrawn	Z
        //     309: aload 17
        //     311: getfield 1910	com/android/server/wm/AppWindowToken:firstWindowDrawn	Z
        //     314: ifeq +9 -> 323
        //     317: aload 13
        //     319: iconst_1
        //     320: putfield 1910	com/android/server/wm/AppWindowToken:firstWindowDrawn	Z
        //     323: aload 17
        //     325: getfield 905	com/android/server/wm/WindowToken:hidden	Z
        //     328: ifne +21 -> 349
        //     331: aload 13
        //     333: iconst_0
        //     334: putfield 905	com/android/server/wm/WindowToken:hidden	Z
        //     337: aload 13
        //     339: iconst_0
        //     340: putfield 1283	com/android/server/wm/AppWindowToken:hiddenRequested	Z
        //     343: aload 13
        //     345: iconst_0
        //     346: putfield 3091	com/android/server/wm/AppWindowToken:willBeHidden	Z
        //     349: aload 13
        //     351: getfield 2251	com/android/server/wm/AppWindowToken:clientHidden	Z
        //     354: aload 17
        //     356: getfield 2251	com/android/server/wm/AppWindowToken:clientHidden	Z
        //     359: if_icmpeq +18 -> 377
        //     362: aload 13
        //     364: aload 17
        //     366: getfield 2251	com/android/server/wm/AppWindowToken:clientHidden	Z
        //     369: putfield 2251	com/android/server/wm/AppWindowToken:clientHidden	Z
        //     372: aload 13
        //     374: invokevirtual 3799	com/android/server/wm/AppWindowToken:sendAppVisibilityToClients	()V
        //     377: aload 17
        //     379: getfield 928	com/android/server/wm/AppWindowToken:mAppAnimator	Lcom/android/server/wm/AppWindowAnimator;
        //     382: astore 28
        //     384: aload 13
        //     386: getfield 928	com/android/server/wm/AppWindowToken:mAppAnimator	Lcom/android/server/wm/AppWindowAnimator;
        //     389: astore 29
        //     391: aload 28
        //     393: getfield 938	com/android/server/wm/AppWindowAnimator:animation	Landroid/view/animation/Animation;
        //     396: ifnull +55 -> 451
        //     399: aload 29
        //     401: aload 28
        //     403: getfield 938	com/android/server/wm/AppWindowAnimator:animation	Landroid/view/animation/Animation;
        //     406: putfield 938	com/android/server/wm/AppWindowAnimator:animation	Landroid/view/animation/Animation;
        //     409: aload 29
        //     411: aload 28
        //     413: getfield 1710	com/android/server/wm/AppWindowAnimator:animating	Z
        //     416: putfield 1710	com/android/server/wm/AppWindowAnimator:animating	Z
        //     419: aload 29
        //     421: aload 28
        //     423: getfield 979	com/android/server/wm/AppWindowAnimator:animLayerAdjustment	I
        //     426: putfield 979	com/android/server/wm/AppWindowAnimator:animLayerAdjustment	I
        //     429: aload 28
        //     431: aconst_null
        //     432: putfield 938	com/android/server/wm/AppWindowAnimator:animation	Landroid/view/animation/Animation;
        //     435: aload 28
        //     437: iconst_0
        //     438: putfield 979	com/android/server/wm/AppWindowAnimator:animLayerAdjustment	I
        //     441: aload 29
        //     443: invokevirtual 3802	com/android/server/wm/AppWindowAnimator:updateLayers	()V
        //     446: aload 28
        //     448: invokevirtual 3802	com/android/server/wm/AppWindowAnimator:updateLayers	()V
        //     451: aload_0
        //     452: iconst_3
        //     453: iconst_1
        //     454: invokespecial 1366	com/android/server/wm/WindowManagerService:updateFocusedWindowLocked	(IZ)Z
        //     457: pop
        //     458: aload_0
        //     459: iconst_1
        //     460: putfield 473	com/android/server/wm/WindowManagerService:mLayoutNeeded	Z
        //     463: aload_0
        //     464: invokespecial 794	com/android/server/wm/WindowManagerService:performLayoutAndPlaceSurfacesLocked	()V
        //     467: lload 23
        //     469: invokestatic 2257	android/os/Binder:restoreCallingIdentity	(J)V
        //     472: aload 11
        //     474: monitorexit
        //     475: goto +321 -> 796
        //     478: aload 17
        //     480: getfield 1914	com/android/server/wm/AppWindowToken:startingData	Lcom/android/server/wm/StartingData;
        //     483: ifnull +53 -> 536
        //     486: aload 13
        //     488: aload 17
        //     490: getfield 1914	com/android/server/wm/AppWindowToken:startingData	Lcom/android/server/wm/StartingData;
        //     493: putfield 1914	com/android/server/wm/AppWindowToken:startingData	Lcom/android/server/wm/StartingData;
        //     496: aload 17
        //     498: aconst_null
        //     499: putfield 1914	com/android/server/wm/AppWindowToken:startingData	Lcom/android/server/wm/StartingData;
        //     502: aload 17
        //     504: iconst_1
        //     505: putfield 3123	com/android/server/wm/AppWindowToken:startingMoved	Z
        //     508: aload_0
        //     509: getfield 526	com/android/server/wm/WindowManagerService:mH	Lcom/android/server/wm/WindowManagerService$H;
        //     512: iconst_5
        //     513: aload 13
        //     515: invokevirtual 1731	com/android/server/wm/WindowManagerService$H:obtainMessage	(ILjava/lang/Object;)Landroid/os/Message;
        //     518: astore 21
        //     520: aload_0
        //     521: getfield 526	com/android/server/wm/WindowManagerService:mH	Lcom/android/server/wm/WindowManagerService$H;
        //     524: aload 21
        //     526: invokevirtual 3805	com/android/server/wm/WindowManagerService$H:sendMessageAtFrontOfQueue	(Landroid/os/Message;)Z
        //     529: pop
        //     530: aload 11
        //     532: monitorexit
        //     533: goto +263 -> 796
        //     536: aload 17
        //     538: getfield 928	com/android/server/wm/AppWindowToken:mAppAnimator	Lcom/android/server/wm/AppWindowAnimator;
        //     541: astore 19
        //     543: aload 13
        //     545: getfield 928	com/android/server/wm/AppWindowToken:mAppAnimator	Lcom/android/server/wm/AppWindowAnimator;
        //     548: astore 20
        //     550: aload 19
        //     552: getfield 3169	com/android/server/wm/AppWindowAnimator:thumbnail	Landroid/view/Surface;
        //     555: ifnull +75 -> 630
        //     558: aload 20
        //     560: getfield 3169	com/android/server/wm/AppWindowAnimator:thumbnail	Landroid/view/Surface;
        //     563: ifnull +11 -> 574
        //     566: aload 20
        //     568: getfield 3169	com/android/server/wm/AppWindowAnimator:thumbnail	Landroid/view/Surface;
        //     571: invokevirtual 3477	android/view/Surface:destroy	()V
        //     574: aload 20
        //     576: aload 19
        //     578: getfield 3169	com/android/server/wm/AppWindowAnimator:thumbnail	Landroid/view/Surface;
        //     581: putfield 3169	com/android/server/wm/AppWindowAnimator:thumbnail	Landroid/view/Surface;
        //     584: aload 20
        //     586: aload 19
        //     588: getfield 3204	com/android/server/wm/AppWindowAnimator:thumbnailX	I
        //     591: putfield 3204	com/android/server/wm/AppWindowAnimator:thumbnailX	I
        //     594: aload 20
        //     596: aload 19
        //     598: getfield 3207	com/android/server/wm/AppWindowAnimator:thumbnailY	I
        //     601: putfield 3207	com/android/server/wm/AppWindowAnimator:thumbnailY	I
        //     604: aload 20
        //     606: aload 19
        //     608: getfield 3192	com/android/server/wm/AppWindowAnimator:thumbnailLayer	I
        //     611: putfield 3192	com/android/server/wm/AppWindowAnimator:thumbnailLayer	I
        //     614: aload 20
        //     616: aload 19
        //     618: getfield 3195	com/android/server/wm/AppWindowAnimator:thumbnailAnimation	Landroid/view/animation/Animation;
        //     621: putfield 3195	com/android/server/wm/AppWindowAnimator:thumbnailAnimation	Landroid/view/animation/Animation;
        //     624: aload 19
        //     626: aconst_null
        //     627: putfield 3169	com/android/server/wm/AppWindowAnimator:thumbnail	Landroid/view/Surface;
        //     630: iload 10
        //     632: ifne +9 -> 641
        //     635: aload 11
        //     637: monitorexit
        //     638: goto +158 -> 796
        //     641: iload_3
        //     642: ifeq +94 -> 736
        //     645: invokestatic 1175	com/android/server/AttributeCache:instance	()Lcom/android/server/AttributeCache;
        //     648: aload_2
        //     649: iload_3
        //     650: getstatic 3808	com/android/internal/R$styleable:Window	[I
        //     653: invokevirtual 1184	com/android/server/AttributeCache:get	(Ljava/lang/String;I[I)Lcom/android/server/AttributeCache$Entry;
        //     656: astore 16
        //     658: aload 16
        //     660: ifnonnull +9 -> 669
        //     663: aload 11
        //     665: monitorexit
        //     666: goto +130 -> 796
        //     669: aload 16
        //     671: getfield 3263	com/android/server/AttributeCache$Entry:array	Landroid/content/res/TypedArray;
        //     674: iconst_5
        //     675: iconst_0
        //     676: invokevirtual 3810	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     679: ifeq +9 -> 688
        //     682: aload 11
        //     684: monitorexit
        //     685: goto +111 -> 796
        //     688: aload 16
        //     690: getfield 3263	com/android/server/AttributeCache$Entry:array	Landroid/content/res/TypedArray;
        //     693: iconst_4
        //     694: iconst_0
        //     695: invokevirtual 3810	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     698: ifeq +9 -> 707
        //     701: aload 11
        //     703: monitorexit
        //     704: goto +92 -> 796
        //     707: aload 16
        //     709: getfield 3263	com/android/server/AttributeCache$Entry:array	Landroid/content/res/TypedArray;
        //     712: bipush 14
        //     714: iconst_0
        //     715: invokevirtual 3810	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     718: ifeq +18 -> 736
        //     721: aload_0
        //     722: getfield 548	com/android/server/wm/WindowManagerService:mWallpaperTarget	Lcom/android/server/wm/WindowState;
        //     725: ifnonnull +68 -> 793
        //     728: iload 8
        //     730: ldc_w 1542
        //     733: ior
        //     734: istore 8
        //     736: aload_0
        //     737: iconst_1
        //     738: putfield 506	com/android/server/wm/WindowManagerService:mStartingIconInTransition	Z
        //     741: aload 13
        //     743: new 3812	com/android/server/wm/StartingData
        //     746: dup
        //     747: aload_2
        //     748: iload_3
        //     749: aload 4
        //     751: aload 5
        //     753: iload 6
        //     755: iload 7
        //     757: iload 8
        //     759: invokespecial 3815	com/android/server/wm/StartingData:<init>	(Ljava/lang/String;ILandroid/content/res/CompatibilityInfo;Ljava/lang/CharSequence;III)V
        //     762: putfield 1914	com/android/server/wm/AppWindowToken:startingData	Lcom/android/server/wm/StartingData;
        //     765: aload_0
        //     766: getfield 526	com/android/server/wm/WindowManagerService:mH	Lcom/android/server/wm/WindowManagerService$H;
        //     769: iconst_5
        //     770: aload 13
        //     772: invokevirtual 1731	com/android/server/wm/WindowManagerService$H:obtainMessage	(ILjava/lang/Object;)Landroid/os/Message;
        //     775: astore 14
        //     777: aload_0
        //     778: getfield 526	com/android/server/wm/WindowManagerService:mH	Lcom/android/server/wm/WindowManagerService$H;
        //     781: aload 14
        //     783: invokevirtual 3805	com/android/server/wm/WindowManagerService$H:sendMessageAtFrontOfQueue	(Landroid/os/Message;)Z
        //     786: pop
        //     787: aload 11
        //     789: monitorexit
        //     790: goto +6 -> 796
        //     793: aload 11
        //     795: monitorexit
        //     796: return
        //
        // Exception table:
        //     from	to	target	type
        //     33	95	90	finally
        //     98	796	90	finally
    }

    public void setAppVisibility(IBinder paramIBinder, boolean paramBoolean)
    {
        boolean bool = true;
        if (!checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "setAppVisibility()"))
            throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
        while (true)
        {
            AppWindowToken localAppWindowToken;
            long l;
            synchronized (this.mWindowMap)
            {
                localAppWindowToken = findAppWindowToken(paramIBinder);
                if (localAppWindowToken == null)
                    Slog.w("WindowManager", "Attempted to set visibility of non-existing app token: " + paramIBinder);
                else if ((okToDisplay()) && (this.mNextAppTransition != -1))
                    if (localAppWindowToken.hiddenRequested == paramBoolean);
            }
            bool = false;
        }
    }

    public void setAppWillBeHidden(IBinder paramIBinder)
    {
        if (!checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "setAppWillBeHidden()"))
            throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
        synchronized (this.mWindowMap)
        {
            AppWindowToken localAppWindowToken = findAppWindowToken(paramIBinder);
            if (localAppWindowToken == null)
                Slog.w("WindowManager", "Attempted to set will be hidden of non-existing app token: " + paramIBinder);
            else
                localAppWindowToken.willBeHidden = true;
        }
    }

    public void setEventDispatching(boolean paramBoolean)
    {
        if (!checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "setEventDispatching()"))
            throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
        synchronized (this.mWindowMap)
        {
            this.mEventDispatchingEnabled = paramBoolean;
            if (this.mDisplayEnabled)
                this.mInputMonitor.setEventDispatchingLw(paramBoolean);
            sendScreenStatusToClientsLocked();
            return;
        }
    }

    public void setFocusedApp(IBinder paramIBinder, boolean paramBoolean)
    {
        if (!checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "setFocusedApp()"))
            throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
        HashMap localHashMap = this.mWindowMap;
        if (paramIBinder == null);
        while (true)
        {
            try
            {
                if (this.mFocusedApp != null)
                {
                    i = 1;
                    this.mFocusedApp = null;
                    if (i != 0)
                        this.mInputMonitor.setFocusedAppLw(null);
                    if ((paramBoolean) && (i != 0))
                    {
                        long l = Binder.clearCallingIdentity();
                        updateFocusedWindowLocked(0, true);
                        Binder.restoreCallingIdentity(l);
                    }
                    continue;
                    localAppWindowToken = findAppWindowToken(paramIBinder);
                    if (localAppWindowToken == null)
                        Slog.w("WindowManager", "Attempted to set focus to non-existing app token: " + paramIBinder);
                }
            }
            finally
            {
                AppWindowToken localAppWindowToken;
                throw localObject;
                if (this.mFocusedApp != localAppWindowToken)
                {
                    i = 1;
                    this.mFocusedApp = localAppWindowToken;
                    if (i == 0)
                        continue;
                    this.mInputMonitor.setFocusedAppLw(localAppWindowToken);
                    continue;
                }
                i = 0;
            }
            int i = 0;
        }
    }

    public void setForcedDisplaySize(int paramInt1, int paramInt2)
    {
        label159: 
        while (true)
            synchronized (this.mWindowMap)
            {
                int i;
                if (this.mInitialDisplayWidth < this.mInitialDisplayHeight)
                {
                    if (paramInt2 < this.mInitialDisplayWidth)
                    {
                        i = paramInt2;
                        if (paramInt1 < this.mInitialDisplayHeight)
                        {
                            j = paramInt1;
                            setForcedDisplaySizeLocked(i, j);
                            Settings.Secure.putString(this.mContext.getContentResolver(), "display_size_forced", i + "," + j);
                        }
                    }
                    else
                    {
                        i = this.mInitialDisplayWidth;
                        continue;
                    }
                    j = this.mInitialDisplayHeight;
                    continue;
                }
                if (paramInt1 < this.mInitialDisplayWidth)
                {
                    i = paramInt1;
                    if (paramInt2 < this.mInitialDisplayHeight)
                    {
                        j = paramInt2;
                        break label159;
                    }
                }
                else
                {
                    i = this.mInitialDisplayWidth;
                    continue;
                }
                int j = this.mInitialDisplayHeight;
            }
    }

    public void setHardKeyboardEnabled(boolean paramBoolean)
    {
        synchronized (this.mWindowMap)
        {
            if (this.mHardKeyboardEnabled != paramBoolean)
            {
                this.mHardKeyboardEnabled = paramBoolean;
                this.mH.sendEmptyMessage(18);
            }
            return;
        }
    }

    void setHoldScreenLocked(boolean paramBoolean)
    {
        if (paramBoolean != this.mHoldingScreenWakeLock.isHeld())
        {
            if (!paramBoolean)
                break label32;
            this.mPolicy.screenOnStartedLw();
            this.mHoldingScreenWakeLock.acquire();
        }
        while (true)
        {
            return;
            label32: this.mPolicy.screenOnStoppedLw();
            this.mHoldingScreenWakeLock.release();
        }
    }

    public void setInTouchMode(boolean paramBoolean)
    {
        synchronized (this.mWindowMap)
        {
            this.mInTouchMode = paramBoolean;
            return;
        }
    }

    public void setInputFilter(InputFilter paramInputFilter)
    {
        this.mInputManager.setInputFilter(paramInputFilter);
    }

    void setInputMethodAnimLayerAdjustment(int paramInt)
    {
        this.mInputMethodAnimLayerAdjustment = paramInt;
        WindowState localWindowState1 = this.mInputMethodWindow;
        if (localWindowState1 != null)
        {
            localWindowState1.mWinAnimator.mAnimLayer = (paramInt + localWindowState1.mLayer);
            int j = localWindowState1.mChildWindows.size();
            while (j > 0)
            {
                j--;
                WindowState localWindowState3 = (WindowState)localWindowState1.mChildWindows.get(j);
                localWindowState3.mWinAnimator.mAnimLayer = (paramInt + localWindowState3.mLayer);
            }
        }
        int i = this.mInputMethodDialogs.size();
        while (i > 0)
        {
            i--;
            WindowState localWindowState2 = (WindowState)this.mInputMethodDialogs.get(i);
            localWindowState2.mWinAnimator.mAnimLayer = (paramInt + localWindowState2.mLayer);
        }
    }

    void setInsetsWindow(Session paramSession, IWindow paramIWindow, int paramInt, Rect paramRect1, Rect paramRect2, Region paramRegion)
    {
        long l = Binder.clearCallingIdentity();
        try
        {
            synchronized (this.mWindowMap)
            {
                WindowState localWindowState = windowForClientLocked(paramSession, paramIWindow, false);
                if (localWindowState != null)
                {
                    localWindowState.mGivenInsetsPending = false;
                    localWindowState.mGivenContentInsets.set(paramRect1);
                    localWindowState.mGivenVisibleInsets.set(paramRect2);
                    localWindowState.mGivenTouchableRegion.set(paramRegion);
                    localWindowState.mTouchableInsets = paramInt;
                    if (localWindowState.mGlobalScale != 1.0F)
                    {
                        localWindowState.mGivenContentInsets.scale(localWindowState.mGlobalScale);
                        localWindowState.mGivenVisibleInsets.scale(localWindowState.mGlobalScale);
                        localWindowState.mGivenTouchableRegion.scale(localWindowState.mGlobalScale);
                    }
                    this.mLayoutNeeded = true;
                    performLayoutAndPlaceSurfacesLocked();
                }
                return;
            }
        }
        finally
        {
            Binder.restoreCallingIdentity(l);
        }
    }

    public void setNewConfiguration(Configuration paramConfiguration)
    {
        if (!checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "setNewConfiguration()"))
            throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
        synchronized (this.mWindowMap)
        {
            this.mCurConfiguration = new Configuration(paramConfiguration);
            this.mWaitingForConfig = false;
            performLayoutAndPlaceSurfacesLocked();
            return;
        }
    }

    public void setOnHardKeyboardStatusChangeListener(OnHardKeyboardStatusChangeListener paramOnHardKeyboardStatusChangeListener)
    {
        synchronized (this.mWindowMap)
        {
            this.mHardKeyboardStatusChangeListener = paramOnHardKeyboardStatusChangeListener;
            return;
        }
    }

    public void setStrictModeVisualIndicatorPreference(String paramString)
    {
        SystemProperties.set("persist.sys.strictmode.visual", paramString);
    }

    boolean setTokenVisibilityLocked(AppWindowToken paramAppWindowToken, WindowManager.LayoutParams paramLayoutParams, boolean paramBoolean1, int paramInt, boolean paramBoolean2)
    {
        boolean bool1 = false;
        boolean bool4;
        int j;
        boolean bool2;
        int m;
        label109: WindowState localWindowState2;
        if (paramAppWindowToken.clientHidden == paramBoolean1)
        {
            if (!paramBoolean1)
            {
                bool4 = true;
                paramAppWindowToken.clientHidden = bool4;
                paramAppWindowToken.sendAppVisibilityToClients();
            }
        }
        else
        {
            paramAppWindowToken.willBeHidden = false;
            if (paramAppWindowToken.hidden != paramBoolean1)
                break label290;
            j = 0;
            bool2 = false;
            if (paramInt != -1)
            {
                if (paramAppWindowToken.mAppAnimator.animation == AppWindowAnimator.sDummyAnimation)
                    paramAppWindowToken.mAppAnimator.animation = null;
                if (applyAnimationLocked(paramAppWindowToken, paramLayoutParams, paramInt, paramBoolean1))
                {
                    bool2 = true;
                    bool1 = bool2;
                }
                j = 1;
            }
            int k = paramAppWindowToken.allAppWindows.size();
            m = 0;
            if (m >= k)
                break label219;
            localWindowState2 = (WindowState)paramAppWindowToken.allAppWindows.get(m);
            if (localWindowState2 != paramAppWindowToken.startingWindow)
                break label151;
        }
        while (true)
        {
            m++;
            break label109;
            bool4 = false;
            break;
            label151: if (paramBoolean1)
            {
                if (!localWindowState2.isVisibleNow())
                {
                    if (!bool2)
                        localWindowState2.mWinAnimator.applyAnimationLocked(4097, true);
                    j = 1;
                }
            }
            else if (localWindowState2.isVisibleNow())
            {
                if (!bool2)
                    localWindowState2.mWinAnimator.applyAnimationLocked(8194, false);
                j = 1;
            }
        }
        label219: boolean bool3;
        if (!paramBoolean1)
        {
            bool3 = true;
            paramAppWindowToken.hiddenRequested = bool3;
            paramAppWindowToken.hidden = bool3;
            if (paramBoolean1)
                break label361;
            unsetAppFreezingScreenLocked(paramAppWindowToken, true, true);
        }
        while (true)
        {
            if (j != 0)
            {
                this.mLayoutNeeded = true;
                this.mInputMonitor.setUpdateInputWindowsNeededLw();
                if (paramBoolean2)
                {
                    updateFocusedWindowLocked(3, false);
                    performLayoutAndPlaceSurfacesLocked();
                }
                this.mInputMonitor.updateInputWindowsLw(false);
            }
            label290: if (paramAppWindowToken.mAppAnimator.animation != null)
                bool1 = true;
            for (int i = -1 + paramAppWindowToken.allAppWindows.size(); (i >= 0) && (!bool1); i--)
                if (((WindowState)paramAppWindowToken.allAppWindows.get(i)).mWinAnimator.isWindowAnimating())
                    bool1 = true;
            bool3 = false;
            break;
            label361: WindowState localWindowState1 = paramAppWindowToken.startingWindow;
            if ((localWindowState1 != null) && (!localWindowState1.isDrawnLw()))
            {
                localWindowState1.mPolicyVisibility = false;
                localWindowState1.mPolicyVisibilityAfterAnim = false;
            }
        }
        return bool1;
    }

    void setTransparentRegionHint(WindowStateAnimator paramWindowStateAnimator, Region paramRegion)
    {
        this.mH.sendMessage(this.mH.obtainMessage(100001, new Pair(paramWindowStateAnimator, paramRegion)));
    }

    void setTransparentRegionWindow(Session paramSession, IWindow paramIWindow, Region paramRegion)
    {
        long l = Binder.clearCallingIdentity();
        try
        {
            synchronized (this.mWindowMap)
            {
                WindowState localWindowState = windowForClientLocked(paramSession, paramIWindow, false);
                if ((localWindowState != null) && (localWindowState.mHasSurface))
                    setTransparentRegionHint(localWindowState.mWinAnimator, paramRegion);
                return;
            }
        }
        finally
        {
            Binder.restoreCallingIdentity(l);
        }
    }

    void setWallpaperAnimLayerAdjustmentLocked(int paramInt)
    {
        this.mWallpaperAnimLayerAdjustment = paramInt;
        int i = this.mWallpaperTokens.size();
        while (i > 0)
        {
            i--;
            WindowToken localWindowToken = (WindowToken)this.mWallpaperTokens.get(i);
            int j = localWindowToken.windows.size();
            while (j > 0)
            {
                j--;
                WindowState localWindowState = (WindowState)localWindowToken.windows.get(j);
                localWindowState.mWinAnimator.mAnimLayer = (paramInt + localWindowState.mLayer);
            }
        }
    }

    void setWallpaperOffset(WindowStateAnimator paramWindowStateAnimator, int paramInt1, int paramInt2)
    {
        this.mH.sendMessage(this.mH.obtainMessage(100002, paramInt1, paramInt2, paramWindowStateAnimator));
    }

    public void setWindowWallpaperPositionLocked(WindowState paramWindowState, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        if ((paramWindowState.mWallpaperX != paramFloat1) || (paramWindowState.mWallpaperY != paramFloat2))
        {
            paramWindowState.mWallpaperX = paramFloat1;
            paramWindowState.mWallpaperY = paramFloat2;
            paramWindowState.mWallpaperXStep = paramFloat3;
            paramWindowState.mWallpaperYStep = paramFloat4;
            updateWallpaperOffsetLocked(paramWindowState, true);
        }
    }

    // ERROR //
    public void showBootMessage(java.lang.CharSequence paramCharSequence, boolean paramBoolean)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore_3
        //     2: aload_0
        //     3: getfield 378	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
        //     6: astore 4
        //     8: aload 4
        //     10: monitorenter
        //     11: aload_0
        //     12: getfield 600	com/android/server/wm/WindowManagerService:mAllowBootMessages	Z
        //     15: ifne +9 -> 24
        //     18: aload 4
        //     20: monitorexit
        //     21: goto +70 -> 91
        //     24: aload_0
        //     25: getfield 425	com/android/server/wm/WindowManagerService:mShowingBootMessages	Z
        //     28: ifne +23 -> 51
        //     31: iload_2
        //     32: ifne +17 -> 49
        //     35: aload 4
        //     37: monitorexit
        //     38: goto +53 -> 91
        //     41: astore 5
        //     43: aload 4
        //     45: monitorexit
        //     46: aload 5
        //     48: athrow
        //     49: iconst_1
        //     50: istore_3
        //     51: aload_0
        //     52: getfield 421	com/android/server/wm/WindowManagerService:mSystemBooted	Z
        //     55: ifeq +9 -> 64
        //     58: aload 4
        //     60: monitorexit
        //     61: goto +30 -> 91
        //     64: aload_0
        //     65: iconst_1
        //     66: putfield 425	com/android/server/wm/WindowManagerService:mShowingBootMessages	Z
        //     69: aload_0
        //     70: getfield 368	com/android/server/wm/WindowManagerService:mPolicy	Landroid/view/WindowManagerPolicy;
        //     73: aload_1
        //     74: iload_2
        //     75: invokeinterface 3922 3 0
        //     80: aload 4
        //     82: monitorexit
        //     83: iload_3
        //     84: ifeq +7 -> 91
        //     87: aload_0
        //     88: invokevirtual 3020	com/android/server/wm/WindowManagerService:performEnableScreen	()V
        //     91: return
        //
        // Exception table:
        //     from	to	target	type
        //     11	46	41	finally
        //     51	83	41	finally
    }

    public void showStrictModeViolation(boolean paramBoolean)
    {
        if (this.mHeadless)
            return;
        H localH1 = this.mH;
        H localH2 = this.mH;
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            localH1.sendMessage(localH2.obtainMessage(26, i, 0));
            break;
        }
    }

    public void shutdown()
    {
        ShutdownThread.shutdown(this.mContext, true);
    }

    // ERROR //
    public void startAppFreezingScreen(IBinder paramIBinder, int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: ldc_w 2067
        //     4: ldc_w 3928
        //     7: invokevirtual 2073	com/android/server/wm/WindowManagerService:checkCallingPermission	(Ljava/lang/String;Ljava/lang/String;)Z
        //     10: ifne +14 -> 24
        //     13: new 2075	java/lang/SecurityException
        //     16: dup
        //     17: ldc_w 2077
        //     20: invokespecial 2079	java/lang/SecurityException:<init>	(Ljava/lang/String;)V
        //     23: athrow
        //     24: aload_0
        //     25: getfield 378	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
        //     28: astore_3
        //     29: aload_3
        //     30: monitorenter
        //     31: iload_2
        //     32: ifne +15 -> 47
        //     35: aload_0
        //     36: invokevirtual 916	com/android/server/wm/WindowManagerService:okToDisplay	()Z
        //     39: ifeq +8 -> 47
        //     42: aload_3
        //     43: monitorexit
        //     44: goto +81 -> 125
        //     47: aload_0
        //     48: aload_1
        //     49: invokevirtual 1873	com/android/server/wm/WindowManagerService:findAppWindowToken	(Landroid/os/IBinder;)Lcom/android/server/wm/AppWindowToken;
        //     52: astore 5
        //     54: aload 5
        //     56: ifnull +11 -> 67
        //     59: aload 5
        //     61: getfield 3694	com/android/server/wm/AppWindowToken:appToken	Landroid/view/IApplicationToken;
        //     64: ifnonnull +42 -> 106
        //     67: ldc 117
        //     69: new 1311	java/lang/StringBuilder
        //     72: dup
        //     73: invokespecial 1312	java/lang/StringBuilder:<init>	()V
        //     76: ldc_w 3930
        //     79: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     82: aload 5
        //     84: invokevirtual 1321	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     87: invokevirtual 1329	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     90: invokestatic 1404	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     93: pop
        //     94: aload_3
        //     95: monitorexit
        //     96: goto +29 -> 125
        //     99: astore 4
        //     101: aload_3
        //     102: monitorexit
        //     103: aload 4
        //     105: athrow
        //     106: invokestatic 2233	android/os/Binder:clearCallingIdentity	()J
        //     109: lstore 7
        //     111: aload_0
        //     112: aload 5
        //     114: iload_2
        //     115: invokevirtual 2042	com/android/server/wm/WindowManagerService:startAppFreezingScreenLocked	(Lcom/android/server/wm/AppWindowToken;I)V
        //     118: lload 7
        //     120: invokestatic 2257	android/os/Binder:restoreCallingIdentity	(J)V
        //     123: aload_3
        //     124: monitorexit
        //     125: return
        //
        // Exception table:
        //     from	to	target	type
        //     35	103	99	finally
        //     106	125	99	finally
    }

    public void startAppFreezingScreenLocked(AppWindowToken paramAppWindowToken, int paramInt)
    {
        if (!paramAppWindowToken.hiddenRequested)
        {
            if (!paramAppWindowToken.mAppAnimator.freezingScreen)
            {
                paramAppWindowToken.mAppAnimator.freezingScreen = true;
                this.mAppsFreezingScreen = (1 + this.mAppsFreezingScreen);
                if (this.mAppsFreezingScreen == 1)
                {
                    startFreezingDisplayLocked(false);
                    this.mH.removeMessages(17);
                    this.mH.sendMessageDelayed(this.mH.obtainMessage(17), 5000L);
                }
            }
            int i = paramAppWindowToken.allAppWindows.size();
            for (int j = 0; j < i; j++)
                ((WindowState)paramAppWindowToken.allAppWindows.get(j)).mAppFreezing = true;
        }
    }

    public boolean startViewServer(int paramInt)
    {
        boolean bool1 = false;
        if (isSystemSecure());
        while (true)
        {
            return bool1;
            if ((checkCallingPermission("android.permission.DUMP", "startViewServer")) && (paramInt >= 1024))
                if (this.mViewServer != null)
                {
                    if (!this.mViewServer.isRunning())
                        try
                        {
                            boolean bool3 = this.mViewServer.start();
                            bool1 = bool3;
                        }
                        catch (IOException localIOException2)
                        {
                            Slog.w("WindowManager", "View server did not start");
                        }
                }
                else
                    try
                    {
                        this.mViewServer = new ViewServer(this, paramInt);
                        boolean bool2 = this.mViewServer.start();
                        bool1 = bool2;
                    }
                    catch (IOException localIOException1)
                    {
                        Slog.w("WindowManager", "View server did not start");
                    }
        }
    }

    public void statusBarVisibilityChanged(int paramInt)
    {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.STATUS_BAR") != 0)
            throw new SecurityException("Caller does not hold permission android.permission.STATUS_BAR");
        synchronized (this.mWindowMap)
        {
            this.mLastStatusBarVisibility = paramInt;
            updateStatusBarVisibilityLocked(this.mPolicy.adjustSystemUiVisibilityLw(paramInt));
            return;
        }
    }

    public void stopAppFreezingScreen(IBinder paramIBinder, boolean paramBoolean)
    {
        if (!checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "setAppFreezingScreen()"))
            throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
        synchronized (this.mWindowMap)
        {
            AppWindowToken localAppWindowToken = findAppWindowToken(paramIBinder);
            if ((localAppWindowToken == null) || (localAppWindowToken.appToken != null))
            {
                long l = Binder.clearCallingIdentity();
                unsetAppFreezingScreenLocked(localAppWindowToken, true, paramBoolean);
                Binder.restoreCallingIdentity(l);
            }
        }
    }

    public boolean stopViewServer()
    {
        boolean bool = false;
        if (isSystemSecure());
        while (true)
        {
            return bool;
            if ((checkCallingPermission("android.permission.DUMP", "stopViewServer")) && (this.mViewServer != null))
                bool = this.mViewServer.stop();
        }
    }

    public void switchKeyboardLayout(int paramInt1, int paramInt2)
    {
        this.mInputManager.switchKeyboardLayout(paramInt1, paramInt2);
    }

    public void systemReady()
    {
        this.mPolicy.systemReady();
    }

    public void thawRotation()
    {
        if (!checkCallingPermission("android.permission.SET_ORIENTATION", "thawRotation()"))
            throw new SecurityException("Requires SET_ORIENTATION permission");
        this.mPolicy.setUserRotationMode(0, 777);
        updateRotationUnchecked(false, false);
    }

    void unsetAppFreezingScreenLocked(AppWindowToken paramAppWindowToken, boolean paramBoolean1, boolean paramBoolean2)
    {
        if (paramAppWindowToken.mAppAnimator.freezingScreen)
        {
            int i = paramAppWindowToken.allAppWindows.size();
            int j = 0;
            for (int k = 0; k < i; k++)
            {
                WindowState localWindowState = (WindowState)paramAppWindowToken.allAppWindows.get(k);
                if (localWindowState.mAppFreezing)
                {
                    localWindowState.mAppFreezing = false;
                    if ((localWindowState.mHasSurface) && (!localWindowState.mOrientationChanging))
                    {
                        localWindowState.mOrientationChanging = true;
                        this.mInnerFields.mOrientationChangeComplete = false;
                    }
                    j = 1;
                }
            }
            if ((paramBoolean2) || (j != 0))
            {
                paramAppWindowToken.mAppAnimator.freezingScreen = false;
                this.mAppsFreezingScreen = (-1 + this.mAppsFreezingScreen);
            }
            if (paramBoolean1)
            {
                if (j != 0)
                {
                    this.mLayoutNeeded = true;
                    performLayoutAndPlaceSurfacesLocked();
                }
                stopFreezingDisplayLocked();
            }
        }
    }

    public Configuration updateOrientationFromAppTokens(Configuration paramConfiguration, IBinder paramIBinder)
    {
        if (!checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "updateOrientationFromAppTokens()"))
            throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
        long l = Binder.clearCallingIdentity();
        synchronized (this.mWindowMap)
        {
            Configuration localConfiguration = updateOrientationFromAppTokensLocked(paramConfiguration, paramIBinder);
            Binder.restoreCallingIdentity(l);
            return localConfiguration;
        }
    }

    boolean updateOrientationFromAppTokensLocked(boolean paramBoolean)
    {
        long l = Binder.clearCallingIdentity();
        try
        {
            int i = computeForcedAppOrientationLocked();
            if (i != this.mForcedAppOrientation)
            {
                this.mForcedAppOrientation = i;
                this.mPolicy.setCurrentOrientationLw(i);
                boolean bool2 = updateRotationUncheckedLocked(paramBoolean);
                if (bool2)
                {
                    bool1 = true;
                    return bool1;
                }
            }
            boolean bool1 = false;
            Binder.restoreCallingIdentity(l);
        }
        finally
        {
            Binder.restoreCallingIdentity(l);
        }
    }

    public void updateRotation(boolean paramBoolean1, boolean paramBoolean2)
    {
        updateRotationUnchecked(paramBoolean1, paramBoolean2);
    }

    public void updateRotationUnchecked(boolean paramBoolean1, boolean paramBoolean2)
    {
        long l = Binder.clearCallingIdentity();
        synchronized (this.mWindowMap)
        {
            boolean bool = updateRotationUncheckedLocked(false);
            if ((!bool) || (paramBoolean2))
            {
                this.mLayoutNeeded = true;
                performLayoutAndPlaceSurfacesLocked();
            }
            if ((bool) || (paramBoolean1))
                sendNewConfiguration();
            Binder.restoreCallingIdentity(l);
            return;
        }
    }

    public boolean updateRotationUncheckedLocked(boolean paramBoolean)
    {
        boolean bool1 = false;
        if (this.mDeferredRotationPauseCount > 0);
        while (true)
        {
            return bool1;
            if (((this.mAnimator.mScreenRotationAnimation != null) && (this.mAnimator.mScreenRotationAnimation.isAnimating())) || (!this.mDisplayEnabled))
                continue;
            int i = this.mPolicy.rotationForOrientationLw(this.mForcedAppOrientation, this.mRotation);
            boolean bool2;
            if (!this.mPolicy.rotationHasCompatibleMetricsLw(this.mForcedAppOrientation, i))
                bool2 = true;
            int m;
            while ((this.mRotation != i) || (this.mAltOrientation != bool2))
            {
                this.mRotation = i;
                this.mAltOrientation = bool2;
                this.mPolicy.setRotationLw(this.mRotation);
                this.mWindowsFreezingScreen = true;
                this.mH.removeMessages(11);
                this.mH.sendMessageDelayed(this.mH.obtainMessage(11), 2000L);
                this.mWaitingForConfig = true;
                this.mLayoutNeeded = true;
                startFreezingDisplayLocked(paramBoolean);
                InputManagerService localInputManagerService = this.mInputManager;
                int j;
                if (this.mDisplay != null)
                {
                    j = this.mDisplay.getExternalRotation();
                    localInputManagerService.setDisplayOrientation(0, i, j);
                    computeScreenConfigurationLocked(null);
                    if (!paramBoolean)
                        Surface.openTransaction();
                }
                label384: 
                try
                {
                    if ((this.mAnimator.mScreenRotationAnimation != null) && (this.mAnimator.mScreenRotationAnimation.hasScreenshot()) && (this.mAnimator.mScreenRotationAnimation.setRotation(i, this.mFxSession, 10000L, this.mTransitionAnimationScale, this.mCurDisplayWidth, this.mCurDisplayHeight)))
                        scheduleAnimationLocked();
                    Surface.setOrientation(0, i);
                    if (!paramBoolean)
                        Surface.closeTransaction();
                    rebuildBlackFrame();
                    int k = -1 + this.mWindows.size();
                    while (true)
                        if (k >= 0)
                        {
                            WindowState localWindowState = (WindowState)this.mWindows.get(k);
                            if (localWindowState.mHasSurface)
                            {
                                localWindowState.mOrientationChanging = true;
                                this.mInnerFields.mOrientationChangeComplete = false;
                            }
                            k--;
                            continue;
                            bool2 = false;
                            break;
                            j = 0;
                        }
                }
                finally
                {
                    if (!paramBoolean)
                        Surface.closeTransaction();
                }
            }
            try
            {
                ((IRotationWatcher)this.mRotationWatchers.get(m)).onRotationChanged(i);
                label407: m--;
                break label384;
                bool1 = true;
            }
            catch (RemoteException localRemoteException)
            {
                break label407;
            }
        }
    }

    void updateStatusBarVisibilityLocked(int paramInt)
    {
        this.mInputManager.setSystemUiVisibility(paramInt);
        int i = this.mWindows.size();
        int j = 0;
        while (true)
        {
            WindowState localWindowState;
            if (j < i)
                localWindowState = (WindowState)this.mWindows.get(j);
            try
            {
                int k = localWindowState.mSystemUiVisibility;
                int m = 0x7 & (k ^ paramInt) & (paramInt ^ 0xFFFFFFFF);
                int n = k & (m ^ 0xFFFFFFFF) | paramInt & m;
                if (n != k)
                {
                    localWindowState.mSeq = (1 + localWindowState.mSeq);
                    localWindowState.mSystemUiVisibility = n;
                }
                if ((n != k) || (localWindowState.mAttrs.hasSystemUiListeners))
                    localWindowState.mClient.dispatchSystemUiVisibilityChanged(localWindowState.mSeq, paramInt, n, m);
                label136: j++;
                continue;
                return;
            }
            catch (RemoteException localRemoteException)
            {
                break label136;
            }
        }
    }

    void updateWallpaperOffsetLocked(WindowState paramWindowState, boolean paramBoolean)
    {
        int i = this.mAppDisplayWidth;
        int j = this.mAppDisplayHeight;
        WindowState localWindowState1 = this.mWallpaperTarget;
        if (localWindowState1 != null)
        {
            if (localWindowState1.mWallpaperX < 0.0F)
                break label218;
            this.mLastWallpaperX = localWindowState1.mWallpaperX;
        }
        while (true)
        {
            label60: label101: WindowState localWindowState2;
            WindowStateAnimator localWindowStateAnimator;
            if (localWindowState1.mWallpaperY >= 0.0F)
            {
                this.mLastWallpaperY = localWindowState1.mWallpaperY;
                int k = this.mWallpaperTokens.size();
                do
                {
                    WindowToken localWindowToken;
                    int m;
                    while (m <= 0)
                    {
                        if (k <= 0)
                            break;
                        k--;
                        localWindowToken = (WindowToken)this.mWallpaperTokens.get(k);
                        m = localWindowToken.windows.size();
                    }
                    m--;
                    localWindowState2 = (WindowState)localWindowToken.windows.get(m);
                }
                while (!updateWallpaperOffsetLocked(localWindowState2, i, j, paramBoolean));
                localWindowStateAnimator = localWindowState2.mWinAnimator;
                localWindowStateAnimator.computeShownFrameLocked();
                if ((localWindowStateAnimator.mSurfaceX != localWindowState2.mShownFrame.left) || (localWindowStateAnimator.mSurfaceY != localWindowState2.mShownFrame.top))
                    Surface.openTransaction();
            }
            try
            {
                setWallpaperOffset(localWindowStateAnimator, (int)localWindowState2.mShownFrame.left, (int)localWindowState2.mShownFrame.top);
                Surface.closeTransaction();
                paramBoolean = false;
                break label101;
                label218: if (paramWindowState.mWallpaperX >= 0.0F)
                {
                    this.mLastWallpaperX = paramWindowState.mWallpaperX;
                    continue;
                    if (paramWindowState.mWallpaperY < 0.0F)
                        break label60;
                    this.mLastWallpaperY = paramWindowState.mWallpaperY;
                }
            }
            catch (RuntimeException localRuntimeException)
            {
                while (true)
                    Slog.w("WindowManager", "Error positioning surface of " + localWindowState2 + " pos=(" + localWindowState2.mShownFrame.left + "," + localWindowState2.mShownFrame.top + ")", localRuntimeException);
            }
        }
    }

    boolean updateWallpaperOffsetLocked(WindowState paramWindowState, int paramInt1, int paramInt2, boolean paramBoolean)
    {
        int i = 0;
        float f1;
        if (this.mLastWallpaperX >= 0.0F)
            f1 = this.mLastWallpaperX;
        while (true)
        {
            float f2;
            label33: int k;
            label71: boolean bool;
            label83: float f3;
            label144: float f4;
            label159: int n;
            if (this.mLastWallpaperXStep >= 0.0F)
            {
                f2 = this.mLastWallpaperXStep;
                int j = paramWindowState.mFrame.right - paramWindowState.mFrame.left - paramInt1;
                if (j <= 0)
                    break label422;
                k = -(int)(0.5F + f1 * j);
                if (paramWindowState.mXOffset == k)
                    break label428;
                bool = true;
                if (bool)
                    paramWindowState.mXOffset = k;
                if ((paramWindowState.mWallpaperX != f1) || (paramWindowState.mWallpaperXStep != f2))
                {
                    paramWindowState.mWallpaperX = f1;
                    paramWindowState.mWallpaperXStep = f2;
                    i = 1;
                }
                if (this.mLastWallpaperY < 0.0F)
                    break label434;
                f3 = this.mLastWallpaperY;
                if (this.mLastWallpaperYStep < 0.0F)
                    break label442;
                f4 = this.mLastWallpaperYStep;
                int m = paramWindowState.mFrame.bottom - paramWindowState.mFrame.top - paramInt2;
                if (m <= 0)
                    break label450;
                n = -(int)(0.5F + f3 * m);
                label197: if (paramWindowState.mYOffset != n)
                {
                    bool = true;
                    paramWindowState.mYOffset = n;
                }
                if ((paramWindowState.mWallpaperY != f3) || (paramWindowState.mWallpaperYStep != f4))
                {
                    paramWindowState.mWallpaperY = f3;
                    paramWindowState.mWallpaperYStep = f4;
                    i = 1;
                }
                if ((i != 0) && ((0x4 & paramWindowState.mAttrs.privateFlags) != 0) && (!paramBoolean));
            }
            try
            {
                this.mWaitingOnWallpaper = paramWindowState;
                paramWindowState.mClient.dispatchWallpaperOffsets(paramWindowState.mWallpaperX, paramWindowState.mWallpaperY, paramWindowState.mWallpaperXStep, paramWindowState.mWallpaperYStep, paramBoolean);
                if ((paramBoolean) && (this.mWaitingOnWallpaper != null))
                {
                    l1 = SystemClock.uptimeMillis();
                    long l2 = this.mLastWallpaperTimeoutTime;
                    if (l2 + 10000L >= l1);
                }
            }
            catch (RemoteException localRemoteException)
            {
                try
                {
                    long l1;
                    this.mWindowMap.wait(150L);
                    label349: long l3 = 150L + l1;
                    if (l3 < SystemClock.uptimeMillis())
                    {
                        Slog.i("WindowManager", "Timeout waiting for wallpaper to offset: " + paramWindowState);
                        this.mLastWallpaperTimeoutTime = l1;
                    }
                    this.mWaitingOnWallpaper = null;
                    while (true)
                    {
                        return bool;
                        f1 = 0.5F;
                        break;
                        f2 = -1.0F;
                        break label33;
                        label422: k = 0;
                        break label71;
                        label428: bool = false;
                        break label83;
                        label434: f3 = 0.5F;
                        break label144;
                        label442: f4 = -1.0F;
                        break label159;
                        label450: n = 0;
                        break label197;
                        localRemoteException = localRemoteException;
                    }
                }
                catch (InterruptedException localInterruptedException)
                {
                    break label349;
                }
            }
        }
    }

    void updateWallpaperVisibilityLocked()
    {
        boolean bool1 = isWallpaperVisible(this.mWallpaperTarget);
        int i = this.mAppDisplayWidth;
        int j = this.mAppDisplayHeight;
        int k = this.mWallpaperTokens.size();
        if (k > 0)
        {
            k--;
            WindowToken localWindowToken = (WindowToken)this.mWallpaperTokens.get(k);
            if (localWindowToken.hidden == bool1)
                if (bool1)
                    break label135;
            label135: for (boolean bool2 = true; ; bool2 = false)
            {
                localWindowToken.hidden = bool2;
                this.mLayoutNeeded = true;
                int m = localWindowToken.windows.size();
                while (m > 0)
                {
                    m--;
                    WindowState localWindowState = (WindowState)localWindowToken.windows.get(m);
                    if (bool1)
                        updateWallpaperOffsetLocked(localWindowState, i, j, false);
                    dispatchWallpaperVisibility(localWindowState, bool1);
                }
                break;
            }
        }
    }

    public void validateAppTokens(List<IBinder> paramList)
    {
        int i = -1 + paramList.size();
        int j = -1 + this.mAppTokens.size();
        while ((i >= 0) && (j >= 0))
        {
            AppWindowToken localAppWindowToken2 = (AppWindowToken)this.mAppTokens.get(j);
            if (localAppWindowToken2.removed)
            {
                j--;
            }
            else
            {
                if (paramList.get(i) != localAppWindowToken2.token)
                    Slog.w("WindowManager", "Tokens out of sync: external is " + paramList.get(i) + " @ " + i + ", internal is " + localAppWindowToken2.token + " @ " + j);
                i--;
                j--;
            }
        }
        while (i >= 0)
        {
            Slog.w("WindowManager", "External token not found: " + paramList.get(i) + " @ " + i);
            i--;
        }
        while (j >= 0)
        {
            AppWindowToken localAppWindowToken1 = (AppWindowToken)this.mAppTokens.get(j);
            if (!localAppWindowToken1.removed)
                Slog.w("WindowManager", "Invalid internal token: " + localAppWindowToken1.token + " @ " + j);
            j--;
        }
    }

    // ERROR //
    boolean viewServerGetFocusedWindow(java.net.Socket paramSocket)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 3249	com/android/server/wm/WindowManagerService:isSystemSecure	()Z
        //     4: ifeq +7 -> 11
        //     7: iconst_0
        //     8: istore_2
        //     9: iload_2
        //     10: ireturn
        //     11: iconst_1
        //     12: istore_2
        //     13: aload_0
        //     14: invokespecial 1139	com/android/server/wm/WindowManagerService:getFocusedWindow	()Lcom/android/server/wm/WindowState;
        //     17: astore_3
        //     18: aconst_null
        //     19: astore 4
        //     21: new 4056	java/io/BufferedWriter
        //     24: dup
        //     25: new 4058	java/io/OutputStreamWriter
        //     28: dup
        //     29: aload_1
        //     30: invokevirtual 4064	java/net/Socket:getOutputStream	()Ljava/io/OutputStream;
        //     33: invokespecial 4067	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;)V
        //     36: sipush 8192
        //     39: invokespecial 4070	java/io/BufferedWriter:<init>	(Ljava/io/Writer;I)V
        //     42: astore 5
        //     44: aload_3
        //     45: ifnull +35 -> 80
        //     48: aload 5
        //     50: aload_3
        //     51: invokestatic 1144	java/lang/System:identityHashCode	(Ljava/lang/Object;)I
        //     54: invokestatic 2486	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     57: invokevirtual 4073	java/io/BufferedWriter:write	(Ljava/lang/String;)V
        //     60: aload 5
        //     62: bipush 32
        //     64: invokevirtual 4075	java/io/BufferedWriter:write	(I)V
        //     67: aload 5
        //     69: aload_3
        //     70: getfield 844	com/android/server/wm/WindowState:mAttrs	Landroid/view/WindowManager$LayoutParams;
        //     73: invokevirtual 2814	android/view/WindowManager$LayoutParams:getTitle	()Ljava/lang/CharSequence;
        //     76: invokevirtual 4078	java/io/BufferedWriter:append	(Ljava/lang/CharSequence;)Ljava/io/Writer;
        //     79: pop
        //     80: aload 5
        //     82: bipush 10
        //     84: invokevirtual 4075	java/io/BufferedWriter:write	(I)V
        //     87: aload 5
        //     89: invokevirtual 4079	java/io/BufferedWriter:flush	()V
        //     92: aload 5
        //     94: ifnull +80 -> 174
        //     97: aload 5
        //     99: invokevirtual 4080	java/io/BufferedWriter:close	()V
        //     102: goto -93 -> 9
        //     105: astore 10
        //     107: iconst_0
        //     108: istore_2
        //     109: goto -100 -> 9
        //     112: astore 12
        //     114: iconst_0
        //     115: istore_2
        //     116: aload 4
        //     118: ifnull -109 -> 9
        //     121: aload 4
        //     123: invokevirtual 4080	java/io/BufferedWriter:close	()V
        //     126: goto -117 -> 9
        //     129: astore 7
        //     131: iconst_0
        //     132: istore_2
        //     133: goto -124 -> 9
        //     136: astore 8
        //     138: aload 4
        //     140: ifnull +8 -> 148
        //     143: aload 4
        //     145: invokevirtual 4080	java/io/BufferedWriter:close	()V
        //     148: aload 8
        //     150: athrow
        //     151: astore 9
        //     153: goto -5 -> 148
        //     156: astore 8
        //     158: aload 5
        //     160: astore 4
        //     162: goto -24 -> 138
        //     165: astore 6
        //     167: aload 5
        //     169: astore 4
        //     171: goto -57 -> 114
        //     174: goto -165 -> 9
        //
        // Exception table:
        //     from	to	target	type
        //     97	102	105	java/io/IOException
        //     21	44	112	java/lang/Exception
        //     121	126	129	java/io/IOException
        //     21	44	136	finally
        //     143	148	151	java/io/IOException
        //     48	92	156	finally
        //     48	92	165	java/lang/Exception
    }

    // ERROR //
    boolean viewServerListWindows(java.net.Socket paramSocket)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 3249	com/android/server/wm/WindowManagerService:isSystemSecure	()Z
        //     4: ifeq +7 -> 11
        //     7: iconst_0
        //     8: istore_2
        //     9: iload_2
        //     10: ireturn
        //     11: iconst_1
        //     12: istore_2
        //     13: aload_0
        //     14: getfield 378	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
        //     17: astore_3
        //     18: aload_3
        //     19: monitorenter
        //     20: aload_0
        //     21: getfield 395	com/android/server/wm/WindowManagerService:mWindows	Ljava/util/ArrayList;
        //     24: aload_0
        //     25: getfield 395	com/android/server/wm/WindowManagerService:mWindows	Ljava/util/ArrayList;
        //     28: invokevirtual 806	java/util/ArrayList:size	()I
        //     31: anewarray 403	com/android/server/wm/WindowState
        //     34: invokevirtual 1383	java/util/ArrayList:toArray	([Ljava/lang/Object;)[Ljava/lang/Object;
        //     37: checkcast 4082	[Lcom/android/server/wm/WindowState;
        //     40: astore 5
        //     42: aload_3
        //     43: monitorexit
        //     44: aconst_null
        //     45: astore 6
        //     47: new 4056	java/io/BufferedWriter
        //     50: dup
        //     51: new 4058	java/io/OutputStreamWriter
        //     54: dup
        //     55: aload_1
        //     56: invokevirtual 4064	java/net/Socket:getOutputStream	()Ljava/io/OutputStream;
        //     59: invokespecial 4067	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;)V
        //     62: sipush 8192
        //     65: invokespecial 4070	java/io/BufferedWriter:<init>	(Ljava/io/Writer;I)V
        //     68: astore 7
        //     70: aload 5
        //     72: arraylength
        //     73: istore 12
        //     75: iconst_0
        //     76: istore 13
        //     78: iload 13
        //     80: iload 12
        //     82: if_icmpge +64 -> 146
        //     85: aload 5
        //     87: iload 13
        //     89: aaload
        //     90: astore 15
        //     92: aload 7
        //     94: aload 15
        //     96: invokestatic 1144	java/lang/System:identityHashCode	(Ljava/lang/Object;)I
        //     99: invokestatic 2486	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     102: invokevirtual 4073	java/io/BufferedWriter:write	(Ljava/lang/String;)V
        //     105: aload 7
        //     107: bipush 32
        //     109: invokevirtual 4075	java/io/BufferedWriter:write	(I)V
        //     112: aload 7
        //     114: aload 15
        //     116: getfield 844	com/android/server/wm/WindowState:mAttrs	Landroid/view/WindowManager$LayoutParams;
        //     119: invokevirtual 2814	android/view/WindowManager$LayoutParams:getTitle	()Ljava/lang/CharSequence;
        //     122: invokevirtual 4078	java/io/BufferedWriter:append	(Ljava/lang/CharSequence;)Ljava/io/Writer;
        //     125: pop
        //     126: aload 7
        //     128: bipush 10
        //     130: invokevirtual 4075	java/io/BufferedWriter:write	(I)V
        //     133: iinc 13 1
        //     136: goto -58 -> 78
        //     139: astore 4
        //     141: aload_3
        //     142: monitorexit
        //     143: aload 4
        //     145: athrow
        //     146: aload 7
        //     148: ldc_w 4084
        //     151: invokevirtual 4073	java/io/BufferedWriter:write	(Ljava/lang/String;)V
        //     154: aload 7
        //     156: invokevirtual 4079	java/io/BufferedWriter:flush	()V
        //     159: aload 7
        //     161: ifnull +80 -> 241
        //     164: aload 7
        //     166: invokevirtual 4080	java/io/BufferedWriter:close	()V
        //     169: goto -160 -> 9
        //     172: astore 14
        //     174: iconst_0
        //     175: istore_2
        //     176: goto -167 -> 9
        //     179: astore 17
        //     181: iconst_0
        //     182: istore_2
        //     183: aload 6
        //     185: ifnull -176 -> 9
        //     188: aload 6
        //     190: invokevirtual 4080	java/io/BufferedWriter:close	()V
        //     193: goto -184 -> 9
        //     196: astore 9
        //     198: iconst_0
        //     199: istore_2
        //     200: goto -191 -> 9
        //     203: astore 10
        //     205: aload 6
        //     207: ifnull +8 -> 215
        //     210: aload 6
        //     212: invokevirtual 4080	java/io/BufferedWriter:close	()V
        //     215: aload 10
        //     217: athrow
        //     218: astore 11
        //     220: goto -5 -> 215
        //     223: astore 10
        //     225: aload 7
        //     227: astore 6
        //     229: goto -24 -> 205
        //     232: astore 8
        //     234: aload 7
        //     236: astore 6
        //     238: goto -57 -> 181
        //     241: goto -232 -> 9
        //
        // Exception table:
        //     from	to	target	type
        //     20	44	139	finally
        //     141	143	139	finally
        //     164	169	172	java/io/IOException
        //     47	70	179	java/lang/Exception
        //     188	193	196	java/io/IOException
        //     47	70	203	finally
        //     210	215	218	java/io/IOException
        //     70	133	223	finally
        //     146	159	223	finally
        //     70	133	232	java/lang/Exception
        //     146	159	232	java/lang/Exception
    }

    // ERROR //
    boolean viewServerWindowCommand(java.net.Socket paramSocket, String paramString1, String paramString2)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 3249	com/android/server/wm/WindowManagerService:isSystemSecure	()Z
        //     4: ifeq +9 -> 13
        //     7: iconst_0
        //     8: istore 4
        //     10: iload 4
        //     12: ireturn
        //     13: iconst_1
        //     14: istore 4
        //     16: aconst_null
        //     17: astore 5
        //     19: aconst_null
        //     20: astore 6
        //     22: aconst_null
        //     23: astore 7
        //     25: aload_3
        //     26: bipush 32
        //     28: invokevirtual 1815	java/lang/String:indexOf	(I)I
        //     31: istore 13
        //     33: iload 13
        //     35: bipush 255
        //     37: if_icmpne +9 -> 46
        //     40: aload_3
        //     41: invokevirtual 1195	java/lang/String:length	()I
        //     44: istore 13
        //     46: aload_3
        //     47: iconst_0
        //     48: iload 13
        //     50: invokevirtual 1822	java/lang/String:substring	(II)Ljava/lang/String;
        //     53: bipush 16
        //     55: invokestatic 4092	java/lang/Long:parseLong	(Ljava/lang/String;I)J
        //     58: l2i
        //     59: istore 14
        //     61: iload 13
        //     63: aload_3
        //     64: invokevirtual 1195	java/lang/String:length	()I
        //     67: if_icmpge +55 -> 122
        //     70: iload 13
        //     72: iconst_1
        //     73: iadd
        //     74: istore 20
        //     76: aload_3
        //     77: iload 20
        //     79: invokevirtual 1824	java/lang/String:substring	(I)Ljava/lang/String;
        //     82: astore_3
        //     83: aload_0
        //     84: iload 14
        //     86: invokespecial 4094	com/android/server/wm/WindowManagerService:findWindow	(I)Lcom/android/server/wm/WindowState;
        //     89: astore 15
        //     91: aload 15
        //     93: ifnonnull +36 -> 129
        //     96: iconst_0
        //     97: istore 4
        //     99: iconst_0
        //     100: ifeq +5 -> 105
        //     103: aconst_null
        //     104: athrow
        //     105: iconst_0
        //     106: ifeq +5 -> 111
        //     109: aconst_null
        //     110: athrow
        //     111: iconst_0
        //     112: ifeq -102 -> 10
        //     115: aconst_null
        //     116: athrow
        //     117: astore 19
        //     119: goto -109 -> 10
        //     122: ldc_w 2351
        //     125: astore_3
        //     126: goto -43 -> 83
        //     129: invokestatic 3365	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //     132: astore 5
        //     134: aload 5
        //     136: ldc_w 4096
        //     139: invokevirtual 3370	android/os/Parcel:writeInterfaceToken	(Ljava/lang/String;)V
        //     142: aload 5
        //     144: aload_2
        //     145: invokevirtual 4099	android/os/Parcel:writeString	(Ljava/lang/String;)V
        //     148: aload 5
        //     150: aload_3
        //     151: invokevirtual 4099	android/os/Parcel:writeString	(Ljava/lang/String;)V
        //     154: aload 5
        //     156: iconst_1
        //     157: invokevirtual 4102	android/os/Parcel:writeInt	(I)V
        //     160: aload_1
        //     161: invokestatic 4108	android/os/ParcelFileDescriptor:fromSocket	(Ljava/net/Socket;)Landroid/os/ParcelFileDescriptor;
        //     164: aload 5
        //     166: iconst_0
        //     167: invokevirtual 4112	android/os/ParcelFileDescriptor:writeToParcel	(Landroid/os/Parcel;I)V
        //     170: invokestatic 3365	android/os/Parcel:obtain	()Landroid/os/Parcel;
        //     173: astore 6
        //     175: aload 15
        //     177: getfield 825	com/android/server/wm/WindowState:mClient	Landroid/view/IWindow;
        //     180: invokeinterface 874 1 0
        //     185: iconst_1
        //     186: aload 5
        //     188: aload 6
        //     190: iconst_0
        //     191: invokeinterface 3373 5 0
        //     196: pop
        //     197: aload 6
        //     199: invokevirtual 4115	android/os/Parcel:readException	()V
        //     202: aload_1
        //     203: invokevirtual 4118	java/net/Socket:isOutputShutdown	()Z
        //     206: ifne +40 -> 246
        //     209: new 4056	java/io/BufferedWriter
        //     212: dup
        //     213: new 4058	java/io/OutputStreamWriter
        //     216: dup
        //     217: aload_1
        //     218: invokevirtual 4064	java/net/Socket:getOutputStream	()Ljava/io/OutputStream;
        //     221: invokespecial 4067	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;)V
        //     224: invokespecial 4119	java/io/BufferedWriter:<init>	(Ljava/io/Writer;)V
        //     227: astore 17
        //     229: aload 17
        //     231: ldc_w 4121
        //     234: invokevirtual 4073	java/io/BufferedWriter:write	(Ljava/lang/String;)V
        //     237: aload 17
        //     239: invokevirtual 4079	java/io/BufferedWriter:flush	()V
        //     242: aload 17
        //     244: astore 7
        //     246: aload 5
        //     248: ifnull +8 -> 256
        //     251: aload 5
        //     253: invokevirtual 3376	android/os/Parcel:recycle	()V
        //     256: aload 6
        //     258: ifnull +8 -> 266
        //     261: aload 6
        //     263: invokevirtual 3376	android/os/Parcel:recycle	()V
        //     266: aload 7
        //     268: ifnull -258 -> 10
        //     271: aload 7
        //     273: invokevirtual 4080	java/io/BufferedWriter:close	()V
        //     276: goto -266 -> 10
        //     279: astore 18
        //     281: goto -271 -> 10
        //     284: astore 10
        //     286: ldc 117
        //     288: new 1311	java/lang/StringBuilder
        //     291: dup
        //     292: invokespecial 1312	java/lang/StringBuilder:<init>	()V
        //     295: ldc_w 4123
        //     298: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     301: aload_2
        //     302: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     305: ldc_w 4125
        //     308: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     311: aload_3
        //     312: invokevirtual 1318	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     315: invokevirtual 1329	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     318: aload 10
        //     320: invokestatic 2093	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     323: pop
        //     324: iconst_0
        //     325: istore 4
        //     327: aload 5
        //     329: ifnull +8 -> 337
        //     332: aload 5
        //     334: invokevirtual 3376	android/os/Parcel:recycle	()V
        //     337: aload 6
        //     339: ifnull +8 -> 347
        //     342: aload 6
        //     344: invokevirtual 3376	android/os/Parcel:recycle	()V
        //     347: aload 7
        //     349: ifnull -339 -> 10
        //     352: aload 7
        //     354: invokevirtual 4080	java/io/BufferedWriter:close	()V
        //     357: goto -347 -> 10
        //     360: astore 12
        //     362: goto -352 -> 10
        //     365: astore 8
        //     367: aload 5
        //     369: ifnull +8 -> 377
        //     372: aload 5
        //     374: invokevirtual 3376	android/os/Parcel:recycle	()V
        //     377: aload 6
        //     379: ifnull +8 -> 387
        //     382: aload 6
        //     384: invokevirtual 3376	android/os/Parcel:recycle	()V
        //     387: aload 7
        //     389: ifnull +8 -> 397
        //     392: aload 7
        //     394: invokevirtual 4080	java/io/BufferedWriter:close	()V
        //     397: aload 8
        //     399: athrow
        //     400: astore 9
        //     402: goto -5 -> 397
        //     405: astore 8
        //     407: aload 17
        //     409: astore 7
        //     411: goto -44 -> 367
        //     414: astore 10
        //     416: aload 17
        //     418: astore 7
        //     420: goto -134 -> 286
        //
        // Exception table:
        //     from	to	target	type
        //     115	117	117	java/io/IOException
        //     271	276	279	java/io/IOException
        //     25	91	284	java/lang/Exception
        //     122	229	284	java/lang/Exception
        //     352	357	360	java/io/IOException
        //     25	91	365	finally
        //     122	229	365	finally
        //     286	324	365	finally
        //     392	397	400	java/io/IOException
        //     229	242	405	finally
        //     229	242	414	java/lang/Exception
    }

    public void waitForWindowDrawn(IBinder paramIBinder, IRemoteCallback paramIRemoteCallback)
    {
        synchronized (this.mWindowMap)
        {
            WindowState localWindowState = windowForClientLocked(null, paramIBinder, true);
            if (localWindowState != null)
            {
                Pair localPair = new Pair(localWindowState, paramIRemoteCallback);
                Message localMessage = this.mH.obtainMessage(24, localPair);
                this.mH.sendMessageDelayed(localMessage, 2000L);
                this.mWaitingForDrawn.add(localPair);
                checkDrawnWindowsLocked();
            }
            return;
        }
    }

    void wallpaperCommandComplete(IBinder paramIBinder, Bundle paramBundle)
    {
        synchronized (this.mWindowMap)
        {
            if ((this.mWaitingOnWallpaper != null) && (this.mWaitingOnWallpaper.mClient.asBinder() == paramIBinder))
            {
                this.mWaitingOnWallpaper = null;
                this.mWindowMap.notifyAll();
            }
            return;
        }
    }

    void wallpaperOffsetsComplete(IBinder paramIBinder)
    {
        synchronized (this.mWindowMap)
        {
            if ((this.mWaitingOnWallpaper != null) && (this.mWaitingOnWallpaper.mClient.asBinder() == paramIBinder))
            {
                this.mWaitingOnWallpaper = null;
                this.mWindowMap.notifyAll();
            }
            return;
        }
    }

    public int watchRotation(IRotationWatcher paramIRotationWatcher)
    {
        IBinder.DeathRecipient local4 = new IBinder.DeathRecipient()
        {
            public void binderDied()
            {
                HashMap localHashMap = WindowManagerService.this.mWindowMap;
                for (int i = 0; ; i++)
                {
                    try
                    {
                        if (i < WindowManagerService.this.mRotationWatchers.size())
                        {
                            if (this.val$watcherBinder != ((IRotationWatcher)WindowManagerService.this.mRotationWatchers.get(i)).asBinder())
                                continue;
                            IRotationWatcher localIRotationWatcher = (IRotationWatcher)WindowManagerService.this.mRotationWatchers.remove(i);
                            if (localIRotationWatcher != null)
                                localIRotationWatcher.asBinder().unlinkToDeath(this, 0);
                        }
                        else
                        {
                            return;
                        }
                    }
                    finally
                    {
                        localObject = finally;
                        throw localObject;
                    }
                    i--;
                }
            }
        };
        try
        {
            synchronized (this.mWindowMap)
            {
                paramIRotationWatcher.asBinder().linkToDeath(local4, 0);
                this.mRotationWatchers.add(paramIRotationWatcher);
                label44: int i = this.mRotation;
                return i;
            }
        }
        catch (RemoteException localRemoteException)
        {
            break label44;
        }
    }

    final WindowState windowForClientLocked(Session paramSession, IBinder paramIBinder, boolean paramBoolean)
    {
        WindowState localWindowState = (WindowState)this.mWindowMap.get(paramIBinder);
        if (localWindowState == null)
        {
            IllegalArgumentException localIllegalArgumentException1 = new IllegalArgumentException("Requested window " + paramIBinder + " does not exist");
            if (paramBoolean)
                throw localIllegalArgumentException1;
            Slog.w("WindowManager", "Failed looking up window", localIllegalArgumentException1);
        }
        for (localWindowState = null; ; localWindowState = null)
        {
            do
                return localWindowState;
            while ((paramSession == null) || (localWindowState.mSession == paramSession));
            IllegalArgumentException localIllegalArgumentException2 = new IllegalArgumentException("Requested window " + paramIBinder + " is in session " + localWindowState.mSession + ", not " + paramSession);
            if (paramBoolean)
                throw localIllegalArgumentException2;
            Slog.w("WindowManager", "Failed looking up window", localIllegalArgumentException2);
        }
    }

    final WindowState windowForClientLocked(Session paramSession, IWindow paramIWindow, boolean paramBoolean)
    {
        return windowForClientLocked(paramSession, paramIWindow.asBinder(), paramBoolean);
    }

    public static abstract interface OnHardKeyboardStatusChangeListener
    {
        public abstract void onHardKeyboardStatusChange(boolean paramBoolean1, boolean paramBoolean2);
    }

    final class H extends Handler
    {
        public static final int ADD_STARTING = 5;
        public static final int ANIMATOR_WHAT_OFFSET = 100000;
        public static final int APP_FREEZE_TIMEOUT = 17;
        public static final int APP_TRANSITION_TIMEOUT = 13;
        public static final int BOOT_TIMEOUT = 23;
        public static final int BULK_UPDATE_PARAMETERS = 25;
        public static final int CLEAR_PENDING_ACTIONS = 100004;
        public static final int DO_ANIMATION_CALLBACK = 27;
        public static final int DO_TRAVERSAL = 4;
        public static final int DRAG_END_TIMEOUT = 21;
        public static final int DRAG_START_TIMEOUT = 20;
        public static final int ENABLE_SCREEN = 16;
        public static final int FINISHED_STARTING = 7;
        public static final int FORCE_GC = 15;
        public static final int HOLD_SCREEN_CHANGED = 12;
        public static final int PERSIST_ANIMATION_SCALE = 14;
        public static final int REMOVE_STARTING = 6;
        public static final int REPORT_APPLICATION_TOKEN_DRAWN = 9;
        public static final int REPORT_APPLICATION_TOKEN_WINDOWS = 8;
        public static final int REPORT_FOCUS_CHANGE = 2;
        public static final int REPORT_HARD_KEYBOARD_STATUS_CHANGE = 22;
        public static final int REPORT_LOSING_FOCUS = 3;
        public static final int REPORT_WINDOWS_CHANGE = 19;
        public static final int SEND_NEW_CONFIGURATION = 18;
        public static final int SET_DIM_PARAMETERS = 100003;
        public static final int SET_TRANSPARENT_REGION = 100001;
        public static final int SET_WALLPAPER_OFFSET = 100002;
        public static final int SHOW_STRICT_MODE_VIOLATION = 26;
        public static final int WAITING_FOR_DRAWN_TIMEOUT = 24;
        public static final int WINDOW_FREEZE_TIMEOUT = 11;
        private Session mLastReportedHold;

        public H()
        {
        }

        // ERROR //
        public void handleMessage(Message paramMessage)
        {
            // Byte code:
            //     0: aload_1
            //     1: getfield 90	android/os/Message:what	I
            //     4: lookupswitch	default:+244->248, 2:+245->249, 3:+410->414, 4:+505->509, 5:+546->550, 6:+765->769, 7:+888->892, 8:+1066->1070, 9:+1039->1043, 11:+1123->1127, 12:+1246->1250, 13:+1361->1365, 14:+1450->1454, 15:+1525->1529, 16:+1615->1619, 17:+1625->1629, 18:+1772->1776, 19:+1788->1792, 20:+1840->1844, 21:+1923->1927, 22:+1988->1992, 23:+1998->2002, 24:+2008->2012, 25:+2113->2117, 26:+2341->2345, 27:+2451->2455, 100001:+2355->2359, 100002:+2383->2387, 100003:+2411->2415, 100004:+2438->2442
            //     249: aload_0
            //     250: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     253: getfield 94	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
            //     256: astore 92
            //     258: aload 92
            //     260: monitorenter
            //     261: aload_0
            //     262: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     265: getfield 98	com/android/server/wm/WindowManagerService:mLastFocus	Lcom/android/server/wm/WindowState;
            //     268: astore 94
            //     270: aload_0
            //     271: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     274: getfield 101	com/android/server/wm/WindowManagerService:mCurrentFocus	Lcom/android/server/wm/WindowState;
            //     277: astore 95
            //     279: aload 94
            //     281: aload 95
            //     283: if_acmpne +17 -> 300
            //     286: aload 92
            //     288: monitorexit
            //     289: goto -41 -> 248
            //     292: astore 93
            //     294: aload 92
            //     296: monitorexit
            //     297: aload 93
            //     299: athrow
            //     300: aload_0
            //     301: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     304: aload 95
            //     306: putfield 98	com/android/server/wm/WindowManagerService:mLastFocus	Lcom/android/server/wm/WindowState;
            //     309: aload 95
            //     311: ifnull +32 -> 343
            //     314: aload 94
            //     316: ifnull +27 -> 343
            //     319: aload 95
            //     321: invokevirtual 107	com/android/server/wm/WindowState:isDisplayedLw	()Z
            //     324: ifne +19 -> 343
            //     327: aload_0
            //     328: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     331: getfield 111	com/android/server/wm/WindowManagerService:mLosingFocus	Ljava/util/ArrayList;
            //     334: aload 94
            //     336: invokevirtual 117	java/util/ArrayList:add	(Ljava/lang/Object;)Z
            //     339: pop
            //     340: aconst_null
            //     341: astore 94
            //     343: aload 92
            //     345: monitorexit
            //     346: aload 94
            //     348: aload 95
            //     350: if_acmpeq -102 -> 248
            //     353: aload 95
            //     355: ifnull +28 -> 383
            //     358: aload 95
            //     360: getfield 121	com/android/server/wm/WindowState:mClient	Landroid/view/IWindow;
            //     363: iconst_1
            //     364: aload_0
            //     365: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     368: getfield 125	com/android/server/wm/WindowManagerService:mInTouchMode	Z
            //     371: invokeinterface 131 3 0
            //     376: aload_0
            //     377: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     380: invokestatic 134	com/android/server/wm/WindowManagerService:access$500	(Lcom/android/server/wm/WindowManagerService;)V
            //     383: aload 94
            //     385: ifnull -137 -> 248
            //     388: aload 94
            //     390: getfield 121	com/android/server/wm/WindowState:mClient	Landroid/view/IWindow;
            //     393: iconst_0
            //     394: aload_0
            //     395: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     398: getfield 125	com/android/server/wm/WindowManagerService:mInTouchMode	Z
            //     401: invokeinterface 131 3 0
            //     406: goto -158 -> 248
            //     409: astore 96
            //     411: goto -163 -> 248
            //     414: aload_0
            //     415: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     418: getfield 94	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
            //     421: astore 86
            //     423: aload 86
            //     425: monitorenter
            //     426: aload_0
            //     427: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     430: getfield 111	com/android/server/wm/WindowManagerService:mLosingFocus	Ljava/util/ArrayList;
            //     433: astore 88
            //     435: aload_0
            //     436: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     439: new 113	java/util/ArrayList
            //     442: dup
            //     443: invokespecial 135	java/util/ArrayList:<init>	()V
            //     446: putfield 111	com/android/server/wm/WindowManagerService:mLosingFocus	Ljava/util/ArrayList;
            //     449: aload 86
            //     451: monitorexit
            //     452: aload 88
            //     454: invokevirtual 139	java/util/ArrayList:size	()I
            //     457: istore 89
            //     459: iconst_0
            //     460: istore 90
            //     462: iload 90
            //     464: iload 89
            //     466: if_icmpge -218 -> 248
            //     469: aload 88
            //     471: iload 90
            //     473: invokevirtual 143	java/util/ArrayList:get	(I)Ljava/lang/Object;
            //     476: checkcast 103	com/android/server/wm/WindowState
            //     479: getfield 121	com/android/server/wm/WindowState:mClient	Landroid/view/IWindow;
            //     482: iconst_0
            //     483: aload_0
            //     484: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     487: getfield 125	com/android/server/wm/WindowManagerService:mInTouchMode	Z
            //     490: invokeinterface 131 3 0
            //     495: iinc 90 1
            //     498: goto -36 -> 462
            //     501: astore 87
            //     503: aload 86
            //     505: monitorexit
            //     506: aload 87
            //     508: athrow
            //     509: aload_0
            //     510: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     513: getfield 94	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
            //     516: astore 84
            //     518: aload 84
            //     520: monitorenter
            //     521: aload_0
            //     522: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     525: iconst_0
            //     526: putfield 146	com/android/server/wm/WindowManagerService:mTraversalScheduled	Z
            //     529: aload_0
            //     530: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     533: invokestatic 149	com/android/server/wm/WindowManagerService:access$600	(Lcom/android/server/wm/WindowManagerService;)V
            //     536: aload 84
            //     538: monitorexit
            //     539: goto -291 -> 248
            //     542: astore 85
            //     544: aload 84
            //     546: monitorexit
            //     547: aload 85
            //     549: athrow
            //     550: aload_1
            //     551: getfield 153	android/os/Message:obj	Ljava/lang/Object;
            //     554: checkcast 155	com/android/server/wm/AppWindowToken
            //     557: astore 73
            //     559: aload 73
            //     561: getfield 159	com/android/server/wm/AppWindowToken:startingData	Lcom/android/server/wm/StartingData;
            //     564: astore 74
            //     566: aload 74
            //     568: ifnull -320 -> 248
            //     571: aconst_null
            //     572: astore 75
            //     574: aload_0
            //     575: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     578: getfield 163	com/android/server/wm/WindowManagerService:mPolicy	Landroid/view/WindowManagerPolicy;
            //     581: aload 73
            //     583: getfield 169	com/android/server/wm/WindowToken:token	Landroid/os/IBinder;
            //     586: aload 74
            //     588: getfield 175	com/android/server/wm/StartingData:pkg	Ljava/lang/String;
            //     591: aload 74
            //     593: getfield 178	com/android/server/wm/StartingData:theme	I
            //     596: aload 74
            //     598: getfield 182	com/android/server/wm/StartingData:compatInfo	Landroid/content/res/CompatibilityInfo;
            //     601: aload 74
            //     603: getfield 186	com/android/server/wm/StartingData:nonLocalizedLabel	Ljava/lang/CharSequence;
            //     606: aload 74
            //     608: getfield 189	com/android/server/wm/StartingData:labelRes	I
            //     611: aload 74
            //     613: getfield 192	com/android/server/wm/StartingData:icon	I
            //     616: aload 74
            //     618: getfield 195	com/android/server/wm/StartingData:windowFlags	I
            //     621: invokeinterface 201 9 0
            //     626: astore 83
            //     628: aload 83
            //     630: astore 75
            //     632: aload 75
            //     634: ifnull -386 -> 248
            //     637: iconst_0
            //     638: istore 78
            //     640: aload_0
            //     641: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     644: getfield 94	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
            //     647: astore 79
            //     649: aload 79
            //     651: monitorenter
            //     652: aload 73
            //     654: getfield 204	com/android/server/wm/AppWindowToken:removed	Z
            //     657: ifne +11 -> 668
            //     660: aload 73
            //     662: getfield 159	com/android/server/wm/AppWindowToken:startingData	Lcom/android/server/wm/StartingData;
            //     665: ifnonnull +86 -> 751
            //     668: aload 73
            //     670: getfield 207	com/android/server/wm/AppWindowToken:startingWindow	Lcom/android/server/wm/WindowState;
            //     673: ifnull +18 -> 691
            //     676: aload 73
            //     678: aconst_null
            //     679: putfield 207	com/android/server/wm/AppWindowToken:startingWindow	Lcom/android/server/wm/WindowState;
            //     682: aload 73
            //     684: aconst_null
            //     685: putfield 159	com/android/server/wm/AppWindowToken:startingData	Lcom/android/server/wm/StartingData;
            //     688: iconst_1
            //     689: istore 78
            //     691: aload 79
            //     693: monitorexit
            //     694: iload 78
            //     696: ifeq -448 -> 248
            //     699: aload_0
            //     700: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     703: getfield 163	com/android/server/wm/WindowManagerService:mPolicy	Landroid/view/WindowManagerPolicy;
            //     706: aload 73
            //     708: getfield 169	com/android/server/wm/WindowToken:token	Landroid/os/IBinder;
            //     711: aload 75
            //     713: invokeinterface 211 3 0
            //     718: goto -470 -> 248
            //     721: astore 81
            //     723: ldc 213
            //     725: ldc 215
            //     727: aload 81
            //     729: invokestatic 221	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     732: pop
            //     733: goto -485 -> 248
            //     736: astore 76
            //     738: ldc 213
            //     740: ldc 223
            //     742: aload 76
            //     744: invokestatic 221	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     747: pop
            //     748: goto -116 -> 632
            //     751: aload 73
            //     753: aload 75
            //     755: putfield 227	com/android/server/wm/AppWindowToken:startingView	Landroid/view/View;
            //     758: goto -67 -> 691
            //     761: astore 80
            //     763: aload 79
            //     765: monitorexit
            //     766: aload 80
            //     768: athrow
            //     769: aload_1
            //     770: getfield 153	android/os/Message:obj	Ljava/lang/Object;
            //     773: checkcast 155	com/android/server/wm/AppWindowToken
            //     776: astore 66
            //     778: aconst_null
            //     779: astore 67
            //     781: aconst_null
            //     782: astore 68
            //     784: aload_0
            //     785: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     788: getfield 94	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
            //     791: astore 69
            //     793: aload 69
            //     795: monitorenter
            //     796: aload 66
            //     798: getfield 207	com/android/server/wm/AppWindowToken:startingWindow	Lcom/android/server/wm/WindowState;
            //     801: ifnull +41 -> 842
            //     804: aload 66
            //     806: getfield 227	com/android/server/wm/AppWindowToken:startingView	Landroid/view/View;
            //     809: astore 68
            //     811: aload 66
            //     813: getfield 169	com/android/server/wm/WindowToken:token	Landroid/os/IBinder;
            //     816: astore 67
            //     818: aload 66
            //     820: aconst_null
            //     821: putfield 159	com/android/server/wm/AppWindowToken:startingData	Lcom/android/server/wm/StartingData;
            //     824: aload 66
            //     826: aconst_null
            //     827: putfield 227	com/android/server/wm/AppWindowToken:startingView	Landroid/view/View;
            //     830: aload 66
            //     832: aconst_null
            //     833: putfield 207	com/android/server/wm/AppWindowToken:startingWindow	Lcom/android/server/wm/WindowState;
            //     836: aload 66
            //     838: iconst_0
            //     839: putfield 230	com/android/server/wm/AppWindowToken:startingDisplayed	Z
            //     842: aload 69
            //     844: monitorexit
            //     845: aload 68
            //     847: ifnull -599 -> 248
            //     850: aload_0
            //     851: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     854: getfield 163	com/android/server/wm/WindowManagerService:mPolicy	Landroid/view/WindowManagerPolicy;
            //     857: aload 67
            //     859: aload 68
            //     861: invokeinterface 211 3 0
            //     866: goto -618 -> 248
            //     869: astore 71
            //     871: ldc 213
            //     873: ldc 215
            //     875: aload 71
            //     877: invokestatic 221	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     880: pop
            //     881: goto -633 -> 248
            //     884: astore 70
            //     886: aload 69
            //     888: monitorexit
            //     889: aload 70
            //     891: athrow
            //     892: aload_0
            //     893: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     896: getfield 94	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
            //     899: astore 58
            //     901: aload 58
            //     903: monitorenter
            //     904: aload_0
            //     905: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     908: getfield 233	com/android/server/wm/WindowManagerService:mFinishedStarting	Ljava/util/ArrayList;
            //     911: invokevirtual 139	java/util/ArrayList:size	()I
            //     914: istore 60
            //     916: iload 60
            //     918: ifgt +17 -> 935
            //     921: aload 58
            //     923: monitorexit
            //     924: goto -676 -> 248
            //     927: astore 59
            //     929: aload 58
            //     931: monitorexit
            //     932: aload 59
            //     934: athrow
            //     935: aload_0
            //     936: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     939: getfield 233	com/android/server/wm/WindowManagerService:mFinishedStarting	Ljava/util/ArrayList;
            //     942: iload 60
            //     944: iconst_1
            //     945: isub
            //     946: invokevirtual 236	java/util/ArrayList:remove	(I)Ljava/lang/Object;
            //     949: checkcast 155	com/android/server/wm/AppWindowToken
            //     952: astore 61
            //     954: aload 61
            //     956: getfield 207	com/android/server/wm/AppWindowToken:startingWindow	Lcom/android/server/wm/WindowState;
            //     959: ifnonnull +9 -> 968
            //     962: aload 58
            //     964: monitorexit
            //     965: goto -73 -> 892
            //     968: aload 61
            //     970: getfield 227	com/android/server/wm/AppWindowToken:startingView	Landroid/view/View;
            //     973: astore 62
            //     975: aload 61
            //     977: getfield 169	com/android/server/wm/WindowToken:token	Landroid/os/IBinder;
            //     980: astore 63
            //     982: aload 61
            //     984: aconst_null
            //     985: putfield 159	com/android/server/wm/AppWindowToken:startingData	Lcom/android/server/wm/StartingData;
            //     988: aload 61
            //     990: aconst_null
            //     991: putfield 227	com/android/server/wm/AppWindowToken:startingView	Landroid/view/View;
            //     994: aload 61
            //     996: aconst_null
            //     997: putfield 207	com/android/server/wm/AppWindowToken:startingWindow	Lcom/android/server/wm/WindowState;
            //     1000: aload 61
            //     1002: iconst_0
            //     1003: putfield 230	com/android/server/wm/AppWindowToken:startingDisplayed	Z
            //     1006: aload 58
            //     1008: monitorexit
            //     1009: aload_0
            //     1010: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1013: getfield 163	com/android/server/wm/WindowManagerService:mPolicy	Landroid/view/WindowManagerPolicy;
            //     1016: aload 63
            //     1018: aload 62
            //     1020: invokeinterface 211 3 0
            //     1025: goto -133 -> 892
            //     1028: astore 64
            //     1030: ldc 213
            //     1032: ldc 215
            //     1034: aload 64
            //     1036: invokestatic 221	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     1039: pop
            //     1040: goto -148 -> 892
            //     1043: aload_1
            //     1044: getfield 153	android/os/Message:obj	Ljava/lang/Object;
            //     1047: checkcast 155	com/android/server/wm/AppWindowToken
            //     1050: astore 56
            //     1052: aload 56
            //     1054: getfield 240	com/android/server/wm/AppWindowToken:appToken	Landroid/view/IApplicationToken;
            //     1057: invokeinterface 245 1 0
            //     1062: goto -814 -> 248
            //     1065: astore 57
            //     1067: goto -819 -> 248
            //     1070: aload_1
            //     1071: getfield 153	android/os/Message:obj	Ljava/lang/Object;
            //     1074: checkcast 155	com/android/server/wm/AppWindowToken
            //     1077: astore 53
            //     1079: aload_1
            //     1080: getfield 248	android/os/Message:arg1	I
            //     1083: ifeq +1407 -> 2490
            //     1086: iconst_1
            //     1087: istore 54
            //     1089: aload_1
            //     1090: getfield 251	android/os/Message:arg2	I
            //     1093: ifeq +1403 -> 2496
            //     1096: iload 54
            //     1098: ifeq +16 -> 1114
            //     1101: aload 53
            //     1103: getfield 240	com/android/server/wm/AppWindowToken:appToken	Landroid/view/IApplicationToken;
            //     1106: invokeinterface 254 1 0
            //     1111: goto -863 -> 248
            //     1114: aload 53
            //     1116: getfield 240	com/android/server/wm/AppWindowToken:appToken	Landroid/view/IApplicationToken;
            //     1119: invokeinterface 257 1 0
            //     1124: goto -876 -> 248
            //     1127: aload_0
            //     1128: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1131: getfield 94	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
            //     1134: astore 47
            //     1136: aload 47
            //     1138: monitorenter
            //     1139: ldc 213
            //     1141: ldc_w 259
            //     1144: invokestatic 262	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     1147: pop
            //     1148: aload_0
            //     1149: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1152: getfield 265	com/android/server/wm/WindowManagerService:mWindows	Ljava/util/ArrayList;
            //     1155: invokevirtual 139	java/util/ArrayList:size	()I
            //     1158: istore 50
            //     1160: iload 50
            //     1162: ifle +75 -> 1237
            //     1165: iinc 50 255
            //     1168: aload_0
            //     1169: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1172: getfield 265	com/android/server/wm/WindowManagerService:mWindows	Ljava/util/ArrayList;
            //     1175: iload 50
            //     1177: invokevirtual 143	java/util/ArrayList:get	(I)Ljava/lang/Object;
            //     1180: checkcast 103	com/android/server/wm/WindowState
            //     1183: astore 51
            //     1185: aload 51
            //     1187: getfield 268	com/android/server/wm/WindowState:mOrientationChanging	Z
            //     1190: ifeq -30 -> 1160
            //     1193: aload 51
            //     1195: iconst_0
            //     1196: putfield 268	com/android/server/wm/WindowState:mOrientationChanging	Z
            //     1199: ldc 213
            //     1201: new 270	java/lang/StringBuilder
            //     1204: dup
            //     1205: invokespecial 271	java/lang/StringBuilder:<init>	()V
            //     1208: ldc_w 273
            //     1211: invokevirtual 277	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     1214: aload 51
            //     1216: invokevirtual 280	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     1219: invokevirtual 284	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     1222: invokestatic 262	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     1225: pop
            //     1226: goto -66 -> 1160
            //     1229: astore 48
            //     1231: aload 47
            //     1233: monitorexit
            //     1234: aload 48
            //     1236: athrow
            //     1237: aload_0
            //     1238: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1241: invokestatic 149	com/android/server/wm/WindowManagerService:access$600	(Lcom/android/server/wm/WindowManagerService;)V
            //     1244: aload 47
            //     1246: monitorexit
            //     1247: goto -999 -> 248
            //     1250: aload_0
            //     1251: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1254: getfield 94	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
            //     1257: astore 42
            //     1259: aload 42
            //     1261: monitorenter
            //     1262: aload_0
            //     1263: getfield 286	com/android/server/wm/WindowManagerService$H:mLastReportedHold	Lcom/android/server/wm/Session;
            //     1266: astore 44
            //     1268: aload_1
            //     1269: getfield 153	android/os/Message:obj	Ljava/lang/Object;
            //     1272: checkcast 288	com/android/server/wm/Session
            //     1275: astore 45
            //     1277: aload_0
            //     1278: aload 45
            //     1280: putfield 286	com/android/server/wm/WindowManagerService$H:mLastReportedHold	Lcom/android/server/wm/Session;
            //     1283: aload 42
            //     1285: monitorexit
            //     1286: aload 44
            //     1288: aload 45
            //     1290: if_acmpeq -1042 -> 248
            //     1293: aload 44
            //     1295: ifnull +26 -> 1321
            //     1298: aload_0
            //     1299: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1302: getfield 292	com/android/server/wm/WindowManagerService:mBatteryStats	Lcom/android/internal/app/IBatteryStats;
            //     1305: aload 44
            //     1307: getfield 295	com/android/server/wm/Session:mUid	I
            //     1310: bipush 255
            //     1312: ldc_w 297
            //     1315: iconst_2
            //     1316: invokeinterface 303 5 0
            //     1321: aload 45
            //     1323: ifnull -1075 -> 248
            //     1326: aload_0
            //     1327: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1330: getfield 292	com/android/server/wm/WindowManagerService:mBatteryStats	Lcom/android/internal/app/IBatteryStats;
            //     1333: aload 45
            //     1335: getfield 295	com/android/server/wm/Session:mUid	I
            //     1338: bipush 255
            //     1340: ldc_w 297
            //     1343: iconst_2
            //     1344: invokeinterface 306 5 0
            //     1349: goto -1101 -> 248
            //     1352: astore 46
            //     1354: goto -1106 -> 248
            //     1357: astore 43
            //     1359: aload 42
            //     1361: monitorexit
            //     1362: aload 43
            //     1364: athrow
            //     1365: aload_0
            //     1366: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1369: getfield 94	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
            //     1372: astore 39
            //     1374: aload 39
            //     1376: monitorenter
            //     1377: aload_0
            //     1378: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1381: getfield 309	com/android/server/wm/WindowManagerService:mNextAppTransition	I
            //     1384: bipush 255
            //     1386: if_icmpeq +54 -> 1440
            //     1389: aload_0
            //     1390: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1393: iconst_1
            //     1394: putfield 312	com/android/server/wm/WindowManagerService:mAppTransitionReady	Z
            //     1397: aload_0
            //     1398: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1401: iconst_1
            //     1402: putfield 315	com/android/server/wm/WindowManagerService:mAppTransitionTimeout	Z
            //     1405: aload_0
            //     1406: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1409: getfield 318	com/android/server/wm/WindowManagerService:mAnimatingAppTokens	Ljava/util/ArrayList;
            //     1412: invokevirtual 321	java/util/ArrayList:clear	()V
            //     1415: aload_0
            //     1416: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1419: getfield 318	com/android/server/wm/WindowManagerService:mAnimatingAppTokens	Ljava/util/ArrayList;
            //     1422: aload_0
            //     1423: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1426: getfield 324	com/android/server/wm/WindowManagerService:mAppTokens	Ljava/util/ArrayList;
            //     1429: invokevirtual 328	java/util/ArrayList:addAll	(Ljava/util/Collection;)Z
            //     1432: pop
            //     1433: aload_0
            //     1434: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1437: invokestatic 149	com/android/server/wm/WindowManagerService:access$600	(Lcom/android/server/wm/WindowManagerService;)V
            //     1440: aload 39
            //     1442: monitorexit
            //     1443: goto -1195 -> 248
            //     1446: astore 40
            //     1448: aload 39
            //     1450: monitorexit
            //     1451: aload 40
            //     1453: athrow
            //     1454: aload_0
            //     1455: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1458: getfield 332	com/android/server/wm/WindowManagerService:mContext	Landroid/content/Context;
            //     1461: invokevirtual 338	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
            //     1464: ldc_w 340
            //     1467: aload_0
            //     1468: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1471: getfield 344	com/android/server/wm/WindowManagerService:mWindowAnimationScale	F
            //     1474: invokestatic 350	android/provider/Settings$System:putFloat	(Landroid/content/ContentResolver;Ljava/lang/String;F)Z
            //     1477: pop
            //     1478: aload_0
            //     1479: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1482: getfield 332	com/android/server/wm/WindowManagerService:mContext	Landroid/content/Context;
            //     1485: invokevirtual 338	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
            //     1488: ldc_w 352
            //     1491: aload_0
            //     1492: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1495: getfield 355	com/android/server/wm/WindowManagerService:mTransitionAnimationScale	F
            //     1498: invokestatic 350	android/provider/Settings$System:putFloat	(Landroid/content/ContentResolver;Ljava/lang/String;F)Z
            //     1501: pop
            //     1502: aload_0
            //     1503: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1506: getfield 332	com/android/server/wm/WindowManagerService:mContext	Landroid/content/Context;
            //     1509: invokevirtual 338	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
            //     1512: ldc_w 357
            //     1515: aload_0
            //     1516: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1519: getfield 360	com/android/server/wm/WindowManagerService:mAnimatorDurationScale	F
            //     1522: invokestatic 350	android/provider/Settings$System:putFloat	(Landroid/content/ContentResolver;Ljava/lang/String;F)Z
            //     1525: pop
            //     1526: goto -1278 -> 248
            //     1529: aload_0
            //     1530: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1533: getfield 94	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
            //     1536: astore 33
            //     1538: aload 33
            //     1540: monitorenter
            //     1541: aload_0
            //     1542: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1545: getfield 363	com/android/server/wm/WindowManagerService:mAnimationScheduled	Z
            //     1548: ifeq +43 -> 1591
            //     1551: aload_0
            //     1552: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1555: getfield 367	com/android/server/wm/WindowManagerService:mH	Lcom/android/server/wm/WindowManagerService$H;
            //     1558: aload_0
            //     1559: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1562: getfield 367	com/android/server/wm/WindowManagerService:mH	Lcom/android/server/wm/WindowManagerService$H;
            //     1565: bipush 15
            //     1567: invokevirtual 371	com/android/server/wm/WindowManagerService$H:obtainMessage	(I)Landroid/os/Message;
            //     1570: ldc2_w 372
            //     1573: invokevirtual 377	com/android/server/wm/WindowManagerService$H:sendMessageDelayed	(Landroid/os/Message;J)Z
            //     1576: pop
            //     1577: aload 33
            //     1579: monitorexit
            //     1580: goto -1332 -> 248
            //     1583: astore 34
            //     1585: aload 33
            //     1587: monitorexit
            //     1588: aload 34
            //     1590: athrow
            //     1591: aload_0
            //     1592: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1595: getfield 380	com/android/server/wm/WindowManagerService:mDisplayFrozen	Z
            //     1598: ifeq +9 -> 1607
            //     1601: aload 33
            //     1603: monitorexit
            //     1604: goto -1356 -> 248
            //     1607: aload 33
            //     1609: monitorexit
            //     1610: invokestatic 386	java/lang/Runtime:getRuntime	()Ljava/lang/Runtime;
            //     1613: invokevirtual 389	java/lang/Runtime:gc	()V
            //     1616: goto -1368 -> 248
            //     1619: aload_0
            //     1620: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1623: invokevirtual 392	com/android/server/wm/WindowManagerService:performEnableScreen	()V
            //     1626: goto -1378 -> 248
            //     1629: aload_0
            //     1630: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1633: getfield 94	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
            //     1636: astore 25
            //     1638: aload 25
            //     1640: monitorenter
            //     1641: aload_0
            //     1642: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1645: getfield 396	com/android/server/wm/WindowManagerService:mAnimator	Lcom/android/server/wm/WindowAnimator;
            //     1648: astore 27
            //     1650: aload 27
            //     1652: monitorenter
            //     1653: ldc 213
            //     1655: ldc_w 398
            //     1658: invokestatic 262	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     1661: pop
            //     1662: aload_0
            //     1663: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1666: getfield 324	com/android/server/wm/WindowManagerService:mAppTokens	Ljava/util/ArrayList;
            //     1669: invokevirtual 139	java/util/ArrayList:size	()I
            //     1672: istore 30
            //     1674: iload 30
            //     1676: ifle +91 -> 1767
            //     1679: iinc 30 255
            //     1682: aload_0
            //     1683: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1686: getfield 324	com/android/server/wm/WindowManagerService:mAppTokens	Ljava/util/ArrayList;
            //     1689: iload 30
            //     1691: invokevirtual 143	java/util/ArrayList:get	(I)Ljava/lang/Object;
            //     1694: checkcast 155	com/android/server/wm/AppWindowToken
            //     1697: astore 31
            //     1699: aload 31
            //     1701: getfield 402	com/android/server/wm/AppWindowToken:mAppAnimator	Lcom/android/server/wm/AppWindowAnimator;
            //     1704: getfield 407	com/android/server/wm/AppWindowAnimator:freezingScreen	Z
            //     1707: ifeq -33 -> 1674
            //     1710: ldc 213
            //     1712: new 270	java/lang/StringBuilder
            //     1715: dup
            //     1716: invokespecial 271	java/lang/StringBuilder:<init>	()V
            //     1719: ldc_w 409
            //     1722: invokevirtual 277	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     1725: aload 31
            //     1727: invokevirtual 280	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     1730: invokevirtual 284	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     1733: invokestatic 262	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     1736: pop
            //     1737: aload_0
            //     1738: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1741: aload 31
            //     1743: iconst_1
            //     1744: iconst_1
            //     1745: invokevirtual 413	com/android/server/wm/WindowManagerService:unsetAppFreezingScreenLocked	(Lcom/android/server/wm/AppWindowToken;ZZ)V
            //     1748: goto -74 -> 1674
            //     1751: astore 28
            //     1753: aload 27
            //     1755: monitorexit
            //     1756: aload 28
            //     1758: athrow
            //     1759: astore 26
            //     1761: aload 25
            //     1763: monitorexit
            //     1764: aload 26
            //     1766: athrow
            //     1767: aload 27
            //     1769: monitorexit
            //     1770: aload 25
            //     1772: monitorexit
            //     1773: goto -1525 -> 248
            //     1776: aload_0
            //     1777: bipush 18
            //     1779: invokevirtual 417	com/android/server/wm/WindowManagerService$H:removeMessages	(I)V
            //     1782: aload_0
            //     1783: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1786: invokevirtual 420	com/android/server/wm/WindowManagerService:sendNewConfiguration	()V
            //     1789: goto -1541 -> 248
            //     1792: aload_0
            //     1793: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1796: invokestatic 424	com/android/server/wm/WindowManagerService:access$700	(Lcom/android/server/wm/WindowManagerService;)Z
            //     1799: ifeq -1551 -> 248
            //     1802: aload_0
            //     1803: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1806: getfield 94	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
            //     1809: astore 22
            //     1811: aload 22
            //     1813: monitorenter
            //     1814: aload_0
            //     1815: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1818: iconst_0
            //     1819: invokestatic 428	com/android/server/wm/WindowManagerService:access$702	(Lcom/android/server/wm/WindowManagerService;Z)Z
            //     1822: pop
            //     1823: aload 22
            //     1825: monitorexit
            //     1826: aload_0
            //     1827: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1830: invokestatic 431	com/android/server/wm/WindowManagerService:access$800	(Lcom/android/server/wm/WindowManagerService;)V
            //     1833: goto -1585 -> 248
            //     1836: astore 23
            //     1838: aload 22
            //     1840: monitorexit
            //     1841: aload 23
            //     1843: athrow
            //     1844: aload_1
            //     1845: getfield 153	android/os/Message:obj	Ljava/lang/Object;
            //     1848: checkcast 433	android/os/IBinder
            //     1851: pop
            //     1852: aload_0
            //     1853: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1856: getfield 94	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
            //     1859: astore 20
            //     1861: aload 20
            //     1863: monitorenter
            //     1864: aload_0
            //     1865: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1868: getfield 437	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
            //     1871: ifnull +42 -> 1913
            //     1874: aload_0
            //     1875: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1878: getfield 437	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
            //     1881: invokevirtual 442	com/android/server/wm/DragState:unregister	()V
            //     1884: aload_0
            //     1885: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1888: getfield 446	com/android/server/wm/WindowManagerService:mInputMonitor	Lcom/android/server/wm/InputMonitor;
            //     1891: iconst_1
            //     1892: invokevirtual 452	com/android/server/wm/InputMonitor:updateInputWindowsLw	(Z)V
            //     1895: aload_0
            //     1896: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1899: getfield 437	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
            //     1902: invokevirtual 455	com/android/server/wm/DragState:reset	()V
            //     1905: aload_0
            //     1906: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1909: aconst_null
            //     1910: putfield 437	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
            //     1913: aload 20
            //     1915: monitorexit
            //     1916: goto -1668 -> 248
            //     1919: astore 21
            //     1921: aload 20
            //     1923: monitorexit
            //     1924: aload 21
            //     1926: athrow
            //     1927: aload_1
            //     1928: getfield 153	android/os/Message:obj	Ljava/lang/Object;
            //     1931: checkcast 433	android/os/IBinder
            //     1934: pop
            //     1935: aload_0
            //     1936: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1939: getfield 94	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
            //     1942: astore 17
            //     1944: aload 17
            //     1946: monitorenter
            //     1947: aload_0
            //     1948: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1951: getfield 437	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
            //     1954: ifnull +24 -> 1978
            //     1957: aload_0
            //     1958: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1961: getfield 437	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
            //     1964: iconst_0
            //     1965: putfield 458	com/android/server/wm/DragState:mDragResult	Z
            //     1968: aload_0
            //     1969: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1972: getfield 437	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
            //     1975: invokevirtual 461	com/android/server/wm/DragState:endDragLw	()V
            //     1978: aload 17
            //     1980: monitorexit
            //     1981: goto -1733 -> 248
            //     1984: astore 18
            //     1986: aload 17
            //     1988: monitorexit
            //     1989: aload 18
            //     1991: athrow
            //     1992: aload_0
            //     1993: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     1996: invokevirtual 464	com/android/server/wm/WindowManagerService:notifyHardKeyboardStatusChange	()V
            //     1999: goto -1751 -> 248
            //     2002: aload_0
            //     2003: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     2006: invokevirtual 467	com/android/server/wm/WindowManagerService:performBootTimeout	()V
            //     2009: goto -1761 -> 248
            //     2012: aload_0
            //     2013: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     2016: getfield 94	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
            //     2019: astore 11
            //     2021: aload 11
            //     2023: monitorenter
            //     2024: aload_1
            //     2025: getfield 153	android/os/Message:obj	Ljava/lang/Object;
            //     2028: checkcast 469	android/util/Pair
            //     2031: astore 13
            //     2033: ldc 213
            //     2035: new 270	java/lang/StringBuilder
            //     2038: dup
            //     2039: invokespecial 271	java/lang/StringBuilder:<init>	()V
            //     2042: ldc_w 471
            //     2045: invokevirtual 277	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     2048: aload 13
            //     2050: getfield 474	android/util/Pair:first	Ljava/lang/Object;
            //     2053: invokevirtual 280	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     2056: invokevirtual 284	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     2059: invokestatic 262	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     2062: pop
            //     2063: aload_0
            //     2064: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     2067: getfield 477	com/android/server/wm/WindowManagerService:mWaitingForDrawn	Ljava/util/ArrayList;
            //     2070: aload 13
            //     2072: invokevirtual 479	java/util/ArrayList:remove	(Ljava/lang/Object;)Z
            //     2075: ifne +17 -> 2092
            //     2078: aload 11
            //     2080: monitorexit
            //     2081: goto -1833 -> 248
            //     2084: astore 12
            //     2086: aload 11
            //     2088: monitorexit
            //     2089: aload 12
            //     2091: athrow
            //     2092: aload 11
            //     2094: monitorexit
            //     2095: aload 13
            //     2097: getfield 482	android/util/Pair:second	Ljava/lang/Object;
            //     2100: checkcast 484	android/os/IRemoteCallback
            //     2103: aconst_null
            //     2104: invokeinterface 488 2 0
            //     2109: goto -1861 -> 248
            //     2112: astore 15
            //     2114: goto -1866 -> 248
            //     2117: aload_0
            //     2118: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     2121: getfield 94	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
            //     2124: astore 4
            //     2126: aload 4
            //     2128: monitorenter
            //     2129: iconst_0
            //     2130: istore 5
            //     2132: iconst_1
            //     2133: aload_1
            //     2134: getfield 248	android/os/Message:arg1	I
            //     2137: iand
            //     2138: ifeq +18 -> 2156
            //     2141: aload_0
            //     2142: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     2145: getfield 492	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
            //     2148: iconst_1
            //     2149: invokestatic 498	com/android/server/wm/WindowManagerService$LayoutFields:access$902	(Lcom/android/server/wm/WindowManagerService$LayoutFields;Z)Z
            //     2152: pop
            //     2153: iconst_1
            //     2154: istore 5
            //     2156: iconst_2
            //     2157: aload_1
            //     2158: getfield 248	android/os/Message:arg1	I
            //     2161: iand
            //     2162: ifeq +17 -> 2179
            //     2165: aload_0
            //     2166: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     2169: getfield 492	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
            //     2172: iconst_1
            //     2173: putfield 501	com/android/server/wm/WindowManagerService$LayoutFields:mWallpaperMayChange	Z
            //     2176: iconst_1
            //     2177: istore 5
            //     2179: iconst_4
            //     2180: aload_1
            //     2181: getfield 248	android/os/Message:arg1	I
            //     2184: iand
            //     2185: ifeq +17 -> 2202
            //     2188: aload_0
            //     2189: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     2192: getfield 492	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
            //     2195: iconst_1
            //     2196: putfield 504	com/android/server/wm/WindowManagerService$LayoutFields:mWallpaperForceHidingChanged	Z
            //     2199: iconst_1
            //     2200: istore 5
            //     2202: bipush 8
            //     2204: aload_1
            //     2205: getfield 248	android/os/Message:arg1	I
            //     2208: iand
            //     2209: ifeq +105 -> 2314
            //     2212: aload_0
            //     2213: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     2216: getfield 492	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
            //     2219: iconst_0
            //     2220: putfield 507	com/android/server/wm/WindowManagerService$LayoutFields:mOrientationChangeComplete	Z
            //     2223: bipush 16
            //     2225: aload_1
            //     2226: getfield 248	android/os/Message:arg1	I
            //     2229: iand
            //     2230: ifeq +11 -> 2241
            //     2233: aload_0
            //     2234: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     2237: iconst_1
            //     2238: putfield 510	com/android/server/wm/WindowManagerService:mTurnOnScreen	Z
            //     2241: aload_0
            //     2242: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     2245: astore 8
            //     2247: aload 8
            //     2249: aload 8
            //     2251: getfield 513	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
            //     2254: aload_1
            //     2255: getfield 251	android/os/Message:arg2	I
            //     2258: ior
            //     2259: putfield 513	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
            //     2262: aload_0
            //     2263: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     2266: getfield 513	com/android/server/wm/WindowManagerService:mPendingLayoutChanges	I
            //     2269: ifeq +6 -> 2275
            //     2272: iconst_1
            //     2273: istore 5
            //     2275: iload 5
            //     2277: ifeq +23 -> 2300
            //     2280: aload_0
            //     2281: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     2284: getfield 367	com/android/server/wm/WindowManagerService:mH	Lcom/android/server/wm/WindowManagerService$H;
            //     2287: ldc 22
            //     2289: invokevirtual 517	com/android/server/wm/WindowManagerService$H:sendEmptyMessage	(I)Z
            //     2292: pop
            //     2293: aload_0
            //     2294: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     2297: invokestatic 149	com/android/server/wm/WindowManagerService:access$600	(Lcom/android/server/wm/WindowManagerService;)V
            //     2300: aload 4
            //     2302: monitorexit
            //     2303: goto -2055 -> 248
            //     2306: astore 6
            //     2308: aload 4
            //     2310: monitorexit
            //     2311: aload 6
            //     2313: athrow
            //     2314: aload_0
            //     2315: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     2318: getfield 492	com/android/server/wm/WindowManagerService:mInnerFields	Lcom/android/server/wm/WindowManagerService$LayoutFields;
            //     2321: iconst_1
            //     2322: putfield 507	com/android/server/wm/WindowManagerService$LayoutFields:mOrientationChangeComplete	Z
            //     2325: aload_0
            //     2326: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     2329: getfield 520	com/android/server/wm/WindowManagerService:mWindowsFreezingScreen	Z
            //     2332: istore 7
            //     2334: iload 7
            //     2336: ifeq -113 -> 2223
            //     2339: iconst_1
            //     2340: istore 5
            //     2342: goto -119 -> 2223
            //     2345: aload_0
            //     2346: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     2349: aload_1
            //     2350: getfield 248	android/os/Message:arg1	I
            //     2353: invokestatic 524	com/android/server/wm/WindowManagerService:access$1000	(Lcom/android/server/wm/WindowManagerService;I)V
            //     2356: goto -2108 -> 248
            //     2359: aload_1
            //     2360: getfield 153	android/os/Message:obj	Ljava/lang/Object;
            //     2363: checkcast 469	android/util/Pair
            //     2366: astore_3
            //     2367: aload_3
            //     2368: getfield 474	android/util/Pair:first	Ljava/lang/Object;
            //     2371: checkcast 526	com/android/server/wm/WindowStateAnimator
            //     2374: aload_3
            //     2375: getfield 482	android/util/Pair:second	Ljava/lang/Object;
            //     2378: checkcast 528	android/graphics/Region
            //     2381: invokevirtual 532	com/android/server/wm/WindowStateAnimator:setTransparentRegionHint	(Landroid/graphics/Region;)V
            //     2384: goto -2136 -> 248
            //     2387: aload_1
            //     2388: getfield 153	android/os/Message:obj	Ljava/lang/Object;
            //     2391: checkcast 526	com/android/server/wm/WindowStateAnimator
            //     2394: aload_1
            //     2395: getfield 248	android/os/Message:arg1	I
            //     2398: aload_1
            //     2399: getfield 251	android/os/Message:arg2	I
            //     2402: invokevirtual 536	com/android/server/wm/WindowStateAnimator:setWallpaperOffset	(II)V
            //     2405: aload_0
            //     2406: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     2409: invokevirtual 539	com/android/server/wm/WindowManagerService:scheduleAnimationLocked	()V
            //     2412: goto -2164 -> 248
            //     2415: aload_0
            //     2416: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     2419: getfield 396	com/android/server/wm/WindowManagerService:mAnimator	Lcom/android/server/wm/WindowAnimator;
            //     2422: aload_1
            //     2423: getfield 153	android/os/Message:obj	Ljava/lang/Object;
            //     2426: checkcast 541	com/android/server/wm/DimAnimator$Parameters
            //     2429: putfield 547	com/android/server/wm/WindowAnimator:mDimParams	Lcom/android/server/wm/DimAnimator$Parameters;
            //     2432: aload_0
            //     2433: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     2436: invokevirtual 539	com/android/server/wm/WindowManagerService:scheduleAnimationLocked	()V
            //     2439: goto -2191 -> 248
            //     2442: aload_0
            //     2443: getfield 76	com/android/server/wm/WindowManagerService$H:this$0	Lcom/android/server/wm/WindowManagerService;
            //     2446: getfield 396	com/android/server/wm/WindowManagerService:mAnimator	Lcom/android/server/wm/WindowAnimator;
            //     2449: invokevirtual 550	com/android/server/wm/WindowAnimator:clearPendingActions	()V
            //     2452: goto -2204 -> 248
            //     2455: aload_1
            //     2456: getfield 153	android/os/Message:obj	Ljava/lang/Object;
            //     2459: checkcast 484	android/os/IRemoteCallback
            //     2462: aconst_null
            //     2463: invokeinterface 488 2 0
            //     2468: goto -2220 -> 248
            //     2471: astore_2
            //     2472: goto -2224 -> 248
            //     2475: astore 91
            //     2477: goto -1982 -> 495
            //     2480: astore 97
            //     2482: goto -2106 -> 376
            //     2485: astore 55
            //     2487: goto -2239 -> 248
            //     2490: iconst_0
            //     2491: istore 54
            //     2493: goto -1404 -> 1089
            //     2496: goto -1400 -> 1096
            //
            // Exception table:
            //     from	to	target	type
            //     261	297	292	finally
            //     300	346	292	finally
            //     388	406	409	android/os/RemoteException
            //     426	452	501	finally
            //     503	506	501	finally
            //     521	547	542	finally
            //     699	718	721	java/lang/Exception
            //     574	628	736	java/lang/Exception
            //     652	694	761	finally
            //     751	766	761	finally
            //     850	866	869	java/lang/Exception
            //     796	845	884	finally
            //     886	889	884	finally
            //     904	932	927	finally
            //     935	1009	927	finally
            //     1009	1025	1028	java/lang/Exception
            //     1052	1062	1065	android/os/RemoteException
            //     1139	1234	1229	finally
            //     1237	1247	1229	finally
            //     1298	1349	1352	android/os/RemoteException
            //     1262	1286	1357	finally
            //     1359	1362	1357	finally
            //     1377	1451	1446	finally
            //     1541	1588	1583	finally
            //     1591	1610	1583	finally
            //     1653	1756	1751	finally
            //     1767	1770	1751	finally
            //     1641	1653	1759	finally
            //     1756	1764	1759	finally
            //     1770	1773	1759	finally
            //     1814	1826	1836	finally
            //     1838	1841	1836	finally
            //     1864	1924	1919	finally
            //     1947	1989	1984	finally
            //     2024	2089	2084	finally
            //     2092	2095	2084	finally
            //     2095	2109	2112	android/os/RemoteException
            //     2132	2311	2306	finally
            //     2314	2334	2306	finally
            //     2455	2468	2471	android/os/RemoteException
            //     469	495	2475	android/os/RemoteException
            //     358	376	2480	android/os/RemoteException
            //     1101	1124	2485	android/os/RemoteException
        }
    }

    static class PolicyThread extends Thread
    {
        private final Context mContext;
        private final PowerManagerService mPM;
        private final WindowManagerPolicy mPolicy;
        boolean mRunning = false;
        private final WindowManagerService mService;

        public PolicyThread(WindowManagerPolicy paramWindowManagerPolicy, WindowManagerService paramWindowManagerService, Context paramContext, PowerManagerService paramPowerManagerService)
        {
            super();
            this.mPolicy = paramWindowManagerPolicy;
            this.mService = paramWindowManagerService;
            this.mContext = paramContext;
            this.mPM = paramPowerManagerService;
        }

        public void run()
        {
            Looper.prepare();
            WindowManagerPolicyThread.set(this, Looper.myLooper());
            Process.setThreadPriority(-2);
            Process.setCanSelfBackground(false);
            this.mPolicy.init(this.mContext, this.mService, this.mService, this.mPM);
            try
            {
                this.mRunning = true;
                notifyAll();
                if (StrictMode.conditionallyEnableDebugLogging())
                    Slog.i("WindowManager", "Enabled StrictMode for PolicyThread's Looper");
                Looper.loop();
                return;
            }
            finally
            {
            }
        }
    }

    static class WMThread extends Thread
    {
        private final boolean mAllowBootMessages;
        private final Context mContext;
        private final boolean mHaveInputMethods;
        private final boolean mOnlyCore;
        private final PowerManagerService mPM;
        WindowManagerService mService;

        public WMThread(Context paramContext, PowerManagerService paramPowerManagerService, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
        {
            super();
            this.mContext = paramContext;
            this.mPM = paramPowerManagerService;
            this.mHaveInputMethods = paramBoolean1;
            this.mAllowBootMessages = paramBoolean2;
            this.mOnlyCore = paramBoolean3;
        }

        public void run()
        {
            Looper.prepare();
            WindowManagerService localWindowManagerService = new WindowManagerService(this.mContext, this.mPM, this.mHaveInputMethods, this.mAllowBootMessages, this.mOnlyCore, null);
            Process.setThreadPriority(-4);
            Process.setCanSelfBackground(false);
            try
            {
                this.mService = localWindowManagerService;
                notifyAll();
                if (StrictMode.conditionallyEnableDebugLogging())
                    Slog.i("WindowManager", "Enabled StrictMode logging for WMThread's Looper");
                Looper.loop();
                return;
            }
            finally
            {
            }
        }
    }

    public static abstract interface WindowChangeListener
    {
        public abstract void focusChanged();

        public abstract void windowsChanged();
    }

    final class DragInputEventReceiver extends InputEventReceiver
    {
        public DragInputEventReceiver(InputChannel paramLooper, Looper arg3)
        {
            super(localLooper);
        }

        // ERROR //
        public void onInputEvent(android.view.InputEvent paramInputEvent)
        {
            // Byte code:
            //     0: iconst_0
            //     1: istore_2
            //     2: aload_1
            //     3: instanceof 22
            //     6: ifeq +114 -> 120
            //     9: iconst_2
            //     10: aload_1
            //     11: invokevirtual 28	android/view/InputEvent:getSource	()I
            //     14: iand
            //     15: ifeq +105 -> 120
            //     18: aload_0
            //     19: getfield 13	com/android/server/wm/WindowManagerService$DragInputEventReceiver:this$0	Lcom/android/server/wm/WindowManagerService;
            //     22: getfield 32	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
            //     25: ifnull +95 -> 120
            //     28: aload_1
            //     29: checkcast 22	android/view/MotionEvent
            //     32: astore 6
            //     34: iconst_0
            //     35: istore 7
            //     37: aload 6
            //     39: invokevirtual 36	android/view/MotionEvent:getRawX	()F
            //     42: fstore 8
            //     44: aload 6
            //     46: invokevirtual 39	android/view/MotionEvent:getRawY	()F
            //     49: fstore 9
            //     51: aload 6
            //     53: invokevirtual 42	android/view/MotionEvent:getAction	()I
            //     56: tableswitch	default:+32 -> 88, 0:+32->88, 1:+132->188, 2:+71->127, 3:+183->239
            //     89: iconst_4
            //     90: ifeq +28 -> 118
            //     93: aload_0
            //     94: getfield 13	com/android/server/wm/WindowManagerService$DragInputEventReceiver:this$0	Lcom/android/server/wm/WindowManagerService;
            //     97: getfield 46	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
            //     100: astore 10
            //     102: aload 10
            //     104: monitorenter
            //     105: aload_0
            //     106: getfield 13	com/android/server/wm/WindowManagerService$DragInputEventReceiver:this$0	Lcom/android/server/wm/WindowManagerService;
            //     109: getfield 32	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
            //     112: invokevirtual 52	com/android/server/wm/DragState:endDragLw	()V
            //     115: aload 10
            //     117: monitorexit
            //     118: iconst_1
            //     119: istore_2
            //     120: aload_0
            //     121: aload_1
            //     122: iload_2
            //     123: invokevirtual 56	com/android/server/wm/WindowManagerService$DragInputEventReceiver:finishInputEvent	(Landroid/view/InputEvent;Z)V
            //     126: return
            //     127: aload_0
            //     128: getfield 13	com/android/server/wm/WindowManagerService$DragInputEventReceiver:this$0	Lcom/android/server/wm/WindowManagerService;
            //     131: getfield 46	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
            //     134: astore 14
            //     136: aload 14
            //     138: monitorenter
            //     139: aload_0
            //     140: getfield 13	com/android/server/wm/WindowManagerService$DragInputEventReceiver:this$0	Lcom/android/server/wm/WindowManagerService;
            //     143: getfield 32	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
            //     146: fload 8
            //     148: fload 9
            //     150: invokevirtual 60	com/android/server/wm/DragState:notifyMoveLw	(FF)V
            //     153: aload 14
            //     155: monitorexit
            //     156: goto -68 -> 88
            //     159: astore 15
            //     161: aload 14
            //     163: monitorexit
            //     164: aload 15
            //     166: athrow
            //     167: astore 4
            //     169: ldc 62
            //     171: ldc 64
            //     173: aload 4
            //     175: invokestatic 70	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     178: pop
            //     179: aload_0
            //     180: aload_1
            //     181: iconst_0
            //     182: invokevirtual 56	com/android/server/wm/WindowManagerService$DragInputEventReceiver:finishInputEvent	(Landroid/view/InputEvent;Z)V
            //     185: goto -59 -> 126
            //     188: aload_0
            //     189: getfield 13	com/android/server/wm/WindowManagerService$DragInputEventReceiver:this$0	Lcom/android/server/wm/WindowManagerService;
            //     192: getfield 46	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
            //     195: astore 12
            //     197: aload 12
            //     199: monitorenter
            //     200: aload_0
            //     201: getfield 13	com/android/server/wm/WindowManagerService$DragInputEventReceiver:this$0	Lcom/android/server/wm/WindowManagerService;
            //     204: getfield 32	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
            //     207: fload 8
            //     209: fload 9
            //     211: invokevirtual 74	com/android/server/wm/DragState:notifyDropLw	(FF)Z
            //     214: istore 7
            //     216: aload 12
            //     218: monitorexit
            //     219: goto -131 -> 88
            //     222: astore 13
            //     224: aload 12
            //     226: monitorexit
            //     227: aload 13
            //     229: athrow
            //     230: astore_3
            //     231: aload_0
            //     232: aload_1
            //     233: iconst_0
            //     234: invokevirtual 56	com/android/server/wm/WindowManagerService$DragInputEventReceiver:finishInputEvent	(Landroid/view/InputEvent;Z)V
            //     237: aload_3
            //     238: athrow
            //     239: iconst_1
            //     240: istore 7
            //     242: goto -154 -> 88
            //     245: astore 11
            //     247: aload 10
            //     249: monitorexit
            //     250: aload 11
            //     252: athrow
            //
            // Exception table:
            //     from	to	target	type
            //     139	164	159	finally
            //     2	105	167	java/lang/Exception
            //     127	139	167	java/lang/Exception
            //     164	167	167	java/lang/Exception
            //     188	200	167	java/lang/Exception
            //     227	230	167	java/lang/Exception
            //     250	253	167	java/lang/Exception
            //     200	227	222	finally
            //     2	105	230	finally
            //     127	139	230	finally
            //     164	167	230	finally
            //     169	179	230	finally
            //     188	200	230	finally
            //     227	230	230	finally
            //     250	253	230	finally
            //     105	118	245	finally
            //     247	250	245	finally
        }
    }

    private final class AnimationRunnable
        implements Runnable
    {
        private AnimationRunnable()
        {
        }

        public void run()
        {
            while (true)
            {
                int j;
                synchronized (WindowManagerService.this.mWindowMap)
                {
                    WindowManagerService.this.mAnimationScheduled = false;
                    synchronized (WindowManagerService.this.mAnimator)
                    {
                        Trace.traceBegin(32L, "wmAnimate");
                        ArrayList localArrayList = WindowManagerService.this.mAnimator.mWinAnimators;
                        localArrayList.clear();
                        int i = WindowManagerService.this.mWindows.size();
                        j = 0;
                        if (j < i)
                        {
                            WindowStateAnimator localWindowStateAnimator = ((WindowState)WindowManagerService.this.mWindows.get(j)).mWinAnimator;
                            if (localWindowStateAnimator.mSurface != null)
                                localArrayList.add(localWindowStateAnimator);
                        }
                        else
                        {
                            WindowManagerService.this.mAnimator.animate();
                            Trace.traceEnd(32L);
                            return;
                        }
                    }
                }
                j++;
            }
        }
    }

    class LayoutFields
    {
        static final int CLEAR_ORIENTATION_CHANGE_COMPLETE = 8;
        static final int SET_FORCE_HIDING_CHANGED = 4;
        static final int SET_TURN_ON_SCREEN = 16;
        static final int SET_UPDATE_ROTATION = 1;
        static final int SET_WALLPAPER_MAY_CHANGE = 2;
        int mAdjResult = 0;
        private float mButtonBrightness = -1.0F;
        boolean mDimming = false;
        private Session mHoldScreen = null;
        private boolean mObscured = false;
        boolean mOrientationChangeComplete = true;
        private float mScreenBrightness = -1.0F;
        private boolean mSyswin = false;
        private boolean mUpdateRotation = false;
        boolean mWallpaperForceHidingChanged = false;
        boolean mWallpaperMayChange = false;

        LayoutFields()
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.wm.WindowManagerService
 * JD-Core Version:        0.6.2
 */