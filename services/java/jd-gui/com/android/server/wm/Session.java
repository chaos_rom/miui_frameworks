package com.android.server.wm;

import android.content.res.Configuration;
import android.graphics.Rect;
import android.graphics.Region;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.Slog;
import android.view.IWindow;
import android.view.IWindowSession.Stub;
import android.view.InputChannel;
import android.view.Surface;
import android.view.SurfaceSession;
import android.view.WindowManager.LayoutParams;
import android.view.WindowManagerPolicy;
import com.android.internal.view.IInputContext;
import com.android.internal.view.IInputMethodClient;
import com.android.internal.view.IInputMethodManager;
import java.io.PrintWriter;
import java.util.HashSet;

final class Session extends IWindowSession.Stub
    implements IBinder.DeathRecipient
{
    final IInputMethodClient mClient;
    boolean mClientDead;
    final IInputContext mInputContext;
    int mNumWindow;
    final int mPid;
    final WindowManagerService mService;
    final String mStringName;
    SurfaceSession mSurfaceSession;
    final int mUid;

    // ERROR //
    public Session(WindowManagerService paramWindowManagerService, IInputMethodClient paramIInputMethodClient, IInputContext paramIInputContext)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 29	android/view/IWindowSession$Stub:<init>	()V
        //     4: aload_0
        //     5: iconst_0
        //     6: putfield 31	com/android/server/wm/Session:mNumWindow	I
        //     9: aload_0
        //     10: iconst_0
        //     11: putfield 33	com/android/server/wm/Session:mClientDead	Z
        //     14: aload_0
        //     15: aload_1
        //     16: putfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     19: aload_0
        //     20: aload_2
        //     21: putfield 37	com/android/server/wm/Session:mClient	Lcom/android/internal/view/IInputMethodClient;
        //     24: aload_0
        //     25: aload_3
        //     26: putfield 39	com/android/server/wm/Session:mInputContext	Lcom/android/internal/view/IInputContext;
        //     29: aload_0
        //     30: invokestatic 45	android/os/Binder:getCallingUid	()I
        //     33: putfield 47	com/android/server/wm/Session:mUid	I
        //     36: aload_0
        //     37: invokestatic 50	android/os/Binder:getCallingPid	()I
        //     40: putfield 52	com/android/server/wm/Session:mPid	I
        //     43: new 54	java/lang/StringBuilder
        //     46: dup
        //     47: invokespecial 55	java/lang/StringBuilder:<init>	()V
        //     50: astore 4
        //     52: aload 4
        //     54: ldc 57
        //     56: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     59: pop
        //     60: aload 4
        //     62: aload_0
        //     63: invokestatic 67	java/lang/System:identityHashCode	(Ljava/lang/Object;)I
        //     66: invokestatic 73	java/lang/Integer:toHexString	(I)Ljava/lang/String;
        //     69: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     72: pop
        //     73: aload 4
        //     75: ldc 75
        //     77: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     80: pop
        //     81: aload 4
        //     83: aload_0
        //     84: getfield 47	com/android/server/wm/Session:mUid	I
        //     87: invokevirtual 78	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     90: pop
        //     91: aload 4
        //     93: ldc 80
        //     95: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     98: pop
        //     99: aload_0
        //     100: aload 4
        //     102: invokevirtual 84	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     105: putfield 86	com/android/server/wm/Session:mStringName	Ljava/lang/String;
        //     108: aload_0
        //     109: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     112: getfield 92	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
        //     115: astore 10
        //     117: aload 10
        //     119: monitorenter
        //     120: aload_0
        //     121: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     124: getfield 96	com/android/server/wm/WindowManagerService:mInputMethodManager	Lcom/android/internal/view/IInputMethodManager;
        //     127: ifnonnull +32 -> 159
        //     130: aload_0
        //     131: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     134: getfield 99	com/android/server/wm/WindowManagerService:mHaveInputMethods	Z
        //     137: ifeq +22 -> 159
        //     140: ldc 101
        //     142: invokestatic 107	android/os/ServiceManager:getService	(Ljava/lang/String;)Landroid/os/IBinder;
        //     145: astore 17
        //     147: aload_0
        //     148: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     151: aload 17
        //     153: invokestatic 113	com/android/internal/view/IInputMethodManager$Stub:asInterface	(Landroid/os/IBinder;)Lcom/android/internal/view/IInputMethodManager;
        //     156: putfield 96	com/android/server/wm/WindowManagerService:mInputMethodManager	Lcom/android/internal/view/IInputMethodManager;
        //     159: aload 10
        //     161: monitorexit
        //     162: invokestatic 117	android/os/Binder:clearCallingIdentity	()J
        //     165: lstore 12
        //     167: aload_0
        //     168: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     171: getfield 96	com/android/server/wm/WindowManagerService:mInputMethodManager	Lcom/android/internal/view/IInputMethodManager;
        //     174: ifnull +52 -> 226
        //     177: aload_0
        //     178: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     181: getfield 96	com/android/server/wm/WindowManagerService:mInputMethodManager	Lcom/android/internal/view/IInputMethodManager;
        //     184: aload_2
        //     185: aload_3
        //     186: aload_0
        //     187: getfield 47	com/android/server/wm/Session:mUid	I
        //     190: aload_0
        //     191: getfield 52	com/android/server/wm/Session:mPid	I
        //     194: invokeinterface 123 5 0
        //     199: aload_2
        //     200: invokeinterface 129 1 0
        //     205: aload_0
        //     206: iconst_0
        //     207: invokeinterface 135 3 0
        //     212: lload 12
        //     214: invokestatic 139	android/os/Binder:restoreCallingIdentity	(J)V
        //     217: return
        //     218: astore 11
        //     220: aload 10
        //     222: monitorexit
        //     223: aload 11
        //     225: athrow
        //     226: aload_2
        //     227: iconst_0
        //     228: invokeinterface 143 2 0
        //     233: goto -34 -> 199
        //     236: astore 15
        //     238: aload_0
        //     239: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     242: getfield 96	com/android/server/wm/WindowManagerService:mInputMethodManager	Lcom/android/internal/view/IInputMethodManager;
        //     245: ifnull +16 -> 261
        //     248: aload_0
        //     249: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     252: getfield 96	com/android/server/wm/WindowManagerService:mInputMethodManager	Lcom/android/internal/view/IInputMethodManager;
        //     255: aload_2
        //     256: invokeinterface 147 2 0
        //     261: lload 12
        //     263: invokestatic 139	android/os/Binder:restoreCallingIdentity	(J)V
        //     266: goto -49 -> 217
        //     269: astore 14
        //     271: lload 12
        //     273: invokestatic 139	android/os/Binder:restoreCallingIdentity	(J)V
        //     276: aload 14
        //     278: athrow
        //     279: astore 16
        //     281: goto -20 -> 261
        //
        // Exception table:
        //     from	to	target	type
        //     120	162	218	finally
        //     220	223	218	finally
        //     167	212	236	android/os/RemoteException
        //     226	233	236	android/os/RemoteException
        //     167	212	269	finally
        //     226	233	269	finally
        //     238	261	269	finally
        //     238	261	279	android/os/RemoteException
    }

    public int add(IWindow paramIWindow, int paramInt1, WindowManager.LayoutParams paramLayoutParams, int paramInt2, Rect paramRect, InputChannel paramInputChannel)
    {
        return this.mService.addWindow(this, paramIWindow, paramInt1, paramLayoutParams, paramInt2, paramRect, paramInputChannel);
    }

    public int addWithoutInputChannel(IWindow paramIWindow, int paramInt1, WindowManager.LayoutParams paramLayoutParams, int paramInt2, Rect paramRect)
    {
        return this.mService.addWindow(this, paramIWindow, paramInt1, paramLayoutParams, paramInt2, paramRect, null);
    }

    public void binderDied()
    {
        try
        {
            if (this.mService.mInputMethodManager != null)
                this.mService.mInputMethodManager.removeClient(this.mClient);
            label26: synchronized (this.mService.mWindowMap)
            {
                this.mClient.asBinder().unlinkToDeath(this, 0);
                this.mClientDead = true;
                killSessionLocked();
                return;
            }
        }
        catch (RemoteException localRemoteException)
        {
            break label26;
        }
    }

    public void dragRecipientEntered(IWindow paramIWindow)
    {
    }

    public void dragRecipientExited(IWindow paramIWindow)
    {
    }

    void dump(PrintWriter paramPrintWriter, String paramString)
    {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mNumWindow=");
        paramPrintWriter.print(this.mNumWindow);
        paramPrintWriter.print(" mClientDead=");
        paramPrintWriter.print(this.mClientDead);
        paramPrintWriter.print(" mSurfaceSession=");
        paramPrintWriter.println(this.mSurfaceSession);
    }

    public void finishDrawing(IWindow paramIWindow)
    {
        this.mService.finishDrawingWindow(this, paramIWindow);
    }

    public void getDisplayFrame(IWindow paramIWindow, Rect paramRect)
    {
        this.mService.getWindowDisplayFrame(this, paramIWindow, paramRect);
    }

    public boolean getInTouchMode()
    {
        synchronized (this.mService.mWindowMap)
        {
            boolean bool = this.mService.mInTouchMode;
            return bool;
        }
    }

    void killSessionLocked()
    {
        if ((this.mNumWindow <= 0) && (this.mClientDead))
        {
            this.mService.mSessions.remove(this);
            if (this.mSurfaceSession == null);
        }
        try
        {
            this.mSurfaceSession.kill();
            this.mSurfaceSession = null;
            return;
        }
        catch (Exception localException)
        {
            while (true)
                Slog.w("WindowManager", "Exception thrown when killing surface session " + this.mSurfaceSession + " in session " + this + ": " + localException.toString());
        }
    }

    public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
        throws RemoteException
    {
        try
        {
            boolean bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            return bool;
        }
        catch (RuntimeException localRuntimeException)
        {
            if (!(localRuntimeException instanceof SecurityException))
                Slog.e("WindowManager", "Window Session Crash", localRuntimeException);
            throw localRuntimeException;
        }
    }

    public boolean outOfMemory(IWindow paramIWindow)
    {
        return this.mService.outOfMemoryWindow(this, paramIWindow);
    }

    public void performDeferredDestroy(IWindow paramIWindow)
    {
        this.mService.performDeferredDestroyWindow(this, paramIWindow);
    }

    // ERROR //
    public boolean performDrag(IWindow paramIWindow, IBinder paramIBinder, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, android.content.ClipData paramClipData)
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore 8
        //     3: aload_0
        //     4: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     7: getfield 92	com/android/server/wm/WindowManagerService:mWindowMap	Ljava/util/HashMap;
        //     10: astore 9
        //     12: aload 9
        //     14: monitorenter
        //     15: aload_0
        //     16: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     19: getfield 272	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
        //     22: ifnonnull +31 -> 53
        //     25: ldc 226
        //     27: ldc_w 274
        //     30: invokestatic 242	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     33: pop
        //     34: new 276	java/lang/IllegalStateException
        //     37: dup
        //     38: ldc_w 278
        //     41: invokespecial 280	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     44: athrow
        //     45: astore 10
        //     47: aload 9
        //     49: monitorexit
        //     50: aload 10
        //     52: athrow
        //     53: aload_2
        //     54: aload_0
        //     55: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     58: getfield 272	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
        //     61: getfield 286	com/android/server/wm/DragState:mToken	Landroid/os/IBinder;
        //     64: if_acmpeq +23 -> 87
        //     67: ldc 226
        //     69: ldc_w 288
        //     72: invokestatic 242	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     75: pop
        //     76: new 276	java/lang/IllegalStateException
        //     79: dup
        //     80: ldc_w 290
        //     83: invokespecial 280	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     86: athrow
        //     87: aload_0
        //     88: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     91: aconst_null
        //     92: aload_1
        //     93: iconst_0
        //     94: invokevirtual 294	com/android/server/wm/WindowManagerService:windowForClientLocked	(Lcom/android/server/wm/Session;Landroid/view/IWindow;Z)Lcom/android/server/wm/WindowState;
        //     97: astore 11
        //     99: aload 11
        //     101: ifnonnull +35 -> 136
        //     104: ldc 226
        //     106: new 54	java/lang/StringBuilder
        //     109: dup
        //     110: invokespecial 55	java/lang/StringBuilder:<init>	()V
        //     113: ldc_w 296
        //     116: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     119: aload_1
        //     120: invokevirtual 231	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     123: invokevirtual 84	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     126: invokestatic 242	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     129: pop
        //     130: aload 9
        //     132: monitorexit
        //     133: goto +271 -> 404
        //     136: aload_0
        //     137: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     140: getfield 300	com/android/server/wm/WindowManagerService:mH	Lcom/android/server/wm/WindowManagerService$H;
        //     143: bipush 20
        //     145: aload_1
        //     146: invokeinterface 303 1 0
        //     151: invokevirtual 309	com/android/server/wm/WindowManagerService$H:removeMessages	(ILjava/lang/Object;)V
        //     154: aload_0
        //     155: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     158: getfield 272	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
        //     161: invokevirtual 312	com/android/server/wm/DragState:register	()V
        //     164: aload_0
        //     165: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     168: getfield 316	com/android/server/wm/WindowManagerService:mInputMonitor	Lcom/android/server/wm/InputMonitor;
        //     171: iconst_1
        //     172: invokevirtual 321	com/android/server/wm/InputMonitor:updateInputWindowsLw	(Z)V
        //     175: aload_0
        //     176: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     179: getfield 325	com/android/server/wm/WindowManagerService:mInputManager	Lcom/android/server/input/InputManagerService;
        //     182: aload 11
        //     184: getfield 331	com/android/server/wm/WindowState:mInputChannel	Landroid/view/InputChannel;
        //     187: aload_0
        //     188: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     191: getfield 272	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
        //     194: getfield 334	com/android/server/wm/DragState:mServerChannel	Landroid/view/InputChannel;
        //     197: invokevirtual 340	com/android/server/input/InputManagerService:transferTouchFocus	(Landroid/view/InputChannel;Landroid/view/InputChannel;)Z
        //     200: ifne +47 -> 247
        //     203: ldc 226
        //     205: ldc_w 342
        //     208: invokestatic 344	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     211: pop
        //     212: aload_0
        //     213: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     216: getfield 272	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
        //     219: invokevirtual 347	com/android/server/wm/DragState:unregister	()V
        //     222: aload_0
        //     223: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     226: aconst_null
        //     227: putfield 272	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
        //     230: aload_0
        //     231: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     234: getfield 316	com/android/server/wm/WindowManagerService:mInputMonitor	Lcom/android/server/wm/InputMonitor;
        //     237: iconst_1
        //     238: invokevirtual 321	com/android/server/wm/InputMonitor:updateInputWindowsLw	(Z)V
        //     241: aload 9
        //     243: monitorexit
        //     244: goto +160 -> 404
        //     247: aload_0
        //     248: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     251: getfield 272	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
        //     254: aload 7
        //     256: putfield 351	com/android/server/wm/DragState:mData	Landroid/content/ClipData;
        //     259: aload_0
        //     260: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     263: getfield 272	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
        //     266: fload_3
        //     267: putfield 355	com/android/server/wm/DragState:mCurrentX	F
        //     270: aload_0
        //     271: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     274: getfield 272	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
        //     277: fload 4
        //     279: putfield 358	com/android/server/wm/DragState:mCurrentY	F
        //     282: aload_0
        //     283: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     286: getfield 272	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
        //     289: fload_3
        //     290: fload 4
        //     292: invokevirtual 362	com/android/server/wm/DragState:broadcastDragStartedLw	(FF)V
        //     295: aload_0
        //     296: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     299: getfield 272	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
        //     302: fload 5
        //     304: putfield 365	com/android/server/wm/DragState:mThumbOffsetX	F
        //     307: aload_0
        //     308: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     311: getfield 272	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
        //     314: fload 6
        //     316: putfield 368	com/android/server/wm/DragState:mThumbOffsetY	F
        //     319: aload_0
        //     320: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     323: getfield 272	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
        //     326: getfield 372	com/android/server/wm/DragState:mSurface	Landroid/view/Surface;
        //     329: astore 12
        //     331: invokestatic 377	android/view/Surface:openTransaction	()V
        //     334: fload_3
        //     335: fload 5
        //     337: fsub
        //     338: fstore 13
        //     340: fload 4
        //     342: fload 6
        //     344: fsub
        //     345: fstore 14
        //     347: aload 12
        //     349: fload 13
        //     351: fload 14
        //     353: invokevirtual 380	android/view/Surface:setPosition	(FF)V
        //     356: aload 12
        //     358: ldc_w 381
        //     361: invokevirtual 385	android/view/Surface:setAlpha	(F)V
        //     364: aload 12
        //     366: aload_0
        //     367: getfield 35	com/android/server/wm/Session:mService	Lcom/android/server/wm/WindowManagerService;
        //     370: getfield 272	com/android/server/wm/WindowManagerService:mDragState	Lcom/android/server/wm/DragState;
        //     373: invokevirtual 388	com/android/server/wm/DragState:getDragLayerLw	()I
        //     376: invokevirtual 391	android/view/Surface:setLayer	(I)V
        //     379: aload 12
        //     381: invokevirtual 394	android/view/Surface:show	()V
        //     384: invokestatic 397	android/view/Surface:closeTransaction	()V
        //     387: aload 9
        //     389: monitorexit
        //     390: iconst_1
        //     391: istore 8
        //     393: goto +11 -> 404
        //     396: astore 15
        //     398: invokestatic 397	android/view/Surface:closeTransaction	()V
        //     401: aload 15
        //     403: athrow
        //     404: iload 8
        //     406: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     15	50	45	finally
        //     53	334	45	finally
        //     384	404	45	finally
        //     347	384	396	finally
    }

    public boolean performHapticFeedback(IWindow paramIWindow, int paramInt, boolean paramBoolean)
    {
        synchronized (this.mService.mWindowMap)
        {
            long l = Binder.clearCallingIdentity();
            try
            {
                boolean bool = this.mService.mPolicy.performHapticFeedbackLw(this.mService.windowForClientLocked(this, paramIWindow, true), paramInt, paramBoolean);
                Binder.restoreCallingIdentity(l);
                return bool;
            }
            finally
            {
                localObject2 = finally;
                Binder.restoreCallingIdentity(l);
                throw localObject2;
            }
        }
    }

    public IBinder prepareDrag(IWindow paramIWindow, int paramInt1, int paramInt2, int paramInt3, Surface paramSurface)
    {
        return this.mService.prepareDragSurface(paramIWindow, this.mSurfaceSession, paramInt1, paramInt2, paramInt3, paramSurface);
    }

    public int relayout(IWindow paramIWindow, int paramInt1, WindowManager.LayoutParams paramLayoutParams, int paramInt2, int paramInt3, int paramInt4, int paramInt5, Rect paramRect1, Rect paramRect2, Rect paramRect3, Configuration paramConfiguration, Surface paramSurface)
    {
        return this.mService.relayoutWindow(this, paramIWindow, paramInt1, paramLayoutParams, paramInt2, paramInt3, paramInt4, paramInt5, paramRect1, paramRect2, paramRect3, paramConfiguration, paramSurface);
    }

    public void remove(IWindow paramIWindow)
    {
        this.mService.removeWindow(this, paramIWindow);
    }

    public void reportDropResult(IWindow paramIWindow, boolean paramBoolean)
    {
        IBinder localIBinder = paramIWindow.asBinder();
        while (true)
        {
            long l;
            synchronized (this.mService.mWindowMap)
            {
                l = Binder.clearCallingIdentity();
                try
                {
                    if (this.mService.mDragState == null)
                    {
                        Slog.w("WindowManager", "Drop result given but no drag in progress");
                        Binder.restoreCallingIdentity(l);
                        return;
                    }
                    if (this.mService.mDragState.mToken != localIBinder)
                    {
                        Slog.w("WindowManager", "Invalid drop-result claim by " + paramIWindow);
                        throw new IllegalStateException("reportDropResult() by non-recipient");
                    }
                }
                finally
                {
                    Binder.restoreCallingIdentity(l);
                }
            }
            this.mService.mH.removeMessages(21, paramIWindow.asBinder());
            if (this.mService.windowForClientLocked(null, paramIWindow, false) == null)
            {
                Slog.w("WindowManager", "Bad result-reporting window " + paramIWindow);
                Binder.restoreCallingIdentity(l);
            }
            else
            {
                this.mService.mDragState.mDragResult = paramBoolean;
                this.mService.mDragState.endDragLw();
                Binder.restoreCallingIdentity(l);
            }
        }
    }

    public Bundle sendWallpaperCommand(IBinder paramIBinder, String paramString, int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle, boolean paramBoolean)
    {
        synchronized (this.mService.mWindowMap)
        {
            long l = Binder.clearCallingIdentity();
            try
            {
                Bundle localBundle = this.mService.sendWindowWallpaperCommandLocked(this.mService.windowForClientLocked(this, paramIBinder, true), paramString, paramInt1, paramInt2, paramInt3, paramBundle, paramBoolean);
                Binder.restoreCallingIdentity(l);
                return localBundle;
            }
            finally
            {
                localObject2 = finally;
                Binder.restoreCallingIdentity(l);
                throw localObject2;
            }
        }
    }

    public void setInTouchMode(boolean paramBoolean)
    {
        synchronized (this.mService.mWindowMap)
        {
            this.mService.mInTouchMode = paramBoolean;
            return;
        }
    }

    public void setInsets(IWindow paramIWindow, int paramInt, Rect paramRect1, Rect paramRect2, Region paramRegion)
    {
        this.mService.setInsetsWindow(this, paramIWindow, paramInt, paramRect1, paramRect2, paramRegion);
    }

    public void setTransparentRegion(IWindow paramIWindow, Region paramRegion)
    {
        this.mService.setTransparentRegionWindow(this, paramIWindow, paramRegion);
    }

    public void setWallpaperPosition(IBinder paramIBinder, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        synchronized (this.mService.mWindowMap)
        {
            long l = Binder.clearCallingIdentity();
            try
            {
                this.mService.setWindowWallpaperPositionLocked(this.mService.windowForClientLocked(this, paramIBinder, true), paramFloat1, paramFloat2, paramFloat3, paramFloat4);
                Binder.restoreCallingIdentity(l);
                return;
            }
            finally
            {
                localObject2 = finally;
                Binder.restoreCallingIdentity(l);
                throw localObject2;
            }
        }
    }

    public String toString()
    {
        return this.mStringName;
    }

    public void wallpaperCommandComplete(IBinder paramIBinder, Bundle paramBundle)
    {
        this.mService.wallpaperCommandComplete(paramIBinder, paramBundle);
    }

    public void wallpaperOffsetsComplete(IBinder paramIBinder)
    {
        this.mService.wallpaperOffsetsComplete(paramIBinder);
    }

    void windowAddedLocked()
    {
        if (this.mSurfaceSession == null)
        {
            this.mSurfaceSession = new SurfaceSession();
            this.mService.mSessions.add(this);
        }
        this.mNumWindow = (1 + this.mNumWindow);
    }

    void windowRemovedLocked()
    {
        this.mNumWindow = (-1 + this.mNumWindow);
        killSessionLocked();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.wm.Session
 * JD-Core Version:        0.6.2
 */