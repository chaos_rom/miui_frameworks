package com.android.server.wm;

import android.os.IBinder;
import java.io.PrintWriter;
import java.util.ArrayList;

class WindowToken
{
    AppWindowToken appWindowToken;
    final boolean explicit;
    boolean hasVisible;
    boolean hidden;
    boolean paused = false;
    boolean sendingToBottom;
    final WindowManagerService service;
    String stringName;
    final IBinder token;
    boolean waitingToHide;
    boolean waitingToShow;
    final int windowType;
    final ArrayList<WindowState> windows = new ArrayList();

    WindowToken(WindowManagerService paramWindowManagerService, IBinder paramIBinder, int paramInt, boolean paramBoolean)
    {
        this.service = paramWindowManagerService;
        this.token = paramIBinder;
        this.windowType = paramInt;
        this.explicit = paramBoolean;
    }

    void dump(PrintWriter paramPrintWriter, String paramString)
    {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("token=");
        paramPrintWriter.println(this.token);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("windows=");
        paramPrintWriter.println(this.windows);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("windowType=");
        paramPrintWriter.print(this.windowType);
        paramPrintWriter.print(" hidden=");
        paramPrintWriter.print(this.hidden);
        paramPrintWriter.print(" hasVisible=");
        paramPrintWriter.println(this.hasVisible);
        if ((this.waitingToShow) || (this.waitingToHide) || (this.sendingToBottom))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("waitingToShow=");
            paramPrintWriter.print(this.waitingToShow);
            paramPrintWriter.print(" waitingToHide=");
            paramPrintWriter.print(this.waitingToHide);
            paramPrintWriter.print(" sendingToBottom=");
            paramPrintWriter.print(this.sendingToBottom);
        }
    }

    public String toString()
    {
        if (this.stringName == null)
        {
            StringBuilder localStringBuilder = new StringBuilder();
            localStringBuilder.append("WindowToken{");
            localStringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
            localStringBuilder.append(" token=");
            localStringBuilder.append(this.token);
            localStringBuilder.append('}');
            this.stringName = localStringBuilder.toString();
        }
        return this.stringName;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.wm.WindowToken
 * JD-Core Version:        0.6.2
 */