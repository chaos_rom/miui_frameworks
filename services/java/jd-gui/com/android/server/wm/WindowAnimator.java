package com.android.server.wm;

import android.content.Context;
import android.view.WindowManager.LayoutParams;
import android.view.WindowManagerPolicy;
import android.view.animation.Animation;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;

public class WindowAnimator
{
    private static final int KEYGUARD_ANIMATING_IN = 1;
    private static final int KEYGUARD_ANIMATING_OUT = 3;
    private static final int KEYGUARD_NOT_SHOWN = 0;
    private static final int KEYGUARD_SHOWN = 2;
    private static final String TAG = "WindowAnimator";
    static final int WALLPAPER_ACTION_PENDING = 1;
    int mAdjResult;
    private int mAnimTransactionSequence;
    boolean mAnimating;
    int mBulkUpdateParams = 0;
    final Context mContext;
    WindowState mCurrentFocus;
    long mCurrentTime;
    WindowState mDetachedWallpaper = null;
    int mDh;
    DimAnimator mDimAnimator = null;
    DimAnimator.Parameters mDimParams = null;
    int mDw;
    int mForceHiding;
    int mInnerDh;
    int mInnerDw;
    int mPendingActions;
    int mPendingLayoutChanges;
    final WindowManagerPolicy mPolicy;
    ScreenRotationAnimation mScreenRotationAnimation = null;
    final WindowManagerService mService;
    ArrayList<WindowStateAnimator> mWinAnimators = new ArrayList();
    WindowState mWindowAnimationBackground;
    int mWindowAnimationBackgroundColor;
    DimSurface mWindowAnimationBackgroundSurface = null;
    WindowState mWindowDetachedWallpaper = null;

    WindowAnimator(WindowManagerService paramWindowManagerService, Context paramContext, WindowManagerPolicy paramWindowManagerPolicy)
    {
        this.mService = paramWindowManagerService;
        this.mContext = paramContext;
        this.mPolicy = paramWindowManagerPolicy;
    }

    private void performAnimationsLocked()
    {
        this.mForceHiding = 0;
        this.mDetachedWallpaper = null;
        this.mWindowAnimationBackground = null;
        this.mWindowAnimationBackgroundColor = 0;
        updateWindowsAndWallpaperLocked();
        if ((0x4 & this.mPendingLayoutChanges) != 0)
            this.mPendingActions = (0x1 | this.mPendingActions);
        testTokenMayBeDrawnLocked();
    }

    private void testTokenMayBeDrawnLocked()
    {
        ArrayList localArrayList = this.mService.mAnimatingAppTokens;
        int i = localArrayList.size();
        int j = 0;
        if (j < i)
        {
            AppWindowToken localAppWindowToken = (AppWindowToken)localArrayList.get(j);
            boolean bool = localAppWindowToken.allDrawn;
            if (bool != localAppWindowToken.mAppAnimator.allDrawn)
            {
                localAppWindowToken.mAppAnimator.allDrawn = bool;
                if (bool)
                {
                    if (!localAppWindowToken.mAppAnimator.freezingScreen)
                        break label112;
                    localAppWindowToken.mAppAnimator.showAllWindowsLocked();
                    this.mService.unsetAppFreezingScreenLocked(localAppWindowToken, false, true);
                    this.mPendingLayoutChanges = (0x4 | this.mPendingLayoutChanges);
                }
            }
            while (true)
            {
                j++;
                break;
                label112: this.mPendingLayoutChanges = (0x8 | this.mPendingLayoutChanges);
                this.mService.debugLayoutRepeats("testTokenMayBeDrawnLocked", this.mPendingLayoutChanges);
                if (!this.mService.mOpeningApps.contains(localAppWindowToken))
                    this.mAnimating |= localAppWindowToken.mAppAnimator.showAllWindowsLocked();
            }
        }
    }

    private void testWallpaperAndBackgroundLocked()
    {
        if (this.mWindowDetachedWallpaper != this.mDetachedWallpaper)
        {
            this.mWindowDetachedWallpaper = this.mDetachedWallpaper;
            this.mBulkUpdateParams = (0x2 | this.mBulkUpdateParams);
        }
        int j;
        if (this.mWindowAnimationBackgroundColor != 0)
        {
            Object localObject = this.mWindowAnimationBackground;
            if ((this.mService.mWallpaperTarget == localObject) || (this.mService.mLowerWallpaperTarget == localObject) || (this.mService.mUpperWallpaperTarget == localObject))
            {
                int i = this.mService.mWindows.size();
                j = 0;
                if (j < i)
                {
                    WindowState localWindowState = (WindowState)this.mService.mWindows.get(j);
                    if (!localWindowState.mIsWallpaper)
                        break label182;
                    localObject = localWindowState;
                }
            }
            if (this.mWindowAnimationBackgroundSurface == null)
                this.mWindowAnimationBackgroundSurface = new DimSurface(this.mService.mFxSession);
            int k = this.mDw;
            int m = this.mDh;
            this.mWindowAnimationBackgroundSurface.show(k, m, -1 + ((WindowState)localObject).mWinAnimator.mAnimLayer, this.mWindowAnimationBackgroundColor);
        }
        while (true)
        {
            return;
            label182: j++;
            break;
            if (this.mWindowAnimationBackgroundSurface != null)
                this.mWindowAnimationBackgroundSurface.hide();
        }
    }

    private void updateWindowsAndWallpaperLocked()
    {
        this.mAnimTransactionSequence = (1 + this.mAnimTransactionSequence);
        ArrayList localArrayList = null;
        boolean bool1 = false;
        int i = -1 + this.mService.mWindows.size();
        if (i >= 0)
        {
            WindowState localWindowState = (WindowState)this.mService.mWindows.get(i);
            WindowStateAnimator localWindowStateAnimator2 = localWindowState.mWinAnimator;
            int k = localWindowStateAnimator2.mAttrFlags;
            AppWindowAnimator localAppWindowAnimator2;
            if (localWindowStateAnimator2.mSurface != null)
            {
                boolean bool2 = localWindowStateAnimator2.mWasAnimating;
                boolean bool3 = localWindowStateAnimator2.stepAnimationLocked(this.mCurrentTime);
                if (bool3)
                {
                    if (localWindowStateAnimator2.mAnimation != null)
                    {
                        if (((0x100000 & k) != 0) && (localWindowStateAnimator2.mAnimation.getDetachWallpaper()))
                            this.mDetachedWallpaper = localWindowState;
                        int i1 = localWindowStateAnimator2.mAnimation.getBackgroundColor();
                        if ((i1 != 0) && ((this.mWindowAnimationBackground == null) || (localWindowStateAnimator2.mAnimLayer < this.mWindowAnimationBackground.mWinAnimator.mAnimLayer)))
                        {
                            this.mWindowAnimationBackground = localWindowState;
                            this.mWindowAnimationBackgroundColor = i1;
                        }
                    }
                    this.mAnimating = true;
                }
                if (localWindowState.mAppToken != null)
                    break label586;
                localAppWindowAnimator2 = null;
                label194: if ((localAppWindowAnimator2 != null) && (localAppWindowAnimator2.animation != null) && (localAppWindowAnimator2.animating))
                {
                    if (((0x100000 & k) != 0) && (localAppWindowAnimator2.animation.getDetachWallpaper()))
                        this.mDetachedWallpaper = localWindowState;
                    int n = localAppWindowAnimator2.animation.getBackgroundColor();
                    if ((n != 0) && ((this.mWindowAnimationBackground == null) || (localWindowStateAnimator2.mAnimLayer < this.mWindowAnimationBackground.mWinAnimator.mAnimLayer)))
                    {
                        this.mWindowAnimationBackground = localWindowState;
                        this.mWindowAnimationBackgroundColor = n;
                    }
                }
                if ((bool2) && (!localWindowStateAnimator2.mAnimating) && (this.mService.mWallpaperTarget == localWindowState))
                {
                    this.mBulkUpdateParams = (0x2 | this.mBulkUpdateParams);
                    this.mPendingLayoutChanges = (0x4 | this.mPendingLayoutChanges);
                    this.mService.debugLayoutRepeats("updateWindowsAndWallpaperLocked 2", this.mPendingLayoutChanges);
                }
                if (!this.mPolicy.doesForceHide(localWindowState, localWindowState.mAttrs))
                    break label615;
                if ((!bool2) && (bool3))
                {
                    this.mBulkUpdateParams = (0x4 | this.mBulkUpdateParams);
                    this.mPendingLayoutChanges = (0x4 | this.mPendingLayoutChanges);
                    this.mService.debugLayoutRepeats("updateWindowsAndWallpaperLocked 3", this.mPendingLayoutChanges);
                    this.mService.mFocusMayChange = true;
                }
                if (localWindowState.isReadyForDisplay())
                {
                    if (!bool3)
                        break label607;
                    if (!localWindowStateAnimator2.mAnimationIsEntrance)
                        break label599;
                    this.mForceHiding = 1;
                }
            }
            label447: AppWindowToken localAppWindowToken = localWindowState.mAppToken;
            if ((localWindowStateAnimator2.mDrawState == 3) && ((localAppWindowToken == null) || (localAppWindowToken.allDrawn)) && (localWindowStateAnimator2.performShowLocked()))
            {
                this.mPendingLayoutChanges = (0x8 | this.mPendingLayoutChanges);
                this.mService.debugLayoutRepeats("updateWindowsAndWallpaperLocked 5", this.mPendingLayoutChanges);
            }
            if (localAppWindowToken == null);
            for (AppWindowAnimator localAppWindowAnimator1 = null; ; localAppWindowAnimator1 = localAppWindowToken.mAppAnimator)
            {
                if ((localAppWindowAnimator1 != null) && (localAppWindowAnimator1.thumbnail != null))
                {
                    if (localAppWindowAnimator1.thumbnailTransactionSeq != this.mAnimTransactionSequence)
                    {
                        localAppWindowAnimator1.thumbnailTransactionSeq = this.mAnimTransactionSequence;
                        localAppWindowAnimator1.thumbnailLayer = 0;
                    }
                    if (localAppWindowAnimator1.thumbnailLayer < localWindowStateAnimator2.mAnimLayer)
                        localAppWindowAnimator1.thumbnailLayer = localWindowStateAnimator2.mAnimLayer;
                }
                i--;
                break;
                label586: localAppWindowAnimator2 = localWindowState.mAppToken.mAppAnimator;
                break label194;
                label599: this.mForceHiding = 3;
                break label447;
                label607: this.mForceHiding = 2;
                break label447;
                label615: if (!this.mPolicy.canBeForceHidden(localWindowState, localWindowState.mAttrs))
                    break label447;
                int m;
                label649: boolean bool4;
                if ((0x80000 & localWindowStateAnimator2.mAttrFlags) == 0)
                {
                    m = 1;
                    if (((this.mForceHiding != 1) || ((localWindowStateAnimator2.isAnimating()) && (m == 0))) && ((this.mForceHiding != 2) || (m == 0)))
                        break label748;
                    bool4 = localWindowState.hideLw(false, false);
                }
                while (true)
                {
                    if ((!bool4) || ((0x100000 & k) == 0))
                        break label845;
                    this.mBulkUpdateParams = (0x2 | this.mBulkUpdateParams);
                    this.mPendingLayoutChanges = (0x4 | this.mPendingLayoutChanges);
                    this.mService.debugLayoutRepeats("updateWindowsAndWallpaperLocked 4", this.mPendingLayoutChanges);
                    break;
                    m = 0;
                    break label649;
                    label748: bool4 = localWindowState.showLw(false, false);
                    if (bool4)
                    {
                        if (((0x4 & this.mBulkUpdateParams) != 0) && (localWindowState.isVisibleNow()))
                        {
                            if (localArrayList == null)
                                localArrayList = new ArrayList();
                            localArrayList.add(localWindowStateAnimator2);
                            if ((0x100000 & localWindowState.mAttrs.flags) != 0)
                                bool1 = true;
                        }
                        if ((this.mCurrentFocus == null) || (this.mCurrentFocus.mLayer < localWindowState.mLayer))
                            this.mService.mFocusMayChange = true;
                    }
                }
                label845: break label447;
            }
        }
        if (localArrayList != null)
            for (int j = -1 + localArrayList.size(); j >= 0; j--)
            {
                Animation localAnimation = this.mPolicy.createForceHideEnterAnimation(bool1);
                if (localAnimation != null)
                {
                    WindowStateAnimator localWindowStateAnimator1 = (WindowStateAnimator)localArrayList.get(j);
                    localWindowStateAnimator1.setAnimation(localAnimation);
                    localWindowStateAnimator1.mAnimationIsEntrance = true;
                }
            }
    }

    private void updateWindowsAppsAndRotationAnimationsLocked()
    {
        ArrayList localArrayList = this.mService.mAnimatingAppTokens;
        int i = localArrayList.size();
        int j = 0;
        if (j < i)
        {
            AppWindowAnimator localAppWindowAnimator2 = ((AppWindowToken)localArrayList.get(j)).mAppAnimator;
            int i1;
            if ((localAppWindowAnimator2.animation != null) && (localAppWindowAnimator2.animation != AppWindowAnimator.sDummyAnimation))
            {
                i1 = 1;
                label55: if (!localAppWindowAnimator2.stepAnimationLocked(this.mCurrentTime, this.mInnerDw, this.mInnerDh))
                    break label92;
                this.mAnimating = true;
            }
            while (true)
            {
                j++;
                break;
                i1 = 0;
                break label55;
                label92: if (i1 != 0)
                {
                    this.mPendingLayoutChanges = (0x4 | this.mPendingLayoutChanges);
                    this.mService.debugLayoutRepeats("appToken " + localAppWindowAnimator2.mAppToken + " done", this.mPendingLayoutChanges);
                }
            }
        }
        int k = this.mService.mExitingAppTokens.size();
        int m = 0;
        if (m < k)
        {
            AppWindowAnimator localAppWindowAnimator1 = ((AppWindowToken)this.mService.mExitingAppTokens.get(m)).mAppAnimator;
            int n;
            if ((localAppWindowAnimator1.animation != null) && (localAppWindowAnimator1.animation != AppWindowAnimator.sDummyAnimation))
            {
                n = 1;
                label215: if (!localAppWindowAnimator1.stepAnimationLocked(this.mCurrentTime, this.mInnerDw, this.mInnerDh))
                    break label252;
                this.mAnimating = true;
            }
            while (true)
            {
                m++;
                break;
                n = 0;
                break label215;
                label252: if (n != 0)
                {
                    this.mPendingLayoutChanges = (0x4 | this.mPendingLayoutChanges);
                    this.mService.debugLayoutRepeats("exiting appToken " + localAppWindowAnimator1.mAppToken + " done", this.mPendingLayoutChanges);
                }
            }
        }
        if ((this.mScreenRotationAnimation != null) && (this.mScreenRotationAnimation.isAnimating()))
        {
            if (!this.mScreenRotationAnimation.stepAnimationLocked(this.mCurrentTime))
                break label348;
            this.mAnimating = true;
        }
        while (true)
        {
            return;
            label348: this.mBulkUpdateParams = (0x1 | this.mBulkUpdateParams);
            this.mScreenRotationAnimation.kill();
            this.mScreenRotationAnimation = null;
        }
    }

    /** @deprecated */
    // ERROR //
    void animate()
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: iconst_0
        //     4: putfield 100	com/android/server/wm/WindowAnimator:mPendingLayoutChanges	I
        //     7: aload_0
        //     8: invokestatic 375	android/os/SystemClock:uptimeMillis	()J
        //     11: putfield 216	com/android/server/wm/WindowAnimator:mCurrentTime	J
        //     14: aload_0
        //     15: iconst_0
        //     16: putfield 78	com/android/server/wm/WindowAnimator:mBulkUpdateParams	I
        //     19: aload_0
        //     20: getfield 155	com/android/server/wm/WindowAnimator:mAnimating	Z
        //     23: istore_2
        //     24: aload_0
        //     25: iconst_0
        //     26: putfield 155	com/android/server/wm/WindowAnimator:mAnimating	Z
        //     29: invokestatic 380	android/view/Surface:openTransaction	()V
        //     32: aload_0
        //     33: invokespecial 382	com/android/server/wm/WindowAnimator:updateWindowsAppsAndRotationAnimationsLocked	()V
        //     36: aload_0
        //     37: invokespecial 384	com/android/server/wm/WindowAnimator:performAnimationsLocked	()V
        //     40: aload_0
        //     41: invokespecial 386	com/android/server/wm/WindowAnimator:testWallpaperAndBackgroundLocked	()V
        //     44: aload_0
        //     45: getfield 70	com/android/server/wm/WindowAnimator:mScreenRotationAnimation	Lcom/android/server/wm/ScreenRotationAnimation;
        //     48: ifnull +10 -> 58
        //     51: aload_0
        //     52: getfield 70	com/android/server/wm/WindowAnimator:mScreenRotationAnimation	Lcom/android/server/wm/ScreenRotationAnimation;
        //     55: invokevirtual 389	com/android/server/wm/ScreenRotationAnimation:updateSurfaces	()V
        //     58: aload_0
        //     59: getfield 68	com/android/server/wm/WindowAnimator:mWinAnimators	Ljava/util/ArrayList;
        //     62: invokevirtual 114	java/util/ArrayList:size	()I
        //     65: istore 6
        //     67: iconst_0
        //     68: istore 7
        //     70: iload 7
        //     72: iload 6
        //     74: if_icmpge +25 -> 99
        //     77: aload_0
        //     78: getfield 68	com/android/server/wm/WindowAnimator:mWinAnimators	Ljava/util/ArrayList;
        //     81: iload 7
        //     83: invokevirtual 118	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     86: checkcast 192	com/android/server/wm/WindowStateAnimator
        //     89: iconst_1
        //     90: invokevirtual 393	com/android/server/wm/WindowStateAnimator:prepareSurfaceLocked	(Z)V
        //     93: iinc 7 1
        //     96: goto -26 -> 70
        //     99: aload_0
        //     100: getfield 82	com/android/server/wm/WindowAnimator:mDimParams	Lcom/android/server/wm/DimAnimator$Parameters;
        //     103: ifnull +25 -> 128
        //     106: aload_0
        //     107: getfield 80	com/android/server/wm/WindowAnimator:mDimAnimator	Lcom/android/server/wm/DimAnimator;
        //     110: aload_0
        //     111: getfield 86	com/android/server/wm/WindowAnimator:mContext	Landroid/content/Context;
        //     114: invokevirtual 399	android/content/Context:getResources	()Landroid/content/res/Resources;
        //     117: aload_0
        //     118: getfield 82	com/android/server/wm/WindowAnimator:mDimParams	Lcom/android/server/wm/DimAnimator$Parameters;
        //     121: aload_0
        //     122: getfield 216	com/android/server/wm/WindowAnimator:mCurrentTime	J
        //     125: invokevirtual 405	com/android/server/wm/DimAnimator:updateParameters	(Landroid/content/res/Resources;Lcom/android/server/wm/DimAnimator$Parameters;J)V
        //     128: aload_0
        //     129: getfield 80	com/android/server/wm/WindowAnimator:mDimAnimator	Lcom/android/server/wm/DimAnimator;
        //     132: ifnull +68 -> 200
        //     135: aload_0
        //     136: getfield 80	com/android/server/wm/WindowAnimator:mDimAnimator	Lcom/android/server/wm/DimAnimator;
        //     139: getfield 408	com/android/server/wm/DimAnimator:mDimShown	Z
        //     142: ifeq +58 -> 200
        //     145: aload_0
        //     146: getfield 155	com/android/server/wm/WindowAnimator:mAnimating	Z
        //     149: istore 8
        //     151: aload_0
        //     152: getfield 80	com/android/server/wm/WindowAnimator:mDimAnimator	Lcom/android/server/wm/DimAnimator;
        //     155: astore 9
        //     157: aload_0
        //     158: invokevirtual 411	com/android/server/wm/WindowAnimator:isDimming	()Z
        //     161: istore 10
        //     163: aload_0
        //     164: getfield 216	com/android/server/wm/WindowAnimator:mCurrentTime	J
        //     167: lstore 11
        //     169: aload_0
        //     170: getfield 84	com/android/server/wm/WindowAnimator:mService	Lcom/android/server/wm/WindowManagerService;
        //     173: invokevirtual 414	com/android/server/wm/WindowManagerService:okToDisplay	()Z
        //     176: ifne +116 -> 292
        //     179: iconst_1
        //     180: istore 13
        //     182: aload_0
        //     183: iload 8
        //     185: aload 9
        //     187: iload 10
        //     189: lload 11
        //     191: iload 13
        //     193: invokevirtual 418	com/android/server/wm/DimAnimator:updateSurface	(ZJZ)Z
        //     196: ior
        //     197: putfield 155	com/android/server/wm/WindowAnimator:mAnimating	Z
        //     200: aload_0
        //     201: getfield 84	com/android/server/wm/WindowAnimator:mService	Lcom/android/server/wm/WindowManagerService;
        //     204: getfield 422	com/android/server/wm/WindowManagerService:mBlackFrame	Lcom/android/server/wm/BlackFrame;
        //     207: ifnull +30 -> 237
        //     210: aload_0
        //     211: getfield 70	com/android/server/wm/WindowAnimator:mScreenRotationAnimation	Lcom/android/server/wm/ScreenRotationAnimation;
        //     214: ifnull +84 -> 298
        //     217: aload_0
        //     218: getfield 84	com/android/server/wm/WindowAnimator:mService	Lcom/android/server/wm/WindowManagerService;
        //     221: getfield 422	com/android/server/wm/WindowManagerService:mBlackFrame	Lcom/android/server/wm/BlackFrame;
        //     224: aload_0
        //     225: getfield 70	com/android/server/wm/WindowAnimator:mScreenRotationAnimation	Lcom/android/server/wm/ScreenRotationAnimation;
        //     228: invokevirtual 426	com/android/server/wm/ScreenRotationAnimation:getEnterTransformation	()Landroid/view/animation/Transformation;
        //     231: invokevirtual 432	android/view/animation/Transformation:getMatrix	()Landroid/graphics/Matrix;
        //     234: invokevirtual 438	com/android/server/wm/BlackFrame:setMatrix	(Landroid/graphics/Matrix;)V
        //     237: aload_0
        //     238: getfield 84	com/android/server/wm/WindowAnimator:mService	Lcom/android/server/wm/WindowManagerService;
        //     241: getfield 442	com/android/server/wm/WindowManagerService:mWatermark	Lcom/android/server/wm/Watermark;
        //     244: ifnull +13 -> 257
        //     247: aload_0
        //     248: getfield 84	com/android/server/wm/WindowAnimator:mService	Lcom/android/server/wm/WindowManagerService;
        //     251: getfield 442	com/android/server/wm/WindowManagerService:mWatermark	Lcom/android/server/wm/Watermark;
        //     254: invokevirtual 447	com/android/server/wm/Watermark:drawIfNeeded	()V
        //     257: invokestatic 450	android/view/Surface:closeTransaction	()V
        //     260: aload_0
        //     261: getfield 84	com/android/server/wm/WindowAnimator:mService	Lcom/android/server/wm/WindowManagerService;
        //     264: aload_0
        //     265: getfield 78	com/android/server/wm/WindowAnimator:mBulkUpdateParams	I
        //     268: aload_0
        //     269: getfield 100	com/android/server/wm/WindowAnimator:mPendingLayoutChanges	I
        //     272: invokevirtual 454	com/android/server/wm/WindowManagerService:bulkSetParameters	(II)V
        //     275: aload_0
        //     276: getfield 155	com/android/server/wm/WindowAnimator:mAnimating	Z
        //     279: ifeq +62 -> 341
        //     282: aload_0
        //     283: getfield 84	com/android/server/wm/WindowAnimator:mService	Lcom/android/server/wm/WindowManagerService;
        //     286: invokevirtual 457	com/android/server/wm/WindowManagerService:scheduleAnimationLocked	()V
        //     289: aload_0
        //     290: monitorexit
        //     291: return
        //     292: iconst_0
        //     293: istore 13
        //     295: goto -113 -> 182
        //     298: aload_0
        //     299: getfield 84	com/android/server/wm/WindowAnimator:mService	Lcom/android/server/wm/WindowManagerService;
        //     302: getfield 422	com/android/server/wm/WindowManagerService:mBlackFrame	Lcom/android/server/wm/BlackFrame;
        //     305: invokevirtual 460	com/android/server/wm/BlackFrame:clearMatrix	()V
        //     308: goto -71 -> 237
        //     311: astore 4
        //     313: ldc 20
        //     315: ldc_w 462
        //     318: aload 4
        //     320: invokestatic 468	android/util/Log:wtf	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     323: pop
        //     324: invokestatic 450	android/view/Surface:closeTransaction	()V
        //     327: goto -67 -> 260
        //     330: astore_1
        //     331: aload_0
        //     332: monitorexit
        //     333: aload_1
        //     334: athrow
        //     335: astore_3
        //     336: invokestatic 450	android/view/Surface:closeTransaction	()V
        //     339: aload_3
        //     340: athrow
        //     341: iload_2
        //     342: ifeq -53 -> 289
        //     345: aload_0
        //     346: getfield 84	com/android/server/wm/WindowAnimator:mService	Lcom/android/server/wm/WindowManagerService;
        //     349: invokevirtual 471	com/android/server/wm/WindowManagerService:requestTraversalLocked	()V
        //     352: goto -63 -> 289
        //
        // Exception table:
        //     from	to	target	type
        //     32	257	311	java/lang/RuntimeException
        //     298	308	311	java/lang/RuntimeException
        //     2	32	330	finally
        //     257	289	330	finally
        //     324	327	330	finally
        //     336	352	330	finally
        //     32	257	335	finally
        //     298	308	335	finally
        //     313	324	335	finally
    }

    /** @deprecated */
    void clearPendingActions()
    {
        try
        {
            this.mPendingActions = 0;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void dump(PrintWriter paramPrintWriter, String paramString, boolean paramBoolean)
    {
        if (paramBoolean)
        {
            if (this.mWindowDetachedWallpaper != null)
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("mWindowDetachedWallpaper=");
                paramPrintWriter.println(this.mWindowDetachedWallpaper);
            }
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("mAnimTransactionSequence=");
            paramPrintWriter.println(this.mAnimTransactionSequence);
            if (this.mWindowAnimationBackgroundSurface != null)
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("mWindowAnimationBackgroundSurface:");
                this.mWindowAnimationBackgroundSurface.printTo(paramString + "    ", paramPrintWriter);
            }
            if (this.mDimAnimator == null)
                break label146;
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("mDimAnimator:");
            this.mDimAnimator.printTo(paramString + "    ", paramPrintWriter);
        }
        while (true)
        {
            return;
            label146: paramPrintWriter.print(paramString);
            paramPrintWriter.print("no DimAnimator ");
        }
    }

    void hideWallpapersLocked(WindowState paramWindowState)
    {
        if (((this.mService.mWallpaperTarget == paramWindowState) && (this.mService.mLowerWallpaperTarget == null)) || (this.mService.mWallpaperTarget == null))
        {
            Iterator localIterator1 = this.mService.mWallpaperTokens.iterator();
            while (localIterator1.hasNext())
            {
                WindowToken localWindowToken = (WindowToken)localIterator1.next();
                Iterator localIterator2 = localWindowToken.windows.iterator();
                while (localIterator2.hasNext())
                {
                    WindowState localWindowState = (WindowState)localIterator2.next();
                    WindowStateAnimator localWindowStateAnimator = localWindowState.mWinAnimator;
                    if (!localWindowStateAnimator.mLastHidden)
                    {
                        localWindowStateAnimator.hide();
                        this.mService.dispatchWallpaperVisibility(localWindowState, false);
                        this.mPendingLayoutChanges = (0x4 | this.mPendingLayoutChanges);
                    }
                }
                localWindowToken.hidden = true;
            }
        }
    }

    boolean isDimming()
    {
        if (this.mDimParams != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean isDimming(WindowStateAnimator paramWindowStateAnimator)
    {
        if ((this.mDimParams != null) && (this.mDimParams.mDimWinAnimator == paramWindowStateAnimator));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    void setCurrentFocus(WindowState paramWindowState)
    {
        this.mCurrentFocus = paramWindowState;
    }

    void setDisplayDimensions(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        this.mDw = paramInt1;
        this.mDh = paramInt2;
        this.mInnerDw = paramInt3;
        this.mInnerDh = paramInt4;
    }

    void startDimming(WindowStateAnimator paramWindowStateAnimator, float paramFloat, int paramInt1, int paramInt2)
    {
        if (this.mDimAnimator == null)
            this.mDimAnimator = new DimAnimator(this.mService.mFxSession);
        if (this.mDimParams == null);
        for (WindowStateAnimator localWindowStateAnimator = null; ; localWindowStateAnimator = this.mDimParams.mDimWinAnimator)
        {
            if ((paramWindowStateAnimator.mSurfaceShown) && ((localWindowStateAnimator == null) || (!localWindowStateAnimator.mSurfaceShown) || (localWindowStateAnimator.mAnimLayer < paramWindowStateAnimator.mAnimLayer)))
                this.mService.mH.sendMessage(this.mService.mH.obtainMessage(100003, new DimAnimator.Parameters(paramWindowStateAnimator, paramInt1, paramInt2, paramFloat)));
            return;
        }
    }

    void stopDimming()
    {
        this.mService.mH.sendMessage(this.mService.mH.obtainMessage(100003, null));
    }

    static class SetAnimationParams
    {
        final int mAnimDh;
        final int mAnimDw;
        final Animation mAnimation;
        final WindowStateAnimator mWinAnimator;

        public SetAnimationParams(WindowStateAnimator paramWindowStateAnimator, Animation paramAnimation, int paramInt1, int paramInt2)
        {
            this.mWinAnimator = paramWindowStateAnimator;
            this.mAnimation = paramAnimation;
            this.mAnimDw = paramInt1;
            this.mAnimDh = paramInt2;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.wm.WindowAnimator
 * JD-Core Version:        0.6.2
 */