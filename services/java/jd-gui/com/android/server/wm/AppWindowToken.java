package com.android.server.wm;

import android.os.Message;
import android.os.RemoteException;
import android.view.IApplicationToken;
import android.view.IWindow;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import com.android.server.input.InputApplicationHandle;
import java.io.PrintWriter;
import java.util.ArrayList;

class AppWindowToken extends WindowToken
{
    final ArrayList<WindowState> allAppWindows = new ArrayList();
    boolean allDrawn;
    boolean appFullscreen;
    final IApplicationToken appToken;
    boolean clientHidden;
    boolean firstWindowDrawn;
    int groupId = -1;
    boolean hiddenRequested;
    boolean inPendingTransaction;
    long inputDispatchingTimeoutNanos;
    long lastTransactionSequence = -9223372036854775808L;
    final WindowAnimator mAnimator;
    final AppWindowAnimator mAppAnimator;
    final InputApplicationHandle mInputApplicationHandle;
    int numDrawnWindows;
    int numInterestingWindows;
    boolean removed;
    boolean reportedDrawn;
    boolean reportedVisible;
    int requestedOrientation = -1;
    StartingData startingData;
    boolean startingDisplayed;
    boolean startingMoved;
    View startingView;
    WindowState startingWindow;
    boolean willBeHidden;

    AppWindowToken(WindowManagerService paramWindowManagerService, IApplicationToken paramIApplicationToken)
    {
        super(paramWindowManagerService, paramIApplicationToken.asBinder(), 2, true);
        this.appWindowToken = this;
        this.appToken = paramIApplicationToken;
        this.mInputApplicationHandle = new InputApplicationHandle(this);
        this.mAnimator = this.service.mAnimator;
        this.mAppAnimator = new AppWindowAnimator(paramWindowManagerService, this);
    }

    void dump(PrintWriter paramPrintWriter, String paramString)
    {
        super.dump(paramPrintWriter, paramString);
        if (this.appToken != null)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.println("app=true");
        }
        if (this.allAppWindows.size() > 0)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("allAppWindows=");
            paramPrintWriter.println(this.allAppWindows);
        }
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("groupId=");
        paramPrintWriter.print(this.groupId);
        paramPrintWriter.print(" appFullscreen=");
        paramPrintWriter.print(this.appFullscreen);
        paramPrintWriter.print(" requestedOrientation=");
        paramPrintWriter.println(this.requestedOrientation);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("hiddenRequested=");
        paramPrintWriter.print(this.hiddenRequested);
        paramPrintWriter.print(" clientHidden=");
        paramPrintWriter.print(this.clientHidden);
        paramPrintWriter.print(" willBeHidden=");
        paramPrintWriter.print(this.willBeHidden);
        paramPrintWriter.print(" reportedDrawn=");
        paramPrintWriter.print(this.reportedDrawn);
        paramPrintWriter.print(" reportedVisible=");
        paramPrintWriter.println(this.reportedVisible);
        if (this.paused)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("paused=");
            paramPrintWriter.println(this.paused);
        }
        if ((this.numInterestingWindows != 0) || (this.numDrawnWindows != 0) || (this.allDrawn) || (this.mAppAnimator.allDrawn))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("numInterestingWindows=");
            paramPrintWriter.print(this.numInterestingWindows);
            paramPrintWriter.print(" numDrawnWindows=");
            paramPrintWriter.print(this.numDrawnWindows);
            paramPrintWriter.print(" inPendingTransaction=");
            paramPrintWriter.print(this.inPendingTransaction);
            paramPrintWriter.print(" allDrawn=");
            paramPrintWriter.print(this.allDrawn);
            paramPrintWriter.print(" (animator=");
            paramPrintWriter.print(this.mAppAnimator.allDrawn);
            paramPrintWriter.println(")");
        }
        if (this.inPendingTransaction)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("inPendingTransaction=");
            paramPrintWriter.println(this.inPendingTransaction);
        }
        if ((this.startingData != null) || (this.removed) || (this.firstWindowDrawn))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("startingData=");
            paramPrintWriter.print(this.startingData);
            paramPrintWriter.print(" removed=");
            paramPrintWriter.print(this.removed);
            paramPrintWriter.print(" firstWindowDrawn=");
            paramPrintWriter.println(this.firstWindowDrawn);
        }
        if ((this.startingWindow != null) || (this.startingView != null) || (this.startingDisplayed) || (this.startingMoved))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("startingWindow=");
            paramPrintWriter.print(this.startingWindow);
            paramPrintWriter.print(" startingView=");
            paramPrintWriter.print(this.startingView);
            paramPrintWriter.print(" startingDisplayed=");
            paramPrintWriter.print(this.startingDisplayed);
            paramPrintWriter.print(" startingMoved");
            paramPrintWriter.println(this.startingMoved);
        }
    }

    WindowState findMainWindow()
    {
        int i = this.windows.size();
        WindowState localWindowState;
        do
        {
            if (i <= 0)
                break;
            i--;
            localWindowState = (WindowState)this.windows.get(i);
        }
        while ((localWindowState.mAttrs.type != 1) && (localWindowState.mAttrs.type != 3));
        while (true)
        {
            return localWindowState;
            localWindowState = null;
        }
    }

    void sendAppVisibilityToClients()
    {
        int i = this.allAppWindows.size();
        int j = 0;
        while (j < i)
        {
            WindowState localWindowState = (WindowState)this.allAppWindows.get(j);
            if ((localWindowState == this.startingWindow) && (this.clientHidden))
                j++;
            else
                while (true)
                {
                    try
                    {
                        IWindow localIWindow = localWindowState.mClient;
                        if (this.clientHidden)
                            break label81;
                        bool = true;
                        localIWindow.dispatchAppVisibility(bool);
                    }
                    catch (RemoteException localRemoteException)
                    {
                    }
                    break;
                    label81: boolean bool = false;
                }
        }
    }

    public String toString()
    {
        if (this.stringName == null)
        {
            StringBuilder localStringBuilder = new StringBuilder();
            localStringBuilder.append("AppWindowToken{");
            localStringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
            localStringBuilder.append(" token=");
            localStringBuilder.append(this.token);
            localStringBuilder.append('}');
            this.stringName = localStringBuilder.toString();
        }
        return this.stringName;
    }

    void updateReportedVisibilityLocked()
    {
        int i = 1;
        if (this.appToken == null);
        int n;
        label168: label180: boolean bool1;
        boolean bool2;
        do
        {
            return;
            int j = 0;
            int k = 0;
            int m = 0;
            n = 1;
            int i1 = this.allAppWindows.size();
            int i2 = 0;
            if (i2 < i1)
            {
                WindowState localWindowState = (WindowState)this.allAppWindows.get(i2);
                if ((localWindowState == this.startingWindow) || (localWindowState.mAppFreezing) || (localWindowState.mViewVisibility != 0) || (localWindowState.mAttrs.type == 3) || (localWindowState.mDestroying));
                while (true)
                {
                    i2++;
                    break;
                    j++;
                    if (localWindowState.isDrawnLw())
                    {
                        m++;
                        if (!localWindowState.mWinAnimator.isAnimating())
                            k++;
                        n = 0;
                    }
                    else if (localWindowState.mWinAnimator.isAnimating())
                    {
                        n = 0;
                    }
                }
            }
            if ((j <= 0) || (m < j))
                break;
            int i3 = i;
            if ((j <= 0) || (k < j))
                break label327;
            int i4 = i;
            if (n == 0)
            {
                if (i3 == 0)
                    bool1 = this.reportedDrawn;
                if (i4 == 0)
                    bool2 = this.reportedVisible;
            }
            if (bool1 != this.reportedDrawn)
            {
                if (bool1)
                {
                    Message localMessage2 = this.service.mH.obtainMessage(9, this);
                    this.service.mH.sendMessage(localMessage2);
                }
                this.reportedDrawn = bool1;
            }
        }
        while (bool2 == this.reportedVisible);
        this.reportedVisible = bool2;
        WindowManagerService.H localH = this.service.mH;
        int i5;
        if (bool2)
        {
            i5 = i;
            label287: if (n == 0)
                break label339;
        }
        while (true)
        {
            Message localMessage1 = localH.obtainMessage(8, i5, i, this);
            this.service.mH.sendMessage(localMessage1);
            break;
            bool1 = false;
            break label168;
            label327: bool2 = false;
            break label180;
            i5 = 0;
            break label287;
            label339: i = 0;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.wm.AppWindowToken
 * JD-Core Version:        0.6.2
 */