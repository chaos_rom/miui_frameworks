package com.android.server.wm;

import android.content.res.Resources;
import android.util.Slog;
import android.util.TypedValue;
import android.view.Surface;
import android.view.SurfaceSession;
import android.view.animation.Animation;
import java.io.PrintWriter;

class DimAnimator
{
    float mDimCurrentAlpha;
    float mDimDeltaPerMs;
    boolean mDimShown = false;
    Surface mDimSurface;
    float mDimTargetAlpha;
    long mLastDimAnimTime;
    int mLastDimHeight;
    int mLastDimWidth;

    DimAnimator(SurfaceSession paramSurfaceSession)
    {
        if (this.mDimSurface == null);
        try
        {
            this.mDimSurface = new Surface(paramSurfaceSession, 0, "DimAnimator", -1, 16, 16, -1, 131072);
            this.mDimSurface.setAlpha(0.0F);
            return;
        }
        catch (Exception localException)
        {
            while (true)
                Slog.e("WindowManager", "Exception creating Dim surface", localException);
        }
    }

    public void printTo(String paramString, PrintWriter paramPrintWriter)
    {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mDimSurface=");
        paramPrintWriter.print(this.mDimSurface);
        paramPrintWriter.print(" ");
        paramPrintWriter.print(this.mLastDimWidth);
        paramPrintWriter.print(" x ");
        paramPrintWriter.println(this.mLastDimHeight);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mDimShown=");
        paramPrintWriter.print(this.mDimShown);
        paramPrintWriter.print(" current=");
        paramPrintWriter.print(this.mDimCurrentAlpha);
        paramPrintWriter.print(" target=");
        paramPrintWriter.print(this.mDimTargetAlpha);
        paramPrintWriter.print(" delta=");
        paramPrintWriter.print(this.mDimDeltaPerMs);
        paramPrintWriter.print(" lastAnimTime=");
        paramPrintWriter.println(this.mLastDimAnimTime);
    }

    void updateParameters(Resources paramResources, Parameters paramParameters, long paramLong)
    {
        int i = (int)(1.5D * paramParameters.mDimWidth);
        int j = (int)(1.5D * paramParameters.mDimHeight);
        WindowStateAnimator localWindowStateAnimator = paramParameters.mDimWinAnimator;
        float f = paramParameters.mDimTarget;
        if (!this.mDimShown)
            this.mDimShown = true;
        while (true)
        {
            long l;
            TypedValue localTypedValue;
            try
            {
                this.mLastDimWidth = i;
                this.mLastDimHeight = j;
                this.mDimSurface.setPosition(i * -1 / 6, j * -1 / 6);
                this.mDimSurface.setSize(i, j);
                this.mDimSurface.show();
                this.mDimSurface.setLayer(-1 + localWindowStateAnimator.mAnimLayer);
                if (this.mDimTargetAlpha != f)
                {
                    this.mLastDimAnimTime = paramLong;
                    if ((!localWindowStateAnimator.mAnimating) || (localWindowStateAnimator.mAnimation == null))
                        break label325;
                    l = localWindowStateAnimator.mAnimation.computeDurationHint();
                    if (f > this.mDimTargetAlpha)
                    {
                        localTypedValue = new TypedValue();
                        paramResources.getValue(17956864, localTypedValue, true);
                        if (localTypedValue.type != 6)
                            break label333;
                        l = ()localTypedValue.getFraction((float)l, (float)l);
                    }
                    if (l < 1L)
                        l = 1L;
                    this.mDimTargetAlpha = f;
                    this.mDimDeltaPerMs = ((this.mDimTargetAlpha - this.mDimCurrentAlpha) / (float)l);
                }
                return;
            }
            catch (RuntimeException localRuntimeException)
            {
                Slog.w("WindowManager", "Failure showing dim surface", localRuntimeException);
                continue;
            }
            if ((this.mLastDimWidth != i) || (this.mLastDimHeight != j))
            {
                this.mLastDimWidth = i;
                this.mLastDimHeight = j;
                this.mDimSurface.setSize(i, j);
                this.mDimSurface.setPosition(i * -1 / 6, j * -1 / 6);
                continue;
                label325: l = 200L;
                continue;
                label333: if ((localTypedValue.type >= 16) && (localTypedValue.type <= 31))
                    l = localTypedValue.data;
            }
        }
    }

    boolean updateSurface(boolean paramBoolean1, long paramLong, boolean paramBoolean2)
    {
        if ((!paramBoolean1) && (this.mDimTargetAlpha != 0.0F))
        {
            this.mLastDimAnimTime = paramLong;
            this.mDimTargetAlpha = 0.0F;
            this.mDimDeltaPerMs = (-this.mDimCurrentAlpha / 200.0F);
        }
        boolean bool;
        if (this.mLastDimAnimTime != 0L)
        {
            bool = true;
            if (bool)
            {
                this.mDimCurrentAlpha += this.mDimDeltaPerMs * (float)(paramLong - this.mLastDimAnimTime);
                if (!paramBoolean2)
                    break label111;
                bool = false;
                label81: if (!bool)
                    break label171;
                this.mLastDimAnimTime = paramLong;
                this.mDimSurface.setAlpha(this.mDimCurrentAlpha);
            }
        }
        while (true)
        {
            return bool;
            bool = false;
            break;
            label111: if (this.mDimDeltaPerMs > 0.0F)
            {
                if (this.mDimCurrentAlpha <= this.mDimTargetAlpha)
                    break label81;
                bool = false;
                break label81;
            }
            if (this.mDimDeltaPerMs < 0.0F)
            {
                if (this.mDimCurrentAlpha >= this.mDimTargetAlpha)
                    break label81;
                bool = false;
                break label81;
            }
            bool = false;
            break label81;
            label171: this.mDimCurrentAlpha = this.mDimTargetAlpha;
            this.mLastDimAnimTime = 0L;
            this.mDimSurface.setAlpha(this.mDimCurrentAlpha);
            if (paramBoolean1)
                continue;
            try
            {
                this.mDimSurface.hide();
                this.mDimShown = false;
            }
            catch (RuntimeException localRuntimeException)
            {
                while (true)
                    Slog.w("WindowManager", "Illegal argument exception hiding dim surface");
            }
        }
    }

    static class Parameters
    {
        final int mDimHeight;
        final float mDimTarget;
        final int mDimWidth;
        final WindowStateAnimator mDimWinAnimator;

        Parameters(WindowStateAnimator paramWindowStateAnimator, int paramInt1, int paramInt2, float paramFloat)
        {
            this.mDimWinAnimator = paramWindowStateAnimator;
            this.mDimWidth = paramInt1;
            this.mDimHeight = paramInt2;
            this.mDimTarget = paramFloat;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.wm.DimAnimator
 * JD-Core Version:        0.6.2
 */