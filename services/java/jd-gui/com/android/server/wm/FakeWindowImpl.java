package com.android.server.wm;

import android.graphics.Region;
import android.os.Looper;
import android.os.Process;
import android.view.InputChannel;
import android.view.InputEventReceiver;
import android.view.InputEventReceiver.Factory;
import android.view.WindowManagerPolicy;
import android.view.WindowManagerPolicy.FakeWindow;
import com.android.server.input.InputApplicationHandle;
import com.android.server.input.InputManagerService;
import com.android.server.input.InputWindowHandle;

public final class FakeWindowImpl
    implements WindowManagerPolicy.FakeWindow
{
    final InputApplicationHandle mApplicationHandle;
    final InputChannel mClientChannel;
    final InputEventReceiver mInputEventReceiver;
    final InputChannel mServerChannel;
    final WindowManagerService mService;
    boolean mTouchFullscreen;
    final InputWindowHandle mWindowHandle;
    final int mWindowLayer;

    public FakeWindowImpl(WindowManagerService paramWindowManagerService, Looper paramLooper, InputEventReceiver.Factory paramFactory, String paramString, int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
    {
        this.mService = paramWindowManagerService;
        InputChannel[] arrayOfInputChannel = InputChannel.openInputChannelPair(paramString);
        this.mServerChannel = arrayOfInputChannel[0];
        this.mClientChannel = arrayOfInputChannel[1];
        this.mService.mInputManager.registerInputChannel(this.mServerChannel, null);
        this.mInputEventReceiver = paramFactory.createInputEventReceiver(this.mClientChannel, paramLooper);
        this.mApplicationHandle = new InputApplicationHandle(null);
        this.mApplicationHandle.name = paramString;
        this.mApplicationHandle.dispatchingTimeoutNanos = 5000000000L;
        this.mWindowHandle = new InputWindowHandle(this.mApplicationHandle, null);
        this.mWindowHandle.name = paramString;
        this.mWindowHandle.inputChannel = this.mServerChannel;
        this.mWindowLayer = getLayerLw(paramInt1);
        this.mWindowHandle.layer = this.mWindowLayer;
        this.mWindowHandle.layoutParamsFlags = paramInt2;
        this.mWindowHandle.layoutParamsType = paramInt1;
        this.mWindowHandle.dispatchingTimeoutNanos = 5000000000L;
        this.mWindowHandle.visible = true;
        this.mWindowHandle.canReceiveKeys = paramBoolean1;
        this.mWindowHandle.hasFocus = paramBoolean2;
        this.mWindowHandle.hasWallpaper = false;
        this.mWindowHandle.paused = false;
        this.mWindowHandle.ownerPid = Process.myPid();
        this.mWindowHandle.ownerUid = Process.myUid();
        this.mWindowHandle.inputFeatures = 0;
        this.mWindowHandle.scaleFactor = 1.0F;
        this.mTouchFullscreen = paramBoolean3;
    }

    private int getLayerLw(int paramInt)
    {
        return 1000 + 10000 * this.mService.mPolicy.windowTypeToLayerLw(paramInt);
    }

    public void dismiss()
    {
        synchronized (this.mService.mWindowMap)
        {
            if (this.mService.removeFakeWindowLocked(this))
            {
                this.mInputEventReceiver.dispose();
                this.mService.mInputManager.unregisterInputChannel(this.mServerChannel);
                this.mClientChannel.dispose();
                this.mServerChannel.dispose();
            }
            return;
        }
    }

    void layout(int paramInt1, int paramInt2)
    {
        if (this.mTouchFullscreen)
            this.mWindowHandle.touchableRegion.set(0, 0, paramInt1, paramInt2);
        while (true)
        {
            this.mWindowHandle.frameLeft = 0;
            this.mWindowHandle.frameTop = 0;
            this.mWindowHandle.frameRight = paramInt1;
            this.mWindowHandle.frameBottom = paramInt2;
            return;
            this.mWindowHandle.touchableRegion.setEmpty();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.wm.FakeWindowImpl
 * JD-Core Version:        0.6.2
 */