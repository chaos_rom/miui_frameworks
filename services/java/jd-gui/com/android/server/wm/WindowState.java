package com.android.server.wm;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.RemoteException;
import android.util.Slog;
import android.view.Gravity;
import android.view.IApplicationToken;
import android.view.IWindow;
import android.view.InputChannel;
import android.view.WindowManager.LayoutParams;
import android.view.WindowManagerPolicy;
import android.view.WindowManagerPolicy.WindowState;
import com.android.server.input.InputApplicationHandle;
import com.android.server.input.InputManagerService;
import com.android.server.input.InputWindowHandle;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

final class WindowState
    implements WindowManagerPolicy.WindowState
{
    static final boolean DEBUG_VISIBILITY = false;
    static final boolean SHOW_LIGHT_TRANSACTIONS = false;
    static final boolean SHOW_SURFACE_ALLOC = false;
    static final boolean SHOW_TRANSACTIONS = false;
    static final String TAG = "WindowState";
    boolean mAppFreezing;
    AppWindowToken mAppToken;
    boolean mAttachedHidden;
    final WindowState mAttachedWindow;
    final WindowManager.LayoutParams mAttrs = new WindowManager.LayoutParams();
    final int mBaseLayer;
    final ArrayList<WindowState> mChildWindows = new ArrayList();
    final IWindow mClient;
    final Rect mCompatFrame = new Rect();
    Configuration mConfiguration = null;
    final Rect mContainingFrame = new Rect();
    boolean mContentChanged;
    final Rect mContentFrame = new Rect();
    final Rect mContentInsets = new Rect();
    boolean mContentInsetsChanged;
    final Context mContext;
    final DeathRecipient mDeathRecipient;
    boolean mDestroying;
    final Rect mDisplayFrame = new Rect();
    boolean mEnforceSizeCompat;
    boolean mExiting;
    final Rect mFrame = new Rect();
    final Rect mGivenContentInsets = new Rect();
    boolean mGivenInsetsPending;
    final Region mGivenTouchableRegion = new Region();
    final Rect mGivenVisibleInsets = new Rect();
    float mGlobalScale = 1.0F;
    float mHScale = 1.0F;
    boolean mHasSurface = false;
    boolean mHaveFrame;
    InputChannel mInputChannel;
    final InputWindowHandle mInputWindowHandle;
    float mInvGlobalScale = 1.0F;
    final boolean mIsFloatingLayer;
    final boolean mIsImWindow;
    final boolean mIsWallpaper;
    final Rect mLastContentInsets = new Rect();
    final Rect mLastFrame = new Rect();
    float mLastHScale = 1.0F;
    int mLastRequestedHeight;
    int mLastRequestedWidth;
    final Rect mLastSystemDecorRect = new Rect();
    CharSequence mLastTitle;
    float mLastVScale = 1.0F;
    final Rect mLastVisibleInsets = new Rect();
    int mLayer;
    final boolean mLayoutAttached;
    boolean mLayoutNeeded;
    int mLayoutSeq = -1;
    boolean mObscured;
    boolean mOrientationChanging;
    final Rect mParentFrame = new Rect();
    final WindowManagerPolicy mPolicy;
    boolean mPolicyVisibility = true;
    boolean mPolicyVisibilityAfterAnim = true;
    boolean mRebuilding;
    boolean mRelayoutCalled;
    boolean mRemoveOnExit;
    boolean mRemoved;
    int mRequestedHeight;
    int mRequestedWidth;
    WindowToken mRootToken;
    int mSeq;
    final WindowManagerService mService;
    final Session mSession;
    final RectF mShownFrame = new RectF();
    String mStringNameCache;
    final int mSubLayer;
    final Rect mSystemDecorRect = new Rect();
    int mSystemUiVisibility;
    AppWindowToken mTargetAppToken;
    final Matrix mTmpMatrix = new Matrix();
    WindowToken mToken;
    int mTouchableInsets = 0;
    boolean mTurnOnScreen;
    float mVScale = 1.0F;
    int mViewVisibility;
    final Rect mVisibleFrame = new Rect();
    final Rect mVisibleInsets = new Rect();
    boolean mVisibleInsetsChanged;
    boolean mWallpaperVisible;
    float mWallpaperX = -1.0F;
    float mWallpaperXStep = -1.0F;
    float mWallpaperY = -1.0F;
    float mWallpaperYStep = -1.0F;
    boolean mWasPaused;
    final WindowStateAnimator mWinAnimator;
    int mXOffset;
    int mYOffset;

    WindowState(WindowManagerService paramWindowManagerService, Session paramSession, IWindow paramIWindow, WindowToken paramWindowToken, WindowState paramWindowState, int paramInt1, WindowManager.LayoutParams paramLayoutParams, int paramInt2)
    {
        this.mService = paramWindowManagerService;
        this.mSession = paramSession;
        this.mClient = paramIWindow;
        this.mToken = paramWindowToken;
        this.mAttrs.copyFrom(paramLayoutParams);
        this.mViewVisibility = paramInt2;
        this.mPolicy = this.mService.mPolicy;
        this.mContext = this.mService.mContext;
        DeathRecipient localDeathRecipient = new DeathRecipient(null);
        this.mSeq = paramInt1;
        boolean bool1;
        if ((0x20000000 & this.mAttrs.flags) != 0)
        {
            bool1 = true;
            this.mEnforceSizeCompat = bool1;
        }
        WindowState localWindowState;
        while (true)
        {
            try
            {
                paramIWindow.asBinder().linkToDeath(localDeathRecipient, 0);
                this.mDeathRecipient = localDeathRecipient;
                if ((this.mAttrs.type < 1000) || (this.mAttrs.type > 1999))
                    break label768;
                this.mBaseLayer = (1000 + 10000 * this.mPolicy.windowTypeToLayerLw(paramWindowState.mAttrs.type));
                this.mSubLayer = this.mPolicy.subWindowTypeToLayerLw(paramLayoutParams.type);
                this.mAttachedWindow = paramWindowState;
                this.mAttachedWindow.mChildWindows.add(this);
                if (this.mAttrs.type == 1003)
                    break label744;
                bool5 = true;
                this.mLayoutAttached = bool5;
                if ((paramWindowState.mAttrs.type != 2011) && (paramWindowState.mAttrs.type != 2012))
                    break label750;
                bool6 = true;
                this.mIsImWindow = bool6;
                if (paramWindowState.mAttrs.type != 2013)
                    break label756;
                bool7 = true;
                this.mIsWallpaper = bool7;
                if ((!this.mIsImWindow) && (!this.mIsWallpaper))
                    break label762;
                bool8 = true;
                this.mIsFloatingLayer = bool8;
                this.mWinAnimator = new WindowStateAnimator(paramWindowManagerService, this, this.mAttachedWindow);
                this.mWinAnimator.mAlpha = paramLayoutParams.alpha;
                localWindowState = this;
                if (localWindowState.mAttachedWindow == null)
                    break label910;
                localWindowState = localWindowState.mAttachedWindow;
                continue;
                bool1 = false;
            }
            catch (RemoteException localRemoteException)
            {
                this.mDeathRecipient = null;
                this.mAttachedWindow = null;
                this.mLayoutAttached = false;
                this.mIsImWindow = false;
                this.mIsWallpaper = false;
                this.mIsFloatingLayer = false;
                this.mBaseLayer = 0;
                this.mSubLayer = 0;
                this.mInputWindowHandle = null;
                this.mWinAnimator = null;
            }
            return;
            label744: boolean bool5 = false;
            continue;
            label750: boolean bool6 = false;
            continue;
            label756: boolean bool7 = false;
            continue;
            label762: boolean bool8 = false;
        }
        label768: this.mBaseLayer = (1000 + 10000 * this.mPolicy.windowTypeToLayerLw(paramLayoutParams.type));
        this.mSubLayer = 0;
        this.mAttachedWindow = null;
        this.mLayoutAttached = false;
        boolean bool2;
        label838: boolean bool3;
        if ((this.mAttrs.type == 2011) || (this.mAttrs.type == 2012))
        {
            bool2 = true;
            this.mIsImWindow = bool2;
            if (this.mAttrs.type != 2013)
                break label898;
            bool3 = true;
            label860: this.mIsWallpaper = bool3;
            if ((!this.mIsImWindow) && (!this.mIsWallpaper))
                break label904;
        }
        label898: label904: for (boolean bool4 = true; ; bool4 = false)
        {
            this.mIsFloatingLayer = bool4;
            break;
            bool2 = false;
            break label838;
            bool3 = false;
            break label860;
        }
        label910: Object localObject = localWindowState.mToken;
        label917: WindowToken localWindowToken;
        if (((WindowToken)localObject).appWindowToken == null)
        {
            localWindowToken = (WindowToken)this.mService.mTokenMap.get(((WindowToken)localObject).token);
            if ((localWindowToken != null) && (localObject != localWindowToken));
        }
        else
        {
            this.mRootToken = ((WindowToken)localObject);
            this.mAppToken = ((WindowToken)localObject).appWindowToken;
            this.mRequestedWidth = 0;
            this.mRequestedHeight = 0;
            this.mLastRequestedWidth = 0;
            this.mLastRequestedHeight = 0;
            this.mXOffset = 0;
            this.mYOffset = 0;
            this.mLayer = 0;
            if (this.mAppToken == null)
                break label1047;
        }
        label1047: for (InputApplicationHandle localInputApplicationHandle = this.mAppToken.mInputApplicationHandle; ; localInputApplicationHandle = null)
        {
            this.mInputWindowHandle = new InputWindowHandle(localInputApplicationHandle, this);
            break;
            localObject = localWindowToken;
            break label917;
        }
    }

    private static void applyInsets(Region paramRegion, Rect paramRect1, Rect paramRect2)
    {
        paramRegion.set(paramRect1.left + paramRect2.left, paramRect1.top + paramRect2.top, paramRect1.right - paramRect2.right, paramRect1.bottom - paramRect2.bottom);
    }

    void attach()
    {
        this.mSession.windowAddedLocked();
    }

    public final boolean canReceiveKeys()
    {
        if ((isVisibleOrAdding()) && (this.mViewVisibility == 0) && ((0x8 & this.mAttrs.flags) == 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void computeFrameLw(Rect paramRect1, Rect paramRect2, Rect paramRect3, Rect paramRect4)
    {
        this.mHaveFrame = true;
        Rect localRect1 = this.mContainingFrame;
        localRect1.set(paramRect1);
        this.mDisplayFrame.set(paramRect2);
        int i = localRect1.right - localRect1.left;
        int j = localRect1.bottom - localRect1.top;
        int k;
        int m;
        label93: Rect localRect2;
        Rect localRect3;
        Rect localRect4;
        int n;
        int i1;
        float f1;
        if ((0x4000 & this.mAttrs.flags) != 0)
            if (this.mAttrs.width < 0)
            {
                k = i;
                if (this.mAttrs.height >= 0)
                    break label766;
                m = j;
                if (!this.mParentFrame.equals(paramRect1))
                {
                    this.mParentFrame.set(paramRect1);
                    this.mContentChanged = true;
                }
                if ((this.mRequestedWidth != this.mLastRequestedWidth) || (this.mRequestedHeight != this.mLastRequestedHeight))
                {
                    this.mLastRequestedWidth = this.mRequestedWidth;
                    this.mLastRequestedHeight = this.mRequestedHeight;
                    this.mContentChanged = true;
                }
                localRect2 = this.mContentFrame;
                localRect2.set(paramRect3);
                localRect3 = this.mVisibleFrame;
                localRect3.set(paramRect4);
                localRect4 = this.mFrame;
                n = localRect4.width();
                i1 = localRect4.height();
                if (!this.mEnforceSizeCompat)
                    break label915;
                f1 = this.mAttrs.x * this.mGlobalScale;
            }
        for (float f2 = this.mAttrs.y * this.mGlobalScale; ; f2 = this.mAttrs.y)
        {
            Gravity.apply(this.mAttrs.gravity, k, m, localRect1, (int)(f1 + this.mAttrs.horizontalMargin * i), (int)(f2 + this.mAttrs.verticalMargin * j), localRect4);
            Gravity.applyDisplay(this.mAttrs.gravity, paramRect2, localRect4);
            if (localRect2.left < localRect4.left)
                localRect2.left = localRect4.left;
            if (localRect2.top < localRect4.top)
                localRect2.top = localRect4.top;
            if (localRect2.right > localRect4.right)
                localRect2.right = localRect4.right;
            if (localRect2.bottom > localRect4.bottom)
                localRect2.bottom = localRect4.bottom;
            if (localRect3.left < localRect4.left)
                localRect3.left = localRect4.left;
            if (localRect3.top < localRect4.top)
                localRect3.top = localRect4.top;
            if (localRect3.right > localRect4.right)
                localRect3.right = localRect4.right;
            if (localRect3.bottom > localRect4.bottom)
                localRect3.bottom = localRect4.bottom;
            Rect localRect5 = this.mContentInsets;
            localRect2.left -= localRect4.left;
            localRect2.top -= localRect4.top;
            localRect4.right -= localRect2.right;
            localRect4.bottom -= localRect2.bottom;
            Rect localRect6 = this.mVisibleInsets;
            localRect3.left -= localRect4.left;
            localRect3.top -= localRect4.top;
            localRect4.right -= localRect3.right;
            localRect4.bottom -= localRect3.bottom;
            this.mCompatFrame.set(localRect4);
            if (this.mEnforceSizeCompat)
            {
                localRect5.scale(this.mInvGlobalScale);
                localRect6.scale(this.mInvGlobalScale);
                this.mCompatFrame.scale(this.mInvGlobalScale);
            }
            if ((this.mIsWallpaper) && ((n != localRect4.width()) || (i1 != localRect4.height())))
                this.mService.updateWallpaperOffsetLocked(this, this.mService.mAppDisplayWidth, this.mService.mAppDisplayHeight, false);
            return;
            if (this.mEnforceSizeCompat)
            {
                k = (int)(0.5F + this.mAttrs.width * this.mGlobalScale);
                break;
            }
            k = this.mAttrs.width;
            break;
            label766: if (this.mEnforceSizeCompat)
            {
                m = (int)(0.5F + this.mAttrs.height * this.mGlobalScale);
                break label93;
            }
            m = this.mAttrs.height;
            break label93;
            if (this.mAttrs.width == -1)
                k = i;
            while (true)
            {
                if (this.mAttrs.height != -1)
                    break label879;
                m = j;
                break;
                if (this.mEnforceSizeCompat)
                    k = (int)(0.5F + this.mRequestedWidth * this.mGlobalScale);
                else
                    k = this.mRequestedWidth;
            }
            label879: if (this.mEnforceSizeCompat)
            {
                m = (int)(0.5F + this.mRequestedHeight * this.mGlobalScale);
                break label93;
            }
            m = this.mRequestedHeight;
            break label93;
            label915: f1 = this.mAttrs.x;
        }
    }

    void disposeInputChannel()
    {
        if (this.mInputChannel != null)
        {
            this.mService.mInputManager.unregisterInputChannel(this.mInputChannel);
            this.mInputChannel.dispose();
            this.mInputChannel = null;
        }
        this.mInputWindowHandle.inputChannel = null;
    }

    void dump(PrintWriter paramPrintWriter, String paramString, boolean paramBoolean)
    {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mSession=");
        paramPrintWriter.print(this.mSession);
        paramPrintWriter.print(" mClient=");
        paramPrintWriter.println(this.mClient.asBinder());
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mAttrs=");
        paramPrintWriter.println(this.mAttrs);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("Requested w=");
        paramPrintWriter.print(this.mRequestedWidth);
        paramPrintWriter.print(" h=");
        paramPrintWriter.print(this.mRequestedHeight);
        paramPrintWriter.print(" mLayoutSeq=");
        paramPrintWriter.println(this.mLayoutSeq);
        if ((this.mRequestedWidth != this.mLastRequestedWidth) || (this.mRequestedHeight != this.mLastRequestedHeight))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("LastRequested w=");
            paramPrintWriter.print(this.mLastRequestedWidth);
            paramPrintWriter.print(" h=");
            paramPrintWriter.println(this.mLastRequestedHeight);
        }
        if ((this.mAttachedWindow != null) || (this.mLayoutAttached))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("mAttachedWindow=");
            paramPrintWriter.print(this.mAttachedWindow);
            paramPrintWriter.print(" mLayoutAttached=");
            paramPrintWriter.println(this.mLayoutAttached);
        }
        if ((this.mIsImWindow) || (this.mIsWallpaper) || (this.mIsFloatingLayer))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("mIsImWindow=");
            paramPrintWriter.print(this.mIsImWindow);
            paramPrintWriter.print(" mIsWallpaper=");
            paramPrintWriter.print(this.mIsWallpaper);
            paramPrintWriter.print(" mIsFloatingLayer=");
            paramPrintWriter.print(this.mIsFloatingLayer);
            paramPrintWriter.print(" mWallpaperVisible=");
            paramPrintWriter.println(this.mWallpaperVisible);
        }
        int i;
        if (paramBoolean)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("mBaseLayer=");
            paramPrintWriter.print(this.mBaseLayer);
            paramPrintWriter.print(" mSubLayer=");
            paramPrintWriter.print(this.mSubLayer);
            paramPrintWriter.print(" mAnimLayer=");
            paramPrintWriter.print(this.mLayer);
            paramPrintWriter.print("+");
            if (this.mTargetAppToken == null)
                break label1559;
            i = this.mTargetAppToken.mAppAnimator.animLayerAdjustment;
        }
        while (true)
        {
            paramPrintWriter.print(i);
            paramPrintWriter.print("=");
            paramPrintWriter.print(this.mWinAnimator.mAnimLayer);
            paramPrintWriter.print(" mLastLayer=");
            paramPrintWriter.println(this.mWinAnimator.mLastLayer);
            if (paramBoolean)
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("mToken=");
                paramPrintWriter.println(this.mToken);
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("mRootToken=");
                paramPrintWriter.println(this.mRootToken);
                if (this.mAppToken != null)
                {
                    paramPrintWriter.print(paramString);
                    paramPrintWriter.print("mAppToken=");
                    paramPrintWriter.println(this.mAppToken);
                }
                if (this.mTargetAppToken != null)
                {
                    paramPrintWriter.print(paramString);
                    paramPrintWriter.print("mTargetAppToken=");
                    paramPrintWriter.println(this.mTargetAppToken);
                }
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("mViewVisibility=0x");
                paramPrintWriter.print(Integer.toHexString(this.mViewVisibility));
                paramPrintWriter.print(" mHaveFrame=");
                paramPrintWriter.print(this.mHaveFrame);
                paramPrintWriter.print(" mObscured=");
                paramPrintWriter.println(this.mObscured);
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("mSeq=");
                paramPrintWriter.print(this.mSeq);
                paramPrintWriter.print(" mSystemUiVisibility=0x");
                paramPrintWriter.println(Integer.toHexString(this.mSystemUiVisibility));
            }
            if ((!this.mPolicyVisibility) || (!this.mPolicyVisibilityAfterAnim) || (this.mAttachedHidden))
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("mPolicyVisibility=");
                paramPrintWriter.print(this.mPolicyVisibility);
                paramPrintWriter.print(" mPolicyVisibilityAfterAnim=");
                paramPrintWriter.print(this.mPolicyVisibilityAfterAnim);
                paramPrintWriter.print(" mAttachedHidden=");
                paramPrintWriter.println(this.mAttachedHidden);
            }
            if ((!this.mRelayoutCalled) || (this.mLayoutNeeded))
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("mRelayoutCalled=");
                paramPrintWriter.print(this.mRelayoutCalled);
                paramPrintWriter.print(" mLayoutNeeded=");
                paramPrintWriter.println(this.mLayoutNeeded);
            }
            if ((this.mXOffset != 0) || (this.mYOffset != 0))
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("Offsets x=");
                paramPrintWriter.print(this.mXOffset);
                paramPrintWriter.print(" y=");
                paramPrintWriter.println(this.mYOffset);
            }
            if (paramBoolean)
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("mGivenContentInsets=");
                this.mGivenContentInsets.printShortString(paramPrintWriter);
                paramPrintWriter.print(" mGivenVisibleInsets=");
                this.mGivenVisibleInsets.printShortString(paramPrintWriter);
                paramPrintWriter.println();
                if ((this.mTouchableInsets != 0) || (this.mGivenInsetsPending))
                {
                    paramPrintWriter.print(paramString);
                    paramPrintWriter.print("mTouchableInsets=");
                    paramPrintWriter.print(this.mTouchableInsets);
                    paramPrintWriter.print(" mGivenInsetsPending=");
                    paramPrintWriter.println(this.mGivenInsetsPending);
                }
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("mConfiguration=");
                paramPrintWriter.println(this.mConfiguration);
            }
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("mHasSurface=");
            paramPrintWriter.print(this.mHasSurface);
            paramPrintWriter.print(" mShownFrame=");
            this.mShownFrame.printShortString(paramPrintWriter);
            paramPrintWriter.println();
            if (paramBoolean)
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("mFrame=");
                this.mFrame.printShortString(paramPrintWriter);
                paramPrintWriter.print(" last=");
                this.mLastFrame.printShortString(paramPrintWriter);
                paramPrintWriter.println();
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("mSystemDecorRect=");
                this.mSystemDecorRect.printShortString(paramPrintWriter);
                paramPrintWriter.print(" last=");
                this.mLastSystemDecorRect.printShortString(paramPrintWriter);
                paramPrintWriter.println();
            }
            if (this.mEnforceSizeCompat)
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("mCompatFrame=");
                this.mCompatFrame.printShortString(paramPrintWriter);
                paramPrintWriter.println();
            }
            if (paramBoolean)
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("Frames: containing=");
                this.mContainingFrame.printShortString(paramPrintWriter);
                paramPrintWriter.print(" parent=");
                this.mParentFrame.printShortString(paramPrintWriter);
                paramPrintWriter.print(" display=");
                this.mDisplayFrame.printShortString(paramPrintWriter);
                paramPrintWriter.println();
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("        content=");
                this.mContentFrame.printShortString(paramPrintWriter);
                paramPrintWriter.print(" visible=");
                this.mVisibleFrame.printShortString(paramPrintWriter);
                paramPrintWriter.println();
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("Cur insets: content=");
                this.mContentInsets.printShortString(paramPrintWriter);
                paramPrintWriter.print(" visible=");
                this.mVisibleInsets.printShortString(paramPrintWriter);
                paramPrintWriter.println();
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("Lst insets: content=");
                this.mLastContentInsets.printShortString(paramPrintWriter);
                paramPrintWriter.print(" visible=");
                this.mLastVisibleInsets.printShortString(paramPrintWriter);
                paramPrintWriter.println();
            }
            this.mWinAnimator.dump(paramPrintWriter, paramString, paramBoolean);
            if ((this.mExiting) || (this.mRemoveOnExit) || (this.mDestroying) || (this.mRemoved))
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("mExiting=");
                paramPrintWriter.print(this.mExiting);
                paramPrintWriter.print(" mRemoveOnExit=");
                paramPrintWriter.print(this.mRemoveOnExit);
                paramPrintWriter.print(" mDestroying=");
                paramPrintWriter.print(this.mDestroying);
                paramPrintWriter.print(" mRemoved=");
                paramPrintWriter.println(this.mRemoved);
            }
            if ((this.mOrientationChanging) || (this.mAppFreezing) || (this.mTurnOnScreen))
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("mOrientationChanging=");
                paramPrintWriter.print(this.mOrientationChanging);
                paramPrintWriter.print(" mAppFreezing=");
                paramPrintWriter.print(this.mAppFreezing);
                paramPrintWriter.print(" mTurnOnScreen=");
                paramPrintWriter.println(this.mTurnOnScreen);
            }
            if ((this.mHScale != 1.0F) || (this.mVScale != 1.0F))
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("mHScale=");
                paramPrintWriter.print(this.mHScale);
                paramPrintWriter.print(" mVScale=");
                paramPrintWriter.println(this.mVScale);
            }
            if ((this.mWallpaperX != -1.0F) || (this.mWallpaperY != -1.0F))
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("mWallpaperX=");
                paramPrintWriter.print(this.mWallpaperX);
                paramPrintWriter.print(" mWallpaperY=");
                paramPrintWriter.println(this.mWallpaperY);
            }
            if ((this.mWallpaperXStep != -1.0F) || (this.mWallpaperYStep != -1.0F))
            {
                paramPrintWriter.print(paramString);
                paramPrintWriter.print("mWallpaperXStep=");
                paramPrintWriter.print(this.mWallpaperXStep);
                paramPrintWriter.print(" mWallpaperYStep=");
                paramPrintWriter.println(this.mWallpaperYStep);
            }
            return;
            label1559: if (this.mAppToken != null)
                i = this.mAppToken.mAppAnimator.animLayerAdjustment;
            else
                i = 0;
        }
    }

    public IApplicationToken getAppToken()
    {
        if (this.mAppToken != null);
        for (IApplicationToken localIApplicationToken = this.mAppToken.appToken; ; localIApplicationToken = null)
            return localIApplicationToken;
    }

    public WindowManager.LayoutParams getAttrs()
    {
        return this.mAttrs;
    }

    public Rect getContentFrameLw()
    {
        return this.mContentFrame;
    }

    public Rect getDisplayFrameLw()
    {
        return this.mDisplayFrame;
    }

    public Rect getFrameLw()
    {
        return this.mFrame;
    }

    public Rect getGivenContentInsetsLw()
    {
        return this.mGivenContentInsets;
    }

    public boolean getGivenInsetsPendingLw()
    {
        return this.mGivenInsetsPending;
    }

    public Rect getGivenVisibleInsetsLw()
    {
        return this.mGivenVisibleInsets;
    }

    public long getInputDispatchingTimeoutNanos()
    {
        if (this.mAppToken != null);
        for (long l = this.mAppToken.inputDispatchingTimeoutNanos; ; l = 5000000000L)
            return l;
    }

    public boolean getNeedsMenuLw(WindowManagerPolicy.WindowState paramWindowState)
    {
        boolean bool = false;
        int i = -1;
        for (WindowState localWindowState = this; ; localWindowState = (WindowState)this.mService.mWindows.get(i))
        {
            if ((0x8 & localWindowState.mAttrs.privateFlags) != 0)
                if ((0x8000000 & localWindowState.mAttrs.flags) != 0)
                    bool = true;
            do
            {
                do
                    return bool;
                while (localWindowState == paramWindowState);
                if (i < 0)
                    i = this.mService.mWindows.indexOf(localWindowState);
                i--;
            }
            while (i < 0);
        }
    }

    public RectF getShownFrameLw()
    {
        return this.mShownFrame;
    }

    public int getSurfaceLayer()
    {
        return this.mLayer;
    }

    public int getSystemUiVisibility()
    {
        return this.mSystemUiVisibility;
    }

    public void getTouchableRegion(Region paramRegion)
    {
        Rect localRect = this.mFrame;
        switch (this.mTouchableInsets)
        {
        default:
            paramRegion.set(localRect);
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            return;
            applyInsets(paramRegion, localRect, this.mGivenContentInsets);
            continue;
            applyInsets(paramRegion, localRect, this.mGivenVisibleInsets);
            continue;
            paramRegion.set(this.mGivenTouchableRegion);
            paramRegion.translate(localRect.left, localRect.top);
        }
    }

    public Rect getVisibleFrameLw()
    {
        return this.mVisibleFrame;
    }

    public boolean hasAppShownWindows()
    {
        if ((this.mAppToken != null) && ((this.mAppToken.firstWindowDrawn) || (this.mAppToken.startingDisplayed)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean hasDrawnLw()
    {
        if (this.mWinAnimator.mDrawState == 4);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean hideLw(boolean paramBoolean)
    {
        return hideLw(paramBoolean, true);
    }

    boolean hideLw(boolean paramBoolean1, boolean paramBoolean2)
    {
        boolean bool1 = false;
        if ((paramBoolean1) && (!this.mService.okToDisplay()))
            paramBoolean1 = false;
        if (paramBoolean1);
        for (boolean bool2 = this.mPolicyVisibilityAfterAnim; !bool2; bool2 = this.mPolicyVisibility)
            return bool1;
        if (paramBoolean1)
        {
            this.mWinAnimator.applyAnimationLocked(8194, false);
            if (this.mWinAnimator.mAnimation == null)
                paramBoolean1 = false;
        }
        if (paramBoolean1)
            this.mPolicyVisibilityAfterAnim = false;
        while (true)
        {
            if (paramBoolean2)
                this.mService.scheduleAnimationLocked();
            bool1 = true;
            break;
            this.mPolicyVisibilityAfterAnim = false;
            this.mPolicyVisibility = false;
            this.mService.enableScreenIfNeededLocked();
            if (this.mService.mCurrentFocus == this)
                this.mService.mFocusMayChange = true;
        }
    }

    public boolean isAlive()
    {
        return this.mClient.asBinder().isBinderAlive();
    }

    public boolean isAnimatingLw()
    {
        if (this.mWinAnimator.mAnimation != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isDisplayedLw()
    {
        AppWindowToken localAppWindowToken = this.mAppToken;
        if ((isDrawnLw()) && (this.mPolicyVisibility) && (((!this.mAttachedHidden) && ((localAppWindowToken == null) || (!localAppWindowToken.hiddenRequested))) || (this.mWinAnimator.mAnimating)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isDrawnLw()
    {
        if ((this.mHasSurface) && (!this.mDestroying) && ((this.mWinAnimator.mDrawState == 3) || (this.mWinAnimator.mDrawState == 4)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean isFullscreen(int paramInt1, int paramInt2)
    {
        if ((this.mFrame.left <= 0) && (this.mFrame.top <= 0) && (this.mFrame.right >= paramInt1) && (this.mFrame.bottom >= paramInt2));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isGoneForLayoutLw()
    {
        AppWindowToken localAppWindowToken = this.mAppToken;
        if ((this.mViewVisibility == 8) || (!this.mRelayoutCalled) || ((localAppWindowToken == null) && (this.mRootToken.hidden)) || ((localAppWindowToken != null) && ((localAppWindowToken.hiddenRequested) || (localAppWindowToken.hidden))) || (this.mAttachedHidden) || (this.mExiting) || (this.mDestroying));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean isIdentityMatrix(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
        boolean bool = false;
        if ((paramFloat1 < 0.99999F) || (paramFloat1 > 1.00001F));
        while (true)
        {
            return bool;
            if ((paramFloat4 >= 0.99999F) && (paramFloat4 <= 1.00001F) && (paramFloat2 >= -1.0E-06F) && (paramFloat2 <= 1.0E-06F) && (paramFloat3 >= -1.0E-06F) && (paramFloat3 <= 1.0E-06F))
                bool = true;
        }
    }

    boolean isOnScreen()
    {
        boolean bool = false;
        if ((!this.mHasSurface) || (!this.mPolicyVisibility) || (this.mDestroying));
        while (true)
        {
            return bool;
            AppWindowToken localAppWindowToken = this.mAppToken;
            if (localAppWindowToken != null)
            {
                if (((!this.mAttachedHidden) && (!localAppWindowToken.hiddenRequested)) || (this.mWinAnimator.mAnimation != null) || (localAppWindowToken.mAppAnimator.animation != null))
                    bool = true;
            }
            else if ((!this.mAttachedHidden) || (this.mWinAnimator.mAnimation != null))
                bool = true;
        }
    }

    boolean isOpaqueDrawn()
    {
        if (((this.mAttrs.format == -1) || (this.mAttrs.type == 2013)) && (isDrawnLw()) && (this.mWinAnimator.mAnimation == null) && ((this.mAppToken == null) || (this.mAppToken.mAppAnimator.animation == null)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean isPotentialDragTarget()
    {
        if ((isVisibleNow()) && (!this.mRemoved) && (this.mInputChannel != null) && (this.mInputWindowHandle != null));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean isReadyForDisplay()
    {
        boolean bool = false;
        if ((this.mRootToken.waitingToShow) && (this.mService.mNextAppTransition != -1));
        while (true)
        {
            return bool;
            if ((this.mHasSurface) && (this.mPolicyVisibility) && (!this.mDestroying) && (((!this.mAttachedHidden) && (this.mViewVisibility == 0) && (!this.mRootToken.hidden)) || (this.mWinAnimator.mAnimation != null) || ((this.mAppToken != null) && (this.mAppToken.mAppAnimator.animation != null))))
                bool = true;
        }
    }

    boolean isReadyForDisplayIgnoringKeyguard()
    {
        boolean bool = false;
        if ((this.mRootToken.waitingToShow) && (this.mService.mNextAppTransition != -1));
        while (true)
        {
            return bool;
            AppWindowToken localAppWindowToken = this.mAppToken;
            if (((localAppWindowToken != null) || (this.mPolicyVisibility)) && (this.mHasSurface) && (!this.mDestroying) && (((!this.mAttachedHidden) && (this.mViewVisibility == 0) && (!this.mRootToken.hidden)) || (this.mWinAnimator.mAnimation != null) || ((localAppWindowToken != null) && (localAppWindowToken.mAppAnimator.animation != null) && (!this.mWinAnimator.isDummyAnimation()))))
                bool = true;
        }
    }

    public boolean isVisibleLw()
    {
        AppWindowToken localAppWindowToken = this.mAppToken;
        if ((this.mHasSurface) && (this.mPolicyVisibility) && (!this.mAttachedHidden) && ((localAppWindowToken == null) || (!localAppWindowToken.hiddenRequested)) && (!this.mExiting) && (!this.mDestroying));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean isVisibleNow()
    {
        if ((this.mHasSurface) && (this.mPolicyVisibility) && (!this.mAttachedHidden) && (!this.mRootToken.hidden) && (!this.mExiting) && (!this.mDestroying));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean isVisibleOrAdding()
    {
        AppWindowToken localAppWindowToken = this.mAppToken;
        if (((this.mHasSurface) || ((!this.mRelayoutCalled) && (this.mViewVisibility == 0))) && (this.mPolicyVisibility) && (!this.mAttachedHidden) && ((localAppWindowToken == null) || (!localAppWindowToken.hiddenRequested)) && (!this.mExiting) && (!this.mDestroying));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isVisibleOrBehindKeyguardLw()
    {
        boolean bool1 = true;
        boolean bool2 = false;
        if ((this.mRootToken.waitingToShow) && (this.mService.mNextAppTransition != -1))
            return bool2;
        AppWindowToken localAppWindowToken = this.mAppToken;
        int i;
        if (localAppWindowToken != null)
            if (localAppWindowToken.mAppAnimator.animation != null)
            {
                i = bool1;
                label50: if ((!this.mHasSurface) || (this.mDestroying) || (this.mExiting))
                    break label145;
                if (localAppWindowToken != null)
                    break label138;
                if (!this.mPolicyVisibility)
                    break label145;
                label82: if (((this.mAttachedHidden) || (this.mViewVisibility != 0) || (this.mRootToken.hidden)) && (this.mWinAnimator.mAnimation == null) && (i == 0))
                    break label145;
            }
        while (true)
        {
            bool2 = bool1;
            break;
            i = 0;
            break label50;
            i = 0;
            break label50;
            label138: if (!localAppWindowToken.hiddenRequested)
                break label82;
            label145: bool1 = false;
        }
    }

    public boolean isWinVisibleLw()
    {
        AppWindowToken localAppWindowToken = this.mAppToken;
        if ((this.mHasSurface) && (this.mPolicyVisibility) && (!this.mAttachedHidden) && ((localAppWindowToken == null) || (!localAppWindowToken.hiddenRequested) || (localAppWindowToken.mAppAnimator.animating)) && (!this.mExiting) && (!this.mDestroying));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    String makeInputChannelName()
    {
        return Integer.toHexString(System.identityHashCode(this)) + " " + this.mAttrs.getTitle();
    }

    void prelayout()
    {
        if (this.mEnforceSizeCompat)
        {
            this.mGlobalScale = this.mService.mCompatibleScreenScale;
            this.mInvGlobalScale = (1.0F / this.mGlobalScale);
        }
        while (true)
        {
            return;
            this.mInvGlobalScale = 1.0F;
            this.mGlobalScale = 1.0F;
        }
    }

    void removeLocked()
    {
        disposeInputChannel();
        if (this.mAttachedWindow != null)
            this.mAttachedWindow.mChildWindows.remove(this);
        this.mWinAnimator.destroyDeferredSurfaceLocked();
        this.mWinAnimator.destroySurfaceLocked();
        this.mSession.windowRemovedLocked();
        try
        {
            this.mClient.asBinder().unlinkToDeath(this.mDeathRecipient, 0);
            label64: return;
        }
        catch (RuntimeException localRuntimeException)
        {
            break label64;
        }
    }

    void setInputChannel(InputChannel paramInputChannel)
    {
        if (this.mInputChannel != null)
            throw new IllegalStateException("Window already has an input channel.");
        this.mInputChannel = paramInputChannel;
        this.mInputWindowHandle.inputChannel = paramInputChannel;
    }

    boolean shouldAnimateMove()
    {
        if ((this.mContentChanged) && (!this.mExiting) && (!this.mWinAnimator.mLastHidden) && (this.mService.okToDisplay()) && ((this.mFrame.top != this.mLastFrame.top) || (this.mFrame.left != this.mLastFrame.left)) && ((this.mAttachedWindow == null) || (!this.mAttachedWindow.shouldAnimateMove())));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean showLw(boolean paramBoolean)
    {
        return showLw(paramBoolean, true);
    }

    boolean showLw(boolean paramBoolean1, boolean paramBoolean2)
    {
        boolean bool = true;
        if ((this.mPolicyVisibility) && (this.mPolicyVisibilityAfterAnim))
        {
            bool = false;
            return bool;
        }
        if (paramBoolean1)
            if (this.mService.okToDisplay())
                break label76;
        for (paramBoolean1 = false; ; paramBoolean1 = false)
            label76: 
            do
            {
                this.mPolicyVisibility = bool;
                this.mPolicyVisibilityAfterAnim = bool;
                if (paramBoolean1)
                    this.mWinAnimator.applyAnimationLocked(4097, bool);
                if (!paramBoolean2)
                    break;
                this.mService.scheduleAnimationLocked();
                break;
            }
            while ((!this.mPolicyVisibility) || (this.mWinAnimator.mAnimation != null));
    }

    public String toString()
    {
        if ((this.mStringNameCache == null) || (this.mLastTitle != this.mAttrs.getTitle()) || (this.mWasPaused != this.mToken.paused))
        {
            this.mLastTitle = this.mAttrs.getTitle();
            this.mWasPaused = this.mToken.paused;
            this.mStringNameCache = ("Window{" + Integer.toHexString(System.identityHashCode(this)) + " " + this.mLastTitle + " paused=" + this.mWasPaused + "}");
        }
        return this.mStringNameCache;
    }

    private class DeathRecipient
        implements IBinder.DeathRecipient
    {
        private DeathRecipient()
        {
        }

        public void binderDied()
        {
            try
            {
                synchronized (WindowState.this.mService.mWindowMap)
                {
                    WindowState localWindowState = WindowState.this.mService.windowForClientLocked(WindowState.this.mSession, WindowState.this.mClient, false);
                    Slog.i("WindowState", "WIN DEATH: " + localWindowState);
                    if (localWindowState != null)
                        WindowState.this.mService.removeWindowLocked(WindowState.this.mSession, localWindowState);
                }
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.wm.WindowState
 * JD-Core Version:        0.6.2
 */