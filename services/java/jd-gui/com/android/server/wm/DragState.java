package com.android.server.wm;

import android.content.ClipData;
import android.content.ClipDescription;
import android.graphics.Rect;
import android.graphics.Region;
import android.os.IBinder;
import android.os.Message;
import android.os.Process;
import android.os.RemoteException;
import android.util.Slog;
import android.view.DragEvent;
import android.view.IWindow;
import android.view.InputChannel;
import android.view.Surface;
import android.view.WindowManager.LayoutParams;
import android.view.WindowManagerPolicy;
import com.android.server.input.InputApplicationHandle;
import com.android.server.input.InputManagerService;
import com.android.server.input.InputWindowHandle;
import java.util.ArrayList;
import java.util.Iterator;

class DragState
{
    InputChannel mClientChannel;
    float mCurrentX;
    float mCurrentY;
    ClipData mData;
    ClipDescription mDataDescription;
    InputApplicationHandle mDragApplicationHandle;
    boolean mDragInProgress;
    boolean mDragResult;
    InputWindowHandle mDragWindowHandle;
    int mFlags;
    WindowManagerService.DragInputEventReceiver mInputEventReceiver;
    IBinder mLocalWin;
    ArrayList<WindowState> mNotifiedWindows;
    InputChannel mServerChannel;
    final WindowManagerService mService;
    Surface mSurface;
    WindowState mTargetWindow;
    float mThumbOffsetX;
    float mThumbOffsetY;
    private final Region mTmpRegion = new Region();
    IBinder mToken;

    DragState(WindowManagerService paramWindowManagerService, IBinder paramIBinder1, Surface paramSurface, int paramInt, IBinder paramIBinder2)
    {
        this.mService = paramWindowManagerService;
        this.mToken = paramIBinder1;
        this.mSurface = paramSurface;
        this.mFlags = paramInt;
        this.mLocalWin = paramIBinder2;
        this.mNotifiedWindows = new ArrayList();
    }

    private WindowState getTouchedWinAtPointLw(float paramFloat1, float paramFloat2)
    {
        Object localObject = null;
        int i = (int)paramFloat1;
        int j = (int)paramFloat2;
        ArrayList localArrayList = this.mService.mWindows;
        int k = -1 + localArrayList.size();
        if (k >= 0)
        {
            WindowState localWindowState = (WindowState)localArrayList.get(k);
            int m = localWindowState.mAttrs.flags;
            if (!localWindowState.isVisibleLw());
            int n;
            do
            {
                do
                {
                    k--;
                    break;
                }
                while ((m & 0x10) != 0);
                localWindowState.getTouchableRegion(this.mTmpRegion);
                n = m & 0x28;
            }
            while ((!this.mTmpRegion.contains(i, j)) && (n != 0));
            localObject = localWindowState;
        }
        return localObject;
    }

    private static DragEvent obtainDragEvent(WindowState paramWindowState, int paramInt, float paramFloat1, float paramFloat2, Object paramObject, ClipDescription paramClipDescription, ClipData paramClipData, boolean paramBoolean)
    {
        float f1 = paramFloat1 - paramWindowState.mFrame.left;
        float f2 = paramFloat2 - paramWindowState.mFrame.top;
        if (paramWindowState.mEnforceSizeCompat)
        {
            f1 *= paramWindowState.mGlobalScale;
            f2 *= paramWindowState.mGlobalScale;
        }
        return DragEvent.obtain(paramInt, f1, f2, paramObject, paramClipDescription, paramClipData, paramBoolean);
    }

    private void sendDragStartedLw(WindowState paramWindowState, float paramFloat1, float paramFloat2, ClipDescription paramClipDescription)
    {
        if (((0x1 & this.mFlags) == 0) && (paramWindowState.mClient.asBinder() != this.mLocalWin));
        while (true)
        {
            return;
            if ((!this.mDragInProgress) || (!paramWindowState.isPotentialDragTarget()))
                continue;
            DragEvent localDragEvent = obtainDragEvent(paramWindowState, 1, paramFloat1, paramFloat2, null, paramClipDescription, null, false);
            try
            {
                paramWindowState.mClient.dispatchDragEvent(localDragEvent);
                this.mNotifiedWindows.add(paramWindowState);
                if (Process.myPid() == paramWindowState.mSession.mPid)
                    continue;
                localDragEvent.recycle();
            }
            catch (RemoteException localRemoteException)
            {
                Slog.w("WindowManager", "Unable to drag-start window " + paramWindowState);
                if (Process.myPid() == paramWindowState.mSession.mPid)
                    continue;
                localDragEvent.recycle();
            }
            finally
            {
                if (Process.myPid() != paramWindowState.mSession.mPid)
                    localDragEvent.recycle();
            }
        }
    }

    void broadcastDragEndedLw()
    {
        DragEvent localDragEvent = DragEvent.obtain(4, 0.0F, 0.0F, null, null, null, this.mDragResult);
        Iterator localIterator = this.mNotifiedWindows.iterator();
        while (localIterator.hasNext())
        {
            WindowState localWindowState = (WindowState)localIterator.next();
            try
            {
                localWindowState.mClient.dispatchDragEvent(localDragEvent);
            }
            catch (RemoteException localRemoteException)
            {
                Slog.w("WindowManager", "Unable to drag-end window " + localWindowState);
            }
        }
        this.mNotifiedWindows.clear();
        this.mDragInProgress = false;
        localDragEvent.recycle();
    }

    void broadcastDragStartedLw(float paramFloat1, float paramFloat2)
    {
        if (this.mData != null);
        for (ClipDescription localClipDescription = this.mData.getDescription(); ; localClipDescription = null)
        {
            this.mDataDescription = localClipDescription;
            this.mNotifiedWindows.clear();
            this.mDragInProgress = true;
            int i = this.mService.mWindows.size();
            for (int j = 0; j < i; j++)
                sendDragStartedLw((WindowState)this.mService.mWindows.get(j), paramFloat1, paramFloat2, this.mDataDescription);
        }
    }

    void endDragLw()
    {
        this.mService.mDragState.broadcastDragEndedLw();
        this.mService.mDragState.unregister();
        this.mService.mInputMonitor.updateInputWindowsLw(true);
        this.mService.mDragState.reset();
        this.mService.mDragState = null;
    }

    int getDragLayerLw()
    {
        return 1000 + 10000 * this.mService.mPolicy.windowTypeToLayerLw(2016);
    }

    boolean notifyDropLw(float paramFloat1, float paramFloat2)
    {
        WindowState localWindowState = getTouchedWinAtPointLw(paramFloat1, paramFloat2);
        boolean bool;
        if (localWindowState == null)
        {
            this.mDragResult = false;
            bool = true;
        }
        while (true)
        {
            return bool;
            int i = Process.myPid();
            IBinder localIBinder = localWindowState.mClient.asBinder();
            DragEvent localDragEvent = obtainDragEvent(localWindowState, 3, paramFloat1, paramFloat2, null, null, this.mData, false);
            try
            {
                localWindowState.mClient.dispatchDragEvent(localDragEvent);
                this.mService.mH.removeMessages(21, localIBinder);
                Message localMessage = this.mService.mH.obtainMessage(21, localIBinder);
                this.mService.mH.sendMessageDelayed(localMessage, 5000L);
                if (i != localWindowState.mSession.mPid)
                    localDragEvent.recycle();
                this.mToken = localIBinder;
                bool = false;
            }
            catch (RemoteException localRemoteException)
            {
                Slog.w("WindowManager", "can't send drop notification to win " + localWindowState);
                bool = true;
                if (i == localWindowState.mSession.mPid)
                    continue;
                localDragEvent.recycle();
            }
            finally
            {
                if (i != localWindowState.mSession.mPid)
                    localDragEvent.recycle();
            }
        }
    }

    void notifyMoveLw(float paramFloat1, float paramFloat2)
    {
        int i = Process.myPid();
        Surface.openTransaction();
        while (true)
        {
            WindowState localWindowState;
            try
            {
                this.mSurface.setPosition(paramFloat1 - this.mThumbOffsetX, paramFloat2 - this.mThumbOffsetY);
                Surface.closeTransaction();
                localWindowState = getTouchedWinAtPointLw(paramFloat1, paramFloat2);
                if (localWindowState == null)
                    return;
            }
            finally
            {
                Surface.closeTransaction();
            }
            if (((0x1 & this.mFlags) == 0) && (localWindowState.mClient.asBinder() != this.mLocalWin))
                localWindowState = null;
            try
            {
                if ((localWindowState != this.mTargetWindow) && (this.mTargetWindow != null))
                {
                    DragEvent localDragEvent2 = obtainDragEvent(this.mTargetWindow, 6, paramFloat1, paramFloat2, null, null, null, false);
                    this.mTargetWindow.mClient.dispatchDragEvent(localDragEvent2);
                    if (i != this.mTargetWindow.mSession.mPid)
                        localDragEvent2.recycle();
                }
                if (localWindowState != null)
                {
                    DragEvent localDragEvent1 = obtainDragEvent(localWindowState, 2, paramFloat1, paramFloat2, null, null, null, false);
                    localWindowState.mClient.dispatchDragEvent(localDragEvent1);
                    if (i != localWindowState.mSession.mPid)
                        localDragEvent1.recycle();
                }
                this.mTargetWindow = localWindowState;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Slog.w("WindowManager", "can't send drag notification to windows");
            }
        }
    }

    void register()
    {
        if (this.mClientChannel != null)
            Slog.e("WindowManager", "Duplicate register of drag input channel");
        while (true)
        {
            return;
            InputChannel[] arrayOfInputChannel = InputChannel.openInputChannelPair("drag");
            this.mServerChannel = arrayOfInputChannel[0];
            this.mClientChannel = arrayOfInputChannel[1];
            this.mService.mInputManager.registerInputChannel(this.mServerChannel, null);
            WindowManagerService localWindowManagerService = this.mService;
            localWindowManagerService.getClass();
            this.mInputEventReceiver = new WindowManagerService.DragInputEventReceiver(localWindowManagerService, this.mClientChannel, this.mService.mH.getLooper());
            this.mDragApplicationHandle = new InputApplicationHandle(null);
            this.mDragApplicationHandle.name = "drag";
            this.mDragApplicationHandle.dispatchingTimeoutNanos = 5000000000L;
            this.mDragWindowHandle = new InputWindowHandle(this.mDragApplicationHandle, null);
            this.mDragWindowHandle.name = "drag";
            this.mDragWindowHandle.inputChannel = this.mServerChannel;
            this.mDragWindowHandle.layer = getDragLayerLw();
            this.mDragWindowHandle.layoutParamsFlags = 0;
            this.mDragWindowHandle.layoutParamsType = 2016;
            this.mDragWindowHandle.dispatchingTimeoutNanos = 5000000000L;
            this.mDragWindowHandle.visible = true;
            this.mDragWindowHandle.canReceiveKeys = false;
            this.mDragWindowHandle.hasFocus = true;
            this.mDragWindowHandle.hasWallpaper = false;
            this.mDragWindowHandle.paused = false;
            this.mDragWindowHandle.ownerPid = Process.myPid();
            this.mDragWindowHandle.ownerUid = Process.myUid();
            this.mDragWindowHandle.inputFeatures = 0;
            this.mDragWindowHandle.scaleFactor = 1.0F;
            this.mDragWindowHandle.touchableRegion.setEmpty();
            this.mDragWindowHandle.frameLeft = 0;
            this.mDragWindowHandle.frameTop = 0;
            this.mDragWindowHandle.frameRight = this.mService.mCurDisplayWidth;
            this.mDragWindowHandle.frameBottom = this.mService.mCurDisplayHeight;
            this.mService.pauseRotationLocked();
        }
    }

    void reset()
    {
        if (this.mSurface != null)
            this.mSurface.destroy();
        this.mSurface = null;
        this.mFlags = 0;
        this.mLocalWin = null;
        this.mToken = null;
        this.mData = null;
        this.mThumbOffsetY = 0.0F;
        this.mThumbOffsetX = 0.0F;
        this.mNotifiedWindows = null;
    }

    void sendDragStartedIfNeededLw(WindowState paramWindowState)
    {
        if (this.mDragInProgress)
        {
            Iterator localIterator = this.mNotifiedWindows.iterator();
            do
                if (!localIterator.hasNext())
                    break;
            while ((WindowState)localIterator.next() != paramWindowState);
        }
        while (true)
        {
            return;
            sendDragStartedLw(paramWindowState, this.mCurrentX, this.mCurrentY, this.mDataDescription);
        }
    }

    void unregister()
    {
        if (this.mClientChannel == null)
            Slog.e("WindowManager", "Unregister of nonexistent drag input channel");
        while (true)
        {
            return;
            this.mService.mInputManager.unregisterInputChannel(this.mServerChannel);
            this.mInputEventReceiver.dispose();
            this.mInputEventReceiver = null;
            this.mClientChannel.dispose();
            this.mServerChannel.dispose();
            this.mClientChannel = null;
            this.mServerChannel = null;
            this.mDragWindowHandle = null;
            this.mDragApplicationHandle = null;
            this.mService.resumeRotationLocked();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.wm.DragState
 * JD-Core Version:        0.6.2
 */