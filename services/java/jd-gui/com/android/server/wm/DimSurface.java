package com.android.server.wm;

import android.util.Slog;
import android.view.Surface;
import android.view.SurfaceSession;
import java.io.PrintWriter;

class DimSurface
{
    int mDimColor = 0;
    boolean mDimShown = false;
    Surface mDimSurface;
    int mLastDimHeight;
    int mLastDimWidth;
    int mLayer = -1;

    DimSurface(SurfaceSession paramSurfaceSession)
    {
        if (this.mDimSurface == null);
        try
        {
            this.mDimSurface = new Surface(paramSurfaceSession, 0, "DimSurface", -1, 16, 16, -1, 131072);
            this.mDimSurface.setAlpha(0.0F);
            return;
        }
        catch (Exception localException)
        {
            while (true)
                Slog.e("WindowManager", "Exception creating Dim surface", localException);
        }
    }

    void hide()
    {
        if (this.mDimShown)
            this.mDimShown = false;
        try
        {
            this.mDimSurface.hide();
            return;
        }
        catch (RuntimeException localRuntimeException)
        {
            while (true)
                Slog.w("WindowManager", "Illegal argument exception hiding dim surface");
        }
    }

    public void printTo(String paramString, PrintWriter paramPrintWriter)
    {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mDimSurface=");
        paramPrintWriter.println(this.mDimSurface);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mDimShown=");
        paramPrintWriter.print(this.mDimShown);
        paramPrintWriter.print(" mLayer=");
        paramPrintWriter.print(this.mLayer);
        paramPrintWriter.print(" mDimColor=0x");
        paramPrintWriter.println(Integer.toHexString(this.mDimColor));
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mLastDimWidth=");
        paramPrintWriter.print(this.mLastDimWidth);
        paramPrintWriter.print(" mLastDimWidth=");
        paramPrintWriter.println(this.mLastDimWidth);
    }

    void show(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        if (!this.mDimShown)
            this.mDimShown = true;
        while (true)
        {
            try
            {
                this.mLastDimWidth = paramInt1;
                this.mLastDimHeight = paramInt2;
                this.mDimSurface.setPosition(0, 0);
                this.mDimSurface.setSize(paramInt1, paramInt2);
                this.mDimSurface.setLayer(paramInt3);
                this.mDimSurface.show();
                return;
            }
            catch (RuntimeException localRuntimeException)
            {
                Slog.w("WindowManager", "Failure showing dim surface", localRuntimeException);
                continue;
            }
            if ((this.mLastDimWidth != paramInt1) || (this.mLastDimHeight != paramInt2) || (this.mDimColor != paramInt4) || (this.mLayer != paramInt3))
            {
                this.mLastDimWidth = paramInt1;
                this.mLastDimHeight = paramInt2;
                this.mLayer = paramInt3;
                this.mDimColor = paramInt4;
                this.mDimSurface.setSize(paramInt1, paramInt2);
                this.mDimSurface.setLayer(paramInt3);
                this.mDimSurface.setAlpha((0xFF & paramInt4 >> 24) / 255.0F);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.wm.DimSurface
 * JD-Core Version:        0.6.2
 */