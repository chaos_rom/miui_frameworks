package com.android.server;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.ObbInfo;
import android.net.Uri;
import android.os.Binder;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Looper;
import android.os.Message;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.os.UserId;
import android.os.storage.IMountService.Stub;
import android.os.storage.IMountServiceListener;
import android.os.storage.IMountShutdownObserver;
import android.os.storage.IObbActionListener;
import android.os.storage.StorageVolume;
import android.text.TextUtils;
import android.util.Slog;
import com.android.internal.app.IMediaContainerService;
import com.android.internal.app.IMediaContainerService.Stub;
import com.android.server.am.ActivityManagerService;
import com.android.server.pm.PackageManagerService;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

class MountService extends IMountService.Stub
    implements INativeDaemonConnectorCallbacks, Watchdog.Monitor
{
    private static final int CRYPTO_ALGORITHM_KEY_SIZE = 128;
    private static final boolean DEBUG_EVENTS = false;
    private static final boolean DEBUG_OBB = false;
    private static final boolean DEBUG_UNMOUNT = false;
    static final ComponentName DEFAULT_CONTAINER_COMPONENT = new ComponentName("com.android.defcontainer", "com.android.defcontainer.DefaultContainerService");
    private static final int H_UNMOUNT_MS = 3;
    private static final int H_UNMOUNT_PM_DONE = 2;
    private static final int H_UNMOUNT_PM_UPDATE = 1;
    private static final boolean LOCAL_LOGD = false;
    private static final int MAX_CONTAINERS = 250;
    private static final int MAX_UNMOUNT_RETRIES = 4;
    private static final int OBB_FLUSH_MOUNT_STATE = 5;
    private static final int OBB_MCS_BOUND = 2;
    private static final int OBB_MCS_RECONNECT = 4;
    private static final int OBB_MCS_UNBIND = 3;
    private static final int OBB_RUN_ACTION = 1;
    private static final int PBKDF2_HASH_ROUNDS = 1024;
    private static final int RETRY_UNMOUNT_DELAY = 30;
    private static final String TAG = "MountService";
    private static final String TAG_STORAGE = "storage";
    private static final String TAG_STORAGE_LIST = "StorageList";
    private static final String VOLD_TAG = "VoldConnector";
    private static final boolean WATCHDOG_ENABLE;
    private final HashSet<String> mAsecMountSet = new HashSet();
    private CountDownLatch mAsecsScanned = new CountDownLatch(1);
    private boolean mBooted = false;
    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            boolean bool = true;
            String str = paramAnonymousIntent.getAction();
            if (str.equals("android.intent.action.BOOT_COMPLETED"))
            {
                MountService.access$502(MountService.this, bool);
                if ("simulator".equals(SystemProperties.get("ro.product.device")))
                    MountService.this.notifyVolumeStateChange(null, "/sdcard", 0, 4);
            }
            while (!str.equals("android.hardware.usb.action.USB_STATE"))
                while (true)
                {
                    return;
                    new Thread()
                    {
                        public void run()
                        {
                            String str1;
                            String str2;
                            try
                            {
                                synchronized (MountService.this.mVolumeStates)
                                {
                                    Set localSet = MountService.this.mVolumeStates.keySet();
                                    int i = localSet.size();
                                    String[] arrayOfString1 = (String[])localSet.toArray(new String[i]);
                                    String[] arrayOfString2 = new String[i];
                                    for (int j = 0; j < i; j++)
                                        arrayOfString2[j] = ((String)MountService.this.mVolumeStates.get(arrayOfString1[j]));
                                    int k = 0;
                                    if (k < i)
                                    {
                                        str1 = arrayOfString1[k];
                                        str2 = arrayOfString2[k];
                                        if (str2.equals("unmounted"))
                                        {
                                            int m = MountService.this.doMountVolume(str1);
                                            if (m != 0)
                                            {
                                                Object[] arrayOfObject = new Object[1];
                                                arrayOfObject[0] = Integer.valueOf(m);
                                                Slog.e("MountService", String.format("Boot-time mount failed (%d)", arrayOfObject));
                                            }
                                            k++;
                                        }
                                    }
                                }
                            }
                            catch (Exception localException)
                            {
                                Slog.e("MountService", "Boot-time mount exception", localException);
                            }
                            while (true)
                            {
                                return;
                                if (!str2.equals("shared"))
                                    break;
                                MountService.this.notifyVolumeStateChange(null, str1, 0, 7);
                                break;
                                if (MountService.this.mEmulateExternalStorage)
                                    MountService.this.notifyVolumeStateChange(null, Environment.getExternalStorageDirectory().getPath(), 0, 4);
                                if (MountService.this.mSendUmsConnectedOnBoot)
                                {
                                    MountService.this.sendUmsIntent(true);
                                    MountService.access$1002(MountService.this, false);
                                }
                            }
                        }
                    }
                    .start();
                }
            if ((paramAnonymousIntent.getBooleanExtra("connected", false)) && (paramAnonymousIntent.getBooleanExtra("mass_storage", false)));
            while (true)
            {
                MountService.this.notifyShareAvailabilityChange(bool);
                break;
                bool = false;
            }
        }
    };
    private CountDownLatch mConnectedSignal = new CountDownLatch(1);
    private NativeDaemonConnector mConnector;
    private IMediaContainerService mContainerService = null;
    private Context mContext;
    private final DefaultContainerConnection mDefContainerConn = new DefaultContainerConnection();
    private boolean mEmulateExternalStorage = false;
    private String mExternalStoragePath;
    private final Handler mHandler;
    private final HandlerThread mHandlerThread;
    private final ArrayList<MountServiceBinderListener> mListeners = new ArrayList();
    private final ObbActionHandler mObbActionHandler;
    private final Map<IBinder, List<ObbState>> mObbMounts = new HashMap();
    private final Map<String, ObbState> mObbPathToStateMap = new HashMap();
    private PackageManagerService mPms;
    private StorageVolume mPrimaryVolume;
    private boolean mSendUmsConnectedOnBoot = false;
    private boolean mUmsAvailable = false;
    private boolean mUmsEnabling;
    private final HashMap<String, StorageVolume> mVolumeMap = new HashMap();
    private final HashMap<String, String> mVolumeStates = new HashMap();
    private final ArrayList<StorageVolume> mVolumes = new ArrayList();

    public MountService(Context paramContext)
    {
        this.mContext = paramContext;
        readStorageList();
        if (this.mPrimaryVolume != null)
        {
            this.mExternalStoragePath = this.mPrimaryVolume.getPath();
            this.mEmulateExternalStorage = this.mPrimaryVolume.isEmulated();
            if (this.mEmulateExternalStorage)
            {
                Slog.d("MountService", "using emulated external storage");
                this.mVolumeStates.put(this.mExternalStoragePath, "mounted");
            }
        }
        this.mPms = ((PackageManagerService)ServiceManager.getService("package"));
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("android.intent.action.BOOT_COMPLETED");
        if ((this.mPrimaryVolume != null) && (this.mPrimaryVolume.allowMassStorage()))
            localIntentFilter.addAction("android.hardware.usb.action.USB_STATE");
        this.mContext.registerReceiver(this.mBroadcastReceiver, localIntentFilter, null, null);
        this.mHandlerThread = new HandlerThread("MountService");
        this.mHandlerThread.start();
        this.mHandler = new MountServiceHandler(this.mHandlerThread.getLooper());
        this.mObbActionHandler = new ObbActionHandler(this.mHandlerThread.getLooper());
        this.mConnector = new NativeDaemonConnector(this, "vold", 500, "VoldConnector", 25);
        new Thread(this.mConnector, "VoldConnector").start();
    }

    private void addObbStateLocked(ObbState paramObbState)
        throws RemoteException
    {
        IBinder localIBinder = paramObbState.getBinder();
        Object localObject = (List)this.mObbMounts.get(localIBinder);
        if (localObject == null)
        {
            localObject = new ArrayList();
            this.mObbMounts.put(localIBinder, localObject);
        }
        while (true)
        {
            ((List)localObject).add(paramObbState);
            try
            {
                paramObbState.link();
                this.mObbPathToStateMap.put(paramObbState.filename, paramObbState);
                return;
                Iterator localIterator = ((List)localObject).iterator();
                do
                    if (!localIterator.hasNext())
                        break;
                while (!((ObbState)localIterator.next()).filename.equals(paramObbState.filename));
                throw new IllegalStateException("Attempt to add ObbState twice. This indicates an error in the MountService logic.");
            }
            catch (RemoteException localRemoteException)
            {
                ((List)localObject).remove(paramObbState);
                if (((List)localObject).isEmpty())
                    this.mObbMounts.remove(localIBinder);
                throw localRemoteException;
            }
        }
    }

    private int doFormatVolume(String paramString)
    {
        int i = 0;
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = "format";
            arrayOfObject[1] = paramString;
            localNativeDaemonConnector.execute("volume", arrayOfObject);
            return i;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            while (true)
            {
                int j = localNativeDaemonConnectorException.getCode();
                if (j == 401)
                    i = -2;
                else if (j == 403)
                    i = -4;
                else
                    i = -1;
            }
        }
    }

    private boolean doGetVolumeShared(String paramString1, String paramString2)
    {
        boolean bool = false;
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[3];
            arrayOfObject[0] = "shared";
            arrayOfObject[1] = paramString1;
            arrayOfObject[2] = paramString2;
            NativeDaemonEvent localNativeDaemonEvent = localNativeDaemonConnector.execute("volume", arrayOfObject);
            if (localNativeDaemonEvent.getCode() == 212)
                bool = localNativeDaemonEvent.getMessage().endsWith("enabled");
            return bool;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            while (true)
                Slog.e("MountService", "Failed to read response to volume shared " + paramString1 + " " + paramString2);
        }
    }

    private int doMountVolume(String paramString)
    {
        int i = 0;
        label133: 
        while (true)
        {
            String str;
            int j;
            try
            {
                NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
                Object[] arrayOfObject = new Object[2];
                arrayOfObject[0] = "mount";
                arrayOfObject[1] = paramString;
                localNativeDaemonConnector.execute("volume", arrayOfObject);
                return i;
            }
            catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
            {
                str = null;
                j = localNativeDaemonConnectorException.getCode();
                if (j != 401)
                    break label75;
            }
            i = -2;
            while (true)
            {
                if (str == null)
                    break label133;
                sendStorageIntent(str, paramString);
                break;
                label75: if (j == 402)
                {
                    updatePublicVolumeState(paramString, "nofs");
                    str = "android.intent.action.MEDIA_NOFS";
                    i = -3;
                }
                else if (j == 403)
                {
                    updatePublicVolumeState(paramString, "unmountable");
                    str = "android.intent.action.MEDIA_UNMOUNTABLE";
                    i = -4;
                }
                else
                {
                    i = -1;
                }
            }
        }
    }

    private void doShareUnshareVolume(String paramString1, String paramString2, boolean paramBoolean)
    {
        if (!paramString2.equals("ums"))
        {
            Object[] arrayOfObject2 = new Object[1];
            arrayOfObject2[0] = paramString2;
            throw new IllegalArgumentException(String.format("Method %s not supported", arrayOfObject2));
        }
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject1 = new Object[3];
            if (paramBoolean);
            for (String str = "share"; ; str = "unshare")
            {
                arrayOfObject1[0] = str;
                arrayOfObject1[1] = paramString1;
                arrayOfObject1[2] = paramString2;
                localNativeDaemonConnector.execute("volume", arrayOfObject1);
                break;
            }
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            Slog.e("MountService", "Failed to share/unshare", localNativeDaemonConnectorException);
        }
    }

    private int doUnmountVolume(String paramString, boolean paramBoolean1, boolean paramBoolean2)
    {
        int i = 404;
        if (!getVolumeState(paramString).equals("mounted"));
        while (true)
        {
            return i;
            Runtime.getRuntime().gc();
            this.mPms.updateExternalMediaStatus(false, false);
            try
            {
                Object[] arrayOfObject = new Object[2];
                arrayOfObject[0] = "unmount";
                arrayOfObject[1] = paramString;
                localCommand = new NativeDaemonConnector.Command("volume", arrayOfObject);
                if (paramBoolean2)
                {
                    localCommand.appendArg("force_and_revert");
                    this.mConnector.execute(localCommand);
                }
            }
            catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
            {
                int j;
                synchronized (this.mAsecMountSet)
                {
                    while (true)
                    {
                        NativeDaemonConnector.Command localCommand;
                        this.mAsecMountSet.clear();
                        i = 0;
                        break;
                        if (paramBoolean1)
                            localCommand.appendArg("force");
                    }
                    localNativeDaemonConnectorException = localNativeDaemonConnectorException;
                    j = localNativeDaemonConnectorException.getCode();
                    if (j == i)
                        i = -5;
                }
                if (j == 405)
                    i = -7;
                else
                    i = -1;
            }
        }
    }

    private boolean getUmsEnabling()
    {
        synchronized (this.mListeners)
        {
            boolean bool = this.mUmsEnabling;
            return bool;
        }
    }

    private boolean isUidOwnerOfPackageOrSystem(String paramString, int paramInt)
    {
        boolean bool = true;
        if (paramInt == 1000);
        while (true)
        {
            return bool;
            if (paramString == null)
                bool = false;
            else if (paramInt != this.mPms.getPackageUid(paramString, UserId.getUserId(paramInt)))
                bool = false;
        }
    }

    private void notifyShareAvailabilityChange(boolean paramBoolean)
    {
        while (true)
        {
            int i;
            MountServiceBinderListener localMountServiceBinderListener;
            synchronized (this.mListeners)
            {
                this.mUmsAvailable = paramBoolean;
                i = -1 + this.mListeners.size();
                if (i < 0)
                    break label105;
                localMountServiceBinderListener = (MountServiceBinderListener)this.mListeners.get(i);
            }
            try
            {
                localMountServiceBinderListener.mListener.onUsbMassStorageConnectionChanged(paramBoolean);
                i--;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                {
                    Slog.e("MountService", "Listener dead");
                    this.mListeners.remove(i);
                }
                localObject = finally;
                throw localObject;
            }
            catch (Exception localException)
            {
                while (true)
                    Slog.e("MountService", "Listener failed", localException);
            }
        }
        label105: if (this.mBooted == true)
            sendUmsIntent(paramBoolean);
        while (true)
        {
            final String str = Environment.getExternalStorageDirectory().getPath();
            if ((!paramBoolean) && (getVolumeState(str).equals("shared")))
                new Thread()
                {
                    public void run()
                    {
                        try
                        {
                            Slog.w("MountService", "Disabling UMS after cable disconnect");
                            MountService.this.doShareUnshareVolume(str, "ums", false);
                            int i = MountService.this.doMountVolume(str);
                            if (i != 0)
                            {
                                Object[] arrayOfObject = new Object[2];
                                arrayOfObject[0] = str;
                                arrayOfObject[1] = Integer.valueOf(i);
                                Slog.e("MountService", String.format("Failed to remount {%s} on UMS enabled-disconnect (%d)", arrayOfObject));
                            }
                            return;
                        }
                        catch (Exception localException)
                        {
                            while (true)
                                Slog.w("MountService", "Failed to mount media on UMS enabled-disconnect", localException);
                        }
                    }
                }
                .start();
            return;
            this.mSendUmsConnectedOnBoot = paramBoolean;
        }
    }

    private void notifyVolumeStateChange(String paramString1, String paramString2, int paramInt1, int paramInt2)
    {
        String str1 = getVolumeState(paramString2);
        String str2 = null;
        if ((paramInt1 == 7) && (paramInt2 != paramInt1))
            sendStorageIntent("android.intent.action.MEDIA_UNSHARED", paramString2);
        if (paramInt2 == -1);
        while (true)
        {
            if (str2 != null)
                sendStorageIntent(str2, paramString2);
            while (true)
            {
                return;
                if (paramInt2 == 0)
                    break label243;
                if (paramInt2 == 1)
                {
                    if ((str1.equals("bad_removal")) || (str1.equals("nofs")) || (str1.equals("unmountable")) || (getUmsEnabling()))
                        break;
                    updatePublicVolumeState(paramString2, "unmounted");
                    str2 = "android.intent.action.MEDIA_UNMOUNTED";
                    break;
                }
                if (paramInt2 == 2)
                    break;
                if (paramInt2 == 3)
                {
                    updatePublicVolumeState(paramString2, "checking");
                    str2 = "android.intent.action.MEDIA_CHECKING";
                    break;
                }
                if (paramInt2 == 4)
                {
                    updatePublicVolumeState(paramString2, "mounted");
                    str2 = "android.intent.action.MEDIA_MOUNTED";
                    break;
                }
                if (paramInt2 == 5)
                {
                    str2 = "android.intent.action.MEDIA_EJECT";
                    break;
                }
                if (paramInt2 == 6)
                    break;
                if (paramInt2 == 7)
                {
                    updatePublicVolumeState(paramString2, "unmounted");
                    sendStorageIntent("android.intent.action.MEDIA_UNMOUNTED", paramString2);
                    updatePublicVolumeState(paramString2, "shared");
                    str2 = "android.intent.action.MEDIA_SHARED";
                    break;
                }
                if (paramInt2 != 8)
                    break label245;
                Slog.e("MountService", "Live shared mounts not supported yet!");
            }
            label243: continue;
            label245: Slog.e("MountService", "Unhandled VolumeState {" + paramInt2 + "}");
        }
    }

    // ERROR //
    private void readStorageList()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 218	com/android/server/MountService:mContext	Landroid/content/Context;
        //     4: invokevirtual 679	android/content/Context:getResources	()Landroid/content/res/Resources;
        //     7: astore_1
        //     8: aload_1
        //     9: ldc_w 680
        //     12: invokevirtual 686	android/content/res/Resources:getXml	(I)Landroid/content/res/XmlResourceParser;
        //     15: astore_2
        //     16: aload_2
        //     17: invokestatic 692	android/util/Xml:asAttributeSet	(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;
        //     20: astore_3
        //     21: aload_2
        //     22: ldc 98
        //     24: invokestatic 698	com/android/internal/util/XmlUtils:beginDocument	(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V
        //     27: aload_2
        //     28: invokestatic 702	com/android/internal/util/XmlUtils:nextElement	(Lorg/xmlpull/v1/XmlPullParser;)V
        //     31: aload_2
        //     32: invokeinterface 707 1 0
        //     37: astore 11
        //     39: aload 11
        //     41: ifnonnull +45 -> 86
        //     44: aload_0
        //     45: getfield 171	com/android/server/MountService:mVolumes	Ljava/util/ArrayList;
        //     48: invokevirtual 611	java/util/ArrayList:size	()I
        //     51: istore 30
        //     53: iconst_0
        //     54: istore 31
        //     56: iload 31
        //     58: iload 30
        //     60: if_icmpge +460 -> 520
        //     63: aload_0
        //     64: getfield 171	com/android/server/MountService:mVolumes	Ljava/util/ArrayList;
        //     67: iload 31
        //     69: invokevirtual 614	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     72: checkcast 225	android/os/storage/StorageVolume
        //     75: iload 31
        //     77: invokevirtual 710	android/os/storage/StorageVolume:setStorageId	(I)V
        //     80: iinc 31 1
        //     83: goto -27 -> 56
        //     86: ldc 95
        //     88: aload 11
        //     90: invokevirtual 460	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     93: ifeq -66 -> 27
        //     96: aload_1
        //     97: aload_3
        //     98: getstatic 716	com/android/internal/R$styleable:Storage	[I
        //     101: invokevirtual 720	android/content/res/Resources:obtainAttributes	(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
        //     104: astore 12
        //     106: aload 12
        //     108: iconst_0
        //     109: invokevirtual 726	android/content/res/TypedArray:getText	(I)Ljava/lang/CharSequence;
        //     112: astore 13
        //     114: aload 12
        //     116: iconst_1
        //     117: bipush 255
        //     119: invokevirtual 730	android/content/res/TypedArray:getResourceId	(II)I
        //     122: istore 14
        //     124: aload 12
        //     126: iconst_1
        //     127: invokevirtual 726	android/content/res/TypedArray:getText	(I)Ljava/lang/CharSequence;
        //     130: astore 15
        //     132: aload 12
        //     134: iconst_2
        //     135: iconst_0
        //     136: invokevirtual 734	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     139: istore 16
        //     141: aload 12
        //     143: iconst_3
        //     144: iconst_0
        //     145: invokevirtual 734	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     148: istore 17
        //     150: aload 12
        //     152: iconst_4
        //     153: iconst_0
        //     154: invokevirtual 734	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     157: istore 18
        //     159: aload 12
        //     161: iconst_5
        //     162: iconst_0
        //     163: invokevirtual 737	android/content/res/TypedArray:getInt	(II)I
        //     166: istore 19
        //     168: aload 12
        //     170: bipush 6
        //     172: iconst_0
        //     173: invokevirtual 734	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     176: istore 20
        //     178: ldc2_w 738
        //     181: ldc2_w 738
        //     184: aload 12
        //     186: bipush 7
        //     188: iconst_0
        //     189: invokevirtual 737	android/content/res/TypedArray:getInt	(II)I
        //     192: i2l
        //     193: lmul
        //     194: lmul
        //     195: lstore 21
        //     197: ldc 92
        //     199: new 508	java/lang/StringBuilder
        //     202: dup
        //     203: invokespecial 509	java/lang/StringBuilder:<init>	()V
        //     206: ldc_w 741
        //     209: invokevirtual 515	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     212: aload 13
        //     214: invokevirtual 744	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     217: ldc_w 746
        //     220: invokevirtual 515	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     223: aload 15
        //     225: invokevirtual 744	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     228: ldc_w 748
        //     231: invokevirtual 515	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     234: iload 16
        //     236: invokevirtual 751	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     239: ldc_w 753
        //     242: invokevirtual 515	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     245: iload 17
        //     247: invokevirtual 751	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     250: ldc_w 755
        //     253: invokevirtual 515	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     256: iload 18
        //     258: invokevirtual 751	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     261: ldc_w 757
        //     264: invokevirtual 515	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     267: iload 19
        //     269: invokevirtual 669	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     272: ldc_w 759
        //     275: invokevirtual 515	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     278: iload 20
        //     280: invokevirtual 751	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     283: ldc_w 761
        //     286: invokevirtual 515	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     289: lload 21
        //     291: invokevirtual 764	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //     294: invokevirtual 520	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     297: invokestatic 243	android/util/Slog:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     300: pop
        //     301: aload 13
        //     303: ifnull +8 -> 311
        //     306: aload 15
        //     308: ifnonnull +80 -> 388
        //     311: ldc 92
        //     313: ldc_w 766
        //     316: invokestatic 523	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     319: pop
        //     320: aload 12
        //     322: invokevirtual 769	android/content/res/TypedArray:recycle	()V
        //     325: goto -298 -> 27
        //     328: astore 9
        //     330: new 771	java/lang/RuntimeException
        //     333: dup
        //     334: aload 9
        //     336: invokespecial 774	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
        //     339: astore 10
        //     341: aload 10
        //     343: athrow
        //     344: astore 6
        //     346: aload_0
        //     347: getfield 171	com/android/server/MountService:mVolumes	Ljava/util/ArrayList;
        //     350: invokevirtual 611	java/util/ArrayList:size	()I
        //     353: istore 7
        //     355: iconst_0
        //     356: istore 8
        //     358: iload 8
        //     360: iload 7
        //     362: if_icmpge +149 -> 511
        //     365: aload_0
        //     366: getfield 171	com/android/server/MountService:mVolumes	Ljava/util/ArrayList;
        //     369: iload 8
        //     371: invokevirtual 614	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     374: checkcast 225	android/os/storage/StorageVolume
        //     377: iload 8
        //     379: invokevirtual 710	android/os/storage/StorageVolume:setStorageId	(I)V
        //     382: iinc 8 1
        //     385: goto -27 -> 358
        //     388: aload 13
        //     390: invokevirtual 775	java/lang/Object:toString	()Ljava/lang/String;
        //     393: astore 25
        //     395: new 225	android/os/storage/StorageVolume
        //     398: dup
        //     399: aload 25
        //     401: iload 14
        //     403: iload 17
        //     405: iload 18
        //     407: iload 19
        //     409: iload 20
        //     411: lload 21
        //     413: invokespecial 778	android/os/storage/StorageVolume:<init>	(Ljava/lang/String;IZZIZJ)V
        //     416: astore 26
        //     418: iload 16
        //     420: ifeq +16 -> 436
        //     423: aload_0
        //     424: getfield 223	com/android/server/MountService:mPrimaryVolume	Landroid/os/storage/StorageVolume;
        //     427: ifnonnull +59 -> 486
        //     430: aload_0
        //     431: aload 26
        //     433: putfield 223	com/android/server/MountService:mPrimaryVolume	Landroid/os/storage/StorageVolume;
        //     436: aload_0
        //     437: getfield 223	com/android/server/MountService:mPrimaryVolume	Landroid/os/storage/StorageVolume;
        //     440: aload 26
        //     442: if_acmpne +56 -> 498
        //     445: aload_0
        //     446: getfield 171	com/android/server/MountService:mVolumes	Ljava/util/ArrayList;
        //     449: iconst_0
        //     450: aload 26
        //     452: invokevirtual 781	java/util/ArrayList:add	(ILjava/lang/Object;)V
        //     455: aload_0
        //     456: getfield 178	com/android/server/MountService:mVolumeMap	Ljava/util/HashMap;
        //     459: aload 25
        //     461: aload 26
        //     463: invokevirtual 249	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     466: pop
        //     467: goto -147 -> 320
        //     470: astore 4
        //     472: new 771	java/lang/RuntimeException
        //     475: dup
        //     476: aload 4
        //     478: invokespecial 774	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
        //     481: astore 5
        //     483: aload 5
        //     485: athrow
        //     486: ldc 92
        //     488: ldc_w 783
        //     491: invokestatic 523	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     494: pop
        //     495: goto -59 -> 436
        //     498: aload_0
        //     499: getfield 171	com/android/server/MountService:mVolumes	Ljava/util/ArrayList;
        //     502: aload 26
        //     504: invokevirtual 784	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     507: pop
        //     508: goto -53 -> 455
        //     511: aload_2
        //     512: invokeinterface 787 1 0
        //     517: aload 6
        //     519: athrow
        //     520: aload_2
        //     521: invokeinterface 787 1 0
        //     526: return
        //
        // Exception table:
        //     from	to	target	type
        //     21	39	328	org/xmlpull/v1/XmlPullParserException
        //     86	325	328	org/xmlpull/v1/XmlPullParserException
        //     388	467	328	org/xmlpull/v1/XmlPullParserException
        //     486	508	328	org/xmlpull/v1/XmlPullParserException
        //     21	39	344	finally
        //     86	325	344	finally
        //     330	344	344	finally
        //     388	467	344	finally
        //     472	486	344	finally
        //     486	508	344	finally
        //     21	39	470	java/io/IOException
        //     86	325	470	java/io/IOException
        //     388	467	470	java/io/IOException
        //     486	508	470	java/io/IOException
    }

    private void removeObbStateLocked(ObbState paramObbState)
    {
        IBinder localIBinder = paramObbState.getBinder();
        List localList = (List)this.mObbMounts.get(localIBinder);
        if (localList != null)
        {
            if (localList.remove(paramObbState))
                paramObbState.unlink();
            if (localList.isEmpty())
                this.mObbMounts.remove(localIBinder);
        }
        this.mObbPathToStateMap.remove(paramObbState.filename);
    }

    private void sendStorageIntent(String paramString1, String paramString2)
    {
        Intent localIntent = new Intent(paramString1, Uri.parse("file://" + paramString2));
        localIntent.putExtra("storage_volume", (Parcelable)this.mVolumeMap.get(paramString2));
        Slog.d("MountService", "sendStorageIntent " + localIntent);
        this.mContext.sendBroadcast(localIntent);
    }

    private void sendUmsIntent(boolean paramBoolean)
    {
        Context localContext = this.mContext;
        if (paramBoolean);
        for (String str = "android.intent.action.UMS_CONNECTED"; ; str = "android.intent.action.UMS_DISCONNECTED")
        {
            localContext.sendBroadcast(new Intent(str));
            return;
        }
    }

    private void setUmsEnabling(boolean paramBoolean)
    {
        synchronized (this.mListeners)
        {
            this.mUmsEnabling = paramBoolean;
            return;
        }
    }

    private void updatePublicVolumeState(String paramString1, String paramString2)
    {
        label311: 
        while (true)
        {
            String str;
            synchronized (this.mVolumeStates)
            {
                str = (String)this.mVolumeStates.put(paramString1, paramString2);
                if (paramString2.equals(str))
                {
                    Object[] arrayOfObject = new Object[3];
                    arrayOfObject[0] = paramString2;
                    arrayOfObject[1] = paramString2;
                    arrayOfObject[2] = paramString1;
                    Slog.w("MountService", String.format("Duplicate state transition (%s -> %s) for %s", arrayOfObject));
                    return;
                }
            }
            Slog.d("MountService", "volume state changed for " + paramString1 + " (" + str + " -> " + paramString2 + ")");
            if ((paramString1.equals(this.mExternalStoragePath)) && (!this.mEmulateExternalStorage))
            {
                if (!"unmounted".equals(paramString2))
                    break label242;
                this.mPms.updateExternalMediaStatus(false, false);
                this.mObbActionHandler.sendMessage(this.mObbActionHandler.obtainMessage(5, paramString1));
            }
            while (true)
            {
                int i;
                MountServiceBinderListener localMountServiceBinderListener;
                synchronized (this.mListeners)
                {
                    i = -1 + this.mListeners.size();
                    if (i < 0)
                        break label311;
                    localMountServiceBinderListener = (MountServiceBinderListener)this.mListeners.get(i);
                }
                try
                {
                    localMountServiceBinderListener.mListener.onStorageStateChanged(paramString1, str, paramString2);
                    i--;
                    continue;
                    label242: if ("mounted".equals(paramString2))
                        this.mPms.updateExternalMediaStatus(true, false);
                }
                catch (RemoteException localRemoteException)
                {
                    while (true)
                    {
                        Slog.e("MountService", "Listener dead");
                        this.mListeners.remove(i);
                    }
                    localObject2 = finally;
                    throw localObject2;
                }
                catch (Exception localException)
                {
                    while (true)
                        Slog.e("MountService", "Listener failed", localException);
                }
            }
        }
    }

    private void validatePermission(String paramString)
    {
        if (this.mContext.checkCallingOrSelfPermission(paramString) != 0)
        {
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = paramString;
            throw new SecurityException(String.format("Requires %s permission", arrayOfObject));
        }
    }

    private void waitForLatch(CountDownLatch paramCountDownLatch)
    {
        if (paramCountDownLatch == null);
        while (true)
        {
            return;
            try
            {
                boolean bool;
                do
                {
                    Slog.w("MountService", "Thread " + Thread.currentThread().getName() + " still waiting for MountService ready...");
                    bool = paramCountDownLatch.await(5000L, TimeUnit.MILLISECONDS);
                }
                while (!bool);
            }
            catch (InterruptedException localInterruptedException)
            {
                while (true)
                    Slog.w("MountService", "Interrupt while waiting for MountService to be ready.");
            }
        }
    }

    private void waitForReady()
    {
        waitForLatch(this.mConnectedSignal);
    }

    private void warnOnNotMounted()
    {
        if (!Environment.getExternalStorageState().equals("mounted"))
            Slog.w("MountService", "getSecureContainerList() called when storage not mounted");
    }

    public int changeEncryptionPassword(String paramString)
    {
        if (TextUtils.isEmpty(paramString))
            throw new IllegalArgumentException("password cannot be empty");
        this.mContext.enforceCallingOrSelfPermission("android.permission.CRYPT_KEEPER", "no permission to access the crypt keeper");
        waitForReady();
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = "changepw";
            arrayOfObject[1] = paramString;
            int j = Integer.parseInt(localNativeDaemonConnector.execute("cryptfs", arrayOfObject).getMessage());
            i = j;
            return i;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            while (true)
                int i = localNativeDaemonConnectorException.getCode();
        }
    }

    public int createSecureContainer(String paramString1, int paramInt1, String paramString2, String paramString3, int paramInt2, boolean paramBoolean)
    {
        validatePermission("android.permission.ASEC_CREATE");
        waitForReady();
        warnOnNotMounted();
        int i = 0;
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[7];
            arrayOfObject[0] = "create";
            arrayOfObject[1] = paramString1;
            arrayOfObject[2] = Integer.valueOf(paramInt1);
            arrayOfObject[3] = paramString2;
            arrayOfObject[4] = paramString3;
            arrayOfObject[5] = Integer.valueOf(paramInt2);
            if (paramBoolean)
            {
                str = "1";
                arrayOfObject[6] = str;
                localNativeDaemonConnector.execute("asec", arrayOfObject);
                if (i != 0);
            }
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            synchronized (this.mAsecMountSet)
            {
                while (true)
                {
                    this.mAsecMountSet.add(paramString1);
                    return i;
                    String str = "0";
                }
                localNativeDaemonConnectorException = localNativeDaemonConnectorException;
                i = -1;
            }
        }
    }

    public int decryptStorage(String paramString)
    {
        if (TextUtils.isEmpty(paramString))
            throw new IllegalArgumentException("password cannot be empty");
        this.mContext.enforceCallingOrSelfPermission("android.permission.CRYPT_KEEPER", "no permission to access the crypt keeper");
        waitForReady();
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = "checkpw";
            arrayOfObject[1] = paramString;
            i = Integer.parseInt(localNativeDaemonConnector.execute("cryptfs", arrayOfObject).getMessage());
            if (i == 0)
                this.mHandler.postDelayed(new Runnable()
                {
                    public void run()
                    {
                        try
                        {
                            NativeDaemonConnector localNativeDaemonConnector = MountService.this.mConnector;
                            Object[] arrayOfObject = new Object[1];
                            arrayOfObject[0] = "restart";
                            localNativeDaemonConnector.execute("cryptfs", arrayOfObject);
                            return;
                        }
                        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
                        {
                            while (true)
                                Slog.e("MountService", "problem executing in background", localNativeDaemonConnectorException);
                        }
                    }
                }
                , 1000L);
            return i;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            while (true)
                int i = localNativeDaemonConnectorException.getCode();
        }
    }

    public int destroySecureContainer(String paramString, boolean paramBoolean)
    {
        validatePermission("android.permission.ASEC_DESTROY");
        waitForReady();
        warnOnNotMounted();
        Runtime.getRuntime().gc();
        int i = 0;
        try
        {
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = "destroy";
            arrayOfObject[1] = paramString;
            NativeDaemonConnector.Command localCommand = new NativeDaemonConnector.Command("asec", arrayOfObject);
            if (paramBoolean)
                localCommand.appendArg("force");
            this.mConnector.execute(localCommand);
            if (i != 0);
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            synchronized (this.mAsecMountSet)
            {
                while (true)
                {
                    if (this.mAsecMountSet.contains(paramString))
                        this.mAsecMountSet.remove(paramString);
                    return i;
                    localNativeDaemonConnectorException = localNativeDaemonConnectorException;
                    if (localNativeDaemonConnectorException.getCode() != 405)
                        break;
                    i = -7;
                }
                i = -1;
            }
        }
    }

    // ERROR //
    protected void dump(java.io.FileDescriptor paramFileDescriptor, java.io.PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 218	com/android/server/MountService:mContext	Landroid/content/Context;
        //     4: ldc_w 961
        //     7: invokevirtual 853	android/content/Context:checkCallingOrSelfPermission	(Ljava/lang/String;)I
        //     10: ifeq +54 -> 64
        //     13: aload_2
        //     14: new 508	java/lang/StringBuilder
        //     17: dup
        //     18: invokespecial 509	java/lang/StringBuilder:<init>	()V
        //     21: ldc_w 963
        //     24: invokevirtual 515	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     27: invokestatic 968	android/os/Binder:getCallingPid	()I
        //     30: invokevirtual 669	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     33: ldc_w 970
        //     36: invokevirtual 515	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     39: invokestatic 973	android/os/Binder:getCallingUid	()I
        //     42: invokevirtual 669	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     45: ldc_w 975
        //     48: invokevirtual 515	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     51: ldc_w 961
        //     54: invokevirtual 515	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     57: invokevirtual 520	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     60: invokevirtual 980	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     63: return
        //     64: aload_0
        //     65: getfield 204	com/android/server/MountService:mObbMounts	Ljava/util/Map;
        //     68: astore 4
        //     70: aload 4
        //     72: monitorenter
        //     73: aload_2
        //     74: ldc_w 982
        //     77: invokevirtual 980	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     80: aload_0
        //     81: getfield 204	com/android/server/MountService:mObbMounts	Ljava/util/Map;
        //     84: invokeinterface 986 1 0
        //     89: invokeinterface 989 1 0
        //     94: astore 6
        //     96: aload 6
        //     98: invokeinterface 451 1 0
        //     103: ifeq +105 -> 208
        //     106: aload 6
        //     108: invokeinterface 455 1 0
        //     113: checkcast 991	java/util/Map$Entry
        //     116: astore 14
        //     118: aload_2
        //     119: ldc_w 993
        //     122: invokevirtual 996	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     125: aload_2
        //     126: aload 14
        //     128: invokeinterface 999 1 0
        //     133: checkcast 1001	android/os/IBinder
        //     136: invokevirtual 775	java/lang/Object:toString	()Ljava/lang/String;
        //     139: invokevirtual 980	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     142: aload 14
        //     144: invokeinterface 1004 1 0
        //     149: checkcast 431	java/util/List
        //     152: invokeinterface 446 1 0
        //     157: astore 15
        //     159: aload 15
        //     161: invokeinterface 451 1 0
        //     166: ifeq -70 -> 96
        //     169: aload 15
        //     171: invokeinterface 455 1 0
        //     176: checkcast 50	com/android/server/MountService$ObbState
        //     179: astore 16
        //     181: aload_2
        //     182: ldc_w 1006
        //     185: invokevirtual 996	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     188: aload_2
        //     189: aload 16
        //     191: invokevirtual 1007	com/android/server/MountService$ObbState:toString	()Ljava/lang/String;
        //     194: invokevirtual 980	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     197: goto -38 -> 159
        //     200: astore 5
        //     202: aload 4
        //     204: monitorexit
        //     205: aload 5
        //     207: athrow
        //     208: aload_2
        //     209: ldc_w 1009
        //     212: invokevirtual 980	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     215: aload_2
        //     216: ldc_w 1011
        //     219: invokevirtual 980	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     222: aload_0
        //     223: getfield 206	com/android/server/MountService:mObbPathToStateMap	Ljava/util/Map;
        //     226: invokeinterface 986 1 0
        //     231: invokeinterface 989 1 0
        //     236: astore 7
        //     238: aload 7
        //     240: invokeinterface 451 1 0
        //     245: ifeq +63 -> 308
        //     248: aload 7
        //     250: invokeinterface 455 1 0
        //     255: checkcast 991	java/util/Map$Entry
        //     258: astore 13
        //     260: aload_2
        //     261: ldc_w 1013
        //     264: invokevirtual 996	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     267: aload_2
        //     268: aload 13
        //     270: invokeinterface 999 1 0
        //     275: checkcast 457	java/lang/String
        //     278: invokevirtual 996	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     281: aload_2
        //     282: ldc_w 835
        //     285: invokevirtual 996	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     288: aload_2
        //     289: aload 13
        //     291: invokeinterface 1004 1 0
        //     296: checkcast 50	com/android/server/MountService$ObbState
        //     299: invokevirtual 1007	com/android/server/MountService$ObbState:toString	()Ljava/lang/String;
        //     302: invokevirtual 980	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     305: goto -67 -> 238
        //     308: aload 4
        //     310: monitorexit
        //     311: aload_2
        //     312: ldc_w 1009
        //     315: invokevirtual 980	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     318: aload_0
        //     319: getfield 171	com/android/server/MountService:mVolumes	Ljava/util/ArrayList;
        //     322: astore 8
        //     324: aload 8
        //     326: monitorenter
        //     327: aload_2
        //     328: ldc_w 1015
        //     331: invokevirtual 980	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     334: aload_0
        //     335: getfield 171	com/android/server/MountService:mVolumes	Ljava/util/ArrayList;
        //     338: invokevirtual 611	java/util/ArrayList:size	()I
        //     341: istore 10
        //     343: iconst_0
        //     344: istore 11
        //     346: iload 11
        //     348: iload 10
        //     350: if_icmpge +39 -> 389
        //     353: aload_0
        //     354: getfield 171	com/android/server/MountService:mVolumes	Ljava/util/ArrayList;
        //     357: iload 11
        //     359: invokevirtual 614	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     362: checkcast 225	android/os/storage/StorageVolume
        //     365: astore 12
        //     367: aload_2
        //     368: ldc_w 1013
        //     371: invokevirtual 996	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     374: aload_2
        //     375: aload 12
        //     377: invokevirtual 1016	android/os/storage/StorageVolume:toString	()Ljava/lang/String;
        //     380: invokevirtual 980	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     383: iinc 11 1
        //     386: goto -40 -> 346
        //     389: aload 8
        //     391: monitorexit
        //     392: aload_2
        //     393: invokevirtual 1018	java/io/PrintWriter:println	()V
        //     396: aload_2
        //     397: ldc_w 1020
        //     400: invokevirtual 980	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     403: aload_0
        //     404: getfield 311	com/android/server/MountService:mConnector	Lcom/android/server/NativeDaemonConnector;
        //     407: aload_1
        //     408: aload_2
        //     409: aload_3
        //     410: invokevirtual 1022	com/android/server/NativeDaemonConnector:dump	(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
        //     413: goto -350 -> 63
        //     416: astore 9
        //     418: aload 8
        //     420: monitorexit
        //     421: aload 9
        //     423: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     73	205	200	finally
        //     208	311	200	finally
        //     327	392	416	finally
        //     418	421	416	finally
    }

    public int encryptStorage(String paramString)
    {
        int i = 0;
        if (TextUtils.isEmpty(paramString))
            throw new IllegalArgumentException("password cannot be empty");
        this.mContext.enforceCallingOrSelfPermission("android.permission.CRYPT_KEEPER", "no permission to access the crypt keeper");
        waitForReady();
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[3];
            arrayOfObject[0] = "enablecrypto";
            arrayOfObject[1] = "inplace";
            arrayOfObject[2] = paramString;
            localNativeDaemonConnector.execute("cryptfs", arrayOfObject);
            return i;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            while (true)
                i = localNativeDaemonConnectorException.getCode();
        }
    }

    public int finalizeSecureContainer(String paramString)
    {
        validatePermission("android.permission.ASEC_CREATE");
        warnOnNotMounted();
        int i = 0;
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = "finalize";
            arrayOfObject[1] = paramString;
            localNativeDaemonConnector.execute("asec", arrayOfObject);
            return i;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            while (true)
                i = -1;
        }
    }

    public void finishMediaUpdate()
    {
        this.mHandler.sendEmptyMessage(2);
    }

    public int fixPermissionsSecureContainer(String paramString1, int paramInt, String paramString2)
    {
        validatePermission("android.permission.ASEC_CREATE");
        warnOnNotMounted();
        int i = 0;
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[4];
            arrayOfObject[0] = "fixperms";
            arrayOfObject[1] = paramString1;
            arrayOfObject[2] = Integer.valueOf(paramInt);
            arrayOfObject[3] = paramString2;
            localNativeDaemonConnector.execute("asec", arrayOfObject);
            return i;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            while (true)
                i = -1;
        }
    }

    public int formatVolume(String paramString)
    {
        validatePermission("android.permission.MOUNT_FORMAT_FILESYSTEMS");
        waitForReady();
        return doFormatVolume(paramString);
    }

    public int getEncryptionState()
    {
        int i = -1;
        this.mContext.enforceCallingOrSelfPermission("android.permission.CRYPT_KEEPER", "no permission to access the crypt keeper");
        waitForReady();
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = "cryptocomplete";
            int j = Integer.parseInt(localNativeDaemonConnector.execute("cryptfs", arrayOfObject).getMessage());
            i = j;
            return i;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            while (true)
                Slog.w("MountService", "Unable to parse result from cryptfs cryptocomplete");
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            while (true)
                Slog.w("MountService", "Error in communicating with cryptfs in validating");
        }
    }

    public String getMountedObbPath(String paramString)
    {
        if (paramString == null)
            throw new IllegalArgumentException("filename cannot be null");
        waitForReady();
        warnOnNotMounted();
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject2 = new Object[2];
            arrayOfObject2[0] = "path";
            arrayOfObject2[1] = paramString;
            NativeDaemonEvent localNativeDaemonEvent = localNativeDaemonConnector.execute("obb", arrayOfObject2);
            localNativeDaemonEvent.checkCode(211);
            String str2 = localNativeDaemonEvent.getMessage();
            str1 = str2;
            return str1;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            int i;
            while (true)
            {
                i = localNativeDaemonConnectorException.getCode();
                if (i != 406)
                    break;
                String str1 = null;
            }
            Object[] arrayOfObject1 = new Object[1];
            arrayOfObject1[0] = Integer.valueOf(i);
            throw new IllegalStateException(String.format("Unexpected response code %d", arrayOfObject1));
        }
    }

    public String getSecureContainerFilesystemPath(String paramString)
    {
        validatePermission("android.permission.ASEC_ACCESS");
        waitForReady();
        warnOnNotMounted();
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject3 = new Object[2];
            arrayOfObject3[0] = "fspath";
            arrayOfObject3[1] = paramString;
            NativeDaemonEvent localNativeDaemonEvent = localNativeDaemonConnector.execute("asec", arrayOfObject3);
            localNativeDaemonEvent.checkCode(211);
            String str2 = localNativeDaemonEvent.getMessage();
            str1 = str2;
            return str1;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            int i;
            while (true)
            {
                i = localNativeDaemonConnectorException.getCode();
                if (i != 406)
                    break;
                Object[] arrayOfObject2 = new Object[1];
                arrayOfObject2[0] = paramString;
                Slog.i("MountService", String.format("Container '%s' not found", arrayOfObject2));
                String str1 = null;
            }
            Object[] arrayOfObject1 = new Object[1];
            arrayOfObject1[0] = Integer.valueOf(i);
            throw new IllegalStateException(String.format("Unexpected response code %d", arrayOfObject1));
        }
    }

    public String[] getSecureContainerList()
    {
        validatePermission("android.permission.ASEC_ACCESS");
        waitForReady();
        warnOnNotMounted();
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = "list";
            String[] arrayOfString2 = NativeDaemonEvent.filterMessageList(localNativeDaemonConnector.executeForList("asec", arrayOfObject), 111);
            arrayOfString1 = arrayOfString2;
            return arrayOfString1;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            while (true)
                String[] arrayOfString1 = new String[0];
        }
    }

    public String getSecureContainerPath(String paramString)
    {
        validatePermission("android.permission.ASEC_ACCESS");
        waitForReady();
        warnOnNotMounted();
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject3 = new Object[2];
            arrayOfObject3[0] = "path";
            arrayOfObject3[1] = paramString;
            NativeDaemonEvent localNativeDaemonEvent = localNativeDaemonConnector.execute("asec", arrayOfObject3);
            localNativeDaemonEvent.checkCode(211);
            String str2 = localNativeDaemonEvent.getMessage();
            str1 = str2;
            return str1;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            int i;
            while (true)
            {
                i = localNativeDaemonConnectorException.getCode();
                if (i != 406)
                    break;
                Object[] arrayOfObject2 = new Object[1];
                arrayOfObject2[0] = paramString;
                Slog.i("MountService", String.format("Container '%s' not found", arrayOfObject2));
                String str1 = null;
            }
            Object[] arrayOfObject1 = new Object[1];
            arrayOfObject1[0] = Integer.valueOf(i);
            throw new IllegalStateException(String.format("Unexpected response code %d", arrayOfObject1));
        }
    }

    public int[] getStorageUsers(String paramString)
    {
        validatePermission("android.permission.MOUNT_UNMOUNT_FILESYSTEMS");
        waitForReady();
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject1 = new Object[2];
            arrayOfObject1[0] = "users";
            arrayOfObject1[1] = paramString;
            String[] arrayOfString1 = NativeDaemonEvent.filterMessageList(localNativeDaemonConnector.executeForList("storage", arrayOfObject1), 112);
            arrayOfInt = new int[arrayOfString1.length];
            int i = 0;
            while (true)
                if (i < arrayOfString1.length)
                {
                    String[] arrayOfString2 = arrayOfString1[i].split(" ");
                    try
                    {
                        arrayOfInt[i] = Integer.parseInt(arrayOfString2[0]);
                        i++;
                    }
                    catch (NumberFormatException localNumberFormatException)
                    {
                        Object[] arrayOfObject2 = new Object[1];
                        arrayOfObject2[0] = arrayOfString2[0];
                        Slog.e("MountService", String.format("Error parsing pid %s", arrayOfObject2));
                        arrayOfInt = new int[0];
                    }
                }
            return arrayOfInt;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            while (true)
            {
                Slog.e("MountService", "Failed to retrieve storage users list", localNativeDaemonConnectorException);
                int[] arrayOfInt = new int[0];
            }
        }
    }

    public Parcelable[] getVolumeList()
    {
        synchronized (this.mVolumes)
        {
            int i = this.mVolumes.size();
            Parcelable[] arrayOfParcelable = new Parcelable[i];
            for (int j = 0; j < i; j++)
                arrayOfParcelable[j] = ((Parcelable)this.mVolumes.get(j));
            return arrayOfParcelable;
        }
    }

    public String getVolumeState(String paramString)
    {
        synchronized (this.mVolumeStates)
        {
            String str = (String)this.mVolumeStates.get(paramString);
            if (str == null)
            {
                Slog.w("MountService", "getVolumeState(" + paramString + "): Unknown volume");
                if (SystemProperties.get("vold.encrypt_progress").length() != 0)
                    str = "removed";
            }
            else
            {
                return str;
            }
            throw new IllegalArgumentException();
        }
    }

    public boolean isExternalStorageEmulated()
    {
        return this.mEmulateExternalStorage;
    }

    public boolean isObbMounted(String paramString)
    {
        if (paramString == null)
            throw new IllegalArgumentException("filename cannot be null");
        synchronized (this.mObbMounts)
        {
            boolean bool = this.mObbPathToStateMap.containsKey(paramString);
            return bool;
        }
    }

    public boolean isSecureContainerMounted(String paramString)
    {
        validatePermission("android.permission.ASEC_ACCESS");
        waitForReady();
        warnOnNotMounted();
        synchronized (this.mAsecMountSet)
        {
            boolean bool = this.mAsecMountSet.contains(paramString);
            return bool;
        }
    }

    public boolean isUsbMassStorageConnected()
    {
        waitForReady();
        boolean bool;
        if (getUmsEnabling())
            bool = true;
        while (true)
        {
            return bool;
            synchronized (this.mListeners)
            {
                bool = this.mUmsAvailable;
            }
        }
    }

    public boolean isUsbMassStorageEnabled()
    {
        waitForReady();
        return doGetVolumeShared(Environment.getExternalStorageDirectory().getPath(), "ums");
    }

    public void monitor()
    {
        if (this.mConnector != null)
            this.mConnector.monitor();
    }

    public void mountObb(String paramString1, String paramString2, IObbActionListener paramIObbActionListener, int paramInt)
        throws RemoteException
    {
        if (paramString1 == null)
            throw new IllegalArgumentException("filename cannot be null");
        if (paramIObbActionListener == null)
            throw new IllegalArgumentException("token cannot be null");
        MountObbAction localMountObbAction = new MountObbAction(new ObbState(paramString1, Binder.getCallingUid(), paramIObbActionListener, paramInt), paramString2);
        this.mObbActionHandler.sendMessage(this.mObbActionHandler.obtainMessage(1, localMountObbAction));
    }

    public int mountSecureContainer(String paramString1, String paramString2, int paramInt)
    {
        validatePermission("android.permission.ASEC_MOUNT_UNMOUNT");
        waitForReady();
        warnOnNotMounted();
        int i;
        synchronized (this.mAsecMountSet)
        {
            if (this.mAsecMountSet.contains(paramString1))
            {
                i = -6;
                break label164;
            }
            i = 0;
        }
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[4];
            arrayOfObject[0] = "mount";
            arrayOfObject[1] = paramString1;
            arrayOfObject[2] = paramString2;
            arrayOfObject[3] = Integer.valueOf(paramInt);
            localNativeDaemonConnector.execute("asec", arrayOfObject);
            if (i == 0)
            {
                synchronized (this.mAsecMountSet)
                {
                    this.mAsecMountSet.add(paramString1);
                }
                localObject1 = finally;
                throw localObject1;
            }
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            while (true)
                if (localNativeDaemonConnectorException.getCode() != 405)
                    i = -1;
        }
        label164: return i;
    }

    public int mountVolume(String paramString)
    {
        validatePermission("android.permission.MOUNT_UNMOUNT_FILESYSTEMS");
        waitForReady();
        return doMountVolume(paramString);
    }

    public void onDaemonConnected()
    {
        new Thread("MountService#onDaemonConnected")
        {
            public void run()
            {
                label261: 
                while (true)
                {
                    int j;
                    int k;
                    String str2;
                    try
                    {
                        NativeDaemonConnector localNativeDaemonConnector = MountService.this.mConnector;
                        Object[] arrayOfObject1 = new Object[1];
                        arrayOfObject1[0] = "list";
                        String[] arrayOfString1 = NativeDaemonEvent.filterMessageList(localNativeDaemonConnector.executeForList("volume", arrayOfObject1), 110);
                        int i = arrayOfString1.length;
                        j = 0;
                        if (j < i)
                        {
                            String[] arrayOfString2 = arrayOfString1[j].split(" ");
                            String str1 = arrayOfString2[1];
                            k = Integer.parseInt(arrayOfString2[2]);
                            if (k != 0)
                                break label261;
                            str2 = "removed";
                            if (str2 == null)
                                break label255;
                            MountService.this.updatePublicVolumeState(str1, str2);
                            break label255;
                            if (k != 4)
                                break label203;
                            str2 = "mounted";
                            Slog.i("MountService", "Media already mounted on daemon connection");
                            continue;
                        }
                    }
                    catch (Exception localException)
                    {
                        Slog.e("MountService", "Error processing initial volume state", localException);
                        MountService.this.updatePublicVolumeState(MountService.this.mExternalStoragePath, "removed");
                        MountService.this.mConnectedSignal.countDown();
                        MountService.access$1702(MountService.this, null);
                        MountService.this.mPms.scanAvailableAsecs();
                        MountService.this.mAsecsScanned.countDown();
                        MountService.access$1802(MountService.this, null);
                        return;
                    }
                    label203: if (k == 7)
                    {
                        str2 = "shared";
                        Slog.i("MountService", "Media shared on daemon connection");
                    }
                    else
                    {
                        Object[] arrayOfObject2 = new Object[1];
                        arrayOfObject2[0] = Integer.valueOf(k);
                        throw new Exception(String.format("Unexpected state %d", arrayOfObject2));
                        label255: j++;
                        continue;
                        if (k == 1)
                            str2 = "unmounted";
                    }
                }
            }
        }
        .start();
    }

    public boolean onEvent(int paramInt, String paramString, String[] paramArrayOfString)
    {
        if (paramInt == 605)
            notifyVolumeStateChange(paramArrayOfString[2], paramArrayOfString[3], Integer.parseInt(paramArrayOfString[7]), Integer.parseInt(paramArrayOfString[10]));
        for (boolean bool = true; ; bool = false)
            while (true)
            {
                return bool;
                if ((paramInt == 630) || (paramInt == 631) || (paramInt == 632))
                {
                    String str1 = null;
                    paramArrayOfString[2];
                    final String str2 = paramArrayOfString[3];
                    try
                    {
                        String[] arrayOfString = paramArrayOfString[6].substring(1, -1 + paramArrayOfString[6].length()).split(":");
                        Integer.parseInt(arrayOfString[0]);
                        Integer.parseInt(arrayOfString[1]);
                        if (paramInt == 630)
                        {
                            new Thread()
                            {
                                public void run()
                                {
                                    try
                                    {
                                        int i = MountService.this.doMountVolume(str2);
                                        if (i != 0)
                                        {
                                            Object[] arrayOfObject = new Object[1];
                                            arrayOfObject[0] = Integer.valueOf(i);
                                            Slog.w("MountService", String.format("Insertion mount failed (%d)", arrayOfObject));
                                        }
                                        return;
                                    }
                                    catch (Exception localException)
                                    {
                                        while (true)
                                            Slog.w("MountService", "Failed to mount media on insertion", localException);
                                    }
                                }
                            }
                            .start();
                            if (str1 == null)
                                break;
                            sendStorageIntent(str1, str2);
                        }
                    }
                    catch (Exception localException)
                    {
                        while (true)
                        {
                            Slog.e("MountService", "Failed to parse major/minor", localException);
                            continue;
                            if (paramInt == 631)
                            {
                                if (getVolumeState(str2).equals("bad_removal"))
                                {
                                    bool = true;
                                    break;
                                }
                                updatePublicVolumeState(str2, "unmounted");
                                sendStorageIntent("unmounted", str2);
                                updatePublicVolumeState(str2, "removed");
                                str1 = "android.intent.action.MEDIA_REMOVED";
                                continue;
                            }
                            if (paramInt == 632)
                            {
                                updatePublicVolumeState(str2, "unmounted");
                                updatePublicVolumeState(str2, "bad_removal");
                                str1 = "android.intent.action.MEDIA_BAD_REMOVAL";
                            }
                            else
                            {
                                Object[] arrayOfObject = new Object[1];
                                arrayOfObject[0] = Integer.valueOf(paramInt);
                                Slog.e("MountService", String.format("Unknown code {%d}", arrayOfObject));
                            }
                        }
                    }
                }
            }
    }

    public void registerListener(IMountServiceListener paramIMountServiceListener)
    {
        synchronized (this.mListeners)
        {
            MountServiceBinderListener localMountServiceBinderListener = new MountServiceBinderListener(paramIMountServiceListener);
            try
            {
                paramIMountServiceListener.asBinder().linkToDeath(localMountServiceBinderListener, 0);
                this.mListeners.add(localMountServiceBinderListener);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Slog.e("MountService", "Failed to link to listener death");
            }
        }
    }

    public int renameSecureContainer(String paramString1, String paramString2)
    {
        validatePermission("android.permission.ASEC_RENAME");
        waitForReady();
        warnOnNotMounted();
        int i;
        synchronized (this.mAsecMountSet)
        {
            if ((this.mAsecMountSet.contains(paramString1)) || (this.mAsecMountSet.contains(paramString2)))
            {
                i = -6;
            }
            else
            {
                i = 0;
                try
                {
                    NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
                    Object[] arrayOfObject = new Object[3];
                    arrayOfObject[0] = "rename";
                    arrayOfObject[1] = paramString1;
                    arrayOfObject[2] = paramString2;
                    localNativeDaemonConnector.execute("asec", arrayOfObject);
                }
                catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
                {
                    i = -1;
                }
            }
        }
        return i;
    }

    public void setUsbMassStorageEnabled(boolean paramBoolean)
    {
        waitForReady();
        validatePermission("android.permission.MOUNT_UNMOUNT_FILESYSTEMS");
        String str1 = Environment.getExternalStorageDirectory().getPath();
        String str2 = getVolumeState(str1);
        if ((paramBoolean) && (str2.equals("mounted")))
        {
            setUmsEnabling(paramBoolean);
            UmsEnableCallBack localUmsEnableCallBack = new UmsEnableCallBack(str1, "ums", true);
            this.mHandler.sendMessage(this.mHandler.obtainMessage(1, localUmsEnableCallBack));
            setUmsEnabling(false);
        }
        if (!paramBoolean)
        {
            doShareUnshareVolume(str1, "ums", paramBoolean);
            if (doMountVolume(str1) != 0)
                Slog.e("MountService", "Failed to remount " + str1 + " after disabling share method " + "ums");
        }
    }

    // ERROR //
    public void shutdown(IMountShutdownObserver paramIMountShutdownObserver)
    {
        // Byte code:
        //     0: aload_0
        //     1: ldc_w 1205
        //     4: invokespecial 922	com/android/server/MountService:validatePermission	(Ljava/lang/String;)V
        //     7: ldc 92
        //     9: ldc_w 1207
        //     12: invokestatic 1075	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     15: pop
        //     16: aload_0
        //     17: getfield 176	com/android/server/MountService:mVolumeStates	Ljava/util/HashMap;
        //     20: astore_3
        //     21: aload_3
        //     22: monitorenter
        //     23: aload_0
        //     24: getfield 176	com/android/server/MountService:mVolumeStates	Ljava/util/HashMap;
        //     27: invokevirtual 1210	java/util/HashMap:keySet	()Ljava/util/Set;
        //     30: invokeinterface 989 1 0
        //     35: astore 5
        //     37: aload 5
        //     39: invokeinterface 451 1 0
        //     44: ifeq +213 -> 257
        //     47: aload 5
        //     49: invokeinterface 455 1 0
        //     54: checkcast 457	java/lang/String
        //     57: astore 6
        //     59: aload_0
        //     60: getfield 176	com/android/server/MountService:mVolumeStates	Ljava/util/HashMap;
        //     63: aload 6
        //     65: invokevirtual 806	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     68: checkcast 457	java/lang/String
        //     71: astore 7
        //     73: aload 7
        //     75: ldc_w 494
        //     78: invokevirtual 460	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     81: ifeq +59 -> 140
        //     84: aload_0
        //     85: iconst_0
        //     86: invokevirtual 1212	com/android/server/MountService:setUsbMassStorageEnabled	(Z)V
        //     89: aload 7
        //     91: ldc 245
        //     93: invokevirtual 460	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     96: ifeq +133 -> 229
        //     99: new 38	com/android/server/MountService$ShutdownCallBack
        //     102: dup
        //     103: aload_0
        //     104: aload 6
        //     106: aload_1
        //     107: invokespecial 1215	com/android/server/MountService$ShutdownCallBack:<init>	(Lcom/android/server/MountService;Ljava/lang/String;Landroid/os/storage/IMountShutdownObserver;)V
        //     110: astore 12
        //     112: aload_0
        //     113: getfield 299	com/android/server/MountService:mHandler	Landroid/os/Handler;
        //     116: aload_0
        //     117: getfield 299	com/android/server/MountService:mHandler	Landroid/os/Handler;
        //     120: iconst_1
        //     121: aload 12
        //     123: invokevirtual 1196	android/os/Handler:obtainMessage	(ILjava/lang/Object;)Landroid/os/Message;
        //     126: invokevirtual 1197	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
        //     129: pop
        //     130: goto -93 -> 37
        //     133: astore 4
        //     135: aload_3
        //     136: monitorexit
        //     137: aload 4
        //     139: athrow
        //     140: aload 7
        //     142: ldc_w 654
        //     145: invokevirtual 460	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     148: ifeq -59 -> 89
        //     151: bipush 30
        //     153: istore 8
        //     155: aload 7
        //     157: ldc_w 654
        //     160: invokevirtual 460	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     163: istore 9
        //     165: iload 9
        //     167: ifeq +93 -> 260
        //     170: iload 8
        //     172: iconst_1
        //     173: isub
        //     174: istore 10
        //     176: iload 8
        //     178: iflt +34 -> 212
        //     181: ldc2_w 940
        //     184: invokestatic 1219	java/lang/Thread:sleep	(J)V
        //     187: invokestatic 890	android/os/Environment:getExternalStorageState	()Ljava/lang/String;
        //     190: astore 7
        //     192: iload 10
        //     194: istore 8
        //     196: goto -41 -> 155
        //     199: astore 16
        //     201: ldc 92
        //     203: ldc_w 1221
        //     206: aload 16
        //     208: invokestatic 555	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     211: pop
        //     212: iload 10
        //     214: ifne -125 -> 89
        //     217: ldc 92
        //     219: ldc_w 1223
        //     222: invokestatic 523	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     225: pop
        //     226: goto -137 -> 89
        //     229: aload_1
        //     230: ifnull -193 -> 37
        //     233: aload_1
        //     234: iconst_0
        //     235: invokeinterface 1228 2 0
        //     240: goto -203 -> 37
        //     243: astore 14
        //     245: ldc 92
        //     247: ldc_w 1230
        //     250: invokestatic 829	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     253: pop
        //     254: goto -217 -> 37
        //     257: aload_3
        //     258: monitorexit
        //     259: return
        //     260: iload 8
        //     262: istore 10
        //     264: goto -52 -> 212
        //
        // Exception table:
        //     from	to	target	type
        //     23	137	133	finally
        //     140	165	133	finally
        //     181	187	133	finally
        //     187	226	133	finally
        //     233	240	133	finally
        //     245	259	133	finally
        //     181	187	199	java/lang/InterruptedException
        //     233	240	243	android/os/RemoteException
    }

    public void unmountObb(String paramString, boolean paramBoolean, IObbActionListener paramIObbActionListener, int paramInt)
        throws RemoteException
    {
        if (paramString == null)
            throw new IllegalArgumentException("filename cannot be null");
        UnmountObbAction localUnmountObbAction = new UnmountObbAction(new ObbState(paramString, Binder.getCallingUid(), paramIObbActionListener, paramInt), paramBoolean);
        this.mObbActionHandler.sendMessage(this.mObbActionHandler.obtainMessage(1, localUnmountObbAction));
    }

    public int unmountSecureContainer(String paramString, boolean paramBoolean)
    {
        validatePermission("android.permission.ASEC_MOUNT_UNMOUNT");
        waitForReady();
        warnOnNotMounted();
        int i;
        synchronized (this.mAsecMountSet)
        {
            if (!this.mAsecMountSet.contains(paramString))
            {
                i = -5;
                break label179;
            }
            Runtime.getRuntime().gc();
            i = 0;
        }
        try
        {
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = "unmount";
            arrayOfObject[1] = paramString;
            NativeDaemonConnector.Command localCommand = new NativeDaemonConnector.Command("asec", arrayOfObject);
            if (paramBoolean)
                localCommand.appendArg("force");
            this.mConnector.execute(localCommand);
            if (i == 0)
            {
                synchronized (this.mAsecMountSet)
                {
                    this.mAsecMountSet.remove(paramString);
                }
                localObject1 = finally;
                throw localObject1;
            }
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            while (true)
                if (localNativeDaemonConnectorException.getCode() == 405)
                    i = -7;
                else
                    i = -1;
        }
        label179: return i;
    }

    public void unmountVolume(String paramString, boolean paramBoolean1, boolean paramBoolean2)
    {
        validatePermission("android.permission.MOUNT_UNMOUNT_FILESYSTEMS");
        waitForReady();
        String str = getVolumeState(paramString);
        if (("unmounted".equals(str)) || ("removed".equals(str)) || ("shared".equals(str)) || ("unmountable".equals(str)));
        while (true)
        {
            return;
            UnmountCallBack localUnmountCallBack = new UnmountCallBack(paramString, paramBoolean1, paramBoolean2);
            this.mHandler.sendMessage(this.mHandler.obtainMessage(1, localUnmountCallBack));
        }
    }

    public void unregisterListener(IMountServiceListener paramIMountServiceListener)
    {
        synchronized (this.mListeners)
        {
            Iterator localIterator = this.mListeners.iterator();
            while (localIterator.hasNext())
            {
                MountServiceBinderListener localMountServiceBinderListener = (MountServiceBinderListener)localIterator.next();
                if (localMountServiceBinderListener.mListener == paramIMountServiceListener)
                {
                    this.mListeners.remove(this.mListeners.indexOf(localMountServiceBinderListener));
                    return;
                }
            }
        }
    }

    public int verifyEncryptionPassword(String paramString)
        throws RemoteException
    {
        if (Binder.getCallingUid() != 1000)
            throw new SecurityException("no permission to access the crypt keeper");
        this.mContext.enforceCallingOrSelfPermission("android.permission.CRYPT_KEEPER", "no permission to access the crypt keeper");
        if (TextUtils.isEmpty(paramString))
            throw new IllegalArgumentException("password cannot be empty");
        waitForReady();
        try
        {
            NativeDaemonConnector localNativeDaemonConnector = this.mConnector;
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = "verifypw";
            arrayOfObject[1] = paramString;
            NativeDaemonEvent localNativeDaemonEvent = localNativeDaemonConnector.execute("cryptfs", arrayOfObject);
            Slog.i("MountService", "cryptfs verifypw => " + localNativeDaemonEvent.getMessage());
            int j = Integer.parseInt(localNativeDaemonEvent.getMessage());
            i = j;
            return i;
        }
        catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
        {
            while (true)
                int i = localNativeDaemonConnectorException.getCode();
        }
    }

    void waitForAsecScan()
    {
        waitForLatch(this.mAsecsScanned);
    }

    class UnmountObbAction extends MountService.ObbAction
    {
        private final boolean mForceUnmount;

        UnmountObbAction(MountService.ObbState paramBoolean, boolean arg3)
        {
            super(paramBoolean);
            boolean bool;
            this.mForceUnmount = bool;
        }

        public void handleError()
        {
            sendNewStatusOrIgnore(20);
        }

        public void handleExecute()
            throws IOException
        {
            MountService.this.waitForReady();
            MountService.this.warnOnNotMounted();
            ObbInfo localObbInfo = getObbInfo();
            while (true)
            {
                MountService.ObbState localObbState;
                synchronized (MountService.this.mObbMounts)
                {
                    localObbState = (MountService.ObbState)MountService.this.mObbPathToStateMap.get(localObbInfo.filename);
                    if (localObbState == null)
                    {
                        sendNewStatusOrIgnore(23);
                        return;
                    }
                }
                if (localObbState.callerUid != this.mObbState.callerUid)
                {
                    Slog.w("MountService", "Permission denied attempting to unmount OBB " + localObbInfo.filename + " (owned by " + localObbInfo.packageName + ")");
                    sendNewStatusOrIgnore(25);
                }
                else
                {
                    this.mObbState.filename = localObbInfo.filename;
                    int i = 0;
                    try
                    {
                        Object[] arrayOfObject = new Object[2];
                        arrayOfObject[0] = "unmount";
                        arrayOfObject[1] = this.mObbState.filename;
                        NativeDaemonConnector.Command localCommand = new NativeDaemonConnector.Command("obb", arrayOfObject);
                        if (this.mForceUnmount)
                            localCommand.appendArg("force");
                        MountService.this.mConnector.execute(localCommand);
                        if (i != 0);
                    }
                    catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
                    {
                        synchronized (MountService.this.mObbMounts)
                        {
                            while (true)
                            {
                                MountService.this.removeObbStateLocked(localObbState);
                                sendNewStatusOrIgnore(2);
                                break;
                                localNativeDaemonConnectorException = localNativeDaemonConnectorException;
                                int j = localNativeDaemonConnectorException.getCode();
                                if (j == 405)
                                {
                                    i = -7;
                                }
                                else
                                {
                                    if (j != 406)
                                        break label291;
                                    i = 0;
                                }
                            }
                            label291: i = -1;
                        }
                    }
                    Slog.w("MountService", "Could not mount OBB: " + this.mObbState.filename);
                    sendNewStatusOrIgnore(22);
                }
            }
        }

        public String toString()
        {
            StringBuilder localStringBuilder = new StringBuilder();
            localStringBuilder.append("UnmountObbAction{");
            localStringBuilder.append("filename=");
            String str1;
            String str2;
            if (this.mObbState.filename != null)
            {
                str1 = this.mObbState.filename;
                localStringBuilder.append(str1);
                localStringBuilder.append(",force=");
                localStringBuilder.append(this.mForceUnmount);
                localStringBuilder.append(",callerUid=");
                localStringBuilder.append(this.mObbState.callerUid);
                localStringBuilder.append(",token=");
                if (this.mObbState.token == null)
                    break label174;
                str2 = this.mObbState.token.toString();
                label112: localStringBuilder.append(str2);
                localStringBuilder.append(",binder=");
                if (this.mObbState.token == null)
                    break label181;
            }
            label174: label181: for (String str3 = this.mObbState.getBinder().toString(); ; str3 = "null")
            {
                localStringBuilder.append(str3);
                localStringBuilder.append('}');
                return localStringBuilder.toString();
                str1 = "null";
                break;
                str2 = "null";
                break label112;
            }
        }
    }

    class MountObbAction extends MountService.ObbAction
    {
        private final String mKey;

        MountObbAction(MountService.ObbState paramString, String arg3)
        {
            super(paramString);
            Object localObject;
            this.mKey = localObject;
        }

        public void handleError()
        {
            sendNewStatusOrIgnore(20);
        }

        public void handleExecute()
            throws IOException, RemoteException
        {
            MountService.this.waitForReady();
            MountService.this.warnOnNotMounted();
            ObbInfo localObbInfo = getObbInfo();
            if (!MountService.this.isUidOwnerOfPackageOrSystem(localObbInfo.packageName, this.mObbState.callerUid))
            {
                Slog.w("MountService", "Denied attempt to mount OBB " + localObbInfo.filename + " which is owned by " + localObbInfo.packageName);
                sendNewStatusOrIgnore(25);
            }
            while (true)
            {
                return;
                synchronized (MountService.this.mObbMounts)
                {
                    boolean bool = MountService.this.mObbPathToStateMap.containsKey(localObbInfo.filename);
                    if (bool)
                    {
                        Slog.w("MountService", "Attempt to mount OBB which is already mounted: " + localObbInfo.filename);
                        sendNewStatusOrIgnore(24);
                    }
                }
                this.mObbState.filename = localObbInfo.filename;
                Object localObject2;
                int i;
                if (this.mKey == null)
                {
                    localObject2 = "none";
                    i = 0;
                }
                try
                {
                    NativeDaemonConnector localNativeDaemonConnector = MountService.this.mConnector;
                    Object[] arrayOfObject = new Object[4];
                    arrayOfObject[0] = "mount";
                    arrayOfObject[1] = this.mObbState.filename;
                    arrayOfObject[2] = localObject2;
                    arrayOfObject[3] = Integer.valueOf(this.mObbState.callerUid);
                    localNativeDaemonConnector.execute("obb", arrayOfObject);
                    if (i != 0);
                }
                catch (NativeDaemonConnectorException localNativeDaemonConnectorException)
                {
                    synchronized (MountService.this.mObbMounts)
                    {
                        do
                        {
                            while (true)
                            {
                                MountService.this.addObbStateLocked(this.mObbState);
                                sendNewStatusOrIgnore(1);
                                break;
                                try
                                {
                                    String str = new BigInteger(SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1").generateSecret(new PBEKeySpec(this.mKey.toCharArray(), localObbInfo.salt, 1024, 128)).getEncoded()).toString(16);
                                    localObject2 = str;
                                }
                                catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
                                {
                                    Slog.e("MountService", "Could not load PBKDF2 algorithm", localNoSuchAlgorithmException);
                                    sendNewStatusOrIgnore(20);
                                    break;
                                }
                                catch (InvalidKeySpecException localInvalidKeySpecException)
                                {
                                    Slog.e("MountService", "Invalid key spec when loading PBKDF2 algorithm", localInvalidKeySpecException);
                                    sendNewStatusOrIgnore(20);
                                }
                            }
                            break;
                            localNativeDaemonConnectorException = localNativeDaemonConnectorException;
                        }
                        while (localNativeDaemonConnectorException.getCode() == 405);
                        i = -1;
                    }
                }
                Slog.e("MountService", "Couldn't mount OBB file: " + i);
                sendNewStatusOrIgnore(21);
            }
        }

        public String toString()
        {
            StringBuilder localStringBuilder = new StringBuilder();
            localStringBuilder.append("MountObbAction{");
            localStringBuilder.append("filename=");
            localStringBuilder.append(this.mObbState.filename);
            localStringBuilder.append(",callerUid=");
            localStringBuilder.append(this.mObbState.callerUid);
            localStringBuilder.append(",token=");
            String str1;
            if (this.mObbState.token != null)
            {
                str1 = this.mObbState.token.toString();
                localStringBuilder.append(str1);
                localStringBuilder.append(",binder=");
                if (this.mObbState.token == null)
                    break label144;
            }
            label144: for (String str2 = this.mObbState.getBinder().toString(); ; str2 = "null")
            {
                localStringBuilder.append(str2);
                localStringBuilder.append('}');
                return localStringBuilder.toString();
                str1 = "NULL";
                break;
            }
        }
    }

    abstract class ObbAction
    {
        private static final int MAX_RETRIES = 3;
        MountService.ObbState mObbState;
        private int mRetries;

        ObbAction(MountService.ObbState arg2)
        {
            Object localObject;
            this.mObbState = localObject;
        }

        public void execute(MountService.ObbActionHandler paramObbActionHandler)
        {
            try
            {
                this.mRetries = (1 + this.mRetries);
                if (this.mRetries > 3)
                {
                    Slog.w("MountService", "Failed to invoke remote methods on default container service. Giving up");
                    MountService.this.mObbActionHandler.sendEmptyMessage(3);
                    handleError();
                }
                else
                {
                    handleExecute();
                    MountService.this.mObbActionHandler.sendEmptyMessage(3);
                }
            }
            catch (RemoteException localRemoteException)
            {
                MountService.this.mObbActionHandler.sendEmptyMessage(4);
            }
            catch (Exception localException)
            {
                handleError();
                MountService.this.mObbActionHandler.sendEmptyMessage(3);
            }
        }

        protected ObbInfo getObbInfo()
            throws IOException
        {
            ObbInfo localObbInfo1;
            try
            {
                ObbInfo localObbInfo2 = MountService.this.mContainerService.getObbInfo(this.mObbState.filename);
                localObbInfo1 = localObbInfo2;
                if (localObbInfo1 == null)
                    throw new IOException("Couldn't read OBB file: " + this.mObbState.filename);
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                {
                    Slog.d("MountService", "Couldn't call DefaultContainerService to fetch OBB info for " + this.mObbState.filename);
                    localObbInfo1 = null;
                }
            }
            return localObbInfo1;
        }

        abstract void handleError();

        abstract void handleExecute()
            throws RemoteException, IOException;

        protected void sendNewStatusOrIgnore(int paramInt)
        {
            if ((this.mObbState == null) || (this.mObbState.token == null));
            while (true)
            {
                return;
                try
                {
                    this.mObbState.token.onObbResult(this.mObbState.filename, this.mObbState.nonce, paramInt);
                }
                catch (RemoteException localRemoteException)
                {
                    Slog.w("MountService", "MountServiceListener went away while calling onObbStateChanged");
                }
            }
        }
    }

    private class ObbActionHandler extends Handler
    {
        private final List<MountService.ObbAction> mActions = new LinkedList();
        private boolean mBound = false;

        ObbActionHandler(Looper arg2)
        {
            super();
        }

        private boolean connectToService()
        {
            int i = 1;
            Intent localIntent = new Intent().setComponent(MountService.DEFAULT_CONTAINER_COMPONENT);
            if (MountService.this.mContext.bindService(localIntent, MountService.this.mDefContainerConn, i))
                this.mBound = i;
            while (true)
            {
                return i;
                int j = 0;
            }
        }

        private void disconnectService()
        {
            MountService.access$1902(MountService.this, null);
            this.mBound = false;
            MountService.this.mContext.unbindService(MountService.this.mDefContainerConn);
        }

        // ERROR //
        public void handleMessage(Message paramMessage)
        {
            // Byte code:
            //     0: aload_1
            //     1: getfield 76	android/os/Message:what	I
            //     4: tableswitch	default:+36 -> 40, 1:+37->41, 2:+91->95, 3:+319->323, 4:+232->236, 5:+383->387
            //     41: aload_1
            //     42: getfield 80	android/os/Message:obj	Ljava/lang/Object;
            //     45: checkcast 82	com/android/server/MountService$ObbAction
            //     48: astore 22
            //     50: aload_0
            //     51: getfield 23	com/android/server/MountService$ObbActionHandler:mBound	Z
            //     54: ifne +26 -> 80
            //     57: aload_0
            //     58: invokespecial 84	com/android/server/MountService$ObbActionHandler:connectToService	()Z
            //     61: ifne +19 -> 80
            //     64: ldc 86
            //     66: ldc 88
            //     68: invokestatic 94	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     71: pop
            //     72: aload 22
            //     74: invokevirtual 97	com/android/server/MountService$ObbAction:handleError	()V
            //     77: goto -37 -> 40
            //     80: aload_0
            //     81: getfield 30	com/android/server/MountService$ObbActionHandler:mActions	Ljava/util/List;
            //     84: aload 22
            //     86: invokeinterface 103 2 0
            //     91: pop
            //     92: goto -52 -> 40
            //     95: aload_1
            //     96: getfield 80	android/os/Message:obj	Ljava/lang/Object;
            //     99: ifnull +18 -> 117
            //     102: aload_0
            //     103: getfield 18	com/android/server/MountService$ObbActionHandler:this$0	Lcom/android/server/MountService;
            //     106: aload_1
            //     107: getfield 80	android/os/Message:obj	Ljava/lang/Object;
            //     110: checkcast 105	com/android/internal/app/IMediaContainerService
            //     113: invokestatic 62	com/android/server/MountService:access$1902	(Lcom/android/server/MountService;Lcom/android/internal/app/IMediaContainerService;)Lcom/android/internal/app/IMediaContainerService;
            //     116: pop
            //     117: aload_0
            //     118: getfield 18	com/android/server/MountService$ObbActionHandler:this$0	Lcom/android/server/MountService;
            //     121: invokestatic 109	com/android/server/MountService:access$1900	(Lcom/android/server/MountService;)Lcom/android/internal/app/IMediaContainerService;
            //     124: ifnonnull +60 -> 184
            //     127: ldc 86
            //     129: ldc 111
            //     131: invokestatic 94	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     134: pop
            //     135: aload_0
            //     136: getfield 30	com/android/server/MountService$ObbActionHandler:mActions	Ljava/util/List;
            //     139: invokeinterface 115 1 0
            //     144: astore 20
            //     146: aload 20
            //     148: invokeinterface 120 1 0
            //     153: ifeq +19 -> 172
            //     156: aload 20
            //     158: invokeinterface 124 1 0
            //     163: checkcast 82	com/android/server/MountService$ObbAction
            //     166: invokevirtual 97	com/android/server/MountService$ObbAction:handleError	()V
            //     169: goto -23 -> 146
            //     172: aload_0
            //     173: getfield 30	com/android/server/MountService$ObbActionHandler:mActions	Ljava/util/List;
            //     176: invokeinterface 127 1 0
            //     181: goto -141 -> 40
            //     184: aload_0
            //     185: getfield 30	com/android/server/MountService$ObbActionHandler:mActions	Ljava/util/List;
            //     188: invokeinterface 131 1 0
            //     193: ifle +32 -> 225
            //     196: aload_0
            //     197: getfield 30	com/android/server/MountService$ObbActionHandler:mActions	Ljava/util/List;
            //     200: iconst_0
            //     201: invokeinterface 135 2 0
            //     206: checkcast 82	com/android/server/MountService$ObbAction
            //     209: astore 18
            //     211: aload 18
            //     213: ifnull -173 -> 40
            //     216: aload 18
            //     218: aload_0
            //     219: invokevirtual 139	com/android/server/MountService$ObbAction:execute	(Lcom/android/server/MountService$ObbActionHandler;)V
            //     222: goto -182 -> 40
            //     225: ldc 86
            //     227: ldc 141
            //     229: invokestatic 144	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
            //     232: pop
            //     233: goto -193 -> 40
            //     236: aload_0
            //     237: getfield 30	com/android/server/MountService$ObbActionHandler:mActions	Ljava/util/List;
            //     240: invokeinterface 131 1 0
            //     245: ifle -205 -> 40
            //     248: aload_0
            //     249: getfield 23	com/android/server/MountService$ObbActionHandler:mBound	Z
            //     252: ifeq +7 -> 259
            //     255: aload_0
            //     256: invokespecial 146	com/android/server/MountService$ObbActionHandler:disconnectService	()V
            //     259: aload_0
            //     260: invokespecial 84	com/android/server/MountService$ObbActionHandler:connectToService	()Z
            //     263: ifne -223 -> 40
            //     266: ldc 86
            //     268: ldc 88
            //     270: invokestatic 94	android/util/Slog:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     273: pop
            //     274: aload_0
            //     275: getfield 30	com/android/server/MountService$ObbActionHandler:mActions	Ljava/util/List;
            //     278: invokeinterface 115 1 0
            //     283: astore 16
            //     285: aload 16
            //     287: invokeinterface 120 1 0
            //     292: ifeq +19 -> 311
            //     295: aload 16
            //     297: invokeinterface 124 1 0
            //     302: checkcast 82	com/android/server/MountService$ObbAction
            //     305: invokevirtual 97	com/android/server/MountService$ObbAction:handleError	()V
            //     308: goto -23 -> 285
            //     311: aload_0
            //     312: getfield 30	com/android/server/MountService$ObbActionHandler:mActions	Ljava/util/List;
            //     315: invokeinterface 127 1 0
            //     320: goto -280 -> 40
            //     323: aload_0
            //     324: getfield 30	com/android/server/MountService$ObbActionHandler:mActions	Ljava/util/List;
            //     327: invokeinterface 131 1 0
            //     332: ifle +14 -> 346
            //     335: aload_0
            //     336: getfield 30	com/android/server/MountService$ObbActionHandler:mActions	Ljava/util/List;
            //     339: iconst_0
            //     340: invokeinterface 149 2 0
            //     345: pop
            //     346: aload_0
            //     347: getfield 30	com/android/server/MountService$ObbActionHandler:mActions	Ljava/util/List;
            //     350: invokeinterface 131 1 0
            //     355: ifne +17 -> 372
            //     358: aload_0
            //     359: getfield 23	com/android/server/MountService$ObbActionHandler:mBound	Z
            //     362: ifeq -322 -> 40
            //     365: aload_0
            //     366: invokespecial 146	com/android/server/MountService$ObbActionHandler:disconnectService	()V
            //     369: goto -329 -> 40
            //     372: aload_0
            //     373: getfield 18	com/android/server/MountService$ObbActionHandler:this$0	Lcom/android/server/MountService;
            //     376: invokestatic 153	com/android/server/MountService:access$000	(Lcom/android/server/MountService;)Lcom/android/server/MountService$ObbActionHandler;
            //     379: iconst_2
            //     380: invokevirtual 157	com/android/server/MountService$ObbActionHandler:sendEmptyMessage	(I)Z
            //     383: pop
            //     384: goto -344 -> 40
            //     387: aload_1
            //     388: getfield 80	android/os/Message:obj	Ljava/lang/Object;
            //     391: checkcast 159	java/lang/String
            //     394: astore_2
            //     395: aload_0
            //     396: getfield 18	com/android/server/MountService$ObbActionHandler:this$0	Lcom/android/server/MountService;
            //     399: invokestatic 163	com/android/server/MountService:access$2000	(Lcom/android/server/MountService;)Ljava/util/Map;
            //     402: astore_3
            //     403: aload_3
            //     404: monitorenter
            //     405: new 25	java/util/LinkedList
            //     408: dup
            //     409: invokespecial 28	java/util/LinkedList:<init>	()V
            //     412: astore 4
            //     414: aload_0
            //     415: getfield 18	com/android/server/MountService$ObbActionHandler:this$0	Lcom/android/server/MountService;
            //     418: invokestatic 166	com/android/server/MountService:access$2100	(Lcom/android/server/MountService;)Ljava/util/Map;
            //     421: invokeinterface 172 1 0
            //     426: invokeinterface 175 1 0
            //     431: astore 6
            //     433: aload 6
            //     435: invokeinterface 120 1 0
            //     440: ifeq +57 -> 497
            //     443: aload 6
            //     445: invokeinterface 124 1 0
            //     450: checkcast 177	java/util/Map$Entry
            //     453: astore 11
            //     455: aload 11
            //     457: invokeinterface 180 1 0
            //     462: checkcast 159	java/lang/String
            //     465: aload_2
            //     466: invokevirtual 184	java/lang/String:startsWith	(Ljava/lang/String;)Z
            //     469: ifeq -36 -> 433
            //     472: aload 4
            //     474: aload 11
            //     476: invokeinterface 187 1 0
            //     481: invokeinterface 103 2 0
            //     486: pop
            //     487: goto -54 -> 433
            //     490: astore 5
            //     492: aload_3
            //     493: monitorexit
            //     494: aload 5
            //     496: athrow
            //     497: aload 4
            //     499: invokeinterface 115 1 0
            //     504: astore 7
            //     506: aload 7
            //     508: invokeinterface 120 1 0
            //     513: ifeq +82 -> 595
            //     516: aload 7
            //     518: invokeinterface 124 1 0
            //     523: checkcast 189	com/android/server/MountService$ObbState
            //     526: astore 8
            //     528: aload_0
            //     529: getfield 18	com/android/server/MountService$ObbActionHandler:this$0	Lcom/android/server/MountService;
            //     532: aload 8
            //     534: invokestatic 193	com/android/server/MountService:access$2200	(Lcom/android/server/MountService;Lcom/android/server/MountService$ObbState;)V
            //     537: aload 8
            //     539: getfield 197	com/android/server/MountService$ObbState:token	Landroid/os/storage/IObbActionListener;
            //     542: aload 8
            //     544: getfield 201	com/android/server/MountService$ObbState:filename	Ljava/lang/String;
            //     547: aload 8
            //     549: getfield 204	com/android/server/MountService$ObbState:nonce	I
            //     552: iconst_2
            //     553: invokeinterface 210 4 0
            //     558: goto -52 -> 506
            //     561: astore 9
            //     563: ldc 86
            //     565: new 212	java/lang/StringBuilder
            //     568: dup
            //     569: invokespecial 213	java/lang/StringBuilder:<init>	()V
            //     572: ldc 215
            //     574: invokevirtual 219	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     577: aload 8
            //     579: getfield 201	com/android/server/MountService$ObbState:filename	Ljava/lang/String;
            //     582: invokevirtual 219	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     585: invokevirtual 223	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     588: invokestatic 226	android/util/Slog:i	(Ljava/lang/String;Ljava/lang/String;)I
            //     591: pop
            //     592: goto -86 -> 506
            //     595: aload_3
            //     596: monitorexit
            //     597: goto -557 -> 40
            //
            // Exception table:
            //     from	to	target	type
            //     405	494	490	finally
            //     497	537	490	finally
            //     537	558	490	finally
            //     563	597	490	finally
            //     537	558	561	android/os/RemoteException
        }
    }

    private final class MountServiceBinderListener
        implements IBinder.DeathRecipient
    {
        final IMountServiceListener mListener;

        MountServiceBinderListener(IMountServiceListener arg2)
        {
            Object localObject;
            this.mListener = localObject;
        }

        public void binderDied()
        {
            synchronized (MountService.this.mListeners)
            {
                MountService.this.mListeners.remove(this);
                this.mListener.asBinder().unlinkToDeath(this, 0);
                return;
            }
        }
    }

    class MountServiceHandler extends Handler
    {
        ArrayList<MountService.UnmountCallBack> mForceUnmounts = new ArrayList();
        boolean mUpdatingStatus = false;

        MountServiceHandler(Looper arg2)
        {
            super();
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
            case 1:
            case 2:
            case 3:
            }
            while (true)
            {
                return;
                MountService.UnmountCallBack localUnmountCallBack2 = (MountService.UnmountCallBack)paramMessage.obj;
                this.mForceUnmounts.add(localUnmountCallBack2);
                if (!this.mUpdatingStatus)
                {
                    this.mUpdatingStatus = true;
                    MountService.this.mPms.updateExternalMediaStatus(false, true);
                    continue;
                    this.mUpdatingStatus = false;
                    int i = this.mForceUnmounts.size();
                    int[] arrayOfInt1 = new int[i];
                    ActivityManagerService localActivityManagerService = (ActivityManagerService)ServiceManager.getService("activity");
                    int j = 0;
                    int k = 0;
                    if (j < i)
                    {
                        MountService.UnmountCallBack localUnmountCallBack1 = (MountService.UnmountCallBack)this.mForceUnmounts.get(j);
                        String str = localUnmountCallBack1.path;
                        int n = 0;
                        label153: int i1;
                        if (!localUnmountCallBack1.force)
                        {
                            n = 1;
                            if ((n != 0) || (localUnmountCallBack1.retries >= 4))
                                break label310;
                            Slog.i("MountService", "Retrying to kill storage users again");
                            Handler localHandler1 = MountService.this.mHandler;
                            Handler localHandler2 = MountService.this.mHandler;
                            int i2 = localUnmountCallBack1.retries;
                            localUnmountCallBack1.retries = (i2 + 1);
                            localHandler1.sendMessageDelayed(localHandler2.obtainMessage(2, Integer.valueOf(i2)), 30L);
                            i1 = k;
                        }
                        while (true)
                        {
                            j++;
                            k = i1;
                            break;
                            int[] arrayOfInt2 = MountService.this.getStorageUsers(str);
                            if ((arrayOfInt2 == null) || (arrayOfInt2.length == 0))
                            {
                                n = 1;
                                break label153;
                            }
                            localActivityManagerService.killPids(arrayOfInt2, "unmount media", true);
                            int[] arrayOfInt3 = MountService.this.getStorageUsers(str);
                            if ((arrayOfInt3 != null) && (arrayOfInt3.length != 0))
                                break label153;
                            n = 1;
                            break label153;
                            label310: if (localUnmountCallBack1.retries >= 4)
                                Slog.i("MountService", "Failed to unmount media inspite of 4 retries. Forcibly killing processes now");
                            i1 = k + 1;
                            arrayOfInt1[k] = j;
                            MountService.this.mHandler.sendMessage(MountService.this.mHandler.obtainMessage(3, localUnmountCallBack1));
                        }
                    }
                    for (int m = k - 1; m >= 0; m--)
                        this.mForceUnmounts.remove(arrayOfInt1[m]);
                    continue;
                    ((MountService.UnmountCallBack)paramMessage.obj).handleFinished();
                }
            }
        }
    }

    class ShutdownCallBack extends MountService.UnmountCallBack
    {
        IMountShutdownObserver observer;

        ShutdownCallBack(String paramIMountShutdownObserver, IMountShutdownObserver arg3)
        {
            super(paramIMountShutdownObserver, true, false);
            Object localObject;
            this.observer = localObject;
        }

        void handleFinished()
        {
            int i = MountService.this.doUnmountVolume(this.path, true, this.removeEncryption);
            if (this.observer != null);
            try
            {
                this.observer.onShutDownComplete(i);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Slog.w("MountService", "RemoteException when shutting down");
            }
        }
    }

    class UmsEnableCallBack extends MountService.UnmountCallBack
    {
        final String method;

        UmsEnableCallBack(String paramString1, String paramBoolean, boolean arg4)
        {
            super(paramString1, bool, false);
            this.method = paramBoolean;
        }

        void handleFinished()
        {
            super.handleFinished();
            MountService.this.doShareUnshareVolume(this.path, this.method, true);
        }
    }

    class UnmountCallBack
    {
        final boolean force;
        final String path;
        final boolean removeEncryption;
        int retries = 0;

        UnmountCallBack(String paramBoolean1, boolean paramBoolean2, boolean arg4)
        {
            this.path = paramBoolean1;
            this.force = paramBoolean2;
            boolean bool;
            this.removeEncryption = bool;
        }

        void handleFinished()
        {
            MountService.this.doUnmountVolume(this.path, true, this.removeEncryption);
        }
    }

    class DefaultContainerConnection
        implements ServiceConnection
    {
        DefaultContainerConnection()
        {
        }

        public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
        {
            IMediaContainerService localIMediaContainerService = IMediaContainerService.Stub.asInterface(paramIBinder);
            MountService.this.mObbActionHandler.sendMessage(MountService.this.mObbActionHandler.obtainMessage(2, localIMediaContainerService));
        }

        public void onServiceDisconnected(ComponentName paramComponentName)
        {
        }
    }

    class ObbState
        implements IBinder.DeathRecipient
    {
        public final int callerUid;
        String filename;
        final int nonce;
        final IObbActionListener token;

        public ObbState(String paramInt1, int paramIObbActionListener, IObbActionListener paramInt2, int arg5)
            throws RemoteException
        {
            this.filename = paramInt1;
            this.callerUid = paramIObbActionListener;
            this.token = paramInt2;
            int i;
            this.nonce = i;
        }

        public void binderDied()
        {
            MountService.UnmountObbAction localUnmountObbAction = new MountService.UnmountObbAction(MountService.this, this, true);
            MountService.this.mObbActionHandler.sendMessage(MountService.this.mObbActionHandler.obtainMessage(1, localUnmountObbAction));
        }

        public IBinder getBinder()
        {
            return this.token.asBinder();
        }

        public void link()
            throws RemoteException
        {
            getBinder().linkToDeath(this, 0);
        }

        public String toString()
        {
            StringBuilder localStringBuilder = new StringBuilder("ObbState{");
            localStringBuilder.append("filename=");
            localStringBuilder.append(this.filename);
            localStringBuilder.append(",token=");
            localStringBuilder.append(this.token.toString());
            localStringBuilder.append(",callerUid=");
            localStringBuilder.append(this.callerUid);
            localStringBuilder.append('}');
            return localStringBuilder.toString();
        }

        public void unlink()
        {
            getBinder().unlinkToDeath(this, 0);
        }
    }

    class VoldResponseCode
    {
        public static final int AsecListResult = 111;
        public static final int AsecPathResult = 211;
        public static final int OpFailedMediaBlank = 402;
        public static final int OpFailedMediaCorrupt = 403;
        public static final int OpFailedNoMedia = 401;
        public static final int OpFailedStorageBusy = 405;
        public static final int OpFailedStorageNotFound = 406;
        public static final int OpFailedVolNotMounted = 404;
        public static final int ShareEnabledResult = 212;
        public static final int ShareStatusResult = 210;
        public static final int StorageUsersListResult = 112;
        public static final int VolumeBadRemoval = 632;
        public static final int VolumeDiskInserted = 630;
        public static final int VolumeDiskRemoved = 631;
        public static final int VolumeListResult = 110;
        public static final int VolumeStateChange = 605;

        VoldResponseCode()
        {
        }
    }

    class VolumeState
    {
        public static final int Checking = 3;
        public static final int Formatting = 6;
        public static final int Idle = 1;
        public static final int Init = -1;
        public static final int Mounted = 4;
        public static final int NoMedia = 0;
        public static final int Pending = 2;
        public static final int Shared = 7;
        public static final int SharedMnt = 8;
        public static final int Unmounting = 5;

        VolumeState()
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.MountService
 * JD-Core Version:        0.6.2
 */