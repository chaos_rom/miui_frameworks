package com.android.server;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetProviderInfo;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.Intent.FilterComparison;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Pair;
import android.util.Slog;
import android.util.SparseArray;
import android.widget.RemoteViews;
import com.android.internal.appwidget.IAppWidgetHost;
import com.android.internal.appwidget.IAppWidgetService.Stub;
import com.android.internal.widget.IRemoteViewsAdapterConnection;
import com.android.internal.widget.IRemoteViewsAdapterConnection.Stub;
import java.io.File;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

class AppWidgetService extends IAppWidgetService.Stub
{
    private static final String TAG = "AppWidgetService";
    AlarmManager mAlarmManager;
    final ArrayList<AppWidgetId> mAppWidgetIds = new ArrayList();
    private final SparseArray<AppWidgetServiceImpl> mAppWidgetServices;
    BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver()
    {
        @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            String str = paramAnonymousIntent.getAction();
            if (AppWidgetService.Injector.handleAction(AppWidgetService.this, str));
            while (true)
            {
                return;
                if ("android.intent.action.BOOT_COMPLETED".equals(str))
                    AppWidgetService.this.getImplForUser().sendInitialBroadcasts();
                else if ("android.intent.action.CONFIGURATION_CHANGED".equals(str))
                    for (int j = 0; j < AppWidgetService.this.mAppWidgetServices.size(); j++)
                        ((AppWidgetServiceImpl)AppWidgetService.this.mAppWidgetServices.valueAt(j)).onConfigurationChanged();
                else
                    for (int i = 0; i < AppWidgetService.this.mAppWidgetServices.size(); i++)
                        ((AppWidgetServiceImpl)AppWidgetService.this.mAppWidgetServices.valueAt(i)).onBroadcastReceived(paramAnonymousIntent);
            }
        }
    };
    Context mContext;
    ArrayList<Host> mHosts = new ArrayList();
    ArrayList<Provider> mInstalledProviders = new ArrayList();
    Locale mLocale;
    int mNextAppWidgetId = 1;
    PackageManager mPackageManager;
    boolean mSafeMode;

    AppWidgetService(Context paramContext)
    {
        this.mContext = paramContext;
        this.mAppWidgetServices = new SparseArray(5);
        AppWidgetServiceImpl localAppWidgetServiceImpl = new AppWidgetServiceImpl(paramContext, 0);
        this.mAppWidgetServices.append(0, localAppWidgetServiceImpl);
    }

    static int[] getAppWidgetIds(Provider paramProvider)
    {
        int i = paramProvider.instances.size();
        int[] arrayOfInt = new int[i];
        for (int j = 0; j < i; j++)
            arrayOfInt[j] = ((AppWidgetId)paramProvider.instances.get(j)).appWidgetId;
        return arrayOfInt;
    }

    private AppWidgetServiceImpl getImplForUser()
    {
        int i = Binder.getOrigCallingUser();
        AppWidgetServiceImpl localAppWidgetServiceImpl = (AppWidgetServiceImpl)this.mAppWidgetServices.get(i);
        if (localAppWidgetServiceImpl == null)
        {
            Slog.e("AppWidgetService", "Unable to find AppWidgetServiceImpl for the current user");
            localAppWidgetServiceImpl = new AppWidgetServiceImpl(this.mContext, i);
            localAppWidgetServiceImpl.systemReady(this.mSafeMode);
            localAppWidgetServiceImpl.sendInitialBroadcasts();
            this.mAppWidgetServices.append(i, localAppWidgetServiceImpl);
        }
        return localAppWidgetServiceImpl;
    }

    public int allocateAppWidgetId(String paramString, int paramInt)
        throws RemoteException
    {
        return getImplForUser().allocateAppWidgetId(paramString, paramInt);
    }

    public void bindAppWidgetId(int paramInt, ComponentName paramComponentName)
        throws RemoteException
    {
        getImplForUser().bindAppWidgetId(paramInt, paramComponentName);
    }

    public boolean bindAppWidgetIdIfAllowed(String paramString, int paramInt, ComponentName paramComponentName)
        throws RemoteException
    {
        return getImplForUser().bindAppWidgetIdIfAllowed(paramString, paramInt, paramComponentName);
    }

    public void bindRemoteViewsService(int paramInt, Intent paramIntent, IBinder paramIBinder)
        throws RemoteException
    {
        getImplForUser().bindRemoteViewsService(paramInt, paramIntent, paramIBinder);
    }

    public void deleteAllHosts()
        throws RemoteException
    {
        getImplForUser().deleteAllHosts();
    }

    public void deleteAppWidgetId(int paramInt)
        throws RemoteException
    {
        getImplForUser().deleteAppWidgetId(paramInt);
    }

    public void deleteHost(int paramInt)
        throws RemoteException
    {
        getImplForUser().deleteHost(paramInt);
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        for (int i = 0; i < this.mAppWidgetServices.size(); i++)
            ((AppWidgetServiceImpl)this.mAppWidgetServices.valueAt(i)).dump(paramFileDescriptor, paramPrintWriter, paramArrayOfString);
    }

    public int[] getAppWidgetIds(ComponentName paramComponentName)
        throws RemoteException
    {
        return getImplForUser().getAppWidgetIds(paramComponentName);
    }

    public AppWidgetProviderInfo getAppWidgetInfo(int paramInt)
        throws RemoteException
    {
        return getImplForUser().getAppWidgetInfo(paramInt);
    }

    public Bundle getAppWidgetOptions(int paramInt)
    {
        return getImplForUser().getAppWidgetOptions(paramInt);
    }

    public RemoteViews getAppWidgetViews(int paramInt)
        throws RemoteException
    {
        return getImplForUser().getAppWidgetViews(paramInt);
    }

    public List<AppWidgetProviderInfo> getInstalledProviders()
        throws RemoteException
    {
        return getImplForUser().getInstalledProviders();
    }

    public boolean hasBindAppWidgetPermission(String paramString)
        throws RemoteException
    {
        return getImplForUser().hasBindAppWidgetPermission(paramString);
    }

    public void notifyAppWidgetViewDataChanged(int[] paramArrayOfInt, int paramInt)
        throws RemoteException
    {
        getImplForUser().notifyAppWidgetViewDataChanged(paramArrayOfInt, paramInt);
    }

    public void onUserRemoved(int paramInt)
    {
        AppWidgetServiceImpl localAppWidgetServiceImpl = (AppWidgetServiceImpl)this.mAppWidgetServices.get(paramInt);
        if (paramInt < 1);
        while (true)
        {
            return;
            if (localAppWidgetServiceImpl == null)
                AppWidgetServiceImpl.getSettingsFile(paramInt).delete();
            else
                localAppWidgetServiceImpl.onUserRemoved();
        }
    }

    public void partiallyUpdateAppWidgetIds(int[] paramArrayOfInt, RemoteViews paramRemoteViews)
        throws RemoteException
    {
        getImplForUser().partiallyUpdateAppWidgetIds(paramArrayOfInt, paramRemoteViews);
    }

    public void setBindAppWidgetPermission(String paramString, boolean paramBoolean)
        throws RemoteException
    {
        getImplForUser().setBindAppWidgetPermission(paramString, paramBoolean);
    }

    public int[] startListening(IAppWidgetHost paramIAppWidgetHost, String paramString, int paramInt, List<RemoteViews> paramList)
        throws RemoteException
    {
        return getImplForUser().startListening(paramIAppWidgetHost, paramString, paramInt, paramList);
    }

    public void stopListening(int paramInt)
        throws RemoteException
    {
        getImplForUser().stopListening(paramInt);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public void systemReady(boolean paramBoolean)
    {
        this.mSafeMode = paramBoolean;
        ((AppWidgetServiceImpl)this.mAppWidgetServices.get(0)).systemReady(paramBoolean);
        Injector.receiveRestoreFinish(this);
        this.mContext.registerReceiver(this.mBroadcastReceiver, new IntentFilter("android.intent.action.BOOT_COMPLETED"), null, null);
        this.mContext.registerReceiver(this.mBroadcastReceiver, new IntentFilter("android.intent.action.CONFIGURATION_CHANGED"), null, null);
        IntentFilter localIntentFilter1 = new IntentFilter();
        localIntentFilter1.addAction("android.intent.action.PACKAGE_ADDED");
        localIntentFilter1.addAction("android.intent.action.PACKAGE_CHANGED");
        localIntentFilter1.addAction("android.intent.action.PACKAGE_REMOVED");
        localIntentFilter1.addDataScheme("package");
        this.mContext.registerReceiver(this.mBroadcastReceiver, localIntentFilter1);
        IntentFilter localIntentFilter2 = new IntentFilter();
        localIntentFilter2.addAction("android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE");
        localIntentFilter2.addAction("android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE");
        this.mContext.registerReceiver(this.mBroadcastReceiver, localIntentFilter2);
        IntentFilter localIntentFilter3 = new IntentFilter();
        localIntentFilter3.addAction("android.intent.action.USER_REMOVED");
        this.mContext.registerReceiver(new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                AppWidgetService.this.onUserRemoved(paramAnonymousIntent.getIntExtra("android.intent.extra.user_id", -1));
            }
        }
        , localIntentFilter3);
    }

    public void unbindRemoteViewsService(int paramInt, Intent paramIntent)
        throws RemoteException
    {
        getImplForUser().unbindRemoteViewsService(paramInt, paramIntent);
    }

    public void updateAppWidgetIds(int[] paramArrayOfInt, RemoteViews paramRemoteViews)
        throws RemoteException
    {
        getImplForUser().updateAppWidgetIds(paramArrayOfInt, paramRemoteViews);
    }

    public void updateAppWidgetOptions(int paramInt, Bundle paramBundle)
    {
        getImplForUser().updateAppWidgetOptions(paramInt, paramBundle);
    }

    public void updateAppWidgetProvider(ComponentName paramComponentName, RemoteViews paramRemoteViews)
        throws RemoteException
    {
        getImplForUser().updateAppWidgetProvider(paramComponentName, paramRemoteViews);
    }

    static class ServiceConnectionProxy
        implements ServiceConnection
    {
        private final IBinder mConnectionCb;

        ServiceConnectionProxy(Pair<Integer, Intent.FilterComparison> paramPair, IBinder paramIBinder)
        {
            this.mConnectionCb = paramIBinder;
        }

        public void disconnect()
        {
            IRemoteViewsAdapterConnection localIRemoteViewsAdapterConnection = IRemoteViewsAdapterConnection.Stub.asInterface(this.mConnectionCb);
            try
            {
                localIRemoteViewsAdapterConnection.onServiceDisconnected();
                return;
            }
            catch (Exception localException)
            {
                while (true)
                    localException.printStackTrace();
            }
        }

        public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
        {
            IRemoteViewsAdapterConnection localIRemoteViewsAdapterConnection = IRemoteViewsAdapterConnection.Stub.asInterface(this.mConnectionCb);
            try
            {
                localIRemoteViewsAdapterConnection.onServiceConnected(paramIBinder);
                return;
            }
            catch (Exception localException)
            {
                while (true)
                    localException.printStackTrace();
            }
        }

        public void onServiceDisconnected(ComponentName paramComponentName)
        {
            disconnect();
        }
    }

    static class AppWidgetId
    {
        int appWidgetId;
        AppWidgetService.Host host;
        AppWidgetService.Provider provider;
        RemoteViews views;
    }

    static class Host
    {
        IAppWidgetHost callbacks;
        int hostId;
        ArrayList<AppWidgetService.AppWidgetId> instances = new ArrayList();
        String packageName;
        int tag;
        int uid;
        boolean zombie;
    }

    static class Provider
    {
        PendingIntent broadcast;
        AppWidgetProviderInfo info;
        ArrayList<AppWidgetService.AppWidgetId> instances = new ArrayList();
        int tag;
        int uid;
        boolean zombie;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static boolean handleAction(AppWidgetService paramAppWidgetService, String paramString)
        {
            if ("android.intent.action.RESTORE_FINISH".equals(paramString))
                paramAppWidgetService.getImplForUser().reload();
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        static void receiveRestoreFinish(AppWidgetService paramAppWidgetService)
        {
            paramAppWidgetService.mContext.registerReceiver(paramAppWidgetService.mBroadcastReceiver, new IntentFilter("android.intent.action.RESTORE_FINISH"), null, null);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.AppWidgetService
 * JD-Core Version:        0.6.2
 */