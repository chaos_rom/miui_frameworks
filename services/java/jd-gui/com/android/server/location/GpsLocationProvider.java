package com.android.server.location;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.location.Criteria;
import android.location.IGpsStatusListener;
import android.location.IGpsStatusProvider;
import android.location.IGpsStatusProvider.Stub;
import android.location.ILocationManager;
import android.location.INetInitiatedListener;
import android.location.INetInitiatedListener.Stub;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager.WakeLock;
import android.os.Process;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.WorkSource;
import android.provider.Settings.Secure;
import android.provider.Telephony.Sms.Intents;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import android.util.NtpTrustedTime;
import android.util.SparseIntArray;
import com.android.internal.app.IBatteryStats;
import com.android.internal.location.GpsNetInitiatedHandler;
import com.android.internal.location.GpsNetInitiatedHandler.GpsNiNotification;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

public class GpsLocationProvider
    implements LocationProviderInterface
{
    private static final int ADD_LISTENER = 8;
    private static final int AGPS_DATA_CONNECTION_CLOSED = 0;
    private static final int AGPS_DATA_CONNECTION_OPEN = 2;
    private static final int AGPS_DATA_CONNECTION_OPENING = 1;
    private static final int AGPS_REF_LOCATION_TYPE_GSM_CELLID = 1;
    private static final int AGPS_REF_LOCATION_TYPE_UMTS_CELLID = 2;
    private static final int AGPS_REG_LOCATION_TYPE_MAC = 3;
    private static final int AGPS_RIL_REQUEST_REFLOC_CELLID = 1;
    private static final int AGPS_RIL_REQUEST_REFLOC_MAC = 2;
    private static final int AGPS_RIL_REQUEST_SETID_IMSI = 1;
    private static final int AGPS_RIL_REQUEST_SETID_MSISDN = 2;
    private static final int AGPS_SETID_TYPE_IMSI = 1;
    private static final int AGPS_SETID_TYPE_MSISDN = 2;
    private static final int AGPS_SETID_TYPE_NONE = 0;
    private static final int AGPS_TYPE_C2K = 2;
    private static final int AGPS_TYPE_SUPL = 1;
    private static final String ALARM_TIMEOUT = "com.android.internal.location.ALARM_TIMEOUT";
    private static final String ALARM_WAKEUP = "com.android.internal.location.ALARM_WAKEUP";
    private static final int ALMANAC_MASK = 1;
    private static final int CHECK_LOCATION = 1;
    private static final boolean DEBUG = false;
    private static final int DOWNLOAD_XTRA_DATA = 6;
    private static final int ENABLE = 2;
    private static final int ENABLE_TRACKING = 3;
    private static final int EPHEMERIS_MASK = 0;
    private static final int GPS_AGPS_DATA_CONNECTED = 3;
    private static final int GPS_AGPS_DATA_CONN_DONE = 4;
    private static final int GPS_AGPS_DATA_CONN_FAILED = 5;
    private static final int GPS_CAPABILITY_MSA = 4;
    private static final int GPS_CAPABILITY_MSB = 2;
    private static final int GPS_CAPABILITY_ON_DEMAND_TIME = 16;
    private static final int GPS_CAPABILITY_SCHEDULING = 1;
    private static final int GPS_CAPABILITY_SINGLE_SHOT = 8;
    private static final int GPS_DELETE_ALL = 65535;
    private static final int GPS_DELETE_ALMANAC = 2;
    private static final int GPS_DELETE_CELLDB_INFO = 32768;
    private static final int GPS_DELETE_EPHEMERIS = 1;
    private static final int GPS_DELETE_HEALTH = 64;
    private static final int GPS_DELETE_IONO = 16;
    private static final int GPS_DELETE_POSITION = 4;
    private static final int GPS_DELETE_RTI = 1024;
    private static final int GPS_DELETE_SADATA = 512;
    private static final int GPS_DELETE_SVDIR = 128;
    private static final int GPS_DELETE_SVSTEER = 256;
    private static final int GPS_DELETE_TIME = 8;
    private static final int GPS_DELETE_UTC = 32;
    private static final int GPS_POLLING_THRESHOLD_INTERVAL = 10000;
    private static final int GPS_POSITION_MODE_MS_ASSISTED = 2;
    private static final int GPS_POSITION_MODE_MS_BASED = 1;
    private static final int GPS_POSITION_MODE_STANDALONE = 0;
    private static final int GPS_POSITION_RECURRENCE_PERIODIC = 0;
    private static final int GPS_POSITION_RECURRENCE_SINGLE = 1;
    private static final int GPS_RELEASE_AGPS_DATA_CONN = 2;
    private static final int GPS_REQUEST_AGPS_DATA_CONN = 1;
    private static final int GPS_STATUS_ENGINE_OFF = 4;
    private static final int GPS_STATUS_ENGINE_ON = 3;
    private static final int GPS_STATUS_NONE = 0;
    private static final int GPS_STATUS_SESSION_BEGIN = 1;
    private static final int GPS_STATUS_SESSION_END = 2;
    private static final int INJECT_NTP_TIME = 5;
    private static final int LOCATION_HAS_ACCURACY = 16;
    private static final int LOCATION_HAS_ALTITUDE = 2;
    private static final int LOCATION_HAS_BEARING = 8;
    private static final int LOCATION_HAS_LAT_LONG = 1;
    private static final int LOCATION_HAS_SPEED = 4;
    private static final int LOCATION_INVALID = 0;
    private static final int MAX_SVS = 32;
    private static final int NO_FIX_TIMEOUT = 60000;
    private static final long NTP_INTERVAL = 86400000L;
    private static final String PROPERTIES_FILE = "/etc/gps.conf";
    private static final long RECENT_FIX_TIMEOUT = 10000L;
    private static final int REMOVE_LISTENER = 9;
    private static final int REQUEST_SINGLE_SHOT = 10;
    private static final long RETRY_INTERVAL = 300000L;
    private static final String TAG = "GpsLocationProvider";
    private static final int UPDATE_LOCATION = 7;
    private static final int UPDATE_NETWORK_STATE = 4;
    private static final int USED_FOR_FIX_MASK = 2;
    private static final boolean VERBOSE = false;
    private static final String WAKELOCK_KEY = "GpsLocationProvider";
    private String mAGpsApn;
    private int mAGpsDataConnectionIpAddr;
    private int mAGpsDataConnectionState;
    private final AlarmManager mAlarmManager;
    private final IBatteryStats mBatteryStats;
    private final BroadcastReceiver mBroadcastReciever;
    private String mC2KServerHost;
    private int mC2KServerPort;
    private final SparseIntArray mClientUids;
    private final ConnectivityManager mConnMgr;
    private final Context mContext;
    private boolean mDownloadXtraDataPending;
    private volatile boolean mEnabled;
    private int mEngineCapabilities;
    private boolean mEngineOn;
    private int mFixInterval;
    private long mFixRequestTime;
    private final IGpsStatusProvider mGpsStatusProvider;
    private Handler mHandler;
    private final CountDownLatch mInitializedLatch;
    private boolean mInjectNtpTimePending;
    private long mLastFixTime;
    private ArrayList<Listener> mListeners;
    private Location mLocation;
    private Bundle mLocationExtras;
    private int mLocationFlags;
    private final ILocationManager mLocationManager;
    private final GpsNetInitiatedHandler mNIHandler;
    private boolean mNavigating;
    private final INetInitiatedListener mNetInitiatedListener;
    private boolean mNetworkAvailable;
    private byte[] mNmeaBuffer;
    private final NtpTrustedTime mNtpTime;
    private int mPendingListenerMessages;
    private int mPendingMessageBits;
    private boolean mPeriodicTimeInjection;
    private int mPositionMode;
    private Properties mProperties;
    private boolean mSingleShot;
    private float[] mSnrs;
    private boolean mStarted;
    private int mStatus;
    private long mStatusUpdateTime;
    private String mSuplServerHost;
    private int mSuplServerPort;
    private boolean mSupportsXtra;
    private float[] mSvAzimuths;
    private int mSvCount;
    private float[] mSvElevations;
    private int[] mSvMasks;
    private int[] mSvs;
    private int mTTFF;
    private final Thread mThread;
    private final PendingIntent mTimeoutIntent;
    private final PowerManager.WakeLock mWakeLock;
    private final PendingIntent mWakeupIntent;

    static
    {
        class_init_native();
    }

    // ERROR //
    public GpsLocationProvider(Context paramContext, ILocationManager paramILocationManager)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 241	java/lang/Object:<init>	()V
        //     4: aload_0
        //     5: iconst_0
        //     6: putfield 243	com/android/server/location/GpsLocationProvider:mLocationFlags	I
        //     9: aload_0
        //     10: iconst_1
        //     11: putfield 245	com/android/server/location/GpsLocationProvider:mStatus	I
        //     14: aload_0
        //     15: invokestatic 251	android/os/SystemClock:elapsedRealtime	()J
        //     18: putfield 253	com/android/server/location/GpsLocationProvider:mStatusUpdateTime	J
        //     21: aload_0
        //     22: iconst_1
        //     23: putfield 255	com/android/server/location/GpsLocationProvider:mInjectNtpTimePending	Z
        //     26: aload_0
        //     27: iconst_1
        //     28: putfield 257	com/android/server/location/GpsLocationProvider:mDownloadXtraDataPending	Z
        //     31: aload_0
        //     32: sipush 1000
        //     35: putfield 259	com/android/server/location/GpsLocationProvider:mFixInterval	I
        //     38: aload_0
        //     39: lconst_0
        //     40: putfield 261	com/android/server/location/GpsLocationProvider:mFixRequestTime	J
        //     43: aload_0
        //     44: iconst_0
        //     45: putfield 263	com/android/server/location/GpsLocationProvider:mTTFF	I
        //     48: aload_0
        //     49: new 265	android/location/Location
        //     52: dup
        //     53: ldc_w 267
        //     56: invokespecial 270	android/location/Location:<init>	(Ljava/lang/String;)V
        //     59: putfield 272	com/android/server/location/GpsLocationProvider:mLocation	Landroid/location/Location;
        //     62: aload_0
        //     63: new 274	android/os/Bundle
        //     66: dup
        //     67: invokespecial 275	android/os/Bundle:<init>	()V
        //     70: putfield 277	com/android/server/location/GpsLocationProvider:mLocationExtras	Landroid/os/Bundle;
        //     73: aload_0
        //     74: new 279	java/util/ArrayList
        //     77: dup
        //     78: invokespecial 280	java/util/ArrayList:<init>	()V
        //     81: putfield 282	com/android/server/location/GpsLocationProvider:mListeners	Ljava/util/ArrayList;
        //     84: aload_0
        //     85: new 284	java/util/concurrent/CountDownLatch
        //     88: dup
        //     89: iconst_1
        //     90: invokespecial 287	java/util/concurrent/CountDownLatch:<init>	(I)V
        //     93: putfield 289	com/android/server/location/GpsLocationProvider:mInitializedLatch	Ljava/util/concurrent/CountDownLatch;
        //     96: aload_0
        //     97: new 291	android/util/SparseIntArray
        //     100: dup
        //     101: invokespecial 292	android/util/SparseIntArray:<init>	()V
        //     104: putfield 294	com/android/server/location/GpsLocationProvider:mClientUids	Landroid/util/SparseIntArray;
        //     107: aload_0
        //     108: new 8	com/android/server/location/GpsLocationProvider$1
        //     111: dup
        //     112: aload_0
        //     113: invokespecial 297	com/android/server/location/GpsLocationProvider$1:<init>	(Lcom/android/server/location/GpsLocationProvider;)V
        //     116: putfield 299	com/android/server/location/GpsLocationProvider:mGpsStatusProvider	Landroid/location/IGpsStatusProvider;
        //     119: aload_0
        //     120: new 10	com/android/server/location/GpsLocationProvider$2
        //     123: dup
        //     124: aload_0
        //     125: invokespecial 300	com/android/server/location/GpsLocationProvider$2:<init>	(Lcom/android/server/location/GpsLocationProvider;)V
        //     128: putfield 302	com/android/server/location/GpsLocationProvider:mBroadcastReciever	Landroid/content/BroadcastReceiver;
        //     131: aload_0
        //     132: new 12	com/android/server/location/GpsLocationProvider$3
        //     135: dup
        //     136: aload_0
        //     137: invokespecial 303	com/android/server/location/GpsLocationProvider$3:<init>	(Lcom/android/server/location/GpsLocationProvider;)V
        //     140: putfield 305	com/android/server/location/GpsLocationProvider:mNetInitiatedListener	Landroid/location/INetInitiatedListener;
        //     143: aload_0
        //     144: bipush 32
        //     146: newarray int
        //     148: putfield 307	com/android/server/location/GpsLocationProvider:mSvs	[I
        //     151: aload_0
        //     152: bipush 32
        //     154: newarray float
        //     156: putfield 309	com/android/server/location/GpsLocationProvider:mSnrs	[F
        //     159: aload_0
        //     160: bipush 32
        //     162: newarray float
        //     164: putfield 311	com/android/server/location/GpsLocationProvider:mSvElevations	[F
        //     167: aload_0
        //     168: bipush 32
        //     170: newarray float
        //     172: putfield 313	com/android/server/location/GpsLocationProvider:mSvAzimuths	[F
        //     175: aload_0
        //     176: iconst_3
        //     177: newarray int
        //     179: putfield 315	com/android/server/location/GpsLocationProvider:mSvMasks	[I
        //     182: aload_0
        //     183: bipush 120
        //     185: newarray byte
        //     187: putfield 317	com/android/server/location/GpsLocationProvider:mNmeaBuffer	[B
        //     190: aload_0
        //     191: aload_1
        //     192: putfield 319	com/android/server/location/GpsLocationProvider:mContext	Landroid/content/Context;
        //     195: aload_0
        //     196: aload_1
        //     197: invokestatic 325	android/util/NtpTrustedTime:getInstance	(Landroid/content/Context;)Landroid/util/NtpTrustedTime;
        //     200: putfield 327	com/android/server/location/GpsLocationProvider:mNtpTime	Landroid/util/NtpTrustedTime;
        //     203: aload_0
        //     204: aload_2
        //     205: putfield 329	com/android/server/location/GpsLocationProvider:mLocationManager	Landroid/location/ILocationManager;
        //     208: aload_0
        //     209: new 331	com/android/internal/location/GpsNetInitiatedHandler
        //     212: dup
        //     213: aload_1
        //     214: invokespecial 334	com/android/internal/location/GpsNetInitiatedHandler:<init>	(Landroid/content/Context;)V
        //     217: putfield 336	com/android/server/location/GpsLocationProvider:mNIHandler	Lcom/android/internal/location/GpsNetInitiatedHandler;
        //     220: aload_0
        //     221: getfield 272	com/android/server/location/GpsLocationProvider:mLocation	Landroid/location/Location;
        //     224: aload_0
        //     225: getfield 277	com/android/server/location/GpsLocationProvider:mLocationExtras	Landroid/os/Bundle;
        //     228: invokevirtual 340	android/location/Location:setExtras	(Landroid/os/Bundle;)V
        //     231: aload_0
        //     232: aload_0
        //     233: getfield 319	com/android/server/location/GpsLocationProvider:mContext	Landroid/content/Context;
        //     236: ldc_w 342
        //     239: invokevirtual 348	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
        //     242: checkcast 350	android/os/PowerManager
        //     245: iconst_1
        //     246: ldc 138
        //     248: invokevirtual 354	android/os/PowerManager:newWakeLock	(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;
        //     251: putfield 356	com/android/server/location/GpsLocationProvider:mWakeLock	Landroid/os/PowerManager$WakeLock;
        //     254: aload_0
        //     255: getfield 356	com/android/server/location/GpsLocationProvider:mWakeLock	Landroid/os/PowerManager$WakeLock;
        //     258: iconst_0
        //     259: invokevirtual 362	android/os/PowerManager$WakeLock:setReferenceCounted	(Z)V
        //     262: aload_0
        //     263: aload_0
        //     264: getfield 319	com/android/server/location/GpsLocationProvider:mContext	Landroid/content/Context;
        //     267: ldc_w 364
        //     270: invokevirtual 348	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
        //     273: checkcast 366	android/app/AlarmManager
        //     276: putfield 368	com/android/server/location/GpsLocationProvider:mAlarmManager	Landroid/app/AlarmManager;
        //     279: aload_0
        //     280: aload_0
        //     281: getfield 319	com/android/server/location/GpsLocationProvider:mContext	Landroid/content/Context;
        //     284: iconst_0
        //     285: new 370	android/content/Intent
        //     288: dup
        //     289: ldc 53
        //     291: invokespecial 371	android/content/Intent:<init>	(Ljava/lang/String;)V
        //     294: iconst_0
        //     295: invokestatic 377	android/app/PendingIntent:getBroadcast	(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
        //     298: putfield 379	com/android/server/location/GpsLocationProvider:mWakeupIntent	Landroid/app/PendingIntent;
        //     301: aload_0
        //     302: aload_0
        //     303: getfield 319	com/android/server/location/GpsLocationProvider:mContext	Landroid/content/Context;
        //     306: iconst_0
        //     307: new 370	android/content/Intent
        //     310: dup
        //     311: ldc 50
        //     313: invokespecial 371	android/content/Intent:<init>	(Ljava/lang/String;)V
        //     316: iconst_0
        //     317: invokestatic 377	android/app/PendingIntent:getBroadcast	(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
        //     320: putfield 381	com/android/server/location/GpsLocationProvider:mTimeoutIntent	Landroid/app/PendingIntent;
        //     323: new 383	android/content/IntentFilter
        //     326: dup
        //     327: invokespecial 384	android/content/IntentFilter:<init>	()V
        //     330: astore_3
        //     331: aload_3
        //     332: ldc_w 386
        //     335: invokevirtual 389	android/content/IntentFilter:addAction	(Ljava/lang/String;)V
        //     338: aload_3
        //     339: ldc_w 391
        //     342: invokevirtual 394	android/content/IntentFilter:addDataScheme	(Ljava/lang/String;)V
        //     345: aload_3
        //     346: ldc_w 396
        //     349: ldc_w 398
        //     352: invokevirtual 402	android/content/IntentFilter:addDataAuthority	(Ljava/lang/String;Ljava/lang/String;)V
        //     355: aload_1
        //     356: aload_0
        //     357: getfield 302	com/android/server/location/GpsLocationProvider:mBroadcastReciever	Landroid/content/BroadcastReceiver;
        //     360: aload_3
        //     361: invokevirtual 406	android/content/Context:registerReceiver	(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
        //     364: pop
        //     365: new 383	android/content/IntentFilter
        //     368: dup
        //     369: invokespecial 384	android/content/IntentFilter:<init>	()V
        //     372: astore 5
        //     374: aload 5
        //     376: ldc_w 408
        //     379: invokevirtual 389	android/content/IntentFilter:addAction	(Ljava/lang/String;)V
        //     382: aload 5
        //     384: ldc_w 410
        //     387: invokevirtual 413	android/content/IntentFilter:addDataType	(Ljava/lang/String;)V
        //     390: aload_1
        //     391: aload_0
        //     392: getfield 302	com/android/server/location/GpsLocationProvider:mBroadcastReciever	Landroid/content/BroadcastReceiver;
        //     395: aload 5
        //     397: invokevirtual 406	android/content/Context:registerReceiver	(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
        //     400: pop
        //     401: aload_0
        //     402: aload_1
        //     403: ldc_w 415
        //     406: invokevirtual 348	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
        //     409: checkcast 417	android/net/ConnectivityManager
        //     412: putfield 419	com/android/server/location/GpsLocationProvider:mConnMgr	Landroid/net/ConnectivityManager;
        //     415: aload_0
        //     416: ldc_w 421
        //     419: invokestatic 427	android/os/ServiceManager:getService	(Ljava/lang/String;)Landroid/os/IBinder;
        //     422: invokestatic 433	com/android/internal/app/IBatteryStats$Stub:asInterface	(Landroid/os/IBinder;)Lcom/android/internal/app/IBatteryStats;
        //     425: putfield 435	com/android/server/location/GpsLocationProvider:mBatteryStats	Lcom/android/internal/app/IBatteryStats;
        //     428: aload_0
        //     429: new 437	java/util/Properties
        //     432: dup
        //     433: invokespecial 438	java/util/Properties:<init>	()V
        //     436: putfield 440	com/android/server/location/GpsLocationProvider:mProperties	Ljava/util/Properties;
        //     439: new 442	java/io/FileInputStream
        //     442: dup
        //     443: new 444	java/io/File
        //     446: dup
        //     447: ldc 125
        //     449: invokespecial 445	java/io/File:<init>	(Ljava/lang/String;)V
        //     452: invokespecial 448	java/io/FileInputStream:<init>	(Ljava/io/File;)V
        //     455: astore 9
        //     457: aload_0
        //     458: getfield 440	com/android/server/location/GpsLocationProvider:mProperties	Ljava/util/Properties;
        //     461: aload 9
        //     463: invokevirtual 452	java/util/Properties:load	(Ljava/io/InputStream;)V
        //     466: aload 9
        //     468: invokevirtual 455	java/io/FileInputStream:close	()V
        //     471: aload_0
        //     472: aload_0
        //     473: getfield 440	com/android/server/location/GpsLocationProvider:mProperties	Ljava/util/Properties;
        //     476: ldc_w 457
        //     479: invokevirtual 461	java/util/Properties:getProperty	(Ljava/lang/String;)Ljava/lang/String;
        //     482: putfield 463	com/android/server/location/GpsLocationProvider:mSuplServerHost	Ljava/lang/String;
        //     485: aload_0
        //     486: getfield 440	com/android/server/location/GpsLocationProvider:mProperties	Ljava/util/Properties;
        //     489: ldc_w 465
        //     492: invokevirtual 461	java/util/Properties:getProperty	(Ljava/lang/String;)Ljava/lang/String;
        //     495: astore 13
        //     497: aload_0
        //     498: getfield 463	com/android/server/location/GpsLocationProvider:mSuplServerHost	Ljava/lang/String;
        //     501: astore 14
        //     503: aload 14
        //     505: ifnull +17 -> 522
        //     508: aload 13
        //     510: ifnull +12 -> 522
        //     513: aload_0
        //     514: aload 13
        //     516: invokestatic 471	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     519: putfield 473	com/android/server/location/GpsLocationProvider:mSuplServerPort	I
        //     522: aload_0
        //     523: aload_0
        //     524: getfield 440	com/android/server/location/GpsLocationProvider:mProperties	Ljava/util/Properties;
        //     527: ldc_w 475
        //     530: invokevirtual 461	java/util/Properties:getProperty	(Ljava/lang/String;)Ljava/lang/String;
        //     533: putfield 477	com/android/server/location/GpsLocationProvider:mC2KServerHost	Ljava/lang/String;
        //     536: aload_0
        //     537: getfield 440	com/android/server/location/GpsLocationProvider:mProperties	Ljava/util/Properties;
        //     540: ldc_w 479
        //     543: invokevirtual 461	java/util/Properties:getProperty	(Ljava/lang/String;)Ljava/lang/String;
        //     546: astore 15
        //     548: aload_0
        //     549: getfield 477	com/android/server/location/GpsLocationProvider:mC2KServerHost	Ljava/lang/String;
        //     552: astore 16
        //     554: aload 16
        //     556: ifnull +17 -> 573
        //     559: aload 15
        //     561: ifnull +12 -> 573
        //     564: aload_0
        //     565: aload 15
        //     567: invokestatic 471	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     570: putfield 481	com/android/server/location/GpsLocationProvider:mC2KServerPort	I
        //     573: aload_0
        //     574: new 14	com/android/server/location/GpsLocationProvider$GpsLocationProviderThread
        //     577: dup
        //     578: aload_0
        //     579: invokespecial 482	com/android/server/location/GpsLocationProvider$GpsLocationProviderThread:<init>	(Lcom/android/server/location/GpsLocationProvider;)V
        //     582: putfield 484	com/android/server/location/GpsLocationProvider:mThread	Ljava/lang/Thread;
        //     585: aload_0
        //     586: getfield 484	com/android/server/location/GpsLocationProvider:mThread	Ljava/lang/Thread;
        //     589: invokevirtual 489	java/lang/Thread:start	()V
        //     592: aload_0
        //     593: getfield 289	com/android/server/location/GpsLocationProvider:mInitializedLatch	Ljava/util/concurrent/CountDownLatch;
        //     596: invokevirtual 492	java/util/concurrent/CountDownLatch:await	()V
        //     599: return
        //     600: astore 6
        //     602: ldc 138
        //     604: ldc_w 494
        //     607: invokestatic 500	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     610: pop
        //     611: goto -221 -> 390
        //     614: astore 19
        //     616: ldc 138
        //     618: new 502	java/lang/StringBuilder
        //     621: dup
        //     622: invokespecial 503	java/lang/StringBuilder:<init>	()V
        //     625: ldc_w 505
        //     628: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     631: aload 13
        //     633: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     636: invokevirtual 513	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     639: invokestatic 516	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     642: pop
        //     643: goto -121 -> 522
        //     646: astore 10
        //     648: ldc 138
        //     650: ldc_w 518
        //     653: invokestatic 500	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     656: pop
        //     657: goto -84 -> 573
        //     660: astore 17
        //     662: ldc 138
        //     664: new 502	java/lang/StringBuilder
        //     667: dup
        //     668: invokespecial 503	java/lang/StringBuilder:<init>	()V
        //     671: ldc_w 520
        //     674: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     677: aload 15
        //     679: invokevirtual 509	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     682: invokevirtual 513	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     685: invokestatic 516	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     688: pop
        //     689: goto -116 -> 573
        //     692: astore 12
        //     694: invokestatic 524	java/lang/Thread:currentThread	()Ljava/lang/Thread;
        //     697: invokevirtual 527	java/lang/Thread:interrupt	()V
        //     700: goto -108 -> 592
        //
        // Exception table:
        //     from	to	target	type
        //     382	390	600	android/content/IntentFilter$MalformedMimeTypeException
        //     513	522	614	java/lang/NumberFormatException
        //     439	503	646	java/io/IOException
        //     513	522	646	java/io/IOException
        //     522	554	646	java/io/IOException
        //     564	573	646	java/io/IOException
        //     616	643	646	java/io/IOException
        //     662	689	646	java/io/IOException
        //     564	573	660	java/lang/NumberFormatException
        //     592	599	692	java/lang/InterruptedException
    }

    private void checkSmsSuplInit(Intent paramIntent)
    {
        SmsMessage[] arrayOfSmsMessage = Telephony.Sms.Intents.getMessagesFromIntent(paramIntent);
        for (int i = 0; i < arrayOfSmsMessage.length; i++)
        {
            byte[] arrayOfByte = arrayOfSmsMessage[i].getUserData();
            native_agps_ni_message(arrayOfByte, arrayOfByte.length);
        }
    }

    private void checkWapSuplInit(Intent paramIntent)
    {
        byte[] arrayOfByte = (byte[])paramIntent.getExtra("data");
        native_agps_ni_message(arrayOfByte, arrayOfByte.length);
    }

    private static native void class_init_native();

    private boolean deleteAidingData(Bundle paramBundle)
    {
        int i;
        if (paramBundle == null)
        {
            i = 65535;
            if (i == 0)
                break label221;
            native_delete_aiding_data(i);
        }
        label221: for (boolean bool = true; ; bool = false)
        {
            return bool;
            i = 0;
            if (paramBundle.getBoolean("ephemeris"))
                i = 0x0 | 0x1;
            if (paramBundle.getBoolean("almanac"))
                i |= 2;
            if (paramBundle.getBoolean("position"))
                i |= 4;
            if (paramBundle.getBoolean("time"))
                i |= 8;
            if (paramBundle.getBoolean("iono"))
                i |= 16;
            if (paramBundle.getBoolean("utc"))
                i |= 32;
            if (paramBundle.getBoolean("health"))
                i |= 64;
            if (paramBundle.getBoolean("svdir"))
                i |= 128;
            if (paramBundle.getBoolean("svsteer"))
                i |= 256;
            if (paramBundle.getBoolean("sadata"))
                i |= 512;
            if (paramBundle.getBoolean("rti"))
                i |= 1024;
            if (paramBundle.getBoolean("celldb-info"))
                i |= 32768;
            if (!paramBundle.getBoolean("all"))
                break;
            i |= 65535;
            break;
        }
    }

    private String getSelectedApn()
    {
        Uri localUri = Uri.parse("content://telephony/carriers/preferapn");
        Object localObject1 = null;
        ContentResolver localContentResolver = this.mContext.getContentResolver();
        String[] arrayOfString = new String[1];
        arrayOfString[0] = "apn";
        Cursor localCursor = localContentResolver.query(localUri, arrayOfString, null, null, "name ASC");
        if (localCursor != null);
        try
        {
            if (localCursor.moveToFirst())
            {
                String str = localCursor.getString(0);
                localObject1 = str;
            }
            return localObject1;
        }
        finally
        {
            localCursor.close();
        }
    }

    private void handleAddListener(int paramInt)
    {
        synchronized (this.mListeners)
        {
            if (this.mClientUids.indexOfKey(paramInt) >= 0)
            {
                Log.w("GpsLocationProvider", "Duplicate add listener for uid " + paramInt);
                return;
            }
            this.mClientUids.put(paramInt, 0);
            boolean bool = this.mNavigating;
            if (!bool);
        }
        try
        {
            this.mBatteryStats.noteStartGps(paramInt);
            return;
            localObject = finally;
            throw localObject;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w("GpsLocationProvider", "RemoteException in addListener");
        }
    }

    private void handleDisable()
    {
        if (!this.mEnabled);
        while (true)
        {
            return;
            this.mEnabled = false;
            stopNavigating();
            native_cleanup();
        }
    }

    private void handleDownloadXtraData()
    {
        if (!this.mNetworkAvailable)
            this.mDownloadXtraDataPending = true;
        while (true)
        {
            return;
            this.mDownloadXtraDataPending = false;
            byte[] arrayOfByte = new GpsXtraDownloader(this.mContext, this.mProperties).downloadXtraData();
            if (arrayOfByte != null)
            {
                native_inject_xtra_data(arrayOfByte, arrayOfByte.length);
            }
            else
            {
                this.mHandler.removeMessages(6);
                this.mHandler.sendMessageDelayed(Message.obtain(this.mHandler, 6), 300000L);
            }
        }
    }

    private void handleEnable()
    {
        if (this.mEnabled);
        while (true)
        {
            return;
            this.mEnabled = native_init();
            if (this.mEnabled)
            {
                this.mSupportsXtra = native_supports_xtra();
                if (this.mSuplServerHost != null)
                    native_set_agps_server(1, this.mSuplServerHost, this.mSuplServerPort);
                if (this.mC2KServerHost != null)
                    native_set_agps_server(2, this.mC2KServerHost, this.mC2KServerPort);
            }
            else
            {
                Log.w("GpsLocationProvider", "Failed to enable location provider");
            }
        }
    }

    private void handleEnableLocationTracking(boolean paramBoolean)
    {
        if (paramBoolean)
        {
            this.mTTFF = 0;
            this.mLastFixTime = 0L;
            startNavigating(false);
        }
        while (true)
        {
            return;
            if (!hasCapability(1))
            {
                this.mAlarmManager.cancel(this.mWakeupIntent);
                this.mAlarmManager.cancel(this.mTimeoutIntent);
            }
            stopNavigating();
        }
    }

    private void handleInjectNtpTime()
    {
        if (!this.mNetworkAvailable)
            this.mInjectNtpTimePending = true;
        label222: 
        while (true)
        {
            return;
            this.mInjectNtpTimePending = false;
            if (this.mNtpTime.getCacheAge() >= 86400000L)
                this.mNtpTime.forceRefresh();
            if (this.mNtpTime.getCacheAge() < 86400000L)
            {
                long l2 = this.mNtpTime.getCachedNtpTime();
                long l3 = this.mNtpTime.getCachedNtpTimeReference();
                long l4 = this.mNtpTime.getCacheCertainty();
                long l5 = System.currentTimeMillis();
                Log.d("GpsLocationProvider", "NTP server returned: " + l2 + " (" + new Date(l2) + ") reference: " + l3 + " certainty: " + l4 + " system time offset: " + (l2 - l5));
                native_inject_time(l2, l3, (int)l4);
            }
            for (long l1 = 86400000L; ; l1 = 300000L)
            {
                if (!this.mPeriodicTimeInjection)
                    break label222;
                this.mHandler.removeMessages(5);
                this.mHandler.sendMessageDelayed(Message.obtain(this.mHandler, 5), l1);
                break;
            }
        }
    }

    private void handleRemoveListener(int paramInt)
    {
        synchronized (this.mListeners)
        {
            if (this.mClientUids.indexOfKey(paramInt) < 0)
            {
                Log.w("GpsLocationProvider", "Unneeded remove listener for uid " + paramInt);
                return;
            }
            this.mClientUids.delete(paramInt);
            boolean bool = this.mNavigating;
            if (!bool);
        }
        try
        {
            this.mBatteryStats.noteStopGps(paramInt);
            return;
            localObject = finally;
            throw localObject;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.w("GpsLocationProvider", "RemoteException in removeListener");
        }
    }

    private void handleRequestSingleShot()
    {
        this.mTTFF = 0;
        this.mLastFixTime = 0L;
        startNavigating(true);
    }

    private void handleUpdateLocation(Location paramLocation)
    {
        if (paramLocation.hasAccuracy())
            native_inject_location(paramLocation.getLatitude(), paramLocation.getLongitude(), paramLocation.getAccuracy());
    }

    private void handleUpdateNetworkState(int paramInt, NetworkInfo paramNetworkInfo)
    {
        boolean bool1;
        int i;
        label37: boolean bool2;
        if (paramInt == 2)
        {
            bool1 = true;
            this.mNetworkAvailable = bool1;
            if (paramNetworkInfo != null)
            {
                if (Settings.Secure.getInt(this.mContext.getContentResolver(), "mobile_data", 1) != 1)
                    break label227;
                i = 1;
                if ((!paramNetworkInfo.isAvailable()) || (i == 0))
                    break label233;
                bool2 = true;
                label52: String str2 = getSelectedApn();
                if (str2 == null)
                    str2 = "dummy-apn";
                native_update_network_state(paramNetworkInfo.isConnected(), paramNetworkInfo.getType(), paramNetworkInfo.isRoaming(), bool2, paramNetworkInfo.getExtraInfo(), str2);
            }
            if ((paramNetworkInfo != null) && (paramNetworkInfo.getType() == 3) && (this.mAGpsDataConnectionState == 1))
            {
                String str1 = paramNetworkInfo.getExtraInfo();
                if (!this.mNetworkAvailable)
                    break label239;
                if (str1 == null)
                    str1 = "dummy-apn";
                this.mAGpsApn = str1;
                if ((this.mAGpsDataConnectionIpAddr != -1) && (!this.mConnMgr.requestRouteToHost(3, this.mAGpsDataConnectionIpAddr)))
                    Log.d("GpsLocationProvider", "call requestRouteToHost failed");
                native_agps_data_conn_open(str1);
                this.mAGpsDataConnectionState = 2;
            }
        }
        while (true)
        {
            if (this.mNetworkAvailable)
            {
                if (this.mInjectNtpTimePending)
                    sendMessage(5, 0, null);
                if (this.mDownloadXtraDataPending)
                    sendMessage(6, 0, null);
            }
            return;
            bool1 = false;
            break;
            label227: i = 0;
            break label37;
            label233: bool2 = false;
            break label52;
            label239: this.mAGpsApn = null;
            this.mAGpsDataConnectionState = 0;
            native_agps_data_conn_failed();
        }
    }

    private boolean hasCapability(int paramInt)
    {
        if ((paramInt & this.mEngineCapabilities) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void hibernate()
    {
        stopNavigating();
        this.mAlarmManager.cancel(this.mTimeoutIntent);
        this.mAlarmManager.cancel(this.mWakeupIntent);
        SystemClock.elapsedRealtime();
        this.mAlarmManager.set(2, SystemClock.elapsedRealtime() + this.mFixInterval, this.mWakeupIntent);
    }

    private void initialize()
    {
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("com.android.internal.location.ALARM_WAKEUP");
        localIntentFilter.addAction("com.android.internal.location.ALARM_TIMEOUT");
        this.mContext.registerReceiver(this.mBroadcastReciever, localIntentFilter);
    }

    public static boolean isSupported()
    {
        return native_is_supported();
    }

    private native void native_agps_data_conn_closed();

    private native void native_agps_data_conn_failed();

    private native void native_agps_data_conn_open(String paramString);

    private native void native_agps_ni_message(byte[] paramArrayOfByte, int paramInt);

    private native void native_agps_set_id(int paramInt, String paramString);

    private native void native_agps_set_ref_location_cellid(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5);

    private native void native_cleanup();

    private native void native_delete_aiding_data(int paramInt);

    private native String native_get_internal_state();

    private native boolean native_init();

    private native void native_inject_location(double paramDouble1, double paramDouble2, float paramFloat);

    private native void native_inject_time(long paramLong1, long paramLong2, int paramInt);

    private native void native_inject_xtra_data(byte[] paramArrayOfByte, int paramInt);

    private static native boolean native_is_supported();

    private native int native_read_nmea(byte[] paramArrayOfByte, int paramInt);

    private native int native_read_sv_status(int[] paramArrayOfInt1, float[] paramArrayOfFloat1, float[] paramArrayOfFloat2, float[] paramArrayOfFloat3, int[] paramArrayOfInt2);

    private native void native_send_ni_response(int paramInt1, int paramInt2);

    private native void native_set_agps_server(int paramInt1, String paramString, int paramInt2);

    private native boolean native_set_position_mode(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5);

    private native boolean native_start();

    private native boolean native_stop();

    private native boolean native_supports_xtra();

    private native void native_update_network_state(boolean paramBoolean1, int paramInt, boolean paramBoolean2, boolean paramBoolean3, String paramString1, String paramString2);

    private void reportAGpsStatus(int paramInt1, int paramInt2, int paramInt3)
    {
        switch (paramInt2)
        {
        case 3:
        case 4:
        default:
        case 1:
        case 2:
        }
        while (true)
        {
            return;
            this.mAGpsDataConnectionState = 1;
            int i = this.mConnMgr.startUsingNetworkFeature(0, "enableSUPL");
            this.mAGpsDataConnectionIpAddr = paramInt3;
            if (i == 0)
            {
                if (this.mAGpsApn != null)
                {
                    Log.d("GpsLocationProvider", "mAGpsDataConnectionIpAddr " + this.mAGpsDataConnectionIpAddr);
                    if ((this.mAGpsDataConnectionIpAddr != -1) && (!this.mConnMgr.requestRouteToHost(3, this.mAGpsDataConnectionIpAddr)))
                        Log.d("GpsLocationProvider", "call requestRouteToHost failed");
                    native_agps_data_conn_open(this.mAGpsApn);
                    this.mAGpsDataConnectionState = 2;
                }
                else
                {
                    Log.e("GpsLocationProvider", "mAGpsApn not set when receiving Phone.APN_ALREADY_ACTIVE");
                    this.mAGpsDataConnectionState = 0;
                    native_agps_data_conn_failed();
                }
            }
            else if (i != 1)
            {
                this.mAGpsDataConnectionState = 0;
                native_agps_data_conn_failed();
                continue;
                if (this.mAGpsDataConnectionState != 0)
                {
                    this.mConnMgr.stopUsingNetworkFeature(0, "enableSUPL");
                    native_agps_data_conn_closed();
                    this.mAGpsDataConnectionState = 0;
                }
            }
        }
    }

    // ERROR //
    private void reportLocation(int paramInt, double paramDouble1, double paramDouble2, double paramDouble3, float paramFloat1, float paramFloat2, float paramFloat3, long paramLong)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 272	com/android/server/location/GpsLocationProvider:mLocation	Landroid/location/Location;
        //     4: astore 13
        //     6: aload 13
        //     8: monitorenter
        //     9: aload_0
        //     10: iload_1
        //     11: putfield 243	com/android/server/location/GpsLocationProvider:mLocationFlags	I
        //     14: iload_1
        //     15: iconst_1
        //     16: iand
        //     17: iconst_1
        //     18: if_icmpne +29 -> 47
        //     21: aload_0
        //     22: getfield 272	com/android/server/location/GpsLocationProvider:mLocation	Landroid/location/Location;
        //     25: dload_2
        //     26: invokevirtual 980	android/location/Location:setLatitude	(D)V
        //     29: aload_0
        //     30: getfield 272	com/android/server/location/GpsLocationProvider:mLocation	Landroid/location/Location;
        //     33: dload 4
        //     35: invokevirtual 983	android/location/Location:setLongitude	(D)V
        //     38: aload_0
        //     39: getfield 272	com/android/server/location/GpsLocationProvider:mLocation	Landroid/location/Location;
        //     42: lload 11
        //     44: invokevirtual 986	android/location/Location:setTime	(J)V
        //     47: iload_1
        //     48: iconst_2
        //     49: iand
        //     50: iconst_2
        //     51: if_icmpne +189 -> 240
        //     54: aload_0
        //     55: getfield 272	com/android/server/location/GpsLocationProvider:mLocation	Landroid/location/Location;
        //     58: dload 6
        //     60: invokevirtual 989	android/location/Location:setAltitude	(D)V
        //     63: iload_1
        //     64: iconst_4
        //     65: iand
        //     66: iconst_4
        //     67: if_icmpne +191 -> 258
        //     70: aload_0
        //     71: getfield 272	com/android/server/location/GpsLocationProvider:mLocation	Landroid/location/Location;
        //     74: fload 8
        //     76: invokevirtual 993	android/location/Location:setSpeed	(F)V
        //     79: iload_1
        //     80: bipush 8
        //     82: iand
        //     83: bipush 8
        //     85: if_icmpne +183 -> 268
        //     88: aload_0
        //     89: getfield 272	com/android/server/location/GpsLocationProvider:mLocation	Landroid/location/Location;
        //     92: fload 9
        //     94: invokevirtual 996	android/location/Location:setBearing	(F)V
        //     97: iload_1
        //     98: bipush 16
        //     100: iand
        //     101: bipush 16
        //     103: if_icmpne +175 -> 278
        //     106: aload_0
        //     107: getfield 272	com/android/server/location/GpsLocationProvider:mLocation	Landroid/location/Location;
        //     110: fload 10
        //     112: invokevirtual 999	android/location/Location:setAccuracy	(F)V
        //     115: aload_0
        //     116: getfield 272	com/android/server/location/GpsLocationProvider:mLocation	Landroid/location/Location;
        //     119: aload_0
        //     120: getfield 277	com/android/server/location/GpsLocationProvider:mLocationExtras	Landroid/os/Bundle;
        //     123: invokevirtual 340	android/location/Location:setExtras	(Landroid/os/Bundle;)V
        //     126: aload_0
        //     127: getfield 329	com/android/server/location/GpsLocationProvider:mLocationManager	Landroid/location/ILocationManager;
        //     130: aload_0
        //     131: getfield 272	com/android/server/location/GpsLocationProvider:mLocation	Landroid/location/Location;
        //     134: iconst_0
        //     135: invokeinterface 1004 3 0
        //     140: aload 13
        //     142: monitorexit
        //     143: aload_0
        //     144: invokestatic 819	java/lang/System:currentTimeMillis	()J
        //     147: putfield 791	com/android/server/location/GpsLocationProvider:mLastFixTime	J
        //     150: aload_0
        //     151: getfield 263	com/android/server/location/GpsLocationProvider:mTTFF	I
        //     154: ifne +178 -> 332
        //     157: iload_1
        //     158: iconst_1
        //     159: iand
        //     160: iconst_1
        //     161: if_icmpne +171 -> 332
        //     164: aload_0
        //     165: aload_0
        //     166: getfield 791	com/android/server/location/GpsLocationProvider:mLastFixTime	J
        //     169: aload_0
        //     170: getfield 261	com/android/server/location/GpsLocationProvider:mFixRequestTime	J
        //     173: lsub
        //     174: l2i
        //     175: putfield 263	com/android/server/location/GpsLocationProvider:mTTFF	I
        //     178: aload_0
        //     179: getfield 282	com/android/server/location/GpsLocationProvider:mListeners	Ljava/util/ArrayList;
        //     182: astore 19
        //     184: aload 19
        //     186: monitorenter
        //     187: aload_0
        //     188: getfield 282	com/android/server/location/GpsLocationProvider:mListeners	Ljava/util/ArrayList;
        //     191: invokevirtual 1007	java/util/ArrayList:size	()I
        //     194: istore 21
        //     196: iconst_0
        //     197: istore 22
        //     199: iload 22
        //     201: iload 21
        //     203: if_icmpge +126 -> 329
        //     206: aload_0
        //     207: getfield 282	com/android/server/location/GpsLocationProvider:mListeners	Ljava/util/ArrayList;
        //     210: iload 22
        //     212: invokevirtual 1011	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     215: checkcast 20	com/android/server/location/GpsLocationProvider$Listener
        //     218: astore 23
        //     220: aload 23
        //     222: getfield 1015	com/android/server/location/GpsLocationProvider$Listener:mListener	Landroid/location/IGpsStatusListener;
        //     225: aload_0
        //     226: getfield 263	com/android/server/location/GpsLocationProvider:mTTFF	I
        //     229: invokeinterface 1020 2 0
        //     234: iinc 22 1
        //     237: goto -38 -> 199
        //     240: aload_0
        //     241: getfield 272	com/android/server/location/GpsLocationProvider:mLocation	Landroid/location/Location;
        //     244: invokevirtual 1023	android/location/Location:removeAltitude	()V
        //     247: goto -184 -> 63
        //     250: astore 14
        //     252: aload 13
        //     254: monitorexit
        //     255: aload 14
        //     257: athrow
        //     258: aload_0
        //     259: getfield 272	com/android/server/location/GpsLocationProvider:mLocation	Landroid/location/Location;
        //     262: invokevirtual 1026	android/location/Location:removeSpeed	()V
        //     265: goto -186 -> 79
        //     268: aload_0
        //     269: getfield 272	com/android/server/location/GpsLocationProvider:mLocation	Landroid/location/Location;
        //     272: invokevirtual 1029	android/location/Location:removeBearing	()V
        //     275: goto -178 -> 97
        //     278: aload_0
        //     279: getfield 272	com/android/server/location/GpsLocationProvider:mLocation	Landroid/location/Location;
        //     282: invokevirtual 1032	android/location/Location:removeAccuracy	()V
        //     285: goto -170 -> 115
        //     288: astore 15
        //     290: ldc 138
        //     292: ldc_w 1034
        //     295: invokestatic 516	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     298: pop
        //     299: goto -159 -> 140
        //     302: astore 24
        //     304: ldc 138
        //     306: ldc_w 1036
        //     309: invokestatic 500	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     312: pop
        //     313: aload_0
        //     314: getfield 282	com/android/server/location/GpsLocationProvider:mListeners	Ljava/util/ArrayList;
        //     317: aload 23
        //     319: invokevirtual 1040	java/util/ArrayList:remove	(Ljava/lang/Object;)Z
        //     322: pop
        //     323: iinc 21 255
        //     326: goto -92 -> 234
        //     329: aload 19
        //     331: monitorexit
        //     332: aload_0
        //     333: getfield 1042	com/android/server/location/GpsLocationProvider:mSingleShot	Z
        //     336: ifeq +7 -> 343
        //     339: aload_0
        //     340: invokespecial 746	com/android/server/location/GpsLocationProvider:stopNavigating	()V
        //     343: aload_0
        //     344: getfield 1044	com/android/server/location/GpsLocationProvider:mStarted	Z
        //     347: ifeq +79 -> 426
        //     350: aload_0
        //     351: getfield 245	com/android/server/location/GpsLocationProvider:mStatus	I
        //     354: iconst_2
        //     355: if_icmpeq +71 -> 426
        //     358: aload_0
        //     359: iconst_1
        //     360: invokespecial 795	com/android/server/location/GpsLocationProvider:hasCapability	(I)Z
        //     363: ifne +23 -> 386
        //     366: aload_0
        //     367: getfield 259	com/android/server/location/GpsLocationProvider:mFixInterval	I
        //     370: ldc 118
        //     372: if_icmpge +14 -> 386
        //     375: aload_0
        //     376: getfield 368	com/android/server/location/GpsLocationProvider:mAlarmManager	Landroid/app/AlarmManager;
        //     379: aload_0
        //     380: getfield 381	com/android/server/location/GpsLocationProvider:mTimeoutIntent	Landroid/app/PendingIntent;
        //     383: invokevirtual 799	android/app/AlarmManager:cancel	(Landroid/app/PendingIntent;)V
        //     386: new 370	android/content/Intent
        //     389: dup
        //     390: ldc_w 1046
        //     393: invokespecial 371	android/content/Intent:<init>	(Ljava/lang/String;)V
        //     396: astore 17
        //     398: aload 17
        //     400: ldc_w 1048
        //     403: iconst_1
        //     404: invokevirtual 1052	android/content/Intent:putExtra	(Ljava/lang/String;Z)Landroid/content/Intent;
        //     407: pop
        //     408: aload_0
        //     409: getfield 319	com/android/server/location/GpsLocationProvider:mContext	Landroid/content/Context;
        //     412: aload 17
        //     414: invokevirtual 1055	android/content/Context:sendBroadcast	(Landroid/content/Intent;)V
        //     417: aload_0
        //     418: iconst_2
        //     419: aload_0
        //     420: getfield 1057	com/android/server/location/GpsLocationProvider:mSvCount	I
        //     423: invokespecial 1060	com/android/server/location/GpsLocationProvider:updateStatus	(II)V
        //     426: aload_0
        //     427: iconst_1
        //     428: invokespecial 795	com/android/server/location/GpsLocationProvider:hasCapability	(I)Z
        //     431: ifne +24 -> 455
        //     434: aload_0
        //     435: getfield 1044	com/android/server/location/GpsLocationProvider:mStarted	Z
        //     438: ifeq +17 -> 455
        //     441: aload_0
        //     442: getfield 259	com/android/server/location/GpsLocationProvider:mFixInterval	I
        //     445: sipush 10000
        //     448: if_icmple +7 -> 455
        //     451: aload_0
        //     452: invokespecial 583	com/android/server/location/GpsLocationProvider:hibernate	()V
        //     455: return
        //     456: astore 20
        //     458: aload 19
        //     460: monitorexit
        //     461: aload 20
        //     463: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     9	126	250	finally
        //     126	140	250	finally
        //     140	143	250	finally
        //     240	255	250	finally
        //     258	299	250	finally
        //     126	140	288	android/os/RemoteException
        //     220	234	302	android/os/RemoteException
        //     187	220	456	finally
        //     220	234	456	finally
        //     304	332	456	finally
        //     458	461	456	finally
    }

    private void reportNmea(long paramLong)
    {
        synchronized (this.mListeners)
        {
            int i = this.mListeners.size();
            if (i > 0)
            {
                int j = native_read_nmea(this.mNmeaBuffer, this.mNmeaBuffer.length);
                String str = new String(this.mNmeaBuffer, 0, j);
                int k = 0;
                while (true)
                    if (k < i)
                    {
                        Listener localListener = (Listener)this.mListeners.get(k);
                        try
                        {
                            localListener.mListener.onNmeaReceived(paramLong, str);
                            k++;
                        }
                        catch (RemoteException localRemoteException)
                        {
                            while (true)
                            {
                                Log.w("GpsLocationProvider", "RemoteException in reportNmea");
                                this.mListeners.remove(localListener);
                                i--;
                            }
                        }
                    }
            }
            return;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private void reportStatus(int paramInt)
    {
        while (true)
        {
            int i;
            int j;
            label65: Listener localListener;
            synchronized (this.mListeners)
            {
                boolean bool = this.mNavigating;
                switch (paramInt)
                {
                default:
                    if (bool == this.mNavigating)
                        break label310;
                    i = this.mListeners.size();
                    j = 0;
                    if (j >= i)
                        break label196;
                    localListener = (Listener)this.mListeners.get(j);
                case 1:
                case 2:
                case 3:
                case 4:
                }
            }
            try
            {
                if (this.mNavigating)
                    localListener.mListener.onGpsStarted();
                while (true)
                {
                    j++;
                    break label65;
                    this.mNavigating = true;
                    this.mEngineOn = true;
                    break;
                    localObject = finally;
                    throw localObject;
                    this.mNavigating = false;
                    break;
                    this.mEngineOn = true;
                    break;
                    this.mEngineOn = false;
                    this.mNavigating = false;
                    break;
                    localListener.mListener.onGpsStopped();
                }
            }
            catch (RemoteException localRemoteException2)
            {
                while (true)
                {
                    Log.w("GpsLocationProvider", "RemoteException in reportStatus");
                    this.mListeners.remove(localListener);
                    i--;
                }
            }
        }
        while (true)
        {
            label196: int k;
            try
            {
                k = -1 + this.mClientUids.size();
                if (k >= 0)
                {
                    int m = this.mClientUids.keyAt(k);
                    if (this.mNavigating)
                        this.mBatteryStats.noteStartGps(m);
                    else
                        this.mBatteryStats.noteStopGps(m);
                }
            }
            catch (RemoteException localRemoteException1)
            {
                Log.w("GpsLocationProvider", "RemoteException in reportStatus");
                Intent localIntent = new Intent("android.location.GPS_ENABLED_CHANGE");
                localIntent.putExtra("enabled", this.mNavigating);
                Injector.appendUidExtra(this, localIntent);
                this.mContext.sendBroadcast(localIntent);
            }
            label310: return;
            k--;
        }
    }

    private void reportSvStatus()
    {
        int i = native_read_sv_status(this.mSvs, this.mSnrs, this.mSvElevations, this.mSvAzimuths, this.mSvMasks);
        synchronized (this.mListeners)
        {
            int j = this.mListeners.size();
            int k = 0;
            while (true)
                if (k < j)
                {
                    Listener localListener = (Listener)this.mListeners.get(k);
                    try
                    {
                        localListener.mListener.onSvStatusChanged(i, this.mSvs, this.mSnrs, this.mSvElevations, this.mSvAzimuths, this.mSvMasks[0], this.mSvMasks[1], this.mSvMasks[2]);
                        k++;
                    }
                    catch (RemoteException localRemoteException)
                    {
                        while (true)
                        {
                            Log.w("GpsLocationProvider", "RemoteException in reportSvInfo");
                            this.mListeners.remove(localListener);
                            j--;
                        }
                    }
                }
            updateStatus(this.mStatus, Integer.bitCount(this.mSvMasks[2]));
            if ((this.mNavigating) && (this.mStatus == 2) && (this.mLastFixTime > 0L) && (System.currentTimeMillis() - this.mLastFixTime > 10000L))
            {
                Intent localIntent = new Intent("android.location.GPS_FIX_CHANGE");
                localIntent.putExtra("enabled", false);
                this.mContext.sendBroadcast(localIntent);
                updateStatus(1, this.mSvCount);
            }
            return;
        }
    }

    private void requestRefLocation(int paramInt)
    {
        TelephonyManager localTelephonyManager = (TelephonyManager)this.mContext.getSystemService("phone");
        int m;
        if (localTelephonyManager.getPhoneType() == 1)
        {
            GsmCellLocation localGsmCellLocation = (GsmCellLocation)localTelephonyManager.getCellLocation();
            if ((localGsmCellLocation != null) && (localTelephonyManager.getPhoneType() == 1) && (localTelephonyManager.getNetworkOperator() != null) && (localTelephonyManager.getNetworkOperator().length() > 3))
            {
                int i = Integer.parseInt(localTelephonyManager.getNetworkOperator().substring(0, 3));
                int j = Integer.parseInt(localTelephonyManager.getNetworkOperator().substring(3));
                int k = localTelephonyManager.getNetworkType();
                if ((k == 3) || (k == 8) || (k == 9) || (k == 10))
                {
                    m = 2;
                    native_agps_set_ref_location_cellid(m, i, j, localGsmCellLocation.getLac(), localGsmCellLocation.getCid());
                }
            }
        }
        while (true)
        {
            return;
            m = 1;
            break;
            Log.e("GpsLocationProvider", "Error getting cell location info.");
            continue;
            Log.e("GpsLocationProvider", "CDMA not supported.");
        }
    }

    private void requestSetID(int paramInt)
    {
        TelephonyManager localTelephonyManager = (TelephonyManager)this.mContext.getSystemService("phone");
        int i = 0;
        Object localObject = "";
        String str2;
        if ((paramInt & 0x1) == 1)
        {
            str2 = localTelephonyManager.getSubscriberId();
            if (str2 != null);
        }
        while (true)
        {
            native_agps_set_id(i, (String)localObject);
            return;
            localObject = str2;
            i = 1;
            continue;
            if ((paramInt & 0x2) == 2)
            {
                String str1 = localTelephonyManager.getLine1Number();
                if (str1 != null)
                {
                    localObject = str1;
                    i = 2;
                }
            }
        }
    }

    private void requestUtcTime()
    {
        sendMessage(5, 0, null);
    }

    private void sendMessage(int paramInt1, int paramInt2, Object paramObject)
    {
        synchronized (this.mWakeLock)
        {
            this.mPendingMessageBits |= 1 << paramInt1;
            this.mWakeLock.acquire();
            this.mHandler.removeMessages(paramInt1);
            Message localMessage = Message.obtain(this.mHandler, paramInt1);
            localMessage.arg1 = paramInt2;
            localMessage.obj = paramObject;
            this.mHandler.sendMessage(localMessage);
            return;
        }
    }

    private void setEngineCapabilities(int paramInt)
    {
        this.mEngineCapabilities = paramInt;
        if ((!hasCapability(16)) && (!this.mPeriodicTimeInjection))
        {
            this.mPeriodicTimeInjection = true;
            requestUtcTime();
        }
    }

    private void startNavigating(boolean paramBoolean)
    {
        if (!this.mStarted)
        {
            this.mStarted = true;
            this.mSingleShot = paramBoolean;
            this.mPositionMode = 0;
            if (Settings.Secure.getInt(this.mContext.getContentResolver(), "assisted_gps_enabled", 1) != 0)
            {
                if ((!paramBoolean) || (!hasCapability(4)))
                    break label99;
                this.mPositionMode = 2;
            }
        }
        label196: 
        while (true)
        {
            int i;
            if (hasCapability(1))
            {
                i = this.mFixInterval;
                label69: if (native_set_position_mode(this.mPositionMode, 0, i, 0, 0))
                    break label122;
                this.mStarted = false;
                Log.e("GpsLocationProvider", "set_position_mode failed in startNavigating()");
            }
            while (true)
            {
                return;
                label99: if (!hasCapability(2))
                    break label196;
                this.mPositionMode = 1;
                break;
                i = 1000;
                break label69;
                label122: if (!native_start())
                {
                    this.mStarted = false;
                    Log.e("GpsLocationProvider", "native_start failed in startNavigating()");
                }
                else
                {
                    updateStatus(1, 0);
                    this.mFixRequestTime = System.currentTimeMillis();
                    if ((!hasCapability(1)) && (this.mFixInterval >= 60000))
                        this.mAlarmManager.set(2, 60000L + SystemClock.elapsedRealtime(), this.mTimeoutIntent);
                }
            }
        }
    }

    private void stopNavigating()
    {
        if (this.mStarted)
        {
            this.mStarted = false;
            this.mSingleShot = false;
            native_stop();
            this.mTTFF = 0;
            this.mLastFixTime = 0L;
            this.mLocationFlags = 0;
            updateStatus(1, 0);
        }
    }

    private void updateStatus(int paramInt1, int paramInt2)
    {
        if ((paramInt1 != this.mStatus) || (paramInt2 != this.mSvCount))
        {
            this.mStatus = paramInt1;
            this.mSvCount = paramInt2;
            this.mLocationExtras.putInt("satellites", paramInt2);
            this.mStatusUpdateTime = SystemClock.elapsedRealtime();
        }
    }

    private void xtraDownloadRequest()
    {
        sendMessage(6, 0, null);
    }

    public void addListener(int paramInt)
    {
        synchronized (this.mWakeLock)
        {
            this.mPendingListenerMessages = (1 + this.mPendingListenerMessages);
            this.mWakeLock.acquire();
            Message localMessage = Message.obtain(this.mHandler, 8);
            localMessage.arg1 = paramInt;
            this.mHandler.sendMessage(localMessage);
            return;
        }
    }

    public void disable()
    {
        synchronized (this.mHandler)
        {
            sendMessage(2, 0, null);
            return;
        }
    }

    public void enable()
    {
        synchronized (this.mHandler)
        {
            sendMessage(2, 1, null);
            return;
        }
    }

    public void enableLocationTracking(boolean paramBoolean)
    {
        Handler localHandler = this.mHandler;
        if (paramBoolean);
        for (int i = 1; ; i = 0)
            try
            {
                sendMessage(3, i, null);
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
    }

    public int getAccuracy()
    {
        return 1;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    SparseIntArray getClientUids()
    {
        return this.mClientUids;
    }

    public IGpsStatusProvider getGpsStatusProvider()
    {
        return this.mGpsStatusProvider;
    }

    public String getInternalState()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("    mFixInterval=").append(this.mFixInterval).append("\n");
        localStringBuilder.append("    mEngineCapabilities=0x").append(Integer.toHexString(this.mEngineCapabilities)).append(" (");
        if (hasCapability(1))
            localStringBuilder.append("SCHED ");
        if (hasCapability(2))
            localStringBuilder.append("MSB ");
        if (hasCapability(4))
            localStringBuilder.append("MSA ");
        if (hasCapability(8))
            localStringBuilder.append("SINGLE_SHOT ");
        if (hasCapability(16))
            localStringBuilder.append("ON_DEMAND_TIME ");
        localStringBuilder.append(")\n");
        localStringBuilder.append(native_get_internal_state());
        return localStringBuilder.toString();
    }

    public String getName()
    {
        return "gps";
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    boolean getNavigating()
    {
        return this.mNavigating;
    }

    public INetInitiatedListener getNetInitiatedListener()
    {
        return this.mNetInitiatedListener;
    }

    public int getPowerRequirement()
    {
        return 3;
    }

    public int getStatus(Bundle paramBundle)
    {
        if (paramBundle != null)
            paramBundle.putInt("satellites", this.mSvCount);
        return this.mStatus;
    }

    public long getStatusUpdateTime()
    {
        return this.mStatusUpdateTime;
    }

    public boolean hasMonetaryCost()
    {
        return false;
    }

    public boolean isEnabled()
    {
        return this.mEnabled;
    }

    public boolean meetsCriteria(Criteria paramCriteria)
    {
        int i = 1;
        if (paramCriteria.getPowerRequirement() != i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public void removeListener(int paramInt)
    {
        synchronized (this.mWakeLock)
        {
            this.mPendingListenerMessages = (1 + this.mPendingListenerMessages);
            this.mWakeLock.acquire();
            Message localMessage = Message.obtain(this.mHandler, 9);
            localMessage.arg1 = paramInt;
            this.mHandler.sendMessage(localMessage);
            return;
        }
    }

    public void reportNiNotification(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, String paramString1, String paramString2, int paramInt6, int paramInt7, String paramString3)
    {
        Log.i("GpsLocationProvider", "reportNiNotification: entered");
        Log.i("GpsLocationProvider", "notificationId: " + paramInt1 + ", niType: " + paramInt2 + ", notifyFlags: " + paramInt3 + ", timeout: " + paramInt4 + ", defaultResponse: " + paramInt5);
        Log.i("GpsLocationProvider", "requestorId: " + paramString1 + ", text: " + paramString2 + ", requestorIdEncoding: " + paramInt6 + ", textEncoding: " + paramInt7);
        GpsNetInitiatedHandler.GpsNiNotification localGpsNiNotification = new GpsNetInitiatedHandler.GpsNiNotification();
        localGpsNiNotification.notificationId = paramInt1;
        localGpsNiNotification.niType = paramInt2;
        boolean bool1;
        if ((paramInt3 & 0x1) != 0)
            bool1 = true;
        while (true)
        {
            localGpsNiNotification.needNotify = bool1;
            boolean bool2;
            label183: boolean bool3;
            Bundle localBundle;
            Properties localProperties;
            if ((paramInt3 & 0x2) != 0)
            {
                bool2 = true;
                localGpsNiNotification.needVerify = bool2;
                if ((paramInt3 & 0x4) == 0)
                    break label364;
                bool3 = true;
                localGpsNiNotification.privacyOverride = bool3;
                localGpsNiNotification.timeout = paramInt4;
                localGpsNiNotification.defaultResponse = paramInt5;
                localGpsNiNotification.requestorId = paramString1;
                localGpsNiNotification.text = paramString2;
                localGpsNiNotification.requestorIdEncoding = paramInt6;
                localGpsNiNotification.textEncoding = paramInt7;
                localBundle = new Bundle();
                if (paramString3 == null)
                    paramString3 = "";
                localProperties = new Properties();
            }
            try
            {
                localProperties.load(new StringReader(paramString3));
                Iterator localIterator = localProperties.entrySet().iterator();
                while (true)
                    if (localIterator.hasNext())
                    {
                        Map.Entry localEntry = (Map.Entry)localIterator.next();
                        localBundle.putString((String)localEntry.getKey(), (String)localEntry.getValue());
                        continue;
                        bool1 = false;
                        break;
                        bool2 = false;
                        break label183;
                        label364: bool3 = false;
                    }
            }
            catch (IOException localIOException)
            {
                while (true)
                    Log.e("GpsLocationProvider", "reportNiNotification cannot parse extras data: " + paramString3);
                localGpsNiNotification.extras = localBundle;
                this.mNIHandler.handleNiNotification(localGpsNiNotification);
            }
        }
    }

    public boolean requestSingleShotFix()
    {
        boolean bool;
        if (this.mStarted)
            bool = false;
        while (true)
        {
            return bool;
            synchronized (this.mHandler)
            {
                this.mHandler.removeMessages(10);
                Message localMessage = Message.obtain(this.mHandler, 10);
                this.mHandler.sendMessage(localMessage);
                bool = true;
            }
        }
    }

    public boolean requiresCell()
    {
        return false;
    }

    public boolean requiresNetwork()
    {
        return true;
    }

    public boolean requiresSatellite()
    {
        return true;
    }

    public boolean sendExtraCommand(String paramString, Bundle paramBundle)
    {
        long l = Binder.clearCallingIdentity();
        boolean bool = false;
        if ("delete_aiding_data".equals(paramString))
            bool = deleteAidingData(paramBundle);
        while (true)
        {
            Binder.restoreCallingIdentity(l);
            return bool;
            if ("force_time_injection".equals(paramString))
            {
                sendMessage(5, 0, null);
                bool = true;
            }
            else if ("force_xtra_injection".equals(paramString))
            {
                if (this.mSupportsXtra)
                {
                    xtraDownloadRequest();
                    bool = true;
                }
            }
            else
            {
                Log.w("GpsLocationProvider", "sendExtraCommand: unknown command " + paramString);
            }
        }
    }

    public void setMinTime(long paramLong, WorkSource paramWorkSource)
    {
        if (paramLong >= 0L)
        {
            this.mFixInterval = ((int)paramLong);
            if ((this.mStarted) && (hasCapability(1)) && (!native_set_position_mode(this.mPositionMode, 0, this.mFixInterval, 0, 0)))
                Log.e("GpsLocationProvider", "set_position_mode failed in setMinTime()");
        }
    }

    public boolean supportsAltitude()
    {
        return true;
    }

    public boolean supportsBearing()
    {
        return true;
    }

    public boolean supportsSpeed()
    {
        return true;
    }

    public void updateLocation(Location paramLocation)
    {
        sendMessage(7, 0, paramLocation);
    }

    public void updateNetworkState(int paramInt, NetworkInfo paramNetworkInfo)
    {
        sendMessage(4, paramInt, paramNetworkInfo);
    }

    private final class GpsLocationProviderThread extends Thread
    {
        public GpsLocationProviderThread()
        {
            super();
        }

        public void run()
        {
            Process.setThreadPriority(10);
            GpsLocationProvider.this.initialize();
            Looper.prepare();
            GpsLocationProvider.access$2102(GpsLocationProvider.this, new GpsLocationProvider.ProviderHandler(GpsLocationProvider.this, null));
            GpsLocationProvider.this.mInitializedLatch.countDown();
            Looper.loop();
        }
    }

    private final class ProviderHandler extends Handler
    {
        private ProviderHandler()
        {
        }

        public void handleMessage(Message paramMessage)
        {
            int i = paramMessage.what;
            switch (i)
            {
            default:
            case 2:
            case 3:
            case 10:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            }
            synchronized (GpsLocationProvider.this.mWakeLock)
            {
                while (true)
                {
                    GpsLocationProvider.access$1872(GpsLocationProvider.this, 0xFFFFFFFF ^ 1 << i);
                    if ((i == 8) || (i == 9))
                        GpsLocationProvider.access$1910(GpsLocationProvider.this);
                    if ((GpsLocationProvider.this.mPendingMessageBits == 0) && (GpsLocationProvider.this.mPendingListenerMessages == 0))
                        GpsLocationProvider.this.mWakeLock.release();
                    return;
                    if (paramMessage.arg1 == 1)
                    {
                        GpsLocationProvider.this.handleEnable();
                    }
                    else
                    {
                        GpsLocationProvider.this.handleDisable();
                        continue;
                        GpsLocationProvider localGpsLocationProvider = GpsLocationProvider.this;
                        if (paramMessage.arg1 == 1);
                        for (boolean bool = true; ; bool = false)
                        {
                            localGpsLocationProvider.handleEnableLocationTracking(bool);
                            break;
                        }
                        GpsLocationProvider.this.handleRequestSingleShot();
                        continue;
                        GpsLocationProvider.this.handleUpdateNetworkState(paramMessage.arg1, (NetworkInfo)paramMessage.obj);
                        continue;
                        GpsLocationProvider.this.handleInjectNtpTime();
                        continue;
                        if (GpsLocationProvider.this.mSupportsXtra)
                        {
                            GpsLocationProvider.this.handleDownloadXtraData();
                            continue;
                            GpsLocationProvider.this.handleUpdateLocation((Location)paramMessage.obj);
                            continue;
                            GpsLocationProvider.this.handleAddListener(paramMessage.arg1);
                        }
                    }
                }
                GpsLocationProvider.this.handleRemoveListener(paramMessage.arg1);
            }
        }
    }

    private final class Listener
        implements IBinder.DeathRecipient
    {
        final IGpsStatusListener mListener;
        int mSensors = 0;

        Listener(IGpsStatusListener arg2)
        {
            Object localObject;
            this.mListener = localObject;
        }

        public void binderDied()
        {
            synchronized (GpsLocationProvider.this.mListeners)
            {
                GpsLocationProvider.this.mListeners.remove(this);
                if (this.mListener != null)
                    this.mListener.asBinder().unlinkToDeath(this, 0);
                return;
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static void appendUidExtra(GpsLocationProvider paramGpsLocationProvider, Intent paramIntent)
        {
            if ((paramGpsLocationProvider.getNavigating()) && (paramGpsLocationProvider.getClientUids().size() > 0))
                paramIntent.putExtra("android.intent.extra.UID", paramGpsLocationProvider.getClientUids().keyAt(0));
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.location.GpsLocationProvider
 * JD-Core Version:        0.6.2
 */