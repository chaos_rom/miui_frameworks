package com.android.server.location;

import android.content.Context;
import android.location.Address;
import android.location.Country;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Slog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class LocationBasedCountryDetector extends CountryDetectorBase
{
    private static final long QUERY_LOCATION_TIMEOUT = 300000L;
    private static final String TAG = "LocationBasedCountryDetector";
    private List<String> mEnabledProviders;
    protected List<LocationListener> mLocationListeners;
    private LocationManager mLocationManager;
    protected Thread mQueryThread;
    protected Timer mTimer;

    public LocationBasedCountryDetector(Context paramContext)
    {
        super(paramContext);
        this.mLocationManager = ((LocationManager)paramContext.getSystemService("location"));
    }

    /** @deprecated */
    private void queryCountryCode(final Location paramLocation)
    {
        if (paramLocation == null);
        try
        {
            notifyListener(null);
            while (true)
            {
                return;
                if (this.mQueryThread == null)
                {
                    this.mQueryThread = new Thread(new Runnable()
                    {
                        public void run()
                        {
                            String str = null;
                            if (paramLocation != null)
                                str = LocationBasedCountryDetector.this.getCountryFromLocation(paramLocation);
                            if (str != null);
                            for (LocationBasedCountryDetector.this.mDetectedCountry = new Country(str, 1); ; LocationBasedCountryDetector.this.mDetectedCountry = null)
                            {
                                LocationBasedCountryDetector.this.notifyListener(LocationBasedCountryDetector.this.mDetectedCountry);
                                LocationBasedCountryDetector.this.mQueryThread = null;
                                return;
                            }
                        }
                    });
                    this.mQueryThread.start();
                }
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public Country detectCountry()
    {
        try
        {
            if (this.mLocationListeners != null)
                throw new IllegalStateException();
        }
        finally
        {
        }
        List localList = getEnabledProviders();
        int i = localList.size();
        if (i > 0)
            this.mLocationListeners = new ArrayList(i);
        for (int j = 0; ; j++)
            if (j < i)
            {
                String str = (String)localList.get(j);
                if (isAcceptableProvider(str))
                {
                    LocationListener local1 = new LocationListener()
                    {
                        public void onLocationChanged(Location paramAnonymousLocation)
                        {
                            if (paramAnonymousLocation != null)
                            {
                                LocationBasedCountryDetector.this.stop();
                                LocationBasedCountryDetector.this.queryCountryCode(paramAnonymousLocation);
                            }
                        }

                        public void onProviderDisabled(String paramAnonymousString)
                        {
                        }

                        public void onProviderEnabled(String paramAnonymousString)
                        {
                        }

                        public void onStatusChanged(String paramAnonymousString, int paramAnonymousInt, Bundle paramAnonymousBundle)
                        {
                        }
                    };
                    this.mLocationListeners.add(local1);
                    registerListener(str, local1);
                }
            }
            else
            {
                this.mTimer = new Timer();
                this.mTimer.schedule(new TimerTask()
                {
                    public void run()
                    {
                        LocationBasedCountryDetector.this.mTimer = null;
                        LocationBasedCountryDetector.this.stop();
                        LocationBasedCountryDetector.this.queryCountryCode(LocationBasedCountryDetector.this.getLastKnownLocation());
                    }
                }
                , getQueryLocationTimeout());
                while (true)
                {
                    Country localCountry = this.mDetectedCountry;
                    return localCountry;
                    queryCountryCode(getLastKnownLocation());
                }
            }
    }

    protected String getCountryFromLocation(Location paramLocation)
    {
        Object localObject = null;
        Geocoder localGeocoder = new Geocoder(this.mContext);
        try
        {
            List localList = localGeocoder.getFromLocation(paramLocation.getLatitude(), paramLocation.getLongitude(), 1);
            if ((localList != null) && (localList.size() > 0))
            {
                String str = ((Address)localList.get(0)).getCountryCode();
                localObject = str;
            }
            return localObject;
        }
        catch (IOException localIOException)
        {
            while (true)
                Slog.w("LocationBasedCountryDetector", "Exception occurs when getting country from location");
        }
    }

    protected List<String> getEnabledProviders()
    {
        if (this.mEnabledProviders == null)
            this.mEnabledProviders = this.mLocationManager.getProviders(true);
        return this.mEnabledProviders;
    }

    protected Location getLastKnownLocation()
    {
        List localList = this.mLocationManager.getAllProviders();
        Object localObject = null;
        Iterator localIterator = localList.iterator();
        while (localIterator.hasNext())
        {
            String str = (String)localIterator.next();
            Location localLocation = this.mLocationManager.getLastKnownLocation(str);
            if ((localLocation != null) && ((localObject == null) || (localObject.getTime() < localLocation.getTime())))
                localObject = localLocation;
        }
        return localObject;
    }

    protected long getQueryLocationTimeout()
    {
        return 300000L;
    }

    protected boolean isAcceptableProvider(String paramString)
    {
        return "passive".equals(paramString);
    }

    protected void registerListener(String paramString, LocationListener paramLocationListener)
    {
        this.mLocationManager.requestLocationUpdates(paramString, 0L, 0.0F, paramLocationListener);
    }

    /** @deprecated */
    public void stop()
    {
        try
        {
            if (this.mLocationListeners == null)
                break label54;
            Iterator localIterator = this.mLocationListeners.iterator();
            while (localIterator.hasNext())
                unregisterListener((LocationListener)localIterator.next());
        }
        finally
        {
        }
        this.mLocationListeners = null;
        label54: if (this.mTimer != null)
        {
            this.mTimer.cancel();
            this.mTimer = null;
        }
    }

    protected void unregisterListener(LocationListener paramLocationListener)
    {
        this.mLocationManager.removeUpdates(paramLocationListener);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.location.LocationBasedCountryDetector
 * JD-Core Version:        0.6.2
 */