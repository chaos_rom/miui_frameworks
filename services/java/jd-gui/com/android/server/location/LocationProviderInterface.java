package com.android.server.location;

import android.location.Criteria;
import android.location.Location;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.WorkSource;

public abstract interface LocationProviderInterface
{
    public abstract void addListener(int paramInt);

    public abstract void disable();

    public abstract void enable();

    public abstract void enableLocationTracking(boolean paramBoolean);

    public abstract int getAccuracy();

    public abstract String getInternalState();

    public abstract String getName();

    public abstract int getPowerRequirement();

    public abstract int getStatus(Bundle paramBundle);

    public abstract long getStatusUpdateTime();

    public abstract boolean hasMonetaryCost();

    public abstract boolean isEnabled();

    public abstract boolean meetsCriteria(Criteria paramCriteria);

    public abstract void removeListener(int paramInt);

    public abstract boolean requestSingleShotFix();

    public abstract boolean requiresCell();

    public abstract boolean requiresNetwork();

    public abstract boolean requiresSatellite();

    public abstract boolean sendExtraCommand(String paramString, Bundle paramBundle);

    public abstract void setMinTime(long paramLong, WorkSource paramWorkSource);

    public abstract boolean supportsAltitude();

    public abstract boolean supportsBearing();

    public abstract boolean supportsSpeed();

    public abstract void updateLocation(Location paramLocation);

    public abstract void updateNetworkState(int paramInt, NetworkInfo paramNetworkInfo);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.location.LocationProviderInterface
 * JD-Core Version:        0.6.2
 */