package com.android.server.location;

import android.location.Criteria;
import android.location.ILocationManager;
import android.location.Location;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.WorkSource;
import android.util.Log;

public class PassiveProvider
    implements LocationProviderInterface
{
    private static final String TAG = "PassiveProvider";
    private final ILocationManager mLocationManager;
    private boolean mTracking;

    public PassiveProvider(ILocationManager paramILocationManager)
    {
        this.mLocationManager = paramILocationManager;
    }

    public void addListener(int paramInt)
    {
    }

    public void disable()
    {
    }

    public void enable()
    {
    }

    public void enableLocationTracking(boolean paramBoolean)
    {
        this.mTracking = paramBoolean;
    }

    public int getAccuracy()
    {
        return -1;
    }

    public String getInternalState()
    {
        return null;
    }

    public String getName()
    {
        return "passive";
    }

    public int getPowerRequirement()
    {
        return -1;
    }

    public int getStatus(Bundle paramBundle)
    {
        if (this.mTracking);
        for (int i = 2; ; i = 1)
            return i;
    }

    public long getStatusUpdateTime()
    {
        return -1L;
    }

    public boolean hasMonetaryCost()
    {
        return false;
    }

    public boolean isEnabled()
    {
        return true;
    }

    public boolean meetsCriteria(Criteria paramCriteria)
    {
        return false;
    }

    public void removeListener(int paramInt)
    {
    }

    public boolean requestSingleShotFix()
    {
        return false;
    }

    public boolean requiresCell()
    {
        return false;
    }

    public boolean requiresNetwork()
    {
        return false;
    }

    public boolean requiresSatellite()
    {
        return false;
    }

    public boolean sendExtraCommand(String paramString, Bundle paramBundle)
    {
        return false;
    }

    public void setMinTime(long paramLong, WorkSource paramWorkSource)
    {
    }

    public boolean supportsAltitude()
    {
        return false;
    }

    public boolean supportsBearing()
    {
        return false;
    }

    public boolean supportsSpeed()
    {
        return false;
    }

    public void updateLocation(Location paramLocation)
    {
        if (this.mTracking);
        try
        {
            this.mLocationManager.reportLocation(paramLocation, true);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("PassiveProvider", "RemoteException calling reportLocation");
        }
    }

    public void updateNetworkState(int paramInt, NetworkInfo paramNetworkInfo)
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.location.PassiveProvider
 * JD-Core Version:        0.6.2
 */