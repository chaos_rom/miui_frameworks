package com.android.server.location;

import android.content.Context;
import android.location.Country;
import android.location.CountryListener;
import android.os.Handler;

public abstract class CountryDetectorBase
{
    protected final Context mContext;
    protected Country mDetectedCountry;
    protected final Handler mHandler;
    protected CountryListener mListener;

    public CountryDetectorBase(Context paramContext)
    {
        this.mContext = paramContext;
        this.mHandler = new Handler();
    }

    public abstract Country detectCountry();

    protected void notifyListener(Country paramCountry)
    {
        if (this.mListener != null)
            this.mListener.onCountryDetected(paramCountry);
    }

    public void setCountryListener(CountryListener paramCountryListener)
    {
        this.mListener = paramCountryListener;
    }

    public abstract void stop();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.location.CountryDetectorBase
 * JD-Core Version:        0.6.2
 */