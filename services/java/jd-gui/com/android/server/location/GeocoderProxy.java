package com.android.server.location;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Address;
import android.location.GeocoderParams;
import android.location.IGeocodeProvider;
import android.location.IGeocodeProvider.Stub;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import java.util.List;

public class GeocoderProxy
{
    public static final String SERVICE_ACTION = "com.android.location.service.GeocodeProvider";
    private static final String TAG = "GeocoderProxy";
    private final Context mContext;
    private final Intent mIntent;
    private final Object mMutex = new Object();
    private Connection mServiceConnection;

    public GeocoderProxy(Context paramContext, String paramString)
    {
        this.mContext = paramContext;
        this.mIntent = new Intent("com.android.location.service.GeocodeProvider");
        reconnect(paramString);
    }

    public String getFromLocation(double paramDouble1, double paramDouble2, int paramInt, GeocoderParams paramGeocoderParams, List<Address> paramList)
    {
        IGeocodeProvider localIGeocodeProvider;
        synchronized (this.mMutex)
        {
            localIGeocodeProvider = this.mServiceConnection.getProvider();
            if (localIGeocodeProvider == null);
        }
        while (true)
        {
            try
            {
                String str2 = localIGeocodeProvider.getFromLocation(paramDouble1, paramDouble2, paramInt, paramGeocoderParams, paramList);
                str1 = str2;
                return str1;
                localObject2 = finally;
                throw localObject2;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("GeocoderProxy", "getFromLocation failed", localRemoteException);
            }
            String str1 = "Service not Available";
        }
    }

    public String getFromLocationName(String paramString, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, int paramInt, GeocoderParams paramGeocoderParams, List<Address> paramList)
    {
        IGeocodeProvider localIGeocodeProvider;
        synchronized (this.mMutex)
        {
            localIGeocodeProvider = this.mServiceConnection.getProvider();
            if (localIGeocodeProvider == null);
        }
        while (true)
        {
            try
            {
                String str2 = localIGeocodeProvider.getFromLocationName(paramString, paramDouble1, paramDouble2, paramDouble3, paramDouble4, paramInt, paramGeocoderParams, paramList);
                str1 = str2;
                return str1;
                localObject2 = finally;
                throw localObject2;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("GeocoderProxy", "getFromLocationName failed", localRemoteException);
            }
            String str1 = "Service not Available";
        }
    }

    public void reconnect(String paramString)
    {
        synchronized (this.mMutex)
        {
            if (this.mServiceConnection != null)
                this.mContext.unbindService(this.mServiceConnection);
            this.mServiceConnection = new Connection(null);
            this.mIntent.setPackage(paramString);
            this.mContext.bindService(this.mIntent, this.mServiceConnection, 21);
            return;
        }
    }

    private class Connection
        implements ServiceConnection
    {
        private IGeocodeProvider mProvider;

        private Connection()
        {
        }

        public IGeocodeProvider getProvider()
        {
            try
            {
                IGeocodeProvider localIGeocodeProvider = this.mProvider;
                return localIGeocodeProvider;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
        {
            try
            {
                this.mProvider = IGeocodeProvider.Stub.asInterface(paramIBinder);
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public void onServiceDisconnected(ComponentName paramComponentName)
        {
            try
            {
                this.mProvider = null;
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.location.GeocoderProxy
 * JD-Core Version:        0.6.2
 */