package com.android.server.location;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Criteria;
import android.location.ILocationProvider;
import android.location.ILocationProvider.Stub;
import android.location.Location;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.WorkSource;
import android.util.Log;
import com.android.internal.location.DummyLocationProvider;

public class LocationProviderProxy
    implements LocationProviderInterface
{
    public static final String SERVICE_ACTION = "com.android.location.service.NetworkLocationProvider";
    private static final String TAG = "LocationProviderProxy";
    private final Context mContext;
    private boolean mEnabled = false;
    private final Handler mHandler;
    private final Intent mIntent;
    private boolean mLocationTracking = false;
    private long mMinTime = -1L;
    private WorkSource mMinTimeSource = new WorkSource();
    private final Object mMutex = new Object();
    private final String mName;
    private NetworkInfo mNetworkInfo;
    private int mNetworkState;
    private Connection mServiceConnection;

    public LocationProviderProxy(Context paramContext, String paramString1, String paramString2, Handler paramHandler)
    {
        this.mContext = paramContext;
        this.mName = paramString1;
        this.mIntent = new Intent("com.android.location.service.NetworkLocationProvider");
        this.mHandler = paramHandler;
        reconnect(paramString2);
    }

    private DummyLocationProvider getCachedAttributes()
    {
        synchronized (this.mMutex)
        {
            DummyLocationProvider localDummyLocationProvider = this.mServiceConnection.getCachedAttributes();
            return localDummyLocationProvider;
        }
    }

    public void addListener(int paramInt)
    {
        ILocationProvider localILocationProvider;
        synchronized (this.mMutex)
        {
            localILocationProvider = this.mServiceConnection.getProvider();
            if (localILocationProvider == null);
        }
        try
        {
            localILocationProvider.addListener(paramInt);
            label29: return;
            localObject2 = finally;
            throw localObject2;
        }
        catch (RemoteException localRemoteException)
        {
            break label29;
        }
    }

    public void disable()
    {
        ILocationProvider localILocationProvider;
        synchronized (this.mMutex)
        {
            this.mEnabled = false;
            localILocationProvider = this.mServiceConnection.getProvider();
            if (localILocationProvider == null);
        }
        try
        {
            localILocationProvider.disable();
            label30: return;
            localObject2 = finally;
            throw localObject2;
        }
        catch (RemoteException localRemoteException)
        {
            break label30;
        }
    }

    public void enable()
    {
        ILocationProvider localILocationProvider;
        synchronized (this.mMutex)
        {
            this.mEnabled = true;
            localILocationProvider = this.mServiceConnection.getProvider();
            if (localILocationProvider == null);
        }
        try
        {
            localILocationProvider.enable();
            label30: return;
            localObject2 = finally;
            throw localObject2;
        }
        catch (RemoteException localRemoteException)
        {
            break label30;
        }
    }

    public void enableLocationTracking(boolean paramBoolean)
    {
        ILocationProvider localILocationProvider;
        synchronized (this.mMutex)
        {
            this.mLocationTracking = paramBoolean;
            if (!paramBoolean)
            {
                this.mMinTime = -1L;
                this.mMinTimeSource.clear();
            }
            localILocationProvider = this.mServiceConnection.getProvider();
            if (localILocationProvider == null);
        }
        try
        {
            localILocationProvider.enableLocationTracking(paramBoolean);
            label52: return;
            localObject2 = finally;
            throw localObject2;
        }
        catch (RemoteException localRemoteException)
        {
            break label52;
        }
    }

    public int getAccuracy()
    {
        DummyLocationProvider localDummyLocationProvider = getCachedAttributes();
        if (localDummyLocationProvider != null);
        for (int i = localDummyLocationProvider.getAccuracy(); ; i = -1)
            return i;
    }

    public String getInternalState()
    {
        ILocationProvider localILocationProvider;
        synchronized (this.mMutex)
        {
            localILocationProvider = this.mServiceConnection.getProvider();
            if (localILocationProvider == null);
        }
        while (true)
        {
            try
            {
                String str2 = localILocationProvider.getInternalState();
                str1 = str2;
                return str1;
                localObject2 = finally;
                throw localObject2;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("LocationProviderProxy", "getInternalState failed", localRemoteException);
            }
            String str1 = null;
        }
    }

    public long getMinTime()
    {
        synchronized (this.mMutex)
        {
            long l = this.mMinTime;
            return l;
        }
    }

    public String getName()
    {
        return this.mName;
    }

    public int getPowerRequirement()
    {
        DummyLocationProvider localDummyLocationProvider = getCachedAttributes();
        if (localDummyLocationProvider != null);
        for (int i = localDummyLocationProvider.getPowerRequirement(); ; i = -1)
            return i;
    }

    public int getStatus(Bundle paramBundle)
    {
        ILocationProvider localILocationProvider;
        synchronized (this.mMutex)
        {
            localILocationProvider = this.mServiceConnection.getProvider();
            if (localILocationProvider == null);
        }
        while (true)
        {
            try
            {
                int j = localILocationProvider.getStatus(paramBundle);
                i = j;
                return i;
                localObject2 = finally;
                throw localObject2;
            }
            catch (RemoteException localRemoteException)
            {
            }
            int i = 0;
        }
    }

    public long getStatusUpdateTime()
    {
        ILocationProvider localILocationProvider;
        synchronized (this.mMutex)
        {
            localILocationProvider = this.mServiceConnection.getProvider();
            if (localILocationProvider == null);
        }
        while (true)
        {
            try
            {
                long l2 = localILocationProvider.getStatusUpdateTime();
                l1 = l2;
                return l1;
                localObject2 = finally;
                throw localObject2;
            }
            catch (RemoteException localRemoteException)
            {
            }
            long l1 = 0L;
        }
    }

    public boolean hasMonetaryCost()
    {
        DummyLocationProvider localDummyLocationProvider = getCachedAttributes();
        if (localDummyLocationProvider != null);
        for (boolean bool = localDummyLocationProvider.hasMonetaryCost(); ; bool = false)
            return bool;
    }

    public boolean isEnabled()
    {
        synchronized (this.mMutex)
        {
            boolean bool = this.mEnabled;
            return bool;
        }
    }

    public boolean isLocationTracking()
    {
        synchronized (this.mMutex)
        {
            boolean bool = this.mLocationTracking;
            return bool;
        }
    }

    public boolean meetsCriteria(Criteria paramCriteria)
    {
        boolean bool1 = false;
        ILocationProvider localILocationProvider;
        synchronized (this.mMutex)
        {
            localILocationProvider = this.mServiceConnection.getProvider();
            if (localILocationProvider == null);
        }
        try
        {
            boolean bool2 = localILocationProvider.meetsCriteria(paramCriteria);
            bool1 = bool2;
            break label135;
            label41: if ((paramCriteria.getAccuracy() == 0) || (paramCriteria.getAccuracy() >= getAccuracy()))
            {
                int i = paramCriteria.getPowerRequirement();
                if (((i == 0) || (i >= getPowerRequirement())) && ((!paramCriteria.isAltitudeRequired()) || (supportsAltitude())) && ((!paramCriteria.isSpeedRequired()) || (supportsSpeed())) && ((!paramCriteria.isBearingRequired()) || (supportsBearing())))
                {
                    bool1 = true;
                    break label135;
                    localObject2 = finally;
                    throw localObject2;
                }
            }
            label135: return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            break label41;
        }
    }

    public void reconnect(String paramString)
    {
        synchronized (this.mMutex)
        {
            if (this.mServiceConnection != null)
                this.mContext.unbindService(this.mServiceConnection);
            this.mServiceConnection = new Connection(null);
            this.mIntent.setPackage(paramString);
            this.mContext.bindService(this.mIntent, this.mServiceConnection, 21);
            return;
        }
    }

    public void removeListener(int paramInt)
    {
        ILocationProvider localILocationProvider;
        synchronized (this.mMutex)
        {
            localILocationProvider = this.mServiceConnection.getProvider();
            if (localILocationProvider == null);
        }
        try
        {
            localILocationProvider.removeListener(paramInt);
            label29: return;
            localObject2 = finally;
            throw localObject2;
        }
        catch (RemoteException localRemoteException)
        {
            break label29;
        }
    }

    public boolean requestSingleShotFix()
    {
        return false;
    }

    public boolean requiresCell()
    {
        DummyLocationProvider localDummyLocationProvider = getCachedAttributes();
        if (localDummyLocationProvider != null);
        for (boolean bool = localDummyLocationProvider.requiresCell(); ; bool = false)
            return bool;
    }

    public boolean requiresNetwork()
    {
        DummyLocationProvider localDummyLocationProvider = getCachedAttributes();
        if (localDummyLocationProvider != null);
        for (boolean bool = localDummyLocationProvider.requiresNetwork(); ; bool = false)
            return bool;
    }

    public boolean requiresSatellite()
    {
        DummyLocationProvider localDummyLocationProvider = getCachedAttributes();
        if (localDummyLocationProvider != null);
        for (boolean bool = localDummyLocationProvider.requiresSatellite(); ; bool = false)
            return bool;
    }

    public boolean sendExtraCommand(String paramString, Bundle paramBundle)
    {
        ILocationProvider localILocationProvider;
        synchronized (this.mMutex)
        {
            localILocationProvider = this.mServiceConnection.getProvider();
            if (localILocationProvider == null);
        }
        try
        {
            boolean bool2 = localILocationProvider.sendExtraCommand(paramString, paramBundle);
            boolean bool1 = bool2;
            break label56;
            label41: bool1 = false;
            break label56;
            localObject2 = finally;
            throw localObject2;
            label56: return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            break label41;
        }
    }

    public void setMinTime(long paramLong, WorkSource paramWorkSource)
    {
        ILocationProvider localILocationProvider;
        synchronized (this.mMutex)
        {
            this.mMinTime = paramLong;
            this.mMinTimeSource.set(paramWorkSource);
            localILocationProvider = this.mServiceConnection.getProvider();
            if (localILocationProvider == null);
        }
        try
        {
            localILocationProvider.setMinTime(paramLong, paramWorkSource);
            label45: return;
            localObject2 = finally;
            throw localObject2;
        }
        catch (RemoteException localRemoteException)
        {
            break label45;
        }
    }

    public boolean supportsAltitude()
    {
        DummyLocationProvider localDummyLocationProvider = getCachedAttributes();
        if (localDummyLocationProvider != null);
        for (boolean bool = localDummyLocationProvider.supportsAltitude(); ; bool = false)
            return bool;
    }

    public boolean supportsBearing()
    {
        DummyLocationProvider localDummyLocationProvider = getCachedAttributes();
        if (localDummyLocationProvider != null);
        for (boolean bool = localDummyLocationProvider.supportsBearing(); ; bool = false)
            return bool;
    }

    public boolean supportsSpeed()
    {
        DummyLocationProvider localDummyLocationProvider = getCachedAttributes();
        if (localDummyLocationProvider != null);
        for (boolean bool = localDummyLocationProvider.supportsSpeed(); ; bool = false)
            return bool;
    }

    public void updateLocation(Location paramLocation)
    {
        ILocationProvider localILocationProvider;
        synchronized (this.mMutex)
        {
            localILocationProvider = this.mServiceConnection.getProvider();
            if (localILocationProvider == null);
        }
        try
        {
            localILocationProvider.updateLocation(paramLocation);
            label29: return;
            localObject2 = finally;
            throw localObject2;
        }
        catch (RemoteException localRemoteException)
        {
            break label29;
        }
    }

    public void updateNetworkState(int paramInt, NetworkInfo paramNetworkInfo)
    {
        ILocationProvider localILocationProvider;
        synchronized (this.mMutex)
        {
            this.mNetworkState = paramInt;
            this.mNetworkInfo = paramNetworkInfo;
            localILocationProvider = this.mServiceConnection.getProvider();
            if (localILocationProvider == null);
        }
        try
        {
            localILocationProvider.updateNetworkState(paramInt, paramNetworkInfo);
            label40: return;
            localObject2 = finally;
            throw localObject2;
        }
        catch (RemoteException localRemoteException)
        {
            break label40;
        }
    }

    private class Connection
        implements ServiceConnection, Runnable
    {
        private DummyLocationProvider mCachedAttributes;
        private ILocationProvider mProvider;

        private Connection()
        {
        }

        /** @deprecated */
        public DummyLocationProvider getCachedAttributes()
        {
            try
            {
                DummyLocationProvider localDummyLocationProvider = this.mCachedAttributes;
                return localDummyLocationProvider;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        /** @deprecated */
        public ILocationProvider getProvider()
        {
            try
            {
                ILocationProvider localILocationProvider = this.mProvider;
                return localILocationProvider;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
        {
            try
            {
                this.mProvider = ILocationProvider.Stub.asInterface(paramIBinder);
                if (this.mProvider != null)
                    LocationProviderProxy.this.mHandler.post(this);
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        public void onServiceDisconnected(ComponentName paramComponentName)
        {
            try
            {
                this.mProvider = null;
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }

        // ERROR //
        public void run()
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 21	com/android/server/location/LocationProviderProxy$Connection:this$0	Lcom/android/server/location/LocationProviderProxy;
            //     4: invokestatic 62	com/android/server/location/LocationProviderProxy:access$200	(Lcom/android/server/location/LocationProviderProxy;)Ljava/lang/Object;
            //     7: astore_1
            //     8: aload_1
            //     9: monitorenter
            //     10: aload_0
            //     11: getfield 21	com/android/server/location/LocationProviderProxy$Connection:this$0	Lcom/android/server/location/LocationProviderProxy;
            //     14: invokestatic 66	com/android/server/location/LocationProviderProxy:access$300	(Lcom/android/server/location/LocationProviderProxy;)Lcom/android/server/location/LocationProviderProxy$Connection;
            //     17: aload_0
            //     18: if_acmpeq +8 -> 26
            //     21: aload_1
            //     22: monitorexit
            //     23: goto +284 -> 307
            //     26: aload_0
            //     27: invokevirtual 68	com/android/server/location/LocationProviderProxy$Connection:getProvider	()Landroid/location/ILocationProvider;
            //     30: astore_3
            //     31: aload_3
            //     32: ifnonnull +13 -> 45
            //     35: aload_1
            //     36: monitorexit
            //     37: goto +270 -> 307
            //     40: astore_2
            //     41: aload_1
            //     42: monitorexit
            //     43: aload_2
            //     44: athrow
            //     45: aload_0
            //     46: getfield 21	com/android/server/location/LocationProviderProxy$Connection:this$0	Lcom/android/server/location/LocationProviderProxy;
            //     49: invokestatic 72	com/android/server/location/LocationProviderProxy:access$400	(Lcom/android/server/location/LocationProviderProxy;)Z
            //     52: ifeq +9 -> 61
            //     55: aload_3
            //     56: invokeinterface 77 1 0
            //     61: aload_0
            //     62: getfield 21	com/android/server/location/LocationProviderProxy$Connection:this$0	Lcom/android/server/location/LocationProviderProxy;
            //     65: invokestatic 80	com/android/server/location/LocationProviderProxy:access$500	(Lcom/android/server/location/LocationProviderProxy;)Z
            //     68: ifeq +10 -> 78
            //     71: aload_3
            //     72: iconst_1
            //     73: invokeinterface 84 2 0
            //     78: aload_0
            //     79: getfield 21	com/android/server/location/LocationProviderProxy$Connection:this$0	Lcom/android/server/location/LocationProviderProxy;
            //     82: invokestatic 88	com/android/server/location/LocationProviderProxy:access$600	(Lcom/android/server/location/LocationProviderProxy;)J
            //     85: lconst_0
            //     86: lcmp
            //     87: iflt +23 -> 110
            //     90: aload_3
            //     91: aload_0
            //     92: getfield 21	com/android/server/location/LocationProviderProxy$Connection:this$0	Lcom/android/server/location/LocationProviderProxy;
            //     95: invokestatic 88	com/android/server/location/LocationProviderProxy:access$600	(Lcom/android/server/location/LocationProviderProxy;)J
            //     98: aload_0
            //     99: getfield 21	com/android/server/location/LocationProviderProxy$Connection:this$0	Lcom/android/server/location/LocationProviderProxy;
            //     102: invokestatic 92	com/android/server/location/LocationProviderProxy:access$700	(Lcom/android/server/location/LocationProviderProxy;)Landroid/os/WorkSource;
            //     105: invokeinterface 96 4 0
            //     110: aload_0
            //     111: getfield 21	com/android/server/location/LocationProviderProxy$Connection:this$0	Lcom/android/server/location/LocationProviderProxy;
            //     114: invokestatic 100	com/android/server/location/LocationProviderProxy:access$800	(Lcom/android/server/location/LocationProviderProxy;)Landroid/net/NetworkInfo;
            //     117: ifnull +23 -> 140
            //     120: aload_3
            //     121: aload_0
            //     122: getfield 21	com/android/server/location/LocationProviderProxy$Connection:this$0	Lcom/android/server/location/LocationProviderProxy;
            //     125: invokestatic 104	com/android/server/location/LocationProviderProxy:access$900	(Lcom/android/server/location/LocationProviderProxy;)I
            //     128: aload_0
            //     129: getfield 21	com/android/server/location/LocationProviderProxy$Connection:this$0	Lcom/android/server/location/LocationProviderProxy;
            //     132: invokestatic 100	com/android/server/location/LocationProviderProxy:access$800	(Lcom/android/server/location/LocationProviderProxy;)Landroid/net/NetworkInfo;
            //     135: invokeinterface 108 3 0
            //     140: aload_0
            //     141: getfield 31	com/android/server/location/LocationProviderProxy$Connection:mCachedAttributes	Lcom/android/internal/location/DummyLocationProvider;
            //     144: astore 5
            //     146: aload 5
            //     148: ifnonnull +139 -> 287
            //     151: aload_0
            //     152: new 110	com/android/internal/location/DummyLocationProvider
            //     155: dup
            //     156: aload_0
            //     157: getfield 21	com/android/server/location/LocationProviderProxy$Connection:this$0	Lcom/android/server/location/LocationProviderProxy;
            //     160: invokestatic 114	com/android/server/location/LocationProviderProxy:access$1000	(Lcom/android/server/location/LocationProviderProxy;)Ljava/lang/String;
            //     163: aconst_null
            //     164: invokespecial 117	com/android/internal/location/DummyLocationProvider:<init>	(Ljava/lang/String;Landroid/location/ILocationManager;)V
            //     167: putfield 31	com/android/server/location/LocationProviderProxy$Connection:mCachedAttributes	Lcom/android/internal/location/DummyLocationProvider;
            //     170: aload_0
            //     171: getfield 31	com/android/server/location/LocationProviderProxy$Connection:mCachedAttributes	Lcom/android/internal/location/DummyLocationProvider;
            //     174: aload_3
            //     175: invokeinterface 121 1 0
            //     180: invokevirtual 124	com/android/internal/location/DummyLocationProvider:setRequiresNetwork	(Z)V
            //     183: aload_0
            //     184: getfield 31	com/android/server/location/LocationProviderProxy$Connection:mCachedAttributes	Lcom/android/internal/location/DummyLocationProvider;
            //     187: aload_3
            //     188: invokeinterface 127 1 0
            //     193: invokevirtual 130	com/android/internal/location/DummyLocationProvider:setRequiresSatellite	(Z)V
            //     196: aload_0
            //     197: getfield 31	com/android/server/location/LocationProviderProxy$Connection:mCachedAttributes	Lcom/android/internal/location/DummyLocationProvider;
            //     200: aload_3
            //     201: invokeinterface 133 1 0
            //     206: invokevirtual 136	com/android/internal/location/DummyLocationProvider:setRequiresCell	(Z)V
            //     209: aload_0
            //     210: getfield 31	com/android/server/location/LocationProviderProxy$Connection:mCachedAttributes	Lcom/android/internal/location/DummyLocationProvider;
            //     213: aload_3
            //     214: invokeinterface 139 1 0
            //     219: invokevirtual 142	com/android/internal/location/DummyLocationProvider:setHasMonetaryCost	(Z)V
            //     222: aload_0
            //     223: getfield 31	com/android/server/location/LocationProviderProxy$Connection:mCachedAttributes	Lcom/android/internal/location/DummyLocationProvider;
            //     226: aload_3
            //     227: invokeinterface 145 1 0
            //     232: invokevirtual 148	com/android/internal/location/DummyLocationProvider:setSupportsAltitude	(Z)V
            //     235: aload_0
            //     236: getfield 31	com/android/server/location/LocationProviderProxy$Connection:mCachedAttributes	Lcom/android/internal/location/DummyLocationProvider;
            //     239: aload_3
            //     240: invokeinterface 151 1 0
            //     245: invokevirtual 154	com/android/internal/location/DummyLocationProvider:setSupportsSpeed	(Z)V
            //     248: aload_0
            //     249: getfield 31	com/android/server/location/LocationProviderProxy$Connection:mCachedAttributes	Lcom/android/internal/location/DummyLocationProvider;
            //     252: aload_3
            //     253: invokeinterface 157 1 0
            //     258: invokevirtual 160	com/android/internal/location/DummyLocationProvider:setSupportsBearing	(Z)V
            //     261: aload_0
            //     262: getfield 31	com/android/server/location/LocationProviderProxy$Connection:mCachedAttributes	Lcom/android/internal/location/DummyLocationProvider;
            //     265: aload_3
            //     266: invokeinterface 164 1 0
            //     271: invokevirtual 168	com/android/internal/location/DummyLocationProvider:setPowerRequirement	(I)V
            //     274: aload_0
            //     275: getfield 31	com/android/server/location/LocationProviderProxy$Connection:mCachedAttributes	Lcom/android/internal/location/DummyLocationProvider;
            //     278: aload_3
            //     279: invokeinterface 171 1 0
            //     284: invokevirtual 174	com/android/internal/location/DummyLocationProvider:setAccuracy	(I)V
            //     287: aload_1
            //     288: monitorexit
            //     289: goto +18 -> 307
            //     292: astore 6
            //     294: aload_0
            //     295: aconst_null
            //     296: putfield 31	com/android/server/location/LocationProviderProxy$Connection:mCachedAttributes	Lcom/android/internal/location/DummyLocationProvider;
            //     299: goto -12 -> 287
            //     302: astore 4
            //     304: goto -164 -> 140
            //     307: return
            //
            // Exception table:
            //     from	to	target	type
            //     10	43	40	finally
            //     45	140	40	finally
            //     140	146	40	finally
            //     151	287	40	finally
            //     287	299	40	finally
            //     151	287	292	android/os/RemoteException
            //     45	140	302	android/os/RemoteException
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.location.LocationProviderProxy
 * JD-Core Version:        0.6.2
 */