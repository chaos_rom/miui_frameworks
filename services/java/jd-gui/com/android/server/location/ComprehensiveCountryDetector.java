package com.android.server.location;

import android.content.Context;
import android.location.Country;
import android.location.CountryListener;
import android.location.Geocoder;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings.System;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Slog;
import java.util.Iterator;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ComprehensiveCountryDetector extends CountryDetectorBase
{
    static final boolean DEBUG = false;
    private static final long LOCATION_REFRESH_INTERVAL = 86400000L;
    private static final int MAX_LENGTH_DEBUG_LOGS = 20;
    private static final String TAG = "CountryDetector";
    private int mCountServiceStateChanges;
    private Country mCountry;
    private Country mCountryFromLocation;
    private final ConcurrentLinkedQueue<Country> mDebugLogs = new ConcurrentLinkedQueue();
    private Country mLastCountryAddedToLogs;
    private CountryListener mLocationBasedCountryDetectionListener = new CountryListener()
    {
        public void onCountryDetected(Country paramAnonymousCountry)
        {
            ComprehensiveCountryDetector.access$002(ComprehensiveCountryDetector.this, paramAnonymousCountry);
            ComprehensiveCountryDetector.this.detectCountry(true, false);
            ComprehensiveCountryDetector.this.stopLocationBasedDetector();
        }
    };
    protected CountryDetectorBase mLocationBasedCountryDetector;
    protected Timer mLocationRefreshTimer;
    private final Object mObject = new Object();
    private PhoneStateListener mPhoneStateListener;
    private long mStartTime;
    private long mStopTime;
    private boolean mStopped = false;
    private final TelephonyManager mTelephonyManager;
    private int mTotalCountServiceStateChanges;
    private long mTotalTime;

    public ComprehensiveCountryDetector(Context paramContext)
    {
        super(paramContext);
        this.mTelephonyManager = ((TelephonyManager)paramContext.getSystemService("phone"));
    }

    // ERROR //
    private void addToLogs(Country paramCountry)
    {
        // Byte code:
        //     0: aload_1
        //     1: ifnonnull +4 -> 5
        //     4: return
        //     5: aload_0
        //     6: getfield 69	com/android/server/location/ComprehensiveCountryDetector:mObject	Ljava/lang/Object;
        //     9: astore_2
        //     10: aload_2
        //     11: monitorenter
        //     12: aload_0
        //     13: getfield 117	com/android/server/location/ComprehensiveCountryDetector:mLastCountryAddedToLogs	Landroid/location/Country;
        //     16: ifnull +24 -> 40
        //     19: aload_0
        //     20: getfield 117	com/android/server/location/ComprehensiveCountryDetector:mLastCountryAddedToLogs	Landroid/location/Country;
        //     23: aload_1
        //     24: invokevirtual 123	android/location/Country:equals	(Ljava/lang/Object;)Z
        //     27: ifeq +13 -> 40
        //     30: aload_2
        //     31: monitorexit
        //     32: goto -28 -> 4
        //     35: astore_3
        //     36: aload_2
        //     37: monitorexit
        //     38: aload_3
        //     39: athrow
        //     40: aload_0
        //     41: aload_1
        //     42: putfield 117	com/android/server/location/ComprehensiveCountryDetector:mLastCountryAddedToLogs	Landroid/location/Country;
        //     45: aload_2
        //     46: monitorexit
        //     47: aload_0
        //     48: getfield 64	com/android/server/location/ComprehensiveCountryDetector:mDebugLogs	Ljava/util/concurrent/ConcurrentLinkedQueue;
        //     51: invokevirtual 127	java/util/concurrent/ConcurrentLinkedQueue:size	()I
        //     54: bipush 20
        //     56: if_icmplt +11 -> 67
        //     59: aload_0
        //     60: getfield 64	com/android/server/location/ComprehensiveCountryDetector:mDebugLogs	Ljava/util/concurrent/ConcurrentLinkedQueue;
        //     63: invokevirtual 131	java/util/concurrent/ConcurrentLinkedQueue:poll	()Ljava/lang/Object;
        //     66: pop
        //     67: aload_0
        //     68: getfield 64	com/android/server/location/ComprehensiveCountryDetector:mDebugLogs	Ljava/util/concurrent/ConcurrentLinkedQueue;
        //     71: aload_1
        //     72: invokevirtual 134	java/util/concurrent/ConcurrentLinkedQueue:add	(Ljava/lang/Object;)Z
        //     75: pop
        //     76: goto -72 -> 4
        //
        // Exception table:
        //     from	to	target	type
        //     12	38	35	finally
        //     40	47	35	finally
    }

    /** @deprecated */
    private void cancelLocationRefresh()
    {
        try
        {
            if (this.mLocationRefreshTimer != null)
            {
                this.mLocationRefreshTimer.cancel();
                this.mLocationRefreshTimer = null;
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private Country detectCountry(boolean paramBoolean1, boolean paramBoolean2)
    {
        Country localCountry1 = getCountry();
        if (this.mCountry != null);
        for (Country localCountry2 = new Country(this.mCountry); ; localCountry2 = this.mCountry)
        {
            runAfterDetectionAsync(localCountry2, localCountry1, paramBoolean1, paramBoolean2);
            this.mCountry = localCountry1;
            return this.mCountry;
        }
    }

    private Country getCountry()
    {
        Country localCountry = getNetworkBasedCountry();
        if (localCountry == null)
            localCountry = getLastKnownLocationBasedCountry();
        if (localCountry == null)
            localCountry = getSimBasedCountry();
        if (localCountry == null)
            localCountry = getLocaleCountry();
        addToLogs(localCountry);
        return localCountry;
    }

    private boolean isNetworkCountryCodeAvailable()
    {
        int i = 1;
        if (this.mTelephonyManager.getPhoneType() == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private void notifyIfCountryChanged(Country paramCountry1, Country paramCountry2)
    {
        if ((paramCountry2 != null) && (this.mListener != null) && ((paramCountry1 == null) || (!paramCountry1.equals(paramCountry2))))
            notifyListener(paramCountry2);
    }

    /** @deprecated */
    private void scheduleLocationRefresh()
    {
        try
        {
            Timer localTimer = this.mLocationRefreshTimer;
            if (localTimer != null);
            while (true)
            {
                return;
                this.mLocationRefreshTimer = new Timer();
                this.mLocationRefreshTimer.schedule(new TimerTask()
                {
                    public void run()
                    {
                        ComprehensiveCountryDetector.this.mLocationRefreshTimer = null;
                        ComprehensiveCountryDetector.this.detectCountry(false, true);
                    }
                }
                , 86400000L);
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    private void startLocationBasedDetector(CountryListener paramCountryListener)
    {
        try
        {
            CountryDetectorBase localCountryDetectorBase = this.mLocationBasedCountryDetector;
            if (localCountryDetectorBase != null);
            while (true)
            {
                return;
                this.mLocationBasedCountryDetector = createLocationBasedCountryDetector();
                this.mLocationBasedCountryDetector.setCountryListener(paramCountryListener);
                this.mLocationBasedCountryDetector.detectCountry();
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    private void stopLocationBasedDetector()
    {
        try
        {
            if (this.mLocationBasedCountryDetector != null)
            {
                this.mLocationBasedCountryDetector.stop();
                this.mLocationBasedCountryDetector = null;
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    protected void addPhoneStateListener()
    {
        try
        {
            if (this.mPhoneStateListener == null)
            {
                this.mPhoneStateListener = new PhoneStateListener()
                {
                    public void onServiceStateChanged(ServiceState paramAnonymousServiceState)
                    {
                        ComprehensiveCountryDetector.access$308(ComprehensiveCountryDetector.this);
                        ComprehensiveCountryDetector.access$408(ComprehensiveCountryDetector.this);
                        if (!ComprehensiveCountryDetector.this.isNetworkCountryCodeAvailable());
                        while (true)
                        {
                            return;
                            ComprehensiveCountryDetector.this.detectCountry(true, true);
                        }
                    }
                };
                this.mTelephonyManager.listen(this.mPhoneStateListener, 1);
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    protected CountryDetectorBase createLocationBasedCountryDetector()
    {
        return new LocationBasedCountryDetector(this.mContext);
    }

    public Country detectCountry()
    {
        if (!this.mStopped);
        for (boolean bool = true; ; bool = false)
            return detectCountry(false, bool);
    }

    protected Country getLastKnownLocationBasedCountry()
    {
        return this.mCountryFromLocation;
    }

    protected Country getLocaleCountry()
    {
        Locale localLocale = Locale.getDefault();
        if (localLocale != null);
        for (Country localCountry = new Country(localLocale.getCountry(), 3); ; localCountry = null)
            return localCountry;
    }

    protected Country getNetworkBasedCountry()
    {
        String str;
        if (isNetworkCountryCodeAvailable())
        {
            str = this.mTelephonyManager.getNetworkCountryIso();
            if (TextUtils.isEmpty(str));
        }
        for (Country localCountry = new Country(str, 0); ; localCountry = null)
            return localCountry;
    }

    protected Country getSimBasedCountry()
    {
        String str = this.mTelephonyManager.getSimCountryIso();
        if (!TextUtils.isEmpty(str));
        for (Country localCountry = new Country(str, 2); ; localCountry = null)
            return localCountry;
    }

    protected boolean isAirplaneModeOff()
    {
        boolean bool = false;
        if (Settings.System.getInt(this.mContext.getContentResolver(), "airplane_mode_on", 0) == 0)
            bool = true;
        return bool;
    }

    protected boolean isGeoCoderImplemented()
    {
        return Geocoder.isPresent();
    }

    /** @deprecated */
    protected void removePhoneStateListener()
    {
        try
        {
            if (this.mPhoneStateListener != null)
            {
                this.mTelephonyManager.listen(this.mPhoneStateListener, 0);
                this.mPhoneStateListener = null;
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    void runAfterDetection(Country paramCountry1, Country paramCountry2, boolean paramBoolean1, boolean paramBoolean2)
    {
        if (paramBoolean1)
            notifyIfCountryChanged(paramCountry1, paramCountry2);
        if ((paramBoolean2) && ((paramCountry2 == null) || (paramCountry2.getSource() > 1)) && (isAirplaneModeOff()) && (this.mListener != null) && (isGeoCoderImplemented()))
            startLocationBasedDetector(this.mLocationBasedCountryDetectionListener);
        if ((paramCountry2 == null) || (paramCountry2.getSource() >= 1))
            scheduleLocationRefresh();
        while (true)
        {
            return;
            cancelLocationRefresh();
            stopLocationBasedDetector();
        }
    }

    protected void runAfterDetectionAsync(final Country paramCountry1, final Country paramCountry2, final boolean paramBoolean1, final boolean paramBoolean2)
    {
        this.mHandler.post(new Runnable()
        {
            public void run()
            {
                ComprehensiveCountryDetector.this.runAfterDetection(paramCountry1, paramCountry2, paramBoolean1, paramBoolean2);
            }
        });
    }

    public void setCountryListener(CountryListener paramCountryListener)
    {
        CountryListener localCountryListener = this.mListener;
        this.mListener = paramCountryListener;
        if (this.mListener == null)
        {
            removePhoneStateListener();
            stopLocationBasedDetector();
            cancelLocationRefresh();
            this.mStopTime = SystemClock.elapsedRealtime();
            this.mTotalTime += this.mStopTime;
        }
        while (true)
        {
            return;
            if (localCountryListener == null)
            {
                addPhoneStateListener();
                detectCountry(false, true);
                this.mStartTime = SystemClock.elapsedRealtime();
                this.mStopTime = 0L;
                this.mCountServiceStateChanges = 0;
            }
        }
    }

    public void stop()
    {
        Slog.i("CountryDetector", "Stop the detector.");
        cancelLocationRefresh();
        removePhoneStateListener();
        stopLocationBasedDetector();
        this.mListener = null;
        this.mStopped = true;
    }

    public String toString()
    {
        long l1 = SystemClock.elapsedRealtime();
        long l2 = 0L;
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("ComprehensiveCountryDetector{");
        if (this.mStopTime == 0L)
        {
            l2 = l1 - this.mStartTime;
            localStringBuilder.append("timeRunning=" + l2 + ", ");
        }
        while (true)
        {
            localStringBuilder.append("totalCountServiceStateChanges=" + this.mTotalCountServiceStateChanges + ", ");
            localStringBuilder.append("currentCountServiceStateChanges=" + this.mCountServiceStateChanges + ", ");
            localStringBuilder.append("totalTime=" + (l2 + this.mTotalTime) + ", ");
            localStringBuilder.append("currentTime=" + l1 + ", ");
            localStringBuilder.append("countries=");
            Iterator localIterator = this.mDebugLogs.iterator();
            while (localIterator.hasNext())
            {
                Country localCountry = (Country)localIterator.next();
                localStringBuilder.append("\n     " + localCountry.toString());
            }
            localStringBuilder.append("lastRunTimeLength=" + (this.mStopTime - this.mStartTime) + ", ");
        }
        localStringBuilder.append("}");
        return localStringBuilder.toString();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.location.ComprehensiveCountryDetector
 * JD-Core Version:        0.6.2
 */