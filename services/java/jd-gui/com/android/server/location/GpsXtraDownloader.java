package com.android.server.location;

import android.content.Context;
import android.net.Proxy;
import android.util.Log;
import java.util.Properties;
import java.util.Random;

public class GpsXtraDownloader
{
    static final boolean DEBUG = false;
    private static final String TAG = "GpsXtraDownloader";
    private Context mContext;
    private int mNextServerIndex;
    private String[] mXtraServers;

    GpsXtraDownloader(Context paramContext, Properties paramProperties)
    {
        this.mContext = paramContext;
        int i = 0;
        String str1 = paramProperties.getProperty("XTRA_SERVER_1");
        String str2 = paramProperties.getProperty("XTRA_SERVER_2");
        String str3 = paramProperties.getProperty("XTRA_SERVER_3");
        if (str1 != null)
            i = 0 + 1;
        if (str2 != null)
            i++;
        if (str3 != null)
            i++;
        if (i == 0)
        {
            Log.e("GpsXtraDownloader", "No XTRA servers were specified in the GPS configuration");
            return;
        }
        this.mXtraServers = new String[i];
        int j;
        if (str1 != null)
        {
            String[] arrayOfString3 = this.mXtraServers;
            j = 0 + 1;
            arrayOfString3[0] = str1;
        }
        while (true)
        {
            if (str2 != null)
            {
                String[] arrayOfString2 = this.mXtraServers;
                int m = j + 1;
                arrayOfString2[j] = str2;
                j = m;
            }
            int k;
            if (str3 != null)
            {
                String[] arrayOfString1 = this.mXtraServers;
                k = j + 1;
                arrayOfString1[j] = str3;
            }
            while (true)
            {
                this.mNextServerIndex = new Random().nextInt(k);
                break;
                k = j;
            }
            j = 0;
        }
    }

    // ERROR //
    protected static byte[] doDownload(String paramString1, boolean paramBoolean, String paramString2, int paramInt)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore 4
        //     3: ldc 65
        //     5: invokestatic 71	android/net/http/AndroidHttpClient:newInstance	(Ljava/lang/String;)Landroid/net/http/AndroidHttpClient;
        //     8: astore 4
        //     10: new 73	org/apache/http/client/methods/HttpGet
        //     13: dup
        //     14: aload_0
        //     15: invokespecial 76	org/apache/http/client/methods/HttpGet:<init>	(Ljava/lang/String;)V
        //     18: astore 8
        //     20: iload_1
        //     21: ifeq +26 -> 47
        //     24: new 78	org/apache/http/HttpHost
        //     27: dup
        //     28: aload_2
        //     29: iload_3
        //     30: invokespecial 81	org/apache/http/HttpHost:<init>	(Ljava/lang/String;I)V
        //     33: astore 9
        //     35: aload 8
        //     37: invokeinterface 87 1 0
        //     42: aload 9
        //     44: invokestatic 93	org/apache/http/conn/params/ConnRouteParams:setDefaultProxy	(Lorg/apache/http/params/HttpParams;Lorg/apache/http/HttpHost;)V
        //     47: aload 8
        //     49: ldc 95
        //     51: ldc 97
        //     53: invokeinterface 101 3 0
        //     58: aload 8
        //     60: ldc 103
        //     62: ldc 105
        //     64: invokeinterface 101 3 0
        //     69: aload 4
        //     71: aload 8
        //     73: invokevirtual 109	android/net/http/AndroidHttpClient:execute	(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
        //     76: astore 10
        //     78: aload 10
        //     80: invokeinterface 115 1 0
        //     85: invokeinterface 121 1 0
        //     90: istore 11
        //     92: iload 11
        //     94: sipush 200
        //     97: if_icmpeq +19 -> 116
        //     100: aconst_null
        //     101: astore 7
        //     103: aload 4
        //     105: ifnull +8 -> 113
        //     108: aload 4
        //     110: invokevirtual 124	android/net/http/AndroidHttpClient:close	()V
        //     113: aload 7
        //     115: areturn
        //     116: aload 10
        //     118: invokeinterface 128 1 0
        //     123: astore 12
        //     125: aconst_null
        //     126: astore 7
        //     128: aload 12
        //     130: ifnull +67 -> 197
        //     133: aload 12
        //     135: invokeinterface 134 1 0
        //     140: lconst_0
        //     141: lcmp
        //     142: ifle +43 -> 185
        //     145: aload 12
        //     147: invokeinterface 134 1 0
        //     152: l2i
        //     153: newarray byte
        //     155: astore 7
        //     157: new 136	java/io/DataInputStream
        //     160: dup
        //     161: aload 12
        //     163: invokeinterface 140 1 0
        //     168: invokespecial 143	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
        //     171: astore 14
        //     173: aload 14
        //     175: aload 7
        //     177: invokevirtual 147	java/io/DataInputStream:readFully	([B)V
        //     180: aload 14
        //     182: invokevirtual 148	java/io/DataInputStream:close	()V
        //     185: aload 12
        //     187: ifnull +10 -> 197
        //     190: aload 12
        //     192: invokeinterface 151 1 0
        //     197: aload 4
        //     199: ifnull -86 -> 113
        //     202: goto -94 -> 108
        //     205: astore 15
        //     207: aload 14
        //     209: invokevirtual 148	java/io/DataInputStream:close	()V
        //     212: aload 15
        //     214: athrow
        //     215: astore 13
        //     217: aload 12
        //     219: ifnull +10 -> 229
        //     222: aload 12
        //     224: invokeinterface 151 1 0
        //     229: aload 13
        //     231: athrow
        //     232: astore 6
        //     234: aload 4
        //     236: ifnull +8 -> 244
        //     239: aload 4
        //     241: invokevirtual 124	android/net/http/AndroidHttpClient:close	()V
        //     244: aconst_null
        //     245: astore 7
        //     247: goto -134 -> 113
        //     250: astore 5
        //     252: aload 4
        //     254: ifnull +8 -> 262
        //     257: aload 4
        //     259: invokevirtual 124	android/net/http/AndroidHttpClient:close	()V
        //     262: aload 5
        //     264: athrow
        //     265: astore 16
        //     267: ldc 11
        //     269: ldc 153
        //     271: aload 16
        //     273: invokestatic 156	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     276: pop
        //     277: goto -65 -> 212
        //     280: astore 18
        //     282: ldc 11
        //     284: ldc 153
        //     286: aload 18
        //     288: invokestatic 156	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     291: pop
        //     292: goto -107 -> 185
        //
        // Exception table:
        //     from	to	target	type
        //     173	180	205	finally
        //     133	173	215	finally
        //     180	185	215	finally
        //     207	212	215	finally
        //     212	215	215	finally
        //     267	292	215	finally
        //     3	92	232	java/lang/Exception
        //     116	125	232	java/lang/Exception
        //     190	197	232	java/lang/Exception
        //     222	232	232	java/lang/Exception
        //     3	92	250	finally
        //     116	125	250	finally
        //     190	197	250	finally
        //     222	232	250	finally
        //     207	212	265	java/io/IOException
        //     180	185	280	java/io/IOException
    }

    byte[] downloadXtraData()
    {
        String str = Proxy.getHost(this.mContext);
        int i = Proxy.getPort(this.mContext);
        boolean bool;
        byte[] arrayOfByte1;
        int j;
        if ((str != null) && (i != -1))
        {
            bool = true;
            arrayOfByte1 = null;
            j = this.mNextServerIndex;
            if (this.mXtraServers != null)
                break label55;
        }
        for (byte[] arrayOfByte2 = null; ; arrayOfByte2 = arrayOfByte1)
        {
            return arrayOfByte2;
            bool = false;
            break;
            label55: 
            do
            {
                if (arrayOfByte1 != null)
                    break;
                arrayOfByte1 = doDownload(this.mXtraServers[this.mNextServerIndex], bool, str, i);
                this.mNextServerIndex = (1 + this.mNextServerIndex);
                if (this.mNextServerIndex == this.mXtraServers.length)
                    this.mNextServerIndex = 0;
            }
            while (this.mNextServerIndex != j);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.location.GpsXtraDownloader
 * JD-Core Version:        0.6.2
 */