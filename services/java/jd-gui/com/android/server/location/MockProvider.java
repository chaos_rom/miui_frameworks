package com.android.server.location;

import android.location.Criteria;
import android.location.ILocationManager;
import android.location.Location;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.WorkSource;
import android.util.Log;
import android.util.PrintWriterPrinter;
import java.io.PrintWriter;

public class MockProvider
    implements LocationProviderInterface
{
    private static final String TAG = "MockProvider";
    private final int mAccuracy;
    private boolean mEnabled;
    private final Bundle mExtras = new Bundle();
    private boolean mHasLocation;
    private final boolean mHasMonetaryCost;
    private boolean mHasStatus;
    private final Location mLocation;
    private final ILocationManager mLocationManager;
    private final String mName;
    private final int mPowerRequirement;
    private final boolean mRequiresCell;
    private final boolean mRequiresNetwork;
    private final boolean mRequiresSatellite;
    private int mStatus;
    private long mStatusUpdateTime;
    private final boolean mSupportsAltitude;
    private final boolean mSupportsBearing;
    private final boolean mSupportsSpeed;

    public MockProvider(String paramString, ILocationManager paramILocationManager, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, boolean paramBoolean7, int paramInt1, int paramInt2)
    {
        this.mName = paramString;
        this.mLocationManager = paramILocationManager;
        this.mRequiresNetwork = paramBoolean1;
        this.mRequiresSatellite = paramBoolean2;
        this.mRequiresCell = paramBoolean3;
        this.mHasMonetaryCost = paramBoolean4;
        this.mSupportsAltitude = paramBoolean5;
        this.mSupportsBearing = paramBoolean7;
        this.mSupportsSpeed = paramBoolean6;
        this.mPowerRequirement = paramInt1;
        this.mAccuracy = paramInt2;
        this.mLocation = new Location(paramString);
    }

    public void addListener(int paramInt)
    {
    }

    public void clearLocation()
    {
        this.mHasLocation = false;
    }

    public void clearStatus()
    {
        this.mHasStatus = false;
        this.mStatusUpdateTime = 0L;
    }

    public void disable()
    {
        this.mEnabled = false;
    }

    public void dump(PrintWriter paramPrintWriter, String paramString)
    {
        paramPrintWriter.println(paramString + this.mName);
        paramPrintWriter.println(paramString + "mHasLocation=" + this.mHasLocation);
        paramPrintWriter.println(paramString + "mLocation:");
        this.mLocation.dump(new PrintWriterPrinter(paramPrintWriter), paramString + "    ");
        paramPrintWriter.println(paramString + "mHasStatus=" + this.mHasStatus);
        paramPrintWriter.println(paramString + "mStatus=" + this.mStatus);
        paramPrintWriter.println(paramString + "mStatusUpdateTime=" + this.mStatusUpdateTime);
        paramPrintWriter.println(paramString + "mExtras=" + this.mExtras);
    }

    public void enable()
    {
        this.mEnabled = true;
    }

    public void enableLocationTracking(boolean paramBoolean)
    {
    }

    public int getAccuracy()
    {
        return this.mAccuracy;
    }

    public String getInternalState()
    {
        return null;
    }

    public String getName()
    {
        return this.mName;
    }

    public int getPowerRequirement()
    {
        return this.mPowerRequirement;
    }

    public int getStatus(Bundle paramBundle)
    {
        if (this.mHasStatus)
        {
            paramBundle.clear();
            paramBundle.putAll(this.mExtras);
        }
        for (int i = this.mStatus; ; i = 2)
            return i;
    }

    public long getStatusUpdateTime()
    {
        return this.mStatusUpdateTime;
    }

    public boolean hasMonetaryCost()
    {
        return this.mHasMonetaryCost;
    }

    public boolean isEnabled()
    {
        return this.mEnabled;
    }

    public boolean meetsCriteria(Criteria paramCriteria)
    {
        boolean bool = false;
        if ((paramCriteria.getAccuracy() != 0) && (paramCriteria.getAccuracy() < this.mAccuracy));
        while (true)
        {
            return bool;
            int i = paramCriteria.getPowerRequirement();
            if (((i == 0) || (i >= this.mPowerRequirement)) && ((!paramCriteria.isAltitudeRequired()) || (this.mSupportsAltitude)) && ((!paramCriteria.isSpeedRequired()) || (this.mSupportsSpeed)) && ((!paramCriteria.isBearingRequired()) || (this.mSupportsBearing)))
                bool = true;
        }
    }

    public void removeListener(int paramInt)
    {
    }

    public boolean requestSingleShotFix()
    {
        return false;
    }

    public boolean requiresCell()
    {
        return this.mRequiresCell;
    }

    public boolean requiresNetwork()
    {
        return this.mRequiresNetwork;
    }

    public boolean requiresSatellite()
    {
        return this.mRequiresSatellite;
    }

    public boolean sendExtraCommand(String paramString, Bundle paramBundle)
    {
        return false;
    }

    public void setLocation(Location paramLocation)
    {
        this.mLocation.set(paramLocation);
        this.mHasLocation = true;
        try
        {
            this.mLocationManager.reportLocation(this.mLocation, false);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("MockProvider", "RemoteException calling reportLocation");
        }
    }

    public void setMinTime(long paramLong, WorkSource paramWorkSource)
    {
    }

    public void setStatus(int paramInt, Bundle paramBundle, long paramLong)
    {
        this.mStatus = paramInt;
        this.mStatusUpdateTime = paramLong;
        this.mExtras.clear();
        if (paramBundle != null)
            this.mExtras.putAll(paramBundle);
        this.mHasStatus = true;
    }

    public boolean supportsAltitude()
    {
        return this.mSupportsAltitude;
    }

    public boolean supportsBearing()
    {
        return this.mSupportsBearing;
    }

    public boolean supportsSpeed()
    {
        return this.mSupportsSpeed;
    }

    public void updateLocation(Location paramLocation)
    {
    }

    public void updateNetworkState(int paramInt, NetworkInfo paramNetworkInfo)
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.location.MockProvider
 * JD-Core Version:        0.6.2
 */