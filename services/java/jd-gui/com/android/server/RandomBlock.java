package com.android.server;

import android.util.Slog;
import java.io.Closeable;
import java.io.DataOutput;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;

class RandomBlock
{
    private static final int BLOCK_SIZE = 4096;
    private static final boolean DEBUG = false;
    private static final String TAG = "RandomBlock";
    private byte[] block = new byte[4096];

    private static void close(Closeable paramCloseable)
    {
        if (paramCloseable == null);
        while (true)
        {
            return;
            try
            {
                paramCloseable.close();
            }
            catch (IOException localIOException)
            {
                Slog.w("RandomBlock", "IOException thrown while closing Closeable", localIOException);
            }
        }
    }

    // ERROR //
    static RandomBlock fromFile(String paramString)
        throws IOException
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_1
        //     2: new 42	java/io/FileInputStream
        //     5: dup
        //     6: aload_0
        //     7: invokespecial 45	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
        //     10: astore_2
        //     11: aload_2
        //     12: invokestatic 49	com/android/server/RandomBlock:fromStream	(Ljava/io/InputStream;)Lcom/android/server/RandomBlock;
        //     15: astore 4
        //     17: aload_2
        //     18: invokestatic 51	com/android/server/RandomBlock:close	(Ljava/io/Closeable;)V
        //     21: aload 4
        //     23: areturn
        //     24: astore_3
        //     25: aload_1
        //     26: invokestatic 51	com/android/server/RandomBlock:close	(Ljava/io/Closeable;)V
        //     29: aload_3
        //     30: athrow
        //     31: astore_3
        //     32: aload_2
        //     33: astore_1
        //     34: goto -9 -> 25
        //
        // Exception table:
        //     from	to	target	type
        //     2	11	24	finally
        //     11	17	31	finally
    }

    private static RandomBlock fromStream(InputStream paramInputStream)
        throws IOException
    {
        RandomBlock localRandomBlock = new RandomBlock();
        int i = 0;
        while (i < 4096)
        {
            int j = paramInputStream.read(localRandomBlock.block, i, 4096 - i);
            if (j == -1)
                throw new EOFException();
            i += j;
        }
        return localRandomBlock;
    }

    private void toDataOut(DataOutput paramDataOutput)
        throws IOException
    {
        paramDataOutput.write(this.block);
    }

    private static void truncateIfPossible(RandomAccessFile paramRandomAccessFile)
    {
        try
        {
            paramRandomAccessFile.setLength(4096L);
            label7: return;
        }
        catch (IOException localIOException)
        {
            break label7;
        }
    }

    // ERROR //
    void toFile(String paramString, boolean paramBoolean)
        throws IOException
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_3
        //     2: iload_2
        //     3: ifeq +36 -> 39
        //     6: ldc 83
        //     8: astore 4
        //     10: new 75	java/io/RandomAccessFile
        //     13: dup
        //     14: aload_1
        //     15: aload 4
        //     17: invokespecial 86	java/io/RandomAccessFile:<init>	(Ljava/lang/String;Ljava/lang/String;)V
        //     20: astore 5
        //     22: aload_0
        //     23: aload 5
        //     25: invokespecial 88	com/android/server/RandomBlock:toDataOut	(Ljava/io/DataOutput;)V
        //     28: aload 5
        //     30: invokestatic 90	com/android/server/RandomBlock:truncateIfPossible	(Ljava/io/RandomAccessFile;)V
        //     33: aload 5
        //     35: invokestatic 51	com/android/server/RandomBlock:close	(Ljava/io/Closeable;)V
        //     38: return
        //     39: ldc 92
        //     41: astore 4
        //     43: goto -33 -> 10
        //     46: astore 6
        //     48: aload_3
        //     49: invokestatic 51	com/android/server/RandomBlock:close	(Ljava/io/Closeable;)V
        //     52: aload 6
        //     54: athrow
        //     55: astore 6
        //     57: aload 5
        //     59: astore_3
        //     60: goto -12 -> 48
        //
        // Exception table:
        //     from	to	target	type
        //     2	22	46	finally
        //     39	43	46	finally
        //     22	33	55	finally
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/services_dex2jar.jar
 * Qualified Name:         com.android.server.RandomBlock
 * JD-Core Version:        0.6.2
 */