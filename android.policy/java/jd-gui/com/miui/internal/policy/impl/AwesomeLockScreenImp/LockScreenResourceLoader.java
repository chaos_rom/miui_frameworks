package com.miui.internal.policy.impl.AwesomeLockScreenImp;

import miui.app.screenelement.ResourceLoader;
import miui.content.res.ThemeResources;
import miui.content.res.ThemeResourcesSystem;

public class LockScreenResourceLoader extends ResourceLoader
{
    private static final String LOG_TAG = "LockScreenResourceLoader";
    private static final String MANIFEST_XML = "manifest.xml";

    private boolean containsFile(String paramString)
    {
        return ThemeResources.getSystem().containsAwesomeLockscreenEntry(paramString);
    }

    // ERROR //
    public miui.app.screenelement.ResourceManager.BitmapInfo getBitmapInfo(String paramString, android.graphics.BitmapFactory.Options paramOptions)
    {
        // Byte code:
        //     0: invokestatic 23	miui/content/res/ThemeResources:getSystem	()Lmiui/content/res/ThemeResourcesSystem;
        //     3: aload_1
        //     4: invokevirtual 38	miui/content/res/ThemeResourcesSystem:getAwesomeLockscreenFileStream	(Ljava/lang/String;)Lmiui/content/res/ThemeZipFile$ThemeFileInfo;
        //     7: astore_3
        //     8: aload_3
        //     9: ifnull +78 -> 87
        //     12: aload_3
        //     13: getfield 44	miui/content/res/ThemeZipFile$ThemeFileInfo:mDensity	I
        //     16: ifeq +11 -> 27
        //     19: aload_2
        //     20: aload_3
        //     21: getfield 44	miui/content/res/ThemeZipFile$ThemeFileInfo:mDensity	I
        //     24: putfield 49	android/graphics/BitmapFactory$Options:inDensity	I
        //     27: new 51	android/graphics/Rect
        //     30: dup
        //     31: invokespecial 52	android/graphics/Rect:<init>	()V
        //     34: astore 10
        //     36: new 54	miui/app/screenelement/ResourceManager$BitmapInfo
        //     39: dup
        //     40: aload_3
        //     41: getfield 58	miui/content/res/ThemeZipFile$ThemeFileInfo:mInput	Ljava/io/InputStream;
        //     44: aload 10
        //     46: aload_2
        //     47: invokestatic 64	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
        //     50: aload 10
        //     52: invokespecial 67	miui/app/screenelement/ResourceManager$BitmapInfo:<init>	(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V
        //     55: astore 4
        //     57: aload_3
        //     58: getfield 58	miui/content/res/ThemeZipFile$ThemeFileInfo:mInput	Ljava/io/InputStream;
        //     61: invokevirtual 72	java/io/InputStream:close	()V
        //     64: aload 4
        //     66: areturn
        //     67: astore 7
        //     69: ldc 74
        //     71: aload 7
        //     73: invokevirtual 78	java/lang/OutOfMemoryError:toString	()Ljava/lang/String;
        //     76: invokestatic 84	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     79: pop
        //     80: aload_3
        //     81: getfield 58	miui/content/res/ThemeZipFile$ThemeFileInfo:mInput	Ljava/io/InputStream;
        //     84: invokevirtual 72	java/io/InputStream:close	()V
        //     87: aconst_null
        //     88: astore 4
        //     90: goto -26 -> 64
        //     93: astore 5
        //     95: aload_3
        //     96: getfield 58	miui/content/res/ThemeZipFile$ThemeFileInfo:mInput	Ljava/io/InputStream;
        //     99: invokevirtual 72	java/io/InputStream:close	()V
        //     102: aload 5
        //     104: athrow
        //     105: astore 6
        //     107: goto -5 -> 102
        //     110: astore 9
        //     112: goto -25 -> 87
        //     115: astore 11
        //     117: goto -53 -> 64
        //
        // Exception table:
        //     from	to	target	type
        //     12	57	67	java/lang/OutOfMemoryError
        //     12	57	93	finally
        //     69	80	93	finally
        //     95	102	105	java/io/IOException
        //     80	87	110	java/io/IOException
        //     57	64	115	java/io/IOException
    }

    // ERROR //
    public android.os.MemoryFile getFile(String paramString)
    {
        // Byte code:
        //     0: invokestatic 23	miui/content/res/ThemeResources:getSystem	()Lmiui/content/res/ThemeResourcesSystem;
        //     3: aload_1
        //     4: invokevirtual 38	miui/content/res/ThemeResourcesSystem:getAwesomeLockscreenFileStream	(Ljava/lang/String;)Lmiui/content/res/ThemeZipFile$ThemeFileInfo;
        //     7: astore_2
        //     8: aload_2
        //     9: ifnull +93 -> 102
        //     12: ldc 87
        //     14: newarray byte
        //     16: astore 4
        //     18: new 89	android/os/MemoryFile
        //     21: dup
        //     22: aconst_null
        //     23: aload_2
        //     24: getfield 93	miui/content/res/ThemeZipFile$ThemeFileInfo:mSize	J
        //     27: l2i
        //     28: invokespecial 96	android/os/MemoryFile:<init>	(Ljava/lang/String;I)V
        //     31: astore_3
        //     32: iconst_0
        //     33: istore 5
        //     35: aload_2
        //     36: getfield 58	miui/content/res/ThemeZipFile$ThemeFileInfo:mInput	Ljava/io/InputStream;
        //     39: aload 4
        //     41: iconst_0
        //     42: ldc 87
        //     44: invokevirtual 100	java/io/InputStream:read	([BII)I
        //     47: istore 14
        //     49: iload 14
        //     51: ifle +24 -> 75
        //     54: aload_3
        //     55: aload 4
        //     57: iconst_0
        //     58: iload 5
        //     60: iload 14
        //     62: invokevirtual 104	android/os/MemoryFile:writeBytes	([BIII)V
        //     65: iload 5
        //     67: iload 14
        //     69: iadd
        //     70: istore 5
        //     72: goto -37 -> 35
        //     75: aload_3
        //     76: invokevirtual 108	android/os/MemoryFile:length	()I
        //     79: istore 15
        //     81: iload 15
        //     83: ifle +12 -> 95
        //     86: aload_2
        //     87: getfield 58	miui/content/res/ThemeZipFile$ThemeFileInfo:mInput	Ljava/io/InputStream;
        //     90: invokevirtual 72	java/io/InputStream:close	()V
        //     93: aload_3
        //     94: areturn
        //     95: aload_2
        //     96: getfield 58	miui/content/res/ThemeZipFile$ThemeFileInfo:mInput	Ljava/io/InputStream;
        //     99: invokevirtual 72	java/io/InputStream:close	()V
        //     102: aconst_null
        //     103: astore_3
        //     104: goto -11 -> 93
        //     107: astore 11
        //     109: ldc 74
        //     111: aload 11
        //     113: invokevirtual 78	java/lang/OutOfMemoryError:toString	()Ljava/lang/String;
        //     116: invokestatic 84	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     119: pop
        //     120: aload_2
        //     121: getfield 58	miui/content/res/ThemeZipFile$ThemeFileInfo:mInput	Ljava/io/InputStream;
        //     124: invokevirtual 72	java/io/InputStream:close	()V
        //     127: goto -25 -> 102
        //     130: astore 13
        //     132: goto -30 -> 102
        //     135: astore 8
        //     137: ldc 74
        //     139: aload 8
        //     141: invokevirtual 109	java/io/IOException:toString	()Ljava/lang/String;
        //     144: invokestatic 84	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     147: pop
        //     148: aload_2
        //     149: getfield 58	miui/content/res/ThemeZipFile$ThemeFileInfo:mInput	Ljava/io/InputStream;
        //     152: invokevirtual 72	java/io/InputStream:close	()V
        //     155: goto -53 -> 102
        //     158: astore 10
        //     160: goto -58 -> 102
        //     163: astore 6
        //     165: aload_2
        //     166: getfield 58	miui/content/res/ThemeZipFile$ThemeFileInfo:mInput	Ljava/io/InputStream;
        //     169: invokevirtual 72	java/io/InputStream:close	()V
        //     172: aload 6
        //     174: athrow
        //     175: astore 7
        //     177: goto -5 -> 172
        //     180: astore 16
        //     182: goto -80 -> 102
        //     185: astore 17
        //     187: goto -94 -> 93
        //
        // Exception table:
        //     from	to	target	type
        //     18	81	107	java/lang/OutOfMemoryError
        //     120	127	130	java/io/IOException
        //     18	81	135	java/io/IOException
        //     148	155	158	java/io/IOException
        //     18	81	163	finally
        //     109	120	163	finally
        //     137	148	163	finally
        //     165	172	175	java/io/IOException
        //     95	102	180	java/io/IOException
        //     86	93	185	java/io/IOException
    }

    // ERROR //
    public org.w3c.dom.Element getManifestRoot()
    {
        // Byte code:
        //     0: invokestatic 119	javax/xml/parsers/DocumentBuilderFactory:newInstance	()Ljavax/xml/parsers/DocumentBuilderFactory;
        //     3: astore_1
        //     4: aconst_null
        //     5: astore_2
        //     6: aload_1
        //     7: invokevirtual 123	javax/xml/parsers/DocumentBuilderFactory:newDocumentBuilder	()Ljavax/xml/parsers/DocumentBuilder;
        //     10: astore 8
        //     12: aconst_null
        //     13: astore 9
        //     15: aload_0
        //     16: getfield 126	miui/app/screenelement/ResourceLoader:mLanguageCountrySuffix	Ljava/lang/String;
        //     19: invokestatic 132	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
        //     22: ifne +26 -> 48
        //     25: ldc 11
        //     27: aload_0
        //     28: getfield 126	miui/app/screenelement/ResourceLoader:mLanguageCountrySuffix	Ljava/lang/String;
        //     31: invokestatic 138	miui/app/screenelement/util/Utils:addFileNameSuffix	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //     34: astore 9
        //     36: aload_0
        //     37: aload 9
        //     39: invokespecial 140	com/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenResourceLoader:containsFile	(Ljava/lang/String;)Z
        //     42: ifne +6 -> 48
        //     45: aconst_null
        //     46: astore 9
        //     48: aload_0
        //     49: getfield 143	miui/app/screenelement/ResourceLoader:mLanguageSuffix	Ljava/lang/String;
        //     52: invokestatic 132	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
        //     55: ifne +205 -> 260
        //     58: aload 9
        //     60: ifnonnull +200 -> 260
        //     63: ldc 11
        //     65: aload_0
        //     66: getfield 143	miui/app/screenelement/ResourceLoader:mLanguageSuffix	Ljava/lang/String;
        //     69: invokestatic 138	miui/app/screenelement/util/Utils:addFileNameSuffix	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //     72: astore 9
        //     74: aload_0
        //     75: aload 9
        //     77: invokespecial 140	com/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenResourceLoader:containsFile	(Ljava/lang/String;)Z
        //     80: ifne +180 -> 260
        //     83: aconst_null
        //     84: astore 9
        //     86: goto +174 -> 260
        //     89: invokestatic 23	miui/content/res/ThemeResources:getSystem	()Lmiui/content/res/ThemeResourcesSystem;
        //     92: aload 9
        //     94: invokevirtual 38	miui/content/res/ThemeResourcesSystem:getAwesomeLockscreenFileStream	(Ljava/lang/String;)Lmiui/content/res/ThemeZipFile$ThemeFileInfo;
        //     97: astore_2
        //     98: aload 8
        //     100: aload_2
        //     101: getfield 58	miui/content/res/ThemeZipFile$ThemeFileInfo:mInput	Ljava/io/InputStream;
        //     104: invokevirtual 149	javax/xml/parsers/DocumentBuilder:parse	(Ljava/io/InputStream;)Lorg/w3c/dom/Document;
        //     107: invokeinterface 154 1 0
        //     112: astore 7
        //     114: ldc 8
        //     116: new 156	java/lang/StringBuilder
        //     119: dup
        //     120: invokespecial 157	java/lang/StringBuilder:<init>	()V
        //     123: ldc 159
        //     125: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     128: aload 7
        //     130: invokeinterface 168 1 0
        //     135: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     138: ldc 170
        //     140: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     143: aload 9
        //     145: invokevirtual 163	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     148: invokevirtual 171	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     151: invokestatic 174	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     154: pop
        //     155: aload 7
        //     157: invokeinterface 168 1 0
        //     162: ldc 176
        //     164: invokevirtual 182	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     167: istore 11
        //     169: iload 11
        //     171: ifeq +17 -> 188
        //     174: aload_2
        //     175: ifnull +10 -> 185
        //     178: aload_2
        //     179: getfield 58	miui/content/res/ThemeZipFile$ThemeFileInfo:mInput	Ljava/io/InputStream;
        //     182: invokevirtual 72	java/io/InputStream:close	()V
        //     185: aload 7
        //     187: areturn
        //     188: aload_2
        //     189: ifnull +10 -> 199
        //     192: aload_2
        //     193: getfield 58	miui/content/res/ThemeZipFile$ThemeFileInfo:mInput	Ljava/io/InputStream;
        //     196: invokevirtual 72	java/io/InputStream:close	()V
        //     199: aconst_null
        //     200: astore 7
        //     202: goto -17 -> 185
        //     205: astore 5
        //     207: aload 5
        //     209: invokevirtual 185	java/lang/Exception:printStackTrace	()V
        //     212: aload_2
        //     213: ifnull -14 -> 199
        //     216: aload_2
        //     217: getfield 58	miui/content/res/ThemeZipFile$ThemeFileInfo:mInput	Ljava/io/InputStream;
        //     220: invokevirtual 72	java/io/InputStream:close	()V
        //     223: goto -24 -> 199
        //     226: astore 6
        //     228: goto -29 -> 199
        //     231: astore_3
        //     232: aload_2
        //     233: ifnull +10 -> 243
        //     236: aload_2
        //     237: getfield 58	miui/content/res/ThemeZipFile$ThemeFileInfo:mInput	Ljava/io/InputStream;
        //     240: invokevirtual 72	java/io/InputStream:close	()V
        //     243: aload_3
        //     244: athrow
        //     245: astore 4
        //     247: goto -4 -> 243
        //     250: astore 12
        //     252: goto -53 -> 199
        //     255: astore 13
        //     257: goto -72 -> 185
        //     260: aload 9
        //     262: ifnonnull -173 -> 89
        //     265: ldc 11
        //     267: astore 9
        //     269: goto -180 -> 89
        //
        // Exception table:
        //     from	to	target	type
        //     6	169	205	java/lang/Exception
        //     216	223	226	java/io/IOException
        //     6	169	231	finally
        //     207	212	231	finally
        //     236	243	245	java/io/IOException
        //     192	199	250	java/io/IOException
        //     178	185	255	java/io/IOException
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.miui.internal.policy.impl.AwesomeLockScreenImp.LockScreenResourceLoader
 * JD-Core Version:        0.6.2
 */