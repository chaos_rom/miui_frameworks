package com.miui.internal.policy.impl.AwesomeLockScreenImp;

import java.util.ArrayList;
import java.util.Iterator;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.ScreenElementRoot;
import miui.app.screenelement.elements.ElementGroup;
import miui.app.screenelement.elements.ScreenElement;
import org.w3c.dom.Element;

public class LockScreenElementGroup extends ElementGroup
    implements UnlockerListener
{
    public LockScreenElementGroup(Element paramElement, ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot)
        throws ScreenElementLoadException
    {
        super(paramElement, paramScreenContext, paramScreenElementRoot);
    }

    public void endUnlockMoving(UnlockerScreenElement paramUnlockerScreenElement)
    {
        Iterator localIterator = this.mElements.iterator();
        while (localIterator.hasNext())
        {
            ScreenElement localScreenElement = (ScreenElement)localIterator.next();
            if ((localScreenElement instanceof UnlockerListener))
                ((UnlockerListener)localScreenElement).endUnlockMoving(paramUnlockerScreenElement);
        }
    }

    public void startUnlockMoving(UnlockerScreenElement paramUnlockerScreenElement)
    {
        Iterator localIterator = this.mElements.iterator();
        while (localIterator.hasNext())
        {
            ScreenElement localScreenElement = (ScreenElement)localIterator.next();
            if ((localScreenElement instanceof UnlockerListener))
                ((UnlockerListener)localScreenElement).startUnlockMoving(paramUnlockerScreenElement);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.miui.internal.policy.impl.AwesomeLockScreenImp.LockScreenElementGroup
 * JD-Core Version:        0.6.2
 */