package com.miui.internal.policy.impl.AwesomeLockScreenImp;

import miui.app.screenelement.data.ContentProviderBinder.Builder;
import miui.app.screenelement.data.VariableBinderManager;

public class BuiltinVariableBinders
{
    public static void fill(VariableBinderManager paramVariableBinderManager)
    {
        fillMissedCall(paramVariableBinderManager);
        fillUnreadSms(paramVariableBinderManager);
    }

    private static void fillMissedCall(VariableBinderManager paramVariableBinderManager)
    {
        String[] arrayOfString = new String[2];
        arrayOfString[0] = "_id";
        arrayOfString[1] = "number";
        paramVariableBinderManager.addContentProviderBinder("content://call_log/calls").setColumns(arrayOfString).setWhere("type=3 AND new=1").setCountName("call_missed_count");
    }

    private static void fillUnreadSms(VariableBinderManager paramVariableBinderManager)
    {
        String[] arrayOfString = new String[1];
        arrayOfString[0] = "_id";
        paramVariableBinderManager.addContentProviderBinder("content://sms/inbox").setColumns(arrayOfString).setWhere("seen=0 AND read=0").setCountName("sms_unread_count");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.miui.internal.policy.impl.AwesomeLockScreenImp.BuiltinVariableBinders
 * JD-Core Version:        0.6.2
 */