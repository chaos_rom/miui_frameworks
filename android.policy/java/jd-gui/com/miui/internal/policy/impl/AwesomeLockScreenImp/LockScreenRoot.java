package com.miui.internal.policy.impl.AwesomeLockScreenImp;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings.System;
import android.view.MotionEvent;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.ScreenElementRoot;
import miui.app.screenelement.data.BatteryVariableUpdater;
import miui.app.screenelement.data.VariableUpdaterManager;
import miui.app.screenelement.data.VolumeVariableUpdater;
import miui.app.screenelement.elements.ButtonScreenElement;
import miui.app.screenelement.elements.ButtonScreenElement.ButtonAction;
import miui.app.screenelement.elements.ElementGroup;
import miui.app.screenelement.elements.ScreenElement;
import miui.app.screenelement.util.IndexedNumberVariable;
import miui.app.screenelement.util.Task;
import miui.app.screenelement.util.Utils;
import miui.net.FirewallManager;
import miui.security.ChooseLockSettingsHelper;
import org.w3c.dom.Element;

public class LockScreenRoot extends ScreenElementRoot
    implements UnlockerListener
{
    private static final String LOG_TAG = "LockScreenRoot";
    public static final String SMS_BODY_PREVIEW = "sms_body_preview";
    private static final String TAG_NAME_BATTERYFULL = "BatteryFull";
    private static final String TAG_NAME_CHARGING = "Charging";
    private static final String TAG_NAME_LOWBATTERY = "BatteryLow";
    private static final String TAG_NAME_NORMAL = "Normal";
    private String curCategory;
    private IndexedNumberVariable mBatteryLevel;
    private IndexedNumberVariable mBatteryState;
    private boolean mDisplayDesktop;
    private float mFrameRateBatteryFull;
    private float mFrameRateBatteryLow;
    private float mFrameRateCharging;
    private final UnlockerCallback mUnlockerCallback;

    public LockScreenRoot(ScreenContext paramScreenContext, UnlockerCallback paramUnlockerCallback)
    {
        super(paramScreenContext);
        this.mUnlockerCallback = paramUnlockerCallback;
        this.mBatteryState = new IndexedNumberVariable("battery_state", this.mContext.mVariables);
        this.mBatteryLevel = new IndexedNumberVariable("battery_level", this.mContext.mVariables);
    }

    protected ElementGroup createElementGroup(Element paramElement, ScreenContext paramScreenContext)
        throws ScreenElementLoadException
    {
        return new LockScreenElementGroup(paramElement, this.mContext, this);
    }

    public void endUnlockMoving(UnlockerScreenElement paramUnlockerScreenElement)
    {
        if (this.mElementGroup != null)
            ((LockScreenElementGroup)this.mElementGroup).endUnlockMoving(paramUnlockerScreenElement);
    }

    public Task findTask(String paramString)
    {
        return this.mUnlockerCallback.findTask(paramString);
    }

    public void haptic(int paramInt)
    {
        this.mUnlockerCallback.haptic(paramInt);
    }

    public void init()
    {
        int i = 1;
        super.init();
        ChooseLockSettingsHelper localChooseLockSettingsHelper = new ChooseLockSettingsHelper(this.mContext.mContext);
        IndexedNumberVariable localIndexedNumberVariable;
        if ((Settings.System.getInt(this.mContext.mContext.getContentResolver(), "pref_key_enable_notification_body", i) == i) && ((!FirewallManager.isAccessControlProtected(this.mContext.mContext, "com.android.mms")) || (!localChooseLockSettingsHelper.isACLockEnabled())) && (!localChooseLockSettingsHelper.isPrivacyModeEnabled()) && (!this.mUnlockerCallback.isSecure()))
        {
            localIndexedNumberVariable = new IndexedNumberVariable("sms_body_preview", this.mContext.mVariables);
            if (i == 0)
                break label118;
        }
        label118: for (double d = 1.0D; ; d = 0.0D)
        {
            localIndexedNumberVariable.set(d);
            return;
            i = 0;
            break;
        }
    }

    public boolean isDisplayDesktop()
    {
        return this.mDisplayDesktop;
    }

    protected void onAddVariableUpdater(VariableUpdaterManager paramVariableUpdaterManager)
    {
        super.onAddVariableUpdater(paramVariableUpdaterManager);
        paramVariableUpdaterManager.add(new BatteryVariableUpdater(paramVariableUpdaterManager));
        paramVariableUpdaterManager.add(new VolumeVariableUpdater(paramVariableUpdaterManager));
    }

    public void onButtonInteractive(ButtonScreenElement paramButtonScreenElement, ButtonScreenElement.ButtonAction paramButtonAction)
    {
        this.mUnlockerCallback.pokeWakelock();
    }

    protected boolean onLoad(Element paramElement)
    {
        if (!super.onLoad(paramElement));
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            this.mDisplayDesktop = Boolean.parseBoolean(paramElement.getAttribute("displayDesktop"));
            this.mFrameRateCharging = Utils.getAttrAsFloat(paramElement, "frameRateCharging", this.mNormalFrameRate);
            this.mFrameRateBatteryLow = Utils.getAttrAsFloat(paramElement, "frameRateBatteryLow", this.mNormalFrameRate);
            this.mFrameRateBatteryFull = Utils.getAttrAsFloat(paramElement, "frameRateBatteryFull", this.mNormalFrameRate);
            BuiltinVariableBinders.fill(this.mVariableBinderManager);
        }
    }

    public void onRefreshBatteryInfo(boolean paramBoolean1, boolean paramBoolean2, int paramInt)
    {
        this.mBatteryLevel.set(paramInt);
        if (this.mElementGroup == null);
        label193: 
        while (true)
        {
            return;
            String str;
            int i;
            if (paramBoolean1)
                if (paramBoolean2)
                    if (paramInt >= 100)
                    {
                        str = "BatteryFull";
                        i = 3;
                        this.mFrameRate = this.mFrameRateBatteryFull;
                    }
            while (true)
            {
                if (str == this.curCategory)
                    break label193;
                requestFramerate(this.mFrameRate);
                requestUpdate();
                this.mBatteryState.set(i);
                this.mElementGroup.showCategory("BatteryFull", false);
                this.mElementGroup.showCategory("Charging", false);
                this.mElementGroup.showCategory("BatteryLow", false);
                this.mElementGroup.showCategory("Normal", false);
                this.mElementGroup.showCategory(str, true);
                this.curCategory = str;
                break;
                str = "Charging";
                i = 1;
                this.mFrameRate = this.mFrameRateCharging;
                continue;
                str = "BatteryLow";
                i = 2;
                this.mFrameRate = this.mFrameRateBatteryLow;
                continue;
                str = "Normal";
                i = 0;
                this.mFrameRate = this.mNormalFrameRate;
            }
        }
    }

    public boolean onTouch(MotionEvent paramMotionEvent)
    {
        if (this.mElementGroup == null)
            this.mUnlockerCallback.unlocked(null);
        for (boolean bool = false; ; bool = super.onTouch(paramMotionEvent))
            return bool;
    }

    public void pokeWakelock()
    {
        this.mUnlockerCallback.pokeWakelock();
    }

    protected boolean shouldPlaySound()
    {
        return this.mUnlockerCallback.isSoundEnable();
    }

    public void startUnlockMoving(UnlockerScreenElement paramUnlockerScreenElement)
    {
        if (this.mElementGroup != null)
            ((LockScreenElementGroup)this.mElementGroup).startUnlockMoving(paramUnlockerScreenElement);
    }

    public static abstract interface UnlockerCallback
    {
        public abstract Task findTask(String paramString);

        public abstract void haptic(int paramInt);

        public abstract boolean isSecure();

        public abstract boolean isSoundEnable();

        public abstract void pokeWakelock();

        public abstract void pokeWakelock(int paramInt);

        public abstract void unlocked(Intent paramIntent);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.miui.internal.policy.impl.AwesomeLockScreenImp.LockScreenRoot
 * JD-Core Version:        0.6.2
 */