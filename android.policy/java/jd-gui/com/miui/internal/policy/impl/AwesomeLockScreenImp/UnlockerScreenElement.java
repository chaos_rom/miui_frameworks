package com.miui.internal.policy.impl.AwesomeLockScreenImp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.util.Log;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.ScreenElementRoot;
import miui.app.screenelement.elements.AdvancedSlider;
import org.w3c.dom.Element;

public class UnlockerScreenElement extends AdvancedSlider
    implements UnlockerListener
{
    private static final String LOG_TAG = "LockScreen_UnlockerScreenElement";
    public static final String TAG_NAME = "Unlocker";
    private boolean mAlwaysShow;
    private boolean mNoUnlock;
    private final LockScreenRoot.UnlockerCallback mUnlockerCallback;
    private final UnlockerListener mUnlockerListener;
    private boolean mUnlockingHide;

    public UnlockerScreenElement(Element paramElement, ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot, LockScreenRoot.UnlockerCallback paramUnlockerCallback, UnlockerListener paramUnlockerListener)
        throws ScreenElementLoadException
    {
        super(paramElement, paramScreenContext, paramScreenElementRoot);
        this.mUnlockerCallback = paramUnlockerCallback;
        this.mUnlockerListener = paramUnlockerListener;
        this.mAlwaysShow = Boolean.parseBoolean(paramElement.getAttribute("alwaysShow"));
        this.mNoUnlock = Boolean.parseBoolean(paramElement.getAttribute("noUnlock"));
    }

    public void endUnlockMoving(UnlockerScreenElement paramUnlockerScreenElement)
    {
        if ((paramUnlockerScreenElement != this) && (!this.mAlwaysShow))
            this.mUnlockingHide = false;
    }

    public boolean isVisible()
    {
        if ((super.isVisible()) && (!this.mUnlockingHide));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected void onCancel()
    {
        super.onCancel();
        this.mUnlockerListener.endUnlockMoving(this);
    }

    protected boolean onLaunch(String paramString, Intent paramIntent)
    {
        super.onLaunch(paramString, paramIntent);
        this.mUnlockerListener.endUnlockMoving(this);
        boolean bool;
        if ((this.mNoUnlock) && (paramIntent == null))
        {
            this.mUnlockerCallback.pokeWakelock();
            bool = false;
        }
        while (true)
        {
            return bool;
            try
            {
                this.mUnlockerCallback.unlocked(paramIntent);
                bool = true;
            }
            catch (ActivityNotFoundException localActivityNotFoundException)
            {
                while (true)
                {
                    Log.e("LockScreen_UnlockerScreenElement", localActivityNotFoundException.toString());
                    localActivityNotFoundException.printStackTrace();
                }
            }
        }
    }

    protected void onStart()
    {
        super.onStart();
        this.mUnlockerListener.startUnlockMoving(this);
        this.mUnlockerCallback.pokeWakelock();
    }

    public void startUnlockMoving(UnlockerScreenElement paramUnlockerScreenElement)
    {
        if ((paramUnlockerScreenElement != this) && (!this.mAlwaysShow))
            this.mUnlockingHide = true;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.miui.internal.policy.impl.AwesomeLockScreenImp.UnlockerScreenElement
 * JD-Core Version:        0.6.2
 */