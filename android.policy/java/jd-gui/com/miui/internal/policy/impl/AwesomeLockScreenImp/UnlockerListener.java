package com.miui.internal.policy.impl.AwesomeLockScreenImp;

public abstract interface UnlockerListener
{
    public abstract void endUnlockMoving(UnlockerScreenElement paramUnlockerScreenElement);

    public abstract void startUnlockMoving(UnlockerScreenElement paramUnlockerScreenElement);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.miui.internal.policy.impl.AwesomeLockScreenImp.UnlockerListener
 * JD-Core Version:        0.6.2
 */