package com.miui.internal.policy.impl.AwesomeLockScreenImp;

import android.graphics.drawable.BitmapDrawable;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.ScreenElementRoot;
import miui.app.screenelement.animation.AnimatedElement;
import miui.app.screenelement.elements.AnimatedScreenElement;
import miui.app.screenelement.elements.ImageScreenElement;
import miui.app.screenelement.elements.ScreenElement;
import miui.content.res.ThemeResources;
import org.w3c.dom.Element;

public class WallpaperScreenElement extends ImageScreenElement
{
    public static final String TAG_NAME = "Wallpaper";

    public WallpaperScreenElement(Element paramElement, ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot)
        throws ScreenElementLoadException
    {
        super(paramElement, paramScreenContext, paramScreenElementRoot);
        BitmapDrawable localBitmapDrawable = (BitmapDrawable)ThemeResources.getLockWallpaperCache(this.mContext.mContext);
        if (localBitmapDrawable != null)
            this.mBitmap = localBitmapDrawable.getBitmap();
    }

    public float getHeight()
    {
        return this.mAni.getHeight();
    }

    public float getMaxHeight()
    {
        return this.mAni.getMaxHeight();
    }

    public float getMaxWidth()
    {
        return this.mAni.getMaxWidth();
    }

    public float getWidth()
    {
        return this.mAni.getWidth();
    }

    protected boolean shouldScaleBitmap()
    {
        return false;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.miui.internal.policy.impl.AwesomeLockScreenImp.WallpaperScreenElement
 * JD-Core Version:        0.6.2
 */