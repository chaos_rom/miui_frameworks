package com.miui.internal.policy.impl.AwesomeLockScreenImp;

import android.content.Context;
import android.os.SystemClock;
import android.view.MotionEvent;
import miui.app.screenelement.MiAdvancedView;

public class AwesomeLockScreenView extends MiAdvancedView
{
    private static final String LOG_TAG = "AwesomeLockScreenView";
    private static final int MOVING_THRESHOLD = 25;
    private long mLastTouchTime;
    private int mPreX;
    private int mPreY;

    public AwesomeLockScreenView(Context paramContext, LockScreenRoot paramLockScreenRoot)
    {
        super(paramContext, paramLockScreenRoot);
        setFocusable(true);
        setFocusableInTouchMode(true);
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        int i = paramMotionEvent.getActionMasked();
        int j = (int)paramMotionEvent.getX();
        int k = (int)paramMotionEvent.getY();
        if (i == 2)
            if (SystemClock.elapsedRealtime() - this.mLastTouchTime >= 1000L)
            {
                int m = j - this.mPreX;
                int n = k - this.mPreY;
                if (m * m + n * n > 25)
                {
                    ((LockScreenRoot)this.mRoot).pokeWakelock();
                    this.mLastTouchTime = SystemClock.elapsedRealtime();
                    this.mPreX = j;
                    this.mPreY = k;
                }
            }
        while (true)
        {
            return super.onTouchEvent(paramMotionEvent);
            if (i == 0)
            {
                this.mPreX = j;
                this.mPreY = k;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.miui.internal.policy.impl.AwesomeLockScreenImp.AwesomeLockScreenView
 * JD-Core Version:        0.6.2
 */