package com.miui.internal.policy.impl.AwesomeLockScreenImp;

import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.ScreenElementRoot;
import miui.app.screenelement.elements.ScreenElement;
import miui.app.screenelement.elements.ScreenElementFactory;
import org.w3c.dom.Element;

public class LockScreenElementFactory extends ScreenElementFactory
{
    private final LockScreenRoot.UnlockerCallback mUnlockerCallback;
    private final UnlockerListener mUnlockerListener;

    public LockScreenElementFactory(LockScreenRoot.UnlockerCallback paramUnlockerCallback, UnlockerListener paramUnlockerListener)
    {
        this.mUnlockerCallback = paramUnlockerCallback;
        this.mUnlockerListener = paramUnlockerListener;
    }

    public ScreenElement createInstance(Element paramElement, ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot)
        throws ScreenElementLoadException
    {
        String str = paramElement.getTagName();
        Object localObject;
        if (str.equalsIgnoreCase("Unlocker"))
            localObject = new UnlockerScreenElement(paramElement, paramScreenContext, paramScreenElementRoot, this.mUnlockerCallback, this.mUnlockerListener);
        while (true)
        {
            return localObject;
            if (str.equalsIgnoreCase("Wallpaper"))
                localObject = new WallpaperScreenElement(paramElement, paramScreenContext, paramScreenElementRoot);
            else
                localObject = super.createInstance(paramElement, paramScreenContext, paramScreenElementRoot);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.miui.internal.policy.impl.AwesomeLockScreenImp.LockScreenElementFactory
 * JD-Core Version:        0.6.2
 */