package com.miui.internal.policy.impl;

import android.content.Intent;

public abstract interface KeyguardScreenCallback extends com.android.internal.policy.impl.KeyguardScreenCallback
{
    public abstract void setPendingIntent(Intent paramIntent);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.miui.internal.policy.impl.KeyguardScreenCallback
 * JD-Core Version:        0.6.2
 */