package com.android.internal.policy.impl;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.provider.Settings.Bookmarks;
import android.util.Log;
import android.util.SparseArray;
import android.view.KeyCharacterMap;
import java.net.URISyntaxException;

class ShortcutManager extends ContentObserver
{
    private static final int COLUMN_INTENT = 1;
    private static final int COLUMN_SHORTCUT = 0;
    private static final String TAG = "ShortcutManager";
    private static final String[] sProjection = arrayOfString;
    private Context mContext;
    private Cursor mCursor;
    private SparseArray<Intent> mShortcutIntents;

    static
    {
        String[] arrayOfString = new String[2];
        arrayOfString[0] = "shortcut";
        arrayOfString[1] = "intent";
    }

    public ShortcutManager(Context paramContext, Handler paramHandler)
    {
        super(paramHandler);
        this.mContext = paramContext;
        this.mShortcutIntents = new SparseArray();
    }

    private void updateShortcuts()
    {
        Cursor localCursor = this.mCursor;
        if (!localCursor.requery())
        {
            Log.e("ShortcutManager", "ShortcutObserver could not re-query shortcuts.");
            label22: return;
        }
        else
        {
            this.mShortcutIntents.clear();
        }
        while (true)
        {
            if (!localCursor.moveToNext())
                break label22;
            int i = localCursor.getInt(0);
            if (i == 0)
                break;
            String str = localCursor.getString(1);
            Object localObject = null;
            try
            {
                Intent localIntent = Intent.getIntent(str);
                localObject = localIntent;
                if (localObject == null)
                    continue;
                this.mShortcutIntents.put(i, localObject);
            }
            catch (URISyntaxException localURISyntaxException)
            {
                while (true)
                    Log.w("ShortcutManager", "Intent URI for shortcut invalid.", localURISyntaxException);
            }
        }
    }

    public Intent getIntent(KeyCharacterMap paramKeyCharacterMap, int paramInt1, int paramInt2)
    {
        Intent localIntent = null;
        int i = paramKeyCharacterMap.get(paramInt1, paramInt2);
        if (i != 0)
            localIntent = (Intent)this.mShortcutIntents.get(i);
        if (localIntent == null)
        {
            int j = Character.toLowerCase(paramKeyCharacterMap.getDisplayLabel(paramInt1));
            if (j != 0)
                localIntent = (Intent)this.mShortcutIntents.get(j);
        }
        return localIntent;
    }

    public void observe()
    {
        this.mCursor = this.mContext.getContentResolver().query(Settings.Bookmarks.CONTENT_URI, sProjection, null, null, null);
        this.mCursor.registerContentObserver(this);
        updateShortcuts();
    }

    public void onChange(boolean paramBoolean)
    {
        updateShortcuts();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.ShortcutManager
 * JD-Core Version:        0.6.2
 */