package com.android.internal.policy.impl;

import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.media.AudioManager;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.android.internal.telephony.IccCard.State;
import com.android.internal.widget.LockPatternUtils;
import com.miui.internal.policy.impl.AwesomeLockScreenImp.AwesomeLockScreenView;
import com.miui.internal.policy.impl.AwesomeLockScreenImp.LockScreenElementFactory;
import com.miui.internal.policy.impl.AwesomeLockScreenImp.LockScreenResourceLoader;
import com.miui.internal.policy.impl.AwesomeLockScreenImp.LockScreenRoot;
import com.miui.internal.policy.impl.AwesomeLockScreenImp.LockScreenRoot.UnlockerCallback;
import com.miui.internal.policy.impl.AwesomeLockScreenImp.UnlockerListener;
import com.miui.internal.policy.impl.AwesomeLockScreenImp.UnlockerScreenElement;
import com.miui.internal.policy.impl.KeyguardScreenCallback;
import java.util.Collection;
import java.util.Iterator;
import miui.app.screenelement.LifecycleResourceManager;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.util.ConfigFile;
import miui.app.screenelement.util.ConfigFile.Variable;
import miui.app.screenelement.util.Task;
import miui.app.screenelement.util.Utils;
import miui.content.res.ThemeResources;
import miui.util.HapticFeedbackUtil;

class AwesomeLockScreen extends FrameLayout
    implements KeyguardScreen, KeyguardUpdateMonitor.InfoCallback, KeyguardUpdateMonitor.SimStateCallback, LockScreenRoot.UnlockerCallback, UnlockerListener, QcomCallback
{
    private static final String COMMAND_PAUSE = "pause";
    private static final String COMMAND_RESUME = "resume";
    private static final boolean DBG = false;
    private static final String OWNER_INFO_VAR = "owner_info";
    private static final String TAG = "AwesomeLockScreen";
    private static HapticFeedbackUtil mHapticFeedbackUtil;
    private static long sStartTime;
    private static long sTotalWakenTime;
    private boolean isPaused = false;
    private AudioManager mAudioManager;
    private final KeyguardScreenCallback mCallback;
    private ConfigFile mConfig;
    private LockPatternUtils mLockPatternUtils;
    private final ScreenContext mLockscreenContext;
    private AwesomeLockScreenView mLockscreenView;
    private LifecycleResourceManager mResourceMgr;
    private LockScreenRoot mRoot;
    private KeyguardUpdateMonitor mUpdateMonitor;
    private long mWakeStartTime;

    AwesomeLockScreen(Context paramContext, Configuration paramConfiguration, LockPatternUtils paramLockPatternUtils, KeyguardUpdateMonitor paramKeyguardUpdateMonitor, KeyguardScreenCallback paramKeyguardScreenCallback)
    {
        super(paramContext);
        this.mLockPatternUtils = paramLockPatternUtils;
        this.mUpdateMonitor = paramKeyguardUpdateMonitor;
        this.mCallback = paramKeyguardScreenCallback;
        setFocusable(true);
        setFocusableInTouchMode(true);
        if (mHapticFeedbackUtil == null)
            mHapticFeedbackUtil = new HapticFeedbackUtil(paramContext, true);
        this.mAudioManager = ((AudioManager)paramContext.getSystemService("audio"));
        paramKeyguardUpdateMonitor.registerInfoCallback(this);
        paramKeyguardUpdateMonitor.registerSimStateCallback(this);
        this.mResourceMgr = new LifecycleResourceManager(new LockScreenResourceLoader().setLocal(this.mContext.getResources().getConfiguration().locale), 3600000L, 3600000L);
        this.mLockscreenContext = new ScreenContext(this.mContext, this.mResourceMgr, new LockScreenElementFactory(this, this));
        loadConfig();
        ContentResolver localContentResolver = getContext().getContentResolver();
        int i;
        if (Settings.Secure.getInt(localContentResolver, "lock_screen_owner_info_enabled", 1) != 0)
        {
            i = 1;
            if (i == 0)
                break label330;
        }
        label330: for (String str = Settings.Secure.getString(localContentResolver, "lock_screen_owner_info"); ; str = null)
        {
            Utils.putVariableString("owner_info", this.mLockscreenContext.mVariables, str);
            this.mRoot = new LockScreenRoot(this.mLockscreenContext, this);
            this.mRoot.load();
            this.mLockscreenView = new AwesomeLockScreenView(this.mContext, this.mRoot);
            FrameLayout.LayoutParams localLayoutParams = new FrameLayout.LayoutParams(-1, -1);
            addView(this.mLockscreenView, localLayoutParams);
            onRefreshBatteryInfo(this.mUpdateMonitor.shouldShowBatteryInfo(), this.mUpdateMonitor.isDevicePluggedIn(), this.mUpdateMonitor.getBatteryLevel());
            if (sStartTime == 0L)
                sStartTime = System.currentTimeMillis() / 1000L;
            this.mWakeStartTime = (System.currentTimeMillis() / 1000L);
            return;
            i = 0;
            break;
        }
    }

    private void loadConfig()
    {
        this.mConfig = new ConfigFile();
        if (!this.mConfig.load(ThemeResources.sAppliedLockstyleConfigPath))
            this.mConfig = null;
        while (true)
        {
            return;
            Iterator localIterator1 = this.mConfig.getVariables().iterator();
            while (localIterator1.hasNext())
            {
                ConfigFile.Variable localVariable = (ConfigFile.Variable)localIterator1.next();
                if (TextUtils.equals(localVariable.type, "string"))
                    Utils.putVariableString(localVariable.name, this.mLockscreenContext.mVariables, localVariable.value);
                else if (TextUtils.equals(localVariable.type, "number"))
                    try
                    {
                        double d = Double.parseDouble(localVariable.value);
                        Utils.putVariableNumber(localVariable.name, this.mLockscreenContext.mVariables, Double.valueOf(d));
                    }
                    catch (NumberFormatException localNumberFormatException)
                    {
                    }
            }
            Iterator localIterator2 = this.mConfig.getTasks().iterator();
            while (localIterator2.hasNext())
            {
                Task localTask = (Task)localIterator2.next();
                Utils.putVariableString(localTask.id, "name", this.mLockscreenContext.mVariables, localTask.name);
                Utils.putVariableString(localTask.id, "package", this.mLockscreenContext.mVariables, localTask.packageName);
                Utils.putVariableString(localTask.id, "class", this.mLockscreenContext.mVariables, localTask.className);
            }
        }
    }

    public void cleanUp()
    {
        this.mLockscreenView.cleanUp();
        this.mUpdateMonitor.removeCallback(this);
        this.mLockPatternUtils = null;
        this.mUpdateMonitor = null;
    }

    public void endUnlockMoving(UnlockerScreenElement paramUnlockerScreenElement)
    {
        if (this.mRoot != null)
            this.mRoot.endUnlockMoving(paramUnlockerScreenElement);
    }

    public Task findTask(String paramString)
    {
        if (this.mConfig == null);
        for (Task localTask = null; ; localTask = this.mConfig.getTask(paramString))
            return localTask;
    }

    public void haptic(int paramInt)
    {
        mHapticFeedbackUtil.performHapticFeedback(1, false);
    }

    public boolean isDisplayDesktop()
    {
        return this.mRoot.isDisplayDesktop();
    }

    public boolean isSecure()
    {
        return this.mLockPatternUtils.isSecure();
    }

    public boolean isSoundEnable()
    {
        if (this.mAudioManager.getRingerMode() == 2);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean needsInput()
    {
        return false;
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
    }

    public void onClockVisibilityChanged()
    {
    }

    protected void onConfigurationChanged(Configuration paramConfiguration)
    {
        super.onConfigurationChanged(paramConfiguration);
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
    }

    public void onDevicePolicyManagerStateChanged()
    {
    }

    public void onDeviceProvisioned()
    {
    }

    public void onPause()
    {
        this.mLockscreenView.onPause();
        this.mRoot.onCommand("pause");
        this.isPaused = true;
        this.mResourceMgr.checkCache();
        sTotalWakenTime = System.currentTimeMillis() / 1000L - this.mWakeStartTime + sTotalWakenTime;
    }

    public void onPhoneStateChanged(int paramInt)
    {
    }

    public void onPhoneStateChanged(String paramString)
    {
    }

    public void onRefreshBatteryInfo(boolean paramBoolean1, boolean paramBoolean2, int paramInt)
    {
        if (this.mRoot != null)
            this.mRoot.onRefreshBatteryInfo(paramBoolean1, paramBoolean2, paramInt);
    }

    public void onRefreshCarrierInfo(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
    {
    }

    public void onRefreshCarrierInfo(CharSequence paramCharSequence1, CharSequence paramCharSequence2, int paramInt)
    {
    }

    public void onResume()
    {
        this.mLockscreenView.onResume();
        this.mRoot.onCommand("resume");
        this.isPaused = false;
        this.mWakeStartTime = (System.currentTimeMillis() / 1000L);
    }

    public void onRingerModeChanged(int paramInt)
    {
    }

    public void onSimStateChanged(IccCard.State paramState)
    {
    }

    public void onSimStateChanged(IccCard.State paramState, int paramInt)
    {
    }

    public void onTimeChanged()
    {
    }

    public void onUserChanged(int paramInt)
    {
    }

    public void pokeWakelock()
    {
        this.mCallback.pokeWakelock();
    }

    public void pokeWakelock(int paramInt)
    {
        this.mCallback.pokeWakelock(paramInt);
    }

    public void startUnlockMoving(UnlockerScreenElement paramUnlockerScreenElement)
    {
        if (this.mRoot != null)
            this.mRoot.startUnlockMoving(paramUnlockerScreenElement);
    }

    public void unlocked(Intent paramIntent)
    {
        this.mCallback.setPendingIntent(paramIntent);
        post(new Runnable()
        {
            public void run()
            {
                try
                {
                    AwesomeLockScreen.this.mCallback.goToUnlockScreen();
                    return;
                }
                catch (ActivityNotFoundException localActivityNotFoundException)
                {
                    while (true)
                    {
                        Log.e("AwesomeLockScreen", localActivityNotFoundException.toString());
                        localActivityNotFoundException.printStackTrace();
                    }
                }
            }
        });
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = Long.valueOf(sTotalWakenTime);
        arrayOfObject[1] = Long.valueOf(System.currentTimeMillis() / 1000L - sStartTime);
        Log.i("AwesomeLockScreen", String.format("lockscreen awake time: [%d sec] in time range: [%d sec]", arrayOfObject));
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.AwesomeLockScreen
 * JD-Core Version:        0.6.2
 */