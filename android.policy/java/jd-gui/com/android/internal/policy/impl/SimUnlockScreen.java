package com.android.internal.policy.impl;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.ITelephony.Stub;
import com.android.internal.widget.LockPatternUtils;

public class SimUnlockScreen extends LinearLayout
    implements KeyguardScreen, View.OnClickListener
{
    private static final char[] DIGITS = arrayOfChar;
    private static final int DIGIT_PRESS_WAKE_MILLIS = 5000;
    private View mBackSpaceButton;
    private final KeyguardScreenCallback mCallback;
    private int mCreationOrientation;
    private int mEnteredDigits;
    private final int[] mEnteredPin;
    private TextView mHeaderText;
    private int mKeyboardHidden;
    private KeyguardStatusViewManager mKeyguardStatusViewManager;
    private LockPatternUtils mLockPatternUtils;
    private TextView mOkButton;
    private TextView mPinText;
    private ProgressDialog mSimUnlockProgressDialog;
    private final KeyguardUpdateMonitor mUpdateMonitor;

    static
    {
        char[] arrayOfChar = new char[10];
        arrayOfChar[0] = 48;
        arrayOfChar[1] = 49;
        arrayOfChar[2] = 50;
        arrayOfChar[3] = 51;
        arrayOfChar[4] = 52;
        arrayOfChar[5] = 53;
        arrayOfChar[6] = 54;
        arrayOfChar[7] = 55;
        arrayOfChar[8] = 56;
        arrayOfChar[9] = 57;
    }

    public SimUnlockScreen(Context paramContext, Configuration paramConfiguration, KeyguardUpdateMonitor paramKeyguardUpdateMonitor, KeyguardScreenCallback paramKeyguardScreenCallback, LockPatternUtils paramLockPatternUtils)
    {
        super(paramContext);
        int[] arrayOfInt = new int[8];
        arrayOfInt[0] = 0;
        arrayOfInt[1] = 0;
        arrayOfInt[2] = 0;
        arrayOfInt[3] = 0;
        arrayOfInt[4] = 0;
        arrayOfInt[5] = 0;
        arrayOfInt[6] = 0;
        arrayOfInt[7] = 0;
        this.mEnteredPin = arrayOfInt;
        this.mEnteredDigits = 0;
        this.mSimUnlockProgressDialog = null;
        this.mUpdateMonitor = paramKeyguardUpdateMonitor;
        this.mCallback = paramKeyguardScreenCallback;
        this.mCreationOrientation = paramConfiguration.orientation;
        this.mKeyboardHidden = paramConfiguration.hardKeyboardHidden;
        this.mLockPatternUtils = paramLockPatternUtils;
        LayoutInflater localLayoutInflater = LayoutInflater.from(paramContext);
        if (this.mKeyboardHidden == 1)
            localLayoutInflater.inflate(17367122, this, true);
        while (true)
        {
            this.mHeaderText = ((TextView)findViewById(16908982));
            this.mPinText = ((TextView)findViewById(16908984));
            this.mBackSpaceButton = findViewById(16908985);
            this.mBackSpaceButton.setOnClickListener(this);
            this.mOkButton = ((TextView)findViewById(16908954));
            this.mHeaderText.setText(17040108);
            this.mPinText.setFocusable(false);
            this.mOkButton.setOnClickListener(this);
            this.mKeyguardStatusViewManager = new KeyguardStatusViewManager(this, paramKeyguardUpdateMonitor, paramLockPatternUtils, paramKeyguardScreenCallback, false);
            setFocusableInTouchMode(true);
            return;
            localLayoutInflater.inflate(17367123, this, true);
            new TouchInput(null);
        }
    }

    private void checkPin()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 65	com/android/internal/policy/impl/SimUnlockScreen:mEnteredDigits	I
        //     4: iconst_4
        //     5: if_icmpge +36 -> 41
        //     8: aload_0
        //     9: getfield 105	com/android/internal/policy/impl/SimUnlockScreen:mHeaderText	Landroid/widget/TextView;
        //     12: ldc 162
        //     14: invokevirtual 125	android/widget/TextView:setText	(I)V
        //     17: aload_0
        //     18: getfield 108	com/android/internal/policy/impl/SimUnlockScreen:mPinText	Landroid/widget/TextView;
        //     21: ldc 164
        //     23: invokevirtual 167	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
        //     26: aload_0
        //     27: iconst_0
        //     28: putfield 65	com/android/internal/policy/impl/SimUnlockScreen:mEnteredDigits	I
        //     31: aload_0
        //     32: getfield 71	com/android/internal/policy/impl/SimUnlockScreen:mCallback	Lcom/android/internal/policy/impl/KeyguardScreenCallback;
        //     35: invokeinterface 172 1 0
        //     40: return
        //     41: aload_0
        //     42: invokespecial 176	com/android/internal/policy/impl/SimUnlockScreen:getSimUnlockProgressDialog	()Landroid/app/Dialog;
        //     45: invokevirtual 181	android/app/Dialog:show	()V
        //     48: new 10	com/android/internal/policy/impl/SimUnlockScreen$1
        //     51: dup
        //     52: aload_0
        //     53: aload_0
        //     54: getfield 108	com/android/internal/policy/impl/SimUnlockScreen:mPinText	Landroid/widget/TextView;
        //     57: invokevirtual 185	android/widget/TextView:getText	()Ljava/lang/CharSequence;
        //     60: invokevirtual 191	java/lang/Object:toString	()Ljava/lang/String;
        //     63: invokespecial 194	com/android/internal/policy/impl/SimUnlockScreen$1:<init>	(Lcom/android/internal/policy/impl/SimUnlockScreen;Ljava/lang/String;)V
        //     66: invokevirtual 197	com/android/internal/policy/impl/SimUnlockScreen$1:start	()V
        //     69: goto -29 -> 40
    }

    private Dialog getSimUnlockProgressDialog()
    {
        if (this.mSimUnlockProgressDialog == null)
        {
            this.mSimUnlockProgressDialog = new ProgressDialog(this.mContext);
            this.mSimUnlockProgressDialog.setMessage(this.mContext.getString(17040149));
            this.mSimUnlockProgressDialog.setIndeterminate(true);
            this.mSimUnlockProgressDialog.setCancelable(false);
            this.mSimUnlockProgressDialog.getWindow().setType(2009);
        }
        return this.mSimUnlockProgressDialog;
    }

    private void reportDigit(int paramInt)
    {
        if (this.mEnteredDigits == 0)
            this.mPinText.setText("");
        if (this.mEnteredDigits == 8);
        while (true)
        {
            return;
            this.mPinText.append(Integer.toString(paramInt));
            int[] arrayOfInt = this.mEnteredPin;
            int i = this.mEnteredDigits;
            this.mEnteredDigits = (i + 1);
            arrayOfInt[i] = paramInt;
        }
    }

    public void cleanUp()
    {
        if (this.mSimUnlockProgressDialog != null)
        {
            this.mSimUnlockProgressDialog.dismiss();
            this.mSimUnlockProgressDialog = null;
        }
        this.mUpdateMonitor.removeCallback(this);
    }

    public boolean needsInput()
    {
        return true;
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        updateConfiguration();
    }

    public void onClick(View paramView)
    {
        if (paramView == this.mBackSpaceButton)
        {
            Editable localEditable = this.mPinText.getEditableText();
            int i = localEditable.length();
            if (i > 0)
            {
                localEditable.delete(i - 1, i);
                this.mEnteredDigits = (-1 + this.mEnteredDigits);
            }
            this.mCallback.pokeWakelock();
        }
        while (true)
        {
            return;
            if (paramView == this.mOkButton)
                checkPin();
        }
    }

    protected void onConfigurationChanged(Configuration paramConfiguration)
    {
        super.onConfigurationChanged(paramConfiguration);
        updateConfiguration();
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
        boolean bool = true;
        if (paramInt == 4)
            this.mCallback.goToLockScreen();
        while (true)
        {
            return bool;
            int i = paramKeyEvent.getMatch(DIGITS);
            if (i != 0)
                reportDigit(i + -48);
            else if (paramInt == 67)
            {
                if (this.mEnteredDigits > 0)
                {
                    this.mPinText.onKeyDown(paramInt, paramKeyEvent);
                    this.mEnteredDigits = (-1 + this.mEnteredDigits);
                }
            }
            else if (paramInt == 66)
                checkPin();
            else
                bool = false;
        }
    }

    public void onPause()
    {
        this.mKeyguardStatusViewManager.onPause();
    }

    public void onResume()
    {
        this.mHeaderText.setText(17040108);
        this.mPinText.setText("");
        this.mEnteredDigits = 0;
        this.mKeyguardStatusViewManager.onResume();
    }

    void updateConfiguration()
    {
        Configuration localConfiguration = getResources().getConfiguration();
        if (localConfiguration.orientation != this.mCreationOrientation)
            this.mCallback.recreateMe(localConfiguration);
        while (true)
        {
            return;
            if (localConfiguration.hardKeyboardHidden != this.mKeyboardHidden)
                this.mKeyboardHidden = localConfiguration.hardKeyboardHidden;
        }
    }

    private class TouchInput
        implements View.OnClickListener
    {
        private TextView mCancelButton = (TextView)SimUnlockScreen.this.findViewById(16908905);
        private TextView mEight = (TextView)SimUnlockScreen.this.findViewById(16909146);
        private TextView mFive = (TextView)SimUnlockScreen.this.findViewById(16909143);
        private TextView mFour = (TextView)SimUnlockScreen.this.findViewById(16909142);
        private TextView mNine = (TextView)SimUnlockScreen.this.findViewById(16909147);
        private TextView mOne = (TextView)SimUnlockScreen.this.findViewById(16909139);
        private TextView mSeven = (TextView)SimUnlockScreen.this.findViewById(16909145);
        private TextView mSix = (TextView)SimUnlockScreen.this.findViewById(16909144);
        private TextView mThree = (TextView)SimUnlockScreen.this.findViewById(16909141);
        private TextView mTwo = (TextView)SimUnlockScreen.this.findViewById(16909140);
        private TextView mZero = (TextView)SimUnlockScreen.this.findViewById(16909148);

        private TouchInput()
        {
            this.mZero.setText("0");
            this.mOne.setText("1");
            this.mTwo.setText("2");
            this.mThree.setText("3");
            this.mFour.setText("4");
            this.mFive.setText("5");
            this.mSix.setText("6");
            this.mSeven.setText("7");
            this.mEight.setText("8");
            this.mNine.setText("9");
            this.mZero.setOnClickListener(this);
            this.mOne.setOnClickListener(this);
            this.mTwo.setOnClickListener(this);
            this.mThree.setOnClickListener(this);
            this.mFour.setOnClickListener(this);
            this.mFive.setOnClickListener(this);
            this.mSix.setOnClickListener(this);
            this.mSeven.setOnClickListener(this);
            this.mEight.setOnClickListener(this);
            this.mNine.setOnClickListener(this);
            this.mCancelButton.setOnClickListener(this);
        }

        private int checkDigit(View paramView)
        {
            int i = -1;
            if (paramView == this.mZero)
                i = 0;
            while (true)
            {
                return i;
                if (paramView == this.mOne)
                    i = 1;
                else if (paramView == this.mTwo)
                    i = 2;
                else if (paramView == this.mThree)
                    i = 3;
                else if (paramView == this.mFour)
                    i = 4;
                else if (paramView == this.mFive)
                    i = 5;
                else if (paramView == this.mSix)
                    i = 6;
                else if (paramView == this.mSeven)
                    i = 7;
                else if (paramView == this.mEight)
                    i = 8;
                else if (paramView == this.mNine)
                    i = 9;
            }
        }

        public void onClick(View paramView)
        {
            if (paramView == this.mCancelButton)
            {
                SimUnlockScreen.this.mPinText.setText("");
                SimUnlockScreen.this.mCallback.goToLockScreen();
            }
            while (true)
            {
                return;
                int i = checkDigit(paramView);
                if (i >= 0)
                {
                    SimUnlockScreen.this.mCallback.pokeWakelock(5000);
                    SimUnlockScreen.this.reportDigit(i);
                }
            }
        }
    }

    private abstract class CheckSimPin extends Thread
    {
        private final String mPin;

        protected CheckSimPin(String arg2)
        {
            Object localObject;
            this.mPin = localObject;
        }

        abstract void onSimLockChangedResponse(boolean paramBoolean);

        public void run()
        {
            try
            {
                final boolean bool = ITelephony.Stub.asInterface(ServiceManager.checkService("phone")).supplyPin(this.mPin);
                SimUnlockScreen.this.post(new Runnable()
                {
                    public void run()
                    {
                        SimUnlockScreen.CheckSimPin.this.onSimLockChangedResponse(bool);
                    }
                });
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    SimUnlockScreen.this.post(new Runnable()
                    {
                        public void run()
                        {
                            SimUnlockScreen.CheckSimPin.this.onSimLockChangedResponse(false);
                        }
                    });
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.SimUnlockScreen
 * JD-Core Version:        0.6.2
 */