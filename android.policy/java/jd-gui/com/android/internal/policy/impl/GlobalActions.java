package com.android.internal.policy.impl;

import android.app.ActivityManagerNative;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.IActivityManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.UserInfo;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.os.Vibrator;
import android.provider.Settings.System;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.IWindowManager;
import android.view.IWindowManager.Stub;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManagerPolicy.WindowManagerFuncs;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class GlobalActions
    implements DialogInterface.OnDismissListener, DialogInterface.OnClickListener
{
    private static final int DIALOG_DISMISS_DELAY = 300;
    private static final int MESSAGE_DISMISS = 0;
    private static final int MESSAGE_REFRESH = 1;
    private static final int MESSAGE_SHOW = 2;
    private static final boolean SHOW_SILENT_TOGGLE = true;
    private static final String TAG = "GlobalActions";
    private MyAdapter mAdapter;
    private ContentObserver mAirplaneModeObserver = new ContentObserver(new Handler())
    {
        public void onChange(boolean paramAnonymousBoolean)
        {
            GlobalActions.this.onAirplaneModeChanged();
        }
    };
    private ToggleAction mAirplaneModeOn;
    private GlobalActions.ToggleAction.State mAirplaneState = GlobalActions.ToggleAction.State.Off;
    private final AudioManager mAudioManager;
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            String str = paramAnonymousIntent.getAction();
            if (("android.intent.action.CLOSE_SYSTEM_DIALOGS".equals(str)) || ("android.intent.action.SCREEN_OFF".equals(str)))
                if (!"globalactions".equals(paramAnonymousIntent.getStringExtra("reason")))
                    GlobalActions.this.mHandler.sendEmptyMessage(0);
            while (true)
            {
                return;
                if (("android.intent.action.EMERGENCY_CALLBACK_MODE_CHANGED".equals(str)) && (!paramAnonymousIntent.getBooleanExtra("PHONE_IN_ECM_STATE", false)) && (GlobalActions.this.mIsWaitingForEcmExit))
                {
                    GlobalActions.access$102(GlobalActions.this, false);
                    GlobalActions.this.changeAirplaneModeSystemSetting(true);
                }
            }
        }
    };
    private final Context mContext;
    private boolean mDeviceProvisioned = false;
    private AlertDialog mDialog;
    private Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            switch (paramAnonymousMessage.what)
            {
            default:
            case 0:
            case 1:
            case 2:
            }
            while (true)
            {
                return;
                if (GlobalActions.this.mDialog != null)
                {
                    GlobalActions.this.mDialog.dismiss();
                    continue;
                    GlobalActions.this.refreshSilentMode();
                    GlobalActions.this.mAdapter.notifyDataSetChanged();
                    continue;
                    GlobalActions.this.handleShow();
                }
            }
        }
    };
    private boolean mHasTelephony;
    private boolean mHasVibrator;
    private IWindowManager mIWindowManager;
    private boolean mIsWaitingForEcmExit = false;
    private ArrayList<Action> mItems;
    private boolean mKeyguardShowing = false;
    PhoneStateListener mPhoneStateListener = new PhoneStateListener()
    {
        public void onServiceStateChanged(ServiceState paramAnonymousServiceState)
        {
            if (!GlobalActions.this.mHasTelephony)
                return;
            int i;
            label21: GlobalActions localGlobalActions;
            if (paramAnonymousServiceState.getState() == 3)
            {
                i = 1;
                localGlobalActions = GlobalActions.this;
                if (i == 0)
                    break label77;
            }
            label77: for (GlobalActions.ToggleAction.State localState = GlobalActions.ToggleAction.State.On; ; localState = GlobalActions.ToggleAction.State.Off)
            {
                GlobalActions.access$402(localGlobalActions, localState);
                GlobalActions.this.mAirplaneModeOn.updateState(GlobalActions.this.mAirplaneState);
                GlobalActions.this.mAdapter.notifyDataSetChanged();
                break;
                i = 0;
                break label21;
            }
        }
    };
    private BroadcastReceiver mRingerModeReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            if (paramAnonymousIntent.getAction().equals("android.media.RINGER_MODE_CHANGED"))
                GlobalActions.this.mHandler.sendEmptyMessage(1);
        }
    };
    private Action mSilentModeAction;
    private final WindowManagerPolicy.WindowManagerFuncs mWindowManagerFuncs;

    public GlobalActions(Context paramContext, WindowManagerPolicy.WindowManagerFuncs paramWindowManagerFuncs)
    {
        this.mContext = paramContext;
        this.mWindowManagerFuncs = paramWindowManagerFuncs;
        this.mAudioManager = ((AudioManager)this.mContext.getSystemService("audio"));
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("android.intent.action.CLOSE_SYSTEM_DIALOGS");
        localIntentFilter.addAction("android.intent.action.SCREEN_OFF");
        localIntentFilter.addAction("android.intent.action.EMERGENCY_CALLBACK_MODE_CHANGED");
        paramContext.registerReceiver(this.mBroadcastReceiver, localIntentFilter);
        ((TelephonyManager)paramContext.getSystemService("phone")).listen(this.mPhoneStateListener, 1);
        this.mHasTelephony = ((ConnectivityManager)paramContext.getSystemService("connectivity")).isNetworkSupported(0);
        this.mContext.getContentResolver().registerContentObserver(Settings.System.getUriFor("airplane_mode_on"), true, this.mAirplaneModeObserver);
        Vibrator localVibrator = (Vibrator)this.mContext.getSystemService("vibrator");
        if ((localVibrator != null) && (localVibrator.hasVibrator()));
        for (boolean bool = true; ; bool = false)
        {
            this.mHasVibrator = bool;
            return;
        }
    }

    private void changeAirplaneModeSystemSetting(boolean paramBoolean)
    {
        ContentResolver localContentResolver = this.mContext.getContentResolver();
        int i;
        if (paramBoolean)
        {
            i = 1;
            Settings.System.putInt(localContentResolver, "airplane_mode_on", i);
            Intent localIntent = new Intent("android.intent.action.AIRPLANE_MODE");
            localIntent.addFlags(536870912);
            localIntent.putExtra("state", paramBoolean);
            this.mContext.sendBroadcast(localIntent);
            if (!this.mHasTelephony)
                if (!paramBoolean)
                    break label90;
        }
        label90: for (GlobalActions.ToggleAction.State localState = GlobalActions.ToggleAction.State.On; ; localState = GlobalActions.ToggleAction.State.Off)
        {
            this.mAirplaneState = localState;
            return;
            i = 0;
            break;
        }
    }

    private AlertDialog createDialog()
    {
        if (!this.mHasVibrator)
            this.mSilentModeAction = new SilentModeToggleAction();
        while (true)
        {
            this.mAirplaneModeOn = new ToggleAction(17302236, 17302237, 17039675, 17039676, 17039677)
            {
                protected void changeStateFromPress(boolean paramAnonymousBoolean)
                {
                    if (!GlobalActions.this.mHasTelephony)
                        return;
                    if (!Boolean.parseBoolean(SystemProperties.get("ril.cdma.inecmmode")))
                        if (!paramAnonymousBoolean)
                            break label50;
                    label50: for (GlobalActions.ToggleAction.State localState = GlobalActions.ToggleAction.State.TurningOn; ; localState = GlobalActions.ToggleAction.State.TurningOff)
                    {
                        this.mState = localState;
                        GlobalActions.access$402(GlobalActions.this, this.mState);
                        break;
                        break;
                    }
                }

                void onToggle(boolean paramAnonymousBoolean)
                {
                    if ((GlobalActions.this.mHasTelephony) && (Boolean.parseBoolean(SystemProperties.get("ril.cdma.inecmmode"))))
                    {
                        GlobalActions.access$102(GlobalActions.this, true);
                        Intent localIntent = new Intent("android.intent.action.ACTION_SHOW_NOTICE_ECM_BLOCK_OTHERS", null);
                        localIntent.addFlags(268435456);
                        GlobalActions.this.mContext.startActivity(localIntent);
                    }
                    while (true)
                    {
                        return;
                        GlobalActions.this.changeAirplaneModeSystemSetting(paramAnonymousBoolean);
                    }
                }

                public boolean showBeforeProvisioning()
                {
                    return false;
                }

                public boolean showDuringKeyguard()
                {
                    return true;
                }
            };
            onAirplaneModeChanged();
            this.mItems = new ArrayList();
            this.mItems.add(new SinglePressAction(17301552, 17039671)
            {
                public boolean onLongPress()
                {
                    GlobalActions.this.mWindowManagerFuncs.rebootSafeMode();
                    return true;
                }

                public void onPress()
                {
                    GlobalActions.this.mWindowManagerFuncs.shutdown();
                }

                public boolean showBeforeProvisioning()
                {
                    return true;
                }

                public boolean showDuringKeyguard()
                {
                    return true;
                }
            });
            this.mItems.add(this.mAirplaneModeOn);
            this.mItems.add(this.mSilentModeAction);
            List localList = this.mContext.getPackageManager().getUsers();
            if (localList.size() > 1)
                try
                {
                    UserInfo localUserInfo3 = ActivityManagerNative.getDefault().getCurrentUser();
                    localUserInfo1 = localUserInfo3;
                    Iterator localIterator = localList.iterator();
                    while (true)
                        if (localIterator.hasNext())
                        {
                            localUserInfo2 = (UserInfo)localIterator.next();
                            if (localUserInfo1 == null)
                                if (localUserInfo2.id == 0)
                                {
                                    i = 1;
                                    StringBuilder localStringBuilder1 = new StringBuilder();
                                    if (localUserInfo2.name == null)
                                        break label337;
                                    str1 = localUserInfo2.name;
                                    StringBuilder localStringBuilder2 = localStringBuilder1.append(str1);
                                    if (i == 0)
                                        break label345;
                                    str2 = " ✔";
                                    SinglePressAction local3 = new SinglePressAction(17302305, str2)
                                    {
                                        public void onPress()
                                        {
                                            try
                                            {
                                                ActivityManagerNative.getDefault().switchUser(localUserInfo2.id);
                                                GlobalActions.this.getWindowManager().lockNow();
                                                return;
                                            }
                                            catch (RemoteException localRemoteException)
                                            {
                                                while (true)
                                                    Log.e("GlobalActions", "Couldn't switch user " + localRemoteException);
                                            }
                                        }

                                        public boolean showBeforeProvisioning()
                                        {
                                            return false;
                                        }

                                        public boolean showDuringKeyguard()
                                        {
                                            return true;
                                        }
                                    };
                                    this.mItems.add(local3);
                                    continue;
                                    this.mSilentModeAction = new SilentModeTriStateAction(this.mContext, this.mAudioManager, this.mHandler);
                                }
                        }
                }
                catch (RemoteException localRemoteException)
                {
                    while (true)
                    {
                        final UserInfo localUserInfo2;
                        String str1;
                        String str2;
                        UserInfo localUserInfo1 = null;
                        continue;
                        int i = 0;
                        continue;
                        if (localUserInfo1.id == localUserInfo2.id)
                        {
                            i = 1;
                        }
                        else
                        {
                            i = 0;
                            continue;
                            label337: str1 = "Primary";
                            continue;
                            label345: str2 = "";
                        }
                    }
                }
        }
        this.mAdapter = new MyAdapter(null);
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this.mContext);
        localBuilder.setAdapter(this.mAdapter, this).setInverseBackgroundForced(true);
        AlertDialog localAlertDialog = localBuilder.create();
        localAlertDialog.getListView().setItemsCanFocus(true);
        localAlertDialog.getListView().setLongClickable(true);
        localAlertDialog.getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            public boolean onItemLongClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
            {
                return GlobalActions.this.mAdapter.getItem(paramAnonymousInt).onLongPress();
            }
        });
        localAlertDialog.getWindow().setType(2008);
        localAlertDialog.setOnDismissListener(this);
        return localAlertDialog;
    }

    private IWindowManager getWindowManager()
    {
        if (this.mIWindowManager == null)
            this.mIWindowManager = IWindowManager.Stub.asInterface(ServiceManager.getService("window"));
        return this.mIWindowManager;
    }

    private void handleShow()
    {
        this.mDialog = createDialog();
        prepareDialog();
        this.mDialog.show();
        this.mDialog.getWindow().getDecorView().setSystemUiVisibility(65536);
    }

    private void onAirplaneModeChanged()
    {
        int i = 1;
        if (this.mHasTelephony)
            return;
        if (Settings.System.getInt(this.mContext.getContentResolver(), "airplane_mode_on", 0) == i)
            label27: if (i == 0)
                break label59;
        label59: for (GlobalActions.ToggleAction.State localState = GlobalActions.ToggleAction.State.On; ; localState = GlobalActions.ToggleAction.State.Off)
        {
            this.mAirplaneState = localState;
            this.mAirplaneModeOn.updateState(this.mAirplaneState);
            break;
            i = 0;
            break label27;
        }
    }

    private void prepareDialog()
    {
        refreshSilentMode();
        this.mAirplaneModeOn.updateState(this.mAirplaneState);
        this.mAdapter.notifyDataSetChanged();
        if (this.mKeyguardShowing)
            this.mDialog.getWindow().setType(2009);
        while (true)
        {
            IntentFilter localIntentFilter = new IntentFilter("android.media.RINGER_MODE_CHANGED");
            this.mContext.registerReceiver(this.mRingerModeReceiver, localIntentFilter);
            return;
            this.mDialog.getWindow().setType(2008);
        }
    }

    private void refreshSilentMode()
    {
        int i;
        ToggleAction localToggleAction;
        if (!this.mHasVibrator)
        {
            if (this.mAudioManager.getRingerMode() == 2)
                break label42;
            i = 1;
            localToggleAction = (ToggleAction)this.mSilentModeAction;
            if (i == 0)
                break label47;
        }
        label42: label47: for (GlobalActions.ToggleAction.State localState = GlobalActions.ToggleAction.State.On; ; localState = GlobalActions.ToggleAction.State.Off)
        {
            localToggleAction.updateState(localState);
            return;
            i = 0;
            break;
        }
    }

    public void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
        if (!(this.mAdapter.getItem(paramInt) instanceof SilentModeTriStateAction))
            paramDialogInterface.dismiss();
        this.mAdapter.getItem(paramInt).onPress();
    }

    public void onDismiss(DialogInterface paramDialogInterface)
    {
        this.mContext.unregisterReceiver(this.mRingerModeReceiver);
    }

    public void showDialog(boolean paramBoolean1, boolean paramBoolean2)
    {
        this.mKeyguardShowing = paramBoolean1;
        this.mDeviceProvisioned = paramBoolean2;
        if (this.mDialog != null)
        {
            this.mDialog.dismiss();
            this.mDialog = null;
            this.mHandler.sendEmptyMessage(2);
        }
        while (true)
        {
            return;
            handleShow();
        }
    }

    private static class SilentModeTriStateAction
        implements GlobalActions.Action, View.OnClickListener
    {
        private final int[] ITEM_IDS;
        private final AudioManager mAudioManager;
        private final Context mContext;
        private final Handler mHandler;

        SilentModeTriStateAction(Context paramContext, AudioManager paramAudioManager, Handler paramHandler)
        {
            int[] arrayOfInt = new int[3];
            arrayOfInt[0] = 16908918;
            arrayOfInt[1] = 16908919;
            arrayOfInt[2] = 16908920;
            this.ITEM_IDS = arrayOfInt;
            this.mAudioManager = paramAudioManager;
            this.mHandler = paramHandler;
            this.mContext = paramContext;
        }

        private int indexToRingerMode(int paramInt)
        {
            return paramInt;
        }

        private int ringerModeToIndex(int paramInt)
        {
            return paramInt;
        }

        public View create(Context paramContext, View paramView, ViewGroup paramViewGroup, LayoutInflater paramLayoutInflater)
        {
            View localView1 = paramLayoutInflater.inflate(17367106, paramViewGroup, false);
            int i = ringerModeToIndex(this.mAudioManager.getRingerMode());
            int j = 0;
            if (j < 3)
            {
                View localView2 = localView1.findViewById(this.ITEM_IDS[j]);
                if (i == j);
                for (boolean bool = true; ; bool = false)
                {
                    localView2.setSelected(bool);
                    localView2.setTag(Integer.valueOf(j));
                    localView2.setOnClickListener(this);
                    j++;
                    break;
                }
            }
            return localView1;
        }

        public boolean isEnabled()
        {
            return true;
        }

        public void onClick(View paramView)
        {
            if (!(paramView.getTag() instanceof Integer));
            while (true)
            {
                return;
                int i = ((Integer)paramView.getTag()).intValue();
                this.mAudioManager.setRingerMode(indexToRingerMode(i));
                this.mHandler.sendEmptyMessageDelayed(0, 300L);
            }
        }

        public boolean onLongPress()
        {
            return false;
        }

        public void onPress()
        {
        }

        public boolean showBeforeProvisioning()
        {
            return false;
        }

        public boolean showDuringKeyguard()
        {
            return true;
        }

        void willCreate()
        {
        }
    }

    private class SilentModeToggleAction extends GlobalActions.ToggleAction
    {
        public SilentModeToggleAction()
        {
            super(17302182, 17039672, 17039673, 17039674);
        }

        void onToggle(boolean paramBoolean)
        {
            if (paramBoolean)
                GlobalActions.this.mAudioManager.setRingerMode(0);
            while (true)
            {
                return;
                GlobalActions.this.mAudioManager.setRingerMode(2);
            }
        }

        public boolean showBeforeProvisioning()
        {
            return false;
        }

        public boolean showDuringKeyguard()
        {
            return true;
        }
    }

    private static abstract class ToggleAction
        implements GlobalActions.Action
    {
        protected int mDisabledIconResid;
        protected int mDisabledStatusMessageResId;
        protected int mEnabledIconResId;
        protected int mEnabledStatusMessageResId;
        protected int mMessageResId;
        protected State mState = State.Off;

        public ToggleAction(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
        {
            this.mEnabledIconResId = paramInt1;
            this.mDisabledIconResid = paramInt2;
            this.mMessageResId = paramInt3;
            this.mEnabledStatusMessageResId = paramInt4;
            this.mDisabledStatusMessageResId = paramInt5;
        }

        protected void changeStateFromPress(boolean paramBoolean)
        {
            if (paramBoolean);
            for (State localState = State.On; ; localState = State.Off)
            {
                this.mState = localState;
                return;
            }
        }

        public View create(Context paramContext, View paramView, ViewGroup paramViewGroup, LayoutInflater paramLayoutInflater)
        {
            willCreate();
            View localView = paramLayoutInflater.inflate(17367105, paramViewGroup, false);
            ImageView localImageView = (ImageView)localView.findViewById(16908294);
            TextView localTextView1 = (TextView)localView.findViewById(16908299);
            TextView localTextView2 = (TextView)localView.findViewById(16908917);
            boolean bool = isEnabled();
            if (localTextView1 != null)
            {
                localTextView1.setText(this.mMessageResId);
                localTextView1.setEnabled(bool);
            }
            int i;
            int k;
            if ((this.mState == State.On) || (this.mState == State.TurningOn))
            {
                i = 1;
                if (localImageView != null)
                {
                    Resources localResources = paramContext.getResources();
                    if (i == 0)
                        break label194;
                    k = this.mEnabledIconResId;
                    label123: localImageView.setImageDrawable(localResources.getDrawable(k));
                    localImageView.setEnabled(bool);
                }
                if (localTextView2 != null)
                    if (i == 0)
                        break label203;
            }
            label194: label203: for (int j = this.mEnabledStatusMessageResId; ; j = this.mDisabledStatusMessageResId)
            {
                localTextView2.setText(j);
                localTextView2.setVisibility(0);
                localTextView2.setEnabled(bool);
                localView.setEnabled(bool);
                return localView;
                i = 0;
                break;
                k = this.mDisabledIconResid;
                break label123;
            }
        }

        public boolean isEnabled()
        {
            if (!this.mState.inTransition());
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public boolean onLongPress()
        {
            return false;
        }

        public final void onPress()
        {
            if (this.mState.inTransition())
            {
                Log.w("GlobalActions", "shouldn't be able to toggle when in transition");
                return;
            }
            if (this.mState != State.On);
            for (boolean bool = true; ; bool = false)
            {
                onToggle(bool);
                changeStateFromPress(bool);
                break;
            }
        }

        abstract void onToggle(boolean paramBoolean);

        public void updateState(State paramState)
        {
            this.mState = paramState;
        }

        void willCreate()
        {
        }

        static enum State
        {
            private final boolean inTransition;

            static
            {
                TurningOff = new State("TurningOff", 2, true);
                On = new State("On", 3, false);
                State[] arrayOfState = new State[4];
                arrayOfState[0] = Off;
                arrayOfState[1] = TurningOn;
                arrayOfState[2] = TurningOff;
                arrayOfState[3] = On;
            }

            private State(boolean paramBoolean)
            {
                this.inTransition = paramBoolean;
            }

            public boolean inTransition()
            {
                return this.inTransition;
            }
        }
    }

    private static abstract class SinglePressAction
        implements GlobalActions.Action
    {
        private final int mIconResId;
        private final CharSequence mMessage;
        private final int mMessageResId;

        protected SinglePressAction(int paramInt1, int paramInt2)
        {
            this.mIconResId = paramInt1;
            this.mMessageResId = paramInt2;
            this.mMessage = null;
        }

        protected SinglePressAction(int paramInt, CharSequence paramCharSequence)
        {
            this.mIconResId = paramInt;
            this.mMessageResId = 0;
            this.mMessage = paramCharSequence;
        }

        public View create(Context paramContext, View paramView, ViewGroup paramViewGroup, LayoutInflater paramLayoutInflater)
        {
            View localView = paramLayoutInflater.inflate(17367105, paramViewGroup, false);
            ImageView localImageView = (ImageView)localView.findViewById(16908294);
            TextView localTextView = (TextView)localView.findViewById(16908299);
            localView.findViewById(16908917).setVisibility(8);
            localImageView.setImageDrawable(paramContext.getResources().getDrawable(this.mIconResId));
            if (this.mMessage != null)
                localTextView.setText(this.mMessage);
            while (true)
            {
                return localView;
                localTextView.setText(this.mMessageResId);
            }
        }

        public boolean isEnabled()
        {
            return true;
        }

        public boolean onLongPress()
        {
            return false;
        }

        public abstract void onPress();
    }

    private static abstract interface Action
    {
        public abstract View create(Context paramContext, View paramView, ViewGroup paramViewGroup, LayoutInflater paramLayoutInflater);

        public abstract boolean isEnabled();

        public abstract boolean onLongPress();

        public abstract void onPress();

        public abstract boolean showBeforeProvisioning();

        public abstract boolean showDuringKeyguard();
    }

    private class MyAdapter extends BaseAdapter
    {
        private MyAdapter()
        {
        }

        public boolean areAllItemsEnabled()
        {
            return false;
        }

        public int getCount()
        {
            int i = 0;
            int j = 0;
            if (j < GlobalActions.this.mItems.size())
            {
                GlobalActions.Action localAction = (GlobalActions.Action)GlobalActions.this.mItems.get(j);
                if ((GlobalActions.this.mKeyguardShowing) && (!localAction.showDuringKeyguard()));
                while (true)
                {
                    j++;
                    break;
                    if ((GlobalActions.this.mDeviceProvisioned) || (localAction.showBeforeProvisioning()))
                        i++;
                }
            }
            return i;
        }

        public GlobalActions.Action getItem(int paramInt)
        {
            int i = 0;
            int j = 0;
            if (j < GlobalActions.this.mItems.size())
            {
                GlobalActions.Action localAction = (GlobalActions.Action)GlobalActions.this.mItems.get(j);
                if ((GlobalActions.this.mKeyguardShowing) && (!localAction.showDuringKeyguard()));
                while (true)
                {
                    j++;
                    break;
                    if ((GlobalActions.this.mDeviceProvisioned) || (localAction.showBeforeProvisioning()))
                    {
                        if (i == paramInt)
                            return localAction;
                        i++;
                    }
                }
            }
            throw new IllegalArgumentException("position " + paramInt + " out of range of showable actions" + ", filtered count=" + getCount() + ", keyguardshowing=" + GlobalActions.this.mKeyguardShowing + ", provisioned=" + GlobalActions.this.mDeviceProvisioned);
        }

        public long getItemId(int paramInt)
        {
            return paramInt;
        }

        public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
        {
            return getItem(paramInt).create(GlobalActions.this.mContext, paramView, paramViewGroup, LayoutInflater.from(GlobalActions.this.mContext));
        }

        public boolean isEnabled(int paramInt)
        {
            return getItem(paramInt).isEnabled();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.GlobalActions
 * JD-Core Version:        0.6.2
 */