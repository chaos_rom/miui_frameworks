package com.android.internal.policy.impl;

import android.content.res.Configuration;

public class KeyguardScreenCallbackProxy
    implements KeyguardScreenCallback
{
    private KeyguardScreenCallback mClient;

    public KeyguardScreenCallbackProxy(KeyguardScreenCallback paramKeyguardScreenCallback)
    {
        this.mClient = paramKeyguardScreenCallback;
    }

    public boolean doesFallbackUnlockScreenExist()
    {
        return this.mClient.doesFallbackUnlockScreenExist();
    }

    public void forgotPattern(boolean paramBoolean)
    {
        this.mClient.forgotPattern(paramBoolean);
    }

    public void goToLockScreen()
    {
        this.mClient.goToLockScreen();
    }

    public void goToUnlockScreen()
    {
        this.mClient.goToUnlockScreen();
    }

    public boolean isSecure()
    {
        return this.mClient.isSecure();
    }

    public boolean isVerifyUnlockOnly()
    {
        return this.mClient.isVerifyUnlockOnly();
    }

    public void keyguardDone(boolean paramBoolean)
    {
        this.mClient.keyguardDone(paramBoolean);
    }

    public void keyguardDoneDrawing()
    {
        this.mClient.keyguardDoneDrawing();
    }

    public void pokeWakelock()
    {
        this.mClient.pokeWakelock();
    }

    public void pokeWakelock(int paramInt)
    {
        this.mClient.pokeWakelock(paramInt);
    }

    public void recreateMe(Configuration paramConfiguration)
    {
        this.mClient.recreateMe(paramConfiguration);
    }

    public void reportFailedUnlockAttempt()
    {
        this.mClient.reportFailedUnlockAttempt();
    }

    public void reportSuccessfulUnlockAttempt()
    {
        this.mClient.reportSuccessfulUnlockAttempt();
    }

    public void takeEmergencyCallAction()
    {
        this.mClient.takeEmergencyCallAction();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.KeyguardScreenCallbackProxy
 * JD-Core Version:        0.6.2
 */