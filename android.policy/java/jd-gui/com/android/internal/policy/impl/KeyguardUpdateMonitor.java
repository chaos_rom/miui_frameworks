package com.android.internal.policy.impl;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.MiuiThemeHelper;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import com.android.internal.telephony.IccCard.State;
import com.google.android.collect.Lists;
import java.util.ArrayList;

public class KeyguardUpdateMonitor
{
    private static final boolean DEBUG = false;
    protected static final boolean DEBUG_SIM_STATES = false;
    private static final int FAILED_BIOMETRIC_UNLOCK_ATTEMPTS_BEFORE_BACKUP = 3;
    static final int LOW_BATTERY_THRESHOLD = 20;
    private static final int MSG_BATTERY_UPDATE = 302;
    private static final int MSG_CARRIER_INFO_UPDATE = 303;
    private static final int MSG_CLOCK_VISIBILITY_CHANGED = 307;
    private static final int MSG_DEVICE_PROVISIONED = 308;
    protected static final int MSG_DPM_STATE_CHANGED = 309;
    private static final int MSG_PHONE_STATE_CHANGED = 306;
    private static final int MSG_RINGER_MODE_CHANGED = 305;
    private static final int MSG_SIM_STATE_CHANGE = 304;
    private static final int MSG_TIME_UPDATE = 301;
    protected static final int MSG_USER_CHANGED = 310;
    private static final String TAG = "KeyguardUpdateMonitor";
    private BatteryStatus mBatteryStatus;
    private boolean mClockVisible;
    private ContentObserver mContentObserver;
    private final Context mContext;
    private boolean mDeviceProvisioned;
    private int mFailedAttempts = 0;
    private int mFailedBiometricUnlockAttempts = 0;
    private Handler mHandler;
    private ArrayList<InfoCallback> mInfoCallbacks = Lists.newArrayList();
    private int mPhoneState;
    private int mRingMode;
    private IccCard.State mSimState = IccCard.State.READY;
    private ArrayList<SimStateCallback> mSimStateCallbacks = Lists.newArrayList();
    private CharSequence mTelephonyPlmn;
    private CharSequence mTelephonySpn;

    public KeyguardUpdateMonitor(Context paramContext)
    {
        this.mContext = paramContext;
        this.mHandler = new Handler()
        {
            public void handleMessage(Message paramAnonymousMessage)
            {
                switch (paramAnonymousMessage.what)
                {
                default:
                case 301:
                case 302:
                case 303:
                case 304:
                case 305:
                case 306:
                case 307:
                case 308:
                case 309:
                case 310:
                }
                while (true)
                {
                    return;
                    KeyguardUpdateMonitor.this.handleTimeUpdate();
                    continue;
                    KeyguardUpdateMonitor.this.handleBatteryUpdate((KeyguardUpdateMonitor.BatteryStatus)paramAnonymousMessage.obj);
                    continue;
                    KeyguardUpdateMonitor.this.handleCarrierInfoUpdate();
                    continue;
                    KeyguardUpdateMonitor.this.handleSimStateChange((KeyguardUpdateMonitor.SimArgs)paramAnonymousMessage.obj);
                    continue;
                    KeyguardUpdateMonitor.this.handleRingerModeChange(paramAnonymousMessage.arg1);
                    continue;
                    KeyguardUpdateMonitor.this.handlePhoneStateChanged((String)paramAnonymousMessage.obj);
                    continue;
                    KeyguardUpdateMonitor.this.handleClockVisibilityChanged();
                    continue;
                    KeyguardUpdateMonitor.this.handleDeviceProvisioned();
                    continue;
                    KeyguardUpdateMonitor.this.handleDevicePolicyManagerStateChanged();
                    continue;
                    KeyguardUpdateMonitor.this.handleUserChanged(paramAnonymousMessage.arg1);
                }
            }
        };
        boolean bool1;
        if (Settings.Secure.getInt(this.mContext.getContentResolver(), "device_provisioned", 0) != 0)
        {
            bool1 = true;
            this.mDeviceProvisioned = bool1;
            if (!this.mDeviceProvisioned)
            {
                this.mContentObserver = new ContentObserver(this.mHandler)
                {
                    public void onChange(boolean paramAnonymousBoolean)
                    {
                        boolean bool = false;
                        super.onChange(paramAnonymousBoolean);
                        KeyguardUpdateMonitor localKeyguardUpdateMonitor = KeyguardUpdateMonitor.this;
                        if (Settings.Secure.getInt(KeyguardUpdateMonitor.this.mContext.getContentResolver(), "device_provisioned", 0) != 0)
                            bool = true;
                        KeyguardUpdateMonitor.access$502(localKeyguardUpdateMonitor, bool);
                        if (KeyguardUpdateMonitor.this.mDeviceProvisioned)
                            KeyguardUpdateMonitor.this.mHandler.sendMessage(KeyguardUpdateMonitor.this.mHandler.obtainMessage(308));
                    }
                };
                this.mContext.getContentResolver().registerContentObserver(Settings.Secure.getUriFor("device_provisioned"), false, this.mContentObserver);
                if (Settings.Secure.getInt(this.mContext.getContentResolver(), "device_provisioned", 0) == 0)
                    break label302;
            }
        }
        label302: for (boolean bool2 = true; ; bool2 = false)
        {
            if (bool2 != this.mDeviceProvisioned)
            {
                this.mDeviceProvisioned = bool2;
                if (this.mDeviceProvisioned)
                    this.mHandler.sendMessage(this.mHandler.obtainMessage(308));
            }
            this.mSimState = IccCard.State.READY;
            this.mBatteryStatus = new BatteryStatus(1, 100, 0, 0);
            this.mTelephonyPlmn = getDefaultPlmn();
            IntentFilter localIntentFilter = new IntentFilter();
            localIntentFilter.addAction("android.intent.action.TIME_TICK");
            localIntentFilter.addAction("android.intent.action.TIME_SET");
            localIntentFilter.addAction("android.intent.action.BATTERY_CHANGED");
            localIntentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
            localIntentFilter.addAction("android.intent.action.SIM_STATE_CHANGED");
            localIntentFilter.addAction("android.intent.action.PHONE_STATE");
            localIntentFilter.addAction("android.provider.Telephony.SPN_STRINGS_UPDATED");
            localIntentFilter.addAction("android.media.RINGER_MODE_CHANGED");
            localIntentFilter.addAction("android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED");
            localIntentFilter.addAction("android.intent.action.USER_SWITCHED");
            localIntentFilter.addAction("android.intent.action.USER_REMOVED");
            paramContext.registerReceiver(new BroadcastReceiver()
            {
                public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
                {
                    String str1 = paramAnonymousIntent.getAction();
                    if (("android.intent.action.TIME_TICK".equals(str1)) || ("android.intent.action.TIME_SET".equals(str1)) || ("android.intent.action.TIMEZONE_CHANGED".equals(str1)))
                        KeyguardUpdateMonitor.this.mHandler.sendMessage(KeyguardUpdateMonitor.this.mHandler.obtainMessage(301));
                    while (true)
                    {
                        return;
                        if ("android.provider.Telephony.SPN_STRINGS_UPDATED".equals(str1))
                        {
                            KeyguardUpdateMonitor.access$802(KeyguardUpdateMonitor.this, KeyguardUpdateMonitor.this.getTelephonyPlmnFrom(paramAnonymousIntent));
                            KeyguardUpdateMonitor.access$1002(KeyguardUpdateMonitor.this, KeyguardUpdateMonitor.this.getTelephonySpnFrom(paramAnonymousIntent));
                            KeyguardUpdateMonitor.this.mHandler.sendMessage(KeyguardUpdateMonitor.this.mHandler.obtainMessage(303));
                        }
                        else if ("android.intent.action.BATTERY_CHANGED".equals(str1))
                        {
                            int i = paramAnonymousIntent.getIntExtra("status", 1);
                            int j = paramAnonymousIntent.getIntExtra("plugged", 0);
                            int k = paramAnonymousIntent.getIntExtra("level", 0);
                            int m = paramAnonymousIntent.getIntExtra("health", 1);
                            Message localMessage = KeyguardUpdateMonitor.this.mHandler.obtainMessage(302, new KeyguardUpdateMonitor.BatteryStatus(i, k, j, m));
                            KeyguardUpdateMonitor.this.mHandler.sendMessage(localMessage);
                        }
                        else if ("android.intent.action.SIM_STATE_CHANGED".equals(str1))
                        {
                            KeyguardUpdateMonitor.this.mHandler.sendMessage(KeyguardUpdateMonitor.this.mHandler.obtainMessage(304, KeyguardUpdateMonitor.SimArgs.fromIntent(paramAnonymousIntent)));
                        }
                        else if ("android.media.RINGER_MODE_CHANGED".equals(str1))
                        {
                            KeyguardUpdateMonitor.this.mHandler.sendMessage(KeyguardUpdateMonitor.this.mHandler.obtainMessage(305, paramAnonymousIntent.getIntExtra("android.media.EXTRA_RINGER_MODE", -1), 0));
                        }
                        else if ("android.intent.action.PHONE_STATE".equals(str1))
                        {
                            String str2 = paramAnonymousIntent.getStringExtra("state");
                            KeyguardUpdateMonitor.this.mHandler.sendMessage(KeyguardUpdateMonitor.this.mHandler.obtainMessage(306, str2));
                        }
                        else if ("android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED".equals(str1))
                        {
                            KeyguardUpdateMonitor.this.mHandler.sendMessage(KeyguardUpdateMonitor.this.mHandler.obtainMessage(309));
                        }
                        else if ("android.intent.action.USER_SWITCHED".equals(str1))
                        {
                            KeyguardUpdateMonitor.this.mHandler.sendMessage(KeyguardUpdateMonitor.this.mHandler.obtainMessage(310, paramAnonymousIntent.getIntExtra("android.intent.extra.user_id", 0), 0));
                        }
                    }
                }
            }
            , localIntentFilter);
            return;
            bool1 = false;
            break;
        }
    }

    private CharSequence getDefaultPlmn()
    {
        return this.mContext.getResources().getText(17040118);
    }

    private CharSequence getTelephonyPlmnFrom(Intent paramIntent)
    {
        Object localObject;
        if (paramIntent.getBooleanExtra("showPlmn", false))
        {
            localObject = paramIntent.getStringExtra("plmn");
            if (localObject == null);
        }
        while (true)
        {
            return localObject;
            localObject = getDefaultPlmn();
            continue;
            localObject = null;
        }
    }

    private CharSequence getTelephonySpnFrom(Intent paramIntent)
    {
        String str;
        if (paramIntent.getBooleanExtra("showSpn", false))
        {
            str = paramIntent.getStringExtra("spn");
            if (str == null);
        }
        while (true)
        {
            return str;
            str = null;
        }
    }

    private void handleBatteryUpdate(BatteryStatus paramBatteryStatus)
    {
        boolean bool = isBatteryUpdateInteresting(this.mBatteryStatus, paramBatteryStatus);
        this.mBatteryStatus = paramBatteryStatus;
        if (bool)
            for (int i = 0; i < this.mInfoCallbacks.size(); i++)
                ((InfoCallback)this.mInfoCallbacks.get(i)).onRefreshBatteryInfo(shouldShowBatteryInfo(), isPluggedIn(paramBatteryStatus), paramBatteryStatus.level);
    }

    private void handleCarrierInfoUpdate()
    {
        for (int i = 0; i < this.mInfoCallbacks.size(); i++)
            ((InfoCallback)this.mInfoCallbacks.get(i)).onRefreshCarrierInfo(this.mTelephonyPlmn, this.mTelephonySpn);
    }

    private void handleClockVisibilityChanged()
    {
        for (int i = 0; i < this.mInfoCallbacks.size(); i++)
            ((InfoCallback)this.mInfoCallbacks.get(i)).onClockVisibilityChanged();
    }

    private void handleSimStateChange(SimArgs paramSimArgs)
    {
        IccCard.State localState = paramSimArgs.simState;
        if ((localState != IccCard.State.UNKNOWN) && (localState != this.mSimState))
        {
            this.mSimState = localState;
            for (int i = 0; i < this.mSimStateCallbacks.size(); i++)
                ((SimStateCallback)this.mSimStateCallbacks.get(i)).onSimStateChanged(localState);
        }
    }

    private void handleTimeUpdate()
    {
        for (int i = 0; i < this.mInfoCallbacks.size(); i++)
            ((InfoCallback)this.mInfoCallbacks.get(i)).onTimeChanged();
    }

    private static boolean isBatteryLow(BatteryStatus paramBatteryStatus)
    {
        if (paramBatteryStatus.level < 20);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static boolean isBatteryUpdateInteresting(BatteryStatus paramBatteryStatus1, BatteryStatus paramBatteryStatus2)
    {
        boolean bool1 = true;
        boolean bool2 = isPluggedIn(paramBatteryStatus2);
        boolean bool3 = isPluggedIn(paramBatteryStatus1);
        boolean bool4;
        if ((bool3 == bool1) && (bool2 == bool1) && (paramBatteryStatus1.status != paramBatteryStatus2.status))
        {
            bool4 = bool1;
            if ((bool3 == bool2) && (!bool4))
                break label57;
        }
        while (true)
        {
            return bool1;
            bool4 = false;
            break;
            label57: if (((!bool2) || (paramBatteryStatus1.level == paramBatteryStatus2.level)) && ((bool2) || (!isBatteryLow(paramBatteryStatus2)) || (paramBatteryStatus2.level == paramBatteryStatus1.level)))
                bool1 = false;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private static boolean isPluggedIn(BatteryStatus paramBatteryStatus)
    {
        int i = 1;
        if (((paramBatteryStatus.plugged == i) || (paramBatteryStatus.plugged == 2)) && (!MiuiThemeHelper.isScreenshotMode()));
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public void clearFailedAttempts()
    {
        this.mFailedAttempts = 0;
        this.mFailedBiometricUnlockAttempts = 0;
    }

    public int getBatteryLevel()
    {
        return this.mBatteryStatus.level;
    }

    public int getFailedAttempts()
    {
        return this.mFailedAttempts;
    }

    public boolean getMaxBiometricUnlockAttemptsReached()
    {
        if (this.mFailedBiometricUnlockAttempts >= 3);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public int getPhoneState()
    {
        return this.mPhoneState;
    }

    public IccCard.State getSimState()
    {
        return this.mSimState;
    }

    public CharSequence getTelephonyPlmn()
    {
        return this.mTelephonyPlmn;
    }

    public CharSequence getTelephonySpn()
    {
        return this.mTelephonySpn;
    }

    protected void handleDevicePolicyManagerStateChanged()
    {
        for (int i = 0; i < this.mInfoCallbacks.size(); i++)
            ((InfoCallback)this.mInfoCallbacks.get(i)).onDevicePolicyManagerStateChanged();
    }

    protected void handleDeviceProvisioned()
    {
        for (int i = 0; i < this.mInfoCallbacks.size(); i++)
            ((InfoCallback)this.mInfoCallbacks.get(i)).onDeviceProvisioned();
        if (this.mContentObserver != null)
        {
            this.mContext.getContentResolver().unregisterContentObserver(this.mContentObserver);
            this.mContentObserver = null;
        }
    }

    protected void handlePhoneStateChanged(String paramString)
    {
        if (TelephonyManager.EXTRA_STATE_IDLE.equals(paramString))
            this.mPhoneState = 0;
        while (true)
        {
            for (int i = 0; i < this.mInfoCallbacks.size(); i++)
                ((InfoCallback)this.mInfoCallbacks.get(i)).onPhoneStateChanged(this.mPhoneState);
            if (TelephonyManager.EXTRA_STATE_OFFHOOK.equals(paramString))
                this.mPhoneState = 2;
            else if (TelephonyManager.EXTRA_STATE_RINGING.equals(paramString))
                this.mPhoneState = 1;
        }
    }

    protected void handleRingerModeChange(int paramInt)
    {
        this.mRingMode = paramInt;
        for (int i = 0; i < this.mInfoCallbacks.size(); i++)
            ((InfoCallback)this.mInfoCallbacks.get(i)).onRingerModeChanged(paramInt);
    }

    protected void handleUserChanged(int paramInt)
    {
        for (int i = 0; i < this.mInfoCallbacks.size(); i++)
            ((InfoCallback)this.mInfoCallbacks.get(i)).onUserChanged(paramInt);
    }

    public boolean isClockVisible()
    {
        return this.mClockVisible;
    }

    public boolean isDeviceCharged()
    {
        if ((this.mBatteryStatus.status == 5) || (this.mBatteryStatus.level >= 100));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isDevicePluggedIn()
    {
        return isPluggedIn(this.mBatteryStatus);
    }

    public boolean isDeviceProvisioned()
    {
        return this.mDeviceProvisioned;
    }

    public boolean isSimLocked()
    {
        if ((this.mSimState == IccCard.State.PIN_REQUIRED) || (this.mSimState == IccCard.State.PUK_REQUIRED) || (this.mSimState == IccCard.State.PERM_DISABLED));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void registerInfoCallback(InfoCallback paramInfoCallback)
    {
        if (!this.mInfoCallbacks.contains(paramInfoCallback))
        {
            this.mInfoCallbacks.add(paramInfoCallback);
            paramInfoCallback.onRefreshBatteryInfo(shouldShowBatteryInfo(), isPluggedIn(this.mBatteryStatus), this.mBatteryStatus.level);
            paramInfoCallback.onTimeChanged();
            paramInfoCallback.onRingerModeChanged(this.mRingMode);
            paramInfoCallback.onPhoneStateChanged(this.mPhoneState);
            paramInfoCallback.onRefreshCarrierInfo(this.mTelephonyPlmn, this.mTelephonySpn);
            paramInfoCallback.onClockVisibilityChanged();
        }
    }

    public void registerSimStateCallback(SimStateCallback paramSimStateCallback)
    {
        if (!this.mSimStateCallbacks.contains(paramSimStateCallback))
        {
            this.mSimStateCallbacks.add(paramSimStateCallback);
            paramSimStateCallback.onSimStateChanged(this.mSimState);
        }
    }

    public void removeCallback(Object paramObject)
    {
        this.mInfoCallbacks.remove(paramObject);
        this.mSimStateCallbacks.remove(paramObject);
    }

    public void reportClockVisible(boolean paramBoolean)
    {
        this.mClockVisible = paramBoolean;
        this.mHandler.obtainMessage(307).sendToTarget();
    }

    public void reportFailedAttempt()
    {
        this.mFailedAttempts = (1 + this.mFailedAttempts);
    }

    public void reportFailedBiometricUnlockAttempt()
    {
        this.mFailedBiometricUnlockAttempts = (1 + this.mFailedBiometricUnlockAttempts);
    }

    public void reportSimUnlocked()
    {
        handleSimStateChange(new SimArgs(IccCard.State.READY));
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public boolean shouldShowBatteryInfo()
    {
        if (((isPluggedIn(this.mBatteryStatus)) || (isBatteryLow(this.mBatteryStatus))) && (!MiuiThemeHelper.isScreenshotMode()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    static abstract interface SimStateCallback
    {
        public abstract void onSimStateChanged(IccCard.State paramState);
    }

    public static class InfoCallbackImpl
        implements KeyguardUpdateMonitor.InfoCallback
    {
        public void onClockVisibilityChanged()
        {
        }

        public void onDevicePolicyManagerStateChanged()
        {
        }

        public void onDeviceProvisioned()
        {
        }

        public void onPhoneStateChanged(int paramInt)
        {
        }

        public void onRefreshBatteryInfo(boolean paramBoolean1, boolean paramBoolean2, int paramInt)
        {
        }

        public void onRefreshCarrierInfo(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
        {
        }

        public void onRingerModeChanged(int paramInt)
        {
        }

        public void onTimeChanged()
        {
        }

        public void onUserChanged(int paramInt)
        {
        }
    }

    static abstract interface InfoCallback
    {
        public abstract void onClockVisibilityChanged();

        public abstract void onDevicePolicyManagerStateChanged();

        public abstract void onDeviceProvisioned();

        public abstract void onPhoneStateChanged(int paramInt);

        public abstract void onRefreshBatteryInfo(boolean paramBoolean1, boolean paramBoolean2, int paramInt);

        public abstract void onRefreshCarrierInfo(CharSequence paramCharSequence1, CharSequence paramCharSequence2);

        public abstract void onRingerModeChanged(int paramInt);

        public abstract void onTimeChanged();

        public abstract void onUserChanged(int paramInt);
    }

    private static class BatteryStatus
    {
        public final int health;
        public final int level;
        public final int plugged;
        public final int status;

        public BatteryStatus(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        {
            this.status = paramInt1;
            this.level = paramInt2;
            this.plugged = paramInt3;
            this.health = paramInt4;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private static class SimArgs
    {
        public final IccCard.State simState;

        SimArgs(IccCard.State paramState)
        {
            this.simState = paramState;
        }

        static SimArgs fromIntent(Intent paramIntent)
        {
            if (!"android.intent.action.SIM_STATE_CHANGED".equals(paramIntent.getAction()))
                throw new IllegalArgumentException("only handles intent ACTION_SIM_STATE_CHANGED");
            String str1 = paramIntent.getStringExtra("ss");
            IccCard.State localState;
            if ("ABSENT".equals(str1))
                if ("PERM_DISABLED".equals(paramIntent.getStringExtra("reason")))
                    localState = IccCard.State.PERM_DISABLED;
            while (true)
            {
                return new SimArgs(localState);
                localState = IccCard.State.ABSENT;
                continue;
                if ("READY".equals(str1))
                {
                    localState = IccCard.State.READY;
                }
                else if ("LOCKED".equals(str1))
                {
                    String str2 = paramIntent.getStringExtra("reason");
                    if ("PIN".equals(str2))
                        localState = IccCard.State.PIN_REQUIRED;
                    else if ("PUK".equals(str2))
                        localState = IccCard.State.PUK_REQUIRED;
                    else
                        localState = IccCard.State.UNKNOWN;
                }
                else if ("NETWORK".equals(str1))
                {
                    localState = IccCard.State.NETWORK_LOCKED;
                }
                else
                {
                    localState = KeyguardUpdateMonitor.Injector.getIccCardState(str1);
                }
            }
        }

        public String toString()
        {
            return this.simState.toString();
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static IccCard.State getIccCardState(String paramString)
        {
            IccCard.State localState;
            if ("IMSI".equals(paramString))
                localState = IccCard.State.READY;
            while (true)
            {
                return localState;
                if ("LOADED".equals(paramString))
                    localState = IccCard.State.READY;
                else
                    localState = IccCard.State.UNKNOWN;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.KeyguardUpdateMonitor
 * JD-Core Version:        0.6.2
 */