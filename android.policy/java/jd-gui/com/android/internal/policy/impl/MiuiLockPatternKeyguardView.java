package com.android.internal.policy.impl;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.android.internal.widget.LockPatternUtils;
import miui.content.res.ThemeResources;
import miui.content.res.ThemeResourcesSystem;
import miui.provider.ExtraSettings.System;

public class MiuiLockPatternKeyguardView extends LockPatternKeyguardView
{
    private boolean mBackDown;
    private com.miui.internal.policy.impl.KeyguardScreenCallback mKeyguardScreenCallback;
    private ImageView mTorchCover;
    private BroadcastReceiver mTorchStateReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            final boolean bool = paramAnonymousIntent.getBooleanExtra("miui.intent.extra.IS_ENABLE", false);
            MiuiLockPatternKeyguardView.this.post(new Runnable()
            {
                public void run()
                {
                    MiuiLockPatternKeyguardView.this.updateTorchCover(bool);
                }
            });
        }
    };

    public MiuiLockPatternKeyguardView(Context paramContext, KeyguardViewCallback paramKeyguardViewCallback, KeyguardUpdateMonitor paramKeyguardUpdateMonitor, LockPatternUtils paramLockPatternUtils, KeyguardWindowController paramKeyguardWindowController)
    {
        super(paramContext, paramKeyguardViewCallback, paramKeyguardUpdateMonitor, paramLockPatternUtils, paramKeyguardWindowController);
    }

    private void updateShowLockBeforeUnlock()
    {
        setShowLockBeforeUnlock(ExtraSettings.System.getBoolean(this.mContext.getContentResolver(), "show_lock_before_unlock", true));
    }

    private void updateTorchCover(boolean paramBoolean)
    {
        if (this.mTorchCover == null)
        {
            this.mTorchCover = new ImageView(this.mContext);
            this.mTorchCover.setClickable(true);
            this.mTorchCover.setImageResource(100794558);
            this.mTorchCover.setScaleType(ImageView.ScaleType.CENTER_CROP);
            this.mTorchCover.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        }
        if (paramBoolean)
            addView(this.mTorchCover);
        while (true)
        {
            return;
            removeView(this.mTorchCover);
        }
    }

    protected com.miui.internal.policy.impl.KeyguardScreenCallback createKeyguardScreenCallback()
    {
        this.mKeyguardScreenCallback = new KeyguardScreenCallbackImpl(super.createKeyguardScreenCallback());
        return this.mKeyguardScreenCallback;
    }

    protected View createLockScreen()
    {
        Object localObject;
        if (ThemeResources.getSystem().hasAwesomeLockscreen())
            localObject = new AwesomeLockScreen(this.mContext, getConfiguration(), getLockPatternUtils(), getUpdateMonitor(), this.mKeyguardScreenCallback);
        while (true)
        {
            return localObject;
            localObject = new MiuiLockScreen(this.mContext, getConfiguration(), getLockPatternUtils(), getUpdateMonitor(), this.mKeyguardScreenCallback);
            ((View)localObject).setBackground(ThemeResources.getLockWallpaperCache(this.mContext));
        }
    }

    View createUnlockScreenFor(LockPatternKeyguardView.UnlockMode paramUnlockMode)
    {
        View localView = super.createUnlockScreenFor(paramUnlockMode);
        localView.setPadding(0, this.mContext.getResources().getDimensionPixelSize(101318656), 0, 0);
        localView.setBackground(ThemeResources.getLockWallpaperCache(this.mContext));
        return localView;
    }

    public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
    {
        int i = 1;
        int j = paramKeyEvent.getKeyCode();
        if (paramKeyEvent.getAction() == 0)
            if (j == 4)
                this.mBackDown = i;
        while (true)
        {
            i = super.dispatchKeyEvent(paramKeyEvent);
            while (true)
            {
                return i;
                if (j == 24)
                    break;
                this.mBackDown = false;
                break;
                if (paramKeyEvent.getAction() != i)
                    break;
                if ((j != 24) || (!this.mBackDown))
                    break label92;
                this.mBackDown = false;
                this.mKeyguardScreenCallback.goToUnlockScreen();
                Log.d("MiuiLockPatternKeyguardView", "Unlock Screen by pressing back + volume_up");
            }
            label92: this.mBackDown = false;
        }
    }

    public boolean isDisplayDesktop()
    {
        if ((getLockScreen() instanceof AwesomeLockScreen));
        for (boolean bool = ((AwesomeLockScreen)getLockScreen()).isDisplayDesktop(); ; bool = false)
            return bool;
    }

    protected void onAttachedToWindow()
    {
        IntentFilter localIntentFilter = new IntentFilter("miui.intent.action.TOGGLE_TORCH");
        localIntentFilter.setPriority(1000);
        this.mContext.registerReceiver(this.mTorchStateReceiver, localIntentFilter);
        super.onAttachedToWindow();
    }

    protected void onConfigurationChanged(Configuration paramConfiguration)
    {
        super.onConfigurationChanged(paramConfiguration);
        updateShowLockBeforeUnlock();
    }

    protected void onDetachedFromWindow()
    {
        this.mContext.unregisterReceiver(this.mTorchStateReceiver);
        super.onDetachedFromWindow();
    }

    public void onScreenTurnedOff()
    {
        super.onScreenTurnedOff();
        setMode(callGetInitialMode());
    }

    public void show()
    {
        setScreenOn(true);
        updateTorchCover(false);
        super.show();
    }

    class KeyguardScreenCallbackImpl extends KeyguardScreenCallbackProxy
        implements com.miui.internal.policy.impl.KeyguardScreenCallback
    {
        private Intent mPendingIntent = null;

        public KeyguardScreenCallbackImpl(KeyguardScreenCallback arg2)
        {
            super();
        }

        public void goToUnlockScreen()
        {
            if (MiuiLockPatternKeyguardView.this.callStuckOnLockScreenBecauseSimMissing());
            while (true)
            {
                return;
                if (!isSecure())
                    keyguardDone(true);
                else
                    super.goToUnlockScreen();
            }
        }

        public void keyguardDone(boolean paramBoolean)
        {
            super.keyguardDone(paramBoolean);
            if ((paramBoolean) && (this.mPendingIntent != null));
            try
            {
                MiuiLockPatternKeyguardView.access$000(MiuiLockPatternKeyguardView.this).startActivity(this.mPendingIntent);
                label30: return;
            }
            catch (ActivityNotFoundException localActivityNotFoundException)
            {
                break label30;
            }
        }

        public void setPendingIntent(Intent paramIntent)
        {
            this.mPendingIntent = paramIntent;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.MiuiLockPatternKeyguardView
 * JD-Core Version:        0.6.2
 */