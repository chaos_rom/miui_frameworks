package com.android.internal.policy.impl;

public abstract interface KeyguardScreen
{
    public abstract void cleanUp();

    public abstract boolean needsInput();

    public abstract void onPause();

    public abstract void onResume();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.KeyguardScreen
 * JD-Core Version:        0.6.2
 */