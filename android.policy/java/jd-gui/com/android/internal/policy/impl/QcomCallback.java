package com.android.internal.policy.impl;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import com.android.internal.telephony.IccCard.State;

@MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
abstract interface QcomCallback
{
    public abstract void onRefreshCarrierInfo(CharSequence paramCharSequence1, CharSequence paramCharSequence2, int paramInt);

    public abstract void onSimStateChanged(IccCard.State paramState, int paramInt);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.QcomCallback
 * JD-Core Version:        0.6.2
 */