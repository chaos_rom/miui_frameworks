package com.android.internal.policy.impl;

public abstract interface KeyguardWindowController
{
    public abstract void setNeedsInput(boolean paramBoolean);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.KeyguardWindowController
 * JD-Core Version:        0.6.2
 */