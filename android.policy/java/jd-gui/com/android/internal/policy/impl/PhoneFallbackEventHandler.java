package com.android.internal.policy.impl;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.KeyguardManager;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.IAudioService;
import android.media.IAudioService.Stub;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.Slog;
import android.view.FallbackEventHandler;
import android.view.KeyEvent;
import android.view.KeyEvent.DispatcherState;
import android.view.View;

public class PhoneFallbackEventHandler
    implements FallbackEventHandler
{
    private static final boolean DEBUG;
    private static String TAG = "PhoneFallbackEventHandler";
    AudioManager mAudioManager;
    Context mContext;
    KeyguardManager mKeyguardManager;
    SearchManager mSearchManager;
    TelephonyManager mTelephonyManager;
    View mView;

    public PhoneFallbackEventHandler(Context paramContext)
    {
        this.mContext = paramContext;
    }

    private void handleMediaKeyEvent(KeyEvent paramKeyEvent)
    {
        IAudioService localIAudioService = IAudioService.Stub.asInterface(ServiceManager.checkService("audio"));
        if (localIAudioService != null);
        while (true)
        {
            try
            {
                localIAudioService.dispatchMediaKeyEvent(paramKeyEvent);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e(TAG, "dispatchMediaKeyEvent threw exception " + localRemoteException);
                continue;
            }
            Slog.w(TAG, "Unable to find IAudioService for media key event.");
        }
    }

    public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
    {
        int i = paramKeyEvent.getAction();
        int j = paramKeyEvent.getKeyCode();
        if (i == 0);
        for (boolean bool = onKeyDown(j, paramKeyEvent); ; bool = onKeyUp(j, paramKeyEvent))
            return bool;
    }

    AudioManager getAudioManager()
    {
        if (this.mAudioManager == null)
            this.mAudioManager = ((AudioManager)this.mContext.getSystemService("audio"));
        return this.mAudioManager;
    }

    KeyguardManager getKeyguardManager()
    {
        if (this.mKeyguardManager == null)
            this.mKeyguardManager = ((KeyguardManager)this.mContext.getSystemService("keyguard"));
        return this.mKeyguardManager;
    }

    SearchManager getSearchManager()
    {
        if (this.mSearchManager == null)
            this.mSearchManager = ((SearchManager)this.mContext.getSystemService("search"));
        return this.mSearchManager;
    }

    TelephonyManager getTelephonyManager()
    {
        if (this.mTelephonyManager == null)
            this.mTelephonyManager = ((TelephonyManager)this.mContext.getSystemService("phone"));
        return this.mTelephonyManager;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void handleCameraKeyEvent()
    {
    }

    boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
        int i = 1;
        KeyEvent.DispatcherState localDispatcherState = this.mView.getKeyDispatcherState();
        switch (paramInt)
        {
        default:
        case 24:
        case 25:
        case 164:
        case 85:
        case 126:
        case 127:
        case 79:
        case 86:
        case 87:
        case 88:
        case 89:
        case 90:
        case 91:
        case 130:
        case 5:
        case 27:
        case 84:
        }
        while (true)
        {
            i = 0;
            while (true)
            {
                return i;
                getAudioManager().handleKeyDown(paramKeyEvent, -2147483648);
                continue;
                if (getTelephonyManager().getCallState() == 0)
                {
                    handleMediaKeyEvent(paramKeyEvent);
                    continue;
                    if ((getKeyguardManager().inKeyguardRestrictedInputMode()) || (localDispatcherState == null))
                        break;
                    if (paramKeyEvent.getRepeatCount() == 0)
                    {
                        localDispatcherState.startTracking(paramKeyEvent, this);
                    }
                    else if ((paramKeyEvent.isLongPress()) && (localDispatcherState.isTracking(paramKeyEvent)))
                    {
                        localDispatcherState.performedLongPress(paramKeyEvent);
                        this.mView.performHapticFeedback(0);
                        Intent localIntent3 = new Intent("android.intent.action.VOICE_COMMAND");
                        localIntent3.setFlags(268435456);
                        try
                        {
                            sendCloseSystemWindows();
                            this.mContext.startActivity(localIntent3);
                        }
                        catch (ActivityNotFoundException localActivityNotFoundException2)
                        {
                            startCallActivity();
                        }
                        continue;
                        if ((getKeyguardManager().inKeyguardRestrictedInputMode()) || (localDispatcherState == null))
                            break;
                        if (paramKeyEvent.getRepeatCount() == 0)
                        {
                            localDispatcherState.startTracking(paramKeyEvent, this);
                        }
                        else if ((paramKeyEvent.isLongPress()) && (localDispatcherState.isTracking(paramKeyEvent)))
                        {
                            localDispatcherState.performedLongPress(paramKeyEvent);
                            this.mView.performHapticFeedback(0);
                            sendCloseSystemWindows();
                            Intent localIntent2 = new Intent("android.intent.action.CAMERA_BUTTON", null);
                            localIntent2.putExtra("android.intent.extra.KEY_EVENT", paramKeyEvent);
                            this.mContext.sendOrderedBroadcast(localIntent2, null);
                            continue;
                            if ((getKeyguardManager().inKeyguardRestrictedInputMode()) || (localDispatcherState == null))
                                break;
                            if (paramKeyEvent.getRepeatCount() == 0)
                            {
                                localDispatcherState.startTracking(paramKeyEvent, this);
                                break;
                            }
                            if ((!paramKeyEvent.isLongPress()) || (!localDispatcherState.isTracking(paramKeyEvent)))
                                break;
                            Configuration localConfiguration = this.mContext.getResources().getConfiguration();
                            if ((localConfiguration.keyboard != i) && (localConfiguration.hardKeyboardHidden != 2))
                                break;
                            Intent localIntent1 = new Intent("android.intent.action.SEARCH_LONG_PRESS");
                            localIntent1.setFlags(268435456);
                            try
                            {
                                this.mView.performHapticFeedback(0);
                                sendCloseSystemWindows();
                                getSearchManager().stopSearch();
                                this.mContext.startActivity(localIntent1);
                                localDispatcherState.performedLongPress(paramKeyEvent);
                            }
                            catch (ActivityNotFoundException localActivityNotFoundException1)
                            {
                            }
                        }
                    }
                }
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
    {
        boolean bool = true;
        KeyEvent.DispatcherState localDispatcherState = this.mView.getKeyDispatcherState();
        if (localDispatcherState != null)
            localDispatcherState.handleUpEvent(paramKeyEvent);
        switch (paramInt)
        {
        default:
            bool = false;
        case 24:
        case 25:
        case 164:
        case 79:
        case 85:
        case 86:
        case 87:
        case 88:
        case 89:
        case 90:
        case 91:
        case 126:
        case 127:
        case 130:
        case 27:
        case 5:
        }
        while (true)
        {
            return bool;
            if ((!paramKeyEvent.isCanceled()) && ((AudioManager)this.mContext.getSystemService("audio") != null))
            {
                getAudioManager().handleKeyUp(paramKeyEvent, -2147483648);
                continue;
                handleMediaKeyEvent(paramKeyEvent);
                continue;
                if (getKeyguardManager().inKeyguardRestrictedInputMode())
                    break;
                if ((paramKeyEvent.isTracking()) && (!paramKeyEvent.isCanceled()))
                {
                    handleCameraKeyEvent();
                    continue;
                    if (getKeyguardManager().inKeyguardRestrictedInputMode())
                        break;
                    if ((paramKeyEvent.isTracking()) && (!paramKeyEvent.isCanceled()))
                        startCallActivity();
                }
            }
        }
    }

    public void preDispatchKeyEvent(KeyEvent paramKeyEvent)
    {
        getAudioManager().preDispatchKeyEvent(paramKeyEvent, -2147483648);
    }

    void sendCloseSystemWindows()
    {
        PhoneWindowManager.sendCloseSystemWindows(this.mContext, null);
    }

    public void setView(View paramView)
    {
        this.mView = paramView;
    }

    void startCallActivity()
    {
        sendCloseSystemWindows();
        Intent localIntent = new Intent("android.intent.action.CALL_BUTTON");
        localIntent.setFlags(268435456);
        try
        {
            this.mContext.startActivity(localIntent);
            return;
        }
        catch (ActivityNotFoundException localActivityNotFoundException)
        {
            while (true)
                Slog.w(TAG, "No activity found for android.intent.action.CALL_BUTTON.");
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.PhoneFallbackEventHandler
 * JD-Core Version:        0.6.2
 */