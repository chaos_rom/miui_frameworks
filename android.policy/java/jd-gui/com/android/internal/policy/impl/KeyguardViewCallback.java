package com.android.internal.policy.impl;

public abstract interface KeyguardViewCallback
{
    public abstract void keyguardDone(boolean paramBoolean);

    public abstract void keyguardDoneDrawing();

    public abstract void pokeWakelock();

    public abstract void pokeWakelock(int paramInt);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.KeyguardViewCallback
 * JD-Core Version:        0.6.2
 */