package com.android.internal.policy.impl;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings.System;
import android.view.KeyEvent;
import android.view.View;

public class MiuiPhoneFallbackEventHandler extends PhoneFallbackEventHandler
{
    public MiuiPhoneFallbackEventHandler(Context paramContext)
    {
        super(paramContext);
    }

    protected void handleCameraKeyEvent()
    {
        int i = 0;
        if (1 == Settings.System.getInt(this.mContext.getContentResolver(), "camera_key_preferred_action_type", 0))
            i = Settings.System.getInt(this.mContext.getContentResolver(), "camera_key_preferred_action_shortcut_id", -1);
        int j = 0;
        switch (i)
        {
        default:
            if (j != 0)
            {
                KeyEvent localKeyEvent1 = new KeyEvent(0, j);
                if (!this.mView.dispatchKeyEvent(localKeyEvent1))
                    dispatchKeyEvent(localKeyEvent1);
                KeyEvent localKeyEvent2 = new KeyEvent(1, j);
                if (!this.mView.dispatchKeyEvent(localKeyEvent2))
                    dispatchKeyEvent(localKeyEvent2);
            }
            break;
        case 3:
        case 2:
        }
        while (true)
        {
            return;
            j = 5;
            break;
            j = 84;
            break;
            this.mContext.sendBroadcast(new Intent("com.miui.app.ExtraStatusBarManager.TRIGGER_CAMERA_KEY"));
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.MiuiPhoneFallbackEventHandler
 * JD-Core Version:        0.6.2
 */