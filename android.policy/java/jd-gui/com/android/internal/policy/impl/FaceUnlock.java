package com.android.internal.policy.impl;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import com.android.internal.policy.IFaceLockCallback;
import com.android.internal.policy.IFaceLockCallback.Stub;
import com.android.internal.policy.IFaceLockInterface;
import com.android.internal.policy.IFaceLockInterface.Stub;
import com.android.internal.widget.LockPatternUtils;

public class FaceUnlock
    implements BiometricSensorUnlock, Handler.Callback
{
    private static final boolean DEBUG = false;
    private static final String TAG = "FULLockscreen";
    private final int BACKUP_LOCK_TIMEOUT = 5000;
    private final int MSG_CANCEL = 5;
    private final int MSG_EXPOSE_FALLBACK = 7;
    private final int MSG_HIDE_FACE_UNLOCK_VIEW = 1;
    private final int MSG_POKE_WAKELOCK = 8;
    private final int MSG_REPORT_FAILED_ATTEMPT = 6;
    private final int MSG_SERVICE_CONNECTED = 2;
    private final int MSG_SERVICE_DISCONNECTED = 3;
    private final int MSG_SHOW_FACE_UNLOCK_VIEW = 0;
    private final int MSG_UNLOCK = 4;
    private final int SERVICE_STARTUP_VIEW_TIMEOUT = 3000;
    private boolean mBoundToService = false;
    private ServiceConnection mConnection = new ServiceConnection()
    {
        public void onServiceConnected(ComponentName paramAnonymousComponentName, IBinder paramAnonymousIBinder)
        {
            Log.d("FULLockscreen", "Connected to Face Unlock service");
            FaceUnlock.access$002(FaceUnlock.this, IFaceLockInterface.Stub.asInterface(paramAnonymousIBinder));
            FaceUnlock.this.mHandler.sendEmptyMessage(2);
        }

        public void onServiceDisconnected(ComponentName paramAnonymousComponentName)
        {
            Log.e("FULLockscreen", "Unexpected disconnect from Face Unlock service");
            FaceUnlock.this.mHandler.sendEmptyMessage(3);
        }
    };
    private final Context mContext;
    private final IFaceLockCallback mFaceUnlockCallback = new IFaceLockCallback.Stub()
    {
        public void cancel()
        {
            FaceUnlock.this.mHandler.sendEmptyMessage(5);
        }

        public void exposeFallback()
        {
            FaceUnlock.this.mHandler.sendEmptyMessage(7);
        }

        public void pokeWakelock(int paramAnonymousInt)
        {
            Message localMessage = FaceUnlock.this.mHandler.obtainMessage(8, paramAnonymousInt, -1);
            FaceUnlock.this.mHandler.sendMessage(localMessage);
        }

        public void reportFailedAttempt()
        {
            FaceUnlock.this.mHandler.sendEmptyMessage(6);
        }

        public void unlock()
        {
            FaceUnlock.this.mHandler.sendEmptyMessage(4);
        }
    };
    private View mFaceUnlockView;
    private Handler mHandler;
    private volatile boolean mIsRunning = false;
    KeyguardScreenCallback mKeyguardScreenCallback;
    private final LockPatternUtils mLockPatternUtils;
    private IFaceLockInterface mService;
    private boolean mServiceRunning = false;
    private final Object mServiceRunningLock = new Object();
    private final KeyguardUpdateMonitor mUpdateMonitor;

    public FaceUnlock(Context paramContext, KeyguardUpdateMonitor paramKeyguardUpdateMonitor, LockPatternUtils paramLockPatternUtils, KeyguardScreenCallback paramKeyguardScreenCallback)
    {
        this.mContext = paramContext;
        this.mUpdateMonitor = paramKeyguardUpdateMonitor;
        this.mLockPatternUtils = paramLockPatternUtils;
        this.mKeyguardScreenCallback = paramKeyguardScreenCallback;
        this.mHandler = new Handler(this);
    }

    private void removeDisplayMessages()
    {
        this.mHandler.removeMessages(0);
        this.mHandler.removeMessages(1);
    }

    private void startUi(IBinder paramIBinder, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        while (true)
        {
            synchronized (this.mServiceRunningLock)
            {
                if (!this.mServiceRunning)
                {
                    Log.d("FULLockscreen", "Starting Face Unlock");
                    try
                    {
                        this.mService.startUi(paramIBinder, paramInt1, paramInt2, paramInt3, paramInt4, this.mLockPatternUtils.isBiometricWeakLivelinessEnabled());
                        this.mServiceRunning = true;
                    }
                    catch (RemoteException localRemoteException)
                    {
                        Log.e("FULLockscreen", "Caught exception starting Face Unlock: " + localRemoteException.toString());
                    }
                }
            }
            Log.w("FULLockscreen", "startUi() attempted while running");
        }
    }

    private void stopUi()
    {
        synchronized (this.mServiceRunningLock)
        {
            if (this.mServiceRunning)
                Log.d("FULLockscreen", "Stopping Face Unlock");
            try
            {
                this.mService.stopUi();
                this.mServiceRunning = false;
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    Log.e("FULLockscreen", "Caught exception stopping Face Unlock: " + localRemoteException.toString());
            }
        }
    }

    public void cleanUp()
    {
        if (this.mService != null);
        try
        {
            this.mService.unregisterCallback(this.mFaceUnlockCallback);
            label20: stopUi();
            this.mService = null;
            return;
        }
        catch (RemoteException localRemoteException)
        {
            break label20;
        }
    }

    public int getQuality()
    {
        return 32768;
    }

    void handleCancel()
    {
        if (this.mFaceUnlockView != null)
            this.mFaceUnlockView.setVisibility(4);
        while (true)
        {
            stop();
            this.mKeyguardScreenCallback.pokeWakelock(5000);
            return;
            Log.e("FULLockscreen", "mFaceUnlockView is null in handleCancel()");
        }
    }

    void handleExposeFallback()
    {
        if (this.mFaceUnlockView != null)
            this.mFaceUnlockView.setVisibility(4);
        while (true)
        {
            return;
            Log.e("FULLockscreen", "mFaceUnlockView is null in handleExposeFallback()");
        }
    }

    void handleHideFaceUnlockView()
    {
        if (this.mFaceUnlockView != null)
            this.mFaceUnlockView.setVisibility(4);
        while (true)
        {
            return;
            Log.e("FULLockscreen", "mFaceUnlockView is null in handleHideFaceUnlockView()");
        }
    }

    public boolean handleMessage(Message paramMessage)
    {
        boolean bool;
        switch (paramMessage.what)
        {
        default:
            Log.e("FULLockscreen", "Unhandled message");
            bool = false;
            return bool;
        case 0:
            handleShowFaceUnlockView();
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        }
        while (true)
        {
            bool = true;
            break;
            handleHideFaceUnlockView();
            continue;
            handleServiceConnected();
            continue;
            handleServiceDisconnected();
            continue;
            handleUnlock();
            continue;
            handleCancel();
            continue;
            handleReportFailedAttempt();
            continue;
            handleExposeFallback();
            continue;
            handlePokeWakelock(paramMessage.arg1);
        }
    }

    void handlePokeWakelock(int paramInt)
    {
        this.mKeyguardScreenCallback.pokeWakelock(paramInt);
    }

    void handleReportFailedAttempt()
    {
        this.mUpdateMonitor.reportFailedBiometricUnlockAttempt();
    }

    void handleServiceConnected()
    {
        Log.d("FULLockscreen", "handleServiceConnected()");
        if (!this.mBoundToService)
            Log.d("FULLockscreen", "Dropping startUi() in handleServiceConnected() because no longer bound");
        while (true)
        {
            return;
            try
            {
                this.mService.registerCallback(this.mFaceUnlockCallback);
                if (this.mFaceUnlockView == null)
                    continue;
                IBinder localIBinder = this.mFaceUnlockView.getWindowToken();
                if (localIBinder == null)
                    break label160;
                this.mKeyguardScreenCallback.pokeWakelock();
                int[] arrayOfInt = new int[2];
                this.mFaceUnlockView.getLocationInWindow(arrayOfInt);
                startUi(localIBinder, arrayOfInt[0], arrayOfInt[1], this.mFaceUnlockView.getWidth(), this.mFaceUnlockView.getHeight());
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("FULLockscreen", "Caught exception connecting to Face Unlock: " + localRemoteException.toString());
                this.mService = null;
                this.mBoundToService = false;
                this.mIsRunning = false;
            }
            continue;
            label160: Log.e("FULLockscreen", "windowToken is null in handleServiceConnected()");
        }
    }

    void handleServiceDisconnected()
    {
        Log.e("FULLockscreen", "handleServiceDisconnected()");
        synchronized (this.mServiceRunningLock)
        {
            this.mService = null;
            this.mServiceRunning = false;
            this.mBoundToService = false;
            this.mIsRunning = false;
            return;
        }
    }

    void handleShowFaceUnlockView()
    {
        if (this.mFaceUnlockView != null)
            this.mFaceUnlockView.setVisibility(0);
        while (true)
        {
            return;
            Log.e("FULLockscreen", "mFaceUnlockView is null in handleShowFaceUnlockView()");
        }
    }

    void handleUnlock()
    {
        removeDisplayMessages();
        if (this.mFaceUnlockView != null)
            this.mFaceUnlockView.setVisibility(0);
        while (true)
        {
            stop();
            this.mKeyguardScreenCallback.keyguardDone(true);
            this.mKeyguardScreenCallback.reportSuccessfulUnlockAttempt();
            return;
            Log.e("FULLockscreen", "mFaceUnlockView is null in handleUnlock()");
        }
    }

    public void hide()
    {
        removeDisplayMessages();
        this.mHandler.sendEmptyMessage(1);
    }

    public void initializeView(View paramView)
    {
        Log.d("FULLockscreen", "initializeView()");
        this.mFaceUnlockView = paramView;
    }

    public boolean isRunning()
    {
        return this.mIsRunning;
    }

    public void show(long paramLong)
    {
        if (this.mHandler.getLooper() != Looper.myLooper())
            Log.e("FULLockscreen", "show() called off of the UI thread");
        removeDisplayMessages();
        if (this.mFaceUnlockView != null)
            this.mFaceUnlockView.setVisibility(0);
        if (paramLong > 0L)
            this.mHandler.sendEmptyMessageDelayed(1, paramLong);
    }

    public boolean start()
    {
        if (this.mHandler.getLooper() != Looper.myLooper())
            Log.e("FULLockscreen", "start() called off of the UI thread");
        if (this.mIsRunning)
            Log.w("FULLockscreen", "start() called when already running");
        show(3000L);
        if (!this.mBoundToService)
        {
            Log.d("FULLockscreen", "Binding to Face Unlock service");
            this.mContext.bindService(new Intent(IFaceLockInterface.class.getName()), this.mConnection, 1, this.mLockPatternUtils.getCurrentUser());
            this.mBoundToService = true;
        }
        while (true)
        {
            this.mIsRunning = true;
            return true;
            Log.w("FULLockscreen", "Attempt to bind to Face Unlock when already bound");
        }
    }

    public boolean stop()
    {
        if (this.mHandler.getLooper() != Looper.myLooper())
            Log.e("FULLockscreen", "stop() called off of the UI thread");
        boolean bool = this.mIsRunning;
        stopUi();
        if ((!this.mBoundToService) || (this.mService != null));
        try
        {
            this.mService.unregisterCallback(this.mFaceUnlockCallback);
            label58: Log.d("FULLockscreen", "Unbinding from Face Unlock service");
            this.mContext.unbindService(this.mConnection);
            this.mBoundToService = false;
            this.mIsRunning = false;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            break label58;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.FaceUnlock
 * JD-Core Version:        0.6.2
 */