package com.android.internal.policy.impl;

import android.app.INotificationManager;
import android.app.NotificationManager;
import android.content.AsyncQueryHandler;
import android.content.AsyncQueryHandler.WorkerHandler;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.database.sqlite.SQLiteDiskIOException;
import android.database.sqlite.SQLiteFullException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.media.AudioSystem;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.provider.CallLog.Calls;
import android.provider.Settings.System;
import android.provider.Telephony.Mms.Inbox;
import android.provider.Telephony.Sms.Inbox;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;
import com.android.internal.telephony.IccCard.State;
import com.android.internal.widget.LockPatternUtils;
import com.google.android.mms.pdu.EncodedStringValue;
import com.miui.internal.policy.impl.AlbumFrameView;
import com.miui.internal.policy.impl.KeyguardScreenCallback;
import com.miui.internal.policy.impl.SlidingPanel;
import com.miui.internal.policy.impl.SlidingPanel.OnTriggerListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.ref.SoftReference;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import miui.net.FirewallManager;
import miui.provider.ExtraTelephony.MmsSms;
import miui.security.ChooseLockSettingsHelper;
import miui.telephony.PhoneNumberUtils;
import miui.util.AudioOutputHelper;
import miui.util.HapticFeedbackUtil;
import miui.widget.SpectrumVisualizer;

public class MiuiLockScreen extends FrameLayout
    implements KeyguardScreen, KeyguardUpdateMonitor.InfoCallback, KeyguardUpdateMonitor.SimStateCallback, SlidingPanel.OnTriggerListener, QcomCallback
{
    private static final int BACKGROUND_MUSIC_SHOW_DEFAULT_ALBUM = 3;
    private static final int BACKGROUND_MUSIC_SHOW_HAS_ALBUM = 2;
    private static final int BACKGROUND_MUSIC_SHOW_NONE = 1;
    private static final int BACKGROUND_NONE = 0;
    private static final int CONTROL_VIEW_BATTERY = 5;
    private static final int CONTROL_VIEW_CALL = 1;
    private static final int CONTROL_VIEW_DATE = 3;
    private static final int CONTROL_VIEW_MUSIC = 4;
    private static final int CONTROL_VIEW_NONE = 0;
    private static final int CONTROL_VIEW_SMS = 2;
    private static final String[] CallLog_COLUMNS = arrayOfString;
    private static final boolean DBG = false;
    private static final String ENABLE_MENU_KEY_FILE = "/data/local/enable_menu_key";
    private static final long MAKR_PREVIEW_READ_DELAY = 2000L;
    private static final int MAX_VISIBLE_ITEM_NUM = 2;
    private static final int QUERY_TOKEN = 53;
    private static final int SMS_RECEIVED_WAKE_UP_DELAY = 25000;
    private static final String TAG = "LockScreen";
    private static Status sPrevStatus;
    private boolean isPaused = false;
    private PreviewCursorAdpater mAdapter;
    private ImageView mBackgroundMask;
    private View mBatteryInfo;
    private TextView mBatteryInfoText;
    private int mBatteryLevel = 100;
    private final KeyguardScreenCallback mCallback;
    private CallLogPreviewListAdapter mCallsAdapter;
    private View mCallsControlView;
    private QueryHandler mCallsHandler;
    private ListView mCallsPreviewList;
    private TextView mCarrier;
    private FrameLayout mControlView;
    private int mControlViewType;
    private int mCreationOrientation;
    private ListView mCurrentPreviewList;
    private View mEmergencyCall;
    private Button mEmergencyCallText;
    private boolean mEnableMenuKeyInLockScreen;
    private final Animation mFadeoutAnim;
    private HapticFeedbackUtil mHapticFeedbackUtil;
    private View mHintView;
    private int mKeyboardHidden;
    private long mLastGrabTime = 9223372036854775807L;
    private LockPatternUtils mLockPatternUtils;
    private MusicController mMusicController;
    private boolean mPluggedIn = false;
    private PowerManager mPowerManager;
    private TextView mScreenLocked;
    private SlidingPanel mSelector;
    private boolean mShowingBatteryInfo = false;
    private SmsPreviewListAdapter mSmsAdapter;
    private View mSmsControlView;
    private QueryHandler mSmsHandler;
    private ListView mSmsPreviewList;
    private Status mStatus = Status.Normal;
    private View mTimeView;
    private View mUnlockIccCard;
    private KeyguardUpdateMonitor mUpdateMonitor;
    private final Runnable sLongPressVibration = new Runnable()
    {
        public void run()
        {
            MiuiLockScreen.this.mHapticFeedbackUtil.performHapticFeedback(0, true);
        }
    };

    static
    {
        String[] arrayOfString = new String[5];
        arrayOfString[0] = "_id";
        arrayOfString[1] = "number";
        arrayOfString[2] = "date";
        arrayOfString[3] = "duration";
        arrayOfString[4] = "name";
    }

    public MiuiLockScreen(Context paramContext, Configuration paramConfiguration, LockPatternUtils paramLockPatternUtils, KeyguardUpdateMonitor paramKeyguardUpdateMonitor, KeyguardScreenCallback paramKeyguardScreenCallback)
    {
        super(paramContext);
        this.mLockPatternUtils = paramLockPatternUtils;
        this.mUpdateMonitor = paramKeyguardUpdateMonitor;
        this.mCallback = paramKeyguardScreenCallback;
        this.mEnableMenuKeyInLockScreen = shouldEnableMenuKey();
        this.mCreationOrientation = paramConfiguration.orientation;
        this.mKeyboardHidden = paramConfiguration.hardKeyboardHidden;
        ImageView localImageView1 = new ImageView(this.mContext);
        localImageView1.setImageResource(100794530);
        localImageView1.setScaleType(ImageView.ScaleType.FIT_XY);
        addView(localImageView1, new FrameLayout.LayoutParams(-1, -1, 80));
        this.mBackgroundMask = new ImageView(this.mContext);
        this.mBackgroundMask.setImageResource(100794506);
        this.mBackgroundMask.setVisibility(8);
        this.mBackgroundMask.setScaleType(ImageView.ScaleType.FIT_XY);
        addView(this.mBackgroundMask, new FrameLayout.LayoutParams(-1, -1, 80));
        LayoutInflater localLayoutInflater = LayoutInflater.from(paramContext);
        localLayoutInflater.inflate(100859963, this, true);
        this.mCarrier = ((TextView)findViewById(101384236));
        this.mCarrier.setSelected(true);
        this.mScreenLocked = ((TextView)findViewById(101384237));
        this.mEmergencyCallText = ((Button)findViewById(101384238));
        this.mEmergencyCall = ((View)this.mEmergencyCallText.getParent());
        this.mEmergencyCall.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View paramAnonymousView)
            {
                MiuiLockScreen.this.mCallback.takeEmergencyCallAction();
            }
        });
        this.mUnlockIccCard = findViewById(101384239);
        this.mUnlockIccCard.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View paramAnonymousView)
            {
            }
        });
        this.mMusicController = new MusicController();
        this.mTimeView = localLayoutInflater.inflate(100859927, null);
        this.mHintView = inflate(this.mContext, 100859926, null);
        this.mFadeoutAnim = AnimationUtils.loadAnimation(this.mContext, 100925444);
        setupSlidingPanel();
        setupBatteryInfo();
        this.mCallsAdapter = new CallLogPreviewListAdapter(this.mContext, null);
        this.mCallsHandler = new QueryHandler(this.mContext, this.mCallsAdapter);
        this.mSmsAdapter = new SmsPreviewListAdapter(this.mContext, null);
        this.mSmsHandler = new QueryHandler(this.mContext, this.mSmsAdapter);
        setFocusable(true);
        setFocusableInTouchMode(true);
        setDescendantFocusability(393216);
        setChildrenDrawnWithCacheEnabled(true);
        paramKeyguardUpdateMonitor.registerInfoCallback(this);
        paramKeyguardUpdateMonitor.registerSimStateCallback(this);
        resetStatusInfo(paramKeyguardUpdateMonitor);
        ImageView localImageView2 = new ImageView(this.mContext);
        localImageView2.setScaleType(ImageView.ScaleType.FIT_XY);
        localImageView2.setBackgroundResource(100794557);
        addView(localImageView2, new FrameLayout.LayoutParams(-1, -2, 48));
        this.mPowerManager = ((PowerManager)this.mContext.getSystemService("power"));
        this.mHapticFeedbackUtil = new HapticFeedbackUtil(this.mContext, true);
        onPause();
    }

    private void callAndSmsHandle(ListView paramListView, PreviewCursorAdpater paramPreviewCursorAdpater, View paramView)
    {
        if ((!this.mCallback.isSecure()) && (paramPreviewCursorAdpater.getCount() > 0))
        {
            this.mAdapter = paramPreviewCursorAdpater;
            this.mAdapter.enableDefaultCount(false);
            this.mCurrentPreviewList = paramListView;
            markReadDelayVibrate();
            paramListView.setAdapter(paramPreviewCursorAdpater);
        }
        while (true)
        {
            setControlView(paramView, null);
            return;
            paramListView.setAdapter(null);
        }
    }

    static CharSequence getCarrierString(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
    {
        if (paramCharSequence1 != null);
        while (true)
        {
            return paramCharSequence1;
            if (paramCharSequence2 != null)
                paramCharSequence1 = paramCharSequence2;
            else
                paramCharSequence1 = "";
        }
    }

    private Status getCurrentStatus(IccCard.State paramState)
    {
        int i;
        Status localStatus;
        if ((!this.mUpdateMonitor.isDeviceProvisioned()) && (paramState == IccCard.State.ABSENT))
        {
            i = 1;
            if ((i == 0) && (paramState != IccCard.State.NETWORK_LOCKED))
                break label41;
            localStatus = Status.SimMissingLocked;
        }
        while (true)
        {
            return localStatus;
            i = 0;
            break;
            label41: switch (4.$SwitchMap$com$android$internal$telephony$IccCard$State[paramState.ordinal()])
            {
            default:
                localStatus = Status.SimMissing;
                break;
            case 1:
                localStatus = Status.SimMissing;
                break;
            case 2:
                localStatus = Status.SimMissing;
                break;
            case 3:
                localStatus = Status.SimLocked;
                break;
            case 4:
                localStatus = Status.SimPukLocked;
                break;
            case 5:
                localStatus = Status.Normal;
                break;
            case 6:
                localStatus = Status.SimMissing;
            }
        }
    }

    private void markReadDelayVibrate()
    {
        if (this.mLastGrabTime == 9223372036854775807L)
        {
            this.mLastGrabTime = System.currentTimeMillis();
            postDelayed(this.sLongPressVibration, 2000L);
        }
    }

    private void noHandle()
    {
        removeCallbacks(this.sLongPressVibration);
        Intent localIntent;
        String str;
        if ((System.currentTimeMillis() - this.mLastGrabTime > 2000L) && (this.mAdapter != null))
        {
            int i = 1 + (this.mCurrentPreviewList.getLastVisiblePosition() - this.mCurrentPreviewList.getFirstVisiblePosition() - this.mCurrentPreviewList.getHeaderViewsCount());
            for (int j = i - 1; j >= 0; j--)
                this.mAdapter.markRead(this.mContext, j);
            this.mAdapter.enableDefaultCount(true);
            int k = this.mAdapter.getCount();
            localIntent = new Intent("android.intent.action.APPLICATION_MESSAGE_UPDATE");
            if (k <= i)
                break label178;
            str = String.valueOf(k - i);
            localIntent.putExtra("android.intent.extra.update_application_message_text", str);
            if (this.mAdapter != this.mCallsAdapter)
                break label184;
            localIntent.putExtra("android.intent.extra.update_application_component_name", "com.android.contacts/.TwelveKeyDialer");
        }
        while (true)
        {
            this.mContext.sendBroadcast(localIntent);
            this.mLastGrabTime = 9223372036854775807L;
            return;
            label178: str = null;
            break;
            label184: if (this.mAdapter == this.mSmsAdapter)
                localIntent.putExtra("android.intent.extra.update_application_component_name", "com.android.mms/.ui.MmsTabActivity");
        }
    }

    private void refreshBatteryStringAndIcon()
    {
        String str = null;
        int i = 0;
        int j = 100794533;
        if (this.mShowingBatteryInfo)
        {
            if (!this.mPluggedIn)
                break label124;
            if (this.mBatteryLevel < 100)
                break label81;
            str = getContext().getString(101450111);
            i = 3;
        }
        while (true)
        {
            setBatteryInfo(str);
            this.mSelector.setBatteryLevel(this.mBatteryLevel);
            this.mSelector.setBackgroundFor(i);
            this.mMusicController.setBatteryIndicator(j);
            updateControlView();
            return;
            label81: Context localContext2 = getContext();
            Object[] arrayOfObject2 = new Object[1];
            arrayOfObject2[0] = Integer.valueOf(this.mBatteryLevel);
            str = localContext2.getString(101450110, arrayOfObject2);
            i = 2;
            j = 100794532;
            continue;
            label124: Context localContext1 = getContext();
            Object[] arrayOfObject1 = new Object[1];
            arrayOfObject1[0] = Integer.valueOf(this.mBatteryLevel);
            str = localContext1.getString(101450112, arrayOfObject1);
            i = 1;
            j = 100794534;
        }
    }

    private void resetStatusInfo(KeyguardUpdateMonitor paramKeyguardUpdateMonitor)
    {
        this.mShowingBatteryInfo = paramKeyguardUpdateMonitor.shouldShowBatteryInfo();
        this.mPluggedIn = paramKeyguardUpdateMonitor.isDevicePluggedIn();
        this.mBatteryLevel = paramKeyguardUpdateMonitor.getBatteryLevel();
        this.mStatus = getCurrentStatus(paramKeyguardUpdateMonitor.getSimState());
        updateLayout(this.mStatus);
        refreshBatteryStringAndIcon();
    }

    private void setControlView(View paramView, ViewGroup.MarginLayoutParams paramMarginLayoutParams)
    {
        int i;
        if (paramView != null)
        {
            i = 0;
            if (paramMarginLayoutParams != null);
        }
        while (true)
        {
            try
            {
                paramMarginLayoutParams = (ViewGroup.MarginLayoutParams)paramView.getLayoutParams();
                if (paramMarginLayoutParams == null)
                {
                    paramMarginLayoutParams = new FrameLayout.LayoutParams(-1, -2, 80);
                    break label108;
                    this.mControlView.setVisibility(0);
                    ViewGroup localViewGroup = (ViewGroup)paramView.getParent();
                    if ((this.mControlView.equals(localViewGroup)) && (i != 0))
                        break;
                    this.mControlView.removeAllViews();
                    this.mControlView.addView(paramView, paramMarginLayoutParams);
                    break;
                    this.mControlView.setVisibility(4);
                }
            }
            catch (IllegalStateException localIllegalStateException)
            {
            }
            label108: i = 1;
        }
    }

    private void setupBatteryInfo()
    {
        this.mBatteryInfo = inflate(this.mContext, 100859918, null);
        this.mBatteryInfoText = ((TextView)this.mBatteryInfo.findViewById(101384242));
    }

    private void setupCallsPreviewList()
    {
        this.mCallsControlView = inflate(this.mContext, 100859920, null);
        this.mCallsPreviewList = ((ListView)this.mCallsControlView.findViewById(101384240));
        this.mCallsPreviewList.setItemsCanFocus(false);
        this.mCallsPreviewList.setVerticalScrollBarEnabled(false);
        this.mCallsPreviewList.setDrawingCacheEnabled(true);
        View localView = inflate(this.mContext, 100859919, null);
        this.mCallsPreviewList.addHeaderView(localView);
        this.mCallsPreviewList.setAdapter(this.mCallsAdapter);
    }

    private void setupSlidingPanel()
    {
        this.mSelector = new SlidingPanel(this.mContext);
        addView(this.mSelector, new FrameLayout.LayoutParams(-1, -2, 83));
        this.mSelector.setTimeView(this.mTimeView, null);
        this.mSelector.setOnTriggerListener(this);
        this.mControlView = this.mSelector.getControlView();
    }

    private void setupSmsPreviewList()
    {
        this.mSmsControlView = inflate(this.mContext, 100859924, null);
        this.mSmsPreviewList = ((ListView)this.mSmsControlView.findViewById(101384241));
        this.mSmsPreviewList.setItemsCanFocus(false);
        this.mSmsPreviewList.setVerticalScrollBarEnabled(false);
        this.mSmsPreviewList.setDrawingCacheEnabled(true);
        View localView = inflate(this.mContext, 100859923, null);
        this.mSmsPreviewList.addHeaderView(localView);
        this.mSmsPreviewList.setAdapter(this.mSmsAdapter);
    }

    private boolean shouldEnableMenuKey()
    {
        boolean bool1 = false;
        boolean bool2 = getResources().getBoolean(101253126);
        boolean bool3 = SystemProperties.getBoolean("ro.monkey", false);
        boolean bool4 = new File("/data/local/enable_menu_key").exists();
        if ((!bool2) || (bool3) || (bool4))
            bool1 = true;
        return bool1;
    }

    private void startCallsQuery()
    {
        this.mCallsAdapter.setLoading(true);
        this.mCallsHandler.cancelOperation(53);
        StringBuilder localStringBuilder = new StringBuilder(" type=");
        localStringBuilder.append(3);
        localStringBuilder.append(" AND new=1 ");
        this.mCallsHandler.startQuery(53, null, CallLog.Calls.CONTENT_URI, CallLog_COLUMNS, localStringBuilder.toString(), null, "date DESC");
    }

    private void startSmsQuery()
    {
        this.mSmsAdapter.setLoading(true);
        this.mSmsHandler.cancelOperation(53);
        this.mSmsHandler.startQuery(53, null, ExtraTelephony.MmsSms.CONTENT_PREVIEW_URI, null, null, null, null);
    }

    private void updateBackground()
    {
        int i = 0;
        int j = 1;
        if (this.mMusicController.isMusicControlVisible())
        {
            if (!this.mMusicController.isAlbumShow())
                break label57;
            j = 2;
        }
        switch (j)
        {
        default:
        case 2:
        case 3:
            while (true)
            {
                return;
                label57: j = 3;
                break;
                this.mBackgroundMask.setImageResource(100794535);
                this.mBackgroundMask.setVisibility(0);
            }
        case 0:
        case 1:
        }
        this.mBackgroundMask.setImageResource(100794506);
        ImageView localImageView = this.mBackgroundMask;
        if (this.mShowingBatteryInfo);
        while (true)
        {
            localImageView.setVisibility(i);
            if ((this.mControlViewType != 0) || (!this.mShowingBatteryInfo))
                break;
            this.mControlViewType = 5;
            break;
            i = 8;
        }
    }

    private void updateControlView()
    {
        if ((this.mControlViewType == 4) && (this.mMusicController.isMusicStatusNone()))
            this.mControlViewType = 0;
        if ((this.mControlViewType != 0) && (this.mMusicController.shouldShowMusic()))
        {
            this.mMusicController.showAlbum();
            this.mMusicController.clearAnimation();
            switch (this.mControlViewType)
            {
            default:
                setControlView(null, null);
            case 4:
            case 2:
            case 1:
            case 3:
            case 5:
            }
        }
        while (true)
        {
            return;
            updateBackground();
            break;
            setControlView(this.mMusicController.getMusicControl(), null);
            continue;
            if (this.mSmsControlView == null)
                setupSmsPreviewList();
            callAndSmsHandle(this.mSmsPreviewList, this.mSmsAdapter, this.mSmsControlView);
            continue;
            if (this.mCallsControlView == null)
                setupCallsPreviewList();
            callAndSmsHandle(this.mCallsPreviewList, this.mCallsAdapter, this.mCallsControlView);
            continue;
            setControlView(this.mHintView, null);
            continue;
            setControlView(this.mBatteryInfo, null);
        }
    }

    private void updateLayout(Status paramStatus)
    {
        this.mEmergencyCall.setVisibility(8);
        this.mUnlockIccCard.setVisibility(8);
        switch (4.$SwitchMap$com$android$internal$policy$impl$MiuiLockScreen$Status[paramStatus.ordinal()])
        {
        default:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        }
        while (true)
        {
            return;
            this.mCarrier.setText(getCarrierString(this.mUpdateMonitor.getTelephonyPlmn(), this.mUpdateMonitor.getTelephonySpn()));
            this.mScreenLocked.setText("");
            this.mCarrier.setVisibility(8);
            this.mScreenLocked.setVisibility(0);
            this.mSelector.setVisibility(0);
            this.mEmergencyCallText.setVisibility(8);
            continue;
            this.mCarrier.setText(getCarrierString(this.mUpdateMonitor.getTelephonyPlmn(), getContext().getText(101450115)));
            this.mScreenLocked.setText(101450119);
            this.mCarrier.setVisibility(0);
            this.mScreenLocked.setVisibility(0);
            this.mSelector.setVisibility(0);
            this.mEmergencyCallText.setVisibility(8);
            continue;
            this.mCarrier.setText(101450113);
            this.mScreenLocked.setText(101450114);
            this.mCarrier.setVisibility(0);
            this.mScreenLocked.setVisibility(4);
            this.mSelector.setVisibility(0);
            this.mEmergencyCallText.setVisibility(0);
            continue;
            this.mCarrier.setText(getCarrierString(this.mUpdateMonitor.getTelephonyPlmn(), getContext().getText(101450113)));
            this.mScreenLocked.setText(101450114);
            this.mCarrier.setVisibility(0);
            this.mScreenLocked.setVisibility(0);
            this.mSelector.setVisibility(8);
            this.mEmergencyCallText.setVisibility(0);
            this.mEmergencyCall.setVisibility(0);
            continue;
            this.mCarrier.setText(getCarrierString(this.mUpdateMonitor.getTelephonyPlmn(), getContext().getText(101450118)));
            this.mCarrier.setVisibility(0);
            this.mScreenLocked.setVisibility(4);
            this.mSelector.setVisibility(0);
            this.mEmergencyCallText.setVisibility(8);
            continue;
            this.mCarrier.setText(getCarrierString(this.mUpdateMonitor.getTelephonyPlmn(), getContext().getText(101450116)));
            this.mScreenLocked.setText(101450117);
            this.mCarrier.setVisibility(0);
            this.mScreenLocked.setVisibility(0);
            this.mSelector.setVisibility(8);
            this.mEmergencyCallText.setVisibility(0);
            this.mEmergencyCall.setVisibility(0);
            this.mUnlockIccCard.setVisibility(0);
        }
    }

    public void cleanUp()
    {
        this.mUpdateMonitor.removeCallback(this);
        this.mLockPatternUtils = null;
        this.mUpdateMonitor = null;
        this.mCallsHandler.closeCursor();
        this.mSmsHandler.closeCursor();
    }

    public boolean needsInput()
    {
        return false;
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        updateConfiguration();
        this.mMusicController.registerPlayerStatusListener();
        startCallsQuery();
        startSmsQuery();
    }

    public void onClockVisibilityChanged()
    {
    }

    protected void onConfigurationChanged(Configuration paramConfiguration)
    {
        super.onConfigurationChanged(paramConfiguration);
        updateConfiguration();
    }

    protected void onDetachedFromWindow()
    {
        this.mMusicController.unregisterPlayerStatusListener();
        super.onDetachedFromWindow();
    }

    public void onDevicePolicyManagerStateChanged()
    {
    }

    public void onDeviceProvisioned()
    {
    }

    public void onGrabbedStateChange(View paramView, int paramInt)
    {
        if (paramView == this.mSelector)
        {
            if ((!this.isPaused) && (this.mPowerManager.isScreenOn()))
                this.mCallback.pokeWakelock();
            switch (paramInt)
            {
            case 4:
            default:
            case 1:
            case 2:
            case 6:
            case 3:
            case 5:
            case 0:
            }
        }
        while (true)
        {
            return;
            this.mControlViewType = 1;
            this.mMusicController.removeCheckStreamCallbacks();
            updateControlView();
            continue;
            this.mControlViewType = 2;
            this.mMusicController.removeCheckStreamCallbacks();
            updateControlView();
            continue;
            this.mMusicController.toggleMusicControl();
            this.mMusicController.removeCheckStreamCallbacks();
            this.mMusicController.updateSpectrumVisualizer();
            continue;
            this.mControlViewType = 3;
            this.mMusicController.removeCheckStreamCallbacks();
            updateControlView();
            continue;
            this.mMusicController.removeCheckStreamCallbacks();
            this.mControlViewType = 0;
            if (this.mControlView.getVisibility() == 0)
            {
                this.mControlView.setVisibility(4);
                this.mControlView.clearAnimation();
                this.mControlView.startAnimation(this.mFadeoutAnim);
            }
            removeCallbacks(this.sLongPressVibration);
            this.mLastGrabTime = 9223372036854775807L;
            continue;
            noHandle();
            this.mMusicController.addCheckStreamCallbacks();
            this.mControlViewType = 4;
            this.mControlView.setVisibility(0);
            updateControlView();
        }
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
        if ((paramInt == 82) && (this.mEnableMenuKeyInLockScreen))
        {
            this.mCallback.goToUnlockScreen();
            Log.d("MiuiLockScreen", "Unlock Screen by pressing menu");
        }
        return false;
    }

    public void onPause()
    {
        this.isPaused = true;
        this.mSelector.onPause();
        this.mMusicController.onPause();
    }

    public void onPhoneStateChanged(int paramInt)
    {
        if ((!TelephonyManager.EXTRA_STATE_IDLE.equals(Integer.valueOf(paramInt))) && (!this.isPaused))
            this.mSelector.clearBatteryAnimations();
        while (true)
        {
            this.mLockPatternUtils.updateEmergencyCallButtonState(this.mEmergencyCallText, paramInt, true);
            return;
            if ((TelephonyManager.EXTRA_STATE_IDLE.equals(Integer.valueOf(paramInt))) && (!this.isPaused))
                refreshBatteryStringAndIcon();
        }
    }

    public void onRefreshBatteryInfo(boolean paramBoolean1, boolean paramBoolean2, int paramInt)
    {
        this.mShowingBatteryInfo = paramBoolean1;
        this.mPluggedIn = paramBoolean2;
        this.mBatteryLevel = paramInt;
        refreshBatteryStringAndIcon();
    }

    public void onRefreshCarrierInfo(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
    {
        updateLayout(this.mStatus);
    }

    public void onRefreshCarrierInfo(CharSequence paramCharSequence1, CharSequence paramCharSequence2, int paramInt)
    {
    }

    public void onResume()
    {
        this.mMusicController.onResume();
        resetStatusInfo(this.mUpdateMonitor);
        updateControlView();
        this.mSelector.onResume();
        this.isPaused = false;
    }

    public void onRingerModeChanged(int paramInt)
    {
    }

    public void onSimStateChanged(IccCard.State paramState)
    {
        this.mStatus = getCurrentStatus(paramState);
        Log.d("LockScreen", "current status is " + this.mStatus + ", and prev status is " + sPrevStatus);
        if ((sPrevStatus == Status.SimPukLocked) && (this.mStatus != Status.SimPukLocked))
        {
            this.mCallback.setPendingIntent(null);
            this.mCallback.goToUnlockScreen();
        }
        while (true)
        {
            sPrevStatus = this.mStatus;
            return;
            updateLayout(this.mStatus);
        }
    }

    public void onSimStateChanged(IccCard.State paramState, int paramInt)
    {
    }

    public void onTimeChanged()
    {
    }

    public void onTrigger(View paramView, int paramInt)
    {
        if (paramView == this.mSelector)
        {
            if (paramInt != 1)
                break label68;
            Intent localIntent1 = new Intent("android.intent.action.VIEW");
            localIntent1.setType("vnd.android.cursor.dir/calls");
            localIntent1.addCategory("android.intent.category.DEFAULT");
            localIntent1.setFlags(872415232);
            this.mCallback.setPendingIntent(localIntent1);
            this.mCallback.goToUnlockScreen();
        }
        while (true)
        {
            return;
            label68: if (paramInt == 3)
            {
                this.mCallback.setPendingIntent(null);
                this.mCallback.goToUnlockScreen();
            }
            else if (paramInt == 2)
            {
                Intent localIntent2 = new Intent("android.intent.action.MAIN");
                HashSet localHashSet = new HashSet();
                this.mSmsAdapter.enableDefaultCount(true);
                for (int i = -1 + this.mSmsAdapter.getCount(); i >= 0; i--)
                {
                    Cursor localCursor = (Cursor)this.mSmsAdapter.getItem(i);
                    if (localCursor != null)
                        localHashSet.add(Long.valueOf(localCursor.getLong(6)));
                }
                if (localHashSet.size() == 1)
                {
                    localIntent2.setAction("android.intent.action.VIEW");
                    localIntent2.putExtra("thread_id", (Serializable)localHashSet.iterator().next());
                    localIntent2.putExtra("view_conversation", true);
                }
                localIntent2.setType("vnd.android-dir/mms-sms");
                localIntent2.addCategory("android.intent.category.DEFAULT");
                localIntent2.setFlags(872415232);
                this.mCallback.setPendingIntent(localIntent2);
                this.mCallback.goToUnlockScreen();
            }
        }
    }

    public void onUserChanged(int paramInt)
    {
    }

    public void setBatteryInfo(String paramString)
    {
        this.mBatteryInfoText.setText(paramString);
    }

    void updateConfiguration()
    {
        int i = 1;
        Configuration localConfiguration = getResources().getConfiguration();
        if (localConfiguration.hardKeyboardHidden != this.mKeyboardHidden)
        {
            this.mKeyboardHidden = localConfiguration.hardKeyboardHidden;
            if (this.mKeyboardHidden != i)
                break label51;
        }
        while (true)
        {
            if (i != 0)
                this.mCallback.goToUnlockScreen();
            return;
            label51: i = 0;
        }
    }

    private class MusicController
        implements View.OnTouchListener
    {
        private static final int ANIM_FADEIN = 1;
        private static final int ANIM_FADEOUT = 2;
        private static final int ANIM_NONE = 0;
        private static final int CHECK_STREAM_MUSIC_DELAY = 1000;
        private static final int MUSIC_SHOW_NONE = 0;
        private static final int MUSIC_SHOW_PLAY = 2;
        private static final int MUSIC_SHOW_STOP = 1;
        private Bitmap mAlbumBm;
        private boolean mAlbumChanged;
        private Animation mAlbumFadeInAnim;
        private Animation mAlbumFadeOutAnim;
        private String mAlbumName;
        private AlbumFrameView mAlbumView;
        private String mArtistName;
        private Runnable mCheckStreamMusicRunnable = new Runnable()
        {
            public void run()
            {
                MiuiLockScreen.MusicController.this.updateMusic();
                MiuiLockScreen.MusicController.this.updateSpectrumVisualizer();
                MiuiLockScreen.MusicController.this.addCheckStreamCallbacks();
            }
        };
        private Bitmap mDefaultAlbum;
        private boolean mEnable;
        private Runnable mEnableSpectrumVisualizerRunnable = new Runnable()
        {
            public void run()
            {
                MiuiLockScreen.this.removeCallbacks(MiuiLockScreen.MusicController.this.mEnableSpectrumVisualizerRunnable);
                MiuiLockScreen.MusicController.this.updateSpectrumVisualizer();
            }
        };
        private final Handler mHandler = new Handler();
        private boolean mIsOnlineSongBlocking;
        private ImageView mMusicBatteryIndicator;
        private int mMusicBatteryIndicatorId;
        private View mMusicControl;
        private ImageView mMusicPlayPauseButton;
        private int mMusicStatus;
        private TextView mMusicTitle;
        private BroadcastReceiver mPlayerStatusListener = new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                if (MiuiLockScreen.MusicController.this.mMusicControl == null);
                while (true)
                {
                    return;
                    String str1 = paramAnonymousIntent.getAction();
                    if (paramAnonymousIntent.getBooleanExtra("playing", false))
                        if ("com.miui.player.metachanged".equals(str1))
                        {
                            String str2 = paramAnonymousIntent.getStringExtra("other");
                            if ("meta_changed_track".equals(str2))
                            {
                                MiuiLockScreen.MusicController.this.setTrackInfo(paramAnonymousIntent);
                                MiuiLockScreen.MusicController.this.requestAlbum(paramAnonymousIntent);
                            }
                            else if ("meta_changed_album".equals(str2))
                            {
                                MiuiLockScreen.MusicController.this.requestAlbum(paramAnonymousIntent, true);
                            }
                        }
                        else if ("lockscreen.action.SONG_METADATA_UPDATED".equals(str1))
                        {
                            MiuiLockScreen.MusicController.this.setTrackInfo(paramAnonymousIntent);
                            MiuiLockScreen.MusicController.this.setAlbumInfo(paramAnonymousIntent);
                        }
                        else if ("com.miui.player.refreshprogress".equals(str1))
                        {
                            MiuiLockScreen.MusicController.access$3002(MiuiLockScreen.MusicController.this, paramAnonymousIntent.getBooleanExtra("blocking", false));
                        }
                        else if (("com.miui.player.playstatechanged".equals(str1)) && (MiuiLockScreen.MusicController.this.mMusicTitle.getVisibility() == 8))
                        {
                            MiuiLockScreen.MusicController.this.setTrackInfo(paramAnonymousIntent);
                            MiuiLockScreen.MusicController.this.requestAlbum(paramAnonymousIntent);
                        }
                }
            }
        };
        private SpectrumVisualizer mSpectrumVisualizer;
        private Bitmap mTempAlbumBm;
        private AlbumFrameView mTempAlbumView;

        public MusicController()
        {
        }

        private void enableMusicControl(boolean paramBoolean)
        {
            View localView = this.mMusicControl;
            int i;
            if (paramBoolean)
            {
                i = 0;
                localView.setVisibility(i);
                if (!paramBoolean)
                    break label48;
                MiuiLockScreen.access$2102(MiuiLockScreen.this, 4);
                MiuiLockScreen.this.setControlView(this.mMusicControl, null);
            }
            while (true)
            {
                return;
                i = 8;
                break;
                label48: this.mMusicStatus = 0;
                if (MiuiLockScreen.this.mControlViewType == 4)
                    MiuiLockScreen.this.updateControlView();
            }
        }

        private void hideAlbum()
        {
            this.mAlbumView.clearAnimation();
            this.mTempAlbumView.clearAnimation();
            this.mTempAlbumView.setVisibility(8);
            this.mAlbumView.setDisplayBitmap(null);
            this.mTempAlbumView.setDisplayBitmap(null);
            MiuiLockScreen.this.updateBackground();
        }

        private boolean isAlbumShow()
        {
            if (this.mAlbumBm != null);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        private void requestAlbum()
        {
            if (this.mAlbumChanged)
            {
                Intent localIntent = new Intent("lockscreen.action.SONG_METADATA_REQUEST");
                MiuiLockScreen.this.mContext.sendBroadcast(localIntent);
            }
        }

        private void requestAlbum(Intent paramIntent)
        {
            requestAlbum(paramIntent, false);
        }

        private void sendMediaButtonBroadcast(int paramInt1, int paramInt2)
        {
            long l = SystemClock.uptimeMillis();
            KeyEvent localKeyEvent = new KeyEvent(l, l, paramInt1, paramInt2, 0);
            Intent localIntent = new Intent("android.intent.action.MEDIA_BUTTON", null);
            localIntent.putExtra("forbid_double_click", true);
            localIntent.putExtra("android.intent.extra.KEY_EVENT", KeyEvent.changeFlags(localKeyEvent, 8));
            MiuiLockScreen.this.mContext.sendOrderedBroadcast(localIntent, null);
            if ((!MiuiLockScreen.this.isPaused) && (MiuiLockScreen.this.mPowerManager.isScreenOn()))
                MiuiLockScreen.this.mCallback.pokeWakelock();
        }

        private void setAlbumInfo(Intent paramIntent)
        {
            if (!this.mEnable);
            while (true)
            {
                return;
                this.mAlbumChanged = false;
                this.mAlbumName = paramIntent.getStringExtra("album");
                this.mArtistName = paramIntent.getStringExtra("artist");
                try
                {
                    String str = paramIntent.getStringExtra("tmp_album_path");
                    Bitmap localBitmap;
                    if (str != null)
                    {
                        localBitmap = BitmapFactory.decodeFile(str);
                        if (localBitmap == null);
                    }
                    for (this.mAlbumBm = localBitmap; ; this.mAlbumBm = null)
                    {
                        if (this.mAlbumBm == null)
                            break label185;
                        this.mAlbumView.setDisplayBitmap(this.mAlbumBm);
                        this.mMusicBatteryIndicator.setVisibility(8);
                        label94: if (this.mTempAlbumBm == null)
                            break label207;
                        this.mTempAlbumView.setDisplayBitmap(this.mTempAlbumBm);
                        label112: startTrackChangeAnim();
                        break;
                    }
                }
                catch (OutOfMemoryError localOutOfMemoryError)
                {
                    this.mAlbumBm = null;
                    if (this.mAlbumBm != null)
                    {
                        this.mAlbumView.setDisplayBitmap(this.mAlbumBm);
                        this.mMusicBatteryIndicator.setVisibility(8);
                        label160: if (this.mTempAlbumBm == null)
                            break label243;
                        this.mTempAlbumView.setDisplayBitmap(this.mTempAlbumBm);
                    }
                    while (true)
                    {
                        startTrackChangeAnim();
                        break;
                        label185: this.mAlbumView.setDisplayBitmap(this.mDefaultAlbum);
                        this.mMusicBatteryIndicator.setVisibility(0);
                        break label94;
                        label207: this.mTempAlbumView.setDisplayBitmap(this.mDefaultAlbum);
                        break label112;
                        this.mAlbumView.setDisplayBitmap(this.mDefaultAlbum);
                        this.mMusicBatteryIndicator.setVisibility(0);
                        break label160;
                        label243: this.mTempAlbumView.setDisplayBitmap(this.mDefaultAlbum);
                    }
                }
                finally
                {
                    if (this.mAlbumBm == null)
                        break label309;
                }
            }
            this.mAlbumView.setDisplayBitmap(this.mAlbumBm);
            this.mMusicBatteryIndicator.setVisibility(8);
            if (this.mTempAlbumBm != null)
                this.mTempAlbumView.setDisplayBitmap(this.mTempAlbumBm);
            while (true)
            {
                startTrackChangeAnim();
                throw localObject;
                label309: this.mAlbumView.setDisplayBitmap(this.mDefaultAlbum);
                this.mMusicBatteryIndicator.setVisibility(0);
                break;
                this.mTempAlbumView.setDisplayBitmap(this.mDefaultAlbum);
            }
        }

        private void setTrackInfo(Intent paramIntent)
        {
            String str1 = paramIntent.getStringExtra("track");
            String str2 = paramIntent.getStringExtra("artist");
            this.mIsOnlineSongBlocking = false;
            if ((TextUtils.isEmpty(str1)) && (TextUtils.isEmpty(str2)))
            {
                this.mMusicTitle.setVisibility(8);
                return;
            }
            if (TextUtils.isEmpty(str1))
                this.mMusicTitle.setText(str2);
            while (true)
            {
                this.mMusicTitle.setVisibility(0);
                this.mMusicTitle.setSelected(true);
                break;
                if (TextUtils.isEmpty(str2))
                {
                    this.mMusicTitle.setText(str1);
                }
                else
                {
                    TextView localTextView = this.mMusicTitle;
                    Object[] arrayOfObject = new Object[2];
                    arrayOfObject[0] = str1;
                    arrayOfObject[1] = str2;
                    localTextView.setText(String.format("%s-%s", arrayOfObject));
                }
            }
        }

        private void setupMusicControl()
        {
            this.mMusicControl = View.inflate(MiuiLockScreen.this.mContext, 100859922, null);
            this.mMusicControl.setLayoutParams(new FrameLayout.LayoutParams(-1, -2, 80));
            this.mMusicControl.setDrawingCacheEnabled(false);
            this.mMusicTitle = ((TextView)this.mMusicControl.findViewById(101384257));
            this.mMusicBatteryIndicator = ((ImageView)this.mMusicControl.findViewById(101384260));
            if (this.mMusicBatteryIndicatorId != 0)
                this.mMusicBatteryIndicator.setImageResource(this.mMusicBatteryIndicatorId);
            this.mMusicControl.findViewById(101384258).setOnTouchListener(this);
            this.mMusicControl.findViewById(101384259).setOnTouchListener(this);
            this.mMusicPlayPauseButton = ((ImageView)this.mMusicControl.findViewById(101384253));
            this.mMusicPlayPauseButton.setOnTouchListener(this);
            this.mSpectrumVisualizer = ((SpectrumVisualizer)this.mMusicControl.findViewById(101384256));
            this.mAlbumView = ((AlbumFrameView)this.mMusicControl.findViewById(101384254));
            this.mTempAlbumView = ((AlbumFrameView)this.mMusicControl.findViewById(101384255));
            this.mMusicControl.setVisibility(8);
            init();
        }

        private void showAlbum()
        {
            this.mTempAlbumView.setVisibility(8);
            this.mAlbumView.setVisibility(0);
            if (this.mAlbumBm != null)
            {
                this.mAlbumView.setDisplayBitmap(this.mAlbumBm);
                this.mMusicBatteryIndicator.setVisibility(8);
            }
            while (true)
            {
                MiuiLockScreen.this.updateBackground();
                return;
                this.mAlbumView.setDisplayBitmap(this.mDefaultAlbum);
                this.mMusicBatteryIndicator.setVisibility(0);
            }
        }

        private void startAlbumAnim(int paramInt)
        {
            this.mAlbumView.clearAnimation();
            switch (paramInt)
            {
            default:
            case 1:
            case 2:
            case 0:
            }
            while (true)
            {
                return;
                this.mMusicControl.startAnimation(this.mAlbumFadeInAnim);
                showAlbum();
                continue;
                this.mMusicControl.startAnimation(this.mAlbumFadeOutAnim);
                continue;
                this.mMusicControl.clearAnimation();
            }
        }

        private void startTrackChangeAnim()
        {
            this.mAlbumView.clearAnimation();
            this.mTempAlbumView.clearAnimation();
            MiuiLockScreen.this.updateBackground();
            this.mTempAlbumView.setVisibility(0);
            this.mTempAlbumView.startAnimation(this.mAlbumFadeOutAnim);
        }

        private void updateMusic()
        {
            if (this.mMusicControl == null);
            while (true)
            {
                return;
                boolean bool1 = AudioSystem.isStreamActive(3, 0);
                boolean bool2 = bool1;
                if (this.mIsOnlineSongBlocking)
                    bool2 = true;
                ImageView localImageView = this.mMusicPlayPauseButton;
                if (bool2);
                for (int i = 100794542; ; i = 100794545)
                {
                    localImageView.setImageResource(i);
                    switch (this.mMusicStatus)
                    {
                    default:
                        break;
                    case 1:
                        if (!bool1)
                            break;
                        this.mMusicStatus = 2;
                        break;
                    case 2:
                    }
                }
                if (!bool1)
                    this.mMusicStatus = 1;
            }
        }

        private void updateSpectrumVisualizer()
        {
            boolean bool = false;
            if (this.mSpectrumVisualizer == null);
            while (true)
            {
                return;
                if ((AudioSystem.isStreamActive(3, 0)) && (this.mMusicControl.getVisibility() == 0) && (!MiuiLockScreen.this.isPaused))
                    bool = true;
                this.mSpectrumVisualizer.enableUpdate(bool);
            }
        }

        public void addCheckStreamCallbacks()
        {
            addCheckStreamCallbacks(1000);
        }

        public void addCheckStreamCallbacks(int paramInt)
        {
            removeCheckStreamCallbacks();
            MiuiLockScreen.this.postDelayed(this.mCheckStreamMusicRunnable, paramInt);
        }

        public void clearAnimation()
        {
            if (this.mMusicControl != null)
                this.mMusicControl.clearAnimation();
        }

        public void enableAlbum(boolean paramBoolean)
        {
            this.mEnable = paramBoolean;
            if (this.mAlbumChanged)
            {
                this.mAlbumBm = null;
                this.mTempAlbumBm = null;
            }
            if (paramBoolean)
            {
                boolean bool = AudioSystem.isStreamActive(3, 0);
                if ((this.mIsOnlineSongBlocking) || (bool))
                    requestAlbum();
            }
        }

        public View getMusicControl()
        {
            if (this.mMusicControl == null)
                setupMusicControl();
            updateMusic();
            return this.mMusicControl;
        }

        public void init()
        {
            this.mAlbumFadeInAnim = AnimationUtils.loadAnimation(MiuiLockScreen.this.mContext, 100925442);
            this.mAlbumFadeOutAnim = AnimationUtils.loadAnimation(MiuiLockScreen.this.mContext, 100925443);
            this.mAlbumChanged = true;
            this.mDefaultAlbum = BitmapFactory.decodeResource(MiuiLockScreen.this.mContext.getResources(), 100794537);
            Animation.AnimationListener local1 = new Animation.AnimationListener()
            {
                public void onAnimationEnd(Animation paramAnonymousAnimation)
                {
                    if (MiuiLockScreen.MusicController.this.mAlbumBm != null)
                    {
                        MiuiLockScreen.MusicController.this.mAlbumView.setDisplayBitmap(MiuiLockScreen.MusicController.this.mAlbumBm);
                        MiuiLockScreen.MusicController.this.mMusicBatteryIndicator.setVisibility(8);
                    }
                    while (true)
                    {
                        MiuiLockScreen.MusicController.this.mAlbumView.setVisibility(0);
                        MiuiLockScreen.MusicController.this.mTempAlbumView.setDisplayBitmap(null);
                        MiuiLockScreen.MusicController.this.mTempAlbumView.setVisibility(8);
                        return;
                        MiuiLockScreen.MusicController.this.mAlbumView.setDisplayBitmap(MiuiLockScreen.MusicController.this.mDefaultAlbum);
                        MiuiLockScreen.MusicController.this.mMusicBatteryIndicator.setVisibility(0);
                    }
                }

                public void onAnimationRepeat(Animation paramAnonymousAnimation)
                {
                }

                public void onAnimationStart(Animation paramAnonymousAnimation)
                {
                }
            };
            this.mAlbumFadeInAnim.setAnimationListener(local1);
            this.mAlbumFadeOutAnim.setAnimationListener(local1);
            this.mAlbumFadeInAnim.setFillAfter(true);
            Resources localResources = MiuiLockScreen.this.mContext.getResources();
            InputStream localInputStream = localResources.openRawResource(100794531);
            Rect localRect = new Rect();
            Bitmap localBitmap = BitmapFactory.decodeResourceStream(localResources, null, localInputStream, localRect, null);
            if (localInputStream != null);
            try
            {
                localInputStream.close();
                this.mAlbumView.setRect(localRect);
                this.mAlbumView.setFilterBitmap(localBitmap);
                this.mAlbumView.setDisplayBitmap(this.mDefaultAlbum);
                FrameLayout.LayoutParams localLayoutParams1 = (FrameLayout.LayoutParams)this.mAlbumView.getLayoutParams();
                localLayoutParams1.width = localBitmap.getWidth();
                localLayoutParams1.height = localBitmap.getHeight();
                this.mAlbumView.setLayoutParams(localLayoutParams1);
                this.mTempAlbumView.setRect(localRect);
                this.mTempAlbumView.setFilterBitmap(localBitmap);
                this.mTempAlbumView.setDisplayBitmap(this.mDefaultAlbum);
                FrameLayout.LayoutParams localLayoutParams2 = (FrameLayout.LayoutParams)this.mTempAlbumView.getLayoutParams();
                localLayoutParams2.width = localBitmap.getWidth();
                localLayoutParams2.height = localBitmap.getHeight();
                this.mTempAlbumView.setLayoutParams(localLayoutParams2);
                return;
            }
            catch (IOException localIOException)
            {
                while (true)
                    localIOException.printStackTrace();
            }
        }

        public boolean isMusicControlVisible()
        {
            if ((this.mMusicControl != null) && (this.mMusicControl.getVisibility() == 0));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public boolean isMusicStatusNone()
        {
            if (this.mMusicStatus == 0);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void onPause()
        {
            MiuiLockScreen.this.removeCallbacks(this.mEnableSpectrumVisualizerRunnable);
            if (this.mMusicControl != null)
            {
                enableMusicControl(false);
                enableAlbum(false);
            }
            removeCheckStreamCallbacks();
            updateSpectrumVisualizer();
        }

        public void onResume()
        {
            if (AudioOutputHelper.hasActiveReceivers(MiuiLockScreen.this.mContext))
            {
                if (this.mMusicControl == null)
                    setupMusicControl();
                MiuiLockScreen.access$2102(MiuiLockScreen.this, 4);
                this.mMusicStatus = 2;
                enableMusicControl(true);
                enableAlbum(true);
                MiuiLockScreen.this.postDelayed(this.mEnableSpectrumVisualizerRunnable, 500L);
            }
            while (true)
            {
                addCheckStreamCallbacks();
                return;
                MiuiLockScreen.access$2102(MiuiLockScreen.this, 0);
                this.mMusicStatus = 0;
                if (this.mMusicControl != null)
                {
                    enableMusicControl(false);
                    enableAlbum(false);
                    hideAlbum();
                    updateSpectrumVisualizer();
                }
            }
        }

        public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
        {
            int i = 0;
            int j = 85;
            int k;
            switch (paramView.getId())
            {
            default:
                k = 0xFF & paramMotionEvent.getAction();
                if (k == 0)
                {
                    MiuiLockScreen.this.mSelector.requestDisallowInterceptTouchEvent(true);
                    sendMediaButtonBroadcast(0, j);
                    paramView.setPressed(true);
                }
                break;
            case 101384258:
            case 101384259:
            }
            while ((k != 1) && (k != 3))
            {
                return true;
                j = 88;
                break;
                j = 87;
                break;
            }
            sendMediaButtonBroadcast(1, j);
            paramView.setPressed(false);
            if ((j == 85) && (this.mMusicStatus == 2))
                i = 1;
            ImageView localImageView = this.mMusicPlayPauseButton;
            int m;
            if (i != 0)
            {
                m = 100794545;
                label142: localImageView.setImageResource(m);
                if (i == 0)
                    break label188;
            }
            label188: for (int n = 1; ; n = 2)
            {
                this.mMusicStatus = n;
                this.mSpectrumVisualizer.enableUpdate(true);
                addCheckStreamCallbacks(3000);
                break;
                m = 100794542;
                break label142;
            }
        }

        public void registerPlayerStatusListener()
        {
            IntentFilter localIntentFilter = new IntentFilter();
            localIntentFilter.addAction("com.miui.player.metachanged");
            localIntentFilter.addAction("lockscreen.action.SONG_METADATA_UPDATED");
            localIntentFilter.addAction("com.miui.player.refreshprogress");
            localIntentFilter.addAction("com.miui.player.playstatechanged");
            MiuiLockScreen.this.mContext.registerReceiver(this.mPlayerStatusListener, localIntentFilter, null, this.mHandler);
        }

        public void removeCheckStreamCallbacks()
        {
            MiuiLockScreen.this.removeCallbacks(this.mCheckStreamMusicRunnable);
        }

        public void requestAlbum(Intent paramIntent, boolean paramBoolean)
        {
            this.mAlbumChanged = true;
            if (!this.mEnable);
            while (true)
            {
                return;
                String str1 = paramIntent.getStringExtra("album");
                String str2 = paramIntent.getStringExtra("artist");
                if (str1 == null)
                    str1 = "";
                if (str2 == null)
                    str2 = "";
                if ((paramBoolean) || (((!str1.equals(this.mAlbumName)) || (!str2.equals(this.mArtistName)) || (this.mAlbumBm == null)) && (this.mEnable)))
                {
                    Uri localUri = (Uri)paramIntent.getParcelableExtra("album_uri");
                    int i = 0;
                    if (localUri != null)
                        i = 1;
                    while (true)
                    {
                        this.mTempAlbumBm = this.mAlbumBm;
                        this.mAlbumBm = null;
                        this.mTempAlbumView.setDisplayBitmap(null);
                        if (i == 0)
                            break label161;
                        requestAlbum();
                        break;
                        if (paramIntent.getStringExtra("album_path") != null)
                            i = 1;
                    }
                    label161: startTrackChangeAnim();
                }
            }
        }

        public void setBatteryIndicator(int paramInt)
        {
            if (this.mMusicBatteryIndicator != null)
                this.mMusicBatteryIndicator.setImageResource(this.mMusicBatteryIndicatorId);
        }

        public boolean shouldShowMusic()
        {
            int i = 1;
            if ((this.mMusicStatus == 2) || (this.mMusicStatus == i))
                if (this.mMusicControl == null)
                    setupMusicControl();
            while (true)
            {
                return i;
                int j = 0;
            }
        }

        public void toggleMusicControl()
        {
            int i = 2;
            if (this.mMusicControl == null)
                setupMusicControl();
            int j;
            if (this.mMusicControl.getVisibility() == 0)
            {
                j = 1;
                boolean bool = AudioSystem.isStreamActive(3, 0);
                if (j != 0)
                    break label82;
                MiuiLockScreen.access$2102(MiuiLockScreen.this, 4);
                if (!bool)
                    break label74;
                this.mMusicStatus = i;
                label53: enableMusicControl(true);
                enableAlbum(true);
                startAlbumAnim(1);
            }
            while (true)
            {
                return;
                j = 0;
                break;
                label74: this.mMusicStatus = 1;
                break label53;
                label82: this.mMusicStatus = 0;
                MiuiLockScreen.access$2102(MiuiLockScreen.this, 0);
                enableAlbum(false);
                if (MiuiLockScreen.this.mShowingBatteryInfo)
                    i = 0;
                startAlbumAnim(i);
                enableMusicControl(false);
            }
        }

        public void unregisterPlayerStatusListener()
        {
            MiuiLockScreen.this.mContext.unregisterReceiver(this.mPlayerStatusListener);
        }
    }

    private static final class QueryHandler extends AsyncQueryHandler
    {
        private final MiuiLockScreen.PreviewCursorAdpater mAdapter;

        public QueryHandler(Context paramContext, MiuiLockScreen.PreviewCursorAdpater paramPreviewCursorAdpater)
        {
            super();
            this.mAdapter = paramPreviewCursorAdpater;
        }

        public void closeCursor()
        {
            this.mAdapter.changeCursor(null);
        }

        protected Handler createHandler(Looper paramLooper)
        {
            return new CatchingWorkerHandler(paramLooper);
        }

        protected void onQueryComplete(int paramInt, Object paramObject, Cursor paramCursor)
        {
            int i = 0;
            this.mAdapter.setLoading(false);
            this.mAdapter.changeCursor(paramCursor);
            MiuiLockScreen.PreviewCursorAdpater localPreviewCursorAdpater = this.mAdapter;
            if (paramCursor == null);
            while (true)
            {
                localPreviewCursorAdpater.onQueryResultCount(i);
                return;
                i = paramCursor.getCount();
            }
        }

        protected class CatchingWorkerHandler extends AsyncQueryHandler.WorkerHandler
        {
            public CatchingWorkerHandler(Looper arg2)
            {
                super(localLooper);
            }

            public void handleMessage(Message paramMessage)
            {
                try
                {
                    super.handleMessage(paramMessage);
                    return;
                }
                catch (SQLiteDiskIOException localSQLiteDiskIOException)
                {
                    while (true)
                        Log.w("LockScreen", "Exception on background worker thread", localSQLiteDiskIOException);
                }
                catch (SQLiteFullException localSQLiteFullException)
                {
                    while (true)
                        Log.w("LockScreen", "Exception on background worker thread", localSQLiteFullException);
                }
                catch (SQLiteDatabaseCorruptException localSQLiteDatabaseCorruptException)
                {
                    while (true)
                        Log.w("LockScreen", "Exception on background worker thread", localSQLiteDatabaseCorruptException);
                }
            }
        }
    }

    private class SmsPreviewListAdapter extends MiuiLockScreen.PreviewCursorAdpater
    {
        public SmsPreviewListAdapter(Context paramCursor, Cursor arg3)
        {
            super(paramCursor, 100859925, localCursor);
        }

        public void bindView(View paramView, Context paramContext, Cursor paramCursor)
        {
            ListItemViews localListItemViews = (ListItemViews)paramView.getTag();
            String str1 = paramCursor.getString(1);
            long l = paramCursor.getLong(2);
            int i = paramCursor.getInt(3);
            Object localObject = paramCursor.getString(4);
            if (localObject == null)
                localObject = "";
            if ((i > 7) && (!TextUtils.isEmpty((CharSequence)localObject)));
            try
            {
                Integer localInteger = Integer.valueOf(paramCursor.getInt(5));
                if (localInteger != null)
                {
                    String str4 = new EncodedStringValue(localInteger.intValue(), ((String)localObject).getBytes("iso-8859-1")).getString();
                    localObject = str4;
                }
                label114: MiuiLockScreen.PreviewCursorAdpater.ContactInfo localContactInfo = getContact(str1);
                int j;
                label230: TextView localTextView2;
                if ((localContactInfo != null) && (!TextUtils.isEmpty(localContactInfo.name)))
                {
                    localListItemViews.line1View.setText(localContactInfo.name + "    " + getValidatedNumber(str1));
                    ChooseLockSettingsHelper localChooseLockSettingsHelper = new ChooseLockSettingsHelper(paramContext);
                    if ((Settings.System.getInt(paramContext.getContentResolver(), "pref_key_enable_notification_body", 1) != 1) || ((FirewallManager.isAccessControlProtected(paramContext, "com.android.mms")) && (localChooseLockSettingsHelper.isACLockEnabled())) || (localChooseLockSettingsHelper.isPrivacyModeEnabled()))
                        break label342;
                    j = 1;
                    localTextView2 = localListItemViews.textView;
                    if (j == 0)
                        break label348;
                }
                while (true)
                {
                    localTextView2.setText((CharSequence)localObject);
                    localListItemViews.dateView.setText(formatDate(l));
                    return;
                    String str2 = PhoneNumberUtils.parseTelocationString(this.mContext, str1);
                    TextView localTextView1 = localListItemViews.line1View;
                    if (TextUtils.isEmpty(str2));
                    for (String str3 = getValidatedNumber(str1); ; str3 = getValidatedNumber(str1) + "    " + str2)
                    {
                        localTextView1.setText(str3);
                        break;
                    }
                    label342: j = 0;
                    break label230;
                    label348: localObject = "";
                }
            }
            catch (UnsupportedEncodingException localUnsupportedEncodingException)
            {
                break label114;
            }
        }

        public void markRead(Context paramContext, int paramInt)
        {
            Cursor localCursor = (Cursor)getItem(paramInt);
            long l = localCursor.getLong(0);
            int i = localCursor.getInt(3);
            ContentValues localContentValues = new ContentValues();
            if (i < 7)
            {
                localContentValues.put("seen", Integer.valueOf(1));
                ContentResolver localContentResolver2 = paramContext.getContentResolver();
                Uri localUri2 = Telephony.Sms.Inbox.CONTENT_URI;
                String[] arrayOfString2 = new String[1];
                arrayOfString2[0] = String.valueOf(l);
                localContentResolver2.update(localUri2, localContentValues, "_id=?", arrayOfString2);
            }
            while (true)
            {
                return;
                localContentValues.put("seen", Integer.valueOf(1));
                ContentResolver localContentResolver1 = paramContext.getContentResolver();
                Uri localUri1 = Telephony.Mms.Inbox.CONTENT_URI;
                String[] arrayOfString1 = new String[1];
                arrayOfString1[0] = String.valueOf(l);
                localContentResolver1.update(localUri1, localContentValues, "_id=?", arrayOfString1);
            }
        }

        public View newView(Context paramContext, Cursor paramCursor, ViewGroup paramViewGroup)
        {
            View localView = super.newView(paramContext, paramCursor, paramViewGroup);
            ListItemViews localListItemViews = new ListItemViews();
            localListItemViews.line1View = ((TextView)localView.findViewById(101384243));
            localListItemViews.textView = ((TextView)localView.findViewById(101384247));
            localListItemViews.dateView = ((TextView)localView.findViewById(101384245));
            localView.setTag(localListItemViews);
            return localView;
        }

        protected void onContentChanged()
        {
            super.onContentChanged();
            if ((getCount() > 0) && (Settings.System.getInt(this.mContext.getContentResolver(), "pref_key_enable_notification", 1) == 1) && (Settings.System.getInt(this.mContext.getContentResolver(), "pref_key_enable_wake_up_screen", 1) == 1))
                MiuiLockScreen.this.mCallback.pokeWakelock(25000);
        }

        protected void onQueryResultCount(int paramInt)
        {
            if (paramInt > 0)
                MiuiLockScreen.this.mSelector.setSliderText(2, String.valueOf(paramInt));
            while (true)
            {
                return;
                if (MiuiLockScreen.this.mSelector.getSliderTextVisibility(2) == 0)
                {
                    MiuiLockScreen.this.mSelector.setSliderText(2, null);
                    Intent localIntent = new Intent("android.provider.Telephony.DISMISS_NEW_MESSAGE_NOTIFICATION");
                    this.mContext.sendBroadcast(localIntent);
                }
            }
        }

        public final class ListItemViews
        {
            TextView dateView;
            TextView line1View;
            TextView textView;

            public ListItemViews()
            {
            }
        }
    }

    private class CallLogPreviewListAdapter extends MiuiLockScreen.PreviewCursorAdpater
    {
        static final int CALLER_NAME_COLUMN_INDEX = 4;
        static final int DATE_COLUMN_INDEX = 2;
        static final int DURATION_COLUMN_INDEX = 3;
        static final int ID_COLUMN_INDEX = 0;
        static final int NUMBER_COLUMN_INDEX = 1;

        public CallLogPreviewListAdapter(Context paramCursor, Cursor arg3)
        {
            super(paramCursor, 100859921, localCursor);
        }

        private String formatRingDuration(Context paramContext, long paramLong)
        {
            long l = 1L + paramLong / 5L;
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = Long.valueOf(l);
            return paramContext.getString(101449791, arrayOfObject);
        }

        public void bindView(View paramView, Context paramContext, Cursor paramCursor)
        {
            ListItemViews localListItemViews = (ListItemViews)paramView.getTag();
            String str1 = paramCursor.getString(1);
            String str2 = paramCursor.getString(4);
            MiuiLockScreen.PreviewCursorAdpater.ContactInfo localContactInfo = getContact(str1);
            if ((localContactInfo != null) && (!TextUtils.isEmpty(localContactInfo.name)))
                str2 = localContactInfo.name;
            String str3 = PhoneNumberUtils.parseTelocationString(this.mContext, str1);
            if (TextUtils.isEmpty(str3))
                str3 = "";
            if (!TextUtils.isEmpty(str2))
            {
                localListItemViews.line1View.setText(str2);
                String str4 = getValidatedNumber(str1);
                TextView localTextView = localListItemViews.numberView;
                Object[] arrayOfObject = new Object[2];
                arrayOfObject[0] = str4;
                arrayOfObject[1] = str3;
                localTextView.setText(String.format("%s    %s", arrayOfObject));
            }
            while (true)
            {
                long l1 = paramCursor.getLong(2);
                localListItemViews.dateView.setText(formatDate(l1));
                long l2 = paramCursor.getLong(3);
                localListItemViews.durationView.setText(formatRingDuration(this.mContext, l2));
                return;
                localListItemViews.line1View.setText(getValidatedNumber(str1));
                localListItemViews.numberView.setText(str3);
            }
        }

        public void markRead(Context paramContext, int paramInt)
        {
            long l = ((Cursor)getItem(paramInt)).getLong(0);
            ContentValues localContentValues = new ContentValues();
            localContentValues.put("new", Integer.valueOf(0));
            ContentResolver localContentResolver = paramContext.getContentResolver();
            Uri localUri = CallLog.Calls.CONTENT_URI;
            String[] arrayOfString = new String[1];
            arrayOfString[0] = String.valueOf(l);
            localContentResolver.update(localUri, localContentValues, "_id=?", arrayOfString);
        }

        public View newView(Context paramContext, Cursor paramCursor, ViewGroup paramViewGroup)
        {
            View localView = super.newView(paramContext, paramCursor, paramViewGroup);
            ListItemViews localListItemViews = new ListItemViews();
            localListItemViews.line1View = ((TextView)localView.findViewById(101384243));
            localListItemViews.numberView = ((TextView)localView.findViewById(101384244));
            localListItemViews.dateView = ((TextView)localView.findViewById(101384245));
            localListItemViews.durationView = ((TextView)localView.findViewById(101384246));
            localView.setTag(localListItemViews);
            return localView;
        }

        protected void onQueryResultCount(int paramInt)
        {
            if (paramInt > 0)
                MiuiLockScreen.this.mSelector.setSliderText(0, String.valueOf(paramInt));
            while (true)
            {
                return;
                if (MiuiLockScreen.this.mSelector.getSliderTextVisibility(0) == 0)
                {
                    MiuiLockScreen.this.mSelector.setSliderText(0, null);
                    try
                    {
                        NotificationManager.getService().cancelNotificationWithTag("com.android.phone", null, 1);
                    }
                    catch (RemoteException localRemoteException)
                    {
                        localRemoteException.printStackTrace();
                    }
                }
            }
        }

        public final class ListItemViews
        {
            TextView dateView;
            TextView durationView;
            TextView line1View;
            TextView numberView;

            public ListItemViews()
            {
            }
        }
    }

    private abstract class PreviewCursorAdpater extends ResourceCursorAdapter
    {
        private HashMap<String, SoftReference<ContactInfo>> mContacts = new HashMap();
        private boolean mLoading = true;
        private boolean mUseDefaultCount = true;

        public PreviewCursorAdpater(Context paramInt, int paramCursor, Cursor arg4)
        {
            super(paramCursor, localCursor);
        }

        public void bindView(View paramView, Context paramContext, Cursor paramCursor)
        {
        }

        public void enableDefaultCount(boolean paramBoolean)
        {
            this.mUseDefaultCount = paramBoolean;
        }

        protected String formatDate(long paramLong)
        {
            String str1;
            String str2;
            if (DateFormat.is24HourFormat(this.mContext))
            {
                str1 = "MMM d, kk:mm";
                str2 = DateFormat.format(str1, paramLong).toString();
                if (!str2.startsWith(DateFormat.format("MMM d", Calendar.getInstance().getTimeInMillis()).toString()))
                    break label68;
            }
            label68: for (String str3 = str2.split(",")[1].trim(); ; str3 = str2.split(",")[0] + this.mContext.getString(101449792))
            {
                return str3;
                str1 = "MMM d,aa h:mm";
                break;
            }
        }

        // ERROR //
        protected ContactInfo getContact(String paramString)
        {
            // Byte code:
            //     0: aconst_null
            //     1: astore_2
            //     2: aload_0
            //     3: getfield 32	com/android/internal/policy/impl/MiuiLockScreen$PreviewCursorAdpater:mContacts	Ljava/util/HashMap;
            //     6: aload_1
            //     7: invokevirtual 116	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
            //     10: ifeq +31 -> 41
            //     13: aload_0
            //     14: getfield 32	com/android/internal/policy/impl/MiuiLockScreen$PreviewCursorAdpater:mContacts	Ljava/util/HashMap;
            //     17: aload_1
            //     18: invokevirtual 120	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
            //     21: checkcast 122	java/lang/ref/SoftReference
            //     24: invokevirtual 125	java/lang/ref/SoftReference:get	()Ljava/lang/Object;
            //     27: checkcast 9	com/android/internal/policy/impl/MiuiLockScreen$PreviewCursorAdpater$ContactInfo
            //     30: astore_2
            //     31: aload_2
            //     32: ifnull +9 -> 41
            //     35: aload_2
            //     36: astore 7
            //     38: aload 7
            //     40: areturn
            //     41: aconst_null
            //     42: astore_3
            //     43: getstatic 131	android/provider/ContactsContract$PhoneLookup:CONTENT_FILTER_URI	Landroid/net/Uri;
            //     46: aload_1
            //     47: invokestatic 137	android/net/Uri:encode	(Ljava/lang/String;)Ljava/lang/String;
            //     50: invokestatic 141	android/net/Uri:withAppendedPath	(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
            //     53: astore 8
            //     55: aload_0
            //     56: getfield 46	com/android/internal/policy/impl/MiuiLockScreen$PreviewCursorAdpater:mContext	Landroid/content/Context;
            //     59: invokevirtual 145	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
            //     62: astore 9
            //     64: iconst_1
            //     65: anewarray 78	java/lang/String
            //     68: astore 10
            //     70: aload 10
            //     72: iconst_0
            //     73: ldc 147
            //     75: aastore
            //     76: aload 9
            //     78: aload 8
            //     80: aload 10
            //     82: aconst_null
            //     83: aconst_null
            //     84: aconst_null
            //     85: invokevirtual 153	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
            //     88: astore_3
            //     89: aload_3
            //     90: ifnull +74 -> 164
            //     93: aload_3
            //     94: invokeinterface 159 1 0
            //     99: ifeq +65 -> 164
            //     102: new 9	com/android/internal/policy/impl/MiuiLockScreen$PreviewCursorAdpater$ContactInfo
            //     105: dup
            //     106: aload_0
            //     107: invokespecial 162	com/android/internal/policy/impl/MiuiLockScreen$PreviewCursorAdpater$ContactInfo:<init>	(Lcom/android/internal/policy/impl/MiuiLockScreen$PreviewCursorAdpater;)V
            //     110: astore 17
            //     112: aload 17
            //     114: aload_3
            //     115: iconst_0
            //     116: invokeinterface 163 2 0
            //     121: putfield 167	com/android/internal/policy/impl/MiuiLockScreen$PreviewCursorAdpater$ContactInfo:name	Ljava/lang/String;
            //     124: aload 17
            //     126: astore_2
            //     127: aload_2
            //     128: ifnull +20 -> 148
            //     131: aload_0
            //     132: getfield 32	com/android/internal/policy/impl/MiuiLockScreen$PreviewCursorAdpater:mContacts	Ljava/util/HashMap;
            //     135: aload_1
            //     136: new 122	java/lang/ref/SoftReference
            //     139: dup
            //     140: aload_2
            //     141: invokespecial 170	java/lang/ref/SoftReference:<init>	(Ljava/lang/Object;)V
            //     144: invokevirtual 174	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
            //     147: pop
            //     148: aload_3
            //     149: ifnull +9 -> 158
            //     152: aload_3
            //     153: invokeinterface 177 1 0
            //     158: aload_2
            //     159: astore 7
            //     161: goto -123 -> 38
            //     164: aload_3
            //     165: ifnull +9 -> 174
            //     168: aload_3
            //     169: invokeinterface 177 1 0
            //     174: aload_1
            //     175: invokestatic 183	miui/telephony/PhoneNumberUtils$PhoneNumber:parse	(Ljava/lang/CharSequence;)Lmiui/telephony/PhoneNumberUtils$PhoneNumber;
            //     178: astore 11
            //     180: aload_1
            //     181: astore 12
            //     183: aload 11
            //     185: ifnull +21 -> 206
            //     188: aload 11
            //     190: invokevirtual 186	miui/telephony/PhoneNumberUtils$PhoneNumber:getEffectiveNumber	()Ljava/lang/String;
            //     193: invokestatic 192	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
            //     196: ifne +10 -> 206
            //     199: aload 11
            //     201: invokevirtual 186	miui/telephony/PhoneNumberUtils$PhoneNumber:getEffectiveNumber	()Ljava/lang/String;
            //     204: astore 12
            //     206: aload_0
            //     207: getfield 46	com/android/internal/policy/impl/MiuiLockScreen$PreviewCursorAdpater:mContext	Landroid/content/Context;
            //     210: invokevirtual 145	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
            //     213: astore 13
            //     215: ldc 194
            //     217: invokestatic 197	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
            //     220: astore 14
            //     222: iconst_1
            //     223: anewarray 78	java/lang/String
            //     226: astore 15
            //     228: aload 15
            //     230: iconst_0
            //     231: ldc 198
            //     233: aastore
            //     234: iconst_1
            //     235: anewarray 78	java/lang/String
            //     238: astore 16
            //     240: aload 16
            //     242: iconst_0
            //     243: aload 12
            //     245: aastore
            //     246: aload 13
            //     248: aload 14
            //     250: aload 15
            //     252: ldc 200
            //     254: aload 16
            //     256: aconst_null
            //     257: invokevirtual 153	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
            //     260: astore_3
            //     261: aload_3
            //     262: ifnull -135 -> 127
            //     265: aload_3
            //     266: invokeinterface 159 1 0
            //     271: ifeq -144 -> 127
            //     274: new 9	com/android/internal/policy/impl/MiuiLockScreen$PreviewCursorAdpater$ContactInfo
            //     277: dup
            //     278: aload_0
            //     279: invokespecial 162	com/android/internal/policy/impl/MiuiLockScreen$PreviewCursorAdpater$ContactInfo:<init>	(Lcom/android/internal/policy/impl/MiuiLockScreen$PreviewCursorAdpater;)V
            //     282: astore 17
            //     284: aload 17
            //     286: aload_3
            //     287: iconst_0
            //     288: invokeinterface 163 2 0
            //     293: putfield 167	com/android/internal/policy/impl/MiuiLockScreen$PreviewCursorAdpater$ContactInfo:name	Ljava/lang/String;
            //     296: aload 17
            //     298: astore_2
            //     299: goto -172 -> 127
            //     302: astore 5
            //     304: ldc 202
            //     306: aload 5
            //     308: invokevirtual 203	java/lang/Exception:toString	()Ljava/lang/String;
            //     311: invokestatic 209	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
            //     314: pop
            //     315: aload_3
            //     316: ifnull -158 -> 158
            //     319: aload_3
            //     320: invokeinterface 177 1 0
            //     325: goto -167 -> 158
            //     328: astore 4
            //     330: aload_3
            //     331: ifnull +9 -> 340
            //     334: aload_3
            //     335: invokeinterface 177 1 0
            //     340: aload 4
            //     342: athrow
            //     343: astore 4
            //     345: aload 17
            //     347: pop
            //     348: goto -18 -> 330
            //     351: astore 5
            //     353: aload 17
            //     355: astore_2
            //     356: goto -52 -> 304
            //
            // Exception table:
            //     from	to	target	type
            //     43	112	302	java/lang/Exception
            //     131	148	302	java/lang/Exception
            //     168	284	302	java/lang/Exception
            //     43	112	328	finally
            //     131	148	328	finally
            //     168	284	328	finally
            //     304	315	328	finally
            //     112	124	343	finally
            //     284	296	343	finally
            //     112	124	351	java/lang/Exception
            //     284	296	351	java/lang/Exception
        }

        public int getCount()
        {
            int i;
            if (this.mUseDefaultCount)
                i = super.getCount();
            while (true)
            {
                return i;
                Cursor localCursor = getCursor();
                if (localCursor == null)
                {
                    i = 0;
                }
                else
                {
                    i = localCursor.getCount();
                    if (i > 2)
                        i = 2;
                }
            }
        }

        String getValidatedNumber(String paramString)
        {
            if (paramString.equals("-1"))
                paramString = this.mContext.getString(101449788);
            while (true)
            {
                return paramString;
                if (paramString.equals("-2"))
                    paramString = this.mContext.getString(101449789);
                else if (paramString.equals("-3"))
                    paramString = this.mContext.getString(101449790);
            }
        }

        public boolean isEmpty()
        {
            if (this.mLoading);
            for (boolean bool = false; ; bool = super.isEmpty())
                return bool;
        }

        public abstract void markRead(Context paramContext, int paramInt);

        protected void onContentChanged()
        {
            super.onContentChanged();
            onQueryResultCount(super.getCount());
            notifyDataSetChanged();
        }

        protected abstract void onQueryResultCount(int paramInt);

        void setLoading(boolean paramBoolean)
        {
            this.mLoading = paramBoolean;
        }

        protected class ContactInfo
        {
            public String name = "";

            protected ContactInfo()
            {
            }
        }
    }

    static enum Status
    {
        private final boolean mShowStatusLines;

        static
        {
            NetworkLocked = new Status("NetworkLocked", 1, true);
            SimMissing = new Status("SimMissing", 2, false);
            SimMissingLocked = new Status("SimMissingLocked", 3, false);
            SimPukLocked = new Status("SimPukLocked", 4, false);
            SimLocked = new Status("SimLocked", 5, true);
            Status[] arrayOfStatus = new Status[6];
            arrayOfStatus[0] = Normal;
            arrayOfStatus[1] = NetworkLocked;
            arrayOfStatus[2] = SimMissing;
            arrayOfStatus[3] = SimMissingLocked;
            arrayOfStatus[4] = SimPukLocked;
            arrayOfStatus[5] = SimLocked;
        }

        private Status(boolean paramBoolean)
        {
            this.mShowStatusLines = paramBoolean;
        }

        public boolean showStatusLines()
        {
            return this.mShowStatusLines;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.MiuiLockScreen
 * JD-Core Version:        0.6.2
 */