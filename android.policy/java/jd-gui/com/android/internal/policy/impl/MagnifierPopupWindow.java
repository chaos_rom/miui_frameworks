package com.android.internal.policy.impl;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Trace;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;

public class MagnifierPopupWindow extends PopupWindow
{
    public static final float DEFAULT_TIMES = 1.2F;
    public static final String LOG_TAG = "MagnifierPopupWindow";
    private static final int MSG_HIDE_MAGNIFIER = 3;
    private static final int MSG_REFRESH_MAGNIFIER = 4;
    private static final int MSG_SHOW_MAGNIFIER = 2;
    private static final int MSG_UPDATE_CACHE = 1;
    private static final int REFRESH_DELAY = 1000;
    private static final int UPDATE_CACHE_DELAY = 80;
    private AnimatorSet mAnimationEnter;
    private AnimatorSet mAnimationExit;
    private Display mDisplay;
    private DisplayMetrics mDisplayMetrics = new DisplayMetrics();
    private int mFingerOffset;
    Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            if (paramAnonymousMessage.what == 2)
                MagnifierPopupWindow.this.showMagnifier();
            while (true)
            {
                return;
                if (paramAnonymousMessage.what == 3)
                    MagnifierPopupWindow.this.mMagnifierView.hide();
                else if (paramAnonymousMessage.what == 4)
                    MagnifierPopupWindow.this.refreshMagnifier();
            }
        }
    };
    private int mLocationX;
    private int mLocationY;
    private MagnifierView mMagnifierView;
    private int mMaxLayer;
    private IBinder mToken;
    private int mX;
    private int mY;

    public MagnifierPopupWindow(Context paramContext, int paramInt)
    {
        this(paramContext, paramInt, 0);
    }

    public MagnifierPopupWindow(Context paramContext, int paramInt1, int paramInt2)
    {
        super(paramContext);
        this.mMaxLayer = paramInt1;
        this.mToken = new Binder();
        this.mDisplay = ((WindowManager)paramContext.getSystemService("window")).getDefaultDisplay();
        this.mDisplay.getMetrics(this.mDisplayMetrics);
        this.mMagnifierView = new MagnifierView(paramContext, 1.2F);
        this.mMagnifierView.setClickable(false);
        setContentView(this.mMagnifierView);
        setFocusable(false);
        setTouchable(false);
        setClippingEnabled(false);
        setInputMethodMode(2);
        setBackgroundDrawable(new ColorDrawable(0));
        this.mAnimationEnter = new AnimatorSet();
        this.mAnimationExit = new AnimatorSet();
        this.mAnimationExit.addListener(new Animator.AnimatorListener()
        {
            public void onAnimationCancel(Animator paramAnonymousAnimator)
            {
            }

            public void onAnimationEnd(Animator paramAnonymousAnimator)
            {
                MagnifierPopupWindow.this.dismiss();
            }

            public void onAnimationRepeat(Animator paramAnonymousAnimator)
            {
            }

            public void onAnimationStart(Animator paramAnonymousAnimator)
            {
            }
        });
        long l = paramContext.getResources().getInteger(17694720);
        this.mAnimationExit.setDuration(l);
        this.mAnimationEnter.setDuration(l);
        setWindowLayoutType(2016);
        setLayoutInScreenEnabled(true);
    }

    private void refreshMagnifier()
    {
        this.mMagnifierView.updateMagnifier(new Rect(this.mX, this.mY, this.mX, this.mY));
    }

    private void showMagnifier()
    {
        showMagnifier(new Rect(this.mX, this.mY, this.mX, this.mY));
    }

    private void showMagnifier(Rect paramRect)
    {
        this.mMagnifierView.updateMagnifier(paramRect);
        if (!isShowing())
        {
            this.mMagnifierView.measure(0, 0);
            this.mMagnifierView.updateCache();
            setWidth(-1);
            setHeight(-1);
            showAtLocation(this.mToken, 51, 0, 0);
            this.mMagnifierView.setPivotX(this.mX);
            this.mMagnifierView.setPivotY(this.mY);
            MagnifierView localMagnifierView1 = this.mMagnifierView;
            float[] arrayOfFloat1 = new float[2];
            arrayOfFloat1[0] = 0.0F;
            arrayOfFloat1[1] = 1.0F;
            ObjectAnimator localObjectAnimator1 = ObjectAnimator.ofFloat(localMagnifierView1, "scaleX", arrayOfFloat1);
            MagnifierView localMagnifierView2 = this.mMagnifierView;
            float[] arrayOfFloat2 = new float[2];
            arrayOfFloat2[0] = 0.0F;
            arrayOfFloat2[1] = 1.0F;
            ObjectAnimator localObjectAnimator2 = ObjectAnimator.ofFloat(localMagnifierView2, "scaleY", arrayOfFloat2);
            AnimatorSet localAnimatorSet = this.mAnimationEnter;
            Animator[] arrayOfAnimator = new Animator[2];
            arrayOfAnimator[0] = localObjectAnimator1;
            arrayOfAnimator[1] = localObjectAnimator2;
            localAnimatorSet.playTogether(arrayOfAnimator);
            this.mAnimationEnter.start();
        }
    }

    public void hide()
    {
        this.mHandler.removeMessages(2);
        this.mHandler.sendEmptyMessage(3);
    }

    public void showMagnifier(int paramInt1, int paramInt2)
    {
        this.mHandler.removeMessages(2);
        if ((Math.abs(paramInt1 - this.mX) >= 1) || (Math.abs(paramInt2 - this.mY) >= 1))
        {
            this.mX = paramInt1;
            this.mY = paramInt2;
            this.mHandler.sendEmptyMessage(2);
        }
    }

    public void updateCache()
    {
        this.mMagnifierView.updateCache();
    }

    private class MagnifierView extends View
    {
        private Bitmap mBitmap = null;
        private Paint mBitmapPaint = new Paint();
        private Matrix mDisplayMatrix = new Matrix();
        private Rect mDstRect = new Rect();
        private Drawable mFilterDrawable;
        private Drawable mFrontDrawable;
        private int mLastHeight;
        private int mLastWidth;
        private int mMagHeight;
        private int mMagWidth;
        private int mSrcHeight;
        private Rect mSrcRect = new Rect();
        private int mSrcWidth;
        private Handler mUpdateHandler;
        private HandlerThread mUpdateThread = new HandlerThread("UpdateMagnifier");

        public MagnifierView(Context paramFloat, float arg3)
        {
            super();
            this.mUpdateThread.start();
            this.mUpdateHandler = new Handler(this.mUpdateThread.getLooper())
            {
                public void handleMessage(Message paramAnonymousMessage)
                {
                    if (paramAnonymousMessage.what == 1)
                    {
                        MagnifierPopupWindow.MagnifierView.this.takeScreenshot();
                        MagnifierPopupWindow.MagnifierView.this.refresh();
                    }
                }
            };
            this.mBitmapPaint.setAntiAlias(true);
            this.mBitmapPaint.setFilterBitmap(true);
            this.mBitmapPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.MULTIPLY));
            this.mFilterDrawable = paramFloat.getResources().getDrawable(100794451);
            this.mFrontDrawable = paramFloat.getResources().getDrawable(100794452);
            this.mMagWidth = this.mFilterDrawable.getIntrinsicWidth();
            this.mMagHeight = this.mFilterDrawable.getIntrinsicHeight();
            Object localObject;
            this.mSrcWidth = Math.round(this.mMagWidth / localObject);
            this.mSrcHeight = Math.round(this.mMagHeight / localObject);
            this.mDstRect.set(0, 0, this.mMagWidth, this.mMagHeight);
            MagnifierPopupWindow.access$102(MagnifierPopupWindow.this, (int)paramFloat.getResources().getDimension(101318681));
        }

        private float getDegreesForRotation(int paramInt)
        {
            float f;
            switch (paramInt)
            {
            default:
                f = 0.0F;
            case 1:
            case 2:
            case 3:
            }
            while (true)
            {
                return f;
                f = 270.0F;
                continue;
                f = 180.0F;
                continue;
                f = 90.0F;
            }
        }

        private void refresh()
        {
            if (MagnifierPopupWindow.this.isShowing())
            {
                MagnifierPopupWindow.this.mHandler.sendEmptyMessage(4);
                this.mUpdateHandler.removeMessages(1);
                this.mUpdateHandler.sendEmptyMessageDelayed(1, 1000L);
            }
        }

        public void hide()
        {
            this.mUpdateHandler.removeMessages(1);
            if (MagnifierPopupWindow.this.isShowing())
            {
                MagnifierPopupWindow.this.mMagnifierView.setPivotX(MagnifierPopupWindow.this.mX);
                MagnifierPopupWindow.this.mMagnifierView.setPivotY(MagnifierPopupWindow.this.mY);
                MagnifierView localMagnifierView1 = MagnifierPopupWindow.this.mMagnifierView;
                float[] arrayOfFloat1 = new float[2];
                arrayOfFloat1[0] = 1.0F;
                arrayOfFloat1[1] = 0.0F;
                ObjectAnimator localObjectAnimator1 = ObjectAnimator.ofFloat(localMagnifierView1, "scaleX", arrayOfFloat1);
                MagnifierView localMagnifierView2 = MagnifierPopupWindow.this.mMagnifierView;
                float[] arrayOfFloat2 = new float[2];
                arrayOfFloat2[0] = 1.0F;
                arrayOfFloat2[1] = 0.0F;
                ObjectAnimator localObjectAnimator2 = ObjectAnimator.ofFloat(localMagnifierView2, "scaleY", arrayOfFloat2);
                AnimatorSet localAnimatorSet = MagnifierPopupWindow.this.mAnimationExit;
                Animator[] arrayOfAnimator = new Animator[2];
                arrayOfAnimator[0] = localObjectAnimator1;
                arrayOfAnimator[1] = localObjectAnimator2;
                localAnimatorSet.playTogether(arrayOfAnimator);
                MagnifierPopupWindow.this.mAnimationExit.start();
            }
        }

        protected void onDraw(Canvas paramCanvas)
        {
            if ((!this.mSrcRect.isEmpty()) && (this.mBitmap != null))
            {
                paramCanvas.save();
                paramCanvas.translate(MagnifierPopupWindow.this.mLocationX, MagnifierPopupWindow.this.mLocationY);
                this.mFilterDrawable.setBounds(this.mDstRect);
                this.mFilterDrawable.draw(paramCanvas);
                paramCanvas.drawBitmap(this.mBitmap, this.mSrcRect, this.mDstRect, this.mBitmapPaint);
                this.mFrontDrawable.setBounds(this.mDstRect);
                this.mFrontDrawable.draw(paramCanvas);
                paramCanvas.restore();
            }
        }

        protected void onMeasure(int paramInt1, int paramInt2)
        {
            MagnifierPopupWindow.this.mDisplay.getRealMetrics(MagnifierPopupWindow.this.mDisplayMetrics);
            setMeasuredDimension(MagnifierPopupWindow.this.mDisplayMetrics.widthPixels, MagnifierPopupWindow.this.mDisplayMetrics.heightPixels);
        }

        void takeScreenshot()
        {
            Trace.traceBegin(2L, "Magnifier");
            MagnifierPopupWindow.this.mDisplay.getRealMetrics(MagnifierPopupWindow.this.mDisplayMetrics);
            float[] arrayOfFloat = new float[2];
            arrayOfFloat[0] = MagnifierPopupWindow.this.mDisplayMetrics.widthPixels;
            arrayOfFloat[1] = MagnifierPopupWindow.this.mDisplayMetrics.heightPixels;
            float f = getDegreesForRotation(MagnifierPopupWindow.this.mDisplay.getRotation());
            int i;
            Object localObject;
            Bitmap localBitmap;
            if (f > 0.0F)
            {
                i = 1;
                if (i != 0)
                {
                    this.mDisplayMatrix.reset();
                    this.mDisplayMatrix.preRotate(-f);
                    this.mDisplayMatrix.mapPoints(arrayOfFloat);
                    arrayOfFloat[0] = Math.abs(arrayOfFloat[0]);
                    arrayOfFloat[1] = Math.abs(arrayOfFloat[1]);
                }
                localObject = this.mBitmap;
                if ((MagnifierPopupWindow.this.mDisplayMetrics.widthPixels != this.mLastWidth) || (MagnifierPopupWindow.this.mDisplayMetrics.heightPixels != this.mLastHeight))
                {
                    localObject = Bitmap.createBitmap(MagnifierPopupWindow.this.mDisplayMetrics.widthPixels, MagnifierPopupWindow.this.mDisplayMetrics.heightPixels, Bitmap.Config.ARGB_8888);
                    this.mLastWidth = MagnifierPopupWindow.this.mDisplayMetrics.widthPixels;
                    this.mLastHeight = MagnifierPopupWindow.this.mDisplayMetrics.heightPixels;
                }
                localBitmap = Surface.screenshot((int)arrayOfFloat[0], (int)arrayOfFloat[1], 0, MagnifierPopupWindow.this.mMaxLayer);
                if (i == 0)
                    break label354;
                Canvas localCanvas = new Canvas((Bitmap)localObject);
                localCanvas.translate(((Bitmap)localObject).getWidth() / 2, ((Bitmap)localObject).getHeight() / 2);
                localCanvas.rotate(f);
                localCanvas.translate(-arrayOfFloat[0] / 2.0F, -arrayOfFloat[1] / 2.0F);
                localCanvas.drawBitmap(localBitmap, 0.0F, 0.0F, null);
                localCanvas.setBitmap(null);
                localBitmap.recycle();
            }
            while (true)
            {
                ((Bitmap)localObject).setHasAlpha(false);
                ((Bitmap)localObject).prepareToDraw();
                this.mBitmap = ((Bitmap)localObject);
                Trace.traceEnd(2L);
                return;
                i = 0;
                break;
                label354: localObject = localBitmap;
            }
        }

        public void updateCache()
        {
            this.mUpdateHandler.removeMessages(1);
            this.mUpdateHandler.sendEmptyMessageDelayed(1, 80L);
        }

        void updateMagnifier(Rect paramRect)
        {
            this.mSrcRect.left = (paramRect.centerX() - this.mSrcWidth / 2);
            this.mSrcRect.top = (paramRect.centerY() - this.mSrcHeight / 2);
            this.mSrcRect.right = (this.mSrcRect.left + this.mSrcWidth);
            this.mSrcRect.bottom = (this.mSrcRect.top + this.mSrcHeight);
            MagnifierPopupWindow.access$902(MagnifierPopupWindow.this, paramRect.centerX() - this.mMagWidth / 2);
            MagnifierPopupWindow.access$1002(MagnifierPopupWindow.this, Math.max(paramRect.centerY() - this.mMagHeight / 2 - MagnifierPopupWindow.this.mFingerOffset, -MagnifierPopupWindow.this.mFingerOffset));
            invalidate();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.MagnifierPopupWindow
 * JD-Core Version:        0.6.2
 */