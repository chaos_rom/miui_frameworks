package com.android.internal.policy.impl;

import android.content.Context;

public abstract interface KeyguardViewProperties
{
    public abstract KeyguardViewBase createKeyguardView(Context paramContext, KeyguardViewCallback paramKeyguardViewCallback, KeyguardUpdateMonitor paramKeyguardUpdateMonitor, KeyguardWindowController paramKeyguardWindowController);

    public abstract boolean isSecure();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.KeyguardViewProperties
 * JD-Core Version:        0.6.2
 */