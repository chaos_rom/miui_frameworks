package com.android.internal.policy.impl;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.provider.Settings.System;
import android.util.DisplayMetrics;

public class RoundedCorners
{
    private static final int[] CORNER_IDS = arrayOfInt;
    private static final int NUM_CORNERS = 4;
    private final Drawable[] mCorners = new Drawable[4];
    private final int mDisplayHeight;
    private final int mDisplayWidth;
    private final boolean mEnabled;
    private final int[] mHeight = new int[4];
    private int mLastBottom;
    private int mLastLeft;
    private int mLastRight;
    private int mLastTop;
    private final int mStatusBarHeight;
    private final int[] mWidth = new int[4];

    static
    {
        int[] arrayOfInt = new int[4];
        arrayOfInt[0] = 100794593;
        arrayOfInt[1] = 100794594;
        arrayOfInt[2] = 100794591;
        arrayOfInt[3] = 100794592;
    }

    public RoundedCorners(Context paramContext)
    {
        Resources localResources = paramContext.getResources();
        this.mDisplayWidth = localResources.getDisplayMetrics().widthPixels;
        this.mDisplayHeight = localResources.getDisplayMetrics().heightPixels;
        this.mStatusBarHeight = localResources.getDimensionPixelSize(101318656);
        boolean bool;
        if (Settings.System.getInt(paramContext.getContentResolver(), "show_rounded_corners", localResources.getInteger(101187595)) != 0)
        {
            bool = true;
            this.mEnabled = bool;
            if (this.mEnabled)
                break label101;
        }
        while (true)
        {
            return;
            bool = false;
            break;
            label101: for (int i = 0; i < 4; i++)
            {
                this.mCorners[i] = localResources.getDrawable(CORNER_IDS[i]);
                this.mWidth[i] = this.mCorners[i].getMinimumWidth();
                this.mHeight[i] = this.mCorners[i].getMinimumHeight();
            }
        }
    }

    private boolean matchLandscape(int paramInt1, int paramInt2)
    {
        if ((paramInt1 == this.mDisplayWidth) && ((paramInt2 == this.mDisplayHeight) || (paramInt2 == this.mDisplayHeight - this.mStatusBarHeight)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean matchPortrait(int paramInt1, int paramInt2)
    {
        if ((paramInt1 == this.mDisplayHeight) && ((paramInt2 == this.mDisplayWidth) || (paramInt2 == this.mDisplayWidth - this.mStatusBarHeight)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void draw(Canvas paramCanvas, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        int i = paramInt3 - paramInt1;
        int j = paramInt4 - paramInt2;
        if ((!this.mEnabled) || ((!matchLandscape(i, j)) && (!matchPortrait(i, j))));
        while (true)
        {
            return;
            if ((paramInt1 != this.mLastLeft) || (paramInt2 != this.mLastTop) || (paramInt3 != this.mLastRight) || (paramInt4 != this.mLastBottom))
            {
                this.mLastLeft = paramInt1;
                this.mLastTop = paramInt2;
                this.mLastRight = paramInt3;
                this.mLastBottom = paramInt4;
                this.mCorners[0].setBounds(paramInt1, paramInt2, paramInt1 + this.mWidth[0], paramInt2 + this.mHeight[0]);
                this.mCorners[1].setBounds(paramInt3 - this.mWidth[1], paramInt2, paramInt3, paramInt2 + this.mHeight[1]);
                this.mCorners[2].setBounds(paramInt1, paramInt4 - this.mHeight[2], paramInt1 + this.mWidth[2], paramInt4);
                this.mCorners[3].setBounds(paramInt3 - this.mWidth[3], paramInt4 - this.mHeight[3], paramInt3, paramInt4);
            }
            for (int k = 0; k < 4; k++)
                this.mCorners[k].draw(paramCanvas);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.RoundedCorners
 * JD-Core Version:        0.6.2
 */