package com.android.internal.policy.impl;

import android.app.ActivityManager;
import android.app.ActivityManagerNative;
import android.app.IActivityManager;
import android.app.SearchManager;
import android.app.admin.DevicePolicyManager;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.media.AudioManager;
import android.os.RemoteException;
import android.os.Vibrator;
import android.util.Log;
import android.util.Slog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import com.android.internal.telephony.IccCard.State;
import com.android.internal.widget.LockPatternUtils;
import com.android.internal.widget.SlidingTab;
import com.android.internal.widget.SlidingTab.OnTriggerListener;
import com.android.internal.widget.WaveView;
import com.android.internal.widget.WaveView.OnTriggerListener;
import com.android.internal.widget.multiwaveview.GlowPadView;
import com.android.internal.widget.multiwaveview.GlowPadView.OnTriggerListener;
import java.io.File;

class LockScreen extends LinearLayout
    implements KeyguardScreen
{
    private static final String ASSIST_ICON_METADATA_NAME = "com.android.systemui.action_assist_icon";
    private static final boolean DBG = false;
    private static final String ENABLE_MENU_KEY_FILE = "/data/local/enable_menu_key";
    private static final int ON_RESUME_PING_DELAY = 500;
    private static final int STAY_ON_WHILE_GRABBED_TIMEOUT = 30000;
    private static final String TAG = "LockScreen";
    private static final int WAIT_FOR_ANIMATION_TIMEOUT;
    private AudioManager mAudioManager;
    private KeyguardScreenCallback mCallback;
    private boolean mCameraDisabled;
    private int mCreationOrientation;
    private boolean mEnableMenuKeyInLockScreen;
    private boolean mEnableRingSilenceFallback = false;
    private final boolean mHasVibrator;
    KeyguardUpdateMonitor.InfoCallbackImpl mInfoCallback = new KeyguardUpdateMonitor.InfoCallbackImpl()
    {
        public void onDevicePolicyManagerStateChanged()
        {
            LockScreen.this.updateTargets();
        }

        public void onRingerModeChanged(int paramAnonymousInt)
        {
            if (2 != paramAnonymousInt);
            for (boolean bool = true; ; bool = false)
            {
                if (bool != LockScreen.this.mSilentMode)
                {
                    LockScreen.access$002(LockScreen.this, bool);
                    LockScreen.this.mUnlockWidgetMethods.updateResources();
                }
                return;
            }
        }
    };
    private LockPatternUtils mLockPatternUtils;
    private final Runnable mOnResumePing = new Runnable()
    {
        public void run()
        {
            LockScreen.this.mUnlockWidgetMethods.ping();
        }
    };
    private boolean mSearchDisabled;
    private boolean mSilentMode;
    KeyguardUpdateMonitor.SimStateCallback mSimStateCallback = new KeyguardUpdateMonitor.SimStateCallback()
    {
        public void onSimStateChanged(IccCard.State paramAnonymousState)
        {
            LockScreen.this.updateTargets();
        }
    };
    private KeyguardStatusViewManager mStatusViewManager;
    private View mUnlockWidget;
    private UnlockWidgetCommonMethods mUnlockWidgetMethods;
    private KeyguardUpdateMonitor mUpdateMonitor;

    LockScreen(Context paramContext, Configuration paramConfiguration, LockPatternUtils paramLockPatternUtils, KeyguardUpdateMonitor paramKeyguardUpdateMonitor, KeyguardScreenCallback paramKeyguardScreenCallback)
    {
        super(paramContext);
        this.mLockPatternUtils = paramLockPatternUtils;
        this.mUpdateMonitor = paramKeyguardUpdateMonitor;
        this.mCallback = paramKeyguardScreenCallback;
        this.mEnableMenuKeyInLockScreen = shouldEnableMenuKey();
        this.mCreationOrientation = paramConfiguration.orientation;
        LayoutInflater localLayoutInflater = LayoutInflater.from(paramContext);
        Vibrator localVibrator;
        if (this.mCreationOrientation != 2)
        {
            localLayoutInflater.inflate(17367128, this, true);
            this.mStatusViewManager = new KeyguardStatusViewManager(this, this.mUpdateMonitor, this.mLockPatternUtils, this.mCallback, false);
            setFocusable(true);
            setFocusableInTouchMode(true);
            setDescendantFocusability(393216);
            localVibrator = (Vibrator)paramContext.getSystemService("vibrator");
            if (localVibrator != null)
                break label229;
        }
        while (true)
        {
            this.mHasVibrator = bool;
            this.mAudioManager = ((AudioManager)this.mContext.getSystemService("audio"));
            this.mSilentMode = isSilentMode();
            this.mUnlockWidget = findViewById(16908994);
            this.mUnlockWidgetMethods = createUnlockMethods(this.mUnlockWidget);
            return;
            localLayoutInflater.inflate(17367129, this, true);
            break;
            label229: bool = localVibrator.hasVibrator();
        }
    }

    private UnlockWidgetCommonMethods createUnlockMethods(View paramView)
    {
        Object localObject;
        if ((paramView instanceof SlidingTab))
        {
            SlidingTab localSlidingTab = (SlidingTab)paramView;
            localSlidingTab.setHoldAfterTrigger(true, false);
            localSlidingTab.setLeftHintText(17040167);
            localSlidingTab.setLeftTabResources(17302233, 17302444, 17302413, 17302432);
            localObject = new SlidingTabMethods(localSlidingTab);
            localSlidingTab.setOnTriggerListener((SlidingTab.OnTriggerListener)localObject);
        }
        while (true)
        {
            return localObject;
            if ((paramView instanceof WaveView))
            {
                WaveView localWaveView = (WaveView)paramView;
                WaveViewMethods localWaveViewMethods = new WaveViewMethods(localWaveView);
                localWaveView.setOnTriggerListener(localWaveViewMethods);
                localObject = localWaveViewMethods;
            }
            else
            {
                if (!(paramView instanceof GlowPadView))
                    break;
                GlowPadView localGlowPadView = (GlowPadView)paramView;
                GlowPadViewMethods localGlowPadViewMethods = new GlowPadViewMethods(localGlowPadView);
                localGlowPadView.setOnTriggerListener(localGlowPadViewMethods);
                localObject = localGlowPadViewMethods;
            }
        }
        throw new IllegalStateException("Unrecognized unlock widget: " + paramView);
    }

    private boolean isSilentMode()
    {
        if (this.mAudioManager.getRingerMode() != 2);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private void requestUnlockScreen()
    {
        postDelayed(new Runnable()
        {
            public void run()
            {
                LockScreen.this.mCallback.goToUnlockScreen();
            }
        }
        , 0L);
    }

    private boolean shouldEnableMenuKey()
    {
        boolean bool1 = getResources().getBoolean(17891358);
        boolean bool2 = ActivityManager.isRunningInTestHarness();
        boolean bool3 = new File("/data/local/enable_menu_key").exists();
        if ((!bool1) || (bool2) || (bool3));
        for (boolean bool4 = true; ; bool4 = false)
            return bool4;
    }

    private void toggleRingMode()
    {
        int i = 1;
        int j;
        if (!this.mSilentMode)
        {
            j = i;
            this.mSilentMode = j;
            if (!this.mSilentMode)
                break label51;
            AudioManager localAudioManager = this.mAudioManager;
            if (!this.mHasVibrator)
                break label46;
            label35: localAudioManager.setRingerMode(i);
        }
        while (true)
        {
            return;
            j = 0;
            break;
            label46: i = 0;
            break label35;
            label51: this.mAudioManager.setRingerMode(2);
        }
    }

    private void updateTargets()
    {
        boolean bool1 = false;
        boolean bool2 = this.mLockPatternUtils.getDevicePolicyManager().getCameraDisabled(null);
        boolean bool3 = this.mUpdateMonitor.isSimLocked();
        boolean bool4;
        boolean bool5;
        label72: label85: int i;
        if ((this.mUnlockWidgetMethods instanceof GlowPadViewMethods))
        {
            bool4 = ((GlowPadViewMethods)this.mUnlockWidgetMethods).isTargetPresent(17302241);
            if (!(this.mUnlockWidgetMethods instanceof GlowPadViewMethods))
                break label157;
            bool5 = ((GlowPadViewMethods)this.mUnlockWidgetMethods).isTargetPresent(17302168);
            if (!bool2)
                break label163;
            Log.v("LockScreen", "Camera disabled by Device Policy");
            if (SearchManager.getAssistIntent(this.mContext) == null)
                break label179;
            i = 1;
            label98: if ((!bool2) && (!bool3) && (bool4))
                break label185;
        }
        label157: label163: label179: label185: for (boolean bool6 = true; ; bool6 = false)
        {
            this.mCameraDisabled = bool6;
            if ((bool3) || (i == 0) || (!bool5))
                bool1 = true;
            this.mSearchDisabled = bool1;
            this.mUnlockWidgetMethods.updateResources();
            return;
            bool4 = false;
            break;
            bool5 = false;
            break label72;
            if (!bool3)
                break label85;
            Log.v("LockScreen", "Camera disabled by Sim State");
            break label85;
            i = 0;
            break label98;
        }
    }

    public void cleanUp()
    {
        this.mUpdateMonitor.removeCallback(this.mInfoCallback);
        this.mUpdateMonitor.removeCallback(this.mSimStateCallback);
        this.mUnlockWidgetMethods.cleanUp();
        this.mLockPatternUtils = null;
        this.mUpdateMonitor = null;
        this.mCallback = null;
    }

    public boolean needsInput()
    {
        return false;
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        updateConfiguration();
    }

    protected void onConfigurationChanged(Configuration paramConfiguration)
    {
        super.onConfigurationChanged(paramConfiguration);
        updateConfiguration();
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
        if ((paramInt == 82) && (this.mEnableMenuKeyInLockScreen))
            this.mCallback.goToUnlockScreen();
        return false;
    }

    public void onPause()
    {
        this.mUpdateMonitor.removeCallback(this.mInfoCallback);
        this.mUpdateMonitor.removeCallback(this.mSimStateCallback);
        this.mStatusViewManager.onPause();
        this.mUnlockWidgetMethods.reset(false);
    }

    public void onResume()
    {
        this.mUpdateMonitor.registerSimStateCallback(this.mSimStateCallback);
        this.mUpdateMonitor.registerInfoCallback(this.mInfoCallback);
        this.mStatusViewManager.onResume();
        postDelayed(this.mOnResumePing, 500L);
    }

    void updateConfiguration()
    {
        Configuration localConfiguration = getResources().getConfiguration();
        if (localConfiguration.orientation != this.mCreationOrientation)
            this.mCallback.recreateMe(localConfiguration);
    }

    class GlowPadViewMethods
        implements GlowPadView.OnTriggerListener, LockScreen.UnlockWidgetCommonMethods
    {
        private final GlowPadView mGlowPadView;

        GlowPadViewMethods(GlowPadView arg2)
        {
            Object localObject;
            this.mGlowPadView = localObject;
        }

        private void launchActivity(Intent paramIntent)
        {
            paramIntent.setFlags(872415232);
            try
            {
                ActivityManagerNative.getDefault().dismissKeyguardOnNextActivity();
            }
            catch (RemoteException localRemoteException)
            {
                try
                {
                    while (true)
                    {
                        LockScreen.this.mContext.startActivity(paramIntent);
                        return;
                        localRemoteException = localRemoteException;
                        Log.w("LockScreen", "can't dismiss keyguard on launch");
                    }
                }
                catch (ActivityNotFoundException localActivityNotFoundException)
                {
                    while (true)
                        Log.w("LockScreen", "Activity not found for intent + " + paramIntent.getAction());
                }
            }
        }

        public void cleanUp()
        {
            this.mGlowPadView.setOnTriggerListener(null);
        }

        public int getTargetPosition(int paramInt)
        {
            return this.mGlowPadView.getTargetPosition(paramInt);
        }

        public View getView()
        {
            return this.mGlowPadView;
        }

        public boolean isTargetPresent(int paramInt)
        {
            if (this.mGlowPadView.getTargetPosition(paramInt) != -1);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void onFinishFinalAnimation()
        {
        }

        public void onGrabbed(View paramView, int paramInt)
        {
        }

        public void onGrabbedStateChange(View paramView, int paramInt)
        {
            if (paramInt != 0)
                LockScreen.this.mCallback.pokeWakelock();
        }

        public void onReleased(View paramView, int paramInt)
        {
        }

        public void onTrigger(View paramView, int paramInt)
        {
            switch (this.mGlowPadView.getResourceIdForTarget(paramInt))
            {
            default:
            case 17302168:
            case 17302241:
            case 17302266:
            case 17302274:
            case 17302277:
            }
            while (true)
            {
                return;
                Intent localIntent = SearchManager.getAssistIntent(LockScreen.this.mContext);
                if (localIntent != null)
                    launchActivity(localIntent);
                while (true)
                {
                    LockScreen.this.mCallback.pokeWakelock();
                    break;
                    Log.w("LockScreen", "Failed to get intent for assist activity");
                }
                launchActivity(new Intent("android.media.action.STILL_IMAGE_CAMERA"));
                LockScreen.this.mCallback.pokeWakelock();
                continue;
                LockScreen.this.toggleRingMode();
                LockScreen.this.mCallback.pokeWakelock();
                continue;
                LockScreen.this.mCallback.goToUnlockScreen();
            }
        }

        public void ping()
        {
            this.mGlowPadView.ping();
        }

        public void reset(boolean paramBoolean)
        {
            this.mGlowPadView.reset(paramBoolean);
        }

        public void setEnabled(int paramInt, boolean paramBoolean)
        {
            this.mGlowPadView.setEnableTarget(paramInt, paramBoolean);
        }

        public void updateResources()
        {
            boolean bool1 = true;
            int i;
            boolean bool2;
            if ((LockScreen.this.mCameraDisabled) && (LockScreen.this.mEnableRingSilenceFallback))
                if (LockScreen.this.mSilentMode)
                {
                    i = 17235977;
                    if (this.mGlowPadView.getTargetResourceId() != i)
                        this.mGlowPadView.setTargetResources(i);
                    if (!LockScreen.this.mSearchDisabled)
                    {
                        Intent localIntent = SearchManager.getAssistIntent(LockScreen.this.mContext);
                        if (localIntent != null)
                        {
                            ComponentName localComponentName = localIntent.getComponent();
                            if ((!this.mGlowPadView.replaceTargetDrawablesIfPresent(localComponentName, "com.android.systemui.action_assist_icon_google", 17302168)) && (!this.mGlowPadView.replaceTargetDrawablesIfPresent(localComponentName, "com.android.systemui.action_assist_icon", 17302168)))
                                Slog.w("LockScreen", "Couldn't grab icon from package " + localComponentName);
                        }
                    }
                    if (LockScreen.this.mCameraDisabled)
                        break label195;
                    bool2 = bool1;
                    label158: setEnabled(17302241, bool2);
                    if (LockScreen.this.mSearchDisabled)
                        break label200;
                }
            while (true)
            {
                setEnabled(17302168, bool1);
                return;
                i = 17235980;
                break;
                i = 17235982;
                break;
                label195: bool2 = false;
                break label158;
                label200: bool1 = false;
            }
        }
    }

    class WaveViewMethods
        implements WaveView.OnTriggerListener, LockScreen.UnlockWidgetCommonMethods
    {
        private final WaveView mWaveView;

        WaveViewMethods(WaveView arg2)
        {
            Object localObject;
            this.mWaveView = localObject;
        }

        public void cleanUp()
        {
            this.mWaveView.setOnTriggerListener(null);
        }

        public int getTargetPosition(int paramInt)
        {
            return -1;
        }

        public View getView()
        {
            return this.mWaveView;
        }

        public void onGrabbedStateChange(View paramView, int paramInt)
        {
            if (paramInt == 10)
                LockScreen.this.mCallback.pokeWakelock(30000);
        }

        public void onTrigger(View paramView, int paramInt)
        {
            if (paramInt == 10)
                LockScreen.this.requestUnlockScreen();
        }

        public void ping()
        {
        }

        public void reset(boolean paramBoolean)
        {
            this.mWaveView.reset();
        }

        public void setEnabled(int paramInt, boolean paramBoolean)
        {
        }

        public void updateResources()
        {
        }
    }

    class SlidingTabMethods
        implements SlidingTab.OnTriggerListener, LockScreen.UnlockWidgetCommonMethods
    {
        private final SlidingTab mSlidingTab;

        SlidingTabMethods(SlidingTab arg2)
        {
            Object localObject;
            this.mSlidingTab = localObject;
        }

        public void cleanUp()
        {
            this.mSlidingTab.setOnTriggerListener(null);
        }

        public int getTargetPosition(int paramInt)
        {
            return -1;
        }

        public View getView()
        {
            return this.mSlidingTab;
        }

        public void onGrabbedStateChange(View paramView, int paramInt)
        {
            SlidingTab localSlidingTab;
            if (paramInt == 2)
            {
                LockScreen.access$002(LockScreen.this, LockScreen.this.isSilentMode());
                localSlidingTab = this.mSlidingTab;
                if (!LockScreen.this.mSilentMode)
                    break label64;
            }
            label64: for (int i = 17040168; ; i = 17040169)
            {
                localSlidingTab.setRightHintText(i);
                if (paramInt != 0)
                    LockScreen.this.mCallback.pokeWakelock();
                return;
            }
        }

        public void onTrigger(View paramView, int paramInt)
        {
            if (paramInt == 1)
                LockScreen.this.mCallback.goToUnlockScreen();
            while (true)
            {
                return;
                if (paramInt == 2)
                {
                    LockScreen.this.toggleRingMode();
                    LockScreen.this.mCallback.pokeWakelock();
                }
            }
        }

        public void ping()
        {
        }

        public void reset(boolean paramBoolean)
        {
            this.mSlidingTab.reset(paramBoolean);
        }

        public void setEnabled(int paramInt, boolean paramBoolean)
        {
        }

        public void updateResources()
        {
            int i = 1;
            SlidingTab localSlidingTab;
            int j;
            label48: int k;
            label62: int m;
            if ((LockScreen.this.mSilentMode) && (LockScreen.this.mAudioManager.getRingerMode() == i))
            {
                localSlidingTab = this.mSlidingTab;
                if (!LockScreen.this.mSilentMode)
                    break label113;
                if (i == 0)
                    break label107;
                j = 17302234;
                if (!LockScreen.this.mSilentMode)
                    break label119;
                k = 17302446;
                if (!LockScreen.this.mSilentMode)
                    break label126;
                m = 17302423;
                label76: if (!LockScreen.this.mSilentMode)
                    break label133;
            }
            label133: for (int n = 17302442; ; n = 17302441)
            {
                localSlidingTab.setRightTabResources(j, k, m, n);
                return;
                i = 0;
                break;
                label107: j = 17302231;
                break label48;
                label113: j = 17302232;
                break label48;
                label119: k = 17302443;
                break label62;
                label126: m = 17302422;
                break label76;
            }
        }
    }

    private static abstract interface UnlockWidgetCommonMethods
    {
        public abstract void cleanUp();

        public abstract int getTargetPosition(int paramInt);

        public abstract View getView();

        public abstract void ping();

        public abstract void reset(boolean paramBoolean);

        public abstract void setEnabled(int paramInt, boolean paramBoolean);

        public abstract void updateResources();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.LockScreen
 * JD-Core Version:        0.6.2
 */