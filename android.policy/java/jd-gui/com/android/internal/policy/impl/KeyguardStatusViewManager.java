package com.android.internal.policy.impl;

import android.content.ContentResolver;
import android.content.Context;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.android.internal.telephony.IccCard.State;
import com.android.internal.widget.DigitalClock;
import com.android.internal.widget.LockPatternUtils;
import com.android.internal.widget.TransportControlView;
import java.util.ArrayList;
import java.util.Date;
import libcore.util.MutableInt;

class KeyguardStatusViewManager
    implements View.OnClickListener
{
    public static final int ALARM_ICON = 17301550;
    private static final int BATTERY_INFO = 15;
    public static final int BATTERY_LOW_ICON = 0;
    private static final int CARRIER_HELP_TEXT = 12;
    private static final int CARRIER_TEXT = 11;
    public static final int CHARGING_ICON = 0;
    private static final boolean DEBUG = false;
    private static final int HELP_MESSAGE_TEXT = 13;
    private static final long INSTRUCTION_RESET_DELAY = 2000L;
    private static final int INSTRUCTION_TEXT = 10;
    public static final int LOCK_ICON = 0;
    private static final int OWNER_INFO = 14;
    private static final String TAG = "KeyguardStatusView";
    private TextView mAlarmStatusView;
    private int mBatteryLevel = 100;
    private KeyguardScreenCallback mCallback;
    private CharSequence mCarrierHelpText;
    private CharSequence mCarrierText;
    private TextView mCarrierView;
    private View mContainer;
    private String mDateFormatString;
    private TextView mDateView;
    private DigitalClock mDigitalClock;
    private boolean mEmergencyButtonEnabledBecauseSimLocked;
    private Button mEmergencyCallButton;
    private final boolean mEmergencyCallButtonEnabledInScreen;
    private String mHelpMessageText;
    private KeyguardUpdateMonitor.InfoCallbackImpl mInfoCallback = new KeyguardUpdateMonitor.InfoCallbackImpl()
    {
        public void onPhoneStateChanged(int paramAnonymousInt)
        {
            KeyguardStatusViewManager.this.mPhoneState = paramAnonymousInt;
            KeyguardStatusViewManager.this.updateEmergencyCallButtonState(paramAnonymousInt);
        }

        public void onRefreshBatteryInfo(boolean paramAnonymousBoolean1, boolean paramAnonymousBoolean2, int paramAnonymousInt)
        {
            KeyguardStatusViewManager.access$402(KeyguardStatusViewManager.this, paramAnonymousBoolean1);
            KeyguardStatusViewManager.access$502(KeyguardStatusViewManager.this, paramAnonymousBoolean2);
            KeyguardStatusViewManager.access$602(KeyguardStatusViewManager.this, paramAnonymousInt);
            MutableInt localMutableInt = new MutableInt(0);
            KeyguardStatusViewManager.this.update(15, KeyguardStatusViewManager.access$100(KeyguardStatusViewManager.this, localMutableInt));
        }

        public void onRefreshCarrierInfo(CharSequence paramAnonymousCharSequence1, CharSequence paramAnonymousCharSequence2)
        {
            KeyguardStatusViewManager.access$802(KeyguardStatusViewManager.this, paramAnonymousCharSequence1);
            KeyguardStatusViewManager.access$902(KeyguardStatusViewManager.this, paramAnonymousCharSequence2);
            KeyguardStatusViewManager.this.updateCarrierStateWithSimStatus(KeyguardStatusViewManager.this.mSimState);
        }

        public void onTimeChanged()
        {
            KeyguardStatusViewManager.this.refreshDate();
        }
    };
    private String mInstructionText;
    private LockPatternUtils mLockPatternUtils;
    private CharSequence mOwnerInfoText;
    private TextView mOwnerInfoView;
    protected int mPhoneState;
    private CharSequence mPlmn;
    private boolean mPluggedIn = false;
    private boolean mShowingBatteryInfo = false;
    private boolean mShowingStatus;
    protected IccCard.State mSimState;
    private KeyguardUpdateMonitor.SimStateCallback mSimStateCallback = new KeyguardUpdateMonitor.SimStateCallback()
    {
        public void onSimStateChanged(IccCard.State paramAnonymousState)
        {
            KeyguardStatusViewManager.this.updateCarrierStateWithSimStatus(paramAnonymousState);
        }
    };
    private CharSequence mSpn;
    private StatusMode mStatus;
    private TextView mStatus1View;
    private TransientTextManager mTransientTextManager;
    private TransportControlView mTransportView;
    private KeyguardUpdateMonitor mUpdateMonitor;

    public KeyguardStatusViewManager(View paramView, KeyguardUpdateMonitor paramKeyguardUpdateMonitor, LockPatternUtils paramLockPatternUtils, KeyguardScreenCallback paramKeyguardScreenCallback, boolean paramBoolean)
    {
        this.mContainer = paramView;
        this.mDateFormatString = getContext().getString(17039540);
        this.mLockPatternUtils = paramLockPatternUtils;
        this.mUpdateMonitor = paramKeyguardUpdateMonitor;
        this.mCallback = paramKeyguardScreenCallback;
        this.mCarrierView = ((TextView)findViewById(16908973));
        this.mDateView = ((TextView)findViewById(16908387));
        this.mStatus1View = ((TextView)findViewById(16908972));
        this.mAlarmStatusView = ((TextView)findViewById(16908971));
        this.mOwnerInfoView = ((TextView)findViewById(16908993));
        this.mTransportView = ((TransportControlView)findViewById(16908977));
        this.mEmergencyCallButton = ((Button)findViewById(16908948));
        this.mEmergencyCallButtonEnabledInScreen = paramBoolean;
        this.mDigitalClock = ((DigitalClock)findViewById(16908388));
        if (this.mTransportView != null)
            this.mTransportView.setVisibility(8);
        if (this.mEmergencyCallButton != null)
        {
            this.mEmergencyCallButton.setText(17040123);
            this.mEmergencyCallButton.setOnClickListener(this);
            this.mEmergencyCallButton.setFocusable(false);
        }
        this.mTransientTextManager = new TransientTextManager(this.mCarrierView);
        this.mUpdateMonitor.registerInfoCallback(this.mInfoCallback);
        this.mUpdateMonitor.registerSimStateCallback(this.mSimStateCallback);
        resetStatusInfo();
        refreshDate();
        updateOwnerInfo();
        View[] arrayOfView = new View[5];
        arrayOfView[0] = this.mCarrierView;
        arrayOfView[1] = this.mDateView;
        arrayOfView[2] = this.mStatus1View;
        arrayOfView[3] = this.mOwnerInfoView;
        arrayOfView[4] = this.mAlarmStatusView;
        int i = arrayOfView.length;
        for (int j = 0; j < i; j++)
        {
            View localView = arrayOfView[j];
            if (localView != null)
                localView.setSelected(true);
        }
    }

    private View findViewById(int paramInt)
    {
        return this.mContainer.findViewById(paramInt);
    }

    private CharSequence getAltTextMessage(MutableInt paramMutableInt)
    {
        Object localObject = null;
        if (this.mShowingBatteryInfo)
            if (this.mPluggedIn)
                if (this.mUpdateMonitor.isDeviceCharged())
                {
                    localObject = getContext().getString(17040130);
                    paramMutableInt.value = 0;
                }
        while (true)
        {
            return localObject;
            Context localContext = getContext();
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = Integer.valueOf(this.mBatteryLevel);
            localObject = localContext.getString(17040129, arrayOfObject);
            break;
            if (this.mBatteryLevel < 20)
            {
                localObject = getContext().getString(17040132);
                paramMutableInt.value = 0;
                continue;
                localObject = this.mCarrierText;
            }
        }
    }

    private Context getContext()
    {
        return this.mContainer.getContext();
    }

    private CharSequence getPriorityTextMessage(MutableInt paramMutableInt)
    {
        Object localObject = null;
        if (!TextUtils.isEmpty(this.mInstructionText))
        {
            localObject = this.mInstructionText;
            paramMutableInt.value = 0;
        }
        while (true)
        {
            return localObject;
            if (this.mShowingBatteryInfo)
            {
                if (this.mPluggedIn)
                {
                    if (this.mUpdateMonitor.isDeviceCharged());
                    Context localContext;
                    Object[] arrayOfObject;
                    for (localObject = getContext().getString(17040130); ; localObject = localContext.getString(17040129, arrayOfObject))
                    {
                        paramMutableInt.value = 0;
                        break;
                        localContext = getContext();
                        arrayOfObject = new Object[1];
                        arrayOfObject[0] = Integer.valueOf(this.mBatteryLevel);
                    }
                }
                if (this.mBatteryLevel < 20)
                {
                    localObject = getContext().getString(17040132);
                    paramMutableInt.value = 0;
                }
            }
            else if ((!inWidgetMode()) && (this.mOwnerInfoView == null) && (this.mOwnerInfoText != null))
            {
                localObject = this.mOwnerInfoText;
            }
        }
    }

    private CharSequence getText(int paramInt)
    {
        if (paramInt == 0);
        for (CharSequence localCharSequence = null; ; localCharSequence = getContext().getText(paramInt))
            return localCharSequence;
    }

    private boolean inWidgetMode()
    {
        if ((this.mTransportView != null) && (this.mTransportView.getVisibility() == 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private static CharSequence makeCarierString(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
    {
        int i;
        int j;
        if (!TextUtils.isEmpty(paramCharSequence1))
        {
            i = 1;
            if (TextUtils.isEmpty(paramCharSequence2))
                break label58;
            j = 1;
            label18: if ((i == 0) || (j == 0))
                break label63;
            paramCharSequence1 = paramCharSequence1 + "|" + paramCharSequence2;
        }
        while (true)
        {
            return paramCharSequence1;
            i = 0;
            break;
            label58: j = 0;
            break label18;
            label63: if (i == 0)
                if (j != 0)
                    paramCharSequence1 = paramCharSequence2;
                else
                    paramCharSequence1 = "";
        }
    }

    private CharSequence makeCarrierStringOnEmergencyCapable(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
    {
        if (this.mLockPatternUtils.isEmergencyCallCapable())
            paramCharSequence1 = makeCarierString(paramCharSequence1, paramCharSequence2);
        return paramCharSequence1;
    }

    private void update(int paramInt, CharSequence paramCharSequence)
    {
        if (inWidgetMode())
            switch (paramInt)
            {
            case 11:
            case 14:
            default:
            case 10:
            case 12:
            case 13:
            case 15:
            }
        while (true)
        {
            return;
            this.mTransientTextManager.post(paramCharSequence, 0, 2000L);
            continue;
            updateStatusLines(this.mShowingStatus);
        }
    }

    private void updateAlarmInfo()
    {
        int i = 0;
        int j;
        TextView localTextView;
        if (this.mAlarmStatusView != null)
        {
            String str = this.mLockPatternUtils.getNextAlarm();
            if ((!this.mShowingStatus) || (TextUtils.isEmpty(str)))
                break label70;
            j = 1;
            this.mAlarmStatusView.setText(str);
            this.mAlarmStatusView.setCompoundDrawablesWithIntrinsicBounds(17301550, 0, 0, 0);
            localTextView = this.mAlarmStatusView;
            if (j == 0)
                break label75;
        }
        while (true)
        {
            localTextView.setVisibility(i);
            return;
            label70: j = 0;
            break;
            label75: i = 8;
        }
    }

    private void updateCarrierStateWithSimStatus(IccCard.State paramState)
    {
        CharSequence localCharSequence = null;
        int i = 0;
        this.mEmergencyButtonEnabledBecauseSimLocked = false;
        this.mStatus = getStatusForIccState(paramState);
        this.mSimState = paramState;
        switch (3.$SwitchMap$com$android$internal$policy$impl$KeyguardStatusViewManager$StatusMode[this.mStatus.ordinal()])
        {
        default:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        }
        while (true)
        {
            setCarrierText(localCharSequence);
            setCarrierHelpText(i);
            updateEmergencyCallButtonState(this.mPhoneState);
            return;
            localCharSequence = makeCarierString(this.mPlmn, this.mSpn);
            continue;
            localCharSequence = makeCarrierStringOnEmergencyCapable(getContext().getText(17040145), this.mPlmn);
            i = 17040121;
            continue;
            localCharSequence = makeCarrierStringOnEmergencyCapable(getContext().getText(17040133), this.mPlmn);
            i = 17040136;
            continue;
            localCharSequence = getContext().getText(17040137);
            i = 17040138;
            this.mEmergencyButtonEnabledBecauseSimLocked = true;
            continue;
            localCharSequence = makeCarrierStringOnEmergencyCapable(getContext().getText(17040133), this.mPlmn);
            i = 17040135;
            this.mEmergencyButtonEnabledBecauseSimLocked = true;
            continue;
            localCharSequence = makeCarrierStringOnEmergencyCapable(getContext().getText(17040148), this.mPlmn);
            this.mEmergencyButtonEnabledBecauseSimLocked = true;
            continue;
            localCharSequence = makeCarrierStringOnEmergencyCapable(getContext().getText(17040146), this.mPlmn);
            if (!this.mLockPatternUtils.isPukUnlockScreenEnable())
                this.mEmergencyButtonEnabledBecauseSimLocked = true;
        }
    }

    private void updateCarrierText()
    {
        if ((!inWidgetMode()) && (this.mCarrierView != null))
            this.mCarrierView.setText(this.mCarrierText);
    }

    private void updateEmergencyCallButtonState(int paramInt)
    {
        int i;
        if (this.mEmergencyCallButton != null)
        {
            if ((!this.mLockPatternUtils.isEmergencyCallEnabledWhileSimLocked()) || (!this.mEmergencyButtonEnabledBecauseSimLocked))
                break label53;
            i = 1;
            if ((!this.mEmergencyCallButtonEnabledInScreen) && (i == 0))
                break label58;
        }
        label53: label58: for (boolean bool = true; ; bool = false)
        {
            this.mLockPatternUtils.updateEmergencyCallButtonState(this.mEmergencyCallButton, paramInt, bool);
            return;
            i = 0;
            break;
        }
    }

    private void updateOwnerInfo()
    {
        int i = 1;
        ContentResolver localContentResolver = getContext().getContentResolver();
        String str;
        label33: TextView localTextView;
        if (Settings.Secure.getInt(localContentResolver, "lock_screen_owner_info_enabled", i) != 0)
        {
            if (i == 0)
                break label89;
            str = Settings.Secure.getString(localContentResolver, "lock_screen_owner_info");
            this.mOwnerInfoText = str;
            if (this.mOwnerInfoView != null)
            {
                this.mOwnerInfoView.setText(this.mOwnerInfoText);
                localTextView = this.mOwnerInfoView;
                if (!TextUtils.isEmpty(this.mOwnerInfoText))
                    break label94;
            }
        }
        label89: label94: for (int j = 8; ; j = 0)
        {
            localTextView.setVisibility(j);
            return;
            i = 0;
            break;
            str = null;
            break label33;
        }
    }

    private void updateStatus1()
    {
        int i = 0;
        TextView localTextView;
        if (this.mStatus1View != null)
        {
            MutableInt localMutableInt = new MutableInt(0);
            CharSequence localCharSequence = getPriorityTextMessage(localMutableInt);
            this.mStatus1View.setText(localCharSequence);
            this.mStatus1View.setCompoundDrawablesWithIntrinsicBounds(localMutableInt.value, 0, 0, 0);
            localTextView = this.mStatus1View;
            if (!this.mShowingStatus)
                break label66;
        }
        while (true)
        {
            localTextView.setVisibility(i);
            return;
            label66: i = 4;
        }
    }

    public StatusMode getStatusForIccState(IccCard.State paramState)
    {
        StatusMode localStatusMode;
        if (paramState == null)
            localStatusMode = StatusMode.Normal;
        while (true)
        {
            label8: return localStatusMode;
            if ((!this.mUpdateMonitor.isDeviceProvisioned()) && ((paramState == IccCard.State.ABSENT) || (paramState == IccCard.State.PERM_DISABLED)));
            for (int i = 1; ; i = 0)
            {
                if (i != 0)
                    paramState = IccCard.State.NETWORK_LOCKED;
                switch (3.$SwitchMap$com$android$internal$telephony$IccCard$State[paramState.ordinal()])
                {
                default:
                    localStatusMode = StatusMode.SimMissing;
                    break label8;
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                }
            }
            localStatusMode = StatusMode.SimMissing;
            continue;
            localStatusMode = StatusMode.SimMissingLocked;
            continue;
            localStatusMode = StatusMode.SimMissing;
            continue;
            localStatusMode = StatusMode.SimLocked;
            continue;
            localStatusMode = StatusMode.SimPukLocked;
            continue;
            localStatusMode = StatusMode.Normal;
            continue;
            localStatusMode = StatusMode.SimPermDisabled;
            continue;
            localStatusMode = StatusMode.SimMissing;
        }
    }

    public void onClick(View paramView)
    {
        if (paramView == this.mEmergencyCallButton)
            this.mCallback.takeEmergencyCallAction();
    }

    public void onPause()
    {
        this.mUpdateMonitor.removeCallback(this.mInfoCallback);
        this.mUpdateMonitor.removeCallback(this.mSimStateCallback);
    }

    public void onResume()
    {
        if (this.mDigitalClock != null)
            this.mDigitalClock.updateTime();
        this.mUpdateMonitor.registerInfoCallback(this.mInfoCallback);
        this.mUpdateMonitor.registerSimStateCallback(this.mSimStateCallback);
        resetStatusInfo();
        if (this.mUpdateMonitor.getMaxBiometricUnlockAttemptsReached())
            setInstructionText(getContext().getString(17040128));
    }

    void refreshDate()
    {
        if (this.mDateView != null)
            this.mDateView.setText(DateFormat.format(this.mDateFormatString, new Date()));
    }

    void resetStatusInfo()
    {
        this.mInstructionText = null;
        this.mShowingBatteryInfo = this.mUpdateMonitor.shouldShowBatteryInfo();
        this.mPluggedIn = this.mUpdateMonitor.isDevicePluggedIn();
        this.mBatteryLevel = this.mUpdateMonitor.getBatteryLevel();
        updateStatusLines(true);
    }

    public void setCarrierHelpText(int paramInt)
    {
        this.mCarrierHelpText = getText(paramInt);
        update(12, this.mCarrierHelpText);
    }

    void setCarrierText(CharSequence paramCharSequence)
    {
        this.mCarrierText = paramCharSequence;
        update(11, paramCharSequence);
    }

    public void setHelpMessage(int paramInt1, int paramInt2)
    {
        CharSequence localCharSequence = getText(paramInt1);
        if (localCharSequence == null);
        for (String str = null; ; str = localCharSequence.toString())
        {
            this.mHelpMessageText = str;
            update(13, this.mHelpMessageText);
            return;
        }
    }

    void setInstructionText(String paramString)
    {
        this.mInstructionText = paramString;
        update(10, paramString);
    }

    void setOwnerInfo(CharSequence paramCharSequence)
    {
        this.mOwnerInfoText = paramCharSequence;
        update(14, paramCharSequence);
    }

    void updateStatusLines(boolean paramBoolean)
    {
        this.mShowingStatus = paramBoolean;
        updateAlarmInfo();
        updateOwnerInfo();
        updateStatus1();
        updateCarrierText();
    }

    static enum StatusMode
    {
        private final boolean mShowStatusLines;

        static
        {
            NetworkLocked = new StatusMode("NetworkLocked", 1, true);
            SimMissing = new StatusMode("SimMissing", 2, false);
            SimMissingLocked = new StatusMode("SimMissingLocked", 3, false);
            SimPukLocked = new StatusMode("SimPukLocked", 4, false);
            SimLocked = new StatusMode("SimLocked", 5, true);
            SimPermDisabled = new StatusMode("SimPermDisabled", 6, false);
            StatusMode[] arrayOfStatusMode = new StatusMode[7];
            arrayOfStatusMode[0] = Normal;
            arrayOfStatusMode[1] = NetworkLocked;
            arrayOfStatusMode[2] = SimMissing;
            arrayOfStatusMode[3] = SimMissingLocked;
            arrayOfStatusMode[4] = SimPukLocked;
            arrayOfStatusMode[5] = SimLocked;
            arrayOfStatusMode[6] = SimPermDisabled;
        }

        private StatusMode(boolean paramBoolean)
        {
            this.mShowStatusLines = paramBoolean;
        }

        public boolean shouldShowStatusLines()
        {
            return this.mShowStatusLines;
        }
    }

    private class TransientTextManager
    {
        private ArrayList<Data> mMessages = new ArrayList(5);
        private TextView mTextView;

        TransientTextManager(TextView arg2)
        {
            Object localObject;
            this.mTextView = localObject;
        }

        void post(CharSequence paramCharSequence, int paramInt, long paramLong)
        {
            if (this.mTextView == null);
            while (true)
            {
                return;
                this.mTextView.setText(paramCharSequence);
                this.mTextView.setCompoundDrawablesWithIntrinsicBounds(paramInt, 0, 0, 0);
                final Data localData = new Data(paramCharSequence, paramInt);
                KeyguardStatusViewManager.this.mContainer.postDelayed(new Runnable()
                {
                    public void run()
                    {
                        KeyguardStatusViewManager.TransientTextManager.this.mMessages.remove(localData);
                        int i = -1 + KeyguardStatusViewManager.TransientTextManager.this.mMessages.size();
                        KeyguardStatusViewManager.TransientTextManager.Data localData;
                        CharSequence localCharSequence;
                        if (i > 0)
                        {
                            localData = (KeyguardStatusViewManager.TransientTextManager.Data)KeyguardStatusViewManager.TransientTextManager.this.mMessages.get(i);
                            localCharSequence = localData.text;
                        }
                        MutableInt localMutableInt;
                        for (int j = localData.icon; ; j = localMutableInt.value)
                        {
                            KeyguardStatusViewManager.TransientTextManager.this.mTextView.setText(localCharSequence);
                            KeyguardStatusViewManager.TransientTextManager.this.mTextView.setCompoundDrawablesWithIntrinsicBounds(j, 0, 0, 0);
                            return;
                            localMutableInt = new MutableInt(0);
                            localCharSequence = KeyguardStatusViewManager.this.getAltTextMessage(localMutableInt);
                        }
                    }
                }
                , paramLong);
            }
        }

        private class Data
        {
            final int icon;
            final CharSequence text;

            Data(CharSequence paramInt, int arg3)
            {
                this.text = paramInt;
                int i;
                this.icon = i;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.KeyguardStatusViewManager
 * JD-Core Version:        0.6.2
 */