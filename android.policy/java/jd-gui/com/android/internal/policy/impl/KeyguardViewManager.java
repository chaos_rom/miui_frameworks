package com.android.internal.policy.impl;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.ActivityManager;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.os.IBinder;
import android.os.SystemProperties;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.ViewManager;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;

public class KeyguardViewManager
    implements KeyguardWindowController
{
    private static final boolean DEBUG;
    private static String TAG = "KeyguardViewManager";
    private final KeyguardViewCallback mCallback;
    private final Context mContext;
    private FrameLayout mKeyguardHost;
    private KeyguardViewBase mKeyguardView;
    private final KeyguardViewProperties mKeyguardViewProperties;
    private boolean mNeedsInput = false;
    private boolean mScreenOn = false;
    private final KeyguardUpdateMonitor mUpdateMonitor;
    private final ViewManager mViewManager;
    private WindowManager.LayoutParams mWindowLayoutParams;

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public KeyguardViewManager(Context paramContext, ViewManager paramViewManager, KeyguardViewCallback paramKeyguardViewCallback, KeyguardViewProperties paramKeyguardViewProperties, KeyguardUpdateMonitor paramKeyguardUpdateMonitor)
    {
        this.mContext = new ContextThemeWrapper(paramContext, 16973931);
        this.mViewManager = paramViewManager;
        this.mCallback = paramKeyguardViewCallback;
        this.mKeyguardViewProperties = paramKeyguardViewProperties;
        this.mUpdateMonitor = paramKeyguardUpdateMonitor;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    KeyguardViewBase getKeyguardView()
    {
        return this.mKeyguardView;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    KeyguardViewProperties getKeyguardViewProperties()
    {
        return this.mKeyguardViewProperties;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    WindowManager.LayoutParams getWindowLayoutParams()
    {
        return this.mWindowLayoutParams;
    }

    /** @deprecated */
    public void hide()
    {
        try
        {
            if (this.mKeyguardHost != null)
            {
                this.mKeyguardHost.setVisibility(8);
                if (this.mKeyguardView != null)
                {
                    final KeyguardViewBase localKeyguardViewBase = this.mKeyguardView;
                    this.mKeyguardView = null;
                    this.mKeyguardHost.postDelayed(new Runnable()
                    {
                        public void run()
                        {
                            synchronized (KeyguardViewManager.this)
                            {
                                localKeyguardViewBase.cleanUp();
                                KeyguardViewManager.this.mKeyguardHost.removeView(localKeyguardViewBase);
                                return;
                            }
                        }
                    }
                    , 500L);
                }
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public boolean isShowing()
    {
        try
        {
            if (this.mKeyguardHost != null)
            {
                int i = this.mKeyguardHost.getVisibility();
                if (i == 0)
                {
                    bool = true;
                    return bool;
                }
            }
            boolean bool = false;
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void onScreenTurnedOff()
    {
        try
        {
            this.mScreenOn = false;
            if (this.mKeyguardView != null)
                this.mKeyguardView.onScreenTurnedOff();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void onScreenTurnedOn(final ShowListener paramShowListener)
    {
        while (true)
        {
            try
            {
                this.mScreenOn = true;
                if (this.mKeyguardView != null)
                {
                    this.mKeyguardView.onScreenTurnedOn();
                    if (this.mKeyguardHost.getVisibility() == 0)
                    {
                        this.mKeyguardHost.post(new Runnable()
                        {
                            public void run()
                            {
                                if (KeyguardViewManager.this.mKeyguardHost.getVisibility() == 0)
                                    paramShowListener.onShown(KeyguardViewManager.this.mKeyguardHost.getWindowToken());
                                while (true)
                                {
                                    return;
                                    paramShowListener.onShown(null);
                                }
                            }
                        });
                        return;
                    }
                    paramShowListener.onShown(null);
                    continue;
                }
            }
            finally
            {
            }
            paramShowListener.onShown(null);
        }
    }

    /** @deprecated */
    public void reset()
    {
        try
        {
            if (this.mKeyguardView != null)
                this.mKeyguardView.reset();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void setNeedsInput(boolean paramBoolean)
    {
        this.mNeedsInput = paramBoolean;
        WindowManager.LayoutParams localLayoutParams2;
        if (this.mWindowLayoutParams != null)
        {
            if (!paramBoolean)
                break label50;
            localLayoutParams2 = this.mWindowLayoutParams;
        }
        label50: WindowManager.LayoutParams localLayoutParams1;
        for (localLayoutParams2.flags = (0xFFFDFFFF & localLayoutParams2.flags); ; localLayoutParams1.flags = (0x20000 | localLayoutParams1.flags))
        {
            this.mViewManager.updateViewLayout(this.mKeyguardHost, this.mWindowLayoutParams);
            return;
            localLayoutParams1 = this.mWindowLayoutParams;
        }
    }

    /** @deprecated */
    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public void show()
    {
        int i = 0;
        while (true)
        {
            try
            {
                Resources localResources = this.mContext.getResources();
                if ((!SystemProperties.getBoolean("lockscreen.rot_override", false)) && (!localResources.getBoolean(17891360)))
                {
                    if (this.mKeyguardHost == null)
                    {
                        this.mKeyguardHost = new KeyguardViewHost(this.mContext, this.mCallback, null);
                        int j = 68159744;
                        if (!this.mNeedsInput)
                            j |= 131072;
                        if (ActivityManager.isHighEndGfx(((WindowManager)this.mContext.getSystemService("window")).getDefaultDisplay()))
                            j |= 16777216;
                        WindowManager.LayoutParams localLayoutParams1 = new WindowManager.LayoutParams(-1, -1, 2004, j, -3);
                        localLayoutParams1.softInputMode = 16;
                        localLayoutParams1.windowAnimations = 16974302;
                        if (ActivityManager.isHighEndGfx(((WindowManager)this.mContext.getSystemService("window")).getDefaultDisplay()))
                        {
                            localLayoutParams1.flags = (0x1000000 | localLayoutParams1.flags);
                            localLayoutParams1.privateFlags = (0x2 | localLayoutParams1.privateFlags);
                        }
                        localLayoutParams1.screenOrientation = 1;
                        localLayoutParams1.privateFlags = (0x8 | localLayoutParams1.privateFlags);
                        localLayoutParams1.setTitle("Keyguard");
                        this.mWindowLayoutParams = localLayoutParams1;
                        this.mViewManager.addView(this.mKeyguardHost, localLayoutParams1);
                    }
                    if (i != 0)
                    {
                        this.mWindowLayoutParams.screenOrientation = 4;
                        this.mViewManager.updateViewLayout(this.mKeyguardHost, this.mWindowLayoutParams);
                        if (this.mKeyguardView == null)
                        {
                            this.mKeyguardView = this.mKeyguardViewProperties.createKeyguardView(this.mContext, this.mCallback, this.mUpdateMonitor, this);
                            this.mKeyguardView.setId(16908854);
                            FrameLayout.LayoutParams localLayoutParams = new FrameLayout.LayoutParams(-1, -1);
                            this.mKeyguardHost.addView(this.mKeyguardView, localLayoutParams);
                            if (this.mScreenOn)
                                this.mKeyguardView.show();
                        }
                        Log.v(TAG, "KGVM: Set visibility on " + this.mKeyguardHost + " to " + 6291456);
                        this.mKeyguardHost.setSystemUiVisibility(6291456);
                        Injector.updateDisplayDesktopFlag(this);
                        this.mViewManager.updateViewLayout(this.mKeyguardHost, this.mWindowLayoutParams);
                        this.mKeyguardHost.setVisibility(0);
                        this.mKeyguardView.requestFocus();
                        return;
                    }
                    this.mWindowLayoutParams.screenOrientation = 1;
                    continue;
                }
            }
            finally
            {
            }
            i = 1;
        }
    }

    /** @deprecated */
    public void verifyUnlock()
    {
        try
        {
            show();
            this.mKeyguardView.verifyUnlock();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public boolean wakeWhenReadyTq(int paramInt)
    {
        if (this.mKeyguardView != null)
            this.mKeyguardView.wakeWhenReadyTq(paramInt);
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            Log.w(TAG, "mKeyguardView is null in wakeWhenReadyTq");
        }
    }

    private static class KeyguardViewHost extends FrameLayout
    {
        private final KeyguardViewCallback mCallback;

        private KeyguardViewHost(Context paramContext, KeyguardViewCallback paramKeyguardViewCallback)
        {
            super();
            this.mCallback = paramKeyguardViewCallback;
        }

        protected void dispatchDraw(Canvas paramCanvas)
        {
            super.dispatchDraw(paramCanvas);
            this.mCallback.keyguardDoneDrawing();
        }
    }

    public static abstract interface ShowListener
    {
        public abstract void onShown(IBinder paramIBinder);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static void updateDisplayDesktopFlag(KeyguardViewManager paramKeyguardViewManager)
        {
            KeyguardViewBase localKeyguardViewBase = paramKeyguardViewManager.getKeyguardView();
            WindowManager.LayoutParams localLayoutParams = paramKeyguardViewManager.getWindowLayoutParams();
            boolean bool = false;
            if ((localKeyguardViewBase instanceof MiuiLockPatternKeyguardView))
                bool = ((MiuiLockPatternKeyguardView)localKeyguardViewBase).isDisplayDesktop();
            if ((bool) && (!paramKeyguardViewManager.getKeyguardViewProperties().isSecure()))
                localLayoutParams.flags = (0xFFEFFFFF & localLayoutParams.flags);
            for (localLayoutParams.privateFlags = (0x40000000 | localLayoutParams.privateFlags); ; localLayoutParams.privateFlags = (0xBFFFFFFF & localLayoutParams.privateFlags))
            {
                return;
                localLayoutParams.flags = (0x100000 | localLayoutParams.flags);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.KeyguardViewManager
 * JD-Core Version:        0.6.2
 */