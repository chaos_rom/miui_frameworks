package com.android.internal.policy.impl;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.ActivityManagerNative;
import android.app.AlarmManager;
import android.app.IActivityManager;
import android.app.StatusBarManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Handler;
import android.os.LocalPowerManager;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.provider.Settings.System;
import android.telephony.TelephonyManager;
import android.util.EventLog;
import android.util.Log;
import android.view.WindowManagerImpl;
import android.view.WindowManagerPolicy.OnKeyguardExitResult;
import com.android.internal.telephony.IccCard.State;
import com.android.internal.widget.LockPatternUtils;

public class KeyguardViewMediator
    implements KeyguardViewCallback, KeyguardUpdateMonitor.SimStateCallback
{
    protected static final int AWAKE_INTERVAL_DEFAULT_MS = 10000;
    private static final boolean DBG_WAKE = false;
    private static final boolean DEBUG = false;
    private static final String DELAYED_KEYGUARD_ACTION = "com.android.internal.policy.impl.PhoneWindowManager.DELAYED_KEYGUARD";
    private static final boolean ENABLE_INSECURE_STATUS_BAR_EXPAND = true;
    private static final int HIDE = 3;
    private static final int KEYGUARD_DISPLAY_TIMEOUT_DELAY_DEFAULT = 30000;
    private static final int KEYGUARD_DONE = 9;
    private static final int KEYGUARD_DONE_AUTHENTICATING = 11;
    private static final int KEYGUARD_DONE_DRAWING = 10;
    private static final int KEYGUARD_DONE_DRAWING_TIMEOUT_MS = 2000;
    private static final int KEYGUARD_LOCK_AFTER_DELAY_DEFAULT = 5000;
    private static final int KEYGUARD_TIMEOUT = 13;
    private static final int NOTIFY_SCREEN_OFF = 6;
    private static final int NOTIFY_SCREEN_ON = 7;
    private static final int RESET = 4;
    private static final int SET_HIDDEN = 12;
    private static final int SHOW = 2;
    private static final String TAG = "KeyguardViewMediator";
    private static final int TIMEOUT = 1;
    private static final int VERIFY_UNLOCK = 5;
    private static final int WAKE_WHEN_READY = 8;
    private AlarmManager mAlarmManager;
    private AudioManager mAudioManager;
    private BroadcastReceiver mBroadCastReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            String str = paramAnonymousIntent.getAction();
            if (str.equals("com.android.internal.policy.impl.PhoneWindowManager.DELAYED_KEYGUARD"))
            {
                int i = paramAnonymousIntent.getIntExtra("seq", 0);
                synchronized (KeyguardViewMediator.this)
                {
                    if (KeyguardViewMediator.this.mDelayedShowingSequence == i)
                    {
                        KeyguardViewMediator.access$602(KeyguardViewMediator.this, true);
                        KeyguardViewMediator.this.doKeyguardLocked();
                    }
                }
            }
            if ("android.intent.action.PHONE_STATE".equals(str))
            {
                KeyguardViewMediator.access$802(KeyguardViewMediator.this, paramAnonymousIntent.getStringExtra("state"));
                synchronized (KeyguardViewMediator.this)
                {
                    if ((TelephonyManager.EXTRA_STATE_IDLE.equals(KeyguardViewMediator.this.mPhoneState)) && (!KeyguardViewMediator.this.mScreenOn) && (KeyguardViewMediator.this.mExternallyEnabled))
                        KeyguardViewMediator.this.doKeyguardLocked();
                }
            }
        }
    };
    private PhoneWindowManager mCallback;
    private Context mContext;
    private int mDelayedShowingSequence;
    private WindowManagerPolicy.OnKeyguardExitResult mExitSecureCallback;
    private boolean mExternallyEnabled = true;
    private Handler mHandler = new Handler()
    {
        public void handleMessage(Message paramAnonymousMessage)
        {
            boolean bool = true;
            switch (paramAnonymousMessage.what)
            {
            default:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            }
            while (true)
            {
                return;
                KeyguardViewMediator.this.handleTimeout(paramAnonymousMessage.arg1);
                continue;
                KeyguardViewMediator.this.handleShow();
                continue;
                KeyguardViewMediator.this.handleHide();
                continue;
                KeyguardViewMediator.this.handleReset();
                continue;
                KeyguardViewMediator.this.handleVerifyUnlock();
                continue;
                KeyguardViewMediator.this.handleNotifyScreenOff();
                continue;
                KeyguardViewMediator.this.handleNotifyScreenOn((KeyguardViewManager.ShowListener)paramAnonymousMessage.obj);
                continue;
                KeyguardViewMediator.this.handleWakeWhenReady(paramAnonymousMessage.arg1);
                continue;
                KeyguardViewMediator localKeyguardViewMediator3 = KeyguardViewMediator.this;
                if (paramAnonymousMessage.arg1 != 0);
                while (true)
                {
                    localKeyguardViewMediator3.handleKeyguardDone(bool);
                    break;
                    bool = false;
                }
                KeyguardViewMediator.this.handleKeyguardDoneDrawing();
                continue;
                KeyguardViewMediator.this.keyguardDone(bool);
                continue;
                KeyguardViewMediator localKeyguardViewMediator2 = KeyguardViewMediator.this;
                if (paramAnonymousMessage.arg1 != 0);
                while (true)
                {
                    localKeyguardViewMediator2.handleSetHidden(bool);
                    break;
                    bool = false;
                }
                synchronized (KeyguardViewMediator.this)
                {
                    KeyguardViewMediator.this.doKeyguardLocked();
                }
            }
        }
    };
    private boolean mHidden = false;
    KeyguardUpdateMonitor.InfoCallbackImpl mInfoCallback = new KeyguardUpdateMonitor.InfoCallbackImpl()
    {
        public void onClockVisibilityChanged()
        {
            KeyguardViewMediator.this.adjustStatusBarLocked();
        }

        public void onDeviceProvisioned()
        {
            KeyguardViewMediator.this.mContext.sendBroadcast(KeyguardViewMediator.this.mUserPresentIntent);
        }
    };
    private KeyguardViewManager mKeyguardViewManager;
    private KeyguardViewProperties mKeyguardViewProperties;
    private LockPatternUtils mLockPatternUtils;
    private int mLockSoundId;
    private int mLockSoundStreamId;
    private final float mLockSoundVolume;
    private SoundPool mLockSounds;
    private int mMasterStreamType;
    private boolean mNeedToReshowWhenReenabled = false;
    private PowerManager mPM;
    private String mPhoneState = TelephonyManager.EXTRA_STATE_IDLE;
    LocalPowerManager mRealPowerManager;
    private boolean mScreenOn = false;
    private PowerManager.WakeLock mShowKeyguardWakeLock;
    private boolean mShowLockIcon;
    private boolean mShowing = false;
    private boolean mShowingLockIcon;
    private StatusBarManager mStatusBarManager;
    private boolean mSuppressNextLockSound = true;
    private boolean mSystemReady;
    private int mUnlockSoundId;
    private KeyguardUpdateMonitor mUpdateMonitor;
    private BroadcastReceiver mUserChangeReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            String str = paramAnonymousIntent.getAction();
            if ("android.intent.action.USER_SWITCHED".equals(str))
                KeyguardViewMediator.this.onUserSwitched(paramAnonymousIntent.getIntExtra("android.intent.extra.user_id", 0));
            while (true)
            {
                return;
                if ("android.intent.action.USER_REMOVED".equals(str))
                    KeyguardViewMediator.this.onUserRemoved(paramAnonymousIntent.getIntExtra("android.intent.extra.user_id", 0));
            }
        }
    };
    private Intent mUserPresentIntent;
    private boolean mWaitingUntilKeyguardVisible = false;
    private PowerManager.WakeLock mWakeAndHandOff;
    private PowerManager.WakeLock mWakeLock;
    private int mWakelockSequence;

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public KeyguardViewMediator(Context paramContext, PhoneWindowManager paramPhoneWindowManager, LocalPowerManager paramLocalPowerManager)
    {
        this.mContext = paramContext;
        this.mRealPowerManager = paramLocalPowerManager;
        this.mPM = ((PowerManager)paramContext.getSystemService("power"));
        this.mWakeLock = this.mPM.newWakeLock(268435482, "keyguard");
        this.mWakeLock.setReferenceCounted(false);
        this.mShowKeyguardWakeLock = this.mPM.newWakeLock(1, "show keyguard");
        this.mShowKeyguardWakeLock.setReferenceCounted(false);
        this.mWakeAndHandOff = this.mPM.newWakeLock(1, "keyguardWakeAndHandOff");
        this.mWakeAndHandOff.setReferenceCounted(false);
        IntentFilter localIntentFilter1 = new IntentFilter();
        localIntentFilter1.addAction("com.android.internal.policy.impl.PhoneWindowManager.DELAYED_KEYGUARD");
        localIntentFilter1.addAction("android.intent.action.PHONE_STATE");
        paramContext.registerReceiver(this.mBroadCastReceiver, localIntentFilter1);
        this.mAlarmManager = ((AlarmManager)paramContext.getSystemService("alarm"));
        this.mCallback = paramPhoneWindowManager;
        this.mUpdateMonitor = new KeyguardUpdateMonitor(paramContext);
        this.mUpdateMonitor.registerInfoCallback(this.mInfoCallback);
        this.mUpdateMonitor.registerSimStateCallback(this);
        this.mLockPatternUtils = new LockPatternUtils(this.mContext);
        this.mKeyguardViewProperties = new MiuiLockPatternKeyguardViewProperties(this.mLockPatternUtils, this.mUpdateMonitor);
        this.mKeyguardViewManager = new KeyguardViewManager(paramContext, WindowManagerImpl.getDefault(), this, this.mKeyguardViewProperties, this.mUpdateMonitor);
        this.mUserPresentIntent = new Intent("android.intent.action.USER_PRESENT");
        this.mUserPresentIntent.addFlags(671088640);
        ContentResolver localContentResolver = this.mContext.getContentResolver();
        if (Settings.System.getInt(localContentResolver, "show_status_bar_lock", 0) == 1);
        for (boolean bool = true; ; bool = false)
        {
            this.mShowLockIcon = bool;
            this.mLockSounds = new SoundPool(1, 1, 0);
            String str1 = Settings.System.getString(localContentResolver, "lock_sound");
            if (str1 != null)
                this.mLockSoundId = this.mLockSounds.load(str1, 1);
            if ((str1 != null) && (this.mLockSoundId == 0));
            String str2 = Settings.System.getString(localContentResolver, "unlock_sound");
            if (str2 != null)
                this.mUnlockSoundId = this.mLockSounds.load(str2, 1);
            if ((str2 != null) && (this.mUnlockSoundId == 0));
            this.mLockSoundVolume = ((float)Math.pow(10.0D, paramContext.getResources().getInteger(17694725) / 20));
            IntentFilter localIntentFilter2 = new IntentFilter();
            localIntentFilter2.addAction("android.intent.action.USER_SWITCHED");
            localIntentFilter2.addAction("android.intent.action.USER_REMOVED");
            this.mContext.registerReceiver(this.mUserChangeReceiver, localIntentFilter2);
            return;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private void adjustStatusBarLocked()
    {
        if (this.mStatusBarManager == null)
            this.mStatusBarManager = ((StatusBarManager)this.mContext.getSystemService("statusbar"));
        if (this.mStatusBarManager == null)
        {
            Log.w("KeyguardViewMediator", "Could not get status bar manager");
            postAdjustStatusBarLocked();
            return;
        }
        if (this.mShowLockIcon)
        {
            if ((!this.mShowing) || (!isSecure()))
                break label150;
            if (!this.mShowingLockIcon)
            {
                String str = this.mContext.getString(17040630);
                this.mStatusBarManager.setIcon("secure", 17302859, 0, str);
                this.mShowingLockIcon = true;
            }
        }
        label104: int i = 0;
        if (this.mShowing)
        {
            i = 0x0 | 0x1000000;
            if (isSecure())
                break label175;
        }
        while (true)
        {
            if (isSecure())
                i |= 524288;
            this.mStatusBarManager.disable(i);
            break;
            label150: if (!this.mShowingLockIcon)
                break label104;
            this.mStatusBarManager.removeIcon("secure");
            this.mShowingLockIcon = false;
            break label104;
            label175: i |= 65536;
        }
    }

    private void adjustUserActivityLocked()
    {
        if ((!this.mShowing) || (this.mHidden));
        for (boolean bool = true; ; bool = false)
        {
            this.mRealPowerManager.enableUserActivity(bool);
            if ((!bool) && (this.mScreenOn))
                pokeWakelock();
            return;
        }
    }

    private void doKeyguardLocked()
    {
        if (!this.mExternallyEnabled);
        label30: label105: label110: label114: 
        while (true)
        {
            return;
            int i;
            boolean bool;
            if (!this.mKeyguardViewManager.isShowing())
            {
                if (SystemProperties.getBoolean("keyguard.no_require_sim", false))
                    break label105;
                i = 1;
                bool = this.mUpdateMonitor.isDeviceProvisioned();
                IccCard.State localState = this.mUpdateMonitor.getSimState();
                if ((!localState.isPinLocked()) && (((localState != IccCard.State.ABSENT) && (localState != IccCard.State.PERM_DISABLED)) || (i == 0)))
                    break label110;
            }
            for (int j = 1; ; j = 0)
            {
                if (((j == 0) && (!bool)) || ((this.mLockPatternUtils.isLockScreenDisabled()) && (j == 0)))
                    break label114;
                showLocked();
                break;
                break;
                i = 0;
                break label30;
            }
        }
    }

    private void handleHide()
    {
        try
        {
            if (this.mWakeAndHandOff.isHeld())
            {
                Log.w("KeyguardViewMediator", "attempt to hide the keyguard while waking, ignored");
            }
            else
            {
                if (TelephonyManager.EXTRA_STATE_IDLE.equals(this.mPhoneState))
                    playSounds(false);
                this.mKeyguardViewManager.hide();
                this.mShowing = false;
                updateActivityLockScreenState();
                adjustUserActivityLocked();
                adjustStatusBarLocked();
            }
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private void handleKeyguardDone(boolean paramBoolean)
    {
        handleHide();
        if (paramBoolean)
            this.mPM.userActivity(SystemClock.uptimeMillis(), true);
        this.mWakeLock.release();
        this.mContext.sendBroadcast(this.mUserPresentIntent);
    }

    private void handleKeyguardDoneDrawing()
    {
        try
        {
            if (this.mWaitingUntilKeyguardVisible)
            {
                this.mWaitingUntilKeyguardVisible = false;
                notifyAll();
                this.mHandler.removeMessages(10);
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private void handleNotifyScreenOff()
    {
        try
        {
            this.mKeyguardViewManager.onScreenTurnedOff();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private void handleNotifyScreenOn(KeyguardViewManager.ShowListener paramShowListener)
    {
        try
        {
            this.mKeyguardViewManager.onScreenTurnedOn(paramShowListener);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private void handleReset()
    {
        try
        {
            this.mKeyguardViewManager.reset();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private void handleSetHidden(boolean paramBoolean)
    {
        try
        {
            if (this.mHidden != paramBoolean)
            {
                this.mHidden = paramBoolean;
                updateActivityLockScreenState();
                adjustUserActivityLocked();
                adjustStatusBarLocked();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    // ERROR //
    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private void handleShow()
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 572	com/android/internal/policy/impl/KeyguardViewMediator:mSystemReady	Z
        //     6: ifne +8 -> 14
        //     9: aload_0
        //     10: monitorexit
        //     11: goto +64 -> 75
        //     14: aload_0
        //     15: iconst_1
        //     16: invokespecial 526	com/android/internal/policy/impl/KeyguardViewMediator:playSounds	(Z)V
        //     19: aload_0
        //     20: getfield 267	com/android/internal/policy/impl/KeyguardViewMediator:mKeyguardViewManager	Lcom/android/internal/policy/impl/KeyguardViewManager;
        //     23: invokevirtual 575	com/android/internal/policy/impl/KeyguardViewManager:show	()V
        //     26: aload_0
        //     27: iconst_1
        //     28: putfield 140	com/android/internal/policy/impl/KeyguardViewMediator:mShowing	Z
        //     31: aload_0
        //     32: invokespecial 532	com/android/internal/policy/impl/KeyguardViewMediator:updateActivityLockScreenState	()V
        //     35: aload_0
        //     36: invokespecial 534	com/android/internal/policy/impl/KeyguardViewMediator:adjustUserActivityLocked	()V
        //     39: aload_0
        //     40: invokespecial 346	com/android/internal/policy/impl/KeyguardViewMediator:adjustStatusBarLocked	()V
        //     43: invokestatic 580	android/app/ActivityManagerNative:getDefault	()Landroid/app/IActivityManager;
        //     46: ldc_w 582
        //     49: invokeinterface 587 2 0
        //     54: aload_0
        //     55: getfield 202	com/android/internal/policy/impl/KeyguardViewMediator:mShowKeyguardWakeLock	Landroid/os/PowerManager$WakeLock;
        //     58: invokevirtual 547	android/os/PowerManager$WakeLock:release	()V
        //     61: aload_0
        //     62: monitorexit
        //     63: goto +12 -> 75
        //     66: astore_1
        //     67: aload_0
        //     68: monitorexit
        //     69: aload_1
        //     70: athrow
        //     71: astore_2
        //     72: goto -18 -> 54
        //     75: return
        //
        // Exception table:
        //     from	to	target	type
        //     2	43	66	finally
        //     43	54	66	finally
        //     54	69	66	finally
        //     43	54	71	android/os/RemoteException
    }

    private void handleTimeout(int paramInt)
    {
        try
        {
            if (paramInt == this.mWakelockSequence)
                this.mWakeLock.release();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private void handleVerifyUnlock()
    {
        try
        {
            this.mKeyguardViewManager.verifyUnlock();
            this.mShowing = true;
            updateActivityLockScreenState();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private void handleWakeWhenReady(int paramInt)
    {
        try
        {
            if (!this.mKeyguardViewManager.wakeWhenReadyTq(paramInt))
            {
                Log.w("KeyguardViewMediator", "mKeyguardViewManager.wakeWhenReadyTq did not poke wake lock, so poke it ourselves");
                pokeWakelock();
            }
            this.mWakeAndHandOff.release();
            if (!this.mWakeLock.isHeld())
                Log.w("KeyguardViewMediator", "mWakeLock not held in mKeyguardViewManager.wakeWhenReadyTq");
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private void hideLocked()
    {
        Message localMessage = this.mHandler.obtainMessage(3);
        this.mHandler.sendMessage(localMessage);
    }

    private boolean isWakeKeyWhenKeyguardShowing(int paramInt, boolean paramBoolean)
    {
        switch (paramInt)
        {
        default:
        case 24:
        case 25:
        case 164:
        case 27:
        case 79:
        case 85:
        case 86:
        case 87:
        case 88:
        case 89:
        case 90:
        case 91:
        case 126:
        case 127:
        case 130:
        }
        for (paramBoolean = true; ; paramBoolean = false)
            return paramBoolean;
    }

    private void notifyScreenOffLocked()
    {
        this.mHandler.sendEmptyMessage(6);
    }

    private void notifyScreenOnLocked(KeyguardViewManager.ShowListener paramShowListener)
    {
        Message localMessage = this.mHandler.obtainMessage(7, paramShowListener);
        this.mHandler.sendMessage(localMessage);
    }

    private void onUserRemoved(int paramInt)
    {
        this.mLockPatternUtils.removeUser(paramInt);
    }

    private void onUserSwitched(int paramInt)
    {
        this.mLockPatternUtils.setCurrentUser(paramInt);
        try
        {
            resetStateLocked();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private void playSounds(boolean paramBoolean)
    {
        if (this.mSuppressNextLockSound)
        {
            this.mSuppressNextLockSound = false;
            return;
        }
        if (Settings.System.getInt(this.mContext.getContentResolver(), "lockscreen_sounds_enabled", 1) == 1)
            if (!paramBoolean)
                break label133;
        label133: for (int i = this.mLockSoundId; ; i = this.mUnlockSoundId)
        {
            this.mLockSounds.stop(this.mLockSoundStreamId);
            if (this.mAudioManager == null)
            {
                this.mAudioManager = ((AudioManager)this.mContext.getSystemService("audio"));
                if (this.mAudioManager == null)
                    break;
                this.mMasterStreamType = this.mAudioManager.getMasterStreamType();
            }
            if (this.mAudioManager.isStreamMute(this.mMasterStreamType))
                break;
            this.mLockSoundStreamId = this.mLockSounds.play(i, this.mLockSoundVolume, this.mLockSoundVolume, 1, 0, 1.0F);
            break;
            break;
        }
    }

    private void resetStateLocked()
    {
        Message localMessage = this.mHandler.obtainMessage(4);
        this.mHandler.sendMessage(localMessage);
    }

    private void showLocked()
    {
        this.mShowKeyguardWakeLock.acquire();
        Message localMessage = this.mHandler.obtainMessage(2);
        this.mHandler.sendMessage(localMessage);
    }

    private void updateActivityLockScreenState()
    {
        try
        {
            IActivityManager localIActivityManager = ActivityManagerNative.getDefault();
            if ((this.mShowing) && (!this.mHidden));
            for (boolean bool = true; ; bool = false)
            {
                localIActivityManager.setLockScreenShown(bool);
                label27: return;
            }
        }
        catch (RemoteException localRemoteException)
        {
            break label27;
        }
    }

    private void verifyUnlockLocked()
    {
        this.mHandler.sendEmptyMessage(5);
    }

    private void wakeWhenReadyLocked(int paramInt)
    {
        this.mWakeAndHandOff.acquire();
        Message localMessage = this.mHandler.obtainMessage(8, paramInt, 0);
        this.mHandler.sendMessage(localMessage);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void callNotifyScreenOffLocked()
    {
        notifyScreenOffLocked();
    }

    public void doKeyguardTimeout()
    {
        this.mHandler.removeMessages(13);
        Message localMessage = this.mHandler.obtainMessage(13);
        this.mHandler.sendMessage(localMessage);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    Context getContext()
    {
        return this.mContext;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    StatusBarManager getStatusBarManager()
    {
        return this.mStatusBarManager;
    }

    public boolean isInputRestricted()
    {
        if ((this.mShowing) || (this.mNeedToReshowWhenReenabled) || (!this.mUpdateMonitor.isDeviceProvisioned()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isSecure()
    {
        return this.mKeyguardViewProperties.isSecure();
    }

    public boolean isShowing()
    {
        return this.mShowing;
    }

    public boolean isShowingAndNotHidden()
    {
        if ((this.mShowing) && (!this.mHidden));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void keyguardDone(boolean paramBoolean)
    {
        keyguardDone(paramBoolean, true);
    }

    public void keyguardDone(boolean paramBoolean1, boolean paramBoolean2)
    {
        int i = 1;
        while (true)
        {
            try
            {
                EventLog.writeEvent(70000, 2);
                Message localMessage = this.mHandler.obtainMessage(9);
                if (paramBoolean2)
                {
                    localMessage.arg1 = i;
                    this.mHandler.sendMessage(localMessage);
                    if (paramBoolean1)
                        this.mUpdateMonitor.clearFailedAttempts();
                    if (this.mExitSecureCallback != null)
                    {
                        this.mExitSecureCallback.onKeyguardExitResult(paramBoolean1);
                        this.mExitSecureCallback = null;
                        if (paramBoolean1)
                        {
                            this.mExternallyEnabled = true;
                            this.mNeedToReshowWhenReenabled = false;
                        }
                    }
                    return;
                }
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
            i = 0;
        }
    }

    public void keyguardDoneDrawing()
    {
        this.mHandler.sendEmptyMessage(10);
    }

    // ERROR //
    public void onScreenTurnedOff(int paramInt)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: iconst_0
        //     4: putfield 144	com/android/internal/policy/impl/KeyguardViewMediator:mScreenOn	Z
        //     7: aload_0
        //     8: getfield 247	com/android/internal/policy/impl/KeyguardViewMediator:mLockPatternUtils	Lcom/android/internal/widget/LockPatternUtils;
        //     11: invokevirtual 709	com/android/internal/widget/LockPatternUtils:getPowerButtonInstantlyLocks	()Z
        //     14: ifne +255 -> 269
        //     17: aload_0
        //     18: getfield 247	com/android/internal/policy/impl/KeyguardViewMediator:mLockPatternUtils	Lcom/android/internal/widget/LockPatternUtils;
        //     21: invokevirtual 710	com/android/internal/widget/LockPatternUtils:isSecure	()Z
        //     24: ifne +250 -> 274
        //     27: goto +242 -> 269
        //     30: aload_0
        //     31: getfield 700	com/android/internal/policy/impl/KeyguardViewMediator:mExitSecureCallback	Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;
        //     34: ifnull +32 -> 66
        //     37: aload_0
        //     38: getfield 700	com/android/internal/policy/impl/KeyguardViewMediator:mExitSecureCallback	Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;
        //     41: iconst_0
        //     42: invokeinterface 705 2 0
        //     47: aload_0
        //     48: aconst_null
        //     49: putfield 700	com/android/internal/policy/impl/KeyguardViewMediator:mExitSecureCallback	Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;
        //     52: aload_0
        //     53: getfield 136	com/android/internal/policy/impl/KeyguardViewMediator:mExternallyEnabled	Z
        //     56: ifne +7 -> 63
        //     59: aload_0
        //     60: invokespecial 712	com/android/internal/policy/impl/KeyguardViewMediator:hideLocked	()V
        //     63: aload_0
        //     64: monitorexit
        //     65: return
        //     66: aload_0
        //     67: getfield 140	com/android/internal/policy/impl/KeyguardViewMediator:mShowing	Z
        //     70: ifeq +19 -> 89
        //     73: aload_0
        //     74: invokespecial 669	com/android/internal/policy/impl/KeyguardViewMediator:notifyScreenOffLocked	()V
        //     77: aload_0
        //     78: invokespecial 628	com/android/internal/policy/impl/KeyguardViewMediator:resetStateLocked	()V
        //     81: goto -18 -> 63
        //     84: astore_2
        //     85: aload_0
        //     86: monitorexit
        //     87: aload_2
        //     88: athrow
        //     89: iload_1
        //     90: iconst_3
        //     91: if_icmpeq +12 -> 103
        //     94: iload_1
        //     95: iconst_2
        //     96: if_icmpne +161 -> 257
        //     99: iload_3
        //     100: ifne +157 -> 257
        //     103: aload_0
        //     104: getfield 169	com/android/internal/policy/impl/KeyguardViewMediator:mContext	Landroid/content/Context;
        //     107: invokevirtual 284	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
        //     110: astore 4
        //     112: aload 4
        //     114: ldc_w 714
        //     117: sipush 30000
        //     120: invokestatic 292	android/provider/Settings$System:getInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)I
        //     123: i2l
        //     124: lstore 5
        //     126: aload 4
        //     128: ldc_w 716
        //     131: sipush 5000
        //     134: invokestatic 719	android/provider/Settings$Secure:getInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)I
        //     137: i2l
        //     138: lstore 7
        //     140: aload_0
        //     141: getfield 247	com/android/internal/policy/impl/KeyguardViewMediator:mLockPatternUtils	Lcom/android/internal/widget/LockPatternUtils;
        //     144: invokevirtual 723	com/android/internal/widget/LockPatternUtils:getDevicePolicyManager	()Landroid/app/admin/DevicePolicyManager;
        //     147: aconst_null
        //     148: invokevirtual 729	android/app/admin/DevicePolicyManager:getMaximumTimeToLock	(Landroid/content/ComponentName;)J
        //     151: lstore 9
        //     153: lload 9
        //     155: lconst_0
        //     156: lcmp
        //     157: ifle +122 -> 279
        //     160: lload 9
        //     162: lload 5
        //     164: lconst_0
        //     165: invokestatic 733	java/lang/Math:max	(JJ)J
        //     168: lsub
        //     169: lload 7
        //     171: invokestatic 736	java/lang/Math:min	(JJ)J
        //     174: lstore 11
        //     176: lload 11
        //     178: lconst_0
        //     179: lcmp
        //     180: ifgt +15 -> 195
        //     183: aload_0
        //     184: iconst_1
        //     185: putfield 134	com/android/internal/policy/impl/KeyguardViewMediator:mSuppressNextLockSound	Z
        //     188: aload_0
        //     189: invokespecial 419	com/android/internal/policy/impl/KeyguardViewMediator:doKeyguardLocked	()V
        //     192: goto -129 -> 63
        //     195: lload 11
        //     197: invokestatic 739	android/os/SystemClock:elapsedRealtime	()J
        //     200: ladd
        //     201: lstore 13
        //     203: new 269	android/content/Intent
        //     206: dup
        //     207: ldc 29
        //     209: invokespecial 273	android/content/Intent:<init>	(Ljava/lang/String;)V
        //     212: astore 15
        //     214: aload 15
        //     216: ldc_w 741
        //     219: aload_0
        //     220: getfield 413	com/android/internal/policy/impl/KeyguardViewMediator:mDelayedShowingSequence	I
        //     223: invokevirtual 745	android/content/Intent:putExtra	(Ljava/lang/String;I)Landroid/content/Intent;
        //     226: pop
        //     227: aload_0
        //     228: getfield 169	com/android/internal/policy/impl/KeyguardViewMediator:mContext	Landroid/content/Context;
        //     231: iconst_0
        //     232: aload 15
        //     234: ldc_w 746
        //     237: invokestatic 752	android/app/PendingIntent:getBroadcast	(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
        //     240: astore 17
        //     242: aload_0
        //     243: getfield 225	com/android/internal/policy/impl/KeyguardViewMediator:mAlarmManager	Landroid/app/AlarmManager;
        //     246: iconst_2
        //     247: lload 13
        //     249: aload 17
        //     251: invokevirtual 756	android/app/AlarmManager:set	(IJLandroid/app/PendingIntent;)V
        //     254: goto -191 -> 63
        //     257: iload_1
        //     258: iconst_4
        //     259: if_icmpeq -196 -> 63
        //     262: aload_0
        //     263: invokespecial 419	com/android/internal/policy/impl/KeyguardViewMediator:doKeyguardLocked	()V
        //     266: goto -203 -> 63
        //     269: iconst_1
        //     270: istore_3
        //     271: goto -241 -> 30
        //     274: iconst_0
        //     275: istore_3
        //     276: goto -246 -> 30
        //     279: lload 7
        //     281: lstore 11
        //     283: goto -107 -> 176
        //
        // Exception table:
        //     from	to	target	type
        //     2	87	84	finally
        //     103	266	84	finally
    }

    public void onScreenTurnedOn(KeyguardViewManager.ShowListener paramShowListener)
    {
        try
        {
            this.mScreenOn = true;
            this.mDelayedShowingSequence = (1 + this.mDelayedShowingSequence);
            if (paramShowListener != null)
                notifyScreenOnLocked(paramShowListener);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    // ERROR //
    public void onSimStateChanged(IccCard.State paramState)
    {
        // Byte code:
        //     0: getstatic 764	com/android/internal/policy/impl/KeyguardViewMediator$5:$SwitchMap$com$android$internal$telephony$IccCard$State	[I
        //     3: aload_1
        //     4: invokevirtual 767	com/android/internal/telephony/IccCard$State:ordinal	()I
        //     7: iaload
        //     8: tableswitch	default:+36 -> 44, 1:+37->45, 2:+79->87, 3:+79->87, 4:+111->119, 5:+141->149
        //     45: aload_0
        //     46: monitorenter
        //     47: aload_0
        //     48: getfield 234	com/android/internal/policy/impl/KeyguardViewMediator:mUpdateMonitor	Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;
        //     51: invokevirtual 490	com/android/internal/policy/impl/KeyguardUpdateMonitor:isDeviceProvisioned	()Z
        //     54: ifne +14 -> 68
        //     57: aload_0
        //     58: invokevirtual 768	com/android/internal/policy/impl/KeyguardViewMediator:isShowing	()Z
        //     61: ifne +19 -> 80
        //     64: aload_0
        //     65: invokespecial 419	com/android/internal/policy/impl/KeyguardViewMediator:doKeyguardLocked	()V
        //     68: aload_0
        //     69: monitorexit
        //     70: goto -26 -> 44
        //     73: astore 5
        //     75: aload_0
        //     76: monitorexit
        //     77: aload 5
        //     79: athrow
        //     80: aload_0
        //     81: invokespecial 628	com/android/internal/policy/impl/KeyguardViewMediator:resetStateLocked	()V
        //     84: goto -16 -> 68
        //     87: aload_0
        //     88: monitorenter
        //     89: aload_0
        //     90: invokevirtual 768	com/android/internal/policy/impl/KeyguardViewMediator:isShowing	()Z
        //     93: ifne +19 -> 112
        //     96: aload_0
        //     97: invokespecial 419	com/android/internal/policy/impl/KeyguardViewMediator:doKeyguardLocked	()V
        //     100: aload_0
        //     101: monitorexit
        //     102: goto -58 -> 44
        //     105: astore 4
        //     107: aload_0
        //     108: monitorexit
        //     109: aload 4
        //     111: athrow
        //     112: aload_0
        //     113: invokespecial 628	com/android/internal/policy/impl/KeyguardViewMediator:resetStateLocked	()V
        //     116: goto -16 -> 100
        //     119: aload_0
        //     120: monitorenter
        //     121: aload_0
        //     122: invokevirtual 768	com/android/internal/policy/impl/KeyguardViewMediator:isShowing	()Z
        //     125: ifne +17 -> 142
        //     128: aload_0
        //     129: invokespecial 419	com/android/internal/policy/impl/KeyguardViewMediator:doKeyguardLocked	()V
        //     132: aload_0
        //     133: monitorexit
        //     134: goto -90 -> 44
        //     137: astore_3
        //     138: aload_0
        //     139: monitorexit
        //     140: aload_3
        //     141: athrow
        //     142: aload_0
        //     143: invokespecial 628	com/android/internal/policy/impl/KeyguardViewMediator:resetStateLocked	()V
        //     146: goto -14 -> 132
        //     149: aload_0
        //     150: monitorenter
        //     151: aload_0
        //     152: invokevirtual 768	com/android/internal/policy/impl/KeyguardViewMediator:isShowing	()Z
        //     155: ifeq +7 -> 162
        //     158: aload_0
        //     159: invokespecial 628	com/android/internal/policy/impl/KeyguardViewMediator:resetStateLocked	()V
        //     162: aload_0
        //     163: monitorexit
        //     164: goto -120 -> 44
        //     167: astore_2
        //     168: aload_0
        //     169: monitorexit
        //     170: aload_2
        //     171: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     47	77	73	finally
        //     80	84	73	finally
        //     89	109	105	finally
        //     112	116	105	finally
        //     121	140	137	finally
        //     142	146	137	finally
        //     151	170	167	finally
    }

    public void onSystemReady()
    {
        try
        {
            this.mSystemReady = true;
            doKeyguardLocked();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public boolean onWakeKeyWhenKeyguardShowingTq(int paramInt, boolean paramBoolean)
    {
        if (isWakeKeyWhenKeyguardShowing(paramInt, paramBoolean))
            wakeWhenReadyLocked(paramInt);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean onWakeMotionWhenKeyguardShowingTq()
    {
        wakeWhenReadyLocked(0);
        return true;
    }

    public void pokeWakelock()
    {
        pokeWakelock(10000);
    }

    public void pokeWakelock(int paramInt)
    {
        try
        {
            this.mWakeLock.acquire();
            this.mHandler.removeMessages(1);
            this.mWakelockSequence = (1 + this.mWakelockSequence);
            Message localMessage = this.mHandler.obtainMessage(1, this.mWakelockSequence, 0);
            this.mHandler.sendMessageDelayed(localMessage, paramInt);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void postAdjustStatusBarLocked()
    {
    }

    public void setHidden(boolean paramBoolean)
    {
        this.mHandler.removeMessages(12);
        Handler localHandler = this.mHandler;
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            Message localMessage = localHandler.obtainMessage(12, i, 0);
            this.mHandler.sendMessage(localMessage);
            return;
        }
    }

    // ERROR //
    public void setKeyguardEnabled(boolean paramBoolean)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: iload_1
        //     4: putfield 136	com/android/internal/policy/impl/KeyguardViewMediator:mExternallyEnabled	Z
        //     7: iload_1
        //     8: ifne +41 -> 49
        //     11: aload_0
        //     12: getfield 140	com/android/internal/policy/impl/KeyguardViewMediator:mShowing	Z
        //     15: ifeq +34 -> 49
        //     18: aload_0
        //     19: getfield 700	com/android/internal/policy/impl/KeyguardViewMediator:mExitSecureCallback	Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;
        //     22: ifnull +8 -> 30
        //     25: aload_0
        //     26: monitorexit
        //     27: goto +118 -> 145
        //     30: aload_0
        //     31: iconst_1
        //     32: putfield 138	com/android/internal/policy/impl/KeyguardViewMediator:mNeedToReshowWhenReenabled	Z
        //     35: aload_0
        //     36: invokespecial 712	com/android/internal/policy/impl/KeyguardViewMediator:hideLocked	()V
        //     39: aload_0
        //     40: monitorexit
        //     41: goto +104 -> 145
        //     44: astore_2
        //     45: aload_0
        //     46: monitorexit
        //     47: aload_2
        //     48: athrow
        //     49: iload_1
        //     50: ifeq -11 -> 39
        //     53: aload_0
        //     54: getfield 138	com/android/internal/policy/impl/KeyguardViewMediator:mNeedToReshowWhenReenabled	Z
        //     57: ifeq -18 -> 39
        //     60: aload_0
        //     61: iconst_0
        //     62: putfield 138	com/android/internal/policy/impl/KeyguardViewMediator:mNeedToReshowWhenReenabled	Z
        //     65: aload_0
        //     66: getfield 700	com/android/internal/policy/impl/KeyguardViewMediator:mExitSecureCallback	Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;
        //     69: ifnull +25 -> 94
        //     72: aload_0
        //     73: getfield 700	com/android/internal/policy/impl/KeyguardViewMediator:mExitSecureCallback	Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;
        //     76: iconst_0
        //     77: invokeinterface 705 2 0
        //     82: aload_0
        //     83: aconst_null
        //     84: putfield 700	com/android/internal/policy/impl/KeyguardViewMediator:mExitSecureCallback	Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;
        //     87: aload_0
        //     88: invokespecial 628	com/android/internal/policy/impl/KeyguardViewMediator:resetStateLocked	()V
        //     91: goto -52 -> 39
        //     94: aload_0
        //     95: invokespecial 512	com/android/internal/policy/impl/KeyguardViewMediator:showLocked	()V
        //     98: aload_0
        //     99: iconst_1
        //     100: putfield 153	com/android/internal/policy/impl/KeyguardViewMediator:mWaitingUntilKeyguardVisible	Z
        //     103: aload_0
        //     104: getfield 167	com/android/internal/policy/impl/KeyguardViewMediator:mHandler	Landroid/os/Handler;
        //     107: bipush 10
        //     109: ldc2_w 786
        //     112: invokevirtual 791	android/os/Handler:sendEmptyMessageDelayed	(IJ)Z
        //     115: pop
        //     116: aload_0
        //     117: getfield 153	com/android/internal/policy/impl/KeyguardViewMediator:mWaitingUntilKeyguardVisible	Z
        //     120: istore 4
        //     122: iload 4
        //     124: ifeq -85 -> 39
        //     127: aload_0
        //     128: invokevirtual 794	java/lang/Object:wait	()V
        //     131: goto -15 -> 116
        //     134: astore 5
        //     136: invokestatic 800	java/lang/Thread:currentThread	()Ljava/lang/Thread;
        //     139: invokevirtual 803	java/lang/Thread:interrupt	()V
        //     142: goto -26 -> 116
        //     145: return
        //
        // Exception table:
        //     from	to	target	type
        //     2	47	44	finally
        //     53	122	44	finally
        //     127	131	44	finally
        //     136	142	44	finally
        //     127	131	134	java/lang/InterruptedException
    }

    // ERROR //
    public void verifyUnlock(WindowManagerPolicy.OnKeyguardExitResult paramOnKeyguardExitResult)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 234	com/android/internal/policy/impl/KeyguardViewMediator:mUpdateMonitor	Lcom/android/internal/policy/impl/KeyguardUpdateMonitor;
        //     6: invokevirtual 490	com/android/internal/policy/impl/KeyguardUpdateMonitor:isDeviceProvisioned	()Z
        //     9: ifne +13 -> 22
        //     12: aload_1
        //     13: iconst_0
        //     14: invokeinterface 705 2 0
        //     19: aload_0
        //     20: monitorexit
        //     21: return
        //     22: aload_0
        //     23: getfield 136	com/android/internal/policy/impl/KeyguardViewMediator:mExternallyEnabled	Z
        //     26: ifeq +27 -> 53
        //     29: ldc 60
        //     31: ldc_w 806
        //     34: invokestatic 438	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     37: pop
        //     38: aload_1
        //     39: iconst_0
        //     40: invokeinterface 705 2 0
        //     45: goto -26 -> 19
        //     48: astore_2
        //     49: aload_0
        //     50: monitorexit
        //     51: aload_2
        //     52: athrow
        //     53: aload_0
        //     54: getfield 700	com/android/internal/policy/impl/KeyguardViewMediator:mExitSecureCallback	Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;
        //     57: ifnull +13 -> 70
        //     60: aload_1
        //     61: iconst_0
        //     62: invokeinterface 705 2 0
        //     67: goto -48 -> 19
        //     70: aload_0
        //     71: aload_1
        //     72: putfield 700	com/android/internal/policy/impl/KeyguardViewMediator:mExitSecureCallback	Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;
        //     75: aload_0
        //     76: invokespecial 808	com/android/internal/policy/impl/KeyguardViewMediator:verifyUnlockLocked	()V
        //     79: goto -60 -> 19
        //
        // Exception table:
        //     from	to	target	type
        //     2	51	48	finally
        //     53	79	48	finally
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.KeyguardViewMediator
 * JD-Core Version:        0.6.2
 */