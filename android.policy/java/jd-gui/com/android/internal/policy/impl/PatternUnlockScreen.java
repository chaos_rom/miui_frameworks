package com.android.internal.policy.impl;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.security.KeyStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.android.internal.widget.LinearLayoutWithDefaultTouchRecepient;
import com.android.internal.widget.LockPatternUtils;
import com.android.internal.widget.LockPatternView;
import com.android.internal.widget.LockPatternView.Cell;
import com.android.internal.widget.LockPatternView.DisplayMode;
import com.android.internal.widget.LockPatternView.OnPatternListener;
import java.util.List;

class PatternUnlockScreen extends LinearLayoutWithDefaultTouchRecepient
    implements KeyguardScreen
{
    private static final boolean DEBUG = false;
    private static final int MIN_PATTERN_BEFORE_POKE_WAKELOCK = 2;
    private static final int PATTERN_CLEAR_TIMEOUT_MS = 2000;
    private static final String TAG = "UnlockScreen";
    private static final int UNLOCK_PATTERN_WAKE_INTERVAL_FIRST_DOTS_MS = 2000;
    private static final int UNLOCK_PATTERN_WAKE_INTERVAL_MS = 7000;
    private KeyguardScreenCallback mCallback;
    private Runnable mCancelPatternRunnable = new Runnable()
    {
        public void run()
        {
            PatternUnlockScreen.this.mLockPatternView.clearPattern();
        }
    };
    private CountDownTimer mCountdownTimer = null;
    private int mCreationOrientation;
    private boolean mEnableFallback;
    private int mFailedPatternAttemptsSinceLastTimeout = 0;
    private Button mForgotPatternButton;
    private final View.OnClickListener mForgotPatternClick = new View.OnClickListener()
    {
        public void onClick(View paramAnonymousView)
        {
            PatternUnlockScreen.this.mCallback.forgotPattern(true);
        }
    };
    private KeyguardStatusViewManager mKeyguardStatusViewManager;
    private long mLastPokeTime = -7000L;
    private LockPatternUtils mLockPatternUtils;
    private LockPatternView mLockPatternView;
    private int mTotalFailedPatternAttempts = 0;
    private KeyguardUpdateMonitor mUpdateMonitor;

    PatternUnlockScreen(Context paramContext, Configuration paramConfiguration, LockPatternUtils paramLockPatternUtils, KeyguardUpdateMonitor paramKeyguardUpdateMonitor, KeyguardScreenCallback paramKeyguardScreenCallback, int paramInt)
    {
        super(paramContext);
        this.mLockPatternUtils = paramLockPatternUtils;
        this.mUpdateMonitor = paramKeyguardUpdateMonitor;
        this.mCallback = paramKeyguardScreenCallback;
        this.mTotalFailedPatternAttempts = paramInt;
        this.mFailedPatternAttemptsSinceLastTimeout = (paramInt % 5);
        this.mCreationOrientation = paramConfiguration.orientation;
        LayoutInflater localLayoutInflater = LayoutInflater.from(paramContext);
        LockPatternView localLockPatternView;
        if (this.mCreationOrientation != 2)
        {
            Log.d("UnlockScreen", "portrait mode");
            localLayoutInflater.inflate(17367131, this, true);
            this.mKeyguardStatusViewManager = new KeyguardStatusViewManager(this, this.mUpdateMonitor, this.mLockPatternUtils, this.mCallback, true);
            this.mLockPatternView = ((LockPatternView)findViewById(16908997));
            this.mForgotPatternButton = ((Button)findViewById(16908996));
            this.mForgotPatternButton.setText(17040157);
            this.mForgotPatternButton.setOnClickListener(this.mForgotPatternClick);
            setDefaultTouchRecepient(this.mLockPatternView);
            this.mLockPatternView.setSaveEnabled(false);
            this.mLockPatternView.setFocusable(false);
            this.mLockPatternView.setOnPatternListener(new UnlockPatternListener(null));
            localLockPatternView = this.mLockPatternView;
            if (this.mLockPatternUtils.isVisiblePatternEnabled())
                break label307;
        }
        label307: for (boolean bool = true; ; bool = false)
        {
            localLockPatternView.setInStealthMode(bool);
            this.mLockPatternView.setTactileFeedbackEnabled(this.mLockPatternUtils.isTactileFeedbackEnabled());
            updateFooter(FooterMode.Normal);
            setFocusableInTouchMode(true);
            return;
            Log.d("UnlockScreen", "landscape mode");
            localLayoutInflater.inflate(17367130, this, true);
            break;
        }
    }

    private void handleAttemptLockout(long paramLong)
    {
        this.mLockPatternView.clearPattern();
        this.mLockPatternView.setEnabled(false);
        this.mCountdownTimer = new CountDownTimer(paramLong - SystemClock.elapsedRealtime(), 1000L)
        {
            public void onFinish()
            {
                PatternUnlockScreen.this.mLockPatternView.setEnabled(true);
                PatternUnlockScreen.this.mKeyguardStatusViewManager.setInstructionText(PatternUnlockScreen.this.getContext().getString(17040122));
                PatternUnlockScreen.this.mKeyguardStatusViewManager.updateStatusLines(true);
                PatternUnlockScreen.access$702(PatternUnlockScreen.this, 0);
                if (PatternUnlockScreen.this.mEnableFallback)
                    PatternUnlockScreen.this.updateFooter(PatternUnlockScreen.FooterMode.ForgotLockPattern);
                while (true)
                {
                    return;
                    PatternUnlockScreen.this.updateFooter(PatternUnlockScreen.FooterMode.Normal);
                }
            }

            public void onTick(long paramAnonymousLong)
            {
                int i = (int)(paramAnonymousLong / 1000L);
                KeyguardStatusViewManager localKeyguardStatusViewManager = PatternUnlockScreen.this.mKeyguardStatusViewManager;
                Context localContext = PatternUnlockScreen.this.getContext();
                Object[] arrayOfObject = new Object[1];
                arrayOfObject[0] = Integer.valueOf(i);
                localKeyguardStatusViewManager.setInstructionText(localContext.getString(17040156, arrayOfObject));
                PatternUnlockScreen.this.mKeyguardStatusViewManager.updateStatusLines(true);
            }
        }
        .start();
    }

    private void hideForgotPatternButton()
    {
        this.mForgotPatternButton.setVisibility(8);
    }

    private void showForgotPatternButton()
    {
        this.mForgotPatternButton.setVisibility(0);
    }

    private void updateFooter(FooterMode paramFooterMode)
    {
        switch (4.$SwitchMap$com$android$internal$policy$impl$PatternUnlockScreen$FooterMode[paramFooterMode.ordinal()])
        {
        default:
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            return;
            hideForgotPatternButton();
            continue;
            showForgotPatternButton();
            continue;
            hideForgotPatternButton();
        }
    }

    public void cleanUp()
    {
        this.mUpdateMonitor.removeCallback(this);
        this.mLockPatternUtils = null;
        this.mUpdateMonitor = null;
        this.mCallback = null;
        this.mLockPatternView.setOnPatternListener(null);
    }

    public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
    {
        boolean bool = super.dispatchTouchEvent(paramMotionEvent);
        if ((bool) && (SystemClock.elapsedRealtime() - this.mLastPokeTime > 6900L))
            this.mLastPokeTime = SystemClock.elapsedRealtime();
        return bool;
    }

    public boolean needsInput()
    {
        return false;
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        if (getResources().getConfiguration().orientation != this.mCreationOrientation)
            this.mCallback.recreateMe(getResources().getConfiguration());
    }

    protected void onConfigurationChanged(Configuration paramConfiguration)
    {
        super.onConfigurationChanged(paramConfiguration);
        if (paramConfiguration.orientation != this.mCreationOrientation)
            this.mCallback.recreateMe(paramConfiguration);
    }

    public void onKeyboardChange(boolean paramBoolean)
    {
    }

    public void onPause()
    {
        if (this.mCountdownTimer != null)
        {
            this.mCountdownTimer.cancel();
            this.mCountdownTimer = null;
        }
        this.mKeyguardStatusViewManager.onPause();
    }

    public void onResume()
    {
        this.mKeyguardStatusViewManager.onResume();
        this.mLockPatternView.enableInput();
        this.mLockPatternView.setEnabled(true);
        this.mLockPatternView.clearPattern();
        if (this.mCallback.doesFallbackUnlockScreenExist())
        {
            showForgotPatternButton();
            long l = this.mLockPatternUtils.getLockoutAttemptDeadline();
            if (l != 0L)
                handleAttemptLockout(l);
            if (!this.mCallback.isVerifyUnlockOnly())
                break label91;
            updateFooter(FooterMode.VerifyUnlocked);
        }
        while (true)
        {
            return;
            hideForgotPatternButton();
            break;
            label91: if ((this.mEnableFallback) && (this.mTotalFailedPatternAttempts >= 5))
                updateFooter(FooterMode.ForgotLockPattern);
            else
                updateFooter(FooterMode.Normal);
        }
    }

    public void onWindowFocusChanged(boolean paramBoolean)
    {
        super.onWindowFocusChanged(paramBoolean);
        if (paramBoolean)
            onResume();
    }

    public void setEnableFallback(boolean paramBoolean)
    {
        this.mEnableFallback = paramBoolean;
    }

    private class UnlockPatternListener
        implements LockPatternView.OnPatternListener
    {
        private UnlockPatternListener()
        {
        }

        public void onPatternCellAdded(List<LockPatternView.Cell> paramList)
        {
            if (paramList.size() > 2)
                PatternUnlockScreen.this.mCallback.pokeWakelock(7000);
            while (true)
            {
                return;
                PatternUnlockScreen.this.mCallback.pokeWakelock(2000);
            }
        }

        public void onPatternCleared()
        {
        }

        public void onPatternDetected(List<LockPatternView.Cell> paramList)
        {
            if (PatternUnlockScreen.this.mLockPatternUtils.checkPattern(paramList))
            {
                PatternUnlockScreen.this.mLockPatternView.setDisplayMode(LockPatternView.DisplayMode.Correct);
                PatternUnlockScreen.this.mKeyguardStatusViewManager.setInstructionText("");
                PatternUnlockScreen.this.mKeyguardStatusViewManager.updateStatusLines(true);
                PatternUnlockScreen.this.mCallback.keyguardDone(true);
                PatternUnlockScreen.this.mCallback.reportSuccessfulUnlockAttempt();
                KeyStore.getInstance().password(LockPatternUtils.patternToString(paramList));
            }
            label261: 
            while (true)
            {
                return;
                int i = 0;
                if (paramList.size() > 2)
                    PatternUnlockScreen.this.mCallback.pokeWakelock(7000);
                PatternUnlockScreen.this.mLockPatternView.setDisplayMode(LockPatternView.DisplayMode.Wrong);
                if (paramList.size() >= 4)
                {
                    PatternUnlockScreen.access$608(PatternUnlockScreen.this);
                    PatternUnlockScreen.access$708(PatternUnlockScreen.this);
                    i = 1;
                }
                if (PatternUnlockScreen.this.mFailedPatternAttemptsSinceLastTimeout >= 5)
                {
                    long l = PatternUnlockScreen.this.mLockPatternUtils.setLockoutAttemptDeadline();
                    PatternUnlockScreen.this.handleAttemptLockout(l);
                }
                while (true)
                {
                    if (i == 0)
                        break label261;
                    PatternUnlockScreen.this.mCallback.reportFailedUnlockAttempt();
                    break;
                    PatternUnlockScreen.this.mKeyguardStatusViewManager.setInstructionText(PatternUnlockScreen.this.getContext().getString(17040126));
                    PatternUnlockScreen.this.mKeyguardStatusViewManager.updateStatusLines(true);
                    PatternUnlockScreen.this.mLockPatternView.postDelayed(PatternUnlockScreen.this.mCancelPatternRunnable, 2000L);
                }
            }
        }

        public void onPatternStart()
        {
            PatternUnlockScreen.this.mLockPatternView.removeCallbacks(PatternUnlockScreen.this.mCancelPatternRunnable);
        }
    }

    static enum FooterMode
    {
        static
        {
            ForgotLockPattern = new FooterMode("ForgotLockPattern", 1);
            VerifyUnlocked = new FooterMode("VerifyUnlocked", 2);
            FooterMode[] arrayOfFooterMode = new FooterMode[3];
            arrayOfFooterMode[0] = Normal;
            arrayOfFooterMode[1] = ForgotLockPattern;
            arrayOfFooterMode[2] = VerifyUnlocked;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.PatternUnlockScreen
 * JD-Core Version:        0.6.2
 */