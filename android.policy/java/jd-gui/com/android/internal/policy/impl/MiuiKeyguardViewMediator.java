package com.android.internal.policy.impl;

import android.app.StatusBarManager;
import android.content.Context;
import android.os.LocalPowerManager;
import miui.app.ExtraStatusBarManager;
import miui.net.FirewallManager;

public class MiuiKeyguardViewMediator extends KeyguardViewMediator
{
    private Context mContext;

    public MiuiKeyguardViewMediator(Context paramContext, PhoneWindowManager paramPhoneWindowManager, LocalPowerManager paramLocalPowerManager)
    {
        super(paramContext, paramPhoneWindowManager, paramLocalPowerManager);
        this.mContext = paramContext;
    }

    public void onScreenTurnedOff(int paramInt)
    {
        try
        {
            callNotifyScreenOffLocked();
            FirewallManager.getInstance().removeAccessControlPass("*");
            super.onScreenTurnedOff(paramInt);
            return;
        }
        finally
        {
        }
    }

    void postAdjustStatusBarLocked()
    {
        int i;
        int j;
        if (getStatusBarManager() != null)
        {
            i = 0;
            if (isShowing())
            {
                j = 0x0 | 0x1000000;
                if (!isShowingAndNotHidden())
                    break label66;
            }
        }
        label66: for (int k = -2147483648; ; k = 0)
        {
            i = j | k;
            if ((isSecure()) || (!ExtraStatusBarManager.isExpandableUnderKeyguard(this.mContext)))
                i |= 589824;
            getStatusBarManager().disable(i);
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.MiuiKeyguardViewMediator
 * JD-Core Version:        0.6.2
 */