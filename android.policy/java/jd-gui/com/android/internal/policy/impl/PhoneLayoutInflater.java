package com.android.internal.policy.impl;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

public class PhoneLayoutInflater extends LayoutInflater
{
    private static final String[] sClassPrefixList = arrayOfString;

    static
    {
        String[] arrayOfString = new String[2];
        arrayOfString[0] = "android.widget.";
        arrayOfString[1] = "android.webkit.";
    }

    public PhoneLayoutInflater(Context paramContext)
    {
        super(paramContext);
    }

    protected PhoneLayoutInflater(LayoutInflater paramLayoutInflater, Context paramContext)
    {
        super(paramLayoutInflater, paramContext);
    }

    public LayoutInflater cloneInContext(Context paramContext)
    {
        return new PhoneLayoutInflater(this, paramContext);
    }

    protected View onCreateView(String paramString, AttributeSet paramAttributeSet)
        throws ClassNotFoundException
    {
        String[] arrayOfString = sClassPrefixList;
        int i = arrayOfString.length;
        int j = 0;
        String str;
        if (j < i)
            str = arrayOfString[j];
        while (true)
        {
            try
            {
                View localView2 = createView(paramString, str, paramAttributeSet);
                localView1 = localView2;
                if (localView1 != null)
                    return localView1;
            }
            catch (ClassNotFoundException localClassNotFoundException)
            {
                j++;
            }
            break;
            View localView1 = super.onCreateView(paramString, paramAttributeSet);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.PhoneLayoutInflater
 * JD-Core Version:        0.6.2
 */