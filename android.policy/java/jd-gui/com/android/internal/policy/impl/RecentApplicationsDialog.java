package com.android.internal.policy.impl;

import android.app.ActivityManager;
import android.app.ActivityManager.RecentTaskInfo;
import android.app.Dialog;
import android.app.StatusBarManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.TextView;
import java.util.List;

public class RecentApplicationsDialog extends Dialog
    implements View.OnClickListener
{
    private static final boolean DBG_FORCE_EMPTY_LIST = false;
    private static final int MAX_RECENT_TASKS = 16;
    private static final int NUM_BUTTONS = 8;
    private static StatusBarManager sStatusBar;
    IntentFilter mBroadcastIntentFilter = new IntentFilter("android.intent.action.CLOSE_SYSTEM_DIALOGS");
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            if (("android.intent.action.CLOSE_SYSTEM_DIALOGS".equals(paramAnonymousIntent.getAction())) && (!"recentapps".equals(paramAnonymousIntent.getStringExtra("reason"))))
                RecentApplicationsDialog.this.dismiss();
        }
    };
    Runnable mCleanup = new Runnable()
    {
        public void run()
        {
            for (TextView localTextView : RecentApplicationsDialog.this.mIcons)
            {
                localTextView.setCompoundDrawables(null, null, null, null);
                localTextView.setTag(null);
            }
        }
    };
    Handler mHandler = new Handler();
    final TextView[] mIcons = new TextView[8];
    View mNoAppsText;

    public RecentApplicationsDialog(Context paramContext)
    {
        super(paramContext, 16974579);
    }

    private void reloadButtons()
    {
        Context localContext = getContext();
        PackageManager localPackageManager = localContext.getPackageManager();
        List localList = ((ActivityManager)localContext.getSystemService("activity")).getRecentTasks(16, 2);
        ActivityInfo localActivityInfo1 = new Intent("android.intent.action.MAIN").addCategory("android.intent.category.HOME").resolveActivityInfo(localPackageManager, 0);
        IconUtilities localIconUtilities = new IconUtilities(getContext());
        int i = 0;
        int j = localList.size();
        int k = 0;
        if ((k < j) && (i < 8))
        {
            ActivityManager.RecentTaskInfo localRecentTaskInfo = (ActivityManager.RecentTaskInfo)localList.get(k);
            Intent localIntent = new Intent(localRecentTaskInfo.baseIntent);
            if (localRecentTaskInfo.origActivity != null)
                localIntent.setComponent(localRecentTaskInfo.origActivity);
            if ((localActivityInfo1 != null) && (localActivityInfo1.packageName.equals(localIntent.getComponent().getPackageName())) && (localActivityInfo1.name.equals(localIntent.getComponent().getClassName())));
            while (true)
            {
                k++;
                break;
                localIntent.setFlags(0x10000000 | 0xFFDFFFFF & localIntent.getFlags());
                ResolveInfo localResolveInfo = localPackageManager.resolveActivity(localIntent, 0);
                if (localResolveInfo != null)
                {
                    ActivityInfo localActivityInfo2 = localResolveInfo.activityInfo;
                    String str = localActivityInfo2.loadLabel(localPackageManager).toString();
                    Drawable localDrawable = localActivityInfo2.loadIcon(localPackageManager);
                    if ((str != null) && (str.length() > 0) && (localDrawable != null))
                    {
                        TextView localTextView = this.mIcons[i];
                        localTextView.setText(str);
                        localTextView.setCompoundDrawables(null, localIconUtilities.createIconDrawable(localDrawable), null, null);
                        RecentTag localRecentTag = new RecentTag();
                        localRecentTag.info = localRecentTaskInfo;
                        localRecentTag.intent = localIntent;
                        localTextView.setTag(localRecentTag);
                        localTextView.setVisibility(0);
                        localTextView.setPressed(false);
                        localTextView.clearFocus();
                        i++;
                    }
                }
            }
        }
        View localView = this.mNoAppsText;
        if (i == 0);
        for (int m = 0; ; m = 8)
        {
            localView.setVisibility(m);
            while (i < 8)
            {
                this.mIcons[i].setVisibility(8);
                i++;
            }
        }
    }

    private void switchTo(RecentTag paramRecentTag)
    {
        if (paramRecentTag.info.id >= 0)
            ((ActivityManager)getContext().getSystemService("activity")).moveTaskToFront(paramRecentTag.info.id, 1);
        while (true)
        {
            return;
            if (paramRecentTag.intent != null)
            {
                paramRecentTag.intent.addFlags(1064960);
                try
                {
                    getContext().startActivity(paramRecentTag.intent);
                }
                catch (ActivityNotFoundException localActivityNotFoundException)
                {
                    Log.w("Recent", "Unable to launch recent task", localActivityNotFoundException);
                }
            }
        }
    }

    public void dismissAndSwitch()
    {
        int i = this.mIcons.length;
        RecentTag localRecentTag = null;
        for (int j = 0; ; j++)
        {
            if ((j >= i) || (this.mIcons[j].getVisibility() != 0));
            do
            {
                if (localRecentTag != null)
                    switchTo(localRecentTag);
                dismiss();
                return;
                if ((j != 0) && (!this.mIcons[j].hasFocus()))
                    break;
                localRecentTag = (RecentTag)this.mIcons[j].getTag();
            }
            while (this.mIcons[j].hasFocus());
        }
    }

    public void onClick(View paramView)
    {
        TextView[] arrayOfTextView = this.mIcons;
        int i = arrayOfTextView.length;
        for (int j = 0; ; j++)
            if (j < i)
            {
                TextView localTextView = arrayOfTextView[j];
                if (localTextView == paramView)
                    switchTo((RecentTag)localTextView.getTag());
            }
            else
            {
                dismiss();
                return;
            }
    }

    protected void onCreate(Bundle paramBundle)
    {
        super.onCreate(paramBundle);
        Context localContext = getContext();
        if (sStatusBar == null)
            sStatusBar = (StatusBarManager)localContext.getSystemService("statusbar");
        Window localWindow = getWindow();
        localWindow.requestFeature(1);
        localWindow.setType(2008);
        localWindow.setFlags(131072, 131072);
        localWindow.setTitle("Recents");
        setContentView(17367184);
        WindowManager.LayoutParams localLayoutParams = localWindow.getAttributes();
        localLayoutParams.width = -1;
        localLayoutParams.height = -1;
        localWindow.setAttributes(localLayoutParams);
        localWindow.setFlags(0, 2);
        this.mIcons[0] = ((TextView)findViewById(16909072));
        this.mIcons[1] = ((TextView)findViewById(16908313));
        this.mIcons[2] = ((TextView)findViewById(16908314));
        this.mIcons[3] = ((TextView)findViewById(16908315));
        this.mIcons[4] = ((TextView)findViewById(16909073));
        this.mIcons[5] = ((TextView)findViewById(16909074));
        this.mIcons[6] = ((TextView)findViewById(16909075));
        this.mIcons[7] = ((TextView)findViewById(16909076));
        this.mNoAppsText = findViewById(16909071);
        TextView[] arrayOfTextView = this.mIcons;
        int i = arrayOfTextView.length;
        for (int j = 0; j < i; j++)
            arrayOfTextView[j].setOnClickListener(this);
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
        int i = 1;
        int k;
        int m;
        int n;
        if (paramInt == 61)
        {
            boolean bool = paramKeyEvent.isShiftPressed();
            int j = this.mIcons.length;
            for (k = 0; (k < j) && (this.mIcons[k].getVisibility() == 0); k++);
            if (k != 0)
            {
                if (!bool)
                    break label147;
                m = k - 1;
                n = 0;
                label69: if (n < k)
                {
                    if (!this.mIcons[n].hasFocus())
                        break label165;
                    if (!bool)
                        break label153;
                    m = (-1 + (n + k)) % k;
                }
                label107: if (!bool)
                    break label171;
                int i1 = i;
                label115: if (this.mIcons[m].requestFocus(i1))
                    this.mIcons[m].playSoundEffect(SoundEffectConstants.getContantForFocusDirection(i1));
            }
        }
        while (true)
        {
            return i;
            label147: m = 0;
            break;
            label153: m = (n + 1) % k;
            break label107;
            label165: n++;
            break label69;
            label171: int i2 = 2;
            break label115;
            i = super.onKeyDown(paramInt, paramKeyEvent);
        }
    }

    public void onStart()
    {
        super.onStart();
        reloadButtons();
        if (sStatusBar != null)
            sStatusBar.disable(65536);
        getContext().registerReceiver(this.mBroadcastReceiver, this.mBroadcastIntentFilter);
        this.mHandler.removeCallbacks(this.mCleanup);
    }

    public void onStop()
    {
        super.onStop();
        if (sStatusBar != null)
            sStatusBar.disable(0);
        getContext().unregisterReceiver(this.mBroadcastReceiver);
        this.mHandler.postDelayed(this.mCleanup, 100L);
    }

    class RecentTag
    {
        ActivityManager.RecentTaskInfo info;
        Intent intent;

        RecentTag()
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.RecentApplicationsDialog
 * JD-Core Version:        0.6.2
 */