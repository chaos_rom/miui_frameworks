package com.android.internal.policy.impl;

import android.content.Context;
import com.android.internal.telephony.IccCard.State;
import com.android.internal.widget.LockPatternUtils;

public class MiuiLockPatternKeyguardViewProperties
    implements KeyguardViewProperties
{
    private final LockPatternUtils mLockPatternUtils;
    private final KeyguardUpdateMonitor mUpdateMonitor;

    public MiuiLockPatternKeyguardViewProperties(LockPatternUtils paramLockPatternUtils, KeyguardUpdateMonitor paramKeyguardUpdateMonitor)
    {
        this.mLockPatternUtils = paramLockPatternUtils;
        this.mUpdateMonitor = paramKeyguardUpdateMonitor;
    }

    private boolean isSimPinSecure()
    {
        IccCard.State localState = this.mUpdateMonitor.getSimState();
        if ((localState == IccCard.State.PIN_REQUIRED) || (localState == IccCard.State.PUK_REQUIRED) || (localState == IccCard.State.PERM_DISABLED));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public KeyguardViewBase createKeyguardView(Context paramContext, KeyguardViewCallback paramKeyguardViewCallback, KeyguardUpdateMonitor paramKeyguardUpdateMonitor, KeyguardWindowController paramKeyguardWindowController)
    {
        return new MiuiLockPatternKeyguardView(paramContext, paramKeyguardViewCallback, paramKeyguardUpdateMonitor, this.mLockPatternUtils, paramKeyguardWindowController);
    }

    public boolean isSecure()
    {
        if ((this.mLockPatternUtils.isSecure()) || (isSimPinSecure()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.MiuiLockPatternKeyguardViewProperties
 * JD-Core Version:        0.6.2
 */