package com.android.internal.policy.impl;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.Context;
import android.util.Log;
import android.view.FallbackEventHandler;
import android.view.LayoutInflater;
import android.view.Window;
import android.view.WindowManagerPolicy;
import com.android.internal.policy.IPolicy;

public class Policy
    implements IPolicy
{
    private static final String TAG = "PhonePolicy";
    private static final String[] preload_classes;

    static
    {
        String[] arrayOfString1 = new String[7];
        arrayOfString1[0] = "com.android.internal.policy.impl.PhoneLayoutInflater";
        arrayOfString1[1] = "com.android.internal.policy.impl.PhoneWindow";
        arrayOfString1[2] = "com.android.internal.policy.impl.PhoneWindow$1";
        arrayOfString1[3] = "com.android.internal.policy.impl.PhoneWindow$ContextMenuCallback";
        arrayOfString1[4] = "com.android.internal.policy.impl.PhoneWindow$DecorView";
        arrayOfString1[5] = "com.android.internal.policy.impl.PhoneWindow$PanelFeatureState";
        arrayOfString1[6] = "com.android.internal.policy.impl.PhoneWindow$PanelFeatureState$SavedState";
        preload_classes = arrayOfString1;
        String[] arrayOfString2 = preload_classes;
        int i = arrayOfString2.length;
        int j = 0;
        while (true)
            if (j < i)
            {
                String str = arrayOfString2[j];
                try
                {
                    Class.forName(str);
                    j++;
                }
                catch (ClassNotFoundException localClassNotFoundException)
                {
                    while (true)
                        Log.e("PhonePolicy", "Could not preload class for phone policy: " + str);
                }
            }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public FallbackEventHandler makeNewFallbackEventHandler(Context paramContext)
    {
        return new MiuiPhoneFallbackEventHandler(paramContext);
    }

    public LayoutInflater makeNewLayoutInflater(Context paramContext)
    {
        return new PhoneLayoutInflater(paramContext);
    }

    public Window makeNewWindow(Context paramContext)
    {
        return new PhoneWindow(paramContext);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public WindowManagerPolicy makeNewWindowManager()
    {
        return new MiuiPhoneWindowManager();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.Policy
 * JD-Core Version:        0.6.2
 */