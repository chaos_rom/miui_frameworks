package com.android.internal.policy.impl;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BlurMaskFilter;
import android.graphics.BlurMaskFilter.Blur;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.TableMaskFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.PaintDrawable;
import android.graphics.drawable.StateListDrawable;
import android.util.DisplayMetrics;
import android.util.TypedValue;

final class IconUtilities
{
    private static final String TAG = "IconUtilities";
    private static final int[] sColors = arrayOfInt;
    private final Paint mBlurPaint = new Paint();
    private final Canvas mCanvas = new Canvas();
    private int mColorIndex = 0;
    private final DisplayMetrics mDisplayMetrics;
    private final Paint mGlowColorFocusedPaint = new Paint();
    private final Paint mGlowColorPressedPaint = new Paint();
    private int mIconHeight = -1;
    private int mIconTextureHeight = -1;
    private int mIconTextureWidth = -1;
    private int mIconWidth = -1;
    private final Rect mOldBounds = new Rect();
    private final Paint mPaint = new Paint();

    static
    {
        int[] arrayOfInt = new int[3];
        arrayOfInt[0] = -65536;
        arrayOfInt[1] = -16711936;
        arrayOfInt[2] = -16776961;
    }

    public IconUtilities(Context paramContext)
    {
        Resources localResources = paramContext.getResources();
        DisplayMetrics localDisplayMetrics = localResources.getDisplayMetrics();
        this.mDisplayMetrics = localDisplayMetrics;
        float f = 5.0F * localDisplayMetrics.density;
        int i = (int)localResources.getDimension(17104896);
        this.mIconHeight = i;
        this.mIconWidth = i;
        int j = this.mIconWidth + (int)(2.0F * f);
        this.mIconTextureHeight = j;
        this.mIconTextureWidth = j;
        this.mBlurPaint.setMaskFilter(new BlurMaskFilter(f, BlurMaskFilter.Blur.NORMAL));
        TypedValue localTypedValue = new TypedValue();
        Paint localPaint1 = this.mGlowColorPressedPaint;
        int k;
        Paint localPaint2;
        if (paramContext.getTheme().resolveAttribute(16843661, localTypedValue, true))
        {
            k = localTypedValue.data;
            localPaint1.setColor(k);
            this.mGlowColorPressedPaint.setMaskFilter(TableMaskFilter.CreateClipTable(0, 30));
            localPaint2 = this.mGlowColorFocusedPaint;
            if (!paramContext.getTheme().resolveAttribute(16843663, localTypedValue, true))
                break label332;
        }
        label332: for (int m = localTypedValue.data; ; m = -29184)
        {
            localPaint2.setColor(m);
            this.mGlowColorFocusedPaint.setMaskFilter(TableMaskFilter.CreateClipTable(0, 30));
            new ColorMatrix().setSaturation(0.2F);
            this.mCanvas.setDrawFilter(new PaintFlagsDrawFilter(4, 2));
            return;
            k = -15616;
            break;
        }
    }

    private Bitmap createIconBitmap(Drawable paramDrawable)
    {
        int i = this.mIconWidth;
        int j = this.mIconHeight;
        int k;
        int m;
        float f;
        if ((paramDrawable instanceof PaintDrawable))
        {
            PaintDrawable localPaintDrawable = (PaintDrawable)paramDrawable;
            localPaintDrawable.setIntrinsicWidth(i);
            localPaintDrawable.setIntrinsicHeight(j);
            k = paramDrawable.getIntrinsicWidth();
            m = paramDrawable.getIntrinsicHeight();
            if ((k > 0) && (k > 0))
            {
                if ((i >= k) && (j >= m))
                    break label242;
                f = k / m;
                if (k <= m)
                    break label225;
                j = (int)(i / f);
            }
        }
        while (true)
        {
            int n = this.mIconTextureWidth;
            int i1 = this.mIconTextureHeight;
            Bitmap localBitmap = Bitmap.createBitmap(n, i1, Bitmap.Config.ARGB_8888);
            Canvas localCanvas = this.mCanvas;
            localCanvas.setBitmap(localBitmap);
            int i2 = (n - i) / 2;
            int i3 = (i1 - j) / 2;
            this.mOldBounds.set(paramDrawable.getBounds());
            paramDrawable.setBounds(i2, i3, i2 + i, i3 + j);
            paramDrawable.draw(localCanvas);
            paramDrawable.setBounds(this.mOldBounds);
            return localBitmap;
            if (!(paramDrawable instanceof BitmapDrawable))
                break;
            BitmapDrawable localBitmapDrawable = (BitmapDrawable)paramDrawable;
            if (localBitmapDrawable.getBitmap().getDensity() != 0)
                break;
            localBitmapDrawable.setTargetDensity(this.mDisplayMetrics);
            break;
            label225: if (m > k)
            {
                i = (int)(f * j);
                continue;
                label242: if ((k < i) && (m < j))
                {
                    i = k;
                    j = m;
                }
            }
        }
    }

    private Bitmap createSelectedBitmap(Bitmap paramBitmap, boolean paramBoolean)
    {
        Bitmap localBitmap1 = Bitmap.createBitmap(this.mIconTextureWidth, this.mIconTextureHeight, Bitmap.Config.ARGB_8888);
        Canvas localCanvas = new Canvas(localBitmap1);
        localCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
        int[] arrayOfInt = new int[2];
        Bitmap localBitmap2 = paramBitmap.extractAlpha(this.mBlurPaint, arrayOfInt);
        float f1 = arrayOfInt[0];
        float f2 = arrayOfInt[1];
        if (paramBoolean);
        for (Paint localPaint = this.mGlowColorPressedPaint; ; localPaint = this.mGlowColorFocusedPaint)
        {
            localCanvas.drawBitmap(localBitmap2, f1, f2, localPaint);
            localBitmap2.recycle();
            localCanvas.drawBitmap(paramBitmap, 0.0F, 0.0F, this.mPaint);
            localCanvas.setBitmap(null);
            return localBitmap1;
        }
    }

    public Drawable createIconDrawable(Drawable paramDrawable)
    {
        Bitmap localBitmap = createIconBitmap(paramDrawable);
        StateListDrawable localStateListDrawable = new StateListDrawable();
        int[] arrayOfInt1 = new int[1];
        arrayOfInt1[0] = 16842908;
        localStateListDrawable.addState(arrayOfInt1, new BitmapDrawable(createSelectedBitmap(localBitmap, false)));
        int[] arrayOfInt2 = new int[1];
        arrayOfInt2[0] = 16842919;
        localStateListDrawable.addState(arrayOfInt2, new BitmapDrawable(createSelectedBitmap(localBitmap, true)));
        localStateListDrawable.addState(new int[0], new BitmapDrawable(localBitmap));
        localStateListDrawable.setBounds(0, 0, this.mIconTextureWidth, this.mIconTextureHeight);
        return localStateListDrawable;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.IconUtilities
 * JD-Core Version:        0.6.2
 */