package com.android.internal.policy.impl;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.PowerManager;
import android.os.SystemProperties;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.Slog;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.accessibility.AccessibilityManager;
import com.android.internal.telephony.IccCard.State;
import com.android.internal.widget.LockPatternUtils;
import com.android.internal.widget.LockScreenWidgetCallback;
import com.android.internal.widget.TransportControlView;
import miui.provider.ExtraSettings.System;

public class LockPatternKeyguardView extends KeyguardViewBase
{
    static final String ACTION_EMERGENCY_DIAL = "com.android.phone.EmergencyDialer.DIAL";
    private static final boolean DEBUG = false;
    static final boolean DEBUG_CONFIGURATION = false;
    private static final int EMERGENCY_CALL_TIMEOUT = 10000;
    private static final String TAG = "LockPatternKeyguardView";
    private static final int TRANSPORT_USERACTIVITY_TIMEOUT = 10000;
    private static boolean sIsFirstAppearanceAfterBoot = true;
    private final int BIOMETRIC_AREA_EMERGENCY_DIALER_TIMEOUT = 1000;
    private BiometricSensorUnlock mBiometricUnlock;
    private final Object mBiometricUnlockStartupLock = new Object();
    private Configuration mConfiguration;
    private boolean mEnableFallback = false;
    private boolean mForgotPattern;
    private boolean mHasDialog = false;
    KeyguardUpdateMonitor.InfoCallbackImpl mInfoCallback = new KeyguardUpdateMonitor.InfoCallbackImpl()
    {
        public void onClockVisibilityChanged()
        {
            int i = 0xFF7FFFFF & LockPatternKeyguardView.this.getSystemUiVisibility();
            if (LockPatternKeyguardView.this.mUpdateMonitor.isClockVisible());
            for (int j = 8388608; ; j = 0)
            {
                int k = i | j;
                Log.v("LockPatternKeyguardView", "Set visibility on " + this + " to " + k);
                LockPatternKeyguardView.this.setSystemUiVisibility(k);
                return;
            }
        }

        public void onPhoneStateChanged(int paramAnonymousInt)
        {
            if ((LockPatternKeyguardView.this.mBiometricUnlock != null) && (paramAnonymousInt == 1))
            {
                LockPatternKeyguardView.access$1202(LockPatternKeyguardView.this, true);
                LockPatternKeyguardView.this.mBiometricUnlock.stop();
                LockPatternKeyguardView.this.mBiometricUnlock.hide();
            }
        }

        public void onRefreshBatteryInfo(boolean paramAnonymousBoolean1, boolean paramAnonymousBoolean2, int paramAnonymousInt)
        {
            if ((LockPatternKeyguardView.this.mBiometricUnlock != null) && (LockPatternKeyguardView.this.mPluggedIn != paramAnonymousBoolean2) && (!LockPatternKeyguardView.this.mBiometricUnlock.isRunning()))
            {
                LockPatternKeyguardView.this.mBiometricUnlock.stop();
                LockPatternKeyguardView.this.mBiometricUnlock.hide();
                LockPatternKeyguardView.access$1202(LockPatternKeyguardView.this, true);
            }
            LockPatternKeyguardView.access$2102(LockPatternKeyguardView.this, paramAnonymousBoolean2);
        }

        public void onUserChanged(int paramAnonymousInt)
        {
            if (LockPatternKeyguardView.this.mBiometricUnlock != null)
                LockPatternKeyguardView.this.mBiometricUnlock.stop();
            LockPatternKeyguardView.this.mLockPatternUtils.setCurrentUser(paramAnonymousInt);
            LockPatternKeyguardView.this.updateScreen(LockPatternKeyguardView.access$2200(LockPatternKeyguardView.this), true);
        }
    };
    private boolean mIsVerifyUnlockOnly = false;
    KeyguardScreenCallback mKeyguardScreenCallback = createKeyguardScreenCallback();
    private final LockPatternUtils mLockPatternUtils;
    private View mLockScreen;
    private Mode mMode = Mode.LockScreen;
    private boolean mPluggedIn;
    private Runnable mRecreateRunnable = new Runnable()
    {
        public void run()
        {
            LockPatternKeyguardView.Mode localMode = LockPatternKeyguardView.this.mMode;
            int i = 0;
            if ((localMode == LockPatternKeyguardView.Mode.UnlockScreen) && (LockPatternKeyguardView.this.getUnlockMode() == LockPatternKeyguardView.UnlockMode.Unknown))
            {
                localMode = LockPatternKeyguardView.Mode.LockScreen;
                i = 1;
            }
            LockPatternKeyguardView.this.updateScreen(localMode, true);
            LockPatternKeyguardView.this.restoreWidgetState();
            if (i != 0)
                LockPatternKeyguardView.this.mKeyguardScreenCallback.keyguardDone(false);
        }
    };
    private boolean mRequiresSim;
    private Parcelable mSavedState;
    private boolean mScreenOn;
    private boolean mShowLockBeforeUnlock = true;
    private boolean mSuppressBiometricUnlock;
    private TransportControlView mTransportControlView;
    private View mUnlockScreen;
    private UnlockMode mUnlockScreenMode = UnlockMode.Unknown;
    private final KeyguardUpdateMonitor mUpdateMonitor;
    private LockScreenWidgetCallback mWidgetCallback = new LockScreenWidgetCallback()
    {
        public boolean isVisible(View paramAnonymousView)
        {
            if (paramAnonymousView.getVisibility() == 0);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void requestHide(View paramAnonymousView)
        {
            paramAnonymousView.setVisibility(8);
            LockPatternKeyguardView.this.mUpdateMonitor.reportClockVisible(true);
            LockPatternKeyguardView.this.resetBackground();
        }

        public void requestShow(View paramAnonymousView)
        {
            paramAnonymousView.setVisibility(0);
            LockPatternKeyguardView.this.mUpdateMonitor.reportClockVisible(false);
            if (LockPatternKeyguardView.this.findViewById(16908981) == null)
                LockPatternKeyguardView.this.setBackgroundColor(-16777216);
            while (true)
            {
                return;
                LockPatternKeyguardView.this.resetBackground();
            }
        }

        public void userActivity(View paramAnonymousView)
        {
            LockPatternKeyguardView.this.mKeyguardScreenCallback.pokeWakelock(10000);
        }
    };
    private final KeyguardWindowController mWindowController;
    private boolean mWindowFocused = false;

    public LockPatternKeyguardView(Context paramContext, KeyguardViewCallback paramKeyguardViewCallback, KeyguardUpdateMonitor paramKeyguardUpdateMonitor, LockPatternUtils paramLockPatternUtils, KeyguardWindowController paramKeyguardWindowController)
    {
        super(paramContext, paramKeyguardViewCallback);
        this.mConfiguration = paramContext.getResources().getConfiguration();
        this.mEnableFallback = false;
        this.mRequiresSim = TextUtils.isEmpty(SystemProperties.get("keyguard.no_require_sim"));
        this.mUpdateMonitor = paramKeyguardUpdateMonitor;
        this.mLockPatternUtils = paramLockPatternUtils;
        this.mWindowController = paramKeyguardWindowController;
        this.mSuppressBiometricUnlock = sIsFirstAppearanceAfterBoot;
        sIsFirstAppearanceAfterBoot = false;
        this.mPluggedIn = this.mUpdateMonitor.isDevicePluggedIn();
        this.mScreenOn = ((PowerManager)paramContext.getSystemService("power")).isScreenOn();
        this.mUpdateMonitor.registerInfoCallback(this.mInfoCallback);
        setFocusableInTouchMode(true);
        setDescendantFocusability(262144);
        updateScreen(getInitialMode(), false);
        maybeEnableFallback(paramContext);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private Mode getInitialMode()
    {
        Injector.updateShowLockBeforeUnlock(this);
        IccCard.State localState = this.mUpdateMonitor.getSimState();
        Mode localMode;
        if ((stuckOnLockScreenBecauseSimMissing()) || ((localState == IccCard.State.PUK_REQUIRED) && (!this.mLockPatternUtils.isPukUnlockScreenEnable())))
            localMode = Mode.LockScreen;
        while (true)
        {
            return localMode;
            if ((!isSecure()) || (this.mShowLockBeforeUnlock))
                localMode = Mode.LockScreen;
            else
                localMode = Mode.UnlockScreen;
        }
    }

    private UnlockMode getUnlockMode()
    {
        IccCard.State localState = this.mUpdateMonitor.getSimState();
        UnlockMode localUnlockMode;
        if (localState == IccCard.State.PIN_REQUIRED)
            localUnlockMode = UnlockMode.SimPin;
        while (true)
        {
            return localUnlockMode;
            if (localState == IccCard.State.PUK_REQUIRED)
            {
                localUnlockMode = UnlockMode.SimPuk;
            }
            else
            {
                int i = this.mLockPatternUtils.getKeyguardStoredPasswordQuality();
                switch (i)
                {
                default:
                    throw new IllegalStateException("Unknown unlock mode:" + i);
                case 131072:
                case 262144:
                case 327680:
                case 393216:
                    localUnlockMode = UnlockMode.Password;
                    break;
                case 0:
                case 65536:
                    if (this.mLockPatternUtils.isLockPatternEnabled())
                    {
                        if ((this.mForgotPattern) || (this.mLockPatternUtils.isPermanentlyLocked()))
                            localUnlockMode = UnlockMode.Account;
                        else
                            localUnlockMode = UnlockMode.Pattern;
                    }
                    else
                        localUnlockMode = UnlockMode.Unknown;
                    break;
                }
            }
        }
    }

    private void initializeBiometricUnlockView(View paramView)
    {
        boolean bool1 = false;
        if (this.mBiometricUnlock != null)
            bool1 = this.mBiometricUnlock.stop();
        boolean bool2;
        if (!this.mScreenOn)
        {
            if ((this.mUpdateMonitor.getPhoneState() != 0) || (this.mHasDialog))
            {
                bool2 = true;
                this.mSuppressBiometricUnlock = bool2;
            }
        }
        else
        {
            this.mBiometricUnlock = null;
            if (useBiometricUnlock())
            {
                View localView = paramView.findViewById(16908978);
                if (localView == null)
                    break label152;
                this.mBiometricUnlock = new FaceUnlock(this.mContext, this.mUpdateMonitor, this.mLockPatternUtils, this.mKeyguardScreenCallback);
                this.mBiometricUnlock.initializeView(localView);
                if (!this.mScreenOn)
                    this.mBiometricUnlock.show(0L);
            }
        }
        while (true)
        {
            if ((this.mBiometricUnlock != null) && (bool1))
                maybeStartBiometricUnlock();
            return;
            bool2 = false;
            break;
            label152: Log.w("LockPatternKeyguardView", "Couldn't find biometric unlock view");
        }
    }

    private void initializeTransportControlView(View paramView)
    {
        this.mTransportControlView = ((TransportControlView)paramView.findViewById(16908977));
        if (this.mTransportControlView == null);
        while (true)
        {
            return;
            this.mUpdateMonitor.reportClockVisible(true);
            this.mTransportControlView.setVisibility(8);
            this.mTransportControlView.setCallback(this.mWidgetCallback);
        }
    }

    private boolean isSecure()
    {
        UnlockMode localUnlockMode = getUnlockMode();
        boolean bool = false;
        switch (5.$SwitchMap$com$android$internal$policy$impl$LockPatternKeyguardView$UnlockMode[localUnlockMode.ordinal()])
        {
        default:
            throw new IllegalStateException("unknown unlock mode " + localUnlockMode);
        case 1:
            bool = this.mLockPatternUtils.isLockPatternEnabled();
        case 6:
        case 2:
        case 3:
        case 4:
        case 5:
        }
        while (true)
        {
            return bool;
            if (this.mUpdateMonitor.getSimState() == IccCard.State.PIN_REQUIRED);
            for (bool = true; ; bool = false)
                break;
            if (this.mUpdateMonitor.getSimState() == IccCard.State.PUK_REQUIRED);
            for (bool = true; ; bool = false)
                break;
            bool = true;
            continue;
            bool = this.mLockPatternUtils.isLockPasswordEnabled();
        }
    }

    private void maybeEnableFallback(Context paramContext)
    {
        new AccountAnalyzer(AccountManager.get(paramContext), null).start();
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private void maybeStartBiometricUnlock()
    {
        if (getMode() == Mode.LockScreen);
        while (true)
        {
            return;
            if (this.mBiometricUnlock != null)
            {
                if (this.mUpdateMonitor.getFailedAttempts() >= 5);
                for (int i = 1; ; i = 0)
                {
                    if ((this.mSuppressBiometricUnlock) || (this.mUpdateMonitor.getPhoneState() != 0) || (this.mUpdateMonitor.getMaxBiometricUnlockAttemptsReached()) || (i != 0))
                        break label80;
                    this.mBiometricUnlock.start();
                    break;
                }
                label80: this.mBiometricUnlock.hide();
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private void recreateLockScreen()
    {
        if (!Injector.needRecreateLockScreen(this));
        while (true)
        {
            return;
            if (this.mLockScreen != null)
            {
                ((KeyguardScreen)this.mLockScreen).onPause();
                ((KeyguardScreen)this.mLockScreen).cleanUp();
                removeView(this.mLockScreen);
            }
            this.mLockScreen = createLockScreen();
            this.mLockScreen.setVisibility(4);
            addView(this.mLockScreen);
        }
    }

    private void recreateUnlockScreen(UnlockMode paramUnlockMode)
    {
        if (this.mUnlockScreen != null)
        {
            ((KeyguardScreen)this.mUnlockScreen).onPause();
            ((KeyguardScreen)this.mUnlockScreen).cleanUp();
            removeView(this.mUnlockScreen);
        }
        this.mUnlockScreen = createUnlockScreenFor(paramUnlockMode);
        this.mUnlockScreen.setVisibility(4);
        addView(this.mUnlockScreen);
    }

    private void restoreWidgetState()
    {
        if ((this.mTransportControlView != null) && (this.mSavedState != null))
            this.mTransportControlView.onRestoreInstanceState(this.mSavedState);
    }

    private void saveWidgetState()
    {
        if (this.mTransportControlView != null)
            this.mSavedState = this.mTransportControlView.onSaveInstanceState();
    }

    private void showAlmostAtAccountLoginDialog()
    {
        Context localContext = this.mContext;
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = Integer.valueOf(15);
        arrayOfObject[1] = Integer.valueOf(5);
        arrayOfObject[2] = Integer.valueOf(30);
        showDialog(null, localContext.getString(17040153, arrayOfObject));
    }

    private void showAlmostAtWipeDialog(int paramInt1, int paramInt2)
    {
        Context localContext = this.mContext;
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = Integer.valueOf(paramInt1);
        arrayOfObject[1] = Integer.valueOf(paramInt2);
        showDialog(null, localContext.getString(17040154, arrayOfObject));
    }

    private void showDialog(String paramString1, String paramString2)
    {
        this.mHasDialog = true;
        AlertDialog localAlertDialog = new AlertDialog.Builder(this.mContext).setTitle(paramString1).setMessage(paramString2).setNeutralButton(17039370, null).create();
        localAlertDialog.getWindow().setType(2009);
        localAlertDialog.show();
    }

    private void showTimeoutDialog()
    {
        int i = 17040150;
        if (getUnlockMode() == UnlockMode.Password)
            if (this.mLockPatternUtils.getKeyguardStoredPasswordQuality() != 131072)
                break label74;
        label74: for (i = 17040152; ; i = 17040151)
        {
            Context localContext = this.mContext;
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = Integer.valueOf(this.mUpdateMonitor.getFailedAttempts());
            arrayOfObject[1] = Integer.valueOf(30);
            showDialog(null, localContext.getString(i, arrayOfObject));
            return;
        }
    }

    private void showWipeDialog(int paramInt)
    {
        Context localContext = this.mContext;
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = Integer.valueOf(paramInt);
        showDialog(null, localContext.getString(17040155, arrayOfObject));
    }

    private boolean stuckOnLockScreenBecauseSimMissing()
    {
        if ((this.mRequiresSim) && (!this.mUpdateMonitor.isDeviceProvisioned()) && ((this.mUpdateMonitor.getSimState() == IccCard.State.ABSENT) || (this.mUpdateMonitor.getSimState() == IccCard.State.PERM_DISABLED)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private void updateScreen(Mode paramMode, boolean paramBoolean)
    {
        this.mMode = paramMode;
        if (((paramMode == Mode.LockScreen) || (this.mShowLockBeforeUnlock)) && ((paramBoolean) || (this.mLockScreen == null)))
            recreateLockScreen();
        UnlockMode localUnlockMode = getUnlockMode();
        if ((paramMode == Mode.UnlockScreen) && (localUnlockMode != UnlockMode.Unknown) && ((paramBoolean) || (this.mUnlockScreen == null) || (localUnlockMode != this.mUnlockScreenMode)))
            recreateUnlockScreen(localUnlockMode);
        View localView1;
        if (paramMode == Mode.LockScreen)
        {
            localView1 = this.mUnlockScreen;
            if (paramMode != Mode.LockScreen)
                break label244;
        }
        label244: for (View localView2 = this.mLockScreen; ; localView2 = this.mUnlockScreen)
        {
            this.mWindowController.setNeedsInput(((KeyguardScreen)localView2).needsInput());
            if (this.mScreenOn)
            {
                if ((localView1 != null) && (localView1.getVisibility() == 0))
                    ((KeyguardScreen)localView1).onPause();
                if (localView2.getVisibility() != 0)
                    ((KeyguardScreen)localView2).onResume();
            }
            if (localView1 != null)
                localView1.setVisibility(8);
            localView2.setVisibility(0);
            requestLayout();
            if (localView2.requestFocus())
                break label253;
            throw new IllegalStateException("keyguard screen must be able to take focus when shown " + localView2.getClass().getCanonicalName());
            localView1 = this.mLockScreen;
            break;
        }
        label253: Injector.startBiometricUnlock(this);
    }

    private boolean useBiometricUnlock()
    {
        boolean bool1 = true;
        UnlockMode localUnlockMode = getUnlockMode();
        boolean bool2;
        if (this.mUpdateMonitor.getFailedAttempts() >= 5)
        {
            bool2 = bool1;
            if ((!this.mLockPatternUtils.usingBiometricWeak()) || (!this.mLockPatternUtils.isBiometricWeakInstalled()) || (this.mUpdateMonitor.getMaxBiometricUnlockAttemptsReached()) || (bool2) || ((localUnlockMode != UnlockMode.Pattern) && (localUnlockMode != UnlockMode.Password)))
                break label75;
        }
        while (true)
        {
            return bool1;
            bool2 = false;
            break;
            label75: bool1 = false;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    Mode callGetInitialMode()
    {
        return getInitialMode();
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void callMaybeStartBiometricUnlock()
    {
        maybeStartBiometricUnlock();
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    boolean callStuckOnLockScreenBecauseSimMissing()
    {
        return stuckOnLockScreenBecauseSimMissing();
    }

    public void cleanUp()
    {
        if (this.mLockScreen != null)
        {
            ((KeyguardScreen)this.mLockScreen).onPause();
            ((KeyguardScreen)this.mLockScreen).cleanUp();
            removeView(this.mLockScreen);
            this.mLockScreen = null;
        }
        if (this.mUnlockScreen != null)
        {
            ((KeyguardScreen)this.mUnlockScreen).onPause();
            ((KeyguardScreen)this.mUnlockScreen).cleanUp();
            removeView(this.mUnlockScreen);
            this.mUnlockScreen = null;
        }
        this.mUpdateMonitor.removeCallback(this);
        if (this.mBiometricUnlock != null)
            this.mBiometricUnlock.cleanUp();
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    protected KeyguardScreenCallback createKeyguardScreenCallback()
    {
        return new KeyguardScreenCallback()
        {
            public boolean doesFallbackUnlockScreenExist()
            {
                return LockPatternKeyguardView.this.mEnableFallback;
            }

            public void forgotPattern(boolean paramAnonymousBoolean)
            {
                if (LockPatternKeyguardView.this.mEnableFallback)
                {
                    LockPatternKeyguardView.access$502(LockPatternKeyguardView.this, paramAnonymousBoolean);
                    LockPatternKeyguardView.this.updateScreen(LockPatternKeyguardView.Mode.UnlockScreen, false);
                }
            }

            public void goToLockScreen()
            {
                LockPatternKeyguardView.access$502(LockPatternKeyguardView.this, false);
                if (LockPatternKeyguardView.this.mIsVerifyUnlockOnly)
                {
                    LockPatternKeyguardView.access$602(LockPatternKeyguardView.this, false);
                    LockPatternKeyguardView.this.getCallback().keyguardDone(false);
                }
                while (true)
                {
                    return;
                    LockPatternKeyguardView.this.updateScreen(LockPatternKeyguardView.Mode.LockScreen, false);
                }
            }

            public void goToUnlockScreen()
            {
                IccCard.State localState = LockPatternKeyguardView.this.mUpdateMonitor.getSimState();
                if ((LockPatternKeyguardView.this.stuckOnLockScreenBecauseSimMissing()) || ((localState == IccCard.State.PUK_REQUIRED) && (!LockPatternKeyguardView.this.mLockPatternUtils.isPukUnlockScreenEnable())));
                while (true)
                {
                    return;
                    if (!isSecure())
                        LockPatternKeyguardView.this.getCallback().keyguardDone(true);
                    else
                        LockPatternKeyguardView.this.updateScreen(LockPatternKeyguardView.Mode.UnlockScreen, false);
                }
            }

            public boolean isSecure()
            {
                return LockPatternKeyguardView.this.isSecure();
            }

            public boolean isVerifyUnlockOnly()
            {
                return LockPatternKeyguardView.this.mIsVerifyUnlockOnly;
            }

            public void keyguardDone(boolean paramAnonymousBoolean)
            {
                LockPatternKeyguardView.this.getCallback().keyguardDone(paramAnonymousBoolean);
                LockPatternKeyguardView.access$1402(LockPatternKeyguardView.this, null);
            }

            public void keyguardDoneDrawing()
            {
            }

            public void pokeWakelock()
            {
                LockPatternKeyguardView.this.getCallback().pokeWakelock();
            }

            public void pokeWakelock(int paramAnonymousInt)
            {
                LockPatternKeyguardView.this.getCallback().pokeWakelock(paramAnonymousInt);
            }

            public void recreateMe(Configuration paramAnonymousConfiguration)
            {
                LockPatternKeyguardView.this.removeCallbacks(LockPatternKeyguardView.this.mRecreateRunnable);
                LockPatternKeyguardView.this.post(LockPatternKeyguardView.this.mRecreateRunnable);
            }

            public void reportFailedUnlockAttempt()
            {
                LockPatternKeyguardView.this.mUpdateMonitor.reportFailedAttempt();
                int i = LockPatternKeyguardView.this.mUpdateMonitor.getFailedAttempts();
                int j;
                int m;
                if (LockPatternKeyguardView.this.mLockPatternUtils.getKeyguardStoredPasswordQuality() == 65536)
                {
                    j = 1;
                    int k = LockPatternKeyguardView.this.mLockPatternUtils.getDevicePolicyManager().getMaximumFailedPasswordsForWipe(null);
                    if (k <= 0)
                        break label99;
                    m = k - i;
                    label62: if (m >= 5)
                        break label125;
                    if (m <= 0)
                        break label106;
                    LockPatternKeyguardView.this.showAlmostAtWipeDialog(i, m);
                }
                label134: label185: label221: 
                while (true)
                {
                    LockPatternKeyguardView.this.mLockPatternUtils.reportFailedPasswordAttempt();
                    return;
                    j = 0;
                    break;
                    label99: m = 2147483647;
                    break label62;
                    label106: Slog.i("LockPatternKeyguardView", "Too many unlock attempts; device will be wiped!");
                    LockPatternKeyguardView.this.showWipeDialog(i);
                    continue;
                    label125: int n;
                    if (i % 5 == 0)
                    {
                        n = 1;
                        if ((j != 0) && (LockPatternKeyguardView.this.mEnableFallback))
                        {
                            if (i != 15)
                                break label185;
                            LockPatternKeyguardView.this.showAlmostAtAccountLoginDialog();
                            n = 0;
                        }
                    }
                    while (true)
                    {
                        if (n == 0)
                            break label221;
                        LockPatternKeyguardView.this.showTimeoutDialog();
                        break;
                        n = 0;
                        break label134;
                        if (i >= 20)
                        {
                            LockPatternKeyguardView.this.mLockPatternUtils.setPermanentlyLocked(true);
                            LockPatternKeyguardView.this.updateScreen(LockPatternKeyguardView.this.mMode, false);
                            n = 0;
                        }
                    }
                }
            }

            public void reportSuccessfulUnlockAttempt()
            {
                LockPatternKeyguardView.this.mLockPatternUtils.reportSuccessfulPasswordAttempt();
            }

            public void takeEmergencyCallAction()
            {
                LockPatternKeyguardView.access$1202(LockPatternKeyguardView.this, true);
                if (LockPatternKeyguardView.this.mBiometricUnlock != null)
                {
                    if (LockPatternKeyguardView.this.mBiometricUnlock.isRunning())
                        LockPatternKeyguardView.this.mBiometricUnlock.show(1000L);
                    LockPatternKeyguardView.this.mBiometricUnlock.stop();
                }
                pokeWakelock(10000);
                if (TelephonyManager.getDefault().getCallState() == 2)
                    LockPatternKeyguardView.this.mLockPatternUtils.resumeCall();
                while (true)
                {
                    return;
                    Intent localIntent = new Intent("com.android.phone.EmergencyDialer.DIAL");
                    localIntent.setFlags(276824064);
                    LockPatternKeyguardView.this.getContext().startActivity(localIntent);
                }
            }
        };
    }

    View createLockScreen()
    {
        LockScreen localLockScreen = new LockScreen(this.mContext, this.mConfiguration, this.mLockPatternUtils, this.mUpdateMonitor, this.mKeyguardScreenCallback);
        initializeTransportControlView(localLockScreen);
        return localLockScreen;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    View createUnlockScreenFor(UnlockMode paramUnlockMode)
    {
        Object localObject1;
        if (paramUnlockMode == UnlockMode.Pattern)
        {
            PatternUnlockScreen localPatternUnlockScreen = new PatternUnlockScreen(this.mContext, this.mConfiguration, this.mLockPatternUtils, this.mUpdateMonitor, this.mKeyguardScreenCallback, this.mUpdateMonitor.getFailedAttempts());
            localPatternUnlockScreen.setEnableFallback(this.mEnableFallback);
            localObject1 = localPatternUnlockScreen;
        }
        while (true)
        {
            initializeTransportControlView((View)localObject1);
            initializeBiometricUnlockView((View)localObject1);
            this.mUnlockScreenMode = paramUnlockMode;
            Object localObject2 = localObject1;
            while (true)
                while (true)
                {
                    return localObject2;
                    if (paramUnlockMode == UnlockMode.SimPuk)
                    {
                        localObject1 = new SimPukUnlockScreen(this.mContext, this.mConfiguration, this.mUpdateMonitor, this.mKeyguardScreenCallback, this.mLockPatternUtils);
                        break;
                    }
                    if (paramUnlockMode == UnlockMode.SimPin)
                    {
                        localObject1 = new SimUnlockScreen(this.mContext, this.mConfiguration, this.mUpdateMonitor, this.mKeyguardScreenCallback, this.mLockPatternUtils);
                        break;
                    }
                    if (paramUnlockMode != UnlockMode.Account)
                        break label214;
                    try
                    {
                        MiuiAccountUnlockScreen localMiuiAccountUnlockScreen = new MiuiAccountUnlockScreen(this.mContext, this.mConfiguration, this.mUpdateMonitor, this.mKeyguardScreenCallback, this.mLockPatternUtils);
                        localObject1 = localMiuiAccountUnlockScreen;
                    }
                    catch (IllegalStateException localIllegalStateException)
                    {
                        Log.i("LockPatternKeyguardView", "Couldn't instantiate AccountUnlockScreen (IAccountsService isn't available)");
                        localObject2 = createUnlockScreenFor(UnlockMode.Pattern);
                    }
                }
            label214: if (paramUnlockMode != UnlockMode.Password)
                break;
            Context localContext = this.mContext;
            Configuration localConfiguration = this.mConfiguration;
            LockPatternUtils localLockPatternUtils = this.mLockPatternUtils;
            KeyguardUpdateMonitor localKeyguardUpdateMonitor = this.mUpdateMonitor;
            KeyguardScreenCallback localKeyguardScreenCallback = this.mKeyguardScreenCallback;
            localObject1 = new PasswordUnlockScreen(localContext, localConfiguration, localLockPatternUtils, localKeyguardUpdateMonitor, localKeyguardScreenCallback);
        }
        throw new IllegalArgumentException("unknown unlock mode " + paramUnlockMode);
    }

    protected void dispatchDraw(Canvas paramCanvas)
    {
        super.dispatchDraw(paramCanvas);
    }

    protected boolean dispatchHoverEvent(MotionEvent paramMotionEvent)
    {
        AccessibilityManager localAccessibilityManager = AccessibilityManager.getInstance(this.mContext);
        if ((localAccessibilityManager.isEnabled()) && (localAccessibilityManager.isTouchExplorationEnabled()))
            getCallback().pokeWakelock();
        return super.dispatchHoverEvent(paramMotionEvent);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    Configuration getConfiguration()
    {
        return this.mConfiguration;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    LockPatternUtils getLockPatternUtils()
    {
        return this.mLockPatternUtils;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    View getLockScreen()
    {
        return this.mLockScreen;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    Mode getMode()
    {
        return this.mMode;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    boolean getScreenOn()
    {
        return this.mScreenOn;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    boolean getShowLockBeforeUnlock()
    {
        return this.mShowLockBeforeUnlock;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    KeyguardUpdateMonitor getUpdateMonitor()
    {
        return this.mUpdateMonitor;
    }

    protected void onConfigurationChanged(Configuration paramConfiguration)
    {
        this.mShowLockBeforeUnlock = getResources().getBoolean(17891359);
        this.mConfiguration = paramConfiguration;
        saveWidgetState();
        removeCallbacks(this.mRecreateRunnable);
        post(this.mRecreateRunnable);
    }

    protected void onDetachedFromWindow()
    {
        this.mUpdateMonitor.removeCallback(this.mInfoCallback);
        removeCallbacks(this.mRecreateRunnable);
        if (this.mBiometricUnlock != null)
            this.mBiometricUnlock.stop();
        super.onDetachedFromWindow();
    }

    public void onScreenTurnedOff()
    {
        this.mScreenOn = false;
        this.mForgotPattern = false;
        if (this.mLockScreen != null)
            ((KeyguardScreen)this.mLockScreen).onPause();
        if (this.mUnlockScreen != null)
            ((KeyguardScreen)this.mUnlockScreen).onPause();
        saveWidgetState();
        if (this.mBiometricUnlock != null)
            this.mBiometricUnlock.stop();
    }

    public void onScreenTurnedOn()
    {
        synchronized (this.mBiometricUnlockStartupLock)
        {
            this.mScreenOn = true;
            boolean bool = this.mWindowFocused;
            show();
            restoreWidgetState();
            if ((this.mBiometricUnlock != null) && (bool))
                maybeStartBiometricUnlock();
            return;
        }
    }

    public void onWindowFocusChanged(boolean paramBoolean)
    {
        boolean bool = false;
        while (true)
        {
            synchronized (this.mBiometricUnlockStartupLock)
            {
                if ((this.mScreenOn) && (!this.mWindowFocused))
                    bool = paramBoolean;
                this.mWindowFocused = paramBoolean;
                if (!paramBoolean)
                {
                    if (this.mBiometricUnlock != null)
                    {
                        this.mSuppressBiometricUnlock = true;
                        this.mBiometricUnlock.stop();
                        this.mBiometricUnlock.hide();
                    }
                    return;
                }
            }
            this.mHasDialog = false;
            if ((this.mBiometricUnlock != null) && (bool))
                maybeStartBiometricUnlock();
        }
    }

    public void reset()
    {
        this.mIsVerifyUnlockOnly = false;
        this.mForgotPattern = false;
        post(this.mRecreateRunnable);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void setMode(Mode paramMode)
    {
        this.mMode = paramMode;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void setScreenOn(boolean paramBoolean)
    {
        this.mScreenOn = paramBoolean;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void setShowLockBeforeUnlock(boolean paramBoolean)
    {
        this.mShowLockBeforeUnlock = paramBoolean;
    }

    public void show()
    {
        if (this.mLockScreen != null)
            ((KeyguardScreen)this.mLockScreen).onResume();
        if (this.mUnlockScreen != null)
            ((KeyguardScreen)this.mUnlockScreen).onResume();
        if ((this.mBiometricUnlock != null) && (this.mSuppressBiometricUnlock))
            this.mBiometricUnlock.hide();
    }

    public void verifyUnlock()
    {
        if (!isSecure())
            getCallback().keyguardDone(true);
        while (true)
        {
            return;
            if ((this.mUnlockScreenMode != UnlockMode.Pattern) && (this.mUnlockScreenMode != UnlockMode.Password))
            {
                getCallback().keyguardDone(false);
            }
            else
            {
                this.mIsVerifyUnlockOnly = true;
                updateScreen(Mode.UnlockScreen, false);
            }
        }
    }

    public void wakeWhenReadyTq(int paramInt)
    {
        if ((paramInt == 82) && (isSecure()) && (this.mMode == Mode.LockScreen) && (this.mUpdateMonitor.getSimState() != IccCard.State.PUK_REQUIRED))
        {
            updateScreen(Mode.UnlockScreen, false);
            getCallback().pokeWakelock();
        }
        while (true)
        {
            return;
            getCallback().pokeWakelock();
        }
    }

    private static class FastBitmapDrawable extends Drawable
    {
        private Bitmap mBitmap;
        private int mOpacity;

        private FastBitmapDrawable(Bitmap paramBitmap)
        {
            this.mBitmap = paramBitmap;
            if (this.mBitmap.hasAlpha());
            for (int i = -3; ; i = -1)
            {
                this.mOpacity = i;
                return;
            }
        }

        public void draw(Canvas paramCanvas)
        {
            paramCanvas.drawBitmap(this.mBitmap, (getBounds().width() - this.mBitmap.getWidth()) / 2, getBounds().height() - this.mBitmap.getHeight(), null);
        }

        public int getIntrinsicHeight()
        {
            return this.mBitmap.getHeight();
        }

        public int getIntrinsicWidth()
        {
            return this.mBitmap.getWidth();
        }

        public int getMinimumHeight()
        {
            return this.mBitmap.getHeight();
        }

        public int getMinimumWidth()
        {
            return this.mBitmap.getWidth();
        }

        public int getOpacity()
        {
            return this.mOpacity;
        }

        public void setAlpha(int paramInt)
        {
        }

        public void setColorFilter(ColorFilter paramColorFilter)
        {
        }
    }

    private class AccountAnalyzer
        implements AccountManagerCallback<Bundle>
    {
        private int mAccountIndex;
        private final AccountManager mAccountManager;
        private final Account[] mAccounts;

        private AccountAnalyzer(AccountManager arg2)
        {
            Object localObject;
            this.mAccountManager = localObject;
            this.mAccounts = localObject.getAccountsByType("com.google");
        }

        private void next()
        {
            if ((LockPatternKeyguardView.this.mEnableFallback) || (this.mAccountIndex >= this.mAccounts.length))
                if (LockPatternKeyguardView.this.mUnlockScreen != null);
            while (true)
            {
                return;
                if ((LockPatternKeyguardView.this.mUnlockScreen instanceof PatternUnlockScreen))
                {
                    ((PatternUnlockScreen)LockPatternKeyguardView.this.mUnlockScreen).setEnableFallback(LockPatternKeyguardView.this.mEnableFallback);
                    continue;
                    this.mAccountManager.confirmCredentials(this.mAccounts[this.mAccountIndex], null, null, this, null);
                }
            }
        }

        // ERROR //
        public void run(android.accounts.AccountManagerFuture<Bundle> paramAccountManagerFuture)
        {
            // Byte code:
            //     0: aload_1
            //     1: invokeinterface 75 1 0
            //     6: checkcast 77	android/os/Bundle
            //     9: ldc 79
            //     11: invokevirtual 83	android/os/Bundle:getParcelable	(Ljava/lang/String;)Landroid/os/Parcelable;
            //     14: ifnull +12 -> 26
            //     17: aload_0
            //     18: getfield 22	com/android/internal/policy/impl/LockPatternKeyguardView$AccountAnalyzer:this$0	Lcom/android/internal/policy/impl/LockPatternKeyguardView;
            //     21: iconst_1
            //     22: invokestatic 87	com/android/internal/policy/impl/LockPatternKeyguardView:access$902	(Lcom/android/internal/policy/impl/LockPatternKeyguardView;Z)Z
            //     25: pop
            //     26: aload_0
            //     27: iconst_1
            //     28: aload_0
            //     29: getfield 47	com/android/internal/policy/impl/LockPatternKeyguardView$AccountAnalyzer:mAccountIndex	I
            //     32: iadd
            //     33: putfield 47	com/android/internal/policy/impl/LockPatternKeyguardView$AccountAnalyzer:mAccountIndex	I
            //     36: aload_0
            //     37: invokespecial 89	com/android/internal/policy/impl/LockPatternKeyguardView$AccountAnalyzer:next	()V
            //     40: return
            //     41: astore 5
            //     43: aload_0
            //     44: iconst_1
            //     45: aload_0
            //     46: getfield 47	com/android/internal/policy/impl/LockPatternKeyguardView$AccountAnalyzer:mAccountIndex	I
            //     49: iadd
            //     50: putfield 47	com/android/internal/policy/impl/LockPatternKeyguardView$AccountAnalyzer:mAccountIndex	I
            //     53: aload_0
            //     54: invokespecial 89	com/android/internal/policy/impl/LockPatternKeyguardView$AccountAnalyzer:next	()V
            //     57: goto -17 -> 40
            //     60: astore 4
            //     62: aload_0
            //     63: iconst_1
            //     64: aload_0
            //     65: getfield 47	com/android/internal/policy/impl/LockPatternKeyguardView$AccountAnalyzer:mAccountIndex	I
            //     68: iadd
            //     69: putfield 47	com/android/internal/policy/impl/LockPatternKeyguardView$AccountAnalyzer:mAccountIndex	I
            //     72: aload_0
            //     73: invokespecial 89	com/android/internal/policy/impl/LockPatternKeyguardView$AccountAnalyzer:next	()V
            //     76: goto -36 -> 40
            //     79: astore_3
            //     80: aload_0
            //     81: iconst_1
            //     82: aload_0
            //     83: getfield 47	com/android/internal/policy/impl/LockPatternKeyguardView$AccountAnalyzer:mAccountIndex	I
            //     86: iadd
            //     87: putfield 47	com/android/internal/policy/impl/LockPatternKeyguardView$AccountAnalyzer:mAccountIndex	I
            //     90: aload_0
            //     91: invokespecial 89	com/android/internal/policy/impl/LockPatternKeyguardView$AccountAnalyzer:next	()V
            //     94: goto -54 -> 40
            //     97: astore_2
            //     98: aload_0
            //     99: iconst_1
            //     100: aload_0
            //     101: getfield 47	com/android/internal/policy/impl/LockPatternKeyguardView$AccountAnalyzer:mAccountIndex	I
            //     104: iadd
            //     105: putfield 47	com/android/internal/policy/impl/LockPatternKeyguardView$AccountAnalyzer:mAccountIndex	I
            //     108: aload_0
            //     109: invokespecial 89	com/android/internal/policy/impl/LockPatternKeyguardView$AccountAnalyzer:next	()V
            //     112: aload_2
            //     113: athrow
            //
            // Exception table:
            //     from	to	target	type
            //     0	26	41	android/accounts/OperationCanceledException
            //     0	26	60	java/io/IOException
            //     0	26	79	android/accounts/AuthenticatorException
            //     0	26	97	finally
        }

        public void start()
        {
            LockPatternKeyguardView.access$902(LockPatternKeyguardView.this, false);
            this.mAccountIndex = 0;
            next();
        }
    }

    static enum UnlockMode
    {
        static
        {
            Account = new UnlockMode("Account", 3);
            Password = new UnlockMode("Password", 4);
            Unknown = new UnlockMode("Unknown", 5);
            UnlockMode[] arrayOfUnlockMode = new UnlockMode[6];
            arrayOfUnlockMode[0] = Pattern;
            arrayOfUnlockMode[1] = SimPin;
            arrayOfUnlockMode[2] = SimPuk;
            arrayOfUnlockMode[3] = Account;
            arrayOfUnlockMode[4] = Password;
            arrayOfUnlockMode[5] = Unknown;
        }
    }

    static enum Mode
    {
        static
        {
            Mode[] arrayOfMode = new Mode[2];
            arrayOfMode[0] = LockScreen;
            arrayOfMode[1] = UnlockScreen;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static boolean needRecreateLockScreen(LockPatternKeyguardView paramLockPatternKeyguardView)
        {
            if ((paramLockPatternKeyguardView.getMode() == LockPatternKeyguardView.Mode.LockScreen) && (paramLockPatternKeyguardView.getLockScreen() != null) && ((paramLockPatternKeyguardView.getLockScreen() instanceof AwesomeLockScreen)));
            for (boolean bool = false; ; bool = true)
                return bool;
        }

        public static void startBiometricUnlock(LockPatternKeyguardView paramLockPatternKeyguardView)
        {
            if (paramLockPatternKeyguardView.getScreenOn())
                paramLockPatternKeyguardView.callMaybeStartBiometricUnlock();
        }

        static void updateShowLockBeforeUnlock(LockPatternKeyguardView paramLockPatternKeyguardView)
        {
            paramLockPatternKeyguardView.setShowLockBeforeUnlock(ExtraSettings.System.getBoolean(paramLockPatternKeyguardView.getContext().getContentResolver(), "show_lock_before_unlock", true));
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.LockPatternKeyguardView
 * JD-Core Version:        0.6.2
 */