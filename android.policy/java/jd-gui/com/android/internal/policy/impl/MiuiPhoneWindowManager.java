package com.android.internal.policy.impl;

import android.app.ActivityManager;
import android.app.ActivityManagerNative;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.IActivityManager;
import android.app.Notification;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.graphics.drawable.AnimationDrawable;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.IPowerManager;
import android.os.IPowerManager.Stub;
import android.os.LocalPowerManager;
import android.os.Looper;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.provider.Settings.System;
import android.text.TextUtils;
import android.util.Log;
import android.view.IWindowManager;
import android.view.IWindowManager.Stub;
import android.view.InputChannel;
import android.view.InputEvent;
import android.view.InputEventReceiver;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.view.WindowManagerPolicy.ScreenOnListener;
import android.view.WindowManagerPolicy.WindowManagerFuncs;
import android.view.WindowManagerPolicy.WindowState;
import android.view.accessibility.AccessibilityEvent;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.internal.statusbar.IStatusBarService;
import com.android.internal.statusbar.IStatusBarService.Stub;
import java.util.HashSet;
import java.util.List;
import miui.app.ExtraStatusBarManager;
import miui.provider.ExtraSettings.System;
import miui.util.HapticFeedbackUtil;

public class MiuiPhoneWindowManager extends PhoneWindowManager
{
    private static final int BTN_MOUSE = 272;
    static final int TYPE_LAYER_MULTIPLIER = 10000;
    static final int TYPE_LAYER_OFFSET = 1000;
    private static HashSet<String> sBackLongPressWhiteList = new HashSet();
    private AnimationDrawable mAnimationDrawable;
    private BackLongPressRunnable mBackLongPress = new BackLongPressRunnable(null);
    boolean mBackLongPressed;
    private boolean mBackPressed;
    private Binder mBinder = new Binder();
    boolean mCameraKeyWakeScreen;
    private boolean mDisableProximitor;
    private int mDownX;
    private int mDownY;
    private HapticFeedbackUtil mHapticFeedbackUtil;
    private boolean mHasCameraFlash = false;
    private boolean mHomeDownWhileScreenOn;
    private boolean mIsStatusBarVisibleInFullscreen;
    private boolean mIsTouchDown;
    private MagnifierPopupWindow mMagnifier;
    private InputChannel mMagnifierInputChannel;
    private InputEventReceiver mMagnifierInputEventReceiver;
    private boolean mMenuPressed;
    private Dialog mMiuiBootMsgDialog;
    private TextView mMsgText;
    Runnable mPowerLongPressOriginal = getPowerLongPress();
    private MiuiScreenOnProximityLock mProximitySensor;
    boolean mScreenButtonsDisabled;
    private int mScreenOffReason;
    BroadcastReceiver mScreenshotReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            MiuiPhoneWindowManager.this.mHandler.removeCallbacks(MiuiPhoneWindowManager.this.getScreenshotChordLongPress());
            MiuiPhoneWindowManager.this.mHandler.postDelayed(MiuiPhoneWindowManager.this.getScreenshotChordLongPress(), paramAnonymousIntent.getLongExtra("capture_delay", 1000L));
        }
    };
    private boolean mShortcutTriggered;
    private boolean mShowMagnifier;
    BroadcastReceiver mShowMagnifierReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            MiuiPhoneWindowManager.access$502(MiuiPhoneWindowManager.this, true);
            if (MiuiPhoneWindowManager.this.mIsTouchDown)
            {
                MiuiPhoneWindowManager.this.getMagnifier().updateCache();
                MiuiPhoneWindowManager.this.getMagnifier().showMagnifier(MiuiPhoneWindowManager.this.mDownX, MiuiPhoneWindowManager.this.mDownY);
            }
        }
    };
    BroadcastReceiver mStatusBarExitFullscreenReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            MiuiPhoneWindowManager.this.setStatusBarInFullscreen(false);
        }
    };
    private InputChannel mStatusBarInputChannel;
    private InputEventReceiver mStatusBarInputEventReceiver;
    private boolean mTorchEnabled = false;
    boolean mTrackballWakeScreen;
    private boolean mVolumeDownPressed;
    boolean mVolumeKeyWakeScreen;
    private boolean mVolumeUpPressed;

    static
    {
        sBackLongPressWhiteList.add("com.android.systemui");
        sBackLongPressWhiteList.add("com.android.phone");
        sBackLongPressWhiteList.add("com.android.mms");
        sBackLongPressWhiteList.add("com.android.contacts");
        sBackLongPressWhiteList.add("com.miui.home");
        sBackLongPressWhiteList.add("com.miui.fmradio");
    }

    static IStatusBarService getStatusBarManagerService()
    {
        IStatusBarService localIStatusBarService = IStatusBarService.Stub.asInterface(ServiceManager.checkService("statusbar"));
        if (localIStatusBarService == null)
            Log.w("WindowManager", "Unable to find IStatusBarService interface.");
        return localIStatusBarService;
    }

    static IWindowManager getWindownManagerService()
    {
        IWindowManager localIWindowManager = IWindowManager.Stub.asInterface(ServiceManager.checkService("window"));
        if (localIWindowManager == null)
            Log.w("WindowManager", "Unable to find IPowerManager interface.");
        return localIWindowManager;
    }

    private boolean isEnableKeyguardTorch()
    {
        if ((this.mHasCameraFlash) && (this.mKeyguardMediator.isShowingAndNotHidden()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean releaseScreenOnProximitySensor()
    {
        boolean bool = false;
        if (!this.mSystemReady);
        while (true)
        {
            return bool;
            this.mDisableProximitor = false;
            bool = this.mProximitySensor.release();
        }
    }

    private void setMaxBacklightBrightness()
    {
        IPowerManager localIPowerManager = IPowerManager.Stub.asInterface(ServiceManager.getService("power"));
        try
        {
            localIPowerManager.setBacklightBrightness(255);
            label19: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label19;
        }
    }

    private void setStatusBarInFullscreen(boolean paramBoolean)
    {
        this.mIsStatusBarVisibleInFullscreen = paramBoolean;
        try
        {
            IStatusBarService localIStatusBarService = this.mStatusBarService;
            if (paramBoolean);
            for (int i = 536870912; ; i = 0)
            {
                localIStatusBarService.disable(i, this.mBinder, "android");
                if (this.mStatusBar != null)
                {
                    if (!paramBoolean)
                        break;
                    this.mStatusBar.showLw(false);
                }
                return;
            }
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                localRemoteException.printStackTrace();
                continue;
                this.mStatusBar.hideLw(true);
            }
        }
    }

    private void setTorch(boolean paramBoolean)
    {
        this.mTorchEnabled = paramBoolean;
        Intent localIntent = new Intent("miui.intent.action.TOGGLE_TORCH");
        localIntent.putExtra("miui.intent.extra.IS_ENABLE", paramBoolean);
        this.mContext.sendBroadcast(localIntent);
    }

    public boolean canBeForceHidden(WindowManagerPolicy.WindowState paramWindowState, WindowManager.LayoutParams paramLayoutParams)
    {
        if ((0x40000000 & paramLayoutParams.privateFlags) != 0);
        for (boolean bool = false; ; bool = super.canBeForceHidden(paramWindowState, paramLayoutParams))
            return bool;
    }

    public boolean doesForceHide(WindowManagerPolicy.WindowState paramWindowState, WindowManager.LayoutParams paramLayoutParams)
    {
        if ((0x40000000 & paramLayoutParams.privateFlags) != 0);
        for (boolean bool = false; ; bool = super.doesForceHide(paramWindowState, paramLayoutParams))
            return bool;
    }

    public int finishAnimationLw()
    {
        WindowManagerPolicy.WindowState localWindowState = this.mStatusBar;
        if (this.mIsStatusBarVisibleInFullscreen)
            this.mStatusBar = null;
        int i = super.finishAnimationLw();
        this.mStatusBar = localWindowState;
        if ((ExtraStatusBarManager.isExpandableUnderFullscreen(this.mContext)) && (this.mStatusBar != null) && (!this.mIsStatusBarVisibleInFullscreen))
        {
            if ((!this.mTopIsFullscreen) || (this.mKeyguardMediator.isShowing()))
                break label116;
            if (this.mStatusBarInputChannel == null)
            {
                this.mStatusBarInputChannel = this.mWindowManagerFuncs.monitorInput("StatusBarView");
                this.mStatusBarInputEventReceiver = new StatusBarInputEventReceiver(this.mStatusBarInputChannel, this.mHandler.getLooper());
            }
        }
        while (true)
        {
            return i;
            label116: if (this.mStatusBarInputChannel != null)
            {
                this.mStatusBarInputChannel.dispose();
                this.mStatusBarInputChannel = null;
            }
            if (this.mStatusBarInputEventReceiver != null)
            {
                this.mStatusBarInputEventReceiver.dispose();
                this.mStatusBarInputEventReceiver = null;
            }
        }
    }

    MagnifierPopupWindow getMagnifier()
    {
        try
        {
            if (this.mMagnifier == null)
            {
                int i = 1000 + 10000 * windowTypeToLayerLw(2000);
                this.mMagnifier = new MagnifierPopupWindow(this.mContext, i);
            }
            MagnifierPopupWindow localMagnifierPopupWindow = this.mMagnifier;
            return localMagnifierPopupWindow;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void hideBootMessages()
    {
        this.mHandler.post(new Runnable()
        {
            public void run()
            {
                if (MiuiPhoneWindowManager.this.mMiuiBootMsgDialog != null)
                {
                    MiuiPhoneWindowManager.this.mAnimationDrawable.stop();
                    MiuiPhoneWindowManager.this.mMiuiBootMsgDialog.dismiss();
                    MiuiPhoneWindowManager.access$702(MiuiPhoneWindowManager.this, null);
                }
            }
        });
    }

    public void init(Context paramContext, IWindowManager paramIWindowManager, WindowManagerPolicy.WindowManagerFuncs paramWindowManagerFuncs, LocalPowerManager paramLocalPowerManager)
    {
        super.init(paramContext, paramIWindowManager, paramWindowManagerFuncs, paramLocalPowerManager);
        new MiuiSettingsObserver(this.mHandler).observe();
        setPowerLongPress(new Runnable()
        {
            public void run()
            {
                if ((!MiuiPhoneWindowManager.this.mKeyguardMediator.isShowing()) || (Settings.System.getInt(MiuiPhoneWindowManager.this.mContext.getContentResolver(), "keyguard_disable_power_key_long_press", 0) == 0))
                    MiuiPhoneWindowManager.this.mPowerLongPressOriginal.run();
                while (true)
                {
                    return;
                    MiuiPhoneWindowManager.this.mPowerKeyHandled = true;
                }
            }
        });
        IntentFilter localIntentFilter1 = new IntentFilter();
        localIntentFilter1.addAction("android.intent.action.CAPTURE_SCREENSHOT");
        paramContext.registerReceiver(this.mScreenshotReceiver, localIntentFilter1);
        IntentFilter localIntentFilter2 = new IntentFilter();
        localIntentFilter2.addAction("com.miui.app.ExtraStatusBarManager.EXIT_FULLSCREEN");
        paramContext.registerReceiver(this.mStatusBarExitFullscreenReceiver, localIntentFilter2);
        this.mHasCameraFlash = paramContext.getPackageManager().hasSystemFeature("android.hardware.camera.flash");
        this.mHapticFeedbackUtil = new HapticFeedbackUtil(paramContext, false);
        this.mMagnifierInputChannel = this.mWindowManagerFuncs.monitorInput("Magnifier");
        this.mMagnifierInputEventReceiver = new MagnifierInputEventReceiver(this.mMagnifierInputChannel, Looper.myLooper());
        IntentFilter localIntentFilter3 = new IntentFilter();
        localIntentFilter3.addAction("android.intent.action.SHOW_MAGNIFIER");
        paramContext.registerReceiver(this.mShowMagnifierReceiver, localIntentFilter3);
    }

    public long interceptKeyBeforeDispatching(WindowManagerPolicy.WindowState paramWindowState, KeyEvent paramKeyEvent, int paramInt)
    {
        int i = paramKeyEvent.getKeyCode();
        int j = paramKeyEvent.getRepeatCount();
        int k;
        long l;
        if (paramKeyEvent.getAction() == 0)
        {
            k = 1;
            if ((!this.mShortcutTriggered) && ((!this.mSystemReady) || (!this.mProximitySensor.shouldBeBlocked(paramKeyEvent))))
                break label61;
            l = -1L;
        }
        while (true)
        {
            return l;
            k = 0;
            break;
            label61: if (this.mScreenButtonsDisabled);
            switch (i)
            {
            default:
                if (i != 3)
                    break label237;
                if ((k == 0) || (!isEnableKeyguardTorch()))
                    break label213;
                if (j == 0)
                {
                    this.mTorchEnabled = false;
                    this.mHomeDownWhileScreenOn = this.mScreenOnFully;
                }
                break;
            case 3:
            case 4:
            case 82:
            case 84:
            }
            while (true)
            {
                l = -1L;
                break;
                l = -1L;
                break;
                if (this.mTorchEnabled)
                {
                    if (j % 10 == 6)
                        this.mKeyguardMediator.pokeWakelock();
                }
                else if ((this.mHomeDownWhileScreenOn) && (paramKeyEvent.isLongPress()))
                    setTorch(true);
            }
            label213: if (this.mTorchEnabled)
                setTorch(false);
            label225: label237: 
            do
                while (true)
                {
                    l = super.interceptKeyBeforeDispatching(paramWindowState, paramKeyEvent, paramInt);
                    break;
                    if (i != 82)
                        break label352;
                    if ((!this.mShortcutTriggered) && (k != 0) && (paramKeyEvent.isLongPress()) && ((!this.mKeyguardMediator.isSecure()) || (!this.mKeyguardMediator.isShowing())) && (Settings.System.getInt(this.mContext.getContentResolver(), "enable_assist_menu_key_long_press", 1) != 0))
                    {
                        this.mShortcutTriggered = true;
                        try
                        {
                            Intent localIntent2 = new Intent("android.intent.action.MAIN");
                            localIntent2.setAction("android.intent.action.ASSIST");
                            localIntent2.setFlags(268435456);
                            this.mContext.startActivity(localIntent2);
                        }
                        catch (ActivityNotFoundException localActivityNotFoundException)
                        {
                        }
                    }
                }
            while (i != 4);
            label352: if (k == 0)
                break label688;
            if (!this.mKeyguardMediator.isShowingAndNotHidden())
                break label517;
            int n = Settings.System.getInt(this.mContext.getContentResolver(), "enable_snapshot_screenlock", 0);
            if ((!paramKeyEvent.isLongPress()) || (n == 0) || (this.mKeyguardMediator.isSecure()))
                break label642;
            PackageManager localPackageManager = this.mContext.getPackageManager();
            Intent localIntent1 = new Intent();
            localIntent1.addFlags(268435456);
            localIntent1.addFlags(134217728);
            localIntent1.setAction("android.media.action.STILL_IMAGE_CAMERA");
            localIntent1.putExtra("captureAfterLaunch", true);
            localIntent1.putExtra("android.intent.extras.CAMERA_FACING", 0);
            if (localPackageManager.queryIntentActivities(localIntent1, 0).size() > 0)
            {
                this.mKeyguardMediator.keyguardDone(true);
                this.mContext.startActivity(localIntent1);
            }
            l = -1L;
        }
        label517: WindowManager.LayoutParams localLayoutParams;
        if (j == 0)
        {
            this.mBackLongPressed = false;
            if (paramWindowState == null)
                break label657;
            localLayoutParams = paramWindowState.getAttrs();
            if ((localLayoutParams == null) || (sBackLongPressWhiteList.contains(localLayoutParams.packageName)) || (((localLayoutParams.type < 1) || (localLayoutParams.type > 99)) && ((localLayoutParams.type < 1000) || (localLayoutParams.type > 1999))));
        }
        while (true)
        {
            try
            {
                m = Settings.System.getInt(this.mContext.getContentResolver(), "back_key_long_press_timeout");
                this.mBackLongPress.setTarget(paramWindowState);
                if (m > 0)
                    this.mHandler.postDelayed(this.mBackLongPress, m);
                label642: if (!this.mBackLongPressed)
                    break label225;
                l = -1L;
                break;
                label657: localLayoutParams = null;
            }
            catch (Settings.SettingNotFoundException localSettingNotFoundException)
            {
                int m = ViewConfiguration.getLongPressTimeout();
                Settings.System.putInt(this.mContext.getContentResolver(), "back_key_long_press_timeout", 0);
                continue;
            }
            label688: this.mHandler.removeCallbacks(this.mBackLongPress);
        }
    }

    public int interceptKeyBeforeQueueing(KeyEvent paramKeyEvent, int paramInt, boolean paramBoolean)
    {
        int i = paramKeyEvent.getKeyCode();
        boolean bool1;
        int j;
        label27: label88: label101: boolean bool2;
        label183: int k;
        if (paramKeyEvent.getAction() == 0)
        {
            bool1 = true;
            if ((0x1000000 & paramInt) == 0)
                break label228;
            j = 1;
            if (this.mScreenButtonsDisabled);
            switch (i)
            {
            default:
                if (i == 82)
                {
                    this.mMenuPressed = bool1;
                    if ((!this.mMenuPressed) && (!this.mBackPressed) && (!this.mVolumeUpPressed) && (!this.mVolumeDownPressed))
                        this.mShortcutTriggered = false;
                    if ((!this.mShortcutTriggered) && (this.mSystemReady) && (this.mBackPressed) && (this.mVolumeUpPressed))
                        this.mShortcutTriggered = releaseScreenOnProximitySensor();
                    if (!paramBoolean)
                        break label338;
                    bool2 = this.mKeyguardMediator.isShowingAndNotHidden();
                    if ((!paramBoolean) && (j == 0))
                        break label350;
                    if ((!this.mCameraKeyWakeScreen) || (!bool2) || (i != 27) || (bool1))
                        break label495;
                    k = 4;
                }
                break;
            case 3:
            case 4:
            case 82:
            case 84:
            case 26:
            }
        }
        while (true)
        {
            return k;
            bool1 = false;
            break;
            label228: j = 0;
            break label27;
            this.mHomePressed = bool1;
            k = 0;
            continue;
            if (!this.mHomePressed)
                break label88;
            if (!bool1)
            {
                this.mHomePressed = false;
                callInterceptPowerKeyUp(false);
                this.mContext.sendBroadcast(new Intent("com.miui.app.ExtraStatusBarManager.TRIGGER_TOGGLE_SCREEN_BUTTONS"));
            }
            k = 0;
            continue;
            if (i == 4)
            {
                this.mBackPressed = bool1;
                break label101;
            }
            if (i == 24)
            {
                this.mVolumeUpPressed = bool1;
                break label101;
            }
            if (i != 25)
                break label101;
            this.mVolumeDownPressed = bool1;
            break label101;
            label338: bool2 = this.mKeyguardMediator.isShowing();
            break label183;
            label350: int n = 1;
            boolean bool3 = false;
            switch (i)
            {
            default:
                n = 0;
            case 272:
            case 27:
            case 24:
            case 25:
            }
            while (true)
                if (n != 0)
                {
                    if (bool3)
                    {
                        if (bool1)
                        {
                            k = 0;
                            break;
                            bool3 = this.mTrackballWakeScreen;
                            continue;
                            bool3 = this.mCameraKeyWakeScreen;
                            continue;
                            bool3 = this.mVolumeKeyWakeScreen;
                            if (this.mScreenOffReason != 4)
                                continue;
                            bool3 = false;
                            continue;
                        }
                        if (bool2)
                        {
                            this.mKeyguardMediator.onWakeKeyWhenKeyguardShowingTq(26, false);
                            k = 0;
                            break;
                        }
                        k = 2;
                        break;
                    }
                    paramInt &= -4;
                }
            label495: if ((!this.mShortcutTriggered) && (this.mMenuPressed) && (this.mVolumeUpPressed))
            {
                this.mShortcutTriggered = true;
                setMaxBacklightBrightness();
            }
            if ((!this.mShortcutTriggered) && (this.mMenuPressed) && (this.mVolumeDownPressed))
            {
                this.mShortcutTriggered = true;
                this.mHandler.removeCallbacks(getScreenshotChordLongPress());
                this.mHandler.postDelayed(getScreenshotChordLongPress(), 0L);
            }
            int m;
            if (bool1)
            {
                if (i != 26)
                    break label707;
                m = 1;
                if (m != 0);
            }
            try
            {
                IWindowManager localIWindowManager = getWindownManagerService();
                if ((localIWindowManager == null) || (!localIWindowManager.isKeyguardLocked()) || ((i != 25) && (i != 24) && (i != 164)))
                {
                    if (m == 0)
                        break label724;
                    IStatusBarService localIStatusBarService = getStatusBarManagerService();
                    if (localIStatusBarService == null)
                        break label724;
                    localIStatusBarService.onPanelRevealed();
                    break label724;
                    Intent localIntent = new Intent("android.intent.action.KEYCODE_MUTE");
                    localIntent.addFlags(1073741824);
                    this.mContext.sendBroadcast(localIntent);
                    k = super.interceptKeyBeforeQueueing(paramKeyEvent, paramInt, paramBoolean);
                    continue;
                    label707: m = 0;
                }
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                {
                    continue;
                    m = 1;
                    continue;
                    label724: if ((i != 26) && (i != 25))
                        if (i != 164);
                }
            }
        }
    }

    public boolean performHapticFeedbackLw(WindowManagerPolicy.WindowState paramWindowState, int paramInt, boolean paramBoolean)
    {
        if (this.mHapticFeedbackUtil.isSupportedEffect(paramInt));
        for (boolean bool = this.mHapticFeedbackUtil.performHapticFeedback(paramInt, paramBoolean); ; bool = super.performHapticFeedbackLw(paramWindowState, paramInt, paramBoolean))
            return bool;
    }

    public void removeWindowLw(WindowManagerPolicy.WindowState paramWindowState)
    {
        if (this.mStatusBar == paramWindowState)
            this.mContext.sendBroadcast(new Intent("com.miui.app.ExtraStatusBarManager.UNLOADED"));
        super.removeWindowLw(paramWindowState);
    }

    public void screenTurnedOff(int paramInt)
    {
        this.mVolumeUpPressed = false;
        this.mVolumeDownPressed = false;
        this.mMenuPressed = false;
        this.mScreenOffReason = paramInt;
        releaseScreenOnProximitySensor();
        super.screenTurnedOff(paramInt);
    }

    public void screenTurningOn(WindowManagerPolicy.ScreenOnListener paramScreenOnListener)
    {
        super.screenTurningOn(paramScreenOnListener);
        if (paramScreenOnListener == null)
            this.mKeyguardMediator.onScreenTurnedOn(new KeyguardViewManager.ShowListener()
            {
                public void onShown(IBinder paramAnonymousIBinder)
                {
                }
            });
        if ((this.mSystemReady) && (!this.mDisableProximitor) && (isDeviceProvisioned()) && (ExtraSettings.System.getBoolean(this.mContext.getContentResolver(), "enable_screen_on_proximity_sensor", false)))
            this.mProximitySensor.aquire();
    }

    public void showBootMessage(final CharSequence paramCharSequence, boolean paramBoolean)
    {
        this.mHandler.post(new Runnable()
        {
            public void run()
            {
                if (MiuiPhoneWindowManager.this.mMiuiBootMsgDialog == null)
                {
                    MiuiPhoneWindowManager.access$702(MiuiPhoneWindowManager.this, new Dialog(MiuiPhoneWindowManager.this.mContext, 16974064)
                    {
                        public boolean dispatchGenericMotionEvent(MotionEvent paramAnonymous2MotionEvent)
                        {
                            return true;
                        }

                        public boolean dispatchKeyEvent(KeyEvent paramAnonymous2KeyEvent)
                        {
                            return true;
                        }

                        public boolean dispatchKeyShortcutEvent(KeyEvent paramAnonymous2KeyEvent)
                        {
                            return true;
                        }

                        public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAnonymous2AccessibilityEvent)
                        {
                            return true;
                        }

                        public boolean dispatchTouchEvent(MotionEvent paramAnonymous2MotionEvent)
                        {
                            return true;
                        }

                        public boolean dispatchTrackballEvent(MotionEvent paramAnonymous2MotionEvent)
                        {
                            return true;
                        }
                    });
                    View localView = LayoutInflater.from(MiuiPhoneWindowManager.this.mMiuiBootMsgDialog.getContext()).inflate(100859950, null);
                    MiuiPhoneWindowManager.access$802(MiuiPhoneWindowManager.this, (TextView)localView.findViewById(101384225));
                    ImageView localImageView = (ImageView)localView.findViewById(101384226);
                    MiuiPhoneWindowManager.this.mMiuiBootMsgDialog.setContentView(localView);
                    MiuiPhoneWindowManager.this.mMiuiBootMsgDialog.getWindow().setType(2021);
                    MiuiPhoneWindowManager.this.mMiuiBootMsgDialog.getWindow().addFlags(258);
                    MiuiPhoneWindowManager.this.mMiuiBootMsgDialog.getWindow().setDimAmount(1.0F);
                    WindowManager.LayoutParams localLayoutParams = MiuiPhoneWindowManager.this.mMiuiBootMsgDialog.getWindow().getAttributes();
                    localLayoutParams.screenOrientation = 5;
                    MiuiPhoneWindowManager.this.mMiuiBootMsgDialog.getWindow().setAttributes(localLayoutParams);
                    MiuiPhoneWindowManager.this.mMiuiBootMsgDialog.getWindow().setBackgroundDrawableResource(100794420);
                    MiuiPhoneWindowManager.this.mMiuiBootMsgDialog.setCancelable(false);
                    MiuiPhoneWindowManager.this.mMiuiBootMsgDialog.show();
                    MiuiPhoneWindowManager.access$902(MiuiPhoneWindowManager.this, (AnimationDrawable)localImageView.getDrawable());
                    MiuiPhoneWindowManager.this.mAnimationDrawable.start();
                }
                MiuiPhoneWindowManager.this.mMsgText.setText(paramCharSequence);
            }
        });
    }

    public void systemReady()
    {
        super.systemReady();
        this.mProximitySensor = new MiuiScreenOnProximityLock(this.mContext, this.mKeyguardMediator);
    }

    class MiuiSettingsObserver extends ContentObserver
    {
        private Notification mScreenButtonNotification;

        MiuiSettingsObserver(Handler arg2)
        {
            super();
        }

        void observe()
        {
            ContentResolver localContentResolver = MiuiPhoneWindowManager.this.mContext.getContentResolver();
            localContentResolver.registerContentObserver(Settings.System.getUriFor("trackball_wake_screen"), false, this);
            localContentResolver.registerContentObserver(Settings.System.getUriFor("camera_key_preferred_action_type"), false, this);
            localContentResolver.registerContentObserver(Settings.System.getUriFor("camera_key_preferred_action_shortcut_id"), false, this);
            localContentResolver.registerContentObserver(Settings.System.getUriFor("volumekey_wake_screen"), false, this);
            localContentResolver.registerContentObserver(Settings.Secure.getUriFor("screen_buttons_state"), false, this);
            onChange(false);
        }

        // ERROR //
        public void onChange(boolean paramBoolean)
        {
            // Byte code:
            //     0: iconst_1
            //     1: istore_2
            //     2: aload_0
            //     3: getfield 15	com/android/internal/policy/impl/MiuiPhoneWindowManager$MiuiSettingsObserver:this$0	Lcom/android/internal/policy/impl/MiuiPhoneWindowManager;
            //     6: getfield 65	com/android/internal/policy/impl/PhoneWindowManager:mLock	Ljava/lang/Object;
            //     9: astore_3
            //     10: aload_3
            //     11: monitorenter
            //     12: aload_0
            //     13: getfield 15	com/android/internal/policy/impl/MiuiPhoneWindowManager$MiuiSettingsObserver:this$0	Lcom/android/internal/policy/impl/MiuiPhoneWindowManager;
            //     16: getfield 26	com/android/internal/policy/impl/PhoneWindowManager:mContext	Landroid/content/Context;
            //     19: invokevirtual 32	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
            //     22: astore 5
            //     24: aload_0
            //     25: getfield 15	com/android/internal/policy/impl/MiuiPhoneWindowManager$MiuiSettingsObserver:this$0	Lcom/android/internal/policy/impl/MiuiPhoneWindowManager;
            //     28: astore 6
            //     30: aload 5
            //     32: ldc 54
            //     34: iconst_0
            //     35: invokestatic 69	android/provider/Settings$Secure:getInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)I
            //     38: ifeq +318 -> 356
            //     41: iload_2
            //     42: istore 7
            //     44: aload 6
            //     46: iload 7
            //     48: putfield 73	com/android/internal/policy/impl/MiuiPhoneWindowManager:mScreenButtonsDisabled	Z
            //     51: aload_0
            //     52: getfield 75	com/android/internal/policy/impl/MiuiPhoneWindowManager$MiuiSettingsObserver:mScreenButtonNotification	Landroid/app/Notification;
            //     55: ifnonnull +307 -> 362
            //     58: iload_2
            //     59: istore 8
            //     61: aload_0
            //     62: getfield 75	com/android/internal/policy/impl/MiuiPhoneWindowManager$MiuiSettingsObserver:mScreenButtonNotification	Landroid/app/Notification;
            //     65: ifnonnull +97 -> 162
            //     68: new 77	android/content/Intent
            //     71: dup
            //     72: ldc 79
            //     74: invokespecial 82	android/content/Intent:<init>	(Ljava/lang/String;)V
            //     77: astore 9
            //     79: aload_0
            //     80: new 84	android/app/Notification$Builder
            //     83: dup
            //     84: aload_0
            //     85: getfield 15	com/android/internal/policy/impl/MiuiPhoneWindowManager$MiuiSettingsObserver:this$0	Lcom/android/internal/policy/impl/MiuiPhoneWindowManager;
            //     88: getfield 26	com/android/internal/policy/impl/PhoneWindowManager:mContext	Landroid/content/Context;
            //     91: invokespecial 87	android/app/Notification$Builder:<init>	(Landroid/content/Context;)V
            //     94: iconst_1
            //     95: invokevirtual 91	android/app/Notification$Builder:setOngoing	(Z)Landroid/app/Notification$Builder;
            //     98: invokestatic 97	java/lang/System:currentTimeMillis	()J
            //     101: invokevirtual 101	android/app/Notification$Builder:setWhen	(J)Landroid/app/Notification$Builder;
            //     104: ldc 102
            //     106: invokevirtual 106	android/app/Notification$Builder:setSmallIcon	(I)Landroid/app/Notification$Builder;
            //     109: aload_0
            //     110: getfield 15	com/android/internal/policy/impl/MiuiPhoneWindowManager$MiuiSettingsObserver:this$0	Lcom/android/internal/policy/impl/MiuiPhoneWindowManager;
            //     113: getfield 26	com/android/internal/policy/impl/PhoneWindowManager:mContext	Landroid/content/Context;
            //     116: ldc 107
            //     118: invokevirtual 111	android/content/Context:getString	(I)Ljava/lang/String;
            //     121: invokevirtual 115	android/app/Notification$Builder:setContentTitle	(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
            //     124: aload_0
            //     125: getfield 15	com/android/internal/policy/impl/MiuiPhoneWindowManager$MiuiSettingsObserver:this$0	Lcom/android/internal/policy/impl/MiuiPhoneWindowManager;
            //     128: getfield 26	com/android/internal/policy/impl/PhoneWindowManager:mContext	Landroid/content/Context;
            //     131: ldc 116
            //     133: invokevirtual 111	android/content/Context:getString	(I)Ljava/lang/String;
            //     136: invokevirtual 119	android/app/Notification$Builder:setContentText	(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
            //     139: aload_0
            //     140: getfield 15	com/android/internal/policy/impl/MiuiPhoneWindowManager$MiuiSettingsObserver:this$0	Lcom/android/internal/policy/impl/MiuiPhoneWindowManager;
            //     143: getfield 26	com/android/internal/policy/impl/PhoneWindowManager:mContext	Landroid/content/Context;
            //     146: iconst_0
            //     147: aload 9
            //     149: iconst_0
            //     150: invokestatic 125	android/app/PendingIntent:getBroadcast	(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
            //     153: invokevirtual 129	android/app/Notification$Builder:setContentIntent	(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;
            //     156: invokevirtual 133	android/app/Notification$Builder:getNotification	()Landroid/app/Notification;
            //     159: putfield 75	com/android/internal/policy/impl/MiuiPhoneWindowManager$MiuiSettingsObserver:mScreenButtonNotification	Landroid/app/Notification;
            //     162: aload_0
            //     163: getfield 15	com/android/internal/policy/impl/MiuiPhoneWindowManager$MiuiSettingsObserver:this$0	Lcom/android/internal/policy/impl/MiuiPhoneWindowManager;
            //     166: getfield 26	com/android/internal/policy/impl/PhoneWindowManager:mContext	Landroid/content/Context;
            //     169: ldc 135
            //     171: invokevirtual 139	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
            //     174: checkcast 141	android/app/NotificationManager
            //     177: astore 10
            //     179: aload_0
            //     180: getfield 15	com/android/internal/policy/impl/MiuiPhoneWindowManager$MiuiSettingsObserver:this$0	Lcom/android/internal/policy/impl/MiuiPhoneWindowManager;
            //     183: getfield 73	com/android/internal/policy/impl/MiuiPhoneWindowManager:mScreenButtonsDisabled	Z
            //     186: ifeq +115 -> 301
            //     189: aload 10
            //     191: aload_0
            //     192: getfield 75	com/android/internal/policy/impl/MiuiPhoneWindowManager$MiuiSettingsObserver:mScreenButtonNotification	Landroid/app/Notification;
            //     195: getfield 147	android/app/Notification:icon	I
            //     198: aload_0
            //     199: getfield 75	com/android/internal/policy/impl/MiuiPhoneWindowManager$MiuiSettingsObserver:mScreenButtonNotification	Landroid/app/Notification;
            //     202: invokevirtual 151	android/app/NotificationManager:notify	(ILandroid/app/Notification;)V
            //     205: aload_0
            //     206: getfield 15	com/android/internal/policy/impl/MiuiPhoneWindowManager$MiuiSettingsObserver:this$0	Lcom/android/internal/policy/impl/MiuiPhoneWindowManager;
            //     209: astore 11
            //     211: aload 5
            //     213: ldc 34
            //     215: iconst_0
            //     216: invokestatic 152	android/provider/Settings$System:getInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)I
            //     219: iload_2
            //     220: if_icmpne +108 -> 328
            //     223: iload_2
            //     224: istore 12
            //     226: aload 11
            //     228: iload 12
            //     230: putfield 155	com/android/internal/policy/impl/MiuiPhoneWindowManager:mTrackballWakeScreen	Z
            //     233: aload_0
            //     234: getfield 15	com/android/internal/policy/impl/MiuiPhoneWindowManager$MiuiSettingsObserver:this$0	Lcom/android/internal/policy/impl/MiuiPhoneWindowManager;
            //     237: astore 13
            //     239: aload 5
            //     241: ldc 52
            //     243: iconst_0
            //     244: invokestatic 152	android/provider/Settings$System:getInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)I
            //     247: iload_2
            //     248: if_icmpne +86 -> 334
            //     251: iload_2
            //     252: istore 14
            //     254: aload 13
            //     256: iload 14
            //     258: putfield 158	com/android/internal/policy/impl/MiuiPhoneWindowManager:mVolumeKeyWakeScreen	Z
            //     261: iload_2
            //     262: aload 5
            //     264: ldc 48
            //     266: iconst_0
            //     267: invokestatic 152	android/provider/Settings$System:getInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)I
            //     270: if_icmpne +75 -> 345
            //     273: aload_0
            //     274: getfield 15	com/android/internal/policy/impl/MiuiPhoneWindowManager$MiuiSettingsObserver:this$0	Lcom/android/internal/policy/impl/MiuiPhoneWindowManager;
            //     277: astore 15
            //     279: iconst_4
            //     280: aload 5
            //     282: ldc 50
            //     284: bipush 255
            //     286: invokestatic 152	android/provider/Settings$System:getInt	(Landroid/content/ContentResolver;Ljava/lang/String;I)I
            //     289: if_icmpne +51 -> 340
            //     292: aload 15
            //     294: iload_2
            //     295: putfield 161	com/android/internal/policy/impl/MiuiPhoneWindowManager:mCameraKeyWakeScreen	Z
            //     298: aload_3
            //     299: monitorexit
            //     300: return
            //     301: iload 8
            //     303: ifne -98 -> 205
            //     306: aload 10
            //     308: aload_0
            //     309: getfield 75	com/android/internal/policy/impl/MiuiPhoneWindowManager$MiuiSettingsObserver:mScreenButtonNotification	Landroid/app/Notification;
            //     312: getfield 147	android/app/Notification:icon	I
            //     315: invokevirtual 165	android/app/NotificationManager:cancel	(I)V
            //     318: goto -113 -> 205
            //     321: astore 4
            //     323: aload_3
            //     324: monitorexit
            //     325: aload 4
            //     327: athrow
            //     328: iconst_0
            //     329: istore 12
            //     331: goto -105 -> 226
            //     334: iconst_0
            //     335: istore 14
            //     337: goto -83 -> 254
            //     340: iconst_0
            //     341: istore_2
            //     342: goto -50 -> 292
            //     345: aload_0
            //     346: getfield 15	com/android/internal/policy/impl/MiuiPhoneWindowManager$MiuiSettingsObserver:this$0	Lcom/android/internal/policy/impl/MiuiPhoneWindowManager;
            //     349: iconst_0
            //     350: putfield 161	com/android/internal/policy/impl/MiuiPhoneWindowManager:mCameraKeyWakeScreen	Z
            //     353: goto -55 -> 298
            //     356: iconst_0
            //     357: istore 7
            //     359: goto -315 -> 44
            //     362: iconst_0
            //     363: istore 8
            //     365: goto -304 -> 61
            //
            // Exception table:
            //     from	to	target	type
            //     12	325	321	finally
            //     345	353	321	finally
        }
    }

    private class BackLongPressRunnable
        implements Runnable
    {
        private WindowManagerPolicy.WindowState mWin = null;

        private BackLongPressRunnable()
        {
        }

        private void showHint()
        {
            AlertDialog.Builder localBuilder = new AlertDialog.Builder(MiuiPhoneWindowManager.this.mContext);
            localBuilder.setMessage(101450095);
            localBuilder.setNegativeButton(17039369, null);
            localBuilder.setPositiveButton(17039379, new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
                {
                    Settings.System.putInt(MiuiPhoneWindowManager.this.mContext.getContentResolver(), "back_key_long_press_timeout", 1500);
                }
            });
            AlertDialog localAlertDialog = localBuilder.create();
            localAlertDialog.getWindow().setType(2008);
            localAlertDialog.getWindow().setFlags(131072, 131072);
            localAlertDialog.show();
        }

        public void run()
        {
            MiuiPhoneWindowManager.this.mBackLongPressed = true;
            MiuiPhoneWindowManager.this.performHapticFeedbackLw(null, 0, false);
            if (this.mWin == null)
                showHint();
            while (true)
            {
                return;
                String str1 = this.mWin.getAttrs().packageName;
                Object localObject = null;
                String str2 = (String)this.mWin.getAttrs().getTitle();
                int i = str2.lastIndexOf('/');
                ComponentName localComponentName;
                PackageManager localPackageManager;
                if (i >= 0)
                {
                    localComponentName = new ComponentName(str1, (String)str2.subSequence(i + 1, str2.length()));
                    localPackageManager = MiuiPhoneWindowManager.this.mContext.getPackageManager();
                }
                try
                {
                    localObject = localPackageManager.getActivityInfo(localComponentName, 0).loadLabel(localPackageManager).toString();
                    if (TextUtils.isEmpty((CharSequence)localObject))
                    {
                        String str3 = localPackageManager.getApplicationInfo(str1, 0).loadLabel(localPackageManager).toString();
                        localObject = str3;
                    }
                    try
                    {
                        label160: ActivityManagerNative.getDefault().finishActivity(this.mWin.getAttrs().token, 0, null);
                        ((ActivityManager)MiuiPhoneWindowManager.this.mContext.getSystemService("activity")).forceStopPackage(str1);
                        Context localContext1 = MiuiPhoneWindowManager.this.mContext;
                        Context localContext2 = MiuiPhoneWindowManager.this.mContext;
                        Object[] arrayOfObject = new Object[1];
                        if (!TextUtils.isEmpty((CharSequence)localObject))
                        {
                            arrayOfObject[0] = localObject;
                            Toast.makeText(localContext1, localContext2.getString(101450096, arrayOfObject), 0).show();
                        }
                    }
                    catch (RemoteException localRemoteException)
                    {
                        while (true)
                        {
                            localRemoteException.printStackTrace();
                            continue;
                            localObject = str1;
                        }
                    }
                }
                catch (PackageManager.NameNotFoundException localNameNotFoundException)
                {
                    break label160;
                }
            }
        }

        public void setTarget(WindowManagerPolicy.WindowState paramWindowState)
        {
            this.mWin = paramWindowState;
        }
    }

    private final class MagnifierInputEventReceiver extends InputEventReceiver
    {
        public MagnifierInputEventReceiver(InputChannel paramLooper, Looper arg3)
        {
            super(localLooper);
        }

        public void onInputEvent(InputEvent paramInputEvent)
        {
            if (!(paramInputEvent instanceof MotionEvent))
            {
                finishInputEvent(paramInputEvent, false);
                return;
            }
            MotionEvent localMotionEvent = (MotionEvent)paramInputEvent;
            boolean bool = false;
            MiuiPhoneWindowManager.access$202(MiuiPhoneWindowManager.this, (int)localMotionEvent.getRawX());
            MiuiPhoneWindowManager.access$302(MiuiPhoneWindowManager.this, (int)localMotionEvent.getRawY());
            while (true)
            {
                try
                {
                    while (true)
                    {
                        int i = localMotionEvent.getSource();
                        if ((i & 0x2) == 0)
                        {
                            finishInputEvent(paramInputEvent, false);
                            break;
                        }
                        bool = true;
                        synchronized (MiuiPhoneWindowManager.this.mLock)
                        {
                            switch (localMotionEvent.getActionMasked())
                            {
                            default:
                            case 0:
                            case 2:
                                do
                                {
                                    finishInputEvent(paramInputEvent, bool);
                                    break;
                                    MiuiPhoneWindowManager.access$402(MiuiPhoneWindowManager.this, true);
                                }
                                while (!MiuiPhoneWindowManager.this.mShowMagnifier);
                                MiuiPhoneWindowManager.this.getMagnifier().showMagnifier(MiuiPhoneWindowManager.this.mDownX, MiuiPhoneWindowManager.this.mDownY);
                            case 1:
                            case 3:
                            }
                        }
                    }
                }
                finally
                {
                    finishInputEvent(paramInputEvent, bool);
                }
                MiuiPhoneWindowManager.access$402(MiuiPhoneWindowManager.this, false);
                MiuiPhoneWindowManager.access$502(MiuiPhoneWindowManager.this, false);
                MiuiPhoneWindowManager.this.getMagnifier().hide();
            }
        }
    }

    private final class StatusBarInputEventReceiver extends InputEventReceiver
    {
        private float mDownX;
        private float mDownY;

        public StatusBarInputEventReceiver(InputChannel paramLooper, Looper arg3)
        {
            super(localLooper);
        }

        public void onInputEvent(InputEvent paramInputEvent)
        {
            if (!(paramInputEvent instanceof MotionEvent))
            {
                finishInputEvent(paramInputEvent, false);
                return;
            }
            MotionEvent localMotionEvent = (MotionEvent)paramInputEvent;
            boolean bool1 = false;
            while (true)
            {
                try
                {
                    while (true)
                    {
                        if (((0x2 & localMotionEvent.getSource()) != 0) && (MiuiPhoneWindowManager.this.mStatusBarService != null))
                        {
                            boolean bool2 = MiuiPhoneWindowManager.this.mIsStatusBarVisibleInFullscreen;
                            if (!bool2);
                        }
                        else
                        {
                            finishInputEvent(paramInputEvent, false);
                            break;
                        }
                        bool1 = true;
                        synchronized (MiuiPhoneWindowManager.this.mLock)
                        {
                            switch (localMotionEvent.getActionMasked())
                            {
                            default:
                                finishInputEvent(paramInputEvent, bool1);
                                break;
                            case 0:
                                this.mDownX = localMotionEvent.getRawX();
                                this.mDownY = localMotionEvent.getRawY();
                            case 1:
                            case 2:
                            case 3:
                            }
                        }
                    }
                }
                finally
                {
                    finishInputEvent(paramInputEvent, bool1);
                }
                if (MiuiPhoneWindowManager.this.mContext.getResources().getFraction(101777408, MiuiPhoneWindowManager.this.mStatusBarHeight, MiuiPhoneWindowManager.this.mStatusBarHeight) >= this.mDownY)
                {
                    float f1 = Math.abs(this.mDownX - localMotionEvent.getRawX());
                    float f2 = Math.abs(this.mDownY - localMotionEvent.getRawY());
                    if ((2.0F * f1 <= f2) && (2 * MiuiPhoneWindowManager.this.mStatusBarHeight <= f2))
                    {
                        MiuiPhoneWindowManager.this.setStatusBarInFullscreen(true);
                        this.mDownY = MiuiPhoneWindowManager.this.mStatusBarHeight;
                    }
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.MiuiPhoneWindowManager
 * JD-Core Version:        0.6.2
 */