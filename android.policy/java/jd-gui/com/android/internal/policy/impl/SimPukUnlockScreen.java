package com.android.internal.policy.impl;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.ITelephony.Stub;
import com.android.internal.widget.LockPatternUtils;

public class SimPukUnlockScreen extends LinearLayout
    implements KeyguardScreen, View.OnClickListener, View.OnFocusChangeListener
{
    private static final char[] DIGITS = arrayOfChar;
    private static final int DIGIT_PRESS_WAKE_MILLIS = 5000;
    private final KeyguardScreenCallback mCallback;
    private int mCreationOrientation;
    private View mDelPinButton;
    private View mDelPukButton;
    private TextView mFocusedEntry;
    private TextView mHeaderText;
    private int mKeyboardHidden;
    private KeyguardStatusViewManager mKeyguardStatusViewManager;
    private LockPatternUtils mLockPatternUtils;
    private View mOkButton;
    private TextView mPinText;
    private TextView mPukText;
    private ProgressDialog mSimUnlockProgressDialog = null;
    private final KeyguardUpdateMonitor mUpdateMonitor;

    static
    {
        char[] arrayOfChar = new char[10];
        arrayOfChar[0] = 48;
        arrayOfChar[1] = 49;
        arrayOfChar[2] = 50;
        arrayOfChar[3] = 51;
        arrayOfChar[4] = 52;
        arrayOfChar[5] = 53;
        arrayOfChar[6] = 54;
        arrayOfChar[7] = 55;
        arrayOfChar[8] = 56;
        arrayOfChar[9] = 57;
    }

    public SimPukUnlockScreen(Context paramContext, Configuration paramConfiguration, KeyguardUpdateMonitor paramKeyguardUpdateMonitor, KeyguardScreenCallback paramKeyguardScreenCallback, LockPatternUtils paramLockPatternUtils)
    {
        super(paramContext);
        this.mUpdateMonitor = paramKeyguardUpdateMonitor;
        this.mCallback = paramKeyguardScreenCallback;
        this.mCreationOrientation = paramConfiguration.orientation;
        this.mKeyboardHidden = paramConfiguration.hardKeyboardHidden;
        this.mLockPatternUtils = paramLockPatternUtils;
        LayoutInflater localLayoutInflater = LayoutInflater.from(paramContext);
        if (this.mKeyboardHidden == 1)
            localLayoutInflater.inflate(17367124, this, true);
        while (true)
        {
            this.mHeaderText = ((TextView)findViewById(16908982));
            this.mPukText = ((TextView)findViewById(16908991));
            this.mPinText = ((TextView)findViewById(16908984));
            this.mDelPukButton = findViewById(16908992);
            this.mDelPinButton = findViewById(16908975);
            this.mOkButton = findViewById(16908954);
            this.mDelPinButton.setOnClickListener(this);
            this.mDelPukButton.setOnClickListener(this);
            this.mOkButton.setOnClickListener(this);
            this.mHeaderText.setText(17040109);
            this.mHeaderText.setSelected(true);
            this.mKeyguardStatusViewManager = new KeyguardStatusViewManager(this, paramKeyguardUpdateMonitor, paramLockPatternUtils, paramKeyguardScreenCallback, true);
            this.mPinText.setFocusableInTouchMode(true);
            this.mPinText.setOnFocusChangeListener(this);
            this.mPukText.setFocusableInTouchMode(true);
            this.mPukText.setOnFocusChangeListener(this);
            return;
            localLayoutInflater.inflate(17367125, this, true);
            new TouchInput(null);
        }
    }

    private void checkPuk()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 106	com/android/internal/policy/impl/SimPukUnlockScreen:mPukText	Landroid/widget/TextView;
        //     4: invokevirtual 171	android/widget/TextView:getText	()Ljava/lang/CharSequence;
        //     7: invokeinterface 177 1 0
        //     12: bipush 8
        //     14: if_icmpge +22 -> 36
        //     17: aload_0
        //     18: getfield 103	com/android/internal/policy/impl/SimPukUnlockScreen:mHeaderText	Landroid/widget/TextView;
        //     21: ldc 178
        //     23: invokevirtual 129	android/widget/TextView:setText	(I)V
        //     26: aload_0
        //     27: getfield 106	com/android/internal/policy/impl/SimPukUnlockScreen:mPukText	Landroid/widget/TextView;
        //     30: ldc 180
        //     32: invokevirtual 183	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
        //     35: return
        //     36: aload_0
        //     37: getfield 109	com/android/internal/policy/impl/SimPukUnlockScreen:mPinText	Landroid/widget/TextView;
        //     40: invokevirtual 171	android/widget/TextView:getText	()Ljava/lang/CharSequence;
        //     43: invokeinterface 177 1 0
        //     48: iconst_4
        //     49: if_icmplt +20 -> 69
        //     52: aload_0
        //     53: getfield 109	com/android/internal/policy/impl/SimPukUnlockScreen:mPinText	Landroid/widget/TextView;
        //     56: invokevirtual 171	android/widget/TextView:getText	()Ljava/lang/CharSequence;
        //     59: invokeinterface 177 1 0
        //     64: bipush 8
        //     66: if_icmple +24 -> 90
        //     69: aload_0
        //     70: getfield 103	com/android/internal/policy/impl/SimPukUnlockScreen:mHeaderText	Landroid/widget/TextView;
        //     73: ldc 184
        //     75: invokevirtual 129	android/widget/TextView:setText	(I)V
        //     78: aload_0
        //     79: getfield 109	com/android/internal/policy/impl/SimPukUnlockScreen:mPinText	Landroid/widget/TextView;
        //     82: ldc 180
        //     84: invokevirtual 183	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
        //     87: goto -52 -> 35
        //     90: aload_0
        //     91: invokespecial 188	com/android/internal/policy/impl/SimPukUnlockScreen:getSimUnlockProgressDialog	()Landroid/app/Dialog;
        //     94: invokevirtual 193	android/app/Dialog:show	()V
        //     97: new 12	com/android/internal/policy/impl/SimPukUnlockScreen$1
        //     100: dup
        //     101: aload_0
        //     102: aload_0
        //     103: getfield 106	com/android/internal/policy/impl/SimPukUnlockScreen:mPukText	Landroid/widget/TextView;
        //     106: invokevirtual 171	android/widget/TextView:getText	()Ljava/lang/CharSequence;
        //     109: invokevirtual 199	java/lang/Object:toString	()Ljava/lang/String;
        //     112: aload_0
        //     113: getfield 109	com/android/internal/policy/impl/SimPukUnlockScreen:mPinText	Landroid/widget/TextView;
        //     116: invokevirtual 171	android/widget/TextView:getText	()Ljava/lang/CharSequence;
        //     119: invokevirtual 199	java/lang/Object:toString	()Ljava/lang/String;
        //     122: invokespecial 202	com/android/internal/policy/impl/SimPukUnlockScreen$1:<init>	(Lcom/android/internal/policy/impl/SimPukUnlockScreen;Ljava/lang/String;Ljava/lang/String;)V
        //     125: invokevirtual 205	com/android/internal/policy/impl/SimPukUnlockScreen$1:start	()V
        //     128: goto -93 -> 35
    }

    private Dialog getSimUnlockProgressDialog()
    {
        if (this.mSimUnlockProgressDialog == null)
        {
            this.mSimUnlockProgressDialog = new ProgressDialog(this.mContext);
            this.mSimUnlockProgressDialog.setMessage(this.mContext.getString(17040149));
            this.mSimUnlockProgressDialog.setIndeterminate(true);
            this.mSimUnlockProgressDialog.setCancelable(false);
            this.mSimUnlockProgressDialog.getWindow().setType(2009);
        }
        return this.mSimUnlockProgressDialog;
    }

    private void reportDigit(int paramInt)
    {
        this.mFocusedEntry.append(Integer.toString(paramInt));
    }

    public void cleanUp()
    {
        if (this.mSimUnlockProgressDialog != null)
        {
            this.mSimUnlockProgressDialog.dismiss();
            this.mSimUnlockProgressDialog = null;
        }
        this.mUpdateMonitor.removeCallback(this);
    }

    public boolean needsInput()
    {
        return false;
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        updateConfiguration();
    }

    public void onClick(View paramView)
    {
        if (paramView == this.mDelPukButton)
        {
            if (this.mFocusedEntry != this.mPukText)
                this.mPukText.requestFocus();
            Editable localEditable2 = this.mPukText.getEditableText();
            int j = localEditable2.length();
            if (j > 0)
                localEditable2.delete(j - 1, j);
        }
        while (true)
        {
            this.mCallback.pokeWakelock(5000);
            return;
            if (paramView == this.mDelPinButton)
            {
                if (this.mFocusedEntry != this.mPinText)
                    this.mPinText.requestFocus();
                Editable localEditable1 = this.mPinText.getEditableText();
                int i = localEditable1.length();
                if (i > 0)
                    localEditable1.delete(i - 1, i);
            }
            else if (paramView == this.mOkButton)
            {
                checkPuk();
            }
        }
    }

    protected void onConfigurationChanged(Configuration paramConfiguration)
    {
        super.onConfigurationChanged(paramConfiguration);
        updateConfiguration();
    }

    public void onFocusChange(View paramView, boolean paramBoolean)
    {
        if (paramBoolean)
            this.mFocusedEntry = ((TextView)paramView);
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
        boolean bool = true;
        if (paramInt == 4)
            this.mCallback.goToLockScreen();
        while (true)
        {
            return bool;
            int i = paramKeyEvent.getMatch(DIGITS);
            if (i != 0)
            {
                reportDigit(i + -48);
            }
            else if (paramInt == 67)
            {
                this.mFocusedEntry.onKeyDown(paramInt, paramKeyEvent);
                Editable localEditable = this.mFocusedEntry.getEditableText();
                int j = localEditable.length();
                if (j > 0)
                    localEditable.delete(j - 1, j);
                this.mCallback.pokeWakelock(5000);
            }
            else if (paramInt == 66)
            {
                checkPuk();
            }
            else
            {
                bool = false;
            }
        }
    }

    public void onPause()
    {
        this.mKeyguardStatusViewManager.onPause();
    }

    public void onResume()
    {
        this.mHeaderText.setText(17040109);
        this.mKeyguardStatusViewManager.onResume();
    }

    void updateConfiguration()
    {
        Configuration localConfiguration = getResources().getConfiguration();
        if (localConfiguration.orientation != this.mCreationOrientation)
            this.mCallback.recreateMe(localConfiguration);
        while (true)
        {
            return;
            if (localConfiguration.hardKeyboardHidden != this.mKeyboardHidden)
                this.mKeyboardHidden = localConfiguration.hardKeyboardHidden;
        }
    }

    private class TouchInput
        implements View.OnClickListener
    {
        private TextView mCancelButton = (TextView)SimPukUnlockScreen.this.findViewById(16908905);
        private TextView mEight = (TextView)SimPukUnlockScreen.this.findViewById(16909146);
        private TextView mFive = (TextView)SimPukUnlockScreen.this.findViewById(16909143);
        private TextView mFour = (TextView)SimPukUnlockScreen.this.findViewById(16909142);
        private TextView mNine = (TextView)SimPukUnlockScreen.this.findViewById(16909147);
        private TextView mOne = (TextView)SimPukUnlockScreen.this.findViewById(16909139);
        private TextView mSeven = (TextView)SimPukUnlockScreen.this.findViewById(16909145);
        private TextView mSix = (TextView)SimPukUnlockScreen.this.findViewById(16909144);
        private TextView mThree = (TextView)SimPukUnlockScreen.this.findViewById(16909141);
        private TextView mTwo = (TextView)SimPukUnlockScreen.this.findViewById(16909140);
        private TextView mZero = (TextView)SimPukUnlockScreen.this.findViewById(16909148);

        private TouchInput()
        {
            this.mZero.setText("0");
            this.mOne.setText("1");
            this.mTwo.setText("2");
            this.mThree.setText("3");
            this.mFour.setText("4");
            this.mFive.setText("5");
            this.mSix.setText("6");
            this.mSeven.setText("7");
            this.mEight.setText("8");
            this.mNine.setText("9");
            this.mZero.setOnClickListener(this);
            this.mOne.setOnClickListener(this);
            this.mTwo.setOnClickListener(this);
            this.mThree.setOnClickListener(this);
            this.mFour.setOnClickListener(this);
            this.mFive.setOnClickListener(this);
            this.mSix.setOnClickListener(this);
            this.mSeven.setOnClickListener(this);
            this.mEight.setOnClickListener(this);
            this.mNine.setOnClickListener(this);
            this.mCancelButton.setOnClickListener(this);
        }

        private int checkDigit(View paramView)
        {
            int i = -1;
            if (paramView == this.mZero)
                i = 0;
            while (true)
            {
                return i;
                if (paramView == this.mOne)
                    i = 1;
                else if (paramView == this.mTwo)
                    i = 2;
                else if (paramView == this.mThree)
                    i = 3;
                else if (paramView == this.mFour)
                    i = 4;
                else if (paramView == this.mFive)
                    i = 5;
                else if (paramView == this.mSix)
                    i = 6;
                else if (paramView == this.mSeven)
                    i = 7;
                else if (paramView == this.mEight)
                    i = 8;
                else if (paramView == this.mNine)
                    i = 9;
            }
        }

        public void onClick(View paramView)
        {
            if (paramView == this.mCancelButton)
            {
                SimPukUnlockScreen.this.mPinText.setText("");
                SimPukUnlockScreen.this.mPukText.setText("");
                SimPukUnlockScreen.this.mCallback.goToLockScreen();
            }
            while (true)
            {
                return;
                int i = checkDigit(paramView);
                if (i >= 0)
                {
                    SimPukUnlockScreen.this.mCallback.pokeWakelock(5000);
                    SimPukUnlockScreen.this.reportDigit(i);
                }
            }
        }
    }

    private abstract class CheckSimPuk extends Thread
    {
        private final String mPin;
        private final String mPuk;

        protected CheckSimPuk(String paramString1, String arg3)
        {
            this.mPuk = paramString1;
            Object localObject;
            this.mPin = localObject;
        }

        abstract void onSimLockChangedResponse(boolean paramBoolean);

        public void run()
        {
            try
            {
                final boolean bool = ITelephony.Stub.asInterface(ServiceManager.checkService("phone")).supplyPuk(this.mPuk, this.mPin);
                SimPukUnlockScreen.this.post(new Runnable()
                {
                    public void run()
                    {
                        SimPukUnlockScreen.CheckSimPuk.this.onSimLockChangedResponse(bool);
                    }
                });
                return;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                    SimPukUnlockScreen.this.post(new Runnable()
                    {
                        public void run()
                        {
                            SimPukUnlockScreen.CheckSimPuk.this.onSimLockChangedResponse(false);
                        }
                    });
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.SimPukUnlockScreen
 * JD-Core Version:        0.6.2
 */