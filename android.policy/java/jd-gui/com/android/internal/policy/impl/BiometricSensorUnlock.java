package com.android.internal.policy.impl;

import android.view.View;

abstract interface BiometricSensorUnlock
{
    public abstract void cleanUp();

    public abstract int getQuality();

    public abstract void hide();

    public abstract void initializeView(View paramView);

    public abstract boolean isRunning();

    public abstract void show(long paramLong);

    public abstract boolean start();

    public abstract boolean stop();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.BiometricSensorUnlock
 * JD-Core Version:        0.6.2
 */