package com.android.internal.policy.impl;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorDescription;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings.Secure;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.android.internal.widget.LockPatternUtils;
import java.io.IOException;
import java.util.ArrayList;
import miui.provider.ExtraSettings.Secure;

public class MiuiAccountUnlockScreen extends RelativeLayout
    implements KeyguardScreen, View.OnClickListener, TextWatcher
{
    private static final int AWAKE_POKE_MILLIS = 30000;
    private static final String CLOSE_FIND_DEVICE_ACTION = "com.xiaomi.action.DISABLE_FIND_DEVICE";
    private static final String LOCK_PATTERN_CLASS = "com.android.settings.ChooseLockGeneric";
    private static final String LOCK_PATTERN_PACKAGE = "com.android.settings";
    private ArrayList<Account> mAccounts = new ArrayList();
    private KeyguardScreenCallback mCallback;
    private ProgressDialog mCheckingDialog;
    private Account mCurAccount;
    private int mGoogleAccountSize;
    private Drawable mGoogleIcon;
    private TextView mInstructions;
    private boolean mIsLockedBySimChange;
    private KeyguardStatusViewManager mKeyguardStatusViewManager;
    private LockPatternUtils mLockPatternUtils;
    private Spinner mLogin;
    private Button mOk;
    private EditText mPassword;
    private TextView mTopHeader;
    private KeyguardUpdateMonitor mUpdateMonitor;
    private Drawable mXiaomiIcon;

    public MiuiAccountUnlockScreen(Context paramContext, Configuration paramConfiguration, KeyguardUpdateMonitor paramKeyguardUpdateMonitor, KeyguardScreenCallback paramKeyguardScreenCallback, LockPatternUtils paramLockPatternUtils)
    {
        super(paramContext);
        this.mCallback = paramKeyguardScreenCallback;
        this.mLockPatternUtils = paramLockPatternUtils;
        LayoutInflater.from(paramContext).inflate(100859960, this, true);
        this.mTopHeader = ((TextView)findViewById(101384311));
        TextView localTextView = this.mTopHeader;
        if (this.mLockPatternUtils.isPermanentlyLocked());
        for (int i = 101450199; ; i = 101450200)
        {
            localTextView.setText(i);
            this.mInstructions = ((TextView)findViewById(101384312));
            this.mLogin = ((Spinner)findViewById(101384313));
            this.mIsLockedBySimChange = isLockedBySimChange();
            findAccounts();
            BaseAdapter local1 = new BaseAdapter()
            {
                public int getCount()
                {
                    return MiuiAccountUnlockScreen.this.mAccounts.size();
                }

                public Object getItem(int paramAnonymousInt)
                {
                    return null;
                }

                public long getItemId(int paramAnonymousInt)
                {
                    return 0L;
                }

                public View getView(int paramAnonymousInt, View paramAnonymousView, ViewGroup paramAnonymousViewGroup)
                {
                    MiuiAccountUnlockScreen.ViewHolder localViewHolder;
                    if (paramAnonymousView == null)
                    {
                        localViewHolder = new MiuiAccountUnlockScreen.ViewHolder(MiuiAccountUnlockScreen.this, null);
                        paramAnonymousView = LayoutInflater.from(MiuiAccountUnlockScreen.this.getContext()).inflate(100859961, null);
                        localViewHolder.mAccountIcon = ((ImageView)paramAnonymousView.findViewById(101384316));
                        localViewHolder.mAccountId = ((TextView)paramAnonymousView.findViewById(101384317));
                        paramAnonymousView.setTag(localViewHolder);
                        if (paramAnonymousInt >= MiuiAccountUnlockScreen.this.mGoogleAccountSize)
                            break label134;
                        localViewHolder.mAccountIcon.setImageDrawable(MiuiAccountUnlockScreen.this.mGoogleIcon);
                    }
                    while (true)
                    {
                        localViewHolder.mAccountId.setText(((Account)MiuiAccountUnlockScreen.this.mAccounts.get(paramAnonymousInt)).name);
                        return paramAnonymousView;
                        localViewHolder = (MiuiAccountUnlockScreen.ViewHolder)paramAnonymousView.getTag();
                        break;
                        label134: localViewHolder.mAccountIcon.setImageDrawable(MiuiAccountUnlockScreen.this.mXiaomiIcon);
                    }
                }
            };
            this.mLogin.setAdapter(local1);
            this.mLogin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
            {
                public void onItemSelected(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
                {
                    MiuiAccountUnlockScreen.access$502(MiuiAccountUnlockScreen.this, (Account)MiuiAccountUnlockScreen.this.mAccounts.get(paramAnonymousInt));
                }

                public void onNothingSelected(AdapterView<?> paramAnonymousAdapterView)
                {
                    MiuiAccountUnlockScreen.access$502(MiuiAccountUnlockScreen.this, null);
                }
            });
            this.mPassword = ((EditText)findViewById(101384314));
            this.mPassword.addTextChangedListener(this);
            this.mOk = ((Button)findViewById(101384315));
            this.mOk.setOnClickListener(this);
            this.mUpdateMonitor = paramKeyguardUpdateMonitor;
            this.mKeyguardStatusViewManager = new KeyguardStatusViewManager(this, paramKeyguardUpdateMonitor, paramLockPatternUtils, paramKeyguardScreenCallback, true);
            return;
        }
    }

    private void asyncCheckPassword()
    {
        this.mCallback.pokeWakelock(30000);
        String str = this.mPassword.getText().toString();
        if (this.mCurAccount == null)
            postOnCheckPasswordResult(false);
        while (true)
        {
            return;
            getProgressDialog().show();
            Bundle localBundle = new Bundle();
            localBundle.putString("password", str);
            AccountManager.get(this.mContext).confirmCredentials(this.mCurAccount, localBundle, null, new AccountManagerCallback()
            {
                public void run(AccountManagerFuture<Bundle> paramAnonymousAccountManagerFuture)
                {
                    try
                    {
                        MiuiAccountUnlockScreen.this.mCallback.pokeWakelock(30000);
                        boolean bool = ((Bundle)paramAnonymousAccountManagerFuture.getResult()).getBoolean("booleanResult");
                        MiuiAccountUnlockScreen.this.postOnCheckPasswordResult(bool);
                        return;
                    }
                    catch (OperationCanceledException localOperationCanceledException)
                    {
                        while (true)
                        {
                            MiuiAccountUnlockScreen.this.postOnCheckPasswordResult(false);
                            MiuiAccountUnlockScreen.this.mLogin.post(new Runnable()
                            {
                                public void run()
                                {
                                    MiuiAccountUnlockScreen.this.getProgressDialog().hide();
                                }
                            });
                        }
                    }
                    catch (IOException localIOException)
                    {
                        while (true)
                        {
                            MiuiAccountUnlockScreen.this.postOnCheckPasswordResult(false);
                            MiuiAccountUnlockScreen.this.mLogin.post(new Runnable()
                            {
                                public void run()
                                {
                                    MiuiAccountUnlockScreen.this.getProgressDialog().hide();
                                }
                            });
                        }
                    }
                    catch (AuthenticatorException localAuthenticatorException)
                    {
                        while (true)
                        {
                            MiuiAccountUnlockScreen.this.postOnCheckPasswordResult(false);
                            MiuiAccountUnlockScreen.this.mLogin.post(new Runnable()
                            {
                                public void run()
                                {
                                    MiuiAccountUnlockScreen.this.getProgressDialog().hide();
                                }
                            });
                        }
                    }
                    finally
                    {
                        MiuiAccountUnlockScreen.this.mLogin.post(new Runnable()
                        {
                            public void run()
                            {
                                MiuiAccountUnlockScreen.this.getProgressDialog().hide();
                            }
                        });
                    }
                }
            }
            , null);
        }
    }

    private void findAccounts()
    {
        AuthenticatorDescription[] arrayOfAuthenticatorDescription = AccountManager.get(this.mContext).getAuthenticatorTypes();
        int i = arrayOfAuthenticatorDescription.length;
        for (int j = 0; ; j++)
        {
            AuthenticatorDescription localAuthenticatorDescription;
            if (j < i)
                localAuthenticatorDescription = arrayOfAuthenticatorDescription[j];
            try
            {
                Context localContext = this.mContext.createPackageContext(localAuthenticatorDescription.packageName, 0);
                if (localAuthenticatorDescription.type.equals("com.google"))
                    this.mGoogleIcon = localContext.getResources().getDrawable(localAuthenticatorDescription.iconId);
                else if (localAuthenticatorDescription.type.equals("com.xiaomi"))
                    this.mXiaomiIcon = localContext.getResources().getDrawable(localAuthenticatorDescription.iconId);
            }
            catch (PackageManager.NameNotFoundException localNameNotFoundException)
            {
                continue;
                if (this.mIsLockedBySimChange)
                {
                    this.mInstructions.setText(101450213);
                    this.mTopHeader.setVisibility(8);
                }
                while (true)
                {
                    for (Account localAccount2 : AccountManager.get(this.mContext).getAccountsByType("com.xiaomi"))
                        this.mAccounts.add(localAccount2);
                    Account[] arrayOfAccount1 = AccountManager.get(this.mContext).getAccountsByType("com.google");
                    this.mGoogleAccountSize = arrayOfAccount1.length;
                    int k = arrayOfAccount1.length;
                    for (int m = 0; m < k; m++)
                    {
                        Account localAccount1 = arrayOfAccount1[m];
                        this.mAccounts.add(localAccount1);
                    }
                }
                return;
            }
            catch (Resources.NotFoundException localNotFoundException)
            {
            }
        }
    }

    private Dialog getProgressDialog()
    {
        if (this.mCheckingDialog == null)
        {
            this.mCheckingDialog = new ProgressDialog(this.mContext);
            this.mCheckingDialog.setMessage(this.mContext.getString(101450202));
            this.mCheckingDialog.setIndeterminate(true);
            this.mCheckingDialog.setCancelable(false);
            this.mCheckingDialog.getWindow().setType(2009);
        }
        return this.mCheckingDialog;
    }

    private boolean isLockedBySimChange()
    {
        int i = 1;
        if (Settings.Secure.getInt(this.mContext.getContentResolver(), ExtraSettings.Secure.PERMANENTLY_LOCK_SIM_CHANGE, 0) == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    private void postOnCheckPasswordResult(final boolean paramBoolean)
    {
        this.mLogin.post(new Runnable()
        {
            public void run()
            {
                Intent localIntent;
                if (paramBoolean)
                {
                    MiuiAccountUnlockScreen.this.mLockPatternUtils.setPermanentlyLocked(false);
                    MiuiAccountUnlockScreen.this.mLockPatternUtils.setLockPatternEnabled(false);
                    MiuiAccountUnlockScreen.this.mLockPatternUtils.saveLockPattern(null);
                    localIntent = new Intent();
                    if (MiuiAccountUnlockScreen.this.mIsLockedBySimChange)
                    {
                        localIntent.setAction("com.xiaomi.action.DISABLE_FIND_DEVICE");
                        localIntent.putExtra("account", (Parcelable)MiuiAccountUnlockScreen.this.mAccounts.get(0));
                        MiuiAccountUnlockScreen.this.mContext.startService(localIntent);
                        MiuiAccountUnlockScreen.this.mCallback.reportSuccessfulUnlockAttempt();
                        MiuiAccountUnlockScreen.this.mCallback.keyguardDone(true);
                    }
                }
                while (true)
                {
                    return;
                    localIntent.setClassName("com.android.settings", "com.android.settings.ChooseLockGeneric");
                    localIntent.setFlags(268435456);
                    MiuiAccountUnlockScreen.this.mContext.startActivity(localIntent);
                    break;
                    MiuiAccountUnlockScreen.this.mInstructions.setText(101450201);
                    MiuiAccountUnlockScreen.this.mPassword.setText("");
                    MiuiAccountUnlockScreen.this.mCallback.reportFailedUnlockAttempt();
                }
            }
        });
    }

    public void afterTextChanged(Editable paramEditable)
    {
    }

    public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
    }

    public void cleanUp()
    {
        if (this.mCheckingDialog != null)
            this.mCheckingDialog.hide();
        this.mUpdateMonitor.removeCallback(this);
        this.mCallback = null;
        this.mLockPatternUtils = null;
        this.mUpdateMonitor = null;
    }

    public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
    {
        if ((paramKeyEvent.getAction() == 0) && (paramKeyEvent.getKeyCode() == 4))
            if (this.mLockPatternUtils.isPermanentlyLocked())
                this.mCallback.goToLockScreen();
        for (boolean bool = true; ; bool = super.dispatchKeyEvent(paramKeyEvent))
        {
            return bool;
            this.mCallback.forgotPattern(false);
            break;
        }
    }

    public boolean needsInput()
    {
        return true;
    }

    public void onClick(View paramView)
    {
        this.mCallback.pokeWakelock();
        if (paramView == this.mOk)
            asyncCheckPassword();
    }

    public void onPause()
    {
        this.mKeyguardStatusViewManager.onPause();
    }

    protected boolean onRequestFocusInDescendants(int paramInt, Rect paramRect)
    {
        return this.mPassword.requestFocus(paramInt, paramRect);
    }

    public void onResume()
    {
        this.mPassword.setText("");
        this.mPassword.requestFocus();
        this.mKeyguardStatusViewManager.onResume();
    }

    public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
        this.mCallback.pokeWakelock(30000);
    }

    private final class ViewHolder
    {
        public ImageView mAccountIcon;
        public TextView mAccountId;

        private ViewHolder()
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.MiuiAccountUnlockScreen
 * JD-Core Version:        0.6.2
 */