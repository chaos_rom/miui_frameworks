package com.android.internal.policy.impl;

import android.content.res.Configuration;

public abstract interface KeyguardScreenCallback extends KeyguardViewCallback
{
    public abstract boolean doesFallbackUnlockScreenExist();

    public abstract void forgotPattern(boolean paramBoolean);

    public abstract void goToLockScreen();

    public abstract void goToUnlockScreen();

    public abstract boolean isSecure();

    public abstract boolean isVerifyUnlockOnly();

    public abstract void recreateMe(Configuration paramConfiguration);

    public abstract void reportFailedUnlockAttempt();

    public abstract void reportSuccessfulUnlockAttempt();

    public abstract void takeEmergencyCallAction();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.KeyguardScreenCallback
 * JD-Core Version:        0.6.2
 */