package com.android.internal.policy.impl;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.KeyguardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.AndroidRuntimeException;
import android.util.DisplayMetrics;
import android.util.EventLog;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.ContextThemeWrapper;
import android.view.Display;
import android.view.IRotationWatcher.Stub;
import android.view.IWindowManager;
import android.view.IWindowManager.Stub;
import android.view.InputQueue.Callback;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.KeyEvent.DispatcherState;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder.Callback2;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewManager;
import android.view.ViewParent;
import android.view.ViewStub;
import android.view.Window;
import android.view.Window.Callback;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.android.internal.R.styleable;
import com.android.internal.view.RootViewSurfaceTaker;
import com.android.internal.view.StandaloneActionMode;
import com.android.internal.view.menu.ContextMenuBuilder;
import com.android.internal.view.menu.IconMenuPresenter;
import com.android.internal.view.menu.ListMenuPresenter;
import com.android.internal.view.menu.MenuBuilder;
import com.android.internal.view.menu.MenuBuilder.Callback;
import com.android.internal.view.menu.MenuDialogHelper;
import com.android.internal.view.menu.MenuPresenter.Callback;
import com.android.internal.view.menu.MenuView;
import com.android.internal.widget.ActionBarContainer;
import com.android.internal.widget.ActionBarContextView;
import com.android.internal.widget.ActionBarView;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class PhoneWindow extends Window
    implements MenuBuilder.Callback
{
    private static final String ACTION_BAR_TAG = "android:ActionBar";
    private static final String FOCUSED_ID_TAG = "android:focusedViewId";
    private static final String PANELS_TAG = "android:Panels";
    private static final boolean SWEEP_OPEN_MENU = false;
    private static final String TAG = "PhoneWindow";
    private static final String VIEWS_TAG = "android:views";
    static final RotationWatcher sRotationWatcher = new RotationWatcher();
    private ActionBarView mActionBar;
    private ActionMenuPresenterCallback mActionMenuPresenterCallback;
    private boolean mAlwaysReadCloseOnTouchAttr = false;
    private AudioManager mAudioManager;
    private Drawable mBackgroundDrawable;
    private int mBackgroundResource = 0;
    private ProgressBar mCircularProgressBar;
    private boolean mClosingActionMenu;
    private ViewGroup mContentParent;
    private ContextMenuBuilder mContextMenu;
    final DialogMenuCallback mContextMenuCallback = new DialogMenuCallback(6);
    private MenuDialogHelper mContextMenuHelper;
    private DecorView mDecor;
    private DrawableFeatureState[] mDrawables;
    TypedValue mFixedHeightMajor;
    TypedValue mFixedHeightMinor;
    TypedValue mFixedWidthMajor;
    TypedValue mFixedWidthMinor;
    private int mFrameResource = 0;
    private ProgressBar mHorizontalProgressBar;
    private boolean mIsFloating;
    private KeyguardManager mKeyguardManager;
    private LayoutInflater mLayoutInflater;
    private ImageView mLeftIconView;
    final TypedValue mMinWidthMajor = new TypedValue();
    final TypedValue mMinWidthMinor = new TypedValue();
    private int mPanelChordingKey;
    private PanelMenuPresenterCallback mPanelMenuPresenterCallback;
    private PanelFeatureState[] mPanels;
    private PanelFeatureState mPreparedPanel;
    private ImageView mRightIconView;
    InputQueue.Callback mTakeInputQueueCallback;
    SurfaceHolder.Callback2 mTakeSurfaceCallback;
    private int mTextColor = 0;
    private CharSequence mTitle = null;
    private int mTitleColor = 0;
    private TextView mTitleView;
    private int mUiOptions = 0;
    private int mVolumeControlStreamType = -2147483648;

    public PhoneWindow(Context paramContext)
    {
        super(paramContext);
        this.mLayoutInflater = LayoutInflater.from(paramContext);
    }

    private void callOnPanelClosed(int paramInt, PanelFeatureState paramPanelFeatureState, Menu paramMenu)
    {
        Window.Callback localCallback = getCallback();
        if (localCallback == null);
        while (true)
        {
            return;
            if (paramMenu == null)
            {
                if ((paramPanelFeatureState == null) && (paramInt >= 0) && (paramInt < this.mPanels.length))
                    paramPanelFeatureState = this.mPanels[paramInt];
                if (paramPanelFeatureState != null)
                    paramMenu = paramPanelFeatureState.menu;
            }
            if (((paramPanelFeatureState == null) || (paramPanelFeatureState.isOpen)) && (!isDestroyed()))
                localCallback.onPanelClosed(paramInt, paramMenu);
        }
    }

    private static void clearMenuViews(PanelFeatureState paramPanelFeatureState)
    {
        paramPanelFeatureState.createdPanelView = null;
        paramPanelFeatureState.refreshDecorView = true;
        paramPanelFeatureState.clearMenuPresenters();
    }

    /** @deprecated */
    private void closeContextMenu()
    {
        try
        {
            if (this.mContextMenu != null)
            {
                this.mContextMenu.close();
                dismissContextMenu();
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    private void dismissContextMenu()
    {
        try
        {
            this.mContextMenu = null;
            if (this.mContextMenuHelper != null)
            {
                this.mContextMenuHelper.dismiss();
                this.mContextMenuHelper = null;
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private ProgressBar getCircularProgressBar(boolean paramBoolean)
    {
        if (this.mCircularProgressBar != null);
        for (ProgressBar localProgressBar = this.mCircularProgressBar; ; localProgressBar = this.mCircularProgressBar)
        {
            return localProgressBar;
            if ((this.mContentParent == null) && (paramBoolean))
                installDecor();
            this.mCircularProgressBar = ((ProgressBar)findViewById(16909084));
            if (this.mCircularProgressBar != null)
                this.mCircularProgressBar.setVisibility(4);
        }
    }

    private DrawableFeatureState getDrawableState(int paramInt, boolean paramBoolean)
    {
        DrawableFeatureState localDrawableFeatureState;
        if ((getFeatures() & 1 << paramInt) == 0)
            if (!paramBoolean)
                localDrawableFeatureState = null;
        while (true)
        {
            return localDrawableFeatureState;
            throw new RuntimeException("The feature has not been requested");
            Object localObject = this.mDrawables;
            if ((localObject == null) || (localObject.length <= paramInt))
            {
                DrawableFeatureState[] arrayOfDrawableFeatureState = new DrawableFeatureState[paramInt + 1];
                if (localObject != null)
                    System.arraycopy(localObject, 0, arrayOfDrawableFeatureState, 0, localObject.length);
                localObject = arrayOfDrawableFeatureState;
                this.mDrawables = arrayOfDrawableFeatureState;
            }
            localDrawableFeatureState = localObject[paramInt];
            if (localDrawableFeatureState == null)
            {
                localDrawableFeatureState = new DrawableFeatureState(paramInt);
                localObject[paramInt] = localDrawableFeatureState;
            }
        }
    }

    private ProgressBar getHorizontalProgressBar(boolean paramBoolean)
    {
        if (this.mHorizontalProgressBar != null);
        for (ProgressBar localProgressBar = this.mHorizontalProgressBar; ; localProgressBar = this.mHorizontalProgressBar)
        {
            return localProgressBar;
            if ((this.mContentParent == null) && (paramBoolean))
                installDecor();
            this.mHorizontalProgressBar = ((ProgressBar)findViewById(16909085));
            if (this.mHorizontalProgressBar != null)
                this.mHorizontalProgressBar.setVisibility(4);
        }
    }

    private KeyguardManager getKeyguardManager()
    {
        if (this.mKeyguardManager == null)
            this.mKeyguardManager = ((KeyguardManager)getContext().getSystemService("keyguard"));
        return this.mKeyguardManager;
    }

    private ImageView getLeftIconView()
    {
        ImageView localImageView;
        if (this.mLeftIconView != null)
            localImageView = this.mLeftIconView;
        while (true)
        {
            return localImageView;
            if (this.mContentParent == null)
                installDecor();
            localImageView = (ImageView)findViewById(16908850);
            this.mLeftIconView = localImageView;
        }
    }

    private int getOptionsPanelGravity()
    {
        try
        {
            int j = WindowManagerHolder.sWindowManager.getPreferredOptionsPanelGravity();
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("PhoneWindow", "Couldn't getOptionsPanelGravity; using default", localRemoteException);
                int i = 81;
            }
        }
    }

    private PanelFeatureState getPanelState(int paramInt, boolean paramBoolean)
    {
        return getPanelState(paramInt, paramBoolean, null);
    }

    private PanelFeatureState getPanelState(int paramInt, boolean paramBoolean, PanelFeatureState paramPanelFeatureState)
    {
        if ((getFeatures() & 1 << paramInt) == 0)
            if (!paramBoolean)
                localPanelFeatureState = null;
        Object localObject;
        do
        {
            return localPanelFeatureState;
            throw new RuntimeException("The feature has not been requested");
            localObject = this.mPanels;
            if ((localObject == null) || (localObject.length <= paramInt))
            {
                PanelFeatureState[] arrayOfPanelFeatureState = new PanelFeatureState[paramInt + 1];
                if (localObject != null)
                    System.arraycopy(localObject, 0, arrayOfPanelFeatureState, 0, localObject.length);
                localObject = arrayOfPanelFeatureState;
                this.mPanels = arrayOfPanelFeatureState;
            }
            localPanelFeatureState = localObject[paramInt];
        }
        while (localPanelFeatureState != null);
        if (paramPanelFeatureState != null);
        for (PanelFeatureState localPanelFeatureState = paramPanelFeatureState; ; localPanelFeatureState = new PanelFeatureState(paramInt))
        {
            localObject[paramInt] = localPanelFeatureState;
            break;
        }
    }

    private ImageView getRightIconView()
    {
        ImageView localImageView;
        if (this.mRightIconView != null)
            localImageView = this.mRightIconView;
        while (true)
        {
            return localImageView;
            if (this.mContentParent == null)
                installDecor();
            localImageView = (ImageView)findViewById(16908852);
            this.mRightIconView = localImageView;
        }
    }

    private void hideProgressBars(ProgressBar paramProgressBar1, ProgressBar paramProgressBar2)
    {
        int i = getLocalFeatures();
        Animation localAnimation = AnimationUtils.loadAnimation(getContext(), 17432577);
        localAnimation.setDuration(1000L);
        if (((i & 0x20) != 0) && (paramProgressBar2.getVisibility() == 0))
        {
            paramProgressBar2.startAnimation(localAnimation);
            paramProgressBar2.setVisibility(4);
        }
        if (((i & 0x4) != 0) && (paramProgressBar1.getVisibility() == 0))
        {
            paramProgressBar1.startAnimation(localAnimation);
            paramProgressBar1.setVisibility(4);
        }
    }

    private void installDecor()
    {
        boolean bool1 = true;
        if (this.mDecor == null)
        {
            this.mDecor = generateDecor();
            this.mDecor.setDescendantFocusability(262144);
            this.mDecor.setIsRootNamespace(bool1);
        }
        if (this.mContentParent == null)
        {
            this.mContentParent = generateLayout(this.mDecor);
            this.mDecor.makeOptionalFitsSystemWindows();
            this.mTitleView = ((TextView)findViewById(16908310));
            if (this.mTitleView == null)
                break label160;
            if ((0x2 & getLocalFeatures()) == 0)
                break label146;
            View localView = findViewById(16908853);
            if (localView == null)
                break label134;
            localView.setVisibility(8);
            if ((this.mContentParent instanceof FrameLayout))
                ((FrameLayout)this.mContentParent).setForeground(null);
        }
        label134: label146: label160: 
        do
        {
            while (true)
            {
                return;
                this.mTitleView.setVisibility(8);
                break;
                this.mTitleView.setText(this.mTitle);
            }
            this.mActionBar = ((ActionBarView)findViewById(16909087));
        }
        while (this.mActionBar == null);
        this.mActionBar.setWindowCallback(getCallback());
        if (this.mActionBar.getTitle() == null)
            this.mActionBar.setWindowTitle(this.mTitle);
        int i = getLocalFeatures();
        if ((i & 0x4) != 0)
            this.mActionBar.initProgress();
        if ((i & 0x20) != 0)
            this.mActionBar.initIndeterminateProgress();
        label254: boolean bool2;
        if ((0x1 & this.mUiOptions) != 0)
        {
            if (!bool1)
                break label369;
            bool2 = getContext().getResources().getBoolean(17891330);
            label272: ActionBarContainer localActionBarContainer = (ActionBarContainer)findViewById(16909089);
            if (localActionBarContainer == null)
                break label383;
            this.mActionBar.setSplitView(localActionBarContainer);
            this.mActionBar.setSplitActionBar(bool2);
            this.mActionBar.setSplitWhenNarrow(bool1);
            ActionBarContextView localActionBarContextView = (ActionBarContextView)findViewById(16909088);
            localActionBarContextView.setSplitView(localActionBarContainer);
            localActionBarContextView.setSplitActionBar(bool2);
            localActionBarContextView.setSplitWhenNarrow(bool1);
        }
        while (true)
        {
            this.mDecor.post(new Runnable()
            {
                public void run()
                {
                    PhoneWindow.PanelFeatureState localPanelFeatureState = PhoneWindow.this.getPanelState(0, false);
                    if ((!PhoneWindow.this.isDestroyed()) && ((localPanelFeatureState == null) || (localPanelFeatureState.menu == null)))
                        PhoneWindow.this.invalidatePanelMenu(8);
                }
            });
            break;
            bool1 = false;
            break label254;
            label369: bool2 = getWindowStyle().getBoolean(22, false);
            break label272;
            label383: if (bool2)
                Log.e("PhoneWindow", "Requested split action bar with incompatible window decor! Ignoring request.");
        }
    }

    private boolean launchDefaultSearch()
    {
        Window.Callback localCallback = getCallback();
        if ((localCallback == null) || (isDestroyed()));
        for (boolean bool = false; ; bool = localCallback.onSearchRequested())
        {
            return bool;
            sendCloseSystemWindows("search");
        }
    }

    private Drawable loadImageURI(Uri paramUri)
    {
        Object localObject = null;
        try
        {
            Drawable localDrawable = Drawable.createFromStream(getContext().getContentResolver().openInputStream(paramUri), null);
            localObject = localDrawable;
            return localObject;
        }
        catch (Exception localException)
        {
            while (true)
                Log.w("PhoneWindow", "Unable to open content: " + paramUri);
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private void openPanel(PanelFeatureState paramPanelFeatureState, KeyEvent paramKeyEvent)
    {
        if ((paramPanelFeatureState.isOpen) || (isDestroyed()));
        label49: label110: label116: label120: label122: WindowManager localWindowManager;
        int i;
        do
        {
            do
            {
                while (true)
                {
                    return;
                    int k;
                    if (paramPanelFeatureState.featureId == 0)
                    {
                        Context localContext = getContext();
                        if ((0xF & localContext.getResources().getConfiguration().screenLayout) != 4)
                            break label110;
                        k = 1;
                        if (localContext.getApplicationInfo().targetSdkVersion < 11)
                            break label116;
                    }
                    for (int m = 1; ; m = 0)
                    {
                        if ((k != 0) && (m != 0))
                            break label120;
                        Window.Callback localCallback = getCallback();
                        if ((localCallback == null) || (localCallback.onMenuOpened(paramPanelFeatureState.featureId, paramPanelFeatureState.menu)))
                            break label122;
                        closePanel(paramPanelFeatureState, true);
                        break;
                        k = 0;
                        break label49;
                    }
                }
                localWindowManager = getWindowManager();
            }
            while ((localWindowManager == null) || (!preparePanel(paramPanelFeatureState, paramKeyEvent)));
            i = -2;
            if ((paramPanelFeatureState.decorView != null) && (!paramPanelFeatureState.refreshDecorView))
                break label464;
            if (paramPanelFeatureState.decorView != null)
                break;
        }
        while ((!initializePanelDecor(paramPanelFeatureState)) || (paramPanelFeatureState.decorView == null));
        label182: int j;
        label244: WindowManager.LayoutParams localLayoutParams;
        if ((initializePanelContent(paramPanelFeatureState)) && (paramPanelFeatureState.hasPanelItems()))
        {
            ViewGroup.LayoutParams localLayoutParams1 = paramPanelFeatureState.shownPanelView.getLayoutParams();
            if (localLayoutParams1 == null)
                localLayoutParams1 = new ViewGroup.LayoutParams(-2, -2);
            if (localLayoutParams1.width != -1)
                break label455;
            j = paramPanelFeatureState.fullBackground;
            i = -1;
            paramPanelFeatureState.decorView.setWindowBackground(getContext().getResources().getDrawable(j));
            ViewParent localViewParent = paramPanelFeatureState.shownPanelView.getParent();
            if ((localViewParent != null) && ((localViewParent instanceof ViewGroup)))
                ((ViewGroup)localViewParent).removeView(paramPanelFeatureState.shownPanelView);
            paramPanelFeatureState.decorView.addView(paramPanelFeatureState.shownPanelView, localLayoutParams1);
            if (!paramPanelFeatureState.shownPanelView.hasFocus())
                paramPanelFeatureState.shownPanelView.requestFocus();
            label328: paramPanelFeatureState.isOpen = true;
            paramPanelFeatureState.isHandled = false;
            localLayoutParams = new WindowManager.LayoutParams(i, -2, paramPanelFeatureState.x, paramPanelFeatureState.y, 1003, 8523776, paramPanelFeatureState.decorView.mDefaultOpacity);
            if (!paramPanelFeatureState.isCompact)
                break label516;
            localLayoutParams.gravity = getOptionsPanelGravity();
            sRotationWatcher.addWindow(this);
        }
        while (true)
        {
            localLayoutParams.windowAnimations = paramPanelFeatureState.windowAnimations;
            Injector.handleIcsAppLayoutParams(this, localWindowManager, localLayoutParams);
            localWindowManager.addView(paramPanelFeatureState.decorView, localLayoutParams);
            break;
            if ((!paramPanelFeatureState.refreshDecorView) || (paramPanelFeatureState.decorView.getChildCount() <= 0))
                break label182;
            paramPanelFeatureState.decorView.removeAllViews();
            break label182;
            break;
            label455: j = paramPanelFeatureState.background;
            break label244;
            label464: if (!paramPanelFeatureState.isInListMode())
            {
                i = -1;
                break label328;
            }
            if (paramPanelFeatureState.createdPanelView == null)
                break label328;
            ViewGroup.LayoutParams localLayoutParams2 = paramPanelFeatureState.createdPanelView.getLayoutParams();
            if ((localLayoutParams2 == null) || (localLayoutParams2.width != -1))
                break label328;
            i = -1;
            break label328;
            label516: localLayoutParams.gravity = paramPanelFeatureState.gravity;
        }
    }

    private void openPanelsAfterRestore()
    {
        PanelFeatureState[] arrayOfPanelFeatureState = this.mPanels;
        if (arrayOfPanelFeatureState == null);
        while (true)
        {
            return;
            for (int i = -1 + arrayOfPanelFeatureState.length; i >= 0; i--)
            {
                PanelFeatureState localPanelFeatureState = arrayOfPanelFeatureState[i];
                if (localPanelFeatureState != null)
                {
                    localPanelFeatureState.applyFrozenState();
                    if ((!localPanelFeatureState.isOpen) && (localPanelFeatureState.wasLastOpen))
                    {
                        localPanelFeatureState.isInExpandedMode = localPanelFeatureState.wasLastExpanded;
                        openPanel(localPanelFeatureState, null);
                    }
                }
            }
        }
    }

    private boolean performPanelShortcut(PanelFeatureState paramPanelFeatureState, int paramInt1, KeyEvent paramKeyEvent, int paramInt2)
    {
        boolean bool;
        if ((paramKeyEvent.isSystem()) || (paramPanelFeatureState == null))
            bool = false;
        while (true)
        {
            return bool;
            bool = false;
            if (((paramPanelFeatureState.isPrepared) || (preparePanel(paramPanelFeatureState, paramKeyEvent))) && (paramPanelFeatureState.menu != null))
                bool = paramPanelFeatureState.menu.performShortcut(paramInt1, paramKeyEvent, paramInt2);
            if (bool)
            {
                paramPanelFeatureState.isHandled = true;
                if (((paramInt2 & 0x1) == 0) && (this.mActionBar == null))
                    closePanel(paramPanelFeatureState, true);
            }
        }
    }

    private void reopenMenu(boolean paramBoolean)
    {
        if ((this.mActionBar != null) && (this.mActionBar.isOverflowReserved()))
        {
            Window.Callback localCallback = getCallback();
            if ((!this.mActionBar.isOverflowMenuShowing()) || (!paramBoolean))
                if ((localCallback != null) && (!isDestroyed()) && (this.mActionBar.getVisibility() == 0))
                {
                    PanelFeatureState localPanelFeatureState2 = getPanelState(0, true);
                    if (localCallback.onPreparePanel(0, localPanelFeatureState2.createdPanelView, localPanelFeatureState2.menu))
                    {
                        localCallback.onMenuOpened(8, localPanelFeatureState2.menu);
                        this.mActionBar.showOverflowMenu();
                    }
                }
            while (true)
            {
                return;
                this.mActionBar.hideOverflowMenu();
                if ((localCallback != null) && (!isDestroyed()))
                    localCallback.onPanelClosed(8, getPanelState(0, true).menu);
            }
        }
        PanelFeatureState localPanelFeatureState1 = getPanelState(0, true);
        boolean bool;
        if (paramBoolean)
            if (!localPanelFeatureState1.isInExpandedMode)
                bool = true;
        while (true)
        {
            localPanelFeatureState1.refreshDecorView = true;
            closePanel(localPanelFeatureState1, false);
            localPanelFeatureState1.isInExpandedMode = bool;
            openPanel(localPanelFeatureState1, null);
            break;
            bool = false;
            continue;
            bool = localPanelFeatureState1.isInExpandedMode;
        }
    }

    private void restorePanelState(SparseArray<Parcelable> paramSparseArray)
    {
        int i = -1 + paramSparseArray.size();
        if (i >= 0)
        {
            PanelFeatureState localPanelFeatureState = getPanelState(i, false);
            if (localPanelFeatureState == null);
            while (true)
            {
                i--;
                break;
                localPanelFeatureState.onRestoreInstanceState((Parcelable)paramSparseArray.get(i));
                invalidatePanelMenu(i);
            }
        }
    }

    private void savePanelState(SparseArray<Parcelable> paramSparseArray)
    {
        PanelFeatureState[] arrayOfPanelFeatureState = this.mPanels;
        if (arrayOfPanelFeatureState == null);
        while (true)
        {
            return;
            for (int i = -1 + arrayOfPanelFeatureState.length; i >= 0; i--)
                if (arrayOfPanelFeatureState[i] != null)
                    paramSparseArray.put(i, arrayOfPanelFeatureState[i].onSaveInstanceState());
        }
    }

    private void showProgressBars(ProgressBar paramProgressBar1, ProgressBar paramProgressBar2)
    {
        int i = getLocalFeatures();
        if (((i & 0x20) != 0) && (paramProgressBar2.getVisibility() == 4))
            paramProgressBar2.setVisibility(0);
        if (((i & 0x4) != 0) && (paramProgressBar1.getProgress() < 10000))
            paramProgressBar1.setVisibility(0);
    }

    private void updateDrawable(int paramInt, DrawableFeatureState paramDrawableFeatureState, boolean paramBoolean)
    {
        if (this.mContentParent == null);
        while (true)
        {
            return;
            int i = 1 << paramInt;
            if (((i & getFeatures()) != 0) || (paramBoolean))
            {
                Drawable localDrawable = null;
                if (paramDrawableFeatureState != null)
                {
                    localDrawable = paramDrawableFeatureState.child;
                    if (localDrawable == null)
                        localDrawable = paramDrawableFeatureState.local;
                    if (localDrawable == null)
                        localDrawable = paramDrawableFeatureState.def;
                }
                if ((i & getLocalFeatures()) == 0)
                {
                    if ((getContainer() != null) && ((isActive()) || (paramBoolean)))
                        getContainer().setChildDrawable(paramInt, localDrawable);
                }
                else if ((paramDrawableFeatureState != null) && ((paramDrawableFeatureState.cur != localDrawable) || (paramDrawableFeatureState.curAlpha != paramDrawableFeatureState.alpha)))
                {
                    paramDrawableFeatureState.cur = localDrawable;
                    paramDrawableFeatureState.curAlpha = paramDrawableFeatureState.alpha;
                    onDrawableChanged(paramInt, localDrawable, paramDrawableFeatureState.alpha);
                }
            }
        }
    }

    private void updateInt(int paramInt1, int paramInt2, boolean paramBoolean)
    {
        if (this.mContentParent == null);
        while (true)
        {
            return;
            int i = 1 << paramInt1;
            if (((i & getFeatures()) != 0) || (paramBoolean))
                if ((i & getLocalFeatures()) == 0)
                {
                    if (getContainer() != null)
                        getContainer().setChildInt(paramInt1, paramInt2);
                }
                else
                    onIntChanged(paramInt1, paramInt2);
        }
    }

    private void updateProgressBars(int paramInt)
    {
        ProgressBar localProgressBar1 = getCircularProgressBar(true);
        ProgressBar localProgressBar2 = getHorizontalProgressBar(true);
        int i = getLocalFeatures();
        int k;
        if (paramInt == -1)
            if ((i & 0x4) != 0)
            {
                int j = localProgressBar2.getProgress();
                if ((localProgressBar2.isIndeterminate()) || (j < 10000))
                {
                    k = 0;
                    localProgressBar2.setVisibility(k);
                }
            }
            else if ((i & 0x20) != 0)
            {
                localProgressBar1.setVisibility(0);
            }
        while (true)
        {
            return;
            k = 4;
            break;
            if (paramInt == -2)
            {
                if ((i & 0x4) != 0)
                    localProgressBar2.setVisibility(8);
                if ((i & 0x20) != 0)
                    localProgressBar1.setVisibility(8);
            }
            else if (paramInt == -3)
            {
                localProgressBar2.setIndeterminate(true);
            }
            else if (paramInt == -4)
            {
                localProgressBar2.setIndeterminate(false);
            }
            else if ((paramInt >= 0) && (paramInt <= 10000))
            {
                localProgressBar2.setProgress(paramInt + 0);
                if (paramInt < 10000)
                    showProgressBars(localProgressBar2, localProgressBar1);
                else
                    hideProgressBars(localProgressBar2, localProgressBar1);
            }
            else if ((20000 <= paramInt) && (paramInt <= 30000))
            {
                localProgressBar2.setSecondaryProgress(paramInt - 20000);
                showProgressBars(localProgressBar2, localProgressBar1);
            }
        }
    }

    public void addContentView(View paramView, ViewGroup.LayoutParams paramLayoutParams)
    {
        if (this.mContentParent == null)
            installDecor();
        this.mContentParent.addView(paramView, paramLayoutParams);
        Window.Callback localCallback = getCallback();
        if ((localCallback != null) && (!isDestroyed()))
            localCallback.onContentChanged();
    }

    public void alwaysReadCloseOnTouchAttr()
    {
        this.mAlwaysReadCloseOnTouchAttr = true;
    }

    void checkCloseActionMenu(Menu paramMenu)
    {
        if (this.mClosingActionMenu);
        while (true)
        {
            return;
            this.mClosingActionMenu = true;
            this.mActionBar.dismissPopupMenus();
            Window.Callback localCallback = getCallback();
            if ((localCallback != null) && (!isDestroyed()))
                localCallback.onPanelClosed(8, paramMenu);
            this.mClosingActionMenu = false;
        }
    }

    public final void closeAllPanels()
    {
        if (getWindowManager() == null);
        while (true)
        {
            return;
            PanelFeatureState[] arrayOfPanelFeatureState = this.mPanels;
            if (arrayOfPanelFeatureState != null);
            for (int i = arrayOfPanelFeatureState.length; ; i = 0)
                for (int j = 0; j < i; j++)
                {
                    PanelFeatureState localPanelFeatureState = arrayOfPanelFeatureState[j];
                    if (localPanelFeatureState != null)
                        closePanel(localPanelFeatureState, true);
                }
            closeContextMenu();
        }
    }

    public final void closePanel(int paramInt)
    {
        if ((paramInt == 0) && (this.mActionBar != null) && (this.mActionBar.isOverflowReserved()))
            this.mActionBar.hideOverflowMenu();
        while (true)
        {
            return;
            if (paramInt == 6)
                closeContextMenu();
            else
                closePanel(getPanelState(paramInt, true), true);
        }
    }

    public final void closePanel(PanelFeatureState paramPanelFeatureState, boolean paramBoolean)
    {
        if ((paramBoolean) && (paramPanelFeatureState.featureId == 0) && (this.mActionBar != null) && (this.mActionBar.isOverflowMenuShowing()))
            checkCloseActionMenu(paramPanelFeatureState.menu);
        while (true)
        {
            return;
            WindowManager localWindowManager = getWindowManager();
            if ((localWindowManager != null) && (paramPanelFeatureState.isOpen))
            {
                if (paramPanelFeatureState.decorView != null)
                {
                    localWindowManager.removeView(paramPanelFeatureState.decorView);
                    if (paramPanelFeatureState.isCompact)
                        sRotationWatcher.removeWindow(this);
                }
                if (paramBoolean)
                    callOnPanelClosed(paramPanelFeatureState.featureId, paramPanelFeatureState, null);
            }
            paramPanelFeatureState.isPrepared = false;
            paramPanelFeatureState.isHandled = false;
            paramPanelFeatureState.isOpen = false;
            paramPanelFeatureState.shownPanelView = null;
            if (paramPanelFeatureState.isInExpandedMode)
            {
                paramPanelFeatureState.refreshDecorView = true;
                paramPanelFeatureState.isInExpandedMode = false;
            }
            if (this.mPreparedPanel == paramPanelFeatureState)
            {
                this.mPreparedPanel = null;
                this.mPanelChordingKey = 0;
            }
        }
    }

    public PanelFeatureState findMenuPanel(Menu paramMenu)
    {
        PanelFeatureState[] arrayOfPanelFeatureState = this.mPanels;
        int i;
        int j;
        label15: PanelFeatureState localPanelFeatureState;
        if (arrayOfPanelFeatureState != null)
        {
            i = arrayOfPanelFeatureState.length;
            j = 0;
            if (j >= i)
                break label55;
            localPanelFeatureState = arrayOfPanelFeatureState[j];
            if ((localPanelFeatureState == null) || (localPanelFeatureState.menu != paramMenu))
                break label49;
        }
        while (true)
        {
            return localPanelFeatureState;
            i = 0;
            break;
            label49: j++;
            break label15;
            label55: localPanelFeatureState = null;
        }
    }

    protected DecorView generateDecor()
    {
        return new DecorView(getContext(), -1);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    protected ViewGroup generateLayout(DecorView paramDecorView)
    {
        TypedArray localTypedArray = getWindowStyle();
        this.mIsFloating = localTypedArray.getBoolean(4, false);
        int i = 0x10100 & (0xFFFFFFFF ^ getForcedWindowFlags());
        label63: boolean bool1;
        label171: int k;
        label400: int m;
        label410: int n;
        label443: label470: int i1;
        int i2;
        if (this.mIsFloating)
        {
            setLayout(-2, -2);
            setFlags(0, i);
            if (!localTypedArray.getBoolean(3, false))
                break label802;
            requestFeature(1);
            if (localTypedArray.getBoolean(17, false))
                requestFeature(9);
            if (localTypedArray.getBoolean(16, false))
                requestFeature(10);
            if (localTypedArray.getBoolean(9, false))
                setFlags(1024, 0x400 & (0xFFFFFFFF ^ getForcedWindowFlags()));
            if (localTypedArray.getBoolean(14, false))
                setFlags(1048576, 0x100000 & (0xFFFFFFFF ^ getForcedWindowFlags()));
            if (getContext().getApplicationInfo().targetSdkVersion < 11)
                break label822;
            bool1 = true;
            if (localTypedArray.getBoolean(18, bool1))
                setFlags(8388608, 0x800000 & (0xFFFFFFFF ^ getForcedWindowFlags()));
            localTypedArray.getValue(19, this.mMinWidthMajor);
            localTypedArray.getValue(20, this.mMinWidthMinor);
            if (localTypedArray.hasValue(23))
            {
                if (this.mFixedWidthMajor == null)
                    this.mFixedWidthMajor = new TypedValue();
                localTypedArray.getValue(23, this.mFixedWidthMajor);
            }
            if (localTypedArray.hasValue(25))
            {
                if (this.mFixedWidthMinor == null)
                    this.mFixedWidthMinor = new TypedValue();
                localTypedArray.getValue(25, this.mFixedWidthMinor);
            }
            if (localTypedArray.hasValue(26))
            {
                if (this.mFixedHeightMajor == null)
                    this.mFixedHeightMajor = new TypedValue();
                localTypedArray.getValue(26, this.mFixedHeightMajor);
            }
            if (localTypedArray.hasValue(24))
            {
                if (this.mFixedHeightMinor == null)
                    this.mFixedHeightMinor = new TypedValue();
                localTypedArray.getValue(24, this.mFixedHeightMinor);
            }
            Context localContext = getContext();
            int j = localContext.getApplicationInfo().targetSdkVersion;
            if (j >= 11)
                break label828;
            k = 1;
            if (j >= 14)
                break label834;
            m = 1;
            boolean bool2 = localContext.getResources().getBoolean(17891334);
            if ((hasFeature(8)) && (!hasFeature(1)))
                break label840;
            n = 1;
            if ((k == 0) && ((m == 0) || (!bool2) || (n == 0)))
                break label846;
            addFlags(134217728);
            if (((this.mAlwaysReadCloseOnTouchAttr) || (getContext().getApplicationInfo().targetSdkVersion >= 11)) && (localTypedArray.getBoolean(21, false)))
                setCloseOnTouchOutsideIfNotSet(true);
            WindowManager.LayoutParams localLayoutParams = getAttributes();
            if (!hasSoftInputMode())
                localLayoutParams.softInputMode = localTypedArray.getInt(13, localLayoutParams.softInputMode);
            if (localTypedArray.getBoolean(11, this.mIsFloating))
            {
                if ((0x2 & getForcedWindowFlags()) == 0)
                    localLayoutParams.flags = (0x2 | localLayoutParams.flags);
                if (!haveDimAmount())
                    localLayoutParams.dimAmount = localTypedArray.getFloat(0, 0.5F);
            }
            if (localLayoutParams.windowAnimations == 0)
                localLayoutParams.windowAnimations = localTypedArray.getResourceId(8, 0);
            if (getContainer() == null)
            {
                if (this.mBackgroundDrawable == null)
                {
                    if (this.mBackgroundResource == 0)
                        this.mBackgroundResource = localTypedArray.getResourceId(1, 0);
                    if (this.mFrameResource == 0)
                        this.mFrameResource = localTypedArray.getResourceId(2, 0);
                }
                this.mTextColor = localTypedArray.getColor(7, -16777216);
            }
            i1 = getLocalFeatures();
            if ((i1 & 0x18) == 0)
                break label864;
            if (!this.mIsFloating)
                break label856;
            TypedValue localTypedValue3 = new TypedValue();
            getContext().getTheme().resolveAttribute(16843723, localTypedValue3, true);
            i2 = localTypedValue3.resourceId;
            label725: removeFeature(8);
        }
        ViewGroup localViewGroup;
        while (true)
        {
            this.mDecor.startChanging();
            paramDecorView.addView(this.mLayoutInflater.inflate(i2, null), new ViewGroup.LayoutParams(-1, -1));
            localViewGroup = (ViewGroup)findViewById(16908290);
            if (localViewGroup != null)
                break label1072;
            throw new RuntimeException("Window couldn't find content container view");
            setFlags(65792, i);
            break;
            label802: if (!localTypedArray.getBoolean(15, false))
                break label63;
            requestFeature(8);
            break label63;
            label822: bool1 = false;
            break label171;
            label828: k = 0;
            break label400;
            label834: m = 0;
            break label410;
            label840: n = 0;
            break label443;
            label846: addFlags(134217728);
            break label470;
            label856: i2 = 17367198;
            break label725;
            label864: if (((i1 & 0x24) != 0) && ((i1 & 0x100) == 0))
            {
                i2 = 17367194;
            }
            else
            {
                if ((i1 & 0x80) != 0)
                {
                    TypedValue localTypedValue2;
                    if (this.mIsFloating)
                    {
                        localTypedValue2 = new TypedValue();
                        getContext().getTheme().resolveAttribute(16843724, localTypedValue2, true);
                    }
                    for (i2 = localTypedValue2.resourceId; ; i2 = 17367193)
                    {
                        removeFeature(8);
                        break;
                    }
                }
                if ((i1 & 0x2) == 0)
                {
                    if (this.mIsFloating)
                    {
                        TypedValue localTypedValue1 = new TypedValue();
                        getContext().getTheme().resolveAttribute(16843725, localTypedValue1, true);
                        i2 = localTypedValue1.resourceId;
                    }
                    else if ((i1 & 0x100) != 0)
                    {
                        if ((i1 & 0x200) != 0)
                            i2 = 17367192;
                        else
                            i2 = 17367191;
                    }
                    else
                    {
                        i2 = 17367197;
                    }
                }
                else if ((i1 & 0x400) != 0)
                    i2 = 17367196;
                else
                    i2 = 17367195;
            }
        }
        label1072: if ((i1 & 0x20) != 0)
        {
            ProgressBar localProgressBar = getCircularProgressBar(false);
            if (localProgressBar != null)
                localProgressBar.setIndeterminate(true);
        }
        if (getContainer() == null)
        {
            Drawable localDrawable1 = this.mBackgroundDrawable;
            if (this.mBackgroundResource != 0)
                localDrawable1 = getContext().getResources().getDrawable(this.mBackgroundResource);
            this.mDecor.setWindowBackground(localDrawable1);
            Drawable localDrawable2 = null;
            if (this.mFrameResource != 0)
                localDrawable2 = getContext().getResources().getDrawable(this.mFrameResource);
            this.mDecor.setWindowFrame(localDrawable2);
            if (this.mTitleColor == 0)
                this.mTitleColor = this.mTextColor;
            if (this.mTitle != null)
                setTitle(this.mTitle);
            setTitleColor(this.mTitleColor);
        }
        this.mDecor.finishChanging();
        return localViewGroup;
    }

    AudioManager getAudioManager()
    {
        if (this.mAudioManager == null)
            this.mAudioManager = ((AudioManager)getContext().getSystemService("audio"));
        return this.mAudioManager;
    }

    public View getCurrentFocus()
    {
        if (this.mDecor != null);
        for (View localView = this.mDecor.findFocus(); ; localView = null)
            return localView;
    }

    public final View getDecorView()
    {
        if (this.mDecor == null)
            installDecor();
        return this.mDecor;
    }

    public LayoutInflater getLayoutInflater()
    {
        return this.mLayoutInflater;
    }

    public int getVolumeControlStream()
    {
        return this.mVolumeControlStreamType;
    }

    protected boolean initializePanelContent(PanelFeatureState paramPanelFeatureState)
    {
        boolean bool;
        if (paramPanelFeatureState.createdPanelView != null)
        {
            paramPanelFeatureState.shownPanelView = paramPanelFeatureState.createdPanelView;
            bool = true;
        }
        while (true)
        {
            return bool;
            if (paramPanelFeatureState.menu == null)
            {
                bool = false;
            }
            else
            {
                if (this.mPanelMenuPresenterCallback == null)
                    this.mPanelMenuPresenterCallback = new PanelMenuPresenterCallback(null);
                if (paramPanelFeatureState.isInListMode());
                for (MenuView localMenuView = paramPanelFeatureState.getListMenuView(getContext(), this.mPanelMenuPresenterCallback); ; localMenuView = paramPanelFeatureState.getIconMenuView(getContext(), this.mPanelMenuPresenterCallback))
                {
                    paramPanelFeatureState.shownPanelView = ((View)localMenuView);
                    if (paramPanelFeatureState.shownPanelView == null)
                        break label126;
                    int i = localMenuView.getWindowAnimations();
                    if (i != 0)
                        paramPanelFeatureState.windowAnimations = i;
                    bool = true;
                    break;
                }
                label126: bool = false;
            }
        }
    }

    protected boolean initializePanelDecor(PanelFeatureState paramPanelFeatureState)
    {
        paramPanelFeatureState.decorView = new DecorView(getContext(), paramPanelFeatureState.featureId);
        paramPanelFeatureState.gravity = 81;
        paramPanelFeatureState.setStyle(getContext());
        return true;
    }

    protected boolean initializePanelMenu(PanelFeatureState paramPanelFeatureState)
    {
        Object localObject = getContext();
        if (((paramPanelFeatureState.featureId == 0) || (paramPanelFeatureState.featureId == 8)) && (this.mActionBar != null))
        {
            TypedValue localTypedValue = new TypedValue();
            ((Context)localObject).getTheme().resolveAttribute(16843671, localTypedValue, true);
            int i = localTypedValue.resourceId;
            if ((i != 0) && (((Context)localObject).getThemeResId() != i))
                localObject = new ContextThemeWrapper((Context)localObject, i);
        }
        MenuBuilder localMenuBuilder = new MenuBuilder((Context)localObject);
        localMenuBuilder.setCallback(this);
        paramPanelFeatureState.setMenu(localMenuBuilder);
        return true;
    }

    public void invalidatePanelMenu(int paramInt)
    {
        PanelFeatureState localPanelFeatureState1 = getPanelState(paramInt, true);
        if (localPanelFeatureState1.menu != null)
        {
            Bundle localBundle = new Bundle();
            localPanelFeatureState1.menu.saveActionViewStates(localBundle);
            if (localBundle.size() > 0)
                localPanelFeatureState1.frozenActionViewState = localBundle;
            localPanelFeatureState1.menu.stopDispatchingItemsChanged();
            localPanelFeatureState1.menu.clear();
        }
        localPanelFeatureState1.refreshMenuContent = true;
        localPanelFeatureState1.refreshDecorView = true;
        if (((paramInt == 8) || (paramInt == 0)) && (this.mActionBar != null))
        {
            PanelFeatureState localPanelFeatureState2 = getPanelState(0, false);
            if (localPanelFeatureState2 != null)
            {
                localPanelFeatureState2.isPrepared = false;
                preparePanel(localPanelFeatureState2, null);
            }
        }
    }

    public boolean isFloating()
    {
        return this.mIsFloating;
    }

    public boolean isShortcutKey(int paramInt, KeyEvent paramKeyEvent)
    {
        boolean bool = true;
        PanelFeatureState localPanelFeatureState = getPanelState(0, bool);
        if ((localPanelFeatureState.menu != null) && (localPanelFeatureState.menu.isShortcutKey(paramInt, paramKeyEvent)));
        while (true)
        {
            return bool;
            bool = false;
        }
    }

    protected void onActive()
    {
    }

    public void onConfigurationChanged(Configuration paramConfiguration)
    {
        PanelFeatureState localPanelFeatureState;
        if (this.mActionBar == null)
        {
            localPanelFeatureState = getPanelState(0, false);
            if ((localPanelFeatureState != null) && (localPanelFeatureState.menu != null))
            {
                if (!localPanelFeatureState.isOpen)
                    break label110;
                Bundle localBundle = new Bundle();
                if (localPanelFeatureState.iconMenuPresenter != null)
                    localPanelFeatureState.iconMenuPresenter.saveHierarchyState(localBundle);
                if (localPanelFeatureState.listMenuPresenter != null)
                    localPanelFeatureState.listMenuPresenter.saveHierarchyState(localBundle);
                clearMenuViews(localPanelFeatureState);
                reopenMenu(false);
                if (localPanelFeatureState.iconMenuPresenter != null)
                    localPanelFeatureState.iconMenuPresenter.restoreHierarchyState(localBundle);
                if (localPanelFeatureState.listMenuPresenter != null)
                    localPanelFeatureState.listMenuPresenter.restoreHierarchyState(localBundle);
            }
        }
        while (true)
        {
            return;
            label110: clearMenuViews(localPanelFeatureState);
        }
    }

    protected void onDrawableChanged(int paramInt1, Drawable paramDrawable, int paramInt2)
    {
        ImageView localImageView;
        if (paramInt1 == 3)
        {
            localImageView = getLeftIconView();
            if (paramDrawable == null)
                break label47;
            paramDrawable.setAlpha(paramInt2);
            localImageView.setImageDrawable(paramDrawable);
            localImageView.setVisibility(0);
        }
        while (true)
        {
            return;
            if (paramInt1 == 4)
            {
                localImageView = getRightIconView();
                break;
                label47: localImageView.setVisibility(8);
            }
        }
    }

    protected void onIntChanged(int paramInt1, int paramInt2)
    {
        if ((paramInt1 == 2) || (paramInt1 == 5))
            updateProgressBars(paramInt2);
        while (true)
        {
            return;
            if (paramInt1 == 7)
            {
                FrameLayout localFrameLayout = (FrameLayout)findViewById(16908853);
                if (localFrameLayout != null)
                    this.mLayoutInflater.inflate(paramInt2, localFrameLayout);
            }
        }
    }

    protected boolean onKeyDown(int paramInt1, int paramInt2, KeyEvent paramKeyEvent)
    {
        boolean bool = false;
        KeyEvent.DispatcherState localDispatcherState;
        if (this.mDecor != null)
        {
            localDispatcherState = this.mDecor.getKeyDispatcherState();
            switch (paramInt2)
            {
            default:
            case 24:
            case 25:
            case 164:
            case 82:
            case 4:
            }
        }
        while (true)
        {
            return bool;
            localDispatcherState = null;
            break;
            getAudioManager().handleKeyDown(paramKeyEvent, this.mVolumeControlStreamType);
            bool = true;
            continue;
            if (paramInt1 < 0)
                paramInt1 = 0;
            onKeyDownPanel(paramInt1, paramKeyEvent);
            bool = true;
            continue;
            if ((paramKeyEvent.getRepeatCount() <= 0) && (paramInt1 >= 0))
            {
                if (localDispatcherState != null)
                    localDispatcherState.startTracking(paramKeyEvent, this);
                bool = true;
            }
        }
    }

    public final boolean onKeyDownPanel(int paramInt, KeyEvent paramKeyEvent)
    {
        int i = paramKeyEvent.getKeyCode();
        PanelFeatureState localPanelFeatureState;
        if (paramKeyEvent.getRepeatCount() == 0)
        {
            this.mPanelChordingKey = i;
            localPanelFeatureState = getPanelState(paramInt, true);
            if (localPanelFeatureState.isOpen);
        }
        for (boolean bool = preparePanel(localPanelFeatureState, paramKeyEvent); ; bool = false)
            return bool;
    }

    protected boolean onKeyUp(int paramInt1, int paramInt2, KeyEvent paramKeyEvent)
    {
        boolean bool = false;
        KeyEvent.DispatcherState localDispatcherState;
        if (this.mDecor != null)
        {
            localDispatcherState = this.mDecor.getKeyDispatcherState();
            if (localDispatcherState != null)
                localDispatcherState.handleUpEvent(paramKeyEvent);
            switch (paramInt2)
            {
            default:
            case 24:
            case 25:
            case 164:
            case 82:
            case 4:
            case 84:
            }
        }
        while (true)
        {
            return bool;
            localDispatcherState = null;
            break;
            getAudioManager().handleKeyUp(paramKeyEvent, this.mVolumeControlStreamType);
            bool = true;
            continue;
            if (paramInt1 < 0)
                paramInt1 = 0;
            onKeyUpPanel(paramInt1, paramKeyEvent);
            bool = true;
            continue;
            if ((paramInt1 >= 0) && (paramKeyEvent.isTracking()) && (!paramKeyEvent.isCanceled()))
                if (paramInt1 == 0)
                {
                    PanelFeatureState localPanelFeatureState = getPanelState(paramInt1, false);
                    if ((localPanelFeatureState != null) && (localPanelFeatureState.isInExpandedMode))
                    {
                        reopenMenu(true);
                        bool = true;
                    }
                }
                else
                {
                    closePanel(paramInt1);
                    bool = true;
                    continue;
                    if (!getKeyguardManager().inKeyguardRestrictedInputMode())
                    {
                        if ((paramKeyEvent.isTracking()) && (!paramKeyEvent.isCanceled()))
                            launchDefaultSearch();
                        bool = true;
                    }
                }
        }
    }

    public final void onKeyUpPanel(int paramInt, KeyEvent paramKeyEvent)
    {
        if (this.mPanelChordingKey != 0)
        {
            this.mPanelChordingKey = 0;
            if ((!paramKeyEvent.isCanceled()) && ((this.mDecor == null) || (this.mDecor.mActionMode == null)))
                break label37;
        }
        while (true)
        {
            return;
            label37: boolean bool1 = false;
            PanelFeatureState localPanelFeatureState = getPanelState(paramInt, true);
            if ((paramInt == 0) && (this.mActionBar != null) && (this.mActionBar.isOverflowReserved()))
                if (this.mActionBar.getVisibility() == 0)
                {
                    if (this.mActionBar.isOverflowMenuShowing())
                        break label146;
                    if ((!isDestroyed()) && (preparePanel(localPanelFeatureState, paramKeyEvent)))
                        bool1 = this.mActionBar.showOverflowMenu();
                }
            while (true)
            {
                if (!bool1)
                    break label246;
                AudioManager localAudioManager = (AudioManager)getContext().getSystemService("audio");
                if (localAudioManager == null)
                    break label248;
                localAudioManager.playSoundEffect(0);
                break;
                label146: bool1 = this.mActionBar.hideOverflowMenu();
                continue;
                if ((localPanelFeatureState.isOpen) || (localPanelFeatureState.isHandled))
                {
                    bool1 = localPanelFeatureState.isOpen;
                    closePanel(localPanelFeatureState, true);
                }
                else if (localPanelFeatureState.isPrepared)
                {
                    boolean bool2 = true;
                    if (localPanelFeatureState.refreshMenuContent)
                    {
                        localPanelFeatureState.isPrepared = false;
                        bool2 = preparePanel(localPanelFeatureState, paramKeyEvent);
                    }
                    if (bool2)
                    {
                        EventLog.writeEvent(50001, 0);
                        openPanel(localPanelFeatureState, paramKeyEvent);
                        bool1 = true;
                    }
                }
            }
            label246: continue;
            label248: Log.w("PhoneWindow", "Couldn't get audio manager");
        }
    }

    public boolean onMenuItemSelected(MenuBuilder paramMenuBuilder, MenuItem paramMenuItem)
    {
        Window.Callback localCallback = getCallback();
        PanelFeatureState localPanelFeatureState;
        if ((localCallback != null) && (!isDestroyed()))
        {
            localPanelFeatureState = findMenuPanel(paramMenuBuilder.getRootMenu());
            if (localPanelFeatureState == null);
        }
        for (boolean bool = localCallback.onMenuItemSelected(localPanelFeatureState.featureId, paramMenuItem); ; bool = false)
            return bool;
    }

    public void onMenuModeChange(MenuBuilder paramMenuBuilder)
    {
        reopenMenu(true);
    }

    void onOptionsPanelRotationChanged()
    {
        PanelFeatureState localPanelFeatureState = getPanelState(0, false);
        if (localPanelFeatureState == null);
        label68: 
        while (true)
        {
            return;
            if (localPanelFeatureState.decorView != null);
            for (WindowManager.LayoutParams localLayoutParams = (WindowManager.LayoutParams)localPanelFeatureState.decorView.getLayoutParams(); ; localLayoutParams = null)
            {
                if (localLayoutParams == null)
                    break label68;
                localLayoutParams.gravity = getOptionsPanelGravity();
                WindowManager localWindowManager = getWindowManager();
                if (localWindowManager == null)
                    break;
                localWindowManager.updateViewLayout(localPanelFeatureState.decorView, localLayoutParams);
                break;
            }
        }
    }

    public final void openPanel(int paramInt, KeyEvent paramKeyEvent)
    {
        if ((paramInt == 0) && (this.mActionBar != null) && (this.mActionBar.isOverflowReserved()))
            if (this.mActionBar.getVisibility() == 0)
                this.mActionBar.showOverflowMenu();
        while (true)
        {
            return;
            openPanel(getPanelState(paramInt, true), paramKeyEvent);
        }
    }

    public final View peekDecorView()
    {
        return this.mDecor;
    }

    public boolean performContextMenuIdentifierAction(int paramInt1, int paramInt2)
    {
        if (this.mContextMenu != null);
        for (boolean bool = this.mContextMenu.performIdentifierAction(paramInt1, paramInt2); ; bool = false)
            return bool;
    }

    public boolean performPanelIdentifierAction(int paramInt1, int paramInt2, int paramInt3)
    {
        boolean bool = false;
        PanelFeatureState localPanelFeatureState = getPanelState(paramInt1, true);
        if (!preparePanel(localPanelFeatureState, new KeyEvent(0, 82)));
        while (true)
        {
            return bool;
            if (localPanelFeatureState.menu != null)
            {
                bool = localPanelFeatureState.menu.performIdentifierAction(paramInt2, paramInt3);
                if (this.mActionBar == null)
                    closePanel(localPanelFeatureState, true);
            }
        }
    }

    public boolean performPanelShortcut(int paramInt1, int paramInt2, KeyEvent paramKeyEvent, int paramInt3)
    {
        return performPanelShortcut(getPanelState(paramInt1, true), paramInt2, paramKeyEvent, paramInt3);
    }

    public final boolean preparePanel(PanelFeatureState paramPanelFeatureState, KeyEvent paramKeyEvent)
    {
        boolean bool1 = false;
        if (isDestroyed());
        while (true)
        {
            return bool1;
            if (paramPanelFeatureState.isPrepared)
            {
                bool1 = true;
            }
            else
            {
                if ((this.mPreparedPanel != null) && (this.mPreparedPanel != paramPanelFeatureState))
                    closePanel(this.mPreparedPanel, false);
                Window.Callback localCallback = getCallback();
                if (localCallback != null)
                    paramPanelFeatureState.createdPanelView = localCallback.onCreatePanelView(paramPanelFeatureState.featureId);
                if (paramPanelFeatureState.createdPanelView != null)
                    break label350;
                if ((paramPanelFeatureState.menu == null) || (paramPanelFeatureState.refreshMenuContent))
                {
                    if ((paramPanelFeatureState.menu != null) || ((initializePanelMenu(paramPanelFeatureState)) && (paramPanelFeatureState.menu != null)))
                    {
                        if (this.mActionBar != null)
                        {
                            if (this.mActionMenuPresenterCallback == null)
                                this.mActionMenuPresenterCallback = new ActionMenuPresenterCallback(null);
                            this.mActionBar.setMenu(paramPanelFeatureState.menu, this.mActionMenuPresenterCallback);
                        }
                        paramPanelFeatureState.menu.stopDispatchingItemsChanged();
                        if ((localCallback == null) || (!localCallback.onCreatePanelMenu(paramPanelFeatureState.featureId, paramPanelFeatureState.menu)))
                        {
                            paramPanelFeatureState.setMenu(null);
                            if (this.mActionBar != null)
                                this.mActionBar.setMenu(null, this.mActionMenuPresenterCallback);
                        }
                        else
                        {
                            paramPanelFeatureState.refreshMenuContent = false;
                        }
                    }
                }
                else
                {
                    paramPanelFeatureState.menu.stopDispatchingItemsChanged();
                    if (paramPanelFeatureState.frozenActionViewState != null)
                    {
                        paramPanelFeatureState.menu.restoreActionViewStates(paramPanelFeatureState.frozenActionViewState);
                        paramPanelFeatureState.frozenActionViewState = null;
                    }
                    if (localCallback.onPreparePanel(paramPanelFeatureState.featureId, paramPanelFeatureState.createdPanelView, paramPanelFeatureState.menu))
                        break;
                    if (this.mActionBar != null)
                        this.mActionBar.setMenu(null, this.mActionMenuPresenterCallback);
                    paramPanelFeatureState.menu.startDispatchingItemsChanged();
                }
            }
        }
        int i;
        if (paramKeyEvent != null)
        {
            i = paramKeyEvent.getDeviceId();
            label311: if (KeyCharacterMap.load(i).getKeyboardType() == 1)
                break label377;
        }
        label350: label377: for (boolean bool2 = true; ; bool2 = false)
        {
            paramPanelFeatureState.qwertyMode = bool2;
            paramPanelFeatureState.menu.setQwertyMode(paramPanelFeatureState.qwertyMode);
            paramPanelFeatureState.menu.startDispatchingItemsChanged();
            paramPanelFeatureState.isPrepared = true;
            paramPanelFeatureState.isHandled = false;
            this.mPreparedPanel = paramPanelFeatureState;
            bool1 = true;
            break;
            i = -1;
            break label311;
        }
    }

    public boolean requestFeature(int paramInt)
    {
        if (this.mContentParent != null)
            throw new AndroidRuntimeException("requestFeature() must be called before adding content");
        int i = getFeatures();
        if ((i != 65) && (paramInt == 7))
            throw new AndroidRuntimeException("You cannot combine custom titles with other title features");
        if (((i & 0x80) != 0) && (paramInt != 7) && (paramInt != 10))
            throw new AndroidRuntimeException("You cannot combine custom titles with other title features");
        if (((i & 0x2) != 0) && (paramInt == 8));
        for (boolean bool = false; ; bool = super.requestFeature(paramInt))
        {
            return bool;
            if (((i & 0x100) != 0) && (paramInt == 1))
                removeFeature(8);
        }
    }

    public void restoreHierarchyState(Bundle paramBundle)
    {
        if (this.mContentParent == null);
        while (true)
        {
            return;
            SparseArray localSparseArray1 = paramBundle.getSparseParcelableArray("android:views");
            if (localSparseArray1 != null)
                this.mContentParent.restoreHierarchyState(localSparseArray1);
            int i = paramBundle.getInt("android:focusedViewId", -1);
            if (i != -1)
            {
                View localView = this.mContentParent.findViewById(i);
                if (localView == null)
                    break label114;
                localView.requestFocus();
            }
            while (true)
            {
                SparseArray localSparseArray2 = paramBundle.getSparseParcelableArray("android:Panels");
                if (localSparseArray2 != null)
                    restorePanelState(localSparseArray2);
                if (this.mActionBar == null)
                    break;
                SparseArray localSparseArray3 = paramBundle.getSparseParcelableArray("android:ActionBar");
                if (localSparseArray3 == null)
                    break label149;
                this.mActionBar.restoreHierarchyState(localSparseArray3);
                break;
                label114: Log.w("PhoneWindow", "Previously focused view reported id " + i + " during save, but can't be found during restore.");
            }
            label149: Log.w("PhoneWindow", "Missing saved instance states for action bar views! State will not be restored.");
        }
    }

    public Bundle saveHierarchyState()
    {
        Bundle localBundle = new Bundle();
        if (this.mContentParent == null);
        while (true)
        {
            return localBundle;
            SparseArray localSparseArray1 = new SparseArray();
            this.mContentParent.saveHierarchyState(localSparseArray1);
            localBundle.putSparseParcelableArray("android:views", localSparseArray1);
            View localView = this.mContentParent.findFocus();
            if ((localView != null) && (localView.getId() != -1))
                localBundle.putInt("android:focusedViewId", localView.getId());
            SparseArray localSparseArray2 = new SparseArray();
            savePanelState(localSparseArray2);
            if (localSparseArray2.size() > 0)
                localBundle.putSparseParcelableArray("android:Panels", localSparseArray2);
            if (this.mActionBar != null)
            {
                SparseArray localSparseArray3 = new SparseArray();
                this.mActionBar.saveHierarchyState(localSparseArray3);
                localBundle.putSparseParcelableArray("android:ActionBar", localSparseArray3);
            }
        }
    }

    void sendCloseSystemWindows()
    {
        PhoneWindowManager.sendCloseSystemWindows(getContext(), null);
    }

    void sendCloseSystemWindows(String paramString)
    {
        PhoneWindowManager.sendCloseSystemWindows(getContext(), paramString);
    }

    public final void setBackgroundDrawable(Drawable paramDrawable)
    {
        if ((paramDrawable != this.mBackgroundDrawable) || (this.mBackgroundResource != 0))
        {
            this.mBackgroundResource = 0;
            this.mBackgroundDrawable = paramDrawable;
            if (this.mDecor != null)
                this.mDecor.setWindowBackground(paramDrawable);
        }
    }

    public final void setChildDrawable(int paramInt, Drawable paramDrawable)
    {
        DrawableFeatureState localDrawableFeatureState = getDrawableState(paramInt, true);
        localDrawableFeatureState.child = paramDrawable;
        updateDrawable(paramInt, localDrawableFeatureState, false);
    }

    public final void setChildInt(int paramInt1, int paramInt2)
    {
        updateInt(paramInt1, paramInt2, false);
    }

    public final void setContainer(Window paramWindow)
    {
        super.setContainer(paramWindow);
    }

    public void setContentView(int paramInt)
    {
        if (this.mContentParent == null)
            installDecor();
        while (true)
        {
            this.mLayoutInflater.inflate(paramInt, this.mContentParent);
            Window.Callback localCallback = getCallback();
            if ((localCallback != null) && (!isDestroyed()))
                localCallback.onContentChanged();
            return;
            this.mContentParent.removeAllViews();
        }
    }

    public void setContentView(View paramView)
    {
        setContentView(paramView, new ViewGroup.LayoutParams(-1, -1));
    }

    public void setContentView(View paramView, ViewGroup.LayoutParams paramLayoutParams)
    {
        if (this.mContentParent == null)
            installDecor();
        while (true)
        {
            this.mContentParent.addView(paramView, paramLayoutParams);
            Window.Callback localCallback = getCallback();
            if ((localCallback != null) && (!isDestroyed()))
                localCallback.onContentChanged();
            return;
            this.mContentParent.removeAllViews();
        }
    }

    protected final void setFeatureDefaultDrawable(int paramInt, Drawable paramDrawable)
    {
        DrawableFeatureState localDrawableFeatureState = getDrawableState(paramInt, true);
        if (localDrawableFeatureState.def != paramDrawable)
        {
            localDrawableFeatureState.def = paramDrawable;
            updateDrawable(paramInt, localDrawableFeatureState, false);
        }
    }

    public final void setFeatureDrawable(int paramInt, Drawable paramDrawable)
    {
        DrawableFeatureState localDrawableFeatureState = getDrawableState(paramInt, true);
        localDrawableFeatureState.resid = 0;
        localDrawableFeatureState.uri = null;
        if (localDrawableFeatureState.local != paramDrawable)
        {
            localDrawableFeatureState.local = paramDrawable;
            updateDrawable(paramInt, localDrawableFeatureState, false);
        }
    }

    public void setFeatureDrawableAlpha(int paramInt1, int paramInt2)
    {
        DrawableFeatureState localDrawableFeatureState = getDrawableState(paramInt1, true);
        if (localDrawableFeatureState.alpha != paramInt2)
        {
            localDrawableFeatureState.alpha = paramInt2;
            updateDrawable(paramInt1, localDrawableFeatureState, false);
        }
    }

    public final void setFeatureDrawableResource(int paramInt1, int paramInt2)
    {
        if (paramInt2 != 0)
        {
            DrawableFeatureState localDrawableFeatureState = getDrawableState(paramInt1, true);
            if (localDrawableFeatureState.resid != paramInt2)
            {
                localDrawableFeatureState.resid = paramInt2;
                localDrawableFeatureState.uri = null;
                localDrawableFeatureState.local = getContext().getResources().getDrawable(paramInt2);
                updateDrawable(paramInt1, localDrawableFeatureState, false);
            }
        }
        while (true)
        {
            return;
            setFeatureDrawable(paramInt1, null);
        }
    }

    public final void setFeatureDrawableUri(int paramInt, Uri paramUri)
    {
        if (paramUri != null)
        {
            DrawableFeatureState localDrawableFeatureState = getDrawableState(paramInt, true);
            if ((localDrawableFeatureState.uri == null) || (!localDrawableFeatureState.uri.equals(paramUri)))
            {
                localDrawableFeatureState.resid = 0;
                localDrawableFeatureState.uri = paramUri;
                localDrawableFeatureState.local = loadImageURI(paramUri);
                updateDrawable(paramInt, localDrawableFeatureState, false);
            }
        }
        while (true)
        {
            return;
            setFeatureDrawable(paramInt, null);
        }
    }

    protected void setFeatureFromAttrs(int paramInt1, TypedArray paramTypedArray, int paramInt2, int paramInt3)
    {
        Drawable localDrawable = paramTypedArray.getDrawable(paramInt2);
        if (localDrawable != null)
        {
            requestFeature(paramInt1);
            setFeatureDefaultDrawable(paramInt1, localDrawable);
        }
        if ((getFeatures() & 1 << paramInt1) != 0)
        {
            int i = paramTypedArray.getInt(paramInt3, -1);
            if (i >= 0)
                setFeatureDrawableAlpha(paramInt1, i);
        }
    }

    public final void setFeatureInt(int paramInt1, int paramInt2)
    {
        updateInt(paramInt1, paramInt2, false);
    }

    public void setTitle(CharSequence paramCharSequence)
    {
        if (this.mTitleView != null)
            this.mTitleView.setText(paramCharSequence);
        while (true)
        {
            this.mTitle = paramCharSequence;
            return;
            if (this.mActionBar != null)
                this.mActionBar.setWindowTitle(paramCharSequence);
        }
    }

    public void setTitleColor(int paramInt)
    {
        if (this.mTitleView != null)
            this.mTitleView.setTextColor(paramInt);
        this.mTitleColor = paramInt;
    }

    public void setUiOptions(int paramInt)
    {
        this.mUiOptions = paramInt;
    }

    public void setUiOptions(int paramInt1, int paramInt2)
    {
        this.mUiOptions = (this.mUiOptions & (paramInt2 ^ 0xFFFFFFFF) | paramInt1 & paramInt2);
    }

    public void setVolumeControlStream(int paramInt)
    {
        this.mVolumeControlStreamType = paramInt;
    }

    public boolean superDispatchGenericMotionEvent(MotionEvent paramMotionEvent)
    {
        return this.mDecor.superDispatchGenericMotionEvent(paramMotionEvent);
    }

    public boolean superDispatchKeyEvent(KeyEvent paramKeyEvent)
    {
        return this.mDecor.superDispatchKeyEvent(paramKeyEvent);
    }

    public boolean superDispatchKeyShortcutEvent(KeyEvent paramKeyEvent)
    {
        return this.mDecor.superDispatchKeyShortcutEvent(paramKeyEvent);
    }

    public boolean superDispatchTouchEvent(MotionEvent paramMotionEvent)
    {
        return this.mDecor.superDispatchTouchEvent(paramMotionEvent);
    }

    public boolean superDispatchTrackballEvent(MotionEvent paramMotionEvent)
    {
        return this.mDecor.superDispatchTrackballEvent(paramMotionEvent);
    }

    public void takeInputQueue(InputQueue.Callback paramCallback)
    {
        this.mTakeInputQueueCallback = paramCallback;
    }

    public void takeKeyEvents(boolean paramBoolean)
    {
        this.mDecor.setFocusable(paramBoolean);
    }

    public void takeSurface(SurfaceHolder.Callback2 paramCallback2)
    {
        this.mTakeSurfaceCallback = paramCallback2;
    }

    public final void togglePanel(int paramInt, KeyEvent paramKeyEvent)
    {
        PanelFeatureState localPanelFeatureState = getPanelState(paramInt, true);
        if (localPanelFeatureState.isOpen)
            closePanel(localPanelFeatureState, true);
        while (true)
        {
            return;
            openPanel(localPanelFeatureState, paramKeyEvent);
        }
    }

    protected final void updateDrawable(int paramInt, boolean paramBoolean)
    {
        DrawableFeatureState localDrawableFeatureState = getDrawableState(paramInt, false);
        if (localDrawableFeatureState != null)
            updateDrawable(paramInt, localDrawableFeatureState, paramBoolean);
    }

    private final class DialogMenuCallback
        implements MenuBuilder.Callback, MenuPresenter.Callback
    {
        private int mFeatureId;
        private MenuDialogHelper mSubMenuHelper;

        public DialogMenuCallback(int arg2)
        {
            int i;
            this.mFeatureId = i;
        }

        public void onCloseMenu(MenuBuilder paramMenuBuilder, boolean paramBoolean)
        {
            if (paramMenuBuilder.getRootMenu() != paramMenuBuilder)
                onCloseSubMenu(paramMenuBuilder);
            if (paramBoolean)
            {
                Window.Callback localCallback = PhoneWindow.this.getCallback();
                if ((localCallback != null) && (!PhoneWindow.this.isDestroyed()))
                    localCallback.onPanelClosed(this.mFeatureId, paramMenuBuilder);
                if (paramMenuBuilder == PhoneWindow.this.mContextMenu)
                    PhoneWindow.this.dismissContextMenu();
                if (this.mSubMenuHelper != null)
                {
                    this.mSubMenuHelper.dismiss();
                    this.mSubMenuHelper = null;
                }
            }
        }

        public void onCloseSubMenu(MenuBuilder paramMenuBuilder)
        {
            Window.Callback localCallback = PhoneWindow.this.getCallback();
            if ((localCallback != null) && (!PhoneWindow.this.isDestroyed()))
                localCallback.onPanelClosed(this.mFeatureId, paramMenuBuilder.getRootMenu());
        }

        public boolean onMenuItemSelected(MenuBuilder paramMenuBuilder, MenuItem paramMenuItem)
        {
            Window.Callback localCallback = PhoneWindow.this.getCallback();
            if ((localCallback != null) && (!PhoneWindow.this.isDestroyed()) && (localCallback.onMenuItemSelected(this.mFeatureId, paramMenuItem)));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void onMenuModeChange(MenuBuilder paramMenuBuilder)
        {
        }

        public boolean onOpenSubMenu(MenuBuilder paramMenuBuilder)
        {
            if (paramMenuBuilder == null);
            for (boolean bool = false; ; bool = true)
            {
                return bool;
                paramMenuBuilder.setCallback(this);
                this.mSubMenuHelper = new MenuDialogHelper(paramMenuBuilder);
                this.mSubMenuHelper.show(null);
            }
        }
    }

    static class RotationWatcher extends IRotationWatcher.Stub
    {
        private Handler mHandler;
        private boolean mIsWatching;
        private final Runnable mRotationChanged = new Runnable()
        {
            public void run()
            {
                PhoneWindow.RotationWatcher.this.dispatchRotationChanged();
            }
        };
        private final ArrayList<WeakReference<PhoneWindow>> mWindows = new ArrayList();

        public void addWindow(PhoneWindow paramPhoneWindow)
        {
            synchronized (this.mWindows)
            {
                boolean bool = this.mIsWatching;
                if (!bool);
                try
                {
                    PhoneWindow.WindowManagerHolder.sWindowManager.watchRotation(this);
                    this.mHandler = new Handler();
                    this.mIsWatching = true;
                    this.mWindows.add(new WeakReference(paramPhoneWindow));
                    return;
                }
                catch (RemoteException localRemoteException)
                {
                    while (true)
                        Log.e("PhoneWindow", "Couldn't start watching for device rotation", localRemoteException);
                }
            }
        }

        // ERROR //
        void dispatchRotationChanged()
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 32	com/android/internal/policy/impl/PhoneWindow$RotationWatcher:mWindows	Ljava/util/ArrayList;
            //     4: astore_1
            //     5: aload_1
            //     6: monitorenter
            //     7: iconst_0
            //     8: istore_2
            //     9: iload_2
            //     10: aload_0
            //     11: getfield 32	com/android/internal/policy/impl/PhoneWindow$RotationWatcher:mWindows	Ljava/util/ArrayList;
            //     14: invokevirtual 79	java/util/ArrayList:size	()I
            //     17: if_icmpge +55 -> 72
            //     20: aload_0
            //     21: getfield 32	com/android/internal/policy/impl/PhoneWindow$RotationWatcher:mWindows	Ljava/util/ArrayList;
            //     24: iload_2
            //     25: invokevirtual 83	java/util/ArrayList:get	(I)Ljava/lang/Object;
            //     28: checkcast 57	java/lang/ref/WeakReference
            //     31: invokevirtual 86	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
            //     34: checkcast 8	com/android/internal/policy/impl/PhoneWindow
            //     37: astore 4
            //     39: aload 4
            //     41: ifnull +14 -> 55
            //     44: aload 4
            //     46: invokevirtual 89	com/android/internal/policy/impl/PhoneWindow:onOptionsPanelRotationChanged	()V
            //     49: iinc 2 1
            //     52: goto -43 -> 9
            //     55: aload_0
            //     56: getfield 32	com/android/internal/policy/impl/PhoneWindow$RotationWatcher:mWindows	Ljava/util/ArrayList;
            //     59: iload_2
            //     60: invokevirtual 92	java/util/ArrayList:remove	(I)Ljava/lang/Object;
            //     63: pop
            //     64: goto -55 -> 9
            //     67: astore_3
            //     68: aload_1
            //     69: monitorexit
            //     70: aload_3
            //     71: athrow
            //     72: aload_1
            //     73: monitorexit
            //     74: return
            //
            // Exception table:
            //     from	to	target	type
            //     9	70	67	finally
            //     72	74	67	finally
        }

        public void onRotationChanged(int paramInt)
            throws RemoteException
        {
            this.mHandler.post(this.mRotationChanged);
        }

        // ERROR //
        public void removeWindow(PhoneWindow paramPhoneWindow)
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 32	com/android/internal/policy/impl/PhoneWindow$RotationWatcher:mWindows	Ljava/util/ArrayList;
            //     4: astore_2
            //     5: aload_2
            //     6: monitorenter
            //     7: iconst_0
            //     8: istore_3
            //     9: iload_3
            //     10: aload_0
            //     11: getfield 32	com/android/internal/policy/impl/PhoneWindow$RotationWatcher:mWindows	Ljava/util/ArrayList;
            //     14: invokevirtual 79	java/util/ArrayList:size	()I
            //     17: if_icmpge +58 -> 75
            //     20: aload_0
            //     21: getfield 32	com/android/internal/policy/impl/PhoneWindow$RotationWatcher:mWindows	Ljava/util/ArrayList;
            //     24: iload_3
            //     25: invokevirtual 83	java/util/ArrayList:get	(I)Ljava/lang/Object;
            //     28: checkcast 57	java/lang/ref/WeakReference
            //     31: invokevirtual 86	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
            //     34: checkcast 8	com/android/internal/policy/impl/PhoneWindow
            //     37: astore 5
            //     39: aload 5
            //     41: ifnull +9 -> 50
            //     44: aload 5
            //     46: aload_1
            //     47: if_acmpne +22 -> 69
            //     50: aload_0
            //     51: getfield 32	com/android/internal/policy/impl/PhoneWindow$RotationWatcher:mWindows	Ljava/util/ArrayList;
            //     54: iload_3
            //     55: invokevirtual 92	java/util/ArrayList:remove	(I)Ljava/lang/Object;
            //     58: pop
            //     59: goto -50 -> 9
            //     62: astore 4
            //     64: aload_2
            //     65: monitorexit
            //     66: aload 4
            //     68: athrow
            //     69: iinc 3 1
            //     72: goto -63 -> 9
            //     75: aload_2
            //     76: monitorexit
            //     77: return
            //
            // Exception table:
            //     from	to	target	type
            //     9	66	62	finally
            //     75	77	62	finally
        }
    }

    private static final class PanelFeatureState
    {
        int background;
        View createdPanelView;
        PhoneWindow.DecorView decorView;
        int featureId;
        Bundle frozenActionViewState;
        Bundle frozenMenuState;
        int fullBackground;
        int gravity;
        IconMenuPresenter iconMenuPresenter;
        boolean isCompact;
        boolean isHandled;
        boolean isInExpandedMode;
        boolean isOpen;
        boolean isPrepared;
        ListMenuPresenter listMenuPresenter;
        int listPresenterTheme;
        MenuBuilder menu;
        public boolean qwertyMode;
        boolean refreshDecorView;
        boolean refreshMenuContent;
        View shownPanelView;
        boolean wasLastExpanded;
        boolean wasLastOpen;
        int windowAnimations;
        int x;
        int y;

        PanelFeatureState(int paramInt)
        {
            this.featureId = paramInt;
            this.refreshDecorView = false;
        }

        void applyFrozenState()
        {
            if ((this.menu != null) && (this.frozenMenuState != null))
            {
                this.menu.restorePresenterStates(this.frozenMenuState);
                this.frozenMenuState = null;
            }
        }

        public void clearMenuPresenters()
        {
            if (this.menu != null)
            {
                this.menu.removeMenuPresenter(this.iconMenuPresenter);
                this.menu.removeMenuPresenter(this.listMenuPresenter);
            }
            this.iconMenuPresenter = null;
            this.listMenuPresenter = null;
        }

        MenuView getIconMenuView(Context paramContext, MenuPresenter.Callback paramCallback)
        {
            if (this.menu == null);
            for (MenuView localMenuView = null; ; localMenuView = this.iconMenuPresenter.getMenuView(this.decorView))
            {
                return localMenuView;
                if (this.iconMenuPresenter == null)
                {
                    this.iconMenuPresenter = new IconMenuPresenter(paramContext);
                    this.iconMenuPresenter.setCallback(paramCallback);
                    this.iconMenuPresenter.setId(16908861);
                    this.menu.addMenuPresenter(this.iconMenuPresenter);
                }
            }
        }

        MenuView getListMenuView(Context paramContext, MenuPresenter.Callback paramCallback)
        {
            if (this.menu == null);
            for (MenuView localMenuView = null; ; localMenuView = this.listMenuPresenter.getMenuView(this.decorView))
            {
                return localMenuView;
                if (!this.isCompact)
                    getIconMenuView(paramContext, paramCallback);
                if (this.listMenuPresenter == null)
                {
                    this.listMenuPresenter = new ListMenuPresenter(17367138, this.listPresenterTheme);
                    this.listMenuPresenter.setCallback(paramCallback);
                    this.listMenuPresenter.setId(16908862);
                    this.menu.addMenuPresenter(this.listMenuPresenter);
                }
                if (this.iconMenuPresenter != null)
                    this.listMenuPresenter.setItemIndexOffset(this.iconMenuPresenter.getNumActualItemsShown());
            }
        }

        public boolean hasPanelItems()
        {
            boolean bool1 = true;
            if (this.shownPanelView == null)
                bool1 = false;
            while (true)
            {
                return bool1;
                if (this.createdPanelView == null)
                {
                    if ((this.isCompact) || (this.isInExpandedMode))
                    {
                        if (this.listMenuPresenter.getAdapter().getCount() > 0);
                        for (boolean bool2 = bool1; ; bool2 = false)
                        {
                            bool1 = bool2;
                            break;
                        }
                    }
                    if (((ViewGroup)this.shownPanelView).getChildCount() <= 0)
                        bool1 = false;
                }
            }
        }

        public boolean isInListMode()
        {
            if ((this.isInExpandedMode) || (this.isCompact));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        void onRestoreInstanceState(Parcelable paramParcelable)
        {
            SavedState localSavedState = (SavedState)paramParcelable;
            this.featureId = localSavedState.featureId;
            this.wasLastOpen = localSavedState.isOpen;
            this.wasLastExpanded = localSavedState.isInExpandedMode;
            this.frozenMenuState = localSavedState.menuState;
            this.createdPanelView = null;
            this.shownPanelView = null;
            this.decorView = null;
        }

        Parcelable onSaveInstanceState()
        {
            SavedState localSavedState = new SavedState(null);
            localSavedState.featureId = this.featureId;
            localSavedState.isOpen = this.isOpen;
            localSavedState.isInExpandedMode = this.isInExpandedMode;
            if (this.menu != null)
            {
                localSavedState.menuState = new Bundle();
                this.menu.savePresenterStates(localSavedState.menuState);
            }
            return localSavedState;
        }

        void setMenu(MenuBuilder paramMenuBuilder)
        {
            if (paramMenuBuilder == this.menu);
            while (true)
            {
                return;
                if (this.menu != null)
                {
                    this.menu.removeMenuPresenter(this.iconMenuPresenter);
                    this.menu.removeMenuPresenter(this.listMenuPresenter);
                }
                this.menu = paramMenuBuilder;
                if (paramMenuBuilder != null)
                {
                    if (this.iconMenuPresenter != null)
                        paramMenuBuilder.addMenuPresenter(this.iconMenuPresenter);
                    if (this.listMenuPresenter != null)
                        paramMenuBuilder.addMenuPresenter(this.listMenuPresenter);
                }
            }
        }

        void setStyle(Context paramContext)
        {
            TypedArray localTypedArray = paramContext.obtainStyledAttributes(R.styleable.Theme);
            this.background = localTypedArray.getResourceId(46, 0);
            this.fullBackground = localTypedArray.getResourceId(47, 0);
            this.windowAnimations = localTypedArray.getResourceId(93, 0);
            this.isCompact = localTypedArray.getBoolean(225, false);
            this.listPresenterTheme = localTypedArray.getResourceId(227, 16974575);
            localTypedArray.recycle();
        }

        private static class SavedState
            implements Parcelable
        {
            public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
            {
                public PhoneWindow.PanelFeatureState.SavedState createFromParcel(Parcel paramAnonymousParcel)
                {
                    return PhoneWindow.PanelFeatureState.SavedState.readFromParcel(paramAnonymousParcel);
                }

                public PhoneWindow.PanelFeatureState.SavedState[] newArray(int paramAnonymousInt)
                {
                    return new PhoneWindow.PanelFeatureState.SavedState[paramAnonymousInt];
                }
            };
            int featureId;
            boolean isInExpandedMode;
            boolean isOpen;
            Bundle menuState;

            private static SavedState readFromParcel(Parcel paramParcel)
            {
                int i = 1;
                SavedState localSavedState = new SavedState();
                localSavedState.featureId = paramParcel.readInt();
                int j;
                if (paramParcel.readInt() == i)
                {
                    j = i;
                    localSavedState.isOpen = j;
                    if (paramParcel.readInt() != i)
                        break label68;
                }
                while (true)
                {
                    localSavedState.isInExpandedMode = i;
                    if (localSavedState.isOpen)
                        localSavedState.menuState = paramParcel.readBundle();
                    return localSavedState;
                    j = 0;
                    break;
                    label68: i = 0;
                }
            }

            public int describeContents()
            {
                return 0;
            }

            public void writeToParcel(Parcel paramParcel, int paramInt)
            {
                int i = 1;
                paramParcel.writeInt(this.featureId);
                int j;
                if (this.isOpen)
                {
                    j = i;
                    paramParcel.writeInt(j);
                    if (!this.isInExpandedMode)
                        break label60;
                }
                while (true)
                {
                    paramParcel.writeInt(i);
                    if (this.isOpen)
                        paramParcel.writeBundle(this.menuState);
                    return;
                    j = 0;
                    break;
                    label60: i = 0;
                }
            }
        }
    }

    private static final class DrawableFeatureState
    {
        int alpha = 255;
        Drawable child;
        Drawable cur;
        int curAlpha = 255;
        Drawable def;
        final int featureId;
        Drawable local;
        int resid;
        Uri uri;

        DrawableFeatureState(int paramInt)
        {
            this.featureId = paramInt;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    final class DecorView extends FrameLayout
        implements RootViewSurfaceTaker
    {
        private ActionMode mActionMode;
        private PopupWindow mActionModePopup;
        private ActionBarContextView mActionModeView;
        private final Rect mBackgroundPadding = new Rect();
        private boolean mChanging;
        int mDefaultOpacity = -1;
        private int mDownY;
        private final Rect mDrawingBounds = new Rect();
        private final int mFeatureId;
        private final Rect mFrameOffsets = new Rect();
        private final Rect mFramePadding = new Rect();
        private Drawable mMenuBackground;

        @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
        RoundedCorners mRoundedCorners;
        private Runnable mShowActionModePopup;
        private boolean mWatchingForMenu;

        public DecorView(Context paramInt, int arg3)
        {
            super();
            int i;
            this.mFeatureId = i;
        }

        private void drawableChanged()
        {
            if (this.mChanging)
                return;
            setPadding(this.mFramePadding.left + this.mBackgroundPadding.left, this.mFramePadding.top + this.mBackgroundPadding.top, this.mFramePadding.right + this.mBackgroundPadding.right, this.mFramePadding.bottom + this.mBackgroundPadding.bottom);
            requestLayout();
            invalidate();
            int i = -1;
            Drawable localDrawable1 = getBackground();
            Drawable localDrawable2 = getForeground();
            if (localDrawable1 != null)
            {
                if (localDrawable2 != null)
                    break label129;
                i = localDrawable1.getOpacity();
            }
            while (true)
            {
                this.mDefaultOpacity = i;
                if (this.mFeatureId >= 0)
                    break;
                PhoneWindow.this.setDefaultWindowFormat(i);
                break;
                label129: if ((this.mFramePadding.left <= 0) && (this.mFramePadding.top <= 0) && (this.mFramePadding.right <= 0) && (this.mFramePadding.bottom <= 0))
                {
                    int j = localDrawable2.getOpacity();
                    int k = localDrawable1.getOpacity();
                    if ((j == -1) || (k == -1))
                        i = -1;
                    else if (j == 0)
                        i = k;
                    else if (k == 0)
                        i = j;
                    else
                        i = Drawable.resolveOpacity(j, k);
                }
                else
                {
                    i = -3;
                }
            }
        }

        private boolean isOutOfBounds(int paramInt1, int paramInt2)
        {
            if ((paramInt1 < -5) || (paramInt2 < -5) || (paramInt1 > 5 + getWidth()) || (paramInt2 > 5 + getHeight()));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
        protected void dispatchDraw(Canvas paramCanvas)
        {
            super.dispatchDraw(paramCanvas);
            PhoneWindow.Injector.drawRoundedCorners(PhoneWindow.this, this, paramCanvas, this.mFrameOffsets, this.mDrawingBounds);
        }

        public boolean dispatchGenericMotionEvent(MotionEvent paramMotionEvent)
        {
            Window.Callback localCallback = PhoneWindow.this.getCallback();
            if ((localCallback != null) && (!PhoneWindow.this.isDestroyed()) && (this.mFeatureId < 0));
            for (boolean bool = localCallback.dispatchGenericMotionEvent(paramMotionEvent); ; bool = super.dispatchGenericMotionEvent(paramMotionEvent))
                return bool;
        }

        public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
        {
            boolean bool1 = true;
            int i = paramKeyEvent.getKeyCode();
            boolean bool2;
            if (paramKeyEvent.getAction() == 0)
            {
                bool2 = bool1;
                if ((!bool2) || (paramKeyEvent.getRepeatCount() != 0))
                    break label109;
                if ((PhoneWindow.this.mPanelChordingKey <= 0) || (PhoneWindow.this.mPanelChordingKey == i) || (!dispatchKeyShortcutEvent(paramKeyEvent)))
                    break label66;
            }
            label180: label188: label190: 
            while (true)
            {
                return bool1;
                bool2 = false;
                break;
                label66: if ((PhoneWindow.this.mPreparedPanel == null) || (!PhoneWindow.this.mPreparedPanel.isOpen) || (!PhoneWindow.this.performPanelShortcut(PhoneWindow.this.mPreparedPanel, i, paramKeyEvent, 0)))
                {
                    label109: Window.Callback localCallback;
                    if (!PhoneWindow.this.isDestroyed())
                    {
                        localCallback = PhoneWindow.this.getCallback();
                        if ((localCallback == null) || (this.mFeatureId >= 0))
                            break label180;
                    }
                    for (boolean bool3 = localCallback.dispatchKeyEvent(paramKeyEvent); ; bool3 = super.dispatchKeyEvent(paramKeyEvent))
                    {
                        if (bool3)
                            break label188;
                        if (!bool2)
                            break label190;
                        bool1 = PhoneWindow.this.onKeyDown(this.mFeatureId, paramKeyEvent.getKeyCode(), paramKeyEvent);
                        break;
                    }
                    continue;
                    bool1 = PhoneWindow.this.onKeyUp(this.mFeatureId, paramKeyEvent.getKeyCode(), paramKeyEvent);
                }
            }
        }

        public boolean dispatchKeyShortcutEvent(KeyEvent paramKeyEvent)
        {
            int i = 1;
            if ((PhoneWindow.this.mPreparedPanel != null) && (PhoneWindow.this.performPanelShortcut(PhoneWindow.this.mPreparedPanel, paramKeyEvent.getKeyCode(), paramKeyEvent, i)))
                if (PhoneWindow.this.mPreparedPanel != null)
                    PhoneWindow.this.mPreparedPanel.isHandled = i;
            label174: 
            while (true)
            {
                return i;
                Window.Callback localCallback = PhoneWindow.this.getCallback();
                if ((localCallback != null) && (!PhoneWindow.this.isDestroyed()) && (this.mFeatureId < 0));
                for (boolean bool1 = localCallback.dispatchKeyShortcutEvent(paramKeyEvent); ; bool1 = super.dispatchKeyShortcutEvent(paramKeyEvent))
                {
                    if (bool1)
                        break label174;
                    if (PhoneWindow.this.mPreparedPanel == null)
                    {
                        PhoneWindow.PanelFeatureState localPanelFeatureState = PhoneWindow.this.getPanelState(0, i);
                        PhoneWindow.this.preparePanel(localPanelFeatureState, paramKeyEvent);
                        boolean bool2 = PhoneWindow.this.performPanelShortcut(localPanelFeatureState, paramKeyEvent.getKeyCode(), paramKeyEvent, i);
                        localPanelFeatureState.isPrepared = false;
                        if (bool2)
                            break;
                    }
                    int j = 0;
                    break;
                }
            }
        }

        public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
        {
            Window.Callback localCallback = PhoneWindow.this.getCallback();
            if ((localCallback != null) && (!PhoneWindow.this.isDestroyed()) && (localCallback.dispatchPopulateAccessibilityEvent(paramAccessibilityEvent)));
            for (boolean bool = true; ; bool = super.dispatchPopulateAccessibilityEvent(paramAccessibilityEvent))
                return bool;
        }

        public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
        {
            Window.Callback localCallback = PhoneWindow.this.getCallback();
            if ((localCallback != null) && (!PhoneWindow.this.isDestroyed()) && (this.mFeatureId < 0));
            for (boolean bool = localCallback.dispatchTouchEvent(paramMotionEvent); ; bool = super.dispatchTouchEvent(paramMotionEvent))
                return bool;
        }

        public boolean dispatchTrackballEvent(MotionEvent paramMotionEvent)
        {
            Window.Callback localCallback = PhoneWindow.this.getCallback();
            if ((localCallback != null) && (!PhoneWindow.this.isDestroyed()) && (this.mFeatureId < 0));
            for (boolean bool = localCallback.dispatchTrackballEvent(paramMotionEvent); ; bool = super.dispatchTrackballEvent(paramMotionEvent))
                return bool;
        }

        public void draw(Canvas paramCanvas)
        {
            super.draw(paramCanvas);
            if (this.mMenuBackground != null)
                this.mMenuBackground.draw(paramCanvas);
        }

        public void finishChanging()
        {
            this.mChanging = false;
            drawableChanged();
        }

        protected boolean fitSystemWindows(Rect paramRect)
        {
            this.mFrameOffsets.set(paramRect);
            if (getForeground() != null)
                drawableChanged();
            return super.fitSystemWindows(paramRect);
        }

        protected void onAttachedToWindow()
        {
            super.onAttachedToWindow();
            updateWindowResizeState();
            Window.Callback localCallback = PhoneWindow.this.getCallback();
            if ((localCallback != null) && (!PhoneWindow.this.isDestroyed()) && (this.mFeatureId < 0))
                localCallback.onAttachedToWindow();
            if (this.mFeatureId == -1)
                PhoneWindow.this.openPanelsAfterRestore();
        }

        public void onCloseSystemDialogs(String paramString)
        {
            if (this.mFeatureId >= 0)
                PhoneWindow.this.closeAllPanels();
        }

        protected void onDetachedFromWindow()
        {
            super.onDetachedFromWindow();
            Window.Callback localCallback = PhoneWindow.this.getCallback();
            if ((localCallback != null) && (this.mFeatureId < 0))
                localCallback.onDetachedFromWindow();
            if (PhoneWindow.this.mActionBar != null)
                PhoneWindow.this.mActionBar.dismissPopupMenus();
            if (this.mActionModePopup != null)
            {
                removeCallbacks(this.mShowActionModePopup);
                if (this.mActionModePopup.isShowing())
                    this.mActionModePopup.dismiss();
                this.mActionModePopup = null;
            }
            PhoneWindow.PanelFeatureState localPanelFeatureState = PhoneWindow.this.getPanelState(0, false);
            if ((localPanelFeatureState != null) && (localPanelFeatureState.menu != null) && (this.mFeatureId < 0))
                localPanelFeatureState.menu.close();
        }

        public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
        {
            int i = paramMotionEvent.getAction();
            if ((this.mFeatureId >= 0) && (i == 0) && (isOutOfBounds((int)paramMotionEvent.getX(), (int)paramMotionEvent.getY())))
                PhoneWindow.this.closePanel(this.mFeatureId);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        protected void onMeasure(int paramInt1, int paramInt2)
        {
            DisplayMetrics localDisplayMetrics = getContext().getResources().getDisplayMetrics();
            int i;
            TypedValue localTypedValue3;
            label62: int i5;
            label93: TypedValue localTypedValue2;
            label139: int i4;
            label170: int n;
            int i1;
            int i2;
            TypedValue localTypedValue1;
            label243: int i3;
            if (localDisplayMetrics.widthPixels < localDisplayMetrics.heightPixels)
            {
                i = 1;
                int j = View.MeasureSpec.getMode(paramInt1);
                int k = View.MeasureSpec.getMode(paramInt2);
                int m = 0;
                if (j == -2147483648)
                {
                    if (i == 0)
                        break label308;
                    localTypedValue3 = PhoneWindow.this.mFixedWidthMinor;
                    if ((localTypedValue3 != null) && (localTypedValue3.type != 0))
                    {
                        if (localTypedValue3.type != 5)
                            break label320;
                        i5 = (int)localTypedValue3.getDimension(localDisplayMetrics);
                        if (i5 > 0)
                        {
                            paramInt1 = View.MeasureSpec.makeMeasureSpec(Math.min(i5, View.MeasureSpec.getSize(paramInt1)), 1073741824);
                            m = 1;
                        }
                    }
                }
                if (k == -2147483648)
                {
                    if (i == 0)
                        break label357;
                    localTypedValue2 = PhoneWindow.this.mFixedHeightMajor;
                    if ((localTypedValue2 != null) && (localTypedValue2.type != 0))
                    {
                        if (localTypedValue2.type != 5)
                            break label369;
                        i4 = (int)localTypedValue2.getDimension(localDisplayMetrics);
                        if (i4 > 0)
                            paramInt2 = View.MeasureSpec.makeMeasureSpec(Math.min(i4, View.MeasureSpec.getSize(paramInt2)), 1073741824);
                    }
                }
                super.onMeasure(paramInt1, paramInt2);
                n = getMeasuredWidth();
                i1 = 0;
                i2 = View.MeasureSpec.makeMeasureSpec(n, 1073741824);
                if ((m == 0) && (j == -2147483648))
                {
                    if (i == 0)
                        break label406;
                    localTypedValue1 = PhoneWindow.this.mMinWidthMinor;
                    if (localTypedValue1.type != 0)
                    {
                        if (localTypedValue1.type != 5)
                            break label418;
                        i3 = (int)localTypedValue1.getDimension(localDisplayMetrics);
                    }
                }
            }
            while (true)
            {
                if (n < i3)
                {
                    i2 = View.MeasureSpec.makeMeasureSpec(i3, 1073741824);
                    i1 = 1;
                }
                if (i1 != 0)
                    super.onMeasure(i2, paramInt2);
                return;
                i = 0;
                break;
                label308: localTypedValue3 = PhoneWindow.this.mFixedWidthMajor;
                break label62;
                label320: if (localTypedValue3.type == 6)
                {
                    i5 = (int)localTypedValue3.getFraction(localDisplayMetrics.widthPixels, localDisplayMetrics.widthPixels);
                    break label93;
                }
                i5 = 0;
                break label93;
                label357: localTypedValue2 = PhoneWindow.this.mFixedHeightMinor;
                break label139;
                label369: if (localTypedValue2.type == 6)
                {
                    i4 = (int)localTypedValue2.getFraction(localDisplayMetrics.heightPixels, localDisplayMetrics.heightPixels);
                    break label170;
                }
                i4 = 0;
                break label170;
                label406: localTypedValue1 = PhoneWindow.this.mMinWidthMajor;
                break label243;
                label418: if (localTypedValue1.type == 6)
                    i3 = (int)localTypedValue1.getFraction(localDisplayMetrics.widthPixels, localDisplayMetrics.widthPixels);
                else
                    i3 = 0;
            }
        }

        public boolean onTouchEvent(MotionEvent paramMotionEvent)
        {
            return onInterceptTouchEvent(paramMotionEvent);
        }

        public void onWindowFocusChanged(boolean paramBoolean)
        {
            super.onWindowFocusChanged(paramBoolean);
            if ((!paramBoolean) && (PhoneWindow.this.mPanelChordingKey != 0))
                PhoneWindow.this.closePanel(0);
            Window.Callback localCallback = PhoneWindow.this.getCallback();
            if ((localCallback != null) && (!PhoneWindow.this.isDestroyed()) && (this.mFeatureId < 0))
                localCallback.onWindowFocusChanged(paramBoolean);
        }

        public void sendAccessibilityEvent(int paramInt)
        {
            if (!AccessibilityManager.getInstance(this.mContext).isEnabled());
            while (true)
            {
                return;
                if (((this.mFeatureId == 0) || (this.mFeatureId == 6) || (this.mFeatureId == 2) || (this.mFeatureId == 5)) && (getChildCount() == 1))
                    getChildAt(0).sendAccessibilityEvent(paramInt);
                else
                    super.sendAccessibilityEvent(paramInt);
            }
        }

        public void setBackgroundDrawable(Drawable paramDrawable)
        {
            super.setBackgroundDrawable(paramDrawable);
            if (getWindowToken() != null)
                updateWindowResizeState();
        }

        protected boolean setFrame(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        {
            boolean bool = super.setFrame(paramInt1, paramInt2, paramInt3, paramInt4);
            if (bool)
            {
                Rect localRect1 = this.mDrawingBounds;
                getDrawingRect(localRect1);
                Drawable localDrawable1 = getForeground();
                if (localDrawable1 != null)
                {
                    Rect localRect2 = this.mFrameOffsets;
                    localRect1.left += localRect2.left;
                    localRect1.top += localRect2.top;
                    localRect1.right -= localRect2.right;
                    localRect1.bottom -= localRect2.bottom;
                    localDrawable1.setBounds(localRect1);
                    Rect localRect3 = this.mFramePadding;
                    localRect1.left += localRect3.left - localRect2.left;
                    localRect1.top += localRect3.top - localRect2.top;
                    localRect1.right -= localRect3.right - localRect2.right;
                    localRect1.bottom -= localRect3.bottom - localRect2.bottom;
                }
                Drawable localDrawable2 = getBackground();
                if (localDrawable2 != null)
                    localDrawable2.setBounds(localRect1);
            }
            return bool;
        }

        public void setSurfaceFormat(int paramInt)
        {
            PhoneWindow.this.setFormat(paramInt);
        }

        public void setSurfaceKeepScreenOn(boolean paramBoolean)
        {
            if (paramBoolean)
                PhoneWindow.this.addFlags(128);
            while (true)
            {
                return;
                PhoneWindow.this.clearFlags(128);
            }
        }

        public void setSurfaceType(int paramInt)
        {
            PhoneWindow.this.setType(paramInt);
        }

        public void setWindowBackground(Drawable paramDrawable)
        {
            if (getBackground() != paramDrawable)
            {
                setBackgroundDrawable(paramDrawable);
                if (paramDrawable == null)
                    break label31;
                paramDrawable.getPadding(this.mBackgroundPadding);
            }
            while (true)
            {
                drawableChanged();
                return;
                label31: this.mBackgroundPadding.setEmpty();
            }
        }

        public void setWindowFrame(Drawable paramDrawable)
        {
            if (getForeground() != paramDrawable)
            {
                setForeground(paramDrawable);
                if (paramDrawable == null)
                    break label31;
                paramDrawable.getPadding(this.mFramePadding);
            }
            while (true)
            {
                drawableChanged();
                return;
                label31: this.mFramePadding.setEmpty();
            }
        }

        public boolean showContextMenuForChild(View paramView)
        {
            if (PhoneWindow.this.mContextMenu == null)
            {
                PhoneWindow.access$902(PhoneWindow.this, new ContextMenuBuilder(getContext()));
                PhoneWindow.this.mContextMenu.setCallback(PhoneWindow.this.mContextMenuCallback);
                MenuDialogHelper localMenuDialogHelper = PhoneWindow.this.mContextMenu.show(paramView, paramView.getWindowToken());
                if (localMenuDialogHelper != null)
                    localMenuDialogHelper.setPresenterCallback(PhoneWindow.this.mContextMenuCallback);
                PhoneWindow.access$1002(PhoneWindow.this, localMenuDialogHelper);
                if (localMenuDialogHelper == null)
                    break label109;
            }
            label109: for (boolean bool = true; ; bool = false)
            {
                return bool;
                PhoneWindow.this.mContextMenu.clearAll();
                break;
            }
        }

        public ActionMode startActionMode(ActionMode.Callback paramCallback)
        {
            if (this.mActionMode != null)
                this.mActionMode.finish();
            ActionModeCallbackWrapper localActionModeCallbackWrapper = new ActionModeCallbackWrapper(paramCallback);
            Object localObject = null;
            if ((PhoneWindow.this.getCallback() != null) && (!PhoneWindow.this.isDestroyed()));
            try
            {
                ActionMode localActionMode = PhoneWindow.this.getCallback().onWindowStartingActionMode(localActionModeCallbackWrapper);
                localObject = localActionMode;
                label64: if (localObject != null)
                    this.mActionMode = localObject;
                while (true)
                {
                    if ((this.mActionMode != null) && (PhoneWindow.this.getCallback() != null) && (!PhoneWindow.this.isDestroyed()));
                    try
                    {
                        PhoneWindow.this.getCallback().onActionModeStarted(this.mActionMode);
                        label116: return this.mActionMode;
                        label292: Context localContext;
                        ActionBarContextView localActionBarContextView;
                        if (this.mActionModeView == null)
                        {
                            if (PhoneWindow.this.isFloating())
                            {
                                this.mActionModeView = new ActionBarContextView(this.mContext);
                                this.mActionModePopup = new PopupWindow(this.mContext, null, 16843720);
                                this.mActionModePopup.setLayoutInScreenEnabled(true);
                                this.mActionModePopup.setLayoutInsetDecor(true);
                                this.mActionModePopup.setWindowLayoutType(2);
                                this.mActionModePopup.setContentView(this.mActionModeView);
                                this.mActionModePopup.setWidth(-1);
                                TypedValue localTypedValue = new TypedValue();
                                this.mContext.getTheme().resolveAttribute(16843499, localTypedValue, true);
                                int i = TypedValue.complexToDimensionPixelSize(localTypedValue.data, this.mContext.getResources().getDisplayMetrics());
                                this.mActionModeView.setContentHeight(i);
                                this.mActionModePopup.setHeight(-2);
                                this.mShowActionModePopup = new Runnable()
                                {
                                    public void run()
                                    {
                                        PhoneWindow.DecorView.this.mActionModePopup.showAtLocation(PhoneWindow.DecorView.this.mActionModeView.getApplicationWindowToken(), 55, 0, 0);
                                    }
                                };
                            }
                        }
                        else
                        {
                            if (this.mActionModeView == null)
                                continue;
                            this.mActionModeView.killMode();
                            localContext = getContext();
                            localActionBarContextView = this.mActionModeView;
                            if (this.mActionModePopup != null)
                                break label448;
                        }
                        label448: for (boolean bool = true; ; bool = false)
                        {
                            StandaloneActionMode localStandaloneActionMode = new StandaloneActionMode(localContext, localActionBarContextView, localActionModeCallbackWrapper, bool);
                            if (!paramCallback.onCreateActionMode(localStandaloneActionMode, localStandaloneActionMode.getMenu()))
                                break label454;
                            localStandaloneActionMode.invalidate();
                            this.mActionModeView.initForMode(localStandaloneActionMode);
                            this.mActionModeView.setVisibility(0);
                            this.mActionMode = localStandaloneActionMode;
                            if (this.mActionModePopup != null)
                                post(this.mShowActionModePopup);
                            this.mActionModeView.sendAccessibilityEvent(32);
                            break;
                            ViewStub localViewStub = (ViewStub)findViewById(16909081);
                            if (localViewStub == null)
                                break label292;
                            this.mActionModeView = ((ActionBarContextView)localViewStub.inflate());
                            break label292;
                        }
                        label454: this.mActionMode = null;
                    }
                    catch (AbstractMethodError localAbstractMethodError1)
                    {
                        break label116;
                    }
                }
            }
            catch (AbstractMethodError localAbstractMethodError2)
            {
                break label64;
            }
        }

        public ActionMode startActionModeForChild(View paramView, ActionMode.Callback paramCallback)
        {
            return startActionMode(paramCallback);
        }

        public void startChanging()
        {
            this.mChanging = true;
        }

        public boolean superDispatchGenericMotionEvent(MotionEvent paramMotionEvent)
        {
            return super.dispatchGenericMotionEvent(paramMotionEvent);
        }

        public boolean superDispatchKeyEvent(KeyEvent paramKeyEvent)
        {
            int i = 1;
            if (super.dispatchKeyEvent(paramKeyEvent));
            while (true)
            {
                return i;
                if (paramKeyEvent.getKeyCode() == 4)
                {
                    int j = paramKeyEvent.getAction();
                    if (this.mActionMode != null)
                    {
                        if (j == i)
                            this.mActionMode.finish();
                    }
                    else if ((PhoneWindow.this.mActionBar != null) && (PhoneWindow.this.mActionBar.hasExpandedActionView()))
                    {
                        if (j != i)
                            continue;
                        PhoneWindow.this.mActionBar.collapseActionView();
                    }
                }
                else
                {
                    i = 0;
                }
            }
        }

        public boolean superDispatchKeyShortcutEvent(KeyEvent paramKeyEvent)
        {
            return super.dispatchKeyShortcutEvent(paramKeyEvent);
        }

        public boolean superDispatchTouchEvent(MotionEvent paramMotionEvent)
        {
            return super.dispatchTouchEvent(paramMotionEvent);
        }

        public boolean superDispatchTrackballEvent(MotionEvent paramMotionEvent)
        {
            return super.dispatchTrackballEvent(paramMotionEvent);
        }

        void updateWindowResizeState()
        {
            Drawable localDrawable = getBackground();
            if ((localDrawable == null) || (localDrawable.getOpacity() != -1));
            for (boolean bool = true; ; bool = false)
            {
                hackTurnOffWindowResizeAnim(bool);
                return;
            }
        }

        public InputQueue.Callback willYouTakeTheInputQueue()
        {
            if (this.mFeatureId < 0);
            for (InputQueue.Callback localCallback = PhoneWindow.this.mTakeInputQueueCallback; ; localCallback = null)
                return localCallback;
        }

        public SurfaceHolder.Callback2 willYouTakeTheSurface()
        {
            if (this.mFeatureId < 0);
            for (SurfaceHolder.Callback2 localCallback2 = PhoneWindow.this.mTakeSurfaceCallback; ; localCallback2 = null)
                return localCallback2;
        }

        private class ActionModeCallbackWrapper
            implements ActionMode.Callback
        {
            private ActionMode.Callback mWrapped;

            public ActionModeCallbackWrapper(ActionMode.Callback arg2)
            {
                Object localObject;
                this.mWrapped = localObject;
            }

            public boolean onActionItemClicked(ActionMode paramActionMode, MenuItem paramMenuItem)
            {
                return this.mWrapped.onActionItemClicked(paramActionMode, paramMenuItem);
            }

            public boolean onCreateActionMode(ActionMode paramActionMode, Menu paramMenu)
            {
                return this.mWrapped.onCreateActionMode(paramActionMode, paramMenu);
            }

            public void onDestroyActionMode(ActionMode paramActionMode)
            {
                this.mWrapped.onDestroyActionMode(paramActionMode);
                if (PhoneWindow.DecorView.this.mActionModePopup != null)
                {
                    PhoneWindow.DecorView.this.removeCallbacks(PhoneWindow.DecorView.this.mShowActionModePopup);
                    PhoneWindow.DecorView.this.mActionModePopup.dismiss();
                }
                while (true)
                {
                    if (PhoneWindow.DecorView.this.mActionModeView != null)
                        PhoneWindow.DecorView.this.mActionModeView.removeAllViews();
                    if ((PhoneWindow.this.getCallback() != null) && (!PhoneWindow.this.isDestroyed()));
                    try
                    {
                        PhoneWindow.this.getCallback().onActionModeFinished(PhoneWindow.DecorView.this.mActionMode);
                        label113: PhoneWindow.DecorView.access$102(PhoneWindow.DecorView.this, null);
                        return;
                        if (PhoneWindow.DecorView.this.mActionModeView == null)
                            continue;
                        PhoneWindow.DecorView.this.mActionModeView.setVisibility(8);
                    }
                    catch (AbstractMethodError localAbstractMethodError)
                    {
                        break label113;
                    }
                }
            }

            public boolean onPrepareActionMode(ActionMode paramActionMode, Menu paramMenu)
            {
                return this.mWrapped.onPrepareActionMode(paramActionMode, paramMenu);
            }
        }
    }

    private final class ActionMenuPresenterCallback
        implements MenuPresenter.Callback
    {
        private ActionMenuPresenterCallback()
        {
        }

        public void onCloseMenu(MenuBuilder paramMenuBuilder, boolean paramBoolean)
        {
            PhoneWindow.this.checkCloseActionMenu(paramMenuBuilder);
        }

        public boolean onOpenSubMenu(MenuBuilder paramMenuBuilder)
        {
            Window.Callback localCallback = PhoneWindow.this.getCallback();
            if (localCallback != null)
                localCallback.onMenuOpened(8, paramMenuBuilder);
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }

    private class PanelMenuPresenterCallback
        implements MenuPresenter.Callback
    {
        private PanelMenuPresenterCallback()
        {
        }

        public void onCloseMenu(MenuBuilder paramMenuBuilder, boolean paramBoolean)
        {
            MenuBuilder localMenuBuilder = paramMenuBuilder.getRootMenu();
            int i;
            PhoneWindow.PanelFeatureState localPanelFeatureState;
            if (localMenuBuilder != paramMenuBuilder)
            {
                i = 1;
                PhoneWindow localPhoneWindow = PhoneWindow.this;
                if (i != 0)
                    paramMenuBuilder = localMenuBuilder;
                localPanelFeatureState = localPhoneWindow.findMenuPanel(paramMenuBuilder);
                if (localPanelFeatureState != null)
                {
                    if (i == 0)
                        break label76;
                    PhoneWindow.this.callOnPanelClosed(localPanelFeatureState.featureId, localPanelFeatureState, localMenuBuilder);
                    PhoneWindow.this.closePanel(localPanelFeatureState, true);
                }
            }
            while (true)
            {
                return;
                i = 0;
                break;
                label76: PhoneWindow.this.closePanel(localPanelFeatureState, paramBoolean);
            }
        }

        public boolean onOpenSubMenu(MenuBuilder paramMenuBuilder)
        {
            if ((paramMenuBuilder == null) && (PhoneWindow.this.hasFeature(8)))
            {
                Window.Callback localCallback = PhoneWindow.this.getCallback();
                if ((localCallback != null) && (!PhoneWindow.this.isDestroyed()))
                    localCallback.onMenuOpened(8, paramMenuBuilder);
            }
            return true;
        }
    }

    static class WindowManagerHolder
    {
        static final IWindowManager sWindowManager = IWindowManager.Stub.asInterface(ServiceManager.getService("window"));
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static void drawRoundedCorners(PhoneWindow paramPhoneWindow, PhoneWindow.DecorView paramDecorView, Canvas paramCanvas, Rect paramRect1, Rect paramRect2)
        {
            if ((paramPhoneWindow.getAttributes().type > 99) || (paramRect1.left != 0) || (paramRect1.right != 0) || (paramRect1.bottom != 0));
            while (true)
            {
                return;
                if (paramDecorView.mRoundedCorners == null)
                    paramDecorView.mRoundedCorners = new RoundedCorners(paramPhoneWindow.getContext());
                paramDecorView.mRoundedCorners.draw(paramCanvas, paramRect2.left, paramRect2.top + paramRect1.top, paramRect2.right, paramRect2.bottom);
            }
        }

        static void handleIcsAppLayoutParams(PhoneWindow paramPhoneWindow, WindowManager paramWindowManager, WindowManager.LayoutParams paramLayoutParams)
        {
            int i = -2;
            if (paramPhoneWindow.getContext().getApplicationInfo().targetSdkVersion >= 14);
            for (int j = 1; ; j = 0)
            {
                if (j != 0)
                {
                    int k = paramWindowManager.getDefaultDisplay().getRotation();
                    if ((k != 0) && (k != 2))
                        i = -1;
                    paramLayoutParams.height = i;
                    paramLayoutParams.flags = (0x2 | paramLayoutParams.flags);
                    paramLayoutParams.dimAmount = 0.7F;
                }
                return;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.PhoneWindow
 * JD-Core Version:        0.6.2
 */