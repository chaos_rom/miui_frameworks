package com.android.internal.policy.impl;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.LoginFilter.UsernameFilterGeneric;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.internal.widget.LockPatternUtils;
import java.io.IOException;

public class AccountUnlockScreen extends RelativeLayout
    implements KeyguardScreen, View.OnClickListener, TextWatcher
{
    private static final int AWAKE_POKE_MILLIS = 30000;
    private static final String LOCK_PATTERN_CLASS = "com.android.settings.ChooseLockGeneric";
    private static final String LOCK_PATTERN_PACKAGE = "com.android.settings";
    private KeyguardScreenCallback mCallback;
    private ProgressDialog mCheckingDialog;
    private TextView mInstructions;
    private KeyguardStatusViewManager mKeyguardStatusViewManager;
    private LockPatternUtils mLockPatternUtils;
    private EditText mLogin;
    private Button mOk;
    private EditText mPassword;
    private TextView mTopHeader;
    private KeyguardUpdateMonitor mUpdateMonitor;

    public AccountUnlockScreen(Context paramContext, Configuration paramConfiguration, KeyguardUpdateMonitor paramKeyguardUpdateMonitor, KeyguardScreenCallback paramKeyguardScreenCallback, LockPatternUtils paramLockPatternUtils)
    {
        super(paramContext);
        this.mCallback = paramKeyguardScreenCallback;
        this.mLockPatternUtils = paramLockPatternUtils;
        LayoutInflater.from(paramContext).inflate(17367118, this, true);
        this.mTopHeader = ((TextView)findViewById(16908949));
        TextView localTextView = this.mTopHeader;
        if (this.mLockPatternUtils.isPermanentlyLocked());
        for (int i = 17040159; ; i = 17040158)
        {
            localTextView.setText(i);
            this.mInstructions = ((TextView)findViewById(16908951));
            this.mLogin = ((EditText)findViewById(16908952));
            EditText localEditText = this.mLogin;
            InputFilter[] arrayOfInputFilter = new InputFilter[1];
            arrayOfInputFilter[0] = new LoginFilter.UsernameFilterGeneric();
            localEditText.setFilters(arrayOfInputFilter);
            this.mLogin.addTextChangedListener(this);
            this.mPassword = ((EditText)findViewById(16908953));
            this.mPassword.addTextChangedListener(this);
            this.mOk = ((Button)findViewById(16908954));
            this.mOk.setOnClickListener(this);
            this.mUpdateMonitor = paramKeyguardUpdateMonitor;
            this.mKeyguardStatusViewManager = new KeyguardStatusViewManager(this, paramKeyguardUpdateMonitor, paramLockPatternUtils, paramKeyguardScreenCallback, true);
            return;
        }
    }

    private void asyncCheckPassword()
    {
        this.mCallback.pokeWakelock(30000);
        String str1 = this.mLogin.getText().toString();
        String str2 = this.mPassword.getText().toString();
        Account localAccount = findIntendedAccount(str1);
        if (localAccount == null)
            postOnCheckPasswordResult(false);
        while (true)
        {
            return;
            getProgressDialog().show();
            Bundle localBundle = new Bundle();
            localBundle.putString("password", str2);
            AccountManager.get(this.mContext).confirmCredentials(localAccount, localBundle, null, new AccountManagerCallback()
            {
                public void run(AccountManagerFuture<Bundle> paramAnonymousAccountManagerFuture)
                {
                    try
                    {
                        AccountUnlockScreen.this.mCallback.pokeWakelock(30000);
                        boolean bool = ((Bundle)paramAnonymousAccountManagerFuture.getResult()).getBoolean("booleanResult");
                        AccountUnlockScreen.this.postOnCheckPasswordResult(bool);
                        return;
                    }
                    catch (OperationCanceledException localOperationCanceledException)
                    {
                        while (true)
                        {
                            AccountUnlockScreen.this.postOnCheckPasswordResult(false);
                            AccountUnlockScreen.this.mLogin.post(new Runnable()
                            {
                                public void run()
                                {
                                    AccountUnlockScreen.this.getProgressDialog().hide();
                                }
                            });
                        }
                    }
                    catch (IOException localIOException)
                    {
                        while (true)
                        {
                            AccountUnlockScreen.this.postOnCheckPasswordResult(false);
                            AccountUnlockScreen.this.mLogin.post(new Runnable()
                            {
                                public void run()
                                {
                                    AccountUnlockScreen.this.getProgressDialog().hide();
                                }
                            });
                        }
                    }
                    catch (AuthenticatorException localAuthenticatorException)
                    {
                        while (true)
                        {
                            AccountUnlockScreen.this.postOnCheckPasswordResult(false);
                            AccountUnlockScreen.this.mLogin.post(new Runnable()
                            {
                                public void run()
                                {
                                    AccountUnlockScreen.this.getProgressDialog().hide();
                                }
                            });
                        }
                    }
                    finally
                    {
                        AccountUnlockScreen.this.mLogin.post(new Runnable()
                        {
                            public void run()
                            {
                                AccountUnlockScreen.this.getProgressDialog().hide();
                            }
                        });
                    }
                }
            }
            , null);
        }
    }

    private Account findIntendedAccount(String paramString)
    {
        Account[] arrayOfAccount = AccountManager.get(this.mContext).getAccountsByType("com.google");
        Object localObject = null;
        int i = 0;
        int j = arrayOfAccount.length;
        int k = 0;
        if (k < j)
        {
            Account localAccount = arrayOfAccount[k];
            int m = 0;
            if (paramString.equals(localAccount.name))
            {
                m = 4;
                label56: if (m <= i)
                    break label163;
                localObject = localAccount;
                i = m;
            }
            while (true)
            {
                k++;
                break;
                if (paramString.equalsIgnoreCase(localAccount.name))
                {
                    m = 3;
                    break label56;
                }
                if (paramString.indexOf('@') >= 0)
                    break label56;
                int n = localAccount.name.indexOf('@');
                if (n < 0)
                    break label56;
                String str = localAccount.name.substring(0, n);
                if (paramString.equals(str))
                {
                    m = 2;
                    break label56;
                }
                if (!paramString.equalsIgnoreCase(str))
                    break label56;
                m = 1;
                break label56;
                label163: if (m == i)
                    localObject = null;
            }
        }
        return localObject;
    }

    private Dialog getProgressDialog()
    {
        if (this.mCheckingDialog == null)
        {
            this.mCheckingDialog = new ProgressDialog(this.mContext);
            this.mCheckingDialog.setMessage(this.mContext.getString(17040166));
            this.mCheckingDialog.setIndeterminate(true);
            this.mCheckingDialog.setCancelable(false);
            this.mCheckingDialog.getWindow().setType(2009);
        }
        return this.mCheckingDialog;
    }

    private void postOnCheckPasswordResult(final boolean paramBoolean)
    {
        this.mLogin.post(new Runnable()
        {
            public void run()
            {
                if (paramBoolean)
                {
                    AccountUnlockScreen.this.mLockPatternUtils.setPermanentlyLocked(false);
                    AccountUnlockScreen.this.mLockPatternUtils.setLockPatternEnabled(false);
                    AccountUnlockScreen.this.mLockPatternUtils.saveLockPattern(null);
                    Intent localIntent = new Intent();
                    localIntent.setClassName("com.android.settings", "com.android.settings.ChooseLockGeneric");
                    localIntent.setFlags(268435456);
                    AccountUnlockScreen.this.mContext.startActivity(localIntent);
                    AccountUnlockScreen.this.mCallback.reportSuccessfulUnlockAttempt();
                    AccountUnlockScreen.this.mCallback.keyguardDone(true);
                }
                while (true)
                {
                    return;
                    AccountUnlockScreen.this.mInstructions.setText(17040164);
                    AccountUnlockScreen.this.mPassword.setText("");
                    AccountUnlockScreen.this.mCallback.reportFailedUnlockAttempt();
                }
            }
        });
    }

    public void afterTextChanged(Editable paramEditable)
    {
    }

    public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
    }

    public void cleanUp()
    {
        if (this.mCheckingDialog != null)
            this.mCheckingDialog.hide();
        this.mUpdateMonitor.removeCallback(this);
        this.mCallback = null;
        this.mLockPatternUtils = null;
        this.mUpdateMonitor = null;
    }

    public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
    {
        if ((paramKeyEvent.getAction() == 0) && (paramKeyEvent.getKeyCode() == 4))
            if (this.mLockPatternUtils.isPermanentlyLocked())
                this.mCallback.goToLockScreen();
        for (boolean bool = true; ; bool = super.dispatchKeyEvent(paramKeyEvent))
        {
            return bool;
            this.mCallback.forgotPattern(false);
            break;
        }
    }

    public boolean needsInput()
    {
        return true;
    }

    public void onClick(View paramView)
    {
        this.mCallback.pokeWakelock();
        if (paramView == this.mOk)
            asyncCheckPassword();
    }

    public void onPause()
    {
        this.mKeyguardStatusViewManager.onPause();
    }

    protected boolean onRequestFocusInDescendants(int paramInt, Rect paramRect)
    {
        return this.mLogin.requestFocus(paramInt, paramRect);
    }

    public void onResume()
    {
        this.mLogin.setText("");
        this.mPassword.setText("");
        this.mLogin.requestFocus();
        this.mKeyguardStatusViewManager.onResume();
    }

    public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
        this.mCallback.pokeWakelock(30000);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.AccountUnlockScreen
 * JD-Core Version:        0.6.2
 */