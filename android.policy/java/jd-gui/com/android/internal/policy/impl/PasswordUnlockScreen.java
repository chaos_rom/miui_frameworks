package com.android.internal.policy.impl;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.provider.Settings.Secure;
import android.security.KeyStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.text.method.TextKeyListener;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import com.android.internal.widget.LockPatternUtils;
import com.android.internal.widget.PasswordEntryKeyboardHelper;
import com.android.internal.widget.PasswordEntryKeyboardView;
import java.util.Iterator;
import java.util.List;
import miui.provider.ExtraSettings.Secure;
import miui.view.inputmethod.CustomizedImeForMiui;

@MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
public class PasswordUnlockScreen extends LinearLayout
    implements KeyguardScreen, TextView.OnEditorActionListener, View.OnKeyListener
{
    private static final int MINIMUM_PASSWORD_LENGTH_BEFORE_REPORT = 3;
    private static final String TAG = "PasswordUnlockScreen";
    private final KeyguardScreenCallback mCallback;
    private final int mCreationHardKeyboardHidden;
    private final int mCreationOrientation;
    private final boolean mIsAlpha;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    boolean mIsLockByFindDevice;
    private final PasswordEntryKeyboardHelper mKeyboardHelper;
    private final PasswordEntryKeyboardView mKeyboardView;
    private final LockPatternUtils mLockPatternUtils;
    private final EditText mPasswordEntry;
    private boolean mResuming;
    private final KeyguardStatusViewManager mStatusViewManager;
    private final KeyguardUpdateMonitor mUpdateMonitor;
    private final boolean mUseSystemIME = true;

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public PasswordUnlockScreen(Context paramContext, Configuration paramConfiguration, LockPatternUtils paramLockPatternUtils, KeyguardUpdateMonitor paramKeyguardUpdateMonitor, KeyguardScreenCallback paramKeyguardScreenCallback)
    {
        super(paramContext);
        this.mCreationHardKeyboardHidden = paramConfiguration.hardKeyboardHidden;
        this.mCreationOrientation = paramConfiguration.orientation;
        this.mUpdateMonitor = paramKeyguardUpdateMonitor;
        this.mCallback = paramKeyguardScreenCallback;
        this.mLockPatternUtils = paramLockPatternUtils;
        LayoutInflater localLayoutInflater = LayoutInflater.from(paramContext);
        boolean bool;
        label122: int j;
        if (this.mCreationOrientation != 2)
        {
            localLayoutInflater.inflate(17367121, this, true);
            this.mStatusViewManager = new KeyguardStatusViewManager(this, this.mUpdateMonitor, this.mLockPatternUtils, this.mCallback, true);
            int i = paramLockPatternUtils.getKeyguardStoredPasswordQuality();
            if ((262144 != i) && (327680 != i) && (393216 != i))
                break label419;
            bool = true;
            this.mIsAlpha = bool;
            this.mKeyboardView = ((PasswordEntryKeyboardView)findViewById(16908820));
            this.mPasswordEntry = ((EditText)findViewById(16908974));
            this.mPasswordEntry.setOnEditorActionListener(this);
            this.mPasswordEntry.setOnKeyListener(this);
            this.mKeyboardHelper = new PasswordEntryKeyboardHelper(paramContext, this.mKeyboardView, this, false);
            this.mKeyboardHelper.setEnableHaptics(this.mLockPatternUtils.isTactileFeedbackEnabled());
            j = 0;
            if (!this.mIsAlpha)
                break label425;
            this.mKeyboardHelper.setKeyboardMode(0);
            this.mKeyboardView.setVisibility(8);
            this.mPasswordEntry.requestFocus();
            if (!this.mIsAlpha)
                break label501;
            this.mPasswordEntry.setKeyListener(TextKeyListener.getInstance());
            this.mPasswordEntry.setInputType(8388737);
        }
        while (true)
        {
            this.mPasswordEntry.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View paramAnonymousView)
                {
                    PasswordUnlockScreen.this.mCallback.pokeWakelock();
                }
            });
            this.mPasswordEntry.addTextChangedListener(new TextWatcher()
            {
                public void afterTextChanged(Editable paramAnonymousEditable)
                {
                    if (!PasswordUnlockScreen.this.mResuming)
                        PasswordUnlockScreen.this.mCallback.pokeWakelock();
                }

                public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
                {
                }

                public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
                {
                }
            });
            View localView2 = findViewById(16908976);
            final InputMethodManager localInputMethodManager = (InputMethodManager)getContext().getSystemService("input_method");
            if ((this.mIsAlpha) && (localView2 != null) && (hasMultipleEnabledIMEsOrSubtypes(localInputMethodManager, false)))
            {
                localView2.setVisibility(0);
                j = 1;
                localView2.setOnClickListener(new View.OnClickListener()
                {
                    public void onClick(View paramAnonymousView)
                    {
                        PasswordUnlockScreen.this.mCallback.pokeWakelock();
                        localInputMethodManager.showInputMethodPicker();
                    }
                });
            }
            if (j == 0)
            {
                ViewGroup.LayoutParams localLayoutParams = this.mPasswordEntry.getLayoutParams();
                if ((localLayoutParams instanceof ViewGroup.MarginLayoutParams))
                {
                    ((ViewGroup.MarginLayoutParams)localLayoutParams).leftMargin = 0;
                    this.mPasswordEntry.setLayoutParams(localLayoutParams);
                }
            }
            Injector.initialize(this);
            return;
            localLayoutInflater.inflate(17367120, this, true);
            break;
            label419: bool = false;
            break label122;
            label425: this.mKeyboardHelper.setKeyboardMode(1);
            PasswordEntryKeyboardView localPasswordEntryKeyboardView = this.mKeyboardView;
            if (this.mCreationHardKeyboardHidden == 1);
            for (int k = 4; ; k = 0)
            {
                localPasswordEntryKeyboardView.setVisibility(k);
                View localView1 = findViewById(16908975);
                if (localView1 == null)
                    break;
                localView1.setVisibility(0);
                j = 1;
                localView1.setOnClickListener(new View.OnClickListener()
                {
                    public void onClick(View paramAnonymousView)
                    {
                        PasswordUnlockScreen.this.mKeyboardHelper.handleBackspace();
                    }
                });
                break;
            }
            label501: this.mPasswordEntry.setKeyListener(DigitsKeyListener.getInstance());
            this.mPasswordEntry.setInputType(8388626);
        }
    }

    private void handleAttemptLockout(long paramLong)
    {
        this.mPasswordEntry.setEnabled(false);
        this.mKeyboardView.setEnabled(false);
        new CountDownTimer(paramLong - SystemClock.elapsedRealtime(), 1000L)
        {
            public void onFinish()
            {
                PasswordUnlockScreen.this.mPasswordEntry.setEnabled(true);
                PasswordUnlockScreen.this.mKeyboardView.setEnabled(true);
                PasswordUnlockScreen.this.mStatusViewManager.resetStatusInfo();
            }

            public void onTick(long paramAnonymousLong)
            {
                int i = (int)(paramAnonymousLong / 1000L);
                Context localContext = PasswordUnlockScreen.this.getContext();
                Object[] arrayOfObject = new Object[1];
                arrayOfObject[0] = Integer.valueOf(i);
                String str = localContext.getString(17040156, arrayOfObject);
                PasswordUnlockScreen.this.mStatusViewManager.setInstructionText(str);
            }
        }
        .start();
    }

    private boolean hasMultipleEnabledIMEsOrSubtypes(InputMethodManager paramInputMethodManager, boolean paramBoolean)
    {
        int i = 0;
        int j = 1;
        List localList1 = paramInputMethodManager.getEnabledInputMethodList();
        int k = 0;
        Iterator localIterator1 = localList1.iterator();
        InputMethodInfo localInputMethodInfo;
        if (localIterator1.hasNext())
        {
            localInputMethodInfo = (InputMethodInfo)localIterator1.next();
            if (k <= j);
        }
        while (true)
        {
            return j;
            List localList2 = paramInputMethodManager.getEnabledInputMethodSubtypeList(localInputMethodInfo, j);
            if (localList2.isEmpty())
            {
                k++;
                break;
            }
            int m = 0;
            Iterator localIterator2 = localList2.iterator();
            while (localIterator2.hasNext())
                if (((InputMethodSubtype)localIterator2.next()).isAuxiliary())
                    m++;
            if ((localList2.size() - m <= 0) && ((!paramBoolean) || (m <= j)))
                break;
            k++;
            break;
            if ((k > j) || (paramInputMethodManager.getEnabledInputMethodSubtypeList(null, false).size() > j))
                i = j;
            j = i;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private void verifyPasswordAndUnlock()
    {
        String str = this.mPasswordEntry.getText().toString();
        if (this.mLockPatternUtils.checkPassword(str))
        {
            this.mCallback.keyguardDone(true);
            this.mCallback.reportSuccessfulUnlockAttempt();
            this.mStatusViewManager.setInstructionText(null);
            Injector.clearPinLockForFindDevice(this, str);
        }
        while (true)
        {
            this.mPasswordEntry.setText("");
            return;
            if (str.length() > 3)
            {
                this.mCallback.reportFailedUnlockAttempt();
                if (this.mUpdateMonitor.getFailedAttempts() % 5 == 0)
                    handleAttemptLockout(this.mLockPatternUtils.setLockoutAttemptDeadline());
                this.mStatusViewManager.setInstructionText(this.mContext.getString(17040127));
            }
            else if (str.length() > 0)
            {
                this.mStatusViewManager.setInstructionText(this.mContext.getString(17040127));
            }
        }
    }

    public void cleanUp()
    {
        this.mUpdateMonitor.removeCallback(this);
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    PasswordEntryKeyboardView getKeyboardView()
    {
        return this.mKeyboardView;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    LockPatternUtils getLockPatternUtils()
    {
        return this.mLockPatternUtils;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public boolean isAlphaOrDefaultImeIsCustomizedForMiui()
    {
        if ((this.mIsAlpha) || (CustomizedImeForMiui.defaultImeIsCustomizedForMiui(getContext().getContentResolver())));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public boolean needsInput()
    {
        return isAlphaOrDefaultImeIsCustomizedForMiui();
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        Configuration localConfiguration = getResources().getConfiguration();
        if ((localConfiguration.orientation != this.mCreationOrientation) || (localConfiguration.hardKeyboardHidden != this.mCreationHardKeyboardHidden))
            this.mCallback.recreateMe(localConfiguration);
    }

    protected void onConfigurationChanged(Configuration paramConfiguration)
    {
        super.onConfigurationChanged(paramConfiguration);
        if ((paramConfiguration.orientation != this.mCreationOrientation) || (paramConfiguration.hardKeyboardHidden != this.mCreationHardKeyboardHidden))
            this.mCallback.recreateMe(paramConfiguration);
    }

    public boolean onEditorAction(TextView paramTextView, int paramInt, KeyEvent paramKeyEvent)
    {
        if ((paramInt == 0) || (paramInt == 6) || (paramInt == 5))
            verifyPasswordAndUnlock();
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    public boolean onKey(View paramView, int paramInt, KeyEvent paramKeyEvent)
    {
        int i = 1;
        if ((paramKeyEvent.getAction() == i) && (paramInt == 5))
            this.mCallback.takeEmergencyCallAction();
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
        this.mCallback.pokeWakelock();
        return false;
    }

    public void onKeyboardChange(boolean paramBoolean)
    {
        PasswordEntryKeyboardView localPasswordEntryKeyboardView = this.mKeyboardView;
        if (paramBoolean);
        for (int i = 4; ; i = 0)
        {
            localPasswordEntryKeyboardView.setVisibility(i);
            return;
        }
    }

    public void onPause()
    {
        this.mStatusViewManager.onPause();
    }

    protected boolean onRequestFocusInDescendants(int paramInt, Rect paramRect)
    {
        return this.mPasswordEntry.requestFocus(paramInt, paramRect);
    }

    public void onResume()
    {
        this.mResuming = true;
        this.mStatusViewManager.onResume();
        this.mPasswordEntry.setText("");
        this.mPasswordEntry.requestFocus();
        long l = this.mLockPatternUtils.getLockoutAttemptDeadline();
        if (l != 0L)
            handleAttemptLockout(l);
        this.mResuming = false;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static void clearPinLockForFindDevice(PasswordUnlockScreen paramPasswordUnlockScreen, String paramString)
        {
            if (paramPasswordUnlockScreen.mIsLockByFindDevice)
            {
                paramPasswordUnlockScreen.getLockPatternUtils().clearLock(false);
                Settings.Secure.putInt(paramPasswordUnlockScreen.getContext().getContentResolver(), ExtraSettings.Secure.FIND_DEVICE_PIN_LOCK, 0);
                paramPasswordUnlockScreen.mIsLockByFindDevice = false;
            }
            while (true)
            {
                return;
                KeyStore.getInstance().password(paramString);
            }
        }

        static void hideKeyBoardViewIfNeed(PasswordUnlockScreen paramPasswordUnlockScreen)
        {
            if (CustomizedImeForMiui.defaultImeIsCustomizedForMiui(paramPasswordUnlockScreen.getContext().getContentResolver()))
                paramPasswordUnlockScreen.getKeyboardView().setVisibility(8);
        }

        static void initLockByFindDevice(PasswordUnlockScreen paramPasswordUnlockScreen)
        {
            int i = 1;
            if (Settings.Secure.getInt(paramPasswordUnlockScreen.getContext().getContentResolver(), ExtraSettings.Secure.FIND_DEVICE_PIN_LOCK, 0) == i);
            while (true)
            {
                paramPasswordUnlockScreen.mIsLockByFindDevice = i;
                return;
                i = 0;
            }
        }

        static void initialize(PasswordUnlockScreen paramPasswordUnlockScreen)
        {
            initLockByFindDevice(paramPasswordUnlockScreen);
            hideKeyBoardViewIfNeed(paramPasswordUnlockScreen);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.PasswordUnlockScreen
 * JD-Core Version:        0.6.2
 */