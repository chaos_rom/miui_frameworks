package com.android.internal.policy.impl;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.ActivityManagerNative;
import android.app.IActivityManager;
import android.app.IUiModeManager;
import android.app.IUiModeManager.Stub;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.app.UiModeManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.database.ContentObserver;
import android.graphics.Rect;
import android.media.AudioManager;
import android.media.IAudioService;
import android.media.IAudioService.Stub;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.IRemoteCallback.Stub;
import android.os.LocalPowerManager;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.UEventObserver;
import android.os.UEventObserver.UEvent;
import android.os.Vibrator;
import android.provider.Settings.Secure;
import android.provider.Settings.System;
import android.service.dreams.IDreamManager;
import android.service.dreams.IDreamManager.Stub;
import android.util.DisplayMetrics;
import android.util.EventLog;
import android.util.Log;
import android.util.Slog;
import android.util.SparseArray;
import android.view.Display;
import android.view.IApplicationToken;
import android.view.IWindowManager;
import android.view.InputChannel;
import android.view.InputEvent;
import android.view.InputEventReceiver;
import android.view.InputEventReceiver.Factory;
import android.view.KeyCharacterMap;
import android.view.KeyCharacterMap.FallbackAction;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.view.WindowManagerPolicy;
import android.view.WindowManagerPolicy.FakeWindow;
import android.view.WindowManagerPolicy.OnKeyguardExitResult;
import android.view.WindowManagerPolicy.ScreenOnListener;
import android.view.WindowManagerPolicy.WindowManagerFuncs;
import android.view.WindowManagerPolicy.WindowState;
import android.view.WindowOrientationListener;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.android.internal.statusbar.IStatusBarService;
import com.android.internal.statusbar.IStatusBarService.Stub;
import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.ITelephony.Stub;
import com.android.internal.widget.PointerLocationView;
import java.io.PrintWriter;

public class PhoneWindowManager
    implements WindowManagerPolicy
{
    static final int APPLICATION_LAYER = 2;
    static final int APPLICATION_MEDIA_OVERLAY_SUBLAYER = -1;
    static final int APPLICATION_MEDIA_SUBLAYER = -2;
    static final int APPLICATION_PANEL_SUBLAYER = 1;
    static final int APPLICATION_SUB_PANEL_SUBLAYER = 2;
    static final int BOOT_PROGRESS_LAYER = 24;
    static final boolean DEBUG = false;
    static final boolean DEBUG_INPUT = false;
    static final boolean DEBUG_LAYOUT = false;
    static final boolean DEBUG_STARTING_WINDOW = false;
    static final int DRAG_LAYER = 22;
    static final boolean ENABLE_CAR_DOCK_HOME_CAPTURE = true;
    static final boolean ENABLE_DESK_DOCK_HOME_CAPTURE = false;
    static final int HIDDEN_NAV_CONSUMER_LAYER = 26;
    static final int INPUT_METHOD_DIALOG_LAYER = 10;
    static final int INPUT_METHOD_LAYER = 9;
    static final int KEYGUARD_DIALOG_LAYER = 12;
    static final int KEYGUARD_LAYER = 11;
    static final int LONG_PRESS_HOME_NOTHING = 0;
    static final int LONG_PRESS_HOME_RECENT_DIALOG = 1;
    static final int LONG_PRESS_HOME_RECENT_SYSTEM_UI = 2;
    static final int LONG_PRESS_POWER_GLOBAL_ACTIONS = 1;
    static final int LONG_PRESS_POWER_NOTHING = 0;
    static final int LONG_PRESS_POWER_SHUT_OFF = 2;
    private static final int MSG_DISABLE_POINTER_LOCATION = 2;
    private static final int MSG_DISPATCH_MEDIA_KEY_REPEAT_WITH_WAKE_LOCK = 4;
    private static final int MSG_DISPATCH_MEDIA_KEY_WITH_WAKE_LOCK = 3;
    private static final int MSG_ENABLE_POINTER_LOCATION = 1;
    static final int NAVIGATION_BAR_LAYER = 19;
    static final int NAVIGATION_BAR_PANEL_LAYER = 20;
    static final int PHONE_LAYER = 3;
    static final int POINTER_LAYER = 25;
    static final boolean PRINT_ANIM = false;
    static final int PRIORITY_PHONE_LAYER = 7;
    static final int RECENT_APPS_BEHAVIOR_DISMISS = 2;
    static final int RECENT_APPS_BEHAVIOR_DISMISS_AND_SWITCH = 3;
    static final int RECENT_APPS_BEHAVIOR_EXIT_TOUCH_MODE_AND_SHOW = 1;
    static final int RECENT_APPS_BEHAVIOR_SHOW_OR_DISMISS = 0;
    static final int SCREENSAVER_LAYER = 13;
    private static final long SCREENSHOT_CHORD_DEBOUNCE_DELAY_MILLIS = 150L;
    static final int SEARCH_BAR_LAYER = 4;
    static final int SECURE_SYSTEM_OVERLAY_LAYER = 23;
    static final boolean SEPARATE_TIMEOUT_FOR_SCREEN_SAVER = false;
    static final boolean SHOW_PROCESSES_ON_ALT_MENU = false;
    static final boolean SHOW_STARTING_ANIMATIONS = true;
    static final int STATUS_BAR_LAYER = 15;
    static final int STATUS_BAR_PANEL_LAYER = 16;
    static final int STATUS_BAR_SUB_PANEL_LAYER = 14;
    static final int SYSTEM_ALERT_LAYER = 8;
    static final int SYSTEM_DIALOG_LAYER = 5;
    public static final String SYSTEM_DIALOG_REASON_ASSIST = "assist";
    public static final String SYSTEM_DIALOG_REASON_GLOBAL_ACTIONS = "globalactions";
    public static final String SYSTEM_DIALOG_REASON_HOME_KEY = "homekey";
    public static final String SYSTEM_DIALOG_REASON_KEY = "reason";
    public static final String SYSTEM_DIALOG_REASON_RECENT_APPS = "recentapps";
    static final int SYSTEM_ERROR_LAYER = 21;
    static final int SYSTEM_OVERLAY_LAYER = 18;
    static final int SYSTEM_UI_CHANGING_LAYOUT = 6;
    static final String TAG = "WindowManager";
    static final int TOAST_LAYER = 6;
    static final int VOLUME_OVERLAY_LAYER = 17;
    static final int WALLPAPER_LAYER = 2;
    private static final int[] WINDOW_TYPES_WHERE_HOME_DOESNT_WORK = arrayOfInt;
    static final boolean localLOGV;
    static final Rect mTmpContentFrame;
    static final Rect mTmpDisplayFrame;
    static final Rect mTmpNavigationFrame;
    static final Rect mTmpParentFrame;
    static final Rect mTmpVisibleFrame;
    static SparseArray<String> sApplicationLaunchKeyCategories = new SparseArray();
    boolean mAccelerometerDefault;
    int mAllowAllRotations = -1;
    boolean mAllowLockscreenWhenOn;
    final Runnable mAllowSystemUiDelay = new Runnable()
    {
        public void run()
        {
        }
    };
    boolean mAssistKeyLongPressed;
    ProgressDialog mBootMsgDialog = null;
    PowerManager.WakeLock mBroadcastWakeLock;
    boolean mCanHideNavigationBar = false;
    boolean mCarDockEnablesAccelerometer;
    Intent mCarDockIntent;
    int mCarDockRotation;
    boolean mConsumeSearchKeyUp;
    int mContentBottom;
    int mContentLeft;
    int mContentRight;
    int mContentTop;
    Context mContext;
    int mCurBottom;
    int mCurLeft;
    int mCurRight;
    int mCurTop;
    int mCurrentAppOrientation = -1;
    boolean mDeskDockEnablesAccelerometer;
    Intent mDeskDockIntent;
    int mDeskDockRotation;
    boolean mDismissKeyguard;
    Display mDisplay;
    int mDockBottom;
    int mDockLayer;
    int mDockLeft;
    int mDockMode = 0;
    BroadcastReceiver mDockReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            if ("android.intent.action.DOCK_EVENT".equals(paramAnonymousIntent.getAction()))
                PhoneWindowManager.this.mDockMode = paramAnonymousIntent.getIntExtra("android.intent.extra.DOCK_STATE", 0);
            while (true)
            {
                PhoneWindowManager.this.updateRotation(true);
                PhoneWindowManager.this.updateOrientationListenerLp();
                return;
                try
                {
                    IUiModeManager localIUiModeManager = IUiModeManager.Stub.asInterface(ServiceManager.getService("uimode"));
                    PhoneWindowManager.this.mUiMode = localIUiModeManager.getCurrentModeType();
                }
                catch (RemoteException localRemoteException)
                {
                }
            }
        }
    };
    int mDockRight;
    int mDockTop;
    boolean mEnableShiftMenuBugReports = false;
    int mEndcallBehavior;
    int mExternalDisplayHeight;
    int mExternalDisplayWidth;
    private final SparseArray<KeyCharacterMap.FallbackAction> mFallbackActions = new SparseArray();
    IApplicationToken mFocusedApp;
    WindowManagerPolicy.WindowState mFocusedWindow;
    int mForceClearedSystemUiFlags = 0;
    boolean mForceStatusBar;

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    MiuiGlobalActions mGlobalActions;
    private UEventObserver mHDMIObserver = new UEventObserver()
    {
        public void onUEvent(UEventObserver.UEvent paramAnonymousUEvent)
        {
            PhoneWindowManager.this.setHdmiPlugged("1".equals(paramAnonymousUEvent.get("SWITCH_STATE")));
        }
    };
    Handler mHandler;
    boolean mHasNavigationBar = false;
    boolean mHasSoftInput = false;
    boolean mHasSystemNavBar;
    boolean mHaveBuiltInKeyboard;
    boolean mHavePendingMediaKeyRepeatWithWakeLock;
    boolean mHdmiPlugged;
    int mHdmiRotation;
    boolean mHeadless;
    boolean mHideLockScreen;
    WindowManagerPolicy.FakeWindow mHideNavFakeWindow = null;
    final InputEventReceiver.Factory mHideNavInputEventReceiverFactory = new InputEventReceiver.Factory()
    {
        public InputEventReceiver createInputEventReceiver(InputChannel paramAnonymousInputChannel, Looper paramAnonymousLooper)
        {
            return new PhoneWindowManager.HideNavInputEventReceiver(PhoneWindowManager.this, paramAnonymousInputChannel, paramAnonymousLooper);
        }
    };
    Intent mHomeIntent;
    boolean mHomeLongPressed;
    boolean mHomePressed;
    int mIncallPowerBehavior;
    long[] mKeyboardTapVibePattern;
    WindowManagerPolicy.WindowState mKeyguard = null;
    KeyguardViewMediator mKeyguardMediator;
    int mLandscapeRotation = 0;
    boolean mLanguageSwitchKeyPressed;
    boolean mLastFocusNeedsMenu = false;
    WindowManagerPolicy.WindowState mLastInputMethodTargetWindow = null;
    WindowManagerPolicy.WindowState mLastInputMethodWindow = null;
    int mLastSystemUiFlags;
    boolean mLidControlsSleep;
    int mLidKeyboardAccessibility;
    int mLidNavigationAccessibility;
    int mLidOpenRotation;
    int mLidState = -1;
    final Object mLock = new Object();
    int mLockScreenTimeout;
    boolean mLockScreenTimerActive;
    private int mLongPressOnHomeBehavior = -1;
    int mLongPressOnPowerBehavior = -1;
    long[] mLongPressVibePattern;
    WindowManagerPolicy.WindowState mNavigationBar = null;
    boolean mNavigationBarCanMove = false;
    int[] mNavigationBarHeightForRotation = new int[4];
    boolean mNavigationBarOnBottom = true;
    int[] mNavigationBarWidthForRotation = new int[4];
    MyOrientationListener mOrientationListener;
    boolean mOrientationSensorEnabled = false;
    boolean mPendingPowerKeyUpCanceled;
    boolean mPluggedIn;
    InputChannel mPointerLocationInputChannel;
    PointerLocationInputEventReceiver mPointerLocationInputEventReceiver;
    int mPointerLocationMode = 0;
    PointerLocationView mPointerLocationView;
    int mPortraitRotation = 0;
    volatile boolean mPowerKeyHandled;
    private long mPowerKeyTime;
    private boolean mPowerKeyTriggered;
    private Runnable mPowerLongPress = new Runnable()
    {
        public void run()
        {
            if (PhoneWindowManager.this.mLongPressOnPowerBehavior < 0)
                PhoneWindowManager.this.mLongPressOnPowerBehavior = PhoneWindowManager.this.mContext.getResources().getInteger(17694742);
            switch (PhoneWindowManager.this.mLongPressOnPowerBehavior)
            {
            case 0:
            default:
            case 1:
            case 2:
            }
            while (true)
            {
                return;
                PhoneWindowManager.this.mPowerKeyHandled = true;
                PhoneWindowManager.this.performHapticFeedbackLw(null, 0, false);
                PhoneWindowManager.this.sendCloseSystemWindows("globalactions");
                PhoneWindowManager.this.showGlobalActionsDialog();
                continue;
                PhoneWindowManager.this.mPowerKeyHandled = true;
                PhoneWindowManager.this.performHapticFeedbackLw(null, 0, false);
                PhoneWindowManager.this.sendCloseSystemWindows("globalactions");
                PhoneWindowManager.this.mWindowManagerFuncs.shutdown();
            }
        }
    };
    LocalPowerManager mPowerManager;
    BroadcastReceiver mPowerReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            boolean bool = false;
            if ("android.intent.action.BATTERY_CHANGED".equals(paramAnonymousIntent.getAction()))
            {
                PhoneWindowManager localPhoneWindowManager = PhoneWindowManager.this;
                if (paramAnonymousIntent.getIntExtra("plugged", 0) != 0)
                    bool = true;
                localPhoneWindowManager.mPluggedIn = bool;
            }
        }
    };
    RecentApplicationsDialog mRecentAppsDialog;
    int mRecentAppsDialogHeldModifiers;
    int mResettingSystemUiFlags = 0;
    int mRestrictedScreenHeight;
    int mRestrictedScreenLeft;
    int mRestrictedScreenTop;
    int mRestrictedScreenWidth;
    boolean mSafeMode;
    long[] mSafeModeDisabledVibePattern;
    long[] mSafeModeEnabledVibePattern;
    Runnable mScreenLockTimeout = new Runnable()
    {
        public void run()
        {
            try
            {
                if (PhoneWindowManager.this.mKeyguardMediator != null)
                    PhoneWindowManager.this.mKeyguardMediator.doKeyguardTimeout();
                PhoneWindowManager.this.mLockScreenTimerActive = false;
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }
    };
    boolean mScreenOnEarly = false;
    boolean mScreenOnFully = false;
    boolean mScreenSaverEnabledByUser = false;
    boolean mScreenSaverFeatureAvailable;
    boolean mScreenSaverMayRun = true;
    int mScreenSaverTimeout = 0;
    private boolean mScreenshotChordEnabled;
    private final Runnable mScreenshotChordLongPress = new Runnable()
    {
        public void run()
        {
            PhoneWindowManager.this.takeScreenshot();
        }
    };
    ServiceConnection mScreenshotConnection = null;
    final Object mScreenshotLock = new Object();
    final Runnable mScreenshotTimeout = new Runnable()
    {
        public void run()
        {
            synchronized (PhoneWindowManager.this.mScreenshotLock)
            {
                if (PhoneWindowManager.this.mScreenshotConnection != null)
                {
                    PhoneWindowManager.this.mContext.unbindService(PhoneWindowManager.this.mScreenshotConnection);
                    PhoneWindowManager.this.mScreenshotConnection = null;
                }
                return;
            }
        }
    };
    boolean mSearchKeyShortcutPending;
    SearchManager mSearchManager;
    int mSeascapeRotation = 0;
    final Object mServiceAquireLock = new Object();
    ShortcutManager mShortcutManager;
    int mStableBottom;
    int mStableFullscreenBottom;
    int mStableFullscreenLeft;
    int mStableFullscreenRight;
    int mStableFullscreenTop;
    int mStableLeft;
    int mStableRight;
    int mStableTop;
    WindowManagerPolicy.WindowState mStatusBar = null;
    int mStatusBarHeight;
    int mStatusBarLayer;
    IStatusBarService mStatusBarService;
    boolean mSystemBooted;
    int mSystemBottom;
    int mSystemLeft;
    boolean mSystemReady;
    int mSystemRight;
    int mSystemTop;
    WindowManagerPolicy.WindowState mTopFullscreenOpaqueWindowState;
    boolean mTopIsFullscreen;
    int mUiMode;
    int mUnrestrictedScreenHeight;
    int mUnrestrictedScreenLeft;
    int mUnrestrictedScreenTop;
    int mUnrestrictedScreenWidth;
    int mUpsideDownRotation = 0;
    int mUserRotation = 0;
    int mUserRotationMode = 0;
    Vibrator mVibrator;
    long[] mVirtualKeyVibePattern;
    private boolean mVolumeDownKeyConsumedByScreenshotChord;
    private long mVolumeDownKeyTime;
    private boolean mVolumeDownKeyTriggered;
    private boolean mVolumeUpKeyTriggered;
    IWindowManager mWindowManager;
    WindowManagerPolicy.WindowManagerFuncs mWindowManagerFuncs;

    static
    {
        sApplicationLaunchKeyCategories.append(64, "android.intent.category.APP_BROWSER");
        sApplicationLaunchKeyCategories.append(65, "android.intent.category.APP_EMAIL");
        sApplicationLaunchKeyCategories.append(207, "android.intent.category.APP_CONTACTS");
        sApplicationLaunchKeyCategories.append(208, "android.intent.category.APP_CALENDAR");
        sApplicationLaunchKeyCategories.append(209, "android.intent.category.APP_MUSIC");
        sApplicationLaunchKeyCategories.append(210, "android.intent.category.APP_CALCULATOR");
        mTmpParentFrame = new Rect();
        mTmpDisplayFrame = new Rect();
        mTmpContentFrame = new Rect();
        mTmpVisibleFrame = new Rect();
        mTmpNavigationFrame = new Rect();
        int[] arrayOfInt = new int[2];
        arrayOfInt[0] = 2003;
        arrayOfInt[1] = 2010;
    }

    private void applyLidSwitchState()
    {
        this.mPowerManager.setKeyboardVisibility(isBuiltInKeyboardVisible());
        if ((this.mLidState == 0) && (this.mLidControlsSleep))
            this.mPowerManager.goToSleep(SystemClock.uptimeMillis());
    }

    private void applyStableConstraints(int paramInt1, int paramInt2, Rect paramRect)
    {
        if ((paramInt1 & 0x100) != 0)
        {
            if ((paramInt2 & 0x400) == 0)
                break label93;
            if (paramRect.left < this.mStableFullscreenLeft)
                paramRect.left = this.mStableFullscreenLeft;
            if (paramRect.top < this.mStableFullscreenTop)
                paramRect.top = this.mStableFullscreenTop;
            if (paramRect.right > this.mStableFullscreenRight)
                paramRect.right = this.mStableFullscreenRight;
            if (paramRect.bottom > this.mStableFullscreenBottom)
                paramRect.bottom = this.mStableFullscreenBottom;
        }
        while (true)
        {
            return;
            label93: if (paramRect.left < this.mStableLeft)
                paramRect.left = this.mStableLeft;
            if (paramRect.top < this.mStableTop)
                paramRect.top = this.mStableTop;
            if (paramRect.right > this.mStableRight)
                paramRect.right = this.mStableRight;
            if (paramRect.bottom > this.mStableBottom)
                paramRect.bottom = this.mStableBottom;
        }
    }

    private void cancelPendingPowerKeyAction()
    {
        if (!this.mPowerKeyHandled)
            this.mHandler.removeCallbacks(this.mPowerLongPress);
        if (this.mPowerKeyTriggered)
            this.mPendingPowerKeyUpCanceled = true;
    }

    private void cancelPendingScreenshotChordAction()
    {
        this.mHandler.removeCallbacks(this.mScreenshotChordLongPress);
    }

    private void disablePointerLocation()
    {
        if (this.mPointerLocationInputEventReceiver != null)
        {
            this.mPointerLocationInputEventReceiver.dispose();
            this.mPointerLocationInputEventReceiver = null;
        }
        if (this.mPointerLocationInputChannel != null)
        {
            this.mPointerLocationInputChannel.dispose();
            this.mPointerLocationInputChannel = null;
        }
        if (this.mPointerLocationView != null)
        {
            ((WindowManager)this.mContext.getSystemService("window")).removeView(this.mPointerLocationView);
            this.mPointerLocationView = null;
        }
    }

    private void enablePointerLocation()
    {
        if (this.mPointerLocationView == null)
        {
            this.mPointerLocationView = new PointerLocationView(this.mContext);
            this.mPointerLocationView.setPrintCoords(false);
            WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams(-1, -1);
            localLayoutParams.type = 2015;
            localLayoutParams.flags = 1304;
            localLayoutParams.format = -3;
            localLayoutParams.setTitle("PointerLocation");
            WindowManager localWindowManager = (WindowManager)this.mContext.getSystemService("window");
            localLayoutParams.inputFeatures = (0x2 | localLayoutParams.inputFeatures);
            localWindowManager.addView(this.mPointerLocationView, localLayoutParams);
            this.mPointerLocationInputChannel = this.mWindowManagerFuncs.monitorInput("PointerLocationView");
            this.mPointerLocationInputEventReceiver = new PointerLocationInputEventReceiver(this.mPointerLocationInputChannel, Looper.myLooper(), this.mPointerLocationView);
        }
    }

    static IAudioService getAudioService()
    {
        IAudioService localIAudioService = IAudioService.Stub.asInterface(ServiceManager.checkService("audio"));
        if (localIAudioService == null)
            Log.w("WindowManager", "Unable to find IAudioService interface.");
        return localIAudioService;
    }

    private IDreamManager getDreamManager()
    {
        IDreamManager localIDreamManager;
        if (!this.mScreenSaverFeatureAvailable)
            localIDreamManager = null;
        while (true)
        {
            return localIDreamManager;
            localIDreamManager = IDreamManager.Stub.asInterface(ServiceManager.checkService("dreams"));
            if (localIDreamManager == null)
                Log.w("WindowManager", "Unable to find IDreamManager");
        }
    }

    static long[] getLongIntArray(Resources paramResources, int paramInt)
    {
        int[] arrayOfInt = paramResources.getIntArray(paramInt);
        long[] arrayOfLong;
        if (arrayOfInt == null)
            arrayOfLong = null;
        while (true)
        {
            return arrayOfLong;
            arrayOfLong = new long[arrayOfInt.length];
            for (int i = 0; i < arrayOfInt.length; i++)
                arrayOfLong[i] = arrayOfInt[i];
        }
    }

    private SearchManager getSearchManager()
    {
        if (this.mSearchManager == null)
            this.mSearchManager = ((SearchManager)this.mContext.getSystemService("search"));
        return this.mSearchManager;
    }

    static ITelephony getTelephonyService()
    {
        return ITelephony.Stub.asInterface(ServiceManager.checkService("phone"));
    }

    private void handleLongPressOnHome()
    {
        if (this.mLongPressOnHomeBehavior < 0)
        {
            this.mLongPressOnHomeBehavior = this.mContext.getResources().getInteger(17694753);
            if ((this.mLongPressOnHomeBehavior < 0) || (this.mLongPressOnHomeBehavior > 2))
                this.mLongPressOnHomeBehavior = 0;
        }
        if (this.mLongPressOnHomeBehavior != 0)
        {
            performHapticFeedbackLw(null, 0, false);
            sendCloseSystemWindows("recentapps");
            this.mHomeLongPressed = true;
        }
        if (this.mLongPressOnHomeBehavior == 1)
            showOrHideRecentAppsDialog(0);
        while (true)
        {
            return;
            if (this.mLongPressOnHomeBehavior == 2)
                try
                {
                    IStatusBarService localIStatusBarService = getStatusBarService();
                    if (localIStatusBarService == null)
                        continue;
                    localIStatusBarService.toggleRecentApps();
                }
                catch (RemoteException localRemoteException)
                {
                    Slog.e("WindowManager", "RemoteException when showing recent apps", localRemoteException);
                    this.mStatusBarService = null;
                }
        }
    }

    private boolean interceptFallback(WindowManagerPolicy.WindowState paramWindowState, KeyEvent paramKeyEvent, int paramInt)
    {
        boolean bool = true;
        if (((0x1 & interceptKeyBeforeQueueing(paramKeyEvent, paramInt, bool)) != 0) && (interceptKeyBeforeDispatching(paramWindowState, paramKeyEvent, paramInt) == 0L));
        while (true)
        {
            return bool;
            bool = false;
        }
    }

    private void interceptPowerKeyDown(boolean paramBoolean)
    {
        this.mPowerKeyHandled = paramBoolean;
        if (!paramBoolean)
            this.mHandler.postDelayed(this.mPowerLongPress, ViewConfiguration.getGlobalActionKeyTimeout());
    }

    private boolean interceptPowerKeyUp(boolean paramBoolean)
    {
        boolean bool = false;
        if (!this.mPowerKeyHandled)
        {
            this.mHandler.removeCallbacks(this.mPowerLongPress);
            if (!paramBoolean)
                bool = true;
        }
        return bool;
    }

    private void interceptScreenshotChord()
    {
        if ((this.mScreenshotChordEnabled) && (this.mVolumeDownKeyTriggered) && (this.mPowerKeyTriggered) && (!this.mVolumeUpKeyTriggered))
        {
            long l = SystemClock.uptimeMillis();
            if ((l <= 150L + this.mVolumeDownKeyTime) && (l <= 150L + this.mPowerKeyTime))
            {
                this.mVolumeDownKeyConsumedByScreenshotChord = true;
                cancelPendingPowerKeyAction();
                this.mHandler.postDelayed(this.mScreenshotChordLongPress, ViewConfiguration.getGlobalActionKeyTimeout());
            }
        }
    }

    private boolean isAnyPortrait(int paramInt)
    {
        if ((paramInt == this.mPortraitRotation) || (paramInt == this.mUpsideDownRotation));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isBuiltInKeyboardVisible()
    {
        if ((this.mHaveBuiltInKeyboard) && (!isHidden(this.mLidKeyboardAccessibility)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean isHidden(int paramInt)
    {
        int i = 1;
        switch (paramInt)
        {
        default:
            i = 0;
        case 1:
        case 2:
        }
        while (true)
        {
            return i;
            if (this.mLidState != 0)
            {
                i = 0;
                continue;
                if (this.mLidState != i)
                    int j = 0;
            }
        }
    }

    private boolean isLandscapeOrSeascape(int paramInt)
    {
        if ((paramInt == this.mLandscapeRotation) || (paramInt == this.mSeascapeRotation));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean keyguardIsShowingTq()
    {
        if (this.mKeyguardMediator == null);
        for (boolean bool = false; ; bool = this.mKeyguardMediator.isShowingAndNotHidden())
            return bool;
    }

    private void launchAssistAction()
    {
        sendCloseSystemWindows("assist");
        Intent localIntent = SearchManager.getAssistIntent(this.mContext);
        if (localIntent != null)
            localIntent.setFlags(872415232);
        try
        {
            this.mContext.startActivity(localIntent);
            return;
        }
        catch (ActivityNotFoundException localActivityNotFoundException)
        {
            while (true)
                Slog.w("WindowManager", "No activity to handle assist action.", localActivityNotFoundException);
        }
    }

    private void launchAssistLongPressAction()
    {
        performHapticFeedbackLw(null, 0, false);
        sendCloseSystemWindows("assist");
        Intent localIntent = new Intent("android.intent.action.SEARCH_LONG_PRESS");
        localIntent.setFlags(268435456);
        try
        {
            SearchManager localSearchManager = getSearchManager();
            if (localSearchManager != null)
                localSearchManager.stopSearch();
            this.mContext.startActivity(localIntent);
            return;
        }
        catch (ActivityNotFoundException localActivityNotFoundException)
        {
            while (true)
                Slog.w("WindowManager", "No activity to handle assist long press action.", localActivityNotFoundException);
        }
    }

    private void offsetInputMethodWindowLw(WindowManagerPolicy.WindowState paramWindowState)
    {
        int i = paramWindowState.getContentFrameLw().top + paramWindowState.getGivenContentInsetsLw().top;
        if (this.mContentBottom > i)
            this.mContentBottom = i;
        int j = paramWindowState.getVisibleFrameLw().top + paramWindowState.getGivenVisibleInsetsLw().top;
        if (this.mCurBottom > j)
            this.mCurBottom = j;
    }

    private int readRotation(int paramInt)
    {
        try
        {
            int j = this.mContext.getResources().getInteger(paramInt);
            label56: int i;
            switch (j)
            {
            default:
                i = -1;
            case 0:
            case 90:
            case 180:
            case 270:
            }
            while (true)
            {
                return i;
                i = 0;
                continue;
                i = 1;
                continue;
                i = 2;
                continue;
                i = 3;
            }
        }
        catch (Resources.NotFoundException localNotFoundException)
        {
            break label56;
        }
    }

    static void sendCloseSystemWindows(Context paramContext, String paramString)
    {
        if (ActivityManagerNative.isSystemReady());
        try
        {
            ActivityManagerNative.getDefault().closeSystemDialogs(paramString);
            label15: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label15;
        }
    }

    private void takeScreenshot()
    {
        synchronized (this.mScreenshotLock)
        {
            if (this.mScreenshotConnection == null)
            {
                ComponentName localComponentName = new ComponentName("com.android.systemui", "com.android.systemui.screenshot.TakeScreenshotService");
                Intent localIntent = new Intent();
                localIntent.setComponent(localComponentName);
                ServiceConnection local11 = new ServiceConnection()
                {
                    public void onServiceConnected(ComponentName paramAnonymousComponentName, IBinder paramAnonymousIBinder)
                    {
                        Messenger localMessenger;
                        Message localMessage;
                        synchronized (PhoneWindowManager.this.mScreenshotLock)
                        {
                            if (PhoneWindowManager.this.mScreenshotConnection != this)
                                return;
                            localMessenger = new Messenger(paramAnonymousIBinder);
                            localMessage = Message.obtain(null, 1);
                            localMessage.replyTo = new Messenger(new Handler(PhoneWindowManager.this.mHandler.getLooper())
                            {
                                public void handleMessage(Message paramAnonymous2Message)
                                {
                                    synchronized (PhoneWindowManager.this.mScreenshotLock)
                                    {
                                        if (PhoneWindowManager.this.mScreenshotConnection == jdField_this)
                                        {
                                            PhoneWindowManager.this.mContext.unbindService(PhoneWindowManager.this.mScreenshotConnection);
                                            PhoneWindowManager.this.mScreenshotConnection = null;
                                            PhoneWindowManager.this.mHandler.removeCallbacks(PhoneWindowManager.this.mScreenshotTimeout);
                                        }
                                        return;
                                    }
                                }
                            });
                            localMessage.arg2 = 0;
                            localMessage.arg1 = 0;
                            if ((PhoneWindowManager.this.mStatusBar != null) && (PhoneWindowManager.this.mStatusBar.isVisibleLw()))
                                localMessage.arg1 = 1;
                            if ((PhoneWindowManager.this.mNavigationBar != null) && (PhoneWindowManager.this.mNavigationBar.isVisibleLw()))
                                localMessage.arg2 = 1;
                        }
                        try
                        {
                            localMessenger.send(localMessage);
                            label155: return;
                            localObject2 = finally;
                            throw localObject2;
                        }
                        catch (RemoteException localRemoteException)
                        {
                            break label155;
                        }
                    }

                    public void onServiceDisconnected(ComponentName paramAnonymousComponentName)
                    {
                    }
                };
                if (this.mContext.bindService(localIntent, local11, 1))
                {
                    this.mScreenshotConnection = local11;
                    this.mHandler.postDelayed(this.mScreenshotTimeout, 10000L);
                }
            }
        }
    }

    private void updateLockScreenTimeout()
    {
        while (true)
        {
            boolean bool;
            synchronized (this.mScreenLockTimeout)
            {
                if ((this.mAllowLockscreenWhenOn) && (this.mScreenOnEarly) && (this.mKeyguardMediator != null) && (this.mKeyguardMediator.isSecure()))
                {
                    bool = true;
                    if (this.mLockScreenTimerActive != bool)
                    {
                        if (bool)
                        {
                            this.mHandler.postDelayed(this.mScreenLockTimeout, this.mLockScreenTimeout);
                            this.mLockScreenTimerActive = bool;
                        }
                    }
                    else
                        return;
                    this.mHandler.removeCallbacks(this.mScreenLockTimeout);
                }
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    private int updateSystemUiVisibilityLw()
    {
        int j;
        if (this.mFocusedWindow == null)
            j = 0;
        while (true)
        {
            return j;
            final int i = this.mFocusedWindow.getSystemUiVisibility() & (0xFFFFFFFF ^ this.mResettingSystemUiFlags) & (0xFFFFFFFF ^ this.mForceClearedSystemUiFlags);
            j = i ^ this.mLastSystemUiFlags;
            final boolean bool = Injector.getNeedsMenuLw(this.mFocusedWindow, this.mTopFullscreenOpaqueWindowState);
            if ((j == 0) && (this.mLastFocusNeedsMenu == bool) && (this.mFocusedApp == this.mFocusedWindow.getAppToken()))
            {
                j = 0;
            }
            else
            {
                this.mLastSystemUiFlags = i;
                this.mLastFocusNeedsMenu = bool;
                this.mFocusedApp = this.mFocusedWindow.getAppToken();
                this.mHandler.post(new Runnable()
                {
                    public void run()
                    {
                        try
                        {
                            IStatusBarService localIStatusBarService = PhoneWindowManager.this.getStatusBarService();
                            if (localIStatusBarService != null)
                            {
                                localIStatusBarService.setSystemUiVisibility(i, -1);
                                localIStatusBarService.topAppWindowChanged(bool);
                            }
                            return;
                        }
                        catch (RemoteException localRemoteException)
                        {
                            while (true)
                                PhoneWindowManager.this.mStatusBarService = null;
                        }
                    }
                });
            }
        }
    }

    // ERROR //
    public View addStartingWindow(IBinder paramIBinder, String paramString, int paramInt1, android.content.res.CompatibilityInfo paramCompatibilityInfo, CharSequence paramCharSequence, int paramInt2, int paramInt3, int paramInt4)
    {
        // Byte code:
        //     0: aload_2
        //     1: ifnonnull +9 -> 10
        //     4: aconst_null
        //     5: astore 11
        //     7: aload 11
        //     9: areturn
        //     10: aload_0
        //     11: getfield 635	com/android/internal/policy/impl/PhoneWindowManager:mContext	Landroid/content/Context;
        //     14: astore 14
        //     16: aload 14
        //     18: invokevirtual 1009	android/content/Context:getThemeResId	()I
        //     21: istore 15
        //     23: iload_3
        //     24: iload 15
        //     26: if_icmpne +8 -> 34
        //     29: iload 6
        //     31: ifeq +18 -> 49
        //     34: aload 14
        //     36: aload_2
        //     37: iconst_0
        //     38: invokevirtual 1013	android/content/Context:createPackageContext	(Ljava/lang/String;I)Landroid/content/Context;
        //     41: astore 14
        //     43: aload 14
        //     45: iload_3
        //     46: invokevirtual 1016	android/content/Context:setTheme	(I)V
        //     49: aload 14
        //     51: invokestatic 1022	com/android/internal/policy/PolicyManager:makeNewWindow	(Landroid/content/Context;)Landroid/view/Window;
        //     54: astore 17
        //     56: aload 17
        //     58: invokevirtual 1028	android/view/Window:getWindowStyle	()Landroid/content/res/TypedArray;
        //     61: astore 18
        //     63: aload 18
        //     65: bipush 12
        //     67: iconst_0
        //     68: invokevirtual 1034	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     71: ifne +301 -> 372
        //     74: aload 18
        //     76: bipush 14
        //     78: iconst_0
        //     79: invokevirtual 1034	android/content/res/TypedArray:getBoolean	(IZ)Z
        //     82: ifeq +6 -> 88
        //     85: goto +287 -> 372
        //     88: aload 17
        //     90: aload 14
        //     92: invokevirtual 772	android/content/Context:getResources	()Landroid/content/res/Resources;
        //     95: iload 6
        //     97: aload 5
        //     99: invokevirtual 1038	android/content/res/Resources:getText	(ILjava/lang/CharSequence;)Ljava/lang/CharSequence;
        //     102: invokevirtual 1039	android/view/Window:setTitle	(Ljava/lang/CharSequence;)V
        //     105: aload 17
        //     107: iconst_3
        //     108: invokevirtual 1042	android/view/Window:setType	(I)V
        //     111: aload 17
        //     113: ldc_w 1043
        //     116: bipush 8
        //     118: iload 8
        //     120: bipush 16
        //     122: ior
        //     123: ior
        //     124: ior
        //     125: ldc_w 1043
        //     128: bipush 8
        //     130: iload 8
        //     132: bipush 16
        //     134: ior
        //     135: ior
        //     136: ior
        //     137: invokevirtual 1045	android/view/Window:setFlags	(II)V
        //     140: aload 4
        //     142: invokevirtual 1050	android/content/res/CompatibilityInfo:supportsScreen	()Z
        //     145: ifne +11 -> 156
        //     148: aload 17
        //     150: ldc_w 1051
        //     153: invokevirtual 1054	android/view/Window:addFlags	(I)V
        //     156: aload 17
        //     158: bipush 255
        //     160: bipush 255
        //     162: invokevirtual 1057	android/view/Window:setLayout	(II)V
        //     165: aload 17
        //     167: invokevirtual 1061	android/view/Window:getAttributes	()Landroid/view/WindowManager$LayoutParams;
        //     170: astore 19
        //     172: aload 19
        //     174: aload_1
        //     175: putfield 1065	android/view/WindowManager$LayoutParams:token	Landroid/os/IBinder;
        //     178: aload 19
        //     180: aload_2
        //     181: putfield 1068	android/view/WindowManager$LayoutParams:packageName	Ljava/lang/String;
        //     184: aload 19
        //     186: aload 17
        //     188: invokevirtual 1028	android/view/Window:getWindowStyle	()Landroid/content/res/TypedArray;
        //     191: bipush 8
        //     193: iconst_0
        //     194: invokevirtual 1072	android/content/res/TypedArray:getResourceId	(II)I
        //     197: putfield 1075	android/view/WindowManager$LayoutParams:windowAnimations	I
        //     200: aload 19
        //     202: iconst_1
        //     203: aload 19
        //     205: getfield 1078	android/view/WindowManager$LayoutParams:privateFlags	I
        //     208: ior
        //     209: putfield 1078	android/view/WindowManager$LayoutParams:privateFlags	I
        //     212: aload 19
        //     214: new 1080	java/lang/StringBuilder
        //     217: dup
        //     218: invokespecial 1081	java/lang/StringBuilder:<init>	()V
        //     221: ldc_w 1083
        //     224: invokevirtual 1086	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     227: aload_2
        //     228: invokevirtual 1086	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     231: invokevirtual 1090	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     234: invokevirtual 677	android/view/WindowManager$LayoutParams:setTitle	(Ljava/lang/CharSequence;)V
        //     237: aload 14
        //     239: ldc_w 637
        //     242: invokevirtual 643	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
        //     245: checkcast 645	android/view/WindowManager
        //     248: astore 20
        //     250: aload 17
        //     252: invokevirtual 1094	android/view/Window:getDecorView	()Landroid/view/View;
        //     255: astore 11
        //     257: aload 17
        //     259: invokevirtual 1097	android/view/Window:isFloating	()Z
        //     262: ifeq +9 -> 271
        //     265: aconst_null
        //     266: astore 11
        //     268: goto -261 -> 7
        //     271: aload 20
        //     273: aload 11
        //     275: aload 19
        //     277: invokeinterface 684 3 0
        //     282: aload 11
        //     284: invokevirtual 1103	android/view/View:getParent	()Landroid/view/ViewParent;
        //     287: astore 21
        //     289: aload 21
        //     291: ifnonnull -284 -> 7
        //     294: aconst_null
        //     295: astore 11
        //     297: goto -290 -> 7
        //     300: astore 12
        //     302: ldc 169
        //     304: new 1080	java/lang/StringBuilder
        //     307: dup
        //     308: invokespecial 1081	java/lang/StringBuilder:<init>	()V
        //     311: aload_1
        //     312: invokevirtual 1106	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     315: ldc_w 1108
        //     318: invokevirtual 1086	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     321: invokevirtual 1090	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     324: invokestatic 727	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     327: pop
        //     328: aconst_null
        //     329: astore 11
        //     331: goto -324 -> 7
        //     334: astore 9
        //     336: ldc 169
        //     338: new 1080	java/lang/StringBuilder
        //     341: dup
        //     342: invokespecial 1081	java/lang/StringBuilder:<init>	()V
        //     345: aload_1
        //     346: invokevirtual 1106	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     349: ldc_w 1110
        //     352: invokevirtual 1086	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     355: invokevirtual 1090	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     358: aload 9
        //     360: invokestatic 1111	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     363: pop
        //     364: goto -36 -> 328
        //     367: astore 16
        //     369: goto -320 -> 49
        //     372: aconst_null
        //     373: astore 11
        //     375: goto -368 -> 7
        //
        // Exception table:
        //     from	to	target	type
        //     10	23	300	android/view/WindowManager$BadTokenException
        //     34	49	300	android/view/WindowManager$BadTokenException
        //     49	289	300	android/view/WindowManager$BadTokenException
        //     10	23	334	java/lang/RuntimeException
        //     34	49	334	java/lang/RuntimeException
        //     49	289	334	java/lang/RuntimeException
        //     34	49	367	android/content/pm/PackageManager$NameNotFoundException
    }

    public void adjustConfigurationLw(Configuration paramConfiguration, int paramInt1, int paramInt2)
    {
        if ((paramInt1 & 0x1) != 0);
        for (boolean bool = true; ; bool = false)
        {
            this.mHaveBuiltInKeyboard = bool;
            readLidState();
            applyLidSwitchState();
            if ((paramConfiguration.keyboard == 1) || ((paramInt1 == 1) && (isHidden(this.mLidKeyboardAccessibility))))
            {
                paramConfiguration.hardKeyboardHidden = 2;
                if (!this.mHasSoftInput)
                    paramConfiguration.keyboardHidden = 2;
            }
            if ((paramConfiguration.navigation == 1) || ((paramInt2 == 1) && (isHidden(this.mLidNavigationAccessibility))))
                paramConfiguration.navigationHidden = 2;
            return;
        }
    }

    public int adjustSystemUiVisibilityLw(int paramInt)
    {
        this.mResettingSystemUiFlags = (paramInt & this.mResettingSystemUiFlags);
        return paramInt & (0xFFFFFFFF ^ this.mResettingSystemUiFlags) & (0xFFFFFFFF ^ this.mForceClearedSystemUiFlags);
    }

    public void adjustWindowParamsLw(WindowManager.LayoutParams paramLayoutParams)
    {
        switch (paramLayoutParams.type)
        {
        default:
        case 2005:
        case 2006:
        case 2015:
        }
        while (true)
        {
            return;
            paramLayoutParams.flags = (0x18 | paramLayoutParams.flags);
            paramLayoutParams.flags = (0xFFFBFFFF & paramLayoutParams.flags);
        }
    }

    public boolean allowAppAnimationsLw()
    {
        if ((this.mKeyguard != null) && (this.mKeyguard.isVisibleLw()) && (!this.mKeyguard.isAnimatingLw()));
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    public boolean allowKeyRepeat()
    {
        return this.mScreenOnEarly;
    }

    public void animatingWindowLw(WindowManagerPolicy.WindowState paramWindowState, WindowManager.LayoutParams paramLayoutParams)
    {
        if ((this.mTopFullscreenOpaqueWindowState == null) && (paramWindowState.isVisibleOrBehindKeyguardLw()) && (!paramWindowState.isGoneForLayoutLw()))
        {
            if ((0x800 & paramLayoutParams.flags) != 0)
                this.mForceStatusBar = true;
            if ((paramLayoutParams.type >= 1) && (paramLayoutParams.type <= 99) && (paramLayoutParams.x == 0) && (paramLayoutParams.y == 0) && (paramLayoutParams.width == -1) && (paramLayoutParams.height == -1))
            {
                this.mTopFullscreenOpaqueWindowState = paramWindowState;
                if ((0x80000 & paramLayoutParams.flags) != 0)
                    this.mHideLockScreen = true;
                if ((0x400000 & paramLayoutParams.flags) != 0)
                    this.mDismissKeyguard = true;
                if ((0x1 & paramLayoutParams.flags) != 0)
                    this.mAllowLockscreenWhenOn = true;
            }
        }
    }

    public void beginAnimationLw(int paramInt1, int paramInt2)
    {
        this.mTopFullscreenOpaqueWindowState = null;
        this.mForceStatusBar = false;
        this.mHideLockScreen = false;
        this.mAllowLockscreenWhenOn = false;
        this.mDismissKeyguard = false;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public void beginLayoutLw(int paramInt1, int paramInt2, int paramInt3)
    {
        this.mUnrestrictedScreenTop = 0;
        this.mUnrestrictedScreenLeft = 0;
        this.mUnrestrictedScreenWidth = paramInt1;
        this.mUnrestrictedScreenHeight = paramInt2;
        this.mRestrictedScreenTop = 0;
        this.mRestrictedScreenLeft = 0;
        this.mRestrictedScreenWidth = paramInt1;
        this.mRestrictedScreenHeight = paramInt2;
        this.mCurLeft = 0;
        this.mSystemLeft = 0;
        this.mStableFullscreenLeft = 0;
        this.mStableLeft = 0;
        this.mContentLeft = 0;
        this.mDockLeft = 0;
        this.mCurTop = 0;
        this.mSystemTop = 0;
        this.mStableFullscreenTop = 0;
        this.mStableTop = 0;
        this.mContentTop = 0;
        this.mDockTop = 0;
        this.mCurRight = paramInt1;
        this.mSystemRight = paramInt1;
        this.mStableFullscreenRight = paramInt1;
        this.mStableRight = paramInt1;
        this.mContentRight = paramInt1;
        this.mDockRight = paramInt1;
        this.mCurBottom = paramInt2;
        this.mSystemBottom = paramInt2;
        this.mStableFullscreenBottom = paramInt2;
        this.mStableBottom = paramInt2;
        this.mContentBottom = paramInt2;
        this.mDockBottom = paramInt2;
        this.mDockLayer = 268435456;
        this.mStatusBarLayer = -1;
        Rect localRect1 = mTmpParentFrame;
        Rect localRect2 = mTmpDisplayFrame;
        Rect localRect3 = mTmpVisibleFrame;
        int i = this.mDockLeft;
        localRect3.left = i;
        localRect2.left = i;
        localRect1.left = i;
        int j = this.mDockTop;
        localRect3.top = j;
        localRect2.top = j;
        localRect1.top = j;
        int k = this.mDockRight;
        localRect3.right = k;
        localRect2.right = k;
        localRect1.right = k;
        int m = this.mDockBottom;
        localRect3.bottom = m;
        localRect2.bottom = m;
        localRect1.bottom = m;
        int n;
        label334: int i1;
        label344: int i2;
        boolean bool;
        label373: int i21;
        label433: int i22;
        label446: int i19;
        if ((0x2 & this.mLastSystemUiFlags) == 0)
        {
            n = 1;
            if (n == 0)
                break label987;
            if (this.mHideNavFakeWindow != null)
            {
                this.mHideNavFakeWindow.dismiss();
                this.mHideNavFakeWindow = null;
            }
            if (this.mCanHideNavigationBar)
                break label1031;
            i1 = 1;
            i2 = n | i1;
            if (this.mNavigationBar != null)
            {
                if ((this.mNavigationBarCanMove) && (paramInt1 >= paramInt2))
                    break label1037;
                bool = true;
                this.mNavigationBarOnBottom = bool;
                if (!this.mNavigationBarOnBottom)
                    break label1075;
                int i17 = paramInt2 - this.mNavigationBarHeightForRotation[paramInt3];
                if (this.mHdmiPlugged)
                {
                    if ((this.mExternalDisplayHeight <= 0) || (paramInt2 <= 0))
                        break label1055;
                    if (this.mExternalDisplayWidth / this.mExternalDisplayHeight <= 1.0F)
                        break label1043;
                    i21 = 1;
                    if (paramInt1 / paramInt2 <= 1.0F)
                        break label1049;
                    i22 = 1;
                    if (i21 != i22)
                        break label1055;
                    i19 = 1;
                    label456: if (i19 != 0)
                    {
                        int i20 = this.mExternalDisplayHeight;
                        if (i17 > i20)
                            i17 = this.mExternalDisplayHeight;
                    }
                }
                mTmpNavigationFrame.set(0, i17, paramInt1, paramInt2);
                int i18 = mTmpNavigationFrame.top;
                this.mStableFullscreenBottom = i18;
                this.mStableBottom = i18;
                if (i2 == 0)
                    break label1061;
                this.mNavigationBar.showLw(true);
                this.mDockBottom = mTmpNavigationFrame.top;
                this.mRestrictedScreenHeight = (this.mDockBottom - this.mDockTop);
                label550: if ((i2 != 0) && (!this.mNavigationBar.isAnimatingLw()))
                    this.mSystemBottom = mTmpNavigationFrame.top;
            }
        }
        label1031: label1037: label1043: label1049: label1055: label1061: label1075: label1219: 
        while (true)
        {
            int i13 = this.mDockTop;
            this.mCurTop = i13;
            this.mContentTop = i13;
            int i14 = this.mDockBottom;
            this.mCurBottom = i14;
            this.mContentBottom = i14;
            int i15 = this.mDockLeft;
            this.mCurLeft = i15;
            this.mContentLeft = i15;
            int i16 = this.mDockRight;
            this.mCurRight = i16;
            this.mContentRight = i16;
            this.mStatusBarLayer = this.mNavigationBar.getSurfaceLayer();
            this.mNavigationBar.computeFrameLw(mTmpNavigationFrame, mTmpNavigationFrame, mTmpNavigationFrame, mTmpNavigationFrame);
            if (this.mStatusBar != null)
            {
                int i3 = this.mUnrestrictedScreenLeft;
                localRect2.left = i3;
                localRect1.left = i3;
                int i4 = this.mUnrestrictedScreenTop;
                localRect2.top = i4;
                localRect1.top = i4;
                int i5 = this.mUnrestrictedScreenWidth - this.mUnrestrictedScreenLeft;
                localRect2.right = i5;
                localRect1.right = i5;
                int i6 = this.mUnrestrictedScreenHeight - this.mUnrestrictedScreenTop;
                localRect2.bottom = i6;
                localRect1.bottom = i6;
                localRect3.left = this.mStableLeft;
                localRect3.top = this.mStableTop;
                localRect3.right = this.mStableRight;
                localRect3.bottom = this.mStableBottom;
                this.mStatusBarLayer = this.mStatusBar.getSurfaceLayer();
                this.mStatusBar.computeFrameLw(localRect1, localRect2, localRect3, localRect3);
                this.mStableTop = (this.mUnrestrictedScreenTop + this.mStatusBarHeight);
                if (this.mStatusBar.isVisibleLw())
                {
                    this.mDockTop = (this.mUnrestrictedScreenTop + this.mStatusBarHeight);
                    int i7 = this.mDockTop;
                    this.mCurTop = i7;
                    this.mContentTop = i7;
                    int i8 = this.mDockBottom;
                    this.mCurBottom = i8;
                    this.mContentBottom = i8;
                    int i9 = this.mDockLeft;
                    this.mCurLeft = i9;
                    this.mContentLeft = i9;
                    int i10 = this.mDockRight;
                    this.mCurRight = i10;
                    this.mContentRight = i10;
                }
                if ((!this.mStatusBar.isVisibleLw()) || (this.mStatusBar.isAnimatingLw()));
            }
            return;
            n = 0;
            break;
            label987: if (this.mHideNavFakeWindow != null)
                break label334;
            this.mHideNavFakeWindow = this.mWindowManagerFuncs.addFakeWindow(this.mHandler.getLooper(), this.mHideNavInputEventReceiverFactory, "hidden nav", 2022, 0, false, false, true);
            break label334;
            i1 = 0;
            break label344;
            bool = false;
            break label373;
            i21 = 0;
            break label433;
            i22 = 0;
            break label446;
            i19 = 0;
            break label456;
            this.mNavigationBar.hideLw(true);
            break label550;
            int i11 = paramInt1 - this.mNavigationBarWidthForRotation[paramInt3];
            if ((this.mHdmiPlugged) && (i11 > this.mExternalDisplayWidth))
                i11 = this.mExternalDisplayWidth;
            mTmpNavigationFrame.set(i11, 0, paramInt1, paramInt2);
            int i12 = mTmpNavigationFrame.left;
            this.mStableFullscreenRight = i12;
            this.mStableRight = i12;
            if (i2 != 0)
            {
                this.mNavigationBar.showLw(true);
                this.mDockRight = mTmpNavigationFrame.left;
                this.mRestrictedScreenWidth = (this.mDockRight - this.mDockLeft);
            }
            while (true)
            {
                if ((i2 == 0) || (this.mNavigationBar.isAnimatingLw()))
                    break label1219;
                this.mSystemRight = mTmpNavigationFrame.left;
                break;
                this.mNavigationBar.hideLw(true);
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void callInterceptPowerKeyUp(boolean paramBoolean)
    {
        interceptPowerKeyUp(paramBoolean);
    }

    public boolean canBeForceHidden(WindowManagerPolicy.WindowState paramWindowState, WindowManager.LayoutParams paramLayoutParams)
    {
        if ((paramLayoutParams.type != 2000) && (paramLayoutParams.type != 2019) && (paramLayoutParams.type != 2013));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public int checkAddPermission(WindowManager.LayoutParams paramLayoutParams)
    {
        int i = 0;
        int j = paramLayoutParams.type;
        if ((j < 2000) || (j > 2999))
            return i;
        String str = null;
        switch (j)
        {
        default:
        case 2005:
        case 2011:
        case 2013:
        case 2023:
        case 2002:
        case 2003:
        case 2006:
        case 2007:
        case 2010:
        }
        for (str = "android.permission.INTERNAL_SYSTEM_WINDOW"; (str != null) && (this.mContext.checkCallingOrSelfPermission(str) != 0); str = "android.permission.SYSTEM_ALERT_WINDOW")
        {
            i = -8;
            break;
        }
    }

    public Animation createForceHideEnterAnimation(boolean paramBoolean)
    {
        Context localContext = this.mContext;
        if (paramBoolean);
        for (int i = 17432618; ; i = 17432615)
            return AnimationUtils.loadAnimation(localContext, i);
    }

    Intent createHomeDockIntent()
    {
        Object localObject = null;
        Intent localIntent1;
        if (this.mUiMode == 3)
        {
            localIntent1 = this.mCarDockIntent;
            if (localIntent1 != null)
                break label36;
        }
        while (true)
        {
            return localObject;
            if (this.mUiMode == 2);
            localIntent1 = null;
            break;
            label36: ActivityInfo localActivityInfo = localIntent1.resolveActivityInfo(this.mContext.getPackageManager(), 128);
            if (localActivityInfo != null)
                if ((localActivityInfo.metaData != null) && (localActivityInfo.metaData.getBoolean("android.dock_home")))
                {
                    Intent localIntent2 = new Intent(localIntent1);
                    localIntent2.setClassName(localActivityInfo.packageName, localActivityInfo.name);
                    localObject = localIntent2;
                }
        }
    }

    public void dismissKeyguardLw()
    {
        if ((!this.mKeyguardMediator.isSecure()) && (this.mKeyguardMediator.isShowing()))
            this.mHandler.post(new Runnable()
            {
                public void run()
                {
                    PhoneWindowManager.this.mKeyguardMediator.keyguardDone(false, true);
                }
            });
    }

    void dispatchMediaKeyRepeatWithWakeLock(KeyEvent paramKeyEvent)
    {
        this.mHavePendingMediaKeyRepeatWithWakeLock = false;
        dispatchMediaKeyWithWakeLockToAudioService(KeyEvent.changeTimeRepeat(paramKeyEvent, SystemClock.uptimeMillis(), 1, 0x80 | paramKeyEvent.getFlags()));
        this.mBroadcastWakeLock.release();
    }

    void dispatchMediaKeyWithWakeLock(KeyEvent paramKeyEvent)
    {
        if (this.mHavePendingMediaKeyRepeatWithWakeLock)
        {
            this.mHandler.removeMessages(4);
            this.mHavePendingMediaKeyRepeatWithWakeLock = false;
            this.mBroadcastWakeLock.release();
        }
        dispatchMediaKeyWithWakeLockToAudioService(paramKeyEvent);
        if ((paramKeyEvent.getAction() == 0) && (paramKeyEvent.getRepeatCount() == 0))
        {
            this.mHavePendingMediaKeyRepeatWithWakeLock = true;
            Message localMessage = this.mHandler.obtainMessage(4, paramKeyEvent);
            localMessage.setAsynchronous(true);
            this.mHandler.sendMessageDelayed(localMessage, ViewConfiguration.getKeyRepeatTimeout());
        }
        while (true)
        {
            return;
            this.mBroadcastWakeLock.release();
        }
    }

    void dispatchMediaKeyWithWakeLockToAudioService(KeyEvent paramKeyEvent)
    {
        IAudioService localIAudioService;
        if (ActivityManagerNative.isSystemReady())
        {
            localIAudioService = getAudioService();
            if (localIAudioService == null);
        }
        try
        {
            localIAudioService.dispatchMediaKeyEventUnderWakelock(paramKeyEvent);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("WindowManager", "dispatchMediaKeyEvent threw exception " + localRemoteException);
        }
    }

    public KeyEvent dispatchUnhandledKey(WindowManagerPolicy.WindowState paramWindowState, KeyEvent paramKeyEvent, int paramInt)
    {
        KeyEvent localKeyEvent = null;
        int i;
        int k;
        KeyCharacterMap.FallbackAction localFallbackAction;
        if ((0x400 & paramKeyEvent.getFlags()) == 0)
        {
            KeyCharacterMap localKeyCharacterMap = paramKeyEvent.getKeyCharacterMap();
            i = paramKeyEvent.getKeyCode();
            int j = paramKeyEvent.getMetaState();
            if ((paramKeyEvent.getAction() != 0) || (paramKeyEvent.getRepeatCount() != 0))
                break label164;
            k = 1;
            if (k == 0)
                break label170;
            localFallbackAction = localKeyCharacterMap.getFallbackAction(i, j);
            label65: if (localFallbackAction != null)
            {
                int m = 0x400 | paramKeyEvent.getFlags();
                localKeyEvent = KeyEvent.obtain(paramKeyEvent.getDownTime(), paramKeyEvent.getEventTime(), paramKeyEvent.getAction(), localFallbackAction.keyCode, paramKeyEvent.getRepeatCount(), localFallbackAction.metaState, paramKeyEvent.getDeviceId(), paramKeyEvent.getScanCode(), m, paramKeyEvent.getSource(), null);
                if (!interceptFallback(paramWindowState, localKeyEvent, paramInt))
                {
                    localKeyEvent.recycle();
                    localKeyEvent = null;
                }
                if (k == 0)
                    break label187;
                this.mFallbackActions.put(i, localFallbackAction);
            }
        }
        while (true)
        {
            return localKeyEvent;
            label164: k = 0;
            break;
            label170: localFallbackAction = (KeyCharacterMap.FallbackAction)this.mFallbackActions.get(i);
            break label65;
            label187: if (paramKeyEvent.getAction() == 1)
            {
                this.mFallbackActions.remove(i);
                localFallbackAction.recycle();
            }
        }
    }

    public boolean doesForceHide(WindowManagerPolicy.WindowState paramWindowState, WindowManager.LayoutParams paramLayoutParams)
    {
        if (paramLayoutParams.type == 2004);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void dump(String paramString, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mSafeMode=");
        paramPrintWriter.print(this.mSafeMode);
        paramPrintWriter.print(" mSystemReady=");
        paramPrintWriter.print(this.mSystemReady);
        paramPrintWriter.print(" mSystemBooted=");
        paramPrintWriter.println(this.mSystemBooted);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mLidState=");
        paramPrintWriter.print(this.mLidState);
        paramPrintWriter.print(" mLidOpenRotation=");
        paramPrintWriter.print(this.mLidOpenRotation);
        paramPrintWriter.print(" mHdmiPlugged=");
        paramPrintWriter.println(this.mHdmiPlugged);
        if ((this.mLastSystemUiFlags != 0) || (this.mResettingSystemUiFlags != 0) || (this.mForceClearedSystemUiFlags != 0))
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("mLastSystemUiFlags=0x");
            paramPrintWriter.print(Integer.toHexString(this.mLastSystemUiFlags));
            paramPrintWriter.print(" mResettingSystemUiFlags=0x");
            paramPrintWriter.print(Integer.toHexString(this.mResettingSystemUiFlags));
            paramPrintWriter.print(" mForceClearedSystemUiFlags=0x");
            paramPrintWriter.println(Integer.toHexString(this.mForceClearedSystemUiFlags));
        }
        if (this.mLastFocusNeedsMenu)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("mLastFocusNeedsMenu=");
            paramPrintWriter.println(this.mLastFocusNeedsMenu);
        }
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mUiMode=");
        paramPrintWriter.print(this.mUiMode);
        paramPrintWriter.print(" mDockMode=");
        paramPrintWriter.print(this.mDockMode);
        paramPrintWriter.print(" mCarDockRotation=");
        paramPrintWriter.print(this.mCarDockRotation);
        paramPrintWriter.print(" mDeskDockRotation=");
        paramPrintWriter.println(this.mDeskDockRotation);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mUserRotationMode=");
        paramPrintWriter.print(this.mUserRotationMode);
        paramPrintWriter.print(" mUserRotation=");
        paramPrintWriter.print(this.mUserRotation);
        paramPrintWriter.print(" mAllowAllRotations=");
        paramPrintWriter.println(this.mAllowAllRotations);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mCurrentAppOrientation=");
        paramPrintWriter.println(this.mCurrentAppOrientation);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mCarDockEnablesAccelerometer=");
        paramPrintWriter.print(this.mCarDockEnablesAccelerometer);
        paramPrintWriter.print(" mDeskDockEnablesAccelerometer=");
        paramPrintWriter.println(this.mDeskDockEnablesAccelerometer);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mLidKeyboardAccessibility=");
        paramPrintWriter.print(this.mLidKeyboardAccessibility);
        paramPrintWriter.print(" mLidNavigationAccessibility=");
        paramPrintWriter.print(this.mLidNavigationAccessibility);
        paramPrintWriter.print(" mLidControlsSleep=");
        paramPrintWriter.println(this.mLidControlsSleep);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mLongPressOnPowerBehavior=");
        paramPrintWriter.print(this.mLongPressOnPowerBehavior);
        paramPrintWriter.print(" mHasSoftInput=");
        paramPrintWriter.println(this.mHasSoftInput);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mScreenOnEarly=");
        paramPrintWriter.print(this.mScreenOnEarly);
        paramPrintWriter.print(" mScreenOnFully=");
        paramPrintWriter.print(this.mScreenOnFully);
        paramPrintWriter.print(" mOrientationSensorEnabled=");
        paramPrintWriter.println(this.mOrientationSensorEnabled);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mUnrestrictedScreen=(");
        paramPrintWriter.print(this.mUnrestrictedScreenLeft);
        paramPrintWriter.print(",");
        paramPrintWriter.print(this.mUnrestrictedScreenTop);
        paramPrintWriter.print(") ");
        paramPrintWriter.print(this.mUnrestrictedScreenWidth);
        paramPrintWriter.print("x");
        paramPrintWriter.println(this.mUnrestrictedScreenHeight);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mRestrictedScreen=(");
        paramPrintWriter.print(this.mRestrictedScreenLeft);
        paramPrintWriter.print(",");
        paramPrintWriter.print(this.mRestrictedScreenTop);
        paramPrintWriter.print(") ");
        paramPrintWriter.print(this.mRestrictedScreenWidth);
        paramPrintWriter.print("x");
        paramPrintWriter.println(this.mRestrictedScreenHeight);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mStableFullscreen=(");
        paramPrintWriter.print(this.mStableFullscreenLeft);
        paramPrintWriter.print(",");
        paramPrintWriter.print(this.mStableFullscreenTop);
        paramPrintWriter.print(")-(");
        paramPrintWriter.print(this.mStableFullscreenRight);
        paramPrintWriter.print(",");
        paramPrintWriter.print(this.mStableFullscreenBottom);
        paramPrintWriter.println(")");
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mStable=(");
        paramPrintWriter.print(this.mStableLeft);
        paramPrintWriter.print(",");
        paramPrintWriter.print(this.mStableTop);
        paramPrintWriter.print(")-(");
        paramPrintWriter.print(this.mStableRight);
        paramPrintWriter.print(",");
        paramPrintWriter.print(this.mStableBottom);
        paramPrintWriter.println(")");
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mSystem=(");
        paramPrintWriter.print(this.mSystemLeft);
        paramPrintWriter.print(",");
        paramPrintWriter.print(this.mSystemTop);
        paramPrintWriter.print(")-(");
        paramPrintWriter.print(this.mSystemRight);
        paramPrintWriter.print(",");
        paramPrintWriter.print(this.mSystemBottom);
        paramPrintWriter.println(")");
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mCur=(");
        paramPrintWriter.print(this.mCurLeft);
        paramPrintWriter.print(",");
        paramPrintWriter.print(this.mCurTop);
        paramPrintWriter.print(")-(");
        paramPrintWriter.print(this.mCurRight);
        paramPrintWriter.print(",");
        paramPrintWriter.print(this.mCurBottom);
        paramPrintWriter.println(")");
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mContent=(");
        paramPrintWriter.print(this.mContentLeft);
        paramPrintWriter.print(",");
        paramPrintWriter.print(this.mContentTop);
        paramPrintWriter.print(")-(");
        paramPrintWriter.print(this.mContentRight);
        paramPrintWriter.print(",");
        paramPrintWriter.print(this.mContentBottom);
        paramPrintWriter.println(")");
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mDock=(");
        paramPrintWriter.print(this.mDockLeft);
        paramPrintWriter.print(",");
        paramPrintWriter.print(this.mDockTop);
        paramPrintWriter.print(")-(");
        paramPrintWriter.print(this.mDockRight);
        paramPrintWriter.print(",");
        paramPrintWriter.print(this.mDockBottom);
        paramPrintWriter.println(")");
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mDockLayer=");
        paramPrintWriter.print(this.mDockLayer);
        paramPrintWriter.print(" mStatusBarLayer=");
        paramPrintWriter.println(this.mStatusBarLayer);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mTopFullscreenOpaqueWindowState=");
        paramPrintWriter.println(this.mTopFullscreenOpaqueWindowState);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mTopIsFullscreen=");
        paramPrintWriter.print(this.mTopIsFullscreen);
        paramPrintWriter.print(" mForceStatusBar=");
        paramPrintWriter.print(this.mForceStatusBar);
        paramPrintWriter.print(" mHideLockScreen=");
        paramPrintWriter.println(this.mHideLockScreen);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mDismissKeyguard=");
        paramPrintWriter.print(this.mDismissKeyguard);
        paramPrintWriter.print(" mHomePressed=");
        paramPrintWriter.println(this.mHomePressed);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mAllowLockscreenWhenOn=");
        paramPrintWriter.print(this.mAllowLockscreenWhenOn);
        paramPrintWriter.print(" mLockScreenTimeout=");
        paramPrintWriter.print(this.mLockScreenTimeout);
        paramPrintWriter.print(" mLockScreenTimerActive=");
        paramPrintWriter.println(this.mLockScreenTimerActive);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mEndcallBehavior=");
        paramPrintWriter.print(this.mEndcallBehavior);
        paramPrintWriter.print(" mIncallPowerBehavior=");
        paramPrintWriter.print(this.mIncallPowerBehavior);
        paramPrintWriter.print(" mLongPressOnHomeBehavior=");
        paramPrintWriter.println(this.mLongPressOnHomeBehavior);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mLandscapeRotation=");
        paramPrintWriter.print(this.mLandscapeRotation);
        paramPrintWriter.print(" mSeascapeRotation=");
        paramPrintWriter.println(this.mSeascapeRotation);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mPortraitRotation=");
        paramPrintWriter.print(this.mPortraitRotation);
        paramPrintWriter.print(" mUpsideDownRotation=");
        paramPrintWriter.println(this.mUpsideDownRotation);
    }

    public void enableKeyguard(boolean paramBoolean)
    {
        if (this.mKeyguardMediator != null)
            this.mKeyguardMediator.setKeyguardEnabled(paramBoolean);
    }

    public void enableScreenAfterBoot()
    {
        readLidState();
        applyLidSwitchState();
        updateRotation(true);
    }

    public void exitKeyguardSecurely(WindowManagerPolicy.OnKeyguardExitResult paramOnKeyguardExitResult)
    {
        if (this.mKeyguardMediator != null)
            this.mKeyguardMediator.verifyUnlock(paramOnKeyguardExitResult);
    }

    public int finishAnimationLw()
    {
        int i = 0;
        boolean bool = false;
        WindowManager.LayoutParams localLayoutParams;
        if (this.mTopFullscreenOpaqueWindowState != null)
        {
            localLayoutParams = this.mTopFullscreenOpaqueWindowState.getAttrs();
            if (this.mStatusBar != null)
            {
                if (!this.mForceStatusBar)
                    break label150;
                if (this.mStatusBar.showLw(true))
                    i = 0x0 | 0x1;
            }
            label52: this.mTopIsFullscreen = bool;
            if (this.mKeyguard != null)
            {
                if ((!this.mDismissKeyguard) || (this.mKeyguardMediator.isSecure()))
                    break label244;
                if (this.mKeyguard.hideLw(true))
                    i |= 7;
                if (this.mKeyguardMediator.isShowing())
                    this.mHandler.post(new Runnable()
                    {
                        public void run()
                        {
                            PhoneWindowManager.this.mKeyguardMediator.keyguardDone(false, false);
                        }
                    });
            }
        }
        while (true)
        {
            if ((0x6 & updateSystemUiVisibilityLw()) != 0)
                i |= 1;
            updateLockScreenTimeout();
            return i;
            localLayoutParams = null;
            break;
            label150: if (this.mTopFullscreenOpaqueWindowState == null)
                break label52;
            if (((0x400 & localLayoutParams.flags) != 0) || ((0x4 & this.mLastSystemUiFlags) != 0));
            for (bool = true; ; bool = false)
            {
                if (!bool)
                    break label224;
                if (!this.mStatusBar.hideLw(true))
                    break;
                i = 0x0 | 0x1;
                this.mHandler.post(new Runnable()
                {
                    public void run()
                    {
                        try
                        {
                            IStatusBarService localIStatusBarService = PhoneWindowManager.this.getStatusBarService();
                            if (localIStatusBarService != null)
                                localIStatusBarService.collapse();
                            return;
                        }
                        catch (RemoteException localRemoteException)
                        {
                            while (true)
                                PhoneWindowManager.this.mStatusBarService = null;
                        }
                    }
                });
                break;
            }
            label224: if (!this.mStatusBar.showLw(true))
                break label52;
            i = 0x0 | 0x1;
            break label52;
            label244: if (this.mHideLockScreen)
            {
                if (this.mKeyguard.hideLw(true))
                    i |= 7;
                this.mKeyguardMediator.setHidden(true);
            }
            else
            {
                if (this.mKeyguard.showLw(true))
                    i |= 7;
                this.mKeyguardMediator.setHidden(false);
            }
        }
    }

    public void finishLayoutLw()
    {
    }

    public int focusChangedLw(WindowManagerPolicy.WindowState paramWindowState1, WindowManagerPolicy.WindowState paramWindowState2)
    {
        this.mFocusedWindow = paramWindowState2;
        if ((0x6 & updateSystemUiVisibilityLw()) != 0);
        for (int i = 1; ; i = 0)
            return i;
    }

    public int getConfigDisplayHeight(int paramInt1, int paramInt2, int paramInt3)
    {
        if (!this.mHasSystemNavBar);
        for (int i = getNonDecorDisplayHeight(paramInt1, paramInt2, paramInt3) - this.mStatusBarHeight; ; i = getNonDecorDisplayHeight(paramInt1, paramInt2, paramInt3))
            return i;
    }

    public int getConfigDisplayWidth(int paramInt1, int paramInt2, int paramInt3)
    {
        return getNonDecorDisplayWidth(paramInt1, paramInt2, paramInt3);
    }

    public void getContentInsetHintLw(WindowManager.LayoutParams paramLayoutParams, Rect paramRect)
    {
        int i = paramLayoutParams.flags;
        int j = paramLayoutParams.systemUiVisibility | paramLayoutParams.subtreeSystemUiVisibility;
        int k;
        int m;
        if ((i & 0x10100) == 65792)
            if ((this.mCanHideNavigationBar) && ((j & 0x200) != 0))
            {
                k = this.mUnrestrictedScreenLeft + this.mUnrestrictedScreenWidth;
                m = this.mUnrestrictedScreenTop + this.mUnrestrictedScreenHeight;
                if ((j & 0x100) == 0)
                    break label163;
                if ((i & 0x400) == 0)
                    break label134;
                paramRect.set(this.mStableFullscreenLeft, this.mStableFullscreenTop, k - this.mStableFullscreenRight, m - this.mStableFullscreenBottom);
            }
        while (true)
        {
            return;
            k = this.mRestrictedScreenLeft + this.mRestrictedScreenWidth;
            m = this.mRestrictedScreenTop + this.mRestrictedScreenHeight;
            break;
            label134: paramRect.set(this.mStableLeft, this.mStableTop, k - this.mStableRight, m - this.mStableBottom);
            continue;
            label163: if ((i & 0x400) != 0)
            {
                paramRect.setEmpty();
            }
            else if ((j & 0x404) == 0)
            {
                paramRect.set(this.mCurLeft, this.mCurTop, k - this.mCurRight, m - this.mCurBottom);
            }
            else
            {
                paramRect.set(this.mCurLeft, this.mCurTop, k - this.mCurRight, m - this.mCurBottom);
                continue;
                paramRect.setEmpty();
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    Context getContext()
    {
        return this.mContext;
    }

    public int getMaxWallpaperLayer()
    {
        return 15;
    }

    public int getNonDecorDisplayHeight(int paramInt1, int paramInt2, int paramInt3)
    {
        if (this.mHasSystemNavBar)
            paramInt2 -= this.mNavigationBarHeightForRotation[paramInt3];
        while (true)
        {
            return paramInt2;
            if ((this.mHasNavigationBar) && ((!this.mNavigationBarCanMove) || (paramInt1 < paramInt2)))
                paramInt2 -= this.mNavigationBarHeightForRotation[paramInt3];
        }
    }

    public int getNonDecorDisplayWidth(int paramInt1, int paramInt2, int paramInt3)
    {
        if ((this.mHasNavigationBar) && (this.mNavigationBarCanMove) && (paramInt1 > paramInt2))
            paramInt1 -= this.mNavigationBarWidthForRotation[paramInt3];
        return paramInt1;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    Runnable getPowerLongPress()
    {
        return this.mPowerLongPress;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    Runnable getScreenshotChordLongPress()
    {
        return this.mScreenshotChordLongPress;
    }

    IStatusBarService getStatusBarService()
    {
        synchronized (this.mServiceAquireLock)
        {
            if (this.mStatusBarService == null)
                this.mStatusBarService = IStatusBarService.Stub.asInterface(ServiceManager.getService("statusbar"));
            IStatusBarService localIStatusBarService = this.mStatusBarService;
            return localIStatusBarService;
        }
    }

    public int getSystemDecorRectLw(Rect paramRect)
    {
        paramRect.left = this.mSystemLeft;
        paramRect.top = this.mSystemTop;
        paramRect.right = this.mSystemRight;
        paramRect.bottom = this.mSystemBottom;
        int i;
        if (this.mStatusBar != null)
            i = this.mStatusBar.getSurfaceLayer();
        while (true)
        {
            return i;
            if (this.mNavigationBar != null)
                i = this.mNavigationBar.getSurfaceLayer();
            else
                i = 0;
        }
    }

    boolean goHome()
    {
        boolean bool;
        try
        {
            if (SystemProperties.getInt("persist.sys.uts-test-mode", 0) == 1)
                Log.d("WindowManager", "UTS-TEST-MODE");
            while (ActivityManagerNative.getDefault().startActivity(null, this.mHomeIntent, this.mHomeIntent.resolveTypeIfNeeded(this.mContext.getContentResolver()), null, null, 0, 1, null, null, null) == 1)
            {
                bool = false;
                break label128;
                ActivityManagerNative.getDefault().stopAppSwitches();
                sendCloseSystemWindows();
                Intent localIntent = createHomeDockIntent();
                if (localIntent != null)
                {
                    int i = ActivityManagerNative.getDefault().startActivity(null, localIntent, localIntent.resolveTypeIfNeeded(this.mContext.getContentResolver()), null, null, 0, 1, null, null, null);
                    if (i == 1)
                        bool = false;
                }
            }
        }
        catch (RemoteException localRemoteException)
        {
            bool = true;
        }
        label128: return bool;
    }

    void handleVolumeKey(int paramInt1, int paramInt2)
    {
        IAudioService localIAudioService = getAudioService();
        if (localIAudioService == null);
        while (true)
        {
            return;
            try
            {
                this.mBroadcastWakeLock.acquire();
                if (paramInt2 == 24);
                for (int i = 1; ; i = -1)
                {
                    localIAudioService.adjustStreamVolume(paramInt1, i, 0);
                    this.mBroadcastWakeLock.release();
                    break;
                }
            }
            catch (RemoteException localRemoteException)
            {
                Log.w("WindowManager", "IAudioService.adjustStreamVolume() threw RemoteException " + localRemoteException);
                this.mBroadcastWakeLock.release();
            }
            finally
            {
                this.mBroadcastWakeLock.release();
            }
        }
    }

    public boolean hasNavigationBar()
    {
        return this.mHasNavigationBar;
    }

    public boolean hasSystemNavBar()
    {
        return this.mHasSystemNavBar;
    }

    public void hideBootMessages()
    {
        this.mHandler.post(new Runnable()
        {
            public void run()
            {
                if (PhoneWindowManager.this.mBootMsgDialog != null)
                {
                    PhoneWindowManager.this.mBootMsgDialog.dismiss();
                    PhoneWindowManager.this.mBootMsgDialog = null;
                }
            }
        });
    }

    public boolean inKeyguardRestrictedKeyInputMode()
    {
        if (this.mKeyguardMediator == null);
        for (boolean bool = false; ; bool = this.mKeyguardMediator.isInputRestricted())
            return bool;
    }

    public void init(Context paramContext, IWindowManager paramIWindowManager, WindowManagerPolicy.WindowManagerFuncs paramWindowManagerFuncs, LocalPowerManager paramLocalPowerManager)
    {
        int i = 1;
        this.mContext = paramContext;
        this.mWindowManager = paramIWindowManager;
        this.mWindowManagerFuncs = paramWindowManagerFuncs;
        this.mPowerManager = paramLocalPowerManager;
        this.mHeadless = "1".equals(SystemProperties.get("ro.config.headless", "0"));
        if (!this.mHeadless)
            this.mKeyguardMediator = new MiuiKeyguardViewMediator(paramContext, this, paramLocalPowerManager);
        this.mHandler = new PolicyHandler(null);
        this.mOrientationListener = new MyOrientationListener(this.mContext);
        try
        {
            this.mOrientationListener.setCurrentRotation(paramIWindowManager.getRotation());
            label107: new SettingsObserver(this.mHandler).observe();
            this.mShortcutManager = new ShortcutManager(paramContext, this.mHandler);
            this.mShortcutManager.observe();
            this.mUiMode = paramContext.getResources().getInteger(17694737);
            this.mHomeIntent = new Intent("android.intent.action.MAIN", null);
            this.mHomeIntent.addCategory("android.intent.category.HOME");
            this.mHomeIntent.addFlags(270532608);
            this.mCarDockIntent = new Intent("android.intent.action.MAIN", null);
            this.mCarDockIntent.addCategory("android.intent.category.CAR_DOCK");
            this.mCarDockIntent.addFlags(270532608);
            this.mDeskDockIntent = new Intent("android.intent.action.MAIN", null);
            this.mDeskDockIntent.addCategory("android.intent.category.DESK_DOCK");
            this.mDeskDockIntent.addFlags(270532608);
            this.mBroadcastWakeLock = ((PowerManager)paramContext.getSystemService("power")).newWakeLock(i, "PhoneWindowManager.mBroadcastWakeLock");
            this.mEnableShiftMenuBugReports = "1".equals(SystemProperties.get("ro.debuggable"));
            this.mLidOpenRotation = readRotation(17694734);
            this.mCarDockRotation = readRotation(17694736);
            this.mDeskDockRotation = readRotation(17694735);
            this.mCarDockEnablesAccelerometer = this.mContext.getResources().getBoolean(17891353);
            this.mDeskDockEnablesAccelerometer = this.mContext.getResources().getBoolean(17891352);
            this.mLidKeyboardAccessibility = this.mContext.getResources().getInteger(17694740);
            this.mLidNavigationAccessibility = this.mContext.getResources().getInteger(17694741);
            this.mLidControlsSleep = this.mContext.getResources().getBoolean(17891354);
            IntentFilter localIntentFilter1 = new IntentFilter();
            localIntentFilter1.addAction(UiModeManager.ACTION_ENTER_CAR_MODE);
            localIntentFilter1.addAction(UiModeManager.ACTION_EXIT_CAR_MODE);
            localIntentFilter1.addAction(UiModeManager.ACTION_ENTER_DESK_MODE);
            localIntentFilter1.addAction(UiModeManager.ACTION_EXIT_DESK_MODE);
            localIntentFilter1.addAction("android.intent.action.DOCK_EVENT");
            Intent localIntent1 = paramContext.registerReceiver(this.mDockReceiver, localIntentFilter1);
            if (localIntent1 != null)
                this.mDockMode = localIntent1.getIntExtra("android.intent.extra.DOCK_STATE", 0);
            IntentFilter localIntentFilter2 = new IntentFilter();
            localIntentFilter2.addAction("android.intent.action.BATTERY_CHANGED");
            Intent localIntent2 = paramContext.registerReceiver(this.mPowerReceiver, localIntentFilter2);
            if (localIntent2 != null)
            {
                if (localIntent2.getIntExtra("plugged", 0) != 0)
                    this.mPluggedIn = i;
            }
            else
            {
                this.mVibrator = ((Vibrator)paramContext.getSystemService("vibrator"));
                this.mLongPressVibePattern = getLongIntArray(this.mContext.getResources(), 17236001);
                this.mVirtualKeyVibePattern = getLongIntArray(this.mContext.getResources(), 17236002);
                this.mKeyboardTapVibePattern = getLongIntArray(this.mContext.getResources(), 17236003);
                this.mSafeModeDisabledVibePattern = getLongIntArray(this.mContext.getResources(), 17236004);
                this.mSafeModeEnabledVibePattern = getLongIntArray(this.mContext.getResources(), 17236005);
                this.mScreenshotChordEnabled = this.mContext.getResources().getBoolean(17891349);
                initializeHdmiState();
                if (!this.mPowerManager.isScreenOn())
                    break label701;
                screenTurningOn(null);
            }
            while (true)
            {
                return;
                i = 0;
                break;
                label701: screenTurnedOff(2);
            }
        }
        catch (RemoteException localRemoteException)
        {
            break label107;
        }
    }

    // ERROR //
    void initializeHdmiState()
    {
        // Byte code:
        //     0: iconst_1
        //     1: istore_1
        //     2: iconst_0
        //     3: istore_2
        //     4: new 1932	java/io/File
        //     7: dup
        //     8: ldc_w 1934
        //     11: invokespecial 1935	java/io/File:<init>	(Ljava/lang/String;)V
        //     14: invokevirtual 1938	java/io/File:exists	()Z
        //     17: ifeq +86 -> 103
        //     20: aload_0
        //     21: getfield 508	com/android/internal/policy/impl/PhoneWindowManager:mHDMIObserver	Landroid/os/UEventObserver;
        //     24: ldc_w 1940
        //     27: invokevirtual 1945	android/os/UEventObserver:startObserving	(Ljava/lang/String;)V
        //     30: aconst_null
        //     31: astore 4
        //     33: new 1947	java/io/FileReader
        //     36: dup
        //     37: ldc_w 1949
        //     40: invokespecial 1950	java/io/FileReader:<init>	(Ljava/lang/String;)V
        //     43: astore 5
        //     45: bipush 15
        //     47: newarray char
        //     49: astore 14
        //     51: aload 5
        //     53: aload 14
        //     55: invokevirtual 1954	java/io/FileReader:read	([C)I
        //     58: istore 15
        //     60: iload 15
        //     62: iload_1
        //     63: if_icmple +30 -> 93
        //     66: new 1763	java/lang/String
        //     69: dup
        //     70: aload 14
        //     72: iconst_0
        //     73: iload 15
        //     75: bipush 255
        //     77: iadd
        //     78: invokespecial 1957	java/lang/String:<init>	([CII)V
        //     81: invokestatic 1960	java/lang/Integer:parseInt	(Ljava/lang/String;)I
        //     84: istore 17
        //     86: iload 17
        //     88: ifeq +39 -> 127
        //     91: iload_1
        //     92: istore_2
        //     93: aload 5
        //     95: ifnull +8 -> 103
        //     98: aload 5
        //     100: invokevirtual 1963	java/io/FileReader:close	()V
        //     103: iload_2
        //     104: ifne +137 -> 241
        //     107: iload_1
        //     108: istore_3
        //     109: aload_0
        //     110: iload_3
        //     111: putfield 1237	com/android/internal/policy/impl/PhoneWindowManager:mHdmiPlugged	Z
        //     114: aload_0
        //     115: getfield 1237	com/android/internal/policy/impl/PhoneWindowManager:mHdmiPlugged	Z
        //     118: ifne +128 -> 246
        //     121: aload_0
        //     122: iload_1
        //     123: invokevirtual 1966	com/android/internal/policy/impl/PhoneWindowManager:setHdmiPlugged	(Z)V
        //     126: return
        //     127: iconst_0
        //     128: istore_2
        //     129: goto -36 -> 93
        //     132: astore 6
        //     134: ldc 169
        //     136: new 1080	java/lang/StringBuilder
        //     139: dup
        //     140: invokespecial 1081	java/lang/StringBuilder:<init>	()V
        //     143: ldc_w 1968
        //     146: invokevirtual 1086	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     149: aload 6
        //     151: invokevirtual 1106	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     154: invokevirtual 1090	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     157: invokestatic 1969	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     160: pop
        //     161: aload 4
        //     163: ifnull -60 -> 103
        //     166: aload 4
        //     168: invokevirtual 1963	java/io/FileReader:close	()V
        //     171: goto -68 -> 103
        //     174: astore 10
        //     176: goto -73 -> 103
        //     179: astore 11
        //     181: ldc 169
        //     183: new 1080	java/lang/StringBuilder
        //     186: dup
        //     187: invokespecial 1081	java/lang/StringBuilder:<init>	()V
        //     190: ldc_w 1968
        //     193: invokevirtual 1086	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     196: aload 11
        //     198: invokevirtual 1106	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     201: invokevirtual 1090	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     204: invokestatic 1969	android/util/Slog:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     207: pop
        //     208: aload 4
        //     210: ifnull -107 -> 103
        //     213: aload 4
        //     215: invokevirtual 1963	java/io/FileReader:close	()V
        //     218: goto -115 -> 103
        //     221: astore 13
        //     223: goto -120 -> 103
        //     226: astore 7
        //     228: aload 4
        //     230: ifnull +8 -> 238
        //     233: aload 4
        //     235: invokevirtual 1963	java/io/FileReader:close	()V
        //     238: aload 7
        //     240: athrow
        //     241: iconst_0
        //     242: istore_3
        //     243: goto -134 -> 109
        //     246: iconst_0
        //     247: istore_1
        //     248: goto -127 -> 121
        //     251: astore 16
        //     253: goto -150 -> 103
        //     256: astore 8
        //     258: goto -20 -> 238
        //     261: astore 7
        //     263: aload 5
        //     265: astore 4
        //     267: goto -39 -> 228
        //     270: astore 11
        //     272: aload 5
        //     274: astore 4
        //     276: goto -95 -> 181
        //     279: astore 6
        //     281: aload 5
        //     283: astore 4
        //     285: goto -151 -> 134
        //
        // Exception table:
        //     from	to	target	type
        //     33	45	132	java/io/IOException
        //     166	171	174	java/io/IOException
        //     33	45	179	java/lang/NumberFormatException
        //     213	218	221	java/io/IOException
        //     33	45	226	finally
        //     134	161	226	finally
        //     181	208	226	finally
        //     98	103	251	java/io/IOException
        //     233	238	256	java/io/IOException
        //     45	86	261	finally
        //     45	86	270	java/lang/NumberFormatException
        //     45	86	279	java/io/IOException
    }

    public long interceptKeyBeforeDispatching(WindowManagerPolicy.WindowState paramWindowState, KeyEvent paramKeyEvent, int paramInt)
    {
        boolean bool1 = keyguardOn();
        int i = paramKeyEvent.getKeyCode();
        int j = paramKeyEvent.getRepeatCount();
        int k = paramKeyEvent.getMetaState();
        int m = paramKeyEvent.getFlags();
        int n;
        boolean bool2;
        long l1;
        if (paramKeyEvent.getAction() == 0)
        {
            n = 1;
            bool2 = paramKeyEvent.isCanceled();
            if ((!this.mScreenshotChordEnabled) || ((m & 0x400) != 0))
                break label147;
            if ((!this.mVolumeDownKeyTriggered) || (this.mPowerKeyTriggered))
                break label115;
            long l2 = SystemClock.uptimeMillis();
            long l3 = 150L + this.mVolumeDownKeyTime;
            if (l2 >= l3)
                break label115;
            l1 = l3 - l2;
        }
        label147: label404: label872: label1259: 
        while (true)
        {
            return l1;
            n = 0;
            break;
            label115: if ((i == 25) && (this.mVolumeDownKeyConsumedByScreenshotChord))
            {
                if (n == 0)
                    this.mVolumeDownKeyConsumedByScreenshotChord = false;
                l1 = -1L;
            }
            else if (i == 3)
            {
                if (n == 0)
                {
                    boolean bool3 = this.mHomeLongPressed;
                    this.mHomePressed = false;
                    this.mHomeLongPressed = false;
                    if (!bool3)
                        try
                        {
                            IStatusBarService localIStatusBarService2 = getStatusBarService();
                            if (localIStatusBarService2 != null)
                                localIStatusBarService2.cancelPreloadRecentApps();
                            this.mHomePressed = false;
                            if (!bool2)
                                i7 = 0;
                        }
                        catch (RemoteException localRemoteException2)
                        {
                            while (true)
                            {
                                try
                                {
                                    int i7;
                                    ITelephony localITelephony = getTelephonyService();
                                    if (localITelephony != null)
                                    {
                                        boolean bool4 = localITelephony.isRinging();
                                        i7 = bool4;
                                    }
                                    if (i7 != 0)
                                    {
                                        Log.i("WindowManager", "Ignoring HOME; there's a ringing incoming call.");
                                        l1 = -1L;
                                        break;
                                        localRemoteException2 = localRemoteException2;
                                        Slog.e("WindowManager", "RemoteException when showing recent apps", localRemoteException2);
                                        this.mStatusBarService = null;
                                    }
                                }
                                catch (RemoteException localRemoteException3)
                                {
                                    Log.w("WindowManager", "RemoteException from getPhoneInterface()", localRemoteException3);
                                    continue;
                                    launchHomeFromHotKey();
                                    continue;
                                }
                                Log.i("WindowManager", "Ignoring HOME; event canceled.");
                            }
                        }
                }
                else
                {
                    WindowManager.LayoutParams localLayoutParams;
                    if (paramWindowState != null)
                        localLayoutParams = paramWindowState.getAttrs();
                    while (true)
                        if (localLayoutParams != null)
                        {
                            int i4 = localLayoutParams.type;
                            if ((i4 == 2004) || (i4 == 2009))
                            {
                                l1 = 0L;
                                break;
                                localLayoutParams = null;
                                continue;
                            }
                            int i5 = WINDOW_TYPES_WHERE_HOME_DOESNT_WORK.length;
                            for (int i6 = 0; ; i6++)
                            {
                                if (i6 >= i5)
                                    break label404;
                                if (i4 == WINDOW_TYPES_WHERE_HOME_DOESNT_WORK[i6])
                                {
                                    l1 = -1L;
                                    break;
                                }
                            }
                        }
                    if ((n == 0) || ((!this.mHomePressed) && (this.mLongPressOnHomeBehavior == 2)));
                    try
                    {
                        IStatusBarService localIStatusBarService1 = getStatusBarService();
                        if (localIStatusBarService1 != null)
                            localIStatusBarService1.preloadRecentApps();
                        if (j == 0)
                        {
                            this.mHomePressed = true;
                            l1 = -1L;
                        }
                    }
                    catch (RemoteException localRemoteException1)
                    {
                        while (true)
                        {
                            Slog.e("WindowManager", "RemoteException when preloading recent apps", localRemoteException1);
                            this.mStatusBarService = null;
                            continue;
                            if (((0x80 & paramKeyEvent.getFlags()) != 0) && (!bool1))
                                handleLongPressOnHome();
                        }
                    }
                }
            }
            else if (i == 82)
            {
                if ((n != 0) && (j == 0) && (this.mEnableShiftMenuBugReports) && ((k & 0x1) == 1))
                {
                    Intent localIntent4 = new Intent("android.intent.action.BUG_REPORT");
                    this.mContext.sendOrderedBroadcast(localIntent4, null);
                    l1 = -1L;
                }
            }
            else if (i == 84)
            {
                if (n != 0)
                    if (j == 0)
                    {
                        this.mSearchKeyShortcutPending = true;
                        this.mConsumeSearchKeyUp = false;
                    }
                do
                {
                    l1 = 0L;
                    break;
                    this.mSearchKeyShortcutPending = false;
                }
                while (!this.mConsumeSearchKeyUp);
                this.mConsumeSearchKeyUp = false;
                l1 = -1L;
            }
            else if (i == 187)
            {
                if ((n != 0) && (j == 0) && (!bool1))
                    showOrHideRecentAppsDialog(0);
                l1 = -1L;
            }
            else
            {
                if (i == 219)
                {
                    if (n != 0)
                        if (j == 0)
                            this.mAssistKeyLongPressed = false;
                    while (true)
                    {
                        l1 = -1L;
                        break;
                        if (j == 1)
                        {
                            this.mAssistKeyLongPressed = true;
                            if (!bool1)
                            {
                                launchAssistLongPressAction();
                                continue;
                                if (this.mAssistKeyLongPressed)
                                    this.mAssistKeyLongPressed = false;
                                else if (!bool1)
                                    launchAssistAction();
                            }
                        }
                    }
                }
                if (this.mSearchKeyShortcutPending)
                {
                    KeyCharacterMap localKeyCharacterMap2 = paramKeyEvent.getKeyCharacterMap();
                    if (localKeyCharacterMap2.isPrintingKey(i))
                    {
                        this.mConsumeSearchKeyUp = true;
                        this.mSearchKeyShortcutPending = false;
                        Intent localIntent3;
                        if ((n != 0) && (j == 0) && (!bool1))
                        {
                            localIntent3 = this.mShortcutManager.getIntent(localKeyCharacterMap2, i, k);
                            if (localIntent3 == null)
                                break label872;
                            localIntent3.addFlags(268435456);
                        }
                        while (true)
                        {
                            try
                            {
                                this.mContext.startActivity(localIntent3);
                                l1 = -1L;
                            }
                            catch (ActivityNotFoundException localActivityNotFoundException3)
                            {
                                Slog.w("WindowManager", "Dropping shortcut key combination because the activity to which it is registered was not found: SEARCH+" + KeyEvent.keyCodeToString(i), localActivityNotFoundException3);
                                continue;
                            }
                            Slog.i("WindowManager", "Dropping unregistered shortcut key combination: SEARCH+" + KeyEvent.keyCodeToString(i));
                        }
                    }
                }
                if ((n != 0) && (j == 0) && (!bool1) && ((0x10000 & k) != 0))
                {
                    KeyCharacterMap localKeyCharacterMap1 = paramKeyEvent.getKeyCharacterMap();
                    if (localKeyCharacterMap1.isPrintingKey(i))
                    {
                        Intent localIntent2 = this.mShortcutManager.getIntent(localKeyCharacterMap1, i, 0xFFF8FFFF & k);
                        if (localIntent2 != null)
                        {
                            localIntent2.addFlags(268435456);
                            try
                            {
                                this.mContext.startActivity(localIntent2);
                                l1 = -1L;
                            }
                            catch (ActivityNotFoundException localActivityNotFoundException2)
                            {
                                while (true)
                                    Slog.w("WindowManager", "Dropping shortcut key combination because the activity to which it is registered was not found: META+" + KeyEvent.keyCodeToString(i), localActivityNotFoundException2);
                            }
                        }
                    }
                }
                else if ((n != 0) && (j == 0) && (!bool1))
                {
                    String str = (String)sApplicationLaunchKeyCategories.get(i);
                    if (str != null)
                    {
                        Intent localIntent1 = Intent.makeMainSelectorActivity("android.intent.action.MAIN", str);
                        localIntent1.setFlags(268435456);
                        try
                        {
                            this.mContext.startActivity(localIntent1);
                            l1 = -1L;
                        }
                        catch (ActivityNotFoundException localActivityNotFoundException1)
                        {
                            while (true)
                                Slog.w("WindowManager", "Dropping application launch key because the activity to which it is registered was not found: keyCode=" + i + ", category=" + str, localActivityNotFoundException1);
                        }
                    }
                }
                else if ((n != 0) && (j == 0) && (i == 61))
                {
                    if ((this.mRecentAppsDialogHeldModifiers == 0) && (!bool1))
                    {
                        int i3 = 0xFFFFFF3E & paramKeyEvent.getModifiers();
                        if ((KeyEvent.metaStateHasModifiers(i3, 2)) || (KeyEvent.metaStateHasModifiers(i3, 65536)))
                        {
                            this.mRecentAppsDialogHeldModifiers = i3;
                            showOrHideRecentAppsDialog(1);
                            l1 = -1L;
                        }
                    }
                }
                else
                {
                    int i2;
                    if ((n == 0) && (this.mRecentAppsDialogHeldModifiers != 0) && ((k & this.mRecentAppsDialogHeldModifiers) == 0))
                    {
                        this.mRecentAppsDialogHeldModifiers = 0;
                        if (bool1)
                        {
                            i2 = 2;
                            showOrHideRecentAppsDialog(i2);
                        }
                    }
                    else
                    {
                        if ((n == 0) || (j != 0) || ((i != 204) && ((i != 62) || ((k & 0x7000) == 0))))
                            break label1347;
                        if ((k & 0xC1) == 0)
                            break label1341;
                    }
                    for (int i1 = -1; ; i1 = 1)
                    {
                        this.mWindowManagerFuncs.switchKeyboardLayout(paramKeyEvent.getDeviceId(), i1);
                        l1 = -1L;
                        break;
                        i2 = 3;
                        break label1259;
                    }
                    if ((this.mLanguageSwitchKeyPressed) && (n == 0) && ((i == 204) || (i == 62)))
                    {
                        this.mLanguageSwitchKeyPressed = false;
                        l1 = -1L;
                    }
                    else
                    {
                        l1 = 0L;
                    }
                }
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public int interceptKeyBeforeQueueing(KeyEvent paramKeyEvent, int paramInt, boolean paramBoolean)
    {
        int i;
        boolean bool1;
        int j;
        int k;
        label33: boolean bool2;
        label43: int n;
        if (paramKeyEvent.getAction() == 0)
        {
            i = 1;
            bool1 = paramKeyEvent.isCanceled();
            j = paramKeyEvent.getKeyCode();
            if ((0x1000000 & paramInt) == 0)
                break label62;
            k = 1;
            if (this.mKeyguardMediator != null)
                break label68;
            bool2 = false;
            if (this.mSystemBooted)
                break label96;
            n = 0;
        }
        while (true)
        {
            return n;
            i = 0;
            break;
            label62: k = 0;
            break label33;
            label68: if (paramBoolean)
            {
                bool2 = this.mKeyguardMediator.isShowingAndNotHidden();
                break label43;
            }
            bool2 = this.mKeyguardMediator.isShowing();
            break label43;
            label96: Injector.performReleaseHapticFeedback(this, paramKeyEvent, paramInt);
            if ((i != 0) && ((0x100 & paramInt) != 0) && (paramKeyEvent.getRepeatCount() == 0))
                performHapticFeedbackLw(null, 1, false);
            if (j == 26)
                paramInt |= 1;
            int m;
            label150: label174: ITelephony localITelephony5;
            if ((paramInt & 0x3) != 0)
            {
                m = 1;
                if (((paramBoolean) && (!this.mHeadless)) || ((k != 0) && (m == 0)))
                {
                    n = 1;
                    switch (j)
                    {
                    default:
                        break;
                    case 5:
                        if (i == 0)
                            continue;
                        localITelephony5 = getTelephonyService();
                        if (localITelephony5 == null)
                            continue;
                    case 24:
                    case 25:
                    case 164:
                    case 6:
                    case 26:
                    case 85:
                    case 126:
                    case 127:
                    case 79:
                    case 86:
                    case 87:
                    case 88:
                    case 89:
                    case 90:
                    case 91:
                    case 130:
                    }
                }
            }
            else
            {
                try
                {
                    if (localITelephony5.isRinging())
                    {
                        Log.i("WindowManager", "interceptKeyBeforeQueueing: CALL key-down while ringing: Answer the call!");
                        localITelephony5.answerRingingCall();
                        n &= -2;
                        continue;
                        m = 0;
                        break label150;
                        n = 0;
                        if ((i == 0) || (m == 0))
                            break label174;
                        if (bool2)
                        {
                            KeyguardViewMediator localKeyguardViewMediator = this.mKeyguardMediator;
                            if (this.mDockMode != 0);
                            for (boolean bool9 = true; ; bool9 = false)
                            {
                                localKeyguardViewMediator.onWakeKeyWhenKeyguardShowingTq(j, bool9);
                                break;
                            }
                        }
                        n = 0x0 | 0x2;
                        break label174;
                        label505: ITelephony localITelephony4;
                        if (j == 25)
                        {
                            if (i != 0)
                            {
                                if ((paramBoolean) && (!this.mVolumeDownKeyTriggered) && ((0x400 & paramKeyEvent.getFlags()) == 0))
                                {
                                    this.mVolumeDownKeyTriggered = true;
                                    this.mVolumeDownKeyTime = paramKeyEvent.getDownTime();
                                    this.mVolumeDownKeyConsumedByScreenshotChord = false;
                                    cancelPendingPowerKeyAction();
                                    interceptScreenshotChord();
                                }
                                if (i == 0)
                                    continue;
                                localITelephony4 = getTelephonyService();
                                if (localITelephony4 == null)
                                    break label670;
                            }
                        }
                        else
                        {
                            try
                            {
                                if (localITelephony4.isRinging())
                                {
                                    Log.i("WindowManager", "interceptKeyBeforeQueueing: VOLUME key-down while ringing: Silence ringer!");
                                    localITelephony4.silenceRinger();
                                    n &= -2;
                                    continue;
                                    this.mVolumeDownKeyTriggered = false;
                                    cancelPendingScreenshotChordAction();
                                    break label505;
                                    if (j != 24)
                                        break label505;
                                    if (i != 0)
                                    {
                                        if ((!paramBoolean) || (this.mVolumeUpKeyTriggered) || ((0x400 & paramKeyEvent.getFlags()) != 0))
                                            break label505;
                                        this.mVolumeUpKeyTriggered = true;
                                        cancelPendingPowerKeyAction();
                                        cancelPendingScreenshotChordAction();
                                        break label505;
                                    }
                                    this.mVolumeUpKeyTriggered = false;
                                    cancelPendingScreenshotChordAction();
                                    break label505;
                                }
                                if ((localITelephony4.isOffhook()) && ((n & 0x1) == 0))
                                    handleVolumeKey(0, j);
                            }
                            catch (RemoteException localRemoteException4)
                            {
                                Log.w("WindowManager", "ITelephony threw RemoteException", localRemoteException4);
                            }
                            label670: if ((isMusicActive()) && ((n & 0x1) == 0))
                            {
                                handleVolumeKey(3, j);
                                continue;
                                n &= -2;
                                if (i != 0)
                                {
                                    ITelephony localITelephony3 = getTelephonyService();
                                    int i2 = 0;
                                    if (localITelephony3 != null);
                                    try
                                    {
                                        boolean bool8 = localITelephony3.endCall();
                                        i2 = bool8;
                                        if ((!paramBoolean) || (i2 != 0))
                                        {
                                            bool7 = true;
                                            interceptPowerKeyDown(bool7);
                                        }
                                    }
                                    catch (RemoteException localRemoteException3)
                                    {
                                        while (true)
                                        {
                                            Log.w("WindowManager", "ITelephony threw RemoteException", localRemoteException3);
                                            continue;
                                            boolean bool7 = false;
                                        }
                                    }
                                }
                                else if ((interceptPowerKeyUp(bool1)) && (((0x1 & this.mEndcallBehavior) == 0) || (!goHome())) && ((0x2 & this.mEndcallBehavior) != 0))
                                {
                                    n = 0x4 | n & 0xFFFFFFFD;
                                    continue;
                                    n &= -2;
                                    if (i != 0)
                                    {
                                        if ((paramBoolean) && (!this.mPowerKeyTriggered) && ((0x400 & paramKeyEvent.getFlags()) == 0))
                                        {
                                            this.mPowerKeyTriggered = true;
                                            this.mPowerKeyTime = paramKeyEvent.getDownTime();
                                            interceptScreenshotChord();
                                        }
                                        ITelephony localITelephony2 = getTelephonyService();
                                        int i1 = 0;
                                        if (localITelephony2 != null);
                                        try
                                        {
                                            if (localITelephony2.isRinging())
                                                localITelephony2.silenceRinger();
                                            while (true)
                                            {
                                                if ((paramBoolean) && (i1 == 0) && (!this.mVolumeDownKeyTriggered) && (!this.mVolumeUpKeyTriggered))
                                                    break label988;
                                                bool5 = true;
                                                interceptPowerKeyDown(bool5);
                                                break;
                                                if (((0x2 & this.mIncallPowerBehavior) != 0) && (localITelephony2.isOffhook()))
                                                {
                                                    boolean bool6 = localITelephony2.endCall();
                                                    i1 = bool6;
                                                }
                                            }
                                        }
                                        catch (RemoteException localRemoteException2)
                                        {
                                            while (true)
                                            {
                                                Log.w("WindowManager", "ITelephony threw RemoteException", localRemoteException2);
                                                continue;
                                                label988: boolean bool5 = false;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        this.mPowerKeyTriggered = false;
                                        cancelPendingScreenshotChordAction();
                                        if ((bool1) || (this.mPendingPowerKeyUpCanceled));
                                        for (boolean bool4 = true; ; bool4 = false)
                                        {
                                            if (interceptPowerKeyUp(bool4))
                                                n = 0x4 | n & 0xFFFFFFFD;
                                            this.mPendingPowerKeyUpCanceled = false;
                                            Injector.sendPowerUpBroadcast(this);
                                            break;
                                        }
                                        ITelephony localITelephony1;
                                        if (i != 0)
                                        {
                                            localITelephony1 = getTelephonyService();
                                            if (localITelephony1 == null);
                                        }
                                        try
                                        {
                                            boolean bool3 = localITelephony1.isIdle();
                                            if ((bool3) && ((n & 0x1) == 0))
                                            {
                                                this.mBroadcastWakeLock.acquire();
                                                Message localMessage = this.mHandler.obtainMessage(3, new KeyEvent(paramKeyEvent));
                                                localMessage.setAsynchronous(true);
                                                localMessage.sendToTarget();
                                            }
                                        }
                                        catch (RemoteException localRemoteException1)
                                        {
                                            while (true)
                                                Log.w("WindowManager", "ITelephony threw RemoteException", localRemoteException1);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (RemoteException localRemoteException5)
                {
                    Log.w("WindowManager", "ITelephony threw RemoteException", localRemoteException5);
                }
            }
        }
    }

    public int interceptMotionBeforeQueueingWhenScreenOff(int paramInt)
    {
        int i = 0;
        int j;
        if ((paramInt & 0x3) != 0)
        {
            j = 1;
            if (j != 0)
            {
                if ((this.mKeyguardMediator == null) || (!this.mKeyguardMediator.isShowing()))
                    break label46;
                this.mKeyguardMediator.onWakeMotionWhenKeyguardShowingTq();
            }
        }
        while (true)
        {
            return i;
            j = 0;
            break;
            label46: i = 0x0 | 0x2;
        }
    }

    boolean isDeviceProvisioned()
    {
        boolean bool = false;
        if (Settings.Secure.getInt(this.mContext.getContentResolver(), "device_provisioned", 0) != 0)
            bool = true;
        return bool;
    }

    public boolean isKeyguardLocked()
    {
        return keyguardOn();
    }

    public boolean isKeyguardSecure()
    {
        if (this.mKeyguardMediator == null);
        for (boolean bool = false; ; bool = this.mKeyguardMediator.isSecure())
            return bool;
    }

    boolean isMusicActive()
    {
        AudioManager localAudioManager = (AudioManager)this.mContext.getSystemService("audio");
        if (localAudioManager == null)
            Log.w("WindowManager", "isMusicActive: couldn't get AudioManager reference");
        for (boolean bool = false; ; bool = localAudioManager.isMusicActive())
            return bool;
    }

    public boolean isScreenOnEarly()
    {
        return this.mScreenOnEarly;
    }

    public boolean isScreenOnFully()
    {
        return this.mScreenOnFully;
    }

    public boolean isScreenSaverEnabled()
    {
        if ((this.mScreenSaverFeatureAvailable) && (this.mScreenSaverEnabledByUser) && (this.mScreenSaverMayRun) && (this.mScreenOnEarly) && (this.mPluggedIn));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    boolean keyguardOn()
    {
        if ((keyguardIsShowingTq()) || (inKeyguardRestrictedKeyInputMode()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    void launchHomeFromHotKey()
    {
        if ((this.mKeyguardMediator != null) && (this.mKeyguardMediator.isShowingAndNotHidden()));
        while (true)
        {
            return;
            if ((!this.mHideLockScreen) && (this.mKeyguardMediator.isInputRestricted()))
            {
                this.mKeyguardMediator.verifyUnlock(new WindowManagerPolicy.OnKeyguardExitResult()
                {
                    public void onKeyguardExitResult(boolean paramAnonymousBoolean)
                    {
                        if (paramAnonymousBoolean);
                        try
                        {
                            ActivityManagerNative.getDefault().stopAppSwitches();
                            label12: PhoneWindowManager.this.sendCloseSystemWindows("homekey");
                            PhoneWindowManager.this.startDockOrHome();
                            return;
                        }
                        catch (RemoteException localRemoteException)
                        {
                            break label12;
                        }
                    }
                });
                continue;
            }
            try
            {
                ActivityManagerNative.getDefault().stopAppSwitches();
                label61: sendCloseSystemWindows("homekey");
                startDockOrHome();
            }
            catch (RemoteException localRemoteException)
            {
                break label61;
            }
        }
    }

    public void layoutWindowLw(WindowManagerPolicy.WindowState paramWindowState1, WindowManager.LayoutParams paramLayoutParams, WindowManagerPolicy.WindowState paramWindowState2)
    {
        if ((paramWindowState1 == this.mStatusBar) || (paramWindowState1 == this.mNavigationBar))
            return;
        int i;
        label35: int j;
        int k;
        int m;
        Rect localRect1;
        Rect localRect2;
        Rect localRect3;
        Rect localRect4;
        int n;
        if ((paramWindowState1 == this.mLastInputMethodTargetWindow) && (this.mLastInputMethodWindow != null))
        {
            i = 1;
            if (i != 0)
                offsetInputMethodWindowLw(this.mLastInputMethodWindow);
            j = paramLayoutParams.flags;
            k = paramLayoutParams.softInputMode;
            m = paramWindowState1.getSystemUiVisibility();
            localRect1 = mTmpParentFrame;
            localRect2 = mTmpDisplayFrame;
            localRect3 = mTmpContentFrame;
            localRect4 = mTmpVisibleFrame;
            if ((!this.mHasNavigationBar) || (this.mNavigationBar == null) || (!this.mNavigationBar.isVisibleLw()))
                break label437;
            n = 1;
            label117: if (paramLayoutParams.type != 2011)
                break label443;
            int i50 = this.mDockLeft;
            localRect4.left = i50;
            localRect3.left = i50;
            localRect2.left = i50;
            localRect1.left = i50;
            int i51 = this.mDockTop;
            localRect4.top = i51;
            localRect3.top = i51;
            localRect2.top = i51;
            localRect1.top = i51;
            int i52 = this.mDockRight;
            localRect4.right = i52;
            localRect3.right = i52;
            localRect2.right = i52;
            localRect1.right = i52;
            int i53 = this.mDockBottom;
            localRect4.bottom = i53;
            localRect3.bottom = i53;
            localRect2.bottom = i53;
            localRect1.bottom = i53;
            paramLayoutParams.gravity = 80;
            this.mDockLayer = paramWindowState1.getSurfaceLayer();
        }
        while (true)
        {
            if ((j & 0x200) != 0)
            {
                localRect4.top = -10000;
                localRect4.left = -10000;
                localRect3.top = -10000;
                localRect3.left = -10000;
                localRect2.top = -10000;
                localRect2.left = -10000;
                localRect4.bottom = 10000;
                localRect4.right = 10000;
                localRect3.bottom = 10000;
                localRect3.right = 10000;
                localRect2.bottom = 10000;
                localRect2.right = 10000;
            }
            paramWindowState1.computeFrameLw(localRect1, localRect2, localRect3, localRect4);
            if ((paramLayoutParams.type != 2011) || (paramWindowState1.getGivenInsetsPendingLw()))
                break;
            setLastInputMethodWindowLw(null, null);
            offsetInputMethodWindowLw(paramWindowState1);
            break;
            i = 0;
            break label35;
            label437: n = 0;
            break label117;
            label443: int i1 = k & 0xF0;
            if (((0x10500 & j) == 65792) && ((m & 0x4) == 0))
            {
                if (paramWindowState2 != null)
                {
                    setAttachedWindowFrames(paramWindowState1, j, k, paramWindowState2, true, localRect1, localRect2, localRect3, localRect4);
                }
                else
                {
                    int i38;
                    label527: int i40;
                    label577: int i41;
                    if ((paramLayoutParams.type == 2014) || (paramLayoutParams.type == 2017))
                        if (n != 0)
                        {
                            i38 = this.mDockLeft;
                            localRect2.left = i38;
                            localRect1.left = i38;
                            int i39 = this.mUnrestrictedScreenTop;
                            localRect2.top = i39;
                            localRect1.top = i39;
                            if (n == 0)
                                break label729;
                            i40 = this.mRestrictedScreenLeft + this.mRestrictedScreenWidth;
                            localRect2.right = i40;
                            localRect1.right = i40;
                            if (n == 0)
                                break label743;
                            i41 = this.mRestrictedScreenTop + this.mRestrictedScreenHeight;
                            label607: localRect2.bottom = i41;
                            localRect1.bottom = i41;
                            label621: if (i1 == 16)
                                break label977;
                            localRect3.left = this.mDockLeft;
                            localRect3.top = this.mDockTop;
                            localRect3.right = this.mDockRight;
                        }
                    for (localRect3.bottom = this.mDockBottom; ; localRect3.bottom = this.mContentBottom)
                    {
                        applyStableConstraints(m, j, localRect3);
                        if (i1 == 48)
                            break label1016;
                        localRect4.left = this.mCurLeft;
                        localRect4.top = this.mCurTop;
                        localRect4.right = this.mCurRight;
                        localRect4.bottom = this.mCurBottom;
                        break;
                        i38 = this.mUnrestrictedScreenLeft;
                        break label527;
                        label729: i40 = this.mUnrestrictedScreenLeft + this.mUnrestrictedScreenWidth;
                        break label577;
                        label743: i41 = this.mUnrestrictedScreenTop + this.mUnrestrictedScreenHeight;
                        break label607;
                        if ((this.mCanHideNavigationBar) && ((m & 0x200) != 0) && (paramLayoutParams.type >= 1) && (paramLayoutParams.type <= 1999))
                        {
                            int i46 = this.mUnrestrictedScreenLeft;
                            localRect2.left = i46;
                            localRect1.left = i46;
                            int i47 = this.mUnrestrictedScreenTop;
                            localRect2.top = i47;
                            localRect1.top = i47;
                            int i48 = this.mUnrestrictedScreenLeft + this.mUnrestrictedScreenWidth;
                            localRect2.right = i48;
                            localRect1.right = i48;
                            int i49 = this.mUnrestrictedScreenTop + this.mUnrestrictedScreenHeight;
                            localRect2.bottom = i49;
                            localRect1.bottom = i49;
                            break label621;
                        }
                        int i42 = this.mRestrictedScreenLeft;
                        localRect2.left = i42;
                        localRect1.left = i42;
                        int i43 = this.mRestrictedScreenTop;
                        localRect2.top = i43;
                        localRect1.top = i43;
                        int i44 = this.mRestrictedScreenLeft + this.mRestrictedScreenWidth;
                        localRect2.right = i44;
                        localRect1.right = i44;
                        int i45 = this.mRestrictedScreenTop + this.mRestrictedScreenHeight;
                        localRect2.bottom = i45;
                        localRect1.bottom = i45;
                        break label621;
                        label977: localRect3.left = this.mContentLeft;
                        localRect3.top = this.mContentTop;
                        localRect3.right = this.mContentRight;
                    }
                    label1016: localRect4.set(localRect3);
                }
            }
            else if (((j & 0x100) != 0) || ((m & 0x600) != 0))
            {
                int i2;
                label1075: int i4;
                label1139: int i5;
                if ((paramLayoutParams.type == 2014) || (paramLayoutParams.type == 2017))
                    if (n != 0)
                    {
                        i2 = this.mDockLeft;
                        localRect3.left = i2;
                        localRect2.left = i2;
                        localRect1.left = i2;
                        int i3 = this.mUnrestrictedScreenTop;
                        localRect3.top = i3;
                        localRect2.top = i3;
                        localRect1.top = i3;
                        if (n == 0)
                            break label1262;
                        i4 = this.mRestrictedScreenLeft + this.mRestrictedScreenWidth;
                        localRect3.right = i4;
                        localRect2.right = i4;
                        localRect1.right = i4;
                        if (n == 0)
                            break label1276;
                        i5 = this.mRestrictedScreenTop + this.mRestrictedScreenHeight;
                        label1176: localRect3.bottom = i5;
                        localRect2.bottom = i5;
                        localRect1.bottom = i5;
                    }
                while (true)
                {
                    applyStableConstraints(m, j, localRect3);
                    if (i1 == 48)
                        break label1932;
                    localRect4.left = this.mCurLeft;
                    localRect4.top = this.mCurTop;
                    localRect4.right = this.mCurRight;
                    localRect4.bottom = this.mCurBottom;
                    break;
                    i2 = this.mUnrestrictedScreenLeft;
                    break label1075;
                    label1262: i4 = this.mUnrestrictedScreenLeft + this.mUnrestrictedScreenWidth;
                    break label1139;
                    label1276: i5 = this.mUnrestrictedScreenTop + this.mUnrestrictedScreenHeight;
                    break label1176;
                    if ((paramLayoutParams.type == 2019) || (paramLayoutParams.type == 2024))
                    {
                        int i6 = this.mUnrestrictedScreenLeft;
                        localRect2.left = i6;
                        localRect1.left = i6;
                        int i7 = this.mUnrestrictedScreenTop;
                        localRect2.top = i7;
                        localRect1.top = i7;
                        int i8 = this.mUnrestrictedScreenLeft + this.mUnrestrictedScreenWidth;
                        localRect2.right = i8;
                        localRect1.right = i8;
                        int i9 = this.mUnrestrictedScreenTop + this.mUnrestrictedScreenHeight;
                        localRect2.bottom = i9;
                        localRect1.bottom = i9;
                    }
                    else if (((paramLayoutParams.type == 2015) || (paramLayoutParams.type == 2021)) && ((j & 0x400) != 0))
                    {
                        int i22 = this.mUnrestrictedScreenLeft;
                        localRect2.left = i22;
                        localRect1.left = i22;
                        int i23 = this.mUnrestrictedScreenTop;
                        localRect2.top = i23;
                        localRect1.top = i23;
                        int i24 = this.mUnrestrictedScreenLeft + this.mUnrestrictedScreenWidth;
                        localRect2.right = i24;
                        localRect1.right = i24;
                        int i25 = this.mUnrestrictedScreenTop + this.mUnrestrictedScreenHeight;
                        localRect2.bottom = i25;
                        localRect1.bottom = i25;
                    }
                    else if (paramLayoutParams.type == 2021)
                    {
                        int i18 = this.mUnrestrictedScreenLeft;
                        localRect3.left = i18;
                        localRect2.left = i18;
                        localRect1.left = i18;
                        int i19 = this.mUnrestrictedScreenTop;
                        localRect3.top = i19;
                        localRect2.top = i19;
                        localRect1.top = i19;
                        int i20 = this.mUnrestrictedScreenLeft + this.mUnrestrictedScreenWidth;
                        localRect3.right = i20;
                        localRect2.right = i20;
                        localRect1.right = i20;
                        int i21 = this.mUnrestrictedScreenTop + this.mUnrestrictedScreenHeight;
                        localRect3.bottom = i21;
                        localRect2.bottom = i21;
                        localRect1.bottom = i21;
                    }
                    else if ((this.mCanHideNavigationBar) && ((m & 0x200) != 0) && (paramLayoutParams.type >= 1) && (paramLayoutParams.type <= 1999))
                    {
                        int i14 = this.mUnrestrictedScreenLeft;
                        localRect3.left = i14;
                        localRect2.left = i14;
                        localRect1.left = i14;
                        int i15 = this.mUnrestrictedScreenTop;
                        localRect3.top = i15;
                        localRect2.top = i15;
                        localRect1.top = i15;
                        int i16 = this.mUnrestrictedScreenLeft + this.mUnrestrictedScreenWidth;
                        localRect3.right = i16;
                        localRect2.right = i16;
                        localRect1.right = i16;
                        int i17 = this.mUnrestrictedScreenTop + this.mUnrestrictedScreenHeight;
                        localRect3.bottom = i17;
                        localRect2.bottom = i17;
                        localRect1.bottom = i17;
                    }
                    else
                    {
                        int i10 = this.mRestrictedScreenLeft;
                        localRect3.left = i10;
                        localRect2.left = i10;
                        localRect1.left = i10;
                        int i11 = this.mRestrictedScreenTop;
                        localRect3.top = i11;
                        localRect2.top = i11;
                        localRect1.top = i11;
                        int i12 = this.mRestrictedScreenLeft + this.mRestrictedScreenWidth;
                        localRect3.right = i12;
                        localRect2.right = i12;
                        localRect1.right = i12;
                        int i13 = this.mRestrictedScreenTop + this.mRestrictedScreenHeight;
                        localRect3.bottom = i13;
                        localRect2.bottom = i13;
                        localRect1.bottom = i13;
                    }
                }
                label1932: localRect4.set(localRect3);
            }
            else if (paramWindowState2 != null)
            {
                setAttachedWindowFrames(paramWindowState1, j, i1, paramWindowState2, false, localRect1, localRect2, localRect3, localRect4);
            }
            else if (paramLayoutParams.type == 2014)
            {
                int i34 = this.mRestrictedScreenLeft;
                localRect3.left = i34;
                localRect2.left = i34;
                localRect1.left = i34;
                int i35 = this.mRestrictedScreenTop;
                localRect3.top = i35;
                localRect2.top = i35;
                localRect1.top = i35;
                int i36 = this.mRestrictedScreenLeft + this.mRestrictedScreenWidth;
                localRect3.right = i36;
                localRect2.right = i36;
                localRect1.right = i36;
                int i37 = this.mRestrictedScreenTop + this.mRestrictedScreenHeight;
                localRect3.bottom = i37;
                localRect2.bottom = i37;
                localRect1.bottom = i37;
            }
            else
            {
                localRect1.left = this.mContentLeft;
                localRect1.top = this.mContentTop;
                localRect1.right = this.mContentRight;
                localRect1.bottom = this.mContentBottom;
                int i33;
                if (i1 != 16)
                {
                    int i30 = this.mDockLeft;
                    localRect3.left = i30;
                    localRect2.left = i30;
                    int i31 = this.mDockTop;
                    localRect3.top = i31;
                    localRect2.top = i31;
                    int i32 = this.mDockRight;
                    localRect3.right = i32;
                    localRect2.right = i32;
                    i33 = this.mDockBottom;
                    localRect3.bottom = i33;
                }
                int i29;
                for (localRect2.bottom = i33; ; localRect2.bottom = i29)
                {
                    if (i1 == 48)
                        break label2351;
                    localRect4.left = this.mCurLeft;
                    localRect4.top = this.mCurTop;
                    localRect4.right = this.mCurRight;
                    localRect4.bottom = this.mCurBottom;
                    break;
                    int i26 = this.mContentLeft;
                    localRect3.left = i26;
                    localRect2.left = i26;
                    int i27 = this.mContentTop;
                    localRect3.top = i27;
                    localRect2.top = i27;
                    int i28 = this.mContentRight;
                    localRect3.right = i28;
                    localRect2.right = i28;
                    i29 = this.mContentBottom;
                    localRect3.bottom = i29;
                }
                label2351: localRect4.set(localRect3);
            }
        }
    }

    public void lockNow()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.DEVICE_POWER", null);
        this.mHandler.removeCallbacks(this.mScreenLockTimeout);
        this.mHandler.post(this.mScreenLockTimeout);
    }

    boolean needSensorRunningLp()
    {
        int i = 1;
        if ((this.mCurrentAppOrientation == 4) || (this.mCurrentAppOrientation == 10) || (this.mCurrentAppOrientation == 7) || (this.mCurrentAppOrientation == 6));
        while (true)
        {
            return i;
            if (((!this.mCarDockEnablesAccelerometer) || (this.mDockMode != 2)) && ((!this.mDeskDockEnablesAccelerometer) || ((this.mDockMode != i) && (this.mDockMode != 3) && (this.mDockMode != 4))) && (this.mUserRotationMode == i))
                int j = 0;
        }
    }

    public void notifyLidSwitchChanged(long paramLong, boolean paramBoolean)
    {
        int i = 1;
        if (this.mHeadless);
        while (true)
        {
            return;
            label19: KeyguardViewMediator localKeyguardViewMediator;
            if (paramBoolean)
            {
                int j = i;
                if (j == this.mLidState)
                    break label85;
                this.mLidState = j;
                applyLidSwitchState();
                updateRotation(i);
                if (!paramBoolean)
                    break label111;
                if (!keyguardIsShowingTq())
                    break label93;
                localKeyguardViewMediator = this.mKeyguardMediator;
                if (this.mDockMode == 0)
                    break label87;
            }
            while (true)
            {
                localKeyguardViewMediator.onWakeKeyWhenKeyguardShowingTq(26, i);
                break;
                int k = 0;
                break label19;
                label85: break;
                label87: i = 0;
            }
            label93: this.mPowerManager.userActivity(SystemClock.uptimeMillis(), false, i);
            continue;
            label111: if (!this.mLidControlsSleep)
                this.mPowerManager.userActivity(SystemClock.uptimeMillis(), false, 0);
        }
    }

    public boolean performHapticFeedbackLw(WindowManagerPolicy.WindowState paramWindowState, int paramInt, boolean paramBoolean)
    {
        boolean bool = false;
        int i;
        if (Settings.System.getInt(this.mContext.getContentResolver(), "haptic_feedback_enabled", 0) == 0)
        {
            i = 1;
            if ((paramBoolean) || ((i == 0) && (!this.mKeyguardMediator.isShowingAndNotHidden())))
                break label51;
        }
        while (true)
        {
            return bool;
            i = 0;
            break;
            label51: switch (paramInt)
            {
            default:
            case 0:
            case 1:
            case 3:
            case 10000:
            case 10001:
            }
        }
        long[] arrayOfLong = this.mLongPressVibePattern;
        label113: if (arrayOfLong.length == 1)
            this.mVibrator.vibrate(arrayOfLong[0]);
        while (true)
        {
            bool = true;
            break;
            arrayOfLong = this.mVirtualKeyVibePattern;
            break label113;
            arrayOfLong = this.mKeyboardTapVibePattern;
            break label113;
            arrayOfLong = this.mSafeModeDisabledVibePattern;
            break label113;
            arrayOfLong = this.mSafeModeEnabledVibePattern;
            break label113;
            this.mVibrator.vibrate(arrayOfLong, -1);
        }
    }

    public int prepareAddWindowLw(WindowManagerPolicy.WindowState paramWindowState, WindowManager.LayoutParams paramLayoutParams)
    {
        int i = -7;
        switch (paramLayoutParams.type)
        {
        default:
        case 2000:
        case 2019:
        case 2024:
        case 2014:
        case 2017:
        case 2004:
        }
        while (true)
        {
            i = 0;
            do
            {
                do
                {
                    do
                    {
                        return i;
                        this.mContext.enforceCallingOrSelfPermission("android.permission.STATUS_BAR_SERVICE", "PhoneWindowManager");
                    }
                    while ((this.mStatusBar != null) && (this.mStatusBar.isAlive()));
                    this.mStatusBar = paramWindowState;
                    break;
                    this.mContext.enforceCallingOrSelfPermission("android.permission.STATUS_BAR_SERVICE", "PhoneWindowManager");
                }
                while ((this.mNavigationBar != null) && (this.mNavigationBar.isAlive()));
                this.mNavigationBar = paramWindowState;
                break;
                this.mContext.enforceCallingOrSelfPermission("android.permission.STATUS_BAR_SERVICE", "PhoneWindowManager");
                break;
                this.mContext.enforceCallingOrSelfPermission("android.permission.STATUS_BAR_SERVICE", "PhoneWindowManager");
                break;
                this.mContext.enforceCallingOrSelfPermission("android.permission.STATUS_BAR_SERVICE", "PhoneWindowManager");
                break;
            }
            while (this.mKeyguard != null);
            this.mKeyguard = paramWindowState;
        }
    }

    void readLidState()
    {
        this.mLidState = this.mWindowManagerFuncs.getLidState();
    }

    public void removeStartingWindow(IBinder paramIBinder, View paramView)
    {
        if (paramView != null)
            ((WindowManager)this.mContext.getSystemService("window")).removeView(paramView);
    }

    public void removeWindowLw(WindowManagerPolicy.WindowState paramWindowState)
    {
        if (this.mStatusBar == paramWindowState)
            this.mStatusBar = null;
        while (true)
        {
            return;
            if (this.mKeyguard == paramWindowState)
                this.mKeyguard = null;
            else if (this.mNavigationBar == paramWindowState)
                this.mNavigationBar = null;
        }
    }

    public int rotationForOrientationLw(int paramInt1, int paramInt2)
    {
        while (true)
        {
            int i;
            int j;
            int k;
            synchronized (this.mLock)
            {
                i = this.mOrientationListener.getProposedRotation();
                if (i < 0)
                    i = paramInt2;
                if ((this.mLidState == 1) && (this.mLidOpenRotation >= 0))
                {
                    j = this.mLidOpenRotation;
                    continue;
                    if (j >= 0)
                        continue;
                }
                else
                {
                    if ((this.mDockMode == 2) && ((this.mCarDockEnablesAccelerometer) || (this.mCarDockRotation >= 0)))
                    {
                        if (this.mCarDockEnablesAccelerometer)
                        {
                            j = i;
                            break label535;
                        }
                        j = this.mCarDockRotation;
                        break label535;
                    }
                    if (((this.mDockMode == 1) || (this.mDockMode == 3) || (this.mDockMode == 4)) && ((this.mDeskDockEnablesAccelerometer) || (this.mDeskDockRotation >= 0)))
                    {
                        if (this.mDeskDockEnablesAccelerometer)
                        {
                            j = i;
                            break label538;
                        }
                        j = this.mDeskDockRotation;
                        break label538;
                    }
                    if (this.mHdmiPlugged)
                    {
                        j = this.mHdmiRotation;
                        continue;
                    }
                    if ((this.mUserRotationMode != 0) || ((paramInt1 != 2) && (paramInt1 != -1)))
                        break label541;
                    if (this.mAllowAllRotations < 0)
                    {
                        if (!this.mContext.getResources().getBoolean(17891350))
                            break label574;
                        k = 1;
                        this.mAllowAllRotations = k;
                    }
                    if ((i != 2) || (this.mAllowAllRotations == 1))
                        break label567;
                    if (paramInt1 != 10)
                        break label580;
                    break label567;
                    if ((this.mUserRotationMode != 1) || (paramInt1 == 5))
                        break label586;
                    j = this.mUserRotation;
                    continue;
                    if (!isAnyPortrait(j));
                }
            }
            label535: continue;
            label538: continue;
            label541: if ((paramInt1 != 4) && (paramInt1 != 10) && (paramInt1 != 6))
                if (paramInt1 == 7)
                {
                    continue;
                    label567: j = i;
                    continue;
                    label574: k = 0;
                    continue;
                    label580: j = paramInt2;
                    continue;
                    label586: j = -1;
                }
        }
    }

    public boolean rotationHasCompatibleMetricsLw(int paramInt1, int paramInt2)
    {
        boolean bool;
        switch (paramInt1)
        {
        case 2:
        case 3:
        case 4:
        case 5:
        default:
            bool = true;
        case 1:
        case 7:
        case 9:
        case 0:
        case 6:
        case 8:
        }
        while (true)
        {
            return bool;
            bool = isAnyPortrait(paramInt2);
            continue;
            bool = isLandscapeOrSeascape(paramInt2);
        }
    }

    public void screenOnStartedLw()
    {
        synchronized (this.mLock)
        {
            this.mScreenSaverMayRun = false;
            return;
        }
    }

    public void screenOnStoppedLw()
    {
        if (this.mPowerManager.isScreenOn())
        {
            if ((this.mKeyguardMediator != null) && (!this.mKeyguardMediator.isShowingAndNotHidden()))
            {
                long l = SystemClock.uptimeMillis();
                this.mPowerManager.userActivity(l, false, 0);
            }
            synchronized (this.mLock)
            {
                this.mScreenSaverMayRun = true;
            }
        }
    }

    public void screenTurnedOff(int paramInt)
    {
        EventLog.writeEvent(70000, 0);
        synchronized (this.mLock)
        {
            this.mScreenOnEarly = false;
            this.mScreenOnFully = false;
            if (this.mKeyguardMediator != null)
                this.mKeyguardMediator.onScreenTurnedOff(paramInt);
        }
        synchronized (this.mLock)
        {
            updateOrientationListenerLp();
            updateLockScreenTimeout();
            return;
            localObject2 = finally;
            throw localObject2;
        }
    }

    public void screenTurningOn(final WindowManagerPolicy.ScreenOnListener paramScreenOnListener)
    {
        EventLog.writeEvent(70000, 1);
        if ((paramScreenOnListener == null) || (this.mKeyguardMediator != null));
        try
        {
            this.mWindowManager.setEventDispatching(true);
            label29: this.mKeyguardMediator.onScreenTurnedOn(new KeyguardViewManager.ShowListener()
            {
                public void onShown(IBinder paramAnonymousIBinder)
                {
                    if (paramAnonymousIBinder != null);
                    try
                    {
                        PhoneWindowManager.this.mWindowManager.waitForWindowDrawn(paramAnonymousIBinder, new IRemoteCallback.Stub()
                        {
                            public void sendResult(Bundle paramAnonymous2Bundle)
                            {
                                Slog.i("WindowManager", "Lock screen displayed!");
                                PhoneWindowManager.14.this.val$screenOnListener.onScreenOn();
                                synchronized (PhoneWindowManager.this.mLock)
                                {
                                    PhoneWindowManager.this.mScreenOnFully = true;
                                    return;
                                }
                            }
                        });
                        while (true)
                        {
                            label25: return;
                            Slog.i("WindowManager", "No lock screen!");
                            paramScreenOnListener.onScreenOn();
                            synchronized (PhoneWindowManager.this.mLock)
                            {
                                PhoneWindowManager.this.mScreenOnFully = true;
                            }
                        }
                    }
                    catch (RemoteException localRemoteException)
                    {
                        break label25;
                    }
                }
            });
            synchronized (this.mLock)
            {
                while (true)
                {
                    this.mScreenOnEarly = true;
                    updateOrientationListenerLp();
                    updateLockScreenTimeout();
                    return;
                    if (this.mKeyguardMediator != null)
                        this.mKeyguardMediator.onScreenTurnedOn(null);
                    synchronized (this.mLock)
                    {
                        this.mScreenOnFully = true;
                    }
                }
            }
        }
        catch (RemoteException localRemoteException)
        {
            break label29;
        }
    }

    public int selectAnimationLw(WindowManagerPolicy.WindowState paramWindowState, int paramInt)
    {
        int i;
        if (paramWindowState == this.mStatusBar)
            if ((paramInt == 8194) || (paramInt == 8196))
                i = 17432603;
        while (true)
        {
            return i;
            if ((paramInt == 4097) || (paramInt == 4099))
            {
                i = 17432602;
                continue;
                if (paramWindowState == this.mNavigationBar)
                {
                    if (this.mNavigationBarOnBottom)
                    {
                        if ((paramInt == 8194) || (paramInt == 8196))
                        {
                            i = 17432597;
                            continue;
                        }
                        if ((paramInt != 4097) && (paramInt != 4099))
                            break label148;
                        i = 17432596;
                        continue;
                    }
                    if ((paramInt == 8194) || (paramInt == 8196))
                    {
                        i = 17432601;
                        continue;
                    }
                    if ((paramInt == 4097) || (paramInt == 4099))
                        i = 17432600;
                }
            }
            else
            {
                label148: if ((paramInt == 5) && (paramWindowState.hasAppShownWindows()))
                    i = 17432593;
                else
                    i = 0;
            }
        }
    }

    void sendCloseSystemWindows()
    {
        sendCloseSystemWindows(this.mContext, null);
    }

    void sendCloseSystemWindows(String paramString)
    {
        sendCloseSystemWindows(this.mContext, paramString);
    }

    void setAttachedWindowFrames(WindowManagerPolicy.WindowState paramWindowState1, int paramInt1, int paramInt2, WindowManagerPolicy.WindowState paramWindowState2, boolean paramBoolean, Rect paramRect1, Rect paramRect2, Rect paramRect3, Rect paramRect4)
    {
        if ((paramWindowState1.getSurfaceLayer() > this.mDockLayer) && (paramWindowState2.getSurfaceLayer() < this.mDockLayer))
        {
            int i = this.mDockLeft;
            paramRect4.left = i;
            paramRect3.left = i;
            paramRect2.left = i;
            int j = this.mDockTop;
            paramRect4.top = j;
            paramRect3.top = j;
            paramRect2.top = j;
            int k = this.mDockRight;
            paramRect4.right = k;
            paramRect3.right = k;
            paramRect2.right = k;
            int m = this.mDockBottom;
            paramRect4.bottom = m;
            paramRect3.bottom = m;
            paramRect2.bottom = m;
            if ((paramInt1 & 0x100) == 0)
                paramRect2 = paramWindowState2.getFrameLw();
            paramRect1.set(paramRect2);
            return;
        }
        if (paramInt2 != 16)
            paramRect3.set(paramWindowState2.getDisplayFrameLw());
        while (true)
        {
            if (paramBoolean)
                paramRect3 = paramWindowState2.getDisplayFrameLw();
            paramRect2.set(paramRect3);
            paramRect4.set(paramWindowState2.getVisibleFrameLw());
            break;
            paramRect3.set(paramWindowState2.getContentFrameLw());
            if (paramWindowState2.getSurfaceLayer() < this.mDockLayer)
            {
                if (paramRect3.left < this.mContentLeft)
                    paramRect3.left = this.mContentLeft;
                if (paramRect3.top < this.mContentTop)
                    paramRect3.top = this.mContentTop;
                if (paramRect3.right > this.mContentRight)
                    paramRect3.right = this.mContentRight;
                if (paramRect3.bottom > this.mContentBottom)
                    paramRect3.bottom = this.mContentBottom;
            }
        }
    }

    public void setCurrentOrientationLw(int paramInt)
    {
        synchronized (this.mLock)
        {
            if (paramInt != this.mCurrentAppOrientation)
            {
                this.mCurrentAppOrientation = paramInt;
                updateOrientationListenerLp();
            }
            return;
        }
    }

    void setHdmiPlugged(boolean paramBoolean)
    {
        if (this.mHdmiPlugged != paramBoolean)
        {
            this.mHdmiPlugged = paramBoolean;
            if ((paramBoolean) && (this.mDisplay != null))
            {
                this.mExternalDisplayWidth = this.mDisplay.getRawExternalWidth();
                this.mExternalDisplayHeight = this.mDisplay.getRawExternalHeight();
            }
            updateRotation(true, true);
            Intent localIntent = new Intent("android.intent.action.HDMI_PLUGGED");
            localIntent.addFlags(134217728);
            localIntent.putExtra("state", paramBoolean);
            this.mContext.sendStickyBroadcast(localIntent);
        }
    }

    public void setInitialDisplaySize(Display paramDisplay, int paramInt1, int paramInt2)
    {
        this.mDisplay = paramDisplay;
        int i;
        int j;
        int i9;
        label318: String str;
        label377: boolean bool;
        if (paramInt1 > paramInt2)
        {
            i = paramInt2;
            j = paramInt1;
            this.mLandscapeRotation = 0;
            this.mSeascapeRotation = 2;
            if (this.mContext.getResources().getBoolean(17891351))
            {
                this.mPortraitRotation = 1;
                this.mUpsideDownRotation = 3;
                this.mExternalDisplayWidth = this.mDisplay.getRawExternalWidth();
                this.mExternalDisplayHeight = this.mDisplay.getRawExternalHeight();
                this.mStatusBarHeight = this.mContext.getResources().getDimensionPixelSize(17104906);
                int[] arrayOfInt1 = this.mNavigationBarHeightForRotation;
                int k = this.mPortraitRotation;
                int[] arrayOfInt2 = this.mNavigationBarHeightForRotation;
                int m = this.mUpsideDownRotation;
                int n = this.mContext.getResources().getDimensionPixelSize(17104907);
                arrayOfInt2[m] = n;
                arrayOfInt1[k] = n;
                int[] arrayOfInt3 = this.mNavigationBarHeightForRotation;
                int i1 = this.mLandscapeRotation;
                int[] arrayOfInt4 = this.mNavigationBarHeightForRotation;
                int i2 = this.mSeascapeRotation;
                int i3 = this.mContext.getResources().getDimensionPixelSize(17104908);
                arrayOfInt4[i2] = i3;
                arrayOfInt3[i1] = i3;
                int[] arrayOfInt5 = this.mNavigationBarWidthForRotation;
                int i4 = this.mPortraitRotation;
                int[] arrayOfInt6 = this.mNavigationBarWidthForRotation;
                int i5 = this.mUpsideDownRotation;
                int[] arrayOfInt7 = this.mNavigationBarWidthForRotation;
                int i6 = this.mLandscapeRotation;
                int[] arrayOfInt8 = this.mNavigationBarWidthForRotation;
                int i7 = this.mSeascapeRotation;
                int i8 = this.mContext.getResources().getDimensionPixelSize(17104909);
                arrayOfInt8[i7] = i8;
                arrayOfInt7[i6] = i8;
                arrayOfInt6[i5] = i8;
                arrayOfInt5[i4] = i8;
                i9 = i * 160 / DisplayMetrics.DENSITY_DEVICE;
                if (i9 >= 600)
                    break label531;
                this.mHasSystemNavBar = false;
                this.mNavigationBarCanMove = true;
                if (this.mHasSystemNavBar)
                    break label584;
                this.mHasNavigationBar = this.mContext.getResources().getBoolean(17891380);
                str = SystemProperties.get("qemu.hw.mainkeys");
                if (!"".equals(str))
                {
                    if (!str.equals("1"))
                        break label565;
                    this.mHasNavigationBar = false;
                }
                if (!this.mHasSystemNavBar)
                    break label598;
                int i10 = j * 160 / DisplayMetrics.DENSITY_DEVICE;
                if (16 * (i9 - 160 * this.mNavigationBarHeightForRotation[this.mLandscapeRotation] / DisplayMetrics.DENSITY_DEVICE) / i10 >= 9)
                    break label592;
                bool = true;
                label430: this.mCanHideNavigationBar = bool;
                label436: if (!"portrait".equals(SystemProperties.get("persist.demo.hdmirotation")))
                    break label621;
            }
        }
        label531: label565: label584: label592: label598: label621: for (this.mHdmiRotation = this.mPortraitRotation; ; this.mHdmiRotation = this.mLandscapeRotation)
        {
            return;
            this.mPortraitRotation = 3;
            this.mUpsideDownRotation = 1;
            break;
            i = paramInt1;
            j = paramInt2;
            this.mPortraitRotation = 0;
            this.mUpsideDownRotation = 2;
            if (this.mContext.getResources().getBoolean(17891351))
            {
                this.mLandscapeRotation = 3;
                this.mSeascapeRotation = 1;
                break;
            }
            this.mLandscapeRotation = 1;
            this.mSeascapeRotation = 3;
            break;
            if (i9 < 720)
            {
                this.mHasSystemNavBar = false;
                this.mNavigationBarCanMove = false;
                break label318;
            }
            this.mHasSystemNavBar = true;
            this.mNavigationBarCanMove = false;
            break label318;
            if (!str.equals("0"))
                break label377;
            this.mHasNavigationBar = true;
            break label377;
            this.mHasNavigationBar = false;
            break label377;
            bool = false;
            break label430;
            if (this.mHasNavigationBar)
            {
                this.mCanHideNavigationBar = true;
                break label436;
            }
            this.mCanHideNavigationBar = false;
            break label436;
        }
    }

    public void setLastInputMethodWindowLw(WindowManagerPolicy.WindowState paramWindowState1, WindowManagerPolicy.WindowState paramWindowState2)
    {
        this.mLastInputMethodWindow = paramWindowState1;
        this.mLastInputMethodTargetWindow = paramWindowState2;
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_METHOD)
    void setPowerLongPress(Runnable paramRunnable)
    {
        this.mPowerLongPress = paramRunnable;
    }

    public void setRotationLw(int paramInt)
    {
        this.mOrientationListener.setCurrentRotation(paramInt);
    }

    public void setSafeMode(boolean paramBoolean)
    {
        this.mSafeMode = paramBoolean;
        if (paramBoolean);
        for (int i = 10001; ; i = 10000)
        {
            performHapticFeedbackLw(null, i, true);
            return;
        }
    }

    public void setUserRotationMode(int paramInt1, int paramInt2)
    {
        ContentResolver localContentResolver = this.mContext.getContentResolver();
        if (paramInt1 == 1)
        {
            Settings.System.putInt(localContentResolver, "user_rotation", paramInt2);
            Settings.System.putInt(localContentResolver, "accelerometer_rotation", 0);
        }
        while (true)
        {
            return;
            Settings.System.putInt(localContentResolver, "accelerometer_rotation", 1);
        }
    }

    public void showBootMessage(final CharSequence paramCharSequence, boolean paramBoolean)
    {
        if (this.mHeadless);
        while (true)
        {
            return;
            this.mHandler.post(new Runnable()
            {
                @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
                public void run()
                {
                    if (PhoneWindowManager.this.mBootMsgDialog == null)
                    {
                        PhoneWindowManager.this.mBootMsgDialog = new ProgressDialog(PhoneWindowManager.this.mContext, 3)
                        {
                            public boolean dispatchGenericMotionEvent(MotionEvent paramAnonymous2MotionEvent)
                            {
                                return true;
                            }

                            public boolean dispatchKeyEvent(KeyEvent paramAnonymous2KeyEvent)
                            {
                                return true;
                            }

                            public boolean dispatchKeyShortcutEvent(KeyEvent paramAnonymous2KeyEvent)
                            {
                                return true;
                            }

                            public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAnonymous2AccessibilityEvent)
                            {
                                return true;
                            }

                            public boolean dispatchTouchEvent(MotionEvent paramAnonymous2MotionEvent)
                            {
                                return true;
                            }

                            public boolean dispatchTrackballEvent(MotionEvent paramAnonymous2MotionEvent)
                            {
                                return true;
                            }
                        };
                        PhoneWindowManager.this.mBootMsgDialog.setTitle(17040355);
                        PhoneWindowManager.this.mBootMsgDialog.setProgressStyle(0);
                        PhoneWindowManager.this.mBootMsgDialog.setIndeterminate(true);
                        PhoneWindowManager.this.mBootMsgDialog.getWindow().setType(2021);
                        PhoneWindowManager.this.mBootMsgDialog.getWindow().addFlags(258);
                        PhoneWindowManager.this.mBootMsgDialog.getWindow().setDimAmount(1.0F);
                        WindowManager.LayoutParams localLayoutParams = PhoneWindowManager.this.mBootMsgDialog.getWindow().getAttributes();
                        localLayoutParams.screenOrientation = 5;
                        PhoneWindowManager.this.mBootMsgDialog.getWindow().setAttributes(localLayoutParams);
                        PhoneWindowManager.this.mBootMsgDialog.setCancelable(false);
                        PhoneWindowManager.this.mBootMsgDialog.show();
                    }
                    PhoneWindowManager.this.mBootMsgDialog.setMessage(paramCharSequence);
                }
            });
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    void showGlobalActionsDialog()
    {
        if (this.mGlobalActions == null)
            this.mGlobalActions = new MiuiGlobalActions(this.mContext, this.mWindowManagerFuncs);
        boolean bool = keyguardIsShowingTq();
        this.mGlobalActions.showDialog(bool, isDeviceProvisioned());
        if (bool)
            this.mKeyguardMediator.pokeWakelock();
    }

    void showOrHideRecentAppsDialog(final int paramInt)
    {
        this.mHandler.post(new Runnable()
        {
            public void run()
            {
                if (PhoneWindowManager.this.mRecentAppsDialog == null)
                    PhoneWindowManager.this.mRecentAppsDialog = new RecentApplicationsDialog(PhoneWindowManager.this.mContext);
                if (PhoneWindowManager.this.mRecentAppsDialog.isShowing())
                    switch (paramInt)
                    {
                    case 1:
                    default:
                    case 0:
                    case 2:
                    case 3:
                    }
                while (true)
                {
                    return;
                    PhoneWindowManager.this.mRecentAppsDialog.dismiss();
                    continue;
                    PhoneWindowManager.this.mRecentAppsDialog.dismissAndSwitch();
                    continue;
                    switch (paramInt)
                    {
                    default:
                        break;
                    case 0:
                        PhoneWindowManager.this.mRecentAppsDialog.show();
                        break;
                    case 1:
                        try
                        {
                            PhoneWindowManager.this.mWindowManager.setInTouchMode(false);
                            label161: PhoneWindowManager.this.mRecentAppsDialog.show();
                        }
                        catch (RemoteException localRemoteException)
                        {
                            break label161;
                        }
                    }
                }
            }
        });
    }

    void startDockOrHome()
    {
        Intent localIntent = createHomeDockIntent();
        if (localIntent != null);
        while (true)
        {
            try
            {
                this.mContext.startActivity(localIntent);
                return;
            }
            catch (ActivityNotFoundException localActivityNotFoundException)
            {
            }
            this.mContext.startActivity(this.mHomeIntent);
        }
    }

    public boolean startScreenSaver()
    {
        boolean bool = false;
        label52: 
        while (true)
        {
            IDreamManager localIDreamManager;
            synchronized (this.mLock)
            {
                if (!isScreenSaverEnabled())
                    break label52;
                localIDreamManager = getDreamManager();
                if (localIDreamManager == null)
                    return bool;
            }
            try
            {
                localIDreamManager.dream();
                bool = true;
                continue;
                localObject2 = finally;
                throw localObject2;
            }
            catch (RemoteException localRemoteException)
            {
            }
        }
    }

    public void stopScreenSaver()
    {
        while (true)
        {
            IDreamManager localIDreamManager;
            synchronized (this.mLock)
            {
                localIDreamManager = getDreamManager();
                if (localIDreamManager == null)
                    return;
            }
            try
            {
                localIDreamManager.awaken();
                label25: continue;
                localObject2 = finally;
                throw localObject2;
            }
            catch (RemoteException localRemoteException)
            {
                break label25;
            }
        }
    }

    public int subWindowTypeToLayerLw(int paramInt)
    {
        int i;
        switch (paramInt)
        {
        default:
            Log.e("WindowManager", "Unknown sub-window type: " + paramInt);
            i = 0;
        case 1000:
        case 1003:
        case 1001:
        case 1004:
        case 1002:
        }
        while (true)
        {
            return i;
            i = 1;
            continue;
            i = -2;
            continue;
            i = -1;
            continue;
            i = 2;
        }
    }

    public void systemBooted()
    {
        synchronized (this.mLock)
        {
            this.mSystemBooted = true;
            return;
        }
    }

    public void systemReady()
    {
        if (this.mKeyguardMediator != null)
            this.mKeyguardMediator.onSystemReady();
        synchronized (this.mLock)
        {
            updateOrientationListenerLp();
            this.mSystemReady = true;
            this.mHandler.post(new Runnable()
            {
                public void run()
                {
                    PhoneWindowManager.this.updateSettings();
                }
            });
            return;
        }
    }

    void updateOrientationListenerLp()
    {
        if (!this.mOrientationListener.canDetectOrientation());
        while (true)
        {
            return;
            int i = 1;
            if ((this.mScreenOnEarly) && (needSensorRunningLp()))
            {
                i = 0;
                if (!this.mOrientationSensorEnabled)
                {
                    this.mOrientationListener.enable();
                    this.mOrientationSensorEnabled = true;
                }
            }
            if ((i != 0) && (this.mOrientationSensorEnabled))
            {
                this.mOrientationListener.disable();
                this.mOrientationSensorEnabled = false;
            }
        }
    }

    void updateRotation(boolean paramBoolean)
    {
        try
        {
            this.mWindowManager.updateRotation(paramBoolean, false);
            label11: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label11;
        }
    }

    void updateRotation(boolean paramBoolean1, boolean paramBoolean2)
    {
        try
        {
            this.mWindowManager.updateRotation(paramBoolean1, paramBoolean2);
            label11: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label11;
        }
    }

    public void updateSettings()
    {
        int i = 2;
        boolean bool1 = false;
        ContentResolver localContentResolver = this.mContext.getContentResolver();
        int j = 0;
        synchronized (this.mLock)
        {
            this.mEndcallBehavior = Settings.System.getInt(localContentResolver, "end_button_behavior", 2);
            this.mIncallPowerBehavior = Settings.Secure.getInt(localContentResolver, "incall_power_button_behavior", 1);
            int k = Settings.System.getInt(localContentResolver, "user_rotation", 0);
            if (this.mUserRotation != k)
            {
                this.mUserRotation = k;
                j = 1;
            }
            if (Settings.System.getInt(localContentResolver, "accelerometer_rotation", 0) != 0);
            for (int m = 0; ; m = 1)
            {
                if (this.mUserRotationMode != m)
                {
                    this.mUserRotationMode = m;
                    j = 1;
                    updateOrientationListenerLp();
                }
                if (this.mSystemReady)
                {
                    int n = Settings.System.getInt(localContentResolver, "pointer_location", 0);
                    if (this.mPointerLocationMode != n)
                    {
                        this.mPointerLocationMode = n;
                        Handler localHandler = this.mHandler;
                        if (n != 0)
                            i = 1;
                        localHandler.sendEmptyMessage(i);
                    }
                }
                this.mLockScreenTimeout = Settings.System.getInt(localContentResolver, "screen_off_timeout", 0);
                String str = Settings.Secure.getString(localContentResolver, "default_input_method");
                if ((str == null) || (str.length() <= 0))
                    break;
                bool2 = true;
                if (this.mHasSoftInput != bool2)
                {
                    this.mHasSoftInput = bool2;
                    j = 1;
                }
                this.mScreenSaverFeatureAvailable = this.mContext.getResources().getBoolean(17891384);
                if (Settings.Secure.getInt(localContentResolver, "screensaver_enabled", 1) != 0)
                    bool1 = true;
                this.mScreenSaverEnabledByUser = bool1;
                this.mScreenSaverTimeout = Settings.System.getInt(localContentResolver, "screen_off_timeout", 0);
                if (this.mScreenSaverTimeout > 0)
                    this.mScreenSaverTimeout = (-5000 + this.mScreenSaverTimeout);
                if (j != 0)
                    updateRotation(true);
                return;
            }
            boolean bool2 = false;
        }
    }

    public void userActivity()
    {
        synchronized (this.mScreenLockTimeout)
        {
            if (this.mLockScreenTimerActive)
            {
                this.mHandler.removeCallbacks(this.mScreenLockTimeout);
                this.mHandler.postDelayed(this.mScreenLockTimeout, this.mLockScreenTimeout);
            }
            return;
        }
    }

    public int windowTypeToLayerLw(int paramInt)
    {
        int i = 2;
        if ((paramInt >= 1) && (paramInt <= 99));
        while (true)
        {
            return i;
            switch (paramInt)
            {
            case 2013:
            default:
                Log.e("WindowManager", "Unknown window type: " + paramInt);
                break;
            case 2000:
                i = 15;
                break;
            case 2014:
                i = 16;
                break;
            case 2017:
                i = 14;
                break;
            case 2008:
                i = 5;
                break;
            case 2001:
                i = 4;
                break;
            case 2002:
                i = 3;
                break;
            case 2004:
                i = 11;
                break;
            case 2009:
                i = 12;
                break;
            case 2003:
                i = 8;
                break;
            case 2010:
                i = 21;
                break;
            case 2011:
                i = 9;
                break;
            case 2012:
                i = 10;
                break;
            case 2020:
                i = 17;
                break;
            case 2006:
                i = 18;
                break;
            case 2015:
                i = 23;
                break;
            case 2007:
                i = 7;
                break;
            case 2005:
                i = 6;
                break;
            case 2016:
                i = 22;
                break;
            case 2018:
                i = 25;
                break;
            case 2019:
                i = 19;
                break;
            case 2024:
                i = 20;
                break;
            case 2021:
                i = 24;
                break;
            case 2022:
                i = 26;
                break;
            case 2023:
                i = 13;
            }
        }
    }

    final class HideNavInputEventReceiver extends InputEventReceiver
    {
        public HideNavInputEventReceiver(InputChannel paramLooper, Looper arg3)
        {
            super(localLooper);
        }

        public void onInputEvent(InputEvent paramInputEvent)
        {
            try
            {
                int i;
                if (((paramInputEvent instanceof MotionEvent)) && ((0x2 & paramInputEvent.getSource()) != 0) && (((MotionEvent)paramInputEvent).getAction() == 0))
                    i = 0;
                synchronized (PhoneWindowManager.this.mLock)
                {
                    int j = 0x4 | (0x1 | (0x2 | PhoneWindowManager.this.mResettingSystemUiFlags));
                    if (PhoneWindowManager.this.mResettingSystemUiFlags != j)
                    {
                        PhoneWindowManager.this.mResettingSystemUiFlags = j;
                        i = 1;
                    }
                    int k = 0x2 | PhoneWindowManager.this.mForceClearedSystemUiFlags;
                    if (PhoneWindowManager.this.mForceClearedSystemUiFlags != k)
                    {
                        PhoneWindowManager.this.mForceClearedSystemUiFlags = k;
                        i = 1;
                        PhoneWindowManager.this.mHandler.postDelayed(new Runnable()
                        {
                            public void run()
                            {
                                synchronized (PhoneWindowManager.this.mLock)
                                {
                                    PhoneWindowManager localPhoneWindowManager = PhoneWindowManager.this;
                                    localPhoneWindowManager.mForceClearedSystemUiFlags = (0xFFFFFFFD & localPhoneWindowManager.mForceClearedSystemUiFlags);
                                    PhoneWindowManager.this.mWindowManagerFuncs.reevaluateStatusBarVisibility();
                                    return;
                                }
                            }
                        }
                        , 1000L);
                    }
                    if (i != 0)
                        PhoneWindowManager.this.mWindowManagerFuncs.reevaluateStatusBarVisibility();
                    return;
                }
            }
            finally
            {
                finishInputEvent(paramInputEvent, false);
            }
        }
    }

    class MyOrientationListener extends WindowOrientationListener
    {
        MyOrientationListener(Context arg2)
        {
            super();
        }

        public void onProposedRotationChanged(int paramInt)
        {
            PhoneWindowManager.this.updateRotation(false);
        }
    }

    class SettingsObserver extends ContentObserver
    {
        SettingsObserver(Handler arg2)
        {
            super();
        }

        void observe()
        {
            ContentResolver localContentResolver = PhoneWindowManager.this.mContext.getContentResolver();
            localContentResolver.registerContentObserver(Settings.System.getUriFor("end_button_behavior"), false, this);
            localContentResolver.registerContentObserver(Settings.Secure.getUriFor("incall_power_button_behavior"), false, this);
            localContentResolver.registerContentObserver(Settings.System.getUriFor("accelerometer_rotation"), false, this);
            localContentResolver.registerContentObserver(Settings.System.getUriFor("user_rotation"), false, this);
            localContentResolver.registerContentObserver(Settings.System.getUriFor("screen_off_timeout"), false, this);
            localContentResolver.registerContentObserver(Settings.System.getUriFor("pointer_location"), false, this);
            localContentResolver.registerContentObserver(Settings.Secure.getUriFor("default_input_method"), false, this);
            localContentResolver.registerContentObserver(Settings.System.getUriFor("fancy_rotation_anim"), false, this);
            localContentResolver.registerContentObserver(Settings.Secure.getUriFor("screensaver_enabled"), false, this);
            PhoneWindowManager.this.updateSettings();
        }

        public void onChange(boolean paramBoolean)
        {
            PhoneWindowManager.this.updateSettings();
            PhoneWindowManager.this.updateRotation(false);
        }
    }

    private class PolicyHandler extends Handler
    {
        private PolicyHandler()
        {
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
            case 1:
            case 2:
            case 3:
            case 4:
            }
            while (true)
            {
                return;
                PhoneWindowManager.this.enablePointerLocation();
                continue;
                PhoneWindowManager.this.disablePointerLocation();
                continue;
                PhoneWindowManager.this.dispatchMediaKeyWithWakeLock((KeyEvent)paramMessage.obj);
                continue;
                PhoneWindowManager.this.dispatchMediaKeyRepeatWithWakeLock((KeyEvent)paramMessage.obj);
            }
        }
    }

    private static final class PointerLocationInputEventReceiver extends InputEventReceiver
    {
        private final PointerLocationView mView;

        public PointerLocationInputEventReceiver(InputChannel paramInputChannel, Looper paramLooper, PointerLocationView paramPointerLocationView)
        {
            super(paramLooper);
            this.mView = paramPointerLocationView;
        }

        public void onInputEvent(InputEvent paramInputEvent)
        {
            boolean bool = false;
            try
            {
                if (((paramInputEvent instanceof MotionEvent)) && ((0x2 & paramInputEvent.getSource()) != 0))
                {
                    MotionEvent localMotionEvent = (MotionEvent)paramInputEvent;
                    this.mView.addPointerEvent(localMotionEvent);
                    bool = true;
                }
                finishInputEvent(paramInputEvent, bool);
                return;
            }
            finally
            {
                finishInputEvent(paramInputEvent, false);
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static boolean getNeedsMenuLw(WindowManagerPolicy.WindowState paramWindowState1, WindowManagerPolicy.WindowState paramWindowState2)
        {
            return true;
        }

        static void performReleaseHapticFeedback(PhoneWindowManager paramPhoneWindowManager, KeyEvent paramKeyEvent, int paramInt)
        {
            if (paramKeyEvent.getAction() == 0);
            for (int i = 1; ; i = 0)
            {
                if ((i == 0) && ((paramInt & 0x100) != 0) && (paramKeyEvent.getRepeatCount() == 0))
                    paramPhoneWindowManager.performHapticFeedbackLw(null, 2, false);
                return;
            }
        }

        static void sendPowerUpBroadcast(PhoneWindowManager paramPhoneWindowManager)
        {
            if (paramPhoneWindowManager.getContext() != null)
                paramPhoneWindowManager.getContext().sendBroadcast(new Intent("android.intent.action.KEYCODE_POWER_UP"));
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.PhoneWindowManager
 * JD-Core Version:        0.6.2
 */