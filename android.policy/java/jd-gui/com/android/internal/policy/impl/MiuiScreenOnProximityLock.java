package com.android.internal.policy.impl;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager.LayoutParams;

public class MiuiScreenOnProximityLock
{
    private static final boolean DEBUG = true;
    private static final int EVENT_FAR_AWAY = 2;
    private static final int EVENT_NO_USER_ACTIVITY = 4;
    private static final int EVENT_RELEASE = 3;
    private static final int EVENT_TOO_CLOSE = 1;
    private static final int FIRST_CHANGE_TIMEOUT = 1000;
    private static final String LOG_TAG = "MiuiScreenOnProximityLock";
    private static final float PROXIMITY_THRESHOLD = 4.0F;
    private static final int RELEASE_DELAY = 300;
    private static int sValidChangeDelay;
    private Context mContext;
    private Dialog mDialog;
    private SparseArray<Boolean> mDownRecieved = new SparseArray();
    private Handler mHandler;
    private boolean mHeld;
    private boolean mIsFirstChange;
    private KeyguardViewMediator mKeyguardMediator;
    private Sensor mSensor;
    private MySensorEventListener mSensorEventListener = new MySensorEventListener(null);
    private SensorManager mSensorManager;

    public MiuiScreenOnProximityLock(Context paramContext, KeyguardViewMediator paramKeyguardViewMediator)
    {
        this.mContext = paramContext;
        sValidChangeDelay = this.mContext.getResources().getInteger(101187596);
        this.mKeyguardMediator = paramKeyguardViewMediator;
        this.mSensorManager = ((SensorManager)this.mContext.getSystemService("sensor"));
        this.mSensor = this.mSensorManager.getDefaultSensor(8);
        this.mHandler = new Handler(this.mContext.getMainLooper())
        {
            // ERROR //
            public void handleMessage(android.os.Message paramAnonymousMessage)
            {
                // Byte code:
                //     0: aload_0
                //     1: getfield 14	com/android/internal/policy/impl/MiuiScreenOnProximityLock$1:this$0	Lcom/android/internal/policy/impl/MiuiScreenOnProximityLock;
                //     4: astore_2
                //     5: aload_2
                //     6: monitorenter
                //     7: aload_1
                //     8: getfield 25	android/os/Message:what	I
                //     11: tableswitch	default:+25 -> 36, 1:+28->39, 2:+97->108, 3:+166->177
                //     37: monitorexit
                //     38: return
                //     39: aload_0
                //     40: getfield 14	com/android/internal/policy/impl/MiuiScreenOnProximityLock$1:this$0	Lcom/android/internal/policy/impl/MiuiScreenOnProximityLock;
                //     43: invokevirtual 29	com/android/internal/policy/impl/MiuiScreenOnProximityLock:isHeld	()Z
                //     46: ifeq -10 -> 36
                //     49: ldc 31
                //     51: ldc 33
                //     53: invokestatic 39	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
                //     56: pop
                //     57: aload_0
                //     58: getfield 14	com/android/internal/policy/impl/MiuiScreenOnProximityLock$1:this$0	Lcom/android/internal/policy/impl/MiuiScreenOnProximityLock;
                //     61: invokestatic 43	com/android/internal/policy/impl/MiuiScreenOnProximityLock:access$500	(Lcom/android/internal/policy/impl/MiuiScreenOnProximityLock;)Landroid/app/Dialog;
                //     64: ifnonnull +20 -> 84
                //     67: aload_0
                //     68: getfield 14	com/android/internal/policy/impl/MiuiScreenOnProximityLock$1:this$0	Lcom/android/internal/policy/impl/MiuiScreenOnProximityLock;
                //     71: invokestatic 47	com/android/internal/policy/impl/MiuiScreenOnProximityLock:access$600	(Lcom/android/internal/policy/impl/MiuiScreenOnProximityLock;)V
                //     74: aload_0
                //     75: getfield 14	com/android/internal/policy/impl/MiuiScreenOnProximityLock$1:this$0	Lcom/android/internal/policy/impl/MiuiScreenOnProximityLock;
                //     78: invokestatic 43	com/android/internal/policy/impl/MiuiScreenOnProximityLock:access$500	(Lcom/android/internal/policy/impl/MiuiScreenOnProximityLock;)Landroid/app/Dialog;
                //     81: invokevirtual 53	android/app/Dialog:show	()V
                //     84: aload_0
                //     85: getfield 14	com/android/internal/policy/impl/MiuiScreenOnProximityLock$1:this$0	Lcom/android/internal/policy/impl/MiuiScreenOnProximityLock;
                //     88: invokestatic 57	com/android/internal/policy/impl/MiuiScreenOnProximityLock:access$700	(Lcom/android/internal/policy/impl/MiuiScreenOnProximityLock;)Lcom/android/internal/policy/impl/KeyguardViewMediator;
                //     91: getfield 63	com/android/internal/policy/impl/KeyguardViewMediator:mRealPowerManager	Landroid/os/LocalPowerManager;
                //     94: iconst_0
                //     95: invokeinterface 69 2 0
                //     100: goto -64 -> 36
                //     103: astore_3
                //     104: aload_2
                //     105: monitorexit
                //     106: aload_3
                //     107: athrow
                //     108: ldc 31
                //     110: ldc 71
                //     112: invokestatic 39	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
                //     115: pop
                //     116: aload_0
                //     117: getfield 14	com/android/internal/policy/impl/MiuiScreenOnProximityLock$1:this$0	Lcom/android/internal/policy/impl/MiuiScreenOnProximityLock;
                //     120: invokestatic 43	com/android/internal/policy/impl/MiuiScreenOnProximityLock:access$500	(Lcom/android/internal/policy/impl/MiuiScreenOnProximityLock;)Landroid/app/Dialog;
                //     123: ifnull +22 -> 145
                //     126: aload_0
                //     127: getfield 14	com/android/internal/policy/impl/MiuiScreenOnProximityLock$1:this$0	Lcom/android/internal/policy/impl/MiuiScreenOnProximityLock;
                //     130: invokestatic 43	com/android/internal/policy/impl/MiuiScreenOnProximityLock:access$500	(Lcom/android/internal/policy/impl/MiuiScreenOnProximityLock;)Landroid/app/Dialog;
                //     133: invokevirtual 74	android/app/Dialog:dismiss	()V
                //     136: aload_0
                //     137: getfield 14	com/android/internal/policy/impl/MiuiScreenOnProximityLock$1:this$0	Lcom/android/internal/policy/impl/MiuiScreenOnProximityLock;
                //     140: aconst_null
                //     141: invokestatic 78	com/android/internal/policy/impl/MiuiScreenOnProximityLock:access$502	(Lcom/android/internal/policy/impl/MiuiScreenOnProximityLock;Landroid/app/Dialog;)Landroid/app/Dialog;
                //     144: pop
                //     145: aload_0
                //     146: getfield 14	com/android/internal/policy/impl/MiuiScreenOnProximityLock$1:this$0	Lcom/android/internal/policy/impl/MiuiScreenOnProximityLock;
                //     149: invokestatic 57	com/android/internal/policy/impl/MiuiScreenOnProximityLock:access$700	(Lcom/android/internal/policy/impl/MiuiScreenOnProximityLock;)Lcom/android/internal/policy/impl/KeyguardViewMediator;
                //     152: invokevirtual 81	com/android/internal/policy/impl/KeyguardViewMediator:isShowingAndNotHidden	()Z
                //     155: ifne -119 -> 36
                //     158: aload_0
                //     159: getfield 14	com/android/internal/policy/impl/MiuiScreenOnProximityLock$1:this$0	Lcom/android/internal/policy/impl/MiuiScreenOnProximityLock;
                //     162: invokestatic 57	com/android/internal/policy/impl/MiuiScreenOnProximityLock:access$700	(Lcom/android/internal/policy/impl/MiuiScreenOnProximityLock;)Lcom/android/internal/policy/impl/KeyguardViewMediator;
                //     165: getfield 63	com/android/internal/policy/impl/KeyguardViewMediator:mRealPowerManager	Landroid/os/LocalPowerManager;
                //     168: iconst_1
                //     169: invokeinterface 69 2 0
                //     174: goto -138 -> 36
                //     177: ldc 31
                //     179: ldc 83
                //     181: invokestatic 39	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
                //     184: pop
                //     185: aload_0
                //     186: getfield 14	com/android/internal/policy/impl/MiuiScreenOnProximityLock$1:this$0	Lcom/android/internal/policy/impl/MiuiScreenOnProximityLock;
                //     189: invokevirtual 86	com/android/internal/policy/impl/MiuiScreenOnProximityLock:release	()Z
                //     192: pop
                //     193: goto -157 -> 36
                //
                // Exception table:
                //     from	to	target	type
                //     7	106	103	finally
                //     108	193	103	finally
            }
        };
    }

    private void prepareHintDialog()
    {
        this.mDialog = new Dialog(this.mContext, 16973931);
        WindowManager.LayoutParams localLayoutParams = this.mDialog.getWindow().getAttributes();
        localLayoutParams.type = 2016;
        localLayoutParams.flags = 4352;
        localLayoutParams.format = -3;
        localLayoutParams.gravity = 17;
        this.mDialog.getWindow().setAttributes(localLayoutParams);
        this.mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(-872415232));
        this.mDialog.getWindow().requestFeature(1);
        this.mDialog.setCancelable(false);
        this.mDialog.setContentView(View.inflate(this.mDialog.getContext(), 100859946, null), new ViewGroup.LayoutParams(-1, -1));
    }

    /** @deprecated */
    public void aquire()
    {
        try
        {
            Log.d("MiuiScreenOnProximityLock", "try to aquire");
            if (!this.mHeld)
            {
                Log.d("MiuiScreenOnProximityLock", "aquire");
                this.mHeld = true;
                this.mIsFirstChange = true;
                this.mHandler.sendEmptyMessageDelayed(3, 1000L);
                this.mSensorManager.registerListener(this.mSensorEventListener, this.mSensor, 1);
            }
            while (true)
            {
                return;
                this.mSensorEventListener.handleChanges();
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public boolean isHeld()
    {
        try
        {
            boolean bool = this.mHeld;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public boolean release()
    {
        boolean bool = true;
        try
        {
            Log.d("MiuiScreenOnProximityLock", "try to release");
            if (this.mHeld)
            {
                Log.d("MiuiScreenOnProximityLock", "release");
                this.mHeld = false;
                this.mDownRecieved.clear();
                this.mSensorManager.unregisterListener(this.mSensorEventListener);
                this.mHandler.removeMessages(1);
                this.mHandler.removeMessages(4);
                this.mHandler.removeMessages(3);
                this.mHandler.sendEmptyMessage(2);
                return bool;
            }
            bool = false;
        }
        finally
        {
        }
    }

    public boolean shouldBeBlocked(KeyEvent paramKeyEvent)
    {
        boolean bool = true;
        if ((paramKeyEvent == null) || (!this.mHeld))
            bool = false;
        while (true)
        {
            return bool;
            switch (paramKeyEvent.getKeyCode())
            {
            default:
                int i = paramKeyEvent.getKeyCode();
                if ((paramKeyEvent.getRepeatCount() == 0) && (paramKeyEvent.getAction() == 0))
                    this.mDownRecieved.put(i, Boolean.valueOf(bool));
                if (this.mDownRecieved.get(i) == null)
                    bool = false;
                break;
            case 24:
            case 25:
                if (((AudioManager)this.mContext.getSystemService("audio")).isMusicActive())
                    bool = false;
                break;
            case 79:
            case 85:
            case 86:
            case 87:
            case 126:
            case 127:
                bool = false;
            }
        }
    }

    private class MySensorEventListener
        implements SensorEventListener
    {
        boolean mIsTooClose;

        private MySensorEventListener()
        {
        }

        public void handleChanges()
        {
            StringBuilder localStringBuilder = new StringBuilder().append("handle change = ");
            String str;
            if (this.mIsTooClose)
            {
                str = "too close";
                Log.d("MiuiScreenOnProximityLock", str);
                if (MiuiScreenOnProximityLock.this.mIsFirstChange)
                    MiuiScreenOnProximityLock.this.mHandler.removeMessages(3);
                if (!this.mIsTooClose)
                    break label143;
                MiuiScreenOnProximityLock.this.mHandler.removeMessages(2);
                MiuiScreenOnProximityLock.this.mHandler.removeMessages(3);
                localHandler2 = MiuiScreenOnProximityLock.this.mHandler;
                if (!MiuiScreenOnProximityLock.this.mIsFirstChange)
                    break label137;
                l2 = MiuiScreenOnProximityLock.sValidChangeDelay;
                localHandler2.sendEmptyMessageDelayed(1, l2);
            }
            label137: label143: 
            while (MiuiScreenOnProximityLock.this.mHandler.hasMessages(3))
                while (true)
                {
                    Handler localHandler2;
                    MiuiScreenOnProximityLock.access$102(MiuiScreenOnProximityLock.this, false);
                    return;
                    str = "far away";
                    break;
                    long l2 = 0L;
                }
            MiuiScreenOnProximityLock.this.mHandler.removeMessages(1);
            MiuiScreenOnProximityLock.this.mHandler.removeMessages(4);
            Handler localHandler1 = MiuiScreenOnProximityLock.this.mHandler;
            if (MiuiScreenOnProximityLock.this.mIsFirstChange);
            for (long l1 = MiuiScreenOnProximityLock.sValidChangeDelay; ; l1 = 300L)
            {
                localHandler1.sendEmptyMessageDelayed(3, l1);
                break;
            }
        }

        public void onAccuracyChanged(Sensor paramSensor, int paramInt)
        {
        }

        public void onSensorChanged(SensorEvent paramSensorEvent)
        {
            boolean bool = false;
            float f = paramSensorEvent.values[0];
            if ((f >= 0.0D) && (f < 4.0F) && (f < MiuiScreenOnProximityLock.this.mSensor.getMaximumRange()))
                bool = true;
            this.mIsTooClose = bool;
            handleChanges();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.MiuiScreenOnProximityLock
 * JD-Core Version:        0.6.2
 */