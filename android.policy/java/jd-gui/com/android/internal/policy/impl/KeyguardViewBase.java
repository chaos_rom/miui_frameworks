package com.android.internal.policy.impl;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.IAudioService;
import android.media.IAudioService.Stub;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.Slog;
import android.view.KeyEvent;
import android.widget.FrameLayout;

public abstract class KeyguardViewBase extends FrameLayout
{
    private static final int BACKGROUND_COLOR = 1879048192;
    private static final boolean KEYGUARD_MANAGES_VOLUME = true;
    private AudioManager mAudioManager;
    Drawable mBackgroundDrawable = new Drawable()
    {
        public void draw(Canvas paramAnonymousCanvas)
        {
            paramAnonymousCanvas.drawColor(1879048192, PorterDuff.Mode.SRC);
        }

        public int getOpacity()
        {
            return -3;
        }

        public void setAlpha(int paramAnonymousInt)
        {
        }

        public void setColorFilter(ColorFilter paramAnonymousColorFilter)
        {
        }
    };
    private KeyguardViewCallback mCallback;
    private TelephonyManager mTelephonyManager = null;

    public KeyguardViewBase(Context paramContext, KeyguardViewCallback paramKeyguardViewCallback)
    {
        super(paramContext);
        this.mCallback = paramKeyguardViewCallback;
        resetBackground();
    }

    private boolean interceptMediaKey(KeyEvent paramKeyEvent)
    {
        int i = 1;
        int j = paramKeyEvent.getKeyCode();
        if (paramKeyEvent.getAction() == 0)
            switch (j)
            {
            default:
                i = 0;
            case 85:
            case 126:
            case 127:
            case 79:
            case 86:
            case 87:
            case 88:
            case 89:
            case 90:
            case 91:
            case 130:
            case 24:
            case 25:
            case 164:
            }
        while (true)
        {
            return i;
            if (this.mTelephonyManager == null)
                this.mTelephonyManager = ((TelephonyManager)getContext().getSystemService("phone"));
            if ((this.mTelephonyManager == null) || (this.mTelephonyManager.getCallState() == 0))
            {
                handleMediaKeyEvent(paramKeyEvent);
                continue;
                while (true)
                {
                    try
                    {
                        if (this.mAudioManager == null)
                            this.mAudioManager = ((AudioManager)getContext().getSystemService("audio"));
                        AudioManager localAudioManager = this.mAudioManager;
                        if (j == 24)
                        {
                            int k = i;
                            localAudioManager.adjustLocalOrRemoteStreamVolume(3, k);
                            break;
                        }
                    }
                    finally
                    {
                    }
                    int m = -1;
                }
                if (paramKeyEvent.getAction() != i)
                    break;
                switch (j)
                {
                default:
                    break;
                case 79:
                case 85:
                case 86:
                case 87:
                case 88:
                case 89:
                case 90:
                case 91:
                case 126:
                case 127:
                case 130:
                    handleMediaKeyEvent(paramKeyEvent);
                }
            }
        }
    }

    private boolean shouldEventKeepScreenOnWhileKeyguardShowing(KeyEvent paramKeyEvent)
    {
        boolean bool = false;
        if (paramKeyEvent.getAction() != 0);
        while (true)
        {
            return bool;
            switch (paramKeyEvent.getKeyCode())
            {
            case 19:
            case 20:
            case 21:
            case 22:
            }
            bool = true;
        }
    }

    public abstract void cleanUp();

    public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
    {
        if (shouldEventKeepScreenOnWhileKeyguardShowing(paramKeyEvent))
            this.mCallback.pokeWakelock();
        if (interceptMediaKey(paramKeyEvent));
        for (boolean bool = true; ; bool = super.dispatchKeyEvent(paramKeyEvent))
            return bool;
    }

    public void dispatchSystemUiVisibilityChanged(int paramInt)
    {
        super.dispatchSystemUiVisibilityChanged(paramInt);
        setSystemUiVisibility(4194304);
    }

    public KeyguardViewCallback getCallback()
    {
        return this.mCallback;
    }

    void handleMediaKeyEvent(KeyEvent paramKeyEvent)
    {
        IAudioService localIAudioService = IAudioService.Stub.asInterface(ServiceManager.checkService("audio"));
        if (localIAudioService != null);
        while (true)
        {
            try
            {
                localIAudioService.dispatchMediaKeyEvent(paramKeyEvent);
                return;
            }
            catch (RemoteException localRemoteException)
            {
                Log.e("KeyguardViewBase", "dispatchMediaKeyEvent threw exception " + localRemoteException);
                continue;
            }
            Slog.w("KeyguardViewBase", "Unable to find IAudioService for media key event");
        }
    }

    public abstract void onScreenTurnedOff();

    public abstract void onScreenTurnedOn();

    public abstract void reset();

    public void resetBackground()
    {
        setBackgroundDrawable(this.mBackgroundDrawable);
    }

    public abstract void show();

    public abstract void verifyUnlock();

    public abstract void wakeWhenReadyTq(int paramInt);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         com.android.internal.policy.impl.KeyguardViewBase
 * JD-Core Version:        0.6.2
 */