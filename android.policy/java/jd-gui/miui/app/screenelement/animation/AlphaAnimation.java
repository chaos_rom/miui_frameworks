package miui.app.screenelement.animation;

import android.text.TextUtils;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.util.Utils;
import org.w3c.dom.Element;

public class AlphaAnimation extends BaseAnimation
{
    public static final String INNER_TAG_NAME = "Alpha";
    public static final String TAG_NAME = "AlphaAnimation";
    private int mCurrentAlpha = 255;
    private int mDelayValue;

    public AlphaAnimation(Element paramElement, String paramString, ScreenContext paramScreenContext)
        throws ScreenElementLoadException
    {
        super(paramElement, paramString, paramScreenContext);
        String str = paramElement.getAttribute("delayValue");
        if (!TextUtils.isEmpty(str));
        try
        {
            for (this.mDelayValue = Integer.parseInt(str); ; this.mDelayValue = ((int)getItem(0).get(0)))
                label41: return;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            break label41;
        }
    }

    public AlphaAnimation(Element paramElement, ScreenContext paramScreenContext)
        throws ScreenElementLoadException
    {
        this(paramElement, "Alpha", paramScreenContext);
        Utils.asserts(paramElement.getNodeName().equalsIgnoreCase("AlphaAnimation"), "wrong tag name:" + paramElement.getNodeName());
    }

    public final int getAlpha()
    {
        return this.mCurrentAlpha;
    }

    protected BaseAnimation.AnimationItem onCreateItem()
    {
        String[] arrayOfString = new String[1];
        arrayOfString[0] = "a";
        return new BaseAnimation.AnimationItem(arrayOfString, this.mContext);
    }

    protected void onTick(BaseAnimation.AnimationItem paramAnimationItem1, BaseAnimation.AnimationItem paramAnimationItem2, float paramFloat)
    {
        if ((paramAnimationItem1 == null) && (paramAnimationItem2 == null))
            return;
        if (paramAnimationItem1 == null);
        for (double d = 255.0D; ; d = paramAnimationItem1.get(0))
        {
            this.mCurrentAlpha = ((int)Math.round(d + (paramAnimationItem2.get(0) - d) * paramFloat));
            break;
        }
    }

    public void reset(long paramLong)
    {
        super.reset(paramLong);
        this.mCurrentAlpha = this.mDelayValue;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.animation.AlphaAnimation
 * JD-Core Version:        0.6.2
 */