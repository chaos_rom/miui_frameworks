package miui.app.screenelement.animation;

import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.data.Expression;
import miui.app.screenelement.util.Utils;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public abstract class BaseAnimation
{
    private static final long INFINITE_TIME = 1000000000000L;
    private static final String LOG_TAG = "BaseAnimation";
    protected ScreenContext mContext;
    private long mDelay;
    protected ArrayList<AnimationItem> mItems = new ArrayList();
    private boolean mLastFrame;
    private long mRealTimeRange;
    private long mStartTime;
    private long mTimeRange;

    public BaseAnimation(Element paramElement, String paramString, ScreenContext paramScreenContext)
        throws ScreenElementLoadException
    {
        this.mContext = paramScreenContext;
        load(paramElement, paramString);
    }

    private void load(Element paramElement, String paramString)
        throws ScreenElementLoadException
    {
        this.mItems.clear();
        String str = paramElement.getAttribute("delay");
        if (!TextUtils.isEmpty(str));
        try
        {
            this.mDelay = Long.parseLong(str);
            NodeList localNodeList = paramElement.getElementsByTagName(paramString);
            for (int i = 0; i < localNodeList.getLength(); i++)
            {
                Element localElement = (Element)localNodeList.item(i);
                this.mItems.add(onCreateItem().load(localElement));
            }
        }
        catch (NumberFormatException localNumberFormatException)
        {
            while (true)
                Log.w("BaseAnimation", "invalid delay attribute");
        }
        if (this.mItems.size() > 0);
        for (boolean bool = true; ; bool = false)
        {
            Utils.asserts(bool, "BaseAnimation: empty items");
            this.mTimeRange = ((AnimationItem)this.mItems.get(-1 + this.mItems.size())).mTime;
            if (this.mItems.size() > 1)
                this.mRealTimeRange = ((AnimationItem)this.mItems.get(-2 + this.mItems.size())).mTime;
            return;
        }
    }

    protected AnimationItem getItem(int paramInt)
    {
        if ((paramInt < 0) || (paramInt >= this.mItems.size()));
        for (AnimationItem localAnimationItem = null; ; localAnimationItem = (AnimationItem)this.mItems.get(paramInt))
            return localAnimationItem;
    }

    public void init()
    {
    }

    protected abstract AnimationItem onCreateItem();

    protected abstract void onTick(AnimationItem paramAnimationItem1, AnimationItem paramAnimationItem2, float paramFloat);

    public void reset(long paramLong)
    {
        this.mStartTime = paramLong;
        this.mLastFrame = false;
    }

    public final void tick(long paramLong)
    {
        long l1 = paramLong - this.mStartTime;
        if (l1 < 0L)
            l1 = 0L;
        if (l1 < this.mDelay)
            onTick(null, null, 0.0F);
        label135: label153: label225: label231: label250: 
        while (true)
        {
            return;
            long l2 = l1 - this.mDelay;
            if ((this.mTimeRange < 1000000000000L) || (l2 <= this.mRealTimeRange) || (!this.mLastFrame))
            {
                long l3 = l2 % this.mTimeRange;
                Object localObject = null;
                for (int i = 0; ; i++)
                {
                    if (i >= this.mItems.size())
                        break label250;
                    AnimationItem localAnimationItem1 = (AnimationItem)this.mItems.get(i);
                    if (l3 <= localAnimationItem1.mTime)
                    {
                        long l4 = 0L;
                        long l5;
                        boolean bool;
                        if (i == 0)
                        {
                            l5 = localAnimationItem1.mTime;
                            if (i != -1 + this.mItems.size())
                                break label225;
                            bool = true;
                            this.mLastFrame = bool;
                            if (l5 != 0L)
                                break label231;
                        }
                        for (float f = 1.0F; ; f = (float)(l3 - l4) / (float)l5)
                        {
                            onTick((AnimationItem)localObject, localAnimationItem1, f);
                            break;
                            AnimationItem localAnimationItem2 = (AnimationItem)this.mItems.get(i - 1);
                            localObject = localAnimationItem2;
                            l5 = localAnimationItem1.mTime - localAnimationItem2.mTime;
                            l4 = localAnimationItem2.mTime;
                            break label135;
                            bool = false;
                            break label153;
                        }
                    }
                }
            }
        }
    }

    public static class AnimationItem
    {
        private String[] mAttrs;
        private ScreenContext mContext;
        public Expression[] mExps;
        public long mTime;

        public AnimationItem(String[] paramArrayOfString, ScreenContext paramScreenContext)
        {
            this.mAttrs = paramArrayOfString;
            this.mContext = paramScreenContext;
        }

        public double get(int paramInt)
        {
            double d = 0.0D;
            if ((paramInt < 0) || (paramInt >= this.mExps.length) || (this.mExps == null))
                Log.e("BaseAnimation", "fail to get number in AnimationItem:" + paramInt);
            while (true)
            {
                return d;
                if (this.mExps[paramInt] != null)
                    d = this.mExps[paramInt].evaluate(this.mContext.mVariables);
            }
        }

        public AnimationItem load(Element paramElement)
            throws ScreenElementLoadException
        {
            try
            {
                this.mTime = Long.parseLong(paramElement.getAttribute("time"));
                if (this.mAttrs != null)
                {
                    this.mExps = new Expression[this.mAttrs.length];
                    String[] arrayOfString = this.mAttrs;
                    int i = arrayOfString.length;
                    int j = 0;
                    int m;
                    for (int k = 0; j < i; k = m)
                    {
                        String str = arrayOfString[j];
                        Expression[] arrayOfExpression = this.mExps;
                        m = k + 1;
                        arrayOfExpression[k] = Expression.build(paramElement.getAttribute(str));
                        j++;
                    }
                }
            }
            catch (NumberFormatException localNumberFormatException)
            {
                Log.e("BaseAnimation", "fail to get time attribute");
                throw new ScreenElementLoadException("fail to get time attribute");
            }
            return this;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.animation.BaseAnimation
 * JD-Core Version:        0.6.2
 */