package miui.app.screenelement.animation;

import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.data.Expression;
import miui.app.screenelement.util.TextFormatter;
import miui.app.screenelement.util.Utils;
import org.w3c.dom.Element;

public class AnimatedElement
{
    private static final boolean DEBUG = false;
    private static final String LOG_TAG = "AnimatedElement";
    private boolean mAlignAbsolute;
    private Expression mAlphaExpression;
    private AlphaAnimation mAlphas;
    private ArrayList<BaseAnimation> mAnimations = new ArrayList();
    protected Expression mBaseXExpression;
    protected Expression mBaseYExpression;
    protected Expression mCenterXExpression;
    protected Expression mCenterYExpression;
    private ScreenContext mContext;
    protected Expression mHeightExpression;
    private PositionAnimation mPositions;
    protected Expression mRotationExpression;
    private RotationAnimation mRotations;
    private SizeAnimation mSizes;
    private SourcesAnimation mSources;
    private TextFormatter mSrcFormatter;
    protected Expression mSrcIdExpression;
    protected Expression mWidthExpression;

    public AnimatedElement(Element paramElement, ScreenContext paramScreenContext)
        throws ScreenElementLoadException
    {
        this.mContext = paramScreenContext;
        load(paramElement);
    }

    private Expression createExp(Element paramElement, String paramString1, String paramString2)
    {
        Expression localExpression = Expression.build(paramElement.getAttribute(paramString1));
        if ((localExpression == null) && (!TextUtils.isEmpty(paramString2)))
            localExpression = Expression.build(paramElement.getAttribute(paramString2));
        return localExpression;
    }

    private void loadAlphaAnimations(Element paramElement)
        throws ScreenElementLoadException
    {
        Element localElement = Utils.getChild(paramElement, "AlphaAnimation");
        if (localElement == null);
        while (true)
        {
            return;
            this.mAlphas = new AlphaAnimation(localElement, this.mContext);
            this.mAnimations.add(this.mAlphas);
        }
    }

    private void loadPositionAnimations(Element paramElement)
        throws ScreenElementLoadException
    {
        Element localElement = Utils.getChild(paramElement, "PositionAnimation");
        if (localElement == null);
        while (true)
        {
            return;
            this.mPositions = new PositionAnimation(localElement, this.mContext);
            this.mAnimations.add(this.mPositions);
        }
    }

    private void loadRotationAnimations(Element paramElement)
        throws ScreenElementLoadException
    {
        Element localElement = Utils.getChild(paramElement, "RotationAnimation");
        if (localElement == null);
        while (true)
        {
            return;
            this.mRotations = new RotationAnimation(localElement, this.mContext);
            this.mAnimations.add(this.mRotations);
        }
    }

    private void loadSizeAnimations(Element paramElement)
        throws ScreenElementLoadException
    {
        Element localElement = Utils.getChild(paramElement, "SizeAnimation");
        if (localElement == null);
        while (true)
        {
            return;
            this.mSizes = new SizeAnimation(localElement, this.mContext);
            this.mAnimations.add(this.mSizes);
        }
    }

    private void loadSourceAnimations(Element paramElement)
        throws ScreenElementLoadException
    {
        Element localElement = Utils.getChild(paramElement, "SourcesAnimation");
        if (localElement == null);
        while (true)
        {
            return;
            this.mSources = new SourcesAnimation(localElement, this.mContext);
            this.mAnimations.add(this.mSources);
        }
    }

    public int getAlpha()
    {
        int i;
        if (this.mAlphaExpression != null)
        {
            i = (int)this.mAlphaExpression.evaluate(this.mContext.mVariables);
            if (this.mAlphas == null)
                break label51;
        }
        label51: for (int j = this.mAlphas.getAlpha(); ; j = 255)
        {
            return Utils.mixAlpha(i, j);
            i = 255;
            break;
        }
    }

    public float getHeight()
    {
        float f;
        if (this.mSizes != null)
        {
            f = (float)this.mSizes.getHeight();
            return f;
        }
        if (this.mHeightExpression != null);
        for (double d = this.mHeightExpression.evaluate(this.mContext.mVariables); ; d = -1.0D)
        {
            f = (float)d;
            break;
        }
    }

    public float getMaxHeight()
    {
        float f;
        if (this.mSizes != null)
        {
            f = (float)this.mSizes.getMaxHeight();
            return f;
        }
        if (this.mHeightExpression != null);
        for (double d = this.mHeightExpression.evaluate(this.mContext.mVariables); ; d = -1.0D)
        {
            f = (float)d;
            break;
        }
    }

    public float getMaxWidth()
    {
        float f;
        if (this.mSizes != null)
        {
            f = (float)this.mSizes.getMaxWidth();
            return f;
        }
        if (this.mWidthExpression != null);
        for (double d = this.mWidthExpression.evaluate(this.mContext.mVariables); ; d = -1.0D)
        {
            f = (float)d;
            break;
        }
    }

    public float getPivotX()
    {
        if (this.mCenterXExpression != null);
        for (double d = this.mCenterXExpression.evaluate(this.mContext.mVariables); ; d = 0.0D)
            return (float)d;
    }

    public float getPivotY()
    {
        if (this.mCenterYExpression != null);
        for (double d = this.mCenterYExpression.evaluate(this.mContext.mVariables); ; d = 0.0D)
            return (float)d;
    }

    public float getRotationAngle()
    {
        double d;
        if (this.mRotationExpression != null)
        {
            d = this.mRotationExpression.evaluate(this.mContext.mVariables);
            if (this.mRotations == null)
                break label48;
        }
        label48: for (float f = this.mRotations.getAngle(); ; f = 0.0F)
        {
            return (float)(d + f);
            d = 0.0D;
            break;
        }
    }

    public String getSrc()
    {
        String str = this.mSrcFormatter.getText(this.mContext.mVariables);
        if (this.mSources != null)
            str = this.mSources.getSrc();
        if ((str != null) && (this.mSrcIdExpression != null))
            str = Utils.addFileNameSuffix(str, String.valueOf(()this.mSrcIdExpression.evaluate(this.mContext.mVariables)));
        return str;
    }

    public float getWidth()
    {
        float f;
        if (this.mSizes != null)
        {
            f = (float)this.mSizes.getWidth();
            return f;
        }
        if (this.mWidthExpression != null);
        for (double d = this.mWidthExpression.evaluate(this.mContext.mVariables); ; d = -1.0D)
        {
            f = (float)d;
            break;
        }
    }

    public float getX()
    {
        if (this.mBaseXExpression != null);
        for (double d = this.mBaseXExpression.evaluate(this.mContext.mVariables); ; d = 0.0D)
        {
            if (this.mSources != null)
                d += this.mSources.getX();
            if (this.mPositions != null)
                d += this.mPositions.getX();
            return (float)d;
        }
    }

    public float getY()
    {
        if (this.mBaseYExpression != null);
        for (double d = this.mBaseYExpression.evaluate(this.mContext.mVariables); ; d = 0.0D)
        {
            if (this.mSources != null)
                d += this.mSources.getY();
            if (this.mPositions != null)
                d += this.mPositions.getY();
            return (float)d;
        }
    }

    public void init()
    {
        Iterator localIterator = this.mAnimations.iterator();
        while (localIterator.hasNext())
            ((BaseAnimation)localIterator.next()).init();
    }

    public boolean isAlignAbsolute()
    {
        return this.mAlignAbsolute;
    }

    public void load(Element paramElement)
        throws ScreenElementLoadException
    {
        if (paramElement == null)
        {
            Log.e("AnimatedElement", "node is null");
            throw new ScreenElementLoadException("node is null");
        }
        this.mBaseXExpression = createExp(paramElement, "x", "left");
        this.mBaseYExpression = createExp(paramElement, "y", "top");
        this.mWidthExpression = createExp(paramElement, "w", "width");
        this.mHeightExpression = createExp(paramElement, "h", "height");
        this.mRotationExpression = createExp(paramElement, "angle", "rotation");
        this.mCenterXExpression = createExp(paramElement, "centerX", "pivotX");
        this.mCenterYExpression = createExp(paramElement, "centerY", "pivotY");
        this.mSrcIdExpression = createExp(paramElement, "srcid", null);
        this.mAlphaExpression = createExp(paramElement, "alpha", null);
        this.mSrcFormatter = TextFormatter.fromElement(paramElement, "src", "srcFormat", "srcParas", "srcExp", "srcFormatExp");
        if (paramElement.getAttribute("align").equalsIgnoreCase("absolute"))
            this.mAlignAbsolute = true;
        loadSourceAnimations(paramElement);
        loadPositionAnimations(paramElement);
        loadRotationAnimations(paramElement);
        loadSizeAnimations(paramElement);
        loadAlphaAnimations(paramElement);
    }

    public void reset(long paramLong)
    {
        Iterator localIterator = this.mAnimations.iterator();
        while (localIterator.hasNext())
            ((BaseAnimation)localIterator.next()).reset(paramLong);
    }

    public void tick(long paramLong)
    {
        Iterator localIterator = this.mAnimations.iterator();
        while (localIterator.hasNext())
            ((BaseAnimation)localIterator.next()).tick(paramLong);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.animation.AnimatedElement
 * JD-Core Version:        0.6.2
 */