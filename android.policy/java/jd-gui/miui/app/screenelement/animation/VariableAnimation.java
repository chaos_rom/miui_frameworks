package miui.app.screenelement.animation;

import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import org.w3c.dom.Element;

public class VariableAnimation extends BaseAnimation
{
    public static final String INNER_TAG_NAME = "AniFrame";
    public static final String TAG_NAME = "VariableAnimation";
    private double mCurrentValue;
    private double mDelayValue = getItem(0).get(0);

    public VariableAnimation(Element paramElement, ScreenContext paramScreenContext)
        throws ScreenElementLoadException
    {
        super(paramElement, "AniFrame", paramScreenContext);
    }

    public final double getValue()
    {
        return this.mCurrentValue;
    }

    protected BaseAnimation.AnimationItem onCreateItem()
    {
        String[] arrayOfString = new String[1];
        arrayOfString[0] = "value";
        return new BaseAnimation.AnimationItem(arrayOfString, this.mContext);
    }

    protected void onTick(BaseAnimation.AnimationItem paramAnimationItem1, BaseAnimation.AnimationItem paramAnimationItem2, float paramFloat)
    {
        if ((paramAnimationItem1 == null) && (paramAnimationItem2 == null))
            return;
        if (paramAnimationItem1 == null);
        for (double d = 0.0D; ; d = paramAnimationItem1.get(0))
        {
            this.mCurrentValue = (d + (paramAnimationItem2.get(0) - d) * paramFloat);
            break;
        }
    }

    public void reset(long paramLong)
    {
        super.reset(paramLong);
        this.mCurrentValue = this.mDelayValue;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.animation.VariableAnimation
 * JD-Core Version:        0.6.2
 */