package miui.app.screenelement;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory.Options;
import android.graphics.NinePatch;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.os.MemoryFile;
import android.text.TextUtils;
import android.util.Log;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import org.w3c.dom.Element;

public class ResourceManager
{
    private static final int DEFAULT_DENSITY = 240;
    private static final int DEFAULT_SCREEN_WIDTH = 480;
    private static final String LOG_TAG = "ResourceManager";
    protected final HashMap<String, BitmapInfo> mBitmaps = new HashMap();
    private int mExtraResourceDensity;
    private String mExtraResourceFolder;
    private int mExtraResourceScreenWidth;
    private final HashSet<String> mFailedBitmaps = new HashSet();
    private Bitmap mMaskBitmap;
    private HashMap<String, Bitmap> mMaskBitmaps;
    private final HashMap<String, NinePatch> mNinePatches = new HashMap();
    private int mResourceDensity;
    private final ResourceLoader mResourceLoader;
    private int mTargetDensity;

    public ResourceManager(ResourceLoader paramResourceLoader)
    {
        this.mResourceLoader = paramResourceLoader;
    }

    private BitmapInfo getBitmapInfo(String paramString)
    {
        BitmapInfo localBitmapInfo;
        if (TextUtils.isEmpty(paramString))
            localBitmapInfo = null;
        while (true)
        {
            return localBitmapInfo;
            localBitmapInfo = (BitmapInfo)this.mBitmaps.get(paramString);
            if (localBitmapInfo != null)
            {
                localBitmapInfo.mLastVisitTime = System.currentTimeMillis();
            }
            else if (this.mFailedBitmaps.contains(paramString))
            {
                localBitmapInfo = null;
            }
            else
            {
                Log.i("ResourceManager", "load image " + paramString);
                int i = 1;
                BitmapFactory.Options localOptions = new BitmapFactory.Options();
                localOptions.inScaled = true;
                localOptions.inTargetDensity = this.mTargetDensity;
                if (this.mExtraResourceScreenWidth != 0)
                {
                    localOptions.inDensity = this.mExtraResourceDensity;
                    localBitmapInfo = this.mResourceLoader.getBitmapInfo(this.mExtraResourceFolder + "/" + paramString, localOptions);
                    if (localBitmapInfo != null)
                        i = 0;
                }
                if (localBitmapInfo == null)
                {
                    localOptions.inDensity = this.mResourceDensity;
                    localBitmapInfo = this.mResourceLoader.getBitmapInfo(paramString, localOptions);
                }
                if (localBitmapInfo != null)
                {
                    if (i == 0)
                        Log.i("ResourceManager", "load image from extra resource: " + this.mExtraResourceFolder);
                    localBitmapInfo.mBitmap.setDensity(this.mTargetDensity);
                    localBitmapInfo.mLastVisitTime = System.currentTimeMillis();
                    this.mBitmaps.put(paramString, localBitmapInfo);
                }
                else
                {
                    this.mFailedBitmaps.add(paramString);
                    Log.e("ResourceManager", "fail to load image: " + paramString);
                }
            }
        }
    }

    public void clear()
    {
        Iterator localIterator1 = this.mBitmaps.values().iterator();
        while (localIterator1.hasNext())
        {
            BitmapInfo localBitmapInfo = (BitmapInfo)localIterator1.next();
            if (localBitmapInfo.mBitmap != null)
                localBitmapInfo.mBitmap.recycle();
        }
        if (this.mMaskBitmaps != null)
        {
            Iterator localIterator2 = this.mMaskBitmaps.values().iterator();
            while (localIterator2.hasNext())
            {
                Bitmap localBitmap = (Bitmap)localIterator2.next();
                if (!localBitmap.isRecycled())
                    localBitmap.recycle();
            }
        }
        if ((this.mMaskBitmap != null) && (!this.mMaskBitmap.isRecycled()))
        {
            this.mMaskBitmap.recycle();
            this.mMaskBitmap = null;
        }
        this.mBitmaps.clear();
        this.mNinePatches.clear();
    }

    public Bitmap getBitmap(String paramString)
    {
        BitmapInfo localBitmapInfo = getBitmapInfo(paramString);
        if (localBitmapInfo != null);
        for (Bitmap localBitmap = localBitmapInfo.mBitmap; ; localBitmap = null)
            return localBitmap;
    }

    public Drawable getDrawable(String paramString)
    {
        BitmapInfo localBitmapInfo = getBitmapInfo(paramString);
        Object localObject;
        if ((localBitmapInfo == null) || (localBitmapInfo.mBitmap == null))
            localObject = null;
        while (true)
        {
            return localObject;
            Bitmap localBitmap = localBitmapInfo.mBitmap;
            if (localBitmap.getNinePatchChunk() != null)
            {
                localObject = new NinePatchDrawable(localBitmap, localBitmap.getNinePatchChunk(), localBitmapInfo.mPadding, paramString);
                ((NinePatchDrawable)localObject).setTargetDensity(this.mTargetDensity);
            }
            else
            {
                BitmapDrawable localBitmapDrawable = new BitmapDrawable(localBitmap);
                localBitmapDrawable.setTargetDensity(this.mTargetDensity);
                localObject = localBitmapDrawable;
            }
        }
    }

    public MemoryFile getFile(String paramString)
    {
        return this.mResourceLoader.getFile(paramString);
    }

    public Element getManifestRoot()
    {
        return this.mResourceLoader.getManifestRoot();
    }

    public Bitmap getMaskBufferBitmap(int paramInt1, int paramInt2, String paramString, boolean paramBoolean)
    {
        if ((paramBoolean) && (this.mMaskBitmaps == null))
            this.mMaskBitmaps = new HashMap();
        Bitmap localBitmap;
        if (paramBoolean)
        {
            localBitmap = (Bitmap)this.mMaskBitmaps.get(paramString);
            if ((localBitmap == null) || (localBitmap.getHeight() < paramInt2) || (localBitmap.getWidth() < paramInt1))
            {
                if (localBitmap != null)
                {
                    int i = localBitmap.getWidth();
                    int j = localBitmap.getHeight();
                    paramInt1 = Math.max(i, paramInt1);
                    paramInt2 = Math.max(j, paramInt2);
                    if (!localBitmap.isRecycled())
                        localBitmap.recycle();
                }
                localBitmap = Bitmap.createBitmap(paramInt1, paramInt2, Bitmap.Config.ARGB_8888);
                localBitmap.setDensity(this.mResourceDensity);
                if (!paramBoolean)
                    break label157;
                this.mMaskBitmaps.put(paramString, localBitmap);
            }
        }
        while (true)
        {
            return localBitmap;
            localBitmap = this.mMaskBitmap;
            break;
            label157: this.mMaskBitmap = localBitmap;
        }
    }

    public NinePatch getNinePatch(String paramString)
    {
        NinePatch localNinePatch = (NinePatch)this.mNinePatches.get(paramString);
        if (localNinePatch == null)
        {
            Bitmap localBitmap = getBitmap(paramString);
            if ((localBitmap != null) && (localBitmap.getNinePatchChunk() != null))
            {
                localNinePatch = new NinePatch(localBitmap, localBitmap.getNinePatchChunk(), null);
                this.mNinePatches.put(paramString, localNinePatch);
            }
        }
        return localNinePatch;
    }

    public void setExtraResource(int paramInt)
    {
        this.mExtraResourceScreenWidth = paramInt;
        this.mExtraResourceFolder = ("sw" + paramInt);
        this.mExtraResourceDensity = (paramInt * 240 / 480);
    }

    public void setResourceDensity(int paramInt)
    {
        this.mResourceDensity = paramInt;
    }

    public void setTargetDensity(int paramInt)
    {
        this.mTargetDensity = paramInt;
    }

    public static class BitmapInfo
    {
        public final Bitmap mBitmap;
        public long mLastVisitTime;
        public final Rect mPadding;

        public BitmapInfo(Bitmap paramBitmap, Rect paramRect)
        {
            this.mBitmap = paramBitmap;
            this.mPadding = paramRect;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.ResourceManager
 * JD-Core Version:        0.6.2
 */