package miui.app.screenelement;

import android.os.SystemClock;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

public class RenderThread extends Thread
{
    private static final String LOG_TAG = "RenderThread";
    private static RenderThread sGlobalThread;
    private static Object sGlobalThreadLock = new Object();
    private boolean mPaused = true;
    private ArrayList<RendererController> mRendererControllerList = new ArrayList();
    private Object mResumeSignal = new Object();
    private boolean mStarted;
    private boolean mStop;

    public RenderThread()
    {
        super("MAML RenderThread");
    }

    public RenderThread(RendererController paramRendererController)
    {
        super("MAML RenderThread");
        addRendererController(paramRendererController);
    }

    // ERROR //
    private void doFinish()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 39	miui/app/screenelement/RenderThread:mRendererControllerList	Ljava/util/ArrayList;
        //     4: invokevirtual 52	java/util/ArrayList:size	()I
        //     7: ifne +4 -> 11
        //     10: return
        //     11: aload_0
        //     12: getfield 39	miui/app/screenelement/RenderThread:mRendererControllerList	Ljava/util/ArrayList;
        //     15: astore_1
        //     16: aload_1
        //     17: monitorenter
        //     18: aload_0
        //     19: getfield 39	miui/app/screenelement/RenderThread:mRendererControllerList	Ljava/util/ArrayList;
        //     22: invokevirtual 56	java/util/ArrayList:iterator	()Ljava/util/Iterator;
        //     25: astore_3
        //     26: aload_3
        //     27: invokeinterface 62 1 0
        //     32: ifeq +23 -> 55
        //     35: aload_3
        //     36: invokeinterface 66 1 0
        //     41: checkcast 68	miui/app/screenelement/RendererController
        //     44: invokevirtual 71	miui/app/screenelement/RendererController:finish	()V
        //     47: goto -21 -> 26
        //     50: astore_2
        //     51: aload_1
        //     52: monitorexit
        //     53: aload_2
        //     54: athrow
        //     55: aload_1
        //     56: monitorexit
        //     57: goto -47 -> 10
        //
        // Exception table:
        //     from	to	target	type
        //     18	53	50	finally
        //     55	57	50	finally
    }

    // ERROR //
    private void doInit()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 39	miui/app/screenelement/RenderThread:mRendererControllerList	Ljava/util/ArrayList;
        //     4: invokevirtual 52	java/util/ArrayList:size	()I
        //     7: ifne +4 -> 11
        //     10: return
        //     11: invokestatic 78	android/os/SystemClock:elapsedRealtime	()J
        //     14: lstore_1
        //     15: aload_0
        //     16: getfield 39	miui/app/screenelement/RenderThread:mRendererControllerList	Ljava/util/ArrayList;
        //     19: astore_3
        //     20: aload_3
        //     21: monitorenter
        //     22: aload_0
        //     23: getfield 39	miui/app/screenelement/RenderThread:mRendererControllerList	Ljava/util/ArrayList;
        //     26: invokevirtual 56	java/util/ArrayList:iterator	()Ljava/util/Iterator;
        //     29: astore 5
        //     31: aload 5
        //     33: invokeinterface 62 1 0
        //     38: ifeq +54 -> 92
        //     41: aload 5
        //     43: invokeinterface 66 1 0
        //     48: checkcast 68	miui/app/screenelement/RendererController
        //     51: astore 6
        //     53: aload 6
        //     55: lload_1
        //     56: invokevirtual 82	miui/app/screenelement/RendererController:setLastUpdateTime	(J)V
        //     59: aload 6
        //     61: invokevirtual 85	miui/app/screenelement/RendererController:init	()V
        //     64: aload_0
        //     65: getfield 41	miui/app/screenelement/RenderThread:mPaused	Z
        //     68: ifeq +9 -> 77
        //     71: aload 6
        //     73: lload_1
        //     74: invokevirtual 88	miui/app/screenelement/RendererController:tick	(J)V
        //     77: aload 6
        //     79: invokevirtual 91	miui/app/screenelement/RendererController:requestUpdate	()V
        //     82: goto -51 -> 31
        //     85: astore 4
        //     87: aload_3
        //     88: monitorexit
        //     89: aload 4
        //     91: athrow
        //     92: aload_3
        //     93: monitorexit
        //     94: goto -84 -> 10
        //
        // Exception table:
        //     from	to	target	type
        //     22	89	85	finally
        //     92	94	85	finally
    }

    // ERROR //
    private void doPause()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 39	miui/app/screenelement/RenderThread:mRendererControllerList	Ljava/util/ArrayList;
        //     4: invokevirtual 52	java/util/ArrayList:size	()I
        //     7: ifne +4 -> 11
        //     10: return
        //     11: aload_0
        //     12: getfield 39	miui/app/screenelement/RenderThread:mRendererControllerList	Ljava/util/ArrayList;
        //     15: astore_1
        //     16: aload_1
        //     17: monitorenter
        //     18: aload_0
        //     19: getfield 39	miui/app/screenelement/RenderThread:mRendererControllerList	Ljava/util/ArrayList;
        //     22: invokevirtual 56	java/util/ArrayList:iterator	()Ljava/util/Iterator;
        //     25: astore_3
        //     26: aload_3
        //     27: invokeinterface 62 1 0
        //     32: ifeq +23 -> 55
        //     35: aload_3
        //     36: invokeinterface 66 1 0
        //     41: checkcast 68	miui/app/screenelement/RendererController
        //     44: invokevirtual 95	miui/app/screenelement/RendererController:pause	()V
        //     47: goto -21 -> 26
        //     50: astore_2
        //     51: aload_1
        //     52: monitorexit
        //     53: aload_2
        //     54: athrow
        //     55: aload_1
        //     56: monitorexit
        //     57: goto -47 -> 10
        //
        // Exception table:
        //     from	to	target	type
        //     18	53	50	finally
        //     55	57	50	finally
    }

    // ERROR //
    private void doResume()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 39	miui/app/screenelement/RenderThread:mRendererControllerList	Ljava/util/ArrayList;
        //     4: invokevirtual 52	java/util/ArrayList:size	()I
        //     7: ifne +4 -> 11
        //     10: return
        //     11: aload_0
        //     12: getfield 39	miui/app/screenelement/RenderThread:mRendererControllerList	Ljava/util/ArrayList;
        //     15: astore_1
        //     16: aload_1
        //     17: monitorenter
        //     18: aload_0
        //     19: getfield 39	miui/app/screenelement/RenderThread:mRendererControllerList	Ljava/util/ArrayList;
        //     22: invokevirtual 56	java/util/ArrayList:iterator	()Ljava/util/Iterator;
        //     25: astore_3
        //     26: aload_3
        //     27: invokeinterface 62 1 0
        //     32: ifeq +23 -> 55
        //     35: aload_3
        //     36: invokeinterface 66 1 0
        //     41: checkcast 68	miui/app/screenelement/RendererController
        //     44: invokevirtual 99	miui/app/screenelement/RendererController:resume	()V
        //     47: goto -21 -> 26
        //     50: astore_2
        //     51: aload_1
        //     52: monitorexit
        //     53: aload_2
        //     54: athrow
        //     55: aload_1
        //     56: monitorexit
        //     57: goto -47 -> 10
        //
        // Exception table:
        //     from	to	target	type
        //     18	53	50	finally
        //     55	57	50	finally
    }

    private boolean doUpdateFramerate(long paramLong)
    {
        boolean bool;
        if (this.mRendererControllerList.size() == 0)
            bool = true;
        while (true)
        {
            return bool;
            bool = true;
            synchronized (this.mRendererControllerList)
            {
                Iterator localIterator = this.mRendererControllerList.iterator();
                while (localIterator.hasNext())
                {
                    RendererController localRendererController = (RendererController)localIterator.next();
                    if (!localRendererController.isSelfPaused())
                    {
                        localRendererController.updateFramerate(paramLong);
                        bool = false;
                    }
                }
            }
        }
    }

    public static RenderThread globalThread()
    {
        if (sGlobalThread == null);
        synchronized (sGlobalThreadLock)
        {
            if (sGlobalThread == null)
                sGlobalThread = new RenderThread();
            return sGlobalThread;
        }
    }

    private void sleepForFramerate(float paramFloat)
    {
        if (paramFloat > 50.0F);
        while (true)
        {
            return;
            long l = 50L;
            if (paramFloat > 10.0F)
                l = ()(500.0F / paramFloat);
            try
            {
                Thread.sleep(l);
            }
            catch (InterruptedException localInterruptedException)
            {
            }
        }
    }

    private void waiteForResume()
    {
        try
        {
            this.mResumeSignal.wait();
            return;
        }
        catch (InterruptedException localInterruptedException)
        {
            while (true)
                localInterruptedException.printStackTrace();
        }
    }

    public void addRendererController(RendererController paramRendererController)
    {
        synchronized (this.mRendererControllerList)
        {
            this.mRendererControllerList.add(paramRendererController);
            paramRendererController.setRenderThread(this);
            setPaused(false);
            return;
        }
    }

    public boolean isStarted()
    {
        return this.mStarted;
    }

    public void removeRendererController(RendererController paramRendererController)
    {
        synchronized (this.mRendererControllerList)
        {
            this.mRendererControllerList.remove(paramRendererController);
            paramRendererController.setRenderThread(null);
            return;
        }
    }

    public void run()
    {
        Log.i("RenderThread", "RenderThread started");
        long l;
        float f1;
        int i;
        try
        {
            doInit();
            this.mStarted = true;
            if ((this.mStop) || (this.mPaused));
            synchronized (this.mResumeSignal)
            {
                if (this.mPaused)
                {
                    doPause();
                    Log.i("RenderThread", "RenderThread paused, waiting for signal");
                    waiteForResume();
                    Log.i("RenderThread", "RenderThread resumed");
                    doResume();
                }
                boolean bool = this.mStop;
                if (bool)
                {
                    doFinish();
                    Log.i("RenderThread", "RenderThread stopped");
                    return;
                }
            }
        }
        catch (Exception localException)
        {
            while (true)
            {
                localException.printStackTrace();
                Log.e("RenderThread", localException.toString());
                continue;
                l = SystemClock.elapsedRealtime();
                if (!doUpdateFramerate(l))
                    break;
                this.mPaused = true;
            }
        }
        catch (OutOfMemoryError localOutOfMemoryError)
        {
            while (true)
            {
                localOutOfMemoryError.printStackTrace();
                Log.e("RenderThread", localOutOfMemoryError.toString());
            }
            f1 = 0.0F;
            i = 0;
        }
        while (true)
        {
            synchronized (this.mRendererControllerList)
            {
                Iterator localIterator = this.mRendererControllerList.iterator();
                if (localIterator.hasNext())
                {
                    RendererController localRendererController = (RendererController)localIterator.next();
                    if (localRendererController.isSelfPaused())
                        continue;
                    int j = 0;
                    float f2 = localRendererController.getFramerate();
                    if (f2 > f1)
                        f1 = f2;
                    if (localRendererController.getCurFramerate() != f2)
                    {
                        if ((localRendererController.getCurFramerate() > 1.0F) && (f2 < 1.0F))
                            j = 1;
                        localRendererController.setCurFramerate(f2);
                        Log.d("RenderThread", "framerate changed: " + f2 + " at time: " + l);
                        if (f2 == 0.0F)
                            break label432;
                        f3 = 1000.0F / f2;
                        localRendererController.setFrameTime((int)f3);
                    }
                    if ((localRendererController.pendingRender()) || ((l - localRendererController.getLastUpdateTime() <= localRendererController.getFrameTime()) && (!localRendererController.shouldUpdate()) && (j == 0)))
                        continue;
                    localRendererController.tick(l);
                    localRendererController.doRender();
                    localRendererController.setLastUpdateTime(l);
                    i = 1;
                    continue;
                }
                if (i != 0)
                    break;
                sleepForFramerate(f1);
            }
            label432: float f3 = 2.147484E+09F;
        }
    }

    public void setPaused(boolean paramBoolean)
    {
        synchronized (this.mResumeSignal)
        {
            this.mPaused = paramBoolean;
            if (!paramBoolean)
                this.mResumeSignal.notify();
            return;
        }
    }

    public void setStop()
    {
        this.mStop = true;
        setPaused(false);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.RenderThread
 * JD-Core Version:        0.6.2
 */