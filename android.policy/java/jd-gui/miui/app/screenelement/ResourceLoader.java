package miui.app.screenelement;

import android.graphics.BitmapFactory.Options;
import android.os.MemoryFile;
import android.text.TextUtils;
import java.util.Locale;
import org.w3c.dom.Element;

public abstract class ResourceLoader
{
    protected String mLanguageCountrySuffix;
    protected String mLanguageSuffix;

    public abstract ResourceManager.BitmapInfo getBitmapInfo(String paramString, BitmapFactory.Options paramOptions);

    public abstract MemoryFile getFile(String paramString);

    public abstract Element getManifestRoot();

    public ResourceLoader setLocal(Locale paramLocale)
    {
        if (paramLocale != null)
        {
            this.mLanguageSuffix = paramLocale.getLanguage();
            this.mLanguageCountrySuffix = paramLocale.toString();
            if (TextUtils.equals(this.mLanguageSuffix, this.mLanguageCountrySuffix))
                this.mLanguageSuffix = null;
        }
        return this;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.ResourceLoader
 * JD-Core Version:        0.6.2
 */