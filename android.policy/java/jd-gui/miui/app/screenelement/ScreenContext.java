package miui.app.screenelement;

import android.content.Context;
import android.os.Handler;
import miui.app.screenelement.data.Variables;
import miui.app.screenelement.elements.ScreenElementFactory;

public class ScreenContext
{
    public static final String MAML_PREFERENCES = "MamlPreferences";
    private Context mApplicationContext;
    public final Context mContext;
    private RendererController mController;
    public final ScreenElementFactory mFactory;
    private boolean mGotApplicationContext;
    public Handler mHandler = new Handler();
    public final ResourceManager mResourceManager;
    public Variables mVariables = new Variables();

    public ScreenContext(Context paramContext, ResourceLoader paramResourceLoader)
    {
        this(paramContext, new ResourceManager(paramResourceLoader), new ScreenElementFactory());
    }

    public ScreenContext(Context paramContext, ResourceLoader paramResourceLoader, ScreenElementFactory paramScreenElementFactory)
    {
        this(paramContext, new ResourceManager(paramResourceLoader), paramScreenElementFactory);
    }

    public ScreenContext(Context paramContext, ResourceManager paramResourceManager)
    {
        this(paramContext, paramResourceManager, new ScreenElementFactory());
    }

    public ScreenContext(Context paramContext, ResourceManager paramResourceManager, ScreenElementFactory paramScreenElementFactory)
    {
        this.mContext = paramContext;
        this.mResourceManager = paramResourceManager;
        this.mFactory = paramScreenElementFactory;
    }

    public FramerateTokenList.FramerateToken createToken(String paramString)
    {
        if (this.mController == null);
        for (FramerateTokenList.FramerateToken localFramerateToken = null; ; localFramerateToken = this.mController.createToken(paramString))
            return localFramerateToken;
    }

    public void doneRender()
    {
        if (this.mController != null)
            this.mController.doneRender();
    }

    public Context getRawContext()
    {
        Context localContext;
        if (this.mApplicationContext != null)
            localContext = this.mApplicationContext;
        while (true)
        {
            return localContext;
            if (!this.mGotApplicationContext)
            {
                this.mApplicationContext = this.mContext.getApplicationContext();
                this.mGotApplicationContext = true;
                if (this.mApplicationContext != null)
                    localContext = this.mApplicationContext;
            }
            else
            {
                localContext = this.mContext;
            }
        }
    }

    public RendererController getRenderController()
    {
        return this.mController;
    }

    public void requestUpdate()
    {
        if (this.mController != null)
            this.mController.requestUpdate();
    }

    public void setExtraResource(int paramInt)
    {
        this.mResourceManager.setExtraResource(paramInt);
    }

    public void setRenderController(RendererController paramRendererController)
    {
        this.mController = paramRendererController;
    }

    public void setResourceDensity(int paramInt)
    {
        this.mResourceManager.setResourceDensity(paramInt);
    }

    public void setTargetDensity(int paramInt)
    {
        this.mResourceManager.setTargetDensity(paramInt);
    }

    public boolean shouldUpdate()
    {
        if (this.mController != null);
        for (boolean bool = this.mController.shouldUpdate(); ; bool = false)
            return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.ScreenContext
 * JD-Core Version:        0.6.2
 */