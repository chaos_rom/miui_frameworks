package miui.app.screenelement.elements;

import android.graphics.Canvas;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import java.util.ArrayList;
import java.util.Iterator;
import miui.app.screenelement.CommandTrigger;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.ScreenElementRoot;
import miui.app.screenelement.util.Utils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ButtonScreenElement extends AnimatedScreenElement
{
    private static final String LOG_TAG = "ButtonScreenElement";
    public static final String TAG_NAME = "Button";
    private ButtonActionListener mListener;
    private String mListenerName;
    private ElementGroup mNormalElements;
    private boolean mPressed;
    private ElementGroup mPressedElements;
    private float mPreviousTapPositionX;
    private float mPreviousTapPositionY;
    private long mPreviousTapUpTime;
    private boolean mTouching;
    private ArrayList<CommandTrigger> mTriggers = new ArrayList();

    public ButtonScreenElement(Element paramElement, ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot)
        throws ScreenElementLoadException
    {
        super(paramElement, paramScreenContext, paramScreenElementRoot);
        load(paramElement, paramScreenElementRoot);
        if (paramElement != null)
            this.mListenerName = paramElement.getAttribute("listener");
    }

    private ElementGroup getCur()
    {
        if ((this.mPressed) && (this.mTouching) && (this.mPressedElements != null));
        for (ElementGroup localElementGroup = this.mPressedElements; ; localElementGroup = this.mNormalElements)
            return localElementGroup;
    }

    private void onCancel()
    {
        performAction(ButtonAction.Cancel);
    }

    private void performAction(ButtonAction paramButtonAction)
    {
        Iterator localIterator = this.mTriggers.iterator();
        while (localIterator.hasNext())
        {
            CommandTrigger localCommandTrigger = (CommandTrigger)localIterator.next();
            if (localCommandTrigger.getAction() == paramButtonAction)
                localCommandTrigger.perform();
        }
        this.mRoot.onButtonInteractive(this, paramButtonAction);
    }

    public void doRender(Canvas paramCanvas)
    {
        ElementGroup localElementGroup = getCur();
        if (localElementGroup != null)
            localElementGroup.render(paramCanvas);
    }

    public void finish()
    {
        if (this.mNormalElements != null)
            this.mNormalElements.finish();
        if (this.mPressedElements != null)
            this.mPressedElements.finish();
        Iterator localIterator = this.mTriggers.iterator();
        while (localIterator.hasNext())
            ((CommandTrigger)localIterator.next()).finish();
    }

    public void init()
    {
        super.init();
        if (this.mNormalElements != null)
            this.mNormalElements.init();
        if (this.mPressedElements != null)
            this.mPressedElements.init();
        Iterator localIterator = this.mTriggers.iterator();
        while (localIterator.hasNext())
            ((CommandTrigger)localIterator.next()).init();
        ScreenElement localScreenElement;
        if ((this.mListener == null) && (!TextUtils.isEmpty(this.mListenerName)))
            localScreenElement = this.mRoot.findElement(this.mListenerName);
        try
        {
            this.mListener = ((ButtonActionListener)localScreenElement);
            return;
        }
        catch (ClassCastException localClassCastException)
        {
            while (true)
                Log.e("ButtonScreenElement", "button listener designated by the name is not actually a listener: " + this.mListenerName);
        }
    }

    public void load(Element paramElement, ScreenElementRoot paramScreenElementRoot)
        throws ScreenElementLoadException
    {
        if (paramElement == null)
        {
            Log.e("ButtonScreenElement", "node is null");
            throw new ScreenElementLoadException("node is null");
        }
        Element localElement1 = Utils.getChild(paramElement, "Normal");
        if (localElement1 != null)
            this.mNormalElements = new ElementGroup(localElement1, this.mContext, paramScreenElementRoot);
        Element localElement2 = Utils.getChild(paramElement, "Pressed");
        if (localElement2 != null)
            this.mPressedElements = new ElementGroup(localElement2, this.mContext, paramScreenElementRoot);
        Element localElement3 = Utils.getChild(paramElement, "Triggers");
        if (localElement3 != null)
        {
            NodeList localNodeList = localElement3.getChildNodes();
            int i = 0;
            if (i < localNodeList.getLength())
            {
                Element localElement4;
                if (localNodeList.item(i).getNodeType() == 1)
                {
                    localElement4 = (Element)localNodeList.item(i);
                    if (localElement4.getNodeName().equals("Trigger"))
                        break label171;
                }
                while (true)
                {
                    i++;
                    break;
                    label171: this.mTriggers.add(new CommandTrigger(this.mContext, localElement4, paramScreenElementRoot));
                }
            }
        }
    }

    public boolean onTouch(MotionEvent paramMotionEvent)
    {
        boolean bool;
        if (!isVisible())
            bool = false;
        while (true)
        {
            return bool;
            float f1 = paramMotionEvent.getX();
            float f2 = paramMotionEvent.getY();
            bool = false;
            switch (paramMotionEvent.getActionMasked())
            {
            default:
                break;
            case 0:
                if (touched(f1, f2))
                {
                    this.mPressed = true;
                    this.mTouching = true;
                    if (this.mListener != null)
                        this.mListener.onButtonDown(this.mName);
                    performAction(ButtonAction.Down);
                    if (SystemClock.uptimeMillis() - this.mPreviousTapUpTime <= ViewConfiguration.getDoubleTapTimeout())
                    {
                        float f3 = f1 - this.mPreviousTapPositionX;
                        float f4 = f2 - this.mPreviousTapPositionY;
                        float f5 = f3 * f3 + f4 * f4;
                        int i = ViewConfiguration.get(this.mContext.mContext).getScaledDoubleTapSlop();
                        if (f5 < i * i)
                        {
                            if (this.mListener != null)
                                this.mListener.onButtonDoubleClick(this.mName);
                            performAction(ButtonAction.Double);
                        }
                    }
                    this.mPreviousTapPositionX = f1;
                    this.mPreviousTapPositionY = f2;
                    if (this.mPressedElements != null)
                        this.mPressedElements.reset();
                    bool = true;
                }
                break;
            case 2:
                this.mTouching = touched(f1, f2);
                bool = this.mTouching;
                break;
            case 1:
                if (this.mPressed)
                {
                    if (touched(f1, f2))
                    {
                        if (this.mListener != null)
                            this.mListener.onButtonUp(this.mName);
                        performAction(ButtonAction.Up);
                        this.mPreviousTapUpTime = SystemClock.uptimeMillis();
                    }
                    while (true)
                    {
                        if (this.mNormalElements != null)
                            this.mNormalElements.reset();
                        this.mPressed = false;
                        this.mTouching = false;
                        bool = true;
                        break;
                        onCancel();
                    }
                }
                break;
            case 3:
            case 4:
                if (this.mNormalElements != null)
                    this.mNormalElements.reset();
                onCancel();
                this.mTouching = false;
                this.mPressed = false;
            }
        }
    }

    public void pause()
    {
        if (this.mNormalElements != null)
            this.mNormalElements.pause();
        if (this.mPressedElements != null)
            this.mPressedElements.pause();
        Iterator localIterator = this.mTriggers.iterator();
        while (localIterator.hasNext())
            ((CommandTrigger)localIterator.next()).pause();
        this.mPressed = false;
    }

    public void reset(long paramLong)
    {
        super.reset(paramLong);
        if (this.mNormalElements != null)
            this.mNormalElements.reset(paramLong);
        if (this.mPressedElements != null)
            this.mPressedElements.reset(paramLong);
    }

    public void resume()
    {
        if (this.mNormalElements != null)
            this.mNormalElements.resume();
        if (this.mPressedElements != null)
            this.mPressedElements.resume();
        Iterator localIterator = this.mTriggers.iterator();
        while (localIterator.hasNext())
            ((CommandTrigger)localIterator.next()).resume();
    }

    public void setListener(ButtonActionListener paramButtonActionListener)
    {
        this.mListener = paramButtonActionListener;
    }

    public void showCategory(String paramString, boolean paramBoolean)
    {
        if (this.mNormalElements != null)
            this.mNormalElements.showCategory(paramString, paramBoolean);
        if (this.mPressedElements != null)
            this.mPressedElements.showCategory(paramString, paramBoolean);
    }

    public void tick(long paramLong)
    {
        super.tick(paramLong);
        if (!isVisible());
        while (true)
        {
            return;
            ElementGroup localElementGroup = getCur();
            if (localElementGroup != null)
                localElementGroup.tick(paramLong);
        }
    }

    public boolean touched(float paramFloat1, float paramFloat2)
    {
        float f1;
        float f2;
        if (this.mParent != null)
        {
            f1 = this.mParent.getX();
            if (this.mParent == null)
                break label102;
            f2 = this.mParent.getY();
            label31: float f3 = getX();
            float f4 = getY();
            if ((paramFloat1 < f1 + f3) || (paramFloat1 > f1 + f3 + getWidth()) || (paramFloat2 < f2 + f4) || (paramFloat2 > f2 + f4 + getHeight()))
                break label108;
        }
        label102: label108: for (boolean bool = true; ; bool = false)
        {
            return bool;
            f1 = 0.0F;
            break;
            f2 = 0.0F;
            break label31;
        }
    }

    public static enum ButtonAction
    {
        static
        {
            Double = new ButtonAction("Double", 2);
            Long = new ButtonAction("Long", 3);
            Cancel = new ButtonAction("Cancel", 4);
            Other = new ButtonAction("Other", 5);
            ButtonAction[] arrayOfButtonAction = new ButtonAction[6];
            arrayOfButtonAction[0] = Down;
            arrayOfButtonAction[1] = Up;
            arrayOfButtonAction[2] = Double;
            arrayOfButtonAction[3] = Long;
            arrayOfButtonAction[4] = Cancel;
            arrayOfButtonAction[5] = Other;
        }
    }

    public static abstract interface ButtonActionListener
    {
        public abstract boolean onButtonDoubleClick(String paramString);

        public abstract boolean onButtonDown(String paramString);

        public abstract boolean onButtonLongClick(String paramString);

        public abstract boolean onButtonUp(String paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.elements.ButtonScreenElement
 * JD-Core Version:        0.6.2
 */