package miui.app.screenelement.elements;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.PorterDuff.Mode;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.ScreenElementRoot;
import miui.app.screenelement.util.Utils;
import org.w3c.dom.Element;

public class VirtualScreen extends ElementGroup
{
    public static final String TAG_NAME = "VirtualScreen";
    private Bitmap mScreenBitmap;
    private Canvas mScreenCanvas;

    public VirtualScreen(Element paramElement, ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot)
        throws ScreenElementLoadException
    {
        super(paramElement, paramScreenContext, paramScreenElementRoot);
    }

    public void doRender(Canvas paramCanvas)
    {
        this.mScreenCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
        super.doRender(this.mScreenCanvas);
    }

    public void finish()
    {
        this.mScreenBitmap.recycle();
    }

    public Bitmap getBitmap()
    {
        return this.mScreenBitmap;
    }

    public void init()
    {
        super.init();
        float f1 = getWidth();
        if (f1 < 0.0F)
            f1 = scale(Utils.getVariableNumber("screen_width", this.mContext.mVariables));
        float f2 = getHeight();
        if (f2 < 0.0F)
            f2 = scale(Utils.getVariableNumber("screen_height", this.mContext.mVariables));
        this.mScreenBitmap = Bitmap.createBitmap(Math.round(f1), Math.round(f2), Bitmap.Config.ARGB_8888);
        this.mScreenBitmap.setDensity(this.mRoot.getTargetDensity());
        this.mScreenCanvas = new Canvas(this.mScreenBitmap);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.elements.VirtualScreen
 * JD-Core Version:        0.6.2
 */