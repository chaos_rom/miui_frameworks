package miui.app.screenelement.elements;

import android.graphics.Canvas;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.MotionEvent;
import miui.app.screenelement.FramerateTokenList.FramerateToken;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementRoot;
import miui.app.screenelement.data.Expression;
import miui.app.screenelement.util.IndexedNumberVariable;
import org.w3c.dom.Element;

public abstract class ScreenElement
{
    public static final String ACTUAL_H = "actual_h";
    public static final String ACTUAL_W = "actual_w";
    public static final String ACTUAL_X = "actual_x";
    public static final String ACTUAL_Y = "actual_y";
    public static final String VISIBILITY = "visibility";
    public static final int VISIBILITY_FALSE = 0;
    public static final int VISIBILITY_TRUE = 1;
    private IndexedNumberVariable mActualHeightVar;
    private IndexedNumberVariable mActualWidthVar;
    protected Align mAlign;
    protected AlignV mAlignV;
    protected String mCategory;
    protected ScreenContext mContext;
    private FramerateTokenList.FramerateToken mFramerateToken;
    protected boolean mHasName;
    protected boolean mIsVisible = true;
    protected String mName;
    protected ElementGroup mParent;
    protected ScreenElementRoot mRoot;
    private boolean mShow = true;
    private Expression mVisibilityExpression;
    private IndexedNumberVariable mVisibilityVar;

    public ScreenElement(Element paramElement, ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot)
    {
        this.mContext = paramScreenContext;
        this.mRoot = paramScreenElementRoot;
        boolean bool;
        String str1;
        label104: String str2;
        label138: String str3;
        if (paramElement != null)
        {
            this.mCategory = paramElement.getAttribute("category");
            this.mName = paramElement.getAttribute("name");
            if (TextUtils.isEmpty(this.mName))
                break label173;
            bool = true;
            this.mHasName = bool;
            str1 = paramElement.getAttribute("visibility");
            if (!TextUtils.isEmpty(str1))
            {
                if (!str1.equalsIgnoreCase("false"))
                    break label179;
                this.mShow = false;
            }
            this.mAlign = Align.LEFT;
            str2 = paramElement.getAttribute("align");
            if (!str2.equalsIgnoreCase("right"))
                break label209;
            this.mAlign = Align.RIGHT;
            this.mAlignV = AlignV.TOP;
            str3 = paramElement.getAttribute("alignV");
            if (!str3.equalsIgnoreCase("bottom"))
                break label249;
            this.mAlignV = AlignV.BOTTOM;
        }
        while (true)
        {
            return;
            label173: bool = false;
            break;
            label179: if (str1.equalsIgnoreCase("true"))
            {
                this.mShow = true;
                break label104;
            }
            this.mVisibilityExpression = Expression.build(str1);
            break label104;
            label209: if (str2.equalsIgnoreCase("left"))
            {
                this.mAlign = Align.LEFT;
                break label138;
            }
            if (!str2.equalsIgnoreCase("center"))
                break label138;
            this.mAlign = Align.CENTER;
            break label138;
            label249: if (str3.equalsIgnoreCase("top"))
                this.mAlignV = AlignV.TOP;
            else if (str3.equalsIgnoreCase("center"))
                this.mAlignV = AlignV.CENTER;
        }
    }

    protected float descale(float paramFloat)
    {
        return paramFloat / this.mRoot.getScale();
    }

    public abstract void doRender(Canvas paramCanvas);

    public ScreenElement findElement(String paramString)
    {
        if (this.mName.equals(paramString));
        while (true)
        {
            return this;
            this = null;
        }
    }

    public void finish()
    {
    }

    protected float getFramerate()
    {
        if (this.mFramerateToken == null);
        for (float f = 0.0F; ; f = this.mFramerateToken.getFramerate())
            return f;
    }

    protected float getLeft(float paramFloat1, float paramFloat2)
    {
        if (paramFloat2 <= 0.0F)
            return paramFloat1;
        float f = paramFloat1;
        switch (1.$SwitchMap$miui$app$screenelement$elements$ScreenElement$Align[this.mAlign.ordinal()])
        {
        default:
        case 1:
        case 2:
        }
        while (true)
        {
            paramFloat1 = f;
            break;
            f -= paramFloat2 / 2.0F;
            continue;
            f -= paramFloat2;
        }
    }

    public String getName()
    {
        return this.mName;
    }

    protected float getTop(float paramFloat1, float paramFloat2)
    {
        if (paramFloat2 <= 0.0F)
            return paramFloat1;
        float f = paramFloat1;
        switch (1.$SwitchMap$miui$app$screenelement$elements$ScreenElement$AlignV[this.mAlignV.ordinal()])
        {
        default:
        case 1:
        case 2:
        }
        while (true)
        {
            paramFloat1 = f;
            break;
            f -= paramFloat2 / 2.0F;
            continue;
            f -= paramFloat2;
        }
    }

    public void init()
    {
        updateVisibility();
        if (isVisible())
            onVisibilityChange(true);
    }

    public boolean isVisible()
    {
        return this.mIsVisible;
    }

    protected boolean isVisibleInner()
    {
        if (this.mShow)
            if (this.mVisibilityExpression != null);
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            if (this.mVisibilityExpression.evaluate(this.mContext.mVariables) > 0.0D)
                break;
        }
    }

    public boolean onTouch(MotionEvent paramMotionEvent)
    {
        return false;
    }

    protected void onVisibilityChange(boolean paramBoolean)
    {
        IndexedNumberVariable localIndexedNumberVariable;
        if (this.mHasName)
        {
            if (this.mVisibilityVar == null)
                this.mVisibilityVar = new IndexedNumberVariable(this.mName, "visibility", this.mContext.mVariables);
            localIndexedNumberVariable = this.mVisibilityVar;
            if (!paramBoolean)
                break label55;
        }
        label55: for (double d = 1.0D; ; d = 0.0D)
        {
            localIndexedNumberVariable.set(d);
            return;
        }
    }

    public void pause()
    {
    }

    public void render(Canvas paramCanvas)
    {
        updateVisibility();
        if (!this.mIsVisible);
        while (true)
        {
            return;
            doRender(paramCanvas);
        }
    }

    protected void requestFramerate(float paramFloat)
    {
        if (paramFloat < 0.0F);
        while (true)
        {
            return;
            if (this.mFramerateToken == null)
            {
                if (paramFloat != 0.0F)
                    this.mFramerateToken = this.mContext.createToken(toString());
            }
            else if (this.mFramerateToken != null)
                this.mFramerateToken.requestFramerate(paramFloat);
        }
    }

    public void requestUpdate()
    {
        this.mContext.requestUpdate();
    }

    public final void reset()
    {
        reset(SystemClock.elapsedRealtime());
    }

    public void reset(long paramLong)
    {
    }

    public void resume()
    {
        updateVisibility();
    }

    protected float scale(double paramDouble)
    {
        return (float)(paramDouble * this.mRoot.getScale());
    }

    protected void setActualHeight(double paramDouble)
    {
        if (!this.mHasName);
        while (true)
        {
            return;
            if (this.mActualHeightVar == null)
                this.mActualHeightVar = new IndexedNumberVariable(this.mName, "actual_h", this.mContext.mVariables);
            this.mActualHeightVar.set(paramDouble);
        }
    }

    protected void setActualWidth(double paramDouble)
    {
        if (!this.mHasName);
        while (true)
        {
            return;
            if (this.mActualWidthVar == null)
                this.mActualWidthVar = new IndexedNumberVariable(this.mName, "actual_w", this.mContext.mVariables);
            this.mActualWidthVar.set(paramDouble);
        }
    }

    public void setParent(ElementGroup paramElementGroup)
    {
        this.mParent = paramElementGroup;
    }

    public void show(boolean paramBoolean)
    {
        this.mShow = paramBoolean;
        updateVisibility();
    }

    public void showCategory(String paramString, boolean paramBoolean)
    {
        if (this.mCategory.equals(paramString))
            show(paramBoolean);
    }

    public void tick(long paramLong)
    {
    }

    protected void updateVisibility()
    {
        boolean bool = isVisibleInner();
        if (this.mIsVisible != bool)
        {
            this.mIsVisible = bool;
            onVisibilityChange(this.mIsVisible);
        }
    }

    protected static enum Align
    {
        static
        {
            CENTER = new Align("CENTER", 1);
            RIGHT = new Align("RIGHT", 2);
            Align[] arrayOfAlign = new Align[3];
            arrayOfAlign[0] = LEFT;
            arrayOfAlign[1] = CENTER;
            arrayOfAlign[2] = RIGHT;
        }
    }

    protected static enum AlignV
    {
        static
        {
            CENTER = new AlignV("CENTER", 1);
            BOTTOM = new AlignV("BOTTOM", 2);
            AlignV[] arrayOfAlignV = new AlignV[3];
            arrayOfAlignV[0] = TOP;
            arrayOfAlignV[1] = CENTER;
            arrayOfAlignV[2] = BOTTOM;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.elements.ScreenElement
 * JD-Core Version:        0.6.2
 */