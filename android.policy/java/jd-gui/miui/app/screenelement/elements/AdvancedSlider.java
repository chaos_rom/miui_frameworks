package miui.app.screenelement.elements;

import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Canvas;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import java.util.ArrayList;
import java.util.Iterator;
import miui.app.screenelement.ActionCommand;
import miui.app.screenelement.CommandTrigger;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.ScreenElementRoot;
import miui.app.screenelement.data.Expression;
import miui.app.screenelement.util.IndexedNumberVariable;
import miui.app.screenelement.util.Task;
import miui.app.screenelement.util.Utils;
import miui.app.screenelement.util.Utils.Point;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class AdvancedSlider extends ScreenElement
{
    private static final boolean DEBUG = false;
    private static final int DEFAULT_DRAG_TOLERANCE = 150;
    private static final float FREE_ENDPOINT_DIST = 1.701412E+38F;
    private static final String LOG_TAG = "LockScreen_AdvancedSlider";
    public static final String MOVE_DIST = "move_dist";
    public static final String MOVE_X = "move_x";
    public static final String MOVE_Y = "move_y";
    private static final float NONE_ENDPOINT_DIST = 3.4028235E+38F;
    public static final int SLIDER_STATE_NORMAL = 0;
    public static final int SLIDER_STATE_PRESSED = 1;
    public static final int SLIDER_STATE_REACHED = 2;
    public static final String STATE = "state";
    public static final String TAG_NAME = "Slider";
    private BounceAnimationController mBounceAnimationController = new BounceAnimationController(null);
    private EndPoint mCurrentEndPoint;
    private ArrayList<EndPoint> mEndPoints = new ArrayList();
    private IndexedNumberVariable mMoveDistVar;
    private IndexedNumberVariable mMoveXVar;
    private IndexedNumberVariable mMoveYVar;
    private boolean mMoving;
    private OnLaunchListener mOnLaunchListener;
    private boolean mPressed;
    private StartPoint mStartPoint;
    private IndexedNumberVariable mStateVar;
    private float mTouchOffsetX;
    private float mTouchOffsetY;

    static
    {
        if (!AdvancedSlider.class.desiredAssertionStatus());
        for (boolean bool = true; ; bool = false)
        {
            $assertionsDisabled = bool;
            return;
        }
    }

    public AdvancedSlider(Element paramElement, ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot)
        throws ScreenElementLoadException
    {
        super(paramElement, paramScreenContext, paramScreenElementRoot);
        if (this.mHasName)
        {
            this.mStateVar = new IndexedNumberVariable(this.mName, "state", this.mContext.mVariables);
            this.mMoveXVar = new IndexedNumberVariable(this.mName, "move_x", this.mContext.mVariables);
            this.mMoveYVar = new IndexedNumberVariable(this.mName, "move_y", this.mContext.mVariables);
            this.mMoveDistVar = new IndexedNumberVariable(this.mName, "move_dist", this.mContext.mVariables);
        }
        load(paramElement);
    }

    private void cancelMoving()
    {
        this.mPressed = false;
        this.mStartPoint.moveTo(this.mStartPoint.getX(), this.mStartPoint.getY());
        this.mStartPoint.setState(State.Normal);
        Iterator localIterator = this.mEndPoints.iterator();
        while (localIterator.hasNext())
            ((EndPoint)localIterator.next()).setState(State.Normal);
        if (this.mHasName)
        {
            this.mMoveXVar.set(0.0D);
            this.mMoveYVar.set(0.0D);
            this.mMoveDistVar.set(0.0D);
            this.mStateVar.set(0.0D);
        }
        this.mMoving = false;
        requestUpdate();
    }

    private boolean checkEndPoint(Utils.Point paramPoint, EndPoint paramEndPoint)
    {
        boolean bool = false;
        if (paramEndPoint.touched((float)paramPoint.x, (float)paramPoint.y))
        {
            if (paramEndPoint.getState() != State.Reached)
            {
                paramEndPoint.setState(State.Reached);
                Iterator localIterator = this.mEndPoints.iterator();
                while (localIterator.hasNext())
                {
                    EndPoint localEndPoint = (EndPoint)localIterator.next();
                    if (localEndPoint != paramEndPoint)
                        localEndPoint.setState(State.Pressed);
                }
                onReach(paramEndPoint.mName);
            }
            bool = true;
        }
        while (true)
        {
            return bool;
            paramEndPoint.setState(State.Pressed);
        }
    }

    private CheckTouchResult checkTouch(float paramFloat1, float paramFloat2)
    {
        float f1 = 3.4028235E+38F;
        Object localObject = null;
        CheckTouchResult localCheckTouchResult = new CheckTouchResult(null);
        Iterator localIterator1 = this.mEndPoints.iterator();
        while (localIterator1.hasNext())
        {
            EndPoint localEndPoint2 = (EndPoint)localIterator1.next();
            Utils.Point localPoint = localEndPoint2.getNearestPoint(paramFloat1, paramFloat2);
            float f2 = localEndPoint2.getTransformedDist(localPoint, paramFloat1, paramFloat2);
            if (f2 < f1)
            {
                f1 = f2;
                localObject = localPoint;
                localCheckTouchResult.endPoint = localEndPoint2;
            }
        }
        boolean bool = false;
        label138: State localState;
        label154: IndexedNumberVariable localIndexedNumberVariable;
        if (f1 < 3.4028235E+38F)
        {
            moveStartPoint((float)localObject.x, (float)localObject.y);
            if (f1 < 1.701412E+38F)
            {
                bool = checkEndPoint(localObject, localCheckTouchResult.endPoint);
                break label210;
                break label210;
                StartPoint localStartPoint = this.mStartPoint;
                if (!bool)
                    break label280;
                localState = State.Reached;
                localStartPoint.setState(localState);
                if (this.mHasName)
                {
                    localIndexedNumberVariable = this.mStateVar;
                    if (!bool)
                        break label288;
                }
            }
        }
        label280: label288: for (double d = 2.0D; ; d = 1.0D)
        {
            localIndexedNumberVariable.set(d);
            localCheckTouchResult.reached = bool;
            while (true)
            {
                return localCheckTouchResult;
                Iterator localIterator2 = this.mEndPoints.iterator();
                label210: if (!localIterator2.hasNext())
                    break label138;
                EndPoint localEndPoint1 = (EndPoint)localIterator2.next();
                if (localEndPoint1.mPath != null)
                    break;
                bool = checkEndPoint(localObject, localEndPoint1);
                if (!bool)
                    break;
                localCheckTouchResult.endPoint = localEndPoint1;
                break label138;
                Log.i("LockScreen_AdvancedSlider", "unlock touch canceled due to exceeding tollerance");
                localCheckTouchResult = null;
            }
            localState = State.Pressed;
            break label154;
        }
    }

    private boolean doLaunch(EndPoint paramEndPoint)
    {
        Intent localIntent = null;
        if (paramEndPoint.mAction != null)
            localIntent = paramEndPoint.mAction.perform();
        return onLaunch(paramEndPoint.mName, localIntent);
    }

    private void loadEndPoint(Element paramElement)
        throws ScreenElementLoadException
    {
        this.mEndPoints.clear();
        NodeList localNodeList = paramElement.getElementsByTagName("EndPoint");
        for (int i = 0; i < localNodeList.getLength(); i++)
        {
            Element localElement = (Element)localNodeList.item(i);
            this.mEndPoints.add(new EndPoint(localElement));
        }
        if (!this.mEndPoints.isEmpty());
        for (boolean bool = true; ; bool = false)
        {
            Utils.asserts(bool, "no end point for unlocker!");
            return;
        }
    }

    private void loadStartPoint(Element paramElement)
        throws ScreenElementLoadException
    {
        Element localElement = Utils.getChild(paramElement, "StartPoint");
        if (localElement != null);
        for (boolean bool = true; ; bool = false)
        {
            Utils.asserts(bool, "no StartPoint node");
            this.mStartPoint = new StartPoint(localElement);
            return;
        }
    }

    private void moveStartPoint(float paramFloat1, float paramFloat2)
    {
        this.mStartPoint.moveTo(paramFloat1, paramFloat2);
        if (this.mHasName)
        {
            double d1 = descale(this.mStartPoint.mCurrentX) - this.mStartPoint.mX.evaluate(this.mContext.mVariables);
            double d2 = descale(this.mStartPoint.mCurrentY) - this.mStartPoint.mY.evaluate(this.mContext.mVariables);
            double d3 = Math.sqrt(d1 * d1 + d2 * d2);
            this.mMoveXVar.set(d1);
            this.mMoveYVar.set(d2);
            this.mMoveDistVar.set(d3);
        }
    }

    public void doRender(Canvas paramCanvas)
    {
        Iterator localIterator = this.mEndPoints.iterator();
        while (localIterator.hasNext())
            ((EndPoint)localIterator.next()).render(paramCanvas);
        this.mStartPoint.render(paramCanvas);
    }

    public ScreenElement findElement(String paramString)
    {
        ScreenElement localScreenElement1 = super.findElement(paramString);
        Object localObject;
        if (localScreenElement1 != null)
            localObject = localScreenElement1;
        while (true)
        {
            return localObject;
            ScreenElement localScreenElement2 = this.mStartPoint.findElement(paramString);
            if (localScreenElement2 != null)
            {
                localObject = localScreenElement2;
            }
            else
            {
                Iterator localIterator = this.mEndPoints.iterator();
                while (true)
                    if (localIterator.hasNext())
                    {
                        ScreenElement localScreenElement3 = ((EndPoint)localIterator.next()).findElement(paramString);
                        if (localScreenElement3 != null)
                        {
                            localObject = localScreenElement3;
                            break;
                        }
                    }
                localObject = null;
            }
        }
    }

    public void finish()
    {
        super.finish();
        this.mStartPoint.finish();
        Iterator localIterator = this.mEndPoints.iterator();
        while (localIterator.hasNext())
            ((EndPoint)localIterator.next()).finish();
    }

    public void init()
    {
        super.init();
        this.mBounceAnimationController.init();
        this.mStartPoint.init();
        Iterator localIterator = this.mEndPoints.iterator();
        while (localIterator.hasNext())
            ((EndPoint)localIterator.next()).init();
    }

    public void load(Element paramElement)
        throws ScreenElementLoadException
    {
        assert (paramElement.getNodeName().equalsIgnoreCase("Slider"));
        this.mBounceAnimationController.load(paramElement);
        loadStartPoint(paramElement);
        loadEndPoint(paramElement);
    }

    protected void onCancel()
    {
        this.mRoot.haptic(1);
    }

    protected boolean onLaunch(String paramString, Intent paramIntent)
    {
        if (this.mOnLaunchListener != null)
            this.mOnLaunchListener.onLaunch(paramString);
        return false;
    }

    protected void onReach(String paramString)
    {
        this.mRoot.haptic(0);
    }

    protected void onStart()
    {
        this.mRoot.haptic(1);
    }

    public boolean onTouch(MotionEvent paramMotionEvent)
    {
        boolean bool1;
        if (!isVisible())
            bool1 = false;
        while (true)
        {
            return bool1;
            float f1 = paramMotionEvent.getX();
            float f2 = paramMotionEvent.getY();
            bool1 = false;
            switch (paramMotionEvent.getActionMasked())
            {
            default:
                break;
            case 0:
                if (this.mStartPoint.touched(f1, f2))
                {
                    this.mMoving = true;
                    this.mTouchOffsetX = (f1 - this.mStartPoint.getX());
                    this.mTouchOffsetY = (f2 - this.mStartPoint.getY());
                    this.mStartPoint.setState(State.Pressed);
                    Iterator localIterator = this.mEndPoints.iterator();
                    while (localIterator.hasNext())
                        ((EndPoint)localIterator.next()).setState(State.Pressed);
                    this.mPressed = true;
                    if (this.mHasName)
                        this.mStateVar.set(1.0D);
                    this.mBounceAnimationController.init();
                    onStart();
                    bool1 = true;
                }
                break;
            case 2:
                if (this.mMoving)
                {
                    CheckTouchResult localCheckTouchResult2 = checkTouch(f1, f2);
                    if (localCheckTouchResult2 != null)
                        this.mCurrentEndPoint = localCheckTouchResult2.endPoint;
                    while (true)
                    {
                        bool1 = true;
                        break;
                        this.mBounceAnimationController.startCancelMoving(this.mCurrentEndPoint);
                        this.mMoving = false;
                        onCancel();
                    }
                }
                break;
            case 1:
            case 3:
                boolean bool2 = false;
                if (this.mMoving)
                {
                    Log.i("LockScreen_AdvancedSlider", "unlock touch up");
                    CheckTouchResult localCheckTouchResult1 = checkTouch(f1, f2);
                    if (localCheckTouchResult1 != null)
                    {
                        if (localCheckTouchResult1.reached)
                            bool2 = doLaunch(localCheckTouchResult1.endPoint);
                        this.mCurrentEndPoint = localCheckTouchResult1.endPoint;
                    }
                    this.mMoving = false;
                    if (!bool2)
                    {
                        this.mBounceAnimationController.startCancelMoving(this.mCurrentEndPoint);
                        onCancel();
                    }
                    bool1 = true;
                }
                break;
            case 4:
                if (this.mMoving)
                {
                    this.mBounceAnimationController.startCancelMoving(null);
                    this.mCurrentEndPoint = null;
                    this.mMoving = false;
                    onCancel();
                    bool1 = true;
                }
                break;
            }
        }
    }

    public void pause()
    {
        super.pause();
        cancelMoving();
        this.mStartPoint.pause();
        Iterator localIterator = this.mEndPoints.iterator();
        while (localIterator.hasNext())
            ((EndPoint)localIterator.next()).pause();
    }

    public void reset(long paramLong)
    {
        super.reset(paramLong);
        this.mStartPoint.reset(paramLong);
        Iterator localIterator = this.mEndPoints.iterator();
        while (localIterator.hasNext())
            ((EndPoint)localIterator.next()).reset(paramLong);
    }

    public void resume()
    {
        super.resume();
        this.mStartPoint.resume();
        Iterator localIterator = this.mEndPoints.iterator();
        while (localIterator.hasNext())
            ((EndPoint)localIterator.next()).resume();
    }

    public void setOnLaunchListener(OnLaunchListener paramOnLaunchListener)
    {
        this.mOnLaunchListener = paramOnLaunchListener;
    }

    public void showCategory(String paramString, boolean paramBoolean)
    {
        this.mStartPoint.showCategory(paramString, paramBoolean);
        Iterator localIterator = this.mEndPoints.iterator();
        while (localIterator.hasNext())
            ((EndPoint)localIterator.next()).showCategory(paramString, paramBoolean);
    }

    public void tick(long paramLong)
    {
        super.tick(paramLong);
        if (!isVisible());
        while (true)
        {
            return;
            this.mBounceAnimationController.tick(paramLong);
            this.mStartPoint.tick(paramLong);
            Iterator localIterator = this.mEndPoints.iterator();
            while (localIterator.hasNext())
                ((EndPoint)localIterator.next()).tick(paramLong);
        }
    }

    private class CheckTouchResult
    {
        public AdvancedSlider.EndPoint endPoint;
        public boolean reached;

        private CheckTouchResult()
        {
        }
    }

    private class EndPoint extends AdvancedSlider.SliderPoint
    {
        public static final String TAG_NAME = "EndPoint";
        public AdvancedSlider.UnlockAction mAction;
        private ArrayList<AdvancedSlider.Position> mPath;
        private Expression mPathX;
        private Expression mPathY;
        private float mTolerance = 150.0F;

        public EndPoint(Element arg2)
            throws ScreenElementLoadException
        {
            super(localElement, "EndPoint");
            load(localElement);
        }

        private Utils.Point getNearestPoint(float paramFloat1, float paramFloat2)
        {
            Object localObject;
            if (this.mPath == null)
                localObject = new Utils.Point(paramFloat1 - AdvancedSlider.this.mTouchOffsetX, paramFloat2 - AdvancedSlider.this.mTouchOffsetY);
            while (true)
            {
                return localObject;
                localObject = null;
                double d1 = 1.7976931348623157E+308D;
                for (int i = 1; i < this.mPath.size(); i++)
                {
                    float f1 = paramFloat1 - AdvancedSlider.this.mTouchOffsetX;
                    float f2 = paramFloat2 - AdvancedSlider.this.mTouchOffsetY;
                    AdvancedSlider.Position localPosition1 = (AdvancedSlider.Position)this.mPath.get(i - 1);
                    AdvancedSlider.Position localPosition2 = (AdvancedSlider.Position)this.mPath.get(i);
                    Utils.Point localPoint1 = new Utils.Point(localPosition1.getX(), localPosition1.getY());
                    Utils.Point localPoint2 = new Utils.Point(localPosition2.getX(), localPosition2.getY());
                    Utils.Point localPoint3 = new Utils.Point(f1, f2);
                    Utils.Point localPoint4 = Utils.pointProjectionOnSegment(localPoint1, localPoint2, localPoint3, true);
                    double d2 = Utils.Dist(localPoint4, localPoint3, false);
                    if (d2 < d1)
                    {
                        d1 = d2;
                        localObject = localPoint4;
                    }
                }
            }
        }

        private void load(Element paramElement)
            throws ScreenElementLoadException
        {
            loadTask(paramElement);
            loadPath(paramElement);
        }

        private void loadPath(Element paramElement)
            throws ScreenElementLoadException
        {
            Element localElement1 = Utils.getChild(paramElement, "Path");
            if (localElement1 == null)
                this.mPath = null;
            while (true)
            {
                return;
                this.mTolerance = Utils.getAttrAsInt(localElement1, "tolerance", 150);
                this.mPath = new ArrayList();
                this.mPathX = Expression.build(localElement1.getAttribute("x"));
                this.mPathY = Expression.build(localElement1.getAttribute("y"));
                NodeList localNodeList = localElement1.getElementsByTagName("Position");
                for (int i = 0; i < localNodeList.getLength(); i++)
                {
                    Element localElement2 = (Element)localNodeList.item(i);
                    this.mPath.add(new AdvancedSlider.Position(AdvancedSlider.this, localElement2, this.mPathX, this.mPathY));
                }
            }
        }

        private void loadTask(Element paramElement)
        {
            Element localElement1 = Utils.getChild(paramElement, "Intent");
            Element localElement2 = Utils.getChild(paramElement, "Command");
            Element localElement3 = Utils.getChild(paramElement, "Trigger");
            if ((localElement1 == null) && (localElement2 == null) && (localElement3 == null));
            while (true)
            {
                return;
                this.mAction = new AdvancedSlider.UnlockAction(AdvancedSlider.this, null);
                if (localElement1 != null)
                {
                    this.mAction.mTask = Task.load(localElement1);
                    continue;
                }
                if (localElement2 != null)
                {
                    this.mAction.mCommand = ActionCommand.create(AdvancedSlider.this.mContext, localElement2, AdvancedSlider.this.mRoot);
                    if (this.mAction.mCommand != null)
                        continue;
                    Log.w("LockScreen_AdvancedSlider", "invalid Command element: " + localElement2.toString());
                    continue;
                }
                if (localElement3 == null)
                    continue;
                try
                {
                    this.mAction.mTrigger = new CommandTrigger(AdvancedSlider.this.mContext, localElement3, AdvancedSlider.this.mRoot);
                    if (this.mAction.mTrigger != null)
                        continue;
                    Log.w("LockScreen_AdvancedSlider", "invalid Trigger element: " + localElement3.toString());
                }
                catch (ScreenElementLoadException localScreenElementLoadException)
                {
                    while (true)
                        localScreenElementLoadException.printStackTrace();
                }
            }
        }

        public void finish()
        {
            super.finish();
            if (this.mAction != null)
                this.mAction.finish();
        }

        public float getTransformedDist(Utils.Point paramPoint, float paramFloat1, float paramFloat2)
        {
            float f3;
            if (this.mPath == null)
                f3 = 1.701412E+38F;
            while (true)
            {
                return f3;
                if (paramPoint == null)
                {
                    f3 = 3.4028235E+38F;
                }
                else
                {
                    float f1 = paramFloat1 - AdvancedSlider.this.mTouchOffsetX;
                    float f2 = paramFloat2 - AdvancedSlider.this.mTouchOffsetY;
                    f3 = (float)Utils.Dist(paramPoint, new Utils.Point(f1, f2), true);
                    if (f3 >= this.mTolerance)
                        f3 = 3.4028235E+38F;
                }
            }
        }

        public void init()
        {
            super.init();
            if (this.mAction != null)
                this.mAction.init();
            this.mTolerance = AdvancedSlider.this.scale(this.mTolerance);
        }

        protected void onStateChange(AdvancedSlider.State paramState1, AdvancedSlider.State paramState2)
        {
            if (paramState1 == AdvancedSlider.State.Invalid);
            while (true)
            {
                return;
                switch (AdvancedSlider.1.$SwitchMap$miui$app$screenelement$elements$AdvancedSlider$State[paramState2.ordinal()])
                {
                default:
                    break;
                case 3:
                    AdvancedSlider.this.mRoot.playSound(this.mReachedSound);
                }
            }
        }

        public void pause()
        {
            super.pause();
            if (this.mAction != null)
                this.mAction.pause();
        }

        public void resume()
        {
            super.resume();
            if (this.mAction != null)
                this.mAction.resume();
        }
    }

    private class UnlockAction
    {
        public ActionCommand mCommand;
        public boolean mConfigTaskLoaded;
        public Task mTask;
        public CommandTrigger mTrigger;

        private UnlockAction()
        {
        }

        private Intent performTask()
        {
            Intent localIntent;
            if (this.mTask == null)
                localIntent = null;
            while (true)
            {
                return localIntent;
                localIntent = null;
                if (!this.mConfigTaskLoaded)
                {
                    Task localTask = AdvancedSlider.this.mRoot.findTask(this.mTask.id);
                    if ((localTask != null) && (!TextUtils.isEmpty(localTask.action)))
                        this.mTask = localTask;
                    this.mConfigTaskLoaded = true;
                }
                if (!TextUtils.isEmpty(this.mTask.action))
                {
                    localIntent = new Intent(this.mTask.action);
                    if (!TextUtils.isEmpty(this.mTask.type))
                        localIntent.setType(this.mTask.type);
                    if (!TextUtils.isEmpty(this.mTask.category))
                        localIntent.addCategory(this.mTask.category);
                    if ((!TextUtils.isEmpty(this.mTask.packageName)) && (!TextUtils.isEmpty(this.mTask.className)))
                        localIntent.setComponent(new ComponentName(this.mTask.packageName, this.mTask.className));
                    localIntent.setFlags(872415232);
                }
            }
        }

        public void finish()
        {
            if (this.mCommand != null)
                this.mCommand.finish();
            if (this.mTrigger != null)
                this.mTrigger.finish();
        }

        public void init()
        {
            if (this.mCommand != null)
                this.mCommand.init();
            if (this.mTrigger != null)
                this.mTrigger.init();
        }

        public void pause()
        {
            if (this.mCommand != null)
                this.mCommand.pause();
            if (this.mTrigger != null)
                this.mTrigger.pause();
        }

        public Intent perform()
        {
            Intent localIntent;
            if (this.mTask != null)
            {
                localIntent = performTask();
                return localIntent;
            }
            if (this.mCommand != null)
                this.mCommand.perform();
            while (true)
            {
                localIntent = null;
                break;
                if (this.mTrigger != null)
                    this.mTrigger.perform();
            }
        }

        public void resume()
        {
            if (this.mCommand != null)
                this.mCommand.resume();
            if (this.mTrigger != null)
                this.mTrigger.resume();
        }
    }

    private class Position
    {
        public static final String TAG_NAME = "Position";
        private Expression mBaseX;
        private Expression mBaseY;
        private int x;
        private int y;

        public Position(Element paramExpression1, Expression paramExpression2, Expression arg4)
            throws ScreenElementLoadException
        {
            this.mBaseX = paramExpression2;
            Object localObject;
            this.mBaseY = localObject;
            load(paramExpression1);
        }

        public float getX()
        {
            AdvancedSlider localAdvancedSlider = AdvancedSlider.this;
            if (this.mBaseX == null);
            for (double d = this.x; ; d = this.x + this.mBaseX.evaluate(AdvancedSlider.this.mContext.mVariables))
                return localAdvancedSlider.scale(d);
        }

        public float getY()
        {
            AdvancedSlider localAdvancedSlider = AdvancedSlider.this;
            if (this.mBaseY == null);
            for (double d = this.y; ; d = this.y + this.mBaseY.evaluate(AdvancedSlider.this.mContext.mVariables))
                return localAdvancedSlider.scale(d);
        }

        public void load(Element paramElement)
            throws ScreenElementLoadException
        {
            if (paramElement == null)
            {
                Log.e("LockScreen_AdvancedSlider", "node is null");
                throw new ScreenElementLoadException("node is null");
            }
            Utils.asserts(paramElement.getNodeName().equalsIgnoreCase("Position"), "wrong node tag");
            this.x = Utils.getAttrAsInt(paramElement, "x", 0);
            this.y = Utils.getAttrAsInt(paramElement, "y", 0);
        }
    }

    private class StartPoint extends AdvancedSlider.SliderPoint
    {
        public static final String TAG_NAME = "StartPoint";

        public StartPoint(Element arg2)
            throws ScreenElementLoadException
        {
            super(localElement, "StartPoint");
        }

        protected void onStateChange(AdvancedSlider.State paramState1, AdvancedSlider.State paramState2)
        {
            if (paramState1 == AdvancedSlider.State.Invalid);
            while (true)
            {
                return;
                switch (AdvancedSlider.1.$SwitchMap$miui$app$screenelement$elements$AdvancedSlider$State[paramState2.ordinal()])
                {
                default:
                    break;
                case 1:
                    AdvancedSlider.this.mRoot.playSound(this.mNormalSound);
                    break;
                case 2:
                    if (!AdvancedSlider.this.mPressed)
                        AdvancedSlider.this.mRoot.playSound(this.mPressedSound);
                    break;
                }
            }
        }
    }

    private class SliderPoint
    {
        private ScreenElement mCurrentStateElements;
        protected float mCurrentX;
        protected float mCurrentY;
        private Expression mHeight;
        protected String mName;
        protected String mNormalSound;
        protected ElementGroup mNormalStateElements;
        protected String mPressedSound;
        protected ElementGroup mPressedStateElements;
        protected String mReachedSound;
        private ElementGroup mReachedStateElements;
        private AdvancedSlider.State mState = AdvancedSlider.State.Invalid;
        private Expression mWidth;
        protected Expression mX;
        protected Expression mY;

        public SliderPoint(Element paramString, String arg3)
            throws ScreenElementLoadException
        {
            String str;
            load(paramString, str);
        }

        private ElementGroup loadGroup(Element paramElement, String paramString)
            throws ScreenElementLoadException
        {
            Element localElement = Utils.getChild(paramElement, paramString);
            if (localElement != null);
            for (ElementGroup localElementGroup = new ElementGroup(localElement, AdvancedSlider.this.mContext, AdvancedSlider.this.mRoot); ; localElementGroup = null)
                return localElementGroup;
        }

        public ScreenElement findElement(String paramString)
        {
            Object localObject;
            if (this.mPressedStateElements != null)
            {
                ScreenElement localScreenElement3 = this.mPressedStateElements.findElement(paramString);
                if (localScreenElement3 != null)
                    localObject = localScreenElement3;
            }
            while (true)
            {
                return localObject;
                if (this.mNormalStateElements != null)
                {
                    ScreenElement localScreenElement2 = this.mNormalStateElements.findElement(paramString);
                    if (localScreenElement2 != null)
                        localObject = localScreenElement2;
                }
                else if (this.mReachedStateElements != null)
                {
                    ScreenElement localScreenElement1 = this.mReachedStateElements.findElement(paramString);
                    if (localScreenElement1 != null)
                        localObject = localScreenElement1;
                }
                else
                {
                    localObject = null;
                }
            }
        }

        public void finish()
        {
            if (this.mNormalStateElements != null)
                this.mNormalStateElements.finish();
            if (this.mPressedStateElements != null)
                this.mPressedStateElements.finish();
            if (this.mReachedStateElements != null)
                this.mReachedStateElements.finish();
        }

        public float getCurrentX()
        {
            return this.mCurrentX;
        }

        public float getCurrentY()
        {
            return this.mCurrentY;
        }

        public AdvancedSlider.State getState()
        {
            return this.mState;
        }

        public float getX()
        {
            return AdvancedSlider.this.scale(this.mX.evaluate(AdvancedSlider.this.mContext.mVariables));
        }

        public float getY()
        {
            return AdvancedSlider.this.scale(this.mY.evaluate(AdvancedSlider.this.mContext.mVariables));
        }

        public void init()
        {
            this.mCurrentX = AdvancedSlider.this.scale(this.mX.evaluate(AdvancedSlider.this.mContext.mVariables));
            this.mCurrentY = AdvancedSlider.this.scale(this.mY.evaluate(AdvancedSlider.this.mContext.mVariables));
            if (this.mNormalStateElements != null)
            {
                this.mNormalStateElements.init();
                this.mNormalStateElements.show(true);
            }
            if (this.mPressedStateElements != null)
            {
                this.mPressedStateElements.init();
                this.mPressedStateElements.show(false);
            }
            if (this.mReachedStateElements != null)
            {
                this.mReachedStateElements.init();
                this.mReachedStateElements.show(false);
            }
            setState(AdvancedSlider.State.Normal);
        }

        public void load(Element paramElement, String paramString)
            throws ScreenElementLoadException
        {
            Utils.asserts(paramElement.getNodeName().equalsIgnoreCase(paramString), "wrong node name");
            this.mName = paramElement.getAttribute("name");
            this.mNormalSound = paramElement.getAttribute("normalSound");
            this.mPressedSound = paramElement.getAttribute("pressedSound");
            this.mReachedSound = paramElement.getAttribute("reachedSound");
            this.mX = Expression.build(paramElement.getAttribute("x"));
            this.mY = Expression.build(paramElement.getAttribute("y"));
            this.mWidth = Expression.build(paramElement.getAttribute("w"));
            this.mHeight = Expression.build(paramElement.getAttribute("h"));
            this.mNormalStateElements = loadGroup(paramElement, "NormalState");
            this.mPressedStateElements = loadGroup(paramElement, "PressedState");
            this.mReachedStateElements = loadGroup(paramElement, "ReachedState");
        }

        public void moveTo(float paramFloat1, float paramFloat2)
        {
            this.mCurrentX = paramFloat1;
            this.mCurrentY = paramFloat2;
        }

        protected void onStateChange(AdvancedSlider.State paramState1, AdvancedSlider.State paramState2)
        {
        }

        public void pause()
        {
            if (this.mNormalStateElements != null)
                this.mNormalStateElements.pause();
            if (this.mPressedStateElements != null)
                this.mPressedStateElements.pause();
            if (this.mReachedStateElements != null)
                this.mReachedStateElements.pause();
        }

        public void render(Canvas paramCanvas)
        {
            float f1 = AdvancedSlider.this.scale(this.mX.evaluate(AdvancedSlider.this.mContext.mVariables));
            float f2 = AdvancedSlider.this.scale(this.mY.evaluate(AdvancedSlider.this.mContext.mVariables));
            int i = paramCanvas.save();
            paramCanvas.translate(this.mCurrentX - f1, this.mCurrentY - f2);
            if (this.mCurrentStateElements != null)
                this.mCurrentStateElements.render(paramCanvas);
            paramCanvas.restoreToCount(i);
        }

        public void reset(long paramLong)
        {
            if (this.mNormalStateElements != null)
                this.mNormalStateElements.reset(paramLong);
            if (this.mPressedStateElements != null)
                this.mPressedStateElements.reset(paramLong);
            if (this.mReachedStateElements != null)
                this.mReachedStateElements.reset(paramLong);
        }

        public void resume()
        {
            if (this.mNormalStateElements != null)
                this.mNormalStateElements.resume();
            if (this.mPressedStateElements != null)
                this.mPressedStateElements.resume();
            if (this.mReachedStateElements != null)
                this.mReachedStateElements.resume();
        }

        public void setState(AdvancedSlider.State paramState)
        {
            if (this.mState == paramState);
            while (true)
            {
                return;
                AdvancedSlider.State localState = this.mState;
                this.mState = paramState;
                localElementGroup = null;
                i = 0;
                switch (AdvancedSlider.1.$SwitchMap$miui$app$screenelement$elements$AdvancedSlider$State[paramState.ordinal()])
                {
                default:
                    if ((localElementGroup != null) && (i != 0))
                        localElementGroup.reset();
                    if (this.mCurrentStateElements != localElementGroup)
                    {
                        if (this.mCurrentStateElements != null)
                            this.mCurrentStateElements.show(false);
                        if (localElementGroup != null)
                            localElementGroup.show(true);
                        this.mCurrentStateElements = localElementGroup;
                    }
                    onStateChange(localState, this.mState);
                case 1:
                case 2:
                case 3:
                }
            }
            ElementGroup localElementGroup = this.mNormalStateElements;
            if (this.mPressedStateElements != null);
            for (int i = 1; ; i = 0)
                break;
            if (this.mPressedStateElements != null)
            {
                localElementGroup = this.mPressedStateElements;
                label158: if ((this.mPressedStateElements == null) || (AdvancedSlider.this.mPressed))
                    break label189;
            }
            label189: for (i = 1; ; i = 0)
            {
                break;
                localElementGroup = this.mNormalStateElements;
                break label158;
            }
            if (this.mReachedStateElements != null)
            {
                localElementGroup = this.mReachedStateElements;
                label207: if (this.mReachedStateElements == null)
                    break label243;
            }
            label243: for (i = 1; ; i = 0)
            {
                break;
                if (this.mPressedStateElements != null)
                {
                    localElementGroup = this.mPressedStateElements;
                    break label207;
                }
                localElementGroup = this.mNormalStateElements;
                break label207;
            }
        }

        public void showCategory(String paramString, boolean paramBoolean)
        {
            if (this.mPressedStateElements != null)
                this.mPressedStateElements.showCategory(paramString, paramBoolean);
            if (this.mNormalStateElements != null)
                this.mNormalStateElements.showCategory(paramString, paramBoolean);
            if (this.mReachedStateElements != null)
                this.mReachedStateElements.showCategory(paramString, paramBoolean);
        }

        public void tick(long paramLong)
        {
            if (this.mCurrentStateElements != null)
                this.mCurrentStateElements.tick(paramLong);
        }

        public boolean touched(float paramFloat1, float paramFloat2)
        {
            float f1 = AdvancedSlider.this.scale(this.mX.evaluate(AdvancedSlider.this.mContext.mVariables));
            float f2 = AdvancedSlider.this.scale(this.mWidth.evaluate(AdvancedSlider.this.mContext.mVariables));
            float f3 = AdvancedSlider.this.scale(this.mY.evaluate(AdvancedSlider.this.mContext.mVariables));
            float f4 = AdvancedSlider.this.scale(this.mHeight.evaluate(AdvancedSlider.this.mContext.mVariables));
            if ((paramFloat1 >= f1) && (paramFloat1 <= f1 + f2) && (paramFloat2 >= f3) && (paramFloat2 <= f3 + f4));
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }

    private static enum State
    {
        static
        {
            Invalid = new State("Invalid", 3);
            State[] arrayOfState = new State[4];
            arrayOfState[0] = Normal;
            arrayOfState[1] = Pressed;
            arrayOfState[2] = Reached;
            arrayOfState[3] = Invalid;
        }
    }

    private class BounceAnimationController
    {
        private static final int BOUNCE_THRESHOLD = 3;
        private int mBounceAccelation;
        private Expression mBounceAccelationExp;
        private int mBounceInitSpeed;
        private Expression mBounceInitSpeedExp;
        private IndexedNumberVariable mBounceProgress;
        private int mBounceStartPointIndex;
        private long mBounceStartTime = -1L;
        private AdvancedSlider.EndPoint mEndPoint;
        private long mPreDistance;
        private float mStartX;
        private float mStartY;
        private double mTraveledDistance;

        private BounceAnimationController()
        {
        }

        private Utils.Point getPoint(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, long paramLong)
        {
            Utils.Point localPoint1 = new Utils.Point(paramFloat1, paramFloat2);
            Utils.Point localPoint2 = new Utils.Point(paramFloat3, paramFloat4);
            double d1 = Utils.Dist(localPoint1, localPoint2, true);
            if (paramLong >= d1);
            double d3;
            double d4;
            for (Utils.Point localPoint3 = null; ; localPoint3 = new Utils.Point(d3 + localPoint1.x, d4 + localPoint1.y))
            {
                return localPoint3;
                double d2 = (d1 - paramLong) / d1;
                d3 = d2 * (localPoint2.x - localPoint1.x);
                d4 = d2 * (localPoint2.y - localPoint1.y);
            }
        }

        private void startBounceAnimation(AdvancedSlider.EndPoint paramEndPoint)
        {
            if (this.mBounceInitSpeedExp != null)
                this.mBounceInitSpeed = ((int)this.mBounceInitSpeedExp.evaluate(AdvancedSlider.this.mContext.mVariables));
            if (this.mBounceAccelationExp != null)
                this.mBounceAccelation = ((int)this.mBounceAccelationExp.evaluate(AdvancedSlider.this.mContext.mVariables));
            this.mBounceStartTime = 0L;
            this.mEndPoint = paramEndPoint;
            this.mStartX = AdvancedSlider.this.mStartPoint.getCurrentX();
            this.mStartY = AdvancedSlider.this.mStartPoint.getCurrentY();
            this.mBounceStartPointIndex = -1;
            this.mTraveledDistance = 0.0D;
            Utils.Point localPoint1 = new Utils.Point(this.mStartX, this.mStartY);
            int i;
            Utils.Point localPoint2;
            Utils.Point localPoint3;
            if ((paramEndPoint != null) && (paramEndPoint.mPath != null))
            {
                i = 1;
                if (i < paramEndPoint.mPath.size())
                {
                    AdvancedSlider.Position localPosition1 = (AdvancedSlider.Position)paramEndPoint.mPath.get(i - 1);
                    AdvancedSlider.Position localPosition2 = (AdvancedSlider.Position)paramEndPoint.mPath.get(i);
                    localPoint2 = new Utils.Point(localPosition1.getX(), localPosition1.getY());
                    localPoint3 = new Utils.Point(localPosition2.getX(), localPosition2.getY());
                    Utils.Point localPoint4 = Utils.pointProjectionOnSegment(localPoint2, localPoint3, localPoint1, false);
                    if (localPoint4 != null)
                    {
                        this.mBounceStartPointIndex = (i - 1);
                        this.mTraveledDistance += Utils.Dist(localPoint2, localPoint4, true);
                    }
                }
                else
                {
                    label259: if (this.mTraveledDistance >= 3.0D)
                        break label349;
                    AdvancedSlider.this.cancelMoving();
                    this.mBounceStartTime = -1L;
                }
            }
            while (true)
            {
                return;
                this.mTraveledDistance += Utils.Dist(localPoint2, localPoint3, true);
                i++;
                break;
                this.mTraveledDistance = Utils.Dist(new Utils.Point(AdvancedSlider.this.mStartPoint.getX(), AdvancedSlider.this.mStartPoint.getY()), localPoint1, true);
                break label259;
                label349: if (this.mBounceProgress != null)
                    this.mBounceProgress.set(0.0D);
                AdvancedSlider.this.requestUpdate();
            }
        }

        public void init()
        {
            this.mBounceStartTime = -1L;
            if (this.mBounceProgress != null)
                this.mBounceProgress.set(1.0D);
        }

        public void load(Element paramElement)
        {
            this.mBounceInitSpeedExp = Expression.build(paramElement.getAttribute("bounceInitSpeed"));
            this.mBounceAccelationExp = Expression.build(paramElement.getAttribute("bounceAcceleration"));
            if (AdvancedSlider.this.mHasName)
                this.mBounceProgress = new IndexedNumberVariable(AdvancedSlider.this.mName, "bounce_progress", AdvancedSlider.this.mContext.mVariables);
        }

        public void startCancelMoving(AdvancedSlider.EndPoint paramEndPoint)
        {
            if (this.mBounceInitSpeedExp == null)
                AdvancedSlider.this.cancelMoving();
            while (true)
            {
                return;
                startBounceAnimation(paramEndPoint);
            }
        }

        public void tick(long paramLong)
        {
            if (this.mBounceStartTime < 0L);
            long l2;
            while (true)
            {
                return;
                if (this.mBounceStartTime == 0L)
                {
                    this.mBounceStartTime = paramLong;
                    this.mPreDistance = 0L;
                    AdvancedSlider.this.requestUpdate();
                    if (this.mTraveledDistance > 0.0D)
                    {
                        double d = this.mPreDistance / this.mTraveledDistance;
                        if (this.mBounceProgress != null)
                        {
                            IndexedNumberVariable localIndexedNumberVariable = this.mBounceProgress;
                            if (d > 1.0D)
                                d = 1.0D;
                            localIndexedNumberVariable.set(d);
                        }
                    }
                }
                else
                {
                    long l1 = paramLong - this.mBounceStartTime;
                    l2 = l1 * this.mBounceInitSpeed / 1000L + l1 * (l1 * this.mBounceAccelation) / 2000000L;
                    if ((this.mBounceInitSpeed + l1 * this.mBounceAccelation / 1000L > 0L) && (this.mTraveledDistance >= 3.0D))
                        break;
                    AdvancedSlider.this.cancelMoving();
                    this.mBounceStartTime = -1L;
                    if (this.mBounceProgress != null)
                        this.mBounceProgress.set(1.0D);
                }
            }
            Utils.Point localPoint1;
            if ((this.mEndPoint == null) || (this.mEndPoint.mPath == null))
            {
                localPoint1 = getPoint(AdvancedSlider.this.mStartPoint.getX(), AdvancedSlider.this.mStartPoint.getY(), this.mStartX, this.mStartY, l2);
                if (localPoint1 == null)
                {
                    AdvancedSlider.this.cancelMoving();
                    this.mBounceStartTime = -1L;
                }
            }
            while (true)
            {
                this.mPreDistance = l2;
                break;
                AdvancedSlider.this.moveStartPoint((int)localPoint1.x, (int)localPoint1.y);
                continue;
                float f1 = AdvancedSlider.this.mStartPoint.getCurrentX();
                float f2 = AdvancedSlider.this.mStartPoint.getCurrentY();
                long l3 = l2 - this.mPreDistance;
                Utils.Point localPoint2;
                for (int i = this.mBounceStartPointIndex; ; i--)
                {
                    if (i < 0)
                        break label473;
                    AdvancedSlider.Position localPosition = (AdvancedSlider.Position)this.mEndPoint.mPath.get(i);
                    localPoint2 = getPoint(localPosition.getX(), localPosition.getY(), f1, f2, l3);
                    if (localPoint2 != null)
                        break label475;
                    if (i == 0)
                    {
                        AdvancedSlider.this.cancelMoving();
                        this.mBounceStartTime = -1L;
                        break;
                    }
                    Utils.Point localPoint3 = new Utils.Point(localPosition.getX(), localPosition.getY());
                    Utils.Point localPoint4 = new Utils.Point(f1, f2);
                    l3 = ()(l3 - Utils.Dist(localPoint3, localPoint4, true));
                    f1 = localPosition.getX();
                    f2 = localPosition.getY();
                }
                label473: continue;
                label475: this.mBounceStartPointIndex = i;
                AdvancedSlider.this.moveStartPoint((int)localPoint2.x, (int)localPoint2.y);
            }
        }
    }

    public static abstract interface OnLaunchListener
    {
        public abstract void onLaunch(String paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.elements.AdvancedSlider
 * JD-Core Version:        0.6.2
 */