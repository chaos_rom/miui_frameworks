package miui.app.screenelement.elements;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.text.TextUtils;
import android.util.Log;
import miui.app.screenelement.ResourceManager;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.ScreenElementRoot;
import miui.widget.SpectrumVisualizer;
import org.w3c.dom.Element;

public class SpectrumVisualizerScreenElement extends ImageScreenElement
{
    public static final String TAG_NAME = "SpectrumVisualizer";
    private Canvas mCanvas;
    private String mDotbar;
    private Bitmap mPanel;
    private String mPanelSrc;
    private int mResDensity;
    private String mShadow;
    private SpectrumVisualizer mSpectrumVisualizer;

    public SpectrumVisualizerScreenElement(Element paramElement, ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot)
        throws ScreenElementLoadException
    {
        super(paramElement, paramScreenContext, paramScreenElementRoot);
        this.mPanelSrc = paramElement.getAttribute("panelSrc");
        this.mDotbar = paramElement.getAttribute("dotbarSrc");
        this.mShadow = paramElement.getAttribute("shadowSrc");
        this.mSpectrumVisualizer = new SpectrumVisualizer(paramScreenContext.mContext);
        this.mSpectrumVisualizer.setSoftDrawEnabled(false);
        this.mSpectrumVisualizer.enableUpdate(false);
    }

    public void doRender(Canvas paramCanvas)
    {
        if (this.mPanel != null)
        {
            this.mPaint.setAlpha(getAlpha());
            paramCanvas.drawBitmap(this.mPanel, getLeft(), getTop(), this.mPaint);
        }
        super.doRender(paramCanvas);
    }

    public void enableUpdate(boolean paramBoolean)
    {
        this.mSpectrumVisualizer.enableUpdate(paramBoolean);
    }

    protected Bitmap getBitmap()
    {
        if (this.mCanvas == null);
        for (Bitmap localBitmap = null; ; localBitmap = this.mBitmap)
        {
            return localBitmap;
            this.mCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
            this.mCanvas.setDensity(0);
            this.mSpectrumVisualizer.draw(this.mCanvas);
            this.mCanvas.setDensity(this.mResDensity);
        }
    }

    public void init()
    {
        super.init();
        Bitmap localBitmap1;
        Bitmap localBitmap2;
        label33: Bitmap localBitmap3;
        label45: int i;
        int j;
        if (TextUtils.isEmpty(this.mPanelSrc))
        {
            localBitmap1 = null;
            this.mPanel = localBitmap1;
            if (!TextUtils.isEmpty(this.mDotbar))
                break label125;
            localBitmap2 = null;
            if (!TextUtils.isEmpty(this.mShadow))
                break label143;
            localBitmap3 = null;
            i = (int)getWidth();
            j = (int)getHeight();
            if ((i <= 0) || (j <= 0))
            {
                if (this.mPanel == null)
                    break label161;
                i = this.mPanel.getWidth();
                j = this.mPanel.getHeight();
            }
            if (localBitmap2 != null)
                break label172;
            Log.e("SpectrumVisualizerScreenElement", "no dotbar");
        }
        while (true)
        {
            return;
            localBitmap1 = this.mContext.mResourceManager.getBitmap(this.mPanelSrc);
            break;
            label125: localBitmap2 = this.mContext.mResourceManager.getBitmap(this.mDotbar);
            break label33;
            label143: localBitmap3 = this.mContext.mResourceManager.getBitmap(this.mShadow);
            break label45;
            label161: Log.e("SpectrumVisualizerScreenElement", "no panel or size");
            continue;
            label172: this.mSpectrumVisualizer.setBitmaps(i, j, localBitmap2, localBitmap3);
            this.mResDensity = localBitmap2.getDensity();
            this.mSpectrumVisualizer.layout(0, 0, i, j);
            this.mBitmap = Bitmap.createBitmap(i, j, Bitmap.Config.ARGB_8888);
            this.mBitmap.setDensity(this.mResDensity);
            this.mCanvas = new Canvas(this.mBitmap);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.elements.SpectrumVisualizerScreenElement
 * JD-Core Version:        0.6.2
 */