package miui.app.screenelement.elements;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;
import miui.app.screenelement.ResourceManager;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.ScreenElementRoot;
import miui.app.screenelement.animation.AnimatedElement;
import miui.app.screenelement.data.Expression;
import miui.app.screenelement.util.IndexedNumberVariable;
import miui.app.screenelement.util.Utils;
import org.w3c.dom.Element;

public class ImageNumberScreenElement extends AnimatedScreenElement
{
    private static final String LOG_TAG = "ImageNumberScreenElement";
    public static final String TAG_NAME = "ImageNumber";
    private IndexedNumberVariable mActualWidthVar;
    private Expression mNumExpression;
    private Paint mPaint = new Paint();

    public ImageNumberScreenElement(Element paramElement, ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot)
        throws ScreenElementLoadException
    {
        super(paramElement, paramScreenContext, paramScreenElementRoot);
        load(paramElement);
        if (this.mHasName)
            this.mActualWidthVar = new IndexedNumberVariable(this.mName, "actual_w", paramScreenContext.mVariables);
    }

    public void doRender(Canvas paramCanvas)
    {
        if (!isVisible());
        while (true)
        {
            return;
            int i = getAlpha();
            String str = String.valueOf((int)this.mNumExpression.evaluate(this.mContext.mVariables));
            float f1 = getX();
            float f2 = getY();
            float f3 = 0.0F;
            this.mPaint.setAlpha(i);
            for (int j = 0; j < str.length(); j++)
            {
                Bitmap localBitmap = getBitmap(str.charAt(j));
                if (localBitmap != null)
                {
                    paramCanvas.drawBitmap(localBitmap, f1 + f3, f2, this.mPaint);
                    f3 += localBitmap.getWidth();
                }
            }
            if (this.mHasName)
                this.mActualWidthVar.set(descale(f3));
        }
    }

    protected Bitmap getBitmap(char paramChar)
    {
        String str = Utils.addFileNameSuffix(this.mAni.getSrc(), String.valueOf(paramChar));
        return this.mContext.mResourceManager.getBitmap(str);
    }

    public void load(Element paramElement)
        throws ScreenElementLoadException
    {
        if (paramElement == null)
        {
            Log.e("ImageNumberScreenElement", "node is null");
            throw new ScreenElementLoadException("node is null");
        }
        this.mNumExpression = Expression.build(paramElement.getAttribute("number"));
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.elements.ImageNumberScreenElement
 * JD-Core Version:        0.6.2
 */