package miui.app.screenelement.elements;

import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.ScreenElementRoot;
import org.w3c.dom.Element;

public class ScreenElementFactory
{
    public ScreenElement createInstance(Element paramElement, ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot)
        throws ScreenElementLoadException
    {
        String str = paramElement.getTagName();
        Object localObject;
        if (str.equalsIgnoreCase("Image"))
            localObject = new ImageScreenElement(paramElement, paramScreenContext, paramScreenElementRoot);
        while (true)
        {
            return localObject;
            if (str.equalsIgnoreCase("Time"))
                localObject = new TimepanelScreenElement(paramElement, paramScreenContext, paramScreenElementRoot);
            else if (str.equalsIgnoreCase("ImageNumber"))
                localObject = new ImageNumberScreenElement(paramElement, paramScreenContext, paramScreenElementRoot);
            else if (str.equalsIgnoreCase("Text"))
                localObject = new TextScreenElement(paramElement, paramScreenContext, paramScreenElementRoot);
            else if (str.equalsIgnoreCase("DateTime"))
                localObject = new DateTimeScreenElement(paramElement, paramScreenContext, paramScreenElementRoot);
            else if (str.equalsIgnoreCase("Button"))
                localObject = new ButtonScreenElement(paramElement, paramScreenContext, paramScreenElementRoot);
            else if (str.equalsIgnoreCase("MusicControl"))
                localObject = new MusicControlScreenElement(paramElement, paramScreenContext, paramScreenElementRoot);
            else if ((str.equalsIgnoreCase("ElementGroup")) || (str.equalsIgnoreCase("Group")))
                localObject = new ElementGroup(paramElement, paramScreenContext, paramScreenElementRoot);
            else if (str.equalsIgnoreCase("Var"))
                localObject = new VariableElement(paramElement, paramScreenContext, paramScreenElementRoot);
            else if (str.equalsIgnoreCase("VarArray"))
                localObject = new VariableArrayElement(paramElement, paramScreenContext, paramScreenElementRoot);
            else if (str.equalsIgnoreCase("SpectrumVisualizer"))
                localObject = new SpectrumVisualizerScreenElement(paramElement, paramScreenContext, paramScreenElementRoot);
            else if (str.equalsIgnoreCase("Slider"))
                localObject = new AdvancedSlider(paramElement, paramScreenContext, paramScreenElementRoot);
            else if (str.equalsIgnoreCase("FramerateController"))
                localObject = new FramerateController(paramElement, paramScreenContext, paramScreenElementRoot);
            else if (str.equalsIgnoreCase("VirtualScreen"))
                localObject = new VirtualScreen(paramElement, paramScreenContext, paramScreenElementRoot);
            else
                localObject = null;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.elements.ScreenElementFactory
 * JD-Core Version:        0.6.2
 */