package miui.app.screenelement.elements;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;
import miui.app.screenelement.ResourceManager;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.ScreenElementRoot;
import miui.app.screenelement.util.IndexedNumberVariable;
import miui.app.screenelement.util.Utils;
import miui.util.AudioOutputHelper;
import org.w3c.dom.Element;

public class MusicControlScreenElement extends ElementGroup
    implements ButtonScreenElement.ButtonActionListener
{
    private static final String BUTTON_MUSIC_NEXT = "music_next";
    private static final String BUTTON_MUSIC_PAUSE = "music_pause";
    private static final String BUTTON_MUSIC_PLAY = "music_play";
    private static final String BUTTON_MUSIC_PREV = "music_prev";
    private static final int CHECK_STREAM_MUSIC_DELAY = 1000;
    private static final String ELE_MUSIC_ALBUM_COVER = "music_album_cover";
    private static final String ELE_MUSIC_DISPLAY = "music_display";
    private static final int FRAMERATE_PLAYING = 30;
    private static final String LOG_TAG = "MusicControlScreenElement";
    private static final int MUSIC_NONE = 0;
    private static final int MUSIC_PLAY = 2;
    private static final int MUSIC_STOP = 1;
    public static final String TAG_NAME = "MusicControl";
    private boolean isPaused = false;
    private Bitmap mAlbumCoverBm;
    private String mAlbumName;
    private String mArtistName;
    private AudioManager mAudioManager;
    private boolean mAutoShow;
    private ButtonScreenElement mButtonNext = (ButtonScreenElement)findElement("music_next");
    private ButtonScreenElement mButtonPause = (ButtonScreenElement)findElement("music_pause");
    private ButtonScreenElement mButtonPlay = (ButtonScreenElement)findElement("music_play");
    private ButtonScreenElement mButtonPrev = (ButtonScreenElement)findElement("music_prev");
    private Runnable mCheckStreamMusicRunnable = new Runnable()
    {
        public void run()
        {
            MusicControlScreenElement.this.updateMusic();
            MusicControlScreenElement.this.updateSpectrumVisualizer();
            MusicControlScreenElement.this.mHandler.postDelayed(this, 1000L);
        }
    };
    private Bitmap mDefaultAlbumCoverBm;
    private Runnable mEnableSpectrumVisualizerRunnable = new Runnable()
    {
        public void run()
        {
            MusicControlScreenElement.this.mHandler.removeCallbacks(MusicControlScreenElement.this.mEnableSpectrumVisualizerRunnable);
            MusicControlScreenElement.this.updateSpectrumVisualizer();
        }
    };
    private final Handler mHandler = new Handler();
    private ImageScreenElement mImageAlbumCover = (ImageScreenElement)findElement("music_album_cover");
    private boolean mIsOnlineSongBlocking;
    private IndexedNumberVariable mMusicStateVar;
    private int mMusicStatus;
    private BroadcastReceiver mPlayerStatusListener = new BroadcastReceiver()
    {
        private void setTrackInfo(Intent paramAnonymousIntent)
        {
            if (MusicControlScreenElement.this.mTextDisplay == null);
            String str1;
            String str2;
            while (true)
            {
                return;
                str1 = paramAnonymousIntent.getStringExtra("track");
                str2 = paramAnonymousIntent.getStringExtra("artist");
                MusicControlScreenElement.access$402(MusicControlScreenElement.this, false);
                if ((!TextUtils.isEmpty(str1)) || (!TextUtils.isEmpty(str2)))
                    break;
                MusicControlScreenElement.this.mTextDisplay.show(false);
            }
            if (TextUtils.isEmpty(str1))
                MusicControlScreenElement.this.mTextDisplay.setText(str2);
            while (true)
            {
                MusicControlScreenElement.this.mTextDisplay.show(true);
                break;
                if (TextUtils.isEmpty(str2))
                {
                    MusicControlScreenElement.this.mTextDisplay.setText(str1);
                }
                else
                {
                    TextScreenElement localTextScreenElement = MusicControlScreenElement.this.mTextDisplay;
                    Object[] arrayOfObject = new Object[2];
                    arrayOfObject[0] = str1;
                    arrayOfObject[1] = str2;
                    localTextScreenElement.setText(String.format("%s     %s", arrayOfObject));
                }
            }
        }

        public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
        {
            String str1 = paramAnonymousIntent.getAction();
            if (!paramAnonymousIntent.getBooleanExtra("playing", false));
            while (true)
            {
                return;
                if ("com.miui.player.metachanged".equals(str1))
                {
                    String str2 = paramAnonymousIntent.getStringExtra("other");
                    if ("meta_changed_track".equals(str2))
                    {
                        setTrackInfo(paramAnonymousIntent);
                        MusicControlScreenElement.this.requestAlbum(paramAnonymousIntent);
                    }
                    else if ("meta_changed_album".equals(str2))
                    {
                        MusicControlScreenElement.this.requestAlbum(paramAnonymousIntent, true);
                    }
                    else
                    {
                        MusicControlScreenElement.this.requestAlbum();
                    }
                }
                else if ("lockscreen.action.SONG_METADATA_UPDATED".equals(str1))
                {
                    setTrackInfo(paramAnonymousIntent);
                    MusicControlScreenElement.this.setAlbumCover(paramAnonymousIntent);
                }
                else if ("com.miui.player.refreshprogress".equals(str1))
                {
                    MusicControlScreenElement.access$402(MusicControlScreenElement.this, paramAnonymousIntent.getBooleanExtra("blocking", false));
                }
                else if ("com.miui.player.playstatechanged".equals(str1))
                {
                    if ((MusicControlScreenElement.this.mTextDisplay != null) && (!MusicControlScreenElement.this.mTextDisplay.isVisible()))
                        setTrackInfo(paramAnonymousIntent);
                    MusicControlScreenElement.this.requestAlbum(paramAnonymousIntent);
                }
            }
        }
    };
    private SpectrumVisualizerScreenElement mSpectrumVisualizer = findSpectrumVisualizer(this);
    private TextScreenElement mTextDisplay = (TextScreenElement)findElement("music_display");

    public MusicControlScreenElement(Element paramElement, ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot)
        throws ScreenElementLoadException
    {
        super(paramElement, paramScreenContext, paramScreenElementRoot);
        if ((this.mButtonPrev == null) || (this.mButtonNext == null) || (this.mButtonPlay == null) || (this.mButtonPause == null))
            throw new ScreenElementLoadException("invalid music control");
        setupButton(this.mButtonPrev);
        setupButton(this.mButtonNext);
        setupButton(this.mButtonPlay);
        setupButton(this.mButtonPause);
        this.mButtonPause.show(false);
        String str;
        if (this.mImageAlbumCover != null)
        {
            str = paramElement.getAttribute("defAlbumCover");
            if (TextUtils.isEmpty(str))
                break label346;
        }
        label346: for (this.mDefaultAlbumCoverBm = this.mContext.mResourceManager.getBitmap(str); ; this.mDefaultAlbumCoverBm = BitmapFactory.decodeResource(this.mContext.mContext.getResources(), 100794537))
        {
            if (this.mDefaultAlbumCoverBm != null)
                this.mDefaultAlbumCoverBm.setDensity(this.mRoot.getResourceDensity());
            this.mAutoShow = Boolean.parseBoolean(paramElement.getAttribute("autoShow"));
            this.mAudioManager = ((AudioManager)paramScreenContext.mContext.getSystemService("audio"));
            if (this.mHasName)
                this.mMusicStateVar = new IndexedNumberVariable(this.mName, "music_state", paramScreenContext.mVariables);
            return;
        }
    }

    private SpectrumVisualizerScreenElement findSpectrumVisualizer(ElementGroup paramElementGroup)
    {
        Iterator localIterator = paramElementGroup.getElements().iterator();
        ScreenElement localScreenElement;
        Object localObject;
        if (localIterator.hasNext())
        {
            localScreenElement = (ScreenElement)localIterator.next();
            if ((localScreenElement instanceof SpectrumVisualizerScreenElement))
                localObject = (SpectrumVisualizerScreenElement)localScreenElement;
        }
        while (true)
        {
            return localObject;
            if (!(localScreenElement instanceof ElementGroup))
                break;
            SpectrumVisualizerScreenElement localSpectrumVisualizerScreenElement = findSpectrumVisualizer((ElementGroup)localScreenElement);
            if (localSpectrumVisualizerScreenElement == null)
                break;
            localObject = localSpectrumVisualizerScreenElement;
            continue;
            localObject = null;
        }
    }

    private int getKeyCode(String paramString)
    {
        int i;
        if ("music_prev".equals(paramString))
            i = 88;
        while (true)
        {
            return i;
            if ("music_next".equals(paramString))
                i = 87;
            else if (("music_play".equals(paramString)) || ("music_pause".equals(paramString)))
                i = 85;
            else
                i = -1;
        }
    }

    private void requestAlbum()
    {
        Intent localIntent = new Intent("lockscreen.action.SONG_METADATA_REQUEST");
        this.mContext.mContext.sendBroadcast(localIntent);
    }

    private void requestAlbum(Intent paramIntent)
    {
        requestAlbum(paramIntent, false);
    }

    private void requestAlbum(Intent paramIntent, boolean paramBoolean)
    {
        if (this.mImageAlbumCover == null);
        while (true)
        {
            return;
            String str1 = paramIntent.getStringExtra("album");
            String str2 = paramIntent.getStringExtra("artist");
            if ((paramBoolean) || (!Utils.equals(str1, this.mAlbumName)) || (!Utils.equals(str2, this.mArtistName)) || (this.mAlbumCoverBm == null))
            {
                Uri localUri = (Uri)paramIntent.getParcelableExtra("album_uri");
                String str3 = paramIntent.getStringExtra("album_path");
                this.mAlbumCoverBm = null;
                if ((localUri != null) || (str3 != null))
                    requestAlbum();
                else
                    this.mImageAlbumCover.setBitmap(this.mDefaultAlbumCoverBm);
            }
        }
    }

    private void sendMediaButtonBroadcast(int paramInt1, int paramInt2)
    {
        long l = SystemClock.uptimeMillis();
        KeyEvent localKeyEvent = new KeyEvent(l, l, paramInt1, paramInt2, 0);
        Intent localIntent = new Intent("android.intent.action.MEDIA_BUTTON", null);
        localIntent.putExtra("android.intent.extra.KEY_EVENT", KeyEvent.changeFlags(localKeyEvent, 8));
        localIntent.putExtra("forbid_double_click", true);
        this.mContext.mContext.sendOrderedBroadcast(localIntent, null);
    }

    private void setAlbumCover(Intent paramIntent)
    {
        if (this.mImageAlbumCover == null);
        ImageScreenElement localImageScreenElement1;
        while (true)
        {
            return;
            this.mAlbumName = paramIntent.getStringExtra("album");
            this.mArtistName = paramIntent.getStringExtra("artist");
            try
            {
                String str = paramIntent.getStringExtra("tmp_album_path");
                if (str != null)
                {
                    this.mAlbumCoverBm = BitmapFactory.decodeFile(str);
                    if (this.mAlbumCoverBm != null)
                        this.mAlbumCoverBm.setDensity(this.mRoot.getResourceDensity());
                }
                ImageScreenElement localImageScreenElement3 = this.mImageAlbumCover;
                if (this.mAlbumCoverBm != null);
                for (Bitmap localBitmap3 = this.mAlbumCoverBm; ; localBitmap3 = this.mDefaultAlbumCoverBm)
                {
                    localImageScreenElement3.setBitmap(localBitmap3);
                    requestUpdate();
                    break;
                }
            }
            catch (OutOfMemoryError localOutOfMemoryError)
            {
                Log.e("MusicControlScreenElement", "failed to load album cover bitmap: " + localOutOfMemoryError.toString());
                ImageScreenElement localImageScreenElement2 = this.mImageAlbumCover;
                if (this.mAlbumCoverBm != null);
                for (Bitmap localBitmap2 = this.mAlbumCoverBm; ; localBitmap2 = this.mDefaultAlbumCoverBm)
                {
                    localImageScreenElement2.setBitmap(localBitmap2);
                    requestUpdate();
                    break;
                }
            }
            finally
            {
                localImageScreenElement1 = this.mImageAlbumCover;
                if (this.mAlbumCoverBm == null);
            }
        }
        for (Bitmap localBitmap1 = this.mAlbumCoverBm; ; localBitmap1 = this.mDefaultAlbumCoverBm)
        {
            localImageScreenElement1.setBitmap(localBitmap1);
            requestUpdate();
            throw localObject;
        }
    }

    private void setupButton(ButtonScreenElement paramButtonScreenElement)
    {
        if (paramButtonScreenElement != null)
        {
            paramButtonScreenElement.setListener(this);
            paramButtonScreenElement.setParent(this);
        }
    }

    private void updateMusic()
    {
        boolean bool1 = false;
        int i = 1;
        boolean bool2 = this.mAudioManager.isMusicActive();
        if (!bool2)
        {
            int j = i;
            if (this.mIsOnlineSongBlocking)
                j = 0;
            this.mButtonPlay.show(j);
            ButtonScreenElement localButtonScreenElement = this.mButtonPause;
            if (j == 0)
                bool1 = i;
            localButtonScreenElement.show(bool1);
            switch (this.mMusicStatus)
            {
            default:
                label84: if (!bool2)
                    break;
            case 1:
            case 2:
            }
        }
        for (float f = 30.0F; ; f = 0.0F)
        {
            requestFramerate(f);
            return;
            int k = 0;
            break;
            if (bool2)
                i = 2;
            this.mMusicStatus = i;
            if (!this.mHasName)
                break label84;
            IndexedNumberVariable localIndexedNumberVariable = this.mMusicStateVar;
            if (bool2);
            for (double d = 1.0D; ; d = 0.0D)
            {
                localIndexedNumberVariable.set(d);
                break;
            }
        }
    }

    public void finish()
    {
        this.mHandler.removeCallbacks(this.mCheckStreamMusicRunnable);
        try
        {
            this.mContext.mContext.unregisterReceiver(this.mPlayerStatusListener);
            if (this.mSpectrumVisualizer != null)
                this.mSpectrumVisualizer.enableUpdate(false);
            return;
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            while (true)
            {
                Log.e("MusicControlScreenElement", localIllegalArgumentException.toString());
                localIllegalArgumentException.printStackTrace();
            }
        }
    }

    public void init()
    {
        super.init();
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("com.miui.player.metachanged");
        localIntentFilter.addAction("lockscreen.action.SONG_METADATA_UPDATED");
        localIntentFilter.addAction("com.miui.player.refreshprogress");
        localIntentFilter.addAction("com.miui.player.playstatechanged");
        this.mContext.mContext.registerReceiver(this.mPlayerStatusListener, localIntentFilter, null, this.mHandler);
        boolean bool = AudioOutputHelper.hasActiveReceivers(this.mContext.mContext);
        if (bool)
        {
            this.mMusicStatus = 2;
            Intent localIntent = new Intent("lockscreen.action.SONG_METADATA_REQUEST");
            this.mContext.mContext.sendBroadcast(localIntent);
            if (this.mAutoShow)
                show(true);
        }
        IndexedNumberVariable localIndexedNumberVariable;
        if (this.mHasName)
        {
            localIndexedNumberVariable = this.mMusicStateVar;
            if (!bool)
                break label145;
        }
        label145: for (double d = 1.0D; ; d = 0.0D)
        {
            localIndexedNumberVariable.set(d);
            return;
        }
    }

    public boolean onButtonDoubleClick(String paramString)
    {
        return false;
    }

    public boolean onButtonDown(String paramString)
    {
        boolean bool = false;
        int i = getKeyCode(paramString);
        if (i == -1);
        while (true)
        {
            return bool;
            sendMediaButtonBroadcast(0, i);
            bool = true;
        }
    }

    public boolean onButtonLongClick(String paramString)
    {
        return false;
    }

    public boolean onButtonUp(final String paramString)
    {
        int i = 1;
        int j = getKeyCode(paramString);
        if (j == -1)
            i = 0;
        while (true)
        {
            return i;
            sendMediaButtonBroadcast(i, j);
            this.mHandler.post(new Runnable()
            {
                public void run()
                {
                    if ("music_pause".equals(paramString))
                    {
                        MusicControlScreenElement.this.mButtonPause.show(false);
                        MusicControlScreenElement.this.mButtonPlay.show(true);
                        MusicControlScreenElement.access$1102(MusicControlScreenElement.this, 1);
                        if (MusicControlScreenElement.this.mHasName)
                            MusicControlScreenElement.this.mMusicStateVar.set(0.0D);
                        MusicControlScreenElement.this.requestFramerate(0.0F);
                    }
                    while (true)
                    {
                        MusicControlScreenElement.this.mHandler.removeCallbacks(MusicControlScreenElement.this.mCheckStreamMusicRunnable);
                        MusicControlScreenElement.this.mHandler.postDelayed(MusicControlScreenElement.this.mCheckStreamMusicRunnable, 3000L);
                        MusicControlScreenElement.this.mHandler.removeCallbacks(MusicControlScreenElement.this.mEnableSpectrumVisualizerRunnable);
                        MusicControlScreenElement.this.mHandler.postDelayed(MusicControlScreenElement.this.mEnableSpectrumVisualizerRunnable, 500L);
                        return;
                        if ("music_play".equals(paramString))
                        {
                            MusicControlScreenElement.this.mButtonPlay.show(false);
                            MusicControlScreenElement.this.mButtonPause.show(true);
                            MusicControlScreenElement.access$1102(MusicControlScreenElement.this, 2);
                            if (MusicControlScreenElement.this.mHasName)
                                MusicControlScreenElement.this.mMusicStateVar.set(1.0D);
                            MusicControlScreenElement.this.requestFramerate(30.0F);
                            MusicControlScreenElement.this.requestAlbum();
                        }
                    }
                }
            });
        }
    }

    public void pause()
    {
        super.pause();
        this.isPaused = true;
        this.mHandler.removeCallbacks(this.mCheckStreamMusicRunnable);
        if (this.mSpectrumVisualizer != null)
            this.mSpectrumVisualizer.enableUpdate(false);
    }

    public void resume()
    {
        super.resume();
        this.isPaused = false;
        if (isVisible())
            this.mHandler.postDelayed(this.mCheckStreamMusicRunnable, 1000L);
    }

    public void show(boolean paramBoolean)
    {
        super.show(paramBoolean);
        if (!paramBoolean)
        {
            this.mMusicStatus = 0;
            this.mHandler.removeCallbacks(this.mCheckStreamMusicRunnable);
            if (this.mSpectrumVisualizer != null)
                this.mSpectrumVisualizer.enableUpdate(false);
            requestFramerate(0.0F);
        }
        while (true)
        {
            return;
            updateMusic();
            this.mHandler.postDelayed(this.mCheckStreamMusicRunnable, 1000L);
        }
    }

    protected void updateSpectrumVisualizer()
    {
        boolean bool1 = this.mAudioManager.isMusicActive();
        SpectrumVisualizerScreenElement localSpectrumVisualizerScreenElement;
        if (this.mSpectrumVisualizer != null)
        {
            localSpectrumVisualizerScreenElement = this.mSpectrumVisualizer;
            if ((!bool1) || (!isVisible()) || (this.isPaused))
                break label46;
        }
        label46: for (boolean bool2 = true; ; bool2 = false)
        {
            localSpectrumVisualizerScreenElement.enableUpdate(bool2);
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.elements.MusicControlScreenElement
 * JD-Core Version:        0.6.2
 */