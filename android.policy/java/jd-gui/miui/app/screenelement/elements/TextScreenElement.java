package miui.app.screenelement.elements;

import android.graphics.Canvas;
import android.text.Layout.Alignment;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.Log;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.ScreenElementRoot;
import miui.app.screenelement.data.Expression;
import miui.app.screenelement.util.ColorParser;
import miui.app.screenelement.util.IndexedNumberVariable;
import miui.app.screenelement.util.TextFormatter;
import miui.app.screenelement.util.Utils;
import org.w3c.dom.Element;

public class TextScreenElement extends AnimatedScreenElement
{
    private static final int DEFAULT_SIZE = 18;
    private static final String LOG_TAG = "TextScreenElement";
    private static final int MARQUEE_FRAMERATE = 30;
    private static final int PADDING = 50;
    public static final String TAG_NAME = "Text";
    public static final String TEXT_HEIGHT = "text_height";
    public static final String TEXT_WIDTH = "text_width";
    private ColorParser mColorParser;
    private TextFormatter mFormatter;
    private float mMarqueePos = 3.4028235E+38F;
    private int mMarqueeSpeed;
    private boolean mMultiLine;
    private TextPaint mPaint = new TextPaint();
    private String mPreText;
    private long mPreviousTime;
    private Expression mSizeExpression;
    private float mSpacingAdd;
    private float mSpacingMult;
    private String mText;
    private IndexedNumberVariable mTextHeightVar;
    private StaticLayout mTextLayout;
    private float mTextWidth;
    private IndexedNumberVariable mTextWidthVar;

    public TextScreenElement(Element paramElement, ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot)
        throws ScreenElementLoadException
    {
        super(paramElement, paramScreenContext, paramScreenElementRoot);
        load(paramElement);
        if (this.mHasName)
        {
            this.mTextWidthVar = new IndexedNumberVariable(this.mName, "text_width", paramScreenContext.mVariables);
            this.mTextHeightVar = new IndexedNumberVariable(this.mName, "text_height", paramScreenContext.mVariables);
        }
    }

    private Layout.Alignment getAlignment()
    {
        Layout.Alignment localAlignment = Layout.Alignment.ALIGN_NORMAL;
        switch (1.$SwitchMap$miui$app$screenelement$elements$ScreenElement$Align[this.mAlign.ordinal()])
        {
        default:
        case 1:
        case 2:
        case 3:
        }
        while (true)
        {
            return localAlignment;
            localAlignment = Layout.Alignment.ALIGN_LEFT;
            continue;
            localAlignment = Layout.Alignment.ALIGN_CENTER;
            continue;
            localAlignment = Layout.Alignment.ALIGN_RIGHT;
        }
    }

    private void updateTextWidth()
    {
        if (TextUtils.isEmpty(this.mText));
        while (true)
        {
            return;
            if (this.mSizeExpression != null)
                this.mPaint.setTextSize(scale(this.mSizeExpression.evaluate(this.mContext.mVariables)));
            this.mTextWidth = this.mPaint.measureText(this.mText);
            if (this.mHasName)
                this.mTextWidthVar.set(descale(this.mTextWidth));
        }
    }

    public void doRender(Canvas paramCanvas)
    {
        if (TextUtils.isEmpty(this.mText))
            return;
        this.mPaint.setColor(getColor());
        this.mPaint.setAlpha(getAlpha());
        float f1 = getWidth();
        if ((f1 < 0.0F) || (f1 > this.mTextWidth))
            f1 = this.mTextWidth;
        float f2 = getHeight();
        float f3 = this.mPaint.getTextSize();
        if ((f2 < 0.0F) && (this.mTextLayout == null))
            f2 = f3;
        float f4 = getLeft(getX(), f1);
        if (f2 > 0.0F);
        for (float f5 = getTop(getY(), f2); ; f5 = getY())
        {
            paramCanvas.save();
            if ((f1 > 0.0F) && (f2 > 0.0F))
                paramCanvas.clipRect(f4, f5 - 10.0F, f4 + f1, 20.0F + (f5 + f2));
            if (this.mTextLayout == null)
                break;
            int i = this.mTextLayout.getLineCount();
            for (int j = 0; j < i; j++)
            {
                int k = this.mTextLayout.getLineStart(j);
                int m = this.mTextLayout.getLineEnd(j);
                int n = this.mTextLayout.getLineTop(j);
                float f7 = this.mTextLayout.getLineLeft(j);
                paramCanvas.drawText(this.mText, k, m, f4 + f7, f5 + f3 + n, this.mPaint);
            }
        }
        String str = this.mText;
        if (this.mMarqueePos == 3.4028235E+38F);
        for (float f6 = 0.0F; ; f6 = this.mMarqueePos)
        {
            paramCanvas.drawText(str, f6 + f4, f5 + f3, this.mPaint);
            paramCanvas.restore();
            break;
        }
    }

    protected int getColor()
    {
        return this.mColorParser.getColor(this.mContext.mVariables);
    }

    protected String getFormat()
    {
        return this.mFormatter.getFormat(this.mContext.mVariables);
    }

    protected String getText()
    {
        return this.mFormatter.getText(this.mContext.mVariables);
    }

    public void init()
    {
        super.init();
        this.mText = getText();
        updateTextWidth();
    }

    public void load(Element paramElement)
        throws ScreenElementLoadException
    {
        if (paramElement == null)
        {
            Log.e("TextScreenElement", "node is null");
            throw new ScreenElementLoadException("node is null");
        }
        this.mFormatter = TextFormatter.fromElement(paramElement);
        this.mColorParser = ColorParser.fromElement(paramElement);
        this.mSizeExpression = Expression.build(paramElement.getAttribute("size"));
        this.mMarqueeSpeed = Utils.getAttrAsInt(paramElement, "marqueeSpeed", 0);
        boolean bool = Boolean.parseBoolean(paramElement.getAttribute("bold"));
        this.mSpacingMult = Utils.getAttrAsFloat(paramElement, "spacingMult", 1.0F);
        this.mSpacingAdd = Utils.getAttrAsFloat(paramElement, "spacingAdd", 0.0F);
        this.mMultiLine = Boolean.parseBoolean(paramElement.getAttribute("multiLine"));
        this.mPaint.setColor(getColor());
        this.mPaint.setTextSize(18.0F);
        this.mPaint.setAntiAlias(true);
        this.mPaint.setFakeBoldText(bool);
    }

    protected void onVisibilityChange(boolean paramBoolean)
    {
        super.onVisibilityChange(paramBoolean);
        float f = getWidth();
        String str = getText();
        if (str == null)
            requestFramerate(0.0F);
        while (true)
        {
            return;
            int i = (int)this.mPaint.measureText(str);
            if ((paramBoolean) && (this.mMarqueeSpeed > 0) && (f > 0.0F) && (i > f))
                requestFramerate(30.0F);
            else
                requestFramerate(0.0F);
        }
    }

    public void setText(String paramString)
    {
        this.mFormatter.setText(paramString);
    }

    public void tick(long paramLong)
    {
        super.tick(paramLong);
        if (!isVisible());
        while (true)
        {
            return;
            this.mText = getText();
            if (TextUtils.isEmpty(this.mText))
            {
                this.mTextLayout = null;
            }
            else
            {
                updateTextWidth();
                float f = getWidth();
                if ((f > 0.0F) && (this.mTextWidth > f))
                {
                    if (this.mMultiLine)
                    {
                        if ((this.mTextLayout == null) || (!this.mPreText.equals(this.mText)))
                        {
                            this.mPreText = this.mText;
                            this.mTextLayout = new StaticLayout(this.mText, this.mPaint, (int)f, getAlignment(), this.mSpacingMult, this.mSpacingAdd, true);
                            if (this.mHasName)
                                this.mTextHeightVar.set(descale(this.mTextLayout.getLineTop(this.mTextLayout.getLineCount())));
                        }
                    }
                    else if (this.mMarqueeSpeed > 0)
                    {
                        if (this.mMarqueePos == 3.4028235E+38F)
                            this.mMarqueePos = 50.0F;
                        while (true)
                        {
                            this.mPreviousTime = paramLong;
                            break;
                            this.mMarqueePos -= (float)(this.mMarqueeSpeed * (paramLong - this.mPreviousTime)) / 1000.0F;
                            if (this.mMarqueePos < f - this.mTextWidth - 50.0F)
                                this.mMarqueePos = 50.0F;
                        }
                    }
                }
                else
                {
                    this.mTextLayout = null;
                    this.mMarqueePos = 3.4028235E+38F;
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.elements.TextScreenElement
 * JD-Core Version:        0.6.2
 */