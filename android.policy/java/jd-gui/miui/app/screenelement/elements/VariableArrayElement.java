package miui.app.screenelement.elements;

import android.graphics.Canvas;
import java.util.ArrayList;
import java.util.Iterator;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementRoot;
import miui.app.screenelement.data.Expression;
import miui.app.screenelement.data.Variables;
import miui.app.screenelement.util.IndexedNumberVariable;
import miui.app.screenelement.util.IndexedStringVariable;
import miui.app.screenelement.util.Utils;
import miui.app.screenelement.util.Utils.XmlTraverseListener;
import org.w3c.dom.Element;

public class VariableArrayElement extends ScreenElement
{
    public static final String TAG_NAME = "VarArray";
    private ArrayList<Item> mArray = new ArrayList();
    private boolean mIsStringType;
    private ArrayList<Var> mVars = new ArrayList();

    public VariableArrayElement(Element paramElement, ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot)
    {
        super(paramElement, paramScreenContext, paramScreenElementRoot);
        if (paramElement != null)
        {
            this.mIsStringType = "string".equalsIgnoreCase(paramElement.getAttribute("type"));
            Utils.traverseXmlElementChildren(Utils.getChild(paramElement, "Vars"), "Var", new Utils.XmlTraverseListener()
            {
                public void onChild(Element paramAnonymousElement)
                {
                    VariableArrayElement.this.mVars.add(new VariableArrayElement.Var(VariableArrayElement.this, paramAnonymousElement));
                }
            });
            Utils.traverseXmlElementChildren(Utils.getChild(paramElement, "Items"), "Item", new Utils.XmlTraverseListener()
            {
                public void onChild(Element paramAnonymousElement)
                {
                    VariableArrayElement.this.mArray.add(new VariableArrayElement.Item(VariableArrayElement.this, paramAnonymousElement));
                }
            });
        }
    }

    public void doRender(Canvas paramCanvas)
    {
    }

    public void init()
    {
        Iterator localIterator = this.mVars.iterator();
        while (localIterator.hasNext())
            ((Var)localIterator.next()).init();
    }

    public void tick(long paramLong)
    {
        Iterator localIterator = this.mVars.iterator();
        while (localIterator.hasNext())
            ((Var)localIterator.next()).tick();
    }

    private class Item
    {
        public Expression mExpression;
        public double mNumValue;
        public String mStrValue;

        public Item(Element arg2)
        {
            Object localObject;
            if (localObject != null)
            {
                this.mExpression = Expression.build(localObject.getAttribute("expression"));
                this.mStrValue = localObject.getAttribute("value");
                if (VariableArrayElement.this.mIsStringType);
            }
            try
            {
                this.mNumValue = Double.parseDouble(this.mStrValue);
                label58: return;
            }
            catch (NumberFormatException localNumberFormatException)
            {
                break label58;
            }
        }

        public Double evaluate(Variables paramVariables)
        {
            Double localDouble;
            if (this.mExpression != null)
                if (this.mExpression.isNull(paramVariables))
                    localDouble = null;
            while (true)
            {
                return localDouble;
                localDouble = Double.valueOf(this.mExpression.evaluate(paramVariables));
                continue;
                localDouble = Double.valueOf(this.mNumValue);
            }
        }

        public String evaluateStr(Variables paramVariables)
        {
            if (this.mExpression != null);
            for (String str = this.mExpression.evaluateStr(paramVariables); ; str = this.mStrValue)
                return str;
        }

        public boolean isExpression()
        {
            if (this.mExpression != null);
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }

    private class Var
    {
        private boolean mConst;
        private boolean mCurrentItemIsExpression;
        private int mIndex = -1;
        private Expression mIndexExpression;
        private String mName;
        private IndexedNumberVariable mNumberVar;
        private IndexedStringVariable mStringVar;

        public Var(Element arg2)
        {
            Object localObject;
            if (localObject != null)
            {
                this.mName = localObject.getAttribute("name");
                this.mIndexExpression = Expression.build(localObject.getAttribute("index"));
                this.mConst = Boolean.parseBoolean(localObject.getAttribute("const"));
                if (!VariableArrayElement.this.mIsStringType)
                    break label91;
                this.mStringVar = new IndexedStringVariable(this.mName, VariableArrayElement.this.mContext.mVariables);
            }
            while (true)
            {
                return;
                label91: this.mNumberVar = new IndexedNumberVariable(this.mName, VariableArrayElement.this.mContext.mVariables);
            }
        }

        private void update()
        {
            if (this.mIndexExpression == null);
            while (true)
            {
                return;
                Variables localVariables = VariableArrayElement.this.mContext.mVariables;
                int i = (int)this.mIndexExpression.evaluate(localVariables);
                if ((i < 0) || (i >= VariableArrayElement.this.mArray.size()))
                {
                    if (VariableArrayElement.this.mIsStringType)
                        this.mStringVar.set(null);
                    else
                        this.mNumberVar.set(null);
                }
                else if ((this.mIndex != i) || (this.mCurrentItemIsExpression))
                {
                    VariableArrayElement.Item localItem = (VariableArrayElement.Item)VariableArrayElement.this.mArray.get(i);
                    if (this.mIndex != i)
                    {
                        this.mIndex = i;
                        this.mCurrentItemIsExpression = localItem.isExpression();
                    }
                    if (VariableArrayElement.this.mIsStringType)
                        this.mStringVar.set(localItem.evaluateStr(localVariables));
                    else
                        this.mNumberVar.set(localItem.evaluate(localVariables));
                }
            }
        }

        public void init()
        {
            update();
        }

        public void tick()
        {
            if (!this.mConst)
                update();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.elements.VariableArrayElement
 * JD-Core Version:        0.6.2
 */