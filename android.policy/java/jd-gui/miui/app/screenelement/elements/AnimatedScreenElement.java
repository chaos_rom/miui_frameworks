package miui.app.screenelement.elements;

import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.text.TextUtils;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.ScreenElementRoot;
import miui.app.screenelement.animation.AnimatedElement;
import miui.app.screenelement.data.Expression;
import miui.app.screenelement.util.IndexedNumberVariable;
import miui.app.screenelement.util.Utils;
import org.w3c.dom.Element;

public abstract class AnimatedScreenElement extends ScreenElement
{
    private IndexedNumberVariable mActualXVar;
    private IndexedNumberVariable mActualYVar;
    protected AnimatedElement mAni = new AnimatedElement(paramElement, this.mContext);
    private Camera mCamera;
    private Matrix mMatrix = new Matrix();
    private Expression mPivotZ;
    private Expression mRotationX;
    private Expression mRotationY;
    private Expression mRotationZ;
    private Expression mScaleExpression;
    private Expression mScaleXExpression;
    private Expression mScaleYExpression;

    public AnimatedScreenElement(Element paramElement, ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot)
        throws ScreenElementLoadException
    {
        super(paramElement, paramScreenContext, paramScreenElementRoot);
        if (this.mHasName)
        {
            this.mActualXVar = new IndexedNumberVariable(this.mName, "actual_x", paramScreenContext.mVariables);
            this.mActualYVar = new IndexedNumberVariable(this.mName, "actual_y", paramScreenContext.mVariables);
        }
        this.mScaleExpression = createExp(paramElement, "scale", null);
        this.mScaleXExpression = createExp(paramElement, "scaleX", null);
        this.mScaleYExpression = createExp(paramElement, "scaleY", null);
        this.mRotationX = createExp(paramElement, "angleX", "rotationX");
        this.mRotationY = createExp(paramElement, "angleY", "rotationY");
        this.mRotationZ = createExp(paramElement, "angleZ", "rotationZ");
        this.mPivotZ = createExp(paramElement, "centerZ", "pivotZ");
        if ((this.mRotationX != null) || (this.mRotationY != null) || (this.mRotationZ != null))
            this.mCamera = new Camera();
    }

    private Expression createExp(Element paramElement, String paramString1, String paramString2)
    {
        Expression localExpression = Expression.build(paramElement.getAttribute(paramString1));
        if ((localExpression == null) && (!TextUtils.isEmpty(paramString2)))
            localExpression = Expression.build(paramElement.getAttribute(paramString2));
        return localExpression;
    }

    public int getAlpha()
    {
        int i = this.mAni.getAlpha();
        if (this.mParent == null);
        while (true)
        {
            return i;
            i = Utils.mixAlpha(i, this.mParent.getAlpha());
        }
    }

    public float getHeight()
    {
        return scale(this.mAni.getHeight());
    }

    protected float getLeft()
    {
        return getLeft(getX(), getWidth());
    }

    public float getMaxHeight()
    {
        return scale(this.mAni.getMaxHeight());
    }

    public float getMaxWidth()
    {
        return scale(this.mAni.getMaxWidth());
    }

    public float getPivotX()
    {
        return scale(this.mAni.getPivotX());
    }

    public float getPivotY()
    {
        return scale(this.mAni.getPivotY());
    }

    public float getRotation()
    {
        return this.mAni.getRotationAngle();
    }

    protected float getTop()
    {
        return getTop(getY(), getHeight());
    }

    public float getWidth()
    {
        return scale(this.mAni.getWidth());
    }

    public float getX()
    {
        return scale(this.mAni.getX());
    }

    public float getY()
    {
        return scale(this.mAni.getY());
    }

    public void init()
    {
        super.init();
        this.mAni.init();
    }

    protected boolean isVisibleInner()
    {
        if ((super.isVisibleInner()) && (getAlpha() > 0));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void render(Canvas paramCanvas)
    {
        updateVisibility();
        if (!this.mIsVisible)
            return;
        this.mMatrix.reset();
        int i = 0;
        float f1 = getLeft() + getPivotX();
        float f2 = getTop() + getPivotY();
        if (this.mCamera != null)
        {
            this.mCamera.save();
            if (this.mRotationX != null)
            {
                this.mCamera.rotateX((float)this.mRotationX.evaluate(this.mContext.mVariables));
                i = 1;
            }
            if (this.mRotationY != null)
            {
                this.mCamera.rotateY((float)this.mRotationY.evaluate(this.mContext.mVariables));
                i = 1;
            }
            if (this.mRotationZ != null)
            {
                this.mCamera.rotateZ((float)this.mRotationZ.evaluate(this.mContext.mVariables));
                i = 1;
            }
            if (this.mPivotZ != null)
                this.mCamera.translate(0.0F, 0.0F, (float)this.mPivotZ.evaluate(this.mContext.mVariables));
            this.mCamera.getMatrix(this.mMatrix);
            this.mMatrix.preTranslate(-f1, -f2);
            this.mMatrix.postTranslate(f1, f2);
            this.mCamera.restore();
        }
        int j = 0;
        if (i != 0)
        {
            j = paramCanvas.save();
            paramCanvas.concat(this.mMatrix);
            this.mMatrix.reset();
        }
        int k = 0;
        float f3 = getRotation();
        if (f3 != 0.0F)
        {
            this.mMatrix.setRotate(f3, f1, f2);
            k = 1;
        }
        if (this.mScaleExpression != null)
        {
            f6 = (float)this.mScaleExpression.evaluate(this.mContext.mVariables);
            this.mMatrix.setScale(f6, f6, f1, f2);
            k = 1;
        }
        while ((this.mScaleXExpression == null) && (this.mScaleYExpression == null))
        {
            float f6;
            if (k != 0)
            {
                if (i == 0)
                    j = paramCanvas.save();
                paramCanvas.concat(this.mMatrix);
            }
            doRender(paramCanvas);
            if ((i == 0) && (k == 0))
                break;
            paramCanvas.restoreToCount(j);
            break;
        }
        float f4;
        if (this.mScaleXExpression == null)
        {
            f4 = 1.0F;
            label392: if (this.mScaleYExpression != null)
                break label442;
        }
        label442: for (float f5 = 1.0F; ; f5 = (float)this.mScaleYExpression.evaluate(this.mContext.mVariables))
        {
            this.mMatrix.setScale(f4, f5, f1, f2);
            k = 1;
            break;
            f4 = (float)this.mScaleXExpression.evaluate(this.mContext.mVariables);
            break label392;
        }
    }

    public void reset(long paramLong)
    {
        super.reset(paramLong);
        this.mAni.reset(paramLong);
    }

    public void tick(long paramLong)
    {
        super.tick(paramLong);
        this.mAni.tick(paramLong);
        if (this.mHasName)
        {
            this.mActualXVar.set(this.mAni.getX());
            this.mActualYVar.set(this.mAni.getY());
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.elements.AnimatedScreenElement
 * JD-Core Version:        0.6.2
 */