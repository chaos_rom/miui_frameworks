package miui.app.screenelement.elements;

import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;
import java.util.ArrayList;
import java.util.Iterator;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.ScreenElementRoot;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ElementGroup extends AnimatedScreenElement
{
    private static final String LOG_TAG = "LockScreen_ElementGroup";
    public static final String TAG_NAME = "ElementGroup";
    public static final String TAG_NAME1 = "Group";
    private boolean mClip;
    protected ArrayList<ScreenElement> mElements = new ArrayList();

    public ElementGroup(Element paramElement, ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot)
        throws ScreenElementLoadException
    {
        super(paramElement, paramScreenContext, paramScreenElementRoot);
        this.mClip = Boolean.parseBoolean(paramElement.getAttribute("clip"));
        load(paramElement, paramScreenElementRoot);
    }

    public void doRender(Canvas paramCanvas)
    {
        float f1 = getX();
        float f2 = getY();
        int i = paramCanvas.save();
        paramCanvas.translate(f1, f2);
        float f3 = getWidth();
        float f4 = getHeight();
        if ((f3 > 0.0F) && (f4 > 0.0F) && (this.mClip))
            paramCanvas.clipRect(0.0F, 0.0F, f3, f4);
        Iterator localIterator = this.mElements.iterator();
        while (localIterator.hasNext())
            ((ScreenElement)localIterator.next()).render(paramCanvas);
        paramCanvas.restoreToCount(i);
    }

    public ScreenElement findElement(String paramString)
    {
        ScreenElement localScreenElement1 = super.findElement(paramString);
        Object localObject;
        if (localScreenElement1 != null)
            localObject = localScreenElement1;
        while (true)
        {
            return localObject;
            Iterator localIterator = this.mElements.iterator();
            while (true)
                if (localIterator.hasNext())
                {
                    ScreenElement localScreenElement2 = ((ScreenElement)localIterator.next()).findElement(paramString);
                    if (localScreenElement2 != null)
                    {
                        localObject = localScreenElement2;
                        break;
                    }
                }
            localObject = null;
        }
    }

    public void finish()
    {
        super.finish();
        Iterator localIterator = this.mElements.iterator();
        while (localIterator.hasNext())
        {
            ScreenElement localScreenElement = (ScreenElement)localIterator.next();
            try
            {
                localScreenElement.finish();
            }
            catch (Exception localException)
            {
                Log.e("LockScreen_ElementGroup", localException.toString());
                localException.printStackTrace();
            }
        }
    }

    public ArrayList<ScreenElement> getElements()
    {
        return this.mElements;
    }

    public void init()
    {
        super.init();
        Iterator localIterator = this.mElements.iterator();
        while (localIterator.hasNext())
            ((ScreenElement)localIterator.next()).init();
    }

    public void load(Element paramElement, ScreenElementRoot paramScreenElementRoot)
        throws ScreenElementLoadException
    {
        if (paramElement == null)
        {
            Log.e("LockScreen_ElementGroup", "node is null");
            throw new ScreenElementLoadException("node is null");
        }
        ScreenElementFactory localScreenElementFactory = this.mContext.mFactory;
        NodeList localNodeList = paramElement.getChildNodes();
        for (int i = 0; i < localNodeList.getLength(); i++)
            if (localNodeList.item(i).getNodeType() == 1)
            {
                ScreenElement localScreenElement = localScreenElementFactory.createInstance((Element)localNodeList.item(i), this.mContext, paramScreenElementRoot);
                if (localScreenElement != null)
                {
                    localScreenElement.setParent(this);
                    this.mElements.add(localScreenElement);
                }
            }
    }

    public boolean onTouch(MotionEvent paramMotionEvent)
    {
        boolean bool;
        if (!isVisible())
            bool = false;
        while (true)
        {
            return bool;
            bool = super.onTouch(paramMotionEvent);
            Iterator localIterator = this.mElements.iterator();
            while (localIterator.hasNext())
                if (((ScreenElement)localIterator.next()).onTouch(paramMotionEvent))
                    bool = true;
        }
    }

    protected void onVisibilityChange(boolean paramBoolean)
    {
        super.onVisibilityChange(paramBoolean);
        Iterator localIterator = this.mElements.iterator();
        while (localIterator.hasNext())
            ((ScreenElement)localIterator.next()).onVisibilityChange(paramBoolean);
    }

    public void pause()
    {
        super.pause();
        Iterator localIterator = this.mElements.iterator();
        while (localIterator.hasNext())
            ((ScreenElement)localIterator.next()).pause();
    }

    public void reset(long paramLong)
    {
        super.reset(paramLong);
        Iterator localIterator = this.mElements.iterator();
        while (localIterator.hasNext())
            ((ScreenElement)localIterator.next()).reset(paramLong);
    }

    public void resume()
    {
        super.resume();
        Iterator localIterator = this.mElements.iterator();
        while (localIterator.hasNext())
            ((ScreenElement)localIterator.next()).resume();
    }

    public void showCategory(String paramString, boolean paramBoolean)
    {
        super.showCategory(paramString, paramBoolean);
        Iterator localIterator = this.mElements.iterator();
        while (localIterator.hasNext())
            ((ScreenElement)localIterator.next()).showCategory(paramString, paramBoolean);
    }

    public void tick(long paramLong)
    {
        super.tick(paramLong);
        if (!this.mIsVisible);
        while (true)
        {
            return;
            Iterator localIterator = this.mElements.iterator();
            while (localIterator.hasNext())
                ((ScreenElement)localIterator.next()).tick(paramLong);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.elements.ElementGroup
 * JD-Core Version:        0.6.2
 */