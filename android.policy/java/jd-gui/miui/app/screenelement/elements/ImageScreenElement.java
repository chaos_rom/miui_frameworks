package miui.app.screenelement.elements;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.NinePatch;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.UUID;
import miui.app.screenelement.ResourceManager;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.ScreenElementRoot;
import miui.app.screenelement.animation.AnimatedElement;
import miui.app.screenelement.data.Expression;
import miui.app.screenelement.util.IndexedNumberVariable;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class ImageScreenElement extends AnimatedScreenElement
{
    private static final String LOG_TAG = "ImageScreenElement";
    public static final String MASK_TAG_NAME = "Mask";
    public static final String TAG_NAME = "Image";
    private static final String VAR_BMP_HEIGHT = "bmp_height";
    private static final String VAR_BMP_WIDTH = "bmp_width";
    private float mAniHeight;
    private float mAniWidth;
    private boolean mAntiAlias;
    protected Bitmap mBitmap;
    protected boolean mBitmapChanged;
    private float mBmpHeight;
    private IndexedNumberVariable mBmpSizeHeightVar;
    private IndexedNumberVariable mBmpSizeWidthVar;
    private float mBmpWidth;
    private Canvas mBufferCanvas;
    private Bitmap mCachedBitmap;
    private String mCachedBitmapName;
    protected Bitmap mCurrentBitmap;
    private Rect mDesRect = new Rect();
    private float mHeight;
    private String mKey;
    private Bitmap mMaskBuffer;
    private Paint mMaskPaint = new Paint();
    private ArrayList<AnimatedElement> mMasks;
    protected Paint mPaint = new Paint();
    private pair<Double, Double> mRotateXYpair;
    private Expression mSrcH;
    private Rect mSrcRect;
    private SrcType mSrcType = SrcType.ResourceImage;
    private Expression mSrcW;
    private Expression mSrcX;
    private Expression mSrcY;
    private boolean mUseVirtualScreen;
    private VirtualScreen mVirtualScreen;
    private float mWidth;
    private float mX;
    private float mY;

    public ImageScreenElement(Element paramElement, ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot)
        throws ScreenElementLoadException
    {
        super(paramElement, paramScreenContext, paramScreenElementRoot);
        load(paramElement);
        this.mAntiAlias = true;
        this.mPaint.setFilterBitmap(this.mAntiAlias);
        this.mMaskPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        this.mSrcX = Expression.build(paramElement.getAttribute("srcX"));
        this.mSrcY = Expression.build(paramElement.getAttribute("srcY"));
        this.mSrcW = Expression.build(paramElement.getAttribute("srcW"));
        this.mSrcH = Expression.build(paramElement.getAttribute("srcH"));
        if ((this.mSrcX != null) && (this.mSrcY != null) && (this.mSrcW != null) && (this.mSrcH != null))
            this.mSrcRect = new Rect();
        this.mUseVirtualScreen = Boolean.parseBoolean(paramElement.getAttribute("useVirtualScreen"));
        String str = paramElement.getAttribute("srcType");
        if (str.equals("ResourceImage"))
            this.mSrcType = SrcType.ResourceImage;
        while (true)
        {
            if (this.mHasName)
            {
                this.mBmpSizeWidthVar = new IndexedNumberVariable(this.mName, "bmp_width", paramScreenContext.mVariables);
                this.mBmpSizeHeightVar = new IndexedNumberVariable(this.mName, "bmp_height", paramScreenContext.mVariables);
            }
            return;
            if ((this.mUseVirtualScreen) || (str.equals("VirtualScreen")))
                this.mSrcType = SrcType.VirtualScreen;
            else if (str.equals("ApplicationIcon"))
                this.mSrcType = SrcType.ApplicationIcon;
            else
                this.mSrcType = SrcType.ResourceImage;
        }
    }

    private String getKey()
    {
        if (this.mKey == null)
            this.mKey = UUID.randomUUID().toString();
        return this.mKey;
    }

    private void loadMask(Element paramElement)
        throws ScreenElementLoadException
    {
        if (this.mMasks == null)
            this.mMasks = new ArrayList();
        this.mMasks.clear();
        NodeList localNodeList = paramElement.getElementsByTagName("Mask");
        for (int i = 0; i < localNodeList.getLength(); i++)
            this.mMasks.add(new AnimatedElement((Element)localNodeList.item(i), this.mContext));
    }

    private void renderWithMask(Canvas paramCanvas, AnimatedElement paramAnimatedElement, int paramInt1, int paramInt2)
    {
        paramCanvas.save();
        Bitmap localBitmap = this.mContext.mResourceManager.getBitmap(paramAnimatedElement.getSrc());
        if (localBitmap == null);
        double d1;
        double d2;
        float f1;
        float f4;
        while (true)
        {
            return;
            d1 = scale(paramAnimatedElement.getX());
            d2 = scale(paramAnimatedElement.getY());
            f1 = paramAnimatedElement.getRotationAngle();
            if (paramAnimatedElement.isAlignAbsolute())
            {
                f4 = getRotation();
                if (f4 != 0.0F)
                    break;
                d1 -= paramInt1;
                d2 -= paramInt2;
            }
            float f2 = (float)(d1 + scale(paramAnimatedElement.getPivotX()));
            float f3 = (float)(d2 + scale(paramAnimatedElement.getPivotY()));
            paramCanvas.rotate(f1, f2, f3);
            int i = (int)d1;
            int j = (int)d2;
            int k = Math.round(scale(paramAnimatedElement.getWidth()));
            if (k < 0)
                k = localBitmap.getWidth();
            int m = Math.round(scale(paramAnimatedElement.getHeight()));
            if (m < 0)
                m = localBitmap.getHeight();
            this.mDesRect.set(i, j, i + k, j + m);
            this.mMaskPaint.setAlpha(paramAnimatedElement.getAlpha());
            paramCanvas.drawBitmap(localBitmap, null, this.mDesRect, this.mMaskPaint);
            paramCanvas.restore();
        }
        f1 -= f4;
        double d3 = 3.1415926535897D * f4 / 180.0D;
        double d4 = getPivotX();
        double d5 = getPivotY();
        if (this.mRotateXYpair == null)
            this.mRotateXYpair = new pair(null);
        rotateXY(d4, d5, d3, this.mRotateXYpair);
        double d6 = paramInt1 + ((Double)this.mRotateXYpair.p1).doubleValue();
        double d7 = paramInt2 + ((Double)this.mRotateXYpair.p2).doubleValue();
        rotateXY(paramAnimatedElement.getPivotX(), paramAnimatedElement.getPivotY(), 3.1415926535897D * paramAnimatedElement.getRotationAngle() / 180.0D, this.mRotateXYpair);
        double d8 = d1 + scale(((Double)this.mRotateXYpair.p1).doubleValue());
        double d9 = d2 + scale(((Double)this.mRotateXYpair.p2).doubleValue());
        double d10 = d8 - d6;
        double d11 = d9 - d7;
        double d12 = Math.sqrt(d10 * d10 + d11 * d11);
        double d13 = Math.asin(d10 / d12);
        if (d11 > 0.0D);
        for (double d14 = d3 + d13; ; d14 = 3.1415926535897D + d3 - d13)
        {
            d1 = d12 * Math.sin(d14);
            d2 = d12 * Math.cos(d14);
            break;
        }
    }

    private void rotateXY(double paramDouble1, double paramDouble2, double paramDouble3, pair<Double, Double> parampair)
    {
        double d1 = Math.sqrt(paramDouble1 * paramDouble1 + paramDouble2 * paramDouble2);
        double d2;
        if (d1 > 0.0D)
        {
            d2 = 3.1415926535897D - Math.acos(paramDouble1 / d1) - paramDouble3;
            parampair.p1 = Double.valueOf(paramDouble1 + d1 * Math.cos(d2));
        }
        for (parampair.p2 = Double.valueOf(paramDouble2 - d1 * Math.sin(d2)); ; parampair.p2 = Double.valueOf(0.0D))
        {
            return;
            parampair.p1 = Double.valueOf(0.0D);
        }
    }

    private void updateBitmap()
    {
        this.mCurrentBitmap = getBitmap();
        updateBmpSizeVar();
        this.mAniWidth = super.getWidth();
        this.mBmpWidth = getBitmapWidth();
        if (this.mAniWidth >= 0.0F)
        {
            this.mWidth = this.mAniWidth;
            this.mAniHeight = super.getHeight();
            this.mBmpHeight = getBitmapHeight();
            if (this.mAniHeight < 0.0F)
                break label108;
        }
        label108: for (this.mHeight = this.mAniHeight; ; this.mHeight = this.mBmpHeight)
        {
            this.mX = super.getX();
            this.mY = super.getY();
            return;
            this.mWidth = this.mBmpWidth;
            break;
        }
    }

    private void updateBmpSizeVar()
    {
        if ((this.mHasName) && (this.mBitmapChanged))
        {
            this.mBmpSizeWidthVar.set(descale(getBitmapWidth()));
            this.mBmpSizeHeightVar.set(descale(getBitmapHeight()));
            this.mBitmapChanged = false;
        }
    }

    public void doRender(Canvas paramCanvas)
    {
        Bitmap localBitmap = this.mCurrentBitmap;
        if (localBitmap == null);
        boolean bool;
        int j;
        do
        {
            return;
            bool = paramCanvas.isHardwareAccelerated();
            int i = getAlpha();
            this.mPaint.setAlpha(i);
            j = paramCanvas.getDensity();
            paramCanvas.setDensity(0);
        }
        while ((this.mWidth == 0.0F) || (this.mHeight == 0.0F));
        int k = (int)getLeft(this.mX, this.mWidth);
        int m = (int)getTop(this.mY, this.mHeight);
        paramCanvas.save();
        if (this.mMasks.size() == 0)
            if (localBitmap.getNinePatchChunk() != null)
            {
                NinePatch localNinePatch = this.mContext.mResourceManager.getNinePatch(this.mAni.getSrc());
                if (localNinePatch != null)
                {
                    this.mDesRect.set(k, m, (int)(k + this.mWidth), (int)(m + this.mHeight));
                    localNinePatch.draw(paramCanvas, this.mDesRect, this.mPaint);
                }
            }
        while (true)
        {
            paramCanvas.restore();
            paramCanvas.setDensity(j);
            break;
            Log.e("ImageScreenElement", "the image contains ninepatch chunk but couldn't get NinePatch object: " + this.mAni.getSrc());
            continue;
            if ((this.mAniWidth > 0.0F) || (this.mAniHeight > 0.0F) || (this.mSrcRect != null))
            {
                this.mDesRect.set(k, m, (int)(k + this.mWidth), (int)(m + this.mHeight));
                if (this.mSrcRect != null)
                {
                    int i6 = (int)scale(this.mSrcX.evaluate(this.mContext.mVariables));
                    int i7 = (int)scale(this.mSrcY.evaluate(this.mContext.mVariables));
                    int i8 = (int)scale(this.mSrcW.evaluate(this.mContext.mVariables));
                    int i9 = (int)scale(this.mSrcH.evaluate(this.mContext.mVariables));
                    this.mSrcRect.set(i6, i7, i6 + i8, i7 + i9);
                }
                paramCanvas.drawBitmap(localBitmap, this.mSrcRect, this.mDesRect, this.mPaint);
            }
            else
            {
                paramCanvas.drawBitmap(localBitmap, k, m, this.mPaint);
                continue;
                float f1 = getMaxWidth();
                float f2 = getMaxHeight();
                float f3 = Math.max(f1, this.mWidth);
                float f4 = Math.max(f2, this.mHeight);
                int n = (int)Math.ceil(f3);
                int i1 = (int)Math.ceil(f4);
                if ((this.mMaskBuffer == null) || (n > this.mMaskBuffer.getWidth()) || (i1 > this.mMaskBuffer.getHeight()) || (!bool))
                {
                    this.mMaskBuffer = this.mContext.mResourceManager.getMaskBufferBitmap(n, i1, getKey(), bool);
                    this.mMaskBuffer.setDensity(localBitmap.getDensity());
                    this.mBufferCanvas = new Canvas(this.mMaskBuffer);
                }
                this.mBufferCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
                float f5 = this.mRoot.getScale();
                if ((this.mAniWidth > 0.0F) || (this.mAniHeight > 0.0F) || (this.mSrcRect != null))
                {
                    this.mDesRect.set(0, 0, (int)this.mWidth, (int)this.mHeight);
                    if (this.mSrcRect != null)
                    {
                        int i2 = (int)(f5 * this.mSrcX.evaluate(this.mContext.mVariables));
                        int i3 = (int)(f5 * this.mSrcY.evaluate(this.mContext.mVariables));
                        int i4 = (int)(f5 * this.mSrcW.evaluate(this.mContext.mVariables));
                        int i5 = (int)(f5 * this.mSrcH.evaluate(this.mContext.mVariables));
                        this.mSrcRect.set(i2, i3, i2 + i4, i3 + i5);
                    }
                    this.mBufferCanvas.drawBitmap(localBitmap, this.mSrcRect, this.mDesRect, this.mPaint);
                }
                while (true)
                {
                    Iterator localIterator = this.mMasks.iterator();
                    while (localIterator.hasNext())
                    {
                        AnimatedElement localAnimatedElement = (AnimatedElement)localIterator.next();
                        renderWithMask(this.mBufferCanvas, localAnimatedElement, k, m);
                    }
                    this.mBufferCanvas.drawBitmap(localBitmap, 0.0F, 0.0F, null);
                }
                paramCanvas.drawBitmap(this.mMaskBuffer, k, m, this.mPaint);
            }
        }
    }

    protected Bitmap getBitmap()
    {
        Bitmap localBitmap;
        switch (1.$SwitchMap$miui$app$screenelement$elements$ImageScreenElement$SrcType[this.mSrcType.ordinal()])
        {
        default:
            if (this.mBitmap != null)
                localBitmap = this.mBitmap;
            break;
        case 1:
        case 2:
        }
        while (true)
        {
            return localBitmap;
            if (this.mVirtualScreen != null)
            {
                localBitmap = this.mVirtualScreen.getBitmap();
            }
            else
            {
                localBitmap = null;
                continue;
                localBitmap = this.mBitmap;
                continue;
                String str = this.mAni.getSrc();
                if (!TextUtils.equals(this.mCachedBitmapName, str))
                {
                    this.mCachedBitmapName = str;
                    this.mCachedBitmap = this.mContext.mResourceManager.getBitmap(str);
                    this.mBitmapChanged = true;
                }
                localBitmap = this.mCachedBitmap;
            }
        }
    }

    protected int getBitmapHeight()
    {
        if (this.mCurrentBitmap != null);
        for (int i = this.mCurrentBitmap.getHeight(); ; i = 0)
            return i;
    }

    protected int getBitmapWidth()
    {
        if (this.mCurrentBitmap != null);
        for (int i = this.mCurrentBitmap.getWidth(); ; i = 0)
            return i;
    }

    public float getHeight()
    {
        return this.mHeight;
    }

    public float getWidth()
    {
        return this.mWidth;
    }

    public float getX()
    {
        return this.mX;
    }

    public float getY()
    {
        return this.mY;
    }

    public void init()
    {
        super.init();
        if (this.mMasks != null)
        {
            Iterator localIterator = this.mMasks.iterator();
            while (localIterator.hasNext())
                ((AnimatedElement)localIterator.next()).init();
        }
        this.mBitmapChanged = true;
        switch (1.$SwitchMap$miui$app$screenelement$elements$ImageScreenElement$SrcType[this.mSrcType.ordinal()])
        {
        default:
        case 1:
        case 2:
        }
        while (true)
        {
            updateBitmap();
            return;
            ScreenElement localScreenElement = this.mRoot.findElement(this.mAni.getSrc());
            if ((localScreenElement instanceof VirtualScreen))
            {
                this.mVirtualScreen = ((VirtualScreen)localScreenElement);
                continue;
                String str = this.mAni.getSrc();
                if (str != null)
                {
                    String[] arrayOfString = str.split(",");
                    if (arrayOfString.length == 2)
                        try
                        {
                            Drawable localDrawable = this.mContext.mContext.getPackageManager().getActivityIcon(new ComponentName(arrayOfString[0], arrayOfString[1]));
                            if (!(localDrawable instanceof BitmapDrawable))
                                continue;
                            this.mBitmap = ((BitmapDrawable)localDrawable).getBitmap();
                        }
                        catch (PackageManager.NameNotFoundException localNameNotFoundException)
                        {
                            Log.e("ImageScreenElement", "fail to get icon for src of ApplicationIcon type: " + str);
                        }
                    else
                        Log.e("ImageScreenElement", "invalid src of ApplicationIcon type: " + str);
                }
                else
                {
                    Log.e("ImageScreenElement", "invalid src of ApplicationIcon type: " + str);
                }
            }
        }
    }

    public void load(Element paramElement)
        throws ScreenElementLoadException
    {
        if (paramElement == null)
        {
            Log.e("ImageScreenElement", "node is null");
            throw new ScreenElementLoadException("node is null");
        }
        loadMask(paramElement);
    }

    public void reset(long paramLong)
    {
        super.reset(paramLong);
        if (this.mMasks != null)
        {
            Iterator localIterator = this.mMasks.iterator();
            while (localIterator.hasNext())
                ((AnimatedElement)localIterator.next()).reset(paramLong);
        }
    }

    public void setBitmap(Bitmap paramBitmap)
    {
        this.mBitmap = paramBitmap;
    }

    protected boolean shouldScaleBitmap()
    {
        return true;
    }

    public void tick(long paramLong)
    {
        super.tick(paramLong);
        if (!isVisible());
        while (true)
        {
            return;
            if (this.mMasks != null)
            {
                Iterator localIterator = this.mMasks.iterator();
                while (localIterator.hasNext())
                    ((AnimatedElement)localIterator.next()).tick(paramLong);
            }
            updateBitmap();
        }
    }

    private static class pair<T1, T2>
    {
        public T1 p1;
        public T2 p2;
    }

    private static enum SrcType
    {
        static
        {
            ApplicationIcon = new SrcType("ApplicationIcon", 2);
            SrcType[] arrayOfSrcType = new SrcType[3];
            arrayOfSrcType[0] = ResourceImage;
            arrayOfSrcType[1] = VirtualScreen;
            arrayOfSrcType[2] = ApplicationIcon;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.elements.ImageScreenElement
 * JD-Core Version:        0.6.2
 */