package miui.app.screenelement.elements;

import android.graphics.Canvas;
import java.util.ArrayList;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.ScreenElementRoot;
import miui.app.screenelement.util.Utils;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class FramerateController extends ScreenElement
{
    public static final String INNER_TAG = "ControlPoint";
    public static final String TAG_NAME = "FramerateController";
    private ArrayList<ControlPoint> mControlPoints = new ArrayList();
    private float mCurFramerate;
    private Object mLock = new Object();
    private boolean mLoop;
    private long mStartTime;
    private boolean mStopped;
    private long mTimeRange;

    public FramerateController(Element paramElement, ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot)
        throws ScreenElementLoadException
    {
        super(paramElement, paramScreenContext, paramScreenElementRoot);
        this.mRoot.addFramerateController(this);
        this.mLoop = Boolean.parseBoolean(paramElement.getAttribute("loop"));
        NodeList localNodeList = paramElement.getElementsByTagName("ControlPoint");
        for (int i = 0; i < localNodeList.getLength(); i++)
        {
            Element localElement = (Element)localNodeList.item(i);
            this.mControlPoints.add(new ControlPoint(localElement));
        }
        this.mTimeRange = ((ControlPoint)this.mControlPoints.get(-1 + this.mControlPoints.size())).mTime;
    }

    public void doRender(Canvas paramCanvas)
    {
    }

    protected void onVisibilityChange(boolean paramBoolean)
    {
        super.onVisibilityChange(paramBoolean);
        if (!paramBoolean)
        {
            this.mCurFramerate = getFramerate();
            requestFramerate(0.0F);
        }
        while (true)
        {
            return;
            requestFramerate(this.mCurFramerate);
        }
    }

    public void reset(long paramLong)
    {
        synchronized (this.mLock)
        {
            this.mStartTime = paramLong;
            this.mStopped = false;
            return;
        }
    }

    // ERROR //
    public void updateFramerate(long paramLong)
    {
        // Byte code:
        //     0: aload_0
        //     1: invokevirtual 130	miui/app/screenelement/elements/FramerateController:isVisible	()Z
        //     4: ifne +4 -> 8
        //     7: return
        //     8: aload_0
        //     9: getfield 44	miui/app/screenelement/elements/FramerateController:mLock	Ljava/lang/Object;
        //     12: astore_3
        //     13: aload_3
        //     14: monitorenter
        //     15: aload_0
        //     16: getfield 125	miui/app/screenelement/elements/FramerateController:mStopped	Z
        //     19: ifeq +15 -> 34
        //     22: aload_3
        //     23: monitorexit
        //     24: goto -17 -> 7
        //     27: astore 4
        //     29: aload_3
        //     30: monitorexit
        //     31: aload 4
        //     33: athrow
        //     34: lload_1
        //     35: aload_0
        //     36: getfield 123	miui/app/screenelement/elements/FramerateController:mStartTime	J
        //     39: lsub
        //     40: lstore 5
        //     42: lload 5
        //     44: lconst_0
        //     45: lcmp
        //     46: ifge +6 -> 52
        //     49: lconst_0
        //     50: lstore 5
        //     52: aload_0
        //     53: getfield 70	miui/app/screenelement/elements/FramerateController:mLoop	Z
        //     56: ifeq +96 -> 152
        //     59: lload 5
        //     61: aload_0
        //     62: getfield 103	miui/app/screenelement/elements/FramerateController:mTimeRange	J
        //     65: lrem
        //     66: lstore 7
        //     68: bipush 255
        //     70: aload_0
        //     71: getfield 39	miui/app/screenelement/elements/FramerateController:mControlPoints	Ljava/util/ArrayList;
        //     74: invokevirtual 94	java/util/ArrayList:size	()I
        //     77: iadd
        //     78: istore 9
        //     80: iload 9
        //     82: iflt +65 -> 147
        //     85: aload_0
        //     86: getfield 39	miui/app/screenelement/elements/FramerateController:mControlPoints	Ljava/util/ArrayList;
        //     89: iload 9
        //     91: invokevirtual 98	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     94: checkcast 6	miui/app/screenelement/elements/FramerateController$ControlPoint
        //     97: astore 10
        //     99: lload 7
        //     101: aload 10
        //     103: getfield 101	miui/app/screenelement/elements/FramerateController$ControlPoint:mTime	J
        //     106: lcmp
        //     107: iflt +52 -> 159
        //     110: aload_0
        //     111: aload 10
        //     113: getfield 134	miui/app/screenelement/elements/FramerateController$ControlPoint:mFramerate	I
        //     116: i2f
        //     117: invokevirtual 119	miui/app/screenelement/elements/FramerateController:requestFramerate	(F)V
        //     120: aload_0
        //     121: getfield 70	miui/app/screenelement/elements/FramerateController:mLoop	Z
        //     124: ifne +23 -> 147
        //     127: iload 9
        //     129: bipush 255
        //     131: aload_0
        //     132: getfield 39	miui/app/screenelement/elements/FramerateController:mControlPoints	Ljava/util/ArrayList;
        //     135: invokevirtual 94	java/util/ArrayList:size	()I
        //     138: iadd
        //     139: if_icmpne +8 -> 147
        //     142: aload_0
        //     143: iconst_1
        //     144: putfield 125	miui/app/screenelement/elements/FramerateController:mStopped	Z
        //     147: aload_3
        //     148: monitorexit
        //     149: goto -142 -> 7
        //     152: lload 5
        //     154: lstore 7
        //     156: goto -88 -> 68
        //     159: iinc 9 255
        //     162: goto -82 -> 80
        //
        // Exception table:
        //     from	to	target	type
        //     15	31	27	finally
        //     34	149	27	finally
    }

    public static class ControlPoint
    {
        public int mFramerate;
        public long mTime;

        public ControlPoint(Element paramElement)
            throws ScreenElementLoadException
        {
            this.mTime = Utils.getAttrAsLongThrows(paramElement, "time");
            this.mFramerate = Utils.getAttrAsInt(paramElement, "frameRate", -1);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.elements.FramerateController
 * JD-Core Version:        0.6.2
 */