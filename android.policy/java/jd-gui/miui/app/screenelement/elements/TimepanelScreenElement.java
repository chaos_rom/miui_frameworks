package miui.app.screenelement.elements;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.PorterDuff.Mode;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import java.util.Calendar;
import miui.app.screenelement.ResourceManager;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.ScreenElementRoot;
import miui.app.screenelement.animation.AnimatedElement;
import miui.app.screenelement.util.Utils;
import org.w3c.dom.Element;

public class TimepanelScreenElement extends ImageScreenElement
{
    private static final String LOG_TAG = "TimepanelScreenElement";
    private static final String M12 = "hh:mm";
    private static final String M24 = "kk:mm";
    public static final String TAG_NAME = "Time";
    private int mBmpHeight;
    private int mBmpWidth;
    protected Calendar mCalendar = Calendar.getInstance();
    private String mFormat = "kk:mm";
    private Handler mHandler;
    private long mLastUpdateTimeMillis;
    private boolean mLoadResourceFailed;
    private CharSequence mPreTime;
    private final Runnable mTimeUpdater = new Runnable()
    {
        public void run()
        {
            try
            {
                TimepanelScreenElement.this.updateTime();
                TimepanelScreenElement.this.mHandler.postDelayed(this, 5000L);
                return;
            }
            catch (Exception localException)
            {
                while (true)
                    localException.printStackTrace();
            }
        }
    };

    public TimepanelScreenElement(Element paramElement, ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot)
        throws ScreenElementLoadException
    {
        super(paramElement, paramScreenContext, paramScreenElementRoot);
        this.mFormat = paramElement.getAttribute("format");
        this.mHandler = paramScreenContext.mHandler;
    }

    private void createBitmap()
    {
        int i = 0;
        int j = 0;
        int k = 0;
        Bitmap localBitmap;
        if (k < "0123456789:".length())
        {
            localBitmap = getDigitBmp("0123456789:".charAt(k));
            if (localBitmap == null)
            {
                this.mLoadResourceFailed = true;
                Log.e("TimepanelScreenElement", "Failed to load digit bitmap: " + "0123456789:".charAt(k));
            }
        }
        while (true)
        {
            return;
            if (i < localBitmap.getWidth())
                i = localBitmap.getWidth();
            if (this.mBmpHeight < localBitmap.getHeight())
                this.mBmpHeight = localBitmap.getHeight();
            if (j == 0)
                j = localBitmap.getDensity();
            k++;
            break;
            this.mBitmap = Bitmap.createBitmap(i * 5, this.mBmpHeight, Bitmap.Config.ARGB_8888);
            this.mBitmap.setDensity(j);
            setActualHeight(this.mBmpHeight);
            this.mBitmapChanged = true;
        }
    }

    private Bitmap getDigitBmp(char paramChar)
    {
        String str1;
        if (TextUtils.isEmpty(this.mAni.getSrc()))
        {
            str1 = "time.png";
            if (paramChar != ':')
                break label52;
        }
        label52: for (String str2 = "dot"; ; str2 = String.valueOf(paramChar))
        {
            return this.mContext.mResourceManager.getBitmap(Utils.addFileNameSuffix(str1, str2));
            str1 = this.mAni.getSrc();
            break;
        }
    }

    private void setDateFormat()
    {
        if (TextUtils.isEmpty(this.mFormat))
            if (!DateFormat.is24HourFormat(this.mContext.mContext))
                break label32;
        label32: for (String str = "kk:mm"; ; str = "hh:mm")
        {
            this.mFormat = str;
            return;
        }
    }

    private void updateTime()
    {
        if (this.mLoadResourceFailed);
        while (true)
        {
            return;
            long l = SystemClock.elapsedRealtime();
            if (l - this.mLastUpdateTimeMillis >= 1000L)
            {
                if (this.mBitmap == null)
                    createBitmap();
                if (this.mBitmap != null)
                {
                    this.mCalendar.setTimeInMillis(System.currentTimeMillis());
                    CharSequence localCharSequence = DateFormat.format(this.mFormat, this.mCalendar);
                    if (!localCharSequence.equals(this.mPreTime))
                    {
                        this.mPreTime = localCharSequence;
                        Canvas localCanvas = new Canvas(this.mBitmap);
                        localCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
                        int i = 0;
                        for (int j = 0; j < localCharSequence.length(); j++)
                        {
                            Bitmap localBitmap = getDigitBmp(localCharSequence.charAt(j));
                            if (localBitmap != null)
                            {
                                localCanvas.drawBitmap(localBitmap, i, 0.0F, null);
                                i += localBitmap.getWidth();
                            }
                        }
                        this.mBmpWidth = i;
                        setActualWidth(descale(this.mBmpWidth));
                        requestUpdate();
                        this.mLastUpdateTimeMillis = l;
                    }
                }
            }
        }
    }

    public void finish()
    {
        this.mHandler.removeCallbacks(this.mTimeUpdater);
    }

    protected int getBitmapWidth()
    {
        return this.mBmpWidth;
    }

    public void init()
    {
        super.init();
        setDateFormat();
        this.mHandler.post(this.mTimeUpdater);
    }

    public void pause()
    {
        this.mHandler.removeCallbacks(this.mTimeUpdater);
    }

    public void resume()
    {
        this.mCalendar = Calendar.getInstance();
        this.mHandler.post(this.mTimeUpdater);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.elements.TimepanelScreenElement
 * JD-Core Version:        0.6.2
 */