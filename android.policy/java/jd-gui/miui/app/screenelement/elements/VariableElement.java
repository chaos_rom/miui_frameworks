package miui.app.screenelement.elements;

import android.graphics.Canvas;
import miui.app.screenelement.CommandTrigger;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.ScreenElementRoot;
import miui.app.screenelement.animation.VariableAnimation;
import miui.app.screenelement.data.Expression;
import miui.app.screenelement.data.Variables;
import miui.app.screenelement.util.IndexedNumberVariable;
import miui.app.screenelement.util.IndexedStringVariable;
import miui.app.screenelement.util.Utils;
import org.w3c.dom.Element;

public class VariableElement extends ScreenElement
{
    private static final String OLD_VALUE = "old_value";
    public static final String TAG_NAME = "Var";
    private VariableAnimation mAnimation;
    private boolean mConst;
    private Expression mExpression;
    private boolean mIsStringType;
    private IndexedNumberVariable mNumberVar;
    private IndexedNumberVariable mOldNumberVar;
    private IndexedStringVariable mOldStringVar;
    private Double mOldValue = null;
    private IndexedStringVariable mStringVar;
    private double mThreshold;
    private CommandTrigger mTrigger;

    public VariableElement(Element paramElement, ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot)
    {
        super(paramElement, paramScreenContext, paramScreenElementRoot);
        if (paramElement != null)
        {
            this.mExpression = Expression.build(paramElement.getAttribute("expression"));
            this.mThreshold = Math.abs(Utils.getAttrAsFloat(paramElement, "threshold", 1.0F));
            this.mIsStringType = "string".equalsIgnoreCase(paramElement.getAttribute("type"));
            this.mConst = Boolean.parseBoolean(paramElement.getAttribute("const"));
            if (!this.mIsStringType)
                break label184;
            this.mStringVar = new IndexedStringVariable(this.mName, paramScreenContext.mVariables);
            this.mOldStringVar = new IndexedStringVariable(this.mName, "old_value", paramScreenContext.mVariables);
        }
        while (true)
        {
            Element localElement1 = Utils.getChild(paramElement, "VariableAnimation");
            if (localElement1 != null);
            try
            {
                this.mAnimation = new VariableAnimation(localElement1, paramScreenContext);
                localElement2 = Utils.getChild(paramElement, "Trigger");
                if (localElement2 == null);
            }
            catch (ScreenElementLoadException localScreenElementLoadException2)
            {
                try
                {
                    Element localElement2;
                    this.mTrigger = new CommandTrigger(this.mContext, localElement2, paramScreenElementRoot);
                    return;
                    label184: this.mNumberVar = new IndexedNumberVariable(this.mName, paramScreenContext.mVariables);
                    this.mOldNumberVar = new IndexedNumberVariable(this.mName, "old_value", paramScreenContext.mVariables);
                    continue;
                    localScreenElementLoadException2 = localScreenElementLoadException2;
                    localScreenElementLoadException2.printStackTrace();
                }
                catch (ScreenElementLoadException localScreenElementLoadException1)
                {
                    while (true)
                        localScreenElementLoadException1.printStackTrace();
                }
            }
        }
    }

    private void update()
    {
        Variables localVariables = this.mContext.mVariables;
        if (this.mIsStringType)
        {
            if (this.mExpression == null);
            while (true)
            {
                return;
                String str1 = this.mExpression.evaluateStr(localVariables);
                String str2 = this.mStringVar.get();
                if (!Utils.equals(str1, str2))
                {
                    this.mOldStringVar.set(str2);
                    this.mStringVar.set(str1);
                    if (this.mTrigger != null)
                        this.mTrigger.perform();
                }
            }
        }
        Double localDouble = null;
        if (this.mAnimation != null);
        for (localDouble = Double.valueOf(this.mAnimation.getValue()); ; localDouble = Double.valueOf(this.mExpression.evaluate(localVariables)))
            do
            {
                this.mNumberVar.set(localDouble);
                if ((localDouble == null) || (localDouble.equals(this.mOldValue)))
                    break;
                if (this.mOldValue == null)
                    this.mOldValue = localDouble;
                this.mOldNumberVar.set(this.mOldValue);
                if ((this.mTrigger != null) && (Math.abs(localDouble.doubleValue() - this.mOldValue.doubleValue()) >= this.mThreshold))
                    this.mTrigger.perform();
                this.mOldValue = localDouble;
                break;
            }
            while ((this.mExpression == null) || (this.mExpression.isNull(localVariables)));
    }

    public void doRender(Canvas paramCanvas)
    {
    }

    public void init()
    {
        if (this.mAnimation != null)
            this.mAnimation.init();
        update();
    }

    public void reset(long paramLong)
    {
        if (this.mAnimation != null)
            this.mAnimation.reset(paramLong);
        update();
    }

    public void tick(long paramLong)
    {
        super.tick(paramLong);
        if (!this.mIsVisible);
        while (true)
        {
            return;
            if (this.mAnimation != null)
                this.mAnimation.tick(paramLong);
            if (!this.mConst)
                update();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.elements.VariableElement
 * JD-Core Version:        0.6.2
 */