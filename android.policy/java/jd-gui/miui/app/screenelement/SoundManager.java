package miui.app.screenelement;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.MemoryFile;
import android.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class SoundManager
    implements SoundPool.OnLoadCompleteListener
{
    private AudioManager mAudioManager;
    private Context mContext;
    private HashMap<Integer, Boolean> mPendingSoundMap = new HashMap();
    private ArrayList<Integer> mPlayingSoundMap = new ArrayList();
    private ResourceManager mResourceManager;
    private SoundPool mSoundPool;
    private HashMap<String, Integer> mSoundPoolMap = new HashMap();

    public SoundManager(Context paramContext, ResourceManager paramResourceManager)
    {
        this.mResourceManager = paramResourceManager;
        this.mContext = paramContext;
        this.mSoundPool = new SoundPool(4, 1, 100);
        this.mSoundPool.setOnLoadCompleteListener(this);
        this.mAudioManager = ((AudioManager)paramContext.getSystemService("audio"));
    }

    /** @deprecated */
    private void playSoundImp(int paramInt, boolean paramBoolean)
    {
        while (true)
        {
            try
            {
                SoundPool localSoundPool = this.mSoundPool;
                if (localSoundPool == null)
                    return;
                if ((!paramBoolean) || (this.mPlayingSoundMap.size() == 0))
                    break label88;
                Iterator localIterator = this.mPlayingSoundMap.iterator();
                if (localIterator.hasNext())
                {
                    Integer localInteger = (Integer)localIterator.next();
                    this.mSoundPool.stop(localInteger.intValue());
                    continue;
                }
            }
            finally
            {
            }
            this.mPlayingSoundMap.clear();
            label88: int i = this.mSoundPool.play(paramInt, 1.0F, 1.0F, 1, 0, 1.0F);
            this.mPlayingSoundMap.add(Integer.valueOf(i));
        }
    }

    public void onLoadComplete(SoundPool paramSoundPool, int paramInt1, int paramInt2)
    {
        if (paramInt2 == 0)
            playSoundImp(paramInt1, ((Boolean)this.mPendingSoundMap.get(Integer.valueOf(paramInt1))).booleanValue());
        this.mPendingSoundMap.remove(Integer.valueOf(paramInt1));
    }

    /** @deprecated */
    public void playSound(String paramString, boolean paramBoolean)
    {
        while (true)
        {
            Integer localInteger;
            MemoryFile localMemoryFile;
            try
            {
                if (this.mSoundPool != null)
                {
                    AudioManager localAudioManager = this.mAudioManager;
                    if (localAudioManager != null);
                }
                else
                {
                    return;
                }
                if (this.mAudioManager.getMode() != 0)
                    continue;
                localInteger = (Integer)this.mSoundPoolMap.get(paramString);
                if (localInteger != null)
                    break label191;
                localMemoryFile = this.mResourceManager.getFile(paramString);
                if (localMemoryFile == null)
                {
                    Log.e("Lockscreen_SoundManager", "the sound does not exist: " + paramString);
                    continue;
                }
            }
            finally
            {
            }
            try
            {
                localInteger = Integer.valueOf(this.mSoundPool.load(localMemoryFile.getFileDescriptor(), 0L, localMemoryFile.length(), 1));
                this.mSoundPoolMap.put(paramString, localInteger);
                localMemoryFile.close();
                this.mPendingSoundMap.put(localInteger, Boolean.valueOf(paramBoolean));
            }
            catch (IOException localIOException)
            {
                while (true)
                    Log.e("Lockscreen_SoundManager", "fail to load sound. " + localIOException.toString());
            }
            label191: playSoundImp(localInteger.intValue(), paramBoolean);
        }
    }

    /** @deprecated */
    public void release()
    {
        try
        {
            if (this.mSoundPool != null)
            {
                this.mSoundPoolMap.clear();
                this.mSoundPool.setOnLoadCompleteListener(null);
                this.mSoundPool.release();
                this.mSoundPool = null;
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.SoundManager
 * JD-Core Version:        0.6.2
 */