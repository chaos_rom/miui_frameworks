package miui.app.screenelement.data;

import miui.app.screenelement.ScreenContext;

public class VariableUpdater
{
    private VariableUpdaterManager mVariableUpdaterManager;

    public VariableUpdater(VariableUpdaterManager paramVariableUpdaterManager)
    {
        this.mVariableUpdaterManager = paramVariableUpdaterManager;
    }

    public void finish()
    {
    }

    protected ScreenContext getContext()
    {
        return this.mVariableUpdaterManager.getContext();
    }

    public void init()
    {
    }

    public void pause()
    {
    }

    public void resume()
    {
    }

    public void tick(long paramLong)
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.data.VariableUpdater
 * JD-Core Version:        0.6.2
 */