package miui.app.screenelement.data;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import miui.app.screenelement.NotifierManager.NotifierType;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.util.IndexedNumberVariable;

public class VolumeVariableUpdater extends NotifierVariableUpdater
{
    private static final int SHOW_DELAY_TIME = 1000;
    public static final String VAR_VOLUME_LEVEL = "volume_level";
    public static final String VAR_VOLUME_LEVEL_OLD = "volume_level_old";
    public static final String VAR_VOLUME_TYPE = "volume_type";
    private Handler mHandler = new Handler();
    private final Runnable mResetType = new Runnable()
    {
        public void run()
        {
            VolumeVariableUpdater.this.mVolumeType.set(-1.0D);
        }
    };
    private IndexedNumberVariable mVolumeLevel = new IndexedNumberVariable("volume_level", getContext().mVariables);
    private IndexedNumberVariable mVolumeLevelOld = new IndexedNumberVariable("volume_level_old", getContext().mVariables);
    private IndexedNumberVariable mVolumeType = new IndexedNumberVariable("volume_type", getContext().mVariables);

    public VolumeVariableUpdater(VariableUpdaterManager paramVariableUpdaterManager)
    {
        super(paramVariableUpdaterManager, NotifierManager.NotifierType.VolumeChanged);
        this.mVolumeType.set(-1.0D);
    }

    public void onNotify(Context paramContext, Intent paramIntent, Object paramObject)
    {
        if (paramIntent.getAction().equals("android.media.VOLUME_CHANGED_ACTION"))
        {
            int i = paramIntent.getIntExtra("android.media.EXTRA_VOLUME_STREAM_TYPE", -1);
            this.mVolumeType.set(i);
            int j = paramIntent.getIntExtra("android.media.EXTRA_VOLUME_STREAM_VALUE", 0);
            this.mVolumeLevel.set(j);
            int k = paramIntent.getIntExtra("android.media.EXTRA_PREV_VOLUME_STREAM_VALUE", 0);
            if (k != j)
                this.mVolumeLevelOld.set(k);
            getContext().requestUpdate();
            this.mHandler.removeCallbacks(this.mResetType);
            this.mHandler.postDelayed(this.mResetType, 1000L);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.data.VolumeVariableUpdater
 * JD-Core Version:        0.6.2
 */