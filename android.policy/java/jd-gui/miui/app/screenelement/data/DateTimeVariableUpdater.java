package miui.app.screenelement.data;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings.System;
import java.util.Calendar;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.util.IndexedNumberVariable;
import miui.app.screenelement.util.IndexedStringVariable;

public class DateTimeVariableUpdater extends VariableUpdater
{
    public static final String USE_TAG = "DateTime";
    private IndexedNumberVariable mAmPm = new IndexedNumberVariable("ampm", getContext().mVariables);
    protected Calendar mCalendar = Calendar.getInstance();
    private IndexedNumberVariable mDate = new IndexedNumberVariable("date", getContext().mVariables);
    private IndexedNumberVariable mDayOfWeek = new IndexedNumberVariable("day_of_week", getContext().mVariables);
    private Handler mHandler = new Handler();
    private IndexedNumberVariable mHour12 = new IndexedNumberVariable("hour12", getContext().mVariables);
    private IndexedNumberVariable mHour24 = new IndexedNumberVariable("hour24", getContext().mVariables);
    private long mLastUpdatedTime;
    private IndexedNumberVariable mMinute = new IndexedNumberVariable("minute", getContext().mVariables);
    private IndexedNumberVariable mMonth = new IndexedNumberVariable("month", getContext().mVariables);
    private IndexedStringVariable mNextAlarm;
    private IndexedNumberVariable mSecond = new IndexedNumberVariable("second", getContext().mVariables);
    private IndexedNumberVariable mTime = new IndexedNumberVariable("time", getContext().mVariables);
    private IndexedNumberVariable mTimeSys = new IndexedNumberVariable("time_sys", getContext().mVariables);
    private final Runnable mTimeUpdater = new Runnable()
    {
        public void run()
        {
            DateTimeVariableUpdater.this.updateTime();
            DateTimeVariableUpdater.this.mHandler.postDelayed(this, 500L);
        }
    };
    private IndexedNumberVariable mYear = new IndexedNumberVariable("year", getContext().mVariables);

    public DateTimeVariableUpdater(VariableUpdaterManager paramVariableUpdaterManager)
    {
        super(paramVariableUpdaterManager);
        this.mTimeSys.set(System.currentTimeMillis());
        this.mNextAlarm = new IndexedStringVariable("next_alarm_time", getContext().mVariables);
    }

    private void refreshAlarm()
    {
        String str = Settings.System.getString(getContext().mContext.getContentResolver(), "next_alarm_formatted");
        this.mNextAlarm.set(str);
    }

    private void updateTime()
    {
        long l = SystemClock.elapsedRealtime();
        if (l - this.mLastUpdatedTime >= 500L)
        {
            this.mCalendar.setTimeInMillis(System.currentTimeMillis());
            this.mAmPm.set(this.mCalendar.get(9));
            this.mHour12.set(this.mCalendar.get(10));
            this.mHour24.set(this.mCalendar.get(11));
            this.mMinute.set(this.mCalendar.get(12));
            this.mYear.set(this.mCalendar.get(1));
            this.mMonth.set(this.mCalendar.get(2));
            this.mDate.set(this.mCalendar.get(5));
            this.mDayOfWeek.set(this.mCalendar.get(7));
            this.mSecond.set(this.mCalendar.get(13));
            this.mLastUpdatedTime = l;
        }
    }

    public void finish()
    {
        super.finish();
        this.mHandler.removeCallbacks(this.mTimeUpdater);
    }

    public void init()
    {
        super.init();
        refreshAlarm();
        updateTime();
        this.mHandler.postDelayed(this.mTimeUpdater, 500L);
    }

    public void pause()
    {
        super.pause();
        this.mHandler.removeCallbacks(this.mTimeUpdater);
    }

    public void resume()
    {
        super.resume();
        refreshAlarm();
        this.mCalendar = Calendar.getInstance();
        updateTime();
        this.mHandler.post(this.mTimeUpdater);
    }

    public void tick(long paramLong)
    {
        super.tick(paramLong);
        this.mTime.set(paramLong);
        this.mTimeSys.set(System.currentTimeMillis());
        updateTime();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.data.DateTimeVariableUpdater
 * JD-Core Version:        0.6.2
 */