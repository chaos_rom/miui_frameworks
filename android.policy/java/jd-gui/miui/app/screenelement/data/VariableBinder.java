package miui.app.screenelement.data;

public abstract interface VariableBinder
{
    public abstract void finish();

    public abstract CharSequence getName();

    public abstract void init();

    public abstract void pause();

    public abstract void refresh();

    public abstract void resume();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.data.VariableBinder
 * JD-Core Version:        0.6.2
 */