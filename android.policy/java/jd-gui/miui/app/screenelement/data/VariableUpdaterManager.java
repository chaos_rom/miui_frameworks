package miui.app.screenelement.data;

import java.util.ArrayList;
import java.util.Iterator;
import miui.app.screenelement.ScreenContext;

public class VariableUpdaterManager
{
    private ScreenContext mContext;
    private ArrayList<VariableUpdater> mUpdaters = new ArrayList();

    public VariableUpdaterManager(ScreenContext paramScreenContext)
    {
        this.mContext = paramScreenContext;
    }

    public void add(VariableUpdater paramVariableUpdater)
    {
        this.mUpdaters.add(paramVariableUpdater);
    }

    public void finish()
    {
        Iterator localIterator = this.mUpdaters.iterator();
        while (localIterator.hasNext())
            ((VariableUpdater)localIterator.next()).finish();
    }

    public ScreenContext getContext()
    {
        return this.mContext;
    }

    public void init()
    {
        Iterator localIterator = this.mUpdaters.iterator();
        while (localIterator.hasNext())
            ((VariableUpdater)localIterator.next()).init();
    }

    public void pause()
    {
        Iterator localIterator = this.mUpdaters.iterator();
        while (localIterator.hasNext())
            ((VariableUpdater)localIterator.next()).pause();
    }

    public void remove(VariableUpdater paramVariableUpdater)
    {
        this.mUpdaters.remove(paramVariableUpdater);
    }

    public void resume()
    {
        Iterator localIterator = this.mUpdaters.iterator();
        while (localIterator.hasNext())
            ((VariableUpdater)localIterator.next()).resume();
    }

    public void tick(long paramLong)
    {
        Iterator localIterator = this.mUpdaters.iterator();
        while (localIterator.hasNext())
            ((VariableUpdater)localIterator.next()).tick(paramLong);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.data.VariableUpdaterManager
 * JD-Core Version:        0.6.2
 */