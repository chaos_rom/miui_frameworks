package miui.app.screenelement.data;

import android.content.AsyncQueryHandler;
import android.content.AsyncQueryHandler.WorkerHandler;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.database.sqlite.SQLiteDiskIOException;
import android.database.sqlite.SQLiteFullException;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.util.IndexedNumberVariable;
import miui.app.screenelement.util.IndexedStringVariable;
import miui.app.screenelement.util.TextFormatter;
import miui.app.screenelement.util.Utils;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class ContentProviderBinder
    implements VariableBinder
{
    private static final boolean DBG = false;
    private static final String LOG_TAG = "ContentProviderBinder";
    private static final int QUERY_TOKEN = 100;
    public static final String TAG_NAME = "ContentProviderBinder";
    private static final String TYPE_STRING = "string";
    protected String[] mArgs;
    public ChangeObserver mChangeObserver = new ChangeObserver();
    protected String[] mColumns;
    private ScreenContext mContext;
    protected String mCountName;
    private IndexedNumberVariable mCountVar;
    private Cursor mCursor;
    private Object mCursorLock = new Object();
    private DataSetObserver mDataSetObserver = new MyDataSetObserver(null);
    private String mDependency;
    private boolean mFinished;
    private long mLastQueryTime;
    private String mLastUri;
    protected String mName;
    protected String mOrder;
    private QueryCompleteListener mQueryCompletedListener;
    private QueryHandler mQueryHandler;
    private int mUpdateInterval = -1;
    protected TextFormatter mUriFormatter;
    private ArrayList<Variable> mVariables = new ArrayList();
    protected TextFormatter mWhereFormatter;

    public ContentProviderBinder(ScreenContext paramScreenContext)
    {
        this(paramScreenContext, null);
    }

    public ContentProviderBinder(ScreenContext paramScreenContext, QueryCompleteListener paramQueryCompleteListener)
    {
        this.mContext = paramScreenContext;
        this.mQueryHandler = new QueryHandler(paramScreenContext.mContext);
        this.mQueryCompletedListener = paramQueryCompleteListener;
    }

    public ContentProviderBinder(Element paramElement, ScreenContext paramScreenContext)
        throws ScreenElementLoadException
    {
        this(paramElement, paramScreenContext, null);
    }

    public ContentProviderBinder(Element paramElement, ScreenContext paramScreenContext, QueryCompleteListener paramQueryCompleteListener)
        throws ScreenElementLoadException
    {
        this.mContext = paramScreenContext;
        this.mQueryHandler = new QueryHandler(paramScreenContext.mContext);
        this.mQueryCompletedListener = paramQueryCompleteListener;
        load(paramElement);
    }

    private void closeCursor()
    {
        synchronized (this.mCursorLock)
        {
            if (this.mCursor != null)
            {
                if (this.mUpdateInterval == -1)
                {
                    this.mCursor.unregisterContentObserver(this.mChangeObserver);
                    this.mCursor.unregisterDataSetObserver(this.mDataSetObserver);
                }
                this.mCursor.close();
                this.mCursor = null;
            }
        }
    }

    private void load(Element paramElement)
        throws ScreenElementLoadException
    {
        Object localObject = null;
        if (paramElement == null)
        {
            Log.e("ContentProviderBinder", "ContentProviderBinder node is null");
            throw new ScreenElementLoadException("node is null");
        }
        this.mName = paramElement.getAttribute("name");
        this.mDependency = paramElement.getAttribute("dependency");
        this.mUriFormatter = new TextFormatter(paramElement.getAttribute("uri"), paramElement.getAttribute("uriFormat"), paramElement.getAttribute("uriParas"));
        String str1 = paramElement.getAttribute("columns");
        String[] arrayOfString1;
        String str2;
        String[] arrayOfString2;
        label164: String str4;
        if (TextUtils.isEmpty(str1))
        {
            arrayOfString1 = null;
            this.mColumns = arrayOfString1;
            this.mWhereFormatter = new TextFormatter(paramElement.getAttribute("where"), paramElement.getAttribute("whereFormat"), paramElement.getAttribute("whereParas"));
            str2 = paramElement.getAttribute("args");
            if (!TextUtils.isEmpty(str2))
                break label278;
            arrayOfString2 = null;
            this.mArgs = arrayOfString2;
            String str3 = paramElement.getAttribute("order");
            if (TextUtils.isEmpty(str3))
                str3 = null;
            this.mOrder = str3;
            str4 = paramElement.getAttribute("countName");
            if (!TextUtils.isEmpty(str4))
                break label290;
        }
        while (true)
        {
            this.mCountName = localObject;
            if (this.mCountName != null)
                this.mCountVar = new IndexedNumberVariable(this.mCountName, this.mContext.mVariables);
            this.mUpdateInterval = Utils.getAttrAsInt(paramElement, "updateInterval", -1);
            loadVariables(paramElement);
            return;
            arrayOfString1 = str1.split(",");
            break;
            label278: arrayOfString2 = str2.split(",");
            break label164;
            label290: localObject = str4;
        }
    }

    private void loadVariables(Element paramElement)
        throws ScreenElementLoadException
    {
        NodeList localNodeList = paramElement.getElementsByTagName("Variable");
        for (int i = 0; i < localNodeList.getLength(); i++)
            addVariable(new Variable((Element)localNodeList.item(i), this.mContext.mVariables));
    }

    private void onQueryComplete(Cursor paramCursor)
    {
        closeCursor();
        if (this.mFinished);
        while (true)
        {
            return;
            this.mCursor = paramCursor;
            synchronized (this.mCursorLock)
            {
                if (this.mCursor != null)
                {
                    if (this.mUpdateInterval == -1)
                    {
                        this.mCursor.registerContentObserver(this.mChangeObserver);
                        this.mCursor.registerDataSetObserver(this.mDataSetObserver);
                    }
                    updateVariables();
                    if (this.mUpdateInterval != -1)
                    {
                        this.mCursor.close();
                        this.mCursor = null;
                    }
                    this.mContext.requestUpdate();
                }
                if (this.mQueryCompletedListener == null)
                    continue;
                this.mQueryCompletedListener.onQueryCompleted(this.mName);
            }
        }
    }

    private void setNull(Variable paramVariable)
    {
        if ("string".equalsIgnoreCase(paramVariable.mType))
            paramVariable.mStringVar.set(null);
        while (true)
        {
            return;
            paramVariable.mNumberVar.set(null);
        }
    }

    // ERROR //
    private void updateVariables()
    {
        // Byte code:
        //     0: iconst_0
        //     1: istore_1
        //     2: aload_0
        //     3: getfield 99	miui/app/screenelement/data/ContentProviderBinder:mCursorLock	Ljava/lang/Object;
        //     6: astore_2
        //     7: aload_2
        //     8: monitorenter
        //     9: aload_0
        //     10: getfield 144	miui/app/screenelement/data/ContentProviderBinder:mCursor	Landroid/database/Cursor;
        //     13: ifnonnull +110 -> 123
        //     16: aload_0
        //     17: getfield 237	miui/app/screenelement/data/ContentProviderBinder:mCountVar	Lmiui/app/screenelement/util/IndexedNumberVariable;
        //     20: ifnull +12 -> 32
        //     23: aload_0
        //     24: getfield 237	miui/app/screenelement/data/ContentProviderBinder:mCountVar	Lmiui/app/screenelement/util/IndexedNumberVariable;
        //     27: iload_1
        //     28: i2d
        //     29: invokevirtual 322	miui/app/screenelement/util/IndexedNumberVariable:set	(D)V
        //     32: ldc 33
        //     34: new 324	java/lang/StringBuilder
        //     37: dup
        //     38: invokespecial 325	java/lang/StringBuilder:<init>	()V
        //     41: ldc_w 327
        //     44: invokevirtual 331	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     47: iload_1
        //     48: invokevirtual 334	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     51: ldc_w 336
        //     54: invokevirtual 331	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     57: aload_0
        //     58: getfield 338	miui/app/screenelement/data/ContentProviderBinder:mLastUri	Ljava/lang/String;
        //     61: invokevirtual 331	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     64: invokevirtual 342	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     67: invokestatic 345	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     70: pop
        //     71: aload_0
        //     72: getfield 144	miui/app/screenelement/data/ContentProviderBinder:mCursor	Landroid/database/Cursor;
        //     75: ifnull +7 -> 82
        //     78: iload_1
        //     79: ifne +62 -> 141
        //     82: aload_0
        //     83: getfield 87	miui/app/screenelement/data/ContentProviderBinder:mVariables	Ljava/util/ArrayList;
        //     86: invokevirtual 349	java/util/ArrayList:iterator	()Ljava/util/Iterator;
        //     89: astore 5
        //     91: aload 5
        //     93: invokeinterface 355 1 0
        //     98: ifeq +38 -> 136
        //     101: aload_0
        //     102: aload 5
        //     104: invokeinterface 359 1 0
        //     109: checkcast 19	miui/app/screenelement/data/ContentProviderBinder$Variable
        //     112: invokespecial 361	miui/app/screenelement/data/ContentProviderBinder:setNull	(Lmiui/app/screenelement/data/ContentProviderBinder$Variable;)V
        //     115: goto -24 -> 91
        //     118: astore_3
        //     119: aload_2
        //     120: monitorexit
        //     121: aload_3
        //     122: athrow
        //     123: aload_0
        //     124: getfield 144	miui/app/screenelement/data/ContentProviderBinder:mCursor	Landroid/database/Cursor;
        //     127: invokeinterface 364 1 0
        //     132: istore_1
        //     133: goto -117 -> 16
        //     136: aload_2
        //     137: monitorexit
        //     138: goto +395 -> 533
        //     141: ldc 33
        //     143: new 324	java/lang/StringBuilder
        //     146: dup
        //     147: invokespecial 325	java/lang/StringBuilder:<init>	()V
        //     150: ldc_w 366
        //     153: invokevirtual 331	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     156: aload_0
        //     157: getfield 338	miui/app/screenelement/data/ContentProviderBinder:mLastUri	Ljava/lang/String;
        //     160: invokevirtual 331	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     163: invokevirtual 342	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     166: invokestatic 345	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
        //     169: pop
        //     170: aload_0
        //     171: getfield 87	miui/app/screenelement/data/ContentProviderBinder:mVariables	Ljava/util/ArrayList;
        //     174: invokevirtual 349	java/util/ArrayList:iterator	()Ljava/util/Iterator;
        //     177: astore 7
        //     179: aload 7
        //     181: invokeinterface 355 1 0
        //     186: ifeq +345 -> 531
        //     189: aload 7
        //     191: invokeinterface 359 1 0
        //     196: checkcast 19	miui/app/screenelement/data/ContentProviderBinder$Variable
        //     199: astore 8
        //     201: iconst_1
        //     202: istore 9
        //     204: aload_0
        //     205: getfield 144	miui/app/screenelement/data/ContentProviderBinder:mCursor	Landroid/database/Cursor;
        //     208: aload 8
        //     210: getfield 369	miui/app/screenelement/data/ContentProviderBinder$Variable:mRow	I
        //     213: invokeinterface 373 2 0
        //     218: istore 10
        //     220: iload 10
        //     222: ifeq +76 -> 298
        //     225: aload 8
        //     227: getfield 376	miui/app/screenelement/data/ContentProviderBinder$Variable:mColumn	Ljava/lang/String;
        //     230: astore 15
        //     232: aload_0
        //     233: getfield 144	miui/app/screenelement/data/ContentProviderBinder:mCursor	Landroid/database/Cursor;
        //     236: aload 15
        //     238: invokeinterface 380 2 0
        //     243: istore 16
        //     245: aload_0
        //     246: getfield 144	miui/app/screenelement/data/ContentProviderBinder:mCursor	Landroid/database/Cursor;
        //     249: iload 16
        //     251: invokeinterface 383 2 0
        //     256: ifne +42 -> 298
        //     259: ldc 40
        //     261: aload 8
        //     263: getfield 296	miui/app/screenelement/data/ContentProviderBinder$Variable:mType	Ljava/lang/String;
        //     266: invokevirtual 300	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
        //     269: ifeq +43 -> 312
        //     272: aload_0
        //     273: getfield 144	miui/app/screenelement/data/ContentProviderBinder:mCursor	Landroid/database/Cursor;
        //     276: iload 16
        //     278: invokeinterface 387 2 0
        //     283: astore 23
        //     285: aload 8
        //     287: getfield 304	miui/app/screenelement/data/ContentProviderBinder$Variable:mStringVar	Lmiui/app/screenelement/util/IndexedStringVariable;
        //     290: aload 23
        //     292: invokevirtual 309	miui/app/screenelement/util/IndexedStringVariable:set	(Ljava/lang/String;)V
        //     295: iconst_0
        //     296: istore 9
        //     298: iload 9
        //     300: ifeq -121 -> 179
        //     303: aload_0
        //     304: aload 8
        //     306: invokespecial 361	miui/app/screenelement/data/ContentProviderBinder:setNull	(Lmiui/app/screenelement/data/ContentProviderBinder$Variable;)V
        //     309: goto -130 -> 179
        //     312: ldc_w 389
        //     315: aload 8
        //     317: getfield 296	miui/app/screenelement/data/ContentProviderBinder$Variable:mType	Ljava/lang/String;
        //     320: invokevirtual 300	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
        //     323: ifeq +50 -> 373
        //     326: aload_0
        //     327: getfield 144	miui/app/screenelement/data/ContentProviderBinder:mCursor	Landroid/database/Cursor;
        //     330: iload 16
        //     332: invokeinterface 393 2 0
        //     337: dstore 21
        //     339: aload 8
        //     341: getfield 312	miui/app/screenelement/data/ContentProviderBinder$Variable:mNumberVar	Lmiui/app/screenelement/util/IndexedNumberVariable;
        //     344: dload 21
        //     346: invokevirtual 322	miui/app/screenelement/util/IndexedNumberVariable:set	(D)V
        //     349: goto -54 -> 295
        //     352: astore 13
        //     354: ldc 33
        //     356: ldc_w 395
        //     359: iconst_0
        //     360: anewarray 4	java/lang/Object
        //     363: invokestatic 399	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //     366: invokestatic 402	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     369: pop
        //     370: goto -72 -> 298
        //     373: ldc_w 404
        //     376: aload 8
        //     378: getfield 296	miui/app/screenelement/data/ContentProviderBinder$Variable:mType	Ljava/lang/String;
        //     381: invokevirtual 300	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
        //     384: ifeq +65 -> 449
        //     387: aload_0
        //     388: getfield 144	miui/app/screenelement/data/ContentProviderBinder:mCursor	Landroid/database/Cursor;
        //     391: iload 16
        //     393: invokeinterface 408 2 0
        //     398: fstore 20
        //     400: aload 8
        //     402: getfield 312	miui/app/screenelement/data/ContentProviderBinder$Variable:mNumberVar	Lmiui/app/screenelement/util/IndexedNumberVariable;
        //     405: fload 20
        //     407: f2d
        //     408: invokevirtual 322	miui/app/screenelement/util/IndexedNumberVariable:set	(D)V
        //     411: goto -116 -> 295
        //     414: astore 11
        //     416: ldc 33
        //     418: new 324	java/lang/StringBuilder
        //     421: dup
        //     422: invokespecial 325	java/lang/StringBuilder:<init>	()V
        //     425: ldc_w 410
        //     428: invokevirtual 331	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     431: aload 8
        //     433: getfield 376	miui/app/screenelement/data/ContentProviderBinder$Variable:mColumn	Ljava/lang/String;
        //     436: invokevirtual 331	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     439: invokevirtual 342	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     442: invokestatic 165	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     445: pop
        //     446: goto -148 -> 298
        //     449: ldc_w 412
        //     452: aload 8
        //     454: getfield 296	miui/app/screenelement/data/ContentProviderBinder$Variable:mType	Ljava/lang/String;
        //     457: invokevirtual 300	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
        //     460: ifeq +30 -> 490
        //     463: aload_0
        //     464: getfield 144	miui/app/screenelement/data/ContentProviderBinder:mCursor	Landroid/database/Cursor;
        //     467: iload 16
        //     469: invokeinterface 416 2 0
        //     474: istore 19
        //     476: aload 8
        //     478: getfield 312	miui/app/screenelement/data/ContentProviderBinder$Variable:mNumberVar	Lmiui/app/screenelement/util/IndexedNumberVariable;
        //     481: iload 19
        //     483: i2d
        //     484: invokevirtual 322	miui/app/screenelement/util/IndexedNumberVariable:set	(D)V
        //     487: goto -192 -> 295
        //     490: ldc_w 418
        //     493: aload 8
        //     495: getfield 296	miui/app/screenelement/data/ContentProviderBinder$Variable:mType	Ljava/lang/String;
        //     498: invokevirtual 300	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
        //     501: ifeq -206 -> 295
        //     504: aload_0
        //     505: getfield 144	miui/app/screenelement/data/ContentProviderBinder:mCursor	Landroid/database/Cursor;
        //     508: iload 16
        //     510: invokeinterface 422 2 0
        //     515: lstore 17
        //     517: aload 8
        //     519: getfield 312	miui/app/screenelement/data/ContentProviderBinder$Variable:mNumberVar	Lmiui/app/screenelement/util/IndexedNumberVariable;
        //     522: lload 17
        //     524: l2d
        //     525: invokevirtual 322	miui/app/screenelement/util/IndexedNumberVariable:set	(D)V
        //     528: goto -233 -> 295
        //     531: aload_2
        //     532: monitorexit
        //     533: return
        //
        // Exception table:
        //     from	to	target	type
        //     9	121	118	finally
        //     123	220	118	finally
        //     225	295	118	finally
        //     303	309	118	finally
        //     312	349	118	finally
        //     354	370	118	finally
        //     373	411	118	finally
        //     416	446	118	finally
        //     449	528	118	finally
        //     531	533	118	finally
        //     225	295	352	java/lang/NumberFormatException
        //     312	349	352	java/lang/NumberFormatException
        //     373	411	352	java/lang/NumberFormatException
        //     449	528	352	java/lang/NumberFormatException
        //     225	295	414	java/lang/IllegalArgumentException
        //     312	349	414	java/lang/IllegalArgumentException
        //     373	411	414	java/lang/IllegalArgumentException
        //     449	528	414	java/lang/IllegalArgumentException
    }

    protected void addVariable(Variable paramVariable)
    {
        this.mVariables.add(paramVariable);
    }

    public void createCountVar()
    {
        if (this.mCountName == null);
        for (this.mCountVar = null; ; this.mCountVar = new IndexedNumberVariable(this.mCountName, this.mContext.mVariables))
            return;
    }

    public void finish()
    {
        closeCursor();
        this.mFinished = true;
    }

    public String getDependency()
    {
        return this.mDependency;
    }

    public String getName()
    {
        return this.mName;
    }

    public void init()
    {
        if (TextUtils.isEmpty(getDependency()))
            startQuery();
    }

    public void onContentChanged()
    {
        Log.i("ContentProviderBinder", "ChangeObserver: content changed.");
        if (this.mFinished);
        while (true)
        {
            return;
            startQuery();
        }
    }

    public void pause()
    {
    }

    public void refresh()
    {
        startQuery();
    }

    public void resume()
    {
        if ((this.mUpdateInterval > 0) && (System.currentTimeMillis() - this.mLastQueryTime > 1000 * this.mUpdateInterval))
            startQuery();
    }

    public void startQuery()
    {
        this.mQueryHandler.cancelOperation(100);
        Uri localUri = Uri.parse(this.mUriFormatter.getText(this.mContext.mVariables));
        this.mLastUri = localUri.toString();
        String str = this.mWhereFormatter.getText(this.mContext.mVariables);
        this.mQueryHandler.startQuery(100, null, localUri, this.mColumns, str, this.mArgs, this.mOrder);
        this.mLastQueryTime = System.currentTimeMillis();
    }

    private class MyDataSetObserver extends DataSetObserver
    {
        private MyDataSetObserver()
        {
        }

        public void onChanged()
        {
            if (ContentProviderBinder.this.mFinished);
            while (true)
            {
                return;
                ContentProviderBinder.this.updateVariables();
                if (ContentProviderBinder.this.mQueryCompletedListener != null)
                    ContentProviderBinder.this.mQueryCompletedListener.onQueryCompleted(ContentProviderBinder.this.mName);
            }
        }

        public void onInvalidated()
        {
            if (ContentProviderBinder.this.mFinished);
            while (true)
            {
                return;
                ContentProviderBinder.this.updateVariables();
                if (ContentProviderBinder.this.mQueryCompletedListener != null)
                    ContentProviderBinder.this.mQueryCompletedListener.onQueryCompleted(ContentProviderBinder.this.mName);
            }
        }
    }

    private class ChangeObserver extends ContentObserver
    {
        public ChangeObserver()
        {
            super();
        }

        public boolean deliverSelfNotifications()
        {
            return true;
        }

        public void onChange(boolean paramBoolean)
        {
            ContentProviderBinder.this.onContentChanged();
        }
    }

    private final class QueryHandler extends AsyncQueryHandler
    {
        public QueryHandler(Context arg2)
        {
            super();
        }

        protected Handler createHandler(Looper paramLooper)
        {
            return new CatchingWorkerHandler(paramLooper);
        }

        protected void onQueryComplete(int paramInt, Object paramObject, Cursor paramCursor)
        {
            ContentProviderBinder.this.onQueryComplete(paramCursor);
        }

        protected class CatchingWorkerHandler extends AsyncQueryHandler.WorkerHandler
        {
            public CatchingWorkerHandler(Looper arg2)
            {
                super(localLooper);
            }

            public void handleMessage(Message paramMessage)
            {
                try
                {
                    super.handleMessage(paramMessage);
                    return;
                }
                catch (SQLiteDiskIOException localSQLiteDiskIOException)
                {
                    while (true)
                        Log.w("ContentProviderBinder", "Exception on background worker thread", localSQLiteDiskIOException);
                }
                catch (SQLiteFullException localSQLiteFullException)
                {
                    while (true)
                        Log.w("ContentProviderBinder", "Exception on background worker thread", localSQLiteFullException);
                }
                catch (SQLiteDatabaseCorruptException localSQLiteDatabaseCorruptException)
                {
                    while (true)
                        Log.w("ContentProviderBinder", "Exception on background worker thread", localSQLiteDatabaseCorruptException);
                }
            }
        }
    }

    public static class Variable
    {
        public static final String TAG_NAME = "Variable";
        public String mColumn;
        public String mName;
        public IndexedNumberVariable mNumberVar;
        public int mRow;
        public IndexedStringVariable mStringVar;
        public String mType;
        private Variables mVar;

        public Variable()
        {
        }

        public Variable(Element paramElement, Variables paramVariables)
            throws ScreenElementLoadException
        {
            this.mVar = paramVariables;
            load(paramElement);
        }

        private void createVar()
        {
            if ("string".equalsIgnoreCase(this.mType))
                this.mStringVar = new IndexedStringVariable(this.mName, this.mVar);
            while (true)
            {
                return;
                this.mNumberVar = new IndexedNumberVariable(this.mName, this.mVar);
            }
        }

        private void load(Element paramElement)
            throws ScreenElementLoadException
        {
            if (paramElement == null)
            {
                Log.e("ContentProviderBinder", "Variable node is null");
                throw new ScreenElementLoadException("node is null");
            }
            this.mName = paramElement.getAttribute("name");
            this.mType = paramElement.getAttribute("type");
            this.mColumn = paramElement.getAttribute("column");
            this.mRow = Utils.getAttrAsInt(paramElement, "row", 0);
            createVar();
        }
    }

    public static class Builder
    {
        private ContentProviderBinder mBinder;

        protected Builder(ContentProviderBinder paramContentProviderBinder)
        {
            this.mBinder = paramContentProviderBinder;
        }

        public void addVariable(String paramString1, String paramString2, String paramString3, int paramInt)
        {
            ContentProviderBinder.Variable localVariable = new ContentProviderBinder.Variable();
            localVariable.mName = paramString1;
            localVariable.mType = paramString2;
            localVariable.mColumn = paramString3;
            localVariable.mRow = paramInt;
            localVariable.createVar();
            this.mBinder.addVariable(localVariable);
        }

        public Builder setArgs(String[] paramArrayOfString)
        {
            this.mBinder.mArgs = paramArrayOfString;
            return this;
        }

        public Builder setColumns(String[] paramArrayOfString)
        {
            this.mBinder.mColumns = paramArrayOfString;
            return this;
        }

        public Builder setCountName(String paramString)
        {
            this.mBinder.mCountName = paramString;
            this.mBinder.createCountVar();
            return this;
        }

        public Builder setName(String paramString)
        {
            this.mBinder.mName = paramString;
            return this;
        }

        public Builder setOrder(String paramString)
        {
            this.mBinder.mOrder = paramString;
            return this;
        }

        public Builder setWhere(String paramString)
        {
            this.mBinder.mWhereFormatter = new TextFormatter(paramString);
            return this;
        }

        public Builder setWhere(String paramString1, String paramString2)
        {
            this.mBinder.mWhereFormatter = new TextFormatter(paramString1, paramString2);
            return this;
        }
    }

    public static abstract interface QueryCompleteListener
    {
        public abstract void onQueryCompleted(String paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.data.ContentProviderBinder
 * JD-Core Version:        0.6.2
 */