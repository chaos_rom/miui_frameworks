package miui.app.screenelement.data;

import android.content.Context;
import android.content.Intent;
import miui.app.screenelement.NotifierManager.NotifierType;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.util.IndexedNumberVariable;

public class BatteryVariableUpdater extends NotifierVariableUpdater
{
    public static final String USE_TAG = "Battery";
    private IndexedNumberVariable mBatteryLevel = new IndexedNumberVariable("battery_level", getContext().mVariables);

    public BatteryVariableUpdater(VariableUpdaterManager paramVariableUpdaterManager)
    {
        super(paramVariableUpdaterManager, NotifierManager.NotifierType.Battery);
    }

    public void onNotify(Context paramContext, Intent paramIntent, Object paramObject)
    {
        int i;
        IndexedNumberVariable localIndexedNumberVariable;
        if (paramIntent.getAction().equals("android.intent.action.BATTERY_CHANGED"))
        {
            i = paramIntent.getIntExtra("level", -1);
            if (i != -1)
            {
                localIndexedNumberVariable = this.mBatteryLevel;
                if (i < 100)
                    break label62;
            }
        }
        label62: for (double d = 100.0D; ; d = i)
        {
            localIndexedNumberVariable.set(d);
            getContext().requestUpdate();
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.data.BatteryVariableUpdater
 * JD-Core Version:        0.6.2
 */