package miui.app.screenelement.data;

import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;

public class Variables
{
    private static boolean DBG = false;
    private static final String GLOBAL = "__global";
    private static final String LOG_TAG = "Variables";
    private ArrayList<Double> mDoubleArray = new ArrayList();
    private int mNextDoubleIndex = 0;
    private int mNextStringIndex = 0;
    private Object mNumLock = new Object();
    private HashMap<String, HashMap<String, Integer>> mNumObjects = new HashMap();
    private Object mStrLock = new Object();
    private HashMap<String, HashMap<String, Integer>> mStrObjects = new HashMap();
    private ArrayList<String> mStringArray = new ArrayList();

    private int getIndex(HashMap<String, HashMap<String, Integer>> paramHashMap, String paramString1, String paramString2, int paramInt)
    {
        if (paramString1 == null)
            paramString1 = "__global";
        HashMap localHashMap = (HashMap)paramHashMap.get(paramString1);
        if (localHashMap == null)
        {
            localHashMap = new HashMap();
            paramHashMap.put(paramString1, localHashMap);
        }
        Integer localInteger = (Integer)localHashMap.get(paramString2);
        if (localInteger == null)
        {
            localInteger = Integer.valueOf(paramInt);
            localHashMap.put(paramString2, localInteger);
        }
        return localInteger.intValue();
    }

    public Double getNum(int paramInt)
    {
        Object localObject1 = this.mNumLock;
        if (paramInt >= -1);
        Double localDouble;
        try
        {
            if (paramInt > -1 + this.mDoubleArray.size())
                localDouble = null;
            else
                localDouble = (Double)this.mDoubleArray.get(paramInt);
        }
        finally
        {
            localObject2 = finally;
            throw localObject2;
        }
        return localDouble;
    }

    public String getStr(int paramInt)
    {
        Object localObject1 = this.mStrLock;
        if (paramInt >= -1);
        String str;
        try
        {
            if (paramInt > -1 + this.mStringArray.size())
                str = null;
            else
                str = (String)this.mStringArray.get(paramInt);
        }
        finally
        {
            localObject2 = finally;
            throw localObject2;
        }
        return str;
    }

    public void putNum(int paramInt, double paramDouble)
    {
        putNum(paramInt, Double.valueOf(paramDouble));
    }

    // ERROR //
    public void putNum(int paramInt, Double paramDouble)
    {
        // Byte code:
        //     0: iload_1
        //     1: ifge +4 -> 5
        //     4: return
        //     5: aload_0
        //     6: getfield 56	miui/app/screenelement/data/Variables:mNumLock	Ljava/lang/Object;
        //     9: astore_3
        //     10: aload_3
        //     11: monitorenter
        //     12: iload_1
        //     13: bipush 255
        //     15: aload_0
        //     16: getfield 52	miui/app/screenelement/data/Variables:mDoubleArray	Ljava/util/ArrayList;
        //     19: invokevirtual 83	java/util/ArrayList:size	()I
        //     22: iadd
        //     23: if_icmple +22 -> 45
        //     26: aload_0
        //     27: getfield 52	miui/app/screenelement/data/Variables:mDoubleArray	Ljava/util/ArrayList;
        //     30: aconst_null
        //     31: invokevirtual 104	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     34: pop
        //     35: goto -23 -> 12
        //     38: astore 4
        //     40: aload_3
        //     41: monitorexit
        //     42: aload 4
        //     44: athrow
        //     45: aload_0
        //     46: getfield 52	miui/app/screenelement/data/Variables:mDoubleArray	Ljava/util/ArrayList;
        //     49: iload_1
        //     50: aload_2
        //     51: invokevirtual 108	java/util/ArrayList:set	(ILjava/lang/Object;)Ljava/lang/Object;
        //     54: pop
        //     55: aload_3
        //     56: monitorexit
        //     57: goto -53 -> 4
        //
        // Exception table:
        //     from	to	target	type
        //     12	42	38	finally
        //     45	57	38	finally
    }

    // ERROR //
    public void putStr(int paramInt, String paramString)
    {
        // Byte code:
        //     0: iload_1
        //     1: ifge +4 -> 5
        //     4: return
        //     5: aload_0
        //     6: getfield 58	miui/app/screenelement/data/Variables:mStrLock	Ljava/lang/Object;
        //     9: astore_3
        //     10: aload_3
        //     11: monitorenter
        //     12: iload_1
        //     13: bipush 255
        //     15: aload_0
        //     16: getfield 54	miui/app/screenelement/data/Variables:mStringArray	Ljava/util/ArrayList;
        //     19: invokevirtual 83	java/util/ArrayList:size	()I
        //     22: iadd
        //     23: if_icmple +22 -> 45
        //     26: aload_0
        //     27: getfield 54	miui/app/screenelement/data/Variables:mStringArray	Ljava/util/ArrayList;
        //     30: aconst_null
        //     31: invokevirtual 104	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     34: pop
        //     35: goto -23 -> 12
        //     38: astore 4
        //     40: aload_3
        //     41: monitorexit
        //     42: aload 4
        //     44: athrow
        //     45: aload_0
        //     46: getfield 54	miui/app/screenelement/data/Variables:mStringArray	Ljava/util/ArrayList;
        //     49: iload_1
        //     50: aload_2
        //     51: invokevirtual 108	java/util/ArrayList:set	(ILjava/lang/Object;)Ljava/lang/Object;
        //     54: pop
        //     55: aload_3
        //     56: monitorexit
        //     57: goto -53 -> 4
        //
        // Exception table:
        //     from	to	target	type
        //     12	42	38	finally
        //     45	57	38	finally
    }

    public int registerNumberVariable(String paramString1, String paramString2)
    {
        synchronized (this.mNumLock)
        {
            int i = getIndex(this.mNumObjects, paramString1, paramString2, this.mNextDoubleIndex);
            if (i == this.mNextDoubleIndex)
                this.mNextDoubleIndex = (1 + this.mNextDoubleIndex);
            if (DBG)
                Log.d("Variables", "registerNumberVariable: " + paramString1 + "." + paramString2 + "    index:" + i);
            return i;
        }
    }

    public int registerStringVariable(String paramString1, String paramString2)
    {
        synchronized (this.mStrLock)
        {
            int i = getIndex(this.mStrObjects, paramString1, paramString2, this.mNextStringIndex);
            if (i == this.mNextStringIndex)
                this.mNextStringIndex = (1 + this.mNextStringIndex);
            if (DBG)
                Log.d("Variables", "registerStringVariable: " + paramString1 + "." + paramString2 + "    index:" + i);
            return i;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.data.Variables
 * JD-Core Version:        0.6.2
 */