package miui.app.screenelement.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.util.IndexedNumberVariable;
import miui.app.screenelement.util.IndexedStringVariable;
import miui.app.screenelement.util.TextFormatter;
import miui.app.screenelement.util.Utils;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class WebServiceBinder
    implements VariableBinder
{
    private static final String LOG_TAG = "WebServiceBinder";
    private static final String PREF_LAST_QUERY_TIME = "LastQueryTime";
    public static final String TAG_NAME = "WebServiceBinder";
    private ScreenContext mContext;
    private long mLastQueryTime;
    protected String mName;
    private TextFormatter mParamsFormatter;
    private boolean mQueryInProgress;
    private boolean mQuerySuccessful = true;
    private Thread mQueryThread;
    private int mUpdateInterval = -1;
    private int mUpdateIntervalFail = -1;
    protected TextFormatter mUriFormatter;
    private ArrayList<Variable> mVariables = new ArrayList();

    public WebServiceBinder(Element paramElement, ScreenContext paramScreenContext)
        throws ScreenElementLoadException
    {
        this.mContext = paramScreenContext;
        load(paramElement);
    }

    private void load(Element paramElement)
        throws ScreenElementLoadException
    {
        if (paramElement == null)
        {
            Log.e("WebServiceBinder", "WebServiceBinder node is null");
            throw new ScreenElementLoadException("node is null");
        }
        this.mName = paramElement.getAttribute("name");
        this.mUriFormatter = new TextFormatter(paramElement.getAttribute("uri"), paramElement.getAttribute("uriFormat"), paramElement.getAttribute("uriParas"));
        this.mParamsFormatter = new TextFormatter(paramElement.getAttribute("params"), paramElement.getAttribute("paramsFormat"), paramElement.getAttribute("paramsParas"));
        this.mUpdateInterval = Utils.getAttrAsInt(paramElement, "updateInterval", -1);
        this.mUpdateIntervalFail = Utils.getAttrAsInt(paramElement, "updateIntervalFail", -1);
        loadVariables(paramElement);
    }

    private void loadVariables(Element paramElement)
        throws ScreenElementLoadException
    {
        NodeList localNodeList = paramElement.getElementsByTagName("Variable");
        for (int i = 0; i < localNodeList.getLength(); i++)
            addVariable(new Variable((Element)localNodeList.item(i), this.mContext.mVariables));
    }

    // ERROR //
    private void onQueryComplete(String paramString)
    {
        // Byte code:
        //     0: aload_1
        //     1: ifnull +209 -> 210
        //     4: invokestatic 182	javax/xml/xpath/XPathFactory:newInstance	()Ljavax/xml/xpath/XPathFactory;
        //     7: invokevirtual 186	javax/xml/xpath/XPathFactory:newXPath	()Ljavax/xml/xpath/XPath;
        //     10: astore_2
        //     11: invokestatic 191	javax/xml/parsers/DocumentBuilderFactory:newInstance	()Ljavax/xml/parsers/DocumentBuilderFactory;
        //     14: astore_3
        //     15: aconst_null
        //     16: astore 4
        //     18: aload_3
        //     19: invokevirtual 195	javax/xml/parsers/DocumentBuilderFactory:newDocumentBuilder	()Ljavax/xml/parsers/DocumentBuilder;
        //     22: astore 22
        //     24: new 197	java/io/ByteArrayInputStream
        //     27: dup
        //     28: aload_1
        //     29: ldc 199
        //     31: invokevirtual 205	java/lang/String:getBytes	(Ljava/lang/String;)[B
        //     34: invokespecial 208	java/io/ByteArrayInputStream:<init>	([B)V
        //     37: astore 23
        //     39: aload 22
        //     41: aload 23
        //     43: invokevirtual 214	javax/xml/parsers/DocumentBuilder:parse	(Ljava/io/InputStream;)Lorg/w3c/dom/Document;
        //     46: astore 24
        //     48: aload_0
        //     49: getfield 51	miui/app/screenelement/data/WebServiceBinder:mVariables	Ljava/util/ArrayList;
        //     52: invokevirtual 218	java/util/ArrayList:iterator	()Ljava/util/Iterator;
        //     55: astore 25
        //     57: aload 25
        //     59: invokeinterface 224 1 0
        //     64: ifeq +286 -> 350
        //     67: aload 25
        //     69: invokeinterface 228 1 0
        //     74: checkcast 11	miui/app/screenelement/data/WebServiceBinder$Variable
        //     77: astore 27
        //     79: aload 27
        //     81: getfield 232	miui/app/screenelement/data/WebServiceBinder$Variable:mStringVar	Lmiui/app/screenelement/util/IndexedStringVariable;
        //     84: astore 28
        //     86: aload 28
        //     88: ifnull +123 -> 211
        //     91: aload_2
        //     92: aload 27
        //     94: getfield 235	miui/app/screenelement/data/WebServiceBinder$Variable:mXPath	Ljava/lang/String;
        //     97: aload 24
        //     99: getstatic 241	javax/xml/xpath/XPathConstants:STRING	Ljavax/xml/namespace/QName;
        //     102: invokeinterface 247 4 0
        //     107: checkcast 201	java/lang/String
        //     110: astore 35
        //     112: aload 27
        //     114: getfield 232	miui/app/screenelement/data/WebServiceBinder$Variable:mStringVar	Lmiui/app/screenelement/util/IndexedStringVariable;
        //     117: aload 35
        //     119: invokevirtual 252	miui/app/screenelement/util/IndexedStringVariable:set	(Ljava/lang/String;)V
        //     122: goto -65 -> 57
        //     125: astore 33
        //     127: aload 27
        //     129: getfield 232	miui/app/screenelement/data/WebServiceBinder$Variable:mStringVar	Lmiui/app/screenelement/util/IndexedStringVariable;
        //     132: aconst_null
        //     133: invokevirtual 252	miui/app/screenelement/util/IndexedStringVariable:set	(Ljava/lang/String;)V
        //     136: ldc 16
        //     138: new 254	java/lang/StringBuilder
        //     141: dup
        //     142: invokespecial 255	java/lang/StringBuilder:<init>	()V
        //     145: ldc_w 257
        //     148: invokevirtual 261	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     151: aload 27
        //     153: getfield 262	miui/app/screenelement/data/WebServiceBinder$Variable:mName	Ljava/lang/String;
        //     156: invokevirtual 261	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     159: ldc_w 264
        //     162: invokevirtual 261	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     165: aload 33
        //     167: invokevirtual 268	javax/xml/xpath/XPathExpressionException:toString	()Ljava/lang/String;
        //     170: invokevirtual 261	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     173: invokevirtual 269	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     176: invokestatic 91	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     179: pop
        //     180: goto -123 -> 57
        //     183: astore 5
        //     185: aload 23
        //     187: astore 4
        //     189: ldc 16
        //     191: aload 5
        //     193: invokevirtual 270	javax/xml/parsers/ParserConfigurationException:toString	()Ljava/lang/String;
        //     196: invokestatic 91	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     199: pop
        //     200: aload 4
        //     202: ifnull +8 -> 210
        //     205: aload 4
        //     207: invokevirtual 275	java/io/InputStream:close	()V
        //     210: return
        //     211: aload 27
        //     213: getfield 279	miui/app/screenelement/data/WebServiceBinder$Variable:mNumberVar	Lmiui/app/screenelement/util/IndexedNumberVariable;
        //     216: astore 29
        //     218: aload 29
        //     220: ifnull -163 -> 57
        //     223: aload_2
        //     224: aload 27
        //     226: getfield 235	miui/app/screenelement/data/WebServiceBinder$Variable:mXPath	Ljava/lang/String;
        //     229: aload 24
        //     231: getstatic 282	javax/xml/xpath/XPathConstants:NUMBER	Ljavax/xml/namespace/QName;
        //     234: invokeinterface 247 4 0
        //     239: checkcast 284	java/lang/Double
        //     242: astore 32
        //     244: aload 27
        //     246: getfield 279	miui/app/screenelement/data/WebServiceBinder$Variable:mNumberVar	Lmiui/app/screenelement/util/IndexedNumberVariable;
        //     249: aload 32
        //     251: invokevirtual 289	miui/app/screenelement/util/IndexedNumberVariable:set	(Ljava/lang/Double;)V
        //     254: goto -197 -> 57
        //     257: astore 30
        //     259: aload 27
        //     261: getfield 279	miui/app/screenelement/data/WebServiceBinder$Variable:mNumberVar	Lmiui/app/screenelement/util/IndexedNumberVariable;
        //     264: aconst_null
        //     265: invokevirtual 289	miui/app/screenelement/util/IndexedNumberVariable:set	(Ljava/lang/Double;)V
        //     268: ldc 16
        //     270: new 254	java/lang/StringBuilder
        //     273: dup
        //     274: invokespecial 255	java/lang/StringBuilder:<init>	()V
        //     277: ldc_w 257
        //     280: invokevirtual 261	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     283: aload 27
        //     285: getfield 262	miui/app/screenelement/data/WebServiceBinder$Variable:mName	Ljava/lang/String;
        //     288: invokevirtual 261	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     291: ldc_w 264
        //     294: invokevirtual 261	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     297: aload 30
        //     299: invokevirtual 268	javax/xml/xpath/XPathExpressionException:toString	()Ljava/lang/String;
        //     302: invokevirtual 261	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     305: invokevirtual 269	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     308: invokestatic 91	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     311: pop
        //     312: goto -255 -> 57
        //     315: astore 10
        //     317: aload 23
        //     319: astore 4
        //     321: ldc 16
        //     323: aload 10
        //     325: invokevirtual 290	org/xml/sax/SAXException:toString	()Ljava/lang/String;
        //     328: invokestatic 91	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     331: pop
        //     332: aload 4
        //     334: ifnull -124 -> 210
        //     337: aload 4
        //     339: invokevirtual 275	java/io/InputStream:close	()V
        //     342: goto -132 -> 210
        //     345: astore 12
        //     347: goto -137 -> 210
        //     350: aload_0
        //     351: iconst_1
        //     352: putfield 57	miui/app/screenelement/data/WebServiceBinder:mQuerySuccessful	Z
        //     355: aload 23
        //     357: ifnull -147 -> 210
        //     360: aload 23
        //     362: invokevirtual 275	java/io/InputStream:close	()V
        //     365: goto -155 -> 210
        //     368: astore 26
        //     370: goto -160 -> 210
        //     373: astore 19
        //     375: ldc 16
        //     377: aload 19
        //     379: invokevirtual 291	java/io/UnsupportedEncodingException:toString	()Ljava/lang/String;
        //     382: invokestatic 91	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     385: pop
        //     386: aload 4
        //     388: ifnull -178 -> 210
        //     391: aload 4
        //     393: invokevirtual 275	java/io/InputStream:close	()V
        //     396: goto -186 -> 210
        //     399: astore 21
        //     401: goto -191 -> 210
        //     404: astore 16
        //     406: ldc 16
        //     408: aload 16
        //     410: invokevirtual 292	java/io/IOException:toString	()Ljava/lang/String;
        //     413: invokestatic 91	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     416: pop
        //     417: aload 4
        //     419: ifnull -209 -> 210
        //     422: aload 4
        //     424: invokevirtual 275	java/io/InputStream:close	()V
        //     427: goto -217 -> 210
        //     430: astore 18
        //     432: goto -222 -> 210
        //     435: astore 13
        //     437: ldc 16
        //     439: aload 13
        //     441: invokevirtual 293	java/lang/Exception:toString	()Ljava/lang/String;
        //     444: invokestatic 91	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     447: pop
        //     448: aload 4
        //     450: ifnull -240 -> 210
        //     453: aload 4
        //     455: invokevirtual 275	java/io/InputStream:close	()V
        //     458: goto -248 -> 210
        //     461: astore 15
        //     463: goto -253 -> 210
        //     466: astore 6
        //     468: aload 4
        //     470: ifnull +8 -> 478
        //     473: aload 4
        //     475: invokevirtual 275	java/io/InputStream:close	()V
        //     478: aload 6
        //     480: athrow
        //     481: astore 9
        //     483: goto -273 -> 210
        //     486: astore 7
        //     488: goto -10 -> 478
        //     491: astore 6
        //     493: aload 23
        //     495: astore 4
        //     497: goto -29 -> 468
        //     500: astore 13
        //     502: aload 23
        //     504: astore 4
        //     506: goto -69 -> 437
        //     509: astore 16
        //     511: aload 23
        //     513: astore 4
        //     515: goto -109 -> 406
        //     518: astore 19
        //     520: aload 23
        //     522: astore 4
        //     524: goto -149 -> 375
        //     527: astore 10
        //     529: goto -208 -> 321
        //     532: astore 5
        //     534: goto -345 -> 189
        //
        // Exception table:
        //     from	to	target	type
        //     91	122	125	javax/xml/xpath/XPathExpressionException
        //     39	86	183	javax/xml/parsers/ParserConfigurationException
        //     91	122	183	javax/xml/parsers/ParserConfigurationException
        //     127	180	183	javax/xml/parsers/ParserConfigurationException
        //     211	218	183	javax/xml/parsers/ParserConfigurationException
        //     223	254	183	javax/xml/parsers/ParserConfigurationException
        //     259	312	183	javax/xml/parsers/ParserConfigurationException
        //     350	355	183	javax/xml/parsers/ParserConfigurationException
        //     223	254	257	javax/xml/xpath/XPathExpressionException
        //     39	86	315	org/xml/sax/SAXException
        //     91	122	315	org/xml/sax/SAXException
        //     127	180	315	org/xml/sax/SAXException
        //     211	218	315	org/xml/sax/SAXException
        //     223	254	315	org/xml/sax/SAXException
        //     259	312	315	org/xml/sax/SAXException
        //     350	355	315	org/xml/sax/SAXException
        //     337	342	345	java/io/IOException
        //     360	365	368	java/io/IOException
        //     18	39	373	java/io/UnsupportedEncodingException
        //     391	396	399	java/io/IOException
        //     18	39	404	java/io/IOException
        //     422	427	430	java/io/IOException
        //     18	39	435	java/lang/Exception
        //     453	458	461	java/io/IOException
        //     18	39	466	finally
        //     189	200	466	finally
        //     321	332	466	finally
        //     375	386	466	finally
        //     406	417	466	finally
        //     437	448	466	finally
        //     205	210	481	java/io/IOException
        //     473	478	486	java/io/IOException
        //     39	86	491	finally
        //     91	122	491	finally
        //     127	180	491	finally
        //     211	218	491	finally
        //     223	254	491	finally
        //     259	312	491	finally
        //     350	355	491	finally
        //     39	86	500	java/lang/Exception
        //     91	122	500	java/lang/Exception
        //     127	180	500	java/lang/Exception
        //     211	218	500	java/lang/Exception
        //     223	254	500	java/lang/Exception
        //     259	312	500	java/lang/Exception
        //     350	355	500	java/lang/Exception
        //     39	86	509	java/io/IOException
        //     91	122	509	java/io/IOException
        //     127	180	509	java/io/IOException
        //     211	218	509	java/io/IOException
        //     223	254	509	java/io/IOException
        //     259	312	509	java/io/IOException
        //     350	355	509	java/io/IOException
        //     39	86	518	java/io/UnsupportedEncodingException
        //     91	122	518	java/io/UnsupportedEncodingException
        //     127	180	518	java/io/UnsupportedEncodingException
        //     211	218	518	java/io/UnsupportedEncodingException
        //     223	254	518	java/io/UnsupportedEncodingException
        //     259	312	518	java/io/UnsupportedEncodingException
        //     350	355	518	java/io/UnsupportedEncodingException
        //     18	39	527	org/xml/sax/SAXException
        //     18	39	532	javax/xml/parsers/ParserConfigurationException
    }

    private void tryStartQuery()
    {
        long l = System.currentTimeMillis() - this.mLastQueryTime;
        if (l < 0L)
            this.mLastQueryTime = 0L;
        if ((this.mLastQueryTime == 0L) || ((this.mUpdateInterval > 0) && (l > 1000 * this.mUpdateInterval)) || ((!this.mQuerySuccessful) && (this.mUpdateIntervalFail > 0) && (l > 1000 * this.mUpdateIntervalFail)))
            startQuery();
    }

    protected void addVariable(Variable paramVariable)
    {
        this.mVariables.add(paramVariable);
    }

    public void finish()
    {
        SharedPreferences.Editor localEditor = this.mContext.mContext.getSharedPreferences("MamlPreferences", 0).edit();
        localEditor.putLong(this.mName + "LastQueryTime", this.mLastQueryTime);
        Log.i("WebServiceBinder", "persist mLastQueryTime: " + this.mLastQueryTime);
        Iterator localIterator = this.mVariables.iterator();
        while (localIterator.hasNext())
        {
            Variable localVariable = (Variable)localIterator.next();
            if (localVariable.mPersist)
                if (localVariable.mStringVar != null)
                    localEditor.putString(this.mName + localVariable.mName, localVariable.mStringVar.get());
                else if (localVariable.mNumberVar != null)
                    localEditor.putFloat(this.mName + localVariable.mName, localVariable.mNumberVar.get().floatValue());
        }
        localEditor.commit();
    }

    public String getName()
    {
        return this.mName;
    }

    public void init()
    {
        this.mQuerySuccessful = true;
        SharedPreferences localSharedPreferences = this.mContext.mContext.getSharedPreferences("MamlPreferences", 0);
        this.mLastQueryTime = localSharedPreferences.getLong(this.mName + "LastQueryTime", 0L);
        Log.i("WebServiceBinder", "get persisted mLastQueryTime: " + this.mLastQueryTime);
        Iterator localIterator = this.mVariables.iterator();
        while (localIterator.hasNext())
        {
            Variable localVariable = (Variable)localIterator.next();
            if (localVariable.mPersist)
                if (localVariable.mStringVar != null)
                    localVariable.mStringVar.set(localSharedPreferences.getString(this.mName + localVariable.mName, null));
                else if (localVariable.mNumberVar != null)
                    localVariable.mNumberVar.set(localSharedPreferences.getFloat(this.mName + localVariable.mName, 0.0F));
        }
        tryStartQuery();
    }

    public void pause()
    {
    }

    public void refresh()
    {
        startQuery();
    }

    public void resume()
    {
        tryStartQuery();
    }

    public void startQuery()
    {
        if (this.mQueryInProgress);
        while (true)
        {
            return;
            this.mQueryInProgress = true;
            this.mQuerySuccessful = false;
            this.mQueryThread = new QueryThread();
            this.mQueryThread.start();
        }
    }

    private class QueryThread extends Thread
    {
        public QueryThread()
        {
            super();
        }

        public void run()
        {
            Log.i("WebServiceBinder", "QueryThread start");
            HttpPost localHttpPost = new HttpPost(Uri.parse(WebServiceBinder.this.mUriFormatter.getText(WebServiceBinder.this.mContext.mVariables)).toString());
            String str1 = WebServiceBinder.this.mParamsFormatter.getText(WebServiceBinder.this.mContext.mVariables);
            ArrayList localArrayList = null;
            if (!TextUtils.isEmpty(str1))
            {
                localArrayList = new ArrayList();
                String[] arrayOfString1 = str1.split(",");
                int i = arrayOfString1.length;
                int j = 0;
                if (j < i)
                {
                    String[] arrayOfString2 = arrayOfString1[j].split(":");
                    if (arrayOfString2.length != 2);
                    while (true)
                    {
                        j++;
                        break;
                        localArrayList.add(new BasicNameValuePair(arrayOfString2[0], arrayOfString2[1]));
                    }
                }
            }
            try
            {
                localHttpPost.setEntity(new UrlEncodedFormEntity(localArrayList, "UTF-8"));
                HttpResponse localHttpResponse = new DefaultHttpClient().execute(localHttpPost);
                String str2 = null;
                int k = localHttpResponse.getStatusLine().getStatusCode();
                if (k == 200)
                    str2 = EntityUtils.toString(localHttpResponse.getEntity());
                Log.i("WebServiceBinder", "QueryThread get result: " + k + " \n" + str2);
                WebServiceBinder.this.onQueryComplete(str2);
                WebServiceBinder.access$302(WebServiceBinder.this, System.currentTimeMillis());
                WebServiceBinder.access$402(WebServiceBinder.this, false);
                Log.i("WebServiceBinder", "QueryThread end");
                return;
            }
            catch (UnsupportedEncodingException localUnsupportedEncodingException)
            {
                while (true)
                    Log.e("WebServiceBinder", "fail to run query, " + localUnsupportedEncodingException.toString());
            }
            catch (ClientProtocolException localClientProtocolException)
            {
                while (true)
                    Log.e("WebServiceBinder", "fail to run query, " + localClientProtocolException.toString());
            }
            catch (IOException localIOException)
            {
                while (true)
                    Log.e("WebServiceBinder", "fail to run query, " + localIOException.toString());
            }
        }
    }

    public static class Variable
    {
        public static final String TAG_NAME = "Variable";
        private static final String TYPE_NUMBER = "number";
        private static final String TYPE_STRING = "string";
        public String mName;
        public IndexedNumberVariable mNumberVar;
        public boolean mPersist;
        public IndexedStringVariable mStringVar;
        public String mType;
        private Variables mVar;
        public String mXPath;

        public Variable()
        {
        }

        public Variable(Element paramElement, Variables paramVariables)
            throws ScreenElementLoadException
        {
            this.mVar = paramVariables;
            load(paramElement);
        }

        private void createVar()
        {
            if ("string".equalsIgnoreCase(this.mType))
                this.mStringVar = new IndexedStringVariable(this.mName, this.mVar);
            while (true)
            {
                return;
                if ("number".equalsIgnoreCase(this.mType))
                    this.mNumberVar = new IndexedNumberVariable(this.mName, this.mVar);
            }
        }

        private void load(Element paramElement)
            throws ScreenElementLoadException
        {
            if (paramElement == null)
            {
                Log.e("WebServiceBinder", "Variable node is null");
                throw new ScreenElementLoadException("node is null");
            }
            this.mName = paramElement.getAttribute("name");
            this.mType = paramElement.getAttribute("type");
            this.mXPath = paramElement.getAttribute("xpath");
            this.mPersist = Boolean.parseBoolean(paramElement.getAttribute("persist"));
            createVar();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.data.WebServiceBinder
 * JD-Core Version:        0.6.2
 */