package miui.app.screenelement.data;

import android.content.Context;
import android.content.Intent;
import miui.app.screenelement.NotifierManager;
import miui.app.screenelement.NotifierManager.NotifierType;
import miui.app.screenelement.NotifierManager.OnNotifyListener;
import miui.app.screenelement.ScreenContext;

public abstract class NotifierVariableUpdater extends VariableUpdater
    implements NotifierManager.OnNotifyListener
{
    protected NotifierManager mNotifierManager;
    private NotifierManager.NotifierType mType;

    public NotifierVariableUpdater(VariableUpdaterManager paramVariableUpdaterManager, NotifierManager.NotifierType paramNotifierType)
    {
        super(paramVariableUpdaterManager);
        this.mType = paramNotifierType;
        this.mNotifierManager = NotifierManager.getInstance(paramVariableUpdaterManager.getContext().getRawContext());
    }

    public void finish()
    {
        this.mNotifierManager.releaseNotifier(this.mType, this);
    }

    public void init()
    {
        this.mNotifierManager.acquireNotifier(this.mType, this);
    }

    public abstract void onNotify(Context paramContext, Intent paramIntent, Object paramObject);

    public void pause()
    {
        this.mNotifierManager.pause(this.mType, this);
    }

    public void resume()
    {
        this.mNotifierManager.resume(this.mType, this);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.data.NotifierVariableUpdater
 * JD-Core Version:        0.6.2
 */