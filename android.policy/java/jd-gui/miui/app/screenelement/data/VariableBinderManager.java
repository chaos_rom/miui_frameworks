package miui.app.screenelement.data;

import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import miui.app.screenelement.ScreenContext;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.util.TextFormatter;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class VariableBinderManager
    implements ContentProviderBinder.QueryCompleteListener
{
    private static final String LOG_TAG = "VariableBinderManager";
    public static final String TAG_NAME = "VariableBinders";
    private ScreenContext mContext;
    private ArrayList<VariableBinder> mVariableBinders = new ArrayList();

    public VariableBinderManager(Element paramElement, ScreenContext paramScreenContext)
        throws ScreenElementLoadException
    {
        this.mContext = paramScreenContext;
        if (paramElement != null)
            load(paramElement);
    }

    private static VariableBinder createBinder(Element paramElement, ScreenContext paramScreenContext, VariableBinderManager paramVariableBinderManager)
    {
        String str = paramElement.getTagName();
        Object localObject;
        try
        {
            if (str.equalsIgnoreCase("ContentProviderBinder"))
                localObject = new ContentProviderBinder(paramElement, paramScreenContext, paramVariableBinderManager);
            else if (str.equalsIgnoreCase("WebServiceBinder"))
                localObject = new WebServiceBinder(paramElement, paramScreenContext);
        }
        catch (ScreenElementLoadException localScreenElementLoadException)
        {
            localScreenElementLoadException.printStackTrace();
            localObject = null;
        }
        return localObject;
    }

    private void load(Element paramElement)
        throws ScreenElementLoadException
    {
        if (paramElement == null)
        {
            Log.e("VariableBinderManager", "node is null");
            throw new ScreenElementLoadException("node is null");
        }
        loadBinders(paramElement);
    }

    private void loadBinders(Element paramElement)
        throws ScreenElementLoadException
    {
        NodeList localNodeList = paramElement.getChildNodes();
        for (int i = 0; i < localNodeList.getLength(); i++)
            if (localNodeList.item(i).getNodeType() == 1)
            {
                VariableBinder localVariableBinder = createBinder((Element)localNodeList.item(i), this.mContext, this);
                if (localVariableBinder != null)
                    this.mVariableBinders.add(localVariableBinder);
            }
    }

    public ContentProviderBinder.Builder addContentProviderBinder(String paramString)
    {
        return addContentProviderBinder(new TextFormatter(paramString));
    }

    public ContentProviderBinder.Builder addContentProviderBinder(String paramString1, String paramString2)
    {
        return addContentProviderBinder(new TextFormatter(paramString1, paramString2));
    }

    public ContentProviderBinder.Builder addContentProviderBinder(TextFormatter paramTextFormatter)
    {
        ContentProviderBinder localContentProviderBinder = new ContentProviderBinder(this.mContext, this);
        localContentProviderBinder.mUriFormatter = paramTextFormatter;
        this.mVariableBinders.add(localContentProviderBinder);
        return new ContentProviderBinder.Builder(localContentProviderBinder);
    }

    public VariableBinder findBinder(String paramString)
    {
        Iterator localIterator = this.mVariableBinders.iterator();
        VariableBinder localVariableBinder;
        do
        {
            if (!localIterator.hasNext())
                break;
            localVariableBinder = (VariableBinder)localIterator.next();
        }
        while (!TextUtils.equals(paramString, localVariableBinder.getName()));
        while (true)
        {
            return localVariableBinder;
            localVariableBinder = null;
        }
    }

    public void finish()
    {
        Iterator localIterator = this.mVariableBinders.iterator();
        while (localIterator.hasNext())
            ((VariableBinder)localIterator.next()).finish();
    }

    public void init()
    {
        Iterator localIterator = this.mVariableBinders.iterator();
        while (localIterator.hasNext())
            ((VariableBinder)localIterator.next()).init();
    }

    public void onQueryCompleted(String paramString)
    {
        Iterator localIterator = this.mVariableBinders.iterator();
        while (localIterator.hasNext())
        {
            VariableBinder localVariableBinder = (VariableBinder)localIterator.next();
            if ((localVariableBinder instanceof ContentProviderBinder))
            {
                ContentProviderBinder localContentProviderBinder = (ContentProviderBinder)localVariableBinder;
                String str = localContentProviderBinder.getDependency();
                if ((!TextUtils.isEmpty(str)) && (str.equals(paramString)))
                    localContentProviderBinder.startQuery();
            }
        }
    }

    public void pause()
    {
        Iterator localIterator = this.mVariableBinders.iterator();
        while (localIterator.hasNext())
            ((VariableBinder)localIterator.next()).pause();
    }

    public void resume()
    {
        Iterator localIterator = this.mVariableBinders.iterator();
        while (localIterator.hasNext())
            ((VariableBinder)localIterator.next()).resume();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.data.VariableBinderManager
 * JD-Core Version:        0.6.2
 */