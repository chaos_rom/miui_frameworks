package miui.app.screenelement;

import miui.app.screenelement.elements.ButtonScreenElement;
import miui.app.screenelement.elements.ButtonScreenElement.ButtonAction;

public abstract interface InteractiveListener
{
    public abstract void onButtonInteractive(ButtonScreenElement paramButtonScreenElement, ButtonScreenElement.ButtonAction paramButtonAction);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.InteractiveListener
 * JD-Core Version:        0.6.2
 */