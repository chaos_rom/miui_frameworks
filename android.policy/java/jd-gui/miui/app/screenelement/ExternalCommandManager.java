package miui.app.screenelement;

import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ExternalCommandManager
{
    private static final String LOG_TAG = "ExternalCommandManager";
    public static final String TAG_NAME = "ExternalCommands";
    private ScreenContext mContext;
    private ArrayList<CommandTrigger> mTriggers = new ArrayList();

    public ExternalCommandManager(Element paramElement, ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot)
        throws ScreenElementLoadException
    {
        this.mContext = paramScreenContext;
        if (paramElement != null)
            load(paramElement, paramScreenElementRoot);
    }

    private void load(Element paramElement, ScreenElementRoot paramScreenElementRoot)
        throws ScreenElementLoadException
    {
        if (paramElement == null)
        {
            Log.e("ExternalCommandManager", "node is null");
            throw new ScreenElementLoadException("node is null");
        }
        NodeList localNodeList = paramElement.getChildNodes();
        int i = 0;
        if (i < localNodeList.getLength())
        {
            Element localElement;
            if (localNodeList.item(i).getNodeType() == 1)
            {
                localElement = (Element)localNodeList.item(i);
                if (localElement.getNodeName().equals("Trigger"))
                    break label94;
            }
            while (true)
            {
                i++;
                break;
                label94: this.mTriggers.add(new CommandTrigger(this.mContext, localElement, paramScreenElementRoot));
            }
        }
    }

    public void finish()
    {
        Iterator localIterator = this.mTriggers.iterator();
        while (localIterator.hasNext())
            ((CommandTrigger)localIterator.next()).finish();
    }

    public void init()
    {
        Iterator localIterator = this.mTriggers.iterator();
        while (localIterator.hasNext())
            ((CommandTrigger)localIterator.next()).init();
    }

    public void onCommand(String paramString)
    {
        Iterator localIterator = this.mTriggers.iterator();
        while (localIterator.hasNext())
        {
            CommandTrigger localCommandTrigger = (CommandTrigger)localIterator.next();
            if (localCommandTrigger.getActionString().equals(paramString))
                localCommandTrigger.perform();
        }
    }

    public void pause()
    {
        Iterator localIterator = this.mTriggers.iterator();
        while (localIterator.hasNext())
            ((CommandTrigger)localIterator.next()).pause();
    }

    public void resume()
    {
        Iterator localIterator = this.mTriggers.iterator();
        while (localIterator.hasNext())
            ((CommandTrigger)localIterator.next()).resume();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.ExternalCommandManager
 * JD-Core Version:        0.6.2
 */