package miui.app.screenelement.util;

import android.text.TextUtils;
import miui.app.screenelement.ResourceLoader;

public class ZipResourceLoader extends ResourceLoader
{
    private static final String IMAGES_FOLDER_NAME = "images";
    private static final String MANIFEST_FILE_NAME = "manifest.xml";
    private static final String TAG = "MAML_ZipResourceLoader";
    private String mInnerPath;
    private String mManifestName;
    private String mResourcePath;

    public ZipResourceLoader(String paramString)
    {
        this(paramString, null, "manifest.xml");
    }

    public ZipResourceLoader(String paramString1, String paramString2)
    {
        this(paramString1, paramString2, "manifest.xml");
    }

    public ZipResourceLoader(String paramString1, String paramString2, String paramString3)
    {
        if (TextUtils.isEmpty(paramString1))
            throw new IllegalArgumentException("empty zip path");
        this.mResourcePath = paramString1;
        if (paramString2 == null)
            paramString2 = "";
        this.mInnerPath = paramString2;
        this.mManifestName = paramString3;
    }

    // ERROR //
    public miui.app.screenelement.ResourceManager.BitmapInfo getBitmapInfo(String paramString, android.graphics.BitmapFactory.Options paramOptions)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_3
        //     2: aconst_null
        //     3: astore 4
        //     5: aconst_null
        //     6: astore 5
        //     8: new 54	java/util/zip/ZipFile
        //     11: dup
        //     12: aload_0
        //     13: getfield 40	miui/app/screenelement/util/ZipResourceLoader:mResourcePath	Ljava/lang/String;
        //     16: invokespecial 55	java/util/zip/ZipFile:<init>	(Ljava/lang/String;)V
        //     19: astore 6
        //     21: aconst_null
        //     22: astore 7
        //     24: aload_0
        //     25: getfield 58	miui/app/screenelement/ResourceLoader:mLanguageCountrySuffix	Ljava/lang/String;
        //     28: invokestatic 32	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
        //     31: ifne +53 -> 84
        //     34: aload 6
        //     36: new 60	java/lang/StringBuilder
        //     39: dup
        //     40: invokespecial 61	java/lang/StringBuilder:<init>	()V
        //     43: aload_0
        //     44: getfield 44	miui/app/screenelement/util/ZipResourceLoader:mInnerPath	Ljava/lang/String;
        //     47: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     50: ldc 8
        //     52: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     55: ldc 67
        //     57: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     60: aload_0
        //     61: getfield 58	miui/app/screenelement/ResourceLoader:mLanguageCountrySuffix	Ljava/lang/String;
        //     64: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     67: ldc 69
        //     69: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     72: aload_1
        //     73: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     76: invokevirtual 73	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     79: invokevirtual 77	java/util/zip/ZipFile:getEntry	(Ljava/lang/String;)Ljava/util/zip/ZipEntry;
        //     82: astore 7
        //     84: aload 7
        //     86: ifnonnull +63 -> 149
        //     89: aload_0
        //     90: getfield 80	miui/app/screenelement/ResourceLoader:mLanguageSuffix	Ljava/lang/String;
        //     93: invokestatic 32	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
        //     96: ifne +53 -> 149
        //     99: aload 6
        //     101: new 60	java/lang/StringBuilder
        //     104: dup
        //     105: invokespecial 61	java/lang/StringBuilder:<init>	()V
        //     108: aload_0
        //     109: getfield 44	miui/app/screenelement/util/ZipResourceLoader:mInnerPath	Ljava/lang/String;
        //     112: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     115: ldc 8
        //     117: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     120: ldc 67
        //     122: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     125: aload_0
        //     126: getfield 80	miui/app/screenelement/ResourceLoader:mLanguageSuffix	Ljava/lang/String;
        //     129: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     132: ldc 69
        //     134: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     137: aload_1
        //     138: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     141: invokevirtual 73	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     144: invokevirtual 77	java/util/zip/ZipFile:getEntry	(Ljava/lang/String;)Ljava/util/zip/ZipEntry;
        //     147: astore 7
        //     149: aload 7
        //     151: ifnonnull +31 -> 182
        //     154: aload 6
        //     156: new 60	java/lang/StringBuilder
        //     159: dup
        //     160: invokespecial 61	java/lang/StringBuilder:<init>	()V
        //     163: aload_0
        //     164: getfield 44	miui/app/screenelement/util/ZipResourceLoader:mInnerPath	Ljava/lang/String;
        //     167: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     170: aload_1
        //     171: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     174: invokevirtual 73	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     177: invokevirtual 77	java/util/zip/ZipFile:getEntry	(Ljava/lang/String;)Ljava/util/zip/ZipEntry;
        //     180: astore 7
        //     182: aload 7
        //     184: ifnull +97 -> 281
        //     187: aload 6
        //     189: aload 7
        //     191: invokevirtual 84	java/util/zip/ZipFile:getInputStream	(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;
        //     194: astore 5
        //     196: new 86	android/graphics/Rect
        //     199: dup
        //     200: invokespecial 87	android/graphics/Rect:<init>	()V
        //     203: astore 21
        //     205: aload 5
        //     207: aload 21
        //     209: aload_2
        //     210: invokestatic 93	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
        //     213: astore 22
        //     215: aload 22
        //     217: ifnonnull +25 -> 242
        //     220: aload 5
        //     222: ifnull +8 -> 230
        //     225: aload 5
        //     227: invokevirtual 98	java/io/InputStream:close	()V
        //     230: aload 6
        //     232: ifnull +8 -> 240
        //     235: aload 6
        //     237: invokevirtual 99	java/util/zip/ZipFile:close	()V
        //     240: aload_3
        //     241: areturn
        //     242: new 101	miui/app/screenelement/ResourceManager$BitmapInfo
        //     245: dup
        //     246: aload 22
        //     248: aload 21
        //     250: invokespecial 104	miui/app/screenelement/ResourceManager$BitmapInfo:<init>	(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V
        //     253: astore 23
        //     255: aload 5
        //     257: ifnull +8 -> 265
        //     260: aload 5
        //     262: invokevirtual 98	java/io/InputStream:close	()V
        //     265: aload 6
        //     267: ifnull +8 -> 275
        //     270: aload 6
        //     272: invokevirtual 99	java/util/zip/ZipFile:close	()V
        //     275: aload 23
        //     277: astore_3
        //     278: goto -38 -> 240
        //     281: iconst_0
        //     282: ifeq +5 -> 287
        //     285: aconst_null
        //     286: athrow
        //     287: aload 6
        //     289: ifnull +195 -> 484
        //     292: aload 6
        //     294: invokevirtual 99	java/util/zip/ZipFile:close	()V
        //     297: goto -57 -> 240
        //     300: astore 19
        //     302: goto -62 -> 240
        //     305: astore 8
        //     307: ldc 14
        //     309: aload 8
        //     311: invokevirtual 105	java/io/IOException:toString	()Ljava/lang/String;
        //     314: invokestatic 111	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     317: pop
        //     318: aload 5
        //     320: ifnull +8 -> 328
        //     323: aload 5
        //     325: invokevirtual 98	java/io/InputStream:close	()V
        //     328: aload 4
        //     330: ifnull -90 -> 240
        //     333: aload 4
        //     335: invokevirtual 99	java/util/zip/ZipFile:close	()V
        //     338: goto -98 -> 240
        //     341: astore 13
        //     343: goto -103 -> 240
        //     346: astore 15
        //     348: ldc 14
        //     350: aload 15
        //     352: invokevirtual 112	java/lang/OutOfMemoryError:toString	()Ljava/lang/String;
        //     355: invokestatic 111	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     358: pop
        //     359: aload 5
        //     361: ifnull +8 -> 369
        //     364: aload 5
        //     366: invokevirtual 98	java/io/InputStream:close	()V
        //     369: aload 4
        //     371: ifnull -131 -> 240
        //     374: aload 4
        //     376: invokevirtual 99	java/util/zip/ZipFile:close	()V
        //     379: goto -139 -> 240
        //     382: astore 17
        //     384: goto -144 -> 240
        //     387: astore 9
        //     389: aload 5
        //     391: ifnull +8 -> 399
        //     394: aload 5
        //     396: invokevirtual 98	java/io/InputStream:close	()V
        //     399: aload 4
        //     401: ifnull +8 -> 409
        //     404: aload 4
        //     406: invokevirtual 99	java/util/zip/ZipFile:close	()V
        //     409: aload 9
        //     411: athrow
        //     412: astore 27
        //     414: goto -184 -> 230
        //     417: astore 26
        //     419: goto -179 -> 240
        //     422: astore 25
        //     424: goto -159 -> 265
        //     427: astore 24
        //     429: goto -154 -> 275
        //     432: astore 20
        //     434: goto -147 -> 287
        //     437: astore 14
        //     439: goto -111 -> 328
        //     442: astore 18
        //     444: goto -75 -> 369
        //     447: astore 11
        //     449: goto -50 -> 399
        //     452: astore 10
        //     454: goto -45 -> 409
        //     457: astore 9
        //     459: aload 6
        //     461: astore 4
        //     463: goto -74 -> 389
        //     466: astore 15
        //     468: aload 6
        //     470: astore 4
        //     472: goto -124 -> 348
        //     475: astore 8
        //     477: aload 6
        //     479: astore 4
        //     481: goto -174 -> 307
        //     484: goto -244 -> 240
        //
        // Exception table:
        //     from	to	target	type
        //     292	297	300	java/io/IOException
        //     8	21	305	java/io/IOException
        //     333	338	341	java/io/IOException
        //     8	21	346	java/lang/OutOfMemoryError
        //     374	379	382	java/io/IOException
        //     8	21	387	finally
        //     307	318	387	finally
        //     348	359	387	finally
        //     225	230	412	java/io/IOException
        //     235	240	417	java/io/IOException
        //     260	265	422	java/io/IOException
        //     270	275	427	java/io/IOException
        //     285	287	432	java/io/IOException
        //     323	328	437	java/io/IOException
        //     364	369	442	java/io/IOException
        //     394	399	447	java/io/IOException
        //     404	409	452	java/io/IOException
        //     24	215	457	finally
        //     242	255	457	finally
        //     24	215	466	java/lang/OutOfMemoryError
        //     242	255	466	java/lang/OutOfMemoryError
        //     24	215	475	java/io/IOException
        //     242	255	475	java/io/IOException
    }

    // ERROR //
    public android.os.MemoryFile getFile(String paramString)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: aconst_null
        //     3: astore_3
        //     4: new 54	java/util/zip/ZipFile
        //     7: dup
        //     8: aload_0
        //     9: getfield 40	miui/app/screenelement/util/ZipResourceLoader:mResourcePath	Ljava/lang/String;
        //     12: invokespecial 55	java/util/zip/ZipFile:<init>	(Ljava/lang/String;)V
        //     15: astore 4
        //     17: aload 4
        //     19: new 60	java/lang/StringBuilder
        //     22: dup
        //     23: invokespecial 61	java/lang/StringBuilder:<init>	()V
        //     26: aload_0
        //     27: getfield 44	miui/app/screenelement/util/ZipResourceLoader:mInnerPath	Ljava/lang/String;
        //     30: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     33: aload_1
        //     34: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     37: invokevirtual 73	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     40: invokevirtual 77	java/util/zip/ZipFile:getEntry	(Ljava/lang/String;)Ljava/util/zip/ZipEntry;
        //     43: astore 17
        //     45: aload 17
        //     47: ifnull +143 -> 190
        //     50: aload 17
        //     52: invokevirtual 120	java/util/zip/ZipEntry:getSize	()J
        //     55: l2i
        //     56: istore 20
        //     58: aload 4
        //     60: aload 17
        //     62: invokevirtual 84	java/util/zip/ZipFile:getInputStream	(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;
        //     65: astore 21
        //     67: aload 21
        //     69: astore_3
        //     70: aload_3
        //     71: ifnonnull +27 -> 98
        //     74: aload_3
        //     75: ifnull +7 -> 82
        //     78: aload_3
        //     79: invokevirtual 98	java/io/InputStream:close	()V
        //     82: aload 4
        //     84: ifnull +8 -> 92
        //     87: aload 4
        //     89: invokevirtual 99	java/util/zip/ZipFile:close	()V
        //     92: aconst_null
        //     93: astore 11
        //     95: aload 11
        //     97: areturn
        //     98: ldc 121
        //     100: newarray byte
        //     102: astore 22
        //     104: new 123	android/os/MemoryFile
        //     107: dup
        //     108: aconst_null
        //     109: iload 20
        //     111: invokespecial 126	android/os/MemoryFile:<init>	(Ljava/lang/String;I)V
        //     114: astore 11
        //     116: iconst_0
        //     117: istore 23
        //     119: aload_3
        //     120: aload 22
        //     122: iconst_0
        //     123: ldc 121
        //     125: invokevirtual 130	java/io/InputStream:read	([BII)I
        //     128: istore 24
        //     130: iload 24
        //     132: ifle +25 -> 157
        //     135: aload 11
        //     137: aload 22
        //     139: iconst_0
        //     140: iload 23
        //     142: iload 24
        //     144: invokevirtual 134	android/os/MemoryFile:writeBytes	([BIII)V
        //     147: iload 23
        //     149: iload 24
        //     151: iadd
        //     152: istore 23
        //     154: goto -35 -> 119
        //     157: aload 11
        //     159: invokevirtual 138	android/os/MemoryFile:length	()I
        //     162: istore 25
        //     164: iload 25
        //     166: ifle +24 -> 190
        //     169: aload_3
        //     170: ifnull +7 -> 177
        //     173: aload_3
        //     174: invokevirtual 98	java/io/InputStream:close	()V
        //     177: aload 4
        //     179: ifnull +8 -> 187
        //     182: aload 4
        //     184: invokevirtual 99	java/util/zip/ZipFile:close	()V
        //     187: goto -92 -> 95
        //     190: aload_3
        //     191: ifnull +7 -> 198
        //     194: aload_3
        //     195: invokevirtual 98	java/io/InputStream:close	()V
        //     198: aload 4
        //     200: ifnull +183 -> 383
        //     203: aload 4
        //     205: invokevirtual 99	java/util/zip/ZipFile:close	()V
        //     208: aconst_null
        //     209: astore 11
        //     211: goto -116 -> 95
        //     214: astore 18
        //     216: goto -8 -> 208
        //     219: astore 5
        //     221: ldc 14
        //     223: aload 5
        //     225: invokevirtual 105	java/io/IOException:toString	()Ljava/lang/String;
        //     228: invokestatic 111	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     231: pop
        //     232: aload_3
        //     233: ifnull +7 -> 240
        //     236: aload_3
        //     237: invokevirtual 98	java/io/InputStream:close	()V
        //     240: aload_2
        //     241: ifnull -33 -> 208
        //     244: aload_2
        //     245: invokevirtual 99	java/util/zip/ZipFile:close	()V
        //     248: goto -40 -> 208
        //     251: astore 10
        //     253: goto -45 -> 208
        //     256: astore 13
        //     258: ldc 14
        //     260: aload 13
        //     262: invokevirtual 112	java/lang/OutOfMemoryError:toString	()Ljava/lang/String;
        //     265: invokestatic 111	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     268: pop
        //     269: aload_3
        //     270: ifnull +7 -> 277
        //     273: aload_3
        //     274: invokevirtual 98	java/io/InputStream:close	()V
        //     277: aload_2
        //     278: ifnull -70 -> 208
        //     281: aload_2
        //     282: invokevirtual 99	java/util/zip/ZipFile:close	()V
        //     285: goto -77 -> 208
        //     288: astore 15
        //     290: goto -82 -> 208
        //     293: astore 6
        //     295: aload_3
        //     296: ifnull +7 -> 303
        //     299: aload_3
        //     300: invokevirtual 98	java/io/InputStream:close	()V
        //     303: aload_2
        //     304: ifnull +7 -> 311
        //     307: aload_2
        //     308: invokevirtual 99	java/util/zip/ZipFile:close	()V
        //     311: aload 6
        //     313: athrow
        //     314: astore 29
        //     316: goto -234 -> 82
        //     319: astore 28
        //     321: goto -229 -> 92
        //     324: astore 27
        //     326: goto -149 -> 177
        //     329: astore 26
        //     331: goto -144 -> 187
        //     334: astore 19
        //     336: goto -138 -> 198
        //     339: astore 12
        //     341: goto -101 -> 240
        //     344: astore 16
        //     346: goto -69 -> 277
        //     349: astore 8
        //     351: goto -48 -> 303
        //     354: astore 7
        //     356: goto -45 -> 311
        //     359: astore 6
        //     361: aload 4
        //     363: astore_2
        //     364: goto -69 -> 295
        //     367: astore 13
        //     369: aload 4
        //     371: astore_2
        //     372: goto -114 -> 258
        //     375: astore 5
        //     377: aload 4
        //     379: astore_2
        //     380: goto -159 -> 221
        //     383: goto -175 -> 208
        //
        // Exception table:
        //     from	to	target	type
        //     203	208	214	java/io/IOException
        //     4	17	219	java/io/IOException
        //     244	248	251	java/io/IOException
        //     4	17	256	java/lang/OutOfMemoryError
        //     281	285	288	java/io/IOException
        //     4	17	293	finally
        //     221	232	293	finally
        //     258	269	293	finally
        //     78	82	314	java/io/IOException
        //     87	92	319	java/io/IOException
        //     173	177	324	java/io/IOException
        //     182	187	329	java/io/IOException
        //     194	198	334	java/io/IOException
        //     236	240	339	java/io/IOException
        //     273	277	344	java/io/IOException
        //     299	303	349	java/io/IOException
        //     307	311	354	java/io/IOException
        //     17	67	359	finally
        //     98	164	359	finally
        //     17	67	367	java/lang/OutOfMemoryError
        //     98	164	367	java/lang/OutOfMemoryError
        //     17	67	375	java/io/IOException
        //     98	164	375	java/io/IOException
    }

    // ERROR //
    public org.w3c.dom.Element getManifestRoot()
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_1
        //     2: aconst_null
        //     3: astore_2
        //     4: aconst_null
        //     5: astore_3
        //     6: new 54	java/util/zip/ZipFile
        //     9: dup
        //     10: aload_0
        //     11: getfield 40	miui/app/screenelement/util/ZipResourceLoader:mResourcePath	Ljava/lang/String;
        //     14: invokespecial 55	java/util/zip/ZipFile:<init>	(Ljava/lang/String;)V
        //     17: astore 4
        //     19: aconst_null
        //     20: astore 5
        //     22: aload_0
        //     23: getfield 58	miui/app/screenelement/ResourceLoader:mLanguageCountrySuffix	Ljava/lang/String;
        //     26: invokestatic 32	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
        //     29: ifne +45 -> 74
        //     32: aload_0
        //     33: getfield 46	miui/app/screenelement/util/ZipResourceLoader:mManifestName	Ljava/lang/String;
        //     36: aload_0
        //     37: getfield 58	miui/app/screenelement/ResourceLoader:mLanguageCountrySuffix	Ljava/lang/String;
        //     40: invokestatic 152	miui/app/screenelement/util/Utils:addFileNameSuffix	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //     43: astore 38
        //     45: aload 4
        //     47: new 60	java/lang/StringBuilder
        //     50: dup
        //     51: invokespecial 61	java/lang/StringBuilder:<init>	()V
        //     54: aload_0
        //     55: getfield 44	miui/app/screenelement/util/ZipResourceLoader:mInnerPath	Ljava/lang/String;
        //     58: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     61: aload 38
        //     63: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     66: invokevirtual 73	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     69: invokevirtual 77	java/util/zip/ZipFile:getEntry	(Ljava/lang/String;)Ljava/util/zip/ZipEntry;
        //     72: astore 5
        //     74: aload 5
        //     76: ifnonnull +55 -> 131
        //     79: aload_0
        //     80: getfield 80	miui/app/screenelement/ResourceLoader:mLanguageSuffix	Ljava/lang/String;
        //     83: invokestatic 32	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
        //     86: ifne +45 -> 131
        //     89: aload_0
        //     90: getfield 46	miui/app/screenelement/util/ZipResourceLoader:mManifestName	Ljava/lang/String;
        //     93: aload_0
        //     94: getfield 80	miui/app/screenelement/ResourceLoader:mLanguageSuffix	Ljava/lang/String;
        //     97: invokestatic 152	miui/app/screenelement/util/Utils:addFileNameSuffix	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //     100: astore 37
        //     102: aload 4
        //     104: new 60	java/lang/StringBuilder
        //     107: dup
        //     108: invokespecial 61	java/lang/StringBuilder:<init>	()V
        //     111: aload_0
        //     112: getfield 44	miui/app/screenelement/util/ZipResourceLoader:mInnerPath	Ljava/lang/String;
        //     115: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     118: aload 37
        //     120: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     123: invokevirtual 73	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     126: invokevirtual 77	java/util/zip/ZipFile:getEntry	(Ljava/lang/String;)Ljava/util/zip/ZipEntry;
        //     129: astore 5
        //     131: aload 5
        //     133: ifnonnull +34 -> 167
        //     136: aload 4
        //     138: new 60	java/lang/StringBuilder
        //     141: dup
        //     142: invokespecial 61	java/lang/StringBuilder:<init>	()V
        //     145: aload_0
        //     146: getfield 44	miui/app/screenelement/util/ZipResourceLoader:mInnerPath	Ljava/lang/String;
        //     149: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     152: aload_0
        //     153: getfield 46	miui/app/screenelement/util/ZipResourceLoader:mManifestName	Ljava/lang/String;
        //     156: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     159: invokevirtual 73	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     162: invokevirtual 77	java/util/zip/ZipFile:getEntry	(Ljava/lang/String;)Ljava/util/zip/ZipEntry;
        //     165: astore 5
        //     167: aload 5
        //     169: ifnull +80 -> 249
        //     172: aload 4
        //     174: aload 5
        //     176: invokevirtual 84	java/util/zip/ZipFile:getInputStream	(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;
        //     179: astore 31
        //     181: aload 31
        //     183: astore_3
        //     184: aload_3
        //     185: ifnonnull +23 -> 208
        //     188: aload_3
        //     189: ifnull +7 -> 196
        //     192: aload_3
        //     193: invokevirtual 98	java/io/InputStream:close	()V
        //     196: aload 4
        //     198: ifnull +8 -> 206
        //     201: aload 4
        //     203: invokevirtual 99	java/util/zip/ZipFile:close	()V
        //     206: aload_1
        //     207: areturn
        //     208: invokestatic 158	javax/xml/parsers/DocumentBuilderFactory:newInstance	()Ljavax/xml/parsers/DocumentBuilderFactory;
        //     211: invokevirtual 162	javax/xml/parsers/DocumentBuilderFactory:newDocumentBuilder	()Ljavax/xml/parsers/DocumentBuilder;
        //     214: aload_3
        //     215: invokevirtual 168	javax/xml/parsers/DocumentBuilder:parse	(Ljava/io/InputStream;)Lorg/w3c/dom/Document;
        //     218: invokeinterface 173 1 0
        //     223: astore 32
        //     225: aload 32
        //     227: astore_1
        //     228: aload_3
        //     229: ifnull +7 -> 236
        //     232: aload_3
        //     233: invokevirtual 98	java/io/InputStream:close	()V
        //     236: aload 4
        //     238: ifnull +8 -> 246
        //     241: aload 4
        //     243: invokevirtual 99	java/util/zip/ZipFile:close	()V
        //     246: goto -40 -> 206
        //     249: iconst_0
        //     250: ifeq +5 -> 255
        //     253: aconst_null
        //     254: athrow
        //     255: aload 4
        //     257: ifnull +330 -> 587
        //     260: aload 4
        //     262: invokevirtual 99	java/util/zip/ZipFile:close	()V
        //     265: goto -59 -> 206
        //     268: astore 29
        //     270: goto -64 -> 206
        //     273: astore 6
        //     275: ldc 14
        //     277: aload 6
        //     279: invokevirtual 105	java/io/IOException:toString	()Ljava/lang/String;
        //     282: invokestatic 111	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     285: pop
        //     286: aload_3
        //     287: ifnull +7 -> 294
        //     290: aload_3
        //     291: invokevirtual 98	java/io/InputStream:close	()V
        //     294: aload_2
        //     295: ifnull -89 -> 206
        //     298: aload_2
        //     299: invokevirtual 99	java/util/zip/ZipFile:close	()V
        //     302: goto -96 -> 206
        //     305: astore 11
        //     307: goto -101 -> 206
        //     310: astore 13
        //     312: ldc 14
        //     314: aload 13
        //     316: invokevirtual 112	java/lang/OutOfMemoryError:toString	()Ljava/lang/String;
        //     319: invokestatic 111	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     322: pop
        //     323: aload_3
        //     324: ifnull +7 -> 331
        //     327: aload_3
        //     328: invokevirtual 98	java/io/InputStream:close	()V
        //     331: aload_2
        //     332: ifnull -126 -> 206
        //     335: aload_2
        //     336: invokevirtual 99	java/util/zip/ZipFile:close	()V
        //     339: goto -133 -> 206
        //     342: astore 15
        //     344: goto -138 -> 206
        //     347: astore 17
        //     349: ldc 14
        //     351: aload 17
        //     353: invokevirtual 174	javax/xml/parsers/ParserConfigurationException:toString	()Ljava/lang/String;
        //     356: invokestatic 111	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     359: pop
        //     360: aload_3
        //     361: ifnull +7 -> 368
        //     364: aload_3
        //     365: invokevirtual 98	java/io/InputStream:close	()V
        //     368: aload_2
        //     369: ifnull -163 -> 206
        //     372: aload_2
        //     373: invokevirtual 99	java/util/zip/ZipFile:close	()V
        //     376: goto -170 -> 206
        //     379: astore 19
        //     381: goto -175 -> 206
        //     384: astore 21
        //     386: ldc 14
        //     388: aload 21
        //     390: invokevirtual 175	org/xml/sax/SAXException:toString	()Ljava/lang/String;
        //     393: invokestatic 111	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     396: pop
        //     397: aload_3
        //     398: ifnull +7 -> 405
        //     401: aload_3
        //     402: invokevirtual 98	java/io/InputStream:close	()V
        //     405: aload_2
        //     406: ifnull -200 -> 206
        //     409: aload_2
        //     410: invokevirtual 99	java/util/zip/ZipFile:close	()V
        //     413: goto -207 -> 206
        //     416: astore 23
        //     418: goto -212 -> 206
        //     421: astore 25
        //     423: ldc 14
        //     425: aload 25
        //     427: invokevirtual 176	java/lang/Exception:toString	()Ljava/lang/String;
        //     430: invokestatic 111	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     433: pop
        //     434: aload_3
        //     435: ifnull +7 -> 442
        //     438: aload_3
        //     439: invokevirtual 98	java/io/InputStream:close	()V
        //     442: aload_2
        //     443: ifnull -237 -> 206
        //     446: aload_2
        //     447: invokevirtual 99	java/util/zip/ZipFile:close	()V
        //     450: goto -244 -> 206
        //     453: astore 27
        //     455: goto -249 -> 206
        //     458: astore 7
        //     460: aload_3
        //     461: ifnull +7 -> 468
        //     464: aload_3
        //     465: invokevirtual 98	java/io/InputStream:close	()V
        //     468: aload_2
        //     469: ifnull +7 -> 476
        //     472: aload_2
        //     473: invokevirtual 99	java/util/zip/ZipFile:close	()V
        //     476: aload 7
        //     478: athrow
        //     479: astore 36
        //     481: goto -285 -> 196
        //     484: astore 35
        //     486: goto -280 -> 206
        //     489: astore 34
        //     491: goto -255 -> 236
        //     494: astore 33
        //     496: goto -250 -> 246
        //     499: astore 30
        //     501: goto -246 -> 255
        //     504: astore 12
        //     506: goto -212 -> 294
        //     509: astore 16
        //     511: goto -180 -> 331
        //     514: astore 20
        //     516: goto -148 -> 368
        //     519: astore 24
        //     521: goto -116 -> 405
        //     524: astore 28
        //     526: goto -84 -> 442
        //     529: astore 9
        //     531: goto -63 -> 468
        //     534: astore 8
        //     536: goto -60 -> 476
        //     539: astore 7
        //     541: aload 4
        //     543: astore_2
        //     544: goto -84 -> 460
        //     547: astore 25
        //     549: aload 4
        //     551: astore_2
        //     552: goto -129 -> 423
        //     555: astore 21
        //     557: aload 4
        //     559: astore_2
        //     560: goto -174 -> 386
        //     563: astore 17
        //     565: aload 4
        //     567: astore_2
        //     568: goto -219 -> 349
        //     571: astore 13
        //     573: aload 4
        //     575: astore_2
        //     576: goto -264 -> 312
        //     579: astore 6
        //     581: aload 4
        //     583: astore_2
        //     584: goto -309 -> 275
        //     587: goto -381 -> 206
        //
        // Exception table:
        //     from	to	target	type
        //     260	265	268	java/io/IOException
        //     6	19	273	java/io/IOException
        //     298	302	305	java/io/IOException
        //     6	19	310	java/lang/OutOfMemoryError
        //     335	339	342	java/io/IOException
        //     6	19	347	javax/xml/parsers/ParserConfigurationException
        //     372	376	379	java/io/IOException
        //     6	19	384	org/xml/sax/SAXException
        //     409	413	416	java/io/IOException
        //     6	19	421	java/lang/Exception
        //     446	450	453	java/io/IOException
        //     6	19	458	finally
        //     275	286	458	finally
        //     312	323	458	finally
        //     349	360	458	finally
        //     386	397	458	finally
        //     423	434	458	finally
        //     192	196	479	java/io/IOException
        //     201	206	484	java/io/IOException
        //     232	236	489	java/io/IOException
        //     241	246	494	java/io/IOException
        //     253	255	499	java/io/IOException
        //     290	294	504	java/io/IOException
        //     327	331	509	java/io/IOException
        //     364	368	514	java/io/IOException
        //     401	405	519	java/io/IOException
        //     438	442	524	java/io/IOException
        //     464	468	529	java/io/IOException
        //     472	476	534	java/io/IOException
        //     22	181	539	finally
        //     208	225	539	finally
        //     22	181	547	java/lang/Exception
        //     208	225	547	java/lang/Exception
        //     22	181	555	org/xml/sax/SAXException
        //     208	225	555	org/xml/sax/SAXException
        //     22	181	563	javax/xml/parsers/ParserConfigurationException
        //     208	225	563	javax/xml/parsers/ParserConfigurationException
        //     22	181	571	java/lang/OutOfMemoryError
        //     208	225	571	java/lang/OutOfMemoryError
        //     22	181	579	java/io/IOException
        //     208	225	579	java/io/IOException
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.util.ZipResourceLoader
 * JD-Core Version:        0.6.2
 */