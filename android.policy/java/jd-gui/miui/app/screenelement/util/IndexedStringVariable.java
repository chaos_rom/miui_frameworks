package miui.app.screenelement.util;

import miui.app.screenelement.data.Variables;

public class IndexedStringVariable
{
    private int mIndex = -1;
    private Variables mVars;

    public IndexedStringVariable(String paramString1, String paramString2, Variables paramVariables)
    {
        this.mIndex = paramVariables.registerStringVariable(paramString1, paramString2);
        this.mVars = paramVariables;
    }

    public IndexedStringVariable(String paramString, Variables paramVariables)
    {
        this(null, paramString, paramVariables);
    }

    public String get()
    {
        return this.mVars.getStr(this.mIndex);
    }

    public void set(String paramString)
    {
        this.mVars.putStr(this.mIndex, paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.util.IndexedStringVariable
 * JD-Core Version:        0.6.2
 */