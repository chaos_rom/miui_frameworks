package miui.app.screenelement.util;

import android.text.TextUtils;
import miui.app.screenelement.ScreenElementLoadException;
import miui.app.screenelement.data.Variables;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Utils
{
    public static double Dist(Point paramPoint1, Point paramPoint2, boolean paramBoolean)
    {
        double d1 = paramPoint1.x - paramPoint2.x;
        double d2 = paramPoint1.y - paramPoint2.y;
        if (paramBoolean);
        for (double d3 = Math.sqrt(d1 * d1 + d2 * d2); ; d3 = d1 * d1 + d2 * d2)
            return d3;
    }

    public static String addFileNameSuffix(String paramString1, String paramString2)
    {
        return addFileNameSuffix(paramString1, "_", paramString2);
    }

    public static String addFileNameSuffix(String paramString1, String paramString2, String paramString3)
    {
        int i = paramString1.indexOf('.');
        return paramString1.substring(0, i) + paramString2 + paramString3 + paramString1.substring(i);
    }

    public static void asserts(boolean paramBoolean)
        throws ScreenElementLoadException
    {
        asserts(paramBoolean, "assert error");
    }

    public static void asserts(boolean paramBoolean, String paramString)
        throws ScreenElementLoadException
    {
        if (!paramBoolean)
            throw new ScreenElementLoadException(paramString);
    }

    public static String doubleToString(double paramDouble)
    {
        String str = String.valueOf(paramDouble);
        if (str.endsWith(".0"))
            str = str.substring(0, -2 + str.length());
        return str;
    }

    public static boolean equals(Object paramObject1, Object paramObject2)
    {
        if (paramObject1 != paramObject2)
            if (paramObject1 != null);
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            if (!paramObject1.equals(paramObject2))
                break;
        }
    }

    public static float getAttrAsFloat(Element paramElement, String paramString, float paramFloat)
    {
        try
        {
            float f = Float.parseFloat(paramElement.getAttribute(paramString));
            paramFloat = f;
            label15: return paramFloat;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            break label15;
        }
    }

    public static float getAttrAsFloatThrows(Element paramElement, String paramString)
        throws ScreenElementLoadException
    {
        try
        {
            float f = Float.parseFloat(paramElement.getAttribute(paramString));
            return f;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = paramString;
            arrayOfObject[1] = paramElement.toString();
            throw new ScreenElementLoadException(String.format("fail to get attribute name: %s of Element %s", arrayOfObject));
        }
    }

    public static int getAttrAsInt(Element paramElement, String paramString, int paramInt)
    {
        try
        {
            int i = Integer.parseInt(paramElement.getAttribute(paramString));
            paramInt = i;
            label15: return paramInt;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            break label15;
        }
    }

    public static int getAttrAsIntThrows(Element paramElement, String paramString)
        throws ScreenElementLoadException
    {
        try
        {
            int i = Integer.parseInt(paramElement.getAttribute(paramString));
            return i;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = paramString;
            arrayOfObject[1] = paramElement.toString();
            throw new ScreenElementLoadException(String.format("fail to get attribute name: %s of Element %s", arrayOfObject));
        }
    }

    public static long getAttrAsLong(Element paramElement, String paramString, long paramLong)
    {
        try
        {
            long l = Long.parseLong(paramElement.getAttribute(paramString));
            paramLong = l;
            label15: return paramLong;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            break label15;
        }
    }

    public static long getAttrAsLongThrows(Element paramElement, String paramString)
        throws ScreenElementLoadException
    {
        try
        {
            long l = Long.parseLong(paramElement.getAttribute(paramString));
            return l;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = paramString;
            arrayOfObject[1] = paramElement.toString();
            throw new ScreenElementLoadException(String.format("fail to get attribute name: %s of Element %s", arrayOfObject));
        }
    }

    public static Element getChild(Element paramElement, String paramString)
    {
        Element localElement;
        if (paramElement == null)
            localElement = null;
        while (true)
        {
            return localElement;
            NodeList localNodeList = paramElement.getChildNodes();
            for (int i = 0; ; i++)
            {
                if (i >= localNodeList.getLength())
                    break label79;
                Node localNode = localNodeList.item(i);
                if ((localNode.getNodeType() == 1) && (localNode.getNodeName().equalsIgnoreCase(paramString)))
                {
                    localElement = (Element)localNode;
                    break;
                }
            }
            label79: localElement = null;
        }
    }

    public static double getVariableNumber(String paramString1, String paramString2, Variables paramVariables)
    {
        return new IndexedNumberVariable(paramString1, paramString2, paramVariables).get().doubleValue();
    }

    public static double getVariableNumber(String paramString, Variables paramVariables)
    {
        return getVariableNumber(null, paramString, paramVariables);
    }

    public static String getVariableString(String paramString1, String paramString2, Variables paramVariables)
    {
        return new IndexedStringVariable(paramString1, paramString2, paramVariables).get();
    }

    public static String getVariableString(String paramString, Variables paramVariables)
    {
        return getVariableString(null, paramString, paramVariables);
    }

    public static int mixAlpha(int paramInt1, int paramInt2)
    {
        if (paramInt1 >= 255);
        while (true)
        {
            return paramInt2;
            if (paramInt2 >= 255)
                paramInt2 = paramInt1;
            else
                paramInt2 = Math.round(paramInt1 * paramInt2 / 255.0F);
        }
    }

    public static Point pointProjectionOnSegment(Point paramPoint1, Point paramPoint2, Point paramPoint3, boolean paramBoolean)
    {
        Point localPoint1 = paramPoint2.minus(paramPoint1);
        Point localPoint2 = paramPoint3.minus(paramPoint1);
        double d = (localPoint1.x * localPoint2.x + localPoint1.y * localPoint2.y) / Dist(paramPoint1, paramPoint2, false);
        Point localPoint3;
        if ((d < 0.0D) || (d > 1.0D))
            if (!paramBoolean)
            {
                paramPoint1 = null;
                localPoint3 = paramPoint1;
            }
        while (true)
        {
            return localPoint3;
            if (d < 0.0D)
                break;
            paramPoint1 = paramPoint2;
            break;
            localPoint3 = localPoint1;
            localPoint3.x = (d * localPoint3.x);
            localPoint3.y = (d * localPoint3.y);
            localPoint3.Offset(paramPoint1);
        }
    }

    public static void putVariableNumber(String paramString1, String paramString2, Variables paramVariables, Double paramDouble)
    {
        new IndexedNumberVariable(paramString1, paramString2, paramVariables).set(paramDouble);
    }

    public static void putVariableNumber(String paramString, Variables paramVariables, Double paramDouble)
    {
        putVariableNumber(null, paramString, paramVariables, paramDouble);
    }

    public static void putVariableString(String paramString1, String paramString2, Variables paramVariables, String paramString3)
    {
        new IndexedStringVariable(paramString1, paramString2, paramVariables).set(paramString3);
    }

    public static void putVariableString(String paramString1, Variables paramVariables, String paramString2)
    {
        putVariableString(null, paramString1, paramVariables, paramString2);
    }

    public static double stringToDouble(String paramString, double paramDouble)
    {
        if (paramString == null);
        while (true)
        {
            return paramDouble;
            try
            {
                double d = Double.parseDouble(paramString);
                paramDouble = d;
            }
            catch (NumberFormatException localNumberFormatException)
            {
            }
        }
    }

    public static void traverseXmlElementChildren(Element paramElement, String paramString, XmlTraverseListener paramXmlTraverseListener)
    {
        NodeList localNodeList = paramElement.getChildNodes();
        for (int i = 0; i < localNodeList.getLength(); i++)
        {
            Node localNode = localNodeList.item(i);
            if ((localNode.getNodeType() == 1) && ((paramString == null) || (TextUtils.equals(localNode.getNodeName(), paramString))))
                paramXmlTraverseListener.onChild((Element)localNode);
        }
    }

    public static abstract interface XmlTraverseListener
    {
        public abstract void onChild(Element paramElement);
    }

    public static class Point
    {
        public double x;
        public double y;

        public Point(double paramDouble1, double paramDouble2)
        {
            this.x = paramDouble1;
            this.y = paramDouble2;
        }

        public void Offset(Point paramPoint)
        {
            this.x += paramPoint.x;
            this.y += paramPoint.y;
        }

        Point minus(Point paramPoint)
        {
            return new Point(this.x - paramPoint.x, this.y - paramPoint.y);
        }
    }

    public static class GetChildWrapper
    {
        private Element mEle;

        public GetChildWrapper(Element paramElement)
        {
            this.mEle = paramElement;
        }

        public GetChildWrapper getChild(String paramString)
        {
            return new GetChildWrapper(Utils.getChild(this.mEle, paramString));
        }

        public Element getElement()
        {
            return this.mEle;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.util.Utils
 * JD-Core Version:        0.6.2
 */