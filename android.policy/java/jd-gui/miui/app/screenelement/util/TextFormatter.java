package miui.app.screenelement.util;

import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;
import java.util.IllegalFormatException;
import miui.app.screenelement.data.Expression;
import miui.app.screenelement.data.Variables;
import org.w3c.dom.Element;

public class TextFormatter
{
    private static final String LOG_TAG = "TextFormatter";
    private String mFormat;
    private Expression mFormatExpression;
    private Variable mFormatVar;
    private IndexedStringVariable mIndexedFormatVar;
    private IndexedStringVariable mIndexedTextVar;
    private FormatPara[] mParas;
    private Object[] mParasValue;
    private String mText;
    private Expression mTextExpression;
    private Variable mTextVar;

    public TextFormatter(String paramString)
    {
        this(paramString, "", "");
    }

    public TextFormatter(String paramString1, String paramString2)
    {
        this("", paramString1, paramString2);
    }

    public TextFormatter(String paramString1, String paramString2, String paramString3)
    {
        this.mText = paramString1;
        if (this.mText.startsWith("@"))
        {
            this.mText = this.mText.substring(1);
            if (!this.mText.startsWith("@"))
            {
                this.mTextVar = new Variable(this.mText);
                this.mText = "";
            }
        }
        this.mFormat = paramString2;
        if (this.mFormat.startsWith("@"))
        {
            this.mFormat = this.mFormat.substring(1);
            if (!this.mFormat.startsWith("@"))
            {
                this.mFormatVar = new Variable(this.mFormat);
                this.mFormat = "";
            }
        }
        if (!TextUtils.isEmpty(paramString3))
        {
            this.mParas = FormatPara.buildArray(paramString3);
            if (this.mParas != null)
                this.mParasValue = new Object[this.mParas.length];
        }
    }

    public TextFormatter(String paramString1, String paramString2, String paramString3, Expression paramExpression1, Expression paramExpression2)
    {
        this(paramString1, paramString2, paramString3);
        this.mTextExpression = paramExpression1;
        this.mFormatExpression = paramExpression2;
    }

    public static TextFormatter fromElement(Element paramElement)
    {
        return new TextFormatter(paramElement.getAttribute("text"), paramElement.getAttribute("format"), paramElement.getAttribute("paras"), Expression.build(paramElement.getAttribute("textExp")), Expression.build(paramElement.getAttribute("formatExp")));
    }

    public static TextFormatter fromElement(Element paramElement, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
    {
        return new TextFormatter(paramElement.getAttribute(paramString1), paramElement.getAttribute(paramString2), paramElement.getAttribute(paramString3), Expression.build(paramElement.getAttribute(paramString4)), Expression.build(paramElement.getAttribute(paramString5)));
    }

    public String getFormat(Variables paramVariables)
    {
        String str;
        if (this.mFormatExpression != null)
            str = this.mFormatExpression.evaluateStr(paramVariables);
        while (true)
        {
            return str;
            if (this.mFormatVar != null)
            {
                if (this.mIndexedFormatVar == null)
                    this.mIndexedFormatVar = new IndexedStringVariable(this.mFormatVar.getObjName(), this.mFormatVar.getPropertyName(), paramVariables);
                str = this.mIndexedFormatVar.get();
            }
            else
            {
                str = this.mFormat;
            }
        }
    }

    public String getText(Variables paramVariables)
    {
        Object localObject;
        if (this.mTextExpression != null)
            localObject = this.mTextExpression.evaluateStr(paramVariables);
        while (true)
        {
            return localObject;
            String str1 = getFormat(paramVariables);
            if ((!TextUtils.isEmpty(str1)) && (this.mParas != null))
            {
                for (int i = 0; i < this.mParas.length; i++)
                    this.mParasValue[i] = this.mParas[i].evaluate(paramVariables);
                try
                {
                    String str2 = String.format(str1, this.mParasValue);
                    localObject = str2;
                }
                catch (IllegalFormatException localIllegalFormatException)
                {
                    localObject = "Format error: " + str1;
                }
            }
            else if (this.mTextVar != null)
            {
                if (this.mIndexedTextVar == null)
                    this.mIndexedTextVar = new IndexedStringVariable(this.mTextVar.getObjName(), this.mTextVar.getPropertyName(), paramVariables);
                localObject = this.mIndexedTextVar.get();
            }
            else
            {
                localObject = this.mText;
            }
        }
    }

    public boolean hasFormat()
    {
        if ((!TextUtils.isEmpty(this.mFormat)) || (this.mFormatVar != null));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void setText(String paramString)
    {
        this.mText = paramString;
        this.mFormat = "";
    }

    private static class StringVarPara extends TextFormatter.FormatPara
    {
        private IndexedStringVariable mVar;
        private Variable mVariable;

        public StringVarPara(Variable paramVariable)
        {
            super();
            this.mVariable = paramVariable;
        }

        public Object evaluate(Variables paramVariables)
        {
            if (this.mVar == null)
                this.mVar = new IndexedStringVariable(this.mVariable.getObjName(), this.mVariable.getPropertyName(), paramVariables);
            String str = this.mVar.get();
            if (str == null)
                str = "";
            return str;
        }
    }

    private static class ExpressioPara extends TextFormatter.FormatPara
    {
        private Expression mExp;

        public ExpressioPara(Expression paramExpression)
        {
            super();
            this.mExp = paramExpression;
        }

        public Object evaluate(Variables paramVariables)
        {
            return Long.valueOf(()this.mExp.evaluate(paramVariables));
        }
    }

    private static abstract class FormatPara
    {
        public static FormatPara build(String paramString)
        {
            String str = paramString.trim();
            Object localObject;
            if (str.startsWith("@"))
                localObject = new TextFormatter.StringVarPara(new Variable(str.substring(1)));
            while (true)
            {
                return localObject;
                Expression localExpression = Expression.build(str);
                if (localExpression == null)
                {
                    Log.e("TextFormatter", "invalid parameter expression:" + paramString);
                    localObject = null;
                }
                else
                {
                    localObject = new TextFormatter.ExpressioPara(localExpression);
                }
            }
        }

        public static FormatPara[] buildArray(String paramString)
        {
            FormatPara[] arrayOfFormatPara = null;
            int i = 0;
            int j = 0;
            ArrayList localArrayList = new ArrayList();
            int k = 0;
            int m;
            FormatPara localFormatPara2;
            if (k < paramString.length())
            {
                m = paramString.charAt(k);
                if ((i == 0) && (m == 44))
                {
                    localFormatPara2 = build(paramString.substring(j, k));
                    if (localFormatPara2 != null);
                }
            }
            while (true)
            {
                return arrayOfFormatPara;
                localArrayList.add(localFormatPara2);
                j = k + 1;
                if (m == 40)
                    i++;
                while (true)
                {
                    k++;
                    break;
                    if (m == 41)
                        i--;
                }
                FormatPara localFormatPara1 = build(paramString.substring(j));
                if (localFormatPara1 != null)
                {
                    localArrayList.add(localFormatPara1);
                    arrayOfFormatPara = (FormatPara[])localArrayList.toArray(new FormatPara[localArrayList.size()]);
                }
            }
        }

        public abstract Object evaluate(Variables paramVariables);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.util.TextFormatter
 * JD-Core Version:        0.6.2
 */