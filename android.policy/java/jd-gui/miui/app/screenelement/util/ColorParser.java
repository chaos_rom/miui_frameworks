package miui.app.screenelement.util;

import android.graphics.Color;
import android.util.Log;
import miui.app.screenelement.data.Expression;
import miui.app.screenelement.data.Variables;
import org.w3c.dom.Element;

public class ColorParser
{
    private static final int DEFAULT_COLOR = -1;
    private static final String LOG_TAG = "ColorParser";
    private int mColor;
    private String mColorExpression;
    private IndexedStringVariable mIndexedColorVar;
    private Expression[] mRGBExpression;
    private ExpressionType mType;

    public ColorParser(String paramString)
    {
        this.mColorExpression = paramString.trim();
        if (this.mColorExpression.startsWith("#"))
            this.mType = ExpressionType.CONST;
        while (true)
        {
            try
            {
                this.mColor = Color.parseColor(this.mColorExpression);
                return;
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
                this.mColor = -1;
                continue;
            }
            if (this.mColorExpression.startsWith("@"))
            {
                this.mType = ExpressionType.VARIABLE;
            }
            else if ((this.mColorExpression.startsWith("argb(")) && (this.mColorExpression.endsWith(")")))
            {
                this.mType = ExpressionType.ARGB;
                this.mRGBExpression = Expression.buildMultiple(this.mColorExpression.substring(5, -1 + this.mColorExpression.length()));
                if (this.mRGBExpression.length != 4)
                {
                    Log.e("ColorParser", "bad expression format");
                    throw new IllegalArgumentException("bad expression format.");
                }
            }
            else
            {
                this.mType = ExpressionType.INVALID;
            }
        }
    }

    public static ColorParser fromElement(Element paramElement)
    {
        return new ColorParser(paramElement.getAttribute("color"));
    }

    public int getColor(Variables paramVariables)
    {
        switch (1.$SwitchMap$miui$app$screenelement$util$ColorParser$ExpressionType[this.mType.ordinal()])
        {
        case 1:
        default:
        case 2:
        case 3:
        }
        while (true)
        {
            return this.mColor;
            if (this.mIndexedColorVar == null)
                this.mIndexedColorVar = new IndexedStringVariable(this.mColorExpression.substring(1), paramVariables);
            if (this.mIndexedColorVar.get() != null);
            for (int i = Color.parseColor(this.mIndexedColorVar.get()); ; i = -1)
            {
                this.mColor = i;
                break;
            }
            this.mColor = Color.argb((int)this.mRGBExpression[0].evaluate(paramVariables), (int)this.mRGBExpression[1].evaluate(paramVariables), (int)this.mRGBExpression[2].evaluate(paramVariables), (int)this.mRGBExpression[3].evaluate(paramVariables));
        }
    }

    private static enum ExpressionType
    {
        static
        {
            ARGB = new ExpressionType("ARGB", 2);
            INVALID = new ExpressionType("INVALID", 3);
            ExpressionType[] arrayOfExpressionType = new ExpressionType[4];
            arrayOfExpressionType[0] = CONST;
            arrayOfExpressionType[1] = VARIABLE;
            arrayOfExpressionType[2] = ARGB;
            arrayOfExpressionType[3] = INVALID;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.util.ColorParser
 * JD-Core Version:        0.6.2
 */