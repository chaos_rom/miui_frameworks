package miui.app.screenelement;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.WindowManager;
import java.util.ArrayList;
import java.util.Iterator;
import miui.app.screenelement.data.BatteryVariableUpdater;
import miui.app.screenelement.data.DateTimeVariableUpdater;
import miui.app.screenelement.data.VariableBinder;
import miui.app.screenelement.data.VariableBinderManager;
import miui.app.screenelement.data.VariableUpdaterManager;
import miui.app.screenelement.elements.ButtonScreenElement;
import miui.app.screenelement.elements.ButtonScreenElement.ButtonAction;
import miui.app.screenelement.elements.ElementGroup;
import miui.app.screenelement.elements.FramerateController;
import miui.app.screenelement.elements.ScreenElement;
import miui.app.screenelement.util.IndexedNumberVariable;
import miui.app.screenelement.util.Task;
import miui.app.screenelement.util.Utils;
import org.w3c.dom.Element;

public class ScreenElementRoot extends ScreenElement
    implements InteractiveListener
{
    private static final boolean CALCULATE_FRAME_RATE = true;
    private static final int DEFAULT_SCREEN_WIDTH = 480;
    private static final String LOG_TAG = "ScreenElementRoot";
    private static final int RES_DENSITY = 240;
    private static final String ROOT_NAME = "__root";
    private float DEFAULT_FRAME_RATE = 30.0F;
    private long mCheckPoint;
    private int mDefaultResourceDensity;
    private int mDefaultScreenWidth;
    protected ElementGroup mElementGroup;
    private ExternalCommandManager mExternalCommandManager;
    private boolean mFinished;
    protected float mFrameRate;
    private IndexedNumberVariable mFrameRateVar;
    private ArrayList<FramerateController> mFramerateControllers = new ArrayList();
    private int mFrames;
    private boolean mNeedDisallowInterceptTouchEvent;
    private IndexedNumberVariable mNeedDisallowInterceptTouchEventVar;
    protected float mNormalFrameRate = this.DEFAULT_FRAME_RATE;
    private float mScale;
    private SoundManager mSoundManager;
    private int mTargetDensity;
    private IndexedNumberVariable mTouchBeginTime;
    private IndexedNumberVariable mTouchBeginX;
    private IndexedNumberVariable mTouchBeginY;
    private IndexedNumberVariable mTouchX;
    private IndexedNumberVariable mTouchY;
    protected VariableBinderManager mVariableBinderManager;
    private VariableUpdaterManager mVariableUpdaterManager;

    public ScreenElementRoot(ScreenContext paramScreenContext)
    {
        super(null, paramScreenContext, null);
        this.mRoot = this;
        this.mVariableUpdaterManager = new VariableUpdaterManager(paramScreenContext);
        this.mTouchX = new IndexedNumberVariable("touch_x", getContext().mVariables);
        this.mTouchY = new IndexedNumberVariable("touch_y", getContext().mVariables);
        this.mTouchBeginX = new IndexedNumberVariable("touch_begin_x", getContext().mVariables);
        this.mTouchBeginY = new IndexedNumberVariable("touch_begin_y", getContext().mVariables);
        this.mTouchBeginTime = new IndexedNumberVariable("touch_begin_time", getContext().mVariables);
        this.mNeedDisallowInterceptTouchEventVar = new IndexedNumberVariable("intercept_sys_touch", getContext().mVariables);
    }

    private void processUseVariableUpdater(Element paramElement)
    {
        String str1 = paramElement.getAttribute("useVariableUpdater");
        if (TextUtils.isEmpty(str1))
        {
            onAddVariableUpdater(this.mVariableUpdaterManager);
            return;
        }
        String[] arrayOfString = str1.split(",");
        int i = arrayOfString.length;
        int j = 0;
        label39: String str2;
        if (j < i)
        {
            str2 = arrayOfString[j];
            if (!str2.equals("DateTime"))
                break label86;
            this.mVariableUpdaterManager.add(new DateTimeVariableUpdater(this.mVariableUpdaterManager));
        }
        while (true)
        {
            j++;
            break label39;
            break;
            label86: if (str2.equals("Battery"))
                this.mVariableUpdaterManager.add(new BatteryVariableUpdater(this.mVariableUpdaterManager));
        }
    }

    private void resolveResource(Element paramElement, int paramInt)
    {
        String str1 = paramElement.getAttribute("extraResourcesScreenWidth");
        String[] arrayOfString;
        int i;
        int j;
        int k;
        int m;
        if (!TextUtils.isEmpty(str1))
        {
            arrayOfString = str1.split(",");
            i = 2147483647;
            j = 0;
            k = arrayOfString.length;
            m = 0;
        }
        while (true)
        {
            String str2;
            if (m < k)
                str2 = arrayOfString[m];
            try
            {
                int n = Integer.parseInt(str2);
                int i1 = Math.abs(paramInt - n);
                if (i1 < i)
                {
                    i = i1;
                    j = n;
                    if (i1 == 0)
                    {
                        if (Math.abs(paramInt - this.mDefaultScreenWidth) >= i)
                            this.mContext.setExtraResource(j);
                        return;
                    }
                }
            }
            catch (NumberFormatException localNumberFormatException)
            {
                m++;
            }
        }
    }

    public void addFramerateController(FramerateController paramFramerateController)
    {
        this.mFramerateControllers.add(paramFramerateController);
    }

    protected ElementGroup createElementGroup(Element paramElement, ScreenContext paramScreenContext)
        throws ScreenElementLoadException
    {
        return new ElementGroup(paramElement, paramScreenContext, this);
    }

    public FramerateTokenList.FramerateToken createFramerateToken(String paramString)
    {
        return this.mContext.createToken(paramString);
    }

    public void doRender(Canvas paramCanvas)
    {
        if (this.mElementGroup != null)
            this.mElementGroup.doRender(paramCanvas);
        this.mFrames = (1 + this.mFrames);
        doneRender();
    }

    public void doneRender()
    {
        this.mContext.doneRender();
    }

    public VariableBinder findBinder(String paramString)
    {
        if (this.mVariableBinderManager != null);
        for (VariableBinder localVariableBinder = this.mVariableBinderManager.findBinder(paramString); ; localVariableBinder = null)
            return localVariableBinder;
    }

    public ScreenElement findElement(String paramString)
    {
        if ("__root".equals(paramString))
            return this;
        if (this.mElementGroup != null);
        for (ScreenElement localScreenElement = this.mElementGroup.findElement(paramString); ; localScreenElement = null)
        {
            this = localScreenElement;
            break;
        }
    }

    public Task findTask(String paramString)
    {
        return null;
    }

    /** @deprecated */
    public void finish()
    {
        try
        {
            boolean bool = this.mFinished;
            if (bool);
            while (true)
            {
                return;
                if (this.mElementGroup != null)
                {
                    this.mElementGroup.finish();
                    this.mElementGroup = null;
                }
                if (this.mVariableBinderManager != null)
                {
                    this.mVariableBinderManager.finish();
                    this.mVariableBinderManager = null;
                }
                if (this.mExternalCommandManager != null)
                {
                    this.mExternalCommandManager.finish();
                    this.mExternalCommandManager = null;
                }
                if (this.mVariableUpdaterManager != null)
                {
                    this.mVariableUpdaterManager.finish();
                    this.mVariableUpdaterManager = null;
                }
                this.mContext.mResourceManager.clear();
                if (this.mSoundManager != null)
                {
                    this.mSoundManager.release();
                    this.mSoundManager = null;
                }
                this.mFinished = true;
            }
        }
        finally
        {
        }
    }

    public ScreenContext getContext()
    {
        return this.mContext;
    }

    public int getDefaultScreenWidth()
    {
        return this.mDefaultScreenWidth;
    }

    public int getResourceDensity()
    {
        return this.mDefaultResourceDensity;
    }

    public float getScale()
    {
        if (this.mScale == 0.0F)
            Log.w("ScreenElementRoot", "scale not initialized!");
        for (float f = 1.0F; ; f = this.mScale)
            return f;
    }

    public int getTargetDensity()
    {
        return this.mTargetDensity;
    }

    public void haptic(int paramInt)
    {
    }

    public void init()
    {
        super.init();
        if (this.mVariableUpdaterManager != null)
            this.mVariableUpdaterManager.init();
        if (this.mVariableBinderManager != null)
            this.mVariableBinderManager.init();
        if (this.mExternalCommandManager != null)
            this.mExternalCommandManager.init();
        if (this.mElementGroup != null)
            this.mElementGroup.init();
        reset();
        requestFramerate(this.mFrameRate);
    }

    public boolean load()
    {
        while (true)
        {
            boolean bool;
            int i;
            int j;
            int k;
            try
            {
                Element localElement1 = this.mContext.mResourceManager.getManifestRoot();
                if (localElement1 == null)
                {
                    bool = false;
                }
                else
                {
                    LanguageHelper.load(this.mContext.mContext.getResources().getConfiguration().locale, this.mContext.mResourceManager, this.mContext.mVariables);
                    this.mNormalFrameRate = Utils.getAttrAsFloat(localElement1, "frameRate", this.DEFAULT_FRAME_RATE);
                    this.mFrameRate = this.mNormalFrameRate;
                    i = Utils.getAttrAsInt(localElement1, "screenWidth", 0);
                    if (i <= 0)
                        break label509;
                    this.mDefaultScreenWidth = i;
                    this.mDefaultResourceDensity = (240 * this.mDefaultScreenWidth / 480);
                    this.mContext.setResourceDensity(this.mDefaultResourceDensity);
                    this.mElementGroup = createElementGroup(localElement1, this.mContext);
                    this.mVariableBinderManager = new VariableBinderManager(Utils.getChild(localElement1, "VariableBinders"), this.mContext);
                    Element localElement2 = Utils.getChild(localElement1, "ExternalCommands");
                    if (localElement2 != null)
                        this.mExternalCommandManager = new ExternalCommandManager(localElement2, this.mContext, this);
                    Display localDisplay = ((WindowManager)this.mContext.mContext.getSystemService("window")).getDefaultDisplay();
                    j = localDisplay.getWidth();
                    k = localDisplay.getHeight();
                    int m = localDisplay.getRotation();
                    if (m == 1)
                        break label485;
                    if (m != 3)
                        break label517;
                    break label485;
                    label258: if (this.mTargetDensity == 0)
                    {
                        this.mScale = (i1 / this.mDefaultScreenWidth);
                        this.mTargetDensity = Math.round(this.mDefaultResourceDensity * this.mScale);
                        Log.i("ScreenElementRoot", "init target density: " + this.mTargetDensity);
                        this.mContext.setTargetDensity(this.mTargetDensity);
                        Utils.putVariableNumber("raw_screen_width", this.mContext.mVariables, Double.valueOf(i1));
                        Utils.putVariableNumber("raw_screen_height", this.mContext.mVariables, Double.valueOf(i2));
                        Utils.putVariableNumber("screen_width", this.mContext.mVariables, Double.valueOf(i1 / this.mScale));
                        Utils.putVariableNumber("screen_height", this.mContext.mVariables, Double.valueOf(i2 / this.mScale));
                        resolveResource(localElement1, i1);
                        processUseVariableUpdater(localElement1);
                        bool = onLoad(localElement1);
                    }
                    else
                    {
                        this.mScale = (this.mTargetDensity / this.mDefaultResourceDensity);
                        continue;
                    }
                }
            }
            catch (ScreenElementLoadException localScreenElementLoadException)
            {
                localScreenElementLoadException.printStackTrace();
                bool = false;
            }
            catch (Exception localException)
            {
                localException.printStackTrace();
                continue;
            }
            return bool;
            label485: int n = 1;
            label488: if (n != 0);
            for (int i1 = k; ; i1 = j)
            {
                if (n == 0)
                    break label530;
                i2 = j;
                break label258;
                label509: i = 480;
                break;
                label517: n = 0;
                break label488;
            }
            label530: int i2 = k;
        }
    }

    public boolean needDisallowInterceptTouchEvent()
    {
        return this.mNeedDisallowInterceptTouchEvent;
    }

    protected void onAddVariableUpdater(VariableUpdaterManager paramVariableUpdaterManager)
    {
        paramVariableUpdaterManager.add(new DateTimeVariableUpdater(paramVariableUpdaterManager));
    }

    public void onButtonInteractive(ButtonScreenElement paramButtonScreenElement, ButtonScreenElement.ButtonAction paramButtonAction)
    {
    }

    public void onCommand(String paramString)
    {
        if (this.mExternalCommandManager != null);
        try
        {
            this.mExternalCommandManager.onCommand(paramString);
            return;
        }
        catch (Exception localException)
        {
            while (true)
            {
                Log.e("ScreenElementRoot", localException.toString());
                localException.printStackTrace();
            }
        }
    }

    protected boolean onLoad(Element paramElement)
    {
        return true;
    }

    public boolean onTouch(MotionEvent paramMotionEvent)
    {
        boolean bool = false;
        if (this.mElementGroup == null)
            return bool;
        float f1 = descale(paramMotionEvent.getX());
        float f2 = descale(paramMotionEvent.getY());
        this.mTouchX.set(f1);
        this.mTouchY.set(f2);
        switch (paramMotionEvent.getActionMasked())
        {
        case 2:
        default:
        case 0:
        case 1:
        }
        while (true)
        {
            bool = this.mElementGroup.onTouch(paramMotionEvent);
            requestUpdate();
            break;
            this.mTouchBeginX.set(f1);
            this.mTouchBeginY.set(f2);
            this.mTouchBeginTime.set(System.currentTimeMillis());
            this.mNeedDisallowInterceptTouchEvent = false;
            continue;
            this.mNeedDisallowInterceptTouchEvent = false;
        }
    }

    public void pause()
    {
        super.pause();
        if (this.mElementGroup != null)
            this.mElementGroup.pause();
        if (this.mVariableBinderManager != null)
            this.mVariableBinderManager.pause();
        if (this.mExternalCommandManager != null)
            this.mExternalCommandManager.pause();
        if (this.mVariableUpdaterManager != null)
            this.mVariableUpdaterManager.pause();
    }

    public void playSound(String paramString)
    {
        if (TextUtils.isEmpty(paramString));
        while (true)
        {
            return;
            if (shouldPlaySound())
            {
                if (this.mSoundManager == null)
                    this.mSoundManager = new SoundManager(this.mContext.mContext, this.mContext.mResourceManager);
                this.mSoundManager.playSound(paramString, true);
            }
        }
    }

    public void reset(long paramLong)
    {
        super.reset(paramLong);
        if (this.mElementGroup != null)
            this.mElementGroup.reset(paramLong);
    }

    public void resume()
    {
        super.resume();
        if (this.mElementGroup != null)
            this.mElementGroup.resume();
        if (this.mVariableBinderManager != null)
            this.mVariableBinderManager.resume();
        if (this.mExternalCommandManager != null)
            this.mExternalCommandManager.resume();
        if (this.mVariableUpdaterManager != null)
            this.mVariableUpdaterManager.resume();
        requestUpdate();
    }

    public void setDefaultFramerate(float paramFloat)
    {
        this.DEFAULT_FRAME_RATE = paramFloat;
    }

    public void setRenderController(RendererController paramRendererController)
    {
        this.mContext.setRenderController(paramRendererController);
    }

    public void setTargetDensity(int paramInt)
    {
        this.mTargetDensity = paramInt;
        this.mContext.setTargetDensity(paramInt);
    }

    protected boolean shouldPlaySound()
    {
        return true;
    }

    public boolean shouldUpdate()
    {
        return this.mContext.shouldUpdate();
    }

    public void tick(long paramLong)
    {
        this.mVariableUpdaterManager.tick(paramLong);
        if (this.mElementGroup != null)
            this.mElementGroup.tick(paramLong);
        Double localDouble = this.mNeedDisallowInterceptTouchEventVar.get();
        if (localDouble != null)
            if (localDouble.doubleValue() <= 0.0D)
                break label54;
        label54: for (boolean bool = true; ; bool = false)
        {
            this.mNeedDisallowInterceptTouchEvent = bool;
            return;
        }
    }

    public void updateFramerate(long paramLong)
    {
        Iterator localIterator = this.mFramerateControllers.iterator();
        while (localIterator.hasNext())
            ((FramerateController)localIterator.next()).updateFramerate(paramLong);
        if (this.mFrameRateVar == null)
        {
            this.mFrameRateVar = new IndexedNumberVariable("frame_rate", this.mContext.mVariables);
            this.mCheckPoint = 0L;
        }
        if (this.mCheckPoint == 0L)
            this.mCheckPoint = paramLong;
        while (true)
        {
            return;
            long l1 = paramLong - this.mCheckPoint;
            if (l1 >= 1000L)
            {
                long l2 = 1000 * this.mFrames / l1;
                this.mFrameRateVar.set(l2);
                this.mFrames = 0;
                this.mCheckPoint = paramLong;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.ScreenElementRoot
 * JD-Core Version:        0.6.2
 */