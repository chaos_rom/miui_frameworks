package miui.app.screenelement;

import android.content.Context;
import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import miui.app.screenelement.util.Utils;

public class MiAdvancedView extends View
    implements RendererController.Listener
{
    private static final String LOG_TAG = "MiAdvancedView";
    private static final String VARIABLE_VIEW_HEIGHT = "view_height";
    private static final String VARIABLE_VIEW_WIDTH = "view_width";
    private boolean mLoggedHardwareRender;
    private boolean mNeedDisallowInterceptTouchEvent;
    private boolean mPaused = true;
    private RendererController mRendererController;
    protected ScreenElementRoot mRoot;
    private RenderThread mThread;
    private boolean mUseExternalRenderThread;

    public MiAdvancedView(Context paramContext, ScreenElementRoot paramScreenElementRoot)
    {
        super(paramContext);
        setFocusable(true);
        setFocusableInTouchMode(true);
        this.mRoot = paramScreenElementRoot;
        this.mRendererController = new RendererController(this);
        this.mRoot.setRenderController(this.mRendererController);
    }

    public MiAdvancedView(Context paramContext, ScreenElementRoot paramScreenElementRoot, RenderThread paramRenderThread)
    {
        this(paramContext, paramScreenElementRoot);
        if (paramRenderThread != null)
        {
            this.mRendererController.init();
            this.mUseExternalRenderThread = true;
            this.mThread = paramRenderThread;
            this.mThread.addRendererController(this.mRendererController);
        }
    }

    public void cleanUp()
    {
        setOnTouchListener(null);
        if (this.mThread != null)
        {
            if (this.mUseExternalRenderThread)
                break label27;
            this.mThread.setStop();
        }
        while (true)
        {
            return;
            label27: this.mThread.removeRendererController(this.mRendererController);
            this.mRendererController.finish();
        }
    }

    public void doRender()
    {
        postInvalidate();
    }

    public void finish()
    {
        this.mRoot.finish();
    }

    public final ScreenElementRoot getRoot()
    {
        return this.mRoot;
    }

    public void init()
    {
        this.mRoot.init();
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        if ((!this.mUseExternalRenderThread) && (this.mThread == null))
        {
            this.mThread = new RenderThread(this.mRendererController);
            this.mThread.setPaused(this.mPaused);
            this.mThread.start();
        }
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
    }

    protected void onDraw(Canvas paramCanvas)
    {
        if ((this.mThread == null) || (!this.mThread.isStarted()));
        while (true)
        {
            return;
            if (!this.mLoggedHardwareRender)
            {
                Log.d("MiAdvancedView", "canvas hardware render: " + paramCanvas.isHardwareAccelerated());
                this.mLoggedHardwareRender = true;
            }
            try
            {
                synchronized (this.mRoot)
                {
                    this.mRoot.render(paramCanvas);
                }
            }
            catch (Exception localException)
            {
                localException.printStackTrace();
                Log.e("MiAdvancedView", localException.toString());
            }
            catch (OutOfMemoryError localOutOfMemoryError)
            {
                localOutOfMemoryError.printStackTrace();
                Log.e("MiAdvancedView", localOutOfMemoryError.toString());
            }
        }
    }

    protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        Utils.putVariableNumber("view_width", this.mRoot.getContext().mVariables, Double.valueOf(Double.valueOf(paramInt3 - paramInt1).doubleValue() / this.mRoot.getScale()));
        Utils.putVariableNumber("view_height", this.mRoot.getContext().mVariables, Double.valueOf(Double.valueOf(paramInt4 - paramInt2).doubleValue() / this.mRoot.getScale()));
    }

    public void onPause()
    {
        this.mPaused = true;
        if (this.mThread != null)
        {
            if (this.mUseExternalRenderThread)
                break label28;
            this.mThread.setPaused(true);
        }
        while (true)
        {
            return;
            label28: this.mRendererController.selfPause();
        }
    }

    public void onResume()
    {
        this.mPaused = false;
        if (this.mThread != null)
        {
            if (this.mUseExternalRenderThread)
                break label28;
            this.mThread.setPaused(false);
        }
        while (true)
        {
            return;
            label28: this.mRendererController.selfResume();
        }
    }

    public boolean onTouchEvent(MotionEvent paramMotionEvent)
    {
        if (this.mRoot != null)
        {
            if (paramMotionEvent.getPointerCount() > 1)
                paramMotionEvent.setAction(4);
            boolean bool2 = this.mRoot.needDisallowInterceptTouchEvent();
            if (this.mNeedDisallowInterceptTouchEvent != bool2)
            {
                getParent().requestDisallowInterceptTouchEvent(bool2);
                this.mNeedDisallowInterceptTouchEvent = bool2;
            }
        }
        boolean bool1;
        try
        {
            synchronized (this.mRoot)
            {
                bool1 = this.mRoot.onTouch(paramMotionEvent);
            }
        }
        catch (Exception localException)
        {
            localException.printStackTrace();
            Log.e("MiAdvancedView", localException.toString());
            bool1 = false;
        }
        catch (OutOfMemoryError localOutOfMemoryError)
        {
            while (true)
            {
                localOutOfMemoryError.printStackTrace();
                Log.e("MiAdvancedView", localOutOfMemoryError.toString());
            }
        }
        return bool1;
    }

    public void pause()
    {
        this.mRoot.pause();
    }

    public void resume()
    {
        this.mRoot.resume();
    }

    public void setVisibility(int paramInt)
    {
        super.setVisibility(paramInt);
        if (paramInt == 0)
            onResume();
        while (true)
        {
            return;
            if ((paramInt == 4) || (paramInt == 8))
                onPause();
        }
    }

    public void tick(long paramLong)
    {
        synchronized (this.mRoot)
        {
            this.mRoot.tick(paramLong);
            return;
        }
    }

    public void updateFramerate(long paramLong)
    {
        this.mRoot.updateFramerate(paramLong);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.MiAdvancedView
 * JD-Core Version:        0.6.2
 */