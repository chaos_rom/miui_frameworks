package miui.app.screenelement;

import android.bluetooth.BluetoothAdapter;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.DetailedState;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.storage.StorageManager;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;
import miui.app.screenelement.data.Expression;
import miui.app.screenelement.data.VariableBinder;
import miui.app.screenelement.elements.ScreenElement;
import miui.app.screenelement.util.IndexedNumberVariable;
import miui.app.screenelement.util.IndexedStringVariable;
import miui.app.screenelement.util.Task;
import miui.app.screenelement.util.Utils;
import miui.app.screenelement.util.Variable;
import org.w3c.dom.Element;

public abstract class ActionCommand
{
    private static final String COMMAND_BLUETOOTH = "Bluetooth";
    private static final String COMMAND_DATA = "Data";
    private static final String COMMAND_RING_MODE = "RingMode";
    private static final String COMMAND_USB_STORAGE = "UsbStorage";
    private static final String COMMAND_WIFI = "Wifi";
    private static final String LOG_TAG = "ActionCommand";
    private static final int STATE_DISABLED = 0;
    private static final int STATE_ENABLED = 1;
    private static final int STATE_INTERMEDIATE = 5;
    private static final int STATE_TURNING_OFF = 3;
    private static final int STATE_TURNING_ON = 2;
    private static final int STATE_UNKNOWN = 4;
    public static final String TAG_NAME = "Command";
    private static final Handler mHandler = new Handler();
    protected ScreenContext mContext;

    public ActionCommand(ScreenContext paramScreenContext)
    {
        this.mContext = paramScreenContext;
    }

    protected static ActionCommand create(ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot, String paramString1, String paramString2)
    {
        Object localObject = null;
        if ((TextUtils.isEmpty(paramString1)) || (TextUtils.isEmpty(paramString2)));
        while (true)
        {
            return localObject;
            Variable localVariable = new Variable(paramString1);
            if (localVariable.getObjName() != null)
            {
                localObject = PropertyCommand.create(paramScreenContext, paramScreenElementRoot, paramString1, paramString2);
            }
            else
            {
                String str = localVariable.getPropertyName();
                if ("RingMode".equals(str))
                    localObject = new RingModeCommand(paramScreenContext, paramString2);
                else if ("Wifi".equals(str))
                    localObject = new WifiSwitchCommand(paramScreenContext, paramString2);
                else if ("Data".equals(str))
                    localObject = new DataSwitchCommand(paramScreenContext, paramString2);
                else if ("Bluetooth".equals(str))
                    localObject = new BluetoothSwitchCommand(paramScreenContext, paramString2);
                else if ("UsbStorage".equals(str))
                    localObject = new UsbStorageSwitchCommand(paramScreenContext, paramString2);
            }
        }
    }

    public static ActionCommand create(ScreenContext paramScreenContext, Element paramElement, ScreenElementRoot paramScreenElementRoot)
    {
        Object localObject2;
        if (paramElement == null)
        {
            localObject2 = null;
            return localObject2;
        }
        Expression localExpression = Expression.build(paramElement.getAttribute("condition"));
        long l = Utils.getAttrAsLong(paramElement, "delay", 0L);
        String str = paramElement.getNodeName();
        Object localObject1;
        if (str.equals("Command"))
            localObject1 = create(paramScreenContext, paramScreenElementRoot, paramElement.getAttribute("target"), paramElement.getAttribute("value"));
        while (true)
        {
            if ((l > 0L) && (localObject1 != null))
                localObject1 = new DelayCommand((ActionCommand)localObject1, l);
            if ((localExpression != null) && (localObject1 != null))
            {
                localObject2 = new ConditionCommand((ActionCommand)localObject1, localExpression);
                break;
                if (str.equals("VariableCommand"))
                {
                    localObject1 = new VariableAssignmentCommand(paramScreenContext, paramElement);
                    continue;
                }
                if (str.equals("BinderCommand"))
                {
                    localObject1 = new VariableBinderCommand(paramScreenContext, paramScreenElementRoot, paramElement);
                    continue;
                }
                if (!str.equals("IntentCommand"))
                    break label202;
                localObject1 = new IntentCommand(paramScreenContext, paramScreenElementRoot, paramElement);
                continue;
            }
            localObject2 = localObject1;
            break;
            label202: localObject1 = null;
        }
    }

    protected abstract void doPerform();

    public void finish()
    {
    }

    protected ScreenContext getContext()
    {
        return this.mContext;
    }

    public void init()
    {
    }

    public void pause()
    {
    }

    public void perform()
    {
        doPerform();
        this.mContext.requestUpdate();
    }

    public void resume()
    {
    }

    private static class AnimationProperty extends ActionCommand.PropertyCommand
    {
        public static final String PROPERTY_NAME = "animation";
        private boolean mIsPlay;

        protected AnimationProperty(ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot, Variable paramVariable, String paramString)
        {
            super(paramScreenElementRoot, paramVariable, paramString);
            if (paramString.equalsIgnoreCase("play"))
                this.mIsPlay = true;
        }

        public void doPerform()
        {
            if (this.mIsPlay)
                this.mTargetElement.reset();
        }
    }

    private static class VisibilityProperty extends ActionCommand.PropertyCommand
    {
        public static final String PROPERTY_NAME = "visibility";
        private boolean mIsShow;
        private boolean mIsToggle;

        protected VisibilityProperty(ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot, Variable paramVariable, String paramString)
        {
            super(paramScreenElementRoot, paramVariable, paramString);
            if (paramString.equalsIgnoreCase("toggle"))
                this.mIsToggle = true;
            while (true)
            {
                return;
                if (paramString.equalsIgnoreCase("true"))
                    this.mIsShow = true;
                else if (paramString.equalsIgnoreCase("false"))
                    this.mIsShow = false;
            }
        }

        public void doPerform()
        {
            boolean bool;
            if (this.mIsToggle)
            {
                ScreenElement localScreenElement = this.mTargetElement;
                if (!this.mTargetElement.isVisible())
                {
                    bool = true;
                    localScreenElement.show(bool);
                }
            }
            while (true)
            {
                return;
                bool = false;
                break;
                this.mTargetElement.show(this.mIsShow);
            }
        }
    }

    public static abstract class PropertyCommand extends ActionCommand
    {
        protected ScreenElementRoot mRoot;
        protected ScreenElement mTargetElement;
        private Variable mTargetObj;

        protected PropertyCommand(ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot, Variable paramVariable, String paramString)
        {
            super();
            this.mRoot = paramScreenElementRoot;
            this.mTargetObj = paramVariable;
        }

        public static PropertyCommand create(ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot, String paramString1, String paramString2)
        {
            Variable localVariable = new Variable(paramString1);
            Object localObject;
            if ("visibility".equals(localVariable.getPropertyName()))
                localObject = new ActionCommand.VisibilityProperty(paramScreenContext, paramScreenElementRoot, localVariable, paramString2);
            while (true)
            {
                return localObject;
                if ("animation".equals(localVariable.getPropertyName()))
                    localObject = new ActionCommand.AnimationProperty(paramScreenContext, paramScreenElementRoot, localVariable, paramString2);
                else
                    localObject = null;
            }
        }

        public void perform()
        {
            if (this.mTargetObj == null);
            while (true)
            {
                return;
                if (this.mTargetElement == null)
                {
                    this.mTargetElement = this.mRoot.findElement(this.mTargetObj.getObjName());
                    if (this.mTargetElement == null)
                    {
                        Log.w("ActionCommand", "could not find PropertyCommand target, name: " + this.mTargetObj.getObjName());
                        this.mTargetObj = null;
                    }
                }
                else
                {
                    doPerform();
                }
            }
        }
    }

    private static class DelayCommand extends ActionCommand
    {
        private ActionCommand mCommand;
        private long mDelay;

        public DelayCommand(ActionCommand paramActionCommand, long paramLong)
        {
            super();
            this.mCommand = paramActionCommand;
            this.mDelay = paramLong;
        }

        protected void doPerform()
        {
            this.mContext.mHandler.postDelayed(new Runnable()
            {
                public void run()
                {
                    ActionCommand.this.perform();
                }
            }
            , this.mDelay);
        }

        public void init()
        {
            this.mCommand.init();
        }
    }

    private static class ConditionCommand extends ActionCommand
    {
        private ActionCommand mCommand;
        private Expression mCondition;

        public ConditionCommand(ActionCommand paramActionCommand, Expression paramExpression)
        {
            super();
            this.mCommand = paramActionCommand;
            this.mCondition = paramExpression;
        }

        protected void doPerform()
        {
            if (this.mCondition.evaluate(this.mContext.mVariables) > 0.0D)
                this.mCommand.perform();
        }

        public void init()
        {
            this.mCommand.init();
        }
    }

    private static class IntentCommand extends ActionCommand
    {
        public static final String TAG_NAME = "IntentCommand";
        private Intent mIntent;
        private ScreenElementRoot mRoot;
        private Task mTask;

        public IntentCommand(ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot, Element paramElement)
        {
            super();
            this.mRoot = paramScreenElementRoot;
            this.mTask = Task.load(paramElement);
        }

        protected void doPerform()
        {
            if (this.mIntent != null);
            try
            {
                this.mContext.mContext.startActivity(this.mIntent);
                return;
            }
            catch (ActivityNotFoundException localActivityNotFoundException)
            {
                while (true)
                {
                    localActivityNotFoundException.printStackTrace();
                    Log.e("ActionCommand", localActivityNotFoundException.toString());
                }
            }
        }

        public void init()
        {
            Task localTask = this.mRoot.findTask(this.mTask.id);
            if ((localTask != null) && (!TextUtils.isEmpty(localTask.action)))
                this.mTask = localTask;
            if (!TextUtils.isEmpty(this.mTask.action))
            {
                this.mIntent = new Intent(this.mTask.action);
                if (!TextUtils.isEmpty(this.mTask.type))
                    this.mIntent.setType(this.mTask.type);
                if (!TextUtils.isEmpty(this.mTask.category))
                    this.mIntent.addCategory(this.mTask.category);
                if ((!TextUtils.isEmpty(this.mTask.packageName)) && (!TextUtils.isEmpty(this.mTask.className)))
                    this.mIntent.setComponent(new ComponentName(this.mTask.packageName, this.mTask.className));
                this.mIntent.setFlags(872415232);
            }
        }
    }

    private static class VariableBinderCommand extends ActionCommand
    {
        public static final String TAG_NAME = "BinderCommand";
        private VariableBinder mBinder;
        private Command mCommand = Command.Invalid;
        private String mName;
        private ScreenElementRoot mRoot;

        public VariableBinderCommand(ScreenContext paramScreenContext, ScreenElementRoot paramScreenElementRoot, Element paramElement)
        {
            super();
            this.mRoot = paramScreenElementRoot;
            this.mName = paramElement.getAttribute("name");
            if (paramElement.getAttribute("command").equals("refresh"))
                this.mCommand = Command.Refresh;
        }

        protected void doPerform()
        {
            if (this.mBinder != null)
                switch (ActionCommand.1.$SwitchMap$miui$app$screenelement$ActionCommand$VariableBinderCommand$Command[this.mCommand.ordinal()])
                {
                default:
                case 1:
                }
            while (true)
            {
                return;
                this.mBinder.refresh();
            }
        }

        public void init()
        {
            this.mBinder = this.mRoot.findBinder(this.mName);
        }

        private static enum Command
        {
            static
            {
                Invalid = new Command("Invalid", 1);
                Command[] arrayOfCommand = new Command[2];
                arrayOfCommand[0] = Refresh;
                arrayOfCommand[1] = Invalid;
            }
        }
    }

    private static class VariableAssignmentCommand extends ActionCommand
    {
        public static final String TAG_NAME = "VariableCommand";
        private Expression mExpression;
        private IndexedNumberVariable mNumVariable;
        private IndexedStringVariable mStrVariable;

        public VariableAssignmentCommand(ScreenContext paramScreenContext, Element paramElement)
        {
            super();
            String str1 = paramElement.getAttribute("name");
            String str2 = paramElement.getAttribute("expression");
            String str3 = paramElement.getAttribute("type");
            Variable localVariable = new Variable(str1);
            if (str3.equals("string"))
                this.mStrVariable = new IndexedStringVariable(localVariable.getObjName(), localVariable.getPropertyName(), paramScreenContext.mVariables);
            while (true)
            {
                this.mExpression = Expression.build(str2);
                if (this.mExpression == null)
                    Log.e("ActionCommand", "invalid expression in VariableAssignmentCommand");
                return;
                this.mNumVariable = new IndexedNumberVariable(localVariable.getObjName(), localVariable.getPropertyName(), paramScreenContext.mVariables);
            }
        }

        protected void doPerform()
        {
            if (this.mNumVariable != null)
                this.mNumVariable.set(this.mExpression.evaluate(this.mContext.mVariables));
            while (true)
            {
                return;
                if (this.mStrVariable != null)
                    this.mStrVariable.set(this.mExpression.evaluateStr(this.mContext.mVariables));
            }
        }
    }

    private static class UsbStorageSwitchCommand extends ActionCommand.NotificationReceiver
    {
        private boolean mConnected;
        private ActionCommand.OnOffCommandHelper mOnOffHelper;
        private StorageManager mStorageManager;

        public UsbStorageSwitchCommand(ScreenContext paramScreenContext, String paramString)
        {
            super("usb_mode", NotifierManager.NotifierType.UsbState);
            this.mOnOffHelper = new ActionCommand.OnOffCommandHelper(paramString);
        }

        protected void doPerform()
        {
            if (this.mStorageManager == null)
                return;
            boolean bool1 = this.mStorageManager.isUsbMassStorageEnabled();
            final boolean bool2;
            if (this.mOnOffHelper.mIsToggle)
                if (!bool1)
                    bool2 = true;
            while (true)
            {
                updateState(3);
                new Thread()
                {
                    public void run()
                    {
                        ActionCommand.UsbStorageSwitchCommand localUsbStorageSwitchCommand;
                        if (bool2)
                        {
                            ActionCommand.UsbStorageSwitchCommand.this.mStorageManager.enableUsbMassStorage();
                            localUsbStorageSwitchCommand = ActionCommand.UsbStorageSwitchCommand.this;
                            if (!bool2)
                                break label50;
                        }
                        label50: for (int i = 2; ; i = 1)
                        {
                            localUsbStorageSwitchCommand.updateState(i);
                            return;
                            ActionCommand.UsbStorageSwitchCommand.this.mStorageManager.disableUsbMassStorage();
                            break;
                        }
                    }
                }
                .start();
                break;
                bool2 = false;
                continue;
                if (this.mOnOffHelper.mIsOn == bool1)
                    break;
                bool2 = this.mOnOffHelper.mIsOn;
            }
        }

        public void onNotify(Context paramContext, Intent paramIntent, Object paramObject)
        {
            this.mConnected = paramIntent.getExtras().getBoolean("connected");
            super.onNotify(paramContext, paramIntent, paramObject);
        }

        protected void update()
        {
            if (this.mStorageManager == null)
            {
                this.mStorageManager = ((StorageManager)this.mContext.mContext.getSystemService("storage"));
                if (this.mStorageManager == null)
                {
                    Log.w("ActionCommand", "Failed to get StorageManager");
                    return;
                }
            }
            boolean bool = this.mStorageManager.isUsbMassStorageEnabled();
            int i;
            if (this.mConnected)
                if (bool)
                    i = 2;
            while (true)
            {
                updateState(i);
                break;
                i = 1;
                continue;
                i = 0;
            }
        }
    }

    private static class BluetoothSwitchCommand extends ActionCommand.NotificationReceiver
    {
        private BluetoothAdapter mBluetoothAdapter;
        private boolean mBluetoothEnable;
        private boolean mBluetoothEnabling;
        private ActionCommand.OnOffCommandHelper mOnOffHelper;

        public BluetoothSwitchCommand(ScreenContext paramScreenContext, String paramString)
        {
            super("bluetooth_state", NotifierManager.NotifierType.Bluetooth);
            this.mOnOffHelper = new ActionCommand.OnOffCommandHelper(paramString);
        }

        private boolean ensureBluetoothAdapter()
        {
            if (this.mBluetoothAdapter == null)
                this.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (this.mBluetoothAdapter != null);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        protected void doPerform()
        {
            if (!ensureBluetoothAdapter())
                return;
            if (this.mOnOffHelper.mIsToggle)
                if (this.mBluetoothEnable)
                {
                    this.mBluetoothAdapter.disable();
                    this.mBluetoothEnabling = false;
                }
            while (true)
            {
                update();
                break;
                this.mBluetoothAdapter.enable();
                this.mBluetoothEnabling = true;
                continue;
                if ((!this.mBluetoothEnabling) && (this.mBluetoothEnable != this.mOnOffHelper.mIsOn))
                    if (this.mOnOffHelper.mIsOn)
                    {
                        this.mBluetoothAdapter.enable();
                        this.mBluetoothEnabling = true;
                    }
                    else
                    {
                        this.mBluetoothAdapter.disable();
                        this.mBluetoothEnabling = false;
                    }
            }
        }

        protected void update()
        {
            int i = 0;
            if (!ensureBluetoothAdapter());
            while (true)
            {
                return;
                this.mBluetoothEnable = this.mBluetoothAdapter.isEnabled();
                if (this.mBluetoothEnable)
                {
                    this.mBluetoothEnabling = false;
                    updateState(1);
                }
                else
                {
                    if (this.mBluetoothEnabling)
                        i = 2;
                    updateState(i);
                }
            }
        }
    }

    private static class DataSwitchCommand extends ActionCommand.NotificationReceiver
    {
        private boolean mApnEnable;
        private ConnectivityManager mCm;
        private ActionCommand.OnOffCommandHelper mOnOffHelper;

        public DataSwitchCommand(ScreenContext paramScreenContext, String paramString)
        {
            super("data_state", NotifierManager.NotifierType.MobileData);
            this.mOnOffHelper = new ActionCommand.OnOffCommandHelper(paramString);
        }

        private boolean ensureConnectivityManager()
        {
            if (this.mCm == null)
                this.mCm = ((ConnectivityManager)this.mContext.mContext.getSystemService("connectivity"));
            if (this.mCm != null);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        protected void doPerform()
        {
            if (!ensureConnectivityManager());
            label65: 
            while (true)
            {
                return;
                boolean bool;
                if (this.mOnOffHelper.mIsToggle)
                    if (!this.mApnEnable)
                        bool = true;
                while (true)
                {
                    if (this.mApnEnable == bool)
                        break label65;
                    this.mCm.setMobileDataEnabled(bool);
                    break;
                    bool = false;
                    continue;
                    bool = this.mOnOffHelper.mIsOn;
                }
            }
        }

        protected void update()
        {
            if (!ensureConnectivityManager())
                return;
            this.mApnEnable = this.mCm.getMobileDataEnabled();
            if (this.mApnEnable);
            for (int i = 1; ; i = 0)
            {
                updateState(i);
                break;
            }
        }
    }

    private static class WifiSwitchCommand extends ActionCommand.NotificationReceiver
    {
        private ActionCommand.OnOffCommandHelper mOnOffHelper;
        private final ActionCommand.StateTracker mWifiState = new ActionCommand.WifiStateTracker(null);

        public WifiSwitchCommand(ScreenContext paramScreenContext, String paramString)
        {
            super("wifi_state", NotifierManager.NotifierType.WifiState);
            update();
            this.mOnOffHelper = new ActionCommand.OnOffCommandHelper(paramString);
        }

        protected void doPerform()
        {
            if (this.mOnOffHelper.mIsToggle)
                this.mWifiState.toggleState(this.mContext.mContext);
            label124: 
            while (true)
            {
                update();
                return;
                int i = 0;
                switch (this.mWifiState.getTriState(this.mContext.mContext))
                {
                default:
                case 0:
                case 1:
                }
                while (true)
                {
                    if (i == 0)
                        break label124;
                    this.mWifiState.requestStateChange(this.mContext.mContext, this.mOnOffHelper.mIsOn);
                    break;
                    if (this.mOnOffHelper.mIsOn)
                    {
                        i = 1;
                        continue;
                        if (!this.mOnOffHelper.mIsOn)
                            i = 1;
                    }
                }
            }
        }

        public void onNotify(Context paramContext, Intent paramIntent, Object paramObject)
        {
            this.mWifiState.onActualStateChange(paramContext, paramIntent);
            super.onNotify(paramContext, paramIntent, paramObject);
        }

        protected void update()
        {
            int i = 0;
            switch (this.mWifiState.getTriState(this.mContext.mContext))
            {
            case 2:
            case 3:
            case 4:
            default:
            case 0:
            case 1:
            case 5:
            }
            while (true)
            {
                return;
                updateState(0);
                continue;
                if (((ActionCommand.WifiStateTracker)this.mWifiState).zConnected);
                for (int j = 1; ; j = 2)
                {
                    updateState(j);
                    break;
                }
                if (this.mWifiState.isTurningOn())
                    i = 3;
                updateState(i);
            }
        }
    }

    private static final class WifiStateTracker extends ActionCommand.StateTracker
    {
        private static final int MAX_SCAN_ATTEMPT = 3;
        public boolean zConnected = false;
        private int zScanAttempt = 0;

        private static int wifiStateToFiveState(int paramInt)
        {
            int i;
            switch (paramInt)
            {
            default:
                i = 4;
            case 1:
            case 3:
            case 0:
            case 2:
            }
            while (true)
            {
                return i;
                i = 0;
                continue;
                i = 1;
                continue;
                i = 3;
                continue;
                i = 2;
            }
        }

        public int getActualState(Context paramContext)
        {
            WifiManager localWifiManager = (WifiManager)paramContext.getSystemService("wifi");
            if (localWifiManager != null);
            for (int i = wifiStateToFiveState(localWifiManager.getWifiState()); ; i = 4)
                return i;
        }

        public void onActualStateChange(Context paramContext, Intent paramIntent)
        {
            boolean bool = false;
            if ("android.net.wifi.WIFI_STATE_CHANGED".equals(paramIntent.getAction()))
            {
                int j = paramIntent.getIntExtra("wifi_state", -1);
                setCurrentState(paramContext, wifiStateToFiveState(j));
                if (3 == j)
                {
                    this.zConnected = true;
                    this.zScanAttempt = 0;
                }
            }
            while (true)
            {
                return;
                if ("android.net.wifi.SCAN_RESULTS".equals(paramIntent.getAction()))
                {
                    if (this.zScanAttempt < 3)
                    {
                        int i = 1 + this.zScanAttempt;
                        this.zScanAttempt = i;
                        if (i == 3)
                            this.zConnected = false;
                    }
                }
                else if ("android.net.wifi.STATE_CHANGE".equals(paramIntent.getAction()))
                {
                    this.zScanAttempt = 3;
                    NetworkInfo.DetailedState localDetailedState = ((NetworkInfo)paramIntent.getParcelableExtra("networkInfo")).getDetailedState();
                    if ((NetworkInfo.DetailedState.SCANNING == localDetailedState) || (NetworkInfo.DetailedState.CONNECTING == localDetailedState) || (NetworkInfo.DetailedState.AUTHENTICATING == localDetailedState) || (NetworkInfo.DetailedState.OBTAINING_IPADDR == localDetailedState) || (NetworkInfo.DetailedState.CONNECTED == localDetailedState))
                        bool = true;
                    this.zConnected = bool;
                }
            }
        }

        protected void requestStateChange(Context paramContext, final boolean paramBoolean)
        {
            final WifiManager localWifiManager = (WifiManager)paramContext.getSystemService("wifi");
            if (localWifiManager == null)
                Log.d("ActionCommand", "No wifiManager.");
            while (true)
            {
                return;
                new AsyncTask()
                {
                    protected Void doInBackground(Void[] paramAnonymousArrayOfVoid)
                    {
                        int i = localWifiManager.getWifiApState();
                        if ((paramBoolean) && ((i == 12) || (i == 13)))
                            localWifiManager.setWifiApEnabled(null, false);
                        localWifiManager.setWifiEnabled(paramBoolean);
                        return null;
                    }
                }
                .execute(new Void[0]);
            }
        }
    }

    public static abstract class StateTracker
    {
        private Boolean mActualState = null;
        private boolean mDeferredStateChangeRequestNeeded = false;
        private boolean mInTransition = false;
        private Boolean mIntendedState = null;

        public abstract int getActualState(Context paramContext);

        public final int getTriState(Context paramContext)
        {
            int i = 5;
            if (this.mInTransition);
            while (true)
            {
                return i;
                switch (getActualState(paramContext))
                {
                default:
                    break;
                case 0:
                    i = 0;
                    break;
                case 1:
                    i = 1;
                }
            }
        }

        public final boolean isTurningOn()
        {
            if ((this.mIntendedState != null) && (this.mIntendedState.booleanValue()));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public abstract void onActualStateChange(Context paramContext, Intent paramIntent);

        protected abstract void requestStateChange(Context paramContext, boolean paramBoolean);

        protected final void setCurrentState(Context paramContext, int paramInt)
        {
            boolean bool = this.mInTransition;
            switch (paramInt)
            {
            default:
                if ((bool) && (!this.mInTransition) && (this.mDeferredStateChangeRequestNeeded))
                {
                    Log.v("ActionCommand", "processing deferred state change");
                    if ((this.mActualState == null) || (this.mIntendedState == null) || (!this.mIntendedState.equals(this.mActualState)))
                        break label168;
                    Log.v("ActionCommand", "... but intended state matches, so no changes.");
                }
                break;
            case 0:
            case 1:
            case 2:
            case 3:
            }
            while (true)
            {
                this.mDeferredStateChangeRequestNeeded = false;
                return;
                this.mInTransition = false;
                this.mActualState = Boolean.valueOf(false);
                break;
                this.mInTransition = false;
                this.mActualState = Boolean.valueOf(true);
                break;
                this.mInTransition = true;
                this.mActualState = Boolean.valueOf(false);
                break;
                this.mInTransition = true;
                this.mActualState = Boolean.valueOf(true);
                break;
                label168: if (this.mIntendedState != null)
                {
                    this.mInTransition = true;
                    requestStateChange(paramContext, this.mIntendedState.booleanValue());
                }
            }
        }

        public final void toggleState(Context paramContext)
        {
            int i = getTriState(paramContext);
            boolean bool = false;
            switch (i)
            {
            case 2:
            case 3:
            case 4:
            default:
                this.mIntendedState = Boolean.valueOf(bool);
                if (this.mInTransition)
                    this.mDeferredStateChangeRequestNeeded = true;
                break;
            case 1:
            case 0:
            case 5:
            }
            while (true)
            {
                return;
                bool = false;
                break;
                bool = true;
                break;
                if (this.mIntendedState == null)
                    break;
                if (!this.mIntendedState.booleanValue());
                for (bool = true; ; bool = false)
                    break;
                this.mInTransition = true;
                requestStateChange(paramContext, bool);
            }
        }
    }

    private static class OnOffCommandHelper
    {
        protected boolean mIsOn;
        protected boolean mIsToggle;

        public OnOffCommandHelper(String paramString)
        {
            if (paramString.equalsIgnoreCase("toggle"))
                this.mIsToggle = true;
            while (true)
            {
                return;
                if (paramString.equalsIgnoreCase("on"))
                    this.mIsOn = true;
                else if (paramString.equalsIgnoreCase("off"))
                    this.mIsOn = false;
            }
        }
    }

    private static class RingModeCommand extends ActionCommand.NotificationReceiver
    {
        private AudioManager mAudioManager;
        private ActionCommand.ModeToggleHelper mToggleHelper = new ActionCommand.ModeToggleHelper(null);

        public RingModeCommand(ScreenContext paramScreenContext, String paramString)
        {
            super("ring_mode", NotifierManager.NotifierType.RingMode);
            this.mToggleHelper.addMode("normal", 2);
            this.mToggleHelper.addMode("silent", 0);
            this.mToggleHelper.addMode("vibrate", 1);
            if (!this.mToggleHelper.build(paramString))
                Log.e("ActionCommand", "invalid ring mode command value: " + paramString);
        }

        protected void doPerform()
        {
            if (this.mAudioManager == null);
            while (true)
            {
                return;
                this.mToggleHelper.click();
                int i = this.mToggleHelper.getModeId();
                this.mAudioManager.setRingerMode(i);
                updateState(i);
            }
        }

        protected void update()
        {
            if ((this.mAudioManager == null) && (this.mContext != null))
                this.mAudioManager = ((AudioManager)this.mContext.mContext.getSystemService("audio"));
            if (this.mAudioManager == null);
            while (true)
            {
                return;
                updateState(this.mAudioManager.getRingerMode());
            }
        }
    }

    private static class ModeToggleHelper
    {
        private int mCurModeIndex;
        private int mCurToggleIndex;
        private ArrayList<Integer> mModeIds = new ArrayList();
        private ArrayList<String> mModeNames = new ArrayList();
        private boolean mToggle;
        private boolean mToggleAll;
        private ArrayList<Integer> mToggleModes = new ArrayList();

        private int findMode(String paramString)
        {
            int i = 0;
            if (i < this.mModeNames.size())
                if (!((String)this.mModeNames.get(i)).equals(paramString));
            while (true)
            {
                return i;
                i++;
                break;
                i = -1;
            }
        }

        public void addMode(String paramString, int paramInt)
        {
            this.mModeNames.add(paramString);
            this.mModeIds.add(Integer.valueOf(paramInt));
        }

        public boolean build(String paramString)
        {
            boolean bool = true;
            int i = findMode(paramString);
            if (i >= 0)
                this.mCurModeIndex = i;
            while (true)
            {
                return bool;
                if ("toggle".equals(paramString))
                {
                    this.mToggleAll = bool;
                }
                else
                {
                    String[] arrayOfString = paramString.split(",");
                    for (int j = 0; ; j++)
                    {
                        if (j >= arrayOfString.length)
                            break label95;
                        int k = findMode(arrayOfString[j]);
                        if (k < 0)
                        {
                            bool = false;
                            break;
                        }
                        this.mToggleModes.add(Integer.valueOf(k));
                    }
                    label95: this.mToggle = bool;
                }
            }
        }

        public void click()
        {
            if (this.mToggle)
            {
                int j = 1 + this.mCurToggleIndex;
                this.mCurToggleIndex = j;
                this.mCurToggleIndex = (j % this.mToggleModes.size());
                this.mCurModeIndex = ((Integer)this.mToggleModes.get(this.mCurToggleIndex)).intValue();
            }
            while (true)
            {
                return;
                if (this.mToggleAll)
                {
                    int i = 1 + this.mCurModeIndex;
                    this.mCurModeIndex = i;
                    this.mCurModeIndex = (i % this.mModeNames.size());
                }
            }
        }

        public int getModeId()
        {
            return ((Integer)this.mModeIds.get(this.mCurModeIndex)).intValue();
        }

        public String getModeName()
        {
            return (String)this.mModeNames.get(this.mCurModeIndex);
        }
    }

    private static abstract class NotificationReceiver extends ActionCommand.StatefulActionCommand
        implements NotifierManager.OnNotifyListener
    {
        private NotifierManager mNotifierManager;
        private NotifierManager.NotifierType mType;

        public NotificationReceiver(ScreenContext paramScreenContext, String paramString, NotifierManager.NotifierType paramNotifierType)
        {
            super(paramString);
            this.mType = paramNotifierType;
            this.mNotifierManager = NotifierManager.getInstance(this.mContext.getRawContext());
        }

        protected void asyncUpdate()
        {
            ActionCommand.mHandler.post(new Runnable()
            {
                public void run()
                {
                    ActionCommand.NotificationReceiver.this.update();
                }
            });
        }

        public void finish()
        {
            NotifierManager.getInstance(this.mContext.getRawContext()).releaseNotifier(this.mType, this);
        }

        public void init()
        {
            update();
            this.mNotifierManager.acquireNotifier(this.mType, this);
        }

        public void onNotify(Context paramContext, Intent paramIntent, Object paramObject)
        {
            asyncUpdate();
        }

        public void pause()
        {
            this.mNotifierManager.pause(this.mType, this);
        }

        public void resume()
        {
            update();
            this.mNotifierManager.resume(this.mType, this);
        }

        protected abstract void update();
    }

    private static abstract class StatefulActionCommand extends ActionCommand
    {
        private IndexedNumberVariable mVar = new IndexedNumberVariable(paramString, this.mContext.mVariables);

        public StatefulActionCommand(ScreenContext paramScreenContext, String paramString)
        {
            super();
        }

        protected final void updateState(int paramInt)
        {
            if (this.mVar == null);
            while (true)
            {
                return;
                this.mVar.set(paramInt);
                this.mContext.requestUpdate();
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.ActionCommand
 * JD-Core Version:        0.6.2
 */