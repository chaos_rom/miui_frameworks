package miui.app.screenelement;

import android.util.Log;

public class RendererController
{
    private static final String LOG_TAG = "RendererController";
    private float mCurFramerate;
    private int mFrameTime = 2147483647;
    private FramerateTokenList mFramerateTokenList = new FramerateTokenList();
    private long mLastUpdateSystemTime;
    private Listener mListener;
    private Object mLock = new Object();
    private boolean mPaused;
    private boolean mPendingRender;
    private RenderThread mRenderThread;
    private boolean mSelfPaused;
    private boolean mShouldUpdate;

    public RendererController(Listener paramListener)
    {
        this.mListener = paramListener;
    }

    public FramerateTokenList.FramerateToken createToken(String paramString)
    {
        return this.mFramerateTokenList.createToken(paramString);
    }

    public void doRender()
    {
        this.mPendingRender = true;
        this.mListener.doRender();
    }

    public void doneRender()
    {
        this.mPendingRender = false;
    }

    public void finish()
    {
        try
        {
            this.mListener.finish();
            return;
        }
        catch (Exception localException)
        {
            while (true)
            {
                localException.printStackTrace();
                Log.e("RendererController", localException.toString());
            }
        }
    }

    public float getCurFramerate()
    {
        return this.mCurFramerate;
    }

    public int getFrameTime()
    {
        return this.mFrameTime;
    }

    public float getFramerate()
    {
        return this.mFramerateTokenList.getFramerate();
    }

    public long getLastUpdateTime()
    {
        return this.mLastUpdateSystemTime;
    }

    public void init()
    {
        try
        {
            this.mListener.init();
            return;
        }
        catch (Exception localException)
        {
            while (true)
            {
                localException.printStackTrace();
                Log.e("RendererController", localException.toString());
            }
        }
    }

    public boolean isSelfPaused()
    {
        return this.mSelfPaused;
    }

    public void pause()
    {
        synchronized (this.mLock)
        {
            this.mPaused = true;
            if (!this.mSelfPaused)
                this.mListener.pause();
            return;
        }
    }

    public boolean pendingRender()
    {
        return this.mPendingRender;
    }

    public void requestUpdate()
    {
        this.mShouldUpdate = true;
    }

    public void resume()
    {
        synchronized (this.mLock)
        {
            this.mPaused = false;
            if (!this.mSelfPaused)
                this.mListener.resume();
            return;
        }
    }

    public void selfPause()
    {
        synchronized (this.mLock)
        {
            this.mSelfPaused = true;
            if (!this.mPaused)
                this.mListener.pause();
            return;
        }
    }

    public void selfResume()
    {
        synchronized (this.mLock)
        {
            this.mSelfPaused = false;
            if (!this.mPaused)
                this.mListener.resume();
            if (this.mRenderThread != null)
                this.mRenderThread.setPaused(false);
            return;
        }
    }

    public void setCurFramerate(float paramFloat)
    {
        this.mCurFramerate = paramFloat;
    }

    public void setFrameTime(int paramInt)
    {
        this.mFrameTime = paramInt;
    }

    public void setLastUpdateTime(long paramLong)
    {
        this.mLastUpdateSystemTime = paramLong;
    }

    public void setRenderThread(RenderThread paramRenderThread)
    {
        this.mRenderThread = paramRenderThread;
    }

    public boolean shouldUpdate()
    {
        return this.mShouldUpdate;
    }

    public void tick(long paramLong)
    {
        this.mShouldUpdate = false;
        this.mListener.tick(paramLong);
    }

    public void updateFramerate(long paramLong)
    {
        this.mListener.updateFramerate(paramLong);
    }

    public static abstract interface Listener
    {
        public abstract void doRender();

        public abstract void finish();

        public abstract void init();

        public abstract void pause();

        public abstract void resume();

        public abstract void tick(long paramLong);

        public abstract void updateFramerate(long paramLong);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.RendererController
 * JD-Core Version:        0.6.2
 */