package miui.app.screenelement;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Iterator;
import miui.app.screenelement.elements.ButtonScreenElement.ButtonAction;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class CommandTrigger
{
    public static final String TAG_NAME = "Trigger";
    private ButtonScreenElement.ButtonAction mAction = ButtonScreenElement.ButtonAction.Other;
    private String mActionString;
    private ArrayList<ActionCommand> mCommands = new ArrayList();
    private ScreenContext mContext;
    private ActionCommand.PropertyCommand mPropertyCommand;
    protected ScreenElementRoot mRoot;

    public CommandTrigger(ScreenContext paramScreenContext, Element paramElement, ScreenElementRoot paramScreenElementRoot)
        throws ScreenElementLoadException
    {
        this.mContext = paramScreenContext;
        this.mRoot = paramScreenElementRoot;
        load(paramElement, paramScreenElementRoot);
    }

    private void load(Element paramElement, ScreenElementRoot paramScreenElementRoot)
        throws ScreenElementLoadException
    {
        if (paramElement != null)
        {
            String str1 = paramElement.getAttribute("target");
            String str2 = paramElement.getAttribute("property");
            String str3 = paramElement.getAttribute("value");
            if ((!TextUtils.isEmpty(str2)) && (!TextUtils.isEmpty(str1)) && (!TextUtils.isEmpty(str3)))
                this.mPropertyCommand = ActionCommand.PropertyCommand.create(this.mContext, paramScreenElementRoot, str1 + "." + str2, str3);
            String str4 = paramElement.getAttribute("action");
            this.mActionString = str4;
            if (!TextUtils.isEmpty(str4))
            {
                if (!str4.equalsIgnoreCase("down"))
                    break label223;
                this.mAction = ButtonScreenElement.ButtonAction.Down;
            }
            while (true)
            {
                NodeList localNodeList = paramElement.getChildNodes();
                for (int i = 0; i < localNodeList.getLength(); i++)
                    if (localNodeList.item(i).getNodeType() == 1)
                    {
                        Element localElement = (Element)localNodeList.item(i);
                        ActionCommand localActionCommand = ActionCommand.create(this.mContext, localElement, paramScreenElementRoot);
                        if (localActionCommand != null)
                            this.mCommands.add(localActionCommand);
                    }
                label223: if (str4.equalsIgnoreCase("up"))
                    this.mAction = ButtonScreenElement.ButtonAction.Up;
                else if (str4.equalsIgnoreCase("double"))
                    this.mAction = ButtonScreenElement.ButtonAction.Double;
                else if (str4.equalsIgnoreCase("long"))
                    this.mAction = ButtonScreenElement.ButtonAction.Long;
                else if (str4.equalsIgnoreCase("cancel"))
                    this.mAction = ButtonScreenElement.ButtonAction.Cancel;
                else
                    this.mAction = ButtonScreenElement.ButtonAction.Other;
            }
        }
    }

    public void finish()
    {
        Iterator localIterator = this.mCommands.iterator();
        while (localIterator.hasNext())
            ((ActionCommand)localIterator.next()).finish();
    }

    public ButtonScreenElement.ButtonAction getAction()
    {
        return this.mAction;
    }

    public String getActionString()
    {
        return this.mActionString;
    }

    public void init()
    {
        Iterator localIterator = this.mCommands.iterator();
        while (localIterator.hasNext())
            ((ActionCommand)localIterator.next()).init();
    }

    public void pause()
    {
        Iterator localIterator = this.mCommands.iterator();
        while (localIterator.hasNext())
            ((ActionCommand)localIterator.next()).pause();
    }

    public void perform()
    {
        if (this.mPropertyCommand != null)
            this.mPropertyCommand.perform();
        Iterator localIterator = this.mCommands.iterator();
        while (localIterator.hasNext())
            ((ActionCommand)localIterator.next()).perform();
    }

    public void resume()
    {
        Iterator localIterator = this.mCommands.iterator();
        while (localIterator.hasNext())
            ((ActionCommand)localIterator.next()).resume();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.CommandTrigger
 * JD-Core Version:        0.6.2
 */