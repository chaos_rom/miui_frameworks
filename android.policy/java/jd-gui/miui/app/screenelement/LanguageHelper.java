package miui.app.screenelement;

import android.os.MemoryFile;
import android.util.Log;
import java.util.Locale;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import miui.app.screenelement.data.Variables;
import miui.app.screenelement.util.IndexedStringVariable;
import miui.app.screenelement.util.Utils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class LanguageHelper
{
    private static final String DEFAULT_STRING_FILE_PATH = "strings/strings.xml";
    private static final String LOG_TAG = "LanguageHelper";
    private static final String STRING_FILE_PATH = "strings/strings.xml";
    private static final String STRING_ROOT_TAG = "strings";
    private static final String STRING_TAG = "string";

    public static boolean load(Locale paramLocale, ResourceManager paramResourceManager, Variables paramVariables)
    {
        boolean bool = false;
        MemoryFile localMemoryFile = null;
        if (paramLocale != null)
        {
            localMemoryFile = paramResourceManager.getFile(Utils.addFileNameSuffix("strings/strings.xml", paramLocale.toString()));
            if (localMemoryFile == null)
                localMemoryFile = paramResourceManager.getFile(Utils.addFileNameSuffix("strings/strings.xml", paramLocale.getLanguage()));
        }
        if (localMemoryFile == null)
        {
            localMemoryFile = paramResourceManager.getFile("strings/strings.xml");
            if (localMemoryFile == null)
                Log.w("LanguageHelper", "no available string resources to load.");
        }
        while (true)
        {
            return bool;
            DocumentBuilderFactory localDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
            try
            {
                Document localDocument = localDocumentBuilderFactory.newDocumentBuilder().parse(localMemoryFile.getInputStream());
                bool = setVariables(localDocument, paramVariables);
            }
            catch (Exception localException)
            {
                Log.e("LanguageHelper", localException.getMessage());
            }
        }
    }

    private static boolean setVariables(Document paramDocument, Variables paramVariables)
    {
        boolean bool = false;
        NodeList localNodeList1 = paramDocument.getElementsByTagName("strings");
        if (localNodeList1.getLength() <= 0);
        while (true)
        {
            return bool;
            NodeList localNodeList2 = ((Element)localNodeList1.item(0)).getElementsByTagName("string");
            for (int i = 0; i < localNodeList2.getLength(); i++)
            {
                Element localElement = (Element)localNodeList2.item(i);
                new IndexedStringVariable(localElement.getAttribute("name"), paramVariables).set(localElement.getAttribute("value"));
            }
            bool = true;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.LanguageHelper
 * JD-Core Version:        0.6.2
 */