package miui.app.screenelement;

public class ScreenElementLoadException extends Exception
{
    private static final long serialVersionUID = 1L;

    public ScreenElementLoadException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.ScreenElementLoadException
 * JD-Core Version:        0.6.2
 */