package miui.app.screenelement;

import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

public class FramerateTokenList
{
    private static final String LOG_TAG = "FramerateTokenList";
    private float mCurFramerate;
    private ArrayList<FramerateToken> mList = new ArrayList();

    private void onChange()
    {
        float f = 0.0F;
        synchronized (this.mList)
        {
            Iterator localIterator = this.mList.iterator();
            while (localIterator.hasNext())
            {
                FramerateToken localFramerateToken = (FramerateToken)localIterator.next();
                if (localFramerateToken.mFramerate > f)
                    f = localFramerateToken.mFramerate;
            }
            this.mCurFramerate = f;
            return;
        }
    }

    public FramerateToken createToken(String paramString)
    {
        Log.d("FramerateTokenList", "createToken: " + paramString);
        FramerateToken localFramerateToken = new FramerateToken(paramString);
        synchronized (this.mList)
        {
            this.mList.add(localFramerateToken);
            return localFramerateToken;
        }
    }

    public float getFramerate()
    {
        return this.mCurFramerate;
    }

    public class FramerateToken
    {
        public float mFramerate;
        public String mName;

        public FramerateToken(String arg2)
        {
            Object localObject;
            this.mName = localObject;
        }

        public float getFramerate()
        {
            return this.mFramerate;
        }

        public void requestFramerate(float paramFloat)
        {
            if (this.mFramerate != paramFloat)
            {
                Log.d("FramerateTokenList", "requestFramerate: " + paramFloat + " by:" + this.mName);
                this.mFramerate = paramFloat;
                FramerateTokenList.this.onChange();
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.FramerateTokenList
 * JD-Core Version:        0.6.2
 */