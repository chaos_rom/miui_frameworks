package miui.app.screenelement;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;

public final class NotifierManager
{
    private static boolean DBG = false;
    private static final String LOG_TAG = "NotifierManager";
    private static NotifierManager sInstance;
    private Context mContext;
    private HashMap<NotifierType, BaseNotifier> mNotifiers = new HashMap();

    private NotifierManager(Context paramContext)
    {
        this.mContext = paramContext;
    }

    private static BaseNotifier createNotifier(NotifierType paramNotifierType, Context paramContext)
    {
        Object localObject;
        switch (1.$SwitchMap$miui$app$screenelement$NotifierManager$NotifierType[paramNotifierType.ordinal()])
        {
        default:
            localObject = null;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        }
        while (true)
        {
            return localObject;
            localObject = new BatteryNotifier(paramContext);
            continue;
            localObject = new UsbStateNotifier(paramContext);
            continue;
            localObject = new BluetoothNotifier(paramContext);
            continue;
            localObject = new RingModeNotifier(paramContext);
            continue;
            localObject = new MobileDataNotifier(paramContext);
            continue;
            localObject = new WifiNotifier(paramContext);
            continue;
            localObject = new VolumeChangedNotifier(paramContext);
        }
    }

    /** @deprecated */
    public static NotifierManager getInstance(Context paramContext)
    {
        try
        {
            if (sInstance == null)
                sInstance = new NotifierManager(paramContext);
            NotifierManager localNotifierManager = sInstance;
            return localNotifierManager;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public void acquireNotifier(NotifierType paramNotifierType, OnNotifyListener paramOnNotifyListener)
    {
        try
        {
            if (DBG)
                Log.i("NotifierManager", "acquireNotifier:" + paramNotifierType.toString() + "    " + paramOnNotifyListener.toString());
            Object localObject2 = (BaseNotifier)this.mNotifiers.get(paramNotifierType);
            if (localObject2 == null)
            {
                BaseNotifier localBaseNotifier = createNotifier(paramNotifierType, this.mContext);
                localObject2 = localBaseNotifier;
                if (localObject2 != null);
            }
            while (true)
            {
                return;
                ((BaseNotifier)localObject2).init();
                this.mNotifiers.put(paramNotifierType, localObject2);
                ((BaseNotifier)localObject2).addListener(paramOnNotifyListener);
                ((BaseNotifier)localObject2).addRef();
                ((BaseNotifier)localObject2).addActiveRef();
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void pause(NotifierType paramNotifierType, OnNotifyListener paramOnNotifyListener)
    {
        try
        {
            BaseNotifier localBaseNotifier = (BaseNotifier)this.mNotifiers.get(paramNotifierType);
            if (localBaseNotifier == null);
            while (true)
            {
                return;
                localBaseNotifier.removeListener(paramOnNotifyListener);
                if (localBaseNotifier.releaseActiveRef() == 0)
                    localBaseNotifier.pause();
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void releaseNotifier(NotifierType paramNotifierType, OnNotifyListener paramOnNotifyListener)
    {
        try
        {
            BaseNotifier localBaseNotifier = (BaseNotifier)this.mNotifiers.get(paramNotifierType);
            if (localBaseNotifier == null);
            while (true)
            {
                return;
                localBaseNotifier.releaseActiveRef();
                localBaseNotifier.removeListener(paramOnNotifyListener);
                if (localBaseNotifier.releaseRef() == 0)
                {
                    localBaseNotifier.finish();
                    this.mNotifiers.remove(paramNotifierType);
                }
            }
        }
        finally
        {
        }
    }

    /** @deprecated */
    public void resume(NotifierType paramNotifierType, OnNotifyListener paramOnNotifyListener)
    {
        try
        {
            BaseNotifier localBaseNotifier = (BaseNotifier)this.mNotifiers.get(paramNotifierType);
            if (localBaseNotifier == null);
            while (true)
            {
                return;
                localBaseNotifier.addListener(paramOnNotifyListener);
                if (localBaseNotifier.addActiveRef() == 1)
                    localBaseNotifier.resume();
            }
        }
        finally
        {
        }
    }

    public static class VolumeChangedNotifier extends NotifierManager.BroadcastNotifier
    {
        public VolumeChangedNotifier(Context paramContext)
        {
            super();
        }

        protected String getIntentAction()
        {
            return "android.media.VOLUME_CHANGED_ACTION";
        }
    }

    public static class RingModeNotifier extends NotifierManager.BroadcastNotifier
    {
        public RingModeNotifier(Context paramContext)
        {
            super();
        }

        protected String getIntentAction()
        {
            return "android.media.RINGER_MODE_CHANGED";
        }
    }

    public static class WifiNotifier extends NotifierManager.BroadcastNotifier
    {
        public WifiNotifier(Context paramContext)
        {
            super();
        }

        protected IntentFilter createIntentFilter()
        {
            IntentFilter localIntentFilter = new IntentFilter();
            localIntentFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
            localIntentFilter.addAction("android.net.wifi.SCAN_RESULTS");
            localIntentFilter.addAction("android.net.wifi.STATE_CHANGE");
            return localIntentFilter;
        }
    }

    public static class MobileDataNotifier extends NotifierManager.BaseNotifier
    {
        private final ContentObserver mMobileDataEnableObserver = new ContentObserver(null)
        {
            public void onChange(boolean paramAnonymousBoolean)
            {
                if (NotifierManager.DBG)
                    Log.i("NotifierManager", "onNotify: " + NotifierManager.MobileDataNotifier.this.toString());
                NotifierManager.MobileDataNotifier.this.onNotify(null, null, Boolean.valueOf(paramAnonymousBoolean));
            }
        };

        public MobileDataNotifier(Context paramContext)
        {
            super();
        }

        protected void onRegister()
        {
            this.mContext.getContentResolver().registerContentObserver(Settings.Secure.getUriFor("mobile_data"), false, this.mMobileDataEnableObserver);
        }

        protected void onUnregister()
        {
            this.mContext.getContentResolver().unregisterContentObserver(this.mMobileDataEnableObserver);
        }
    }

    public static class BluetoothNotifier extends NotifierManager.BroadcastNotifier
    {
        public BluetoothNotifier(Context paramContext)
        {
            super();
        }

        protected String getIntentAction()
        {
            return "android.bluetooth.adapter.action.STATE_CHANGED";
        }
    }

    public static class UsbStateNotifier extends NotifierManager.BroadcastNotifier
    {
        public UsbStateNotifier(Context paramContext)
        {
            super();
        }

        protected String getIntentAction()
        {
            return "android.hardware.usb.action.USB_STATE";
        }
    }

    public static class BatteryNotifier extends NotifierManager.BroadcastNotifier
    {
        public BatteryNotifier(Context paramContext)
        {
            super();
        }

        protected String getIntentAction()
        {
            return "android.intent.action.BATTERY_CHANGED";
        }
    }

    public static abstract class BroadcastNotifier extends NotifierManager.BaseNotifier
    {
        private IntentFilter mIntentFilter;
        private final BroadcastReceiver mIntentReceiver = new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                if (NotifierManager.DBG)
                    Log.i("NotifierManager", "onNotify: " + NotifierManager.BroadcastNotifier.this.toString());
                NotifierManager.BroadcastNotifier.this.onNotify(paramAnonymousContext, paramAnonymousIntent, null);
            }
        };

        public BroadcastNotifier(Context paramContext)
        {
            super();
        }

        protected IntentFilter createIntentFilter()
        {
            String str = getIntentAction();
            if (str == null);
            for (IntentFilter localIntentFilter = null; ; localIntentFilter = new IntentFilter(str))
                return localIntentFilter;
        }

        protected String getIntentAction()
        {
            throw new IllegalStateException("no intent filter action");
        }

        protected void onRegister()
        {
            if (this.mIntentFilter == null)
                this.mIntentFilter = createIntentFilter();
            this.mContext.registerReceiver(this.mIntentReceiver, this.mIntentFilter);
        }

        protected void onUnregister()
        {
            this.mContext.unregisterReceiver(this.mIntentReceiver);
        }
    }

    public static abstract class BaseNotifier
    {
        private int mActiveReference;
        protected Context mContext;
        private ArrayList<NotifierManager.OnNotifyListener> mListeners = new ArrayList();
        private int mReference;
        private boolean mRegistered;

        public BaseNotifier(Context paramContext)
        {
            this.mContext = paramContext;
        }

        public final int addActiveRef()
        {
            int i = 1 + this.mActiveReference;
            this.mActiveReference = i;
            return i;
        }

        public final void addListener(NotifierManager.OnNotifyListener paramOnNotifyListener)
        {
            synchronized (this.mListeners)
            {
                this.mListeners.add(paramOnNotifyListener);
                return;
            }
        }

        public final int addRef()
        {
            int i = 1 + this.mReference;
            this.mReference = i;
            return i;
        }

        public void finish()
        {
            unregister();
        }

        public void init()
        {
            register();
        }

        // ERROR //
        protected void onNotify(Context paramContext, Intent paramIntent, Object paramObject)
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 27	miui/app/screenelement/NotifierManager$BaseNotifier:mListeners	Ljava/util/ArrayList;
            //     4: astore 4
            //     6: aload 4
            //     8: monitorenter
            //     9: aload_0
            //     10: getfield 27	miui/app/screenelement/NotifierManager$BaseNotifier:mListeners	Ljava/util/ArrayList;
            //     13: invokevirtual 56	java/util/ArrayList:iterator	()Ljava/util/Iterator;
            //     16: astore 6
            //     18: aload 6
            //     20: invokeinterface 62 1 0
            //     25: ifeq +32 -> 57
            //     28: aload 6
            //     30: invokeinterface 66 1 0
            //     35: checkcast 68	miui/app/screenelement/NotifierManager$OnNotifyListener
            //     38: aload_1
            //     39: aload_2
            //     40: aload_3
            //     41: invokeinterface 70 4 0
            //     46: goto -28 -> 18
            //     49: astore 5
            //     51: aload 4
            //     53: monitorexit
            //     54: aload 5
            //     56: athrow
            //     57: aload 4
            //     59: monitorexit
            //     60: return
            //
            // Exception table:
            //     from	to	target	type
            //     9	54	49	finally
            //     57	60	49	finally
        }

        protected abstract void onRegister();

        protected abstract void onUnregister();

        public void pause()
        {
            unregister();
        }

        protected void register()
        {
            if (this.mRegistered);
            while (true)
            {
                return;
                onRegister();
                this.mRegistered = true;
                if (NotifierManager.DBG)
                    Log.i("NotifierManager", "onRegister: " + toString());
            }
        }

        public final int releaseActiveRef()
        {
            int i;
            if (this.mActiveReference > 0)
            {
                i = -1 + this.mActiveReference;
                this.mActiveReference = i;
            }
            while (true)
            {
                return i;
                i = 0;
            }
        }

        public final int releaseRef()
        {
            int i;
            if (this.mReference > 0)
            {
                i = -1 + this.mReference;
                this.mReference = i;
            }
            while (true)
            {
                return i;
                i = 0;
            }
        }

        public final void removeListener(NotifierManager.OnNotifyListener paramOnNotifyListener)
        {
            synchronized (this.mListeners)
            {
                this.mListeners.remove(paramOnNotifyListener);
                return;
            }
        }

        public void resume()
        {
            register();
        }

        protected void unregister()
        {
            if (!this.mRegistered);
            while (true)
            {
                return;
                try
                {
                    onUnregister();
                    this.mRegistered = false;
                    if (!NotifierManager.DBG)
                        continue;
                    Log.i("NotifierManager", "onUnregister: " + toString());
                }
                catch (IllegalArgumentException localIllegalArgumentException)
                {
                    while (true)
                        Log.w("NotifierManager", localIllegalArgumentException.toString());
                }
            }
        }
    }

    public static abstract interface OnNotifyListener
    {
        public abstract void onNotify(Context paramContext, Intent paramIntent, Object paramObject);
    }

    public static enum NotifierType
    {
        static
        {
            Bluetooth = new NotifierType("Bluetooth", 2);
            WifiState = new NotifierType("WifiState", 3);
            RingMode = new NotifierType("RingMode", 4);
            MobileData = new NotifierType("MobileData", 5);
            VolumeChanged = new NotifierType("VolumeChanged", 6);
            NotifierType[] arrayOfNotifierType = new NotifierType[7];
            arrayOfNotifierType[0] = Battery;
            arrayOfNotifierType[1] = UsbState;
            arrayOfNotifierType[2] = Bluetooth;
            arrayOfNotifierType[3] = WifiState;
            arrayOfNotifierType[4] = RingMode;
            arrayOfNotifierType[5] = MobileData;
            arrayOfNotifierType[6] = VolumeChanged;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.NotifierManager
 * JD-Core Version:        0.6.2
 */