package miui.app.screenelement;

import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class LifecycleResourceManager extends ResourceManager
{
    private static final String LOG_TAG = "LifecycleResourceManager";
    private static long mLastCheckCacheTime;
    private long mCheckTime;
    private long mInactiveTime;

    public LifecycleResourceManager(ResourceLoader paramResourceLoader, long paramLong1, long paramLong2)
    {
        super(paramResourceLoader);
        this.mInactiveTime = paramLong1;
        this.mCheckTime = paramLong2;
    }

    public void checkCache()
    {
        long l = System.currentTimeMillis();
        if (l - mLastCheckCacheTime < this.mCheckTime);
        while (true)
        {
            return;
            Log.d("LifecycleResourceManager", "beging check cache... ");
            ArrayList localArrayList = new ArrayList();
            Iterator localIterator1 = this.mBitmaps.keySet().iterator();
            while (localIterator1.hasNext())
            {
                String str2 = (String)localIterator1.next();
                if (l - ((ResourceManager.BitmapInfo)this.mBitmaps.get(str2)).mLastVisitTime > this.mInactiveTime)
                    localArrayList.add(str2);
            }
            Iterator localIterator2 = localArrayList.iterator();
            while (localIterator2.hasNext())
            {
                String str1 = (String)localIterator2.next();
                Log.d("LifecycleResourceManager", "remove cache: " + str1);
                this.mBitmaps.remove(str1);
            }
            mLastCheckCacheTime = l;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.LifecycleResourceManager
 * JD-Core Version:        0.6.2
 */