package miui.app.screenelement;

public abstract interface IRendererController
{
    public abstract FramerateTokenList.FramerateToken createToken(String paramString);

    public abstract void doRender();

    public abstract void doneUpdate();

    public abstract void finish();

    public abstract float getFramerate();

    public abstract void init();

    public abstract void pause();

    public abstract void requestUpdate();

    public abstract void resume();

    public abstract boolean shouldUpdate();

    public abstract void updateFramerate(long paramLong);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/android.policy_dex2jar.jar
 * Qualified Name:         miui.app.screenelement.IRendererController
 * JD-Core Version:        0.6.2
 */