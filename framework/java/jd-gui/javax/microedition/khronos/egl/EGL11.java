package javax.microedition.khronos.egl;

public abstract interface EGL11 extends EGL10
{
    public static final int EGL_CONTEXT_LOST = 12302;
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         javax.microedition.khronos.egl.EGL11
 * JD-Core Version:        0.6.2
 */