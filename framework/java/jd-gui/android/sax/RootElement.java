package android.sax;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class RootElement extends Element
{
    final Handler handler = new Handler();

    public RootElement(String paramString)
    {
        this("", paramString);
    }

    public RootElement(String paramString1, String paramString2)
    {
        super(null, paramString1, paramString2, 0);
    }

    public ContentHandler getContentHandler()
    {
        return this.handler;
    }

    class Handler extends DefaultHandler
    {
        StringBuilder bodyBuilder = null;
        Element current = null;
        int depth = -1;
        Locator locator;

        Handler()
        {
        }

        public void characters(char[] paramArrayOfChar, int paramInt1, int paramInt2)
            throws SAXException
        {
            if (this.bodyBuilder != null)
                this.bodyBuilder.append(paramArrayOfChar, paramInt1, paramInt2);
        }

        public void endElement(String paramString1, String paramString2, String paramString3)
            throws SAXException
        {
            Element localElement = this.current;
            if (this.depth == localElement.depth)
            {
                localElement.checkRequiredChildren(this.locator);
                if (localElement.endElementListener != null)
                    localElement.endElementListener.end();
                if (this.bodyBuilder != null)
                {
                    String str = this.bodyBuilder.toString();
                    this.bodyBuilder = null;
                    localElement.endTextElementListener.end(str);
                }
                this.current = localElement.parent;
            }
            this.depth = (-1 + this.depth);
        }

        public void setDocumentLocator(Locator paramLocator)
        {
            this.locator = paramLocator;
        }

        void start(Element paramElement, Attributes paramAttributes)
        {
            this.current = paramElement;
            if (paramElement.startElementListener != null)
                paramElement.startElementListener.start(paramAttributes);
            if (paramElement.endTextElementListener != null)
                this.bodyBuilder = new StringBuilder();
            paramElement.resetRequiredChildren();
            paramElement.visited = true;
        }

        public void startElement(String paramString1, String paramString2, String paramString3, Attributes paramAttributes)
            throws SAXException
        {
            int i = 1 + this.depth;
            this.depth = i;
            if (i == 0)
                startRoot(paramString1, paramString2, paramAttributes);
            while (true)
            {
                return;
                if (this.bodyBuilder != null)
                    throw new BadXmlException("Encountered mixed content within text element named " + this.current + ".", this.locator);
                if (i == 1 + this.current.depth)
                {
                    Children localChildren = this.current.children;
                    if (localChildren != null)
                    {
                        Element localElement = localChildren.get(paramString1, paramString2);
                        if (localElement != null)
                            start(localElement, paramAttributes);
                    }
                }
            }
        }

        void startRoot(String paramString1, String paramString2, Attributes paramAttributes)
            throws SAXException
        {
            RootElement localRootElement = RootElement.this;
            if ((localRootElement.uri.compareTo(paramString1) != 0) || (localRootElement.localName.compareTo(paramString2) != 0))
                throw new BadXmlException("Root element name does not match. Expected: " + localRootElement + ", Got: " + Element.toString(paramString1, paramString2), this.locator);
            start(localRootElement, paramAttributes);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.sax.RootElement
 * JD-Core Version:        0.6.2
 */