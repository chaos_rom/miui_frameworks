package android.sax;

import org.xml.sax.Locator;
import org.xml.sax.SAXParseException;

class BadXmlException extends SAXParseException
{
    public BadXmlException(String paramString, Locator paramLocator)
    {
        super(paramString, paramLocator);
    }

    public String getMessage()
    {
        return "Line " + getLineNumber() + ": " + super.getMessage();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.sax.BadXmlException
 * JD-Core Version:        0.6.2
 */