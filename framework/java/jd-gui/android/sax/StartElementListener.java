package android.sax;

import org.xml.sax.Attributes;

public abstract interface StartElementListener
{
    public abstract void start(Attributes paramAttributes);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.sax.StartElementListener
 * JD-Core Version:        0.6.2
 */