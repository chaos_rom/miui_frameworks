package android.sax;

public abstract interface EndTextElementListener
{
    public abstract void end(String paramString);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.sax.EndTextElementListener
 * JD-Core Version:        0.6.2
 */