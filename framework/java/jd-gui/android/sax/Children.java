package android.sax;

class Children
{
    Child[] children = new Child[16];

    Element get(String paramString1, String paramString2)
    {
        Object localObject = null;
        int i = 31 * paramString1.hashCode() + paramString2.hashCode();
        int j = i & 0xF;
        Child localChild = this.children[j];
        if (localChild == null);
        while (true)
        {
            return localObject;
            do
            {
                if ((localChild.hash == i) && (localChild.uri.compareTo(paramString1) == 0) && (localChild.localName.compareTo(paramString2) == 0))
                {
                    localObject = localChild;
                    break;
                }
                localChild = localChild.next;
            }
            while (localChild != null);
        }
    }

    Element getOrCreate(Element paramElement, String paramString1, String paramString2)
    {
        int i = 31 * paramString1.hashCode() + paramString2.hashCode();
        int j = i & 0xF;
        Child localChild1 = this.children[j];
        Object localObject;
        if (localChild1 == null)
        {
            Child localChild2 = new Child(paramElement, paramString1, paramString2, 1 + paramElement.depth, i);
            this.children[j] = localChild2;
            localObject = localChild2;
        }
        while (true)
        {
            return localObject;
            Child localChild3;
            do
            {
                if ((localChild1.hash == i) && (localChild1.uri.compareTo(paramString1) == 0) && (localChild1.localName.compareTo(paramString2) == 0))
                {
                    localObject = localChild1;
                    break;
                }
                localChild3 = localChild1;
                localChild1 = localChild1.next;
            }
            while (localChild1 != null);
            Child localChild4 = new Child(paramElement, paramString1, paramString2, 1 + paramElement.depth, i);
            localChild3.next = localChild4;
            localObject = localChild4;
        }
    }

    static class Child extends Element
    {
        final int hash;
        Child next;

        Child(Element paramElement, String paramString1, String paramString2, int paramInt1, int paramInt2)
        {
            super(paramString1, paramString2, paramInt1);
            this.hash = paramInt2;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.sax.Children
 * JD-Core Version:        0.6.2
 */