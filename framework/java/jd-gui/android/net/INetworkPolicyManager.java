package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface INetworkPolicyManager extends IInterface
{
    public abstract int getAppPolicy(int paramInt)
        throws RemoteException;

    public abstract int[] getAppsWithPolicy(int paramInt)
        throws RemoteException;

    public abstract NetworkPolicy[] getNetworkPolicies()
        throws RemoteException;

    public abstract NetworkQuotaInfo getNetworkQuotaInfo(NetworkState paramNetworkState)
        throws RemoteException;

    public abstract boolean getRestrictBackground()
        throws RemoteException;

    public abstract boolean isNetworkMetered(NetworkState paramNetworkState)
        throws RemoteException;

    public abstract boolean isUidForeground(int paramInt)
        throws RemoteException;

    public abstract void registerListener(INetworkPolicyListener paramINetworkPolicyListener)
        throws RemoteException;

    public abstract void setAppPolicy(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void setNetworkPolicies(NetworkPolicy[] paramArrayOfNetworkPolicy)
        throws RemoteException;

    public abstract void setRestrictBackground(boolean paramBoolean)
        throws RemoteException;

    public abstract void snoozeLimit(NetworkTemplate paramNetworkTemplate)
        throws RemoteException;

    public abstract void unregisterListener(INetworkPolicyListener paramINetworkPolicyListener)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements INetworkPolicyManager
    {
        private static final String DESCRIPTOR = "android.net.INetworkPolicyManager";
        static final int TRANSACTION_getAppPolicy = 2;
        static final int TRANSACTION_getAppsWithPolicy = 3;
        static final int TRANSACTION_getNetworkPolicies = 8;
        static final int TRANSACTION_getNetworkQuotaInfo = 12;
        static final int TRANSACTION_getRestrictBackground = 11;
        static final int TRANSACTION_isNetworkMetered = 13;
        static final int TRANSACTION_isUidForeground = 4;
        static final int TRANSACTION_registerListener = 5;
        static final int TRANSACTION_setAppPolicy = 1;
        static final int TRANSACTION_setNetworkPolicies = 7;
        static final int TRANSACTION_setRestrictBackground = 10;
        static final int TRANSACTION_snoozeLimit = 9;
        static final int TRANSACTION_unregisterListener = 6;

        public Stub()
        {
            attachInterface(this, "android.net.INetworkPolicyManager");
        }

        public static INetworkPolicyManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.net.INetworkPolicyManager");
                if ((localIInterface != null) && ((localIInterface instanceof INetworkPolicyManager)))
                    localObject = (INetworkPolicyManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
                while (true)
                {
                    return j;
                    paramParcel2.writeString("android.net.INetworkPolicyManager");
                    continue;
                    paramParcel1.enforceInterface("android.net.INetworkPolicyManager");
                    setAppPolicy(paramParcel1.readInt(), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    continue;
                    paramParcel1.enforceInterface("android.net.INetworkPolicyManager");
                    int n = getAppPolicy(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(n);
                    continue;
                    paramParcel1.enforceInterface("android.net.INetworkPolicyManager");
                    int[] arrayOfInt = getAppsWithPolicy(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    paramParcel2.writeIntArray(arrayOfInt);
                    continue;
                    paramParcel1.enforceInterface("android.net.INetworkPolicyManager");
                    boolean bool3 = isUidForeground(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    if (bool3)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.net.INetworkPolicyManager");
                    registerListener(INetworkPolicyListener.Stub.asInterface(paramParcel1.readStrongBinder()));
                    paramParcel2.writeNoException();
                    continue;
                    paramParcel1.enforceInterface("android.net.INetworkPolicyManager");
                    unregisterListener(INetworkPolicyListener.Stub.asInterface(paramParcel1.readStrongBinder()));
                    paramParcel2.writeNoException();
                    continue;
                    paramParcel1.enforceInterface("android.net.INetworkPolicyManager");
                    setNetworkPolicies((NetworkPolicy[])paramParcel1.createTypedArray(NetworkPolicy.CREATOR));
                    paramParcel2.writeNoException();
                    continue;
                    paramParcel1.enforceInterface("android.net.INetworkPolicyManager");
                    NetworkPolicy[] arrayOfNetworkPolicy = getNetworkPolicies();
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedArray(arrayOfNetworkPolicy, j);
                    continue;
                    paramParcel1.enforceInterface("android.net.INetworkPolicyManager");
                    if (paramParcel1.readInt() != 0);
                    for (NetworkTemplate localNetworkTemplate = (NetworkTemplate)NetworkTemplate.CREATOR.createFromParcel(paramParcel1); ; localNetworkTemplate = null)
                    {
                        snoozeLimit(localNetworkTemplate);
                        paramParcel2.writeNoException();
                        break;
                    }
                    paramParcel1.enforceInterface("android.net.INetworkPolicyManager");
                    if (paramParcel1.readInt() != 0);
                    int m;
                    for (int k = j; ; m = 0)
                    {
                        setRestrictBackground(k);
                        paramParcel2.writeNoException();
                        break;
                    }
                    paramParcel1.enforceInterface("android.net.INetworkPolicyManager");
                    boolean bool2 = getRestrictBackground();
                    paramParcel2.writeNoException();
                    if (bool2)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.net.INetworkPolicyManager");
                    if (paramParcel1.readInt() != 0);
                    for (NetworkState localNetworkState2 = (NetworkState)NetworkState.CREATOR.createFromParcel(paramParcel1); ; localNetworkState2 = null)
                    {
                        NetworkQuotaInfo localNetworkQuotaInfo = getNetworkQuotaInfo(localNetworkState2);
                        paramParcel2.writeNoException();
                        if (localNetworkQuotaInfo == null)
                            break label557;
                        paramParcel2.writeInt(j);
                        localNetworkQuotaInfo.writeToParcel(paramParcel2, j);
                        break;
                    }
                    label557: paramParcel2.writeInt(0);
                }
            case 13:
            }
            paramParcel1.enforceInterface("android.net.INetworkPolicyManager");
            if (paramParcel1.readInt() != 0);
            for (NetworkState localNetworkState1 = (NetworkState)NetworkState.CREATOR.createFromParcel(paramParcel1); ; localNetworkState1 = null)
            {
                boolean bool1 = isNetworkMetered(localNetworkState1);
                paramParcel2.writeNoException();
                if (bool1)
                    i = j;
                paramParcel2.writeInt(i);
                break;
            }
        }

        private static class Proxy
            implements INetworkPolicyManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public int getAppPolicy(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int[] getAppsWithPolicy(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int[] arrayOfInt = localParcel2.createIntArray();
                    return arrayOfInt;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.net.INetworkPolicyManager";
            }

            public NetworkPolicy[] getNetworkPolicies()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    NetworkPolicy[] arrayOfNetworkPolicy = (NetworkPolicy[])localParcel2.createTypedArray(NetworkPolicy.CREATOR);
                    return arrayOfNetworkPolicy;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public NetworkQuotaInfo getNetworkQuotaInfo(NetworkState paramNetworkState)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
                        if (paramNetworkState != null)
                        {
                            localParcel1.writeInt(1);
                            paramNetworkState.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(12, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            if (localParcel2.readInt() != 0)
                            {
                                localNetworkQuotaInfo = (NetworkQuotaInfo)NetworkQuotaInfo.CREATOR.createFromParcel(localParcel2);
                                return localNetworkQuotaInfo;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    NetworkQuotaInfo localNetworkQuotaInfo = null;
                }
            }

            public boolean getRestrictBackground()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
                    this.mRemote.transact(11, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isNetworkMetered(NetworkState paramNetworkState)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
                        if (paramNetworkState != null)
                        {
                            localParcel1.writeInt(1);
                            paramNetworkState.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(13, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean isUidForeground(int paramInt)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void registerListener(INetworkPolicyListener paramINetworkPolicyListener)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
                    if (paramINetworkPolicyListener != null)
                    {
                        localIBinder = paramINetworkPolicyListener.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(5, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setAppPolicy(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setNetworkPolicies(NetworkPolicy[] paramArrayOfNetworkPolicy)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
                    localParcel1.writeTypedArray(paramArrayOfNetworkPolicy, 0);
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setRestrictBackground(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(10, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void snoozeLimit(NetworkTemplate paramNetworkTemplate)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
                    if (paramNetworkTemplate != null)
                    {
                        localParcel1.writeInt(1);
                        paramNetworkTemplate.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(9, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void unregisterListener(INetworkPolicyListener paramINetworkPolicyListener)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.INetworkPolicyManager");
                    if (paramINetworkPolicyListener != null)
                    {
                        localIBinder = paramINetworkPolicyListener.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(6, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.INetworkPolicyManager
 * JD-Core Version:        0.6.2
 */