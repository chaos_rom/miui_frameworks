package android.net;

import android.content.Context;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import org.apache.harmony.xnet.provider.jsse.FileClientSessionCache;
import org.apache.harmony.xnet.provider.jsse.SSLClientSessionCache;

public final class SSLSessionCache
{
    private static final String TAG = "SSLSessionCache";
    final SSLClientSessionCache mSessionCache;

    public SSLSessionCache(Context paramContext)
    {
        File localFile = paramContext.getDir("sslcache", 0);
        Object localObject = null;
        try
        {
            SSLClientSessionCache localSSLClientSessionCache = FileClientSessionCache.usingDirectory(localFile);
            localObject = localSSLClientSessionCache;
            this.mSessionCache = localObject;
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
                Log.w("SSLSessionCache", "Unable to create SSL session cache in " + localFile, localIOException);
        }
    }

    public SSLSessionCache(File paramFile)
        throws IOException
    {
        this.mSessionCache = FileClientSessionCache.usingDirectory(paramFile);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.SSLSessionCache
 * JD-Core Version:        0.6.2
 */