package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.android.internal.util.Objects;
import com.android.internal.util.Preconditions;

public class NetworkPolicy
    implements Parcelable, Comparable<NetworkPolicy>
{
    public static final Parcelable.Creator<NetworkPolicy> CREATOR = new Parcelable.Creator()
    {
        public NetworkPolicy createFromParcel(Parcel paramAnonymousParcel)
        {
            return new NetworkPolicy(paramAnonymousParcel);
        }

        public NetworkPolicy[] newArray(int paramAnonymousInt)
        {
            return new NetworkPolicy[paramAnonymousInt];
        }
    };
    public static final int CYCLE_NONE = -1;
    private static final long DEFAULT_MTU = 1500L;
    public static final long LIMIT_DISABLED = -1L;
    public static final long SNOOZE_NEVER = -1L;
    public static final long WARNING_DISABLED = -1L;
    public int cycleDay;
    public String cycleTimezone;
    public boolean inferred;
    public long lastLimitSnooze;
    public long lastWarningSnooze;
    public long limitBytes;
    public boolean metered;
    public final NetworkTemplate template;
    public long warningBytes;

    public NetworkPolicy(NetworkTemplate paramNetworkTemplate, int paramInt, String paramString, long paramLong1, long paramLong2, long paramLong3, long paramLong4, boolean paramBoolean1, boolean paramBoolean2)
    {
        this.template = ((NetworkTemplate)Preconditions.checkNotNull(paramNetworkTemplate, "missing NetworkTemplate"));
        this.cycleDay = paramInt;
        this.cycleTimezone = ((String)Preconditions.checkNotNull(paramString, "missing cycleTimezone"));
        this.warningBytes = paramLong1;
        this.limitBytes = paramLong2;
        this.lastWarningSnooze = paramLong3;
        this.lastLimitSnooze = paramLong4;
        this.metered = paramBoolean1;
        this.inferred = paramBoolean2;
    }

    @Deprecated
    public NetworkPolicy(NetworkTemplate paramNetworkTemplate, int paramInt, String paramString, long paramLong1, long paramLong2, boolean paramBoolean)
    {
        this(paramNetworkTemplate, paramInt, paramString, paramLong1, paramLong2, -1L, -1L, paramBoolean, false);
    }

    public NetworkPolicy(Parcel paramParcel)
    {
        this.template = ((NetworkTemplate)paramParcel.readParcelable(null));
        this.cycleDay = paramParcel.readInt();
        this.cycleTimezone = paramParcel.readString();
        this.warningBytes = paramParcel.readLong();
        this.limitBytes = paramParcel.readLong();
        this.lastWarningSnooze = paramParcel.readLong();
        this.lastLimitSnooze = paramParcel.readLong();
        boolean bool2;
        if (paramParcel.readInt() != 0)
        {
            bool2 = bool1;
            this.metered = bool2;
            if (paramParcel.readInt() == 0)
                break label98;
        }
        while (true)
        {
            this.inferred = bool1;
            return;
            bool2 = false;
            break;
            label98: bool1 = false;
        }
    }

    public void clearSnooze()
    {
        this.lastWarningSnooze = -1L;
        this.lastLimitSnooze = -1L;
    }

    public int compareTo(NetworkPolicy paramNetworkPolicy)
    {
        int i;
        if ((paramNetworkPolicy == null) || (paramNetworkPolicy.limitBytes == -1L))
            i = -1;
        while (true)
        {
            return i;
            if ((this.limitBytes == -1L) || (paramNetworkPolicy.limitBytes < this.limitBytes))
                i = 1;
            else
                i = 0;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = false;
        if ((paramObject instanceof NetworkPolicy))
        {
            NetworkPolicy localNetworkPolicy = (NetworkPolicy)paramObject;
            if ((this.cycleDay == localNetworkPolicy.cycleDay) && (this.warningBytes == localNetworkPolicy.warningBytes) && (this.limitBytes == localNetworkPolicy.limitBytes) && (this.lastWarningSnooze == localNetworkPolicy.lastWarningSnooze) && (this.lastLimitSnooze == localNetworkPolicy.lastLimitSnooze) && (this.metered == localNetworkPolicy.metered) && (this.inferred == localNetworkPolicy.inferred) && (Objects.equal(this.cycleTimezone, localNetworkPolicy.cycleTimezone)) && (Objects.equal(this.template, localNetworkPolicy.template)))
                bool = true;
        }
        return bool;
    }

    public boolean hasCycle()
    {
        if (this.cycleDay != -1);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public int hashCode()
    {
        Object[] arrayOfObject = new Object[9];
        arrayOfObject[0] = this.template;
        arrayOfObject[1] = Integer.valueOf(this.cycleDay);
        arrayOfObject[2] = this.cycleTimezone;
        arrayOfObject[3] = Long.valueOf(this.warningBytes);
        arrayOfObject[4] = Long.valueOf(this.limitBytes);
        arrayOfObject[5] = Long.valueOf(this.lastWarningSnooze);
        arrayOfObject[6] = Long.valueOf(this.lastLimitSnooze);
        arrayOfObject[7] = Boolean.valueOf(this.metered);
        arrayOfObject[8] = Boolean.valueOf(this.inferred);
        return Objects.hashCode(arrayOfObject);
    }

    public boolean isOverLimit(long paramLong)
    {
        long l = paramLong + 3000L;
        if ((this.limitBytes != -1L) && (l >= this.limitBytes));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isOverWarning(long paramLong)
    {
        if ((this.warningBytes != -1L) && (paramLong >= this.warningBytes));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder("NetworkPolicy");
        localStringBuilder.append("[").append(this.template).append("]:");
        localStringBuilder.append(" cycleDay=").append(this.cycleDay);
        localStringBuilder.append(", cycleTimezone=").append(this.cycleTimezone);
        localStringBuilder.append(", warningBytes=").append(this.warningBytes);
        localStringBuilder.append(", limitBytes=").append(this.limitBytes);
        localStringBuilder.append(", lastWarningSnooze=").append(this.lastWarningSnooze);
        localStringBuilder.append(", lastLimitSnooze=").append(this.lastLimitSnooze);
        localStringBuilder.append(", metered=").append(this.metered);
        localStringBuilder.append(", inferred=").append(this.inferred);
        return localStringBuilder.toString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        int i = 1;
        paramParcel.writeParcelable(this.template, paramInt);
        paramParcel.writeInt(this.cycleDay);
        paramParcel.writeString(this.cycleTimezone);
        paramParcel.writeLong(this.warningBytes);
        paramParcel.writeLong(this.limitBytes);
        paramParcel.writeLong(this.lastWarningSnooze);
        paramParcel.writeLong(this.lastLimitSnooze);
        int j;
        if (this.metered)
        {
            j = i;
            paramParcel.writeInt(j);
            if (!this.inferred)
                break label94;
        }
        while (true)
        {
            paramParcel.writeInt(i);
            return;
            j = 0;
            break;
            label94: i = 0;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.NetworkPolicy
 * JD-Core Version:        0.6.2
 */