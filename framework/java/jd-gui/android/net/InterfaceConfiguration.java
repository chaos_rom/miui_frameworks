package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.collect.Sets;
import java.net.InetAddress;
import java.util.HashSet;
import java.util.Iterator;

public class InterfaceConfiguration
    implements Parcelable
{
    public static final Parcelable.Creator<InterfaceConfiguration> CREATOR = new Parcelable.Creator()
    {
        public InterfaceConfiguration createFromParcel(Parcel paramAnonymousParcel)
        {
            InterfaceConfiguration localInterfaceConfiguration = new InterfaceConfiguration();
            InterfaceConfiguration.access$002(localInterfaceConfiguration, paramAnonymousParcel.readString());
            if (paramAnonymousParcel.readByte() == 1)
                InterfaceConfiguration.access$102(localInterfaceConfiguration, (LinkAddress)paramAnonymousParcel.readParcelable(null));
            int i = paramAnonymousParcel.readInt();
            for (int j = 0; j < i; j++)
                localInterfaceConfiguration.mFlags.add(paramAnonymousParcel.readString());
            return localInterfaceConfiguration;
        }

        public InterfaceConfiguration[] newArray(int paramAnonymousInt)
        {
            return new InterfaceConfiguration[paramAnonymousInt];
        }
    };
    private static final String FLAG_DOWN = "down";
    private static final String FLAG_UP = "up";
    private LinkAddress mAddr;
    private HashSet<String> mFlags = Sets.newHashSet();
    private String mHwAddr;

    private static void validateFlag(String paramString)
    {
        if (paramString.indexOf(' ') >= 0)
            throw new IllegalArgumentException("flag contains space: " + paramString);
    }

    public void clearFlag(String paramString)
    {
        validateFlag(paramString);
        this.mFlags.remove(paramString);
    }

    public int describeContents()
    {
        return 0;
    }

    public Iterable<String> getFlags()
    {
        return this.mFlags;
    }

    public String getHardwareAddress()
    {
        return this.mHwAddr;
    }

    public LinkAddress getLinkAddress()
    {
        return this.mAddr;
    }

    public boolean hasFlag(String paramString)
    {
        validateFlag(paramString);
        return this.mFlags.contains(paramString);
    }

    public boolean isActive()
    {
        boolean bool = false;
        try
        {
            byte[] arrayOfByte;
            int i;
            if (hasFlag("up"))
            {
                arrayOfByte = this.mAddr.getAddress().getAddress();
                i = arrayOfByte.length;
            }
            for (int j = 0; ; j++)
                if (j < i)
                {
                    int k = arrayOfByte[j];
                    if (k != 0)
                        bool = true;
                }
                else
                {
                    label49: return bool;
                }
        }
        catch (NullPointerException localNullPointerException)
        {
            break label49;
        }
    }

    public void setFlag(String paramString)
    {
        validateFlag(paramString);
        this.mFlags.add(paramString);
    }

    public void setHardwareAddress(String paramString)
    {
        this.mHwAddr = paramString;
    }

    public void setInterfaceDown()
    {
        this.mFlags.remove("up");
        this.mFlags.add("down");
    }

    public void setInterfaceUp()
    {
        this.mFlags.remove("down");
        this.mFlags.add("up");
    }

    public void setLinkAddress(LinkAddress paramLinkAddress)
    {
        this.mAddr = paramLinkAddress;
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("mHwAddr=").append(this.mHwAddr);
        localStringBuilder.append(" mAddr=").append(String.valueOf(this.mAddr));
        localStringBuilder.append(" mFlags=").append(getFlags());
        return localStringBuilder.toString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.mHwAddr);
        if (this.mAddr != null)
        {
            paramParcel.writeByte((byte)1);
            paramParcel.writeParcelable(this.mAddr, paramInt);
        }
        while (true)
        {
            paramParcel.writeInt(this.mFlags.size());
            Iterator localIterator = this.mFlags.iterator();
            while (localIterator.hasNext())
                paramParcel.writeString((String)localIterator.next());
            paramParcel.writeByte((byte)0);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.InterfaceConfiguration
 * JD-Core Version:        0.6.2
 */