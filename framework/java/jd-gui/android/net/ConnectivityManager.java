package android.net;

import android.content.Context;
import android.os.Binder;
import android.os.RemoteException;
import com.android.internal.util.Preconditions;
import java.net.InetAddress;

public class ConnectivityManager
{

    @Deprecated
    public static final String ACTION_BACKGROUND_DATA_SETTING_CHANGED = "android.net.conn.BACKGROUND_DATA_SETTING_CHANGED";
    public static final String ACTION_TETHER_STATE_CHANGED = "android.net.conn.TETHER_STATE_CHANGED";
    public static final String CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
    public static final String CONNECTIVITY_ACTION_IMMEDIATE = "android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE";
    public static final int DEFAULT_NETWORK_PREFERENCE = 1;
    public static final String EXTRA_ACTIVE_TETHER = "activeArray";
    public static final String EXTRA_AVAILABLE_TETHER = "availableArray";
    public static final String EXTRA_ERRORED_TETHER = "erroredArray";
    public static final String EXTRA_EXTRA_INFO = "extraInfo";
    public static final String EXTRA_INET_CONDITION = "inetCondition";
    public static final String EXTRA_IS_FAILOVER = "isFailover";

    @Deprecated
    public static final String EXTRA_NETWORK_INFO = "networkInfo";
    public static final String EXTRA_NO_CONNECTIVITY = "noConnectivity";
    public static final String EXTRA_OTHER_NETWORK_INFO = "otherNetwork";
    public static final String EXTRA_REASON = "reason";
    public static final String INET_CONDITION_ACTION = "android.net.conn.INET_CONDITION_ACTION";
    public static final int MAX_NETWORK_TYPE = 13;
    public static final int MAX_RADIO_TYPE = 13;
    private static final String TAG = "ConnectivityManager";
    public static final int TETHER_ERROR_DISABLE_NAT_ERROR = 9;
    public static final int TETHER_ERROR_ENABLE_NAT_ERROR = 8;
    public static final int TETHER_ERROR_IFACE_CFG_ERROR = 10;
    public static final int TETHER_ERROR_MASTER_ERROR = 5;
    public static final int TETHER_ERROR_NO_ERROR = 0;
    public static final int TETHER_ERROR_SERVICE_UNAVAIL = 2;
    public static final int TETHER_ERROR_TETHER_IFACE_ERROR = 6;
    public static final int TETHER_ERROR_UNAVAIL_IFACE = 4;
    public static final int TETHER_ERROR_UNKNOWN_IFACE = 1;
    public static final int TETHER_ERROR_UNSUPPORTED = 3;
    public static final int TETHER_ERROR_UNTETHER_IFACE_ERROR = 7;
    public static final int TYPE_BLUETOOTH = 7;
    public static final int TYPE_DUMMY = 8;
    public static final int TYPE_ETHERNET = 9;
    public static final int TYPE_MOBILE = 0;
    public static final int TYPE_MOBILE_CBS = 12;
    public static final int TYPE_MOBILE_DUN = 4;
    public static final int TYPE_MOBILE_FOTA = 10;
    public static final int TYPE_MOBILE_HIPRI = 5;
    public static final int TYPE_MOBILE_IMS = 11;
    public static final int TYPE_MOBILE_MMS = 2;
    public static final int TYPE_MOBILE_SUPL = 3;
    public static final int TYPE_NONE = -1;
    public static final int TYPE_WIFI = 1;
    public static final int TYPE_WIFI_P2P = 13;
    public static final int TYPE_WIMAX = 6;
    private final IConnectivityManager mService;

    public ConnectivityManager(IConnectivityManager paramIConnectivityManager)
    {
        this.mService = ((IConnectivityManager)Preconditions.checkNotNull(paramIConnectivityManager, "missing IConnectivityManager"));
    }

    public static ConnectivityManager from(Context paramContext)
    {
        return (ConnectivityManager)paramContext.getSystemService("connectivity");
    }

    public static String getNetworkTypeName(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = Integer.toString(paramInt);
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
        case 13:
        }
        while (true)
        {
            return str;
            str = "MOBILE";
            continue;
            str = "WIFI";
            continue;
            str = "MOBILE_MMS";
            continue;
            str = "MOBILE_SUPL";
            continue;
            str = "MOBILE_DUN";
            continue;
            str = "MOBILE_HIPRI";
            continue;
            str = "WIMAX";
            continue;
            str = "BLUETOOTH";
            continue;
            str = "DUMMY";
            continue;
            str = "ETHERNET";
            continue;
            str = "MOBILE_FOTA";
            continue;
            str = "MOBILE_IMS";
            continue;
            str = "MOBILE_CBS";
            continue;
            str = "WIFI_P2P";
        }
    }

    public static boolean isNetworkTypeMobile(int paramInt)
    {
        switch (paramInt)
        {
        case 1:
        case 6:
        case 7:
        case 8:
        case 9:
        default:
        case 0:
        case 2:
        case 3:
        case 4:
        case 5:
        case 10:
        case 11:
        case 12:
        }
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    public static boolean isNetworkTypeValid(int paramInt)
    {
        if ((paramInt >= 0) && (paramInt <= 13));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public LinkProperties getActiveLinkProperties()
    {
        try
        {
            LinkProperties localLinkProperties2 = this.mService.getActiveLinkProperties();
            localLinkProperties1 = localLinkProperties2;
            return localLinkProperties1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                LinkProperties localLinkProperties1 = null;
        }
    }

    public NetworkInfo getActiveNetworkInfo()
    {
        try
        {
            NetworkInfo localNetworkInfo2 = this.mService.getActiveNetworkInfo();
            localNetworkInfo1 = localNetworkInfo2;
            return localNetworkInfo1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                NetworkInfo localNetworkInfo1 = null;
        }
    }

    public NetworkInfo getActiveNetworkInfoForUid(int paramInt)
    {
        try
        {
            NetworkInfo localNetworkInfo2 = this.mService.getActiveNetworkInfoForUid(paramInt);
            localNetworkInfo1 = localNetworkInfo2;
            return localNetworkInfo1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                NetworkInfo localNetworkInfo1 = null;
        }
    }

    public NetworkQuotaInfo getActiveNetworkQuotaInfo()
    {
        try
        {
            NetworkQuotaInfo localNetworkQuotaInfo2 = this.mService.getActiveNetworkQuotaInfo();
            localNetworkQuotaInfo1 = localNetworkQuotaInfo2;
            return localNetworkQuotaInfo1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                NetworkQuotaInfo localNetworkQuotaInfo1 = null;
        }
    }

    public NetworkInfo[] getAllNetworkInfo()
    {
        try
        {
            NetworkInfo[] arrayOfNetworkInfo2 = this.mService.getAllNetworkInfo();
            arrayOfNetworkInfo1 = arrayOfNetworkInfo2;
            return arrayOfNetworkInfo1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                NetworkInfo[] arrayOfNetworkInfo1 = null;
        }
    }

    @Deprecated
    public boolean getBackgroundDataSetting()
    {
        return true;
    }

    public ProxyProperties getGlobalProxy()
    {
        try
        {
            ProxyProperties localProxyProperties2 = this.mService.getGlobalProxy();
            localProxyProperties1 = localProxyProperties2;
            return localProxyProperties1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                ProxyProperties localProxyProperties1 = null;
        }
    }

    public int getLastTetherError(String paramString)
    {
        try
        {
            int j = this.mService.getLastTetherError(paramString);
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                int i = 2;
        }
    }

    public LinkProperties getLinkProperties(int paramInt)
    {
        try
        {
            LinkProperties localLinkProperties2 = this.mService.getLinkProperties(paramInt);
            localLinkProperties1 = localLinkProperties2;
            return localLinkProperties1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                LinkProperties localLinkProperties1 = null;
        }
    }

    public boolean getMobileDataEnabled()
    {
        try
        {
            boolean bool2 = this.mService.getMobileDataEnabled();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = true;
        }
    }

    public NetworkInfo getNetworkInfo(int paramInt)
    {
        try
        {
            NetworkInfo localNetworkInfo2 = this.mService.getNetworkInfo(paramInt);
            localNetworkInfo1 = localNetworkInfo2;
            return localNetworkInfo1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                NetworkInfo localNetworkInfo1 = null;
        }
    }

    public int getNetworkPreference()
    {
        try
        {
            int j = this.mService.getNetworkPreference();
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                int i = -1;
        }
    }

    public ProxyProperties getProxy()
    {
        try
        {
            ProxyProperties localProxyProperties2 = this.mService.getProxy();
            localProxyProperties1 = localProxyProperties2;
            return localProxyProperties1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                ProxyProperties localProxyProperties1 = null;
        }
    }

    public String[] getTetherableBluetoothRegexs()
    {
        try
        {
            String[] arrayOfString2 = this.mService.getTetherableBluetoothRegexs();
            arrayOfString1 = arrayOfString2;
            return arrayOfString1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                String[] arrayOfString1 = new String[0];
        }
    }

    public String[] getTetherableIfaces()
    {
        try
        {
            String[] arrayOfString2 = this.mService.getTetherableIfaces();
            arrayOfString1 = arrayOfString2;
            return arrayOfString1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                String[] arrayOfString1 = new String[0];
        }
    }

    public String[] getTetherableUsbRegexs()
    {
        try
        {
            String[] arrayOfString2 = this.mService.getTetherableUsbRegexs();
            arrayOfString1 = arrayOfString2;
            return arrayOfString1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                String[] arrayOfString1 = new String[0];
        }
    }

    public String[] getTetherableWifiRegexs()
    {
        try
        {
            String[] arrayOfString2 = this.mService.getTetherableWifiRegexs();
            arrayOfString1 = arrayOfString2;
            return arrayOfString1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                String[] arrayOfString1 = new String[0];
        }
    }

    public String[] getTetheredIfaces()
    {
        try
        {
            String[] arrayOfString2 = this.mService.getTetheredIfaces();
            arrayOfString1 = arrayOfString2;
            return arrayOfString1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                String[] arrayOfString1 = new String[0];
        }
    }

    public String[] getTetheringErroredIfaces()
    {
        try
        {
            String[] arrayOfString2 = this.mService.getTetheringErroredIfaces();
            arrayOfString1 = arrayOfString2;
            return arrayOfString1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                String[] arrayOfString1 = new String[0];
        }
    }

    public boolean isActiveNetworkMetered()
    {
        try
        {
            boolean bool2 = this.mService.isActiveNetworkMetered();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public boolean isNetworkSupported(int paramInt)
    {
        try
        {
            boolean bool2 = this.mService.isNetworkSupported(paramInt);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public boolean isTetheringSupported()
    {
        try
        {
            boolean bool2 = this.mService.isTetheringSupported();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public void reportInetCondition(int paramInt1, int paramInt2)
    {
        try
        {
            this.mService.reportInetCondition(paramInt1, paramInt2);
            label11: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label11;
        }
    }

    public boolean requestNetworkTransitionWakelock(String paramString)
    {
        try
        {
            this.mService.requestNetworkTransitionWakelock(paramString);
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public boolean requestRouteToHost(int paramInt1, int paramInt2)
    {
        InetAddress localInetAddress = NetworkUtils.intToInetAddress(paramInt2);
        if (localInetAddress == null);
        for (boolean bool = false; ; bool = requestRouteToHostAddress(paramInt1, localInetAddress))
            return bool;
    }

    public boolean requestRouteToHostAddress(int paramInt, InetAddress paramInetAddress)
    {
        byte[] arrayOfByte = paramInetAddress.getAddress();
        try
        {
            boolean bool2 = this.mService.requestRouteToHostAddress(paramInt, arrayOfByte);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    @Deprecated
    public void setBackgroundDataSetting(boolean paramBoolean)
    {
    }

    public void setDataDependency(int paramInt, boolean paramBoolean)
    {
        try
        {
            this.mService.setDataDependency(paramInt, paramBoolean);
            label11: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label11;
        }
    }

    public void setGlobalProxy(ProxyProperties paramProxyProperties)
    {
        try
        {
            this.mService.setGlobalProxy(paramProxyProperties);
            label10: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label10;
        }
    }

    public void setMobileDataEnabled(boolean paramBoolean)
    {
        try
        {
            this.mService.setMobileDataEnabled(paramBoolean);
            label10: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label10;
        }
    }

    public void setNetworkPreference(int paramInt)
    {
        try
        {
            this.mService.setNetworkPreference(paramInt);
            label10: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label10;
        }
    }

    public boolean setRadio(int paramInt, boolean paramBoolean)
    {
        try
        {
            boolean bool2 = this.mService.setRadio(paramInt, paramBoolean);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public boolean setRadios(boolean paramBoolean)
    {
        try
        {
            boolean bool2 = this.mService.setRadios(paramBoolean);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public int setUsbTethering(boolean paramBoolean)
    {
        try
        {
            int j = this.mService.setUsbTethering(paramBoolean);
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                int i = 2;
        }
    }

    public int startUsingNetworkFeature(int paramInt, String paramString)
    {
        try
        {
            int j = this.mService.startUsingNetworkFeature(paramInt, paramString, new Binder());
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                int i = -1;
        }
    }

    public int stopUsingNetworkFeature(int paramInt, String paramString)
    {
        try
        {
            int j = this.mService.stopUsingNetworkFeature(paramInt, paramString);
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                int i = -1;
        }
    }

    public int tether(String paramString)
    {
        try
        {
            int j = this.mService.tether(paramString);
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                int i = 2;
        }
    }

    public int untether(String paramString)
    {
        try
        {
            int j = this.mService.untether(paramString);
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                int i = 2;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.ConnectivityManager
 * JD-Core Version:        0.6.2
 */