package android.net;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.util.Log;
import com.android.internal.util.State;
import com.android.internal.util.StateMachine;

public class DhcpStateMachine extends StateMachine
{
    private static final String ACTION_DHCP_RENEW = "android.net.wifi.DHCP_RENEW";
    private static final int BASE = 196608;
    public static final int CMD_POST_DHCP_ACTION = 196613;
    public static final int CMD_PRE_DHCP_ACTION = 196612;
    public static final int CMD_PRE_DHCP_ACTION_COMPLETE = 196614;
    public static final int CMD_RENEW_DHCP = 196611;
    public static final int CMD_START_DHCP = 196609;
    public static final int CMD_STOP_DHCP = 196610;
    private static final boolean DBG = false;
    public static final int DHCP_FAILURE = 2;
    private static final int DHCP_RENEW = 0;
    public static final int DHCP_SUCCESS = 1;
    private static final int MIN_RENEWAL_TIME_SECS = 300;
    private static final String TAG = "DhcpStateMachine";
    private static final String WAKELOCK_TAG = "DHCP";
    private AlarmManager mAlarmManager;
    private BroadcastReceiver mBroadcastReceiver;
    private Context mContext;
    private StateMachine mController;
    private State mDefaultState = new DefaultState();
    private DhcpInfoInternal mDhcpInfo;
    private PowerManager.WakeLock mDhcpRenewWakeLock;
    private PendingIntent mDhcpRenewalIntent;
    private String mInterfaceName;
    private boolean mRegisteredForPreDhcpNotification = false;
    private State mRunningState = new RunningState();
    private State mStoppedState = new StoppedState();
    private State mWaitBeforeRenewalState = new WaitBeforeRenewalState();
    private State mWaitBeforeStartState = new WaitBeforeStartState();

    private DhcpStateMachine(Context paramContext, StateMachine paramStateMachine, String paramString)
    {
        super("DhcpStateMachine");
        this.mContext = paramContext;
        this.mController = paramStateMachine;
        this.mInterfaceName = paramString;
        this.mAlarmManager = ((AlarmManager)this.mContext.getSystemService("alarm"));
        Intent localIntent = new Intent("android.net.wifi.DHCP_RENEW", null);
        this.mDhcpRenewalIntent = PendingIntent.getBroadcast(this.mContext, 0, localIntent, 0);
        this.mDhcpRenewWakeLock = ((PowerManager)this.mContext.getSystemService("power")).newWakeLock(1, "DHCP");
        this.mDhcpRenewWakeLock.setReferenceCounted(false);
        this.mBroadcastReceiver = new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                DhcpStateMachine.this.mDhcpRenewWakeLock.acquire(40000L);
                DhcpStateMachine.this.sendMessage(196611);
            }
        };
        this.mContext.registerReceiver(this.mBroadcastReceiver, new IntentFilter("android.net.wifi.DHCP_RENEW"));
        addState(this.mDefaultState);
        addState(this.mStoppedState, this.mDefaultState);
        addState(this.mWaitBeforeStartState, this.mDefaultState);
        addState(this.mRunningState, this.mDefaultState);
        addState(this.mWaitBeforeRenewalState, this.mDefaultState);
        setInitialState(this.mStoppedState);
    }

    public static DhcpStateMachine makeDhcpStateMachine(Context paramContext, StateMachine paramStateMachine, String paramString)
    {
        DhcpStateMachine localDhcpStateMachine = new DhcpStateMachine(paramContext, paramStateMachine, paramString);
        localDhcpStateMachine.start();
        return localDhcpStateMachine;
    }

    private boolean runDhcp(DhcpAction paramDhcpAction)
    {
        boolean bool = false;
        DhcpInfoInternal localDhcpInfoInternal = new DhcpInfoInternal();
        if (paramDhcpAction == DhcpAction.START)
        {
            bool = NetworkUtils.runDhcp(this.mInterfaceName, localDhcpInfoInternal);
            this.mDhcpInfo = localDhcpInfoInternal;
            if (!bool)
                break label129;
            long l = localDhcpInfoInternal.leaseDuration;
            if (l >= 0L)
            {
                if (l < 300L)
                    l = 300L;
                this.mAlarmManager.set(2, SystemClock.elapsedRealtime() + 480L * l, this.mDhcpRenewalIntent);
            }
            this.mController.obtainMessage(196613, 1, 0, localDhcpInfoInternal).sendToTarget();
        }
        while (true)
        {
            return bool;
            if (paramDhcpAction != DhcpAction.RENEW)
                break;
            bool = NetworkUtils.runDhcpRenew(this.mInterfaceName, localDhcpInfoInternal);
            localDhcpInfoInternal.updateFromDhcpRequest(this.mDhcpInfo);
            break;
            label129: Log.e("DhcpStateMachine", "DHCP failed on " + this.mInterfaceName + ": " + NetworkUtils.getDhcpError());
            NetworkUtils.stopDhcp(this.mInterfaceName);
            this.mController.obtainMessage(196613, 2, 0).sendToTarget();
        }
    }

    public void registerForPreDhcpNotification()
    {
        this.mRegisteredForPreDhcpNotification = true;
    }

    class WaitBeforeRenewalState extends State
    {
        WaitBeforeRenewalState()
        {
        }

        public void enter()
        {
        }

        public void exit()
        {
            DhcpStateMachine.this.mDhcpRenewWakeLock.release();
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool = true;
            switch (paramMessage.what)
            {
            case 196611:
            case 196612:
            case 196613:
            default:
                bool = false;
            case 196609:
            case 196610:
            case 196614:
            }
            while (true)
            {
                return bool;
                DhcpStateMachine.this.mAlarmManager.cancel(DhcpStateMachine.this.mDhcpRenewalIntent);
                if (!NetworkUtils.stopDhcp(DhcpStateMachine.this.mInterfaceName))
                    Log.e("DhcpStateMachine", "Failed to stop Dhcp on " + DhcpStateMachine.this.mInterfaceName);
                DhcpStateMachine.this.transitionTo(DhcpStateMachine.this.mStoppedState);
                continue;
                if (DhcpStateMachine.this.runDhcp(DhcpStateMachine.DhcpAction.RENEW))
                    DhcpStateMachine.this.transitionTo(DhcpStateMachine.this.mRunningState);
                else
                    DhcpStateMachine.this.transitionTo(DhcpStateMachine.this.mStoppedState);
            }
        }
    }

    class RunningState extends State
    {
        RunningState()
        {
        }

        public void enter()
        {
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool = true;
            switch (paramMessage.what)
            {
            default:
                bool = false;
            case 196609:
            case 196610:
            case 196611:
            }
            while (true)
            {
                return bool;
                DhcpStateMachine.this.mAlarmManager.cancel(DhcpStateMachine.this.mDhcpRenewalIntent);
                if (!NetworkUtils.stopDhcp(DhcpStateMachine.this.mInterfaceName))
                    Log.e("DhcpStateMachine", "Failed to stop Dhcp on " + DhcpStateMachine.this.mInterfaceName);
                DhcpStateMachine.this.transitionTo(DhcpStateMachine.this.mStoppedState);
                continue;
                if (DhcpStateMachine.this.mRegisteredForPreDhcpNotification)
                {
                    DhcpStateMachine.this.mController.sendMessage(196612);
                    DhcpStateMachine.this.transitionTo(DhcpStateMachine.this.mWaitBeforeRenewalState);
                }
                else
                {
                    if (!DhcpStateMachine.this.runDhcp(DhcpStateMachine.DhcpAction.RENEW))
                        DhcpStateMachine.this.transitionTo(DhcpStateMachine.this.mStoppedState);
                    DhcpStateMachine.this.mDhcpRenewWakeLock.release();
                }
            }
        }
    }

    class WaitBeforeStartState extends State
    {
        WaitBeforeStartState()
        {
        }

        public void enter()
        {
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool = true;
            switch (paramMessage.what)
            {
            case 196611:
            case 196612:
            case 196613:
            default:
                bool = false;
            case 196609:
            case 196614:
            case 196610:
            }
            while (true)
            {
                return bool;
                if (DhcpStateMachine.this.runDhcp(DhcpStateMachine.DhcpAction.START))
                {
                    DhcpStateMachine.this.transitionTo(DhcpStateMachine.this.mRunningState);
                }
                else
                {
                    DhcpStateMachine.this.transitionTo(DhcpStateMachine.this.mStoppedState);
                    continue;
                    DhcpStateMachine.this.transitionTo(DhcpStateMachine.this.mStoppedState);
                }
            }
        }
    }

    class StoppedState extends State
    {
        StoppedState()
        {
        }

        public void enter()
        {
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool = true;
            switch (paramMessage.what)
            {
            default:
                bool = false;
            case 196610:
            case 196609:
            }
            while (true)
            {
                return bool;
                if (DhcpStateMachine.this.mRegisteredForPreDhcpNotification)
                {
                    DhcpStateMachine.this.mController.sendMessage(196612);
                    DhcpStateMachine.this.transitionTo(DhcpStateMachine.this.mWaitBeforeStartState);
                }
                else if (DhcpStateMachine.this.runDhcp(DhcpStateMachine.DhcpAction.START))
                {
                    DhcpStateMachine.this.transitionTo(DhcpStateMachine.this.mRunningState);
                }
            }
        }
    }

    class DefaultState extends State
    {
        DefaultState()
        {
        }

        public boolean processMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
                Log.e("DhcpStateMachine", "Error! unhandled message    " + paramMessage);
            case 196611:
            case -1:
            }
            for (boolean bool = true; ; bool = false)
            {
                return bool;
                Log.e("DhcpStateMachine", "Error! Failed to handle a DHCP renewal on " + DhcpStateMachine.this.mInterfaceName);
                DhcpStateMachine.this.mDhcpRenewWakeLock.release();
                break;
                DhcpStateMachine.this.mContext.unregisterReceiver(DhcpStateMachine.this.mBroadcastReceiver);
            }
        }
    }

    private static enum DhcpAction
    {
        static
        {
            RENEW = new DhcpAction("RENEW", 1);
            DhcpAction[] arrayOfDhcpAction = new DhcpAction[2];
            arrayOfDhcpAction[0] = START;
            arrayOfDhcpAction[1] = RENEW;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.DhcpStateMachine
 * JD-Core Version:        0.6.2
 */