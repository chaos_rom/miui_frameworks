package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.MathUtils;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.IndentingPrintWriter;
import java.io.CharArrayWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.Arrays;
import java.util.Random;

public class NetworkStatsHistory
    implements Parcelable
{
    public static final Parcelable.Creator<NetworkStatsHistory> CREATOR = new Parcelable.Creator()
    {
        public NetworkStatsHistory createFromParcel(Parcel paramAnonymousParcel)
        {
            return new NetworkStatsHistory(paramAnonymousParcel);
        }

        public NetworkStatsHistory[] newArray(int paramAnonymousInt)
        {
            return new NetworkStatsHistory[paramAnonymousInt];
        }
    };
    public static final int FIELD_ACTIVE_TIME = 1;
    public static final int FIELD_ALL = -1;
    public static final int FIELD_OPERATIONS = 32;
    public static final int FIELD_RX_BYTES = 2;
    public static final int FIELD_RX_PACKETS = 4;
    public static final int FIELD_TX_BYTES = 8;
    public static final int FIELD_TX_PACKETS = 16;
    private static final int VERSION_ADD_ACTIVE = 3;
    private static final int VERSION_ADD_PACKETS = 2;
    private static final int VERSION_INIT = 1;
    private long[] activeTime;
    private int bucketCount;
    private long bucketDuration;
    private long[] bucketStart;
    private long[] operations;
    private long[] rxBytes;
    private long[] rxPackets;
    private long totalBytes;
    private long[] txBytes;
    private long[] txPackets;

    public NetworkStatsHistory(long paramLong)
    {
        this(paramLong, 10, -1);
    }

    public NetworkStatsHistory(long paramLong, int paramInt)
    {
        this(paramLong, paramInt, -1);
    }

    public NetworkStatsHistory(long paramLong, int paramInt1, int paramInt2)
    {
        this.bucketDuration = paramLong;
        this.bucketStart = new long[paramInt1];
        if ((paramInt2 & 0x1) != 0)
            this.activeTime = new long[paramInt1];
        if ((paramInt2 & 0x2) != 0)
            this.rxBytes = new long[paramInt1];
        if ((paramInt2 & 0x4) != 0)
            this.rxPackets = new long[paramInt1];
        if ((paramInt2 & 0x8) != 0)
            this.txBytes = new long[paramInt1];
        if ((paramInt2 & 0x10) != 0)
            this.txPackets = new long[paramInt1];
        if ((paramInt2 & 0x20) != 0)
            this.operations = new long[paramInt1];
        this.bucketCount = 0;
        this.totalBytes = 0L;
    }

    public NetworkStatsHistory(NetworkStatsHistory paramNetworkStatsHistory, long paramLong)
    {
        this(paramLong, paramNetworkStatsHistory.estimateResizeBuckets(paramLong));
        recordEntireHistory(paramNetworkStatsHistory);
    }

    public NetworkStatsHistory(Parcel paramParcel)
    {
        this.bucketDuration = paramParcel.readLong();
        this.bucketStart = ParcelUtils.readLongArray(paramParcel);
        this.activeTime = ParcelUtils.readLongArray(paramParcel);
        this.rxBytes = ParcelUtils.readLongArray(paramParcel);
        this.rxPackets = ParcelUtils.readLongArray(paramParcel);
        this.txBytes = ParcelUtils.readLongArray(paramParcel);
        this.txPackets = ParcelUtils.readLongArray(paramParcel);
        this.operations = ParcelUtils.readLongArray(paramParcel);
        this.bucketCount = this.bucketStart.length;
        this.totalBytes = paramParcel.readLong();
    }

    public NetworkStatsHistory(DataInputStream paramDataInputStream)
        throws IOException
    {
        int i = paramDataInputStream.readInt();
        switch (i)
        {
        default:
            throw new ProtocolException("unexpected version: " + i);
        case 1:
            this.bucketDuration = paramDataInputStream.readLong();
            this.bucketStart = DataStreamUtils.readFullLongArray(paramDataInputStream);
            this.rxBytes = DataStreamUtils.readFullLongArray(paramDataInputStream);
            this.rxPackets = new long[this.bucketStart.length];
            this.txBytes = DataStreamUtils.readFullLongArray(paramDataInputStream);
            this.txPackets = new long[this.bucketStart.length];
            this.operations = new long[this.bucketStart.length];
            this.bucketCount = this.bucketStart.length;
            this.totalBytes = (ArrayUtils.total(this.rxBytes) + ArrayUtils.total(this.txBytes));
            return;
        case 2:
        case 3:
        }
        this.bucketDuration = paramDataInputStream.readLong();
        this.bucketStart = DataStreamUtils.readVarLongArray(paramDataInputStream);
        if (i >= 3);
        for (long[] arrayOfLong = DataStreamUtils.readVarLongArray(paramDataInputStream); ; arrayOfLong = new long[this.bucketStart.length])
        {
            this.activeTime = arrayOfLong;
            this.rxBytes = DataStreamUtils.readVarLongArray(paramDataInputStream);
            this.rxPackets = DataStreamUtils.readVarLongArray(paramDataInputStream);
            this.txBytes = DataStreamUtils.readVarLongArray(paramDataInputStream);
            this.txPackets = DataStreamUtils.readVarLongArray(paramDataInputStream);
            this.operations = DataStreamUtils.readVarLongArray(paramDataInputStream);
            this.bucketCount = this.bucketStart.length;
            this.totalBytes = (ArrayUtils.total(this.rxBytes) + ArrayUtils.total(this.txBytes));
            break;
        }
    }

    private static void addLong(long[] paramArrayOfLong, int paramInt, long paramLong)
    {
        if (paramArrayOfLong != null)
            paramArrayOfLong[paramInt] = (paramLong + paramArrayOfLong[paramInt]);
    }

    private void ensureBuckets(long paramLong1, long paramLong2)
    {
        long l1 = paramLong1 - paramLong1 % this.bucketDuration;
        long l2 = paramLong2 + (this.bucketDuration - paramLong2 % this.bucketDuration) % this.bucketDuration;
        for (long l3 = l1; l3 < l2; l3 += this.bucketDuration)
        {
            int i = Arrays.binarySearch(this.bucketStart, 0, this.bucketCount, l3);
            if (i < 0)
                insertBucket(i ^ 0xFFFFFFFF, l3);
        }
    }

    private static long getLong(long[] paramArrayOfLong, int paramInt, long paramLong)
    {
        if (paramArrayOfLong != null)
            paramLong = paramArrayOfLong[paramInt];
        return paramLong;
    }

    private void insertBucket(int paramInt, long paramLong)
    {
        if (this.bucketCount >= this.bucketStart.length)
        {
            int k = 3 * Math.max(this.bucketStart.length, 10) / 2;
            this.bucketStart = Arrays.copyOf(this.bucketStart, k);
            if (this.activeTime != null)
                this.activeTime = Arrays.copyOf(this.activeTime, k);
            if (this.rxBytes != null)
                this.rxBytes = Arrays.copyOf(this.rxBytes, k);
            if (this.rxPackets != null)
                this.rxPackets = Arrays.copyOf(this.rxPackets, k);
            if (this.txBytes != null)
                this.txBytes = Arrays.copyOf(this.txBytes, k);
            if (this.txPackets != null)
                this.txPackets = Arrays.copyOf(this.txPackets, k);
            if (this.operations != null)
                this.operations = Arrays.copyOf(this.operations, k);
        }
        if (paramInt < this.bucketCount)
        {
            int i = paramInt + 1;
            int j = this.bucketCount - paramInt;
            System.arraycopy(this.bucketStart, paramInt, this.bucketStart, i, j);
            if (this.activeTime != null)
                System.arraycopy(this.activeTime, paramInt, this.activeTime, i, j);
            if (this.rxBytes != null)
                System.arraycopy(this.rxBytes, paramInt, this.rxBytes, i, j);
            if (this.rxPackets != null)
                System.arraycopy(this.rxPackets, paramInt, this.rxPackets, i, j);
            if (this.txBytes != null)
                System.arraycopy(this.txBytes, paramInt, this.txBytes, i, j);
            if (this.txPackets != null)
                System.arraycopy(this.txPackets, paramInt, this.txPackets, i, j);
            if (this.operations != null)
                System.arraycopy(this.operations, paramInt, this.operations, i, j);
        }
        this.bucketStart[paramInt] = paramLong;
        setLong(this.activeTime, paramInt, 0L);
        setLong(this.rxBytes, paramInt, 0L);
        setLong(this.rxPackets, paramInt, 0L);
        setLong(this.txBytes, paramInt, 0L);
        setLong(this.txPackets, paramInt, 0L);
        setLong(this.operations, paramInt, 0L);
        this.bucketCount = (1 + this.bucketCount);
    }

    public static long randomLong(Random paramRandom, long paramLong1, long paramLong2)
    {
        return ()((float)paramLong1 + paramRandom.nextFloat() * (float)(paramLong2 - paramLong1));
    }

    private static void setLong(long[] paramArrayOfLong, int paramInt, long paramLong)
    {
        if (paramArrayOfLong != null)
            paramArrayOfLong[paramInt] = paramLong;
    }

    public int describeContents()
    {
        return 0;
    }

    public void dump(IndentingPrintWriter paramIndentingPrintWriter, boolean paramBoolean)
    {
        int i = 0;
        paramIndentingPrintWriter.print("NetworkStatsHistory: bucketDuration=");
        paramIndentingPrintWriter.println(this.bucketDuration);
        paramIndentingPrintWriter.increaseIndent();
        if (paramBoolean);
        while (true)
        {
            if (i > 0)
            {
                paramIndentingPrintWriter.print("(omitting ");
                paramIndentingPrintWriter.print(i);
                paramIndentingPrintWriter.println(" buckets)");
            }
            for (int j = i; j < this.bucketCount; j++)
            {
                paramIndentingPrintWriter.print("bucketStart=");
                paramIndentingPrintWriter.print(this.bucketStart[j]);
                if (this.activeTime != null)
                {
                    paramIndentingPrintWriter.print(" activeTime=");
                    paramIndentingPrintWriter.print(this.activeTime[j]);
                }
                if (this.rxBytes != null)
                {
                    paramIndentingPrintWriter.print(" rxBytes=");
                    paramIndentingPrintWriter.print(this.rxBytes[j]);
                }
                if (this.rxPackets != null)
                {
                    paramIndentingPrintWriter.print(" rxPackets=");
                    paramIndentingPrintWriter.print(this.rxPackets[j]);
                }
                if (this.txBytes != null)
                {
                    paramIndentingPrintWriter.print(" txBytes=");
                    paramIndentingPrintWriter.print(this.txBytes[j]);
                }
                if (this.txPackets != null)
                {
                    paramIndentingPrintWriter.print(" txPackets=");
                    paramIndentingPrintWriter.print(this.txPackets[j]);
                }
                if (this.operations != null)
                {
                    paramIndentingPrintWriter.print(" operations=");
                    paramIndentingPrintWriter.print(this.operations[j]);
                }
                paramIndentingPrintWriter.println();
            }
            i = Math.max(0, -32 + this.bucketCount);
        }
        paramIndentingPrintWriter.decreaseIndent();
    }

    public int estimateResizeBuckets(long paramLong)
    {
        return (int)(size() * getBucketDuration() / paramLong);
    }

    @Deprecated
    public void generateRandom(long paramLong1, long paramLong2, long paramLong3)
    {
        Random localRandom = new Random();
        float f = localRandom.nextFloat();
        long l1 = ()(f * (float)paramLong3);
        long l2 = ()((float)paramLong3 * (1.0F - f));
        generateRandom(paramLong1, paramLong2, l1, l1 / 1024L, l2, l2 / 1024L, l1 / 2048L, localRandom);
    }

    @Deprecated
    public void generateRandom(long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5, long paramLong6, long paramLong7, Random paramRandom)
    {
        ensureBuckets(paramLong1, paramLong2);
        NetworkStats.Entry localEntry = new NetworkStats.Entry(NetworkStats.IFACE_ALL, -1, 0, 0, 0L, 0L, 0L, 0L, 0L);
        while ((paramLong3 > 1024L) || (paramLong4 > 128L) || (paramLong5 > 1024L) || (paramLong6 > 128L) || (paramLong7 > 32L))
        {
            long l1 = randomLong(paramRandom, paramLong1, paramLong2);
            long l2 = l1 + randomLong(paramRandom, 0L, (paramLong2 - l1) / 2L);
            localEntry.rxBytes = randomLong(paramRandom, 0L, paramLong3);
            localEntry.rxPackets = randomLong(paramRandom, 0L, paramLong4);
            localEntry.txBytes = randomLong(paramRandom, 0L, paramLong5);
            localEntry.txPackets = randomLong(paramRandom, 0L, paramLong6);
            localEntry.operations = randomLong(paramRandom, 0L, paramLong7);
            paramLong3 -= localEntry.rxBytes;
            paramLong4 -= localEntry.rxPackets;
            paramLong5 -= localEntry.txBytes;
            paramLong6 -= localEntry.txPackets;
            paramLong7 -= localEntry.operations;
            recordData(l1, l2, localEntry);
        }
    }

    public long getBucketDuration()
    {
        return this.bucketDuration;
    }

    public long getEnd()
    {
        if (this.bucketCount > 0);
        for (long l = this.bucketStart[(-1 + this.bucketCount)] + this.bucketDuration; ; l = -9223372036854775808L)
            return l;
    }

    public int getIndexAfter(long paramLong)
    {
        int i = Arrays.binarySearch(this.bucketStart, 0, this.bucketCount, paramLong);
        if (i < 0);
        for (int j = i ^ 0xFFFFFFFF; ; j = i + 1)
            return MathUtils.constrain(j, 0, -1 + this.bucketCount);
    }

    public int getIndexBefore(long paramLong)
    {
        int i = Arrays.binarySearch(this.bucketStart, 0, this.bucketCount, paramLong);
        if (i < 0);
        for (int j = -1 + (i ^ 0xFFFFFFFF); ; j = i - 1)
            return MathUtils.constrain(j, 0, -1 + this.bucketCount);
    }

    public long getStart()
    {
        if (this.bucketCount > 0);
        for (long l = this.bucketStart[0]; ; l = 9223372036854775807L)
            return l;
    }

    public long getTotalBytes()
    {
        return this.totalBytes;
    }

    public Entry getValues(int paramInt, Entry paramEntry)
    {
        if (paramEntry != null);
        for (Entry localEntry = paramEntry; ; localEntry = new Entry())
        {
            localEntry.bucketStart = this.bucketStart[paramInt];
            localEntry.bucketDuration = this.bucketDuration;
            localEntry.activeTime = getLong(this.activeTime, paramInt, -1L);
            localEntry.rxBytes = getLong(this.rxBytes, paramInt, -1L);
            localEntry.rxPackets = getLong(this.rxPackets, paramInt, -1L);
            localEntry.txBytes = getLong(this.txBytes, paramInt, -1L);
            localEntry.txPackets = getLong(this.txPackets, paramInt, -1L);
            localEntry.operations = getLong(this.operations, paramInt, -1L);
            return localEntry;
        }
    }

    public Entry getValues(long paramLong1, long paramLong2, long paramLong3, Entry paramEntry)
    {
        Entry localEntry;
        long l1;
        label33: long l2;
        label50: long l3;
        label67: long l4;
        label84: long l5;
        label101: long l6;
        if (paramEntry != null)
        {
            localEntry = paramEntry;
            localEntry.bucketDuration = (paramLong2 - paramLong1);
            localEntry.bucketStart = paramLong1;
            if (this.activeTime == null)
                break label177;
            l1 = 0L;
            localEntry.activeTime = l1;
            if (this.rxBytes == null)
                break label185;
            l2 = 0L;
            localEntry.rxBytes = l2;
            if (this.rxPackets == null)
                break label193;
            l3 = 0L;
            localEntry.rxPackets = l3;
            if (this.txBytes == null)
                break label201;
            l4 = 0L;
            localEntry.txBytes = l4;
            if (this.txPackets == null)
                break label209;
            l5 = 0L;
            localEntry.txPackets = l5;
            if (this.operations == null)
                break label217;
            l6 = 0L;
            label118: localEntry.operations = l6;
        }
        long l7;
        long l8;
        for (int i = getIndexAfter(paramLong2); ; i--)
        {
            if (i >= 0)
            {
                l7 = this.bucketStart[i];
                l8 = l7 + this.bucketDuration;
                if (l8 > paramLong1);
            }
            else
            {
                return localEntry;
                localEntry = new Entry();
                break;
                label177: l1 = -1L;
                break label33;
                label185: l2 = -1L;
                break label50;
                label193: l3 = -1L;
                break label67;
                label201: l4 = -1L;
                break label84;
                label209: l5 = -1L;
                break label101;
                label217: l6 = -1L;
                break label118;
            }
            if (l7 < paramLong2)
                break label238;
        }
        label238: if ((l7 < paramLong3) && (l8 > paramLong3));
        long l11;
        for (int j = 1; ; j = 0)
        {
            if (j == 0)
                break label482;
            l11 = this.bucketDuration;
            label268: if (l11 <= 0L)
                break label512;
            if (this.activeTime != null)
                localEntry.activeTime += l11 * this.activeTime[i] / this.bucketDuration;
            if (this.rxBytes != null)
                localEntry.rxBytes += l11 * this.rxBytes[i] / this.bucketDuration;
            if (this.rxPackets != null)
                localEntry.rxPackets += l11 * this.rxPackets[i] / this.bucketDuration;
            if (this.txBytes != null)
                localEntry.txBytes += l11 * this.txBytes[i] / this.bucketDuration;
            if (this.txPackets != null)
                localEntry.txPackets += l11 * this.txPackets[i] / this.bucketDuration;
            if (this.operations == null)
                break;
            localEntry.operations += l11 * this.operations[i] / this.bucketDuration;
            break;
        }
        label482: long l9;
        if (l8 < paramLong2)
        {
            l9 = l8;
            label493: if (l7 <= paramLong1)
                break label520;
        }
        label512: label520: for (long l10 = l7; ; l10 = paramLong1)
        {
            l11 = l9 - l10;
            break label268;
            break;
            l9 = paramLong2;
            break label493;
        }
    }

    public Entry getValues(long paramLong1, long paramLong2, Entry paramEntry)
    {
        return getValues(paramLong1, paramLong2, 9223372036854775807L, paramEntry);
    }

    @Deprecated
    public void recordData(long paramLong1, long paramLong2, long paramLong3, long paramLong4)
    {
        recordData(paramLong1, paramLong2, new NetworkStats.Entry(NetworkStats.IFACE_ALL, -1, 0, 0, paramLong3, 0L, paramLong4, 0L, 0L));
    }

    public void recordData(long paramLong1, long paramLong2, NetworkStats.Entry paramEntry)
    {
        long l1 = paramEntry.rxBytes;
        long l2 = paramEntry.rxPackets;
        long l3 = paramEntry.txBytes;
        long l4 = paramEntry.txPackets;
        long l5 = paramEntry.operations;
        if (paramEntry.isNegative())
            throw new IllegalArgumentException("tried recording negative data");
        if (paramEntry.isEmpty());
        long l6;
        int i;
        long l7;
        long l8;
        while (true)
        {
            return;
            ensureBuckets(paramLong1, paramLong2);
            l6 = paramLong2 - paramLong1;
            i = getIndexAfter(paramLong2);
            if (i >= 0)
            {
                l7 = this.bucketStart[i];
                l8 = l7 + this.bucketDuration;
                if (l8 >= paramLong1)
                    break;
            }
            this.totalBytes += paramEntry.rxBytes + paramEntry.txBytes;
        }
        if (l7 > paramLong2);
        while (true)
        {
            i--;
            break;
            long l9 = Math.min(l8, paramLong2) - Math.max(l7, paramLong1);
            if (l9 > 0L)
            {
                long l10 = l1 * l9 / l6;
                long l11 = l2 * l9 / l6;
                long l12 = l3 * l9 / l6;
                long l13 = l4 * l9 / l6;
                long l14 = l5 * l9 / l6;
                addLong(this.activeTime, i, l9);
                addLong(this.rxBytes, i, l10);
                l1 -= l10;
                addLong(this.rxPackets, i, l11);
                l2 -= l11;
                addLong(this.txBytes, i, l12);
                l3 -= l12;
                addLong(this.txPackets, i, l13);
                l4 -= l13;
                addLong(this.operations, i, l14);
                l5 -= l14;
                l6 -= l9;
            }
        }
    }

    public void recordEntireHistory(NetworkStatsHistory paramNetworkStatsHistory)
    {
        recordHistory(paramNetworkStatsHistory, -9223372036854775808L, 9223372036854775807L);
    }

    public void recordHistory(NetworkStatsHistory paramNetworkStatsHistory, long paramLong1, long paramLong2)
    {
        NetworkStats.Entry localEntry = new NetworkStats.Entry(NetworkStats.IFACE_ALL, -1, 0, 0, 0L, 0L, 0L, 0L, 0L);
        int i = 0;
        int j = paramNetworkStatsHistory.bucketCount;
        if (i < j)
        {
            long l1 = paramNetworkStatsHistory.bucketStart[i];
            long l2 = l1 + paramNetworkStatsHistory.bucketDuration;
            if ((l1 < paramLong1) || (l2 > paramLong2));
            while (true)
            {
                i++;
                break;
                localEntry.rxBytes = getLong(paramNetworkStatsHistory.rxBytes, i, 0L);
                localEntry.rxPackets = getLong(paramNetworkStatsHistory.rxPackets, i, 0L);
                localEntry.txBytes = getLong(paramNetworkStatsHistory.txBytes, i, 0L);
                localEntry.txPackets = getLong(paramNetworkStatsHistory.txPackets, i, 0L);
                localEntry.operations = getLong(paramNetworkStatsHistory.operations, i, 0L);
                recordData(l1, l2, localEntry);
            }
        }
    }

    @Deprecated
    public void removeBucketsBefore(long paramLong)
    {
        for (int i = 0; ; i++)
            if ((i >= this.bucketCount) || (this.bucketStart[i] + this.bucketDuration > paramLong))
            {
                if (i > 0)
                {
                    int j = this.bucketStart.length;
                    this.bucketStart = Arrays.copyOfRange(this.bucketStart, i, j);
                    if (this.activeTime != null)
                        this.activeTime = Arrays.copyOfRange(this.activeTime, i, j);
                    if (this.rxBytes != null)
                        this.rxBytes = Arrays.copyOfRange(this.rxBytes, i, j);
                    if (this.rxPackets != null)
                        this.rxPackets = Arrays.copyOfRange(this.rxPackets, i, j);
                    if (this.txBytes != null)
                        this.txBytes = Arrays.copyOfRange(this.txBytes, i, j);
                    if (this.txPackets != null)
                        this.txPackets = Arrays.copyOfRange(this.txPackets, i, j);
                    if (this.operations != null)
                        this.operations = Arrays.copyOfRange(this.operations, i, j);
                    this.bucketCount -= i;
                }
                return;
            }
    }

    public int size()
    {
        return this.bucketCount;
    }

    public String toString()
    {
        CharArrayWriter localCharArrayWriter = new CharArrayWriter();
        dump(new IndentingPrintWriter(localCharArrayWriter, "    "), false);
        return localCharArrayWriter.toString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeLong(this.bucketDuration);
        ParcelUtils.writeLongArray(paramParcel, this.bucketStart, this.bucketCount);
        ParcelUtils.writeLongArray(paramParcel, this.activeTime, this.bucketCount);
        ParcelUtils.writeLongArray(paramParcel, this.rxBytes, this.bucketCount);
        ParcelUtils.writeLongArray(paramParcel, this.rxPackets, this.bucketCount);
        ParcelUtils.writeLongArray(paramParcel, this.txBytes, this.bucketCount);
        ParcelUtils.writeLongArray(paramParcel, this.txPackets, this.bucketCount);
        ParcelUtils.writeLongArray(paramParcel, this.operations, this.bucketCount);
        paramParcel.writeLong(this.totalBytes);
    }

    public void writeToStream(DataOutputStream paramDataOutputStream)
        throws IOException
    {
        paramDataOutputStream.writeInt(3);
        paramDataOutputStream.writeLong(this.bucketDuration);
        DataStreamUtils.writeVarLongArray(paramDataOutputStream, this.bucketStart, this.bucketCount);
        DataStreamUtils.writeVarLongArray(paramDataOutputStream, this.activeTime, this.bucketCount);
        DataStreamUtils.writeVarLongArray(paramDataOutputStream, this.rxBytes, this.bucketCount);
        DataStreamUtils.writeVarLongArray(paramDataOutputStream, this.rxPackets, this.bucketCount);
        DataStreamUtils.writeVarLongArray(paramDataOutputStream, this.txBytes, this.bucketCount);
        DataStreamUtils.writeVarLongArray(paramDataOutputStream, this.txPackets, this.bucketCount);
        DataStreamUtils.writeVarLongArray(paramDataOutputStream, this.operations, this.bucketCount);
    }

    public static class ParcelUtils
    {
        public static long[] readLongArray(Parcel paramParcel)
        {
            int i = paramParcel.readInt();
            long[] arrayOfLong;
            if (i == -1)
                arrayOfLong = null;
            while (true)
            {
                return arrayOfLong;
                arrayOfLong = new long[i];
                for (int j = 0; j < arrayOfLong.length; j++)
                    arrayOfLong[j] = paramParcel.readLong();
            }
        }

        public static void writeLongArray(Parcel paramParcel, long[] paramArrayOfLong, int paramInt)
        {
            if (paramArrayOfLong == null)
                paramParcel.writeInt(-1);
            while (true)
            {
                return;
                if (paramInt > paramArrayOfLong.length)
                    throw new IllegalArgumentException("size larger than length");
                paramParcel.writeInt(paramInt);
                for (int i = 0; i < paramInt; i++)
                    paramParcel.writeLong(paramArrayOfLong[i]);
            }
        }
    }

    public static class DataStreamUtils
    {
        @Deprecated
        public static long[] readFullLongArray(DataInputStream paramDataInputStream)
            throws IOException
        {
            long[] arrayOfLong = new long[paramDataInputStream.readInt()];
            for (int i = 0; i < arrayOfLong.length; i++)
                arrayOfLong[i] = paramDataInputStream.readLong();
            return arrayOfLong;
        }

        public static long readVarLong(DataInputStream paramDataInputStream)
            throws IOException
        {
            int i = 0;
            long l = 0L;
            while (i < 64)
            {
                int j = paramDataInputStream.readByte();
                l |= (j & 0x7F) << i;
                if ((j & 0x80) == 0)
                    return l;
                i += 7;
            }
            throw new ProtocolException("malformed long");
        }

        public static long[] readVarLongArray(DataInputStream paramDataInputStream)
            throws IOException
        {
            int i = paramDataInputStream.readInt();
            long[] arrayOfLong;
            if (i == -1)
                arrayOfLong = null;
            while (true)
            {
                return arrayOfLong;
                arrayOfLong = new long[i];
                for (int j = 0; j < arrayOfLong.length; j++)
                    arrayOfLong[j] = readVarLong(paramDataInputStream);
            }
        }

        public static void writeVarLong(DataOutputStream paramDataOutputStream, long paramLong)
            throws IOException
        {
            while (true)
            {
                if ((0xFFFFFF80 & paramLong) == 0L)
                {
                    paramDataOutputStream.writeByte((int)paramLong);
                    return;
                }
                paramDataOutputStream.writeByte(0x80 | 0x7F & (int)paramLong);
                paramLong >>>= 7;
            }
        }

        public static void writeVarLongArray(DataOutputStream paramDataOutputStream, long[] paramArrayOfLong, int paramInt)
            throws IOException
        {
            if (paramArrayOfLong == null)
                paramDataOutputStream.writeInt(-1);
            while (true)
            {
                return;
                if (paramInt > paramArrayOfLong.length)
                    throw new IllegalArgumentException("size larger than length");
                paramDataOutputStream.writeInt(paramInt);
                for (int i = 0; i < paramInt; i++)
                    writeVarLong(paramDataOutputStream, paramArrayOfLong[i]);
            }
        }
    }

    public static class Entry
    {
        public static final long UNKNOWN = -1L;
        public long activeTime;
        public long bucketDuration;
        public long bucketStart;
        public long operations;
        public long rxBytes;
        public long rxPackets;
        public long txBytes;
        public long txPackets;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.NetworkStatsHistory
 * JD-Core Version:        0.6.2
 */