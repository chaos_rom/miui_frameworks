package android.net;

public class SntpClient
{
    private static final int NTP_MODE_CLIENT = 3;
    private static final int NTP_PACKET_SIZE = 48;
    private static final int NTP_PORT = 123;
    private static final int NTP_VERSION = 3;
    private static final long OFFSET_1900_TO_1970 = 2208988800L;
    private static final int ORIGINATE_TIME_OFFSET = 24;
    private static final int RECEIVE_TIME_OFFSET = 32;
    private static final int REFERENCE_TIME_OFFSET = 16;
    private static final String TAG = "SntpClient";
    private static final int TRANSMIT_TIME_OFFSET = 40;
    private long mNtpTime;
    private long mNtpTimeReference;
    private long mRoundTripTime;

    private long read32(byte[] paramArrayOfByte, int paramInt)
    {
        int i = paramArrayOfByte[paramInt];
        int j = paramArrayOfByte[(paramInt + 1)];
        int k = paramArrayOfByte[(paramInt + 2)];
        int m = paramArrayOfByte[(paramInt + 3)];
        int n;
        int i1;
        label69: int i2;
        if ((i & 0x80) == 128)
        {
            n = 128 + (i & 0x7F);
            if ((j & 0x80) != 128)
                break label146;
            i1 = 128 + (j & 0x7F);
            if ((k & 0x80) != 128)
                break label153;
            i2 = 128 + (k & 0x7F);
            label92: if ((m & 0x80) != 128)
                break label160;
        }
        label146: label153: label160: for (int i3 = 128 + (m & 0x7F); ; i3 = m)
        {
            return (n << 24) + (i1 << 16) + (i2 << 8) + i3;
            n = i;
            break;
            i1 = j;
            break label69;
            i2 = k;
            break label92;
        }
    }

    private long readTimeStamp(byte[] paramArrayOfByte, int paramInt)
    {
        long l1 = read32(paramArrayOfByte, paramInt);
        long l2 = read32(paramArrayOfByte, paramInt + 4);
        return 1000L * (l1 - 2208988800L) + 1000L * l2 / 4294967296L;
    }

    private void writeTimeStamp(byte[] paramArrayOfByte, int paramInt, long paramLong)
    {
        long l1 = paramLong / 1000L;
        long l2 = paramLong - 1000L * l1;
        long l3 = l1 + 2208988800L;
        int i = paramInt + 1;
        paramArrayOfByte[paramInt] = ((byte)(int)(l3 >> 24));
        int j = i + 1;
        paramArrayOfByte[i] = ((byte)(int)(l3 >> 16));
        int k = j + 1;
        paramArrayOfByte[j] = ((byte)(int)(l3 >> 8));
        int m = k + 1;
        paramArrayOfByte[k] = ((byte)(int)(l3 >> 0));
        long l4 = 4294967296L * l2 / 1000L;
        int n = m + 1;
        paramArrayOfByte[m] = ((byte)(int)(l4 >> 24));
        int i1 = n + 1;
        paramArrayOfByte[n] = ((byte)(int)(l4 >> 16));
        int i2 = i1 + 1;
        paramArrayOfByte[i1] = ((byte)(int)(l4 >> 8));
        (i2 + 1);
        paramArrayOfByte[i2] = ((byte)(int)(255.0D * Math.random()));
    }

    public long getNtpTime()
    {
        return this.mNtpTime;
    }

    public long getNtpTimeReference()
    {
        return this.mNtpTimeReference;
    }

    public long getRoundTripTime()
    {
        return this.mRoundTripTime;
    }

    // ERROR //
    public boolean requestTime(String paramString, int paramInt)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_3
        //     2: new 70	java/net/DatagramSocket
        //     5: dup
        //     6: invokespecial 71	java/net/DatagramSocket:<init>	()V
        //     9: astore 4
        //     11: aload 4
        //     13: iload_2
        //     14: invokevirtual 75	java/net/DatagramSocket:setSoTimeout	(I)V
        //     17: aload_1
        //     18: invokestatic 81	java/net/InetAddress:getByName	(Ljava/lang/String;)Ljava/net/InetAddress;
        //     21: astore 8
        //     23: bipush 48
        //     25: newarray byte
        //     27: astore 9
        //     29: new 83	java/net/DatagramPacket
        //     32: dup
        //     33: aload 9
        //     35: aload 9
        //     37: arraylength
        //     38: aload 8
        //     40: bipush 123
        //     42: invokespecial 86	java/net/DatagramPacket:<init>	([BILjava/net/InetAddress;I)V
        //     45: astore 10
        //     47: aload 9
        //     49: iconst_0
        //     50: bipush 27
        //     52: bastore
        //     53: invokestatic 91	java/lang/System:currentTimeMillis	()J
        //     56: lstore 11
        //     58: invokestatic 96	android/os/SystemClock:elapsedRealtime	()J
        //     61: lstore 13
        //     63: aload_0
        //     64: aload 9
        //     66: bipush 40
        //     68: lload 11
        //     70: invokespecial 98	android/net/SntpClient:writeTimeStamp	([BIJ)V
        //     73: aload 4
        //     75: aload 10
        //     77: invokevirtual 102	java/net/DatagramSocket:send	(Ljava/net/DatagramPacket;)V
        //     80: new 83	java/net/DatagramPacket
        //     83: dup
        //     84: aload 9
        //     86: aload 9
        //     88: arraylength
        //     89: invokespecial 105	java/net/DatagramPacket:<init>	([BI)V
        //     92: astore 15
        //     94: aload 4
        //     96: aload 15
        //     98: invokevirtual 108	java/net/DatagramSocket:receive	(Ljava/net/DatagramPacket;)V
        //     101: invokestatic 96	android/os/SystemClock:elapsedRealtime	()J
        //     104: lstore 16
        //     106: lload 11
        //     108: lload 16
        //     110: lload 13
        //     112: lsub
        //     113: ladd
        //     114: lstore 18
        //     116: aload_0
        //     117: aload 9
        //     119: bipush 24
        //     121: invokespecial 110	android/net/SntpClient:readTimeStamp	([BI)J
        //     124: lstore 20
        //     126: aload_0
        //     127: aload 9
        //     129: bipush 32
        //     131: invokespecial 110	android/net/SntpClient:readTimeStamp	([BI)J
        //     134: lstore 22
        //     136: aload_0
        //     137: aload 9
        //     139: bipush 40
        //     141: invokespecial 110	android/net/SntpClient:readTimeStamp	([BI)J
        //     144: lstore 24
        //     146: lload 16
        //     148: lload 13
        //     150: lsub
        //     151: lload 24
        //     153: lload 22
        //     155: lsub
        //     156: lsub
        //     157: lstore 26
        //     159: aload_0
        //     160: lload 18
        //     162: lload 22
        //     164: lload 20
        //     166: lsub
        //     167: lload 24
        //     169: lload 18
        //     171: lsub
        //     172: ladd
        //     173: ldc2_w 111
        //     176: ldiv
        //     177: ladd
        //     178: putfield 58	android/net/SntpClient:mNtpTime	J
        //     181: aload_0
        //     182: lload 16
        //     184: putfield 61	android/net/SntpClient:mNtpTimeReference	J
        //     187: aload_0
        //     188: lload 26
        //     190: putfield 64	android/net/SntpClient:mRoundTripTime	J
        //     193: aload 4
        //     195: ifnull +8 -> 203
        //     198: aload 4
        //     200: invokevirtual 115	java/net/DatagramSocket:close	()V
        //     203: iconst_1
        //     204: istore 6
        //     206: iload 6
        //     208: ireturn
        //     209: astore 28
        //     211: iconst_0
        //     212: istore 6
        //     214: aload_3
        //     215: ifnull -9 -> 206
        //     218: aload_3
        //     219: invokevirtual 115	java/net/DatagramSocket:close	()V
        //     222: goto -16 -> 206
        //     225: astore 7
        //     227: aload_3
        //     228: ifnull +7 -> 235
        //     231: aload_3
        //     232: invokevirtual 115	java/net/DatagramSocket:close	()V
        //     235: aload 7
        //     237: athrow
        //     238: astore 7
        //     240: aload 4
        //     242: astore_3
        //     243: goto -16 -> 227
        //     246: astore 5
        //     248: aload 4
        //     250: astore_3
        //     251: goto -40 -> 211
        //
        // Exception table:
        //     from	to	target	type
        //     2	11	209	java/lang/Exception
        //     2	11	225	finally
        //     11	193	238	finally
        //     11	193	246	java/lang/Exception
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.SntpClient
 * JD-Core Version:        0.6.2
 */