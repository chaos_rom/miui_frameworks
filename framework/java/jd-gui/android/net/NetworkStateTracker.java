package android.net;

import android.content.Context;
import android.os.Handler;

public abstract interface NetworkStateTracker
{
    public static final int EVENT_CONFIGURATION_CHANGED = 3;
    public static final int EVENT_RESTORE_DEFAULT_NETWORK = 6;
    public static final int EVENT_STATE_CHANGED = 1;
    public static final int MAX_NETWORK_STATE_TRACKER_EVENT = 100;
    public static final int MIN_NETWORK_STATE_TRACKER_EVENT = 1;

    public abstract void defaultRouteSet(boolean paramBoolean);

    public abstract LinkCapabilities getLinkCapabilities();

    public abstract LinkProperties getLinkProperties();

    public abstract NetworkInfo getNetworkInfo();

    public abstract String getTcpBufferSizesPropName();

    public abstract boolean isAvailable();

    public abstract boolean isDefaultRouteSet();

    public abstract boolean isPrivateDnsRouteSet();

    public abstract boolean isTeardownRequested();

    public abstract void privateDnsRouteSet(boolean paramBoolean);

    public abstract boolean reconnect();

    public abstract void setDependencyMet(boolean paramBoolean);

    public abstract void setPolicyDataEnable(boolean paramBoolean);

    public abstract boolean setRadio(boolean paramBoolean);

    public abstract void setTeardownRequested(boolean paramBoolean);

    public abstract void setUserDataEnable(boolean paramBoolean);

    public abstract void startMonitoring(Context paramContext, Handler paramHandler);

    public abstract boolean teardown();
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.NetworkStateTracker
 * JD-Core Version:        0.6.2
 */