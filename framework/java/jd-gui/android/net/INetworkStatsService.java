package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface INetworkStatsService extends IInterface
{
    public abstract void advisePersistThreshold(long paramLong)
        throws RemoteException;

    public abstract void forceUpdate()
        throws RemoteException;

    public abstract NetworkStats getDataLayerSnapshotForUid(int paramInt)
        throws RemoteException;

    public abstract String[] getMobileIfaces()
        throws RemoteException;

    public abstract long getNetworkTotalBytes(NetworkTemplate paramNetworkTemplate, long paramLong1, long paramLong2)
        throws RemoteException;

    public abstract void incrementOperationCount(int paramInt1, int paramInt2, int paramInt3)
        throws RemoteException;

    public abstract INetworkStatsSession openSession()
        throws RemoteException;

    public abstract void setUidForeground(int paramInt, boolean paramBoolean)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements INetworkStatsService
    {
        private static final String DESCRIPTOR = "android.net.INetworkStatsService";
        static final int TRANSACTION_advisePersistThreshold = 8;
        static final int TRANSACTION_forceUpdate = 7;
        static final int TRANSACTION_getDataLayerSnapshotForUid = 3;
        static final int TRANSACTION_getMobileIfaces = 4;
        static final int TRANSACTION_getNetworkTotalBytes = 2;
        static final int TRANSACTION_incrementOperationCount = 5;
        static final int TRANSACTION_openSession = 1;
        static final int TRANSACTION_setUidForeground = 6;

        public Stub()
        {
            attachInterface(this, "android.net.INetworkStatsService");
        }

        public static INetworkStatsService asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.net.INetworkStatsService");
                if ((localIInterface != null) && ((localIInterface instanceof INetworkStatsService)))
                    localObject = (INetworkStatsService)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = false;
            int i = 1;
            switch (paramInt1)
            {
            default:
                i = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            }
            while (true)
            {
                return i;
                paramParcel2.writeString("android.net.INetworkStatsService");
                continue;
                paramParcel1.enforceInterface("android.net.INetworkStatsService");
                INetworkStatsSession localINetworkStatsSession = openSession();
                paramParcel2.writeNoException();
                if (localINetworkStatsSession != null);
                for (IBinder localIBinder = localINetworkStatsSession.asBinder(); ; localIBinder = null)
                {
                    paramParcel2.writeStrongBinder(localIBinder);
                    break;
                }
                paramParcel1.enforceInterface("android.net.INetworkStatsService");
                if (paramParcel1.readInt() != 0);
                for (NetworkTemplate localNetworkTemplate = (NetworkTemplate)NetworkTemplate.CREATOR.createFromParcel(paramParcel1); ; localNetworkTemplate = null)
                {
                    long l = getNetworkTotalBytes(localNetworkTemplate, paramParcel1.readLong(), paramParcel1.readLong());
                    paramParcel2.writeNoException();
                    paramParcel2.writeLong(l);
                    break;
                }
                paramParcel1.enforceInterface("android.net.INetworkStatsService");
                NetworkStats localNetworkStats = getDataLayerSnapshotForUid(paramParcel1.readInt());
                paramParcel2.writeNoException();
                if (localNetworkStats != null)
                {
                    paramParcel2.writeInt(i);
                    localNetworkStats.writeToParcel(paramParcel2, i);
                }
                else
                {
                    paramParcel2.writeInt(0);
                    continue;
                    paramParcel1.enforceInterface("android.net.INetworkStatsService");
                    String[] arrayOfString = getMobileIfaces();
                    paramParcel2.writeNoException();
                    paramParcel2.writeStringArray(arrayOfString);
                    continue;
                    paramParcel1.enforceInterface("android.net.INetworkStatsService");
                    incrementOperationCount(paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    continue;
                    paramParcel1.enforceInterface("android.net.INetworkStatsService");
                    int j = paramParcel1.readInt();
                    if (paramParcel1.readInt() != 0)
                        bool = i;
                    setUidForeground(j, bool);
                    paramParcel2.writeNoException();
                    continue;
                    paramParcel1.enforceInterface("android.net.INetworkStatsService");
                    forceUpdate();
                    paramParcel2.writeNoException();
                    continue;
                    paramParcel1.enforceInterface("android.net.INetworkStatsService");
                    advisePersistThreshold(paramParcel1.readLong());
                    paramParcel2.writeNoException();
                }
            }
        }

        private static class Proxy
            implements INetworkStatsService
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public void advisePersistThreshold(long paramLong)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.INetworkStatsService");
                    localParcel1.writeLong(paramLong);
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void forceUpdate()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.INetworkStatsService");
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public NetworkStats getDataLayerSnapshotForUid(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.INetworkStatsService");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localNetworkStats = (NetworkStats)NetworkStats.CREATOR.createFromParcel(localParcel2);
                        return localNetworkStats;
                    }
                    NetworkStats localNetworkStats = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.net.INetworkStatsService";
            }

            public String[] getMobileIfaces()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.INetworkStatsService");
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String[] arrayOfString = localParcel2.createStringArray();
                    return arrayOfString;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public long getNetworkTotalBytes(NetworkTemplate paramNetworkTemplate, long paramLong1, long paramLong2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.INetworkStatsService");
                    if (paramNetworkTemplate != null)
                    {
                        localParcel1.writeInt(1);
                        paramNetworkTemplate.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeLong(paramLong1);
                        localParcel1.writeLong(paramLong2);
                        this.mRemote.transact(2, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        long l = localParcel2.readLong();
                        return l;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void incrementOperationCount(int paramInt1, int paramInt2, int paramInt3)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.INetworkStatsService");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    localParcel1.writeInt(paramInt3);
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public INetworkStatsSession openSession()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.INetworkStatsService");
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    INetworkStatsSession localINetworkStatsSession = INetworkStatsSession.Stub.asInterface(localParcel2.readStrongBinder());
                    return localINetworkStatsSession;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setUidForeground(int paramInt, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.INetworkStatsService");
                    localParcel1.writeInt(paramInt);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.INetworkStatsService
 * JD-Core Version:        0.6.2
 */