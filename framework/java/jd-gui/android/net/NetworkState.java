package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class NetworkState
    implements Parcelable
{
    public static final Parcelable.Creator<NetworkState> CREATOR = new Parcelable.Creator()
    {
        public NetworkState createFromParcel(Parcel paramAnonymousParcel)
        {
            return new NetworkState(paramAnonymousParcel);
        }

        public NetworkState[] newArray(int paramAnonymousInt)
        {
            return new NetworkState[paramAnonymousInt];
        }
    };
    public final LinkCapabilities linkCapabilities;
    public final LinkProperties linkProperties;
    public final String networkId;
    public final NetworkInfo networkInfo;
    public final String subscriberId;

    public NetworkState(NetworkInfo paramNetworkInfo, LinkProperties paramLinkProperties, LinkCapabilities paramLinkCapabilities)
    {
        this(paramNetworkInfo, paramLinkProperties, paramLinkCapabilities, null, null);
    }

    public NetworkState(NetworkInfo paramNetworkInfo, LinkProperties paramLinkProperties, LinkCapabilities paramLinkCapabilities, String paramString1, String paramString2)
    {
        this.networkInfo = paramNetworkInfo;
        this.linkProperties = paramLinkProperties;
        this.linkCapabilities = paramLinkCapabilities;
        this.subscriberId = paramString1;
        this.networkId = paramString2;
    }

    public NetworkState(Parcel paramParcel)
    {
        this.networkInfo = ((NetworkInfo)paramParcel.readParcelable(null));
        this.linkProperties = ((LinkProperties)paramParcel.readParcelable(null));
        this.linkCapabilities = ((LinkCapabilities)paramParcel.readParcelable(null));
        this.subscriberId = paramParcel.readString();
        this.networkId = paramParcel.readString();
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeParcelable(this.networkInfo, paramInt);
        paramParcel.writeParcelable(this.linkProperties, paramInt);
        paramParcel.writeParcelable(this.linkCapabilities, paramInt);
        paramParcel.writeString(this.subscriberId);
        paramParcel.writeString(this.networkId);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.NetworkState
 * JD-Core Version:        0.6.2
 */