package android.net;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Slog;
import com.android.internal.telephony.DataConnectionTracker;
import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.ITelephony.Stub;
import com.android.internal.telephony.Phone.DataState;
import com.android.internal.util.AsyncChannel;
import java.io.CharArrayWriter;
import java.io.PrintWriter;

public class MobileDataStateTracker
    implements NetworkStateTracker
{
    private static final boolean DBG = false;
    private static final String TAG = "MobileDataStateTracker";
    private static final boolean VDBG;
    private String mApnType;
    private Context mContext;
    private AsyncChannel mDataConnectionTrackerAc;
    private boolean mDefaultRouteSet = false;
    private Handler mHandler;
    private LinkCapabilities mLinkCapabilities;
    private LinkProperties mLinkProperties;
    private Messenger mMessenger;
    private Phone.DataState mMobileDataState;
    private NetworkInfo mNetworkInfo;
    private ITelephony mPhoneService;
    protected boolean mPolicyDataEnabled = true;
    private boolean mPrivateDnsRouteSet = false;
    private Handler mTarget;
    private boolean mTeardownRequested = false;
    protected boolean mUserDataEnabled = true;

    public MobileDataStateTracker(int paramInt, String paramString)
    {
        this.mNetworkInfo = new NetworkInfo(paramInt, TelephonyManager.getDefault().getNetworkType(), paramString, TelephonyManager.getDefault().getNetworkTypeName());
        this.mApnType = networkTypeToApnType(paramInt);
    }

    private void getPhoneService(boolean paramBoolean)
    {
        if ((this.mPhoneService == null) || (paramBoolean))
            this.mPhoneService = ITelephony.Stub.asInterface(ServiceManager.getService("phone"));
    }

    private void log(String paramString)
    {
        Slog.d("MobileDataStateTracker", this.mApnType + ": " + paramString);
    }

    private void loge(String paramString)
    {
        Slog.e("MobileDataStateTracker", this.mApnType + ": " + paramString);
    }

    public static String networkTypeToApnType(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        case 1:
        case 6:
        case 7:
        case 8:
        case 9:
        default:
            sloge("Error mapping networkType " + paramInt + " to apnType.");
            str = null;
        case 0:
        case 2:
        case 3:
        case 4:
        case 5:
        case 10:
        case 11:
        case 12:
        }
        while (true)
        {
            return str;
            str = "default";
            continue;
            str = "mms";
            continue;
            str = "supl";
            continue;
            str = "dun";
            continue;
            str = "hipri";
            continue;
            str = "fota";
            continue;
            str = "ims";
            continue;
            str = "cbs";
        }
    }

    private void setDetailedState(NetworkInfo.DetailedState paramDetailedState, String paramString1, String paramString2)
    {
        if (paramDetailedState != this.mNetworkInfo.getDetailedState())
            if (this.mNetworkInfo.getState() != NetworkInfo.State.CONNECTING)
                break label93;
        label93: for (int i = 1; ; i = 0)
        {
            String str = this.mNetworkInfo.getReason();
            if ((i != 0) && (paramDetailedState == NetworkInfo.DetailedState.CONNECTED) && (paramString1 == null) && (str != null))
                paramString1 = str;
            this.mNetworkInfo.setDetailedState(paramDetailedState, paramString1, paramString2);
            this.mTarget.obtainMessage(1, new NetworkInfo(this.mNetworkInfo)).sendToTarget();
            return;
        }
    }

    private int setEnableApn(String paramString, boolean paramBoolean)
    {
        getPhoneService(false);
        int i = 0;
        StringBuilder localStringBuilder;
        if (i < 2)
        {
            if (this.mPhoneService == null)
                loge("Ignoring feature request because could not acquire PhoneService");
        }
        else
        {
            localStringBuilder = new StringBuilder().append("Could not ");
            if (!paramBoolean)
                break label141;
        }
        label141: for (String str = "enable"; ; str = "disable")
        {
            loge(str + " APN type \"" + paramString + "\"");
            int j = 3;
            while (true)
            {
                return j;
                if (paramBoolean);
                try
                {
                    j = this.mPhoneService.enableApnType(paramString);
                    continue;
                    int k = this.mPhoneService.disableApnType(paramString);
                    j = k;
                }
                catch (RemoteException localRemoteException)
                {
                    if (i == 0)
                        getPhoneService(true);
                    i++;
                }
            }
            break;
        }
    }

    private static void sloge(String paramString)
    {
        Slog.e("MobileDataStateTracker", paramString);
    }

    public void defaultRouteSet(boolean paramBoolean)
    {
        this.mDefaultRouteSet = paramBoolean;
    }

    public LinkCapabilities getLinkCapabilities()
    {
        return new LinkCapabilities(this.mLinkCapabilities);
    }

    public LinkProperties getLinkProperties()
    {
        return new LinkProperties(this.mLinkProperties);
    }

    public NetworkInfo getNetworkInfo()
    {
        return this.mNetworkInfo;
    }

    public String getTcpBufferSizesPropName()
    {
        String str = "unknown";
        TelephonyManager localTelephonyManager = new TelephonyManager(this.mContext);
        switch (localTelephonyManager.getNetworkType())
        {
        default:
            loge("unknown network type: " + localTelephonyManager.getNetworkType());
        case 1:
        case 2:
        case 3:
        case 8:
        case 9:
        case 10:
        case 4:
        case 7:
        case 5:
        case 6:
        case 12:
        case 11:
        case 13:
        case 14:
        }
        while (true)
        {
            return "net.tcp.buffersize." + str;
            str = "gprs";
            continue;
            str = "edge";
            continue;
            str = "umts";
            continue;
            str = "hsdpa";
            continue;
            str = "hsupa";
            continue;
            str = "hspa";
            continue;
            str = "cdma";
            continue;
            str = "1xrtt";
            continue;
            str = "evdo";
            continue;
            str = "evdo";
            continue;
            str = "evdo";
            continue;
            str = "iden";
            continue;
            str = "lte";
            continue;
            str = "ehrpd";
        }
    }

    public boolean isAvailable()
    {
        return this.mNetworkInfo.isAvailable();
    }

    public boolean isDefaultRouteSet()
    {
        return this.mDefaultRouteSet;
    }

    public boolean isPrivateDnsRouteSet()
    {
        return this.mPrivateDnsRouteSet;
    }

    public boolean isTeardownRequested()
    {
        return this.mTeardownRequested;
    }

    public void privateDnsRouteSet(boolean paramBoolean)
    {
        this.mPrivateDnsRouteSet = paramBoolean;
    }

    public boolean reconnect()
    {
        boolean bool = false;
        setTeardownRequested(false);
        switch (setEnableApn(this.mApnType, true))
        {
        default:
            loge("Error in reconnect - unexpected response.");
        case 2:
        case 3:
        case 0:
        case 1:
        }
        while (true)
        {
            return bool;
            bool = true;
            continue;
            this.mNetworkInfo.setDetailedState(NetworkInfo.DetailedState.IDLE, null, null);
            bool = true;
        }
    }

    public void releaseWakeLock()
    {
    }

    public void setDependencyMet(boolean paramBoolean)
    {
        Bundle localBundle = Bundle.forPair("apnType", this.mApnType);
        try
        {
            Message localMessage = Message.obtain();
            localMessage.what = 270367;
            if (paramBoolean);
            for (int i = 1; ; i = 0)
            {
                localMessage.arg1 = i;
                localMessage.setData(localBundle);
                this.mDataConnectionTrackerAc.sendMessage(localMessage);
                return;
            }
        }
        catch (NullPointerException localNullPointerException)
        {
            while (true)
                loge("setDependencyMet: X mAc was null" + localNullPointerException);
        }
    }

    public void setPolicyDataEnable(boolean paramBoolean)
    {
        AsyncChannel localAsyncChannel = this.mDataConnectionTrackerAc;
        if (localAsyncChannel != null)
            if (!paramBoolean)
                break label29;
        label29: for (int i = 1; ; i = 0)
        {
            localAsyncChannel.sendMessage(270368, i);
            this.mPolicyDataEnabled = paramBoolean;
            return;
        }
    }

    public boolean setRadio(boolean paramBoolean)
    {
        getPhoneService(false);
        int i = 0;
        StringBuilder localStringBuilder;
        if (i < 2)
        {
            if (this.mPhoneService == null)
                loge("Ignoring mobile radio request because could not acquire PhoneService");
        }
        else
        {
            localStringBuilder = new StringBuilder().append("Could not set radio power to ");
            if (!paramBoolean)
                break label104;
        }
        label104: for (String str = "on"; ; str = "off")
        {
            loge(str);
            boolean bool1 = false;
            while (true)
            {
                return bool1;
                try
                {
                    boolean bool2 = this.mPhoneService.setRadio(paramBoolean);
                    bool1 = bool2;
                }
                catch (RemoteException localRemoteException)
                {
                    if (i == 0)
                        getPhoneService(true);
                    i++;
                }
            }
            break;
        }
    }

    public void setTeardownRequested(boolean paramBoolean)
    {
        this.mTeardownRequested = paramBoolean;
    }

    public void setUserDataEnable(boolean paramBoolean)
    {
        AsyncChannel localAsyncChannel = this.mDataConnectionTrackerAc;
        if (localAsyncChannel != null)
            if (!paramBoolean)
                break label29;
        label29: for (int i = 1; ; i = 0)
        {
            localAsyncChannel.sendMessage(270365, i);
            this.mUserDataEnabled = paramBoolean;
            return;
        }
    }

    public void startMonitoring(Context paramContext, Handler paramHandler)
    {
        this.mTarget = paramHandler;
        this.mContext = paramContext;
        this.mHandler = new MdstHandler(paramHandler.getLooper(), this);
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("android.intent.action.ANY_DATA_STATE");
        localIntentFilter.addAction("android.intent.action.DATA_CONNECTION_FAILED");
        localIntentFilter.addAction(DataConnectionTracker.ACTION_DATA_CONNECTION_TRACKER_MESSENGER);
        this.mContext.registerReceiver(new MobileDataStateReceiver(null), localIntentFilter);
        this.mMobileDataState = Phone.DataState.DISCONNECTED;
    }

    public boolean teardown()
    {
        boolean bool = true;
        setTeardownRequested(bool);
        if (setEnableApn(this.mApnType, false) != 3);
        while (true)
        {
            return bool;
            bool = false;
        }
    }

    public String toString()
    {
        CharArrayWriter localCharArrayWriter = new CharArrayWriter();
        PrintWriter localPrintWriter = new PrintWriter(localCharArrayWriter);
        localPrintWriter.print("Mobile data state: ");
        localPrintWriter.println(this.mMobileDataState);
        localPrintWriter.print("Data enabled: user=");
        localPrintWriter.print(this.mUserDataEnabled);
        localPrintWriter.print(", policy=");
        localPrintWriter.println(this.mPolicyDataEnabled);
        return localCharArrayWriter.toString();
    }

    private class MobileDataStateReceiver extends BroadcastReceiver
    {
        private MobileDataStateReceiver()
        {
        }

        public void onReceive(Context paramContext, Intent paramIntent)
        {
            if (paramIntent.getAction().equals("android.intent.action.ANY_DATA_STATE"))
                if (TextUtils.equals(paramIntent.getStringExtra("apnType"), MobileDataStateTracker.this.mApnType));
            while (true)
            {
                return;
                MobileDataStateTracker.this.mNetworkInfo.setSubtype(TelephonyManager.getDefault().getNetworkType(), TelephonyManager.getDefault().getNetworkTypeName());
                Phone.DataState localDataState = (Phone.DataState)Enum.valueOf(Phone.DataState.class, paramIntent.getStringExtra("state"));
                String str3 = paramIntent.getStringExtra("reason");
                String str4 = paramIntent.getStringExtra("apn");
                MobileDataStateTracker.this.mNetworkInfo.setRoaming(paramIntent.getBooleanExtra("networkRoaming", false));
                NetworkInfo localNetworkInfo = MobileDataStateTracker.this.mNetworkInfo;
                if (!paramIntent.getBooleanExtra("networkUnvailable", false));
                for (boolean bool = true; ; bool = false)
                {
                    localNetworkInfo.setIsAvailable(bool);
                    if (MobileDataStateTracker.this.mMobileDataState == localDataState)
                        break label389;
                    MobileDataStateTracker.access$402(MobileDataStateTracker.this, localDataState);
                    switch (MobileDataStateTracker.1.$SwitchMap$com$android$internal$telephony$Phone$DataState[localDataState.ordinal()])
                    {
                    default:
                        break;
                    case 1:
                        if (MobileDataStateTracker.this.isTeardownRequested())
                            MobileDataStateTracker.this.setTeardownRequested(false);
                        MobileDataStateTracker.this.setDetailedState(NetworkInfo.DetailedState.DISCONNECTED, str3, str4);
                        break;
                    case 2:
                    case 3:
                    case 4:
                    }
                }
                MobileDataStateTracker.this.setDetailedState(NetworkInfo.DetailedState.CONNECTING, str3, str4);
                continue;
                MobileDataStateTracker.this.setDetailedState(NetworkInfo.DetailedState.SUSPENDED, str3, str4);
                continue;
                MobileDataStateTracker.access$602(MobileDataStateTracker.this, (LinkProperties)paramIntent.getParcelableExtra("linkProperties"));
                if (MobileDataStateTracker.this.mLinkProperties == null)
                {
                    MobileDataStateTracker.this.loge("CONNECTED event did not supply link properties.");
                    MobileDataStateTracker.access$602(MobileDataStateTracker.this, new LinkProperties());
                }
                MobileDataStateTracker.access$802(MobileDataStateTracker.this, (LinkCapabilities)paramIntent.getParcelableExtra("linkCapabilities"));
                if (MobileDataStateTracker.this.mLinkCapabilities == null)
                {
                    MobileDataStateTracker.this.loge("CONNECTED event did not supply link capabilities.");
                    MobileDataStateTracker.access$802(MobileDataStateTracker.this, new LinkCapabilities());
                }
                MobileDataStateTracker.this.setDetailedState(NetworkInfo.DetailedState.CONNECTED, str3, str4);
                continue;
                label389: if (TextUtils.equals(str3, "linkPropertiesChanged"))
                {
                    MobileDataStateTracker.access$602(MobileDataStateTracker.this, (LinkProperties)paramIntent.getParcelableExtra("linkProperties"));
                    if (MobileDataStateTracker.this.mLinkProperties == null)
                    {
                        MobileDataStateTracker.this.loge("No link property in LINK_PROPERTIES change event.");
                        MobileDataStateTracker.access$602(MobileDataStateTracker.this, new LinkProperties());
                    }
                    MobileDataStateTracker.this.mNetworkInfo.setDetailedState(MobileDataStateTracker.this.mNetworkInfo.getDetailedState(), str3, MobileDataStateTracker.this.mNetworkInfo.getExtraInfo());
                    MobileDataStateTracker.this.mTarget.obtainMessage(3, MobileDataStateTracker.this.mNetworkInfo).sendToTarget();
                    continue;
                    if (paramIntent.getAction().equals("android.intent.action.DATA_CONNECTION_FAILED"))
                    {
                        if (TextUtils.equals(paramIntent.getStringExtra("apnType"), MobileDataStateTracker.this.mApnType))
                        {
                            String str1 = paramIntent.getStringExtra("reason");
                            String str2 = paramIntent.getStringExtra("apn");
                            MobileDataStateTracker.this.setDetailedState(NetworkInfo.DetailedState.FAILED, str1, str2);
                        }
                    }
                    else if (paramIntent.getAction().equals(DataConnectionTracker.ACTION_DATA_CONNECTION_TRACKER_MESSENGER))
                    {
                        MobileDataStateTracker.access$1002(MobileDataStateTracker.this, (Messenger)paramIntent.getParcelableExtra(DataConnectionTracker.EXTRA_MESSENGER));
                        new AsyncChannel().connect(MobileDataStateTracker.this.mContext, MobileDataStateTracker.this.mHandler, MobileDataStateTracker.this.mMessenger);
                    }
                }
            }
        }
    }

    static class MdstHandler extends Handler
    {
        private MobileDataStateTracker mMdst;

        MdstHandler(Looper paramLooper, MobileDataStateTracker paramMobileDataStateTracker)
        {
            super();
            this.mMdst = paramMobileDataStateTracker;
        }

        public void handleMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
            case 69632:
            case 69636:
            }
            while (true)
            {
                return;
                if (paramMessage.arg1 == 0)
                {
                    MobileDataStateTracker.access$102(this.mMdst, (AsyncChannel)paramMessage.obj);
                    continue;
                    MobileDataStateTracker.access$102(this.mMdst, null);
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.MobileDataStateTracker
 * JD-Core Version:        0.6.2
 */