package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.UnknownHostException;

public class LinkAddress
    implements Parcelable
{
    public static final Parcelable.Creator<LinkAddress> CREATOR = new Parcelable.Creator()
    {
        public LinkAddress createFromParcel(Parcel paramAnonymousParcel)
        {
            InetAddress localInetAddress = null;
            int i = 0;
            if (paramAnonymousParcel.readByte() == 1);
            try
            {
                localInetAddress = InetAddress.getByAddress(paramAnonymousParcel.createByteArray());
                int j = paramAnonymousParcel.readInt();
                i = j;
                label29: return new LinkAddress(localInetAddress, i);
            }
            catch (UnknownHostException localUnknownHostException)
            {
                break label29;
            }
        }

        public LinkAddress[] newArray(int paramAnonymousInt)
        {
            return new LinkAddress[paramAnonymousInt];
        }
    };
    private final InetAddress address;
    private final int prefixLength;

    public LinkAddress(InetAddress paramInetAddress, int paramInt)
    {
        if ((paramInetAddress == null) || (paramInt < 0) || (((paramInetAddress instanceof Inet4Address)) && (paramInt > 32)) || (paramInt > 128))
            throw new IllegalArgumentException("Bad LinkAddress params " + paramInetAddress + paramInt);
        this.address = paramInetAddress;
        this.prefixLength = paramInt;
    }

    public LinkAddress(InterfaceAddress paramInterfaceAddress)
    {
        this.address = paramInterfaceAddress.getAddress();
        this.prefixLength = paramInterfaceAddress.getNetworkPrefixLength();
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = false;
        if (!(paramObject instanceof LinkAddress));
        while (true)
        {
            return bool;
            LinkAddress localLinkAddress = (LinkAddress)paramObject;
            if ((this.address.equals(localLinkAddress.address)) && (this.prefixLength == localLinkAddress.prefixLength))
                bool = true;
        }
    }

    public InetAddress getAddress()
    {
        return this.address;
    }

    public int getNetworkPrefixLength()
    {
        return this.prefixLength;
    }

    public int hashCode()
    {
        if (this.address == null);
        for (int i = 0; ; i = this.address.hashCode())
            return i + this.prefixLength;
    }

    public String toString()
    {
        if (this.address == null);
        for (String str = ""; ; str = this.address.getHostAddress() + "/" + this.prefixLength)
            return str;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        if (this.address != null)
        {
            paramParcel.writeByte((byte)1);
            paramParcel.writeByteArray(this.address.getAddress());
            paramParcel.writeInt(this.prefixLength);
        }
        while (true)
        {
            return;
            paramParcel.writeByte((byte)0);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.LinkAddress
 * JD-Core Version:        0.6.2
 */