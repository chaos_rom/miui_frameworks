package android.net;

import android.text.TextUtils;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import org.apache.http.HttpHost;

public final class ExtraProxy
{
    public static final HttpHost getPreferredHttpHost(InetSocketAddress paramInetSocketAddress)
    {
        String str = null;
        if (paramInetSocketAddress.getAddress() != null)
            str = paramInetSocketAddress.getAddress().getHostAddress();
        if (TextUtils.isEmpty(str))
            str = paramInetSocketAddress.getHostName();
        return new HttpHost(str, paramInetSocketAddress.getPort(), "http");
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.ExtraProxy
 * JD-Core Version:        0.6.2
 */