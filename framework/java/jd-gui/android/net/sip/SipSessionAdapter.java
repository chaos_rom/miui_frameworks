package android.net.sip;

public class SipSessionAdapter extends ISipSessionListener.Stub
{
    public void onCallBusy(ISipSession paramISipSession)
    {
    }

    public void onCallChangeFailed(ISipSession paramISipSession, int paramInt, String paramString)
    {
    }

    public void onCallEnded(ISipSession paramISipSession)
    {
    }

    public void onCallEstablished(ISipSession paramISipSession, String paramString)
    {
    }

    public void onCallTransferring(ISipSession paramISipSession, String paramString)
    {
    }

    public void onCalling(ISipSession paramISipSession)
    {
    }

    public void onError(ISipSession paramISipSession, int paramInt, String paramString)
    {
    }

    public void onRegistering(ISipSession paramISipSession)
    {
    }

    public void onRegistrationDone(ISipSession paramISipSession, int paramInt)
    {
    }

    public void onRegistrationFailed(ISipSession paramISipSession, int paramInt, String paramString)
    {
    }

    public void onRegistrationTimeout(ISipSession paramISipSession)
    {
    }

    public void onRinging(ISipSession paramISipSession, SipProfile paramSipProfile, String paramString)
    {
    }

    public void onRingingBack(ISipSession paramISipSession)
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.sip.SipSessionAdapter
 * JD-Core Version:        0.6.2
 */