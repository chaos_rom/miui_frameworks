package android.net.sip;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface ISipSession extends IInterface
{
    public abstract void answerCall(String paramString, int paramInt)
        throws RemoteException;

    public abstract void changeCall(String paramString, int paramInt)
        throws RemoteException;

    public abstract void endCall()
        throws RemoteException;

    public abstract String getCallId()
        throws RemoteException;

    public abstract String getLocalIp()
        throws RemoteException;

    public abstract SipProfile getLocalProfile()
        throws RemoteException;

    public abstract SipProfile getPeerProfile()
        throws RemoteException;

    public abstract int getState()
        throws RemoteException;

    public abstract boolean isInCall()
        throws RemoteException;

    public abstract void makeCall(SipProfile paramSipProfile, String paramString, int paramInt)
        throws RemoteException;

    public abstract void register(int paramInt)
        throws RemoteException;

    public abstract void setListener(ISipSessionListener paramISipSessionListener)
        throws RemoteException;

    public abstract void unregister()
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements ISipSession
    {
        private static final String DESCRIPTOR = "android.net.sip.ISipSession";
        static final int TRANSACTION_answerCall = 11;
        static final int TRANSACTION_changeCall = 13;
        static final int TRANSACTION_endCall = 12;
        static final int TRANSACTION_getCallId = 6;
        static final int TRANSACTION_getLocalIp = 1;
        static final int TRANSACTION_getLocalProfile = 2;
        static final int TRANSACTION_getPeerProfile = 3;
        static final int TRANSACTION_getState = 4;
        static final int TRANSACTION_isInCall = 5;
        static final int TRANSACTION_makeCall = 10;
        static final int TRANSACTION_register = 8;
        static final int TRANSACTION_setListener = 7;
        static final int TRANSACTION_unregister = 9;

        public Stub()
        {
            attachInterface(this, "android.net.sip.ISipSession");
        }

        public static ISipSession asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.net.sip.ISipSession");
                if ((localIInterface != null) && ((localIInterface instanceof ISipSession)))
                    localObject = (ISipSession)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            }
            while (true)
            {
                return j;
                paramParcel2.writeString("android.net.sip.ISipSession");
                continue;
                paramParcel1.enforceInterface("android.net.sip.ISipSession");
                String str2 = getLocalIp();
                paramParcel2.writeNoException();
                paramParcel2.writeString(str2);
                continue;
                paramParcel1.enforceInterface("android.net.sip.ISipSession");
                SipProfile localSipProfile3 = getLocalProfile();
                paramParcel2.writeNoException();
                if (localSipProfile3 != null)
                {
                    paramParcel2.writeInt(j);
                    localSipProfile3.writeToParcel(paramParcel2, j);
                }
                else
                {
                    paramParcel2.writeInt(0);
                    continue;
                    paramParcel1.enforceInterface("android.net.sip.ISipSession");
                    SipProfile localSipProfile2 = getPeerProfile();
                    paramParcel2.writeNoException();
                    if (localSipProfile2 != null)
                    {
                        paramParcel2.writeInt(j);
                        localSipProfile2.writeToParcel(paramParcel2, j);
                    }
                    else
                    {
                        paramParcel2.writeInt(0);
                        continue;
                        paramParcel1.enforceInterface("android.net.sip.ISipSession");
                        int k = getState();
                        paramParcel2.writeNoException();
                        paramParcel2.writeInt(k);
                        continue;
                        paramParcel1.enforceInterface("android.net.sip.ISipSession");
                        boolean bool = isInCall();
                        paramParcel2.writeNoException();
                        if (bool)
                            i = j;
                        paramParcel2.writeInt(i);
                        continue;
                        paramParcel1.enforceInterface("android.net.sip.ISipSession");
                        String str1 = getCallId();
                        paramParcel2.writeNoException();
                        paramParcel2.writeString(str1);
                        continue;
                        paramParcel1.enforceInterface("android.net.sip.ISipSession");
                        setListener(ISipSessionListener.Stub.asInterface(paramParcel1.readStrongBinder()));
                        paramParcel2.writeNoException();
                        continue;
                        paramParcel1.enforceInterface("android.net.sip.ISipSession");
                        register(paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        continue;
                        paramParcel1.enforceInterface("android.net.sip.ISipSession");
                        unregister();
                        paramParcel2.writeNoException();
                        continue;
                        paramParcel1.enforceInterface("android.net.sip.ISipSession");
                        if (paramParcel1.readInt() != 0);
                        for (SipProfile localSipProfile1 = (SipProfile)SipProfile.CREATOR.createFromParcel(paramParcel1); ; localSipProfile1 = null)
                        {
                            makeCall(localSipProfile1, paramParcel1.readString(), paramParcel1.readInt());
                            paramParcel2.writeNoException();
                            break;
                        }
                        paramParcel1.enforceInterface("android.net.sip.ISipSession");
                        answerCall(paramParcel1.readString(), paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        continue;
                        paramParcel1.enforceInterface("android.net.sip.ISipSession");
                        endCall();
                        paramParcel2.writeNoException();
                        continue;
                        paramParcel1.enforceInterface("android.net.sip.ISipSession");
                        changeCall(paramParcel1.readString(), paramParcel1.readInt());
                        paramParcel2.writeNoException();
                    }
                }
            }
        }

        private static class Proxy
            implements ISipSession
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public void answerCall(String paramString, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSession");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(11, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void changeCall(String paramString, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSession");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(13, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void endCall()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSession");
                    this.mRemote.transact(12, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getCallId()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSession");
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.net.sip.ISipSession";
            }

            public String getLocalIp()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSession");
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public SipProfile getLocalProfile()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSession");
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localSipProfile = (SipProfile)SipProfile.CREATOR.createFromParcel(localParcel2);
                        return localSipProfile;
                    }
                    SipProfile localSipProfile = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public SipProfile getPeerProfile()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSession");
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localSipProfile = (SipProfile)SipProfile.CREATOR.createFromParcel(localParcel2);
                        return localSipProfile;
                    }
                    SipProfile localSipProfile = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getState()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSession");
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isInCall()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSession");
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void makeCall(SipProfile paramSipProfile, String paramString, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSession");
                    if (paramSipProfile != null)
                    {
                        localParcel1.writeInt(1);
                        paramSipProfile.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeString(paramString);
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(10, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void register(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSession");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setListener(ISipSessionListener paramISipSessionListener)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSession");
                    if (paramISipSessionListener != null)
                    {
                        localIBinder = paramISipSessionListener.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(7, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void unregister()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSession");
                    this.mRemote.transact(9, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.sip.ISipSession
 * JD-Core Version:        0.6.2
 */