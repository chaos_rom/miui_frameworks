package android.net.sip;

public class SipErrorCode
{
    public static final int CLIENT_ERROR = -4;
    public static final int CROSS_DOMAIN_AUTHENTICATION = -11;
    public static final int DATA_CONNECTION_LOST = -10;
    public static final int INVALID_CREDENTIALS = -8;
    public static final int INVALID_REMOTE_URI = -6;
    public static final int IN_PROGRESS = -9;
    public static final int NO_ERROR = 0;
    public static final int PEER_NOT_REACHABLE = -7;
    public static final int SERVER_ERROR = -2;
    public static final int SERVER_UNREACHABLE = -12;
    public static final int SOCKET_ERROR = -1;
    public static final int TIME_OUT = -5;
    public static final int TRANSACTION_TERMINTED = -3;

    public static String toString(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = "UNKNOWN";
        case 0:
        case -1:
        case -2:
        case -3:
        case -4:
        case -5:
        case -6:
        case -7:
        case -8:
        case -9:
        case -10:
        case -11:
        case -12:
        }
        while (true)
        {
            return str;
            str = "NO_ERROR";
            continue;
            str = "SOCKET_ERROR";
            continue;
            str = "SERVER_ERROR";
            continue;
            str = "TRANSACTION_TERMINTED";
            continue;
            str = "CLIENT_ERROR";
            continue;
            str = "TIME_OUT";
            continue;
            str = "INVALID_REMOTE_URI";
            continue;
            str = "PEER_NOT_REACHABLE";
            continue;
            str = "INVALID_CREDENTIALS";
            continue;
            str = "IN_PROGRESS";
            continue;
            str = "DATA_CONNECTION_LOST";
            continue;
            str = "CROSS_DOMAIN_AUTHENTICATION";
            continue;
            str = "SERVER_UNREACHABLE";
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.sip.SipErrorCode
 * JD-Core Version:        0.6.2
 */