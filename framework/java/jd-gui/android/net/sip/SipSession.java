package android.net.sip;

import android.os.RemoteException;
import android.util.Log;

public final class SipSession
{
    private static final String TAG = "SipSession";
    private Listener mListener;
    private final ISipSession mSession;

    SipSession(ISipSession paramISipSession)
    {
        this.mSession = paramISipSession;
        if (paramISipSession != null);
        try
        {
            paramISipSession.setListener(createListener());
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("SipSession", "SipSession.setListener(): " + localRemoteException);
        }
    }

    SipSession(ISipSession paramISipSession, Listener paramListener)
    {
        this(paramISipSession);
        setListener(paramListener);
    }

    private ISipSessionListener createListener()
    {
        return new ISipSessionListener.Stub()
        {
            public void onCallBusy(ISipSession paramAnonymousISipSession)
            {
                if (SipSession.this.mListener != null)
                    SipSession.this.mListener.onCallBusy(SipSession.this);
            }

            public void onCallChangeFailed(ISipSession paramAnonymousISipSession, int paramAnonymousInt, String paramAnonymousString)
            {
                if (SipSession.this.mListener != null)
                    SipSession.this.mListener.onCallChangeFailed(SipSession.this, paramAnonymousInt, paramAnonymousString);
            }

            public void onCallEnded(ISipSession paramAnonymousISipSession)
            {
                if (SipSession.this.mListener != null)
                    SipSession.this.mListener.onCallEnded(SipSession.this);
            }

            public void onCallEstablished(ISipSession paramAnonymousISipSession, String paramAnonymousString)
            {
                if (SipSession.this.mListener != null)
                    SipSession.this.mListener.onCallEstablished(SipSession.this, paramAnonymousString);
            }

            public void onCallTransferring(ISipSession paramAnonymousISipSession, String paramAnonymousString)
            {
                if (SipSession.this.mListener != null)
                    SipSession.this.mListener.onCallTransferring(new SipSession(paramAnonymousISipSession, SipSession.this.mListener), paramAnonymousString);
            }

            public void onCalling(ISipSession paramAnonymousISipSession)
            {
                if (SipSession.this.mListener != null)
                    SipSession.this.mListener.onCalling(SipSession.this);
            }

            public void onError(ISipSession paramAnonymousISipSession, int paramAnonymousInt, String paramAnonymousString)
            {
                if (SipSession.this.mListener != null)
                    SipSession.this.mListener.onError(SipSession.this, paramAnonymousInt, paramAnonymousString);
            }

            public void onRegistering(ISipSession paramAnonymousISipSession)
            {
                if (SipSession.this.mListener != null)
                    SipSession.this.mListener.onRegistering(SipSession.this);
            }

            public void onRegistrationDone(ISipSession paramAnonymousISipSession, int paramAnonymousInt)
            {
                if (SipSession.this.mListener != null)
                    SipSession.this.mListener.onRegistrationDone(SipSession.this, paramAnonymousInt);
            }

            public void onRegistrationFailed(ISipSession paramAnonymousISipSession, int paramAnonymousInt, String paramAnonymousString)
            {
                if (SipSession.this.mListener != null)
                    SipSession.this.mListener.onRegistrationFailed(SipSession.this, paramAnonymousInt, paramAnonymousString);
            }

            public void onRegistrationTimeout(ISipSession paramAnonymousISipSession)
            {
                if (SipSession.this.mListener != null)
                    SipSession.this.mListener.onRegistrationTimeout(SipSession.this);
            }

            public void onRinging(ISipSession paramAnonymousISipSession, SipProfile paramAnonymousSipProfile, String paramAnonymousString)
            {
                if (SipSession.this.mListener != null)
                    SipSession.this.mListener.onRinging(SipSession.this, paramAnonymousSipProfile, paramAnonymousString);
            }

            public void onRingingBack(ISipSession paramAnonymousISipSession)
            {
                if (SipSession.this.mListener != null)
                    SipSession.this.mListener.onRingingBack(SipSession.this);
            }
        };
    }

    public void answerCall(String paramString, int paramInt)
    {
        try
        {
            this.mSession.answerCall(paramString, paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("SipSession", "answerCall(): " + localRemoteException);
        }
    }

    public void changeCall(String paramString, int paramInt)
    {
        try
        {
            this.mSession.changeCall(paramString, paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("SipSession", "changeCall(): " + localRemoteException);
        }
    }

    public void endCall()
    {
        try
        {
            this.mSession.endCall();
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("SipSession", "endCall(): " + localRemoteException);
        }
    }

    public String getCallId()
    {
        try
        {
            String str2 = this.mSession.getCallId();
            str1 = str2;
            return str1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("SipSession", "getCallId(): " + localRemoteException);
                String str1 = null;
            }
        }
    }

    public String getLocalIp()
    {
        try
        {
            String str2 = this.mSession.getLocalIp();
            str1 = str2;
            return str1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("SipSession", "getLocalIp(): " + localRemoteException);
                String str1 = "127.0.0.1";
            }
        }
    }

    public SipProfile getLocalProfile()
    {
        try
        {
            SipProfile localSipProfile2 = this.mSession.getLocalProfile();
            localSipProfile1 = localSipProfile2;
            return localSipProfile1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("SipSession", "getLocalProfile(): " + localRemoteException);
                SipProfile localSipProfile1 = null;
            }
        }
    }

    public SipProfile getPeerProfile()
    {
        try
        {
            SipProfile localSipProfile2 = this.mSession.getPeerProfile();
            localSipProfile1 = localSipProfile2;
            return localSipProfile1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("SipSession", "getPeerProfile(): " + localRemoteException);
                SipProfile localSipProfile1 = null;
            }
        }
    }

    ISipSession getRealSession()
    {
        return this.mSession;
    }

    public int getState()
    {
        try
        {
            int j = this.mSession.getState();
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("SipSession", "getState(): " + localRemoteException);
                int i = 101;
            }
        }
    }

    public boolean isInCall()
    {
        try
        {
            boolean bool2 = this.mSession.isInCall();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
            {
                Log.e("SipSession", "isInCall(): " + localRemoteException);
                boolean bool1 = false;
            }
        }
    }

    public void makeCall(SipProfile paramSipProfile, String paramString, int paramInt)
    {
        try
        {
            this.mSession.makeCall(paramSipProfile, paramString, paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("SipSession", "makeCall(): " + localRemoteException);
        }
    }

    public void register(int paramInt)
    {
        try
        {
            this.mSession.register(paramInt);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("SipSession", "register(): " + localRemoteException);
        }
    }

    public void setListener(Listener paramListener)
    {
        this.mListener = paramListener;
    }

    public void unregister()
    {
        try
        {
            this.mSession.unregister();
            return;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Log.e("SipSession", "unregister(): " + localRemoteException);
        }
    }

    public static class Listener
    {
        public void onCallBusy(SipSession paramSipSession)
        {
        }

        public void onCallChangeFailed(SipSession paramSipSession, int paramInt, String paramString)
        {
        }

        public void onCallEnded(SipSession paramSipSession)
        {
        }

        public void onCallEstablished(SipSession paramSipSession, String paramString)
        {
        }

        public void onCallTransferring(SipSession paramSipSession, String paramString)
        {
        }

        public void onCalling(SipSession paramSipSession)
        {
        }

        public void onError(SipSession paramSipSession, int paramInt, String paramString)
        {
        }

        public void onRegistering(SipSession paramSipSession)
        {
        }

        public void onRegistrationDone(SipSession paramSipSession, int paramInt)
        {
        }

        public void onRegistrationFailed(SipSession paramSipSession, int paramInt, String paramString)
        {
        }

        public void onRegistrationTimeout(SipSession paramSipSession)
        {
        }

        public void onRinging(SipSession paramSipSession, SipProfile paramSipProfile, String paramString)
        {
        }

        public void onRingingBack(SipSession paramSipSession)
        {
        }
    }

    public static class State
    {
        public static final int DEREGISTERING = 2;
        public static final int ENDING_CALL = 10;
        public static final int INCOMING_CALL = 3;
        public static final int INCOMING_CALL_ANSWERING = 4;
        public static final int IN_CALL = 8;
        public static final int NOT_DEFINED = 101;
        public static final int OUTGOING_CALL = 5;
        public static final int OUTGOING_CALL_CANCELING = 7;
        public static final int OUTGOING_CALL_RING_BACK = 6;
        public static final int PINGING = 9;
        public static final int READY_TO_CALL = 0;
        public static final int REGISTERING = 1;

        public static String toString(int paramInt)
        {
            String str;
            switch (paramInt)
            {
            default:
                str = "NOT_DEFINED";
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            }
            while (true)
            {
                return str;
                str = "READY_TO_CALL";
                continue;
                str = "REGISTERING";
                continue;
                str = "DEREGISTERING";
                continue;
                str = "INCOMING_CALL";
                continue;
                str = "INCOMING_CALL_ANSWERING";
                continue;
                str = "OUTGOING_CALL";
                continue;
                str = "OUTGOING_CALL_RING_BACK";
                continue;
                str = "OUTGOING_CALL_CANCELING";
                continue;
                str = "IN_CALL";
                continue;
                str = "PINGING";
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.sip.SipSession
 * JD-Core Version:        0.6.2
 */