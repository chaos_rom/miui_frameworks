package android.net.sip;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class SimpleSessionDescription
{
    private final Fields mFields = new Fields("voscbtka");
    private final ArrayList<Media> mMedia = new ArrayList();

    public SimpleSessionDescription(long paramLong, String paramString)
    {
        StringBuilder localStringBuilder = new StringBuilder();
        if (paramString.indexOf(':') < 0);
        for (String str1 = "IN IP4 "; ; str1 = "IN IP6 ")
        {
            String str2 = str1 + paramString;
            this.mFields.parse("v=0");
            Fields localFields = this.mFields;
            Object[] arrayOfObject = new Object[3];
            arrayOfObject[0] = Long.valueOf(paramLong);
            arrayOfObject[1] = Long.valueOf(System.currentTimeMillis());
            arrayOfObject[2] = str2;
            localFields.parse(String.format("o=- %d %d %s", arrayOfObject));
            this.mFields.parse("s=-");
            this.mFields.parse("t=0 0");
            this.mFields.parse("c=" + str2);
            return;
        }
    }

    public SimpleSessionDescription(String paramString)
    {
        String[] arrayOfString1 = paramString.trim().replaceAll(" +", " ").split("[\r\n]+");
        Object localObject = this.mFields;
        int i = arrayOfString1.length;
        for (int j = 0; ; j++)
        {
            Media localMedia;
            if (j < i)
            {
                String str1 = arrayOfString1[j];
                try
                {
                    if (str1.charAt(1) != '=')
                        throw new IllegalArgumentException();
                }
                catch (Exception localException)
                {
                    throw new IllegalArgumentException("Invalid SDP: " + str1);
                }
                if (str1.charAt(0) == 'm')
                {
                    String[] arrayOfString2 = str1.substring(2).split(" ", 4);
                    String[] arrayOfString3 = arrayOfString2[1].split("/", 2);
                    String str2 = arrayOfString2[0];
                    int k = Integer.parseInt(arrayOfString3[0]);
                    if (arrayOfString3.length < 2);
                    for (int m = 1; ; m = Integer.parseInt(arrayOfString3[1]))
                    {
                        String str3 = arrayOfString2[2];
                        localMedia = newMedia(str2, k, m, str3);
                        String[] arrayOfString4 = arrayOfString2[3].split(" ");
                        int n = arrayOfString4.length;
                        for (int i1 = 0; i1 < n; i1++)
                            localMedia.setFormat(arrayOfString4[i1], null);
                    }
                }
                ((Fields)localObject).parse(str1);
            }
            else
            {
                return;
                localObject = localMedia;
            }
        }
    }

    public String encode()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        this.mFields.write(localStringBuilder);
        Iterator localIterator = this.mMedia.iterator();
        while (localIterator.hasNext())
            ((Media)localIterator.next()).write(localStringBuilder);
        return localStringBuilder.toString();
    }

    public String getAddress()
    {
        return this.mFields.getAddress();
    }

    public String getAttribute(String paramString)
    {
        return this.mFields.getAttribute(paramString);
    }

    public String[] getAttributeNames()
    {
        return this.mFields.getAttributeNames();
    }

    public int getBandwidth(String paramString)
    {
        return this.mFields.getBandwidth(paramString);
    }

    public String[] getBandwidthTypes()
    {
        return this.mFields.getBandwidthTypes();
    }

    public String getEncryptionKey()
    {
        return this.mFields.getEncryptionKey();
    }

    public String getEncryptionMethod()
    {
        return this.mFields.getEncryptionMethod();
    }

    public Media[] getMedia()
    {
        return (Media[])this.mMedia.toArray(new Media[this.mMedia.size()]);
    }

    public Media newMedia(String paramString1, int paramInt1, int paramInt2, String paramString2)
    {
        Media localMedia = new Media(paramString1, paramInt1, paramInt2, paramString2, null);
        this.mMedia.add(localMedia);
        return localMedia;
    }

    public void setAddress(String paramString)
    {
        this.mFields.setAddress(paramString);
    }

    public void setAttribute(String paramString1, String paramString2)
    {
        this.mFields.setAttribute(paramString1, paramString2);
    }

    public void setBandwidth(String paramString, int paramInt)
    {
        this.mFields.setBandwidth(paramString, paramInt);
    }

    public void setEncryption(String paramString1, String paramString2)
    {
        this.mFields.setEncryption(paramString1, paramString2);
    }

    private static class Fields
    {
        private final ArrayList<String> mLines = new ArrayList();
        private final String mOrder;

        Fields(String paramString)
        {
            this.mOrder = paramString;
        }

        private String[] cut(String paramString, char paramChar)
        {
            String[] arrayOfString = new String[this.mLines.size()];
            int i = 0;
            Iterator localIterator = this.mLines.iterator();
            while (localIterator.hasNext())
            {
                String str = (String)localIterator.next();
                if (str.startsWith(paramString))
                {
                    int j = str.indexOf(paramChar);
                    if (j == -1)
                        j = str.length();
                    arrayOfString[i] = str.substring(paramString.length(), j);
                    i++;
                }
            }
            return (String[])Arrays.copyOf(arrayOfString, i);
        }

        private int find(String paramString, char paramChar)
        {
            int i = paramString.length();
            int j = -1 + this.mLines.size();
            if (j >= 0)
            {
                String str = (String)this.mLines.get(j);
                if ((!str.startsWith(paramString)) || ((str.length() != i) && (str.charAt(i) != paramChar)));
            }
            while (true)
            {
                return j;
                j--;
                break;
                j = -1;
            }
        }

        private String get(String paramString, char paramChar)
        {
            int i = find(paramString, paramChar);
            String str2;
            if (i == -1)
                str2 = null;
            while (true)
            {
                return str2;
                String str1 = (String)this.mLines.get(i);
                int j = paramString.length();
                if (str1.length() == j)
                    str2 = "";
                else
                    str2 = str1.substring(j + 1);
            }
        }

        private void parse(String paramString)
        {
            int i = paramString.charAt(0);
            if (this.mOrder.indexOf(i) == -1);
            while (true)
            {
                return;
                char c = '=';
                if ((paramString.startsWith("a=rtpmap:")) || (paramString.startsWith("a=fmtp:")));
                int j;
                for (c = ' '; ; c = ':')
                    do
                    {
                        j = paramString.indexOf(c);
                        if (j != -1)
                            break label87;
                        set(paramString, c, "");
                        break;
                    }
                    while ((i != 98) && (i != 97));
                label87: set(paramString.substring(0, j), c, paramString.substring(j + 1));
            }
        }

        private void set(String paramString1, char paramChar, String paramString2)
        {
            int i = find(paramString1, paramChar);
            if (paramString2 != null)
            {
                if (paramString2.length() != 0)
                    paramString1 = paramString1 + paramChar + paramString2;
                if (i == -1)
                    this.mLines.add(paramString1);
            }
            while (true)
            {
                return;
                this.mLines.set(i, paramString1);
                continue;
                if (i != -1)
                    this.mLines.remove(i);
            }
        }

        private void write(StringBuilder paramStringBuilder)
        {
            for (int i = 0; i < this.mOrder.length(); i++)
            {
                int j = this.mOrder.charAt(i);
                Iterator localIterator = this.mLines.iterator();
                while (localIterator.hasNext())
                {
                    String str = (String)localIterator.next();
                    if (str.charAt(0) == j)
                        paramStringBuilder.append(str).append("\r\n");
                }
            }
        }

        public String getAddress()
        {
            String str1 = null;
            String str2 = get("c", '=');
            if (str2 == null);
            while (true)
            {
                return str1;
                String[] arrayOfString = str2.split(" ");
                if (arrayOfString.length == 3)
                {
                    int i = arrayOfString[2].indexOf('/');
                    if (i < 0)
                        str1 = arrayOfString[2];
                    else
                        str1 = arrayOfString[2].substring(0, i);
                }
            }
        }

        public String getAttribute(String paramString)
        {
            return get("a=" + paramString, ':');
        }

        public String[] getAttributeNames()
        {
            return cut("a=", ':');
        }

        public int getBandwidth(String paramString)
        {
            int i = -1;
            String str = get("b=" + paramString, ':');
            if (str != null);
            try
            {
                int j = Integer.parseInt(str);
                i = j;
                return i;
            }
            catch (NumberFormatException localNumberFormatException)
            {
                while (true)
                    setBandwidth(paramString, i);
            }
        }

        public String[] getBandwidthTypes()
        {
            return cut("b=", ':');
        }

        public String getEncryptionKey()
        {
            String str1 = null;
            String str2 = get("k", '=');
            if (str2 == null);
            while (true)
            {
                return str1;
                int i = str2.indexOf(':');
                if (i != -1)
                    str1 = str2.substring(0, i + 1);
            }
        }

        public String getEncryptionMethod()
        {
            String str = get("k", '=');
            if (str == null)
                str = null;
            while (true)
            {
                return str;
                int i = str.indexOf(':');
                if (i != -1)
                    str = str.substring(0, i);
            }
        }

        public void setAddress(String paramString)
        {
            StringBuilder localStringBuilder;
            if (paramString != null)
            {
                localStringBuilder = new StringBuilder();
                if (paramString.indexOf(':') >= 0)
                    break label47;
            }
            label47: for (String str = "IN IP4 "; ; str = "IN IP6 ")
            {
                paramString = str + paramString;
                set("c", '=', paramString);
                return;
            }
        }

        public void setAttribute(String paramString1, String paramString2)
        {
            set("a=" + paramString1, ':', paramString2);
        }

        public void setBandwidth(String paramString, int paramInt)
        {
            String str1 = "b=" + paramString;
            if (paramInt < 0);
            for (String str2 = null; ; str2 = String.valueOf(paramInt))
            {
                set(str1, ':', str2);
                return;
            }
        }

        public void setEncryption(String paramString1, String paramString2)
        {
            if ((paramString1 == null) || (paramString2 == null));
            while (true)
            {
                set("k", '=', paramString1);
                return;
                paramString1 = paramString1 + ':' + paramString2;
            }
        }
    }

    public static class Media extends SimpleSessionDescription.Fields
    {
        private ArrayList<String> mFormats = new ArrayList();
        private final int mPort;
        private final int mPortCount;
        private final String mProtocol;
        private final String mType;

        private Media(String paramString1, int paramInt1, int paramInt2, String paramString2)
        {
            super();
            this.mType = paramString1;
            this.mPort = paramInt1;
            this.mPortCount = paramInt2;
            this.mProtocol = paramString2;
        }

        private void write(StringBuilder paramStringBuilder)
        {
            paramStringBuilder.append("m=").append(this.mType).append(' ').append(this.mPort);
            if (this.mPortCount != 1)
                paramStringBuilder.append('/').append(this.mPortCount);
            paramStringBuilder.append(' ').append(this.mProtocol);
            Iterator localIterator = this.mFormats.iterator();
            while (localIterator.hasNext())
            {
                String str = (String)localIterator.next();
                paramStringBuilder.append(' ').append(str);
            }
            paramStringBuilder.append("\r\n");
            write(paramStringBuilder);
        }

        public String getFmtp(int paramInt)
        {
            return super.get("a=fmtp:" + paramInt, ' ');
        }

        public String getFmtp(String paramString)
        {
            return super.get("a=fmtp:" + paramString, ' ');
        }

        public String[] getFormats()
        {
            return (String[])this.mFormats.toArray(new String[this.mFormats.size()]);
        }

        public int getPort()
        {
            return this.mPort;
        }

        public int getPortCount()
        {
            return this.mPortCount;
        }

        public String getProtocol()
        {
            return this.mProtocol;
        }

        public int[] getRtpPayloadTypes()
        {
            int[] arrayOfInt = new int[this.mFormats.size()];
            int i = 0;
            Iterator localIterator = this.mFormats.iterator();
            while (true)
            {
                String str;
                if (localIterator.hasNext())
                    str = (String)localIterator.next();
                try
                {
                    arrayOfInt[i] = Integer.parseInt(str);
                    i++;
                    continue;
                    return Arrays.copyOf(arrayOfInt, i);
                }
                catch (NumberFormatException localNumberFormatException)
                {
                }
            }
        }

        public String getRtpmap(int paramInt)
        {
            return super.get("a=rtpmap:" + paramInt, ' ');
        }

        public String getType()
        {
            return this.mType;
        }

        public void removeFormat(String paramString)
        {
            this.mFormats.remove(paramString);
            super.set("a=rtpmap:" + paramString, ' ', null);
            super.set("a=fmtp:" + paramString, ' ', null);
        }

        public void removeRtpPayload(int paramInt)
        {
            removeFormat(String.valueOf(paramInt));
        }

        public void setFormat(String paramString1, String paramString2)
        {
            this.mFormats.remove(paramString1);
            this.mFormats.add(paramString1);
            super.set("a=rtpmap:" + paramString1, ' ', null);
            super.set("a=fmtp:" + paramString1, ' ', paramString2);
        }

        public void setRtpPayload(int paramInt, String paramString1, String paramString2)
        {
            String str = String.valueOf(paramInt);
            this.mFormats.remove(str);
            this.mFormats.add(str);
            super.set("a=rtpmap:" + str, ' ', paramString1);
            super.set("a=fmtp:" + str, ' ', paramString2);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.sip.SimpleSessionDescription
 * JD-Core Version:        0.6.2
 */