package android.net.sip;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import java.text.ParseException;

public class SipManager
{
    public static final String ACTION_SIP_ADD_PHONE = "com.android.phone.SIP_ADD_PHONE";
    public static final String ACTION_SIP_INCOMING_CALL = "com.android.phone.SIP_INCOMING_CALL";
    public static final String ACTION_SIP_REMOVE_PHONE = "com.android.phone.SIP_REMOVE_PHONE";
    public static final String ACTION_SIP_SERVICE_UP = "android.net.sip.SIP_SERVICE_UP";
    public static final String EXTRA_CALL_ID = "android:sipCallID";
    public static final String EXTRA_LOCAL_URI = "android:localSipUri";
    public static final String EXTRA_OFFER_SD = "android:sipOfferSD";
    public static final int INCOMING_CALL_RESULT_CODE = 101;
    private static final String TAG = "SipManager";
    private Context mContext;
    private ISipService mSipService;

    private SipManager(Context paramContext)
    {
        this.mContext = paramContext;
        createSipService();
    }

    public static Intent createIncomingCallBroadcast(String paramString1, String paramString2)
    {
        Intent localIntent = new Intent();
        localIntent.putExtra("android:sipCallID", paramString1);
        localIntent.putExtra("android:sipOfferSD", paramString2);
        return localIntent;
    }

    private static ISipSessionListener createRelay(SipRegistrationListener paramSipRegistrationListener, String paramString)
    {
        if (paramSipRegistrationListener == null);
        for (Object localObject = null; ; localObject = new ListenerRelay(paramSipRegistrationListener, paramString))
            return localObject;
    }

    private void createSipService()
    {
        this.mSipService = ISipService.Stub.asInterface(ServiceManager.getService("sip"));
    }

    public static String getCallId(Intent paramIntent)
    {
        return paramIntent.getStringExtra("android:sipCallID");
    }

    public static String getOfferSessionDescription(Intent paramIntent)
    {
        return paramIntent.getStringExtra("android:sipOfferSD");
    }

    public static boolean isApiSupported(Context paramContext)
    {
        return paramContext.getPackageManager().hasSystemFeature("android.software.sip");
    }

    public static boolean isIncomingCallIntent(Intent paramIntent)
    {
        boolean bool = false;
        if (paramIntent == null);
        while (true)
        {
            return bool;
            String str1 = getCallId(paramIntent);
            String str2 = getOfferSessionDescription(paramIntent);
            if ((str1 != null) && (str2 != null))
                bool = true;
        }
    }

    public static boolean isSipWifiOnly(Context paramContext)
    {
        return paramContext.getResources().getBoolean(17891371);
    }

    public static boolean isVoipSupported(Context paramContext)
    {
        if ((paramContext.getPackageManager().hasSystemFeature("android.software.sip.voip")) && (isApiSupported(paramContext)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static SipManager newInstance(Context paramContext)
    {
        if (isApiSupported(paramContext));
        for (SipManager localSipManager = new SipManager(paramContext); ; localSipManager = null)
            return localSipManager;
    }

    public void close(String paramString)
        throws SipException
    {
        try
        {
            this.mSipService.close(paramString);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new SipException("close()", localRemoteException);
        }
    }

    public SipSession createSipSession(SipProfile paramSipProfile, SipSession.Listener paramListener)
        throws SipException
    {
        ISipSession localISipSession;
        try
        {
            localISipSession = this.mSipService.createSession(paramSipProfile, null);
            if (localISipSession == null)
                throw new SipException("Failed to create SipSession; network unavailable?");
        }
        catch (RemoteException localRemoteException)
        {
            throw new SipException("createSipSession()", localRemoteException);
        }
        SipSession localSipSession = new SipSession(localISipSession, paramListener);
        return localSipSession;
    }

    public SipProfile[] getListOfProfiles()
    {
        try
        {
            SipProfile[] arrayOfSipProfile2 = this.mSipService.getListOfProfiles();
            arrayOfSipProfile1 = arrayOfSipProfile2;
            return arrayOfSipProfile1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                SipProfile[] arrayOfSipProfile1 = new SipProfile[0];
        }
    }

    public SipSession getSessionFor(Intent paramIntent)
        throws SipException
    {
        SipSession localSipSession;
        try
        {
            String str = getCallId(paramIntent);
            ISipSession localISipSession = this.mSipService.getPendingSession(str);
            if (localISipSession == null)
                localSipSession = null;
            else
                localSipSession = new SipSession(localISipSession);
        }
        catch (RemoteException localRemoteException)
        {
            throw new SipException("getSessionFor()", localRemoteException);
        }
        return localSipSession;
    }

    public boolean isOpened(String paramString)
        throws SipException
    {
        try
        {
            boolean bool = this.mSipService.isOpened(paramString);
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            throw new SipException("isOpened()", localRemoteException);
        }
    }

    public boolean isRegistered(String paramString)
        throws SipException
    {
        try
        {
            boolean bool = this.mSipService.isRegistered(paramString);
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            throw new SipException("isRegistered()", localRemoteException);
        }
    }

    public SipAudioCall makeAudioCall(SipProfile paramSipProfile1, SipProfile paramSipProfile2, SipAudioCall.Listener paramListener, int paramInt)
        throws SipException
    {
        if (!isVoipSupported(this.mContext))
            throw new SipException("VOIP API is not supported");
        SipAudioCall localSipAudioCall = new SipAudioCall(this.mContext, paramSipProfile1);
        localSipAudioCall.setListener(paramListener);
        localSipAudioCall.makeCall(paramSipProfile2, createSipSession(paramSipProfile1, null), paramInt);
        return localSipAudioCall;
    }

    public SipAudioCall makeAudioCall(String paramString1, String paramString2, SipAudioCall.Listener paramListener, int paramInt)
        throws SipException
    {
        if (!isVoipSupported(this.mContext))
            throw new SipException("VOIP API is not supported");
        try
        {
            SipAudioCall localSipAudioCall = makeAudioCall(new SipProfile.Builder(paramString1).build(), new SipProfile.Builder(paramString2).build(), paramListener, paramInt);
            return localSipAudioCall;
        }
        catch (ParseException localParseException)
        {
            throw new SipException("build SipProfile", localParseException);
        }
    }

    public void open(SipProfile paramSipProfile)
        throws SipException
    {
        try
        {
            this.mSipService.open(paramSipProfile);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new SipException("open()", localRemoteException);
        }
    }

    public void open(SipProfile paramSipProfile, PendingIntent paramPendingIntent, SipRegistrationListener paramSipRegistrationListener)
        throws SipException
    {
        if (paramPendingIntent == null)
            throw new NullPointerException("incomingCallPendingIntent cannot be null");
        try
        {
            this.mSipService.open3(paramSipProfile, paramPendingIntent, createRelay(paramSipRegistrationListener, paramSipProfile.getUriString()));
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new SipException("open()", localRemoteException);
        }
    }

    public void register(SipProfile paramSipProfile, int paramInt, SipRegistrationListener paramSipRegistrationListener)
        throws SipException
    {
        ISipSession localISipSession;
        try
        {
            localISipSession = this.mSipService.createSession(paramSipProfile, createRelay(paramSipRegistrationListener, paramSipProfile.getUriString()));
            if (localISipSession == null)
                throw new SipException("SipService.createSession() returns null");
        }
        catch (RemoteException localRemoteException)
        {
            throw new SipException("register()", localRemoteException);
        }
        localISipSession.register(paramInt);
    }

    public void setRegistrationListener(String paramString, SipRegistrationListener paramSipRegistrationListener)
        throws SipException
    {
        try
        {
            this.mSipService.setRegistrationListener(paramString, createRelay(paramSipRegistrationListener, paramString));
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new SipException("setRegistrationListener()", localRemoteException);
        }
    }

    public SipAudioCall takeAudioCall(Intent paramIntent, SipAudioCall.Listener paramListener)
        throws SipException
    {
        if (paramIntent == null)
            throw new SipException("Cannot retrieve session with null intent");
        String str1 = getCallId(paramIntent);
        if (str1 == null)
            throw new SipException("Call ID missing in incoming call intent");
        String str2 = getOfferSessionDescription(paramIntent);
        if (str2 == null)
            throw new SipException("Session description missing in incoming call intent");
        ISipSession localISipSession;
        try
        {
            localISipSession = this.mSipService.getPendingSession(str1);
            if (localISipSession == null)
                throw new SipException("No pending session for the call");
        }
        catch (Throwable localThrowable)
        {
            throw new SipException("takeAudioCall()", localThrowable);
        }
        SipAudioCall localSipAudioCall = new SipAudioCall(this.mContext, localISipSession.getLocalProfile());
        localSipAudioCall.attachCall(new SipSession(localISipSession), str2);
        localSipAudioCall.setListener(paramListener);
        return localSipAudioCall;
    }

    public void unregister(SipProfile paramSipProfile, SipRegistrationListener paramSipRegistrationListener)
        throws SipException
    {
        ISipSession localISipSession;
        try
        {
            localISipSession = this.mSipService.createSession(paramSipProfile, createRelay(paramSipRegistrationListener, paramSipProfile.getUriString()));
            if (localISipSession == null)
                throw new SipException("SipService.createSession() returns null");
        }
        catch (RemoteException localRemoteException)
        {
            throw new SipException("unregister()", localRemoteException);
        }
        localISipSession.unregister();
    }

    private static class ListenerRelay extends SipSessionAdapter
    {
        private SipRegistrationListener mListener;
        private String mUri;

        public ListenerRelay(SipRegistrationListener paramSipRegistrationListener, String paramString)
        {
            this.mListener = paramSipRegistrationListener;
            this.mUri = paramString;
        }

        private String getUri(ISipSession paramISipSession)
        {
            if (paramISipSession == null);
            Object localObject;
            try
            {
                localObject = this.mUri;
                break label60;
                String str = paramISipSession.getLocalProfile().getUriString();
                localObject = str;
            }
            catch (Throwable localThrowable)
            {
                Log.w("SipManager", "getUri(): " + localThrowable);
                localObject = null;
            }
            label60: return localObject;
        }

        public void onRegistering(ISipSession paramISipSession)
        {
            this.mListener.onRegistering(getUri(paramISipSession));
        }

        public void onRegistrationDone(ISipSession paramISipSession, int paramInt)
        {
            long l = paramInt;
            if (paramInt > 0)
                l += System.currentTimeMillis();
            this.mListener.onRegistrationDone(getUri(paramISipSession), l);
        }

        public void onRegistrationFailed(ISipSession paramISipSession, int paramInt, String paramString)
        {
            this.mListener.onRegistrationFailed(getUri(paramISipSession), paramInt, paramString);
        }

        public void onRegistrationTimeout(ISipSession paramISipSession)
        {
            this.mListener.onRegistrationFailed(getUri(paramISipSession), -5, "registration timed out");
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.sip.SipManager
 * JD-Core Version:        0.6.2
 */