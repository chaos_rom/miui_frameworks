package android.net.sip;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface ISipSessionListener extends IInterface
{
    public abstract void onCallBusy(ISipSession paramISipSession)
        throws RemoteException;

    public abstract void onCallChangeFailed(ISipSession paramISipSession, int paramInt, String paramString)
        throws RemoteException;

    public abstract void onCallEnded(ISipSession paramISipSession)
        throws RemoteException;

    public abstract void onCallEstablished(ISipSession paramISipSession, String paramString)
        throws RemoteException;

    public abstract void onCallTransferring(ISipSession paramISipSession, String paramString)
        throws RemoteException;

    public abstract void onCalling(ISipSession paramISipSession)
        throws RemoteException;

    public abstract void onError(ISipSession paramISipSession, int paramInt, String paramString)
        throws RemoteException;

    public abstract void onRegistering(ISipSession paramISipSession)
        throws RemoteException;

    public abstract void onRegistrationDone(ISipSession paramISipSession, int paramInt)
        throws RemoteException;

    public abstract void onRegistrationFailed(ISipSession paramISipSession, int paramInt, String paramString)
        throws RemoteException;

    public abstract void onRegistrationTimeout(ISipSession paramISipSession)
        throws RemoteException;

    public abstract void onRinging(ISipSession paramISipSession, SipProfile paramSipProfile, String paramString)
        throws RemoteException;

    public abstract void onRingingBack(ISipSession paramISipSession)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements ISipSessionListener
    {
        private static final String DESCRIPTOR = "android.net.sip.ISipSessionListener";
        static final int TRANSACTION_onCallBusy = 6;
        static final int TRANSACTION_onCallChangeFailed = 9;
        static final int TRANSACTION_onCallEnded = 5;
        static final int TRANSACTION_onCallEstablished = 4;
        static final int TRANSACTION_onCallTransferring = 7;
        static final int TRANSACTION_onCalling = 1;
        static final int TRANSACTION_onError = 8;
        static final int TRANSACTION_onRegistering = 10;
        static final int TRANSACTION_onRegistrationDone = 11;
        static final int TRANSACTION_onRegistrationFailed = 12;
        static final int TRANSACTION_onRegistrationTimeout = 13;
        static final int TRANSACTION_onRinging = 2;
        static final int TRANSACTION_onRingingBack = 3;

        public Stub()
        {
            attachInterface(this, "android.net.sip.ISipSessionListener");
        }

        public static ISipSessionListener asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.net.sip.ISipSessionListener");
                if ((localIInterface != null) && ((localIInterface instanceof ISipSessionListener)))
                    localObject = (ISipSessionListener)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.net.sip.ISipSessionListener");
                continue;
                paramParcel1.enforceInterface("android.net.sip.ISipSessionListener");
                onCalling(ISipSession.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.net.sip.ISipSessionListener");
                ISipSession localISipSession = ISipSession.Stub.asInterface(paramParcel1.readStrongBinder());
                if (paramParcel1.readInt() != 0);
                for (SipProfile localSipProfile = (SipProfile)SipProfile.CREATOR.createFromParcel(paramParcel1); ; localSipProfile = null)
                {
                    onRinging(localISipSession, localSipProfile, paramParcel1.readString());
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.net.sip.ISipSessionListener");
                onRingingBack(ISipSession.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.net.sip.ISipSessionListener");
                onCallEstablished(ISipSession.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readString());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.net.sip.ISipSessionListener");
                onCallEnded(ISipSession.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.net.sip.ISipSessionListener");
                onCallBusy(ISipSession.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.net.sip.ISipSessionListener");
                onCallTransferring(ISipSession.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readString());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.net.sip.ISipSessionListener");
                onError(ISipSession.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readInt(), paramParcel1.readString());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.net.sip.ISipSessionListener");
                onCallChangeFailed(ISipSession.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readInt(), paramParcel1.readString());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.net.sip.ISipSessionListener");
                onRegistering(ISipSession.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.net.sip.ISipSessionListener");
                onRegistrationDone(ISipSession.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.net.sip.ISipSessionListener");
                onRegistrationFailed(ISipSession.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readInt(), paramParcel1.readString());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.net.sip.ISipSessionListener");
                onRegistrationTimeout(ISipSession.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
            }
        }

        private static class Proxy
            implements ISipSessionListener
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.net.sip.ISipSessionListener";
            }

            public void onCallBusy(ISipSession paramISipSession)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSessionListener");
                    if (paramISipSession != null)
                    {
                        localIBinder = paramISipSession.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(6, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void onCallChangeFailed(ISipSession paramISipSession, int paramInt, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSessionListener");
                    if (paramISipSession != null)
                    {
                        localIBinder = paramISipSession.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeInt(paramInt);
                        localParcel1.writeString(paramString);
                        this.mRemote.transact(9, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void onCallEnded(ISipSession paramISipSession)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSessionListener");
                    if (paramISipSession != null)
                    {
                        localIBinder = paramISipSession.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(5, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void onCallEstablished(ISipSession paramISipSession, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSessionListener");
                    if (paramISipSession != null)
                    {
                        localIBinder = paramISipSession.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeString(paramString);
                        this.mRemote.transact(4, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void onCallTransferring(ISipSession paramISipSession, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSessionListener");
                    if (paramISipSession != null)
                    {
                        localIBinder = paramISipSession.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeString(paramString);
                        this.mRemote.transact(7, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void onCalling(ISipSession paramISipSession)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSessionListener");
                    if (paramISipSession != null)
                    {
                        localIBinder = paramISipSession.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void onError(ISipSession paramISipSession, int paramInt, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSessionListener");
                    if (paramISipSession != null)
                    {
                        localIBinder = paramISipSession.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeInt(paramInt);
                        localParcel1.writeString(paramString);
                        this.mRemote.transact(8, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void onRegistering(ISipSession paramISipSession)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSessionListener");
                    if (paramISipSession != null)
                    {
                        localIBinder = paramISipSession.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(10, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void onRegistrationDone(ISipSession paramISipSession, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSessionListener");
                    if (paramISipSession != null)
                    {
                        localIBinder = paramISipSession.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(11, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void onRegistrationFailed(ISipSession paramISipSession, int paramInt, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSessionListener");
                    if (paramISipSession != null)
                    {
                        localIBinder = paramISipSession.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeInt(paramInt);
                        localParcel1.writeString(paramString);
                        this.mRemote.transact(12, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void onRegistrationTimeout(ISipSession paramISipSession)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSessionListener");
                    if (paramISipSession != null)
                    {
                        localIBinder = paramISipSession.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(13, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void onRinging(ISipSession paramISipSession, SipProfile paramSipProfile, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSessionListener");
                    IBinder localIBinder;
                    if (paramISipSession != null)
                    {
                        localIBinder = paramISipSession.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        if (paramSipProfile == null)
                            break label97;
                        localParcel1.writeInt(1);
                        paramSipProfile.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeString(paramString);
                        this.mRemote.transact(2, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localIBinder = null;
                        break;
                        label97: localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void onRingingBack(ISipSession paramISipSession)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipSessionListener");
                    if (paramISipSession != null)
                    {
                        localIBinder = paramISipSession.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(3, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.sip.ISipSessionListener
 * JD-Core Version:        0.6.2
 */