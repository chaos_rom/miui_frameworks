package android.net.sip;

import android.content.Context;
import android.media.AudioManager;
import android.net.rtp.AudioCodec;
import android.net.rtp.AudioGroup;
import android.net.rtp.AudioStream;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class SipAudioCall
{
    private static final boolean DONT_RELEASE_SOCKET = false;
    private static final boolean RELEASE_SOCKET = true;
    private static final int SESSION_TIMEOUT = 5;
    private static final String TAG = SipAudioCall.class.getSimpleName();
    private static final int TRANSFER_TIMEOUT = 15;
    private AudioGroup mAudioGroup;
    private AudioStream mAudioStream;
    private Context mContext;
    private int mErrorCode = 0;
    private String mErrorMessage;
    private boolean mHold = false;
    private boolean mInCall = false;
    private Listener mListener;
    private SipProfile mLocalProfile;
    private boolean mMuted = false;
    private String mPeerSd;
    private SipProfile mPendingCallRequest;
    private long mSessionId = System.currentTimeMillis();
    private SipSession mSipSession;
    private SipSession mTransferringSession;
    private WifiManager.WifiLock mWifiHighPerfLock;
    private WifiManager mWm;

    public SipAudioCall(Context paramContext, SipProfile paramSipProfile)
    {
        this.mContext = paramContext;
        this.mLocalProfile = paramSipProfile;
        this.mWm = ((WifiManager)paramContext.getSystemService("wifi"));
    }

    /** @deprecated */
    private void close(boolean paramBoolean)
    {
        if (paramBoolean);
        try
        {
            stopCall(true);
            this.mInCall = false;
            this.mHold = false;
            this.mSessionId = System.currentTimeMillis();
            this.mErrorCode = 0;
            this.mErrorMessage = null;
            if (this.mSipSession != null)
            {
                this.mSipSession.setListener(null);
                this.mSipSession = null;
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    private SimpleSessionDescription createAnswer(String paramString)
    {
        SimpleSessionDescription localSimpleSessionDescription2;
        if (TextUtils.isEmpty(paramString))
            localSimpleSessionDescription2 = createOffer();
        AudioCodec localAudioCodec;
        label391: 
        do
        {
            return localSimpleSessionDescription2;
            SimpleSessionDescription localSimpleSessionDescription1 = new SimpleSessionDescription(paramString);
            localSimpleSessionDescription2 = new SimpleSessionDescription(this.mSessionId, getLocalIp());
            localAudioCodec = null;
            SimpleSessionDescription.Media[] arrayOfMedia = localSimpleSessionDescription1.getMedia();
            int i = arrayOfMedia.length;
            int j = 0;
            if (j < i)
            {
                SimpleSessionDescription.Media localMedia1 = arrayOfMedia[j];
                if ((localAudioCodec == null) && (localMedia1.getPort() > 0) && ("audio".equals(localMedia1.getType())) && ("RTP/AVP".equals(localMedia1.getProtocol())))
                {
                    int[] arrayOfInt1 = localMedia1.getRtpPayloadTypes();
                    int n = arrayOfInt1.length;
                    for (int i1 = 0; ; i1++)
                        if (i1 < n)
                        {
                            int i5 = arrayOfInt1[i1];
                            localAudioCodec = AudioCodec.getCodec(i5, localMedia1.getRtpmap(i5), localMedia1.getFmtp(i5));
                            if (localAudioCodec == null);
                        }
                        else
                        {
                            if (localAudioCodec == null)
                                break label391;
                            SimpleSessionDescription.Media localMedia3 = localSimpleSessionDescription2.newMedia("audio", this.mAudioStream.getLocalPort(), 1, "RTP/AVP");
                            localMedia3.setRtpPayload(localAudioCodec.type, localAudioCodec.rtpmap, localAudioCodec.fmtp);
                            for (int i4 : localMedia1.getRtpPayloadTypes())
                            {
                                String str = localMedia1.getRtpmap(i4);
                                if ((i4 != localAudioCodec.type) && (str != null) && (str.startsWith("telephone-event")))
                                    localMedia3.setRtpPayload(i4, str, localMedia1.getFmtp(i4));
                            }
                        }
                    if (localMedia1.getAttribute("recvonly") != null)
                        localSimpleSessionDescription2.setAttribute("sendonly", "");
                }
                while (true)
                {
                    j++;
                    break;
                    if (localMedia1.getAttribute("sendonly") != null)
                    {
                        localSimpleSessionDescription2.setAttribute("recvonly", "");
                    }
                    else if (localSimpleSessionDescription1.getAttribute("recvonly") != null)
                    {
                        localSimpleSessionDescription2.setAttribute("sendonly", "");
                    }
                    else if (localSimpleSessionDescription1.getAttribute("sendonly") != null)
                    {
                        localSimpleSessionDescription2.setAttribute("recvonly", "");
                        continue;
                        SimpleSessionDescription.Media localMedia2 = localSimpleSessionDescription2.newMedia(localMedia1.getType(), 0, 1, localMedia1.getProtocol());
                        String[] arrayOfString = localMedia1.getFormats();
                        int k = arrayOfString.length;
                        for (int m = 0; m < k; m++)
                            localMedia2.setFormat(arrayOfString[m], null);
                    }
                }
            }
        }
        while (localAudioCodec != null);
        throw new IllegalStateException("Reject SDP: no suitable codecs");
    }

    private SimpleSessionDescription createContinueOffer()
    {
        SimpleSessionDescription localSimpleSessionDescription = new SimpleSessionDescription(this.mSessionId, getLocalIp());
        SimpleSessionDescription.Media localMedia = localSimpleSessionDescription.newMedia("audio", this.mAudioStream.getLocalPort(), 1, "RTP/AVP");
        AudioCodec localAudioCodec = this.mAudioStream.getCodec();
        localMedia.setRtpPayload(localAudioCodec.type, localAudioCodec.rtpmap, localAudioCodec.fmtp);
        int i = this.mAudioStream.getDtmfType();
        if (i != -1)
            localMedia.setRtpPayload(i, "telephone-event/8000", "0-15");
        return localSimpleSessionDescription;
    }

    private SimpleSessionDescription createHoldOffer()
    {
        SimpleSessionDescription localSimpleSessionDescription = createContinueOffer();
        localSimpleSessionDescription.setAttribute("sendonly", "");
        return localSimpleSessionDescription;
    }

    private SipSession.Listener createListener()
    {
        return new SipSession.Listener()
        {
            public void onCallBusy(SipSession paramAnonymousSipSession)
            {
                Log.d(SipAudioCall.TAG, "sip call busy: " + paramAnonymousSipSession);
                SipAudioCall.Listener localListener = SipAudioCall.this.mListener;
                if (localListener != null);
                try
                {
                    localListener.onCallBusy(SipAudioCall.this);
                    SipAudioCall.this.close(false);
                    return;
                }
                catch (Throwable localThrowable)
                {
                    while (true)
                        Log.i(SipAudioCall.TAG, "onCallBusy(): " + localThrowable);
                }
            }

            public void onCallChangeFailed(SipSession paramAnonymousSipSession, int paramAnonymousInt, String paramAnonymousString)
            {
                Log.d(SipAudioCall.TAG, "sip call change failed: " + paramAnonymousString);
                SipAudioCall.access$1002(SipAudioCall.this, paramAnonymousInt);
                SipAudioCall.access$1102(SipAudioCall.this, paramAnonymousString);
                SipAudioCall.Listener localListener = SipAudioCall.this.mListener;
                if (localListener != null);
                try
                {
                    localListener.onError(SipAudioCall.this, SipAudioCall.this.mErrorCode, paramAnonymousString);
                    return;
                }
                catch (Throwable localThrowable)
                {
                    while (true)
                        Log.i(SipAudioCall.TAG, "onCallBusy(): " + localThrowable);
                }
            }

            public void onCallEnded(SipSession paramAnonymousSipSession)
            {
                Log.d(SipAudioCall.TAG, "sip call ended: " + paramAnonymousSipSession + " mSipSession:" + SipAudioCall.this.mSipSession);
                if (paramAnonymousSipSession == SipAudioCall.this.mTransferringSession)
                    SipAudioCall.access$602(SipAudioCall.this, null);
                while (true)
                {
                    return;
                    if ((SipAudioCall.this.mTransferringSession != null) || (paramAnonymousSipSession != SipAudioCall.this.mSipSession))
                        continue;
                    SipAudioCall.Listener localListener = SipAudioCall.this.mListener;
                    if (localListener != null);
                    try
                    {
                        localListener.onCallEnded(SipAudioCall.this);
                        SipAudioCall.this.close();
                    }
                    catch (Throwable localThrowable)
                    {
                        while (true)
                            Log.i(SipAudioCall.TAG, "onCallEnded(): " + localThrowable);
                    }
                }
            }

            public void onCallEstablished(SipSession paramAnonymousSipSession, String paramAnonymousString)
            {
                SipAudioCall.access$502(SipAudioCall.this, paramAnonymousString);
                Log.v(SipAudioCall.TAG, "onCallEstablished()" + SipAudioCall.this.mPeerSd);
                if ((SipAudioCall.this.mTransferringSession != null) && (paramAnonymousSipSession == SipAudioCall.this.mTransferringSession))
                    SipAudioCall.this.transferToNewSession();
                while (true)
                {
                    return;
                    SipAudioCall.Listener localListener = SipAudioCall.this.mListener;
                    if (localListener != null)
                    {
                        try
                        {
                            if (!SipAudioCall.this.mHold)
                                break label138;
                            localListener.onCallHeld(SipAudioCall.this);
                        }
                        catch (Throwable localThrowable)
                        {
                            Log.i(SipAudioCall.TAG, "onCallEstablished(): " + localThrowable);
                        }
                        continue;
                        label138: localListener.onCallEstablished(SipAudioCall.this);
                    }
                }
            }

            public void onCallTransferring(SipSession paramAnonymousSipSession, String paramAnonymousString)
            {
                Log.v(SipAudioCall.TAG, "onCallTransferring mSipSession:" + SipAudioCall.this.mSipSession + " newSession:" + paramAnonymousSipSession);
                SipAudioCall.access$602(SipAudioCall.this, paramAnonymousSipSession);
                if (paramAnonymousString == null);
                try
                {
                    paramAnonymousSipSession.makeCall(paramAnonymousSipSession.getPeerProfile(), SipAudioCall.this.createOffer().encode(), 15);
                    return;
                    paramAnonymousSipSession.answerCall(SipAudioCall.this.createAnswer(paramAnonymousString).encode(), 5);
                }
                catch (Throwable localThrowable)
                {
                    Log.e(SipAudioCall.TAG, "onCallTransferring()", localThrowable);
                    paramAnonymousSipSession.endCall();
                }
            }

            public void onCalling(SipSession paramAnonymousSipSession)
            {
                Log.d(SipAudioCall.TAG, "calling... " + paramAnonymousSipSession);
                SipAudioCall.Listener localListener = SipAudioCall.this.mListener;
                if (localListener != null);
                try
                {
                    localListener.onCalling(SipAudioCall.this);
                    return;
                }
                catch (Throwable localThrowable)
                {
                    while (true)
                        Log.i(SipAudioCall.TAG, "onCalling(): " + localThrowable);
                }
            }

            public void onError(SipSession paramAnonymousSipSession, int paramAnonymousInt, String paramAnonymousString)
            {
                SipAudioCall.this.onError(paramAnonymousInt, paramAnonymousString);
            }

            public void onRegistering(SipSession paramAnonymousSipSession)
            {
            }

            public void onRegistrationDone(SipSession paramAnonymousSipSession, int paramAnonymousInt)
            {
            }

            public void onRegistrationFailed(SipSession paramAnonymousSipSession, int paramAnonymousInt, String paramAnonymousString)
            {
            }

            public void onRegistrationTimeout(SipSession paramAnonymousSipSession)
            {
            }

            public void onRinging(SipSession paramAnonymousSipSession, SipProfile paramAnonymousSipProfile, String paramAnonymousString)
            {
                while (true)
                {
                    synchronized (SipAudioCall.this)
                    {
                        if ((SipAudioCall.this.mSipSession == null) || (!SipAudioCall.this.mInCall) || (!paramAnonymousSipSession.getCallId().equals(SipAudioCall.this.mSipSession.getCallId())))
                        {
                            paramAnonymousSipSession.endCall();
                            return;
                        }
                    }
                    try
                    {
                        String str = SipAudioCall.this.createAnswer(paramAnonymousString).encode();
                        SipAudioCall.this.mSipSession.answerCall(str, 5);
                        continue;
                        localObject = finally;
                        throw localObject;
                    }
                    catch (Throwable localThrowable)
                    {
                        while (true)
                        {
                            Log.e(SipAudioCall.TAG, "onRinging()", localThrowable);
                            paramAnonymousSipSession.endCall();
                        }
                    }
                }
            }

            public void onRingingBack(SipSession paramAnonymousSipSession)
            {
                Log.d(SipAudioCall.TAG, "sip call ringing back: " + paramAnonymousSipSession);
                SipAudioCall.Listener localListener = SipAudioCall.this.mListener;
                if (localListener != null);
                try
                {
                    localListener.onRingingBack(SipAudioCall.this);
                    return;
                }
                catch (Throwable localThrowable)
                {
                    while (true)
                        Log.i(SipAudioCall.TAG, "onRingingBack(): " + localThrowable);
                }
            }
        };
    }

    private SimpleSessionDescription createOffer()
    {
        SimpleSessionDescription localSimpleSessionDescription = new SimpleSessionDescription(this.mSessionId, getLocalIp());
        AudioCodec.getCodecs();
        SimpleSessionDescription.Media localMedia = localSimpleSessionDescription.newMedia("audio", this.mAudioStream.getLocalPort(), 1, "RTP/AVP");
        for (AudioCodec localAudioCodec : AudioCodec.getCodecs())
            localMedia.setRtpPayload(localAudioCodec.type, localAudioCodec.rtpmap, localAudioCodec.fmtp);
        localMedia.setRtpPayload(127, "telephone-event/8000", "0-15");
        return localSimpleSessionDescription;
    }

    private String getLocalIp()
    {
        return this.mSipSession.getLocalIp();
    }

    private SipProfile getPeerProfile(SipSession paramSipSession)
    {
        return paramSipSession.getPeerProfile();
    }

    private void grabWifiHighPerfLock()
    {
        if (this.mWifiHighPerfLock == null)
        {
            Log.v(TAG, "acquire wifi high perf lock");
            this.mWifiHighPerfLock = ((WifiManager)this.mContext.getSystemService("wifi")).createWifiLock(3, TAG);
            this.mWifiHighPerfLock.acquire();
        }
    }

    private boolean isSpeakerOn()
    {
        return ((AudioManager)this.mContext.getSystemService("audio")).isSpeakerphoneOn();
    }

    private boolean isWifiOn()
    {
        if (this.mWm.getConnectionInfo().getBSSID() == null);
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    private void onError(int paramInt, String paramString)
    {
        Log.d(TAG, "sip session error: " + SipErrorCode.toString(paramInt) + ": " + paramString);
        this.mErrorCode = paramInt;
        this.mErrorMessage = paramString;
        Listener localListener = this.mListener;
        if (localListener != null);
        try
        {
            localListener.onError(this, paramInt, paramString);
            if (paramInt == -10);
        }
        catch (Throwable localThrowable)
        {
            try
            {
                while (true)
                {
                    if (!isInCall())
                        close(true);
                    return;
                    localThrowable = localThrowable;
                    Log.i(TAG, "onError(): " + localThrowable);
                }
            }
            finally
            {
            }
        }
    }

    private void releaseWifiHighPerfLock()
    {
        if (this.mWifiHighPerfLock != null)
        {
            Log.v(TAG, "release wifi high perf lock");
            this.mWifiHighPerfLock.release();
            this.mWifiHighPerfLock = null;
        }
    }

    private void setAudioGroupMode()
    {
        AudioGroup localAudioGroup = getAudioGroup();
        if (localAudioGroup != null)
        {
            if (!this.mHold)
                break label22;
            localAudioGroup.setMode(0);
        }
        while (true)
        {
            return;
            label22: if (this.mMuted)
                localAudioGroup.setMode(1);
            else if (isSpeakerOn())
                localAudioGroup.setMode(3);
            else
                localAudioGroup.setMode(2);
        }
    }

    /** @deprecated */
    private void startAudioInternal()
        throws UnknownHostException
    {
        try
        {
            if (this.mPeerSd == null)
            {
                Log.v(TAG, "startAudioInternal() mPeerSd = null");
                throw new IllegalStateException("mPeerSd = null");
            }
        }
        finally
        {
        }
        stopCall(false);
        this.mInCall = true;
        SimpleSessionDescription localSimpleSessionDescription = new SimpleSessionDescription(this.mPeerSd);
        AudioStream localAudioStream = this.mAudioStream;
        AudioCodec localAudioCodec = null;
        SimpleSessionDescription.Media[] arrayOfMedia = localSimpleSessionDescription.getMedia();
        int i = arrayOfMedia.length;
        for (int j = 0; ; j++)
        {
            SimpleSessionDescription.Media localMedia;
            int[] arrayOfInt1;
            int k;
            if (j < i)
            {
                localMedia = arrayOfMedia[j];
                if ((localAudioCodec == null) && (localMedia.getPort() > 0) && ("audio".equals(localMedia.getType())) && ("RTP/AVP".equals(localMedia.getProtocol())))
                {
                    arrayOfInt1 = localMedia.getRtpPayloadTypes();
                    k = arrayOfInt1.length;
                }
            }
            else
            {
                for (int m = 0; ; m++)
                    if (m < k)
                    {
                        int i3 = arrayOfInt1[m];
                        localAudioCodec = AudioCodec.getCodec(i3, localMedia.getRtpmap(i3), localMedia.getFmtp(i3));
                        if (localAudioCodec == null);
                    }
                    else
                    {
                        if (localAudioCodec == null)
                            break;
                        String str1 = localMedia.getAddress();
                        if (str1 == null)
                            str1 = localSimpleSessionDescription.getAddress();
                        localAudioStream.associate(InetAddress.getByName(str1), localMedia.getPort());
                        localAudioStream.setDtmfType(-1);
                        localAudioStream.setCodec(localAudioCodec);
                        int[] arrayOfInt2 = localMedia.getRtpPayloadTypes();
                        int n = arrayOfInt2.length;
                        for (int i1 = 0; ; i1++)
                            if (i1 < n)
                            {
                                int i2 = arrayOfInt2[i1];
                                String str2 = localMedia.getRtpmap(i2);
                                if ((i2 != localAudioCodec.type) && (str2 != null) && (str2.startsWith("telephone-event")))
                                    localAudioStream.setDtmfType(i2);
                            }
                            else
                            {
                                if (this.mHold)
                                    localAudioStream.setMode(0);
                                while (localAudioCodec == null)
                                {
                                    throw new IllegalStateException("Reject SDP: no suitable codecs");
                                    if (localMedia.getAttribute("recvonly") != null)
                                        localAudioStream.setMode(1);
                                    else if (localMedia.getAttribute("sendonly") != null)
                                        localAudioStream.setMode(2);
                                    else if (localSimpleSessionDescription.getAttribute("recvonly") != null)
                                        localAudioStream.setMode(1);
                                    else if (localSimpleSessionDescription.getAttribute("sendonly") != null)
                                        localAudioStream.setMode(2);
                                    else
                                        localAudioStream.setMode(0);
                                }
                                if (isWifiOn())
                                    grabWifiHighPerfLock();
                                AudioGroup localAudioGroup = getAudioGroup();
                                if (this.mHold);
                                while (true)
                                {
                                    setAudioGroupMode();
                                    return;
                                    if (localAudioGroup == null)
                                        localAudioGroup = new AudioGroup();
                                    localAudioStream.join(localAudioGroup);
                                }
                            }
                    }
            }
        }
    }

    private void stopCall(boolean paramBoolean)
    {
        Log.d(TAG, "stop audiocall");
        releaseWifiHighPerfLock();
        if (this.mAudioStream != null)
        {
            this.mAudioStream.join(null);
            if (paramBoolean)
            {
                this.mAudioStream.release();
                this.mAudioStream = null;
            }
        }
    }

    private void throwSipException(Throwable paramThrowable)
        throws SipException
    {
        if ((paramThrowable instanceof SipException))
            throw ((SipException)paramThrowable);
        throw new SipException("", paramThrowable);
    }

    /** @deprecated */
    private void transferToNewSession()
    {
        while (true)
        {
            try
            {
                SipSession localSipSession1 = this.mTransferringSession;
                if (localSipSession1 == null)
                    return;
                SipSession localSipSession2 = this.mSipSession;
                this.mSipSession = this.mTransferringSession;
                this.mTransferringSession = null;
                if (this.mAudioStream != null)
                {
                    this.mAudioStream.join(null);
                    if (localSipSession2 != null)
                        localSipSession2.endCall();
                    startAudio();
                    continue;
                }
            }
            finally
            {
            }
            try
            {
                this.mAudioStream = new AudioStream(InetAddress.getByName(getLocalIp()));
            }
            catch (Throwable localThrowable)
            {
                Log.i(TAG, "transferToNewSession(): " + localThrowable);
            }
        }
    }

    // ERROR //
    public void answerCall(int paramInt)
        throws SipException
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 122	android/net/sip/SipAudioCall:mSipSession	Landroid/net/sip/SipSession;
        //     6: ifnonnull +19 -> 25
        //     9: new 447	android/net/sip/SipException
        //     12: dup
        //     13: ldc_w 466
        //     16: invokespecial 467	android/net/sip/SipException:<init>	(Ljava/lang/String;)V
        //     19: athrow
        //     20: astore_2
        //     21: aload_0
        //     22: monitorexit
        //     23: aload_2
        //     24: athrow
        //     25: aload_0
        //     26: new 225	android/net/rtp/AudioStream
        //     29: dup
        //     30: aload_0
        //     31: invokespecial 175	android/net/sip/SipAudioCall:getLocalIp	()Ljava/lang/String;
        //     34: invokestatic 415	java/net/InetAddress:getByName	(Ljava/lang/String;)Ljava/net/InetAddress;
        //     37: invokespecial 459	android/net/rtp/AudioStream:<init>	(Ljava/net/InetAddress;)V
        //     40: putfield 223	android/net/sip/SipAudioCall:mAudioStream	Landroid/net/rtp/AudioStream;
        //     43: aload_0
        //     44: getfield 122	android/net/sip/SipAudioCall:mSipSession	Landroid/net/sip/SipSession;
        //     47: aload_0
        //     48: aload_0
        //     49: getfield 134	android/net/sip/SipAudioCall:mPeerSd	Ljava/lang/String;
        //     52: invokespecial 130	android/net/sip/SipAudioCall:createAnswer	(Ljava/lang/String;)Landroid/net/sip/SimpleSessionDescription;
        //     55: invokevirtual 470	android/net/sip/SimpleSessionDescription:encode	()Ljava/lang/String;
        //     58: iload_1
        //     59: invokevirtual 473	android/net/sip/SipSession:answerCall	(Ljava/lang/String;I)V
        //     62: aload_0
        //     63: monitorexit
        //     64: return
        //     65: astore_3
        //     66: new 447	android/net/sip/SipException
        //     69: dup
        //     70: ldc_w 475
        //     73: aload_3
        //     74: invokespecial 450	android/net/sip/SipException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
        //     77: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     2	23	20	finally
        //     25	62	20	finally
        //     62	78	20	finally
        //     25	62	65	java/io/IOException
    }

    public void attachCall(SipSession paramSipSession, String paramString)
        throws SipException
    {
        if (!SipManager.isVoipSupported(this.mContext))
            throw new SipException("VOIP API is not supported");
        try
        {
            this.mSipSession = paramSipSession;
            this.mPeerSd = paramString;
            Log.v(TAG, "attachCall()" + this.mPeerSd);
            try
            {
                paramSipSession.setListener(createListener());
                return;
            }
            catch (Throwable localThrowable)
            {
                while (true)
                {
                    Log.e(TAG, "attachCall()", localThrowable);
                    throwSipException(localThrowable);
                }
            }
        }
        finally
        {
        }
    }

    public void close()
    {
        close(true);
    }

    public void continueCall(int paramInt)
        throws SipException
    {
        try
        {
            if (!this.mHold)
                return;
            this.mSipSession.changeCall(createContinueOffer().encode(), paramInt);
            this.mHold = false;
            setAudioGroupMode();
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void endCall()
        throws SipException
    {
        try
        {
            stopCall(true);
            this.mInCall = false;
            if (this.mSipSession != null)
                this.mSipSession.endCall();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    // ERROR //
    public AudioGroup getAudioGroup()
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 501	android/net/sip/SipAudioCall:mAudioGroup	Landroid/net/rtp/AudioGroup;
        //     6: ifnull +13 -> 19
        //     9: aload_0
        //     10: getfield 501	android/net/sip/SipAudioCall:mAudioGroup	Landroid/net/rtp/AudioGroup;
        //     13: astore_3
        //     14: aload_0
        //     15: monitorexit
        //     16: goto +35 -> 51
        //     19: aload_0
        //     20: getfield 223	android/net/sip/SipAudioCall:mAudioStream	Landroid/net/rtp/AudioStream;
        //     23: ifnonnull +15 -> 38
        //     26: aconst_null
        //     27: astore_3
        //     28: aload_0
        //     29: monitorexit
        //     30: goto +21 -> 51
        //     33: astore_1
        //     34: aload_0
        //     35: monitorexit
        //     36: aload_1
        //     37: athrow
        //     38: aload_0
        //     39: getfield 223	android/net/sip/SipAudioCall:mAudioStream	Landroid/net/rtp/AudioStream;
        //     42: invokevirtual 504	android/net/rtp/AudioStream:getGroup	()Landroid/net/rtp/AudioGroup;
        //     45: astore_2
        //     46: aload_2
        //     47: astore_3
        //     48: goto -20 -> 28
        //     51: aload_3
        //     52: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     2	36	33	finally
        //     38	46	33	finally
    }

    public AudioStream getAudioStream()
    {
        try
        {
            AudioStream localAudioStream = this.mAudioStream;
            return localAudioStream;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public SipProfile getLocalProfile()
    {
        try
        {
            SipProfile localSipProfile = this.mLocalProfile;
            return localSipProfile;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public SipProfile getPeerProfile()
    {
        try
        {
            if (this.mSipSession == null)
            {
                localSipProfile = null;
                return localSipProfile;
            }
            SipProfile localSipProfile = this.mSipSession.getPeerProfile();
        }
        finally
        {
        }
    }

    public SipSession getSipSession()
    {
        try
        {
            SipSession localSipSession = this.mSipSession;
            return localSipSession;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public int getState()
    {
        int i;
        try
        {
            if (this.mSipSession == null)
                i = 0;
            else
                i = this.mSipSession.getState();
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
        return i;
    }

    // ERROR //
    public void holdCall(int paramInt)
        throws SipException
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 75	android/net/sip/SipAudioCall:mHold	Z
        //     6: ifeq +8 -> 14
        //     9: aload_0
        //     10: monitorexit
        //     11: goto +52 -> 63
        //     14: aload_0
        //     15: getfield 122	android/net/sip/SipAudioCall:mSipSession	Landroid/net/sip/SipSession;
        //     18: ifnonnull +19 -> 37
        //     21: new 447	android/net/sip/SipException
        //     24: dup
        //     25: ldc_w 515
        //     28: invokespecial 467	android/net/sip/SipException:<init>	(Ljava/lang/String;)V
        //     31: athrow
        //     32: astore_2
        //     33: aload_0
        //     34: monitorexit
        //     35: aload_2
        //     36: athrow
        //     37: aload_0
        //     38: getfield 122	android/net/sip/SipAudioCall:mSipSession	Landroid/net/sip/SipSession;
        //     41: aload_0
        //     42: invokespecial 517	android/net/sip/SipAudioCall:createHoldOffer	()Landroid/net/sip/SimpleSessionDescription;
        //     45: invokevirtual 470	android/net/sip/SimpleSessionDescription:encode	()Ljava/lang/String;
        //     48: iload_1
        //     49: invokevirtual 499	android/net/sip/SipSession:changeCall	(Ljava/lang/String;I)V
        //     52: aload_0
        //     53: iconst_1
        //     54: putfield 75	android/net/sip/SipAudioCall:mHold	Z
        //     57: aload_0
        //     58: invokespecial 433	android/net/sip/SipAudioCall:setAudioGroupMode	()V
        //     61: aload_0
        //     62: monitorexit
        //     63: return
        //
        // Exception table:
        //     from	to	target	type
        //     2	35	32	finally
        //     37	63	32	finally
    }

    public boolean isInCall()
    {
        try
        {
            boolean bool = this.mInCall;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public boolean isMuted()
    {
        try
        {
            boolean bool = this.mMuted;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public boolean isOnHold()
    {
        try
        {
            boolean bool = this.mHold;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void makeCall(SipProfile paramSipProfile, SipSession paramSipSession, int paramInt)
        throws SipException
    {
        if (!SipManager.isVoipSupported(this.mContext))
            throw new SipException("VOIP API is not supported");
        try
        {
            this.mSipSession = paramSipSession;
            try
            {
                this.mAudioStream = new AudioStream(InetAddress.getByName(getLocalIp()));
                paramSipSession.setListener(createListener());
                paramSipSession.makeCall(paramSipProfile, createOffer().encode(), paramInt);
                return;
            }
            catch (IOException localIOException)
            {
                throw new SipException("makeCall()", localIOException);
            }
        }
        finally
        {
        }
    }

    public void sendDtmf(int paramInt)
    {
        sendDtmf(paramInt, null);
    }

    public void sendDtmf(int paramInt, Message paramMessage)
    {
        try
        {
            AudioGroup localAudioGroup = getAudioGroup();
            if ((localAudioGroup != null) && (this.mSipSession != null) && (8 == getState()))
            {
                Log.v(TAG, "send DTMF: " + paramInt);
                localAudioGroup.sendDtmf(paramInt);
            }
            if (paramMessage != null)
                paramMessage.sendToTarget();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void setAudioGroup(AudioGroup paramAudioGroup)
    {
        try
        {
            if ((this.mAudioStream != null) && (this.mAudioStream.getGroup() != null))
                this.mAudioStream.join(paramAudioGroup);
            this.mAudioGroup = paramAudioGroup;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void setListener(Listener paramListener)
    {
        setListener(paramListener, false);
    }

    public void setListener(Listener paramListener, boolean paramBoolean)
    {
        this.mListener = paramListener;
        if ((paramListener == null) || (!paramBoolean));
        while (true)
        {
            return;
            try
            {
                if (this.mErrorCode == 0)
                    break label52;
                paramListener.onError(this, this.mErrorCode, this.mErrorMessage);
            }
            catch (Throwable localThrowable)
            {
                Log.e(TAG, "setListener()", localThrowable);
            }
            continue;
            label52: if (this.mInCall)
            {
                if (this.mHold)
                    paramListener.onCallHeld(this);
                else
                    paramListener.onCallEstablished(this);
            }
            else
                switch (getState())
                {
                case 0:
                    paramListener.onReadyToCall(this);
                    break;
                case 3:
                    paramListener.onRinging(this, getPeerProfile());
                    break;
                case 5:
                    paramListener.onCalling(this);
                    break;
                case 6:
                    paramListener.onRingingBack(this);
                case 1:
                case 2:
                case 4:
                }
        }
    }

    public void setSpeakerMode(boolean paramBoolean)
    {
        try
        {
            ((AudioManager)this.mContext.getSystemService("audio")).setSpeakerphoneOn(paramBoolean);
            setAudioGroupMode();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void startAudio()
    {
        try
        {
            startAudioInternal();
            return;
        }
        catch (UnknownHostException localUnknownHostException)
        {
            while (true)
                onError(-7, localUnknownHostException.getMessage());
        }
        catch (Throwable localThrowable)
        {
            while (true)
                onError(-4, localThrowable.getMessage());
        }
    }

    public void toggleMute()
    {
        while (true)
        {
            try
            {
                if (!this.mMuted)
                {
                    bool = true;
                    this.mMuted = bool;
                    setAudioGroupMode();
                    return;
                }
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
            boolean bool = false;
        }
    }

    public static class Listener
    {
        public void onCallBusy(SipAudioCall paramSipAudioCall)
        {
            onChanged(paramSipAudioCall);
        }

        public void onCallEnded(SipAudioCall paramSipAudioCall)
        {
            onChanged(paramSipAudioCall);
        }

        public void onCallEstablished(SipAudioCall paramSipAudioCall)
        {
            onChanged(paramSipAudioCall);
        }

        public void onCallHeld(SipAudioCall paramSipAudioCall)
        {
            onChanged(paramSipAudioCall);
        }

        public void onCalling(SipAudioCall paramSipAudioCall)
        {
            onChanged(paramSipAudioCall);
        }

        public void onChanged(SipAudioCall paramSipAudioCall)
        {
        }

        public void onError(SipAudioCall paramSipAudioCall, int paramInt, String paramString)
        {
        }

        public void onReadyToCall(SipAudioCall paramSipAudioCall)
        {
            onChanged(paramSipAudioCall);
        }

        public void onRinging(SipAudioCall paramSipAudioCall, SipProfile paramSipProfile)
        {
            onChanged(paramSipAudioCall);
        }

        public void onRingingBack(SipAudioCall paramSipAudioCall)
        {
            onChanged(paramSipAudioCall);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.sip.SipAudioCall
 * JD-Core Version:        0.6.2
 */