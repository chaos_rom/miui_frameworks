package android.net.sip;

public class SipException extends Exception
{
    public SipException()
    {
    }

    public SipException(String paramString)
    {
        super(paramString);
    }

    public SipException(String paramString, Throwable paramThrowable)
    {
        super(paramString, paramThrowable);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.sip.SipException
 * JD-Core Version:        0.6.2
 */