package android.net.sip;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.text.ParseException;
import javax.sip.InvalidArgumentException;
import javax.sip.PeerUnavailableException;
import javax.sip.SipFactory;
import javax.sip.address.Address;
import javax.sip.address.AddressFactory;
import javax.sip.address.SipURI;
import javax.sip.address.URI;

public class SipProfile
    implements Parcelable, Serializable, Cloneable
{
    public static final Parcelable.Creator<SipProfile> CREATOR = new Parcelable.Creator()
    {
        public SipProfile createFromParcel(Parcel paramAnonymousParcel)
        {
            return new SipProfile(paramAnonymousParcel, null);
        }

        public SipProfile[] newArray(int paramAnonymousInt)
        {
            return new SipProfile[paramAnonymousInt];
        }
    };
    private static final int DEFAULT_PORT = 5060;
    private static final String TCP = "TCP";
    private static final String UDP = "UDP";
    private static final long serialVersionUID = 1L;
    private Address mAddress;
    private String mAuthUserName;
    private boolean mAutoRegistration = true;
    private transient int mCallingUid = 0;
    private String mDomain;
    private String mPassword;
    private int mPort = 5060;
    private String mProfileName;
    private String mProtocol = "UDP";
    private String mProxyAddress;
    private boolean mSendKeepAlive = false;

    private SipProfile()
    {
    }

    private SipProfile(Parcel paramParcel)
    {
        this.mAddress = ((Address)paramParcel.readSerializable());
        this.mProxyAddress = paramParcel.readString();
        this.mPassword = paramParcel.readString();
        this.mDomain = paramParcel.readString();
        this.mProtocol = paramParcel.readString();
        this.mProfileName = paramParcel.readString();
        boolean bool2;
        if (paramParcel.readInt() == 0)
        {
            bool2 = false;
            this.mSendKeepAlive = bool2;
            if (paramParcel.readInt() != 0)
                break label141;
        }
        while (true)
        {
            this.mAutoRegistration = bool1;
            this.mCallingUid = paramParcel.readInt();
            this.mPort = paramParcel.readInt();
            this.mAuthUserName = paramParcel.readString();
            return;
            bool2 = true;
            break;
            label141: bool1 = true;
        }
    }

    private Object readResolve()
        throws ObjectStreamException
    {
        if (this.mPort == 0)
            this.mPort = 5060;
        return this;
    }

    public int describeContents()
    {
        return 0;
    }

    public String getAuthUserName()
    {
        return this.mAuthUserName;
    }

    public boolean getAutoRegistration()
    {
        return this.mAutoRegistration;
    }

    public int getCallingUid()
    {
        return this.mCallingUid;
    }

    public String getDisplayName()
    {
        return this.mAddress.getDisplayName();
    }

    public String getPassword()
    {
        return this.mPassword;
    }

    public int getPort()
    {
        return this.mPort;
    }

    public String getProfileName()
    {
        return this.mProfileName;
    }

    public String getProtocol()
    {
        return this.mProtocol;
    }

    public String getProxyAddress()
    {
        return this.mProxyAddress;
    }

    public boolean getSendKeepAlive()
    {
        return this.mSendKeepAlive;
    }

    public Address getSipAddress()
    {
        return this.mAddress;
    }

    public String getSipDomain()
    {
        return this.mDomain;
    }

    public SipURI getUri()
    {
        return (SipURI)this.mAddress.getURI();
    }

    public String getUriString()
    {
        if (!TextUtils.isEmpty(this.mProxyAddress));
        for (String str = "sip:" + getUserName() + "@" + this.mDomain; ; str = getUri().toString())
            return str;
    }

    public String getUserName()
    {
        return getUri().getUser();
    }

    public void setCallingUid(int paramInt)
    {
        this.mCallingUid = paramInt;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        int i = 1;
        paramParcel.writeSerializable(this.mAddress);
        paramParcel.writeString(this.mProxyAddress);
        paramParcel.writeString(this.mPassword);
        paramParcel.writeString(this.mDomain);
        paramParcel.writeString(this.mProtocol);
        paramParcel.writeString(this.mProfileName);
        int j;
        if (this.mSendKeepAlive)
        {
            j = i;
            paramParcel.writeInt(j);
            if (!this.mAutoRegistration)
                break label109;
        }
        while (true)
        {
            paramParcel.writeInt(i);
            paramParcel.writeInt(this.mCallingUid);
            paramParcel.writeInt(this.mPort);
            paramParcel.writeString(this.mAuthUserName);
            return;
            j = 0;
            break;
            label109: i = 0;
        }
    }

    public static class Builder
    {
        private AddressFactory mAddressFactory;
        private String mDisplayName;
        private SipProfile mProfile = new SipProfile(null);
        private String mProxyAddress;
        private SipURI mUri;

        public Builder(SipProfile paramSipProfile)
        {
            try
            {
                this.mAddressFactory = SipFactory.getInstance().createAddressFactory();
                if (paramSipProfile == null)
                    throw new NullPointerException();
            }
            catch (PeerUnavailableException localPeerUnavailableException)
            {
                throw new RuntimeException(localPeerUnavailableException);
            }
            try
            {
                this.mProfile = ((SipProfile)paramSipProfile.clone());
                SipProfile.access$302(this.mProfile, null);
                this.mUri = paramSipProfile.getUri();
                this.mUri.setUserPassword(paramSipProfile.getPassword());
                this.mDisplayName = paramSipProfile.getDisplayName();
                this.mProxyAddress = paramSipProfile.getProxyAddress();
                SipProfile.access$402(this.mProfile, paramSipProfile.getPort());
                return;
            }
            catch (CloneNotSupportedException localCloneNotSupportedException)
            {
                throw new RuntimeException("should not occur", localCloneNotSupportedException);
            }
        }

        public Builder(String paramString)
            throws ParseException
        {
            try
            {
                this.mAddressFactory = SipFactory.getInstance().createAddressFactory();
                if (paramString == null)
                    throw new NullPointerException("uriString cannot be null");
            }
            catch (PeerUnavailableException localPeerUnavailableException)
            {
                throw new RuntimeException(localPeerUnavailableException);
            }
            URI localURI = this.mAddressFactory.createURI(fix(paramString));
            if ((localURI instanceof SipURI))
            {
                this.mUri = ((SipURI)localURI);
                SipProfile.access$502(this.mProfile, this.mUri.getHost());
                return;
            }
            throw new ParseException(paramString + " is not a SIP URI", 0);
        }

        public Builder(String paramString1, String paramString2)
            throws ParseException
        {
            try
            {
                this.mAddressFactory = SipFactory.getInstance().createAddressFactory();
                if ((paramString1 == null) || (paramString2 == null))
                    throw new NullPointerException("username and serverDomain cannot be null");
            }
            catch (PeerUnavailableException localPeerUnavailableException)
            {
                throw new RuntimeException(localPeerUnavailableException);
            }
            this.mUri = this.mAddressFactory.createSipURI(paramString1, paramString2);
            SipProfile.access$502(this.mProfile, paramString2);
        }

        private String fix(String paramString)
        {
            if (paramString.trim().toLowerCase().startsWith("sip:"));
            while (true)
            {
                return paramString;
                paramString = "sip:" + paramString;
            }
        }

        public SipProfile build()
        {
            SipProfile.access$1102(this.mProfile, this.mUri.getUserPassword());
            this.mUri.setUserPassword(null);
            try
            {
                if (!TextUtils.isEmpty(this.mProxyAddress))
                {
                    SipURI localSipURI = (SipURI)this.mAddressFactory.createURI(fix(this.mProxyAddress));
                    SipProfile.access$1202(this.mProfile, localSipURI.getHost());
                }
                while (true)
                {
                    SipProfile.access$302(this.mProfile, this.mAddressFactory.createAddress(this.mDisplayName, this.mUri));
                    return this.mProfile;
                    if (!this.mProfile.mProtocol.equals("UDP"))
                        this.mUri.setTransportParam(this.mProfile.mProtocol);
                    if (this.mProfile.mPort != 5060)
                        this.mUri.setPort(this.mProfile.mPort);
                }
            }
            catch (InvalidArgumentException localInvalidArgumentException)
            {
                throw new RuntimeException(localInvalidArgumentException);
            }
            catch (ParseException localParseException)
            {
                throw new RuntimeException(localParseException);
            }
        }

        public Builder setAuthUserName(String paramString)
        {
            SipProfile.access$602(this.mProfile, paramString);
            return this;
        }

        public Builder setAutoRegistration(boolean paramBoolean)
        {
            SipProfile.access$1002(this.mProfile, paramBoolean);
            return this;
        }

        public Builder setDisplayName(String paramString)
        {
            this.mDisplayName = paramString;
            return this;
        }

        public Builder setOutboundProxy(String paramString)
        {
            this.mProxyAddress = paramString;
            return this;
        }

        public Builder setPassword(String paramString)
        {
            this.mUri.setUserPassword(paramString);
            return this;
        }

        public Builder setPort(int paramInt)
            throws IllegalArgumentException
        {
            if ((paramInt > 65535) || (paramInt < 1000))
                throw new IllegalArgumentException("incorrect port arugment: " + paramInt);
            SipProfile.access$402(this.mProfile, paramInt);
            return this;
        }

        public Builder setProfileName(String paramString)
        {
            SipProfile.access$702(this.mProfile, paramString);
            return this;
        }

        public Builder setProtocol(String paramString)
            throws IllegalArgumentException
        {
            if (paramString == null)
                throw new NullPointerException("protocol cannot be null");
            String str = paramString.toUpperCase();
            if ((!str.equals("UDP")) && (!str.equals("TCP")))
                throw new IllegalArgumentException("unsupported protocol: " + str);
            SipProfile.access$802(this.mProfile, str);
            return this;
        }

        public Builder setSendKeepAlive(boolean paramBoolean)
        {
            SipProfile.access$902(this.mProfile, paramBoolean);
            return this;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.sip.SipProfile
 * JD-Core Version:        0.6.2
 */