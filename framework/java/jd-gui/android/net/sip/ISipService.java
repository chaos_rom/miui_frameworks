package android.net.sip;

import android.app.PendingIntent;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface ISipService extends IInterface
{
    public abstract void close(String paramString)
        throws RemoteException;

    public abstract ISipSession createSession(SipProfile paramSipProfile, ISipSessionListener paramISipSessionListener)
        throws RemoteException;

    public abstract SipProfile[] getListOfProfiles()
        throws RemoteException;

    public abstract ISipSession getPendingSession(String paramString)
        throws RemoteException;

    public abstract boolean isOpened(String paramString)
        throws RemoteException;

    public abstract boolean isRegistered(String paramString)
        throws RemoteException;

    public abstract void open(SipProfile paramSipProfile)
        throws RemoteException;

    public abstract void open3(SipProfile paramSipProfile, PendingIntent paramPendingIntent, ISipSessionListener paramISipSessionListener)
        throws RemoteException;

    public abstract void setRegistrationListener(String paramString, ISipSessionListener paramISipSessionListener)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements ISipService
    {
        private static final String DESCRIPTOR = "android.net.sip.ISipService";
        static final int TRANSACTION_close = 3;
        static final int TRANSACTION_createSession = 7;
        static final int TRANSACTION_getListOfProfiles = 9;
        static final int TRANSACTION_getPendingSession = 8;
        static final int TRANSACTION_isOpened = 4;
        static final int TRANSACTION_isRegistered = 5;
        static final int TRANSACTION_open = 1;
        static final int TRANSACTION_open3 = 2;
        static final int TRANSACTION_setRegistrationListener = 6;

        public Stub()
        {
            attachInterface(this, "android.net.sip.ISipService");
        }

        public static ISipService asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.net.sip.ISipService");
                if ((localIInterface != null) && ((localIInterface instanceof ISipService)))
                    localObject = (ISipService)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            IBinder localIBinder1 = null;
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            }
            while (true)
            {
                return j;
                paramParcel2.writeString("android.net.sip.ISipService");
                continue;
                paramParcel1.enforceInterface("android.net.sip.ISipService");
                if (paramParcel1.readInt() != 0);
                for (SipProfile localSipProfile3 = (SipProfile)SipProfile.CREATOR.createFromParcel(paramParcel1); ; localSipProfile3 = null)
                {
                    open(localSipProfile3);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.net.sip.ISipService");
                SipProfile localSipProfile2;
                if (paramParcel1.readInt() != 0)
                {
                    localSipProfile2 = (SipProfile)SipProfile.CREATOR.createFromParcel(paramParcel1);
                    label196: if (paramParcel1.readInt() == 0)
                        break label245;
                }
                label245: for (PendingIntent localPendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel(paramParcel1); ; localPendingIntent = null)
                {
                    open3(localSipProfile2, localPendingIntent, ISipSessionListener.Stub.asInterface(paramParcel1.readStrongBinder()));
                    paramParcel2.writeNoException();
                    break;
                    localSipProfile2 = null;
                    break label196;
                }
                paramParcel1.enforceInterface("android.net.sip.ISipService");
                close(paramParcel1.readString());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.net.sip.ISipService");
                boolean bool2 = isOpened(paramParcel1.readString());
                paramParcel2.writeNoException();
                if (bool2)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.net.sip.ISipService");
                boolean bool1 = isRegistered(paramParcel1.readString());
                paramParcel2.writeNoException();
                if (bool1)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.net.sip.ISipService");
                setRegistrationListener(paramParcel1.readString(), ISipSessionListener.Stub.asInterface(paramParcel1.readStrongBinder()));
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.net.sip.ISipService");
                SipProfile localSipProfile1;
                label403: ISipSession localISipSession2;
                if (paramParcel1.readInt() != 0)
                {
                    localSipProfile1 = (SipProfile)SipProfile.CREATOR.createFromParcel(paramParcel1);
                    localISipSession2 = createSession(localSipProfile1, ISipSessionListener.Stub.asInterface(paramParcel1.readStrongBinder()));
                    paramParcel2.writeNoException();
                    if (localISipSession2 == null)
                        break label451;
                }
                label451: for (IBinder localIBinder2 = localISipSession2.asBinder(); ; localIBinder2 = null)
                {
                    paramParcel2.writeStrongBinder(localIBinder2);
                    break;
                    localSipProfile1 = null;
                    break label403;
                }
                paramParcel1.enforceInterface("android.net.sip.ISipService");
                ISipSession localISipSession1 = getPendingSession(paramParcel1.readString());
                paramParcel2.writeNoException();
                if (localISipSession1 != null)
                    localIBinder1 = localISipSession1.asBinder();
                paramParcel2.writeStrongBinder(localIBinder1);
                continue;
                paramParcel1.enforceInterface("android.net.sip.ISipService");
                SipProfile[] arrayOfSipProfile = getListOfProfiles();
                paramParcel2.writeNoException();
                paramParcel2.writeTypedArray(arrayOfSipProfile, j);
            }
        }

        private static class Proxy
            implements ISipService
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void close(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public ISipSession createSession(SipProfile paramSipProfile, ISipSessionListener paramISipSessionListener)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.net.sip.ISipService");
                        if (paramSipProfile != null)
                        {
                            localParcel1.writeInt(1);
                            paramSipProfile.writeToParcel(localParcel1, 0);
                            if (paramISipSessionListener != null)
                            {
                                localIBinder = paramISipSessionListener.asBinder();
                                localParcel1.writeStrongBinder(localIBinder);
                                this.mRemote.transact(7, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                                ISipSession localISipSession = ISipSession.Stub.asInterface(localParcel2.readStrongBinder());
                                return localISipSession;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    IBinder localIBinder = null;
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.net.sip.ISipService";
            }

            public SipProfile[] getListOfProfiles()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipService");
                    this.mRemote.transact(9, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    SipProfile[] arrayOfSipProfile = (SipProfile[])localParcel2.createTypedArray(SipProfile.CREATOR);
                    return arrayOfSipProfile;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public ISipSession getPendingSession(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ISipSession localISipSession = ISipSession.Stub.asInterface(localParcel2.readStrongBinder());
                    return localISipSession;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isOpened(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isRegistered(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipService");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void open(SipProfile paramSipProfile)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipService");
                    if (paramSipProfile != null)
                    {
                        localParcel1.writeInt(1);
                        paramSipProfile.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void open3(SipProfile paramSipProfile, PendingIntent paramPendingIntent, ISipSessionListener paramISipSessionListener)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.net.sip.ISipService");
                        if (paramSipProfile != null)
                        {
                            localParcel1.writeInt(1);
                            paramSipProfile.writeToParcel(localParcel1, 0);
                            if (paramPendingIntent != null)
                            {
                                localParcel1.writeInt(1);
                                paramPendingIntent.writeToParcel(localParcel1, 0);
                                if (paramISipSessionListener == null)
                                    break label135;
                                localIBinder = paramISipSessionListener.asBinder();
                                localParcel1.writeStrongBinder(localIBinder);
                                this.mRemote.transact(2, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    localParcel1.writeInt(0);
                    continue;
                    label135: IBinder localIBinder = null;
                }
            }

            public void setRegistrationListener(String paramString, ISipSessionListener paramISipSessionListener)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.sip.ISipService");
                    localParcel1.writeString(paramString);
                    if (paramISipSessionListener != null)
                    {
                        localIBinder = paramISipSessionListener.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(6, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.sip.ISipService
 * JD-Core Version:        0.6.2
 */