package android.net;

import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.telephony.TelephonyManager;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.Objects;

public class NetworkTemplate
    implements Parcelable
{
    public static final Parcelable.Creator<NetworkTemplate> CREATOR = new Parcelable.Creator()
    {
        public NetworkTemplate createFromParcel(Parcel paramAnonymousParcel)
        {
            return new NetworkTemplate(paramAnonymousParcel, null);
        }

        public NetworkTemplate[] newArray(int paramAnonymousInt)
        {
            return new NetworkTemplate[paramAnonymousInt];
        }
    };
    private static final int[] DATA_USAGE_NETWORK_TYPES = Resources.getSystem().getIntArray(17235990);
    public static final int MATCH_ETHERNET = 5;
    public static final int MATCH_MOBILE_3G_LOWER = 2;
    public static final int MATCH_MOBILE_4G = 3;
    public static final int MATCH_MOBILE_ALL = 1;
    public static final int MATCH_MOBILE_WILDCARD = 6;
    public static final int MATCH_WIFI = 4;
    public static final int MATCH_WIFI_WILDCARD = 7;
    private static boolean sForceAllNetworkTypes = false;
    private final int mMatchRule;
    private final String mNetworkId;
    private final String mSubscriberId;

    public NetworkTemplate(int paramInt, String paramString1, String paramString2)
    {
        this.mMatchRule = paramInt;
        this.mSubscriberId = paramString1;
        this.mNetworkId = paramString2;
    }

    private NetworkTemplate(Parcel paramParcel)
    {
        this.mMatchRule = paramParcel.readInt();
        this.mSubscriberId = paramParcel.readString();
        this.mNetworkId = paramParcel.readString();
    }

    public static NetworkTemplate buildTemplateEthernet()
    {
        return new NetworkTemplate(5, null, null);
    }

    @Deprecated
    public static NetworkTemplate buildTemplateMobile3gLower(String paramString)
    {
        return new NetworkTemplate(2, paramString, null);
    }

    @Deprecated
    public static NetworkTemplate buildTemplateMobile4g(String paramString)
    {
        return new NetworkTemplate(3, paramString, null);
    }

    public static NetworkTemplate buildTemplateMobileAll(String paramString)
    {
        return new NetworkTemplate(1, paramString, null);
    }

    public static NetworkTemplate buildTemplateMobileWildcard()
    {
        return new NetworkTemplate(6, null, null);
    }

    @Deprecated
    public static NetworkTemplate buildTemplateWifi()
    {
        return buildTemplateWifiWildcard();
    }

    public static NetworkTemplate buildTemplateWifi(String paramString)
    {
        return new NetworkTemplate(4, null, paramString);
    }

    public static NetworkTemplate buildTemplateWifiWildcard()
    {
        return new NetworkTemplate(7, null, null);
    }

    private static void ensureSubtypeAvailable()
    {
        throw new IllegalArgumentException("Unable to enforce 3G_LOWER template on combined data.");
    }

    public static void forceAllNetworkTypes()
    {
        sForceAllNetworkTypes = true;
    }

    private static String getMatchRuleName(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = "UNKNOWN";
        case 2:
        case 3:
        case 1:
        case 4:
        case 5:
        case 6:
        case 7:
        }
        while (true)
        {
            return str;
            str = "MOBILE_3G_LOWER";
            continue;
            str = "MOBILE_4G";
            continue;
            str = "MOBILE_ALL";
            continue;
            str = "WIFI";
            continue;
            str = "ETHERNET";
            continue;
            str = "MOBILE_WILDCARD";
            continue;
            str = "WIFI_WILDCARD";
        }
    }

    private boolean matchesEthernet(NetworkIdentity paramNetworkIdentity)
    {
        if (paramNetworkIdentity.mType == 9);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private boolean matchesMobile(NetworkIdentity paramNetworkIdentity)
    {
        boolean bool = true;
        if (paramNetworkIdentity.mType == 6);
        while (true)
        {
            return bool;
            if (((!sForceAllNetworkTypes) && (!ArrayUtils.contains(DATA_USAGE_NETWORK_TYPES, paramNetworkIdentity.mType))) || (!Objects.equal(this.mSubscriberId, paramNetworkIdentity.mSubscriberId)))
                bool = false;
        }
    }

    private boolean matchesMobile3gLower(NetworkIdentity paramNetworkIdentity)
    {
        boolean bool = false;
        ensureSubtypeAvailable();
        if (paramNetworkIdentity.mType == 6);
        while (true)
        {
            return bool;
            if (matchesMobile(paramNetworkIdentity))
                switch (TelephonyManager.getNetworkClass(paramNetworkIdentity.mSubType))
                {
                default:
                    break;
                case 0:
                case 1:
                case 2:
                    bool = true;
                }
        }
    }

    private boolean matchesMobile4g(NetworkIdentity paramNetworkIdentity)
    {
        boolean bool = true;
        ensureSubtypeAvailable();
        if (paramNetworkIdentity.mType == 6);
        while (true)
        {
            return bool;
            if (matchesMobile(paramNetworkIdentity))
                switch (TelephonyManager.getNetworkClass(paramNetworkIdentity.mSubType))
                {
                case 3:
                }
            bool = false;
        }
    }

    private boolean matchesMobileWildcard(NetworkIdentity paramNetworkIdentity)
    {
        boolean bool = true;
        if (paramNetworkIdentity.mType == 6);
        while (true)
        {
            return bool;
            if ((!sForceAllNetworkTypes) && (!ArrayUtils.contains(DATA_USAGE_NETWORK_TYPES, paramNetworkIdentity.mType)))
                bool = false;
        }
    }

    private boolean matchesWifi(NetworkIdentity paramNetworkIdentity)
    {
        switch (paramNetworkIdentity.mType)
        {
        default:
        case 1:
        }
        for (boolean bool = false; ; bool = Objects.equal(this.mNetworkId, paramNetworkIdentity.mNetworkId))
            return bool;
    }

    private boolean matchesWifiWildcard(NetworkIdentity paramNetworkIdentity)
    {
        switch (paramNetworkIdentity.mType)
        {
        default:
        case 1:
        case 13:
        }
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = false;
        if ((paramObject instanceof NetworkTemplate))
        {
            NetworkTemplate localNetworkTemplate = (NetworkTemplate)paramObject;
            if ((this.mMatchRule == localNetworkTemplate.mMatchRule) && (Objects.equal(this.mSubscriberId, localNetworkTemplate.mSubscriberId)) && (Objects.equal(this.mNetworkId, localNetworkTemplate.mNetworkId)))
                bool = true;
        }
        return bool;
    }

    public int getMatchRule()
    {
        return this.mMatchRule;
    }

    public String getNetworkId()
    {
        return this.mNetworkId;
    }

    public String getSubscriberId()
    {
        return this.mSubscriberId;
    }

    public int hashCode()
    {
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = Integer.valueOf(this.mMatchRule);
        arrayOfObject[1] = this.mSubscriberId;
        arrayOfObject[2] = this.mNetworkId;
        return Objects.hashCode(arrayOfObject);
    }

    public boolean matches(NetworkIdentity paramNetworkIdentity)
    {
        boolean bool;
        switch (this.mMatchRule)
        {
        default:
            throw new IllegalArgumentException("unknown network template");
        case 1:
            bool = matchesMobile(paramNetworkIdentity);
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        }
        while (true)
        {
            return bool;
            bool = matchesMobile3gLower(paramNetworkIdentity);
            continue;
            bool = matchesMobile4g(paramNetworkIdentity);
            continue;
            bool = matchesWifi(paramNetworkIdentity);
            continue;
            bool = matchesEthernet(paramNetworkIdentity);
            continue;
            bool = matchesMobileWildcard(paramNetworkIdentity);
            continue;
            bool = matchesWifiWildcard(paramNetworkIdentity);
        }
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder("NetworkTemplate: ");
        localStringBuilder.append("matchRule=").append(getMatchRuleName(this.mMatchRule));
        if (this.mSubscriberId != null)
            localStringBuilder.append(", subscriberId=").append(NetworkIdentity.scrubSubscriberId(this.mSubscriberId));
        if (this.mNetworkId != null)
            localStringBuilder.append(", networkId=").append(this.mNetworkId);
        return localStringBuilder.toString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mMatchRule);
        paramParcel.writeString(this.mSubscriberId);
        paramParcel.writeString(this.mNetworkId);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.NetworkTemplate
 * JD-Core Version:        0.6.2
 */