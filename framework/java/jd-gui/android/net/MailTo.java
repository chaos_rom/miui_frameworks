package android.net;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class MailTo
{
    private static final String BODY = "body";
    private static final String CC = "cc";
    public static final String MAILTO_SCHEME = "mailto:";
    private static final String SUBJECT = "subject";
    private static final String TO = "to";
    private HashMap<String, String> mHeaders = new HashMap();

    public static boolean isMailTo(String paramString)
    {
        if ((paramString != null) && (paramString.startsWith("mailto:")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static MailTo parse(String paramString)
        throws ParseException
    {
        if (paramString == null)
            throw new NullPointerException();
        if (!isMailTo(paramString))
            throw new ParseException("Not a mailto scheme");
        Uri localUri = Uri.parse(paramString.substring("mailto:".length()));
        MailTo localMailTo = new MailTo();
        String str1 = localUri.getQuery();
        if (str1 != null)
        {
            String[] arrayOfString1 = str1.split("&");
            int i = arrayOfString1.length;
            int j = 0;
            while (j < i)
            {
                String[] arrayOfString2 = arrayOfString1[j].split("=");
                if (arrayOfString2.length == 0)
                {
                    j++;
                }
                else
                {
                    HashMap localHashMap = localMailTo.mHeaders;
                    String str4 = Uri.decode(arrayOfString2[0]).toLowerCase();
                    if (arrayOfString2.length > 1);
                    for (String str5 = Uri.decode(arrayOfString2[1]); ; str5 = null)
                    {
                        localHashMap.put(str4, str5);
                        break;
                    }
                }
            }
        }
        String str2 = localUri.getPath();
        if (str2 != null)
        {
            String str3 = localMailTo.getTo();
            if (str3 != null)
                str2 = str2 + ", " + str3;
            localMailTo.mHeaders.put("to", str2);
        }
        return localMailTo;
    }

    public String getBody()
    {
        return (String)this.mHeaders.get("body");
    }

    public String getCc()
    {
        return (String)this.mHeaders.get("cc");
    }

    public Map<String, String> getHeaders()
    {
        return this.mHeaders;
    }

    public String getSubject()
    {
        return (String)this.mHeaders.get("subject");
    }

    public String getTo()
    {
        return (String)this.mHeaders.get("to");
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder("mailto:");
        localStringBuilder.append('?');
        Iterator localIterator = this.mHeaders.entrySet().iterator();
        while (localIterator.hasNext())
        {
            Map.Entry localEntry = (Map.Entry)localIterator.next();
            localStringBuilder.append(Uri.encode((String)localEntry.getKey()));
            localStringBuilder.append('=');
            localStringBuilder.append(Uri.encode((String)localEntry.getValue()));
            localStringBuilder.append('&');
        }
        return localStringBuilder.toString();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.MailTo
 * JD-Core Version:        0.6.2
 */