package android.net;

import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.provider.Settings.Secure;
import android.util.Log;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public final class DnsPinger extends Handler
{
    private static final int ACTION_CANCEL_ALL_PINGS = 327683;
    private static final int ACTION_LISTEN_FOR_RESPONSE = 327682;
    private static final int ACTION_PING_DNS = 327681;
    private static final int BASE = 327680;
    private static final boolean DBG = false;
    public static final int DNS_PING_RESULT = 327680;
    private static final int DNS_PORT = 53;
    private static final int RECEIVE_POLL_INTERVAL_MS = 200;
    public static final int SOCKET_EXCEPTION = -2;
    private static final int SOCKET_TIMEOUT_MS = 1;
    public static final int TIMEOUT = -1;
    private static final byte[] mDnsQuery = arrayOfByte;
    private static final AtomicInteger sCounter;
    private static final Random sRandom = new Random();
    private String TAG;
    private List<ActivePing> mActivePings = new ArrayList();
    private final int mConnectionType;
    private ConnectivityManager mConnectivityManager = null;
    private final Context mContext;
    private AtomicInteger mCurrentToken = new AtomicInteger();
    private final ArrayList<InetAddress> mDefaultDns;
    private int mEventCounter;
    private final Handler mTarget;

    static
    {
        sCounter = new AtomicInteger();
        byte[] arrayOfByte = new byte[32];
        arrayOfByte[0] = 0;
        arrayOfByte[1] = 0;
        arrayOfByte[2] = 1;
        arrayOfByte[3] = 0;
        arrayOfByte[4] = 0;
        arrayOfByte[5] = 1;
        arrayOfByte[6] = 0;
        arrayOfByte[7] = 0;
        arrayOfByte[8] = 0;
        arrayOfByte[9] = 0;
        arrayOfByte[10] = 0;
        arrayOfByte[11] = 0;
        arrayOfByte[12] = 3;
        arrayOfByte[13] = 119;
        arrayOfByte[14] = 119;
        arrayOfByte[15] = 119;
        arrayOfByte[16] = 6;
        arrayOfByte[17] = 103;
        arrayOfByte[18] = 111;
        arrayOfByte[19] = 111;
        arrayOfByte[20] = 103;
        arrayOfByte[21] = 108;
        arrayOfByte[22] = 101;
        arrayOfByte[23] = 3;
        arrayOfByte[24] = 99;
        arrayOfByte[25] = 111;
        arrayOfByte[26] = 109;
        arrayOfByte[27] = 0;
        arrayOfByte[28] = 0;
        arrayOfByte[29] = 1;
        arrayOfByte[30] = 0;
        arrayOfByte[31] = 1;
    }

    public DnsPinger(Context paramContext, String paramString, Looper paramLooper, Handler paramHandler, int paramInt)
    {
        super(paramLooper);
        this.TAG = paramString;
        this.mContext = paramContext;
        this.mTarget = paramHandler;
        this.mConnectionType = paramInt;
        if (!ConnectivityManager.isNetworkTypeValid(paramInt))
            throw new IllegalArgumentException("Invalid connectionType in constructor: " + paramInt);
        this.mDefaultDns = new ArrayList();
        this.mDefaultDns.add(getDefaultDns());
        this.mEventCounter = 0;
    }

    private LinkProperties getCurrentLinkProperties()
    {
        if (this.mConnectivityManager == null)
            this.mConnectivityManager = ((ConnectivityManager)this.mContext.getSystemService("connectivity"));
        return this.mConnectivityManager.getLinkProperties(this.mConnectionType);
    }

    private InetAddress getDefaultDns()
    {
        String str = Settings.Secure.getString(this.mContext.getContentResolver(), "default_dns_server");
        if ((str == null) || (str.length() == 0))
            str = this.mContext.getResources().getString(17039391);
        try
        {
            InetAddress localInetAddress2 = NetworkUtils.numericToInetAddress(str);
            localInetAddress1 = localInetAddress2;
            return localInetAddress1;
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            while (true)
            {
                loge("getDefaultDns::malformed default dns address");
                InetAddress localInetAddress1 = null;
            }
        }
    }

    private void log(String paramString)
    {
        Log.d(this.TAG, paramString);
    }

    private void loge(String paramString)
    {
        Log.e(this.TAG, paramString);
    }

    private void sendResponse(int paramInt1, int paramInt2, int paramInt3)
    {
        this.mTarget.sendMessage(obtainMessage(327680, paramInt1, paramInt3));
    }

    public void cancelPings()
    {
        this.mCurrentToken.incrementAndGet();
        obtainMessage(327683).sendToTarget();
    }

    public List<InetAddress> getDnsList()
    {
        LinkProperties localLinkProperties = getCurrentLinkProperties();
        ArrayList localArrayList;
        if (localLinkProperties == null)
        {
            loge("getCurLinkProperties:: LP for type" + this.mConnectionType + " is null!");
            localArrayList = this.mDefaultDns;
        }
        while (true)
        {
            return localArrayList;
            Collection localCollection = localLinkProperties.getDnses();
            if ((localCollection == null) || (localCollection.size() == 0))
            {
                loge("getDns::LinkProps has null dns - returning default");
                localArrayList = this.mDefaultDns;
            }
            else
            {
                localArrayList = new ArrayList(localCollection);
            }
        }
    }

    // ERROR //
    public void handleMessage(Message paramMessage)
    {
        // Byte code:
        //     0: aload_1
        //     1: getfield 263	android/os/Message:what	I
        //     4: tableswitch	default:+28 -> 32, 327681:+29->33, 327682:+294->298, 327683:+600->604
        //     33: aload_1
        //     34: getfield 267	android/os/Message:obj	Ljava/lang/Object;
        //     37: checkcast 8	android/net/DnsPinger$DnsArg
        //     40: astore 12
        //     42: aload 12
        //     44: getfield 270	android/net/DnsPinger$DnsArg:seq	I
        //     47: aload_0
        //     48: getfield 91	android/net/DnsPinger:mCurrentToken	Ljava/util/concurrent/atomic/AtomicInteger;
        //     51: invokevirtual 273	java/util/concurrent/atomic/AtomicInteger:get	()I
        //     54: if_icmpne -22 -> 32
        //     57: new 11	android/net/DnsPinger$ActivePing
        //     60: dup
        //     61: aload_0
        //     62: aconst_null
        //     63: invokespecial 276	android/net/DnsPinger$ActivePing:<init>	(Landroid/net/DnsPinger;Landroid/net/DnsPinger$1;)V
        //     66: astore 13
        //     68: aload 12
        //     70: getfield 280	android/net/DnsPinger$DnsArg:dns	Ljava/net/InetAddress;
        //     73: astore 15
        //     75: aload 13
        //     77: aload_1
        //     78: getfield 283	android/os/Message:arg1	I
        //     81: putfield 286	android/net/DnsPinger$ActivePing:internalId	I
        //     84: aload 13
        //     86: aload_1
        //     87: getfield 289	android/os/Message:arg2	I
        //     90: putfield 292	android/net/DnsPinger$ActivePing:timeout	I
        //     93: aload 13
        //     95: new 294	java/net/DatagramSocket
        //     98: dup
        //     99: invokespecial 295	java/net/DatagramSocket:<init>	()V
        //     102: putfield 299	android/net/DnsPinger$ActivePing:socket	Ljava/net/DatagramSocket;
        //     105: aload 13
        //     107: getfield 299	android/net/DnsPinger$ActivePing:socket	Ljava/net/DatagramSocket;
        //     110: iconst_1
        //     111: invokevirtual 303	java/net/DatagramSocket:setSoTimeout	(I)V
        //     114: aload 13
        //     116: getfield 299	android/net/DnsPinger$ActivePing:socket	Ljava/net/DatagramSocket;
        //     119: aload_0
        //     120: invokespecial 232	android/net/DnsPinger:getCurrentLinkProperties	()Landroid/net/LinkProperties;
        //     123: invokevirtual 306	android/net/LinkProperties:getInterfaceName	()Ljava/lang/String;
        //     126: invokestatic 312	java/net/NetworkInterface:getByName	(Ljava/lang/String;)Ljava/net/NetworkInterface;
        //     129: invokevirtual 316	java/net/DatagramSocket:setNetworkInterface	(Ljava/net/NetworkInterface;)V
        //     132: aload 13
        //     134: getstatic 67	android/net/DnsPinger:sRandom	Ljava/util/Random;
        //     137: invokevirtual 319	java/util/Random:nextInt	()I
        //     140: i2s
        //     141: putfield 323	android/net/DnsPinger$ActivePing:packetId	S
        //     144: getstatic 83	android/net/DnsPinger:mDnsQuery	[B
        //     147: invokevirtual 328	[B:clone	()Ljava/lang/Object;
        //     150: checkcast 324	[B
        //     153: astore 17
        //     155: aload 17
        //     157: iconst_0
        //     158: aload 13
        //     160: getfield 323	android/net/DnsPinger$ActivePing:packetId	S
        //     163: bipush 8
        //     165: ishr
        //     166: i2b
        //     167: bastore
        //     168: aload 17
        //     170: iconst_1
        //     171: aload 13
        //     173: getfield 323	android/net/DnsPinger$ActivePing:packetId	S
        //     176: i2b
        //     177: bastore
        //     178: new 330	java/net/DatagramPacket
        //     181: dup
        //     182: aload 17
        //     184: aload 17
        //     186: arraylength
        //     187: aload 15
        //     189: bipush 53
        //     191: invokespecial 333	java/net/DatagramPacket:<init>	([BILjava/net/InetAddress;I)V
        //     194: astore 18
        //     196: aload 13
        //     198: getfield 299	android/net/DnsPinger$ActivePing:socket	Ljava/net/DatagramSocket;
        //     201: aload 18
        //     203: invokevirtual 337	java/net/DatagramSocket:send	(Ljava/net/DatagramPacket;)V
        //     206: aload_0
        //     207: getfield 96	android/net/DnsPinger:mActivePings	Ljava/util/List;
        //     210: aload 13
        //     212: invokeinterface 340 2 0
        //     217: pop
        //     218: aload_0
        //     219: iconst_1
        //     220: aload_0
        //     221: getfield 143	android/net/DnsPinger:mEventCounter	I
        //     224: iadd
        //     225: putfield 143	android/net/DnsPinger:mEventCounter	I
        //     228: aload_0
        //     229: aload_0
        //     230: ldc 17
        //     232: aload_0
        //     233: getfield 143	android/net/DnsPinger:mEventCounter	I
        //     236: iconst_0
        //     237: invokevirtual 212	android/net/DnsPinger:obtainMessage	(III)Landroid/os/Message;
        //     240: ldc2_w 341
        //     243: invokevirtual 346	android/net/DnsPinger:sendMessageDelayed	(Landroid/os/Message;J)Z
        //     246: pop
        //     247: goto -215 -> 32
        //     250: astore 14
        //     252: aload_0
        //     253: aload_1
        //     254: getfield 283	android/os/Message:arg1	I
        //     257: sipush -9999
        //     260: bipush 254
        //     262: invokespecial 348	android/net/DnsPinger:sendResponse	(III)V
        //     265: goto -233 -> 32
        //     268: astore 16
        //     270: aload_0
        //     271: new 114	java/lang/StringBuilder
        //     274: dup
        //     275: invokespecial 115	java/lang/StringBuilder:<init>	()V
        //     278: ldc_w 350
        //     281: invokevirtual 121	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     284: aload 16
        //     286: invokevirtual 353	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     289: invokevirtual 128	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     292: invokespecial 196	android/net/DnsPinger:loge	(Ljava/lang/String;)V
        //     295: goto -163 -> 132
        //     298: aload_1
        //     299: getfield 283	android/os/Message:arg1	I
        //     302: aload_0
        //     303: getfield 143	android/net/DnsPinger:mEventCounter	I
        //     306: if_icmpne -274 -> 32
        //     309: aload_0
        //     310: getfield 96	android/net/DnsPinger:mActivePings	Ljava/util/List;
        //     313: invokeinterface 357 1 0
        //     318: astore_3
        //     319: aload_3
        //     320: invokeinterface 363 1 0
        //     325: ifeq +111 -> 436
        //     328: aload_3
        //     329: invokeinterface 366 1 0
        //     334: checkcast 11	android/net/DnsPinger$ActivePing
        //     337: astore 7
        //     339: iconst_2
        //     340: newarray byte
        //     342: astore 10
        //     344: new 330	java/net/DatagramPacket
        //     347: dup
        //     348: aload 10
        //     350: iconst_2
        //     351: invokespecial 369	java/net/DatagramPacket:<init>	([BI)V
        //     354: astore 11
        //     356: aload 7
        //     358: getfield 299	android/net/DnsPinger$ActivePing:socket	Ljava/net/DatagramSocket;
        //     361: aload 11
        //     363: invokevirtual 372	java/net/DatagramSocket:receive	(Ljava/net/DatagramPacket;)V
        //     366: aload 10
        //     368: iconst_0
        //     369: baload
        //     370: aload 7
        //     372: getfield 323	android/net/DnsPinger$ActivePing:packetId	S
        //     375: bipush 8
        //     377: ishr
        //     378: i2b
        //     379: if_icmpne -60 -> 319
        //     382: aload 10
        //     384: iconst_1
        //     385: baload
        //     386: aload 7
        //     388: getfield 323	android/net/DnsPinger$ActivePing:packetId	S
        //     391: i2b
        //     392: if_icmpne -73 -> 319
        //     395: aload 7
        //     397: invokestatic 378	android/os/SystemClock:elapsedRealtime	()J
        //     400: aload 7
        //     402: getfield 382	android/net/DnsPinger$ActivePing:start	J
        //     405: lsub
        //     406: l2i
        //     407: invokestatic 388	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     410: putfield 392	android/net/DnsPinger$ActivePing:result	Ljava/lang/Integer;
        //     413: goto -94 -> 319
        //     416: astore 9
        //     418: goto -99 -> 319
        //     421: astore 8
        //     423: aload 7
        //     425: bipush 254
        //     427: invokestatic 388	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     430: putfield 392	android/net/DnsPinger$ActivePing:result	Ljava/lang/Integer;
        //     433: goto -114 -> 319
        //     436: aload_0
        //     437: getfield 96	android/net/DnsPinger:mActivePings	Ljava/util/List;
        //     440: invokeinterface 357 1 0
        //     445: astore 4
        //     447: aload 4
        //     449: invokeinterface 363 1 0
        //     454: ifeq +116 -> 570
        //     457: aload 4
        //     459: invokeinterface 366 1 0
        //     464: checkcast 11	android/net/DnsPinger$ActivePing
        //     467: astore 6
        //     469: aload 6
        //     471: getfield 392	android/net/DnsPinger$ActivePing:result	Ljava/lang/Integer;
        //     474: ifnull +43 -> 517
        //     477: aload_0
        //     478: aload 6
        //     480: getfield 286	android/net/DnsPinger$ActivePing:internalId	I
        //     483: aload 6
        //     485: getfield 323	android/net/DnsPinger$ActivePing:packetId	S
        //     488: aload 6
        //     490: getfield 392	android/net/DnsPinger$ActivePing:result	Ljava/lang/Integer;
        //     493: invokevirtual 395	java/lang/Integer:intValue	()I
        //     496: invokespecial 348	android/net/DnsPinger:sendResponse	(III)V
        //     499: aload 6
        //     501: getfield 299	android/net/DnsPinger$ActivePing:socket	Ljava/net/DatagramSocket;
        //     504: invokevirtual 398	java/net/DatagramSocket:close	()V
        //     507: aload 4
        //     509: invokeinterface 401 1 0
        //     514: goto -67 -> 447
        //     517: invokestatic 378	android/os/SystemClock:elapsedRealtime	()J
        //     520: aload 6
        //     522: getfield 382	android/net/DnsPinger$ActivePing:start	J
        //     525: aload 6
        //     527: getfield 292	android/net/DnsPinger$ActivePing:timeout	I
        //     530: i2l
        //     531: ladd
        //     532: lcmp
        //     533: ifle -86 -> 447
        //     536: aload_0
        //     537: aload 6
        //     539: getfield 286	android/net/DnsPinger$ActivePing:internalId	I
        //     542: aload 6
        //     544: getfield 323	android/net/DnsPinger$ActivePing:packetId	S
        //     547: bipush 255
        //     549: invokespecial 348	android/net/DnsPinger:sendResponse	(III)V
        //     552: aload 6
        //     554: getfield 299	android/net/DnsPinger$ActivePing:socket	Ljava/net/DatagramSocket;
        //     557: invokevirtual 398	java/net/DatagramSocket:close	()V
        //     560: aload 4
        //     562: invokeinterface 401 1 0
        //     567: goto -120 -> 447
        //     570: aload_0
        //     571: getfield 96	android/net/DnsPinger:mActivePings	Ljava/util/List;
        //     574: invokeinterface 404 1 0
        //     579: ifne -547 -> 32
        //     582: aload_0
        //     583: aload_0
        //     584: ldc 17
        //     586: aload_0
        //     587: getfield 143	android/net/DnsPinger:mEventCounter	I
        //     590: iconst_0
        //     591: invokevirtual 212	android/net/DnsPinger:obtainMessage	(III)Landroid/os/Message;
        //     594: ldc2_w 341
        //     597: invokevirtual 346	android/net/DnsPinger:sendMessageDelayed	(Landroid/os/Message;J)Z
        //     600: pop
        //     601: goto -569 -> 32
        //     604: aload_0
        //     605: getfield 96	android/net/DnsPinger:mActivePings	Ljava/util/List;
        //     608: invokeinterface 357 1 0
        //     613: astore_2
        //     614: aload_2
        //     615: invokeinterface 363 1 0
        //     620: ifeq +21 -> 641
        //     623: aload_2
        //     624: invokeinterface 366 1 0
        //     629: checkcast 11	android/net/DnsPinger$ActivePing
        //     632: getfield 299	android/net/DnsPinger$ActivePing:socket	Ljava/net/DatagramSocket;
        //     635: invokevirtual 398	java/net/DatagramSocket:close	()V
        //     638: goto -24 -> 614
        //     641: aload_0
        //     642: getfield 96	android/net/DnsPinger:mActivePings	Ljava/util/List;
        //     645: invokeinterface 407 1 0
        //     650: goto -618 -> 32
        //
        // Exception table:
        //     from	to	target	type
        //     57	114	250	java/io/IOException
        //     114	132	250	java/io/IOException
        //     132	247	250	java/io/IOException
        //     270	295	250	java/io/IOException
        //     114	132	268	java/lang/Exception
        //     339	413	416	java/net/SocketTimeoutException
        //     339	413	421	java/lang/Exception
    }

    public int pingDnsAsync(InetAddress paramInetAddress, int paramInt1, int paramInt2)
    {
        int i = sCounter.incrementAndGet();
        sendMessageDelayed(obtainMessage(327681, i, paramInt1, new DnsArg(paramInetAddress, this.mCurrentToken.get())), paramInt2);
        return i;
    }

    private class DnsArg
    {
        InetAddress dns;
        int seq;

        DnsArg(InetAddress paramInt, int arg3)
        {
            this.dns = paramInt;
            int i;
            this.seq = i;
        }
    }

    private class ActivePing
    {
        int internalId;
        short packetId;
        Integer result;
        DatagramSocket socket;
        long start = SystemClock.elapsedRealtime();
        int timeout;

        private ActivePing()
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.DnsPinger
 * JD-Core Version:        0.6.2
 */