package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Iterator;

public class RouteInfo
    implements Parcelable
{
    public static final Parcelable.Creator<RouteInfo> CREATOR = new Parcelable.Creator()
    {
        public RouteInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            Object localObject1 = null;
            int i = 0;
            Object localObject2 = null;
            byte[] arrayOfByte2;
            if (paramAnonymousParcel.readByte() == 1)
            {
                arrayOfByte2 = paramAnonymousParcel.createByteArray();
                i = paramAnonymousParcel.readInt();
            }
            try
            {
                InetAddress localInetAddress2 = InetAddress.getByAddress(arrayOfByte2);
                localObject1 = localInetAddress2;
                if (paramAnonymousParcel.readByte() == 1)
                    arrayOfByte1 = paramAnonymousParcel.createByteArray();
            }
            catch (UnknownHostException localUnknownHostException2)
            {
                try
                {
                    byte[] arrayOfByte1;
                    InetAddress localInetAddress1 = InetAddress.getByAddress(arrayOfByte1);
                    localObject2 = localInetAddress1;
                    label61: LinkAddress localLinkAddress = null;
                    if (localObject1 != null)
                        localLinkAddress = new LinkAddress(localObject1, i);
                    return new RouteInfo(localLinkAddress, localObject2);
                    localUnknownHostException2 = localUnknownHostException2;
                }
                catch (UnknownHostException localUnknownHostException1)
                {
                    break label61;
                }
            }
        }

        public RouteInfo[] newArray(int paramAnonymousInt)
        {
            return new RouteInfo[paramAnonymousInt];
        }
    };
    private final LinkAddress mDestination;
    private final InetAddress mGateway;
    private final boolean mIsDefault;
    private final boolean mIsHost;

    public RouteInfo(LinkAddress paramLinkAddress, InetAddress paramInetAddress)
    {
        if (paramLinkAddress == null)
        {
            if (paramInetAddress == null)
                break label112;
            if ((paramInetAddress instanceof Inet4Address))
                paramLinkAddress = new LinkAddress(Inet4Address.ANY, 0);
        }
        else if (paramInetAddress == null)
        {
            if (!(paramLinkAddress.getAddress() instanceof Inet4Address))
                break label122;
        }
        label112: label122: for (paramInetAddress = Inet4Address.ANY; ; paramInetAddress = Inet6Address.ANY)
        {
            this.mDestination = new LinkAddress(NetworkUtils.getNetworkPart(paramLinkAddress.getAddress(), paramLinkAddress.getNetworkPrefixLength()), paramLinkAddress.getNetworkPrefixLength());
            this.mGateway = paramInetAddress;
            this.mIsDefault = isDefault();
            this.mIsHost = isHost();
            return;
            paramLinkAddress = new LinkAddress(Inet6Address.ANY, 0);
            break;
            throw new RuntimeException("Invalid arguments passed in.");
        }
    }

    public RouteInfo(InetAddress paramInetAddress)
    {
        this(null, paramInetAddress);
    }

    private boolean isDefault()
    {
        boolean bool = false;
        if (this.mGateway != null)
        {
            if (!(this.mGateway instanceof Inet4Address))
                break label45;
            if ((this.mDestination != null) && (this.mDestination.getNetworkPrefixLength() != 0))
                break label40;
        }
        label40: for (bool = true; ; bool = false)
            return bool;
        label45: if ((this.mDestination == null) || (this.mDestination.getNetworkPrefixLength() == 0));
        for (bool = true; ; bool = false)
            break;
    }

    private boolean isHost()
    {
        if ((this.mGateway.equals(Inet4Address.ANY)) || (this.mGateway.equals(Inet6Address.ANY)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static RouteInfo makeHostRoute(InetAddress paramInetAddress)
    {
        return makeHostRoute(paramInetAddress, null);
    }

    public static RouteInfo makeHostRoute(InetAddress paramInetAddress1, InetAddress paramInetAddress2)
    {
        RouteInfo localRouteInfo;
        if (paramInetAddress1 == null)
            localRouteInfo = null;
        while (true)
        {
            return localRouteInfo;
            if ((paramInetAddress1 instanceof Inet4Address))
                localRouteInfo = new RouteInfo(new LinkAddress(paramInetAddress1, 32), paramInetAddress2);
            else
                localRouteInfo = new RouteInfo(new LinkAddress(paramInetAddress1, 128), paramInetAddress2);
        }
    }

    private boolean matches(InetAddress paramInetAddress)
    {
        boolean bool;
        if (paramInetAddress == null)
            bool = false;
        while (true)
        {
            return bool;
            if (isDefault())
            {
                bool = true;
            }
            else
            {
                InetAddress localInetAddress = NetworkUtils.getNetworkPart(paramInetAddress, this.mDestination.getNetworkPrefixLength());
                bool = this.mDestination.getAddress().equals(localInetAddress);
            }
        }
    }

    public static RouteInfo selectBestRoute(Collection<RouteInfo> paramCollection, InetAddress paramInetAddress)
    {
        Object localObject;
        if ((paramCollection == null) || (paramInetAddress == null))
            localObject = null;
        while (true)
        {
            return localObject;
            localObject = null;
            Iterator localIterator = paramCollection.iterator();
            while (localIterator.hasNext())
            {
                RouteInfo localRouteInfo = (RouteInfo)localIterator.next();
                if ((NetworkUtils.addressTypeMatches(localRouteInfo.mDestination.getAddress(), paramInetAddress)) && ((localObject == null) || (((RouteInfo)localObject).mDestination.getNetworkPrefixLength() < localRouteInfo.mDestination.getNetworkPrefixLength())) && (localRouteInfo.matches(paramInetAddress)))
                    localObject = localRouteInfo;
            }
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool1 = true;
        if (this == paramObject);
        label128: 
        while (true)
        {
            return bool1;
            if (!(paramObject instanceof RouteInfo))
            {
                bool1 = false;
            }
            else
            {
                RouteInfo localRouteInfo = (RouteInfo)paramObject;
                boolean bool2;
                label43: boolean bool3;
                if (this.mDestination == null)
                    if (localRouteInfo.getDestination() == null)
                    {
                        bool2 = bool1;
                        if (this.mGateway != null)
                            break label114;
                        if (localRouteInfo.getGateway() != null)
                            break label108;
                        bool3 = bool1;
                    }
                while (true)
                {
                    if ((bool2) && (bool3) && (this.mIsDefault == localRouteInfo.mIsDefault))
                        break label128;
                    bool1 = false;
                    break;
                    bool2 = false;
                    break label43;
                    bool2 = this.mDestination.equals(localRouteInfo.getDestination());
                    break label43;
                    label108: bool3 = false;
                    continue;
                    label114: bool3 = this.mGateway.equals(localRouteInfo.getGateway());
                }
            }
        }
    }

    public LinkAddress getDestination()
    {
        return this.mDestination;
    }

    public InetAddress getGateway()
    {
        return this.mGateway;
    }

    public int hashCode()
    {
        int i = 0;
        int j;
        label18: int k;
        if (this.mDestination == null)
        {
            j = 0;
            if (this.mGateway != null)
                break label48;
            k = i + j;
            if (!this.mIsDefault)
                break label59;
        }
        label48: label59: for (int m = 3; ; m = 7)
        {
            return m + k;
            j = this.mDestination.hashCode();
            break;
            i = this.mGateway.hashCode();
            break label18;
        }
    }

    public boolean isDefaultRoute()
    {
        return this.mIsDefault;
    }

    public boolean isHostRoute()
    {
        return this.mIsHost;
    }

    public String toString()
    {
        String str = "";
        if (this.mDestination != null)
            str = this.mDestination.toString();
        if (this.mGateway != null)
            str = str + " -> " + this.mGateway.getHostAddress();
        return str;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        if (this.mDestination == null)
        {
            paramParcel.writeByte((byte)0);
            if (this.mGateway != null)
                break label58;
            paramParcel.writeByte((byte)0);
        }
        while (true)
        {
            return;
            paramParcel.writeByte((byte)1);
            paramParcel.writeByteArray(this.mDestination.getAddress().getAddress());
            paramParcel.writeInt(this.mDestination.getNetworkPrefixLength());
            break;
            label58: paramParcel.writeByte((byte)1);
            paramParcel.writeByteArray(this.mGateway.getAddress());
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.RouteInfo
 * JD-Core Version:        0.6.2
 */