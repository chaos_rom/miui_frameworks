package android.net;

import android.util.Log;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Set;

public class LinkSocket extends Socket
{
    private static final boolean DBG = true;
    private static final String TAG = "LinkSocket";

    public LinkSocket()
    {
        log("LinkSocket() EX");
    }

    public LinkSocket(LinkSocketNotifier paramLinkSocketNotifier)
    {
        log("LinkSocket(notifier) EX");
    }

    public LinkSocket(LinkSocketNotifier paramLinkSocketNotifier, Proxy paramProxy)
    {
        log("LinkSocket(notifier, proxy) EX");
    }

    protected static void log(String paramString)
    {
        Log.d("LinkSocket", paramString);
    }

    @Deprecated
    public void bind(SocketAddress paramSocketAddress)
        throws UnsupportedOperationException
    {
        log("bind(localAddr) EX throws IOException");
        throw new UnsupportedOperationException("bind is deprecated for LinkSocket");
    }

    /** @deprecated */
    public void close()
        throws IOException
    {
        try
        {
            log("close() EX");
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void connect()
        throws IOException
    {
        log("connect() EX");
    }

    public void connect(int paramInt)
        throws IOException
    {
        log("connect(timeout) EX");
    }

    public void connect(String paramString, int paramInt)
        throws UnknownHostException, IOException
    {
        log("connect(dstName, dstPort, timeout) EX");
    }

    public void connect(String paramString, int paramInt1, int paramInt2)
        throws UnknownHostException, IOException, SocketTimeoutException
    {
        log("connect(dstName, dstPort, timeout) EX");
    }

    @Deprecated
    public void connect(SocketAddress paramSocketAddress)
        throws IOException
    {
        log("connect(remoteAddr) EX DEPRECATED");
    }

    @Deprecated
    public void connect(SocketAddress paramSocketAddress, int paramInt)
        throws IOException, SocketTimeoutException
    {
        log("connect(remoteAddr, timeout) EX DEPRECATED");
    }

    public LinkCapabilities getCapabilities()
    {
        log("getCapabilities() EX");
        return null;
    }

    public LinkCapabilities getCapabilities(Set<Integer> paramSet)
    {
        log("getCapabilities(capabilities) EX");
        return new LinkCapabilities();
    }

    public LinkProperties getLinkProperties()
    {
        log("LinkProperties() EX");
        return new LinkProperties();
    }

    public LinkCapabilities getNeededCapabilities()
    {
        log("getNeeds() EX");
        return null;
    }

    public Set<Integer> getTrackedCapabilities()
    {
        log("getTrackedCapabilities(capabilities) EX");
        return new HashSet();
    }

    public void requestNewLink(LinkRequestReason paramLinkRequestReason)
    {
        log("requestNewLink(linkRequestReason) EX");
    }

    public boolean setNeededCapabilities(LinkCapabilities paramLinkCapabilities)
    {
        log("setNeeds() EX");
        return false;
    }

    public void setTrackedCapabilities(Set<Integer> paramSet)
    {
        log("setTrackedCapabilities(capabilities) EX");
    }

    public static final class LinkRequestReason
    {
        public static final int LINK_PROBLEM_NONE = 0;
        public static final int LINK_PROBLEM_UNKNOWN = 1;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.LinkSocket
 * JD-Core Version:        0.6.2
 */