package android.net;

public class NetworkConfig
{
    public boolean dependencyMet;
    public String name;
    public int priority;
    public int radio;
    public int restoreTime;
    public int type;

    public NetworkConfig(String paramString)
    {
        String[] arrayOfString = paramString.split(",");
        this.name = arrayOfString[0].trim().toLowerCase();
        this.type = Integer.parseInt(arrayOfString[1]);
        this.radio = Integer.parseInt(arrayOfString[2]);
        this.priority = Integer.parseInt(arrayOfString[3]);
        this.restoreTime = Integer.parseInt(arrayOfString[4]);
        this.dependencyMet = Boolean.parseBoolean(arrayOfString[5]);
    }

    public boolean isDefault()
    {
        if (this.type == this.radio);
        for (boolean bool = true; ; bool = false)
            return bool;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.NetworkConfig
 * JD-Core Version:        0.6.2
 */