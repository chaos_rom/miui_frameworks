package android.net.dhcp;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.List;

class DhcpOfferPacket extends DhcpPacket
{
    private final InetAddress mSrcIp;

    DhcpOfferPacket(int paramInt, boolean paramBoolean, InetAddress paramInetAddress1, InetAddress paramInetAddress2, byte[] paramArrayOfByte)
    {
        super(paramInt, Inet4Address.ANY, paramInetAddress2, Inet4Address.ANY, Inet4Address.ANY, paramArrayOfByte, paramBoolean);
        this.mSrcIp = paramInetAddress1;
    }

    public ByteBuffer buildPacket(int paramInt, short paramShort1, short paramShort2)
    {
        ByteBuffer localByteBuffer = ByteBuffer.allocate(1500);
        InetAddress localInetAddress1;
        if (this.mBroadcast)
        {
            localInetAddress1 = Inet4Address.ALL;
            if (!this.mBroadcast)
                break label68;
        }
        label68: for (InetAddress localInetAddress2 = Inet4Address.ANY; ; localInetAddress2 = this.mSrcIp)
        {
            fillInPacket(paramInt, localInetAddress1, localInetAddress2, paramShort1, paramShort2, localByteBuffer, (byte)2, this.mBroadcast);
            localByteBuffer.flip();
            return localByteBuffer;
            localInetAddress1 = this.mYourIp;
            break;
        }
    }

    public void doNextOp(DhcpStateMachine paramDhcpStateMachine)
    {
        paramDhcpStateMachine.onOfferReceived(this.mBroadcast, this.mTransId, this.mClientMac, this.mYourIp, this.mServerIdentifier);
    }

    void finishPacket(ByteBuffer paramByteBuffer)
    {
        addTlv(paramByteBuffer, (byte)53, (byte)2);
        addTlv(paramByteBuffer, (byte)54, this.mServerIdentifier);
        addTlv(paramByteBuffer, (byte)51, this.mLeaseTime);
        if (this.mLeaseTime != null)
            addTlv(paramByteBuffer, (byte)58, Integer.valueOf(this.mLeaseTime.intValue() / 2));
        addTlv(paramByteBuffer, (byte)1, this.mSubnetMask);
        addTlv(paramByteBuffer, (byte)3, this.mGateway);
        addTlv(paramByteBuffer, (byte)15, this.mDomainName);
        addTlv(paramByteBuffer, (byte)28, this.mBroadcastAddress);
        addTlv(paramByteBuffer, (byte)6, this.mDnsServers);
        addTlvEnd(paramByteBuffer);
    }

    public String toString()
    {
        String str1 = super.toString();
        String str2 = ", DNS servers: ";
        if (this.mDnsServers != null)
        {
            Iterator localIterator = this.mDnsServers.iterator();
            while (localIterator.hasNext())
            {
                InetAddress localInetAddress = (InetAddress)localIterator.next();
                str2 = str2 + localInetAddress + " ";
            }
        }
        return str1 + " OFFER, ip " + this.mYourIp + ", mask " + this.mSubnetMask + str2 + ", gateway " + this.mGateway + " lease time " + this.mLeaseTime + ", domain " + this.mDomainName;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.dhcp.DhcpOfferPacket
 * JD-Core Version:        0.6.2
 */