package android.net.dhcp;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.List;

class DhcpAckPacket extends DhcpPacket
{
    private final InetAddress mSrcIp;

    DhcpAckPacket(int paramInt, boolean paramBoolean, InetAddress paramInetAddress1, InetAddress paramInetAddress2, byte[] paramArrayOfByte)
    {
        super(paramInt, Inet4Address.ANY, paramInetAddress2, paramInetAddress1, Inet4Address.ANY, paramArrayOfByte, paramBoolean);
        this.mBroadcast = paramBoolean;
        this.mSrcIp = paramInetAddress1;
    }

    private static final int getInt(Integer paramInteger)
    {
        if (paramInteger == null);
        for (int i = 0; ; i = paramInteger.intValue())
            return i;
    }

    public ByteBuffer buildPacket(int paramInt, short paramShort1, short paramShort2)
    {
        ByteBuffer localByteBuffer = ByteBuffer.allocate(1500);
        InetAddress localInetAddress1;
        if (this.mBroadcast)
        {
            localInetAddress1 = Inet4Address.ALL;
            if (!this.mBroadcast)
                break label68;
        }
        label68: for (InetAddress localInetAddress2 = Inet4Address.ANY; ; localInetAddress2 = this.mSrcIp)
        {
            fillInPacket(paramInt, localInetAddress1, localInetAddress2, paramShort1, paramShort2, localByteBuffer, (byte)2, this.mBroadcast);
            localByteBuffer.flip();
            return localByteBuffer;
            localInetAddress1 = this.mYourIp;
            break;
        }
    }

    public void doNextOp(DhcpStateMachine paramDhcpStateMachine)
    {
        paramDhcpStateMachine.onAckReceived(this.mYourIp, this.mSubnetMask, this.mGateway, this.mDnsServers, this.mServerIdentifier, getInt(this.mLeaseTime));
    }

    void finishPacket(ByteBuffer paramByteBuffer)
    {
        addTlv(paramByteBuffer, (byte)53, (byte)5);
        addTlv(paramByteBuffer, (byte)54, this.mServerIdentifier);
        addTlv(paramByteBuffer, (byte)51, this.mLeaseTime);
        if (this.mLeaseTime != null)
            addTlv(paramByteBuffer, (byte)58, Integer.valueOf(this.mLeaseTime.intValue() / 2));
        addTlv(paramByteBuffer, (byte)1, this.mSubnetMask);
        addTlv(paramByteBuffer, (byte)3, this.mGateway);
        addTlv(paramByteBuffer, (byte)15, this.mDomainName);
        addTlv(paramByteBuffer, (byte)28, this.mBroadcastAddress);
        addTlv(paramByteBuffer, (byte)6, this.mDnsServers);
        addTlvEnd(paramByteBuffer);
    }

    public String toString()
    {
        String str1 = super.toString();
        String str2 = " DNS servers: ";
        Iterator localIterator = this.mDnsServers.iterator();
        while (localIterator.hasNext())
        {
            InetAddress localInetAddress = (InetAddress)localIterator.next();
            str2 = str2 + localInetAddress.toString() + " ";
        }
        return str1 + " ACK: your new IP " + this.mYourIp + ", netmask " + this.mSubnetMask + ", gateway " + this.mGateway + str2 + ", lease time " + this.mLeaseTime;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.dhcp.DhcpAckPacket
 * JD-Core Version:        0.6.2
 */