package android.net.dhcp;

import java.net.InetAddress;
import java.util.List;

abstract interface DhcpStateMachine
{
    public abstract void onAckReceived(InetAddress paramInetAddress1, InetAddress paramInetAddress2, InetAddress paramInetAddress3, List<InetAddress> paramList, InetAddress paramInetAddress4, int paramInt);

    public abstract void onDeclineReceived(byte[] paramArrayOfByte, InetAddress paramInetAddress);

    public abstract void onDiscoverReceived(boolean paramBoolean, int paramInt, byte[] paramArrayOfByte1, byte[] paramArrayOfByte2);

    public abstract void onInformReceived(int paramInt, byte[] paramArrayOfByte1, InetAddress paramInetAddress, byte[] paramArrayOfByte2);

    public abstract void onNakReceived();

    public abstract void onOfferReceived(boolean paramBoolean, int paramInt, byte[] paramArrayOfByte, InetAddress paramInetAddress1, InetAddress paramInetAddress2);

    public abstract void onRequestReceived(boolean paramBoolean, int paramInt, byte[] paramArrayOfByte1, InetAddress paramInetAddress, byte[] paramArrayOfByte2, String paramString);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.dhcp.DhcpStateMachine
 * JD-Core Version:        0.6.2
 */