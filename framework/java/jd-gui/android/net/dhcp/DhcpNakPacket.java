package android.net.dhcp;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.nio.ByteBuffer;

class DhcpNakPacket extends DhcpPacket
{
    DhcpNakPacket(int paramInt, InetAddress paramInetAddress1, InetAddress paramInetAddress2, InetAddress paramInetAddress3, InetAddress paramInetAddress4, byte[] paramArrayOfByte)
    {
        super(paramInt, Inet4Address.ANY, Inet4Address.ANY, paramInetAddress3, paramInetAddress4, paramArrayOfByte, false);
    }

    public ByteBuffer buildPacket(int paramInt, short paramShort1, short paramShort2)
    {
        ByteBuffer localByteBuffer = ByteBuffer.allocate(1500);
        fillInPacket(paramInt, this.mClientIp, this.mYourIp, paramShort1, paramShort2, localByteBuffer, (byte)2, this.mBroadcast);
        localByteBuffer.flip();
        return localByteBuffer;
    }

    public void doNextOp(DhcpStateMachine paramDhcpStateMachine)
    {
        paramDhcpStateMachine.onNakReceived();
    }

    void finishPacket(ByteBuffer paramByteBuffer)
    {
        addTlv(paramByteBuffer, (byte)53, (byte)6);
        addTlv(paramByteBuffer, (byte)54, this.mServerIdentifier);
        addTlv(paramByteBuffer, (byte)56, this.mMessage);
        addTlvEnd(paramByteBuffer);
    }

    public String toString()
    {
        String str1 = super.toString();
        StringBuilder localStringBuilder = new StringBuilder().append(str1).append(" NAK, reason ");
        if (this.mMessage == null);
        for (String str2 = "(none)"; ; str2 = this.mMessage)
            return str2;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.dhcp.DhcpNakPacket
 * JD-Core Version:        0.6.2
 */