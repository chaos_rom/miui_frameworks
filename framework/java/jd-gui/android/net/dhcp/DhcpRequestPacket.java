package android.net.dhcp;

import android.util.Log;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.nio.ByteBuffer;

class DhcpRequestPacket extends DhcpPacket
{
    DhcpRequestPacket(int paramInt, InetAddress paramInetAddress, byte[] paramArrayOfByte, boolean paramBoolean)
    {
        super(paramInt, paramInetAddress, Inet4Address.ANY, Inet4Address.ANY, Inet4Address.ANY, paramArrayOfByte, paramBoolean);
    }

    public ByteBuffer buildPacket(int paramInt, short paramShort1, short paramShort2)
    {
        ByteBuffer localByteBuffer = ByteBuffer.allocate(1500);
        fillInPacket(paramInt, Inet4Address.ALL, Inet4Address.ANY, paramShort1, paramShort2, localByteBuffer, (byte)1, this.mBroadcast);
        localByteBuffer.flip();
        return localByteBuffer;
    }

    public void doNextOp(DhcpStateMachine paramDhcpStateMachine)
    {
        if (this.mRequestedIp == null);
        for (InetAddress localInetAddress = this.mClientIp; ; localInetAddress = this.mRequestedIp)
        {
            Log.v("DhcpPacket", "requested IP is " + this.mRequestedIp + " and client IP is " + this.mClientIp);
            paramDhcpStateMachine.onRequestReceived(this.mBroadcast, this.mTransId, this.mClientMac, localInetAddress, this.mRequestedParams, this.mHostName);
            return;
        }
    }

    void finishPacket(ByteBuffer paramByteBuffer)
    {
        byte[] arrayOfByte = new byte[7];
        arrayOfByte[0] = 1;
        System.arraycopy(this.mClientMac, 0, arrayOfByte, 1, 6);
        addTlv(paramByteBuffer, (byte)53, (byte)3);
        addTlv(paramByteBuffer, (byte)55, this.mRequestedParams);
        addTlv(paramByteBuffer, (byte)50, this.mRequestedIp);
        addTlv(paramByteBuffer, (byte)54, this.mServerIdentifier);
        addTlv(paramByteBuffer, (byte)61, arrayOfByte);
        addTlvEnd(paramByteBuffer);
    }

    public String toString()
    {
        String str = super.toString();
        StringBuilder localStringBuilder = new StringBuilder().append(str).append(" REQUEST, desired IP ").append(this.mRequestedIp).append(" from host '").append(this.mHostName).append("', param list length ");
        if (this.mRequestedParams == null);
        for (int i = 0; ; i = this.mRequestedParams.length)
            return i;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.dhcp.DhcpRequestPacket
 * JD-Core Version:        0.6.2
 */