package android.net.dhcp;

import java.net.Inet4Address;
import java.nio.ByteBuffer;

class DhcpDiscoverPacket extends DhcpPacket
{
    DhcpDiscoverPacket(int paramInt, byte[] paramArrayOfByte, boolean paramBoolean)
    {
        super(paramInt, Inet4Address.ANY, Inet4Address.ANY, Inet4Address.ANY, Inet4Address.ANY, paramArrayOfByte, paramBoolean);
    }

    public ByteBuffer buildPacket(int paramInt, short paramShort1, short paramShort2)
    {
        ByteBuffer localByteBuffer = ByteBuffer.allocate(1500);
        fillInPacket(paramInt, Inet4Address.ALL, Inet4Address.ANY, paramShort1, paramShort2, localByteBuffer, (byte)1, true);
        localByteBuffer.flip();
        return localByteBuffer;
    }

    public void doNextOp(DhcpStateMachine paramDhcpStateMachine)
    {
        paramDhcpStateMachine.onDiscoverReceived(this.mBroadcast, this.mTransId, this.mClientMac, this.mRequestedParams);
    }

    void finishPacket(ByteBuffer paramByteBuffer)
    {
        addTlv(paramByteBuffer, (byte)53, (byte)1);
        addTlv(paramByteBuffer, (byte)55, this.mRequestedParams);
        addTlvEnd(paramByteBuffer);
    }

    public String toString()
    {
        String str1 = super.toString();
        StringBuilder localStringBuilder = new StringBuilder().append(str1).append(" DISCOVER ");
        if (this.mBroadcast);
        for (String str2 = "broadcast "; ; str2 = "unicast ")
            return str2;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.dhcp.DhcpDiscoverPacket
 * JD-Core Version:        0.6.2
 */