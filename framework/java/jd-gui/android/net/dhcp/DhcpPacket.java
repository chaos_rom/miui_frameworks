package android.net.dhcp;

import java.io.PrintStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.nio.charset.Charsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

abstract class DhcpPacket
{
    protected static final byte CLIENT_ID_ETHER = 1;
    protected static final byte DHCP_BOOTREPLY = 2;
    protected static final byte DHCP_BOOTREQUEST = 1;
    protected static final byte DHCP_BROADCAST_ADDRESS = 28;
    static final short DHCP_CLIENT = 68;
    protected static final byte DHCP_CLIENT_IDENTIFIER = 61;
    protected static final byte DHCP_DNS_SERVER = 6;
    protected static final byte DHCP_DOMAIN_NAME = 15;
    protected static final byte DHCP_HOST_NAME = 12;
    protected static final byte DHCP_LEASE_TIME = 51;
    protected static final byte DHCP_MESSAGE = 56;
    protected static final byte DHCP_MESSAGE_TYPE = 53;
    protected static final byte DHCP_MESSAGE_TYPE_ACK = 5;
    protected static final byte DHCP_MESSAGE_TYPE_DECLINE = 4;
    protected static final byte DHCP_MESSAGE_TYPE_DISCOVER = 1;
    protected static final byte DHCP_MESSAGE_TYPE_INFORM = 8;
    protected static final byte DHCP_MESSAGE_TYPE_NAK = 6;
    protected static final byte DHCP_MESSAGE_TYPE_OFFER = 2;
    protected static final byte DHCP_MESSAGE_TYPE_REQUEST = 3;
    protected static final byte DHCP_PARAMETER_LIST = 55;
    protected static final byte DHCP_RENEWAL_TIME = 58;
    protected static final byte DHCP_REQUESTED_IP = 50;
    protected static final byte DHCP_ROUTER = 3;
    static final short DHCP_SERVER = 67;
    protected static final byte DHCP_SERVER_IDENTIFIER = 54;
    protected static final byte DHCP_SUBNET_MASK = 1;
    protected static final byte DHCP_VENDOR_CLASS_ID = 60;
    public static final int ENCAP_BOOTP = 2;
    public static final int ENCAP_L2 = 0;
    public static final int ENCAP_L3 = 1;
    private static final short IP_FLAGS_OFFSET = 16384;
    private static final byte IP_TOS_LOWDELAY = 16;
    private static final byte IP_TTL = 64;
    private static final byte IP_TYPE_UDP = 17;
    private static final byte IP_VERSION_HEADER_LEN = 69;
    protected static final int MAX_LENGTH = 1500;
    protected static final String TAG = "DhcpPacket";
    protected boolean mBroadcast;
    protected InetAddress mBroadcastAddress;
    protected final InetAddress mClientIp;
    protected final byte[] mClientMac;
    protected List<InetAddress> mDnsServers;
    protected String mDomainName;
    protected InetAddress mGateway;
    protected String mHostName;
    protected Integer mLeaseTime;
    protected String mMessage;
    private final InetAddress mNextIp;
    private final InetAddress mRelayIp;
    protected InetAddress mRequestedIp;
    protected byte[] mRequestedParams;
    protected InetAddress mServerIdentifier;
    protected InetAddress mSubnetMask;
    protected final int mTransId;
    protected final InetAddress mYourIp;

    protected DhcpPacket(int paramInt, InetAddress paramInetAddress1, InetAddress paramInetAddress2, InetAddress paramInetAddress3, InetAddress paramInetAddress4, byte[] paramArrayOfByte, boolean paramBoolean)
    {
        this.mTransId = paramInt;
        this.mClientIp = paramInetAddress1;
        this.mYourIp = paramInetAddress2;
        this.mNextIp = paramInetAddress3;
        this.mRelayIp = paramInetAddress4;
        this.mClientMac = paramArrayOfByte;
        this.mBroadcast = paramBoolean;
    }

    public static ByteBuffer buildAckPacket(int paramInt1, int paramInt2, boolean paramBoolean, InetAddress paramInetAddress1, InetAddress paramInetAddress2, byte[] paramArrayOfByte, Integer paramInteger, InetAddress paramInetAddress3, InetAddress paramInetAddress4, InetAddress paramInetAddress5, List<InetAddress> paramList, InetAddress paramInetAddress6, String paramString)
    {
        DhcpAckPacket localDhcpAckPacket = new DhcpAckPacket(paramInt2, paramBoolean, paramInetAddress1, paramInetAddress2, paramArrayOfByte);
        localDhcpAckPacket.mGateway = paramInetAddress5;
        localDhcpAckPacket.mDnsServers = paramList;
        localDhcpAckPacket.mLeaseTime = paramInteger;
        localDhcpAckPacket.mDomainName = paramString;
        localDhcpAckPacket.mSubnetMask = paramInetAddress3;
        localDhcpAckPacket.mServerIdentifier = paramInetAddress6;
        localDhcpAckPacket.mBroadcastAddress = paramInetAddress4;
        return localDhcpAckPacket.buildPacket(paramInt1, (short)68, (short)67);
    }

    public static ByteBuffer buildDiscoverPacket(int paramInt1, int paramInt2, byte[] paramArrayOfByte1, boolean paramBoolean, byte[] paramArrayOfByte2)
    {
        DhcpDiscoverPacket localDhcpDiscoverPacket = new DhcpDiscoverPacket(paramInt2, paramArrayOfByte1, paramBoolean);
        localDhcpDiscoverPacket.mRequestedParams = paramArrayOfByte2;
        return localDhcpDiscoverPacket.buildPacket(paramInt1, (short)67, (short)68);
    }

    public static ByteBuffer buildNakPacket(int paramInt1, int paramInt2, InetAddress paramInetAddress1, InetAddress paramInetAddress2, byte[] paramArrayOfByte)
    {
        DhcpNakPacket localDhcpNakPacket = new DhcpNakPacket(paramInt2, paramInetAddress2, paramInetAddress1, paramInetAddress1, paramInetAddress1, paramArrayOfByte);
        localDhcpNakPacket.mMessage = "requested address not available";
        localDhcpNakPacket.mRequestedIp = paramInetAddress2;
        return localDhcpNakPacket.buildPacket(paramInt1, (short)68, (short)67);
    }

    public static ByteBuffer buildOfferPacket(int paramInt1, int paramInt2, boolean paramBoolean, InetAddress paramInetAddress1, InetAddress paramInetAddress2, byte[] paramArrayOfByte, Integer paramInteger, InetAddress paramInetAddress3, InetAddress paramInetAddress4, InetAddress paramInetAddress5, List<InetAddress> paramList, InetAddress paramInetAddress6, String paramString)
    {
        DhcpOfferPacket localDhcpOfferPacket = new DhcpOfferPacket(paramInt2, paramBoolean, paramInetAddress1, paramInetAddress2, paramArrayOfByte);
        localDhcpOfferPacket.mGateway = paramInetAddress5;
        localDhcpOfferPacket.mDnsServers = paramList;
        localDhcpOfferPacket.mLeaseTime = paramInteger;
        localDhcpOfferPacket.mDomainName = paramString;
        localDhcpOfferPacket.mServerIdentifier = paramInetAddress6;
        localDhcpOfferPacket.mSubnetMask = paramInetAddress3;
        localDhcpOfferPacket.mBroadcastAddress = paramInetAddress4;
        return localDhcpOfferPacket.buildPacket(paramInt1, (short)68, (short)67);
    }

    public static ByteBuffer buildRequestPacket(int paramInt1, int paramInt2, InetAddress paramInetAddress1, boolean paramBoolean, byte[] paramArrayOfByte1, InetAddress paramInetAddress2, InetAddress paramInetAddress3, byte[] paramArrayOfByte2, String paramString)
    {
        DhcpRequestPacket localDhcpRequestPacket = new DhcpRequestPacket(paramInt2, paramInetAddress1, paramArrayOfByte1, paramBoolean);
        localDhcpRequestPacket.mRequestedIp = paramInetAddress2;
        localDhcpRequestPacket.mServerIdentifier = paramInetAddress3;
        localDhcpRequestPacket.mHostName = paramString;
        localDhcpRequestPacket.mRequestedParams = paramArrayOfByte2;
        return localDhcpRequestPacket.buildPacket(paramInt1, (short)67, (short)68);
    }

    private int checksum(ByteBuffer paramByteBuffer, int paramInt1, int paramInt2, int paramInt3)
    {
        int i = paramInt1;
        int j = paramByteBuffer.position();
        paramByteBuffer.position(paramInt2);
        ShortBuffer localShortBuffer = paramByteBuffer.asShortBuffer();
        paramByteBuffer.position(j);
        short[] arrayOfShort = new short[(paramInt3 - paramInt2) / 2];
        localShortBuffer.get(arrayOfShort);
        int k = arrayOfShort.length;
        for (int m = 0; m < k; m++)
            i += intAbs(arrayOfShort[m]);
        int n = paramInt2 + 2 * arrayOfShort.length;
        if (paramInt3 != n)
        {
            int i2 = (short)paramByteBuffer.get(n);
            if (i2 < 0)
                i2 = (short)(i2 + 256);
            i += i2 * 256;
        }
        int i1 = (0xFFFF & i >> 16) + (0xFFFF & i);
        return intAbs((short)(0xFFFFFFFF ^ 0xFFFF & i1 + (0xFFFF & i1 >> 16)));
    }

    public static DhcpPacket decodeFullPacket(ByteBuffer paramByteBuffer, int paramInt)
    {
        ArrayList localArrayList = new ArrayList();
        InetAddress localInetAddress1 = null;
        Integer localInteger = null;
        InetAddress localInetAddress2 = null;
        InetAddress localInetAddress3 = null;
        String str1 = null;
        byte[] arrayOfByte1 = null;
        String str2 = null;
        String str3 = null;
        InetAddress localInetAddress4 = null;
        InetAddress localInetAddress5 = null;
        InetAddress localInetAddress6 = null;
        int i = -1;
        paramByteBuffer.order(ByteOrder.BIG_ENDIAN);
        if (paramInt == 0)
        {
            byte[] arrayOfByte4 = new byte[6];
            byte[] arrayOfByte5 = new byte[6];
            paramByteBuffer.get(arrayOfByte4);
            paramByteBuffer.get(arrayOfByte5);
            if (paramByteBuffer.getShort() != 2048)
                localObject = null;
        }
        int n;
        boolean bool;
        InetAddress localInetAddress7;
        InetAddress localInetAddress8;
        InetAddress localInetAddress9;
        InetAddress localInetAddress10;
        byte[] arrayOfByte3;
        while (true)
        {
            return localObject;
            if ((paramInt == 0) || (paramInt == 1))
            {
                paramByteBuffer.get();
                paramByteBuffer.get();
                paramByteBuffer.getShort();
                paramByteBuffer.getShort();
                paramByteBuffer.get();
                paramByteBuffer.get();
                paramByteBuffer.get();
                int j = paramByteBuffer.get();
                paramByteBuffer.getShort();
                localInetAddress4 = readIpAddress(paramByteBuffer);
                readIpAddress(paramByteBuffer);
                if (j != 17)
                {
                    localObject = null;
                }
                else
                {
                    int k = paramByteBuffer.getShort();
                    paramByteBuffer.getShort();
                    paramByteBuffer.getShort();
                    paramByteBuffer.getShort();
                    if ((k != 67) && (k != 68))
                        localObject = null;
                }
            }
            else
            {
                paramByteBuffer.get();
                paramByteBuffer.get();
                int m = paramByteBuffer.get();
                paramByteBuffer.get();
                n = paramByteBuffer.getInt();
                paramByteBuffer.getShort();
                byte[] arrayOfByte2;
                if ((0x8000 & paramByteBuffer.getShort()) != 0)
                {
                    bool = true;
                    arrayOfByte2 = new byte[4];
                }
                try
                {
                    paramByteBuffer.get(arrayOfByte2);
                    localInetAddress7 = InetAddress.getByAddress(arrayOfByte2);
                    paramByteBuffer.get(arrayOfByte2);
                    localInetAddress8 = InetAddress.getByAddress(arrayOfByte2);
                    paramByteBuffer.get(arrayOfByte2);
                    localInetAddress9 = InetAddress.getByAddress(arrayOfByte2);
                    paramByteBuffer.get(arrayOfByte2);
                    localInetAddress10 = InetAddress.getByAddress(arrayOfByte2);
                    arrayOfByte3 = new byte[m];
                    paramByteBuffer.get(arrayOfByte3);
                    paramByteBuffer.position(128 + (64 + (paramByteBuffer.position() + (16 - m))));
                    if (paramByteBuffer.getInt() == 1669485411)
                        break label388;
                    localObject = null;
                    continue;
                    bool = false;
                }
                catch (UnknownHostException localUnknownHostException)
                {
                    localObject = null;
                }
                continue;
                label388: int i1 = 1;
                label391: 
                while ((paramByteBuffer.position() < paramByteBuffer.limit()) && (i1 != 0))
                {
                    int i2 = paramByteBuffer.get();
                    if (i2 == -1)
                    {
                        i1 = 0;
                    }
                    else
                    {
                        int i3 = paramByteBuffer.get();
                        int i4 = 0;
                        int i5;
                        switch (i2)
                        {
                        default:
                            i5 = 0;
                        case 1:
                            while (i5 < i3)
                            {
                                i4 += 1;
                                paramByteBuffer.get();
                                i5 += 1;
                                continue;
                                localInetAddress3 = readIpAddress(paramByteBuffer);
                                i4 = 4;
                            }
                        case 3:
                        case 6:
                        case 12:
                        case 15:
                        case 28:
                        case 50:
                        case 51:
                        case 53:
                        case 54:
                        case 55:
                        case 56:
                        case 60:
                        case 61:
                        }
                        while (true)
                        {
                            if (i4 == i3)
                                break label391;
                            localObject = null;
                            break;
                            localInetAddress1 = readIpAddress(paramByteBuffer);
                            i4 = 4;
                            continue;
                            i4 = 0;
                            while (i4 < i3)
                            {
                                localArrayList.add(readIpAddress(paramByteBuffer));
                                i4 += 4;
                            }
                            i4 = i3;
                            str2 = readAsciiString(paramByteBuffer, i3);
                            continue;
                            i4 = i3;
                            str3 = readAsciiString(paramByteBuffer, i3);
                            continue;
                            localInetAddress5 = readIpAddress(paramByteBuffer);
                            i4 = 4;
                            continue;
                            localInetAddress6 = readIpAddress(paramByteBuffer);
                            i4 = 4;
                            continue;
                            localInteger = Integer.valueOf(paramByteBuffer.getInt());
                            i4 = 4;
                            continue;
                            i = paramByteBuffer.get();
                            i4 = 1;
                            continue;
                            localInetAddress2 = readIpAddress(paramByteBuffer);
                            i4 = 4;
                            continue;
                            arrayOfByte1 = new byte[i3];
                            paramByteBuffer.get(arrayOfByte1);
                            i4 = i3;
                            continue;
                            i4 = i3;
                            str1 = readAsciiString(paramByteBuffer, i3);
                            continue;
                            i4 = i3;
                            readAsciiString(paramByteBuffer, i3);
                            continue;
                            paramByteBuffer.get(new byte[i3]);
                            i4 = i3;
                        }
                    }
                }
                switch (i)
                {
                case 0:
                case 7:
                default:
                    System.out.println("Unimplemented type: " + i);
                    localObject = null;
                    break;
                case -1:
                    localObject = null;
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 8:
                }
            }
        }
        Object localObject = new DhcpDiscoverPacket(n, arrayOfByte3, bool);
        while (true)
        {
            ((DhcpPacket)localObject).mBroadcastAddress = localInetAddress5;
            ((DhcpPacket)localObject).mDnsServers = localArrayList;
            ((DhcpPacket)localObject).mDomainName = str3;
            ((DhcpPacket)localObject).mGateway = localInetAddress1;
            ((DhcpPacket)localObject).mHostName = str2;
            ((DhcpPacket)localObject).mLeaseTime = localInteger;
            ((DhcpPacket)localObject).mMessage = str1;
            ((DhcpPacket)localObject).mRequestedIp = localInetAddress6;
            ((DhcpPacket)localObject).mRequestedParams = arrayOfByte1;
            ((DhcpPacket)localObject).mServerIdentifier = localInetAddress2;
            ((DhcpPacket)localObject).mSubnetMask = localInetAddress3;
            break;
            localObject = new DhcpOfferPacket(n, bool, localInetAddress4, localInetAddress8, arrayOfByte3);
            continue;
            localObject = new DhcpRequestPacket(n, localInetAddress7, arrayOfByte3, bool);
            continue;
            localObject = new DhcpDeclinePacket(n, localInetAddress7, localInetAddress8, localInetAddress9, localInetAddress10, arrayOfByte3);
            continue;
            localObject = new DhcpAckPacket(n, bool, localInetAddress4, localInetAddress8, arrayOfByte3);
            continue;
            localObject = new DhcpNakPacket(n, localInetAddress7, localInetAddress8, localInetAddress9, localInetAddress10, arrayOfByte3);
            continue;
            localObject = new DhcpInformPacket(n, localInetAddress7, localInetAddress8, localInetAddress9, localInetAddress10, arrayOfByte3);
        }
    }

    public static DhcpPacket decodeFullPacket(byte[] paramArrayOfByte, int paramInt)
    {
        return decodeFullPacket(ByteBuffer.wrap(paramArrayOfByte).order(ByteOrder.BIG_ENDIAN), paramInt);
    }

    private int intAbs(short paramShort)
    {
        if (paramShort < 0);
        for (int i = paramShort + 65536; ; i = paramShort)
            return i;
    }

    public static String macToString(byte[] paramArrayOfByte)
    {
        String str1 = "";
        for (int i = 0; i < paramArrayOfByte.length; i++)
        {
            String str2 = "0" + Integer.toHexString(paramArrayOfByte[i]);
            str1 = str1 + str2.substring(-2 + str2.length());
            if (i != -1 + paramArrayOfByte.length)
                str1 = str1 + ":";
        }
        return str1;
    }

    private static String readAsciiString(ByteBuffer paramByteBuffer, int paramInt)
    {
        byte[] arrayOfByte = new byte[paramInt];
        paramByteBuffer.get(arrayOfByte);
        return new String(arrayOfByte, 0, arrayOfByte.length, Charsets.US_ASCII);
    }

    private static InetAddress readIpAddress(ByteBuffer paramByteBuffer)
    {
        byte[] arrayOfByte = new byte[4];
        paramByteBuffer.get(arrayOfByte);
        try
        {
            InetAddress localInetAddress2 = InetAddress.getByAddress(arrayOfByte);
            localInetAddress1 = localInetAddress2;
            return localInetAddress1;
        }
        catch (UnknownHostException localUnknownHostException)
        {
            while (true)
                InetAddress localInetAddress1 = null;
        }
    }

    protected void addTlv(ByteBuffer paramByteBuffer, byte paramByte1, byte paramByte2)
    {
        paramByteBuffer.put(paramByte1);
        paramByteBuffer.put((byte)1);
        paramByteBuffer.put(paramByte2);
    }

    protected void addTlv(ByteBuffer paramByteBuffer, byte paramByte, Integer paramInteger)
    {
        if (paramInteger != null)
        {
            paramByteBuffer.put(paramByte);
            paramByteBuffer.put((byte)4);
            paramByteBuffer.putInt(paramInteger.intValue());
        }
    }

    protected void addTlv(ByteBuffer paramByteBuffer, byte paramByte, String paramString)
    {
        if (paramString != null)
        {
            paramByteBuffer.put(paramByte);
            paramByteBuffer.put((byte)paramString.length());
            for (int i = 0; i < paramString.length(); i++)
                paramByteBuffer.put((byte)paramString.charAt(i));
        }
    }

    protected void addTlv(ByteBuffer paramByteBuffer, byte paramByte, InetAddress paramInetAddress)
    {
        if (paramInetAddress != null)
            addTlv(paramByteBuffer, paramByte, paramInetAddress.getAddress());
    }

    protected void addTlv(ByteBuffer paramByteBuffer, byte paramByte, List<InetAddress> paramList)
    {
        if ((paramList != null) && (paramList.size() > 0))
        {
            paramByteBuffer.put(paramByte);
            paramByteBuffer.put((byte)(4 * paramList.size()));
            Iterator localIterator = paramList.iterator();
            while (localIterator.hasNext())
                paramByteBuffer.put(((InetAddress)localIterator.next()).getAddress());
        }
    }

    protected void addTlv(ByteBuffer paramByteBuffer, byte paramByte, byte[] paramArrayOfByte)
    {
        if (paramArrayOfByte != null)
        {
            paramByteBuffer.put(paramByte);
            paramByteBuffer.put((byte)paramArrayOfByte.length);
            paramByteBuffer.put(paramArrayOfByte);
        }
    }

    protected void addTlvEnd(ByteBuffer paramByteBuffer)
    {
        paramByteBuffer.put((byte)-1);
    }

    public abstract ByteBuffer buildPacket(int paramInt, short paramShort1, short paramShort2);

    public abstract void doNextOp(DhcpStateMachine paramDhcpStateMachine);

    protected void fillInPacket(int paramInt, InetAddress paramInetAddress1, InetAddress paramInetAddress2, short paramShort1, short paramShort2, ByteBuffer paramByteBuffer, byte paramByte, boolean paramBoolean)
    {
        byte[] arrayOfByte1 = paramInetAddress1.getAddress();
        byte[] arrayOfByte2 = paramInetAddress2.getAddress();
        int i = 0;
        int j = 0;
        int k = 0;
        int m = 0;
        int n = 0;
        int i1 = 0;
        paramByteBuffer.clear();
        paramByteBuffer.order(ByteOrder.BIG_ENDIAN);
        if (paramInt == 1)
        {
            paramByteBuffer.put((byte)69);
            paramByteBuffer.put((byte)16);
            i = paramByteBuffer.position();
            paramByteBuffer.putShort((short)0);
            paramByteBuffer.putShort((short)0);
            paramByteBuffer.putShort((short)16384);
            paramByteBuffer.put((byte)64);
            paramByteBuffer.put((byte)17);
            j = paramByteBuffer.position();
            paramByteBuffer.putShort((short)0);
            paramByteBuffer.put(arrayOfByte2);
            paramByteBuffer.put(arrayOfByte1);
            k = paramByteBuffer.position();
            m = paramByteBuffer.position();
            paramByteBuffer.putShort(paramShort2);
            paramByteBuffer.putShort(paramShort1);
            n = paramByteBuffer.position();
            paramByteBuffer.putShort((short)0);
            i1 = paramByteBuffer.position();
            paramByteBuffer.putShort((short)0);
        }
        paramByteBuffer.put(paramByte);
        paramByteBuffer.put((byte)1);
        paramByteBuffer.put((byte)this.mClientMac.length);
        paramByteBuffer.put((byte)0);
        paramByteBuffer.putInt(this.mTransId);
        paramByteBuffer.putShort((short)0);
        if (paramBoolean)
            paramByteBuffer.putShort((short)-32768);
        while (true)
        {
            paramByteBuffer.put(this.mClientIp.getAddress());
            paramByteBuffer.put(this.mYourIp.getAddress());
            paramByteBuffer.put(this.mNextIp.getAddress());
            paramByteBuffer.put(this.mRelayIp.getAddress());
            paramByteBuffer.put(this.mClientMac);
            paramByteBuffer.position(128 + (64 + (paramByteBuffer.position() + (16 - this.mClientMac.length))));
            paramByteBuffer.putInt(1669485411);
            finishPacket(paramByteBuffer);
            if ((0x1 & paramByteBuffer.position()) == 1)
                paramByteBuffer.put((byte)0);
            if (paramInt == 1)
            {
                int i2 = (short)(paramByteBuffer.position() - m);
                paramByteBuffer.putShort(n, i2);
                paramByteBuffer.putShort(i1, (short)checksum(paramByteBuffer, i2 + (17 + (0 + intAbs(paramByteBuffer.getShort(j + 2)) + intAbs(paramByteBuffer.getShort(j + 4)) + intAbs(paramByteBuffer.getShort(j + 6)) + intAbs(paramByteBuffer.getShort(j + 8)))), m, paramByteBuffer.position()));
                paramByteBuffer.putShort(i, (short)paramByteBuffer.position());
                paramByteBuffer.putShort(j, (short)checksum(paramByteBuffer, 0, 0, k));
            }
            return;
            paramByteBuffer.putShort((short)0);
        }
    }

    abstract void finishPacket(ByteBuffer paramByteBuffer);

    public int getTransactionId()
    {
        return this.mTransId;
    }

    public String toString()
    {
        return macToString(this.mClientMac);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.dhcp.DhcpPacket
 * JD-Core Version:        0.6.2
 */