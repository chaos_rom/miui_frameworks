package android.net.dhcp;

import java.net.InetAddress;
import java.nio.ByteBuffer;

class DhcpDeclinePacket extends DhcpPacket
{
    DhcpDeclinePacket(int paramInt, InetAddress paramInetAddress1, InetAddress paramInetAddress2, InetAddress paramInetAddress3, InetAddress paramInetAddress4, byte[] paramArrayOfByte)
    {
        super(paramInt, paramInetAddress1, paramInetAddress2, paramInetAddress3, paramInetAddress4, paramArrayOfByte, false);
    }

    public ByteBuffer buildPacket(int paramInt, short paramShort1, short paramShort2)
    {
        ByteBuffer localByteBuffer = ByteBuffer.allocate(1500);
        fillInPacket(paramInt, this.mClientIp, this.mYourIp, paramShort1, paramShort2, localByteBuffer, (byte)1, false);
        localByteBuffer.flip();
        return localByteBuffer;
    }

    public void doNextOp(DhcpStateMachine paramDhcpStateMachine)
    {
        paramDhcpStateMachine.onDeclineReceived(this.mClientMac, this.mRequestedIp);
    }

    void finishPacket(ByteBuffer paramByteBuffer)
    {
    }

    public String toString()
    {
        String str = super.toString();
        return str + " DECLINE";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.dhcp.DhcpDeclinePacket
 * JD-Core Version:        0.6.2
 */