package android.net.dhcp;

import java.net.InetAddress;
import java.nio.ByteBuffer;

class DhcpInformPacket extends DhcpPacket
{
    DhcpInformPacket(int paramInt, InetAddress paramInetAddress1, InetAddress paramInetAddress2, InetAddress paramInetAddress3, InetAddress paramInetAddress4, byte[] paramArrayOfByte)
    {
        super(paramInt, paramInetAddress1, paramInetAddress2, paramInetAddress3, paramInetAddress4, paramArrayOfByte, false);
    }

    public ByteBuffer buildPacket(int paramInt, short paramShort1, short paramShort2)
    {
        ByteBuffer localByteBuffer = ByteBuffer.allocate(1500);
        fillInPacket(paramInt, this.mClientIp, this.mYourIp, paramShort1, paramShort2, localByteBuffer, (byte)1, false);
        localByteBuffer.flip();
        return localByteBuffer;
    }

    public void doNextOp(DhcpStateMachine paramDhcpStateMachine)
    {
        if (this.mRequestedIp == null);
        for (InetAddress localInetAddress = this.mClientIp; ; localInetAddress = this.mRequestedIp)
        {
            paramDhcpStateMachine.onInformReceived(this.mTransId, this.mClientMac, localInetAddress, this.mRequestedParams);
            return;
        }
    }

    void finishPacket(ByteBuffer paramByteBuffer)
    {
        byte[] arrayOfByte = new byte[7];
        arrayOfByte[0] = 1;
        System.arraycopy(this.mClientMac, 0, arrayOfByte, 1, 6);
        addTlv(paramByteBuffer, (byte)53, (byte)3);
        addTlv(paramByteBuffer, (byte)55, this.mRequestedParams);
        addTlvEnd(paramByteBuffer);
    }

    public String toString()
    {
        String str = super.toString();
        return str + " INFORM";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.dhcp.DhcpInformPacket
 * JD-Core Version:        0.6.2
 */