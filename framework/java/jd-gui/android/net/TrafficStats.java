package android.net;

import android.content.Context;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import com.android.server.NetworkManagementSocketTagger;
import dalvik.system.SocketTagger;
import java.net.Socket;
import java.net.SocketException;

public class TrafficStats
{
    public static final long GB_IN_BYTES = 1073741824L;
    public static final long KB_IN_BYTES = 1024L;
    public static final long MB_IN_BYTES = 1048576L;
    public static final int TAG_SYSTEM_BACKUP = -253;
    public static final int TAG_SYSTEM_DOWNLOAD = -255;
    public static final int TAG_SYSTEM_MEDIA = -254;
    private static final int TYPE_RX_BYTES = 0;
    private static final int TYPE_RX_PACKETS = 1;
    private static final int TYPE_TX_BYTES = 2;
    private static final int TYPE_TX_PACKETS = 3;
    public static final int UID_REMOVED = -4;
    public static final int UID_TETHERING = -5;
    public static final int UNSUPPORTED = -1;
    private static NetworkStats sActiveProfilingStart;
    private static Object sProfilingLock = new Object();
    private static INetworkStatsService sStatsService;

    public static void clearThreadStatsTag()
    {
        NetworkManagementSocketTagger.setThreadSocketStatsTag(-1);
    }

    public static void clearThreadStatsUid()
    {
        NetworkManagementSocketTagger.setThreadSocketStatsUid(-1);
    }

    public static void closeQuietly(INetworkStatsSession paramINetworkStatsSession)
    {
        if (paramINetworkStatsSession != null);
        try
        {
            paramINetworkStatsSession.close();
            label10: return;
        }
        catch (RuntimeException localRuntimeException)
        {
            throw localRuntimeException;
        }
        catch (Exception localException)
        {
            break label10;
        }
    }

    private static NetworkStats getDataLayerSnapshotForUid(Context paramContext)
    {
        int i = Process.myUid();
        try
        {
            NetworkStats localNetworkStats = getStatsService().getDataLayerSnapshotForUid(i);
            return localNetworkStats;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    private static String[] getMobileIfaces()
    {
        try
        {
            String[] arrayOfString = getStatsService().getMobileIfaces();
            return arrayOfString;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    public static long getMobileRxBytes()
    {
        long l = 0L;
        String[] arrayOfString = getMobileIfaces();
        int i = arrayOfString.length;
        for (int j = 0; j < i; j++)
            l += getRxBytes(arrayOfString[j]);
        return l;
    }

    public static long getMobileRxPackets()
    {
        long l = 0L;
        String[] arrayOfString = getMobileIfaces();
        int i = arrayOfString.length;
        for (int j = 0; j < i; j++)
            l += getRxPackets(arrayOfString[j]);
        return l;
    }

    public static long getMobileTxBytes()
    {
        long l = 0L;
        String[] arrayOfString = getMobileIfaces();
        int i = arrayOfString.length;
        for (int j = 0; j < i; j++)
            l += getTxBytes(arrayOfString[j]);
        return l;
    }

    public static long getMobileTxPackets()
    {
        long l = 0L;
        String[] arrayOfString = getMobileIfaces();
        int i = arrayOfString.length;
        for (int j = 0; j < i; j++)
            l += getTxPackets(arrayOfString[j]);
        return l;
    }

    public static long getRxBytes(String paramString)
    {
        return nativeGetIfaceStat(paramString, 0);
    }

    public static long getRxPackets(String paramString)
    {
        return nativeGetIfaceStat(paramString, 1);
    }

    /** @deprecated */
    private static INetworkStatsService getStatsService()
    {
        try
        {
            if (sStatsService == null)
                sStatsService = INetworkStatsService.Stub.asInterface(ServiceManager.getService("netstats"));
            INetworkStatsService localINetworkStatsService = sStatsService;
            return localINetworkStatsService;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public static int getThreadStatsTag()
    {
        return NetworkManagementSocketTagger.getThreadSocketStatsTag();
    }

    public static long getTotalRxBytes()
    {
        return nativeGetTotalStat(0);
    }

    public static long getTotalRxPackets()
    {
        return nativeGetTotalStat(1);
    }

    public static long getTotalTxBytes()
    {
        return nativeGetTotalStat(2);
    }

    public static long getTotalTxPackets()
    {
        return nativeGetTotalStat(3);
    }

    public static long getTxBytes(String paramString)
    {
        return nativeGetIfaceStat(paramString, 2);
    }

    public static long getTxPackets(String paramString)
    {
        return nativeGetIfaceStat(paramString, 3);
    }

    public static native long getUidRxBytes(int paramInt);

    public static native long getUidRxPackets(int paramInt);

    public static native long getUidTcpRxBytes(int paramInt);

    public static native long getUidTcpRxSegments(int paramInt);

    public static native long getUidTcpTxBytes(int paramInt);

    public static native long getUidTcpTxSegments(int paramInt);

    public static native long getUidTxBytes(int paramInt);

    public static native long getUidTxPackets(int paramInt);

    public static native long getUidUdpRxBytes(int paramInt);

    public static native long getUidUdpRxPackets(int paramInt);

    public static native long getUidUdpTxBytes(int paramInt);

    public static native long getUidUdpTxPackets(int paramInt);

    public static void incrementOperationCount(int paramInt)
    {
        incrementOperationCount(getThreadStatsTag(), paramInt);
    }

    public static void incrementOperationCount(int paramInt1, int paramInt2)
    {
        int i = Process.myUid();
        try
        {
            getStatsService().incrementOperationCount(i, paramInt1, paramInt2);
            return;
        }
        catch (RemoteException localRemoteException)
        {
            throw new RuntimeException(localRemoteException);
        }
    }

    private static native long nativeGetIfaceStat(String paramString, int paramInt);

    private static native long nativeGetTotalStat(int paramInt);

    public static void setThreadStatsTag(int paramInt)
    {
        NetworkManagementSocketTagger.setThreadSocketStatsTag(paramInt);
    }

    public static void setThreadStatsUid(int paramInt)
    {
        NetworkManagementSocketTagger.setThreadSocketStatsUid(paramInt);
    }

    // ERROR //
    public static void startDataProfiling(Context paramContext)
    {
        // Byte code:
        //     0: getstatic 48	android/net/TrafficStats:sProfilingLock	Ljava/lang/Object;
        //     3: astore_1
        //     4: aload_1
        //     5: monitorenter
        //     6: getstatic 174	android/net/TrafficStats:sActiveProfilingStart	Landroid/net/NetworkStats;
        //     9: ifnull +18 -> 27
        //     12: new 176	java/lang/IllegalStateException
        //     15: dup
        //     16: ldc 178
        //     18: invokespecial 181	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     21: athrow
        //     22: astore_2
        //     23: aload_1
        //     24: monitorexit
        //     25: aload_2
        //     26: athrow
        //     27: aload_0
        //     28: invokestatic 183	android/net/TrafficStats:getDataLayerSnapshotForUid	(Landroid/content/Context;)Landroid/net/NetworkStats;
        //     31: putstatic 174	android/net/TrafficStats:sActiveProfilingStart	Landroid/net/NetworkStats;
        //     34: aload_1
        //     35: monitorexit
        //     36: return
        //
        // Exception table:
        //     from	to	target	type
        //     6	25	22	finally
        //     27	36	22	finally
    }

    // ERROR //
    public static NetworkStats stopDataProfiling(Context paramContext)
    {
        // Byte code:
        //     0: getstatic 48	android/net/TrafficStats:sProfilingLock	Ljava/lang/Object;
        //     3: astore_1
        //     4: aload_1
        //     5: monitorenter
        //     6: getstatic 174	android/net/TrafficStats:sActiveProfilingStart	Landroid/net/NetworkStats;
        //     9: ifnonnull +18 -> 27
        //     12: new 176	java/lang/IllegalStateException
        //     15: dup
        //     16: ldc 186
        //     18: invokespecial 181	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     21: athrow
        //     22: astore_2
        //     23: aload_1
        //     24: monitorexit
        //     25: aload_2
        //     26: athrow
        //     27: aload_0
        //     28: invokestatic 183	android/net/TrafficStats:getDataLayerSnapshotForUid	(Landroid/content/Context;)Landroid/net/NetworkStats;
        //     31: getstatic 174	android/net/TrafficStats:sActiveProfilingStart	Landroid/net/NetworkStats;
        //     34: aconst_null
        //     35: aconst_null
        //     36: invokestatic 192	android/net/NetworkStats:subtract	(Landroid/net/NetworkStats;Landroid/net/NetworkStats;Landroid/net/NetworkStats$NonMonotonicObserver;Ljava/lang/Object;)Landroid/net/NetworkStats;
        //     39: astore_3
        //     40: aconst_null
        //     41: putstatic 174	android/net/TrafficStats:sActiveProfilingStart	Landroid/net/NetworkStats;
        //     44: aload_1
        //     45: monitorexit
        //     46: aload_3
        //     47: areturn
        //
        // Exception table:
        //     from	to	target	type
        //     6	25	22	finally
        //     27	46	22	finally
    }

    public static void tagSocket(Socket paramSocket)
        throws SocketException
    {
        SocketTagger.get().tag(paramSocket);
    }

    public static void untagSocket(Socket paramSocket)
        throws SocketException
    {
        SocketTagger.get().untag(paramSocket);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.TrafficStats
 * JD-Core Version:        0.6.2
 */