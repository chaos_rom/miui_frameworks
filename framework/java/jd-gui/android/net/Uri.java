package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Log;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charsets;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.RandomAccess;
import java.util.Set;
import libcore.net.UriCodec;

public abstract class Uri
    implements Parcelable, Comparable<Uri>
{
    public static final Parcelable.Creator<Uri> CREATOR = new Parcelable.Creator()
    {
        public Uri createFromParcel(Parcel paramAnonymousParcel)
        {
            int i = paramAnonymousParcel.readInt();
            Uri localUri;
            switch (i)
            {
            default:
                throw new IllegalArgumentException("Unknown URI type: " + i);
            case 0:
                localUri = null;
            case 1:
            case 2:
            case 3:
            }
            while (true)
            {
                return localUri;
                localUri = Uri.StringUri.readFrom(paramAnonymousParcel);
                continue;
                localUri = Uri.OpaqueUri.readFrom(paramAnonymousParcel);
                continue;
                localUri = Uri.HierarchicalUri.readFrom(paramAnonymousParcel);
            }
        }

        public Uri[] newArray(int paramAnonymousInt)
        {
            return new Uri[paramAnonymousInt];
        }
    };
    private static final String DEFAULT_ENCODING = "UTF-8";
    public static final Uri EMPTY;
    private static final char[] HEX_DIGITS = "0123456789ABCDEF".toCharArray();
    private static final String LOG = Uri.class.getSimpleName();
    private static final String NOT_CACHED = new String("NOT CACHED");
    private static final int NOT_CALCULATED = -2;
    private static final int NOT_FOUND = -1;
    private static final String NOT_HIERARCHICAL = "This isn't a hierarchical URI.";
    private static final int NULL_TYPE_ID;

    static
    {
        EMPTY = new HierarchicalUri(null, Part.NULL, PathPart.EMPTY, Part.NULL, Part.NULL, null);
    }

    public static String decode(String paramString)
    {
        if (paramString == null);
        for (String str = null; ; str = UriCodec.decode(paramString, false, Charsets.UTF_8, false))
            return str;
    }

    public static String encode(String paramString)
    {
        return encode(paramString, null);
    }

    public static String encode(String paramString1, String paramString2)
    {
        if (paramString1 == null)
            paramString1 = null;
        while (true)
        {
            return paramString1;
            StringBuilder localStringBuilder = null;
            int i = paramString1.length();
            int n;
            for (int j = 0; ; j = n)
            {
                if (j >= i)
                    break label248;
                for (int k = j; (k < i) && (isAllowed(paramString1.charAt(k), paramString2)); k++);
                if (k == i)
                {
                    if (j == 0)
                        break;
                    localStringBuilder.append(paramString1, j, i);
                    paramString1 = localStringBuilder.toString();
                    break;
                }
                if (localStringBuilder == null)
                    localStringBuilder = new StringBuilder();
                if (k > j)
                    localStringBuilder.append(paramString1, j, k);
                int m = k;
                for (n = m + 1; (n < i) && (!isAllowed(paramString1.charAt(n), paramString2)); n++);
                String str = paramString1.substring(m, n);
                try
                {
                    byte[] arrayOfByte = str.getBytes("UTF-8");
                    int i1 = arrayOfByte.length;
                    for (int i2 = 0; i2 < i1; i2++)
                    {
                        localStringBuilder.append('%');
                        localStringBuilder.append(HEX_DIGITS[((0xF0 & arrayOfByte[i2]) >> 4)]);
                        localStringBuilder.append(HEX_DIGITS[(0xF & arrayOfByte[i2])]);
                    }
                }
                catch (UnsupportedEncodingException localUnsupportedEncodingException)
                {
                    throw new AssertionError(localUnsupportedEncodingException);
                }
            }
            label248: if (localStringBuilder != null)
                paramString1 = localStringBuilder.toString();
        }
    }

    public static Uri fromFile(File paramFile)
    {
        if (paramFile == null)
            throw new NullPointerException("file");
        PathPart localPathPart = PathPart.fromDecoded(paramFile.getAbsolutePath());
        return new HierarchicalUri("file", Part.EMPTY, localPathPart, Part.NULL, Part.NULL, null);
    }

    public static Uri fromParts(String paramString1, String paramString2, String paramString3)
    {
        if (paramString1 == null)
            throw new NullPointerException("scheme");
        if (paramString2 == null)
            throw new NullPointerException("ssp");
        return new OpaqueUri(paramString1, Part.fromDecoded(paramString2), Part.fromDecoded(paramString3), null);
    }

    private static boolean isAllowed(char paramChar, String paramString)
    {
        if (((paramChar >= 'A') && (paramChar <= 'Z')) || ((paramChar >= 'a') && (paramChar <= 'z')) || ((paramChar >= '0') && (paramChar <= '9')) || ("_-!.~'()*".indexOf(paramChar) != -1) || ((paramString != null) && (paramString.indexOf(paramChar) != -1)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static Uri parse(String paramString)
    {
        return new StringUri(paramString, null);
    }

    public static Uri withAppendedPath(Uri paramUri, String paramString)
    {
        return paramUri.buildUpon().appendEncodedPath(paramString).build();
    }

    public static void writeToParcel(Parcel paramParcel, Uri paramUri)
    {
        if (paramUri == null)
            paramParcel.writeInt(0);
        while (true)
        {
            return;
            paramUri.writeToParcel(paramParcel, 0);
        }
    }

    public abstract Builder buildUpon();

    public int compareTo(Uri paramUri)
    {
        return toString().compareTo(paramUri.toString());
    }

    public boolean equals(Object paramObject)
    {
        if (!(paramObject instanceof Uri));
        Uri localUri;
        for (boolean bool = false; ; bool = toString().equals(localUri.toString()))
        {
            return bool;
            localUri = (Uri)paramObject;
        }
    }

    public abstract String getAuthority();

    public boolean getBooleanQueryParameter(String paramString, boolean paramBoolean)
    {
        String str1 = getQueryParameter(paramString);
        if (str1 == null)
            return paramBoolean;
        String str2 = str1.toLowerCase();
        if ((!"false".equals(str2)) && (!"0".equals(str2)));
        for (boolean bool = true; ; bool = false)
        {
            paramBoolean = bool;
            break;
        }
    }

    public abstract String getEncodedAuthority();

    public abstract String getEncodedFragment();

    public abstract String getEncodedPath();

    public abstract String getEncodedQuery();

    public abstract String getEncodedSchemeSpecificPart();

    public abstract String getEncodedUserInfo();

    public abstract String getFragment();

    public abstract String getHost();

    public abstract String getLastPathSegment();

    public abstract String getPath();

    public abstract List<String> getPathSegments();

    public abstract int getPort();

    public abstract String getQuery();

    public String getQueryParameter(String paramString)
    {
        String str1 = null;
        if (isOpaque())
            throw new UnsupportedOperationException("This isn't a hierarchical URI.");
        if (paramString == null)
            throw new NullPointerException("key");
        String str2 = getEncodedQuery();
        if (str2 == null)
            return str1;
        String str3 = encode(paramString, null);
        int i = str2.length();
        int k;
        for (int j = 0; ; j = k + 1)
        {
            k = str2.indexOf('&', j);
            if (k != -1);
            int n;
            for (int m = k; ; m = i)
            {
                n = str2.indexOf('=', j);
                if ((n > m) || (n == -1))
                    n = m;
                if ((n - j != str3.length()) || (!str2.regionMatches(j, str3, 0, str3.length())))
                    break label183;
                if (n != m)
                    break label161;
                str1 = "";
                break;
            }
            label161: str1 = UriCodec.decode(str2.substring(n + 1, m), true, Charsets.UTF_8, false);
            break;
            label183: if (k == -1)
                break;
        }
    }

    public Set<String> getQueryParameterNames()
    {
        if (isOpaque())
            throw new UnsupportedOperationException("This isn't a hierarchical URI.");
        String str = getEncodedQuery();
        Set localSet;
        if (str == null)
        {
            localSet = Collections.emptySet();
            return localSet;
        }
        LinkedHashSet localLinkedHashSet = new LinkedHashSet();
        int i = 0;
        label44: int j = str.indexOf('&', i);
        if (j == -1);
        for (int k = str.length(); ; k = j)
        {
            int m = str.indexOf('=', i);
            if ((m > k) || (m == -1))
                m = k;
            localLinkedHashSet.add(decode(str.substring(i, m)));
            i = k + 1;
            if (i < str.length())
                break label44;
            localSet = Collections.unmodifiableSet(localLinkedHashSet);
            break;
        }
    }

    public List<String> getQueryParameters(String paramString)
    {
        if (isOpaque())
            throw new UnsupportedOperationException("This isn't a hierarchical URI.");
        if (paramString == null)
            throw new NullPointerException("key");
        String str1 = getEncodedQuery();
        if (str1 == null);
        ArrayList localArrayList;
        for (List localList = Collections.emptyList(); ; localList = Collections.unmodifiableList(localArrayList))
        {
            return localList;
            while (true)
            {
                int m;
                try
                {
                    String str2 = URLEncoder.encode(paramString, "UTF-8");
                    localArrayList = new ArrayList();
                    int i = 0;
                    int j = str1.indexOf('&', i);
                    if (j != -1)
                    {
                        k = j;
                        m = str1.indexOf('=', i);
                        if ((m > k) || (m == -1))
                            m = k;
                        if ((m - i == str2.length()) && (str1.regionMatches(i, str2, 0, str2.length())))
                        {
                            if (m != k)
                                break label199;
                            localArrayList.add("");
                        }
                        if (j == -1)
                            break;
                        i = j + 1;
                        continue;
                    }
                }
                catch (UnsupportedEncodingException localUnsupportedEncodingException)
                {
                    throw new AssertionError(localUnsupportedEncodingException);
                }
                int k = str1.length();
                continue;
                label199: localArrayList.add(decode(str1.substring(m + 1, k)));
            }
        }
    }

    public abstract String getScheme();

    public abstract String getSchemeSpecificPart();

    public abstract String getUserInfo();

    public int hashCode()
    {
        return toString().hashCode();
    }

    public boolean isAbsolute()
    {
        if (!isRelative());
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public abstract boolean isHierarchical();

    public boolean isOpaque()
    {
        if (!isHierarchical());
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public abstract boolean isRelative();

    public Uri normalizeScheme()
    {
        String str1 = getScheme();
        if (str1 == null);
        while (true)
        {
            return this;
            String str2 = str1.toLowerCase(Locale.US);
            if (!str1.equals(str2))
                this = buildUpon().scheme(str2).build();
        }
    }

    public String toSafeString()
    {
        String str1 = getScheme();
        String str2 = getSchemeSpecificPart();
        StringBuilder localStringBuilder2;
        if ((str1 != null) && ((str1.equalsIgnoreCase("tel")) || (str1.equalsIgnoreCase("sip")) || (str1.equalsIgnoreCase("sms")) || (str1.equalsIgnoreCase("smsto")) || (str1.equalsIgnoreCase("mailto"))))
        {
            localStringBuilder2 = new StringBuilder(64);
            localStringBuilder2.append(str1);
            localStringBuilder2.append(':');
            if (str2 != null)
            {
                int i = 0;
                if (i < str2.length())
                {
                    char c = str2.charAt(i);
                    if ((c == '-') || (c == '@') || (c == '.'))
                        localStringBuilder2.append(c);
                    while (true)
                    {
                        i++;
                        break;
                        localStringBuilder2.append('x');
                    }
                }
            }
        }
        StringBuilder localStringBuilder1;
        for (String str3 = localStringBuilder2.toString(); ; str3 = localStringBuilder1.toString())
        {
            return str3;
            localStringBuilder1 = new StringBuilder(64);
            if (str1 != null)
            {
                localStringBuilder1.append(str1);
                localStringBuilder1.append(':');
            }
            if (str2 != null)
                localStringBuilder1.append(str2);
        }
    }

    public abstract String toString();

    static class PathPart extends Uri.AbstractPart
    {
        static final PathPart EMPTY = new PathPart("", "");
        static final PathPart NULL = new PathPart(null, null);
        private Uri.PathSegments pathSegments;

        private PathPart(String paramString1, String paramString2)
        {
            super(paramString2);
        }

        static PathPart appendDecodedSegment(PathPart paramPathPart, String paramString)
        {
            return appendEncodedSegment(paramPathPart, Uri.encode(paramString));
        }

        static PathPart appendEncodedSegment(PathPart paramPathPart, String paramString)
        {
            PathPart localPathPart;
            if (paramPathPart == null)
            {
                localPathPart = fromEncoded("/" + paramString);
                return localPathPart;
            }
            String str1 = paramPathPart.getEncoded();
            if (str1 == null)
                str1 = "";
            int i = str1.length();
            String str2;
            if (i == 0)
                str2 = "/" + paramString;
            while (true)
            {
                localPathPart = fromEncoded(str2);
                break;
                if (str1.charAt(i - 1) == '/')
                    str2 = str1 + paramString;
                else
                    str2 = str1 + "/" + paramString;
            }
        }

        static PathPart from(String paramString1, String paramString2)
        {
            PathPart localPathPart;
            if (paramString1 == null)
                localPathPart = NULL;
            while (true)
            {
                return localPathPart;
                if (paramString1.length() == 0)
                    localPathPart = EMPTY;
                else
                    localPathPart = new PathPart(paramString1, paramString2);
            }
        }

        static PathPart fromDecoded(String paramString)
        {
            return from(Uri.NOT_CACHED, paramString);
        }

        static PathPart fromEncoded(String paramString)
        {
            return from(paramString, Uri.NOT_CACHED);
        }

        static PathPart makeAbsolute(PathPart paramPathPart)
        {
            int i = 1;
            int j;
            if (paramPathPart.encoded != Uri.NOT_CACHED)
            {
                j = i;
                if (j == 0)
                    break label50;
            }
            label50: for (String str1 = paramPathPart.encoded; ; str1 = paramPathPart.decoded)
            {
                if ((str1 != null) && (str1.length() != 0) && (!str1.startsWith("/")))
                    break label58;
                return paramPathPart;
                j = 0;
                break;
            }
            label58: String str2;
            if (j != 0)
            {
                str2 = "/" + paramPathPart.encoded;
                label86: if (paramPathPart.decoded == Uri.NOT_CACHED)
                    break label147;
                label96: if (i == 0)
                    break label152;
            }
            label147: label152: for (String str3 = "/" + paramPathPart.decoded; ; str3 = Uri.NOT_CACHED)
            {
                paramPathPart = new PathPart(str2, str3);
                break;
                str2 = Uri.NOT_CACHED;
                break label86;
                i = 0;
                break label96;
            }
        }

        static PathPart readFrom(Parcel paramParcel)
        {
            int i = paramParcel.readInt();
            PathPart localPathPart;
            switch (i)
            {
            default:
                throw new IllegalArgumentException("Bad representation: " + i);
            case 0:
                localPathPart = from(paramParcel.readString(), paramParcel.readString());
            case 1:
            case 2:
            }
            while (true)
            {
                return localPathPart;
                localPathPart = fromEncoded(paramParcel.readString());
                continue;
                localPathPart = fromDecoded(paramParcel.readString());
            }
        }

        String getEncoded()
        {
            int i;
            String str;
            if (this.encoded != Uri.NOT_CACHED)
            {
                i = 1;
                if (i == 0)
                    break label28;
                str = this.encoded;
            }
            while (true)
            {
                return str;
                i = 0;
                break;
                label28: str = Uri.encode(this.decoded, "/");
                this.encoded = str;
            }
        }

        Uri.PathSegments getPathSegments()
        {
            Uri.PathSegments localPathSegments;
            if (this.pathSegments != null)
                localPathSegments = this.pathSegments;
            while (true)
            {
                return localPathSegments;
                String str = getEncoded();
                if (str == null)
                {
                    localPathSegments = Uri.PathSegments.EMPTY;
                    this.pathSegments = localPathSegments;
                }
                else
                {
                    Uri.PathSegmentsBuilder localPathSegmentsBuilder = new Uri.PathSegmentsBuilder();
                    int j;
                    for (int i = 0; ; i = j + 1)
                    {
                        j = str.indexOf('/', i);
                        if (j <= -1)
                            break;
                        if (i < j)
                            localPathSegmentsBuilder.add(Uri.decode(str.substring(i, j)));
                    }
                    if (i < str.length())
                        localPathSegmentsBuilder.add(Uri.decode(str.substring(i)));
                    localPathSegments = localPathSegmentsBuilder.build();
                    this.pathSegments = localPathSegments;
                }
            }
        }
    }

    static class Part extends Uri.AbstractPart
    {
        static final Part EMPTY = new EmptyPart("");
        static final Part NULL = new EmptyPart(null);

        private Part(String paramString1, String paramString2)
        {
            super(paramString2);
        }

        static Part from(String paramString1, String paramString2)
        {
            Part localPart;
            if (paramString1 == null)
                localPart = NULL;
            while (true)
            {
                return localPart;
                if (paramString1.length() == 0)
                    localPart = EMPTY;
                else if (paramString2 == null)
                    localPart = NULL;
                else if (paramString2.length() == 0)
                    localPart = EMPTY;
                else
                    localPart = new Part(paramString1, paramString2);
            }
        }

        static Part fromDecoded(String paramString)
        {
            return from(Uri.NOT_CACHED, paramString);
        }

        static Part fromEncoded(String paramString)
        {
            return from(paramString, Uri.NOT_CACHED);
        }

        static Part nonNull(Part paramPart)
        {
            if (paramPart == null)
                paramPart = NULL;
            return paramPart;
        }

        static Part readFrom(Parcel paramParcel)
        {
            int i = paramParcel.readInt();
            Part localPart;
            switch (i)
            {
            default:
                throw new IllegalArgumentException("Unknown representation: " + i);
            case 0:
                localPart = from(paramParcel.readString(), paramParcel.readString());
            case 1:
            case 2:
            }
            while (true)
            {
                return localPart;
                localPart = fromEncoded(paramParcel.readString());
                continue;
                localPart = fromDecoded(paramParcel.readString());
            }
        }

        String getEncoded()
        {
            int i;
            String str;
            if (this.encoded != Uri.NOT_CACHED)
            {
                i = 1;
                if (i == 0)
                    break label28;
                str = this.encoded;
            }
            while (true)
            {
                return str;
                i = 0;
                break;
                label28: str = Uri.encode(this.decoded);
                this.encoded = str;
            }
        }

        boolean isEmpty()
        {
            return false;
        }

        private static class EmptyPart extends Uri.Part
        {
            public EmptyPart(String paramString)
            {
                super(paramString, null);
            }

            boolean isEmpty()
            {
                return true;
            }
        }
    }

    static abstract class AbstractPart
    {
        volatile String decoded;
        volatile String encoded;

        AbstractPart(String paramString1, String paramString2)
        {
            this.encoded = paramString1;
            this.decoded = paramString2;
        }

        final String getDecoded()
        {
            int i;
            String str;
            if (this.decoded != Uri.NOT_CACHED)
            {
                i = 1;
                if (i == 0)
                    break label28;
                str = this.decoded;
            }
            while (true)
            {
                return str;
                i = 0;
                break;
                label28: str = Uri.decode(this.encoded);
                this.decoded = str;
            }
        }

        abstract String getEncoded();

        final void writeTo(Parcel paramParcel)
        {
            int i;
            int j;
            if (this.encoded != Uri.NOT_CACHED)
            {
                i = 1;
                if (this.decoded == Uri.NOT_CACHED)
                    break label59;
                j = 1;
                label24: if ((i == 0) || (j == 0))
                    break label64;
                paramParcel.writeInt(0);
                paramParcel.writeString(this.encoded);
                paramParcel.writeString(this.decoded);
            }
            while (true)
            {
                return;
                i = 0;
                break;
                label59: j = 0;
                break label24;
                label64: if (i != 0)
                {
                    paramParcel.writeInt(1);
                    paramParcel.writeString(this.encoded);
                }
                else
                {
                    if (j == 0)
                        break label104;
                    paramParcel.writeInt(2);
                    paramParcel.writeString(this.decoded);
                }
            }
            label104: throw new IllegalArgumentException("Neither encoded nor decoded");
        }

        static class Representation
        {
            static final int BOTH = 0;
            static final int DECODED = 2;
            static final int ENCODED = 1;
        }
    }

    public static final class Builder
    {
        private Uri.Part authority;
        private Uri.Part fragment;
        private Uri.Part opaquePart;
        private Uri.PathPart path;
        private Uri.Part query;
        private String scheme;

        private boolean hasSchemeOrAuthority()
        {
            if ((this.scheme != null) || ((this.authority != null) && (this.authority != Uri.Part.NULL)));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public Builder appendEncodedPath(String paramString)
        {
            return path(Uri.PathPart.appendEncodedSegment(this.path, paramString));
        }

        public Builder appendPath(String paramString)
        {
            return path(Uri.PathPart.appendDecodedSegment(this.path, paramString));
        }

        public Builder appendQueryParameter(String paramString1, String paramString2)
        {
            this.opaquePart = null;
            String str1 = Uri.encode(paramString1, null) + "=" + Uri.encode(paramString2, null);
            if (this.query == null)
                this.query = Uri.Part.fromEncoded(str1);
            while (true)
            {
                return this;
                String str2 = this.query.getEncoded();
                if ((str2 == null) || (str2.length() == 0))
                    this.query = Uri.Part.fromEncoded(str1);
                else
                    this.query = Uri.Part.fromEncoded(str2 + "&" + str1);
            }
        }

        Builder authority(Uri.Part paramPart)
        {
            this.opaquePart = null;
            this.authority = paramPart;
            return this;
        }

        public Builder authority(String paramString)
        {
            return authority(Uri.Part.fromDecoded(paramString));
        }

        public Uri build()
        {
            Object localObject;
            if (this.opaquePart != null)
            {
                if (this.scheme == null)
                    throw new UnsupportedOperationException("An opaque URI must have a scheme.");
                localObject = new Uri.OpaqueUri(this.scheme, this.opaquePart, this.fragment, null);
                return localObject;
            }
            Uri.PathPart localPathPart = this.path;
            if ((localPathPart == null) || (localPathPart == Uri.PathPart.NULL))
                localPathPart = Uri.PathPart.EMPTY;
            while (true)
            {
                localObject = new Uri.HierarchicalUri(this.scheme, this.authority, localPathPart, this.query, this.fragment, null);
                break;
                if (hasSchemeOrAuthority())
                    localPathPart = Uri.PathPart.makeAbsolute(localPathPart);
            }
        }

        public Builder clearQuery()
        {
            return query((Uri.Part)null);
        }

        public Builder encodedAuthority(String paramString)
        {
            return authority(Uri.Part.fromEncoded(paramString));
        }

        public Builder encodedFragment(String paramString)
        {
            return fragment(Uri.Part.fromEncoded(paramString));
        }

        public Builder encodedOpaquePart(String paramString)
        {
            return opaquePart(Uri.Part.fromEncoded(paramString));
        }

        public Builder encodedPath(String paramString)
        {
            return path(Uri.PathPart.fromEncoded(paramString));
        }

        public Builder encodedQuery(String paramString)
        {
            return query(Uri.Part.fromEncoded(paramString));
        }

        Builder fragment(Uri.Part paramPart)
        {
            this.fragment = paramPart;
            return this;
        }

        public Builder fragment(String paramString)
        {
            return fragment(Uri.Part.fromDecoded(paramString));
        }

        Builder opaquePart(Uri.Part paramPart)
        {
            this.opaquePart = paramPart;
            return this;
        }

        public Builder opaquePart(String paramString)
        {
            return opaquePart(Uri.Part.fromDecoded(paramString));
        }

        Builder path(Uri.PathPart paramPathPart)
        {
            this.opaquePart = null;
            this.path = paramPathPart;
            return this;
        }

        public Builder path(String paramString)
        {
            return path(Uri.PathPart.fromDecoded(paramString));
        }

        Builder query(Uri.Part paramPart)
        {
            this.opaquePart = null;
            this.query = paramPart;
            return this;
        }

        public Builder query(String paramString)
        {
            return query(Uri.Part.fromDecoded(paramString));
        }

        public Builder scheme(String paramString)
        {
            this.scheme = paramString;
            return this;
        }

        public String toString()
        {
            return build().toString();
        }
    }

    private static class HierarchicalUri extends Uri.AbstractHierarchicalUri
    {
        static final int TYPE_ID = 3;
        private final Uri.Part authority;
        private final Uri.Part fragment;
        private final Uri.PathPart path;
        private final Uri.Part query;
        private final String scheme;
        private Uri.Part ssp;
        private volatile String uriString = Uri.NOT_CACHED;

        private HierarchicalUri(String paramString, Uri.Part paramPart1, Uri.PathPart paramPathPart, Uri.Part paramPart2, Uri.Part paramPart3)
        {
            super();
            this.scheme = paramString;
            this.authority = Uri.Part.nonNull(paramPart1);
            if (paramPathPart == null)
                paramPathPart = Uri.PathPart.NULL;
            this.path = paramPathPart;
            this.query = Uri.Part.nonNull(paramPart2);
            this.fragment = Uri.Part.nonNull(paramPart3);
        }

        private void appendSspTo(StringBuilder paramStringBuilder)
        {
            String str1 = this.authority.getEncoded();
            if (str1 != null)
                paramStringBuilder.append("//").append(str1);
            String str2 = this.path.getEncoded();
            if (str2 != null)
                paramStringBuilder.append(str2);
            if (!this.query.isEmpty())
                paramStringBuilder.append('?').append(this.query.getEncoded());
        }

        private Uri.Part getSsp()
        {
            Uri.Part localPart;
            if (this.ssp == null)
            {
                localPart = Uri.Part.fromEncoded(makeSchemeSpecificPart());
                this.ssp = localPart;
            }
            while (true)
            {
                return localPart;
                localPart = this.ssp;
            }
        }

        private String makeSchemeSpecificPart()
        {
            StringBuilder localStringBuilder = new StringBuilder();
            appendSspTo(localStringBuilder);
            return localStringBuilder.toString();
        }

        private String makeUriString()
        {
            StringBuilder localStringBuilder = new StringBuilder();
            if (this.scheme != null)
                localStringBuilder.append(this.scheme).append(':');
            appendSspTo(localStringBuilder);
            if (!this.fragment.isEmpty())
                localStringBuilder.append('#').append(this.fragment.getEncoded());
            return localStringBuilder.toString();
        }

        static Uri readFrom(Parcel paramParcel)
        {
            return new HierarchicalUri(paramParcel.readString(), Uri.Part.readFrom(paramParcel), Uri.PathPart.readFrom(paramParcel), Uri.Part.readFrom(paramParcel), Uri.Part.readFrom(paramParcel));
        }

        public Uri.Builder buildUpon()
        {
            return new Uri.Builder().scheme(this.scheme).authority(this.authority).path(this.path).query(this.query).fragment(this.fragment);
        }

        public int describeContents()
        {
            return 0;
        }

        public String getAuthority()
        {
            return this.authority.getDecoded();
        }

        public String getEncodedAuthority()
        {
            return this.authority.getEncoded();
        }

        public String getEncodedFragment()
        {
            return this.fragment.getEncoded();
        }

        public String getEncodedPath()
        {
            return this.path.getEncoded();
        }

        public String getEncodedQuery()
        {
            return this.query.getEncoded();
        }

        public String getEncodedSchemeSpecificPart()
        {
            return getSsp().getEncoded();
        }

        public String getFragment()
        {
            return this.fragment.getDecoded();
        }

        public String getPath()
        {
            return this.path.getDecoded();
        }

        public List<String> getPathSegments()
        {
            return this.path.getPathSegments();
        }

        public String getQuery()
        {
            return this.query.getDecoded();
        }

        public String getScheme()
        {
            return this.scheme;
        }

        public String getSchemeSpecificPart()
        {
            return getSsp().getDecoded();
        }

        public boolean isHierarchical()
        {
            return true;
        }

        public boolean isRelative()
        {
            if (this.scheme == null);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public String toString()
        {
            int i;
            String str;
            if (this.uriString != Uri.NOT_CACHED)
            {
                i = 1;
                if (i == 0)
                    break label28;
                str = this.uriString;
            }
            while (true)
            {
                return str;
                i = 0;
                break;
                label28: str = makeUriString();
                this.uriString = str;
            }
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeInt(3);
            paramParcel.writeString(this.scheme);
            this.authority.writeTo(paramParcel);
            this.path.writeTo(paramParcel);
            this.query.writeTo(paramParcel);
            this.fragment.writeTo(paramParcel);
        }
    }

    private static abstract class AbstractHierarchicalUri extends Uri
    {
        private volatile String host = Uri.NOT_CACHED;
        private volatile int port = -2;
        private Uri.Part userInfo;

        private AbstractHierarchicalUri()
        {
            super();
        }

        private Uri.Part getUserInfoPart()
        {
            Uri.Part localPart;
            if (this.userInfo == null)
            {
                localPart = Uri.Part.fromEncoded(parseUserInfo());
                this.userInfo = localPart;
            }
            while (true)
            {
                return localPart;
                localPart = this.userInfo;
            }
        }

        private String parseHost()
        {
            String str1 = getEncodedAuthority();
            String str3;
            if (str1 == null)
            {
                str3 = null;
                return str3;
            }
            int i = str1.indexOf('@');
            int j = str1.indexOf(':', i);
            if (j == -1);
            for (String str2 = str1.substring(i + 1); ; str2 = str1.substring(i + 1, j))
            {
                str3 = decode(str2);
                break;
            }
        }

        private int parsePort()
        {
            int i = -1;
            String str1 = getEncodedAuthority();
            if (str1 == null);
            while (true)
            {
                return i;
                int j = str1.indexOf(':', str1.indexOf(64));
                if (j != i)
                {
                    String str2 = decode(str1.substring(j + 1));
                    try
                    {
                        int k = Integer.parseInt(str2);
                        i = k;
                    }
                    catch (NumberFormatException localNumberFormatException)
                    {
                        Log.w(Uri.LOG, "Error parsing port string.", localNumberFormatException);
                    }
                }
            }
        }

        private String parseUserInfo()
        {
            String str1 = null;
            String str2 = getEncodedAuthority();
            if (str2 == null);
            while (true)
            {
                return str1;
                int i = str2.indexOf('@');
                if (i != -1)
                    str1 = str2.substring(0, i);
            }
        }

        public final String getEncodedUserInfo()
        {
            return getUserInfoPart().getEncoded();
        }

        public String getHost()
        {
            int i;
            String str;
            if (this.host != Uri.NOT_CACHED)
            {
                i = 1;
                if (i == 0)
                    break label28;
                str = this.host;
            }
            while (true)
            {
                return str;
                i = 0;
                break;
                label28: str = parseHost();
                this.host = str;
            }
        }

        public String getLastPathSegment()
        {
            List localList = getPathSegments();
            int i = localList.size();
            if (i == 0);
            for (String str = null; ; str = (String)localList.get(i - 1))
                return str;
        }

        public int getPort()
        {
            int i;
            if (this.port == -2)
            {
                i = parsePort();
                this.port = i;
            }
            while (true)
            {
                return i;
                i = this.port;
            }
        }

        public String getUserInfo()
        {
            return getUserInfoPart().getDecoded();
        }
    }

    static class PathSegmentsBuilder
    {
        String[] segments;
        int size = 0;

        void add(String paramString)
        {
            if (this.segments == null)
                this.segments = new String[4];
            while (true)
            {
                String[] arrayOfString2 = this.segments;
                int i = this.size;
                this.size = (i + 1);
                arrayOfString2[i] = paramString;
                return;
                if (1 + this.size == this.segments.length)
                {
                    String[] arrayOfString1 = new String[2 * this.segments.length];
                    System.arraycopy(this.segments, 0, arrayOfString1, 0, this.segments.length);
                    this.segments = arrayOfString1;
                }
            }
        }

        Uri.PathSegments build()
        {
            Uri.PathSegments localPathSegments;
            if (this.segments == null)
                localPathSegments = Uri.PathSegments.EMPTY;
            while (true)
            {
                return localPathSegments;
                try
                {
                    localPathSegments = new Uri.PathSegments(this.segments, this.size);
                    this.segments = null;
                }
                finally
                {
                    this.segments = null;
                }
            }
        }
    }

    static class PathSegments extends AbstractList<String>
        implements RandomAccess
    {
        static final PathSegments EMPTY = new PathSegments(null, 0);
        final String[] segments;
        final int size;

        PathSegments(String[] paramArrayOfString, int paramInt)
        {
            this.segments = paramArrayOfString;
            this.size = paramInt;
        }

        public String get(int paramInt)
        {
            if (paramInt >= this.size)
                throw new IndexOutOfBoundsException();
            return this.segments[paramInt];
        }

        public int size()
        {
            return this.size;
        }
    }

    private static class OpaqueUri extends Uri
    {
        static final int TYPE_ID = 2;
        private volatile String cachedString = Uri.NOT_CACHED;
        private final Uri.Part fragment;
        private final String scheme;
        private final Uri.Part ssp;

        private OpaqueUri(String paramString, Uri.Part paramPart1, Uri.Part paramPart2)
        {
            super();
            this.scheme = paramString;
            this.ssp = paramPart1;
            if (paramPart2 == null)
                paramPart2 = Uri.Part.NULL;
            this.fragment = paramPart2;
        }

        static Uri readFrom(Parcel paramParcel)
        {
            return new OpaqueUri(paramParcel.readString(), Uri.Part.readFrom(paramParcel), Uri.Part.readFrom(paramParcel));
        }

        public Uri.Builder buildUpon()
        {
            return new Uri.Builder().scheme(this.scheme).opaquePart(this.ssp).fragment(this.fragment);
        }

        public int describeContents()
        {
            return 0;
        }

        public String getAuthority()
        {
            return null;
        }

        public String getEncodedAuthority()
        {
            return null;
        }

        public String getEncodedFragment()
        {
            return this.fragment.getEncoded();
        }

        public String getEncodedPath()
        {
            return null;
        }

        public String getEncodedQuery()
        {
            return null;
        }

        public String getEncodedSchemeSpecificPart()
        {
            return this.ssp.getEncoded();
        }

        public String getEncodedUserInfo()
        {
            return null;
        }

        public String getFragment()
        {
            return this.fragment.getDecoded();
        }

        public String getHost()
        {
            return null;
        }

        public String getLastPathSegment()
        {
            return null;
        }

        public String getPath()
        {
            return null;
        }

        public List<String> getPathSegments()
        {
            return Collections.emptyList();
        }

        public int getPort()
        {
            return -1;
        }

        public String getQuery()
        {
            return null;
        }

        public String getScheme()
        {
            return this.scheme;
        }

        public String getSchemeSpecificPart()
        {
            return this.ssp.getDecoded();
        }

        public String getUserInfo()
        {
            return null;
        }

        public boolean isHierarchical()
        {
            return false;
        }

        public boolean isRelative()
        {
            if (this.scheme == null);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public String toString()
        {
            int i;
            String str;
            if (this.cachedString != Uri.NOT_CACHED)
            {
                i = 1;
                if (i == 0)
                    break label30;
                str = this.cachedString;
            }
            while (true)
            {
                return str;
                i = 0;
                break;
                label30: StringBuilder localStringBuilder = new StringBuilder();
                localStringBuilder.append(this.scheme).append(':');
                localStringBuilder.append(getEncodedSchemeSpecificPart());
                if (!this.fragment.isEmpty())
                    localStringBuilder.append('#').append(this.fragment.getEncoded());
                str = localStringBuilder.toString();
                this.cachedString = str;
            }
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeInt(2);
            paramParcel.writeString(this.scheme);
            this.ssp.writeTo(paramParcel);
            this.fragment.writeTo(paramParcel);
        }
    }

    private static class StringUri extends Uri.AbstractHierarchicalUri
    {
        static final int TYPE_ID = 1;
        private Uri.Part authority;
        private volatile int cachedFsi = -2;
        private volatile int cachedSsi = -2;
        private Uri.Part fragment;
        private Uri.PathPart path;
        private Uri.Part query;
        private volatile String scheme = Uri.NOT_CACHED;
        private Uri.Part ssp;
        private final String uriString;

        private StringUri(String paramString)
        {
            super();
            if (paramString == null)
                throw new NullPointerException("uriString");
            this.uriString = paramString;
        }

        private int findFragmentSeparator()
        {
            int i;
            if (this.cachedFsi == -2)
            {
                i = this.uriString.indexOf('#', findSchemeSeparator());
                this.cachedFsi = i;
            }
            while (true)
            {
                return i;
                i = this.cachedFsi;
            }
        }

        private int findSchemeSeparator()
        {
            int i;
            if (this.cachedSsi == -2)
            {
                i = this.uriString.indexOf(':');
                this.cachedSsi = i;
            }
            while (true)
            {
                return i;
                i = this.cachedSsi;
            }
        }

        private Uri.Part getAuthorityPart()
        {
            Uri.Part localPart;
            if (this.authority == null)
            {
                localPart = Uri.Part.fromEncoded(parseAuthority(this.uriString, findSchemeSeparator()));
                this.authority = localPart;
            }
            while (true)
            {
                return localPart;
                localPart = this.authority;
            }
        }

        private Uri.Part getFragmentPart()
        {
            Uri.Part localPart;
            if (this.fragment == null)
            {
                localPart = Uri.Part.fromEncoded(parseFragment());
                this.fragment = localPart;
            }
            while (true)
            {
                return localPart;
                localPart = this.fragment;
            }
        }

        private Uri.PathPart getPathPart()
        {
            Uri.PathPart localPathPart;
            if (this.path == null)
            {
                localPathPart = Uri.PathPart.fromEncoded(parsePath());
                this.path = localPathPart;
            }
            while (true)
            {
                return localPathPart;
                localPathPart = this.path;
            }
        }

        private Uri.Part getQueryPart()
        {
            Uri.Part localPart;
            if (this.query == null)
            {
                localPart = Uri.Part.fromEncoded(parseQuery());
                this.query = localPart;
            }
            while (true)
            {
                return localPart;
                localPart = this.query;
            }
        }

        private Uri.Part getSsp()
        {
            Uri.Part localPart;
            if (this.ssp == null)
            {
                localPart = Uri.Part.fromEncoded(parseSsp());
                this.ssp = localPart;
            }
            while (true)
            {
                return localPart;
                localPart = this.ssp;
            }
        }

        static String parseAuthority(String paramString, int paramInt)
        {
            int i = paramString.length();
            int j;
            if ((i > paramInt + 2) && (paramString.charAt(paramInt + 1) == '/') && (paramString.charAt(paramInt + 2) == '/'))
                for (j = paramInt + 3; j < i; j++)
                    switch (paramString.charAt(j))
                    {
                    default:
                    case '#':
                    case '/':
                    case '?':
                    }
            for (String str = paramString.substring(paramInt + 3, j); ; str = null)
                return str;
        }

        private String parseFragment()
        {
            int i = findFragmentSeparator();
            if (i == -1);
            for (String str = null; ; str = this.uriString.substring(i + 1))
                return str;
        }

        private String parsePath()
        {
            String str1 = null;
            String str2 = this.uriString;
            int i = findSchemeSeparator();
            int j;
            if (i > -1)
                if (i + 1 == str2.length())
                {
                    j = 1;
                    if (j == 0)
                        break label44;
                }
            while (true)
            {
                return str1;
                j = 0;
                break;
                label44: if (str2.charAt(i + 1) == '/')
                    str1 = parsePath(str2, i);
            }
        }

        static String parsePath(String paramString, int paramInt)
        {
            int i = paramString.length();
            int j;
            if ((i > paramInt + 2) && (paramString.charAt(paramInt + 1) == '/') && (paramString.charAt(paramInt + 2) == '/'))
                for (j = paramInt + 3; j < i; j++)
                    switch (paramString.charAt(j))
                    {
                    default:
                    case '#':
                    case '?':
                    case '/':
                    }
            int k;
            for (String str = ""; ; str = paramString.substring(j, k))
            {
                return str;
                j = paramInt + 1;
                for (k = j; k < i; k++)
                    switch (paramString.charAt(k))
                    {
                    default:
                    case '#':
                    case '?':
                    }
            }
        }

        private String parseQuery()
        {
            String str = null;
            int i = this.uriString.indexOf('?', findSchemeSeparator());
            if (i == -1);
            while (true)
            {
                return str;
                int j = findFragmentSeparator();
                if (j == -1)
                    str = this.uriString.substring(i + 1);
                else if (j >= i)
                    str = this.uriString.substring(i + 1, j);
            }
        }

        private String parseScheme()
        {
            int i = findSchemeSeparator();
            if (i == -1);
            for (String str = null; ; str = this.uriString.substring(0, i))
                return str;
        }

        private String parseSsp()
        {
            int i = findSchemeSeparator();
            int j = findFragmentSeparator();
            if (j == -1);
            for (String str = this.uriString.substring(i + 1); ; str = this.uriString.substring(i + 1, j))
                return str;
        }

        static Uri readFrom(Parcel paramParcel)
        {
            return new StringUri(paramParcel.readString());
        }

        public Uri.Builder buildUpon()
        {
            if (isHierarchical());
            for (Uri.Builder localBuilder = new Uri.Builder().scheme(getScheme()).authority(getAuthorityPart()).path(getPathPart()).query(getQueryPart()).fragment(getFragmentPart()); ; localBuilder = new Uri.Builder().scheme(getScheme()).opaquePart(getSsp()).fragment(getFragmentPart()))
                return localBuilder;
        }

        public int describeContents()
        {
            return 0;
        }

        public String getAuthority()
        {
            return getAuthorityPart().getDecoded();
        }

        public String getEncodedAuthority()
        {
            return getAuthorityPart().getEncoded();
        }

        public String getEncodedFragment()
        {
            return getFragmentPart().getEncoded();
        }

        public String getEncodedPath()
        {
            return getPathPart().getEncoded();
        }

        public String getEncodedQuery()
        {
            return getQueryPart().getEncoded();
        }

        public String getEncodedSchemeSpecificPart()
        {
            return getSsp().getEncoded();
        }

        public String getFragment()
        {
            return getFragmentPart().getDecoded();
        }

        public String getPath()
        {
            return getPathPart().getDecoded();
        }

        public List<String> getPathSegments()
        {
            return getPathPart().getPathSegments();
        }

        public String getQuery()
        {
            return getQueryPart().getDecoded();
        }

        public String getScheme()
        {
            int i;
            String str;
            if (this.scheme != Uri.NOT_CACHED)
            {
                i = 1;
                if (i == 0)
                    break label28;
                str = this.scheme;
            }
            while (true)
            {
                return str;
                i = 0;
                break;
                label28: str = parseScheme();
                this.scheme = str;
            }
        }

        public String getSchemeSpecificPart()
        {
            return getSsp().getDecoded();
        }

        public boolean isHierarchical()
        {
            boolean bool = true;
            int i = findSchemeSeparator();
            if (i == -1);
            while (true)
            {
                return bool;
                if (this.uriString.length() == i + 1)
                    bool = false;
                else if (this.uriString.charAt(i + 1) != '/')
                    bool = false;
            }
        }

        public boolean isRelative()
        {
            if (findSchemeSeparator() == -1);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public String toString()
        {
            return this.uriString;
        }

        public void writeToParcel(Parcel paramParcel, int paramInt)
        {
            paramParcel.writeInt(1);
            paramParcel.writeString(this.uriString);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.Uri
 * JD-Core Version:        0.6.2
 */