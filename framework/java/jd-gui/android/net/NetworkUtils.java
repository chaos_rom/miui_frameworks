package android.net;

import android.util.Log;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Iterator;

public class NetworkUtils
{
    public static final int RESET_ALL_ADDRESSES = 3;
    public static final int RESET_IPV4_ADDRESSES = 1;
    public static final int RESET_IPV6_ADDRESSES = 2;
    private static final String TAG = "NetworkUtils";

    public static boolean addressTypeMatches(InetAddress paramInetAddress1, InetAddress paramInetAddress2)
    {
        if ((((paramInetAddress1 instanceof Inet4Address)) && ((paramInetAddress2 instanceof Inet4Address))) || (((paramInetAddress1 instanceof Inet6Address)) && ((paramInetAddress2 instanceof Inet6Address))));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static native int disableInterface(String paramString);

    public static native int enableInterface(String paramString);

    public static native String getDhcpError();

    public static InetAddress getNetworkPart(InetAddress paramInetAddress, int paramInt)
    {
        if (paramInetAddress == null)
            throw new RuntimeException("getNetworkPart doesn't accept null address");
        byte[] arrayOfByte = paramInetAddress.getAddress();
        if ((paramInt < 0) || (paramInt > 8 * arrayOfByte.length))
            throw new RuntimeException("getNetworkPart - bad prefixLength");
        int i = paramInt / 8;
        int j = (byte)(255 << 8 - paramInt % 8);
        if (i < arrayOfByte.length)
            arrayOfByte[i] = (j & arrayOfByte[i]);
        for (int k = i + 1; k < arrayOfByte.length; k++)
            arrayOfByte[k] = 0;
        try
        {
            InetAddress localInetAddress = InetAddress.getByAddress(arrayOfByte);
            return localInetAddress;
        }
        catch (UnknownHostException localUnknownHostException)
        {
            throw new RuntimeException("getNetworkPart error - " + localUnknownHostException.toString());
        }
    }

    public static InetAddress hexToInet6Address(String paramString)
        throws IllegalArgumentException
    {
        try
        {
            Object[] arrayOfObject = new Object[8];
            arrayOfObject[0] = paramString.substring(0, 4);
            arrayOfObject[1] = paramString.substring(4, 8);
            arrayOfObject[2] = paramString.substring(8, 12);
            arrayOfObject[3] = paramString.substring(12, 16);
            arrayOfObject[4] = paramString.substring(16, 20);
            arrayOfObject[5] = paramString.substring(20, 24);
            arrayOfObject[6] = paramString.substring(24, 28);
            arrayOfObject[7] = paramString.substring(28, 32);
            InetAddress localInetAddress = numericToInetAddress(String.format("%s:%s:%s:%s:%s:%s:%s:%s", arrayOfObject));
            return localInetAddress;
        }
        catch (Exception localException)
        {
            Log.e("NetworkUtils", "error in hexToInet6Address(" + paramString + "): " + localException);
            throw new IllegalArgumentException(localException);
        }
    }

    public static int inetAddressToInt(InetAddress paramInetAddress)
        throws IllegalArgumentException
    {
        byte[] arrayOfByte = paramInetAddress.getAddress();
        if (arrayOfByte.length != 4)
            throw new IllegalArgumentException("Not an IPv4 address");
        return (0xFF & arrayOfByte[3]) << 24 | (0xFF & arrayOfByte[2]) << 16 | (0xFF & arrayOfByte[1]) << 8 | 0xFF & arrayOfByte[0];
    }

    public static InetAddress intToInetAddress(int paramInt)
    {
        byte[] arrayOfByte = new byte[4];
        arrayOfByte[0] = ((byte)(paramInt & 0xFF));
        arrayOfByte[1] = ((byte)(0xFF & paramInt >> 8));
        arrayOfByte[2] = ((byte)(0xFF & paramInt >> 16));
        arrayOfByte[3] = ((byte)(0xFF & paramInt >> 24));
        try
        {
            InetAddress localInetAddress = InetAddress.getByAddress(arrayOfByte);
            return localInetAddress;
        }
        catch (UnknownHostException localUnknownHostException)
        {
        }
        throw new AssertionError();
    }

    public static String[] makeStrings(Collection<InetAddress> paramCollection)
    {
        String[] arrayOfString = new String[paramCollection.size()];
        int i = 0;
        Iterator localIterator = paramCollection.iterator();
        while (localIterator.hasNext())
        {
            InetAddress localInetAddress = (InetAddress)localIterator.next();
            int j = i + 1;
            arrayOfString[i] = localInetAddress.getHostAddress();
            i = j;
        }
        return arrayOfString;
    }

    public static int netmaskIntToPrefixLength(int paramInt)
    {
        return Integer.bitCount(paramInt);
    }

    public static InetAddress numericToInetAddress(String paramString)
        throws IllegalArgumentException
    {
        return InetAddress.parseNumericAddress(paramString);
    }

    public static int prefixLengthToNetmaskInt(int paramInt)
        throws IllegalArgumentException
    {
        if ((paramInt < 0) || (paramInt > 32))
            throw new IllegalArgumentException("Invalid prefix length (0 <= prefix <= 32)");
        return Integer.reverseBytes(-1 << 32 - paramInt);
    }

    public static native boolean releaseDhcpLease(String paramString);

    public static native int resetConnections(String paramString, int paramInt);

    public static native boolean runDhcp(String paramString, DhcpInfoInternal paramDhcpInfoInternal);

    public static native boolean runDhcpRenew(String paramString, DhcpInfoInternal paramDhcpInfoInternal);

    public static native boolean stopDhcp(String paramString);

    public static String trimV4AddrZeros(String paramString)
    {
        if (paramString == null)
            paramString = null;
        while (true)
        {
            return paramString;
            String[] arrayOfString = paramString.split("\\.");
            if (arrayOfString.length == 4)
            {
                StringBuilder localStringBuilder = new StringBuilder(16);
                int i = 0;
                if (i < 4)
                    try
                    {
                        if (arrayOfString[i].length() > 3)
                            continue;
                        localStringBuilder.append(Integer.parseInt(arrayOfString[i]));
                        if (i < 3)
                            localStringBuilder.append('.');
                        i++;
                    }
                    catch (NumberFormatException localNumberFormatException)
                    {
                    }
                else
                    paramString = localStringBuilder.toString();
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.NetworkUtils
 * JD-Core Version:        0.6.2
 */