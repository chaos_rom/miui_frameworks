package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Log;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public class LinkCapabilities
    implements Parcelable
{
    public static final Parcelable.Creator<LinkCapabilities> CREATOR = new Parcelable.Creator()
    {
        public LinkCapabilities createFromParcel(Parcel paramAnonymousParcel)
        {
            LinkCapabilities localLinkCapabilities = new LinkCapabilities();
            int j;
            for (int i = paramAnonymousParcel.readInt(); ; i = j)
            {
                j = i - 1;
                if (i == 0)
                    break;
                int k = paramAnonymousParcel.readInt();
                String str = paramAnonymousParcel.readString();
                localLinkCapabilities.mCapabilities.put(Integer.valueOf(k), str);
            }
            return localLinkCapabilities;
        }

        public LinkCapabilities[] newArray(int paramAnonymousInt)
        {
            return new LinkCapabilities[paramAnonymousInt];
        }
    };
    private static final boolean DBG = false;
    private static final String TAG = "LinkCapabilities";
    private HashMap<Integer, String> mCapabilities;

    public LinkCapabilities()
    {
        this.mCapabilities = new HashMap();
    }

    public LinkCapabilities(LinkCapabilities paramLinkCapabilities)
    {
        if (paramLinkCapabilities != null);
        for (this.mCapabilities = new HashMap(paramLinkCapabilities.mCapabilities); ; this.mCapabilities = new HashMap())
            return;
    }

    public static LinkCapabilities createNeedsMap(String paramString)
    {
        return new LinkCapabilities();
    }

    protected static void log(String paramString)
    {
        Log.d("LinkCapabilities", paramString);
    }

    public void clear()
    {
        this.mCapabilities.clear();
    }

    public boolean containsKey(int paramInt)
    {
        return this.mCapabilities.containsKey(Integer.valueOf(paramInt));
    }

    public boolean containsValue(String paramString)
    {
        return this.mCapabilities.containsValue(paramString);
    }

    public int describeContents()
    {
        return 0;
    }

    public Set<Map.Entry<Integer, String>> entrySet()
    {
        return this.mCapabilities.entrySet();
    }

    public String get(int paramInt)
    {
        return (String)this.mCapabilities.get(Integer.valueOf(paramInt));
    }

    public boolean isEmpty()
    {
        return this.mCapabilities.isEmpty();
    }

    public Set<Integer> keySet()
    {
        return this.mCapabilities.keySet();
    }

    public void put(int paramInt, String paramString)
    {
        this.mCapabilities.put(Integer.valueOf(paramInt), paramString);
    }

    public int size()
    {
        return this.mCapabilities.size();
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("{");
        Iterator localIterator = this.mCapabilities.entrySet().iterator();
        if (localIterator.hasNext())
        {
            Map.Entry localEntry = (Map.Entry)localIterator.next();
            if (1 != 0)
            {
                localStringBuilder.append(localEntry.getKey());
                localStringBuilder.append(":\"");
                localStringBuilder.append((String)localEntry.getValue());
                localStringBuilder.append("\"");
            }
        }
        for (String str = this.mCapabilities.toString(); ; str = localStringBuilder.toString())
        {
            return str;
            localStringBuilder.append(",");
            break;
        }
    }

    public Collection<String> values()
    {
        return this.mCapabilities.values();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mCapabilities.size());
        Iterator localIterator = this.mCapabilities.entrySet().iterator();
        while (localIterator.hasNext())
        {
            Map.Entry localEntry = (Map.Entry)localIterator.next();
            paramParcel.writeInt(((Integer)localEntry.getKey()).intValue());
            paramParcel.writeString((String)localEntry.getValue());
        }
    }

    public static final class Role
    {
        public static final String BULK_DOWNLOAD = "bulk.download";
        public static final String BULK_UPLOAD = "bulk.upload";
        public static final String DEFAULT = "default";
        public static final String VIDEO_CHAT_360P = "video.chat.360p";
        public static final String VIDEO_CHAT_480P = "video.chat.480i";
        public static final String VIDEO_STREAMING_480P = "video.streaming.480p";
        public static final String VIDEO_STREAMING_720I = "video.streaming.720i";
        public static final String VOIP_24KBPS = "voip.24k";
        public static final String VOIP_32KBPS = "voip.32k";
    }

    public static final class Key
    {
        public static final int RO_AVAILABLE_FWD_BW = 4;
        public static final int RO_AVAILABLE_REV_BW = 7;
        public static final int RO_BOUND_INTERFACE = 9;
        public static final int RO_NETWORK_TYPE = 1;
        public static final int RO_PHYSICAL_INTERFACE = 10;
        public static final int RW_DESIRED_FWD_BW = 2;
        public static final int RW_DESIRED_REV_BW = 5;
        public static final int RW_MAX_ALLOWED_LATENCY = 8;
        public static final int RW_REQUIRED_FWD_BW = 3;
        public static final int RW_REQUIRED_REV_BW = 6;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.LinkCapabilities
 * JD-Core Version:        0.6.2
 */