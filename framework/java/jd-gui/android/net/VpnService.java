package android.net;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.os.ServiceManager;
import com.android.internal.net.VpnConfig;
import java.io.FileDescriptor;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class VpnService extends Service
{
    public static final String SERVICE_INTERFACE = "android.net.VpnService";

    private static IConnectivityManager getService()
    {
        return IConnectivityManager.Stub.asInterface(ServiceManager.getService("connectivity"));
    }

    public static Intent prepare(Context paramContext)
    {
        Intent localIntent = null;
        try
        {
            boolean bool = getService().prepareVpn(paramContext.getPackageName(), null);
            if (bool)
                return localIntent;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                localIntent = VpnConfig.getIntentForConfirmation();
        }
    }

    public IBinder onBind(Intent paramIntent)
    {
        if ((paramIntent != null) && ("android.net.VpnService".equals(paramIntent.getAction())));
        for (Callback localCallback = new Callback(null); ; localCallback = null)
            return localCallback;
    }

    public void onRevoke()
    {
        stopSelf();
    }

    // ERROR //
    public boolean protect(int paramInt)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: iload_1
        //     3: invokestatic 90	android/os/ParcelFileDescriptor:fromFd	(I)Landroid/os/ParcelFileDescriptor;
        //     6: astore_2
        //     7: invokestatic 25	android/net/VpnService:getService	()Landroid/net/IConnectivityManager;
        //     10: aload_2
        //     11: invokeinterface 94 2 0
        //     16: istore 8
        //     18: iload 8
        //     20: istore 6
        //     22: aload_2
        //     23: invokevirtual 97	android/os/ParcelFileDescriptor:close	()V
        //     26: goto +32 -> 58
        //     29: astore 5
        //     31: iconst_0
        //     32: istore 6
        //     34: aload_2
        //     35: invokevirtual 97	android/os/ParcelFileDescriptor:close	()V
        //     38: goto +20 -> 58
        //     41: astore 7
        //     43: goto +15 -> 58
        //     46: astore_3
        //     47: aload_2
        //     48: invokevirtual 97	android/os/ParcelFileDescriptor:close	()V
        //     51: aload_3
        //     52: athrow
        //     53: astore 4
        //     55: goto -4 -> 51
        //     58: iload 6
        //     60: ireturn
        //
        // Exception table:
        //     from	to	target	type
        //     2	18	29	java/lang/Exception
        //     22	38	41	java/lang/Exception
        //     2	18	46	finally
        //     47	51	53	java/lang/Exception
    }

    public boolean protect(DatagramSocket paramDatagramSocket)
    {
        return protect(paramDatagramSocket.getFileDescriptor$().getInt$());
    }

    public boolean protect(Socket paramSocket)
    {
        return protect(paramSocket.getFileDescriptor$().getInt$());
    }

    public class Builder
    {
        private final StringBuilder mAddresses = new StringBuilder();
        private final VpnConfig mConfig = new VpnConfig();
        private final StringBuilder mRoutes = new StringBuilder();

        public Builder()
        {
            this.mConfig.user = VpnService.this.getClass().getName();
        }

        private void check(InetAddress paramInetAddress, int paramInt)
        {
            if (paramInetAddress.isLoopbackAddress())
                throw new IllegalArgumentException("Bad address");
            if ((paramInetAddress instanceof Inet4Address))
            {
                if ((paramInt < 0) || (paramInt > 32))
                    throw new IllegalArgumentException("Bad prefixLength");
            }
            else if ((paramInetAddress instanceof Inet6Address))
            {
                if ((paramInt < 0) || (paramInt > 128))
                    throw new IllegalArgumentException("Bad prefixLength");
            }
            else
                throw new IllegalArgumentException("Unsupported family");
        }

        public Builder addAddress(String paramString, int paramInt)
        {
            return addAddress(InetAddress.parseNumericAddress(paramString), paramInt);
        }

        public Builder addAddress(InetAddress paramInetAddress, int paramInt)
        {
            check(paramInetAddress, paramInt);
            if (paramInetAddress.isAnyLocalAddress())
                throw new IllegalArgumentException("Bad address");
            StringBuilder localStringBuilder = this.mAddresses;
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = paramInetAddress.getHostAddress();
            arrayOfObject[1] = Integer.valueOf(paramInt);
            localStringBuilder.append(String.format(" %s/%d", arrayOfObject));
            return this;
        }

        public Builder addDnsServer(String paramString)
        {
            return addDnsServer(InetAddress.parseNumericAddress(paramString));
        }

        public Builder addDnsServer(InetAddress paramInetAddress)
        {
            if ((paramInetAddress.isLoopbackAddress()) || (paramInetAddress.isAnyLocalAddress()))
                throw new IllegalArgumentException("Bad address");
            if (this.mConfig.dnsServers == null)
                this.mConfig.dnsServers = new ArrayList();
            this.mConfig.dnsServers.add(paramInetAddress.getHostAddress());
            return this;
        }

        public Builder addRoute(String paramString, int paramInt)
        {
            return addRoute(InetAddress.parseNumericAddress(paramString), paramInt);
        }

        public Builder addRoute(InetAddress paramInetAddress, int paramInt)
        {
            check(paramInetAddress, paramInt);
            int i = paramInt / 8;
            byte[] arrayOfByte = paramInetAddress.getAddress();
            if (i < arrayOfByte.length)
            {
                arrayOfByte[i] = ((byte)(arrayOfByte[i] << paramInt % 8));
                while (i < arrayOfByte.length)
                {
                    if (arrayOfByte[i] != 0)
                        throw new IllegalArgumentException("Bad address");
                    i++;
                }
            }
            StringBuilder localStringBuilder = this.mRoutes;
            Object[] arrayOfObject = new Object[2];
            arrayOfObject[0] = paramInetAddress.getHostAddress();
            arrayOfObject[1] = Integer.valueOf(paramInt);
            localStringBuilder.append(String.format(" %s/%d", arrayOfObject));
            return this;
        }

        public Builder addSearchDomain(String paramString)
        {
            if (this.mConfig.searchDomains == null)
                this.mConfig.searchDomains = new ArrayList();
            this.mConfig.searchDomains.add(paramString);
            return this;
        }

        public ParcelFileDescriptor establish()
        {
            this.mConfig.addresses = this.mAddresses.toString();
            this.mConfig.routes = this.mRoutes.toString();
            try
            {
                ParcelFileDescriptor localParcelFileDescriptor = VpnService.access$100().establishVpn(this.mConfig);
                return localParcelFileDescriptor;
            }
            catch (RemoteException localRemoteException)
            {
                throw new IllegalStateException(localRemoteException);
            }
        }

        public Builder setConfigureIntent(PendingIntent paramPendingIntent)
        {
            this.mConfig.configureIntent = paramPendingIntent;
            return this;
        }

        public Builder setMtu(int paramInt)
        {
            if (paramInt <= 0)
                throw new IllegalArgumentException("Bad mtu");
            this.mConfig.mtu = paramInt;
            return this;
        }

        public Builder setSession(String paramString)
        {
            this.mConfig.session = paramString;
            return this;
        }
    }

    private class Callback extends Binder
    {
        private Callback()
        {
        }

        protected boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
        {
            if (paramInt1 == 16777215)
                VpnService.this.onRevoke();
            for (boolean bool = true; ; bool = false)
                return bool;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.VpnService
 * JD-Core Version:        0.6.2
 */