package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface INetworkManagementEventObserver extends IInterface
{
    public abstract void interfaceAdded(String paramString)
        throws RemoteException;

    public abstract void interfaceLinkStateChanged(String paramString, boolean paramBoolean)
        throws RemoteException;

    public abstract void interfaceRemoved(String paramString)
        throws RemoteException;

    public abstract void interfaceStatusChanged(String paramString, boolean paramBoolean)
        throws RemoteException;

    public abstract void limitReached(String paramString1, String paramString2)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements INetworkManagementEventObserver
    {
        private static final String DESCRIPTOR = "android.net.INetworkManagementEventObserver";
        static final int TRANSACTION_interfaceAdded = 3;
        static final int TRANSACTION_interfaceLinkStateChanged = 2;
        static final int TRANSACTION_interfaceRemoved = 4;
        static final int TRANSACTION_interfaceStatusChanged = 1;
        static final int TRANSACTION_limitReached = 5;

        public Stub()
        {
            attachInterface(this, "android.net.INetworkManagementEventObserver");
        }

        public static INetworkManagementEventObserver asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.net.INetworkManagementEventObserver");
                if ((localIInterface != null) && ((localIInterface instanceof INetworkManagementEventObserver)))
                    localObject = (INetworkManagementEventObserver)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1 = false;
            boolean bool2 = true;
            switch (paramInt1)
            {
            default:
                bool2 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            }
            while (true)
            {
                return bool2;
                paramParcel2.writeString("android.net.INetworkManagementEventObserver");
                continue;
                paramParcel1.enforceInterface("android.net.INetworkManagementEventObserver");
                String str2 = paramParcel1.readString();
                if (paramParcel1.readInt() != 0)
                    bool1 = bool2;
                interfaceStatusChanged(str2, bool1);
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.net.INetworkManagementEventObserver");
                String str1 = paramParcel1.readString();
                if (paramParcel1.readInt() != 0)
                    bool1 = bool2;
                interfaceLinkStateChanged(str1, bool1);
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.net.INetworkManagementEventObserver");
                interfaceAdded(paramParcel1.readString());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.net.INetworkManagementEventObserver");
                interfaceRemoved(paramParcel1.readString());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.net.INetworkManagementEventObserver");
                limitReached(paramParcel1.readString(), paramParcel1.readString());
                paramParcel2.writeNoException();
            }
        }

        private static class Proxy
            implements INetworkManagementEventObserver
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.net.INetworkManagementEventObserver";
            }

            public void interfaceAdded(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.INetworkManagementEventObserver");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void interfaceLinkStateChanged(String paramString, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.INetworkManagementEventObserver");
                    localParcel1.writeString(paramString);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void interfaceRemoved(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.INetworkManagementEventObserver");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void interfaceStatusChanged(String paramString, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.INetworkManagementEventObserver");
                    localParcel1.writeString(paramString);
                    if (paramBoolean)
                    {
                        localParcel1.writeInt(i);
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void limitReached(String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.INetworkManagementEventObserver");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.INetworkManagementEventObserver
 * JD-Core Version:        0.6.2
 */