package android.net;

import android.os.RemoteException;

public class ThrottleManager
{
    public static final int DIRECTION_RX = 1;
    public static final int DIRECTION_TX = 0;
    public static final String EXTRA_CYCLE_END = "cycleEnd";
    public static final String EXTRA_CYCLE_READ = "cycleRead";
    public static final String EXTRA_CYCLE_START = "cycleStart";
    public static final String EXTRA_CYCLE_WRITE = "cycleWrite";
    public static final String EXTRA_THROTTLE_LEVEL = "level";
    public static final int PERIOD_24HOUR = 6;
    public static final int PERIOD_60MIN = 8;
    public static final int PERIOD_60SEC = 10;
    public static final int PERIOD_7DAY = 4;
    public static final int PERIOD_CYCLE = 0;
    public static final int PERIOD_DAY = 5;
    public static final int PERIOD_HOUR = 7;
    public static final int PERIOD_MINUTE = 9;
    public static final int PERIOD_MONTH = 2;
    public static final int PERIOD_SECOND = 11;
    public static final int PERIOD_WEEK = 3;
    public static final int PERIOD_YEAR = 1;
    public static final String POLICY_CHANGED_ACTION = "android.net.thrott.POLICY_CHANGED_ACTION";
    public static final String THROTTLE_ACTION = "android.net.thrott.THROTTLE_ACTION";
    public static final String THROTTLE_POLL_ACTION = "android.net.thrott.POLL_ACTION";
    private IThrottleManager mService;

    private ThrottleManager()
    {
    }

    public ThrottleManager(IThrottleManager paramIThrottleManager)
    {
        if (paramIThrottleManager == null)
            throw new IllegalArgumentException("ThrottleManager() cannot be constructed with null service");
        this.mService = paramIThrottleManager;
    }

    public long getByteCount(String paramString, int paramInt1, int paramInt2, int paramInt3)
    {
        try
        {
            long l2 = this.mService.getByteCount(paramString, paramInt1, paramInt2, paramInt3);
            l1 = l2;
            return l1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                long l1 = -1L;
        }
    }

    public int getCliffLevel(String paramString, int paramInt)
    {
        try
        {
            int j = this.mService.getCliffLevel(paramString, paramInt);
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                int i = -1;
        }
    }

    public long getCliffThreshold(String paramString, int paramInt)
    {
        try
        {
            long l2 = this.mService.getCliffThreshold(paramString, paramInt);
            l1 = l2;
            return l1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                long l1 = -1L;
        }
    }

    public String getHelpUri()
    {
        try
        {
            String str2 = this.mService.getHelpUri();
            str1 = str2;
            return str1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                String str1 = null;
        }
    }

    public long getPeriodStartTime(String paramString)
    {
        try
        {
            long l2 = this.mService.getPeriodStartTime(paramString);
            l1 = l2;
            return l1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                long l1 = -1L;
        }
    }

    public long getResetTime(String paramString)
    {
        try
        {
            long l2 = this.mService.getResetTime(paramString);
            l1 = l2;
            return l1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                long l1 = -1L;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.ThrottleManager
 * JD-Core Version:        0.6.2
 */