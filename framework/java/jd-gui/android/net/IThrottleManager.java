package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IThrottleManager extends IInterface
{
    public abstract long getByteCount(String paramString, int paramInt1, int paramInt2, int paramInt3)
        throws RemoteException;

    public abstract int getCliffLevel(String paramString, int paramInt)
        throws RemoteException;

    public abstract long getCliffThreshold(String paramString, int paramInt)
        throws RemoteException;

    public abstract String getHelpUri()
        throws RemoteException;

    public abstract long getPeriodStartTime(String paramString)
        throws RemoteException;

    public abstract long getResetTime(String paramString)
        throws RemoteException;

    public abstract int getThrottle(String paramString)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IThrottleManager
    {
        private static final String DESCRIPTOR = "android.net.IThrottleManager";
        static final int TRANSACTION_getByteCount = 1;
        static final int TRANSACTION_getCliffLevel = 6;
        static final int TRANSACTION_getCliffThreshold = 5;
        static final int TRANSACTION_getHelpUri = 7;
        static final int TRANSACTION_getPeriodStartTime = 4;
        static final int TRANSACTION_getResetTime = 3;
        static final int TRANSACTION_getThrottle = 2;

        public Stub()
        {
            attachInterface(this, "android.net.IThrottleManager");
        }

        public static IThrottleManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.net.IThrottleManager");
                if ((localIInterface != null) && ((localIInterface instanceof IThrottleManager)))
                    localObject = (IThrottleManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.net.IThrottleManager");
                continue;
                paramParcel1.enforceInterface("android.net.IThrottleManager");
                long l4 = getByteCount(paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                paramParcel2.writeLong(l4);
                continue;
                paramParcel1.enforceInterface("android.net.IThrottleManager");
                int j = getThrottle(paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(j);
                continue;
                paramParcel1.enforceInterface("android.net.IThrottleManager");
                long l3 = getResetTime(paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeLong(l3);
                continue;
                paramParcel1.enforceInterface("android.net.IThrottleManager");
                long l2 = getPeriodStartTime(paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeLong(l2);
                continue;
                paramParcel1.enforceInterface("android.net.IThrottleManager");
                long l1 = getCliffThreshold(paramParcel1.readString(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                paramParcel2.writeLong(l1);
                continue;
                paramParcel1.enforceInterface("android.net.IThrottleManager");
                int i = getCliffLevel(paramParcel1.readString(), paramParcel1.readInt());
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.net.IThrottleManager");
                String str = getHelpUri();
                paramParcel2.writeNoException();
                paramParcel2.writeString(str);
            }
        }

        private static class Proxy
            implements IThrottleManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public long getByteCount(String paramString, int paramInt1, int paramInt2, int paramInt3)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IThrottleManager");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    localParcel1.writeInt(paramInt3);
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    long l = localParcel2.readLong();
                    return l;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getCliffLevel(String paramString, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IThrottleManager");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public long getCliffThreshold(String paramString, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IThrottleManager");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    long l = localParcel2.readLong();
                    return l;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getHelpUri()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IThrottleManager");
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.net.IThrottleManager";
            }

            public long getPeriodStartTime(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IThrottleManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    long l = localParcel2.readLong();
                    return l;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public long getResetTime(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IThrottleManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    long l = localParcel2.readLong();
                    return l;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getThrottle(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IThrottleManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.IThrottleManager
 * JD-Core Version:        0.6.2
 */