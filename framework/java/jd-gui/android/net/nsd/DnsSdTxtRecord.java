package android.net.nsd;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class DnsSdTxtRecord
    implements Parcelable
{
    public static final Parcelable.Creator<DnsSdTxtRecord> CREATOR = new Parcelable.Creator()
    {
        public DnsSdTxtRecord createFromParcel(Parcel paramAnonymousParcel)
        {
            DnsSdTxtRecord localDnsSdTxtRecord = new DnsSdTxtRecord();
            paramAnonymousParcel.readByteArray(localDnsSdTxtRecord.mData);
            return localDnsSdTxtRecord;
        }

        public DnsSdTxtRecord[] newArray(int paramAnonymousInt)
        {
            return new DnsSdTxtRecord[paramAnonymousInt];
        }
    };
    private static final byte mSeperator = 61;
    private byte[] mData;

    public DnsSdTxtRecord()
    {
        this.mData = new byte[0];
    }

    public DnsSdTxtRecord(DnsSdTxtRecord paramDnsSdTxtRecord)
    {
        if ((paramDnsSdTxtRecord != null) && (paramDnsSdTxtRecord.mData != null))
            this.mData = ((byte[])paramDnsSdTxtRecord.mData.clone());
    }

    public DnsSdTxtRecord(byte[] paramArrayOfByte)
    {
        this.mData = ((byte[])paramArrayOfByte.clone());
    }

    private String getKey(int paramInt)
    {
        int i = 0;
        for (int j = 0; (j < paramInt) && (i < this.mData.length); j++)
            i += 1 + this.mData[i];
        int m;
        if (i < this.mData.length)
        {
            int k = this.mData[i];
            m = 0;
            if ((m < k) && (this.mData[(1 + (i + m))] != 61));
        }
        for (String str = new String(this.mData, i + 1, m); ; str = null)
        {
            return str;
            m++;
            break;
        }
    }

    private byte[] getValue(int paramInt)
    {
        int i = 0;
        byte[] arrayOfByte = null;
        for (int j = 0; (j < paramInt) && (i < this.mData.length); j++)
            i += 1 + this.mData[i];
        int k;
        if (i < this.mData.length)
            k = this.mData[i];
        for (int m = 0; ; m++)
            if (m < k)
            {
                if (this.mData[(1 + (i + m))] == 61)
                {
                    arrayOfByte = new byte[-1 + (k - m)];
                    System.arraycopy(this.mData, 2 + (i + m), arrayOfByte, 0, -1 + (k - m));
                }
            }
            else
                return arrayOfByte;
    }

    private byte[] getValue(String paramString)
    {
        int i = 0;
        String str = getKey(i);
        if (str != null)
            if (paramString.compareToIgnoreCase(str) != 0);
        for (byte[] arrayOfByte = getValue(i); ; arrayOfByte = null)
        {
            return arrayOfByte;
            i++;
            break;
        }
    }

    private String getValueAsString(int paramInt)
    {
        byte[] arrayOfByte = getValue(paramInt);
        if (arrayOfByte != null);
        for (String str = new String(arrayOfByte); ; str = null)
            return str;
    }

    private void insert(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, int paramInt)
    {
        byte[] arrayOfByte = this.mData;
        if (paramArrayOfByte2 != null);
        int j;
        for (int i = paramArrayOfByte2.length; ; i = 0)
        {
            j = 0;
            for (int k = 0; (k < paramInt) && (j < this.mData.length); k++)
                j += (0xFF & 1 + this.mData[j]);
        }
        int m = i + paramArrayOfByte1.length;
        if (paramArrayOfByte2 != null);
        for (int n = 1; ; n = 0)
        {
            int i1 = m + n;
            int i2 = 1 + (i1 + arrayOfByte.length);
            this.mData = new byte[i2];
            System.arraycopy(arrayOfByte, 0, this.mData, 0, j);
            int i3 = arrayOfByte.length - j;
            System.arraycopy(arrayOfByte, j, this.mData, i2 - i3, i3);
            this.mData[j] = ((byte)i1);
            System.arraycopy(paramArrayOfByte1, 0, this.mData, j + 1, paramArrayOfByte1.length);
            if (paramArrayOfByte2 != null)
            {
                this.mData[(j + 1 + paramArrayOfByte1.length)] = 61;
                System.arraycopy(paramArrayOfByte2, 0, this.mData, 2 + (j + paramArrayOfByte1.length), i);
            }
            return;
        }
    }

    public boolean contains(String paramString)
    {
        int i = 0;
        String str = getKey(i);
        if (str != null)
            if (paramString.compareToIgnoreCase(str) != 0);
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            i++;
            break;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool;
        if (paramObject == this)
            bool = true;
        while (true)
        {
            return bool;
            if (!(paramObject instanceof DnsSdTxtRecord))
                bool = false;
            else
                bool = Arrays.equals(((DnsSdTxtRecord)paramObject).mData, this.mData);
        }
    }

    public String get(String paramString)
    {
        byte[] arrayOfByte = getValue(paramString);
        if (arrayOfByte != null);
        for (String str = new String(arrayOfByte); ; str = null)
            return str;
    }

    public byte[] getRawData()
    {
        return (byte[])this.mData.clone();
    }

    public int hashCode()
    {
        return Arrays.hashCode(this.mData);
    }

    public int keyCount()
    {
        int i = 0;
        int j = 0;
        while (j < this.mData.length)
        {
            j += (0xFF & 1 + this.mData[j]);
            i++;
        }
        return i;
    }

    public int remove(String paramString)
    {
        int i = 0;
        int j = 0;
        int k;
        if (i < this.mData.length)
        {
            k = this.mData[i];
            if ((paramString.length() <= k) && ((paramString.length() == k) || (this.mData[(1 + (i + paramString.length()))] == 61)) && (paramString.compareToIgnoreCase(new String(this.mData, i + 1, paramString.length())) == 0))
            {
                byte[] arrayOfByte = this.mData;
                this.mData = new byte[-1 + (arrayOfByte.length - k)];
                System.arraycopy(arrayOfByte, 0, this.mData, 0, i);
                System.arraycopy(arrayOfByte, 1 + (i + k), this.mData, i, -1 + (arrayOfByte.length - i - k));
            }
        }
        while (true)
        {
            return j;
            i += (0xFF & k + 1);
            j++;
            break;
            j = -1;
        }
    }

    public void set(String paramString1, String paramString2)
    {
        byte[] arrayOfByte1;
        int i;
        if (paramString2 != null)
        {
            arrayOfByte1 = paramString2.getBytes();
            i = arrayOfByte1.length;
        }
        byte[] arrayOfByte2;
        while (true)
        {
            int j;
            try
            {
                arrayOfByte2 = paramString1.getBytes("US-ASCII");
                j = 0;
                if (j >= arrayOfByte2.length)
                    break;
                if (arrayOfByte2[j] == 61)
                {
                    throw new IllegalArgumentException("= is not a valid character in key");
                    arrayOfByte1 = null;
                    i = 0;
                    continue;
                }
            }
            catch (UnsupportedEncodingException localUnsupportedEncodingException)
            {
                throw new IllegalArgumentException("key should be US-ASCII");
            }
            j++;
        }
        if (i + arrayOfByte2.length >= 255)
            throw new IllegalArgumentException("Key and Value length cannot exceed 255 bytes");
        int k = remove(paramString1);
        if (k == -1)
            k = keyCount();
        insert(arrayOfByte2, arrayOfByte1, k);
    }

    public int size()
    {
        return this.mData.length;
    }

    public String toString()
    {
        Object localObject = null;
        int i = 0;
        String str1 = getKey(i);
        if (str1 != null)
        {
            String str2 = "{" + str1;
            String str3 = getValueAsString(i);
            String str4;
            if (str3 != null)
            {
                str4 = str2 + "=" + str3 + "}";
                label79: if (localObject != null)
                    break label117;
            }
            label117: for (localObject = str4; ; localObject = (String)localObject + ", " + str4)
            {
                i++;
                break;
                str4 = str2 + "}";
                break label79;
            }
        }
        if (localObject != null);
        while (true)
        {
            return localObject;
            localObject = "";
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeByteArray(this.mData);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.nsd.DnsSdTxtRecord
 * JD-Core Version:        0.6.2
 */