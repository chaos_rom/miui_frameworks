package android.net.nsd;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface INsdManager extends IInterface
{
    public abstract Messenger getMessenger()
        throws RemoteException;

    public abstract void setEnabled(boolean paramBoolean)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements INsdManager
    {
        private static final String DESCRIPTOR = "android.net.nsd.INsdManager";
        static final int TRANSACTION_getMessenger = 1;
        static final int TRANSACTION_setEnabled = 2;

        public Stub()
        {
            attachInterface(this, "android.net.nsd.INsdManager");
        }

        public static INsdManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.net.nsd.INsdManager");
                if ((localIInterface != null) && ((localIInterface instanceof INsdManager)))
                    localObject = (INsdManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = false;
            int i = 1;
            switch (paramInt1)
            {
            default:
                i = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            }
            while (true)
            {
                return i;
                paramParcel2.writeString("android.net.nsd.INsdManager");
                continue;
                paramParcel1.enforceInterface("android.net.nsd.INsdManager");
                Messenger localMessenger = getMessenger();
                paramParcel2.writeNoException();
                if (localMessenger != null)
                {
                    paramParcel2.writeInt(i);
                    localMessenger.writeToParcel(paramParcel2, i);
                }
                else
                {
                    paramParcel2.writeInt(0);
                    continue;
                    paramParcel1.enforceInterface("android.net.nsd.INsdManager");
                    if (paramParcel1.readInt() != 0)
                        bool = i;
                    setEnabled(bool);
                    paramParcel2.writeNoException();
                }
            }
        }

        private static class Proxy
            implements INsdManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.net.nsd.INsdManager";
            }

            public Messenger getMessenger()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.nsd.INsdManager");
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localMessenger = (Messenger)Messenger.CREATOR.createFromParcel(localParcel2);
                        return localMessenger;
                    }
                    Messenger localMessenger = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setEnabled(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.nsd.INsdManager");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.nsd.INsdManager
 * JD-Core Version:        0.6.2
 */