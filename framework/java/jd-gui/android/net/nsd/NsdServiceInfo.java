package android.net.nsd;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.net.InetAddress;
import java.net.UnknownHostException;

public final class NsdServiceInfo
    implements Parcelable
{
    public static final Parcelable.Creator<NsdServiceInfo> CREATOR = new Parcelable.Creator()
    {
        public NsdServiceInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            NsdServiceInfo localNsdServiceInfo = new NsdServiceInfo();
            NsdServiceInfo.access$002(localNsdServiceInfo, paramAnonymousParcel.readString());
            NsdServiceInfo.access$102(localNsdServiceInfo, paramAnonymousParcel.readString());
            NsdServiceInfo.access$202(localNsdServiceInfo, (DnsSdTxtRecord)paramAnonymousParcel.readParcelable(null));
            if (paramAnonymousParcel.readByte() == 1);
            try
            {
                NsdServiceInfo.access$302(localNsdServiceInfo, InetAddress.getByAddress(paramAnonymousParcel.createByteArray()));
                label59: NsdServiceInfo.access$402(localNsdServiceInfo, paramAnonymousParcel.readInt());
                return localNsdServiceInfo;
            }
            catch (UnknownHostException localUnknownHostException)
            {
                break label59;
            }
        }

        public NsdServiceInfo[] newArray(int paramAnonymousInt)
        {
            return new NsdServiceInfo[paramAnonymousInt];
        }
    };
    private InetAddress mHost;
    private int mPort;
    private String mServiceName;
    private String mServiceType;
    private DnsSdTxtRecord mTxtRecord;

    public NsdServiceInfo()
    {
    }

    public NsdServiceInfo(String paramString1, String paramString2, DnsSdTxtRecord paramDnsSdTxtRecord)
    {
        this.mServiceName = paramString1;
        this.mServiceType = paramString2;
        this.mTxtRecord = paramDnsSdTxtRecord;
    }

    public int describeContents()
    {
        return 0;
    }

    public InetAddress getHost()
    {
        return this.mHost;
    }

    public int getPort()
    {
        return this.mPort;
    }

    public String getServiceName()
    {
        return this.mServiceName;
    }

    public String getServiceType()
    {
        return this.mServiceType;
    }

    public DnsSdTxtRecord getTxtRecord()
    {
        return this.mTxtRecord;
    }

    public void setHost(InetAddress paramInetAddress)
    {
        this.mHost = paramInetAddress;
    }

    public void setPort(int paramInt)
    {
        this.mPort = paramInt;
    }

    public void setServiceName(String paramString)
    {
        this.mServiceName = paramString;
    }

    public void setServiceType(String paramString)
    {
        this.mServiceType = paramString;
    }

    public void setTxtRecord(DnsSdTxtRecord paramDnsSdTxtRecord)
    {
        this.mTxtRecord = new DnsSdTxtRecord(paramDnsSdTxtRecord);
    }

    public String toString()
    {
        StringBuffer localStringBuffer = new StringBuffer();
        localStringBuffer.append("name: ").append(this.mServiceName).append("type: ").append(this.mServiceType).append("host: ").append(this.mHost).append("port: ").append(this.mPort).append("txtRecord: ").append(this.mTxtRecord);
        return localStringBuffer.toString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.mServiceName);
        paramParcel.writeString(this.mServiceType);
        paramParcel.writeParcelable(this.mTxtRecord, paramInt);
        if (this.mHost != null)
        {
            paramParcel.writeByte((byte)1);
            paramParcel.writeByteArray(this.mHost.getAddress());
        }
        while (true)
        {
            paramParcel.writeInt(this.mPort);
            return;
            paramParcel.writeByte((byte)0);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.nsd.NsdServiceInfo
 * JD-Core Version:        0.6.2
 */