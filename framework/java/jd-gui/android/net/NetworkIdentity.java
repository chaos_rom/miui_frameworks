package android.net;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import com.android.internal.util.Objects;

public class NetworkIdentity
{
    public static final boolean COMBINE_SUBTYPE_ENABLED = true;
    public static final int SUBTYPE_COMBINED = -1;
    final String mNetworkId;
    final boolean mRoaming;
    final int mSubType;
    final String mSubscriberId;
    final int mType;

    public NetworkIdentity(int paramInt1, int paramInt2, String paramString1, String paramString2, boolean paramBoolean)
    {
        this.mType = paramInt1;
        this.mSubType = -1;
        this.mSubscriberId = paramString1;
        this.mNetworkId = paramString2;
        this.mRoaming = paramBoolean;
    }

    public static NetworkIdentity buildNetworkIdentity(Context paramContext, NetworkState paramNetworkState)
    {
        int i = paramNetworkState.networkInfo.getType();
        int j = paramNetworkState.networkInfo.getSubtype();
        String str1 = null;
        String str2 = null;
        boolean bool = false;
        TelephonyManager localTelephonyManager;
        if (ConnectivityManager.isNetworkTypeMobile(i))
        {
            localTelephonyManager = (TelephonyManager)paramContext.getSystemService("phone");
            bool = localTelephonyManager.isNetworkRoaming();
            if (paramNetworkState.subscriberId != null)
                str1 = paramNetworkState.subscriberId;
        }
        while (true)
        {
            return new NetworkIdentity(i, j, str1, str2, bool);
            str1 = localTelephonyManager.getSubscriberId();
            continue;
            if (i == 1)
            {
                if (paramNetworkState.networkId == null)
                    break;
                str2 = paramNetworkState.networkId;
            }
        }
        WifiInfo localWifiInfo = ((WifiManager)paramContext.getSystemService("wifi")).getConnectionInfo();
        if (localWifiInfo != null);
        for (str2 = localWifiInfo.getSSID(); ; str2 = null)
            break;
    }

    public static String scrubSubscriberId(String paramString)
    {
        if ("eng".equals(Build.TYPE));
        while (true)
        {
            return paramString;
            if (paramString != null)
                paramString = paramString.substring(0, Math.min(6, paramString.length())) + "...";
            else
                paramString = "null";
        }
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = false;
        if ((paramObject instanceof NetworkIdentity))
        {
            NetworkIdentity localNetworkIdentity = (NetworkIdentity)paramObject;
            if ((this.mType == localNetworkIdentity.mType) && (this.mSubType == localNetworkIdentity.mSubType) && (this.mRoaming == localNetworkIdentity.mRoaming) && (Objects.equal(this.mSubscriberId, localNetworkIdentity.mSubscriberId)) && (Objects.equal(this.mNetworkId, localNetworkIdentity.mNetworkId)))
                bool = true;
        }
        return bool;
    }

    public String getNetworkId()
    {
        return this.mNetworkId;
    }

    public boolean getRoaming()
    {
        return this.mRoaming;
    }

    public int getSubType()
    {
        return this.mSubType;
    }

    public String getSubscriberId()
    {
        return this.mSubscriberId;
    }

    public int getType()
    {
        return this.mType;
    }

    public int hashCode()
    {
        Object[] arrayOfObject = new Object[5];
        arrayOfObject[0] = Integer.valueOf(this.mType);
        arrayOfObject[1] = Integer.valueOf(this.mSubType);
        arrayOfObject[2] = this.mSubscriberId;
        arrayOfObject[3] = this.mNetworkId;
        arrayOfObject[4] = Boolean.valueOf(this.mRoaming);
        return Objects.hashCode(arrayOfObject);
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder("[");
        localStringBuilder.append("type=").append(ConnectivityManager.getNetworkTypeName(this.mType));
        localStringBuilder.append(", subType=");
        localStringBuilder.append("COMBINED");
        if (this.mSubscriberId != null)
            localStringBuilder.append(", subscriberId=").append(scrubSubscriberId(this.mSubscriberId));
        if (this.mNetworkId != null)
            localStringBuilder.append(", networkId=").append(this.mNetworkId);
        if (this.mRoaming)
            localStringBuilder.append(", ROAMING");
        return "]";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.NetworkIdentity
 * JD-Core Version:        0.6.2
 */