package android.net;

public class LocalSocketAddress
{
    private final String name;
    private final Namespace namespace;

    public LocalSocketAddress(String paramString)
    {
        this(paramString, Namespace.ABSTRACT);
    }

    public LocalSocketAddress(String paramString, Namespace paramNamespace)
    {
        this.name = paramString;
        this.namespace = paramNamespace;
    }

    public String getName()
    {
        return this.name;
    }

    public Namespace getNamespace()
    {
        return this.namespace;
    }

    public static enum Namespace
    {
        private int id;

        static
        {
            FILESYSTEM = new Namespace("FILESYSTEM", 2, 2);
            Namespace[] arrayOfNamespace = new Namespace[3];
            arrayOfNamespace[0] = ABSTRACT;
            arrayOfNamespace[1] = RESERVED;
            arrayOfNamespace[2] = FILESYSTEM;
        }

        private Namespace(int paramInt)
        {
            this.id = paramInt;
        }

        int getId()
        {
            return this.id;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.LocalSocketAddress
 * JD-Core Version:        0.6.2
 */