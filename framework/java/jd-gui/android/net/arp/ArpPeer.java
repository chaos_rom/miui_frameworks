package android.net.arp;

import android.os.SystemClock;
import java.io.IOException;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import libcore.net.RawSocket;

public class ArpPeer
{
    private static final int ARP_LENGTH = 28;
    private static final int ETHERNET_TYPE = 1;
    private static final int IPV4_LENGTH = 4;
    private static final int MAC_ADDR_LENGTH = 6;
    private static final int MAX_LENGTH = 1500;
    private static final String TAG = "ArpPeer";
    private final byte[] L2_BROADCAST;
    private String mInterfaceName;
    private final InetAddress mMyAddr;
    private final byte[] mMyMac = new byte[6];
    private final InetAddress mPeer;
    private final RawSocket mSocket;

    public ArpPeer(String paramString1, InetAddress paramInetAddress1, String paramString2, InetAddress paramInetAddress2)
        throws SocketException
    {
        this.mInterfaceName = paramString1;
        this.mMyAddr = paramInetAddress1;
        for (int i = 0; i < 6; i++)
            this.mMyMac[i] = ((byte)Integer.parseInt(paramString2.substring(i * 3, 2 + i * 3), 16));
        if (((paramInetAddress1 instanceof Inet6Address)) || ((paramInetAddress2 instanceof Inet6Address)))
            throw new IllegalArgumentException("IPv6 unsupported");
        this.mPeer = paramInetAddress2;
        this.L2_BROADCAST = new byte[6];
        Arrays.fill(this.L2_BROADCAST, (byte)-1);
        this.mSocket = new RawSocket(this.mInterfaceName, (short)2054);
    }

    public void close()
    {
        try
        {
            this.mSocket.close();
            label7: return;
        }
        catch (IOException localIOException)
        {
            break label7;
        }
    }

    public byte[] doArp(int paramInt)
    {
        ByteBuffer localByteBuffer = ByteBuffer.allocate(1500);
        byte[] arrayOfByte1 = this.mPeer.getAddress();
        long l1 = SystemClock.elapsedRealtime() + paramInt;
        localByteBuffer.clear();
        localByteBuffer.order(ByteOrder.BIG_ENDIAN);
        localByteBuffer.putShort((short)1);
        localByteBuffer.putShort((short)2048);
        localByteBuffer.put((byte)6);
        localByteBuffer.put((byte)4);
        localByteBuffer.putShort((short)1);
        localByteBuffer.put(this.mMyMac);
        localByteBuffer.put(this.mMyAddr.getAddress());
        localByteBuffer.put(new byte[6]);
        localByteBuffer.put(arrayOfByte1);
        localByteBuffer.flip();
        this.mSocket.write(this.L2_BROADCAST, localByteBuffer.array(), 0, localByteBuffer.limit());
        byte[] arrayOfByte2 = new byte[1500];
        byte[] arrayOfByte3;
        while (SystemClock.elapsedRealtime() < l1)
        {
            long l2 = l1 - SystemClock.elapsedRealtime();
            if ((this.mSocket.read(arrayOfByte2, 0, arrayOfByte2.length, -1, (int)l2) >= 28) && (arrayOfByte2[0] == 0) && (arrayOfByte2[1] == 1) && (arrayOfByte2[2] == 8) && (arrayOfByte2[3] == 0) && (arrayOfByte2[4] == 6) && (arrayOfByte2[5] == 4) && (arrayOfByte2[6] == 0) && (arrayOfByte2[7] == 2) && (arrayOfByte2[14] == arrayOfByte1[0]) && (arrayOfByte2[15] == arrayOfByte1[1]) && (arrayOfByte2[16] == arrayOfByte1[2]) && (arrayOfByte2[17] == arrayOfByte1[3]))
            {
                arrayOfByte3 = new byte[6];
                System.arraycopy(arrayOfByte2, 8, arrayOfByte3, 0, 6);
            }
        }
        while (true)
        {
            return arrayOfByte3;
            arrayOfByte3 = null;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.arp.ArpPeer
 * JD-Core Version:        0.6.2
 */