package android.net.http;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.ContentResolver;
import android.content.Context;
import android.net.SSLCertificateSocketFactory;
import android.net.SSLSessionCache;
import android.os.Looper;
import android.util.Base64;
import android.util.Log;
import com.android.internal.http.HttpDateTime;
import com.android.internal.os.RuntimeInit;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.RequestWrapper;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.BasicHttpProcessor;
import org.apache.http.protocol.HttpContext;

public final class AndroidHttpClient
    implements HttpClient
{
    public static long DEFAULT_SYNC_MIN_GZIP_BYTES = 0L;
    private static final int SOCKET_OPERATION_TIMEOUT = 60000;
    private static final String TAG = "AndroidHttpClient";
    private static final HttpRequestInterceptor sThreadCheckInterceptor = new HttpRequestInterceptor()
    {
        public void process(HttpRequest paramAnonymousHttpRequest, HttpContext paramAnonymousHttpContext)
        {
            if ((Looper.myLooper() != null) && (Looper.myLooper() == Looper.getMainLooper()))
                throw new RuntimeException("This thread forbids HTTP requests");
        }
    };
    private static String[] textContentTypes;
    private volatile LoggingConfiguration curlConfiguration;
    private final HttpClient delegate;
    private RuntimeException mLeakedException = new IllegalStateException("AndroidHttpClient created and never closed");

    static
    {
        String[] arrayOfString = new String[3];
        arrayOfString[0] = "text/";
        arrayOfString[1] = "application/xml";
        arrayOfString[2] = "application/json";
        textContentTypes = arrayOfString;
    }

    private AndroidHttpClient(ClientConnectionManager paramClientConnectionManager, HttpParams paramHttpParams)
    {
        this.delegate = new DefaultHttpClient(paramClientConnectionManager, paramHttpParams)
        {
            protected HttpContext createHttpContext()
            {
                BasicHttpContext localBasicHttpContext = new BasicHttpContext();
                localBasicHttpContext.setAttribute("http.authscheme-registry", getAuthSchemes());
                localBasicHttpContext.setAttribute("http.cookiespec-registry", getCookieSpecs());
                localBasicHttpContext.setAttribute("http.auth.credentials-provider", getCredentialsProvider());
                return localBasicHttpContext;
            }

            protected BasicHttpProcessor createHttpProcessor()
            {
                BasicHttpProcessor localBasicHttpProcessor = super.createHttpProcessor();
                localBasicHttpProcessor.addRequestInterceptor(AndroidHttpClient.sThreadCheckInterceptor);
                localBasicHttpProcessor.addRequestInterceptor(new AndroidHttpClient.CurlLogger(AndroidHttpClient.this, null));
                return localBasicHttpProcessor;
            }
        };
    }

    public static AbstractHttpEntity getCompressedEntity(byte[] paramArrayOfByte, ContentResolver paramContentResolver)
        throws IOException
    {
        ByteArrayEntity localByteArrayEntity;
        if (paramArrayOfByte.length < getMinGzipSize(paramContentResolver))
            localByteArrayEntity = new ByteArrayEntity(paramArrayOfByte);
        while (true)
        {
            return localByteArrayEntity;
            ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream localGZIPOutputStream = new GZIPOutputStream(localByteArrayOutputStream);
            localGZIPOutputStream.write(paramArrayOfByte);
            localGZIPOutputStream.close();
            localByteArrayEntity = new ByteArrayEntity(localByteArrayOutputStream.toByteArray());
            localByteArrayEntity.setContentEncoding("gzip");
        }
    }

    public static long getMinGzipSize(ContentResolver paramContentResolver)
    {
        return DEFAULT_SYNC_MIN_GZIP_BYTES;
    }

    public static InputStream getUngzippedContent(HttpEntity paramHttpEntity)
        throws IOException
    {
        Object localObject1 = paramHttpEntity.getContent();
        Object localObject2;
        if (localObject1 == null)
            localObject2 = localObject1;
        while (true)
        {
            return localObject2;
            Header localHeader = paramHttpEntity.getContentEncoding();
            if (localHeader == null)
            {
                localObject2 = localObject1;
            }
            else
            {
                String str = localHeader.getValue();
                if (str == null)
                {
                    localObject2 = localObject1;
                }
                else
                {
                    if (str.contains("gzip"))
                        localObject1 = new GZIPInputStream((InputStream)localObject1);
                    localObject2 = localObject1;
                }
            }
        }
    }

    private static boolean isBinaryContent(HttpUriRequest paramHttpUriRequest)
    {
        boolean bool = true;
        Header[] arrayOfHeader1 = paramHttpUriRequest.getHeaders("content-encoding");
        int i1;
        if (arrayOfHeader1 != null)
        {
            int n = arrayOfHeader1.length;
            i1 = 0;
            if (i1 < n)
                if (!"gzip".equalsIgnoreCase(arrayOfHeader1[i1].getValue()));
        }
        label140: label144: 
        while (true)
        {
            return bool;
            i1++;
            break;
            Header[] arrayOfHeader2 = paramHttpUriRequest.getHeaders("content-type");
            if (arrayOfHeader2 != null)
            {
                int i = arrayOfHeader2.length;
                for (int j = 0; ; j++)
                {
                    if (j >= i)
                        break label144;
                    Header localHeader = arrayOfHeader2[j];
                    String[] arrayOfString = textContentTypes;
                    int k = arrayOfString.length;
                    for (int m = 0; ; m++)
                    {
                        if (m >= k)
                            break label140;
                        String str = arrayOfString[m];
                        if (localHeader.getValue().startsWith(str))
                        {
                            bool = false;
                            break;
                        }
                    }
                }
            }
        }
    }

    public static void modifyRequestToAcceptGzipResponse(HttpRequest paramHttpRequest)
    {
        paramHttpRequest.addHeader("Accept-Encoding", "gzip");
    }

    public static AndroidHttpClient newInstance(String paramString)
    {
        return newInstance(paramString, null);
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public static AndroidHttpClient newInstance(String paramString, Context paramContext)
    {
        BasicHttpParams localBasicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setStaleCheckingEnabled(localBasicHttpParams, false);
        HttpConnectionParams.setConnectionTimeout(localBasicHttpParams, 60000);
        HttpConnectionParams.setSoTimeout(localBasicHttpParams, 60000);
        HttpConnectionParams.setSocketBufferSize(localBasicHttpParams, 8192);
        HttpClientParams.setRedirecting(localBasicHttpParams, false);
        if (paramContext == null);
        for (SSLSessionCache localSSLSessionCache = null; ; localSSLSessionCache = new SSLSessionCache(paramContext))
        {
            HttpProtocolParams.setUserAgent(localBasicHttpParams, Injector.getUserAgent(paramString));
            SchemeRegistry localSchemeRegistry = new SchemeRegistry();
            localSchemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            localSchemeRegistry.register(new Scheme("https", SSLCertificateSocketFactory.getHttpSocketFactory(60000, localSSLSessionCache), 443));
            return new AndroidHttpClient(new ThreadSafeClientConnManager(localBasicHttpParams, localSchemeRegistry), localBasicHttpParams);
        }
    }

    public static long parseDate(String paramString)
    {
        return HttpDateTime.parse(paramString);
    }

    private static String toCurl(HttpUriRequest paramHttpUriRequest, boolean paramBoolean)
        throws IOException
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("curl ");
        Header[] arrayOfHeader = paramHttpUriRequest.getAllHeaders();
        int i = arrayOfHeader.length;
        int j = 0;
        if (j < i)
        {
            Header localHeader = arrayOfHeader[j];
            if ((!paramBoolean) && ((localHeader.getName().equals("Authorization")) || (localHeader.getName().equals("Cookie"))));
            while (true)
            {
                j++;
                break;
                localStringBuilder.append("--header \"");
                localStringBuilder.append(localHeader.toString().trim());
                localStringBuilder.append("\" ");
            }
        }
        URI localURI = paramHttpUriRequest.getURI();
        if ((paramHttpUriRequest instanceof RequestWrapper))
        {
            HttpRequest localHttpRequest = ((RequestWrapper)paramHttpUriRequest).getOriginal();
            if ((localHttpRequest instanceof HttpUriRequest))
                localURI = ((HttpUriRequest)localHttpRequest).getURI();
        }
        localStringBuilder.append("\"");
        localStringBuilder.append(localURI);
        localStringBuilder.append("\"");
        ByteArrayOutputStream localByteArrayOutputStream;
        if ((paramHttpUriRequest instanceof HttpEntityEnclosingRequest))
        {
            HttpEntity localHttpEntity = ((HttpEntityEnclosingRequest)paramHttpUriRequest).getEntity();
            if ((localHttpEntity != null) && (localHttpEntity.isRepeatable()))
            {
                if (localHttpEntity.getContentLength() >= 1024L)
                    break label345;
                localByteArrayOutputStream = new ByteArrayOutputStream();
                localHttpEntity.writeTo(localByteArrayOutputStream);
                if (!isBinaryContent(paramHttpUriRequest))
                    break label316;
                String str2 = Base64.encodeToString(localByteArrayOutputStream.toByteArray(), 2);
                localStringBuilder.insert(0, "echo '" + str2 + "' | base64 -d > /tmp/$$.bin; ");
                localStringBuilder.append(" --data-binary @/tmp/$$.bin");
            }
        }
        while (true)
        {
            return localStringBuilder.toString();
            label316: String str1 = localByteArrayOutputStream.toString();
            localStringBuilder.append(" --data-ascii \"").append(str1).append("\"");
            continue;
            label345: localStringBuilder.append(" [TOO MUCH DATA TO INCLUDE]");
        }
    }

    public void close()
    {
        if (this.mLeakedException != null)
        {
            getConnectionManager().shutdown();
            this.mLeakedException = null;
        }
    }

    public void disableCurlLogging()
    {
        this.curlConfiguration = null;
    }

    public void enableCurlLogging(String paramString, int paramInt)
    {
        if (paramString == null)
            throw new NullPointerException("name");
        if ((paramInt < 2) || (paramInt > 7))
            throw new IllegalArgumentException("Level is out of range [2..7]");
        this.curlConfiguration = new LoggingConfiguration(paramString, paramInt, null);
    }

    public <T> T execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, ResponseHandler<? extends T> paramResponseHandler)
        throws IOException, ClientProtocolException
    {
        return this.delegate.execute(paramHttpHost, paramHttpRequest, paramResponseHandler);
    }

    public <T> T execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, ResponseHandler<? extends T> paramResponseHandler, HttpContext paramHttpContext)
        throws IOException, ClientProtocolException
    {
        return this.delegate.execute(paramHttpHost, paramHttpRequest, paramResponseHandler, paramHttpContext);
    }

    public <T> T execute(HttpUriRequest paramHttpUriRequest, ResponseHandler<? extends T> paramResponseHandler)
        throws IOException, ClientProtocolException
    {
        return this.delegate.execute(paramHttpUriRequest, paramResponseHandler);
    }

    public <T> T execute(HttpUriRequest paramHttpUriRequest, ResponseHandler<? extends T> paramResponseHandler, HttpContext paramHttpContext)
        throws IOException, ClientProtocolException
    {
        return this.delegate.execute(paramHttpUriRequest, paramResponseHandler, paramHttpContext);
    }

    public HttpResponse execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest)
        throws IOException
    {
        return this.delegate.execute(paramHttpHost, paramHttpRequest);
    }

    public HttpResponse execute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
        throws IOException
    {
        return this.delegate.execute(paramHttpHost, paramHttpRequest, paramHttpContext);
    }

    public HttpResponse execute(HttpUriRequest paramHttpUriRequest)
        throws IOException
    {
        return this.delegate.execute(paramHttpUriRequest);
    }

    public HttpResponse execute(HttpUriRequest paramHttpUriRequest, HttpContext paramHttpContext)
        throws IOException
    {
        return this.delegate.execute(paramHttpUriRequest, paramHttpContext);
    }

    protected void finalize()
        throws Throwable
    {
        super.finalize();
        if (this.mLeakedException != null)
        {
            Log.e("AndroidHttpClient", "Leak found", this.mLeakedException);
            this.mLeakedException = null;
        }
    }

    public ClientConnectionManager getConnectionManager()
    {
        return this.delegate.getConnectionManager();
    }

    public HttpParams getParams()
    {
        return this.delegate.getParams();
    }

    private class CurlLogger
        implements HttpRequestInterceptor
    {
        private CurlLogger()
        {
        }

        public void process(HttpRequest paramHttpRequest, HttpContext paramHttpContext)
            throws HttpException, IOException
        {
            AndroidHttpClient.LoggingConfiguration localLoggingConfiguration = AndroidHttpClient.this.curlConfiguration;
            if ((localLoggingConfiguration != null) && (AndroidHttpClient.LoggingConfiguration.access$400(localLoggingConfiguration)) && ((paramHttpRequest instanceof HttpUriRequest)))
                AndroidHttpClient.LoggingConfiguration.access$600(localLoggingConfiguration, AndroidHttpClient.toCurl((HttpUriRequest)paramHttpRequest, false));
        }
    }

    private static class LoggingConfiguration
    {
        private final int level;
        private final String tag;

        private LoggingConfiguration(String paramString, int paramInt)
        {
            this.tag = paramString;
            this.level = paramInt;
        }

        private boolean isLoggable()
        {
            return Log.isLoggable(this.tag, this.level);
        }

        private void println(String paramString)
        {
            Log.println(this.level, this.tag, paramString);
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static String getUserAgent(String paramString)
        {
            if (paramString != null);
            while (true)
            {
                return paramString;
                paramString = RuntimeInit.callGetDefaultUserAgent();
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.http.AndroidHttpClient
 * JD-Core Version:        0.6.2
 */