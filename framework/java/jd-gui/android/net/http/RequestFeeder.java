package android.net.http;

import org.apache.http.HttpHost;

abstract interface RequestFeeder
{
    public abstract Request getRequest();

    public abstract Request getRequest(HttpHost paramHttpHost);

    public abstract boolean haveRequest(HttpHost paramHttpHost);

    public abstract void requeueRequest(Request paramRequest);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.http.RequestFeeder
 * JD-Core Version:        0.6.2
 */