package android.net.http;

import android.content.Context;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.cert.X509Certificate;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.harmony.xnet.provider.jsse.ClientSessionContext;
import org.apache.harmony.xnet.provider.jsse.FileClientSessionCache;
import org.apache.harmony.xnet.provider.jsse.OpenSSLContextImpl;
import org.apache.harmony.xnet.provider.jsse.SSLClientSessionCache;
import org.apache.http.HttpHost;

public class HttpsConnection extends Connection
{
    private static SSLSocketFactory mSslSocketFactory = null;
    private boolean mAborted = false;
    private HttpHost mProxyHost;
    private Object mSuspendLock = new Object();
    private boolean mSuspended = false;

    static
    {
        initializeEngine(null);
    }

    HttpsConnection(Context paramContext, HttpHost paramHttpHost1, HttpHost paramHttpHost2, RequestFeeder paramRequestFeeder)
    {
        super(paramContext, paramHttpHost1, paramRequestFeeder);
        this.mProxyHost = paramHttpHost2;
    }

    /** @deprecated */
    private static SSLSocketFactory getSocketFactory()
    {
        try
        {
            SSLSocketFactory localSSLSocketFactory = mSslSocketFactory;
            return localSSLSocketFactory;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public static void initializeEngine(File paramFile)
    {
        SSLClientSessionCache localSSLClientSessionCache = null;
        if (paramFile != null);
        try
        {
            Log.d("HttpsConnection", "Caching SSL sessions in " + paramFile + ".");
            localSSLClientSessionCache = FileClientSessionCache.usingDirectory(paramFile);
            OpenSSLContextImpl localOpenSSLContextImpl = new OpenSSLContextImpl();
            TrustManager[] arrayOfTrustManager = new TrustManager[1];
            arrayOfTrustManager[0] = new X509TrustManager()
            {
                public void checkClientTrusted(X509Certificate[] paramAnonymousArrayOfX509Certificate, String paramAnonymousString)
                {
                }

                public void checkServerTrusted(X509Certificate[] paramAnonymousArrayOfX509Certificate, String paramAnonymousString)
                {
                }

                public X509Certificate[] getAcceptedIssuers()
                {
                    return null;
                }
            };
            localOpenSSLContextImpl.engineInit(null, arrayOfTrustManager, null);
            localOpenSSLContextImpl.engineGetClientSessionContext().setPersistentCache(localSSLClientSessionCache);
            try
            {
                mSslSocketFactory = localOpenSSLContextImpl.engineGetSocketFactory();
                return;
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
        }
        catch (KeyManagementException localKeyManagementException)
        {
            throw new RuntimeException(localKeyManagementException);
        }
        catch (IOException localIOException)
        {
            throw new RuntimeException(localIOException);
        }
    }

    void closeConnection()
    {
        if (this.mSuspended)
            restartConnection(false);
        try
        {
            if ((this.mHttpClientConnection != null) && (this.mHttpClientConnection.isOpen()))
                this.mHttpClientConnection.close();
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
                localIOException.printStackTrace();
        }
    }

    String getScheme()
    {
        return "https";
    }

    // ERROR //
    AndroidHttpClientConnection openConnection(Request paramRequest)
        throws IOException
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: aload_0
        //     3: getfield 40	android/net/http/HttpsConnection:mProxyHost	Lorg/apache/http/HttpHost;
        //     6: ifnull +595 -> 601
        //     9: aconst_null
        //     10: astore 19
        //     12: new 140	java/net/Socket
        //     15: dup
        //     16: aload_0
        //     17: getfield 40	android/net/http/HttpsConnection:mProxyHost	Lorg/apache/http/HttpHost;
        //     20: invokevirtual 145	org/apache/http/HttpHost:getHostName	()Ljava/lang/String;
        //     23: aload_0
        //     24: getfield 40	android/net/http/HttpsConnection:mProxyHost	Lorg/apache/http/HttpHost;
        //     27: invokevirtual 149	org/apache/http/HttpHost:getPort	()I
        //     30: invokespecial 152	java/net/Socket:<init>	(Ljava/lang/String;I)V
        //     33: astore 20
        //     35: aload 20
        //     37: ldc 153
        //     39: invokevirtual 157	java/net/Socket:setSoTimeout	(I)V
        //     42: new 117	android/net/http/AndroidHttpClientConnection
        //     45: dup
        //     46: invokespecial 158	android/net/http/AndroidHttpClientConnection:<init>	()V
        //     49: astore 24
        //     51: new 160	org/apache/http/params/BasicHttpParams
        //     54: dup
        //     55: invokespecial 161	org/apache/http/params/BasicHttpParams:<init>	()V
        //     58: astore 25
        //     60: aload 25
        //     62: sipush 8192
        //     65: invokestatic 167	org/apache/http/params/HttpConnectionParams:setSocketBufferSize	(Lorg/apache/http/params/HttpParams;I)V
        //     68: aload 24
        //     70: aload 20
        //     72: aload 25
        //     74: invokevirtual 171	android/net/http/AndroidHttpClientConnection:bind	(Ljava/net/Socket;Lorg/apache/http/params/HttpParams;)V
        //     77: new 173	android/net/http/Headers
        //     80: dup
        //     81: invokespecial 174	android/net/http/Headers:<init>	()V
        //     84: astore 26
        //     86: new 176	org/apache/http/message/BasicHttpRequest
        //     89: dup
        //     90: ldc 178
        //     92: aload_0
        //     93: getfield 181	android/net/http/Connection:mHost	Lorg/apache/http/HttpHost;
        //     96: invokevirtual 184	org/apache/http/HttpHost:toHostString	()Ljava/lang/String;
        //     99: invokespecial 187	org/apache/http/message/BasicHttpRequest:<init>	(Ljava/lang/String;Ljava/lang/String;)V
        //     102: astore 27
        //     104: aload_1
        //     105: getfield 193	android/net/http/Request:mHttpRequest	Lorg/apache/http/message/BasicHttpRequest;
        //     108: invokevirtual 197	org/apache/http/message/BasicHttpRequest:getAllHeaders	()[Lorg/apache/http/Header;
        //     111: astore 37
        //     113: aload 37
        //     115: arraylength
        //     116: istore 38
        //     118: iconst_0
        //     119: istore 39
        //     121: iload 39
        //     123: iload 38
        //     125: if_icmpge +107 -> 232
        //     128: aload 37
        //     130: iload 39
        //     132: aaload
        //     133: astore 46
        //     135: aload 46
        //     137: invokeinterface 202 1 0
        //     142: invokevirtual 207	java/lang/String:toLowerCase	()Ljava/lang/String;
        //     145: astore 47
        //     147: aload 47
        //     149: ldc 209
        //     151: invokevirtual 213	java/lang/String:startsWith	(Ljava/lang/String;)Z
        //     154: ifne +23 -> 177
        //     157: aload 47
        //     159: ldc 215
        //     161: invokevirtual 219	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     164: ifne +13 -> 177
        //     167: aload 47
        //     169: ldc 221
        //     171: invokevirtual 219	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     174: ifeq +10 -> 184
        //     177: aload 27
        //     179: aload 46
        //     181: invokevirtual 225	org/apache/http/message/BasicHttpRequest:addHeader	(Lorg/apache/http/Header;)V
        //     184: iinc 39 1
        //     187: goto -66 -> 121
        //     190: astore 21
        //     192: aload 19
        //     194: ifnull +8 -> 202
        //     197: aload 19
        //     199: invokevirtual 124	android/net/http/AndroidHttpClientConnection:close	()V
        //     202: aload 21
        //     204: invokevirtual 228	java/io/IOException:getMessage	()Ljava/lang/String;
        //     207: astore 22
        //     209: aload 22
        //     211: ifnonnull +7 -> 218
        //     214: ldc 230
        //     216: astore 22
        //     218: new 46	java/io/IOException
        //     221: dup
        //     222: aload 22
        //     224: invokespecial 233	java/io/IOException:<init>	(Ljava/lang/String;)V
        //     227: astore 23
        //     229: aload 23
        //     231: athrow
        //     232: aload 24
        //     234: aload 27
        //     236: invokevirtual 237	android/net/http/AndroidHttpClientConnection:sendRequestHeader	(Lorg/apache/http/HttpRequest;)V
        //     239: aload 24
        //     241: invokevirtual 240	android/net/http/AndroidHttpClientConnection:flush	()V
        //     244: aload 24
        //     246: aload 26
        //     248: invokevirtual 244	android/net/http/AndroidHttpClientConnection:parseResponseHeader	(Landroid/net/http/Headers;)Lorg/apache/http/StatusLine;
        //     251: astore 40
        //     253: aload 40
        //     255: invokeinterface 249 1 0
        //     260: istore 41
        //     262: iload 41
        //     264: sipush 200
        //     267: if_icmplt -23 -> 244
        //     270: iload 41
        //     272: sipush 200
        //     275: if_icmpne +258 -> 533
        //     278: invokestatic 251	android/net/http/HttpsConnection:getSocketFactory	()Ljavax/net/ssl/SSLSocketFactory;
        //     281: aload 20
        //     283: aload_0
        //     284: getfield 181	android/net/http/Connection:mHost	Lorg/apache/http/HttpHost;
        //     287: invokevirtual 145	org/apache/http/HttpHost:getHostName	()Ljava/lang/String;
        //     290: aload_0
        //     291: getfield 181	android/net/http/Connection:mHost	Lorg/apache/http/HttpHost;
        //     294: invokevirtual 149	org/apache/http/HttpHost:getPort	()I
        //     297: iconst_1
        //     298: invokevirtual 257	javax/net/ssl/SSLSocketFactory:createSocket	(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;
        //     301: checkcast 259	javax/net/ssl/SSLSocket
        //     304: astore_2
        //     305: invokestatic 265	android/net/http/CertificateChainValidator:getInstance	()Landroid/net/http/CertificateChainValidator;
        //     308: astore 6
        //     310: aload_0
        //     311: getfield 181	android/net/http/Connection:mHost	Lorg/apache/http/HttpHost;
        //     314: invokevirtual 145	org/apache/http/HttpHost:getHostName	()Ljava/lang/String;
        //     317: astore 7
        //     319: aload 6
        //     321: aload_0
        //     322: aload_2
        //     323: aload 7
        //     325: invokevirtual 269	android/net/http/CertificateChainValidator:doHandshakeAndValidateServerCertificates	(Landroid/net/http/HttpsConnection;Ljavax/net/ssl/SSLSocket;Ljava/lang/String;)Landroid/net/http/SslError;
        //     328: astore 8
        //     330: aload 8
        //     332: ifnull +433 -> 765
        //     335: aload_0
        //     336: getfield 34	android/net/http/HttpsConnection:mSuspendLock	Ljava/lang/Object;
        //     339: astore 12
        //     341: aload 12
        //     343: monitorenter
        //     344: aload_0
        //     345: iconst_1
        //     346: putfield 36	android/net/http/HttpsConnection:mSuspended	Z
        //     349: aload 12
        //     351: monitorexit
        //     352: aload_1
        //     353: invokevirtual 273	android/net/http/Request:getEventHandler	()Landroid/net/http/EventHandler;
        //     356: aload 8
        //     358: invokeinterface 279 2 0
        //     363: ifne +318 -> 681
        //     366: new 46	java/io/IOException
        //     369: dup
        //     370: new 50	java/lang/StringBuilder
        //     373: dup
        //     374: invokespecial 51	java/lang/StringBuilder:<init>	()V
        //     377: ldc_w 281
        //     380: invokevirtual 57	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     383: aload 8
        //     385: invokevirtual 60	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     388: invokevirtual 66	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     391: invokespecial 233	java/io/IOException:<init>	(Ljava/lang/String;)V
        //     394: athrow
        //     395: astore 34
        //     397: aload 34
        //     399: invokevirtual 282	org/apache/http/ParseException:getMessage	()Ljava/lang/String;
        //     402: astore 35
        //     404: aload 35
        //     406: ifnonnull +8 -> 414
        //     409: ldc_w 284
        //     412: astore 35
        //     414: new 46	java/io/IOException
        //     417: dup
        //     418: aload 35
        //     420: invokespecial 233	java/io/IOException:<init>	(Ljava/lang/String;)V
        //     423: astore 36
        //     425: aload 36
        //     427: athrow
        //     428: astore 31
        //     430: aload 31
        //     432: invokevirtual 285	org/apache/http/HttpException:getMessage	()Ljava/lang/String;
        //     435: astore 32
        //     437: aload 32
        //     439: ifnonnull +8 -> 447
        //     442: ldc_w 284
        //     445: astore 32
        //     447: new 46	java/io/IOException
        //     450: dup
        //     451: aload 32
        //     453: invokespecial 233	java/io/IOException:<init>	(Ljava/lang/String;)V
        //     456: astore 33
        //     458: aload 33
        //     460: athrow
        //     461: astore 28
        //     463: aload 28
        //     465: invokevirtual 228	java/io/IOException:getMessage	()Ljava/lang/String;
        //     468: astore 29
        //     470: aload 29
        //     472: ifnonnull +8 -> 480
        //     475: ldc_w 284
        //     478: astore 29
        //     480: new 46	java/io/IOException
        //     483: dup
        //     484: aload 29
        //     486: invokespecial 233	java/io/IOException:<init>	(Ljava/lang/String;)V
        //     489: astore 30
        //     491: aload 30
        //     493: athrow
        //     494: astore 43
        //     496: iconst_0
        //     497: ifeq +5 -> 502
        //     500: aconst_null
        //     501: athrow
        //     502: aload 43
        //     504: invokevirtual 228	java/io/IOException:getMessage	()Ljava/lang/String;
        //     507: astore 44
        //     509: aload 44
        //     511: ifnonnull +8 -> 519
        //     514: ldc_w 287
        //     517: astore 44
        //     519: new 46	java/io/IOException
        //     522: dup
        //     523: aload 44
        //     525: invokespecial 233	java/io/IOException:<init>	(Ljava/lang/String;)V
        //     528: astore 45
        //     530: aload 45
        //     532: athrow
        //     533: aload 40
        //     535: invokeinterface 291 1 0
        //     540: astore 42
        //     542: aload_1
        //     543: getfield 295	android/net/http/Request:mEventHandler	Landroid/net/http/EventHandler;
        //     546: aload 42
        //     548: invokevirtual 300	org/apache/http/ProtocolVersion:getMajor	()I
        //     551: aload 42
        //     553: invokevirtual 303	org/apache/http/ProtocolVersion:getMinor	()I
        //     556: iload 41
        //     558: aload 40
        //     560: invokeinterface 306 1 0
        //     565: invokeinterface 310 5 0
        //     570: aload_1
        //     571: getfield 295	android/net/http/Request:mEventHandler	Landroid/net/http/EventHandler;
        //     574: aload 26
        //     576: invokeinterface 314 2 0
        //     581: aload_1
        //     582: getfield 295	android/net/http/Request:mEventHandler	Landroid/net/http/EventHandler;
        //     585: invokeinterface 317 1 0
        //     590: aload 24
        //     592: invokevirtual 124	android/net/http/AndroidHttpClientConnection:close	()V
        //     595: aconst_null
        //     596: astore 9
        //     598: aload 9
        //     600: areturn
        //     601: invokestatic 251	android/net/http/HttpsConnection:getSocketFactory	()Ljavax/net/ssl/SSLSocketFactory;
        //     604: aload_0
        //     605: getfield 181	android/net/http/Connection:mHost	Lorg/apache/http/HttpHost;
        //     608: invokevirtual 145	org/apache/http/HttpHost:getHostName	()Ljava/lang/String;
        //     611: aload_0
        //     612: getfield 181	android/net/http/Connection:mHost	Lorg/apache/http/HttpHost;
        //     615: invokevirtual 149	org/apache/http/HttpHost:getPort	()I
        //     618: invokevirtual 320	javax/net/ssl/SSLSocketFactory:createSocket	(Ljava/lang/String;I)Ljava/net/Socket;
        //     621: checkcast 259	javax/net/ssl/SSLSocket
        //     624: astore_2
        //     625: aload_2
        //     626: ldc 153
        //     628: invokevirtual 321	javax/net/ssl/SSLSocket:setSoTimeout	(I)V
        //     631: goto -326 -> 305
        //     634: astore_3
        //     635: aload_2
        //     636: ifnull +7 -> 643
        //     639: aload_2
        //     640: invokevirtual 322	javax/net/ssl/SSLSocket:close	()V
        //     643: aload_3
        //     644: invokevirtual 228	java/io/IOException:getMessage	()Ljava/lang/String;
        //     647: astore 4
        //     649: aload 4
        //     651: ifnonnull +8 -> 659
        //     654: ldc_w 287
        //     657: astore 4
        //     659: new 46	java/io/IOException
        //     662: dup
        //     663: aload 4
        //     665: invokespecial 233	java/io/IOException:<init>	(Ljava/lang/String;)V
        //     668: astore 5
        //     670: aload 5
        //     672: athrow
        //     673: astore 13
        //     675: aload 12
        //     677: monitorexit
        //     678: aload 13
        //     680: athrow
        //     681: aload_0
        //     682: getfield 34	android/net/http/HttpsConnection:mSuspendLock	Ljava/lang/Object;
        //     685: astore 14
        //     687: aload 14
        //     689: monitorenter
        //     690: aload_0
        //     691: getfield 36	android/net/http/HttpsConnection:mSuspended	Z
        //     694: istore 16
        //     696: iload 16
        //     698: ifeq +30 -> 728
        //     701: aload_0
        //     702: getfield 34	android/net/http/HttpsConnection:mSuspendLock	Ljava/lang/Object;
        //     705: ldc2_w 323
        //     708: invokevirtual 328	java/lang/Object:wait	(J)V
        //     711: aload_0
        //     712: getfield 36	android/net/http/HttpsConnection:mSuspended	Z
        //     715: ifeq +13 -> 728
        //     718: aload_0
        //     719: iconst_0
        //     720: putfield 36	android/net/http/HttpsConnection:mSuspended	Z
        //     723: aload_0
        //     724: iconst_1
        //     725: putfield 38	android/net/http/HttpsConnection:mAborted	Z
        //     728: aload_0
        //     729: getfield 38	android/net/http/HttpsConnection:mAborted	Z
        //     732: ifeq +30 -> 762
        //     735: aload_2
        //     736: invokevirtual 322	javax/net/ssl/SSLSocket:close	()V
        //     739: new 330	android/net/http/SSLConnectionClosedByUserException
        //     742: dup
        //     743: ldc_w 332
        //     746: invokespecial 333	android/net/http/SSLConnectionClosedByUserException:<init>	(Ljava/lang/String;)V
        //     749: astore 17
        //     751: aload 17
        //     753: athrow
        //     754: astore 15
        //     756: aload 14
        //     758: monitorexit
        //     759: aload 15
        //     761: athrow
        //     762: aload 14
        //     764: monitorexit
        //     765: new 117	android/net/http/AndroidHttpClientConnection
        //     768: dup
        //     769: invokespecial 158	android/net/http/AndroidHttpClientConnection:<init>	()V
        //     772: astore 9
        //     774: new 160	org/apache/http/params/BasicHttpParams
        //     777: dup
        //     778: invokespecial 161	org/apache/http/params/BasicHttpParams:<init>	()V
        //     781: astore 10
        //     783: aload 10
        //     785: ldc_w 335
        //     788: sipush 8192
        //     791: invokevirtual 339	org/apache/http/params/BasicHttpParams:setIntParameter	(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;
        //     794: pop
        //     795: aload 9
        //     797: aload_2
        //     798: aload 10
        //     800: invokevirtual 171	android/net/http/AndroidHttpClientConnection:bind	(Ljava/net/Socket;Lorg/apache/http/params/HttpParams;)V
        //     803: goto -205 -> 598
        //     806: astore 18
        //     808: goto -80 -> 728
        //     811: astore 21
        //     813: goto -621 -> 192
        //     816: astore 21
        //     818: aload 24
        //     820: astore 19
        //     822: goto -630 -> 192
        //
        // Exception table:
        //     from	to	target	type
        //     12	35	190	java/io/IOException
        //     86	184	395	org/apache/http/ParseException
        //     232	262	395	org/apache/http/ParseException
        //     86	184	428	org/apache/http/HttpException
        //     232	262	428	org/apache/http/HttpException
        //     86	184	461	java/io/IOException
        //     232	262	461	java/io/IOException
        //     278	305	494	java/io/IOException
        //     601	631	634	java/io/IOException
        //     344	352	673	finally
        //     675	678	673	finally
        //     690	696	754	finally
        //     701	728	754	finally
        //     728	759	754	finally
        //     762	765	754	finally
        //     701	728	806	java/lang/InterruptedException
        //     35	51	811	java/io/IOException
        //     51	77	816	java/io/IOException
    }

    void restartConnection(boolean paramBoolean)
    {
        boolean bool = false;
        synchronized (this.mSuspendLock)
        {
            if (this.mSuspended)
            {
                this.mSuspended = false;
                if (!paramBoolean)
                    bool = true;
                this.mAborted = bool;
                this.mSuspendLock.notify();
            }
            return;
        }
    }

    void setCertificate(SslCertificate paramSslCertificate)
    {
        this.mCertificate = paramSslCertificate;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.http.HttpsConnection
 * JD-Core Version:        0.6.2
 */