package android.net.http;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.net.CacheRequest;
import java.net.CacheResponse;
import java.net.ExtendedResponseCache;
import java.net.HttpURLConnection;
import java.net.ResponseCache;
import java.net.ResponseSource;
import java.net.URI;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import libcore.io.DiskLruCache;
import libcore.io.IoUtils;

public final class HttpResponseCache extends ResponseCache
    implements Closeable, ExtendedResponseCache
{
    private final libcore.net.http.HttpResponseCache delegate;

    private HttpResponseCache(File paramFile, long paramLong)
        throws IOException
    {
        this.delegate = new libcore.net.http.HttpResponseCache(paramFile, paramLong);
    }

    public static HttpResponseCache getInstalled()
    {
        ResponseCache localResponseCache = ResponseCache.getDefault();
        if ((localResponseCache instanceof HttpResponseCache));
        for (HttpResponseCache localHttpResponseCache = (HttpResponseCache)localResponseCache; ; localHttpResponseCache = null)
            return localHttpResponseCache;
    }

    public static HttpResponseCache install(File paramFile, long paramLong)
        throws IOException
    {
        Object localObject = getInstalled();
        if (localObject != null)
        {
            DiskLruCache localDiskLruCache = ((HttpResponseCache)localObject).delegate.getCache();
            if ((!localDiskLruCache.getDirectory().equals(paramFile)) || (localDiskLruCache.maxSize() != paramLong) || (localDiskLruCache.isClosed()));
        }
        while (true)
        {
            return localObject;
            IoUtils.closeQuietly((AutoCloseable)localObject);
            HttpResponseCache localHttpResponseCache = new HttpResponseCache(paramFile, paramLong);
            ResponseCache.setDefault(localHttpResponseCache);
            localObject = localHttpResponseCache;
        }
    }

    public void close()
        throws IOException
    {
        if (ResponseCache.getDefault() == this)
            ResponseCache.setDefault(null);
        this.delegate.getCache().close();
    }

    public void delete()
        throws IOException
    {
        if (ResponseCache.getDefault() == this)
            ResponseCache.setDefault(null);
        this.delegate.getCache().delete();
    }

    public void flush()
    {
        try
        {
            this.delegate.getCache().flush();
            label10: return;
        }
        catch (IOException localIOException)
        {
            break label10;
        }
    }

    public CacheResponse get(URI paramURI, String paramString, Map<String, List<String>> paramMap)
        throws IOException
    {
        return this.delegate.get(paramURI, paramString, paramMap);
    }

    public int getHitCount()
    {
        return this.delegate.getHitCount();
    }

    public int getNetworkCount()
    {
        return this.delegate.getNetworkCount();
    }

    public int getRequestCount()
    {
        return this.delegate.getRequestCount();
    }

    public long maxSize()
    {
        return this.delegate.getCache().maxSize();
    }

    public CacheRequest put(URI paramURI, URLConnection paramURLConnection)
        throws IOException
    {
        return this.delegate.put(paramURI, paramURLConnection);
    }

    public long size()
    {
        return this.delegate.getCache().size();
    }

    public void trackConditionalCacheHit()
    {
        this.delegate.trackConditionalCacheHit();
    }

    public void trackResponse(ResponseSource paramResponseSource)
    {
        this.delegate.trackResponse(paramResponseSource);
    }

    public void update(CacheResponse paramCacheResponse, HttpURLConnection paramHttpURLConnection)
    {
        this.delegate.update(paramCacheResponse, paramHttpURLConnection);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.http.HttpResponseCache
 * JD-Core Version:        0.6.2
 */