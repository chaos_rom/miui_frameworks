package android.net.http;

import android.net.ParseException;
import android.net.WebAddress;
import android.webkit.CookieManager;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import junit.framework.Assert;
import org.apache.commons.codec.binary.Base64;

public class RequestHandle
{
    private static final String AUTHORIZATION_HEADER = "Authorization";
    public static final int MAX_REDIRECT_COUNT = 16;
    private static final String PROXY_AUTHORIZATION_HEADER = "Proxy-Authorization";
    private int mBodyLength;
    private InputStream mBodyProvider;
    private Connection mConnection;
    private Map<String, String> mHeaders;
    private String mMethod;
    private int mRedirectCount = 0;
    private Request mRequest;
    private RequestQueue mRequestQueue;
    private WebAddress mUri;
    private String mUrl;

    public RequestHandle(RequestQueue paramRequestQueue, String paramString1, WebAddress paramWebAddress, String paramString2, Map<String, String> paramMap, InputStream paramInputStream, int paramInt, Request paramRequest)
    {
        if (paramMap == null)
            paramMap = new HashMap();
        this.mHeaders = paramMap;
        this.mBodyProvider = paramInputStream;
        this.mBodyLength = paramInt;
        if (paramString2 == null)
            paramString2 = "GET";
        this.mMethod = paramString2;
        this.mUrl = paramString1;
        this.mUri = paramWebAddress;
        this.mRequestQueue = paramRequestQueue;
        this.mRequest = paramRequest;
    }

    public RequestHandle(RequestQueue paramRequestQueue, String paramString1, WebAddress paramWebAddress, String paramString2, Map<String, String> paramMap, InputStream paramInputStream, int paramInt, Request paramRequest, Connection paramConnection)
    {
        this(paramRequestQueue, paramString1, paramWebAddress, paramString2, paramMap, paramInputStream, paramInt, paramRequest);
        this.mConnection = paramConnection;
    }

    private String H(String paramString)
    {
        if (paramString != null);
        while (true)
        {
            try
            {
                byte[] arrayOfByte = MessageDigest.getInstance("MD5").digest(paramString.getBytes());
                if (arrayOfByte != null)
                {
                    String str2 = bufferToHex(arrayOfByte);
                    str1 = str2;
                    return str1;
                }
            }
            catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
            {
                throw new RuntimeException(localNoSuchAlgorithmException);
            }
            String str1 = null;
        }
    }

    private String KD(String paramString1, String paramString2)
    {
        return H(paramString1 + ":" + paramString2);
    }

    public static String authorizationHeader(boolean paramBoolean)
    {
        if (!paramBoolean);
        for (String str = "Authorization"; ; str = "Proxy-Authorization")
            return str;
    }

    private String bufferToHex(byte[] paramArrayOfByte)
    {
        char[] arrayOfChar = new char[16];
        arrayOfChar[0] = 48;
        arrayOfChar[1] = 49;
        arrayOfChar[2] = 50;
        arrayOfChar[3] = 51;
        arrayOfChar[4] = 52;
        arrayOfChar[5] = 53;
        arrayOfChar[6] = 54;
        arrayOfChar[7] = 55;
        arrayOfChar[8] = 56;
        arrayOfChar[9] = 57;
        arrayOfChar[10] = 97;
        arrayOfChar[11] = 98;
        arrayOfChar[12] = 99;
        arrayOfChar[13] = 100;
        arrayOfChar[14] = 101;
        arrayOfChar[15] = 102;
        String str;
        if (paramArrayOfByte != null)
        {
            int i = paramArrayOfByte.length;
            if (i > 0)
            {
                StringBuilder localStringBuilder = new StringBuilder(i * 2);
                for (int j = 0; j < i; j++)
                {
                    int k = (byte)(0xF & paramArrayOfByte[j]);
                    localStringBuilder.append(arrayOfChar[((byte)((0xF0 & paramArrayOfByte[j]) >> 4))]);
                    localStringBuilder.append(arrayOfChar[k]);
                }
                str = localStringBuilder.toString();
            }
        }
        while (true)
        {
            return str;
            str = "";
            continue;
            str = null;
        }
    }

    public static String computeBasicAuthResponse(String paramString1, String paramString2)
    {
        Assert.assertNotNull(paramString1);
        Assert.assertNotNull(paramString2);
        return new String(Base64.encodeBase64((paramString1 + ':' + paramString2).getBytes()));
    }

    private String computeCnonce()
    {
        int i = new Random().nextInt();
        if (i == -2147483648);
        for (int j = 2147483647; ; j = Math.abs(i))
            return Integer.toString(j, 16);
    }

    private String computeDigest(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6)
    {
        String str;
        if (paramString4 == null)
            str = KD(H(paramString1), paramString3 + ":" + H(paramString2));
        while (true)
        {
            return str;
            if (paramString4.equalsIgnoreCase("auth"))
                str = KD(H(paramString1), paramString3 + ":" + paramString5 + ":" + paramString6 + ":" + paramString4 + ":" + H(paramString2));
            else
                str = null;
        }
    }

    private String computeDigestAuthResponse(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7)
    {
        Assert.assertNotNull(paramString1);
        Assert.assertNotNull(paramString2);
        Assert.assertNotNull(paramString3);
        String str1 = paramString1 + ":" + paramString3 + ":" + paramString2;
        String str2 = this.mMethod + ":" + this.mUrl;
        String str3 = computeCnonce();
        String str4 = computeDigest(str1, str2, paramString4, paramString5, "00000001", str3);
        String str5 = "" + "username=" + doubleQuote(paramString1) + ", ";
        String str6 = str5 + "realm=" + doubleQuote(paramString3) + ", ";
        String str7 = str6 + "nonce=" + doubleQuote(paramString4) + ", ";
        String str8 = str7 + "uri=" + doubleQuote(this.mUrl) + ", ";
        String str9 = str8 + "response=" + doubleQuote(str4);
        if (paramString7 != null)
            str9 = str9 + ", opaque=" + doubleQuote(paramString7);
        if (paramString6 != null)
            str9 = str9 + ", algorithm=" + paramString6;
        if (paramString5 != null)
            str9 = str9 + ", qop=" + paramString5 + ", nc=" + "00000001" + ", cnonce=" + doubleQuote(str3);
        return str9;
    }

    private void createAndQueueNewRequest()
    {
        if (this.mConnection != null)
        {
            RequestHandle localRequestHandle = this.mRequestQueue.queueSynchronousRequest(this.mUrl, this.mUri, this.mMethod, this.mHeaders, this.mRequest.mEventHandler, this.mBodyProvider, this.mBodyLength);
            this.mRequest = localRequestHandle.mRequest;
            this.mConnection = localRequestHandle.mConnection;
            localRequestHandle.processRequest();
        }
        while (true)
        {
            return;
            this.mRequest = this.mRequestQueue.queueRequest(this.mUrl, this.mUri, this.mMethod, this.mHeaders, this.mRequest.mEventHandler, this.mBodyProvider, this.mBodyLength).mRequest;
        }
    }

    private String doubleQuote(String paramString)
    {
        if (paramString != null);
        for (String str = "\"" + paramString + "\""; ; str = null)
            return str;
    }

    private void setupAuthResponse()
    {
        try
        {
            if (this.mBodyProvider != null)
                this.mBodyProvider.reset();
            label14: createAndQueueNewRequest();
            return;
        }
        catch (IOException localIOException)
        {
            break label14;
        }
    }

    public void cancel()
    {
        if (this.mRequest != null)
            this.mRequest.cancel();
    }

    public String getMethod()
    {
        return this.mMethod;
    }

    public int getRedirectCount()
    {
        return this.mRedirectCount;
    }

    public void handleSslErrorResponse(boolean paramBoolean)
    {
        if (this.mRequest != null)
            this.mRequest.handleSslErrorResponse(paramBoolean);
    }

    public boolean isRedirectMax()
    {
        if (this.mRedirectCount >= 16);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void pauseRequest(boolean paramBoolean)
    {
        if (this.mRequest != null)
            this.mRequest.setLoadingPaused(paramBoolean);
    }

    public void processRequest()
    {
        if (this.mConnection != null)
            this.mConnection.processRequests(this.mRequest);
    }

    public void setRedirectCount(int paramInt)
    {
        this.mRedirectCount = paramInt;
    }

    public void setupBasicAuthResponse(boolean paramBoolean, String paramString1, String paramString2)
    {
        String str = computeBasicAuthResponse(paramString1, paramString2);
        this.mHeaders.put(authorizationHeader(paramBoolean), "Basic " + str);
        setupAuthResponse();
    }

    public void setupDigestAuthResponse(boolean paramBoolean, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7)
    {
        String str = computeDigestAuthResponse(paramString1, paramString2, paramString3, paramString4, paramString5, paramString6, paramString7);
        this.mHeaders.put(authorizationHeader(paramBoolean), "Digest " + str);
        setupAuthResponse();
    }

    public boolean setupRedirect(String paramString, int paramInt, Map<String, String> paramMap)
    {
        boolean bool = false;
        this.mHeaders.remove("Authorization");
        this.mHeaders.remove("Proxy-Authorization");
        int i = 1 + this.mRedirectCount;
        this.mRedirectCount = i;
        if (i == 16)
        {
            this.mRequest.error(-9, 17039642);
            return bool;
        }
        if ((this.mUrl.startsWith("https:")) && (paramString.startsWith("http:")))
            this.mHeaders.remove("Referer");
        this.mUrl = paramString;
        while (true)
        {
            try
            {
                this.mUri = new WebAddress(this.mUrl);
                this.mHeaders.remove("Cookie");
                String str = CookieManager.getInstance().getCookie(this.mUri);
                if ((str != null) && (str.length() > 0))
                    this.mHeaders.put("Cookie", str);
                if (((paramInt == 302) || (paramInt == 303)) && (this.mMethod.equals("POST")))
                    this.mMethod = "GET";
                if (paramInt != 307)
                    break label261;
            }
            catch (ParseException localParseException)
            {
                try
                {
                    if (this.mBodyProvider != null)
                        this.mBodyProvider.reset();
                    this.mHeaders.putAll(paramMap);
                    createAndQueueNewRequest();
                    bool = true;
                    break;
                    localParseException = localParseException;
                    localParseException.printStackTrace();
                }
                catch (IOException localIOException)
                {
                }
            }
            break;
            label261: this.mHeaders.remove("Content-Type");
            this.mBodyProvider = null;
        }
    }

    public void waitUntilComplete()
    {
        this.mRequest.waitUntilComplete();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.http.RequestHandle
 * JD-Core Version:        0.6.2
 */