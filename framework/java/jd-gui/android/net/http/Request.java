package android.net.http;

import android.content.Context;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.ParseException;
import org.apache.http.ProtocolVersion;
import org.apache.http.RequestLine;
import org.apache.http.StatusLine;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;
import org.apache.http.message.BasicHttpRequest;
import org.apache.http.protocol.RequestContent;

class Request
{
    private static final String ACCEPT_ENCODING_HEADER = "Accept-Encoding";
    private static final String CONTENT_LENGTH_HEADER = "content-length";
    private static final String HOST_HEADER = "Host";
    private static RequestContent requestContentProcessor = new RequestContent();
    private int mBodyLength;
    private InputStream mBodyProvider;
    volatile boolean mCancelled = false;
    private final Object mClientResource = new Object();
    private Connection mConnection;
    EventHandler mEventHandler;
    int mFailCount = 0;
    HttpHost mHost;
    BasicHttpRequest mHttpRequest;
    private boolean mLoadingPaused = false;
    String mPath;
    HttpHost mProxyHost;
    private int mReceivedBytes = 0;

    Request(String paramString1, HttpHost paramHttpHost1, HttpHost paramHttpHost2, String paramString2, InputStream paramInputStream, int paramInt, EventHandler paramEventHandler, Map<String, String> paramMap)
    {
        this.mEventHandler = paramEventHandler;
        this.mHost = paramHttpHost1;
        this.mProxyHost = paramHttpHost2;
        this.mPath = paramString2;
        this.mBodyProvider = paramInputStream;
        this.mBodyLength = paramInt;
        if ((paramInputStream == null) && (!"POST".equalsIgnoreCase(paramString1)))
            this.mHttpRequest = new BasicHttpRequest(paramString1, getUri());
        while (true)
        {
            addHeader("Host", getHostPort());
            addHeader("Accept-Encoding", "gzip");
            addHeaders(paramMap);
            return;
            this.mHttpRequest = new BasicHttpEntityEnclosingRequest(paramString1, getUri());
            if (paramInputStream != null)
                setBodyProvider(paramInputStream, paramInt);
        }
    }

    private static boolean canResponseHaveBody(HttpRequest paramHttpRequest, int paramInt)
    {
        boolean bool = false;
        if ("HEAD".equalsIgnoreCase(paramHttpRequest.getRequestLine().getMethod()));
        while (true)
        {
            return bool;
            if ((paramInt >= 200) && (paramInt != 204) && (paramInt != 304))
                bool = true;
        }
    }

    private void setBodyProvider(InputStream paramInputStream, int paramInt)
    {
        if (!paramInputStream.markSupported())
            throw new IllegalArgumentException("bodyProvider must support mark()");
        paramInputStream.mark(2147483647);
        ((BasicHttpEntityEnclosingRequest)this.mHttpRequest).setEntity(new InputStreamEntity(paramInputStream, paramInt));
    }

    void addHeader(String paramString1, String paramString2)
    {
        if (paramString1 == null)
        {
            HttpLog.e("Null http header name");
            throw new NullPointerException("Null http header name");
        }
        if ((paramString2 == null) || (paramString2.length() == 0))
        {
            String str = "Null or empty value for header \"" + paramString1 + "\"";
            HttpLog.e(str);
            throw new RuntimeException(str);
        }
        this.mHttpRequest.addHeader(paramString1, paramString2);
    }

    void addHeaders(Map<String, String> paramMap)
    {
        if (paramMap == null);
        while (true)
        {
            return;
            Iterator localIterator = paramMap.entrySet().iterator();
            while (localIterator.hasNext())
            {
                Map.Entry localEntry = (Map.Entry)localIterator.next();
                addHeader((String)localEntry.getKey(), (String)localEntry.getValue());
            }
        }
    }

    /** @deprecated */
    void cancel()
    {
        try
        {
            this.mLoadingPaused = false;
            notify();
            this.mCancelled = true;
            if (this.mConnection != null)
                this.mConnection.cancel();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    void complete()
    {
        synchronized (this.mClientResource)
        {
            this.mClientResource.notifyAll();
            return;
        }
    }

    void error(int paramInt1, int paramInt2)
    {
        this.mEventHandler.error(paramInt1, this.mConnection.mContext.getText(paramInt2).toString());
    }

    EventHandler getEventHandler()
    {
        return this.mEventHandler;
    }

    String getHostPort()
    {
        String str1 = this.mHost.getSchemeName();
        int i = this.mHost.getPort();
        if (((i != 80) && (str1.equals("http"))) || ((i != 443) && (str1.equals("https"))));
        for (String str2 = this.mHost.toHostString(); ; str2 = this.mHost.getHostName())
            return str2;
    }

    String getUri()
    {
        if ((this.mProxyHost == null) || (this.mHost.getSchemeName().equals("https")));
        for (String str = this.mPath; ; str = this.mHost.getSchemeName() + "://" + getHostPort() + this.mPath)
            return str;
    }

    public void handleSslErrorResponse(boolean paramBoolean)
    {
        HttpsConnection localHttpsConnection = (HttpsConnection)this.mConnection;
        if (localHttpsConnection != null)
            localHttpsConnection.restartConnection(paramBoolean);
    }

    void readResponse(AndroidHttpClientConnection paramAndroidHttpClientConnection)
        throws IOException, ParseException
    {
        if (this.mCancelled)
            return;
        paramAndroidHttpClientConnection.flush();
        Headers localHeaders = new Headers();
        StatusLine localStatusLine;
        int i;
        do
        {
            localStatusLine = paramAndroidHttpClientConnection.parseResponseHeader(localHeaders);
            i = localStatusLine.getStatusCode();
        }
        while (i < 200);
        ProtocolVersion localProtocolVersion = localStatusLine.getProtocolVersion();
        this.mEventHandler.status(localProtocolVersion.getMajor(), localProtocolVersion.getMinor(), i, localStatusLine.getReasonPhrase());
        this.mEventHandler.headers(localHeaders);
        HttpEntity localHttpEntity = null;
        if (canResponseHaveBody(this.mHttpRequest, i))
            localHttpEntity = paramAndroidHttpClientConnection.receiveResponseEntity(localHeaders);
        boolean bool1 = "bytes".equalsIgnoreCase(localHeaders.getAcceptRanges());
        InputStream localInputStream;
        Header localHeader;
        Object localObject1;
        byte[] arrayOfByte;
        int j;
        if (localHttpEntity != null)
        {
            localInputStream = localHttpEntity.getContent();
            localHeader = localHttpEntity.getContentEncoding();
            localObject1 = null;
            arrayOfByte = null;
            j = 0;
            if (localHeader == null)
                break label338;
        }
        while (true)
        {
            int k;
            int m;
            try
            {
                if (localHeader.getValue().equals("gzip"))
                {
                    GZIPInputStream localGZIPInputStream = new GZIPInputStream(localInputStream);
                    localObject1 = localGZIPInputStream;
                    arrayOfByte = this.mConnection.getBuf();
                    k = 0;
                    m = arrayOfByte.length / 2;
                    if (k == -1)
                        continue;
                    try
                    {
                        boolean bool2 = this.mLoadingPaused;
                        if (bool2)
                        {
                            try
                            {
                                wait();
                            }
                            catch (InterruptedException localInterruptedException)
                            {
                                HttpLog.e("Interrupted exception whilst network thread paused at WebCore's request. " + localInterruptedException.getMessage());
                            }
                            continue;
                        }
                    }
                    finally
                    {
                    }
                }
            }
            catch (EOFException localEOFException)
            {
                if (j > 0)
                    this.mEventHandler.data(arrayOfByte, j);
                if (localObject1 != null)
                    ((InputStream)localObject1).close();
                this.mConnection.setCanPersist(localHttpEntity, localStatusLine.getProtocolVersion(), localHeaders.getConnectionType());
                this.mEventHandler.endData();
                complete();
                break;
                localObject1 = localInputStream;
                continue;
                k = ((InputStream)localObject1).read(arrayOfByte, j, arrayOfByte.length - j);
                if (k == -1)
                    break label501;
                j += k;
                if (!bool1)
                    break label501;
                this.mReceivedBytes = (k + this.mReceivedBytes);
                break label501;
                this.mEventHandler.data(arrayOfByte, j);
                j = 0;
                continue;
                if (localObject1 == null)
                    continue;
                ((InputStream)localObject1).close();
                continue;
            }
            catch (IOException localIOException)
            {
                label338: if ((i == 200) || (i == 206))
                {
                    if ((bool1) && (j > 0))
                        this.mEventHandler.data(arrayOfByte, j);
                    throw localIOException;
                }
            }
            finally
            {
                if (localObject1 != null)
                    ((InputStream)localObject1).close();
            }
            if (localObject1 != null)
            {
                ((InputStream)localObject1).close();
                continue;
                label501: if (k != -1)
                    if (j < m);
            }
        }
    }

    void reset()
    {
        this.mHttpRequest.removeHeaders("content-length");
        if (this.mBodyProvider != null);
        try
        {
            this.mBodyProvider.reset();
            label23: setBodyProvider(this.mBodyProvider, this.mBodyLength);
            if (this.mReceivedBytes > 0)
            {
                this.mFailCount = 0;
                HttpLog.v("*** Request.reset() to range:" + this.mReceivedBytes);
                this.mHttpRequest.setHeader("Range", "bytes=" + this.mReceivedBytes + "-");
            }
            return;
        }
        catch (IOException localIOException)
        {
            break label23;
        }
    }

    void sendRequest(AndroidHttpClientConnection paramAndroidHttpClientConnection)
        throws HttpException, IOException
    {
        if (this.mCancelled);
        while (true)
        {
            return;
            requestContentProcessor.process(this.mHttpRequest, this.mConnection.getHttpContext());
            paramAndroidHttpClientConnection.sendRequestHeader(this.mHttpRequest);
            if ((this.mHttpRequest instanceof HttpEntityEnclosingRequest))
                paramAndroidHttpClientConnection.sendRequestEntity((HttpEntityEnclosingRequest)this.mHttpRequest);
        }
    }

    void setConnection(Connection paramConnection)
    {
        this.mConnection = paramConnection;
    }

    /** @deprecated */
    void setLoadingPaused(boolean paramBoolean)
    {
        try
        {
            this.mLoadingPaused = paramBoolean;
            if (!this.mLoadingPaused)
                notify();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public String toString()
    {
        return this.mPath;
    }

    void waitUntilComplete()
    {
        try
        {
            synchronized (this.mClientResource)
            {
                this.mClientResource.wait();
                label14: return;
            }
        }
        catch (InterruptedException localInterruptedException)
        {
            break label14;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.http.Request
 * JD-Core Version:        0.6.2
 */