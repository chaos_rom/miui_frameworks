package android.net.http;

import java.security.cert.X509Certificate;

public class SslError
{
    public static final int SSL_DATE_INVALID = 4;
    public static final int SSL_EXPIRED = 1;
    public static final int SSL_IDMISMATCH = 2;
    public static final int SSL_INVALID = 5;

    @Deprecated
    public static final int SSL_MAX_ERROR = 6;
    public static final int SSL_NOTYETVALID = 0;
    public static final int SSL_UNTRUSTED = 3;
    final SslCertificate mCertificate;
    int mErrors;
    final String mUrl;

    static
    {
        if (!SslError.class.desiredAssertionStatus());
        for (boolean bool = true; ; bool = false)
        {
            $assertionsDisabled = bool;
            return;
        }
    }

    @Deprecated
    public SslError(int paramInt, SslCertificate paramSslCertificate)
    {
        this(paramInt, paramSslCertificate, "");
    }

    public SslError(int paramInt, SslCertificate paramSslCertificate, String paramString)
    {
        assert (paramSslCertificate != null);
        assert (paramString != null);
        addError(paramInt);
        this.mCertificate = paramSslCertificate;
        this.mUrl = paramString;
    }

    @Deprecated
    public SslError(int paramInt, X509Certificate paramX509Certificate)
    {
        this(paramInt, paramX509Certificate, "");
    }

    public SslError(int paramInt, X509Certificate paramX509Certificate, String paramString)
    {
        this(paramInt, new SslCertificate(paramX509Certificate), paramString);
    }

    public static SslError SslErrorFromChromiumErrorCode(int paramInt, SslCertificate paramSslCertificate, String paramString)
    {
        assert ((paramInt >= -299) && (paramInt <= -200));
        SslError localSslError;
        if (paramInt == -200)
            localSslError = new SslError(2, paramSslCertificate, paramString);
        while (true)
        {
            return localSslError;
            if (paramInt == -201)
                localSslError = new SslError(4, paramSslCertificate, paramString);
            else if (paramInt == -202)
                localSslError = new SslError(3, paramSslCertificate, paramString);
            else
                localSslError = new SslError(5, paramSslCertificate, paramString);
        }
    }

    public boolean addError(int paramInt)
    {
        if ((paramInt >= 0) && (paramInt < 6));
        for (boolean bool = true; ; bool = false)
        {
            if (bool)
                this.mErrors |= 1 << paramInt;
            return bool;
        }
    }

    public SslCertificate getCertificate()
    {
        return this.mCertificate;
    }

    public int getPrimaryError()
    {
        int i;
        if (this.mErrors != 0)
        {
            i = 5;
            if (i >= 0)
                if ((this.mErrors & 1 << i) == 0);
        }
        while (true)
        {
            return i;
            i--;
            break;
            if (!$assertionsDisabled)
                throw new AssertionError();
            i = -1;
        }
    }

    public String getUrl()
    {
        return this.mUrl;
    }

    public boolean hasError(int paramInt)
    {
        if ((paramInt >= 0) && (paramInt < 6))
        {
            bool = true;
            if (bool)
                if ((this.mErrors & 1 << paramInt) == 0)
                    break label36;
        }
        label36: for (boolean bool = true; ; bool = false)
        {
            return bool;
            bool = false;
            break;
        }
    }

    public String toString()
    {
        return "primary error: " + getPrimaryError() + " certificate: " + getCertificate() + " on URL: " + getUrl();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.http.SslError
 * JD-Core Version:        0.6.2
 */