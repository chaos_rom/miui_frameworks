package android.net.http;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import javax.net.ssl.DefaultHostnameVerifier;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.X509TrustManager;
import org.apache.harmony.security.provider.cert.X509CertImpl;
import org.apache.harmony.xnet.provider.jsse.SSLParametersImpl;
import org.apache.harmony.xnet.provider.jsse.TrustManagerImpl;

public class CertificateChainValidator
{
    private static final CertificateChainValidator sInstance = new CertificateChainValidator();
    private static final DefaultHostnameVerifier sVerifier = new DefaultHostnameVerifier();

    private void closeSocketThrowException(SSLSocket paramSSLSocket, String paramString)
        throws IOException
    {
        if (paramSSLSocket != null)
        {
            SSLSession localSSLSession = paramSSLSocket.getSession();
            if (localSSLSession != null)
                localSSLSession.invalidate();
            paramSSLSocket.close();
        }
        throw new SSLHandshakeException(paramString);
    }

    private void closeSocketThrowException(SSLSocket paramSSLSocket, String paramString1, String paramString2)
        throws IOException
    {
        if (paramString1 != null);
        while (true)
        {
            closeSocketThrowException(paramSSLSocket, paramString1);
            return;
            paramString1 = paramString2;
        }
    }

    public static CertificateChainValidator getInstance()
    {
        return sInstance;
    }

    public static void handleTrustStorageUpdate()
    {
        try
        {
            X509TrustManager localX509TrustManager = SSLParametersImpl.getDefaultTrustManager();
            if ((localX509TrustManager instanceof TrustManagerImpl))
                ((TrustManagerImpl)localX509TrustManager).handleTrustStorageUpdate();
            label18: return;
        }
        catch (KeyManagementException localKeyManagementException)
        {
            break label18;
        }
    }

    public static SslError verifyServerCertificates(byte[][] paramArrayOfByte, String paramString1, String paramString2)
        throws IOException
    {
        if ((paramArrayOfByte == null) || (paramArrayOfByte.length == 0))
            throw new IllegalArgumentException("bad certificate chain");
        X509Certificate[] arrayOfX509Certificate = new X509Certificate[paramArrayOfByte.length];
        for (int i = 0; i < paramArrayOfByte.length; i++)
            arrayOfX509Certificate[i] = new X509CertImpl(paramArrayOfByte[i]);
        return verifyServerDomainAndCertificates(arrayOfX509Certificate, paramString1, paramString2);
    }

    private static SslError verifyServerDomainAndCertificates(X509Certificate[] paramArrayOfX509Certificate, String paramString1, String paramString2)
        throws IOException
    {
        int i = 0;
        X509Certificate localX509Certificate = paramArrayOfX509Certificate[0];
        if (localX509Certificate == null)
            throw new IllegalArgumentException("certificate for this site is null");
        if ((paramString1 != null) && (!paramString1.isEmpty()) && (sVerifier.verify(paramString1, localX509Certificate)))
            i = 1;
        SslError localSslError;
        if (i == 0)
            localSslError = new SslError(2, localX509Certificate);
        while (true)
        {
            return localSslError;
            try
            {
                SSLParametersImpl.getDefaultTrustManager().checkServerTrusted(paramArrayOfX509Certificate, paramString2);
                localSslError = null;
            }
            catch (GeneralSecurityException localGeneralSecurityException)
            {
                localSslError = new SslError(3, localX509Certificate);
            }
        }
    }

    public SslError doHandshakeAndValidateServerCertificates(HttpsConnection paramHttpsConnection, SSLSocket paramSSLSocket, String paramString)
        throws IOException
    {
        if (!paramSSLSocket.getSession().isValid())
            closeSocketThrowException(paramSSLSocket, "failed to perform SSL handshake");
        Certificate[] arrayOfCertificate = paramSSLSocket.getSession().getPeerCertificates();
        if ((arrayOfCertificate == null) || (arrayOfCertificate.length == 0))
            closeSocketThrowException(paramSSLSocket, "failed to retrieve peer certificates");
        while (true)
        {
            return verifyServerDomainAndCertificates((X509Certificate[])arrayOfCertificate, paramString, "RSA");
            if ((paramHttpsConnection != null) && (arrayOfCertificate[0] != null))
                paramHttpsConnection.setCertificate(new SslCertificate((X509Certificate)arrayOfCertificate[0]));
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.http.CertificateChainValidator
 * JD-Core Version:        0.6.2
 */