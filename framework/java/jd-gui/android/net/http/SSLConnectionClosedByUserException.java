package android.net.http;

import javax.net.ssl.SSLException;

class SSLConnectionClosedByUserException extends SSLException
{
    public SSLConnectionClosedByUserException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.http.SSLConnectionClosedByUserException
 * JD-Core Version:        0.6.2
 */