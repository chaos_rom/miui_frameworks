package android.net.http;

import android.os.SystemClock;
import org.apache.http.HttpHost;

class IdleCache
{
    private static final int CHECK_INTERVAL = 2000;
    private static final int EMPTY_CHECK_MAX = 5;
    private static final int IDLE_CACHE_MAX = 8;
    private static final int TIMEOUT = 6000;
    private int mCached = 0;
    private int mCount = 0;
    private Entry[] mEntries = new Entry[8];
    private int mReused = 0;
    private IdleReaper mThread = null;

    IdleCache()
    {
        for (int i = 0; i < 8; i++)
            this.mEntries[i] = new Entry();
    }

    /** @deprecated */
    private void clearIdle()
    {
        try
        {
            if (this.mCount > 0)
            {
                long l = SystemClock.uptimeMillis();
                for (int i = 0; i < 8; i++)
                {
                    Entry localEntry = this.mEntries[i];
                    if ((localEntry.mHost != null) && (l > localEntry.mTimeout))
                    {
                        localEntry.mHost = null;
                        localEntry.mConnection.closeConnection();
                        localEntry.mConnection = null;
                        this.mCount = (-1 + this.mCount);
                    }
                }
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    boolean cacheConnection(HttpHost paramHttpHost, Connection paramConnection)
    {
        boolean bool = false;
        try
        {
            int i;
            if (this.mCount < 8)
            {
                long l = SystemClock.uptimeMillis();
                i = 0;
                if (i < 8)
                {
                    Entry localEntry = this.mEntries[i];
                    if (localEntry.mHost != null)
                        break label111;
                    localEntry.mHost = paramHttpHost;
                    localEntry.mConnection = paramConnection;
                    localEntry.mTimeout = (6000L + l);
                    this.mCount = (1 + this.mCount);
                    bool = true;
                    if (this.mThread == null)
                    {
                        this.mThread = new IdleReaper(null);
                        this.mThread.start();
                    }
                }
            }
            return bool;
            label111: i++;
        }
        finally
        {
        }
    }

    /** @deprecated */
    void clear()
    {
        int i = 0;
        try
        {
            while ((this.mCount > 0) && (i < 8))
            {
                Entry localEntry = this.mEntries[i];
                if (localEntry.mHost != null)
                {
                    localEntry.mHost = null;
                    localEntry.mConnection.closeConnection();
                    localEntry.mConnection = null;
                    this.mCount = (-1 + this.mCount);
                }
                i++;
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    Connection getConnection(HttpHost paramHttpHost)
    {
        Connection localConnection = null;
        try
        {
            int i;
            if (this.mCount > 0)
            {
                i = 0;
                if (i < 8)
                {
                    Entry localEntry = this.mEntries[i];
                    HttpHost localHttpHost = localEntry.mHost;
                    if ((localHttpHost == null) || (!localHttpHost.equals(paramHttpHost)))
                        break label84;
                    localConnection = localEntry.mConnection;
                    localEntry.mHost = null;
                    localEntry.mConnection = null;
                    this.mCount = (-1 + this.mCount);
                }
            }
            return localConnection;
            label84: i++;
        }
        finally
        {
        }
    }

    private class IdleReaper extends Thread
    {
        private IdleReaper()
        {
        }

        // ERROR //
        public void run()
        {
            // Byte code:
            //     0: iconst_0
            //     1: istore_1
            //     2: aload_0
            //     3: ldc 23
            //     5: invokevirtual 27	android/net/http/IdleCache$IdleReaper:setName	(Ljava/lang/String;)V
            //     8: bipush 10
            //     10: invokestatic 33	android/os/Process:setThreadPriority	(I)V
            //     13: aload_0
            //     14: getfield 13	android/net/http/IdleCache$IdleReaper:this$0	Landroid/net/http/IdleCache;
            //     17: astore_2
            //     18: aload_2
            //     19: monitorenter
            //     20: iload_1
            //     21: iconst_5
            //     22: if_icmpge +48 -> 70
            //     25: aload_0
            //     26: getfield 13	android/net/http/IdleCache$IdleReaper:this$0	Landroid/net/http/IdleCache;
            //     29: ldc2_w 34
            //     32: invokevirtual 41	java/lang/Object:wait	(J)V
            //     35: aload_0
            //     36: getfield 13	android/net/http/IdleCache$IdleReaper:this$0	Landroid/net/http/IdleCache;
            //     39: invokestatic 45	android/net/http/IdleCache:access$100	(Landroid/net/http/IdleCache;)I
            //     42: ifne +9 -> 51
            //     45: iinc 1 1
            //     48: goto -28 -> 20
            //     51: iconst_0
            //     52: istore_1
            //     53: aload_0
            //     54: getfield 13	android/net/http/IdleCache$IdleReaper:this$0	Landroid/net/http/IdleCache;
            //     57: invokestatic 48	android/net/http/IdleCache:access$200	(Landroid/net/http/IdleCache;)V
            //     60: goto -40 -> 20
            //     63: astore 4
            //     65: aload_2
            //     66: monitorexit
            //     67: aload 4
            //     69: athrow
            //     70: aload_0
            //     71: getfield 13	android/net/http/IdleCache$IdleReaper:this$0	Landroid/net/http/IdleCache;
            //     74: aconst_null
            //     75: invokestatic 52	android/net/http/IdleCache:access$302	(Landroid/net/http/IdleCache;Landroid/net/http/IdleCache$IdleReaper;)Landroid/net/http/IdleCache$IdleReaper;
            //     78: pop
            //     79: aload_2
            //     80: monitorexit
            //     81: return
            //     82: astore 5
            //     84: goto -49 -> 35
            //
            // Exception table:
            //     from	to	target	type
            //     25	35	63	finally
            //     35	67	63	finally
            //     70	81	63	finally
            //     25	35	82	java/lang/InterruptedException
        }
    }

    class Entry
    {
        Connection mConnection;
        HttpHost mHost;
        long mTimeout;

        Entry()
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.http.IdleCache
 * JD-Core Version:        0.6.2
 */