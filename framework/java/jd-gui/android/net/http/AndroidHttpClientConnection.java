package android.net.http;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import org.apache.http.HttpConnection;
import org.apache.http.HttpConnectionMetrics;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpException;
import org.apache.http.HttpInetConnection;
import org.apache.http.HttpRequest;
import org.apache.http.NoHttpResponseException;
import org.apache.http.ParseException;
import org.apache.http.StatusLine;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.impl.HttpConnectionMetricsImpl;
import org.apache.http.impl.entity.EntitySerializer;
import org.apache.http.impl.entity.StrictContentLengthStrategy;
import org.apache.http.impl.io.ChunkedInputStream;
import org.apache.http.impl.io.ContentLengthInputStream;
import org.apache.http.impl.io.HttpRequestWriter;
import org.apache.http.impl.io.IdentityInputStream;
import org.apache.http.impl.io.SocketInputBuffer;
import org.apache.http.impl.io.SocketOutputBuffer;
import org.apache.http.io.HttpMessageWriter;
import org.apache.http.io.SessionInputBuffer;
import org.apache.http.io.SessionOutputBuffer;
import org.apache.http.message.BasicLineParser;
import org.apache.http.message.ParserCursor;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.CharArrayBuffer;

public class AndroidHttpClientConnection
    implements HttpInetConnection, HttpConnection
{
    private final EntitySerializer entityserializer = new EntitySerializer(new StrictContentLengthStrategy());
    private SessionInputBuffer inbuffer = null;
    private int maxHeaderCount;
    private int maxLineLength;
    private HttpConnectionMetricsImpl metrics = null;
    private volatile boolean open;
    private SessionOutputBuffer outbuffer = null;
    private HttpMessageWriter requestWriter = null;
    private Socket socket = null;

    private void assertNotOpen()
    {
        if (this.open)
            throw new IllegalStateException("Connection is already open");
    }

    private void assertOpen()
    {
        if (!this.open)
            throw new IllegalStateException("Connection is not open");
    }

    private long determineLength(Headers paramHeaders)
    {
        long l1 = paramHeaders.getTransferEncoding();
        if (l1 < 0L);
        while (true)
        {
            return l1;
            long l2 = paramHeaders.getContentLength();
            if (l2 > -1L)
                l1 = l2;
            else
                l1 = -1L;
        }
    }

    public void bind(Socket paramSocket, HttpParams paramHttpParams)
        throws IOException
    {
        if (paramSocket == null)
            throw new IllegalArgumentException("Socket may not be null");
        if (paramHttpParams == null)
            throw new IllegalArgumentException("HTTP parameters may not be null");
        assertNotOpen();
        paramSocket.setTcpNoDelay(HttpConnectionParams.getTcpNoDelay(paramHttpParams));
        paramSocket.setSoTimeout(HttpConnectionParams.getSoTimeout(paramHttpParams));
        int i = HttpConnectionParams.getLinger(paramHttpParams);
        if (i >= 0)
            if (i <= 0)
                break label192;
        label192: for (boolean bool = true; ; bool = false)
        {
            paramSocket.setSoLinger(bool, i);
            this.socket = paramSocket;
            int j = HttpConnectionParams.getSocketBufferSize(paramHttpParams);
            this.inbuffer = new SocketInputBuffer(paramSocket, j, paramHttpParams);
            this.outbuffer = new SocketOutputBuffer(paramSocket, j, paramHttpParams);
            this.maxHeaderCount = paramHttpParams.getIntParameter("http.connection.max-header-count", -1);
            this.maxLineLength = paramHttpParams.getIntParameter("http.connection.max-line-length", -1);
            this.requestWriter = new HttpRequestWriter(this.outbuffer, null, paramHttpParams);
            this.metrics = new HttpConnectionMetricsImpl(this.inbuffer.getMetrics(), this.outbuffer.getMetrics());
            this.open = true;
            return;
        }
    }

    // ERROR //
    public void close()
        throws IOException
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 52	android/net/http/AndroidHttpClientConnection:open	Z
        //     4: ifne +4 -> 8
        //     7: return
        //     8: aload_0
        //     9: iconst_0
        //     10: putfield 52	android/net/http/AndroidHttpClientConnection:open	Z
        //     13: aload_0
        //     14: invokevirtual 165	android/net/http/AndroidHttpClientConnection:doFlush	()V
        //     17: aload_0
        //     18: getfield 39	android/net/http/AndroidHttpClientConnection:socket	Ljava/net/Socket;
        //     21: invokevirtual 168	java/net/Socket:shutdownOutput	()V
        //     24: aload_0
        //     25: getfield 39	android/net/http/AndroidHttpClientConnection:socket	Ljava/net/Socket;
        //     28: invokevirtual 171	java/net/Socket:shutdownInput	()V
        //     31: aload_0
        //     32: getfield 39	android/net/http/AndroidHttpClientConnection:socket	Ljava/net/Socket;
        //     35: invokevirtual 173	java/net/Socket:close	()V
        //     38: goto -31 -> 7
        //     41: astore_3
        //     42: goto -11 -> 31
        //     45: astore_2
        //     46: goto -15 -> 31
        //     49: astore_1
        //     50: goto -26 -> 24
        //
        // Exception table:
        //     from	to	target	type
        //     17	24	41	java/lang/UnsupportedOperationException
        //     24	31	41	java/lang/UnsupportedOperationException
        //     24	31	45	java/io/IOException
        //     17	24	49	java/io/IOException
    }

    protected void doFlush()
        throws IOException
    {
        this.outbuffer.flush();
    }

    public void flush()
        throws IOException
    {
        assertOpen();
        doFlush();
    }

    public InetAddress getLocalAddress()
    {
        if (this.socket != null);
        for (InetAddress localInetAddress = this.socket.getLocalAddress(); ; localInetAddress = null)
            return localInetAddress;
    }

    public int getLocalPort()
    {
        if (this.socket != null);
        for (int i = this.socket.getLocalPort(); ; i = -1)
            return i;
    }

    public HttpConnectionMetrics getMetrics()
    {
        return this.metrics;
    }

    public InetAddress getRemoteAddress()
    {
        if (this.socket != null);
        for (InetAddress localInetAddress = this.socket.getInetAddress(); ; localInetAddress = null)
            return localInetAddress;
    }

    public int getRemotePort()
    {
        if (this.socket != null);
        for (int i = this.socket.getPort(); ; i = -1)
            return i;
    }

    public int getSocketTimeout()
    {
        int i = -1;
        if (this.socket != null);
        try
        {
            int j = this.socket.getSoTimeout();
            i = j;
            label20: return i;
        }
        catch (SocketException localSocketException)
        {
            break label20;
        }
    }

    public boolean isOpen()
    {
        if ((this.open) && (this.socket != null) && (this.socket.isConnected()));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isStale()
    {
        boolean bool = true;
        assertOpen();
        try
        {
            this.inbuffer.isDataAvailable(1);
            bool = false;
            label19: return bool;
        }
        catch (IOException localIOException)
        {
            break label19;
        }
    }

    public StatusLine parseResponseHeader(Headers paramHeaders)
        throws IOException, ParseException
    {
        assertOpen();
        CharArrayBuffer localCharArrayBuffer1 = new CharArrayBuffer(64);
        if (this.inbuffer.readLine(localCharArrayBuffer1) == -1)
            throw new NoHttpResponseException("The target server failed to respond");
        StatusLine localStatusLine = BasicLineParser.DEFAULT.parseStatusLine(localCharArrayBuffer1, new ParserCursor(0, localCharArrayBuffer1.length()));
        int i = localStatusLine.getStatusCode();
        CharArrayBuffer localCharArrayBuffer2 = null;
        int j = 0;
        while (true)
        {
            if (localCharArrayBuffer1 == null)
                localCharArrayBuffer1 = new CharArrayBuffer(64);
            while ((this.inbuffer.readLine(localCharArrayBuffer1) == -1) || (localCharArrayBuffer1.length() < 1))
            {
                if (localCharArrayBuffer2 != null)
                    paramHeaders.parseHeader(localCharArrayBuffer2);
                if (i >= 200)
                    this.metrics.incrementResponseCount();
                return localStatusLine;
                localCharArrayBuffer1.clear();
            }
            int k = localCharArrayBuffer1.charAt(0);
            if (((k == 32) || (k == 9)) && (localCharArrayBuffer2 != null))
            {
                int m = 0;
                int n = localCharArrayBuffer1.length();
                while (true)
                {
                    if (m < n)
                    {
                        int i1 = localCharArrayBuffer1.charAt(m);
                        if ((i1 == 32) || (i1 == 9));
                    }
                    else
                    {
                        if ((this.maxLineLength <= 0) || (1 + localCharArrayBuffer2.length() + localCharArrayBuffer1.length() - m <= this.maxLineLength))
                            break;
                        throw new IOException("Maximum line length limit exceeded");
                    }
                    m++;
                }
                localCharArrayBuffer2.append(' ');
                localCharArrayBuffer2.append(localCharArrayBuffer1, m, localCharArrayBuffer1.length() - m);
            }
            while ((this.maxHeaderCount > 0) && (j >= this.maxHeaderCount))
            {
                throw new IOException("Maximum header count exceeded");
                if (localCharArrayBuffer2 != null)
                    paramHeaders.parseHeader(localCharArrayBuffer2);
                j++;
                localCharArrayBuffer2 = localCharArrayBuffer1;
                localCharArrayBuffer1 = null;
            }
        }
    }

    public HttpEntity receiveResponseEntity(Headers paramHeaders)
    {
        assertOpen();
        BasicHttpEntity localBasicHttpEntity = new BasicHttpEntity();
        long l = determineLength(paramHeaders);
        if (l == -2L)
        {
            localBasicHttpEntity.setChunked(true);
            localBasicHttpEntity.setContentLength(-1L);
            localBasicHttpEntity.setContent(new ChunkedInputStream(this.inbuffer));
        }
        while (true)
        {
            String str1 = paramHeaders.getContentType();
            if (str1 != null)
                localBasicHttpEntity.setContentType(str1);
            String str2 = paramHeaders.getContentEncoding();
            if (str2 != null)
                localBasicHttpEntity.setContentEncoding(str2);
            return localBasicHttpEntity;
            if (l == -1L)
            {
                localBasicHttpEntity.setChunked(false);
                localBasicHttpEntity.setContentLength(-1L);
                localBasicHttpEntity.setContent(new IdentityInputStream(this.inbuffer));
            }
            else
            {
                localBasicHttpEntity.setChunked(false);
                localBasicHttpEntity.setContentLength(l);
                localBasicHttpEntity.setContent(new ContentLengthInputStream(this.inbuffer, l));
            }
        }
    }

    public void sendRequestEntity(HttpEntityEnclosingRequest paramHttpEntityEnclosingRequest)
        throws HttpException, IOException
    {
        if (paramHttpEntityEnclosingRequest == null)
            throw new IllegalArgumentException("HTTP request may not be null");
        assertOpen();
        if (paramHttpEntityEnclosingRequest.getEntity() == null);
        while (true)
        {
            return;
            this.entityserializer.serialize(this.outbuffer, paramHttpEntityEnclosingRequest, paramHttpEntityEnclosingRequest.getEntity());
        }
    }

    public void sendRequestHeader(HttpRequest paramHttpRequest)
        throws HttpException, IOException
    {
        if (paramHttpRequest == null)
            throw new IllegalArgumentException("HTTP request may not be null");
        assertOpen();
        this.requestWriter.write(paramHttpRequest);
        this.metrics.incrementRequestCount();
    }

    public void setSocketTimeout(int paramInt)
    {
        assertOpen();
        if (this.socket != null);
        try
        {
            this.socket.setSoTimeout(paramInt);
            label19: return;
        }
        catch (SocketException localSocketException)
        {
            break label19;
        }
    }

    public void shutdown()
        throws IOException
    {
        this.open = false;
        Socket localSocket = this.socket;
        if (localSocket != null)
            localSocket.close();
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append(getClass().getSimpleName()).append("[");
        if (isOpen())
            localStringBuilder.append(getRemotePort());
        while (true)
        {
            localStringBuilder.append("]");
            return localStringBuilder.toString();
            localStringBuilder.append("closed");
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.http.AndroidHttpClientConnection
 * JD-Core Version:        0.6.2
 */