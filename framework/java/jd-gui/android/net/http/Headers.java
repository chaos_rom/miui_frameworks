package android.net.http;

import java.util.ArrayList;
import org.apache.http.HeaderElement;
import org.apache.http.message.BasicHeaderValueParser;
import org.apache.http.message.ParserCursor;
import org.apache.http.util.CharArrayBuffer;

public final class Headers
{
    public static final String ACCEPT_RANGES = "accept-ranges";
    public static final String CACHE_CONTROL = "cache-control";
    public static final int CONN_CLOSE = 1;
    public static final String CONN_DIRECTIVE = "connection";
    public static final int CONN_KEEP_ALIVE = 2;
    public static final String CONTENT_DISPOSITION = "content-disposition";
    public static final String CONTENT_ENCODING = "content-encoding";
    public static final String CONTENT_LEN = "content-length";
    public static final String CONTENT_TYPE = "content-type";
    public static final String ETAG = "etag";
    public static final String EXPIRES = "expires";
    private static final int HASH_ACCEPT_RANGES = 1397189435;
    private static final int HASH_CACHE_CONTROL = -208775662;
    private static final int HASH_CONN_DIRECTIVE = -775651618;
    private static final int HASH_CONTENT_DISPOSITION = -1267267485;
    private static final int HASH_CONTENT_ENCODING = 2095084583;
    private static final int HASH_CONTENT_LEN = -1132779846;
    private static final int HASH_CONTENT_TYPE = 785670158;
    private static final int HASH_ETAG = 3123477;
    private static final int HASH_EXPIRES = -1309235404;
    private static final int HASH_LAST_MODIFIED = 150043680;
    private static final int HASH_LOCATION = 1901043637;
    private static final int HASH_PRAGMA = -980228804;
    private static final int HASH_PROXY_AUTHENTICATE = -301767724;
    private static final int HASH_PROXY_CONNECTION = 285929373;
    private static final int HASH_REFRESH = 1085444827;
    private static final int HASH_SET_COOKIE = 1237214767;
    private static final int HASH_TRANSFER_ENCODING = 1274458357;
    private static final int HASH_WWW_AUTHENTICATE = -243037365;
    private static final int HASH_X_PERMITTED_CROSS_DOMAIN_POLICIES = -1345594014;
    private static final int HEADER_COUNT = 19;
    private static final int IDX_ACCEPT_RANGES = 10;
    private static final int IDX_CACHE_CONTROL = 12;
    private static final int IDX_CONN_DIRECTIVE = 4;
    private static final int IDX_CONTENT_DISPOSITION = 9;
    private static final int IDX_CONTENT_ENCODING = 3;
    private static final int IDX_CONTENT_LEN = 1;
    private static final int IDX_CONTENT_TYPE = 2;
    private static final int IDX_ETAG = 14;
    private static final int IDX_EXPIRES = 11;
    private static final int IDX_LAST_MODIFIED = 13;
    private static final int IDX_LOCATION = 5;
    private static final int IDX_PRAGMA = 16;
    private static final int IDX_PROXY_AUTHENTICATE = 8;
    private static final int IDX_PROXY_CONNECTION = 6;
    private static final int IDX_REFRESH = 17;
    private static final int IDX_SET_COOKIE = 15;
    private static final int IDX_TRANSFER_ENCODING = 0;
    private static final int IDX_WWW_AUTHENTICATE = 7;
    private static final int IDX_X_PERMITTED_CROSS_DOMAIN_POLICIES = 18;
    public static final String LAST_MODIFIED = "last-modified";
    public static final String LOCATION = "location";
    private static final String LOGTAG = "Http";
    public static final int NO_CONN_TYPE = 0;
    public static final long NO_CONTENT_LENGTH = -1L;
    public static final long NO_TRANSFER_ENCODING = 0L;
    public static final String PRAGMA = "pragma";
    public static final String PROXY_AUTHENTICATE = "proxy-authenticate";
    public static final String PROXY_CONNECTION = "proxy-connection";
    public static final String REFRESH = "refresh";
    public static final String SET_COOKIE = "set-cookie";
    public static final String TRANSFER_ENCODING = "transfer-encoding";
    public static final String WWW_AUTHENTICATE = "www-authenticate";
    public static final String X_PERMITTED_CROSS_DOMAIN_POLICIES = "x-permitted-cross-domain-policies";
    private static final String[] sHeaderNames = arrayOfString;
    private int connectionType = 0;
    private long contentLength = -1L;
    private ArrayList<String> cookies = new ArrayList(2);
    private ArrayList<String> mExtraHeaderNames = new ArrayList(4);
    private ArrayList<String> mExtraHeaderValues = new ArrayList(4);
    private String[] mHeaders = new String[19];
    private long transferEncoding = 0L;

    static
    {
        String[] arrayOfString = new String[19];
        arrayOfString[0] = "transfer-encoding";
        arrayOfString[1] = "content-length";
        arrayOfString[2] = "content-type";
        arrayOfString[3] = "content-encoding";
        arrayOfString[4] = "connection";
        arrayOfString[5] = "location";
        arrayOfString[6] = "proxy-connection";
        arrayOfString[7] = "www-authenticate";
        arrayOfString[8] = "proxy-authenticate";
        arrayOfString[9] = "content-disposition";
        arrayOfString[10] = "accept-ranges";
        arrayOfString[11] = "expires";
        arrayOfString[12] = "cache-control";
        arrayOfString[13] = "last-modified";
        arrayOfString[14] = "etag";
        arrayOfString[15] = "set-cookie";
        arrayOfString[16] = "pragma";
        arrayOfString[17] = "refresh";
        arrayOfString[18] = "x-permitted-cross-domain-policies";
    }

    private void setConnectionType(CharArrayBuffer paramCharArrayBuffer, int paramInt)
    {
        if (CharArrayBuffers.containsIgnoreCaseTrimmed(paramCharArrayBuffer, paramInt, "Close"))
            this.connectionType = 1;
        while (true)
        {
            return;
            if (CharArrayBuffers.containsIgnoreCaseTrimmed(paramCharArrayBuffer, paramInt, "Keep-Alive"))
                this.connectionType = 2;
        }
    }

    public String getAcceptRanges()
    {
        return this.mHeaders[10];
    }

    public String getCacheControl()
    {
        return this.mHeaders[12];
    }

    public int getConnectionType()
    {
        return this.connectionType;
    }

    public String getContentDisposition()
    {
        return this.mHeaders[9];
    }

    public String getContentEncoding()
    {
        return this.mHeaders[3];
    }

    public long getContentLength()
    {
        return this.contentLength;
    }

    public String getContentType()
    {
        return this.mHeaders[2];
    }

    public String getEtag()
    {
        return this.mHeaders[14];
    }

    public String getExpires()
    {
        return this.mHeaders[11];
    }

    public void getHeaders(HeaderCallback paramHeaderCallback)
    {
        for (int i = 0; i < 19; i++)
        {
            String str = this.mHeaders[i];
            if (str != null)
                paramHeaderCallback.header(sHeaderNames[i], str);
        }
        int j = this.mExtraHeaderNames.size();
        for (int k = 0; k < j; k++)
            paramHeaderCallback.header((String)this.mExtraHeaderNames.get(k), (String)this.mExtraHeaderValues.get(k));
    }

    public String getLastModified()
    {
        return this.mHeaders[13];
    }

    public String getLocation()
    {
        return this.mHeaders[5];
    }

    public String getPragma()
    {
        return this.mHeaders[16];
    }

    public String getProxyAuthenticate()
    {
        return this.mHeaders[8];
    }

    public String getRefresh()
    {
        return this.mHeaders[17];
    }

    public ArrayList<String> getSetCookie()
    {
        return this.cookies;
    }

    public long getTransferEncoding()
    {
        return this.transferEncoding;
    }

    public String getWwwAuthenticate()
    {
        return this.mHeaders[7];
    }

    public String getXPermittedCrossDomainPolicies()
    {
        return this.mHeaders[18];
    }

    public void parseHeader(CharArrayBuffer paramCharArrayBuffer)
    {
        int i = CharArrayBuffers.setLowercaseIndexOf(paramCharArrayBuffer, 58);
        if (i == -1);
        while (true)
        {
            return;
            String str1 = paramCharArrayBuffer.substringTrimmed(0, i);
            if (str1.length() != 0)
            {
                int j = i + 1;
                String str2 = paramCharArrayBuffer.substringTrimmed(j, paramCharArrayBuffer.length());
                switch (str1.hashCode())
                {
                default:
                    this.mExtraHeaderNames.add(str1);
                    this.mExtraHeaderValues.add(str2);
                    break;
                case 1274458357:
                    if (str1.equals("transfer-encoding"))
                    {
                        this.mHeaders[0] = str2;
                        HeaderElement[] arrayOfHeaderElement = BasicHeaderValueParser.DEFAULT.parseElements(paramCharArrayBuffer, new ParserCursor(j, paramCharArrayBuffer.length()));
                        int k = arrayOfHeaderElement.length;
                        if ("identity".equalsIgnoreCase(str2))
                            this.transferEncoding = -1L;
                        else if ((k > 0) && ("chunked".equalsIgnoreCase(arrayOfHeaderElement[(k - 1)].getName())))
                            this.transferEncoding = -2L;
                        else
                            this.transferEncoding = -1L;
                    }
                    break;
                case -1132779846:
                    if (str1.equals("content-length"))
                    {
                        this.mHeaders[1] = str2;
                        try
                        {
                            this.contentLength = Long.parseLong(str2);
                        }
                        catch (NumberFormatException localNumberFormatException)
                        {
                        }
                    }
                    break;
                case 785670158:
                    if (str1.equals("content-type"))
                        this.mHeaders[2] = str2;
                    break;
                case 2095084583:
                    if (str1.equals("content-encoding"))
                        this.mHeaders[3] = str2;
                    break;
                case -775651618:
                    if (str1.equals("connection"))
                    {
                        this.mHeaders[4] = str2;
                        setConnectionType(paramCharArrayBuffer, j);
                    }
                    break;
                case 1901043637:
                    if (str1.equals("location"))
                        this.mHeaders[5] = str2;
                    break;
                case 285929373:
                    if (str1.equals("proxy-connection"))
                    {
                        this.mHeaders[6] = str2;
                        setConnectionType(paramCharArrayBuffer, j);
                    }
                    break;
                case -243037365:
                    if (str1.equals("www-authenticate"))
                        this.mHeaders[7] = str2;
                    break;
                case -301767724:
                    if (str1.equals("proxy-authenticate"))
                        this.mHeaders[8] = str2;
                    break;
                case -1267267485:
                    if (str1.equals("content-disposition"))
                        this.mHeaders[9] = str2;
                    break;
                case 1397189435:
                    if (str1.equals("accept-ranges"))
                        this.mHeaders[10] = str2;
                    break;
                case -1309235404:
                    if (str1.equals("expires"))
                        this.mHeaders[11] = str2;
                    break;
                case -208775662:
                    if (str1.equals("cache-control"))
                        if ((this.mHeaders[12] != null) && (this.mHeaders[12].length() > 0))
                        {
                            StringBuilder localStringBuilder = new StringBuilder();
                            String[] arrayOfString = this.mHeaders;
                            arrayOfString[12] = (arrayOfString[12] + ',' + str2);
                        }
                        else
                        {
                            this.mHeaders[12] = str2;
                        }
                    break;
                case 150043680:
                    if (str1.equals("last-modified"))
                        this.mHeaders[13] = str2;
                    break;
                case 3123477:
                    if (str1.equals("etag"))
                        this.mHeaders[14] = str2;
                    break;
                case 1237214767:
                    if (str1.equals("set-cookie"))
                    {
                        this.mHeaders[15] = str2;
                        this.cookies.add(str2);
                    }
                    break;
                case -980228804:
                    if (str1.equals("pragma"))
                        this.mHeaders[16] = str2;
                    break;
                case 1085444827:
                    if (str1.equals("refresh"))
                        this.mHeaders[17] = str2;
                    break;
                case -1345594014:
                    if (str1.equals("x-permitted-cross-domain-policies"))
                        this.mHeaders[18] = str2;
                    break;
                }
            }
        }
    }

    public void setAcceptRanges(String paramString)
    {
        this.mHeaders[10] = paramString;
    }

    public void setCacheControl(String paramString)
    {
        this.mHeaders[12] = paramString;
    }

    public void setContentDisposition(String paramString)
    {
        this.mHeaders[9] = paramString;
    }

    public void setContentEncoding(String paramString)
    {
        this.mHeaders[3] = paramString;
    }

    public void setContentLength(long paramLong)
    {
        this.contentLength = paramLong;
    }

    public void setContentType(String paramString)
    {
        this.mHeaders[2] = paramString;
    }

    public void setEtag(String paramString)
    {
        this.mHeaders[14] = paramString;
    }

    public void setExpires(String paramString)
    {
        this.mHeaders[11] = paramString;
    }

    public void setLastModified(String paramString)
    {
        this.mHeaders[13] = paramString;
    }

    public void setLocation(String paramString)
    {
        this.mHeaders[5] = paramString;
    }

    public void setProxyAuthenticate(String paramString)
    {
        this.mHeaders[8] = paramString;
    }

    public void setWwwAuthenticate(String paramString)
    {
        this.mHeaders[7] = paramString;
    }

    public void setXPermittedCrossDomainPolicies(String paramString)
    {
        this.mHeaders[18] = paramString;
    }

    public static abstract interface HeaderCallback
    {
        public abstract void header(String paramString1, String paramString2);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.http.Headers
 * JD-Core Version:        0.6.2
 */