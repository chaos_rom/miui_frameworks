package android.net.http;

import android.os.SystemClock;
import android.util.Log;

class HttpLog
{
    private static final boolean DEBUG = false;
    private static final String LOGTAG = "http";
    static final boolean LOGV;

    static void e(String paramString)
    {
        Log.e("http", paramString);
    }

    static void v(String paramString)
    {
        Log.v("http", SystemClock.uptimeMillis() + " " + Thread.currentThread().getName() + " " + paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.http.HttpLog
 * JD-Core Version:        0.6.2
 */