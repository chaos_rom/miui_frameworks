package android.net.http;

public class LoggingEventHandler
    implements EventHandler
{
    public void certificate(SslCertificate paramSslCertificate)
    {
    }

    public void data(byte[] paramArrayOfByte, int paramInt)
    {
    }

    public void endData()
    {
    }

    public void error(int paramInt, String paramString)
    {
    }

    public boolean handleSslErrorRequest(SslError paramSslError)
    {
        return false;
    }

    public void headers(Headers paramHeaders)
    {
    }

    public void locationChanged(String paramString, boolean paramBoolean)
    {
    }

    public void requestSent()
    {
        HttpLog.v("LoggingEventHandler:requestSent()");
    }

    public void status(int paramInt1, int paramInt2, int paramInt3, String paramString)
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.http.LoggingEventHandler
 * JD-Core Version:        0.6.2
 */