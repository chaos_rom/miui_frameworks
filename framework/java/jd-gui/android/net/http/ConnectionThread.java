package android.net.http;

import android.content.Context;
import android.os.Process;
import android.os.SystemClock;

class ConnectionThread extends Thread
{
    static final int WAIT_TICK = 1000;
    static final int WAIT_TIMEOUT = 5000;
    Connection mConnection;
    private RequestQueue.ConnectionManager mConnectionManager;
    private Context mContext;
    long mCurrentThreadTime;
    private int mId;
    private RequestFeeder mRequestFeeder;
    private volatile boolean mRunning = true;
    long mTotalThreadTime;
    private boolean mWaiting;

    ConnectionThread(Context paramContext, int paramInt, RequestQueue.ConnectionManager paramConnectionManager, RequestFeeder paramRequestFeeder)
    {
        this.mContext = paramContext;
        setName("http" + paramInt);
        this.mId = paramInt;
        this.mConnectionManager = paramConnectionManager;
        this.mRequestFeeder = paramRequestFeeder;
    }

    void requestStop()
    {
        synchronized (this.mRequestFeeder)
        {
            this.mRunning = false;
            this.mRequestFeeder.notify();
            return;
        }
    }

    public void run()
    {
        Process.setThreadPriority(1);
        this.mCurrentThreadTime = 0L;
        this.mTotalThreadTime = 0L;
        while (true)
        {
            Request localRequest;
            if (this.mRunning)
            {
                if (this.mCurrentThreadTime == -1L)
                    this.mCurrentThreadTime = SystemClock.currentThreadTimeMillis();
                localRequest = this.mRequestFeeder.getRequest();
                if (localRequest == null)
                    synchronized (this.mRequestFeeder)
                    {
                        this.mWaiting = true;
                    }
            }
            try
            {
                this.mRequestFeeder.wait();
                label74: this.mWaiting = false;
                if (this.mCurrentThreadTime != 0L)
                    this.mCurrentThreadTime = SystemClock.currentThreadTimeMillis();
                continue;
                localObject = finally;
                throw localObject;
                this.mConnection = this.mConnectionManager.getConnection(this.mContext, localRequest.mHost);
                this.mConnection.processRequests(localRequest);
                if (this.mConnection.getCanPersist())
                    if (!this.mConnectionManager.recycleConnection(this.mConnection))
                        this.mConnection.closeConnection();
                while (true)
                {
                    this.mConnection = null;
                    if (this.mCurrentThreadTime <= 0L)
                        break;
                    long l = this.mCurrentThreadTime;
                    this.mCurrentThreadTime = SystemClock.currentThreadTimeMillis();
                    this.mTotalThreadTime += this.mCurrentThreadTime - l;
                    break;
                    this.mConnection.closeConnection();
                }
                return;
            }
            catch (InterruptedException localInterruptedException)
            {
                break label74;
            }
        }
    }

    /** @deprecated */
    public String toString()
    {
        try
        {
            String str1;
            if (this.mConnection == null)
            {
                str1 = "";
                if (!this.mWaiting)
                    break label80;
            }
            label80: for (String str2 = "w"; ; str2 = "a")
            {
                String str3 = "cid " + this.mId + " " + str2 + " " + str1;
                return str3;
                str1 = this.mConnection.toString();
                break;
            }
        }
        finally
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.http.ConnectionThread
 * JD-Core Version:        0.6.2
 */