package android.net.http;

import android.content.Context;
import android.util.Log;

public class ErrorStrings
{
    private static final String LOGTAG = "Http";

    public static int getResource(int paramInt)
    {
        int i = 17039634;
        switch (paramInt)
        {
        default:
            Log.w("Http", "Using generic message for unknown error code: " + paramInt);
        case -1:
        case 0:
        case -2:
        case -3:
        case -4:
        case -5:
        case -6:
        case -7:
        case -8:
        case -9:
        case -10:
        case -11:
        case -12:
        case -13:
        case -14:
        case -15:
        }
        while (true)
        {
            return i;
            i = 17039633;
            continue;
            i = 17039635;
            continue;
            i = 17039636;
            continue;
            i = 17039637;
            continue;
            i = 17039638;
            continue;
            i = 17039639;
            continue;
            i = 17039640;
            continue;
            i = 17039641;
            continue;
            i = 17039642;
            continue;
            i = 17039368;
            continue;
            i = 17039643;
            continue;
            i = 17039367;
            continue;
            i = 17039644;
            continue;
            i = 17039645;
            continue;
            i = 17039646;
        }
    }

    public static String getString(int paramInt, Context paramContext)
    {
        return paramContext.getText(getResource(paramInt)).toString();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.http.ErrorStrings
 * JD-Core Version:        0.6.2
 */