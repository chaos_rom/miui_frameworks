package android.net.http;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.android.org.bouncycastle.asn1.x509.X509Name;
import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

public class SslCertificate
{
    private static String ISO_8601_DATE_FORMAT = "yyyy-MM-dd HH:mm:ssZ";
    private static final String ISSUED_BY = "issued-by";
    private static final String ISSUED_TO = "issued-to";
    private static final String VALID_NOT_AFTER = "valid-not-after";
    private static final String VALID_NOT_BEFORE = "valid-not-before";
    private static final String X509_CERTIFICATE = "x509-certificate";
    private final DName mIssuedBy;
    private final DName mIssuedTo;
    private final Date mValidNotAfter;
    private final Date mValidNotBefore;
    private final X509Certificate mX509Certificate;

    @Deprecated
    public SslCertificate(String paramString1, String paramString2, String paramString3, String paramString4)
    {
        this(paramString1, paramString2, parseDate(paramString3), parseDate(paramString4), null);
    }

    @Deprecated
    public SslCertificate(String paramString1, String paramString2, Date paramDate1, Date paramDate2)
    {
        this(paramString1, paramString2, paramDate1, paramDate2, null);
    }

    private SslCertificate(String paramString1, String paramString2, Date paramDate1, Date paramDate2, X509Certificate paramX509Certificate)
    {
        this.mIssuedTo = new DName(paramString1);
        this.mIssuedBy = new DName(paramString2);
        this.mValidNotBefore = cloneDate(paramDate1);
        this.mValidNotAfter = cloneDate(paramDate2);
        this.mX509Certificate = paramX509Certificate;
    }

    public SslCertificate(X509Certificate paramX509Certificate)
    {
        this(paramX509Certificate.getSubjectDN().getName(), paramX509Certificate.getIssuerDN().getName(), paramX509Certificate.getNotBefore(), paramX509Certificate.getNotAfter(), paramX509Certificate);
    }

    private static Date cloneDate(Date paramDate)
    {
        if (paramDate == null);
        for (Date localDate = null; ; localDate = (Date)paramDate.clone())
            return localDate;
    }

    private static final String fingerprint(byte[] paramArrayOfByte)
    {
        if (paramArrayOfByte == null);
        StringBuilder localStringBuilder;
        for (String str = ""; ; str = localStringBuilder.toString())
        {
            return str;
            localStringBuilder = new StringBuilder();
            for (int i = 0; i < paramArrayOfByte.length; i++)
            {
                IntegralToString.appendByteAsHex(localStringBuilder, paramArrayOfByte[i], true);
                if (i + 1 != paramArrayOfByte.length)
                    localStringBuilder.append(':');
            }
        }
    }

    private String formatCertificateDate(Context paramContext, Date paramDate)
    {
        if (paramDate == null);
        for (String str = ""; ; str = android.text.format.DateFormat.getDateFormat(paramContext).format(paramDate))
            return str;
    }

    private static String formatDate(Date paramDate)
    {
        if (paramDate == null);
        for (String str = ""; ; str = new SimpleDateFormat(ISO_8601_DATE_FORMAT).format(paramDate))
            return str;
    }

    private static String getDigest(X509Certificate paramX509Certificate, String paramString)
    {
        Object localObject;
        if (paramX509Certificate == null)
            localObject = "";
        while (true)
        {
            return localObject;
            try
            {
                byte[] arrayOfByte = paramX509Certificate.getEncoded();
                String str = fingerprint(MessageDigest.getInstance(paramString).digest(arrayOfByte));
                localObject = str;
            }
            catch (CertificateEncodingException localCertificateEncodingException)
            {
                localObject = "";
            }
            catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
            {
                localObject = "";
            }
        }
    }

    private static String getSerialNumber(X509Certificate paramX509Certificate)
    {
        String str;
        if (paramX509Certificate == null)
            str = "";
        while (true)
        {
            return str;
            BigInteger localBigInteger = paramX509Certificate.getSerialNumber();
            if (localBigInteger == null)
                str = "";
            else
                str = fingerprint(localBigInteger.toByteArray());
        }
    }

    private static Date parseDate(String paramString)
    {
        try
        {
            Date localDate2 = new SimpleDateFormat(ISO_8601_DATE_FORMAT).parse(paramString);
            localDate1 = localDate2;
            return localDate1;
        }
        catch (ParseException localParseException)
        {
            while (true)
                Date localDate1 = null;
        }
    }

    public static SslCertificate restoreState(Bundle paramBundle)
    {
        SslCertificate localSslCertificate;
        if (paramBundle == null)
        {
            localSslCertificate = null;
            return localSslCertificate;
        }
        byte[] arrayOfByte = paramBundle.getByteArray("x509-certificate");
        X509Certificate localX509Certificate;
        if (arrayOfByte == null)
            localX509Certificate = null;
        while (true)
        {
            localSslCertificate = new SslCertificate(paramBundle.getString("issued-to"), paramBundle.getString("issued-by"), parseDate(paramBundle.getString("valid-not-before")), parseDate(paramBundle.getString("valid-not-after")), localX509Certificate);
            break;
            try
            {
                localX509Certificate = (X509Certificate)CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(arrayOfByte));
            }
            catch (CertificateException localCertificateException)
            {
                localX509Certificate = null;
            }
        }
    }

    public static Bundle saveState(SslCertificate paramSslCertificate)
    {
        Bundle localBundle;
        if (paramSslCertificate == null)
            localBundle = null;
        while (true)
        {
            return localBundle;
            localBundle = new Bundle();
            localBundle.putString("issued-to", paramSslCertificate.getIssuedTo().getDName());
            localBundle.putString("issued-by", paramSslCertificate.getIssuedBy().getDName());
            localBundle.putString("valid-not-before", paramSslCertificate.getValidNotBefore());
            localBundle.putString("valid-not-after", paramSslCertificate.getValidNotAfter());
            X509Certificate localX509Certificate = paramSslCertificate.mX509Certificate;
            if (localX509Certificate != null)
                try
                {
                    localBundle.putByteArray("x509-certificate", localX509Certificate.getEncoded());
                }
                catch (CertificateEncodingException localCertificateEncodingException)
                {
                }
        }
    }

    public DName getIssuedBy()
    {
        return this.mIssuedBy;
    }

    public DName getIssuedTo()
    {
        return this.mIssuedTo;
    }

    @Deprecated
    public String getValidNotAfter()
    {
        return formatDate(this.mValidNotAfter);
    }

    public Date getValidNotAfterDate()
    {
        return cloneDate(this.mValidNotAfter);
    }

    @Deprecated
    public String getValidNotBefore()
    {
        return formatDate(this.mValidNotBefore);
    }

    public Date getValidNotBeforeDate()
    {
        return cloneDate(this.mValidNotBefore);
    }

    public View inflateCertificateView(Context paramContext)
    {
        View localView = LayoutInflater.from(paramContext).inflate(17367212, null);
        DName localDName1 = getIssuedTo();
        if (localDName1 != null)
        {
            ((TextView)localView.findViewById(16909111)).setText(localDName1.getCName());
            ((TextView)localView.findViewById(16909113)).setText(localDName1.getOName());
            ((TextView)localView.findViewById(16909115)).setText(localDName1.getUName());
        }
        ((TextView)localView.findViewById(16909117)).setText(getSerialNumber(this.mX509Certificate));
        DName localDName2 = getIssuedBy();
        if (localDName2 != null)
        {
            ((TextView)localView.findViewById(16909119)).setText(localDName2.getCName());
            ((TextView)localView.findViewById(16909121)).setText(localDName2.getOName());
            ((TextView)localView.findViewById(16909123)).setText(localDName2.getUName());
        }
        String str1 = formatCertificateDate(paramContext, getValidNotBeforeDate());
        ((TextView)localView.findViewById(16909126)).setText(str1);
        String str2 = formatCertificateDate(paramContext, getValidNotAfterDate());
        ((TextView)localView.findViewById(16909128)).setText(str2);
        ((TextView)localView.findViewById(16909131)).setText(getDigest(this.mX509Certificate, "SHA256"));
        ((TextView)localView.findViewById(16909133)).setText(getDigest(this.mX509Certificate, "SHA1"));
        return localView;
    }

    public String toString()
    {
        return "Issued to: " + this.mIssuedTo.getDName() + ";\n" + "Issued by: " + this.mIssuedBy.getDName() + ";\n";
    }

    public class DName
    {
        private String mCName;
        private String mDName;
        private String mOName;
        private String mUName;

        public DName(String arg2)
        {
            String str;
            if (str != null)
            {
                this.mDName = str;
                try
                {
                    X509Name localX509Name = new X509Name(str);
                    Vector localVector1 = localX509Name.getValues();
                    Vector localVector2 = localX509Name.getOIDs();
                    for (int i = 0; i < localVector2.size(); i++)
                        if (localVector2.elementAt(i).equals(X509Name.CN))
                            this.mCName = ((String)localVector1.elementAt(i));
                        else if (localVector2.elementAt(i).equals(X509Name.O))
                            this.mOName = ((String)localVector1.elementAt(i));
                        else if (localVector2.elementAt(i).equals(X509Name.OU))
                            this.mUName = ((String)localVector1.elementAt(i));
                }
                catch (IllegalArgumentException localIllegalArgumentException)
                {
                }
            }
        }

        public String getCName()
        {
            if (this.mCName != null);
            for (String str = this.mCName; ; str = "")
                return str;
        }

        public String getDName()
        {
            if (this.mDName != null);
            for (String str = this.mDName; ; str = "")
                return str;
        }

        public String getOName()
        {
            if (this.mOName != null);
            for (String str = this.mOName; ; str = "")
                return str;
        }

        public String getUName()
        {
            if (this.mUName != null);
            for (String str = this.mUName; ; str = "")
                return str;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.http.SslCertificate
 * JD-Core Version:        0.6.2
 */