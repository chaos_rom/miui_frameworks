package android.net.http;

import android.content.Context;
import java.io.IOException;
import java.net.Socket;
import org.apache.http.HttpHost;
import org.apache.http.params.BasicHttpParams;

class HttpConnection extends Connection
{
    HttpConnection(Context paramContext, HttpHost paramHttpHost, RequestFeeder paramRequestFeeder)
    {
        super(paramContext, paramHttpHost, paramRequestFeeder);
    }

    void closeConnection()
    {
        try
        {
            if ((this.mHttpClientConnection != null) && (this.mHttpClientConnection.isOpen()))
                this.mHttpClientConnection.close();
            return;
        }
        catch (IOException localIOException)
        {
            while (true)
                localIOException.printStackTrace();
        }
    }

    String getScheme()
    {
        return "http";
    }

    AndroidHttpClientConnection openConnection(Request paramRequest)
        throws IOException
    {
        EventHandler localEventHandler = paramRequest.getEventHandler();
        this.mCertificate = null;
        localEventHandler.certificate(this.mCertificate);
        AndroidHttpClientConnection localAndroidHttpClientConnection = new AndroidHttpClientConnection();
        BasicHttpParams localBasicHttpParams = new BasicHttpParams();
        Socket localSocket = new Socket(this.mHost.getHostName(), this.mHost.getPort());
        localBasicHttpParams.setIntParameter("http.socket.buffer-size", 8192);
        localAndroidHttpClientConnection.bind(localSocket, localBasicHttpParams);
        return localAndroidHttpClientConnection;
    }

    void restartConnection(boolean paramBoolean)
    {
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.http.HttpConnection
 * JD-Core Version:        0.6.2
 */