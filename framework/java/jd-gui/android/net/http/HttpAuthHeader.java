package android.net.http;

public class HttpAuthHeader
{
    private static final String ALGORITHM_TOKEN = "algorithm";
    public static final int BASIC = 1;
    public static final String BASIC_TOKEN = "Basic";
    public static final int DIGEST = 2;
    public static final String DIGEST_TOKEN = "Digest";
    private static final String NONCE_TOKEN = "nonce";
    private static final String OPAQUE_TOKEN = "opaque";
    private static final String QOP_TOKEN = "qop";
    private static final String REALM_TOKEN = "realm";
    private static final String STALE_TOKEN = "stale";
    public static final int UNKNOWN;
    private String mAlgorithm;
    private boolean mIsProxy;
    private String mNonce;
    private String mOpaque;
    private String mPassword;
    private String mQop;
    private String mRealm;
    private int mScheme;
    private boolean mStale;
    private String mUsername;

    public HttpAuthHeader(String paramString)
    {
        if (paramString != null)
            parseHeader(paramString);
    }

    private void parseHeader(String paramString)
    {
        if (paramString != null)
        {
            String str = parseScheme(paramString);
            if ((str != null) && (this.mScheme != 0))
                parseParameters(str);
        }
    }

    private void parseParameter(String paramString)
    {
        String str1;
        String str2;
        if (paramString != null)
        {
            int i = paramString.indexOf('=');
            if (i >= 0)
            {
                str1 = paramString.substring(0, i).trim();
                str2 = trimDoubleQuotesIfAny(paramString.substring(i + 1).trim());
                if (!str1.equalsIgnoreCase("realm"))
                    break label56;
                this.mRealm = str2;
            }
        }
        while (true)
        {
            return;
            label56: if (this.mScheme == 2)
                parseParameter(str1, str2);
        }
    }

    private void parseParameter(String paramString1, String paramString2)
    {
        if ((paramString1 != null) && (paramString2 != null))
        {
            if (!paramString1.equalsIgnoreCase("nonce"))
                break label23;
            this.mNonce = paramString2;
        }
        while (true)
        {
            return;
            label23: if (paramString1.equalsIgnoreCase("stale"))
                parseStale(paramString2);
            else if (paramString1.equalsIgnoreCase("opaque"))
                this.mOpaque = paramString2;
            else if (paramString1.equalsIgnoreCase("qop"))
                this.mQop = paramString2.toLowerCase();
            else if (paramString1.equalsIgnoreCase("algorithm"))
                this.mAlgorithm = paramString2.toLowerCase();
        }
    }

    private void parseParameters(String paramString)
    {
        if (paramString != null);
        while (true)
        {
            int i = paramString.indexOf(',');
            if (i < 0)
                parseParameter(paramString);
            while (i < 0)
            {
                return;
                parseParameter(paramString.substring(0, i));
                paramString = paramString.substring(i + 1);
            }
        }
    }

    private String parseScheme(String paramString)
    {
        int i;
        String str2;
        if (paramString != null)
        {
            i = paramString.indexOf(' ');
            if (i >= 0)
            {
                str2 = paramString.substring(0, i).trim();
                if (str2.equalsIgnoreCase("Digest"))
                {
                    this.mScheme = 2;
                    this.mAlgorithm = "md5";
                }
            }
        }
        for (String str1 = paramString.substring(i + 1); ; str1 = null)
        {
            return str1;
            if (!str2.equalsIgnoreCase("Basic"))
                break;
            this.mScheme = 1;
            break;
        }
    }

    private void parseStale(String paramString)
    {
        if ((paramString != null) && (paramString.equalsIgnoreCase("true")))
            this.mStale = true;
    }

    private static String trimDoubleQuotesIfAny(String paramString)
    {
        if (paramString != null)
        {
            int i = paramString.length();
            if ((i > 2) && (paramString.charAt(0) == '"') && (paramString.charAt(i - 1) == '"'))
                paramString = paramString.substring(1, i - 1);
        }
        return paramString;
    }

    public String getAlgorithm()
    {
        return this.mAlgorithm;
    }

    public String getNonce()
    {
        return this.mNonce;
    }

    public String getOpaque()
    {
        return this.mOpaque;
    }

    public String getPassword()
    {
        return this.mPassword;
    }

    public String getQop()
    {
        return this.mQop;
    }

    public String getRealm()
    {
        return this.mRealm;
    }

    public int getScheme()
    {
        return this.mScheme;
    }

    public boolean getStale()
    {
        return this.mStale;
    }

    public String getUsername()
    {
        return this.mUsername;
    }

    public boolean isBasic()
    {
        int i = 1;
        if (this.mScheme == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public boolean isDigest()
    {
        if (this.mScheme == 2);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isProxy()
    {
        return this.mIsProxy;
    }

    public boolean isSupportedScheme()
    {
        int i = 1;
        if (this.mRealm != null)
            if (this.mScheme != i);
        while (true)
        {
            return i;
            int j;
            if (this.mScheme == 2)
            {
                if ((!this.mAlgorithm.equals("md5")) || ((this.mQop != null) && (!this.mQop.equals("auth"))))
                    j = 0;
            }
            else
                j = 0;
        }
    }

    public void setPassword(String paramString)
    {
        this.mPassword = paramString;
    }

    public void setProxy()
    {
        this.mIsProxy = true;
    }

    public void setUsername(String paramString)
    {
        this.mUsername = paramString;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.http.HttpAuthHeader
 * JD-Core Version:        0.6.2
 */