package android.net.http;

import android.os.SystemClock;

class Timer
{
    private long mLast;
    private long mStart;

    public Timer()
    {
        long l = SystemClock.uptimeMillis();
        this.mLast = l;
        this.mStart = l;
    }

    public void mark(String paramString)
    {
        this.mLast = SystemClock.uptimeMillis();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.http.Timer
 * JD-Core Version:        0.6.2
 */