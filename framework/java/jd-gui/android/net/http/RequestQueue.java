package android.net.http;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.net.WebAddress;
import android.util.Log;
import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.http.HttpHost;

public class RequestQueue
    implements RequestFeeder
{
    private static final int CONNECTION_COUNT = 4;
    private final ActivePool mActivePool;
    private final ConnectivityManager mConnectivityManager;
    private final Context mContext;
    private final LinkedHashMap<HttpHost, LinkedList<Request>> mPending;
    private BroadcastReceiver mProxyChangeReceiver;
    private HttpHost mProxyHost = null;

    public RequestQueue(Context paramContext)
    {
        this(paramContext, 4);
    }

    public RequestQueue(Context paramContext, int paramInt)
    {
        this.mContext = paramContext;
        this.mPending = new LinkedHashMap(32);
        this.mActivePool = new ActivePool(paramInt);
        this.mActivePool.startup();
        this.mConnectivityManager = ((ConnectivityManager)paramContext.getSystemService("connectivity"));
    }

    private HttpHost determineHost(HttpHost paramHttpHost)
    {
        if ((this.mProxyHost == null) || ("https".equals(paramHttpHost.getSchemeName())));
        while (true)
        {
            return paramHttpHost;
            paramHttpHost = this.mProxyHost;
        }
    }

    private Request removeFirst(LinkedHashMap<HttpHost, LinkedList<Request>> paramLinkedHashMap)
    {
        Request localRequest = null;
        Iterator localIterator = paramLinkedHashMap.entrySet().iterator();
        if (localIterator.hasNext())
        {
            Map.Entry localEntry = (Map.Entry)localIterator.next();
            LinkedList localLinkedList = (LinkedList)localEntry.getValue();
            localRequest = (Request)localLinkedList.removeFirst();
            if (localLinkedList.isEmpty())
                paramLinkedHashMap.remove(localEntry.getKey());
        }
        return localRequest;
    }

    /** @deprecated */
    private void setProxyConfig()
    {
        while (true)
        {
            String str;
            try
            {
                NetworkInfo localNetworkInfo = this.mConnectivityManager.getActiveNetworkInfo();
                if ((localNetworkInfo != null) && (localNetworkInfo.getType() == 1))
                {
                    this.mProxyHost = null;
                    return;
                }
                str = Proxy.getHost(this.mContext);
                if (str == null)
                {
                    this.mProxyHost = null;
                    continue;
                }
            }
            finally
            {
            }
            this.mActivePool.disablePersistence();
            this.mProxyHost = new HttpHost(str, Proxy.getPort(this.mContext), "http");
        }
    }

    /** @deprecated */
    public void disablePlatformNotifications()
    {
        try
        {
            if (this.mProxyChangeReceiver != null)
            {
                this.mContext.unregisterReceiver(this.mProxyChangeReceiver);
                this.mProxyChangeReceiver = null;
            }
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void dump()
    {
        StringBuilder localStringBuilder1;
        int i;
        while (true)
        {
            int j;
            StringBuilder localStringBuilder3;
            try
            {
                HttpLog.v("dump()");
                localStringBuilder1 = new StringBuilder();
                if (this.mPending.isEmpty())
                    break;
                Iterator localIterator = this.mPending.entrySet().iterator();
                i = 0;
                if (!localIterator.hasNext())
                    break label230;
                Map.Entry localEntry = (Map.Entry)localIterator.next();
                String str = ((HttpHost)localEntry.getKey()).getHostName();
                StringBuilder localStringBuilder2 = new StringBuilder().append("p");
                j = i + 1;
                localStringBuilder3 = new StringBuilder(i + " " + str + " ");
                ((LinkedList)localEntry.getValue()).listIterator(0);
                if (localIterator.hasNext())
                {
                    Request localRequest = (Request)localIterator.next();
                    localStringBuilder3.append(localRequest + " ");
                    continue;
                }
            }
            finally
            {
            }
            localStringBuilder1.append(localStringBuilder3);
            localStringBuilder1.append("\n");
            i = j;
        }
        label230: 
        while (true)
        {
            HttpLog.v(localStringBuilder1.toString());
            return;
        }
    }

    /** @deprecated */
    public void enablePlatformNotifications()
    {
        try
        {
            if (this.mProxyChangeReceiver == null)
            {
                this.mProxyChangeReceiver = new BroadcastReceiver()
                {
                    public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
                    {
                        RequestQueue.this.setProxyConfig();
                    }
                };
                this.mContext.registerReceiver(this.mProxyChangeReceiver, new IntentFilter("android.intent.action.PROXY_CHANGE"));
            }
            setProxyConfig();
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public HttpHost getProxyHost()
    {
        return this.mProxyHost;
    }

    /** @deprecated */
    public Request getRequest()
    {
        Object localObject1 = null;
        try
        {
            if (!this.mPending.isEmpty())
            {
                Request localRequest = removeFirst(this.mPending);
                localObject1 = localRequest;
            }
            return localObject1;
        }
        finally
        {
            localObject2 = finally;
            throw localObject2;
        }
    }

    /** @deprecated */
    public Request getRequest(HttpHost paramHttpHost)
    {
        Request localRequest = null;
        try
        {
            if (this.mPending.containsKey(paramHttpHost))
            {
                LinkedList localLinkedList = (LinkedList)this.mPending.get(paramHttpHost);
                localRequest = (Request)localLinkedList.removeFirst();
                if (localLinkedList.isEmpty())
                    this.mPending.remove(paramHttpHost);
            }
            return localRequest;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public boolean haveRequest(HttpHost paramHttpHost)
    {
        try
        {
            boolean bool = this.mPending.containsKey(paramHttpHost);
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public RequestHandle queueRequest(String paramString1, WebAddress paramWebAddress, String paramString2, Map<String, String> paramMap, EventHandler paramEventHandler, InputStream paramInputStream, int paramInt)
    {
        if (paramEventHandler == null)
            paramEventHandler = new LoggingEventHandler();
        Request localRequest = new Request(paramString2, new HttpHost(paramWebAddress.getHost(), paramWebAddress.getPort(), paramWebAddress.getScheme()), this.mProxyHost, paramWebAddress.getPath(), paramInputStream, paramInt, paramEventHandler, paramMap);
        queueRequest(localRequest, false);
        ActivePool.access$408(this.mActivePool);
        this.mActivePool.startConnectionThread();
        return new RequestHandle(this, paramString1, paramWebAddress, paramString2, paramMap, paramInputStream, paramInt, localRequest);
    }

    public RequestHandle queueRequest(String paramString1, String paramString2, Map<String, String> paramMap, EventHandler paramEventHandler, InputStream paramInputStream, int paramInt)
    {
        return queueRequest(paramString1, new WebAddress(paramString1), paramString2, paramMap, paramEventHandler, paramInputStream, paramInt);
    }

    /** @deprecated */
    protected void queueRequest(Request paramRequest, boolean paramBoolean)
    {
        while (true)
        {
            LinkedList localLinkedList;
            try
            {
                HttpHost localHttpHost;
                if (paramRequest.mProxyHost == null)
                {
                    localHttpHost = paramRequest.mHost;
                    if (this.mPending.containsKey(localHttpHost))
                    {
                        localLinkedList = (LinkedList)this.mPending.get(localHttpHost);
                        if (!paramBoolean)
                            break label92;
                        localLinkedList.addFirst(paramRequest);
                    }
                }
                else
                {
                    localHttpHost = paramRequest.mProxyHost;
                    continue;
                }
                localLinkedList = new LinkedList();
                this.mPending.put(localHttpHost, localLinkedList);
                continue;
            }
            finally
            {
            }
            label92: localLinkedList.add(paramRequest);
        }
    }

    public RequestHandle queueSynchronousRequest(String paramString1, WebAddress paramWebAddress, String paramString2, Map<String, String> paramMap, EventHandler paramEventHandler, InputStream paramInputStream, int paramInt)
    {
        HttpHost localHttpHost1 = new HttpHost(paramWebAddress.getHost(), paramWebAddress.getPort(), paramWebAddress.getScheme());
        Request localRequest = new Request(paramString2, localHttpHost1, this.mProxyHost, paramWebAddress.getPath(), paramInputStream, paramInt, paramEventHandler, paramMap);
        HttpHost localHttpHost2 = determineHost(localHttpHost1);
        return new RequestHandle(this, paramString1, paramWebAddress, paramString2, paramMap, paramInputStream, paramInt, localRequest, Connection.getConnection(this.mContext, localHttpHost2, this.mProxyHost, new SyncFeeder()));
    }

    /** @deprecated */
    boolean requestsPending()
    {
        try
        {
            boolean bool1 = this.mPending.isEmpty();
            if (!bool1)
            {
                bool2 = true;
                return bool2;
            }
            boolean bool2 = false;
        }
        finally
        {
        }
    }

    public void requeueRequest(Request paramRequest)
    {
        queueRequest(paramRequest, true);
    }

    public void shutdown()
    {
        this.mActivePool.shutdown();
    }

    public void startTiming()
    {
        this.mActivePool.startTiming();
    }

    public void stopTiming()
    {
        this.mActivePool.stopTiming();
    }

    static abstract interface ConnectionManager
    {
        public abstract Connection getConnection(Context paramContext, HttpHost paramHttpHost);

        public abstract HttpHost getProxyHost();

        public abstract boolean recycleConnection(Connection paramConnection);
    }

    private static class SyncFeeder
        implements RequestFeeder
    {
        private Request mRequest;

        public Request getRequest()
        {
            Request localRequest = this.mRequest;
            this.mRequest = null;
            return localRequest;
        }

        public Request getRequest(HttpHost paramHttpHost)
        {
            return getRequest();
        }

        public boolean haveRequest(HttpHost paramHttpHost)
        {
            if (this.mRequest != null);
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public void requeueRequest(Request paramRequest)
        {
            this.mRequest = paramRequest;
        }
    }

    class ActivePool
        implements RequestQueue.ConnectionManager
    {
        private int mConnectionCount;
        IdleCache mIdleCache = new IdleCache();
        ConnectionThread[] mThreads;
        private int mTotalConnection;
        private int mTotalRequest;

        ActivePool(int arg2)
        {
            int i;
            this.mConnectionCount = i;
            this.mThreads = new ConnectionThread[this.mConnectionCount];
            for (int j = 0; j < this.mConnectionCount; j++)
                this.mThreads[j] = new ConnectionThread(RequestQueue.this.mContext, j, this, RequestQueue.this);
        }

        void disablePersistence()
        {
            for (int i = 0; i < this.mConnectionCount; i++)
            {
                Connection localConnection = this.mThreads[i].mConnection;
                if (localConnection != null)
                    localConnection.setCanPersist(false);
            }
            this.mIdleCache.clear();
        }

        public Connection getConnection(Context paramContext, HttpHost paramHttpHost)
        {
            HttpHost localHttpHost = RequestQueue.this.determineHost(paramHttpHost);
            Connection localConnection = this.mIdleCache.getConnection(localHttpHost);
            if (localConnection == null)
            {
                this.mTotalConnection = (1 + this.mTotalConnection);
                localConnection = Connection.getConnection(RequestQueue.this.mContext, localHttpHost, RequestQueue.this.mProxyHost, RequestQueue.this);
            }
            return localConnection;
        }

        public HttpHost getProxyHost()
        {
            return RequestQueue.this.mProxyHost;
        }

        ConnectionThread getThread(HttpHost paramHttpHost)
        {
            RequestQueue localRequestQueue = RequestQueue.this;
            for (int i = 0; ; i++)
            {
                ConnectionThread localConnectionThread;
                try
                {
                    if (i < this.mThreads.length)
                    {
                        localConnectionThread = this.mThreads[i];
                        Connection localConnection = localConnectionThread.mConnection;
                        if ((localConnection == null) || (!localConnection.mHost.equals(paramHttpHost)))
                            continue;
                    }
                    else
                    {
                        localConnectionThread = null;
                    }
                }
                finally
                {
                }
            }
        }

        void logState()
        {
            StringBuilder localStringBuilder = new StringBuilder();
            for (int i = 0; i < this.mConnectionCount; i++)
                localStringBuilder.append(this.mThreads[i] + "\n");
            HttpLog.v(localStringBuilder.toString());
        }

        public boolean recycleConnection(Connection paramConnection)
        {
            return this.mIdleCache.cacheConnection(paramConnection.getHost(), paramConnection);
        }

        void shutdown()
        {
            for (int i = 0; i < this.mConnectionCount; i++)
                this.mThreads[i].requestStop();
        }

        void startConnectionThread()
        {
            synchronized (RequestQueue.this)
            {
                RequestQueue.this.notify();
                return;
            }
        }

        public void startTiming()
        {
            for (int i = 0; i < this.mConnectionCount; i++)
            {
                ConnectionThread localConnectionThread = this.mThreads[i];
                localConnectionThread.mCurrentThreadTime = -1L;
                localConnectionThread.mTotalThreadTime = 0L;
            }
            this.mTotalRequest = 0;
            this.mTotalConnection = 0;
        }

        void startup()
        {
            for (int i = 0; i < this.mConnectionCount; i++)
                this.mThreads[i].start();
        }

        public void stopTiming()
        {
            int i = 0;
            for (int j = 0; j < this.mConnectionCount; j++)
            {
                ConnectionThread localConnectionThread = this.mThreads[j];
                if (localConnectionThread.mCurrentThreadTime != -1L)
                    i = (int)(i + localConnectionThread.mTotalThreadTime);
                localConnectionThread.mCurrentThreadTime = 0L;
            }
            Log.d("Http", "Http thread used " + i + " ms " + " for " + this.mTotalRequest + " requests and " + this.mTotalConnection + " new connections");
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.http.RequestQueue
 * JD-Core Version:        0.6.2
 */