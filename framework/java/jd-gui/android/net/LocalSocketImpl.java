package android.net;

import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

class LocalSocketImpl
{
    private FileDescriptor fd;
    private SocketInputStream fis;
    private SocketOutputStream fos;
    FileDescriptor[] inboundFileDescriptors;
    FileDescriptor[] outboundFileDescriptors;
    private Object readMonitor = new Object();
    private Object writeMonitor = new Object();

    LocalSocketImpl()
    {
    }

    LocalSocketImpl(FileDescriptor paramFileDescriptor)
        throws IOException
    {
        this.fd = paramFileDescriptor;
    }

    private native FileDescriptor accept(FileDescriptor paramFileDescriptor, LocalSocketImpl paramLocalSocketImpl)
        throws IOException;

    private native int available_native(FileDescriptor paramFileDescriptor)
        throws IOException;

    private native void bindLocal(FileDescriptor paramFileDescriptor, String paramString, int paramInt)
        throws IOException;

    private native void close_native(FileDescriptor paramFileDescriptor)
        throws IOException;

    private native void connectLocal(FileDescriptor paramFileDescriptor, String paramString, int paramInt)
        throws IOException;

    private native FileDescriptor create_native(boolean paramBoolean)
        throws IOException;

    private native int getOption_native(FileDescriptor paramFileDescriptor, int paramInt)
        throws IOException;

    private native Credentials getPeerCredentials_native(FileDescriptor paramFileDescriptor)
        throws IOException;

    private native void listen_native(FileDescriptor paramFileDescriptor, int paramInt)
        throws IOException;

    private native int read_native(FileDescriptor paramFileDescriptor)
        throws IOException;

    private native int readba_native(byte[] paramArrayOfByte, int paramInt1, int paramInt2, FileDescriptor paramFileDescriptor)
        throws IOException;

    private native void setOption_native(FileDescriptor paramFileDescriptor, int paramInt1, int paramInt2, int paramInt3)
        throws IOException;

    private native void shutdown(FileDescriptor paramFileDescriptor, boolean paramBoolean);

    private native void write_native(int paramInt, FileDescriptor paramFileDescriptor)
        throws IOException;

    private native void writeba_native(byte[] paramArrayOfByte, int paramInt1, int paramInt2, FileDescriptor paramFileDescriptor)
        throws IOException;

    protected void accept(LocalSocketImpl paramLocalSocketImpl)
        throws IOException
    {
        if (this.fd == null)
            throw new IOException("socket not created");
        paramLocalSocketImpl.fd = accept(this.fd, paramLocalSocketImpl);
    }

    protected int available()
        throws IOException
    {
        return getInputStream().available();
    }

    public void bind(LocalSocketAddress paramLocalSocketAddress)
        throws IOException
    {
        if (this.fd == null)
            throw new IOException("socket not created");
        bindLocal(this.fd, paramLocalSocketAddress.getName(), paramLocalSocketAddress.getNamespace().getId());
    }

    public void close()
        throws IOException
    {
        try
        {
            if (this.fd == null)
                return;
            close_native(this.fd);
            this.fd = null;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    protected void connect(LocalSocketAddress paramLocalSocketAddress, int paramInt)
        throws IOException
    {
        if (this.fd == null)
            throw new IOException("socket not created");
        connectLocal(this.fd, paramLocalSocketAddress.getName(), paramLocalSocketAddress.getNamespace().getId());
    }

    public void create(boolean paramBoolean)
        throws IOException
    {
        if (this.fd == null)
            this.fd = create_native(paramBoolean);
    }

    protected void finalize()
        throws IOException
    {
        close();
    }

    public FileDescriptor[] getAncillaryFileDescriptors()
        throws IOException
    {
        synchronized (this.readMonitor)
        {
            FileDescriptor[] arrayOfFileDescriptor = this.inboundFileDescriptors;
            this.inboundFileDescriptors = null;
            return arrayOfFileDescriptor;
        }
    }

    protected FileDescriptor getFileDescriptor()
    {
        return this.fd;
    }

    protected InputStream getInputStream()
        throws IOException
    {
        if (this.fd == null)
            throw new IOException("socket not created");
        try
        {
            if (this.fis == null)
                this.fis = new SocketInputStream();
            SocketInputStream localSocketInputStream = this.fis;
            return localSocketInputStream;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public Object getOption(int paramInt)
        throws IOException
    {
        if (this.fd == null)
            throw new IOException("socket not created");
        Integer localInteger;
        if (paramInt == 4102)
            localInteger = Integer.valueOf(0);
        while (true)
        {
            return localInteger;
            int i = getOption_native(this.fd, paramInt);
            switch (paramInt)
            {
            default:
                localInteger = Integer.valueOf(i);
                break;
            case 4097:
            case 4098:
                localInteger = Integer.valueOf(i);
            }
        }
    }

    protected OutputStream getOutputStream()
        throws IOException
    {
        if (this.fd == null)
            throw new IOException("socket not created");
        try
        {
            if (this.fos == null)
                this.fos = new SocketOutputStream();
            SocketOutputStream localSocketOutputStream = this.fos;
            return localSocketOutputStream;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public Credentials getPeerCredentials()
        throws IOException
    {
        return getPeerCredentials_native(this.fd);
    }

    public LocalSocketAddress getSockAddress()
        throws IOException
    {
        return null;
    }

    protected void listen(int paramInt)
        throws IOException
    {
        if (this.fd == null)
            throw new IOException("socket not created");
        listen_native(this.fd, paramInt);
    }

    protected void sendUrgentData(int paramInt)
        throws IOException
    {
        throw new RuntimeException("not impled");
    }

    public void setFileDescriptorsForSend(FileDescriptor[] paramArrayOfFileDescriptor)
    {
        synchronized (this.writeMonitor)
        {
            this.outboundFileDescriptors = paramArrayOfFileDescriptor;
            return;
        }
    }

    public void setOption(int paramInt, Object paramObject)
        throws IOException
    {
        int i = -1;
        int j = 0;
        if (this.fd == null)
            throw new IOException("socket not created");
        if ((paramObject instanceof Integer))
        {
            j = ((Integer)paramObject).intValue();
            setOption_native(this.fd, paramInt, i, j);
            return;
        }
        if ((paramObject instanceof Boolean))
        {
            if (((Boolean)paramObject).booleanValue());
            for (i = 1; ; i = 0)
                break;
        }
        throw new IOException("bad value: " + paramObject);
    }

    protected void shutdownInput()
        throws IOException
    {
        if (this.fd == null)
            throw new IOException("socket not created");
        shutdown(this.fd, true);
    }

    protected void shutdownOutput()
        throws IOException
    {
        if (this.fd == null)
            throw new IOException("socket not created");
        shutdown(this.fd, false);
    }

    protected boolean supportsUrgentData()
    {
        return false;
    }

    public String toString()
    {
        return super.toString() + " fd:" + this.fd;
    }

    class SocketOutputStream extends OutputStream
    {
        SocketOutputStream()
        {
        }

        public void close()
            throws IOException
        {
            LocalSocketImpl.this.close();
        }

        // ERROR //
        public void write(int paramInt)
            throws IOException
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 13	android/net/LocalSocketImpl$SocketOutputStream:this$0	Landroid/net/LocalSocketImpl;
            //     4: invokestatic 27	android/net/LocalSocketImpl:access$500	(Landroid/net/LocalSocketImpl;)Ljava/lang/Object;
            //     7: astore_2
            //     8: aload_2
            //     9: monitorenter
            //     10: aload_0
            //     11: getfield 13	android/net/LocalSocketImpl$SocketOutputStream:this$0	Landroid/net/LocalSocketImpl;
            //     14: invokestatic 31	android/net/LocalSocketImpl:access$000	(Landroid/net/LocalSocketImpl;)Ljava/io/FileDescriptor;
            //     17: astore 4
            //     19: aload 4
            //     21: ifnonnull +18 -> 39
            //     24: new 19	java/io/IOException
            //     27: dup
            //     28: ldc 33
            //     30: invokespecial 36	java/io/IOException:<init>	(Ljava/lang/String;)V
            //     33: athrow
            //     34: astore_3
            //     35: aload_2
            //     36: monitorexit
            //     37: aload_3
            //     38: athrow
            //     39: aload_0
            //     40: getfield 13	android/net/LocalSocketImpl$SocketOutputStream:this$0	Landroid/net/LocalSocketImpl;
            //     43: iload_1
            //     44: aload 4
            //     46: invokestatic 40	android/net/LocalSocketImpl:access$700	(Landroid/net/LocalSocketImpl;ILjava/io/FileDescriptor;)V
            //     49: aload_2
            //     50: monitorexit
            //     51: return
            //
            // Exception table:
            //     from	to	target	type
            //     10	37	34	finally
            //     39	51	34	finally
        }

        public void write(byte[] paramArrayOfByte)
            throws IOException
        {
            write(paramArrayOfByte, 0, paramArrayOfByte.length);
        }

        // ERROR //
        public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
            throws IOException
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 13	android/net/LocalSocketImpl$SocketOutputStream:this$0	Landroid/net/LocalSocketImpl;
            //     4: invokestatic 27	android/net/LocalSocketImpl:access$500	(Landroid/net/LocalSocketImpl;)Ljava/lang/Object;
            //     7: astore 4
            //     9: aload 4
            //     11: monitorenter
            //     12: aload_0
            //     13: getfield 13	android/net/LocalSocketImpl$SocketOutputStream:this$0	Landroid/net/LocalSocketImpl;
            //     16: invokestatic 31	android/net/LocalSocketImpl:access$000	(Landroid/net/LocalSocketImpl;)Ljava/io/FileDescriptor;
            //     19: astore 6
            //     21: aload 6
            //     23: ifnonnull +21 -> 44
            //     26: new 19	java/io/IOException
            //     29: dup
            //     30: ldc 33
            //     32: invokespecial 36	java/io/IOException:<init>	(Ljava/lang/String;)V
            //     35: athrow
            //     36: astore 5
            //     38: aload 4
            //     40: monitorexit
            //     41: aload 5
            //     43: athrow
            //     44: iload_2
            //     45: iflt +19 -> 64
            //     48: iload_3
            //     49: iflt +15 -> 64
            //     52: iload_2
            //     53: iload_3
            //     54: iadd
            //     55: istore 7
            //     57: iload 7
            //     59: aload_1
            //     60: arraylength
            //     61: if_icmple +11 -> 72
            //     64: new 46	java/lang/ArrayIndexOutOfBoundsException
            //     67: dup
            //     68: invokespecial 47	java/lang/ArrayIndexOutOfBoundsException:<init>	()V
            //     71: athrow
            //     72: aload_0
            //     73: getfield 13	android/net/LocalSocketImpl$SocketOutputStream:this$0	Landroid/net/LocalSocketImpl;
            //     76: aload_1
            //     77: iload_2
            //     78: iload_3
            //     79: aload 6
            //     81: invokestatic 51	android/net/LocalSocketImpl:access$600	(Landroid/net/LocalSocketImpl;[BIILjava/io/FileDescriptor;)V
            //     84: aload 4
            //     86: monitorexit
            //     87: return
            //
            // Exception table:
            //     from	to	target	type
            //     12	41	36	finally
            //     57	87	36	finally
        }
    }

    class SocketInputStream extends InputStream
    {
        SocketInputStream()
        {
        }

        public int available()
            throws IOException
        {
            return LocalSocketImpl.this.available_native(LocalSocketImpl.this.fd);
        }

        public void close()
            throws IOException
        {
            LocalSocketImpl.this.close();
        }

        // ERROR //
        public int read()
            throws IOException
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 13	android/net/LocalSocketImpl$SocketInputStream:this$0	Landroid/net/LocalSocketImpl;
            //     4: invokestatic 36	android/net/LocalSocketImpl:access$200	(Landroid/net/LocalSocketImpl;)Ljava/lang/Object;
            //     7: astore_1
            //     8: aload_1
            //     9: monitorenter
            //     10: aload_0
            //     11: getfield 13	android/net/LocalSocketImpl$SocketInputStream:this$0	Landroid/net/LocalSocketImpl;
            //     14: invokestatic 24	android/net/LocalSocketImpl:access$000	(Landroid/net/LocalSocketImpl;)Ljava/io/FileDescriptor;
            //     17: astore_3
            //     18: aload_3
            //     19: ifnonnull +18 -> 37
            //     22: new 20	java/io/IOException
            //     25: dup
            //     26: ldc 38
            //     28: invokespecial 41	java/io/IOException:<init>	(Ljava/lang/String;)V
            //     31: athrow
            //     32: astore_2
            //     33: aload_1
            //     34: monitorexit
            //     35: aload_2
            //     36: athrow
            //     37: aload_0
            //     38: getfield 13	android/net/LocalSocketImpl$SocketInputStream:this$0	Landroid/net/LocalSocketImpl;
            //     41: aload_3
            //     42: invokestatic 44	android/net/LocalSocketImpl:access$300	(Landroid/net/LocalSocketImpl;Ljava/io/FileDescriptor;)I
            //     45: istore 4
            //     47: aload_1
            //     48: monitorexit
            //     49: iload 4
            //     51: ireturn
            //
            // Exception table:
            //     from	to	target	type
            //     10	35	32	finally
            //     37	49	32	finally
        }

        public int read(byte[] paramArrayOfByte)
            throws IOException
        {
            return read(paramArrayOfByte, 0, paramArrayOfByte.length);
        }

        // ERROR //
        public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
            throws IOException
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 13	android/net/LocalSocketImpl$SocketInputStream:this$0	Landroid/net/LocalSocketImpl;
            //     4: invokestatic 36	android/net/LocalSocketImpl:access$200	(Landroid/net/LocalSocketImpl;)Ljava/lang/Object;
            //     7: astore 4
            //     9: aload 4
            //     11: monitorenter
            //     12: aload_0
            //     13: getfield 13	android/net/LocalSocketImpl$SocketInputStream:this$0	Landroid/net/LocalSocketImpl;
            //     16: invokestatic 24	android/net/LocalSocketImpl:access$000	(Landroid/net/LocalSocketImpl;)Ljava/io/FileDescriptor;
            //     19: astore 6
            //     21: aload 6
            //     23: ifnonnull +21 -> 44
            //     26: new 20	java/io/IOException
            //     29: dup
            //     30: ldc 38
            //     32: invokespecial 41	java/io/IOException:<init>	(Ljava/lang/String;)V
            //     35: athrow
            //     36: astore 5
            //     38: aload 4
            //     40: monitorexit
            //     41: aload 5
            //     43: athrow
            //     44: iload_2
            //     45: iflt +19 -> 64
            //     48: iload_3
            //     49: iflt +15 -> 64
            //     52: iload_2
            //     53: iload_3
            //     54: iadd
            //     55: istore 7
            //     57: iload 7
            //     59: aload_1
            //     60: arraylength
            //     61: if_icmple +11 -> 72
            //     64: new 50	java/lang/ArrayIndexOutOfBoundsException
            //     67: dup
            //     68: invokespecial 51	java/lang/ArrayIndexOutOfBoundsException:<init>	()V
            //     71: athrow
            //     72: aload_0
            //     73: getfield 13	android/net/LocalSocketImpl$SocketInputStream:this$0	Landroid/net/LocalSocketImpl;
            //     76: aload_1
            //     77: iload_2
            //     78: iload_3
            //     79: aload 6
            //     81: invokestatic 55	android/net/LocalSocketImpl:access$400	(Landroid/net/LocalSocketImpl;[BIILjava/io/FileDescriptor;)I
            //     84: istore 8
            //     86: aload 4
            //     88: monitorexit
            //     89: iload 8
            //     91: ireturn
            //
            // Exception table:
            //     from	to	target	type
            //     12	41	36	finally
            //     57	89	36	finally
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.LocalSocketImpl
 * JD-Core Version:        0.6.2
 */