package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

public class LinkProperties
    implements Parcelable
{
    public static final Parcelable.Creator<LinkProperties> CREATOR = new Parcelable.Creator()
    {
        public LinkProperties createFromParcel(Parcel paramAnonymousParcel)
        {
            LinkProperties localLinkProperties = new LinkProperties();
            String str = paramAnonymousParcel.readString();
            if (str != null);
            try
            {
                localLinkProperties.setInterfaceName(str);
                int i = paramAnonymousParcel.readInt();
                for (int j = 0; j < i; j++)
                    localLinkProperties.addLinkAddress((LinkAddress)paramAnonymousParcel.readParcelable(null));
            }
            catch (Exception localException)
            {
                localLinkProperties = null;
            }
            while (true)
            {
                return localLinkProperties;
                int k = paramAnonymousParcel.readInt();
                int m = 0;
                label71: if (m < k);
                try
                {
                    localLinkProperties.addDns(InetAddress.getByAddress(paramAnonymousParcel.createByteArray()));
                    label89: m++;
                    break label71;
                    int n = paramAnonymousParcel.readInt();
                    for (int i1 = 0; i1 < n; i1++)
                        localLinkProperties.addRoute((RouteInfo)paramAnonymousParcel.readParcelable(null));
                    if (paramAnonymousParcel.readByte() != 1)
                        continue;
                    localLinkProperties.setHttpProxy((ProxyProperties)paramAnonymousParcel.readParcelable(null));
                }
                catch (UnknownHostException localUnknownHostException)
                {
                    break label89;
                }
            }
        }

        public LinkProperties[] newArray(int paramAnonymousInt)
        {
            return new LinkProperties[paramAnonymousInt];
        }
    };
    private Collection<InetAddress> mDnses = new ArrayList();
    private ProxyProperties mHttpProxy;
    String mIfaceName;
    private Collection<LinkAddress> mLinkAddresses = new ArrayList();
    private Collection<RouteInfo> mRoutes = new ArrayList();

    public LinkProperties()
    {
        clear();
    }

    public LinkProperties(LinkProperties paramLinkProperties)
    {
        if (paramLinkProperties != null)
        {
            this.mIfaceName = paramLinkProperties.getInterfaceName();
            Iterator localIterator1 = paramLinkProperties.getLinkAddresses().iterator();
            while (localIterator1.hasNext())
            {
                LinkAddress localLinkAddress = (LinkAddress)localIterator1.next();
                this.mLinkAddresses.add(localLinkAddress);
            }
            Iterator localIterator2 = paramLinkProperties.getDnses().iterator();
            while (localIterator2.hasNext())
            {
                InetAddress localInetAddress = (InetAddress)localIterator2.next();
                this.mDnses.add(localInetAddress);
            }
            Iterator localIterator3 = paramLinkProperties.getRoutes().iterator();
            while (localIterator3.hasNext())
            {
                RouteInfo localRouteInfo = (RouteInfo)localIterator3.next();
                this.mRoutes.add(localRouteInfo);
            }
            if (paramLinkProperties.getHttpProxy() != null)
                break label204;
        }
        label204: for (ProxyProperties localProxyProperties = null; ; localProxyProperties = new ProxyProperties(paramLinkProperties.getHttpProxy()))
        {
            this.mHttpProxy = localProxyProperties;
            return;
        }
    }

    public void addDns(InetAddress paramInetAddress)
    {
        if (paramInetAddress != null)
            this.mDnses.add(paramInetAddress);
    }

    public void addLinkAddress(LinkAddress paramLinkAddress)
    {
        if (paramLinkAddress != null)
            this.mLinkAddresses.add(paramLinkAddress);
    }

    public void addRoute(RouteInfo paramRouteInfo)
    {
        if (paramRouteInfo != null)
            this.mRoutes.add(paramRouteInfo);
    }

    public void clear()
    {
        this.mIfaceName = null;
        this.mLinkAddresses.clear();
        this.mDnses.clear();
        this.mRoutes.clear();
        this.mHttpProxy = null;
    }

    public CompareResult<LinkAddress> compareAddresses(LinkProperties paramLinkProperties)
    {
        CompareResult localCompareResult = new CompareResult();
        localCompareResult.removed = new ArrayList(this.mLinkAddresses);
        localCompareResult.added.clear();
        if (paramLinkProperties != null)
        {
            Iterator localIterator = paramLinkProperties.getLinkAddresses().iterator();
            while (localIterator.hasNext())
            {
                LinkAddress localLinkAddress = (LinkAddress)localIterator.next();
                if (!localCompareResult.removed.remove(localLinkAddress))
                    localCompareResult.added.add(localLinkAddress);
            }
        }
        return localCompareResult;
    }

    public CompareResult<InetAddress> compareDnses(LinkProperties paramLinkProperties)
    {
        CompareResult localCompareResult = new CompareResult();
        localCompareResult.removed = new ArrayList(this.mDnses);
        localCompareResult.added.clear();
        if (paramLinkProperties != null)
        {
            Iterator localIterator = paramLinkProperties.getDnses().iterator();
            while (localIterator.hasNext())
            {
                InetAddress localInetAddress = (InetAddress)localIterator.next();
                if (!localCompareResult.removed.remove(localInetAddress))
                    localCompareResult.added.add(localInetAddress);
            }
        }
        return localCompareResult;
    }

    public CompareResult<RouteInfo> compareRoutes(LinkProperties paramLinkProperties)
    {
        CompareResult localCompareResult = new CompareResult();
        localCompareResult.removed = new ArrayList(this.mRoutes);
        localCompareResult.added.clear();
        if (paramLinkProperties != null)
        {
            Iterator localIterator = paramLinkProperties.getRoutes().iterator();
            while (localIterator.hasNext())
            {
                RouteInfo localRouteInfo = (RouteInfo)localIterator.next();
                if (!localCompareResult.removed.remove(localRouteInfo))
                    localCompareResult.added.add(localRouteInfo);
            }
        }
        return localCompareResult;
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = true;
        if (this == paramObject);
        while (true)
        {
            return bool;
            if (!(paramObject instanceof LinkProperties))
            {
                bool = false;
            }
            else
            {
                LinkProperties localLinkProperties = (LinkProperties)paramObject;
                if ((!isIdenticalInterfaceName(localLinkProperties)) || (!isIdenticalAddresses(localLinkProperties)) || (!isIdenticalDnses(localLinkProperties)) || (!isIdenticalRoutes(localLinkProperties)) || (!isIdenticalHttpProxy(localLinkProperties)))
                    bool = false;
            }
        }
    }

    public Collection<InetAddress> getAddresses()
    {
        ArrayList localArrayList = new ArrayList();
        Iterator localIterator = this.mLinkAddresses.iterator();
        while (localIterator.hasNext())
            localArrayList.add(((LinkAddress)localIterator.next()).getAddress());
        return Collections.unmodifiableCollection(localArrayList);
    }

    public Collection<InetAddress> getDnses()
    {
        return Collections.unmodifiableCollection(this.mDnses);
    }

    public ProxyProperties getHttpProxy()
    {
        return this.mHttpProxy;
    }

    public String getInterfaceName()
    {
        return this.mIfaceName;
    }

    public Collection<LinkAddress> getLinkAddresses()
    {
        return Collections.unmodifiableCollection(this.mLinkAddresses);
    }

    public Collection<RouteInfo> getRoutes()
    {
        return Collections.unmodifiableCollection(this.mRoutes);
    }

    public int hashCode()
    {
        int i = 0;
        if (this.mIfaceName == null)
            return i;
        int j = this.mIfaceName.hashCode() + 31 * this.mLinkAddresses.size() + 37 * this.mDnses.size() + 41 * this.mRoutes.size();
        if (this.mHttpProxy == null);
        while (true)
        {
            i += j;
            break;
            i = this.mHttpProxy.hashCode();
        }
    }

    public boolean isIdenticalAddresses(LinkProperties paramLinkProperties)
    {
        Collection localCollection1 = paramLinkProperties.getAddresses();
        Collection localCollection2 = getAddresses();
        if (localCollection2.size() == localCollection1.size());
        for (boolean bool = localCollection2.containsAll(localCollection1); ; bool = false)
            return bool;
    }

    public boolean isIdenticalDnses(LinkProperties paramLinkProperties)
    {
        Collection localCollection = paramLinkProperties.getDnses();
        if (this.mDnses.size() == localCollection.size());
        for (boolean bool = this.mDnses.containsAll(localCollection); ; bool = false)
            return bool;
    }

    public boolean isIdenticalHttpProxy(LinkProperties paramLinkProperties)
    {
        boolean bool;
        if (getHttpProxy() == null)
            if (paramLinkProperties.getHttpProxy() == null)
                bool = true;
        while (true)
        {
            return bool;
            bool = false;
            continue;
            bool = getHttpProxy().equals(paramLinkProperties.getHttpProxy());
        }
    }

    public boolean isIdenticalInterfaceName(LinkProperties paramLinkProperties)
    {
        return TextUtils.equals(getInterfaceName(), paramLinkProperties.getInterfaceName());
    }

    public boolean isIdenticalRoutes(LinkProperties paramLinkProperties)
    {
        Collection localCollection = paramLinkProperties.getRoutes();
        if (this.mRoutes.size() == localCollection.size());
        for (boolean bool = this.mRoutes.containsAll(localCollection); ; bool = false)
            return bool;
    }

    public void setHttpProxy(ProxyProperties paramProxyProperties)
    {
        this.mHttpProxy = paramProxyProperties;
    }

    public void setInterfaceName(String paramString)
    {
        this.mIfaceName = paramString;
    }

    public String toString()
    {
        if (this.mIfaceName == null);
        String str2;
        for (String str1 = ""; ; str1 = "InterfaceName: " + this.mIfaceName + " ")
        {
            str2 = "LinkAddresses: [";
            Iterator localIterator1 = this.mLinkAddresses.iterator();
            while (localIterator1.hasNext())
            {
                LinkAddress localLinkAddress = (LinkAddress)localIterator1.next();
                str2 = str2 + localLinkAddress.toString() + ",";
            }
        }
        String str3 = str2 + "] ";
        String str4 = "DnsAddresses: [";
        Iterator localIterator2 = this.mDnses.iterator();
        while (localIterator2.hasNext())
        {
            InetAddress localInetAddress = (InetAddress)localIterator2.next();
            str4 = str4 + localInetAddress.getHostAddress() + ",";
        }
        String str5 = str4 + "] ";
        String str6 = "Routes: [";
        Iterator localIterator3 = this.mRoutes.iterator();
        while (localIterator3.hasNext())
        {
            RouteInfo localRouteInfo = (RouteInfo)localIterator3.next();
            str6 = str6 + localRouteInfo.toString() + ",";
        }
        String str7 = str6 + "] ";
        if (this.mHttpProxy == null);
        for (String str8 = ""; ; str8 = "HttpProxy: " + this.mHttpProxy.toString() + " ")
            return str1 + str3 + str7 + str5 + str8;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(getInterfaceName());
        paramParcel.writeInt(this.mLinkAddresses.size());
        Iterator localIterator1 = this.mLinkAddresses.iterator();
        while (localIterator1.hasNext())
            paramParcel.writeParcelable((LinkAddress)localIterator1.next(), paramInt);
        paramParcel.writeInt(this.mDnses.size());
        Iterator localIterator2 = this.mDnses.iterator();
        while (localIterator2.hasNext())
            paramParcel.writeByteArray(((InetAddress)localIterator2.next()).getAddress());
        paramParcel.writeInt(this.mRoutes.size());
        Iterator localIterator3 = this.mRoutes.iterator();
        while (localIterator3.hasNext())
            paramParcel.writeParcelable((RouteInfo)localIterator3.next(), paramInt);
        if (this.mHttpProxy != null)
        {
            paramParcel.writeByte((byte)1);
            paramParcel.writeParcelable(this.mHttpProxy, paramInt);
        }
        while (true)
        {
            return;
            paramParcel.writeByte((byte)0);
        }
    }

    public static class CompareResult<T>
    {
        public Collection<T> added = new ArrayList();
        public Collection<T> removed = new ArrayList();

        public String toString()
        {
            String str1 = "removed=[";
            Iterator localIterator1 = this.removed.iterator();
            while (localIterator1.hasNext())
            {
                Object localObject2 = localIterator1.next();
                str1 = str1 + localObject2.toString() + ",";
            }
            String str2 = str1 + "] added=[";
            Iterator localIterator2 = this.added.iterator();
            while (localIterator2.hasNext())
            {
                Object localObject1 = localIterator2.next();
                str2 = str2 + localObject1.toString() + ",";
            }
            return str2 + "]";
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.LinkProperties
 * JD-Core Version:        0.6.2
 */