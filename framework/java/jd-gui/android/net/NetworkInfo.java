package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.EnumMap;

public class NetworkInfo
    implements Parcelable
{
    public static final Parcelable.Creator<NetworkInfo> CREATOR = new Parcelable.Creator()
    {
        public NetworkInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            boolean bool1 = true;
            NetworkInfo localNetworkInfo = new NetworkInfo(paramAnonymousParcel.readInt(), paramAnonymousParcel.readInt(), paramAnonymousParcel.readString(), paramAnonymousParcel.readString());
            NetworkInfo.access$002(localNetworkInfo, NetworkInfo.State.valueOf(paramAnonymousParcel.readString()));
            NetworkInfo.access$102(localNetworkInfo, NetworkInfo.DetailedState.valueOf(paramAnonymousParcel.readString()));
            boolean bool2;
            boolean bool3;
            if (paramAnonymousParcel.readInt() != 0)
            {
                bool2 = bool1;
                NetworkInfo.access$202(localNetworkInfo, bool2);
                if (paramAnonymousParcel.readInt() == 0)
                    break label123;
                bool3 = bool1;
                label77: NetworkInfo.access$302(localNetworkInfo, bool3);
                if (paramAnonymousParcel.readInt() == 0)
                    break label129;
            }
            while (true)
            {
                NetworkInfo.access$402(localNetworkInfo, bool1);
                NetworkInfo.access$502(localNetworkInfo, paramAnonymousParcel.readString());
                NetworkInfo.access$602(localNetworkInfo, paramAnonymousParcel.readString());
                return localNetworkInfo;
                bool2 = false;
                break;
                label123: bool3 = false;
                break label77;
                label129: bool1 = false;
            }
        }

        public NetworkInfo[] newArray(int paramAnonymousInt)
        {
            return new NetworkInfo[paramAnonymousInt];
        }
    };
    private static final EnumMap<DetailedState, State> stateMap = new EnumMap(DetailedState.class);
    private DetailedState mDetailedState;
    private String mExtraInfo;
    private boolean mIsAvailable;
    private boolean mIsFailover;
    private boolean mIsRoaming;
    private int mNetworkType;
    private String mReason;
    private State mState;
    private int mSubtype;
    private String mSubtypeName;
    private String mTypeName;

    static
    {
        stateMap.put(DetailedState.IDLE, State.DISCONNECTED);
        stateMap.put(DetailedState.SCANNING, State.DISCONNECTED);
        stateMap.put(DetailedState.CONNECTING, State.CONNECTING);
        stateMap.put(DetailedState.AUTHENTICATING, State.CONNECTING);
        stateMap.put(DetailedState.OBTAINING_IPADDR, State.CONNECTING);
        stateMap.put(DetailedState.VERIFYING_POOR_LINK, State.CONNECTING);
        stateMap.put(DetailedState.CONNECTED, State.CONNECTED);
        stateMap.put(DetailedState.SUSPENDED, State.SUSPENDED);
        stateMap.put(DetailedState.DISCONNECTING, State.DISCONNECTING);
        stateMap.put(DetailedState.DISCONNECTED, State.DISCONNECTED);
        stateMap.put(DetailedState.FAILED, State.DISCONNECTED);
        stateMap.put(DetailedState.BLOCKED, State.DISCONNECTED);
    }

    public NetworkInfo(int paramInt)
    {
    }

    public NetworkInfo(int paramInt1, int paramInt2, String paramString1, String paramString2)
    {
        if (!ConnectivityManager.isNetworkTypeValid(paramInt1))
            throw new IllegalArgumentException("Invalid network type: " + paramInt1);
        this.mNetworkType = paramInt1;
        this.mSubtype = paramInt2;
        this.mTypeName = paramString1;
        this.mSubtypeName = paramString2;
        setDetailedState(DetailedState.IDLE, null, null);
        this.mState = State.UNKNOWN;
        this.mIsAvailable = false;
        this.mIsRoaming = false;
    }

    public NetworkInfo(NetworkInfo paramNetworkInfo)
    {
        if (paramNetworkInfo != null)
        {
            this.mNetworkType = paramNetworkInfo.mNetworkType;
            this.mSubtype = paramNetworkInfo.mSubtype;
            this.mTypeName = paramNetworkInfo.mTypeName;
            this.mSubtypeName = paramNetworkInfo.mSubtypeName;
            this.mState = paramNetworkInfo.mState;
            this.mDetailedState = paramNetworkInfo.mDetailedState;
            this.mReason = paramNetworkInfo.mReason;
            this.mExtraInfo = paramNetworkInfo.mExtraInfo;
            this.mIsFailover = paramNetworkInfo.mIsFailover;
            this.mIsRoaming = paramNetworkInfo.mIsRoaming;
            this.mIsAvailable = paramNetworkInfo.mIsAvailable;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public DetailedState getDetailedState()
    {
        try
        {
            DetailedState localDetailedState = this.mDetailedState;
            return localDetailedState;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public String getExtraInfo()
    {
        try
        {
            String str = this.mExtraInfo;
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public String getReason()
    {
        try
        {
            String str = this.mReason;
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public State getState()
    {
        try
        {
            State localState = this.mState;
            return localState;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public int getSubtype()
    {
        try
        {
            int i = this.mSubtype;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public String getSubtypeName()
    {
        try
        {
            String str = this.mSubtypeName;
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public int getType()
    {
        try
        {
            int i = this.mNetworkType;
            return i;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public String getTypeName()
    {
        try
        {
            String str = this.mTypeName;
            return str;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public boolean isAvailable()
    {
        try
        {
            boolean bool = this.mIsAvailable;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public boolean isConnected()
    {
        while (true)
        {
            try
            {
                if (this.mState == State.CONNECTED)
                {
                    bool = true;
                    return bool;
                }
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
            boolean bool = false;
        }
    }

    public boolean isConnectedOrConnecting()
    {
        while (true)
        {
            try
            {
                if (this.mState != State.CONNECTED)
                {
                    if (this.mState != State.CONNECTING)
                        break label39;
                    break label34;
                    return bool;
                }
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
            label34: boolean bool = true;
            continue;
            label39: bool = false;
        }
    }

    public boolean isFailover()
    {
        try
        {
            boolean bool = this.mIsFailover;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public boolean isRoaming()
    {
        try
        {
            boolean bool = this.mIsRoaming;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void setDetailedState(DetailedState paramDetailedState, String paramString1, String paramString2)
    {
        try
        {
            this.mDetailedState = paramDetailedState;
            this.mState = ((State)stateMap.get(paramDetailedState));
            this.mReason = paramString1;
            this.mExtraInfo = paramString2;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void setExtraInfo(String paramString)
    {
        try
        {
            this.mExtraInfo = paramString;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void setFailover(boolean paramBoolean)
    {
        try
        {
            this.mIsFailover = paramBoolean;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void setIsAvailable(boolean paramBoolean)
    {
        try
        {
            this.mIsAvailable = paramBoolean;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    void setRoaming(boolean paramBoolean)
    {
        try
        {
            this.mIsRoaming = paramBoolean;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    void setSubtype(int paramInt, String paramString)
    {
        try
        {
            this.mSubtype = paramInt;
            this.mSubtypeName = paramString;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public String toString()
    {
        try
        {
            StringBuilder localStringBuilder1 = new StringBuilder("NetworkInfo: ");
            StringBuilder localStringBuilder2 = localStringBuilder1.append("type: ").append(getTypeName()).append("[").append(getSubtypeName()).append("], state: ").append(this.mState).append("/").append(this.mDetailedState).append(", reason: ");
            if (this.mReason == null);
            for (String str1 = "(unspecified)"; ; str1 = this.mReason)
            {
                StringBuilder localStringBuilder3 = localStringBuilder2.append(str1).append(", extra: ");
                if (this.mExtraInfo != null)
                    break;
                str2 = "(none)";
                localStringBuilder3.append(str2).append(", roaming: ").append(this.mIsRoaming).append(", failover: ").append(this.mIsFailover).append(", isAvailable: ").append(this.mIsAvailable);
                String str3 = localStringBuilder1.toString();
                return str3;
            }
            String str2 = this.mExtraInfo;
        }
        finally
        {
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        int i = 1;
        while (true)
        {
            try
            {
                paramParcel.writeInt(this.mNetworkType);
                paramParcel.writeInt(this.mSubtype);
                paramParcel.writeString(this.mTypeName);
                paramParcel.writeString(this.mSubtypeName);
                paramParcel.writeString(this.mState.name());
                paramParcel.writeString(this.mDetailedState.name());
                if (this.mIsFailover)
                {
                    j = i;
                    paramParcel.writeInt(j);
                    if (!this.mIsAvailable)
                        break label134;
                    k = i;
                    paramParcel.writeInt(k);
                    if (!this.mIsRoaming)
                        break label140;
                    paramParcel.writeInt(i);
                    paramParcel.writeString(this.mReason);
                    paramParcel.writeString(this.mExtraInfo);
                    return;
                }
            }
            finally
            {
                localObject = finally;
                throw localObject;
            }
            int j = 0;
            continue;
            label134: int k = 0;
            continue;
            label140: i = 0;
        }
    }

    public static enum DetailedState
    {
        static
        {
            CONNECTING = new DetailedState("CONNECTING", 2);
            AUTHENTICATING = new DetailedState("AUTHENTICATING", 3);
            OBTAINING_IPADDR = new DetailedState("OBTAINING_IPADDR", 4);
            CONNECTED = new DetailedState("CONNECTED", 5);
            SUSPENDED = new DetailedState("SUSPENDED", 6);
            DISCONNECTING = new DetailedState("DISCONNECTING", 7);
            DISCONNECTED = new DetailedState("DISCONNECTED", 8);
            FAILED = new DetailedState("FAILED", 9);
            BLOCKED = new DetailedState("BLOCKED", 10);
            VERIFYING_POOR_LINK = new DetailedState("VERIFYING_POOR_LINK", 11);
            DetailedState[] arrayOfDetailedState = new DetailedState[12];
            arrayOfDetailedState[0] = IDLE;
            arrayOfDetailedState[1] = SCANNING;
            arrayOfDetailedState[2] = CONNECTING;
            arrayOfDetailedState[3] = AUTHENTICATING;
            arrayOfDetailedState[4] = OBTAINING_IPADDR;
            arrayOfDetailedState[5] = CONNECTED;
            arrayOfDetailedState[6] = SUSPENDED;
            arrayOfDetailedState[7] = DISCONNECTING;
            arrayOfDetailedState[8] = DISCONNECTED;
            arrayOfDetailedState[9] = FAILED;
            arrayOfDetailedState[10] = BLOCKED;
            arrayOfDetailedState[11] = VERIFYING_POOR_LINK;
        }
    }

    public static enum State
    {
        static
        {
            CONNECTED = new State("CONNECTED", 1);
            SUSPENDED = new State("SUSPENDED", 2);
            DISCONNECTING = new State("DISCONNECTING", 3);
            DISCONNECTED = new State("DISCONNECTED", 4);
            UNKNOWN = new State("UNKNOWN", 5);
            State[] arrayOfState = new State[6];
            arrayOfState[0] = CONNECTING;
            arrayOfState[1] = CONNECTED;
            arrayOfState[2] = SUSPENDED;
            arrayOfState[3] = DISCONNECTING;
            arrayOfState[4] = DISCONNECTED;
            arrayOfState[5] = UNKNOWN;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.NetworkInfo
 * JD-Core Version:        0.6.2
 */