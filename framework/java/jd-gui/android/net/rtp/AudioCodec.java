package android.net.rtp;

import java.util.Arrays;

public class AudioCodec
{
    public static final AudioCodec AMR;
    public static final AudioCodec GSM;
    public static final AudioCodec GSM_EFR;
    public static final AudioCodec PCMA;
    public static final AudioCodec PCMU = new AudioCodec(0, "PCMU/8000", null);
    private static final AudioCodec[] sCodecs = arrayOfAudioCodec;
    public final String fmtp;
    public final String rtpmap;
    public final int type;

    static
    {
        PCMA = new AudioCodec(8, "PCMA/8000", null);
        GSM = new AudioCodec(3, "GSM/8000", null);
        GSM_EFR = new AudioCodec(96, "GSM-EFR/8000", null);
        AMR = new AudioCodec(97, "AMR/8000", null);
        AudioCodec[] arrayOfAudioCodec = new AudioCodec[5];
        arrayOfAudioCodec[0] = GSM_EFR;
        arrayOfAudioCodec[1] = AMR;
        arrayOfAudioCodec[2] = GSM;
        arrayOfAudioCodec[3] = PCMU;
        arrayOfAudioCodec[4] = PCMA;
    }

    private AudioCodec(int paramInt, String paramString1, String paramString2)
    {
        this.type = paramInt;
        this.rtpmap = paramString1;
        this.fmtp = paramString2;
    }

    public static AudioCodec getCodec(int paramInt, String paramString1, String paramString2)
    {
        AudioCodec localAudioCodec1 = null;
        if ((paramInt < 0) || (paramInt > 127))
            return localAudioCodec1;
        Object localObject = null;
        int m;
        if (paramString1 != null)
        {
            String str2 = paramString1.trim().toUpperCase();
            AudioCodec[] arrayOfAudioCodec2 = sCodecs;
            int k = arrayOfAudioCodec2.length;
            m = 0;
            label43: if (m < k)
            {
                AudioCodec localAudioCodec3 = arrayOfAudioCodec2[m];
                if (!str2.startsWith(localAudioCodec3.rtpmap))
                    break label174;
                String str3 = str2.substring(localAudioCodec3.rtpmap.length());
                if ((str3.length() == 0) || (str3.equals("/1")))
                    localObject = localAudioCodec3;
            }
        }
        label107: label239: 
        while (true)
        {
            AudioCodec[] arrayOfAudioCodec1;
            int i;
            if (localObject != null)
            {
                if ((localObject == AMR) && (paramString2 != null))
                {
                    String str1 = paramString2.toLowerCase();
                    if ((str1.contains("crc=1")) || (str1.contains("robust-sorting=1")) || (str1.contains("interleaving=")))
                        break;
                }
                localAudioCodec1 = new AudioCodec(paramInt, paramString1, paramString2);
                break;
                m++;
                break label43;
                if (paramInt >= 96)
                    continue;
                arrayOfAudioCodec1 = sCodecs;
                i = arrayOfAudioCodec1.length;
            }
            for (int j = 0; ; j++)
            {
                if (j >= i)
                    break label239;
                AudioCodec localAudioCodec2 = arrayOfAudioCodec1[j];
                if (paramInt == localAudioCodec2.type)
                {
                    localObject = localAudioCodec2;
                    paramString1 = localAudioCodec2.rtpmap;
                    break label107;
                    break;
                }
            }
        }
    }

    public static AudioCodec[] getCodecs()
    {
        return (AudioCodec[])Arrays.copyOf(sCodecs, sCodecs.length);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.rtp.AudioCodec
 * JD-Core Version:        0.6.2
 */