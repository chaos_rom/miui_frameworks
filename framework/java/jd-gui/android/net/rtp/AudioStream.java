package android.net.rtp;

import java.net.InetAddress;
import java.net.SocketException;

public class AudioStream extends RtpStream
{
    private AudioCodec mCodec;
    private int mDtmfType = -1;
    private AudioGroup mGroup;

    public AudioStream(InetAddress paramInetAddress)
        throws SocketException
    {
        super(paramInetAddress);
    }

    public AudioCodec getCodec()
    {
        return this.mCodec;
    }

    public int getDtmfType()
    {
        return this.mDtmfType;
    }

    public AudioGroup getGroup()
    {
        return this.mGroup;
    }

    public final boolean isBusy()
    {
        if (this.mGroup != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void join(AudioGroup paramAudioGroup)
    {
        try
        {
            if (this.mGroup == paramAudioGroup)
                return;
            if (this.mGroup != null)
            {
                this.mGroup.remove(this);
                this.mGroup = null;
            }
            if (paramAudioGroup != null)
            {
                paramAudioGroup.add(this);
                this.mGroup = paramAudioGroup;
            }
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void setCodec(AudioCodec paramAudioCodec)
    {
        if (isBusy())
            throw new IllegalStateException("Busy");
        if (paramAudioCodec.type == this.mDtmfType)
            throw new IllegalArgumentException("The type is used by DTMF");
        this.mCodec = paramAudioCodec;
    }

    public void setDtmfType(int paramInt)
    {
        if (isBusy())
            throw new IllegalStateException("Busy");
        if (paramInt != -1)
        {
            if ((paramInt < 96) || (paramInt > 127))
                throw new IllegalArgumentException("Invalid type");
            if ((this.mCodec != null) && (paramInt == this.mCodec.type))
                throw new IllegalArgumentException("The type is used by codec");
        }
        this.mDtmfType = paramInt;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.rtp.AudioStream
 * JD-Core Version:        0.6.2
 */