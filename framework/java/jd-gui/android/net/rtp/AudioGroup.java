package android.net.rtp;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class AudioGroup
{
    public static final int MODE_ECHO_SUPPRESSION = 3;
    private static final int MODE_LAST = 3;
    public static final int MODE_MUTED = 1;
    public static final int MODE_NORMAL = 2;
    public static final int MODE_ON_HOLD;
    private int mMode = 0;
    private int mNative;
    private final Map<AudioStream, Integer> mStreams = new HashMap();

    static
    {
        System.loadLibrary("rtp_jni");
    }

    private native int nativeAdd(int paramInt1, int paramInt2, String paramString1, int paramInt3, String paramString2, int paramInt4);

    private native void nativeRemove(int paramInt);

    private native void nativeSendDtmf(int paramInt);

    private native void nativeSetMode(int paramInt);

    /** @deprecated */
    void add(AudioStream paramAudioStream)
    {
        try
        {
            boolean bool = this.mStreams.containsKey(paramAudioStream);
            if (!bool);
            try
            {
                AudioCodec localAudioCodec = paramAudioStream.getCodec();
                Object[] arrayOfObject = new Object[3];
                arrayOfObject[0] = Integer.valueOf(localAudioCodec.type);
                arrayOfObject[1] = localAudioCodec.rtpmap;
                arrayOfObject[2] = localAudioCodec.fmtp;
                String str = String.format("%d %s %s", arrayOfObject);
                int i = nativeAdd(paramAudioStream.getMode(), paramAudioStream.getSocket(), paramAudioStream.getRemoteAddress().getHostAddress(), paramAudioStream.getRemotePort(), str, paramAudioStream.getDtmfType());
                this.mStreams.put(paramAudioStream, Integer.valueOf(i));
                return;
            }
            catch (NullPointerException localNullPointerException)
            {
                throw new IllegalStateException(localNullPointerException);
            }
        }
        finally
        {
        }
    }

    public void clear()
    {
        AudioStream[] arrayOfAudioStream = getStreams();
        int i = arrayOfAudioStream.length;
        for (int j = 0; j < i; j++)
            arrayOfAudioStream[j].join(null);
    }

    protected void finalize()
        throws Throwable
    {
        nativeRemove(0);
        super.finalize();
    }

    public int getMode()
    {
        return this.mMode;
    }

    public AudioStream[] getStreams()
    {
        try
        {
            AudioStream[] arrayOfAudioStream = (AudioStream[])this.mStreams.keySet().toArray(new AudioStream[this.mStreams.size()]);
            return arrayOfAudioStream;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    void remove(AudioStream paramAudioStream)
    {
        try
        {
            Integer localInteger = (Integer)this.mStreams.remove(paramAudioStream);
            if (localInteger != null)
                nativeRemove(localInteger.intValue());
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void sendDtmf(int paramInt)
    {
        if ((paramInt < 0) || (paramInt > 15))
            throw new IllegalArgumentException("Invalid event");
        try
        {
            nativeSendDtmf(paramInt);
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public void setMode(int paramInt)
    {
        if ((paramInt < 0) || (paramInt > 3))
            throw new IllegalArgumentException("Invalid mode");
        try
        {
            nativeSetMode(paramInt);
            this.mMode = paramInt;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.rtp.AudioGroup
 * JD-Core Version:        0.6.2
 */