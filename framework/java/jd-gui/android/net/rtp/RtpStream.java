package android.net.rtp;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.SocketException;

public class RtpStream
{
    private static final int MODE_LAST = 2;
    public static final int MODE_NORMAL = 0;
    public static final int MODE_RECEIVE_ONLY = 2;
    public static final int MODE_SEND_ONLY = 1;
    private final InetAddress mLocalAddress;
    private final int mLocalPort = create(paramInetAddress.getHostAddress());
    private int mMode = 0;
    private InetAddress mRemoteAddress;
    private int mRemotePort = -1;
    private int mSocket = -1;

    static
    {
        System.loadLibrary("rtp_jni");
    }

    RtpStream(InetAddress paramInetAddress)
        throws SocketException
    {
        this.mLocalAddress = paramInetAddress;
    }

    private native void close();

    private native int create(String paramString)
        throws SocketException;

    public void associate(InetAddress paramInetAddress, int paramInt)
    {
        if (isBusy())
            throw new IllegalStateException("Busy");
        if (((!(paramInetAddress instanceof Inet4Address)) || (!(this.mLocalAddress instanceof Inet4Address))) && ((!(paramInetAddress instanceof Inet6Address)) || (!(this.mLocalAddress instanceof Inet6Address))))
            throw new IllegalArgumentException("Unsupported address");
        if ((paramInt < 0) || (paramInt > 65535))
            throw new IllegalArgumentException("Invalid port");
        this.mRemoteAddress = paramInetAddress;
        this.mRemotePort = paramInt;
    }

    protected void finalize()
        throws Throwable
    {
        close();
        super.finalize();
    }

    public InetAddress getLocalAddress()
    {
        return this.mLocalAddress;
    }

    public int getLocalPort()
    {
        return this.mLocalPort;
    }

    public int getMode()
    {
        return this.mMode;
    }

    public InetAddress getRemoteAddress()
    {
        return this.mRemoteAddress;
    }

    public int getRemotePort()
    {
        return this.mRemotePort;
    }

    int getSocket()
    {
        return this.mSocket;
    }

    public boolean isBusy()
    {
        return false;
    }

    // ERROR //
    public void release()
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: invokevirtual 62	android/net/rtp/RtpStream:isBusy	()Z
        //     6: ifeq +18 -> 24
        //     9: new 64	java/lang/IllegalStateException
        //     12: dup
        //     13: ldc 66
        //     15: invokespecial 68	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
        //     18: athrow
        //     19: astore_1
        //     20: aload_0
        //     21: monitorexit
        //     22: aload_1
        //     23: athrow
        //     24: aload_0
        //     25: invokespecial 87	android/net/rtp/RtpStream:close	()V
        //     28: aload_0
        //     29: monitorexit
        //     30: return
        //
        // Exception table:
        //     from	to	target	type
        //     2	22	19	finally
        //     24	30	19	finally
    }

    public void setMode(int paramInt)
    {
        if (isBusy())
            throw new IllegalStateException("Busy");
        if ((paramInt < 0) || (paramInt > 2))
            throw new IllegalArgumentException("Invalid mode");
        this.mMode = paramInt;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.rtp.RtpStream
 * JD-Core Version:        0.6.2
 */