package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;

public class ProxyProperties
    implements Parcelable
{
    public static final Parcelable.Creator<ProxyProperties> CREATOR = new Parcelable.Creator()
    {
        public ProxyProperties createFromParcel(Parcel paramAnonymousParcel)
        {
            String str = null;
            int i = 0;
            if (paramAnonymousParcel.readByte() == 1)
            {
                str = paramAnonymousParcel.readString();
                i = paramAnonymousParcel.readInt();
            }
            return new ProxyProperties(str, i, paramAnonymousParcel.readString(), paramAnonymousParcel.readStringArray(), null);
        }

        public ProxyProperties[] newArray(int paramAnonymousInt)
        {
            return new ProxyProperties[paramAnonymousInt];
        }
    };
    private String mExclusionList;
    private String mHost;
    private String[] mParsedExclusionList;
    private int mPort;

    public ProxyProperties(ProxyProperties paramProxyProperties)
    {
        if (paramProxyProperties != null)
        {
            this.mHost = paramProxyProperties.getHost();
            this.mPort = paramProxyProperties.getPort();
            this.mExclusionList = paramProxyProperties.getExclusionList();
            this.mParsedExclusionList = paramProxyProperties.mParsedExclusionList;
        }
    }

    public ProxyProperties(String paramString1, int paramInt, String paramString2)
    {
        this.mHost = paramString1;
        this.mPort = paramInt;
        setExclusionList(paramString2);
    }

    private ProxyProperties(String paramString1, int paramInt, String paramString2, String[] paramArrayOfString)
    {
        this.mHost = paramString1;
        this.mPort = paramInt;
        this.mExclusionList = paramString2;
        this.mParsedExclusionList = paramArrayOfString;
    }

    private void setExclusionList(String paramString)
    {
        this.mExclusionList = paramString;
        if (this.mExclusionList == null)
            this.mParsedExclusionList = new String[0];
        while (true)
        {
            return;
            String[] arrayOfString = paramString.toLowerCase().split(",");
            this.mParsedExclusionList = new String[2 * arrayOfString.length];
            for (int i = 0; i < arrayOfString.length; i++)
            {
                String str = arrayOfString[i].trim();
                if (str.startsWith("."))
                    str = str.substring(1);
                this.mParsedExclusionList[(i * 2)] = str;
                this.mParsedExclusionList[(1 + i * 2)] = ("." + str);
            }
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = false;
        if (!(paramObject instanceof ProxyProperties));
        while (true)
        {
            return bool;
            ProxyProperties localProxyProperties = (ProxyProperties)paramObject;
            if (((this.mExclusionList == null) || (this.mExclusionList.equals(localProxyProperties.getExclusionList()))) && ((this.mHost == null) || (localProxyProperties.getHost() == null) || (this.mHost.equals(localProxyProperties.getHost()))) && ((this.mHost == null) || (localProxyProperties.mHost != null)) && ((this.mHost != null) || (localProxyProperties.mHost == null)) && (this.mPort == localProxyProperties.mPort))
                bool = true;
        }
    }

    public String getExclusionList()
    {
        return this.mExclusionList;
    }

    public String getHost()
    {
        return this.mHost;
    }

    public int getPort()
    {
        return this.mPort;
    }

    public InetSocketAddress getSocketAddress()
    {
        Object localObject = null;
        try
        {
            InetSocketAddress localInetSocketAddress = new InetSocketAddress(this.mHost, this.mPort);
            localObject = localInetSocketAddress;
            label20: return localObject;
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            break label20;
        }
    }

    public int hashCode()
    {
        int i = 0;
        int j;
        if (this.mHost == null)
        {
            j = 0;
            if (this.mExclusionList != null)
                break label38;
        }
        while (true)
        {
            return j + i + this.mPort;
            j = this.mHost.hashCode();
            break;
            label38: i = this.mExclusionList.hashCode();
        }
    }

    public boolean isExcluded(String paramString)
    {
        boolean bool = false;
        if ((TextUtils.isEmpty(paramString)) || (this.mParsedExclusionList == null) || (this.mParsedExclusionList.length == 0));
        label90: 
        while (true)
        {
            return bool;
            String str = Uri.parse(paramString).getHost();
            if (str != null)
                for (int i = 0; ; i += 2)
                {
                    if (i >= this.mParsedExclusionList.length)
                        break label90;
                    if ((str.equals(this.mParsedExclusionList[i])) || (str.endsWith(this.mParsedExclusionList[(i + 1)])))
                    {
                        bool = true;
                        break;
                    }
                }
        }
    }

    public Proxy makeProxy()
    {
        Object localObject = Proxy.NO_PROXY;
        if (this.mHost != null);
        try
        {
            InetSocketAddress localInetSocketAddress = new InetSocketAddress(this.mHost, this.mPort);
            Proxy localProxy = new Proxy(Proxy.Type.HTTP, localInetSocketAddress);
            localObject = localProxy;
            label41: return localObject;
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            break label41;
        }
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        if (this.mHost != null)
        {
            localStringBuilder.append("[");
            localStringBuilder.append(this.mHost);
            localStringBuilder.append("] ");
            localStringBuilder.append(Integer.toString(this.mPort));
            if (this.mExclusionList != null)
                localStringBuilder.append(" xl=").append(this.mExclusionList);
        }
        while (true)
        {
            return localStringBuilder.toString();
            localStringBuilder.append("[ProxyProperties.mHost == null]");
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        if (this.mHost != null)
        {
            paramParcel.writeByte((byte)1);
            paramParcel.writeString(this.mHost);
            paramParcel.writeInt(this.mPort);
        }
        while (true)
        {
            paramParcel.writeString(this.mExclusionList);
            paramParcel.writeStringArray(this.mParsedExclusionList);
            return;
            paramParcel.writeByte((byte)0);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.ProxyProperties
 * JD-Core Version:        0.6.2
 */