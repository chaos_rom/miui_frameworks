package android.net;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Slog;

public class DummyDataStateTracker
    implements NetworkStateTracker
{
    private static final boolean DBG = true;
    private static final String TAG = "DummyDataStateTracker";
    private static final boolean VDBG;
    private Context mContext;
    private boolean mDefaultRouteSet = false;
    private boolean mIsDefaultOrHipri = false;
    private LinkCapabilities mLinkCapabilities;
    private LinkProperties mLinkProperties;
    private NetworkInfo mNetworkInfo;
    private boolean mPrivateDnsRouteSet = false;
    private Handler mTarget;
    private boolean mTeardownRequested = false;

    public DummyDataStateTracker(int paramInt, String paramString)
    {
        this.mNetworkInfo = new NetworkInfo(paramInt);
    }

    private static void log(String paramString)
    {
        Slog.d("DummyDataStateTracker", paramString);
    }

    private static void loge(String paramString)
    {
        Slog.e("DummyDataStateTracker", paramString);
    }

    private void setDetailedState(NetworkInfo.DetailedState paramDetailedState, String paramString1, String paramString2)
    {
        log("setDetailed state, old =" + this.mNetworkInfo.getDetailedState() + " and new state=" + paramDetailedState);
        this.mNetworkInfo.setDetailedState(paramDetailedState, paramString1, paramString2);
        this.mTarget.obtainMessage(1, this.mNetworkInfo).sendToTarget();
    }

    public void defaultRouteSet(boolean paramBoolean)
    {
        this.mDefaultRouteSet = paramBoolean;
    }

    public LinkCapabilities getLinkCapabilities()
    {
        return new LinkCapabilities(this.mLinkCapabilities);
    }

    public LinkProperties getLinkProperties()
    {
        return new LinkProperties(this.mLinkProperties);
    }

    public NetworkInfo getNetworkInfo()
    {
        return this.mNetworkInfo;
    }

    public String getTcpBufferSizesPropName()
    {
        return "net.tcp.buffersize.unknown";
    }

    public boolean isAvailable()
    {
        return true;
    }

    public boolean isDefaultRouteSet()
    {
        return this.mDefaultRouteSet;
    }

    public boolean isPrivateDnsRouteSet()
    {
        return this.mPrivateDnsRouteSet;
    }

    public boolean isTeardownRequested()
    {
        return this.mTeardownRequested;
    }

    public void privateDnsRouteSet(boolean paramBoolean)
    {
        this.mPrivateDnsRouteSet = paramBoolean;
    }

    public boolean reconnect()
    {
        setDetailedState(NetworkInfo.DetailedState.CONNECTING, "enabled", null);
        setDetailedState(NetworkInfo.DetailedState.CONNECTED, "enabled", null);
        setTeardownRequested(false);
        return true;
    }

    public void releaseWakeLock()
    {
    }

    public void setDependencyMet(boolean paramBoolean)
    {
    }

    public void setPolicyDataEnable(boolean paramBoolean)
    {
    }

    public boolean setRadio(boolean paramBoolean)
    {
        return true;
    }

    public void setTeardownRequested(boolean paramBoolean)
    {
        this.mTeardownRequested = paramBoolean;
    }

    public void setUserDataEnable(boolean paramBoolean)
    {
    }

    public void startMonitoring(Context paramContext, Handler paramHandler)
    {
        this.mTarget = paramHandler;
        this.mContext = paramContext;
    }

    public boolean teardown()
    {
        setDetailedState(NetworkInfo.DetailedState.DISCONNECTING, "disabled", null);
        setDetailedState(NetworkInfo.DetailedState.DISCONNECTED, "disabled", null);
        return true;
    }

    public String toString()
    {
        return "Dummy data state: none, dummy!";
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.DummyDataStateTracker
 * JD-Core Version:        0.6.2
 */