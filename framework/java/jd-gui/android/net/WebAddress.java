package android.net;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WebAddress
{
    static final int MATCH_GROUP_AUTHORITY = 2;
    static final int MATCH_GROUP_HOST = 3;
    static final int MATCH_GROUP_PATH = 5;
    static final int MATCH_GROUP_PORT = 4;
    static final int MATCH_GROUP_SCHEME = 1;
    static Pattern sAddressPattern = Pattern.compile("(?:(http|https|file)\\:\\/\\/)?(?:([-A-Za-z0-9$_.+!*'(),;?&=]+(?:\\:[-A-Za-z0-9$_.+!*'(),;?&=]+)?)@)?([a-zA-Z0-9 -퟿豈-﷏ﷰ-￯%_-][a-zA-Z0-9 -퟿豈-﷏ﷰ-￯%_\\.-]*|\\[[0-9a-fA-F:\\.]+\\])?(?:\\:([0-9]*))?(\\/?[^#]*)?.*", 2);
    private String mAuthInfo;
    private String mHost;
    private String mPath;
    private int mPort;
    private String mScheme;

    public WebAddress(String paramString)
        throws ParseException
    {
        if (paramString == null)
            throw new NullPointerException();
        this.mScheme = "";
        this.mHost = "";
        this.mPort = -1;
        this.mPath = "/";
        this.mAuthInfo = "";
        Matcher localMatcher = sAddressPattern.matcher(paramString);
        String str4;
        if (localMatcher.matches())
        {
            String str1 = localMatcher.group(1);
            if (str1 != null)
                this.mScheme = str1.toLowerCase();
            String str2 = localMatcher.group(2);
            if (str2 != null)
                this.mAuthInfo = str2;
            String str3 = localMatcher.group(3);
            if (str3 != null)
                this.mHost = str3;
            str4 = localMatcher.group(4);
            if ((str4 == null) || (str4.length() <= 0));
        }
        while (true)
        {
            String str5;
            try
            {
                this.mPort = Integer.parseInt(str4);
                str5 = localMatcher.group(5);
                if ((str5 != null) && (str5.length() > 0))
                {
                    if (str5.charAt(0) == '/')
                        this.mPath = str5;
                }
                else
                {
                    if ((this.mPort != 443) || (!this.mScheme.equals("")))
                        break label277;
                    this.mScheme = "https";
                    if (this.mScheme.equals(""))
                        this.mScheme = "http";
                    return;
                }
            }
            catch (NumberFormatException localNumberFormatException)
            {
                throw new ParseException("Bad port");
            }
            this.mPath = ("/" + str5);
            continue;
            throw new ParseException("Bad address");
            label277: if (this.mPort == -1)
                if (this.mScheme.equals("https"))
                    this.mPort = 443;
                else
                    this.mPort = 80;
        }
    }

    public String getAuthInfo()
    {
        return this.mAuthInfo;
    }

    public String getHost()
    {
        return this.mHost;
    }

    public String getPath()
    {
        return this.mPath;
    }

    public int getPort()
    {
        return this.mPort;
    }

    public String getScheme()
    {
        return this.mScheme;
    }

    public void setAuthInfo(String paramString)
    {
        this.mAuthInfo = paramString;
    }

    public void setHost(String paramString)
    {
        this.mHost = paramString;
    }

    public void setPath(String paramString)
    {
        this.mPath = paramString;
    }

    public void setPort(int paramInt)
    {
        this.mPort = paramInt;
    }

    public void setScheme(String paramString)
    {
        this.mScheme = paramString;
    }

    public String toString()
    {
        String str1 = "";
        if (((this.mPort != 443) && (this.mScheme.equals("https"))) || ((this.mPort != 80) && (this.mScheme.equals("http"))))
            str1 = ":" + Integer.toString(this.mPort);
        String str2 = "";
        if (this.mAuthInfo.length() > 0)
            str2 = this.mAuthInfo + "@";
        return this.mScheme + "://" + str2 + this.mHost + str1 + this.mPath;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.WebAddress
 * JD-Core Version:        0.6.2
 */