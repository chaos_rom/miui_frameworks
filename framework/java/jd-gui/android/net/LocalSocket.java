package android.net;

import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class LocalSocket
{
    private LocalSocketImpl impl;
    private volatile boolean implCreated;
    private boolean isBound;
    private boolean isConnected;
    private LocalSocketAddress localAddress;

    public LocalSocket()
    {
        this(new LocalSocketImpl());
        this.isBound = false;
        this.isConnected = false;
    }

    LocalSocket(LocalSocketImpl paramLocalSocketImpl)
    {
        this.impl = paramLocalSocketImpl;
        this.isConnected = false;
        this.isBound = false;
    }

    private void implCreateIfNeeded()
        throws IOException
    {
        if (!this.implCreated)
            try
            {
                boolean bool = this.implCreated;
                if (!bool);
                try
                {
                    this.impl.create(true);
                    this.implCreated = true;
                }
                finally
                {
                    localObject2 = finally;
                    this.implCreated = true;
                    throw localObject2;
                }
            }
            finally
            {
            }
    }

    // ERROR //
    public void bind(LocalSocketAddress paramLocalSocketAddress)
        throws IOException
    {
        // Byte code:
        //     0: aload_0
        //     1: invokespecial 41	android/net/LocalSocket:implCreateIfNeeded	()V
        //     4: aload_0
        //     5: monitorenter
        //     6: aload_0
        //     7: getfield 23	android/net/LocalSocket:isBound	Z
        //     10: ifeq +18 -> 28
        //     13: new 31	java/io/IOException
        //     16: dup
        //     17: ldc 43
        //     19: invokespecial 46	java/io/IOException:<init>	(Ljava/lang/String;)V
        //     22: athrow
        //     23: astore_2
        //     24: aload_0
        //     25: monitorexit
        //     26: aload_2
        //     27: athrow
        //     28: aload_0
        //     29: aload_1
        //     30: putfield 48	android/net/LocalSocket:localAddress	Landroid/net/LocalSocketAddress;
        //     33: aload_0
        //     34: getfield 28	android/net/LocalSocket:impl	Landroid/net/LocalSocketImpl;
        //     37: aload_0
        //     38: getfield 48	android/net/LocalSocket:localAddress	Landroid/net/LocalSocketAddress;
        //     41: invokevirtual 50	android/net/LocalSocketImpl:bind	(Landroid/net/LocalSocketAddress;)V
        //     44: aload_0
        //     45: iconst_1
        //     46: putfield 23	android/net/LocalSocket:isBound	Z
        //     49: aload_0
        //     50: monitorexit
        //     51: return
        //
        // Exception table:
        //     from	to	target	type
        //     6	26	23	finally
        //     28	51	23	finally
    }

    public void close()
        throws IOException
    {
        implCreateIfNeeded();
        this.impl.close();
    }

    // ERROR //
    public void connect(LocalSocketAddress paramLocalSocketAddress)
        throws IOException
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 25	android/net/LocalSocket:isConnected	Z
        //     6: ifeq +18 -> 24
        //     9: new 31	java/io/IOException
        //     12: dup
        //     13: ldc 56
        //     15: invokespecial 46	java/io/IOException:<init>	(Ljava/lang/String;)V
        //     18: athrow
        //     19: astore_2
        //     20: aload_0
        //     21: monitorexit
        //     22: aload_2
        //     23: athrow
        //     24: aload_0
        //     25: invokespecial 41	android/net/LocalSocket:implCreateIfNeeded	()V
        //     28: aload_0
        //     29: getfield 28	android/net/LocalSocket:impl	Landroid/net/LocalSocketImpl;
        //     32: aload_1
        //     33: iconst_0
        //     34: invokevirtual 59	android/net/LocalSocketImpl:connect	(Landroid/net/LocalSocketAddress;I)V
        //     37: aload_0
        //     38: iconst_1
        //     39: putfield 25	android/net/LocalSocket:isConnected	Z
        //     42: aload_0
        //     43: iconst_1
        //     44: putfield 23	android/net/LocalSocket:isBound	Z
        //     47: aload_0
        //     48: monitorexit
        //     49: return
        //
        // Exception table:
        //     from	to	target	type
        //     2	22	19	finally
        //     24	49	19	finally
    }

    public void connect(LocalSocketAddress paramLocalSocketAddress, int paramInt)
        throws IOException
    {
        throw new UnsupportedOperationException();
    }

    public FileDescriptor[] getAncillaryFileDescriptors()
        throws IOException
    {
        return this.impl.getAncillaryFileDescriptors();
    }

    public FileDescriptor getFileDescriptor()
    {
        return this.impl.getFileDescriptor();
    }

    public InputStream getInputStream()
        throws IOException
    {
        implCreateIfNeeded();
        return this.impl.getInputStream();
    }

    public LocalSocketAddress getLocalSocketAddress()
    {
        return this.localAddress;
    }

    public OutputStream getOutputStream()
        throws IOException
    {
        implCreateIfNeeded();
        return this.impl.getOutputStream();
    }

    public Credentials getPeerCredentials()
        throws IOException
    {
        return this.impl.getPeerCredentials();
    }

    public int getReceiveBufferSize()
        throws IOException
    {
        return ((Integer)this.impl.getOption(4098)).intValue();
    }

    public LocalSocketAddress getRemoteSocketAddress()
    {
        throw new UnsupportedOperationException();
    }

    public int getSendBufferSize()
        throws IOException
    {
        return ((Integer)this.impl.getOption(4097)).intValue();
    }

    public int getSoTimeout()
        throws IOException
    {
        return ((Integer)this.impl.getOption(4102)).intValue();
    }

    /** @deprecated */
    public boolean isBound()
    {
        try
        {
            boolean bool = this.isBound;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public boolean isClosed()
    {
        throw new UnsupportedOperationException();
    }

    /** @deprecated */
    public boolean isConnected()
    {
        try
        {
            boolean bool = this.isConnected;
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public boolean isInputShutdown()
    {
        throw new UnsupportedOperationException();
    }

    public boolean isOutputShutdown()
    {
        throw new UnsupportedOperationException();
    }

    public void setFileDescriptorsForSend(FileDescriptor[] paramArrayOfFileDescriptor)
    {
        this.impl.setFileDescriptorsForSend(paramArrayOfFileDescriptor);
    }

    public void setReceiveBufferSize(int paramInt)
        throws IOException
    {
        this.impl.setOption(4098, Integer.valueOf(paramInt));
    }

    public void setSendBufferSize(int paramInt)
        throws IOException
    {
        this.impl.setOption(4097, Integer.valueOf(paramInt));
    }

    public void setSoTimeout(int paramInt)
        throws IOException
    {
        this.impl.setOption(4102, Integer.valueOf(paramInt));
    }

    public void shutdownInput()
        throws IOException
    {
        implCreateIfNeeded();
        this.impl.shutdownInput();
    }

    public void shutdownOutput()
        throws IOException
    {
        implCreateIfNeeded();
        this.impl.shutdownOutput();
    }

    public String toString()
    {
        return super.toString() + " impl:" + this.impl;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.LocalSocket
 * JD-Core Version:        0.6.2
 */