package android.net;

import android.content.Context;
import android.os.RemoteException;
import android.text.format.Time;
import java.io.PrintWriter;

public class NetworkPolicyManager
{
    private static final boolean ALLOW_PLATFORM_APP_POLICY = true;
    public static final String EXTRA_NETWORK_TEMPLATE = "android.net.NETWORK_TEMPLATE";
    public static final int POLICY_NONE = 0;
    public static final int POLICY_REJECT_METERED_BACKGROUND = 1;
    public static final int RULE_ALLOW_ALL = 0;
    public static final int RULE_REJECT_METERED = 1;
    private INetworkPolicyManager mService;

    public NetworkPolicyManager(INetworkPolicyManager paramINetworkPolicyManager)
    {
        if (paramINetworkPolicyManager == null)
            throw new IllegalArgumentException("missing INetworkPolicyManager");
        this.mService = paramINetworkPolicyManager;
    }

    public static long computeLastCycleBoundary(long paramLong, NetworkPolicy paramNetworkPolicy)
    {
        if (paramNetworkPolicy.cycleDay == -1)
            throw new IllegalArgumentException("Unable to compute boundary without cycleDay");
        Time localTime1 = new Time(paramNetworkPolicy.cycleTimezone);
        localTime1.set(paramLong);
        Time localTime2 = new Time(localTime1);
        localTime2.second = 0;
        localTime2.minute = 0;
        localTime2.hour = 0;
        snapToCycleDay(localTime2, paramNetworkPolicy.cycleDay);
        if (Time.compare(localTime2, localTime1) >= 0)
        {
            Time localTime3 = new Time(localTime1);
            localTime3.second = 0;
            localTime3.minute = 0;
            localTime3.hour = 0;
            localTime3.monthDay = 1;
            localTime3.month = (-1 + localTime3.month);
            localTime3.normalize(true);
            localTime2.set(localTime3);
            snapToCycleDay(localTime2, paramNetworkPolicy.cycleDay);
        }
        return localTime2.toMillis(true);
    }

    public static long computeNextCycleBoundary(long paramLong, NetworkPolicy paramNetworkPolicy)
    {
        if (paramNetworkPolicy.cycleDay == -1)
            throw new IllegalArgumentException("Unable to compute boundary without cycleDay");
        Time localTime1 = new Time(paramNetworkPolicy.cycleTimezone);
        localTime1.set(paramLong);
        Time localTime2 = new Time(localTime1);
        localTime2.second = 0;
        localTime2.minute = 0;
        localTime2.hour = 0;
        snapToCycleDay(localTime2, paramNetworkPolicy.cycleDay);
        if (Time.compare(localTime2, localTime1) <= 0)
        {
            Time localTime3 = new Time(localTime1);
            localTime3.second = 0;
            localTime3.minute = 0;
            localTime3.hour = 0;
            localTime3.monthDay = 1;
            localTime3.month = (1 + localTime3.month);
            localTime3.normalize(true);
            localTime2.set(localTime3);
            snapToCycleDay(localTime2, paramNetworkPolicy.cycleDay);
        }
        return localTime2.toMillis(true);
    }

    public static void dumpPolicy(PrintWriter paramPrintWriter, int paramInt)
    {
        paramPrintWriter.write("[");
        if ((paramInt & 0x1) != 0)
            paramPrintWriter.write("REJECT_METERED_BACKGROUND");
        paramPrintWriter.write("]");
    }

    public static void dumpRules(PrintWriter paramPrintWriter, int paramInt)
    {
        paramPrintWriter.write("[");
        if ((paramInt & 0x1) != 0)
            paramPrintWriter.write("REJECT_METERED");
        paramPrintWriter.write("]");
    }

    public static NetworkPolicyManager from(Context paramContext)
    {
        return (NetworkPolicyManager)paramContext.getSystemService("netpolicy");
    }

    @Deprecated
    public static boolean isUidValidForPolicy(Context paramContext, int paramInt)
    {
        if ((paramInt < 10000) || (paramInt > 19999));
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    public static void snapToCycleDay(Time paramTime, int paramInt)
    {
        if (paramInt > paramTime.getActualMaximum(4))
        {
            paramTime.month = (1 + paramTime.month);
            paramTime.monthDay = 1;
            paramTime.second = -1;
        }
        while (true)
        {
            paramTime.normalize(true);
            return;
            paramTime.monthDay = paramInt;
        }
    }

    public int getAppPolicy(int paramInt)
    {
        try
        {
            int j = this.mService.getAppPolicy(paramInt);
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                int i = 0;
        }
    }

    public int[] getAppsWithPolicy(int paramInt)
    {
        try
        {
            int[] arrayOfInt2 = this.mService.getAppsWithPolicy(paramInt);
            arrayOfInt1 = arrayOfInt2;
            return arrayOfInt1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                int[] arrayOfInt1 = new int[0];
        }
    }

    public NetworkPolicy[] getNetworkPolicies()
    {
        try
        {
            NetworkPolicy[] arrayOfNetworkPolicy2 = this.mService.getNetworkPolicies();
            arrayOfNetworkPolicy1 = arrayOfNetworkPolicy2;
            return arrayOfNetworkPolicy1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                NetworkPolicy[] arrayOfNetworkPolicy1 = null;
        }
    }

    public boolean getRestrictBackground()
    {
        try
        {
            boolean bool2 = this.mService.getRestrictBackground();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public void registerListener(INetworkPolicyListener paramINetworkPolicyListener)
    {
        try
        {
            this.mService.registerListener(paramINetworkPolicyListener);
            label10: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label10;
        }
    }

    public void setAppPolicy(int paramInt1, int paramInt2)
    {
        try
        {
            this.mService.setAppPolicy(paramInt1, paramInt2);
            label11: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label11;
        }
    }

    public void setNetworkPolicies(NetworkPolicy[] paramArrayOfNetworkPolicy)
    {
        try
        {
            this.mService.setNetworkPolicies(paramArrayOfNetworkPolicy);
            label10: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label10;
        }
    }

    public void setRestrictBackground(boolean paramBoolean)
    {
        try
        {
            this.mService.setRestrictBackground(paramBoolean);
            label10: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label10;
        }
    }

    public void unregisterListener(INetworkPolicyListener paramINetworkPolicyListener)
    {
        try
        {
            this.mService.unregisterListener(paramINetworkPolicyListener);
            label10: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label10;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.NetworkPolicyManager
 * JD-Core Version:        0.6.2
 */