package android.net;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.SystemClock;
import android.util.SparseBooleanArray;
import com.android.internal.util.Objects;
import java.io.CharArrayWriter;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashSet;

public class NetworkStats
    implements Parcelable
{
    public static final Parcelable.Creator<NetworkStats> CREATOR = new Parcelable.Creator()
    {
        public NetworkStats createFromParcel(Parcel paramAnonymousParcel)
        {
            return new NetworkStats(paramAnonymousParcel);
        }

        public NetworkStats[] newArray(int paramAnonymousInt)
        {
            return new NetworkStats[paramAnonymousInt];
        }
    };
    public static final String IFACE_ALL = null;
    public static final int SET_ALL = -1;
    public static final int SET_DEFAULT = 0;
    public static final int SET_FOREGROUND = 1;
    public static final int TAG_NONE = 0;
    public static final int UID_ALL = -1;
    private final long elapsedRealtime;
    private String[] iface;
    private long[] operations;
    private long[] rxBytes;
    private long[] rxPackets;
    private int[] set;
    private int size;
    private int[] tag;
    private long[] txBytes;
    private long[] txPackets;
    private int[] uid;

    public NetworkStats(long paramLong, int paramInt)
    {
        this.elapsedRealtime = paramLong;
        this.size = 0;
        this.iface = new String[paramInt];
        this.uid = new int[paramInt];
        this.set = new int[paramInt];
        this.tag = new int[paramInt];
        this.rxBytes = new long[paramInt];
        this.rxPackets = new long[paramInt];
        this.txBytes = new long[paramInt];
        this.txPackets = new long[paramInt];
        this.operations = new long[paramInt];
    }

    public NetworkStats(Parcel paramParcel)
    {
        this.elapsedRealtime = paramParcel.readLong();
        this.size = paramParcel.readInt();
        this.iface = paramParcel.createStringArray();
        this.uid = paramParcel.createIntArray();
        this.set = paramParcel.createIntArray();
        this.tag = paramParcel.createIntArray();
        this.rxBytes = paramParcel.createLongArray();
        this.rxPackets = paramParcel.createLongArray();
        this.txBytes = paramParcel.createLongArray();
        this.txPackets = paramParcel.createLongArray();
        this.operations = paramParcel.createLongArray();
    }

    private Entry getTotal(Entry paramEntry, HashSet<String> paramHashSet, int paramInt, boolean paramBoolean)
    {
        Entry localEntry;
        int i;
        label67: int j;
        label96: int k;
        if (paramEntry != null)
        {
            localEntry = paramEntry;
            localEntry.iface = IFACE_ALL;
            localEntry.uid = paramInt;
            localEntry.set = -1;
            localEntry.tag = 0;
            localEntry.rxBytes = 0L;
            localEntry.rxPackets = 0L;
            localEntry.txBytes = 0L;
            localEntry.txPackets = 0L;
            localEntry.operations = 0L;
            i = 0;
            if (i >= this.size)
                break label265;
            if ((paramInt != -1) && (paramInt != this.uid[i]))
                break label160;
            j = 1;
            if ((paramHashSet != null) && (!paramHashSet.contains(this.iface[i])))
                break label166;
            k = 1;
            label117: if ((j != 0) && (k != 0) && ((this.tag[i] == 0) || (paramBoolean)))
                break label172;
        }
        while (true)
        {
            i++;
            break label67;
            localEntry = new Entry();
            break;
            label160: j = 0;
            break label96;
            label166: k = 0;
            break label117;
            label172: localEntry.rxBytes += this.rxBytes[i];
            localEntry.rxPackets += this.rxPackets[i];
            localEntry.txBytes += this.txBytes[i];
            localEntry.txPackets += this.txPackets[i];
            localEntry.operations += this.operations[i];
        }
        label265: return localEntry;
    }

    public static String setToString(int paramInt)
    {
        String str;
        switch (paramInt)
        {
        default:
            str = "UNKNOWN";
        case -1:
        case 0:
        case 1:
        }
        while (true)
        {
            return str;
            str = "ALL";
            continue;
            str = "DEFAULT";
            continue;
            str = "FOREGROUND";
        }
    }

    public static <C> NetworkStats subtract(NetworkStats paramNetworkStats1, NetworkStats paramNetworkStats2, NonMonotonicObserver<C> paramNonMonotonicObserver, C paramC)
    {
        long l = paramNetworkStats1.elapsedRealtime - paramNetworkStats2.elapsedRealtime;
        if (l < 0L)
        {
            if (paramNonMonotonicObserver != null)
                paramNonMonotonicObserver.foundNonMonotonic(paramNetworkStats1, -1, paramNetworkStats2, -1, paramC);
            l = 0L;
        }
        Entry localEntry = new Entry();
        NetworkStats localNetworkStats = new NetworkStats(l, paramNetworkStats1.size);
        int i = 0;
        if (i < paramNetworkStats1.size)
        {
            localEntry.iface = paramNetworkStats1.iface[i];
            localEntry.uid = paramNetworkStats1.uid[i];
            localEntry.set = paramNetworkStats1.set[i];
            localEntry.tag = paramNetworkStats1.tag[i];
            int j = paramNetworkStats2.findIndexHinted(localEntry.iface, localEntry.uid, localEntry.set, localEntry.tag, i);
            if (j == -1)
            {
                localEntry.rxBytes = paramNetworkStats1.rxBytes[i];
                localEntry.rxPackets = paramNetworkStats1.rxPackets[i];
                localEntry.txBytes = paramNetworkStats1.txBytes[i];
                localEntry.txPackets = paramNetworkStats1.txPackets[i];
            }
            for (localEntry.operations = paramNetworkStats1.operations[i]; ; localEntry.operations = Math.max(localEntry.operations, 0L))
            {
                do
                {
                    localNetworkStats.addValues(localEntry);
                    i++;
                    break;
                    localEntry.rxBytes = (paramNetworkStats1.rxBytes[i] - paramNetworkStats2.rxBytes[j]);
                    localEntry.rxPackets = (paramNetworkStats1.rxPackets[i] - paramNetworkStats2.rxPackets[j]);
                    localEntry.txBytes = (paramNetworkStats1.txBytes[i] - paramNetworkStats2.txBytes[j]);
                    localEntry.txPackets = (paramNetworkStats1.txPackets[i] - paramNetworkStats2.txPackets[j]);
                    localEntry.operations = (paramNetworkStats1.operations[i] - paramNetworkStats2.operations[j]);
                }
                while ((localEntry.rxBytes >= 0L) && (localEntry.rxPackets >= 0L) && (localEntry.txBytes >= 0L) && (localEntry.txPackets >= 0L) && (localEntry.operations >= 0L));
                if (paramNonMonotonicObserver != null)
                    paramNonMonotonicObserver.foundNonMonotonic(paramNetworkStats1, i, paramNetworkStats2, j, paramC);
                localEntry.rxBytes = Math.max(localEntry.rxBytes, 0L);
                localEntry.rxPackets = Math.max(localEntry.rxPackets, 0L);
                localEntry.txBytes = Math.max(localEntry.txBytes, 0L);
                localEntry.txPackets = Math.max(localEntry.txPackets, 0L);
            }
        }
        return localNetworkStats;
    }

    public static String tagToString(int paramInt)
    {
        return "0x" + Integer.toHexString(paramInt);
    }

    public NetworkStats addIfaceValues(String paramString, long paramLong1, long paramLong2, long paramLong3, long paramLong4)
    {
        return addValues(paramString, -1, 0, 0, paramLong1, paramLong2, paramLong3, paramLong4, 0L);
    }

    public NetworkStats addValues(Entry paramEntry)
    {
        if (this.size >= this.iface.length)
        {
            int i = 3 * Math.max(this.iface.length, 10) / 2;
            this.iface = ((String[])Arrays.copyOf(this.iface, i));
            this.uid = Arrays.copyOf(this.uid, i);
            this.set = Arrays.copyOf(this.set, i);
            this.tag = Arrays.copyOf(this.tag, i);
            this.rxBytes = Arrays.copyOf(this.rxBytes, i);
            this.rxPackets = Arrays.copyOf(this.rxPackets, i);
            this.txBytes = Arrays.copyOf(this.txBytes, i);
            this.txPackets = Arrays.copyOf(this.txPackets, i);
            this.operations = Arrays.copyOf(this.operations, i);
        }
        this.iface[this.size] = paramEntry.iface;
        this.uid[this.size] = paramEntry.uid;
        this.set[this.size] = paramEntry.set;
        this.tag[this.size] = paramEntry.tag;
        this.rxBytes[this.size] = paramEntry.rxBytes;
        this.rxPackets[this.size] = paramEntry.rxPackets;
        this.txBytes[this.size] = paramEntry.txBytes;
        this.txPackets[this.size] = paramEntry.txPackets;
        this.operations[this.size] = paramEntry.operations;
        this.size = (1 + this.size);
        return this;
    }

    public NetworkStats addValues(String paramString, int paramInt1, int paramInt2, int paramInt3, long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5)
    {
        return addValues(new Entry(paramString, paramInt1, paramInt2, paramInt3, paramLong1, paramLong2, paramLong3, paramLong4, paramLong5));
    }

    public NetworkStats clone()
    {
        NetworkStats localNetworkStats = new NetworkStats(this.elapsedRealtime, this.size);
        Entry localEntry = null;
        for (int i = 0; i < this.size; i++)
        {
            localEntry = getValues(i, localEntry);
            localNetworkStats.addValues(localEntry);
        }
        return localNetworkStats;
    }

    public void combineAllValues(NetworkStats paramNetworkStats)
    {
        Entry localEntry = null;
        for (int i = 0; i < paramNetworkStats.size; i++)
        {
            localEntry = paramNetworkStats.getValues(i, localEntry);
            combineValues(localEntry);
        }
    }

    public NetworkStats combineValues(Entry paramEntry)
    {
        int i = findIndex(paramEntry.iface, paramEntry.uid, paramEntry.set, paramEntry.tag);
        if (i == -1)
            addValues(paramEntry);
        while (true)
        {
            return this;
            long[] arrayOfLong1 = this.rxBytes;
            arrayOfLong1[i] += paramEntry.rxBytes;
            long[] arrayOfLong2 = this.rxPackets;
            arrayOfLong2[i] += paramEntry.rxPackets;
            long[] arrayOfLong3 = this.txBytes;
            arrayOfLong3[i] += paramEntry.txBytes;
            long[] arrayOfLong4 = this.txPackets;
            arrayOfLong4[i] += paramEntry.txPackets;
            long[] arrayOfLong5 = this.operations;
            arrayOfLong5[i] += paramEntry.operations;
        }
    }

    public NetworkStats combineValues(String paramString, int paramInt1, int paramInt2, int paramInt3, long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5)
    {
        return combineValues(new Entry(paramString, paramInt1, paramInt2, paramInt3, paramLong1, paramLong2, paramLong3, paramLong4, paramLong5));
    }

    @Deprecated
    public NetworkStats combineValues(String paramString, int paramInt1, int paramInt2, long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5)
    {
        return combineValues(paramString, paramInt1, 0, paramInt2, paramLong1, paramLong2, paramLong3, paramLong4, paramLong5);
    }

    public int describeContents()
    {
        return 0;
    }

    public void dump(String paramString, PrintWriter paramPrintWriter)
    {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("NetworkStats: elapsedRealtime=");
        paramPrintWriter.println(this.elapsedRealtime);
        for (int i = 0; i < this.size; i++)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("    [");
            paramPrintWriter.print(i);
            paramPrintWriter.print("]");
            paramPrintWriter.print(" iface=");
            paramPrintWriter.print(this.iface[i]);
            paramPrintWriter.print(" uid=");
            paramPrintWriter.print(this.uid[i]);
            paramPrintWriter.print(" set=");
            paramPrintWriter.print(setToString(this.set[i]));
            paramPrintWriter.print(" tag=");
            paramPrintWriter.print(tagToString(this.tag[i]));
            paramPrintWriter.print(" rxBytes=");
            paramPrintWriter.print(this.rxBytes[i]);
            paramPrintWriter.print(" rxPackets=");
            paramPrintWriter.print(this.rxPackets[i]);
            paramPrintWriter.print(" txBytes=");
            paramPrintWriter.print(this.txBytes[i]);
            paramPrintWriter.print(" txPackets=");
            paramPrintWriter.print(this.txPackets[i]);
            paramPrintWriter.print(" operations=");
            paramPrintWriter.println(this.operations[i]);
        }
    }

    public int findIndex(String paramString, int paramInt1, int paramInt2, int paramInt3)
    {
        int i = 0;
        if (i < this.size)
            if ((paramInt1 != this.uid[i]) || (paramInt2 != this.set[i]) || (paramInt3 != this.tag[i]) || (!Objects.equal(paramString, this.iface[i])));
        while (true)
        {
            return i;
            i++;
            break;
            i = -1;
        }
    }

    public int findIndexHinted(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
        int i = 0;
        int k;
        int j;
        if (i < this.size)
        {
            k = i / 2;
            if (i % 2 == 0)
            {
                j = (paramInt4 + k) % this.size;
                label37: if ((paramInt1 != this.uid[j]) || (paramInt2 != this.set[j]) || (paramInt3 != this.tag[j]) || (!Objects.equal(paramString, this.iface[j])))
                    break label111;
            }
        }
        while (true)
        {
            return j;
            j = (-1 + (paramInt4 + this.size - k)) % this.size;
            break label37;
            label111: i++;
            break;
            j = -1;
        }
    }

    public long getElapsedRealtime()
    {
        return this.elapsedRealtime;
    }

    public long getElapsedRealtimeAge()
    {
        return SystemClock.elapsedRealtime() - this.elapsedRealtime;
    }

    public Entry getTotal(Entry paramEntry)
    {
        return getTotal(paramEntry, null, -1, false);
    }

    public Entry getTotal(Entry paramEntry, int paramInt)
    {
        return getTotal(paramEntry, null, paramInt, false);
    }

    public Entry getTotal(Entry paramEntry, HashSet<String> paramHashSet)
    {
        return getTotal(paramEntry, paramHashSet, -1, false);
    }

    public long getTotalBytes()
    {
        Entry localEntry = getTotal(null);
        return localEntry.rxBytes + localEntry.txBytes;
    }

    public Entry getTotalIncludingTags(Entry paramEntry)
    {
        return getTotal(paramEntry, null, -1, true);
    }

    public String[] getUniqueIfaces()
    {
        HashSet localHashSet = new HashSet();
        for (String str : this.iface)
            if (str != IFACE_ALL)
                localHashSet.add(str);
        return (String[])localHashSet.toArray(new String[localHashSet.size()]);
    }

    public int[] getUniqueUids()
    {
        SparseBooleanArray localSparseBooleanArray = new SparseBooleanArray();
        int[] arrayOfInt1 = this.uid;
        int i = arrayOfInt1.length;
        for (int j = 0; j < i; j++)
            localSparseBooleanArray.put(arrayOfInt1[j], true);
        int k = localSparseBooleanArray.size();
        int[] arrayOfInt2 = new int[k];
        for (int m = 0; m < k; m++)
            arrayOfInt2[m] = localSparseBooleanArray.keyAt(m);
        return arrayOfInt2;
    }

    public Entry getValues(int paramInt, Entry paramEntry)
    {
        if (paramEntry != null);
        for (Entry localEntry = paramEntry; ; localEntry = new Entry())
        {
            localEntry.iface = this.iface[paramInt];
            localEntry.uid = this.uid[paramInt];
            localEntry.set = this.set[paramInt];
            localEntry.tag = this.tag[paramInt];
            localEntry.rxBytes = this.rxBytes[paramInt];
            localEntry.rxPackets = this.rxPackets[paramInt];
            localEntry.txBytes = this.txBytes[paramInt];
            localEntry.txPackets = this.txPackets[paramInt];
            localEntry.operations = this.operations[paramInt];
            return localEntry;
        }
    }

    public NetworkStats groupedByIface()
    {
        NetworkStats localNetworkStats = new NetworkStats(this.elapsedRealtime, 10);
        Entry localEntry = new Entry();
        localEntry.uid = -1;
        localEntry.set = -1;
        localEntry.tag = 0;
        localEntry.operations = 0L;
        int i = 0;
        if (i < this.size)
        {
            if (this.tag[i] != 0);
            while (true)
            {
                i++;
                break;
                localEntry.iface = this.iface[i];
                localEntry.rxBytes = this.rxBytes[i];
                localEntry.rxPackets = this.rxPackets[i];
                localEntry.txBytes = this.txBytes[i];
                localEntry.txPackets = this.txPackets[i];
                localNetworkStats.combineValues(localEntry);
            }
        }
        return localNetworkStats;
    }

    public NetworkStats groupedByUid()
    {
        NetworkStats localNetworkStats = new NetworkStats(this.elapsedRealtime, 10);
        Entry localEntry = new Entry();
        localEntry.iface = IFACE_ALL;
        localEntry.set = -1;
        localEntry.tag = 0;
        int i = 0;
        if (i < this.size)
        {
            if (this.tag[i] != 0);
            while (true)
            {
                i++;
                break;
                localEntry.uid = this.uid[i];
                localEntry.rxBytes = this.rxBytes[i];
                localEntry.rxPackets = this.rxPackets[i];
                localEntry.txBytes = this.txBytes[i];
                localEntry.txPackets = this.txPackets[i];
                localEntry.operations = this.operations[i];
                localNetworkStats.combineValues(localEntry);
            }
        }
        return localNetworkStats;
    }

    public int internalSize()
    {
        return this.iface.length;
    }

    public int size()
    {
        return this.size;
    }

    public void spliceOperationsFrom(NetworkStats paramNetworkStats)
    {
        int i = 0;
        if (i < this.size)
        {
            int j = paramNetworkStats.findIndex(this.iface[i], this.uid[i], this.set[i], this.tag[i]);
            if (j == -1)
                this.operations[i] = 0L;
            while (true)
            {
                i++;
                break;
                this.operations[i] = paramNetworkStats.operations[j];
            }
        }
    }

    public NetworkStats subtract(NetworkStats paramNetworkStats)
    {
        return subtract(this, paramNetworkStats, null, null);
    }

    public String toString()
    {
        CharArrayWriter localCharArrayWriter = new CharArrayWriter();
        dump("", new PrintWriter(localCharArrayWriter));
        return localCharArrayWriter.toString();
    }

    public NetworkStats withoutUid(int paramInt)
    {
        NetworkStats localNetworkStats = new NetworkStats(this.elapsedRealtime, 10);
        Entry localEntry = new Entry();
        for (int i = 0; i < this.size; i++)
        {
            localEntry = getValues(i, localEntry);
            if (localEntry.uid != paramInt)
                localNetworkStats.addValues(localEntry);
        }
        return localNetworkStats;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeLong(this.elapsedRealtime);
        paramParcel.writeInt(this.size);
        paramParcel.writeStringArray(this.iface);
        paramParcel.writeIntArray(this.uid);
        paramParcel.writeIntArray(this.set);
        paramParcel.writeIntArray(this.tag);
        paramParcel.writeLongArray(this.rxBytes);
        paramParcel.writeLongArray(this.rxPackets);
        paramParcel.writeLongArray(this.txBytes);
        paramParcel.writeLongArray(this.txPackets);
        paramParcel.writeLongArray(this.operations);
    }

    public static abstract interface NonMonotonicObserver<C>
    {
        public abstract void foundNonMonotonic(NetworkStats paramNetworkStats1, int paramInt1, NetworkStats paramNetworkStats2, int paramInt2, C paramC);
    }

    public static class Entry
    {
        public String iface;
        public long operations;
        public long rxBytes;
        public long rxPackets;
        public int set;
        public int tag;
        public long txBytes;
        public long txPackets;
        public int uid;

        public Entry()
        {
            this(NetworkStats.IFACE_ALL, -1, 0, 0, 0L, 0L, 0L, 0L, 0L);
        }

        public Entry(long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5)
        {
            this(NetworkStats.IFACE_ALL, -1, 0, 0, paramLong1, paramLong2, paramLong3, paramLong4, paramLong5);
        }

        public Entry(String paramString, int paramInt1, int paramInt2, int paramInt3, long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5)
        {
            this.iface = paramString;
            this.uid = paramInt1;
            this.set = paramInt2;
            this.tag = paramInt3;
            this.rxBytes = paramLong1;
            this.rxPackets = paramLong2;
            this.txBytes = paramLong3;
            this.txPackets = paramLong4;
            this.operations = paramLong5;
        }

        public void add(Entry paramEntry)
        {
            this.rxBytes += paramEntry.rxBytes;
            this.rxPackets += paramEntry.rxPackets;
            this.txBytes += paramEntry.txBytes;
            this.txPackets += paramEntry.txPackets;
            this.operations += paramEntry.operations;
        }

        public boolean isEmpty()
        {
            if ((this.rxBytes == 0L) && (this.rxPackets == 0L) && (this.txBytes == 0L) && (this.txPackets == 0L) && (this.operations == 0L));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public boolean isNegative()
        {
            if ((this.rxBytes < 0L) || (this.rxPackets < 0L) || (this.txBytes < 0L) || (this.txPackets < 0L) || (this.operations < 0L));
            for (boolean bool = true; ; bool = false)
                return bool;
        }

        public String toString()
        {
            StringBuilder localStringBuilder = new StringBuilder();
            localStringBuilder.append("iface=").append(this.iface);
            localStringBuilder.append(" uid=").append(this.uid);
            localStringBuilder.append(" set=").append(NetworkStats.setToString(this.set));
            localStringBuilder.append(" tag=").append(NetworkStats.tagToString(this.tag));
            localStringBuilder.append(" rxBytes=").append(this.rxBytes);
            localStringBuilder.append(" rxPackets=").append(this.rxPackets);
            localStringBuilder.append(" txBytes=").append(this.txBytes);
            localStringBuilder.append(" txPackets=").append(this.txPackets);
            localStringBuilder.append(" operations=").append(this.operations);
            return localStringBuilder.toString();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.NetworkStats
 * JD-Core Version:        0.6.2
 */