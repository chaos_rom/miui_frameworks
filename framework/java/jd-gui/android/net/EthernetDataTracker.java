package android.net;

import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.os.INetworkManagementService;
import android.os.INetworkManagementService.Stub;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class EthernetDataTracker
    implements NetworkStateTracker
{
    private static final String NETWORKTYPE = "ETHERNET";
    private static final String TAG = "Ethernet";
    private static String mIface = "";
    private static boolean mLinkUp;
    private static String sIfaceMatch = "";
    private static EthernetDataTracker sInstance;
    private Context mContext;
    private Handler mCsHandler;
    private AtomicInteger mDefaultGatewayAddr = new AtomicInteger(0);
    private AtomicBoolean mDefaultRouteSet = new AtomicBoolean(false);
    private String mHwAddr;
    private InterfaceObserver mInterfaceObserver;
    private LinkCapabilities mLinkCapabilities = new LinkCapabilities();
    private LinkProperties mLinkProperties = new LinkProperties();
    private INetworkManagementService mNMService;
    private NetworkInfo mNetworkInfo = new NetworkInfo(9, 0, "ETHERNET", "");
    private AtomicBoolean mPrivateDnsRouteSet = new AtomicBoolean(false);
    private AtomicBoolean mTeardownRequested = new AtomicBoolean(false);

    /** @deprecated */
    public static EthernetDataTracker getInstance()
    {
        try
        {
            if (sInstance == null)
                sInstance = new EthernetDataTracker();
            EthernetDataTracker localEthernetDataTracker = sInstance;
            return localEthernetDataTracker;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    // ERROR //
    private void interfaceAdded(String paramString)
    {
        // Byte code:
        //     0: aload_1
        //     1: getstatic 51	android/net/EthernetDataTracker:sIfaceMatch	Ljava/lang/String;
        //     4: invokevirtual 136	java/lang/String:matches	(Ljava/lang/String;)Z
        //     7: ifne +4 -> 11
        //     10: return
        //     11: ldc 18
        //     13: new 138	java/lang/StringBuilder
        //     16: dup
        //     17: invokespecial 139	java/lang/StringBuilder:<init>	()V
        //     20: ldc 141
        //     22: invokevirtual 145	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     25: aload_1
        //     26: invokevirtual 145	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     29: invokevirtual 148	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     32: invokestatic 154	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     35: pop
        //     36: aload_0
        //     37: monitorenter
        //     38: getstatic 53	android/net/EthernetDataTracker:mIface	Ljava/lang/String;
        //     41: invokevirtual 157	java/lang/String:isEmpty	()Z
        //     44: ifne +13 -> 57
        //     47: aload_0
        //     48: monitorexit
        //     49: goto -39 -> 10
        //     52: astore_3
        //     53: aload_0
        //     54: monitorexit
        //     55: aload_3
        //     56: athrow
        //     57: aload_1
        //     58: putstatic 53	android/net/EthernetDataTracker:mIface	Ljava/lang/String;
        //     61: aload_0
        //     62: monitorexit
        //     63: aload_0
        //     64: getfield 159	android/net/EthernetDataTracker:mNMService	Landroid/os/INetworkManagementService;
        //     67: aload_1
        //     68: invokeinterface 164 2 0
        //     73: aload_0
        //     74: getfield 81	android/net/EthernetDataTracker:mNetworkInfo	Landroid/net/NetworkInfo;
        //     77: iconst_1
        //     78: invokevirtual 167	android/net/NetworkInfo:setIsAvailable	(Z)V
        //     81: aload_0
        //     82: getfield 123	android/net/EthernetDataTracker:mCsHandler	Landroid/os/Handler;
        //     85: iconst_3
        //     86: aload_0
        //     87: getfield 81	android/net/EthernetDataTracker:mNetworkInfo	Landroid/net/NetworkInfo;
        //     90: invokevirtual 173	android/os/Handler:obtainMessage	(ILjava/lang/Object;)Landroid/os/Message;
        //     93: invokevirtual 178	android/os/Message:sendToTarget	()V
        //     96: goto -86 -> 10
        //     99: astore 4
        //     101: ldc 18
        //     103: new 138	java/lang/StringBuilder
        //     106: dup
        //     107: invokespecial 139	java/lang/StringBuilder:<init>	()V
        //     110: ldc 180
        //     112: invokevirtual 145	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     115: aload_1
        //     116: invokevirtual 145	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     119: ldc 182
        //     121: invokevirtual 145	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     124: aload 4
        //     126: invokevirtual 185	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     129: invokevirtual 148	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     132: invokestatic 188	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     135: pop
        //     136: goto -63 -> 73
        //
        // Exception table:
        //     from	to	target	type
        //     38	55	52	finally
        //     57	63	52	finally
        //     63	73	99	java/lang/Exception
    }

    private void interfaceRemoved(String paramString)
    {
        if (!paramString.equals(mIface));
        while (true)
        {
            return;
            Log.d("Ethernet", "Removing " + paramString);
            disconnect();
            mIface = "";
        }
    }

    private void runDhcp()
    {
        new Thread(new Runnable()
        {
            public void run()
            {
                DhcpInfoInternal localDhcpInfoInternal = new DhcpInfoInternal();
                if (!NetworkUtils.runDhcp(EthernetDataTracker.mIface, localDhcpInfoInternal))
                    Log.e("Ethernet", "DHCP request error:" + NetworkUtils.getDhcpError());
                while (true)
                {
                    return;
                    EthernetDataTracker.access$502(EthernetDataTracker.this, localDhcpInfoInternal.makeLinkProperties());
                    EthernetDataTracker.this.mLinkProperties.setInterfaceName(EthernetDataTracker.mIface);
                    EthernetDataTracker.this.mNetworkInfo.setDetailedState(NetworkInfo.DetailedState.CONNECTED, null, EthernetDataTracker.this.mHwAddr);
                    EthernetDataTracker.this.mCsHandler.obtainMessage(1, EthernetDataTracker.this.mNetworkInfo).sendToTarget();
                }
            }
        }).start();
    }

    public Object Clone()
        throws CloneNotSupportedException
    {
        throw new CloneNotSupportedException();
    }

    public void defaultRouteSet(boolean paramBoolean)
    {
        this.mDefaultRouteSet.set(paramBoolean);
    }

    public void disconnect()
    {
        NetworkUtils.stopDhcp(mIface);
        this.mLinkProperties.clear();
        this.mNetworkInfo.setIsAvailable(false);
        this.mNetworkInfo.setDetailedState(NetworkInfo.DetailedState.DISCONNECTED, null, this.mHwAddr);
        this.mCsHandler.obtainMessage(3, this.mNetworkInfo).sendToTarget();
        this.mCsHandler.obtainMessage(1, this.mNetworkInfo).sendToTarget();
        INetworkManagementService localINetworkManagementService = INetworkManagementService.Stub.asInterface(ServiceManager.getService("network_management"));
        try
        {
            localINetworkManagementService.clearInterfaceAddresses(mIface);
            return;
        }
        catch (Exception localException)
        {
            while (true)
                Log.e("Ethernet", "Failed to clear addresses or disable ipv6" + localException);
        }
    }

    public int getDefaultGatewayAddr()
    {
        return this.mDefaultGatewayAddr.get();
    }

    public LinkCapabilities getLinkCapabilities()
    {
        return new LinkCapabilities(this.mLinkCapabilities);
    }

    /** @deprecated */
    public LinkProperties getLinkProperties()
    {
        try
        {
            LinkProperties localLinkProperties = new LinkProperties(this.mLinkProperties);
            return localLinkProperties;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    /** @deprecated */
    public NetworkInfo getNetworkInfo()
    {
        try
        {
            NetworkInfo localNetworkInfo = this.mNetworkInfo;
            return localNetworkInfo;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public String getTcpBufferSizesPropName()
    {
        return "net.tcp.buffersize.wifi";
    }

    /** @deprecated */
    public boolean isAvailable()
    {
        try
        {
            boolean bool = this.mNetworkInfo.isAvailable();
            return bool;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public boolean isDefaultRouteSet()
    {
        return this.mDefaultRouteSet.get();
    }

    public boolean isPrivateDnsRouteSet()
    {
        return this.mPrivateDnsRouteSet.get();
    }

    public boolean isTeardownRequested()
    {
        return this.mTeardownRequested.get();
    }

    public void privateDnsRouteSet(boolean paramBoolean)
    {
        this.mPrivateDnsRouteSet.set(paramBoolean);
    }

    public boolean reconnect()
    {
        if (mLinkUp)
        {
            this.mTeardownRequested.set(false);
            runDhcp();
        }
        return mLinkUp;
    }

    public void setDependencyMet(boolean paramBoolean)
    {
    }

    public void setPolicyDataEnable(boolean paramBoolean)
    {
        Log.w("Ethernet", "ignoring setPolicyDataEnable(" + paramBoolean + ")");
    }

    public boolean setRadio(boolean paramBoolean)
    {
        return true;
    }

    public void setTeardownRequested(boolean paramBoolean)
    {
        this.mTeardownRequested.set(paramBoolean);
    }

    public void setUserDataEnable(boolean paramBoolean)
    {
        Log.w("Ethernet", "ignoring setUserDataEnable(" + paramBoolean + ")");
    }

    public void startMonitoring(Context paramContext, Handler paramHandler)
    {
        this.mContext = paramContext;
        this.mCsHandler = paramHandler;
        this.mNMService = INetworkManagementService.Stub.asInterface(ServiceManager.getService("network_management"));
        this.mInterfaceObserver = new InterfaceObserver(this);
        sIfaceMatch = paramContext.getResources().getString(17039384);
        try
        {
            String[] arrayOfString = this.mNMService.listInterfaces();
            int i = arrayOfString.length;
            j = 0;
            if (j < i)
            {
                String str = arrayOfString[j];
                if (!str.matches(sIfaceMatch))
                    break label186;
                mIface = str;
                this.mNMService.setInterfaceUp(str);
                InterfaceConfiguration localInterfaceConfiguration = this.mNMService.getInterfaceConfig(str);
                mLinkUp = localInterfaceConfiguration.isActive();
                if ((localInterfaceConfiguration != null) && (this.mHwAddr == null))
                {
                    this.mHwAddr = localInterfaceConfiguration.getHardwareAddress();
                    if (this.mHwAddr != null)
                        this.mNetworkInfo.setExtraInfo(this.mHwAddr);
                }
                reconnect();
            }
        }
        catch (RemoteException localRemoteException1)
        {
            try
            {
                while (true)
                {
                    int j;
                    this.mNMService.registerObserver(this.mInterfaceObserver);
                    return;
                    label186: j++;
                    continue;
                    localRemoteException1 = localRemoteException1;
                    Log.e("Ethernet", "Could not get list of interfaces " + localRemoteException1);
                }
            }
            catch (RemoteException localRemoteException2)
            {
                while (true)
                    Log.e("Ethernet", "Could not register InterfaceObserver " + localRemoteException2);
            }
        }
    }

    public int startUsingNetworkFeature(String paramString, int paramInt1, int paramInt2)
    {
        return -1;
    }

    public int stopUsingNetworkFeature(String paramString, int paramInt1, int paramInt2)
    {
        return -1;
    }

    public boolean teardown()
    {
        this.mTeardownRequested.set(true);
        NetworkUtils.stopDhcp(mIface);
        return true;
    }

    private static class InterfaceObserver extends INetworkManagementEventObserver.Stub
    {
        private EthernetDataTracker mTracker;

        InterfaceObserver(EthernetDataTracker paramEthernetDataTracker)
        {
            this.mTracker = paramEthernetDataTracker;
        }

        public void interfaceAdded(String paramString)
        {
            this.mTracker.interfaceAdded(paramString);
        }

        public void interfaceLinkStateChanged(String paramString, boolean paramBoolean)
        {
            String str;
            if ((EthernetDataTracker.mIface.equals(paramString)) && (EthernetDataTracker.mLinkUp != paramBoolean))
            {
                StringBuilder localStringBuilder = new StringBuilder().append("Interface ").append(paramString).append(" link ");
                if (!paramBoolean)
                    break label91;
                str = "up";
                Log.d("Ethernet", str);
                EthernetDataTracker.access$102(paramBoolean);
                this.mTracker.mNetworkInfo.setIsAvailable(paramBoolean);
                if (!paramBoolean)
                    break label98;
                this.mTracker.reconnect();
            }
            while (true)
            {
                return;
                label91: str = "down";
                break;
                label98: this.mTracker.disconnect();
            }
        }

        public void interfaceRemoved(String paramString)
        {
            this.mTracker.interfaceRemoved(paramString);
        }

        public void interfaceStatusChanged(String paramString, boolean paramBoolean)
        {
            StringBuilder localStringBuilder = new StringBuilder().append("Interface status changed: ").append(paramString);
            if (paramBoolean);
            for (String str = "up"; ; str = "down")
            {
                Log.d("Ethernet", str);
                return;
            }
        }

        public void limitReached(String paramString1, String paramString2)
        {
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.EthernetDataTracker
 * JD-Core Version:        0.6.2
 */