package android.net;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.Context;
import android.text.TextUtils;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ProxySelector;
import java.net.URI;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.routing.HttpRoutePlanner;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.conn.ProxySelectorRoutePlanner;
import org.apache.http.protocol.HttpContext;

public final class Proxy
{
    private static final boolean DEBUG = false;
    private static final Pattern EXCLLIST_PATTERN = Pattern.compile("$|^(.?[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*(\\.[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*)*)+(,(.?[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*(\\.[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*)*))*$");
    private static final String EXCLLIST_REGEXP = "$|^(.?[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*(\\.[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*)*)+(,(.?[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*(\\.[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*)*))*$";
    public static final String EXTRA_PROXY_INFO = "proxy";
    private static final Pattern HOSTNAME_PATTERN;
    private static final String HOSTNAME_REGEXP = "^$|^[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*(\\.[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*)*$";
    private static final String NAME_IP_REGEX = "[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*(\\.[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*)*";
    public static final String PROXY_CHANGE_ACTION = "android.intent.action.PROXY_CHANGE";
    private static final String TAG = "Proxy";
    private static ConnectivityManager sConnectivityManager = null;

    static
    {
        HOSTNAME_PATTERN = Pattern.compile("^$|^[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*(\\.[a-zA-Z0-9]+(\\-[a-zA-Z0-9]+)*)*$");
    }

    public static final HttpRoutePlanner getAndroidProxySelectorRoutePlanner(Context paramContext)
    {
        return new AndroidProxySelectorRoutePlanner(new SchemeRegistry(), ProxySelector.getDefault(), paramContext);
    }

    public static final String getDefaultHost()
    {
        String str = System.getProperty("http.proxyHost");
        if (TextUtils.isEmpty(str))
            str = null;
        return str;
    }

    public static final int getDefaultPort()
    {
        int i = -1;
        if (getDefaultHost() == null);
        while (true)
        {
            return i;
            try
            {
                int j = Integer.parseInt(System.getProperty("http.proxyPort"));
                i = j;
            }
            catch (NumberFormatException localNumberFormatException)
            {
            }
        }
    }

    public static final String getHost(Context paramContext)
    {
        java.net.Proxy localProxy = getProxy(paramContext, null);
        Object localObject;
        if (localProxy == java.net.Proxy.NO_PROXY)
            localObject = null;
        while (true)
        {
            return localObject;
            try
            {
                String str = ((InetSocketAddress)localProxy.address()).getHostName();
                localObject = str;
            }
            catch (Exception localException)
            {
                localObject = null;
            }
        }
    }

    public static final int getPort(Context paramContext)
    {
        java.net.Proxy localProxy = getProxy(paramContext, null);
        int i;
        if (localProxy == java.net.Proxy.NO_PROXY)
            i = -1;
        while (true)
        {
            return i;
            try
            {
                int j = ((InetSocketAddress)localProxy.address()).getPort();
                i = j;
            }
            catch (Exception localException)
            {
                i = -1;
            }
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public static final HttpHost getPreferredHttpHost(Context paramContext, String paramString)
    {
        java.net.Proxy localProxy = getProxy(paramContext, paramString);
        if (localProxy.equals(java.net.Proxy.NO_PROXY));
        InetSocketAddress localInetSocketAddress;
        for (HttpHost localHttpHost = null; ; localHttpHost = new HttpHost(Injector.getHostName(localInetSocketAddress), localInetSocketAddress.getPort(), "http"))
        {
            return localHttpHost;
            localInetSocketAddress = (InetSocketAddress)localProxy.address();
        }
    }

    public static final java.net.Proxy getProxy(Context paramContext, String paramString)
    {
        String str = "";
        if (paramString != null)
            str = URI.create(paramString).getHost();
        java.net.Proxy localProxy;
        if (!isLocalHost(str))
        {
            if (sConnectivityManager == null)
                sConnectivityManager = (ConnectivityManager)paramContext.getSystemService("connectivity");
            if (sConnectivityManager == null)
                localProxy = java.net.Proxy.NO_PROXY;
        }
        while (true)
        {
            return localProxy;
            ProxyProperties localProxyProperties = sConnectivityManager.getProxy();
            if ((localProxyProperties != null) && (!localProxyProperties.isExcluded(str)))
                localProxy = localProxyProperties.makeProxy();
            else
                localProxy = java.net.Proxy.NO_PROXY;
        }
    }

    private static final boolean isLocalHost(String paramString)
    {
        boolean bool1 = false;
        if (paramString == null);
        while (true)
        {
            return bool1;
            if (paramString != null)
                try
                {
                    if (paramString.equalsIgnoreCase("localhost"))
                    {
                        bool1 = true;
                    }
                    else
                    {
                        boolean bool2 = NetworkUtils.numericToInetAddress(paramString).isLoopbackAddress();
                        if (bool2)
                            bool1 = true;
                    }
                }
                catch (IllegalArgumentException localIllegalArgumentException)
                {
                }
        }
    }

    public static final void setHttpProxySystemProperty(ProxyProperties paramProxyProperties)
    {
        String str1 = null;
        String str2 = null;
        String str3 = null;
        if (paramProxyProperties != null)
        {
            str1 = paramProxyProperties.getHost();
            str2 = Integer.toString(paramProxyProperties.getPort());
            str3 = paramProxyProperties.getExclusionList();
        }
        setHttpProxySystemProperty(str1, str2, str3);
    }

    public static final void setHttpProxySystemProperty(String paramString1, String paramString2, String paramString3)
    {
        if (paramString3 != null)
            paramString3 = paramString3.replace(",", "|");
        if (paramString1 != null)
        {
            System.setProperty("http.proxyHost", paramString1);
            System.setProperty("https.proxyHost", paramString1);
            if (paramString2 == null)
                break label83;
            System.setProperty("http.proxyPort", paramString2);
            System.setProperty("https.proxyPort", paramString2);
            label49: if (paramString3 == null)
                break label98;
            System.setProperty("http.nonProxyHosts", paramString3);
            System.setProperty("https.nonProxyHosts", paramString3);
        }
        while (true)
        {
            return;
            System.clearProperty("http.proxyHost");
            System.clearProperty("https.proxyHost");
            break;
            label83: System.clearProperty("http.proxyPort");
            System.clearProperty("https.proxyPort");
            break label49;
            label98: System.clearProperty("http.nonProxyHosts");
            System.clearProperty("https.nonProxyHosts");
        }
    }

    public static void validate(String paramString1, String paramString2, String paramString3)
    {
        Matcher localMatcher1 = HOSTNAME_PATTERN.matcher(paramString1);
        Matcher localMatcher2 = EXCLLIST_PATTERN.matcher(paramString3);
        if (!localMatcher1.matches())
            throw new IllegalArgumentException();
        if (!localMatcher2.matches())
            throw new IllegalArgumentException();
        if ((paramString1.length() > 0) && (paramString2.length() == 0))
            throw new IllegalArgumentException();
        if (paramString2.length() > 0)
        {
            if (paramString1.length() == 0)
                throw new IllegalArgumentException();
            try
            {
                int i = Integer.parseInt(paramString2);
                if ((i <= 0) || (i > 65535))
                    throw new IllegalArgumentException();
            }
            catch (NumberFormatException localNumberFormatException)
            {
                throw new IllegalArgumentException();
            }
        }
    }

    static class AndroidProxySelectorRoutePlanner extends ProxySelectorRoutePlanner
    {
        private Context mContext;

        public AndroidProxySelectorRoutePlanner(SchemeRegistry paramSchemeRegistry, ProxySelector paramProxySelector, Context paramContext)
        {
            super(paramProxySelector);
            this.mContext = paramContext;
        }

        protected java.net.Proxy chooseProxy(List<java.net.Proxy> paramList, HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
        {
            return Proxy.getProxy(this.mContext, paramHttpHost.getHostName());
        }

        protected HttpHost determineProxy(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
        {
            return Proxy.getPreferredHttpHost(this.mContext, paramHttpHost.getHostName());
        }

        public HttpRoute determineRoute(HttpHost paramHttpHost, HttpRequest paramHttpRequest, HttpContext paramHttpContext)
        {
            HttpHost localHttpHost = Proxy.getPreferredHttpHost(this.mContext, paramHttpHost.getHostName());
            if (localHttpHost == null);
            for (HttpRoute localHttpRoute = new HttpRoute(paramHttpHost); ; localHttpRoute = new HttpRoute(paramHttpHost, null, localHttpHost, false))
                return localHttpRoute;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static String getHostName(InetSocketAddress paramInetSocketAddress)
        {
            String str = null;
            if (paramInetSocketAddress.getAddress() != null)
                str = paramInetSocketAddress.getAddress().getHostAddress();
            if (TextUtils.isEmpty(str))
                str = paramInetSocketAddress.getHostName();
            return str;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.Proxy
 * JD-Core Version:        0.6.2
 */