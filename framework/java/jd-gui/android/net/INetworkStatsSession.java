package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface INetworkStatsSession extends IInterface
{
    public abstract void close()
        throws RemoteException;

    public abstract NetworkStatsHistory getHistoryForNetwork(NetworkTemplate paramNetworkTemplate, int paramInt)
        throws RemoteException;

    public abstract NetworkStatsHistory getHistoryForUid(NetworkTemplate paramNetworkTemplate, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
        throws RemoteException;

    public abstract NetworkStats getSummaryForAllUid(NetworkTemplate paramNetworkTemplate, long paramLong1, long paramLong2, boolean paramBoolean)
        throws RemoteException;

    public abstract NetworkStats getSummaryForNetwork(NetworkTemplate paramNetworkTemplate, long paramLong1, long paramLong2)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements INetworkStatsSession
    {
        private static final String DESCRIPTOR = "android.net.INetworkStatsSession";
        static final int TRANSACTION_close = 5;
        static final int TRANSACTION_getHistoryForNetwork = 2;
        static final int TRANSACTION_getHistoryForUid = 4;
        static final int TRANSACTION_getSummaryForAllUid = 3;
        static final int TRANSACTION_getSummaryForNetwork = 1;

        public Stub()
        {
            attachInterface(this, "android.net.INetworkStatsSession");
        }

        public static INetworkStatsSession asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.net.INetworkStatsSession");
                if ((localIInterface != null) && ((localIInterface instanceof INetworkStatsSession)))
                    localObject = (INetworkStatsSession)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            }
            while (true)
            {
                return bool1;
                paramParcel2.writeString("android.net.INetworkStatsSession");
                bool1 = true;
                continue;
                paramParcel1.enforceInterface("android.net.INetworkStatsSession");
                NetworkTemplate localNetworkTemplate4;
                if (paramParcel1.readInt() != 0)
                {
                    localNetworkTemplate4 = (NetworkTemplate)NetworkTemplate.CREATOR.createFromParcel(paramParcel1);
                    label113: NetworkStats localNetworkStats2 = getSummaryForNetwork(localNetworkTemplate4, paramParcel1.readLong(), paramParcel1.readLong());
                    paramParcel2.writeNoException();
                    if (localNetworkStats2 == null)
                        break label162;
                    paramParcel2.writeInt(1);
                    localNetworkStats2.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool1 = true;
                    break;
                    localNetworkTemplate4 = null;
                    break label113;
                    label162: paramParcel2.writeInt(0);
                }
                paramParcel1.enforceInterface("android.net.INetworkStatsSession");
                NetworkTemplate localNetworkTemplate3;
                if (paramParcel1.readInt() != 0)
                {
                    localNetworkTemplate3 = (NetworkTemplate)NetworkTemplate.CREATOR.createFromParcel(paramParcel1);
                    label197: NetworkStatsHistory localNetworkStatsHistory2 = getHistoryForNetwork(localNetworkTemplate3, paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    if (localNetworkStatsHistory2 == null)
                        break label242;
                    paramParcel2.writeInt(1);
                    localNetworkStatsHistory2.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool1 = true;
                    break;
                    localNetworkTemplate3 = null;
                    break label197;
                    label242: paramParcel2.writeInt(0);
                }
                paramParcel1.enforceInterface("android.net.INetworkStatsSession");
                NetworkTemplate localNetworkTemplate2;
                label277: boolean bool2;
                if (paramParcel1.readInt() != 0)
                {
                    localNetworkTemplate2 = (NetworkTemplate)NetworkTemplate.CREATOR.createFromParcel(paramParcel1);
                    long l1 = paramParcel1.readLong();
                    long l2 = paramParcel1.readLong();
                    if (paramParcel1.readInt() == 0)
                        break label346;
                    bool2 = true;
                    label299: NetworkStats localNetworkStats1 = getSummaryForAllUid(localNetworkTemplate2, l1, l2, bool2);
                    paramParcel2.writeNoException();
                    if (localNetworkStats1 == null)
                        break label352;
                    paramParcel2.writeInt(1);
                    localNetworkStats1.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool1 = true;
                    break;
                    localNetworkTemplate2 = null;
                    break label277;
                    label346: bool2 = false;
                    break label299;
                    label352: paramParcel2.writeInt(0);
                }
                paramParcel1.enforceInterface("android.net.INetworkStatsSession");
                NetworkTemplate localNetworkTemplate1;
                if (paramParcel1.readInt() != 0)
                {
                    localNetworkTemplate1 = (NetworkTemplate)NetworkTemplate.CREATOR.createFromParcel(paramParcel1);
                    label387: int i = paramParcel1.readInt();
                    int j = paramParcel1.readInt();
                    int k = paramParcel1.readInt();
                    int m = paramParcel1.readInt();
                    NetworkStatsHistory localNetworkStatsHistory1 = getHistoryForUid(localNetworkTemplate1, i, j, k, m);
                    paramParcel2.writeNoException();
                    if (localNetworkStatsHistory1 == null)
                        break label460;
                    paramParcel2.writeInt(1);
                    localNetworkStatsHistory1.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool1 = true;
                    break;
                    localNetworkTemplate1 = null;
                    break label387;
                    label460: paramParcel2.writeInt(0);
                }
                paramParcel1.enforceInterface("android.net.INetworkStatsSession");
                close();
                paramParcel2.writeNoException();
                bool1 = true;
            }
        }

        private static class Proxy
            implements INetworkStatsSession
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void close()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.INetworkStatsSession");
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public NetworkStatsHistory getHistoryForNetwork(NetworkTemplate paramNetworkTemplate, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.net.INetworkStatsSession");
                        if (paramNetworkTemplate != null)
                        {
                            localParcel1.writeInt(1);
                            paramNetworkTemplate.writeToParcel(localParcel1, 0);
                            localParcel1.writeInt(paramInt);
                            this.mRemote.transact(2, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            if (localParcel2.readInt() != 0)
                            {
                                localNetworkStatsHistory = (NetworkStatsHistory)NetworkStatsHistory.CREATOR.createFromParcel(localParcel2);
                                return localNetworkStatsHistory;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    NetworkStatsHistory localNetworkStatsHistory = null;
                }
            }

            public NetworkStatsHistory getHistoryForUid(NetworkTemplate paramNetworkTemplate, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.net.INetworkStatsSession");
                        if (paramNetworkTemplate != null)
                        {
                            localParcel1.writeInt(1);
                            paramNetworkTemplate.writeToParcel(localParcel1, 0);
                            localParcel1.writeInt(paramInt1);
                            localParcel1.writeInt(paramInt2);
                            localParcel1.writeInt(paramInt3);
                            localParcel1.writeInt(paramInt4);
                            this.mRemote.transact(4, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            if (localParcel2.readInt() != 0)
                            {
                                localNetworkStatsHistory = (NetworkStatsHistory)NetworkStatsHistory.CREATOR.createFromParcel(localParcel2);
                                return localNetworkStatsHistory;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    NetworkStatsHistory localNetworkStatsHistory = null;
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.net.INetworkStatsSession";
            }

            public NetworkStats getSummaryForAllUid(NetworkTemplate paramNetworkTemplate, long paramLong1, long paramLong2, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.net.INetworkStatsSession");
                        if (paramNetworkTemplate != null)
                        {
                            localParcel1.writeInt(1);
                            paramNetworkTemplate.writeToParcel(localParcel1, 0);
                            localParcel1.writeLong(paramLong1);
                            localParcel1.writeLong(paramLong2);
                            if (paramBoolean)
                            {
                                localParcel1.writeInt(i);
                                this.mRemote.transact(3, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                                if (localParcel2.readInt() == 0)
                                    break label149;
                                localNetworkStats = (NetworkStats)NetworkStats.CREATOR.createFromParcel(localParcel2);
                                return localNetworkStats;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    i = 0;
                    continue;
                    label149: NetworkStats localNetworkStats = null;
                }
            }

            public NetworkStats getSummaryForNetwork(NetworkTemplate paramNetworkTemplate, long paramLong1, long paramLong2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.net.INetworkStatsSession");
                        if (paramNetworkTemplate != null)
                        {
                            localParcel1.writeInt(1);
                            paramNetworkTemplate.writeToParcel(localParcel1, 0);
                            localParcel1.writeLong(paramLong1);
                            localParcel1.writeLong(paramLong2);
                            this.mRemote.transact(1, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            if (localParcel2.readInt() != 0)
                            {
                                localNetworkStats = (NetworkStats)NetworkStats.CREATOR.createFromParcel(localParcel2);
                                return localNetworkStats;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    NetworkStats localNetworkStats = null;
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.INetworkStatsSession
 * JD-Core Version:        0.6.2
 */