package android.net;

import android.text.TextUtils;
import android.util.Log;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

public class DhcpInfoInternal
{
    private static final String TAG = "DhcpInfoInternal";
    public String dns1;
    public String dns2;
    public String ipAddress;
    public int leaseDuration;
    private Collection<RouteInfo> mRoutes = new ArrayList();
    public int prefixLength;
    public String serverAddress;
    public String vendorInfo;

    private int convertToInt(String paramString)
    {
        if (paramString != null);
        while (true)
        {
            try
            {
                InetAddress localInetAddress = NetworkUtils.numericToInetAddress(paramString);
                if ((localInetAddress instanceof Inet4Address))
                {
                    int j = NetworkUtils.inetAddressToInt(localInetAddress);
                    i = j;
                    return i;
                }
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
            }
            int i = 0;
        }
    }

    public void addRoute(RouteInfo paramRouteInfo)
    {
        this.mRoutes.add(paramRouteInfo);
    }

    public Collection<RouteInfo> getRoutes()
    {
        return Collections.unmodifiableCollection(this.mRoutes);
    }

    public boolean hasMeteredHint()
    {
        if (this.vendorInfo != null);
        for (boolean bool = this.vendorInfo.contains("ANDROID_METERED"); ; bool = false)
            return bool;
    }

    public DhcpInfo makeDhcpInfo()
    {
        DhcpInfo localDhcpInfo = new DhcpInfo();
        localDhcpInfo.ipAddress = convertToInt(this.ipAddress);
        Iterator localIterator = this.mRoutes.iterator();
        while (localIterator.hasNext())
        {
            RouteInfo localRouteInfo = (RouteInfo)localIterator.next();
            if (localRouteInfo.isDefaultRoute())
                localDhcpInfo.gateway = convertToInt(localRouteInfo.getGateway().getHostAddress());
        }
        try
        {
            NetworkUtils.numericToInetAddress(this.ipAddress);
            localDhcpInfo.netmask = NetworkUtils.prefixLengthToNetmaskInt(this.prefixLength);
            label93: localDhcpInfo.dns1 = convertToInt(this.dns1);
            localDhcpInfo.dns2 = convertToInt(this.dns2);
            localDhcpInfo.serverAddress = convertToInt(this.serverAddress);
            localDhcpInfo.leaseDuration = this.leaseDuration;
            return localDhcpInfo;
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
            break label93;
        }
    }

    public LinkAddress makeLinkAddress()
    {
        if (TextUtils.isEmpty(this.ipAddress))
            Log.e("DhcpInfoInternal", "makeLinkAddress with empty ipAddress");
        for (LinkAddress localLinkAddress = null; ; localLinkAddress = new LinkAddress(NetworkUtils.numericToInetAddress(this.ipAddress), this.prefixLength))
            return localLinkAddress;
    }

    public LinkProperties makeLinkProperties()
    {
        LinkProperties localLinkProperties = new LinkProperties();
        localLinkProperties.addLinkAddress(makeLinkAddress());
        Iterator localIterator = this.mRoutes.iterator();
        while (localIterator.hasNext())
            localLinkProperties.addRoute((RouteInfo)localIterator.next());
        if (!TextUtils.isEmpty(this.dns1))
        {
            localLinkProperties.addDns(NetworkUtils.numericToInetAddress(this.dns1));
            if (TextUtils.isEmpty(this.dns2))
                break label106;
            localLinkProperties.addDns(NetworkUtils.numericToInetAddress(this.dns2));
        }
        while (true)
        {
            return localLinkProperties;
            Log.d("DhcpInfoInternal", "makeLinkProperties with empty dns1!");
            break;
            label106: Log.d("DhcpInfoInternal", "makeLinkProperties with empty dns2!");
        }
    }

    public String toString()
    {
        String str = "";
        Iterator localIterator = this.mRoutes.iterator();
        while (localIterator.hasNext())
        {
            RouteInfo localRouteInfo = (RouteInfo)localIterator.next();
            str = str + localRouteInfo.toString() + " | ";
        }
        return "addr: " + this.ipAddress + "/" + this.prefixLength + " mRoutes: " + str + " dns: " + this.dns1 + "," + this.dns2 + " dhcpServer: " + this.serverAddress + " leaseDuration: " + this.leaseDuration;
    }

    public void updateFromDhcpRequest(DhcpInfoInternal paramDhcpInfoInternal)
    {
        if (paramDhcpInfoInternal == null);
        while (true)
        {
            return;
            if (TextUtils.isEmpty(this.dns1))
                this.dns1 = paramDhcpInfoInternal.dns1;
            if (TextUtils.isEmpty(this.dns2))
                this.dns2 = paramDhcpInfoInternal.dns2;
            if (this.mRoutes.size() == 0)
            {
                Iterator localIterator = paramDhcpInfoInternal.getRoutes().iterator();
                while (localIterator.hasNext())
                    addRoute((RouteInfo)localIterator.next());
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.DhcpInfoInternal
 * JD-Core Version:        0.6.2
 */