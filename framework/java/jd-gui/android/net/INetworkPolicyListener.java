package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface INetworkPolicyListener extends IInterface
{
    public abstract void onMeteredIfacesChanged(String[] paramArrayOfString)
        throws RemoteException;

    public abstract void onRestrictBackgroundChanged(boolean paramBoolean)
        throws RemoteException;

    public abstract void onUidRulesChanged(int paramInt1, int paramInt2)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements INetworkPolicyListener
    {
        private static final String DESCRIPTOR = "android.net.INetworkPolicyListener";
        static final int TRANSACTION_onMeteredIfacesChanged = 2;
        static final int TRANSACTION_onRestrictBackgroundChanged = 3;
        static final int TRANSACTION_onUidRulesChanged = 1;

        public Stub()
        {
            attachInterface(this, "android.net.INetworkPolicyListener");
        }

        public static INetworkPolicyListener asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.net.INetworkPolicyListener");
                if ((localIInterface != null) && ((localIInterface instanceof INetworkPolicyListener)))
                    localObject = (INetworkPolicyListener)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool1 = true;
            switch (paramInt1)
            {
            default:
                bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
                while (true)
                {
                    return bool1;
                    paramParcel2.writeString("android.net.INetworkPolicyListener");
                    continue;
                    paramParcel1.enforceInterface("android.net.INetworkPolicyListener");
                    onUidRulesChanged(paramParcel1.readInt(), paramParcel1.readInt());
                    continue;
                    paramParcel1.enforceInterface("android.net.INetworkPolicyListener");
                    onMeteredIfacesChanged(paramParcel1.createStringArray());
                }
            case 3:
            }
            paramParcel1.enforceInterface("android.net.INetworkPolicyListener");
            if (paramParcel1.readInt() != 0);
            for (boolean bool2 = bool1; ; bool2 = false)
            {
                onRestrictBackgroundChanged(bool2);
                break;
            }
        }

        private static class Proxy
            implements INetworkPolicyListener
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.net.INetworkPolicyListener";
            }

            public void onMeteredIfacesChanged(String[] paramArrayOfString)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.net.INetworkPolicyListener");
                    localParcel.writeStringArray(paramArrayOfString);
                    this.mRemote.transact(2, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onRestrictBackgroundChanged(boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.net.INetworkPolicyListener");
                    if (paramBoolean)
                    {
                        localParcel.writeInt(i);
                        this.mRemote.transact(3, localParcel, null, 1);
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void onUidRulesChanged(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.net.INetworkPolicyListener");
                    localParcel.writeInt(paramInt1);
                    localParcel.writeInt(paramInt2);
                    this.mRemote.transact(1, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.INetworkPolicyListener
 * JD-Core Version:        0.6.2
 */