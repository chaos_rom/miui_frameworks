package android.net.wifi;

import android.net.DhcpInfo;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import android.os.WorkSource;
import java.util.ArrayList;
import java.util.List;

public abstract interface IWifiManager extends IInterface
{
    public abstract void acquireMulticastLock(IBinder paramIBinder, String paramString)
        throws RemoteException;

    public abstract boolean acquireWifiLock(IBinder paramIBinder, int paramInt, String paramString, WorkSource paramWorkSource)
        throws RemoteException;

    public abstract int addOrUpdateNetwork(WifiConfiguration paramWifiConfiguration)
        throws RemoteException;

    public abstract void addToBlacklist(String paramString)
        throws RemoteException;

    public abstract void clearBlacklist()
        throws RemoteException;

    public abstract boolean disableNetwork(int paramInt)
        throws RemoteException;

    public abstract void disconnect()
        throws RemoteException;

    public abstract boolean enableNetwork(int paramInt, boolean paramBoolean)
        throws RemoteException;

    public abstract String getConfigFile()
        throws RemoteException;

    public abstract List<WifiConfiguration> getConfiguredNetworks()
        throws RemoteException;

    public abstract WifiInfo getConnectionInfo()
        throws RemoteException;

    public abstract DhcpInfo getDhcpInfo()
        throws RemoteException;

    public abstract int getFrequencyBand()
        throws RemoteException;

    public abstract List<ScanResult> getScanResults()
        throws RemoteException;

    public abstract WifiConfiguration getWifiApConfiguration()
        throws RemoteException;

    public abstract int getWifiApEnabledState()
        throws RemoteException;

    public abstract int getWifiEnabledState()
        throws RemoteException;

    public abstract Messenger getWifiServiceMessenger()
        throws RemoteException;

    public abstract Messenger getWifiStateMachineMessenger()
        throws RemoteException;

    public abstract void initializeMulticastFiltering()
        throws RemoteException;

    public abstract boolean isDualBandSupported()
        throws RemoteException;

    public abstract boolean isMulticastEnabled()
        throws RemoteException;

    public abstract boolean pingSupplicant()
        throws RemoteException;

    public abstract void reassociate()
        throws RemoteException;

    public abstract void reconnect()
        throws RemoteException;

    public abstract void releaseMulticastLock()
        throws RemoteException;

    public abstract boolean releaseWifiLock(IBinder paramIBinder)
        throws RemoteException;

    public abstract boolean removeNetwork(int paramInt)
        throws RemoteException;

    public abstract boolean saveConfiguration()
        throws RemoteException;

    public abstract void setCountryCode(String paramString, boolean paramBoolean)
        throws RemoteException;

    public abstract void setFrequencyBand(int paramInt, boolean paramBoolean)
        throws RemoteException;

    public abstract void setWifiApConfiguration(WifiConfiguration paramWifiConfiguration)
        throws RemoteException;

    public abstract void setWifiApEnabled(WifiConfiguration paramWifiConfiguration, boolean paramBoolean)
        throws RemoteException;

    public abstract boolean setWifiEnabled(boolean paramBoolean)
        throws RemoteException;

    public abstract void startScan(boolean paramBoolean)
        throws RemoteException;

    public abstract void startWifi()
        throws RemoteException;

    public abstract void stopWifi()
        throws RemoteException;

    public abstract void updateWifiLockWorkSource(IBinder paramIBinder, WorkSource paramWorkSource)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IWifiManager
    {
        private static final String DESCRIPTOR = "android.net.wifi.IWifiManager";
        static final int TRANSACTION_acquireMulticastLock = 26;
        static final int TRANSACTION_acquireWifiLock = 21;
        static final int TRANSACTION_addOrUpdateNetwork = 2;
        static final int TRANSACTION_addToBlacklist = 34;
        static final int TRANSACTION_clearBlacklist = 35;
        static final int TRANSACTION_disableNetwork = 5;
        static final int TRANSACTION_disconnect = 9;
        static final int TRANSACTION_enableNetwork = 4;
        static final int TRANSACTION_getConfigFile = 38;
        static final int TRANSACTION_getConfiguredNetworks = 1;
        static final int TRANSACTION_getConnectionInfo = 12;
        static final int TRANSACTION_getDhcpInfo = 20;
        static final int TRANSACTION_getFrequencyBand = 17;
        static final int TRANSACTION_getScanResults = 8;
        static final int TRANSACTION_getWifiApConfiguration = 30;
        static final int TRANSACTION_getWifiApEnabledState = 29;
        static final int TRANSACTION_getWifiEnabledState = 14;
        static final int TRANSACTION_getWifiServiceMessenger = 36;
        static final int TRANSACTION_getWifiStateMachineMessenger = 37;
        static final int TRANSACTION_initializeMulticastFiltering = 24;
        static final int TRANSACTION_isDualBandSupported = 18;
        static final int TRANSACTION_isMulticastEnabled = 25;
        static final int TRANSACTION_pingSupplicant = 6;
        static final int TRANSACTION_reassociate = 11;
        static final int TRANSACTION_reconnect = 10;
        static final int TRANSACTION_releaseMulticastLock = 27;
        static final int TRANSACTION_releaseWifiLock = 23;
        static final int TRANSACTION_removeNetwork = 3;
        static final int TRANSACTION_saveConfiguration = 19;
        static final int TRANSACTION_setCountryCode = 15;
        static final int TRANSACTION_setFrequencyBand = 16;
        static final int TRANSACTION_setWifiApConfiguration = 31;
        static final int TRANSACTION_setWifiApEnabled = 28;
        static final int TRANSACTION_setWifiEnabled = 13;
        static final int TRANSACTION_startScan = 7;
        static final int TRANSACTION_startWifi = 32;
        static final int TRANSACTION_stopWifi = 33;
        static final int TRANSACTION_updateWifiLockWorkSource = 22;

        public Stub()
        {
            attachInterface(this, "android.net.wifi.IWifiManager");
        }

        public static IWifiManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.net.wifi.IWifiManager");
                if ((localIInterface != null) && ((localIInterface instanceof IWifiManager)))
                    localObject = (IWifiManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            }
            while (true)
            {
                return j;
                paramParcel2.writeString("android.net.wifi.IWifiManager");
                continue;
                paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                List localList2 = getConfiguredNetworks();
                paramParcel2.writeNoException();
                paramParcel2.writeTypedList(localList2);
                continue;
                paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                if (paramParcel1.readInt() != 0);
                for (WifiConfiguration localWifiConfiguration4 = (WifiConfiguration)WifiConfiguration.CREATOR.createFromParcel(paramParcel1); ; localWifiConfiguration4 = null)
                {
                    int i16 = addOrUpdateNetwork(localWifiConfiguration4);
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i16);
                    break;
                }
                paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                boolean bool10 = removeNetwork(paramParcel1.readInt());
                paramParcel2.writeNoException();
                if (bool10)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                int i13 = paramParcel1.readInt();
                if (paramParcel1.readInt() != 0);
                int i15;
                for (int i14 = j; ; i15 = 0)
                {
                    boolean bool9 = enableNetwork(i13, i14);
                    paramParcel2.writeNoException();
                    if (bool9)
                        i = j;
                    paramParcel2.writeInt(i);
                    break;
                }
                paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                boolean bool8 = disableNetwork(paramParcel1.readInt());
                paramParcel2.writeNoException();
                if (bool8)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                boolean bool7 = pingSupplicant();
                paramParcel2.writeNoException();
                if (bool7)
                    i = j;
                paramParcel2.writeInt(i);
                continue;
                paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                if (paramParcel1.readInt() != 0);
                int i12;
                for (int i11 = j; ; i12 = 0)
                {
                    startScan(i11);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                List localList1 = getScanResults();
                paramParcel2.writeNoException();
                paramParcel2.writeTypedList(localList1);
                continue;
                paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                disconnect();
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                reconnect();
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                reassociate();
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                WifiInfo localWifiInfo = getConnectionInfo();
                paramParcel2.writeNoException();
                if (localWifiInfo != null)
                {
                    paramParcel2.writeInt(j);
                    localWifiInfo.writeToParcel(paramParcel2, j);
                }
                else
                {
                    paramParcel2.writeInt(0);
                    continue;
                    paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                    if (paramParcel1.readInt() != 0);
                    int i10;
                    for (int i9 = j; ; i10 = 0)
                    {
                        boolean bool6 = setWifiEnabled(i9);
                        paramParcel2.writeNoException();
                        if (bool6)
                            i = j;
                        paramParcel2.writeInt(i);
                        break;
                    }
                    paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                    int i8 = getWifiEnabledState();
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i8);
                    continue;
                    paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                    String str3 = paramParcel1.readString();
                    if (paramParcel1.readInt() != 0);
                    int i7;
                    for (int i6 = j; ; i7 = 0)
                    {
                        setCountryCode(str3, i6);
                        paramParcel2.writeNoException();
                        break;
                    }
                    paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                    int i3 = paramParcel1.readInt();
                    if (paramParcel1.readInt() != 0);
                    int i5;
                    for (int i4 = j; ; i5 = 0)
                    {
                        setFrequencyBand(i3, i4);
                        paramParcel2.writeNoException();
                        break;
                    }
                    paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                    int i2 = getFrequencyBand();
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i2);
                    continue;
                    paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                    boolean bool5 = isDualBandSupported();
                    paramParcel2.writeNoException();
                    if (bool5)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                    boolean bool4 = saveConfiguration();
                    paramParcel2.writeNoException();
                    if (bool4)
                        i = j;
                    paramParcel2.writeInt(i);
                    continue;
                    paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                    DhcpInfo localDhcpInfo = getDhcpInfo();
                    paramParcel2.writeNoException();
                    if (localDhcpInfo != null)
                    {
                        paramParcel2.writeInt(j);
                        localDhcpInfo.writeToParcel(paramParcel2, j);
                    }
                    else
                    {
                        paramParcel2.writeInt(0);
                        continue;
                        paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                        IBinder localIBinder2 = paramParcel1.readStrongBinder();
                        int i1 = paramParcel1.readInt();
                        String str2 = paramParcel1.readString();
                        if (paramParcel1.readInt() != 0);
                        for (WorkSource localWorkSource2 = (WorkSource)WorkSource.CREATOR.createFromParcel(paramParcel1); ; localWorkSource2 = null)
                        {
                            boolean bool3 = acquireWifiLock(localIBinder2, i1, str2, localWorkSource2);
                            paramParcel2.writeNoException();
                            if (bool3)
                                i = j;
                            paramParcel2.writeInt(i);
                            break;
                        }
                        paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                        IBinder localIBinder1 = paramParcel1.readStrongBinder();
                        if (paramParcel1.readInt() != 0);
                        for (WorkSource localWorkSource1 = (WorkSource)WorkSource.CREATOR.createFromParcel(paramParcel1); ; localWorkSource1 = null)
                        {
                            updateWifiLockWorkSource(localIBinder1, localWorkSource1);
                            paramParcel2.writeNoException();
                            break;
                        }
                        paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                        boolean bool2 = releaseWifiLock(paramParcel1.readStrongBinder());
                        paramParcel2.writeNoException();
                        if (bool2)
                            i = j;
                        paramParcel2.writeInt(i);
                        continue;
                        paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                        initializeMulticastFiltering();
                        paramParcel2.writeNoException();
                        continue;
                        paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                        boolean bool1 = isMulticastEnabled();
                        paramParcel2.writeNoException();
                        if (bool1)
                            i = j;
                        paramParcel2.writeInt(i);
                        continue;
                        paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                        acquireMulticastLock(paramParcel1.readStrongBinder(), paramParcel1.readString());
                        paramParcel2.writeNoException();
                        continue;
                        paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                        releaseMulticastLock();
                        paramParcel2.writeNoException();
                        continue;
                        paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                        WifiConfiguration localWifiConfiguration3;
                        if (paramParcel1.readInt() != 0)
                        {
                            localWifiConfiguration3 = (WifiConfiguration)WifiConfiguration.CREATOR.createFromParcel(paramParcel1);
                            label1363: if (paramParcel1.readInt() == 0)
                                break label1395;
                        }
                        label1395: int n;
                        for (int m = j; ; n = 0)
                        {
                            setWifiApEnabled(localWifiConfiguration3, m);
                            paramParcel2.writeNoException();
                            break;
                            localWifiConfiguration3 = null;
                            break label1363;
                        }
                        paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                        int k = getWifiApEnabledState();
                        paramParcel2.writeNoException();
                        paramParcel2.writeInt(k);
                        continue;
                        paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                        WifiConfiguration localWifiConfiguration2 = getWifiApConfiguration();
                        paramParcel2.writeNoException();
                        if (localWifiConfiguration2 != null)
                        {
                            paramParcel2.writeInt(j);
                            localWifiConfiguration2.writeToParcel(paramParcel2, j);
                        }
                        else
                        {
                            paramParcel2.writeInt(0);
                            continue;
                            paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                            if (paramParcel1.readInt() != 0);
                            for (WifiConfiguration localWifiConfiguration1 = (WifiConfiguration)WifiConfiguration.CREATOR.createFromParcel(paramParcel1); ; localWifiConfiguration1 = null)
                            {
                                setWifiApConfiguration(localWifiConfiguration1);
                                paramParcel2.writeNoException();
                                break;
                            }
                            paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                            startWifi();
                            paramParcel2.writeNoException();
                            continue;
                            paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                            stopWifi();
                            paramParcel2.writeNoException();
                            continue;
                            paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                            addToBlacklist(paramParcel1.readString());
                            paramParcel2.writeNoException();
                            continue;
                            paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                            clearBlacklist();
                            paramParcel2.writeNoException();
                            continue;
                            paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                            Messenger localMessenger2 = getWifiServiceMessenger();
                            paramParcel2.writeNoException();
                            if (localMessenger2 != null)
                            {
                                paramParcel2.writeInt(j);
                                localMessenger2.writeToParcel(paramParcel2, j);
                            }
                            else
                            {
                                paramParcel2.writeInt(0);
                                continue;
                                paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                                Messenger localMessenger1 = getWifiStateMachineMessenger();
                                paramParcel2.writeNoException();
                                if (localMessenger1 != null)
                                {
                                    paramParcel2.writeInt(j);
                                    localMessenger1.writeToParcel(paramParcel2, j);
                                }
                                else
                                {
                                    paramParcel2.writeInt(0);
                                    continue;
                                    paramParcel1.enforceInterface("android.net.wifi.IWifiManager");
                                    String str1 = getConfigFile();
                                    paramParcel2.writeNoException();
                                    paramParcel2.writeString(str1);
                                }
                            }
                        }
                    }
                }
            }
        }

        private static class Proxy
            implements IWifiManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public void acquireMulticastLock(IBinder paramIBinder, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(26, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean acquireWifiLock(IBinder paramIBinder, int paramInt, String paramString, WorkSource paramWorkSource)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                        localParcel1.writeStrongBinder(paramIBinder);
                        localParcel1.writeInt(paramInt);
                        localParcel1.writeString(paramString);
                        if (paramWorkSource != null)
                        {
                            localParcel1.writeInt(1);
                            paramWorkSource.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(21, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public int addOrUpdateNetwork(WifiConfiguration paramWifiConfiguration)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    if (paramWifiConfiguration != null)
                    {
                        localParcel1.writeInt(1);
                        paramWifiConfiguration.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(2, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void addToBlacklist(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(34, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void clearBlacklist()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    this.mRemote.transact(35, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean disableNetwork(int paramInt)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void disconnect()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    this.mRemote.transact(9, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean enableNetwork(int paramInt, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    localParcel1.writeInt(paramInt);
                    if (paramBoolean);
                    int k;
                    for (int j = i; ; k = 0)
                    {
                        localParcel1.writeInt(j);
                        this.mRemote.transact(4, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int m = localParcel2.readInt();
                        if (m == 0)
                            break;
                        return i;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getConfigFile()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    this.mRemote.transact(38, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<WifiConfiguration> getConfiguredNetworks()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(WifiConfiguration.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public WifiInfo getConnectionInfo()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    this.mRemote.transact(12, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localWifiInfo = (WifiInfo)WifiInfo.CREATOR.createFromParcel(localParcel2);
                        return localWifiInfo;
                    }
                    WifiInfo localWifiInfo = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public DhcpInfo getDhcpInfo()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    this.mRemote.transact(20, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localDhcpInfo = (DhcpInfo)DhcpInfo.CREATOR.createFromParcel(localParcel2);
                        return localDhcpInfo;
                    }
                    DhcpInfo localDhcpInfo = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getFrequencyBand()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    this.mRemote.transact(17, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.net.wifi.IWifiManager";
            }

            public List<ScanResult> getScanResults()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(ScanResult.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public WifiConfiguration getWifiApConfiguration()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    this.mRemote.transact(30, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localWifiConfiguration = (WifiConfiguration)WifiConfiguration.CREATOR.createFromParcel(localParcel2);
                        return localWifiConfiguration;
                    }
                    WifiConfiguration localWifiConfiguration = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getWifiApEnabledState()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    this.mRemote.transact(29, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getWifiEnabledState()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    this.mRemote.transact(14, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public Messenger getWifiServiceMessenger()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    this.mRemote.transact(36, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localMessenger = (Messenger)Messenger.CREATOR.createFromParcel(localParcel2);
                        return localMessenger;
                    }
                    Messenger localMessenger = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public Messenger getWifiStateMachineMessenger()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    this.mRemote.transact(37, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localMessenger = (Messenger)Messenger.CREATOR.createFromParcel(localParcel2);
                        return localMessenger;
                    }
                    Messenger localMessenger = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void initializeMulticastFiltering()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    this.mRemote.transact(24, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isDualBandSupported()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    this.mRemote.transact(18, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isMulticastEnabled()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    this.mRemote.transact(25, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean pingSupplicant()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void reassociate()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    this.mRemote.transact(11, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void reconnect()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    this.mRemote.transact(10, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void releaseMulticastLock()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    this.mRemote.transact(27, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean releaseWifiLock(IBinder paramIBinder)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(23, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean removeNetwork(int paramInt)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean saveConfiguration()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    this.mRemote.transact(19, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setCountryCode(String paramString, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    localParcel1.writeString(paramString);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(15, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setFrequencyBand(int paramInt, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    localParcel1.writeInt(paramInt);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(16, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setWifiApConfiguration(WifiConfiguration paramWifiConfiguration)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    if (paramWifiConfiguration != null)
                    {
                        localParcel1.writeInt(1);
                        paramWifiConfiguration.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(31, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setWifiApEnabled(WifiConfiguration paramWifiConfiguration, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                        if (paramWifiConfiguration != null)
                        {
                            localParcel1.writeInt(1);
                            paramWifiConfiguration.writeToParcel(localParcel1, 0);
                            break label107;
                            localParcel1.writeInt(i);
                            this.mRemote.transact(28, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    label107: 
                    while (!paramBoolean)
                    {
                        i = 0;
                        break;
                    }
                }
            }

            public boolean setWifiEnabled(boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    if (paramBoolean);
                    int k;
                    for (int j = i; ; k = 0)
                    {
                        localParcel1.writeInt(j);
                        this.mRemote.transact(13, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int m = localParcel2.readInt();
                        if (m == 0)
                            break;
                        return i;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void startScan(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void startWifi()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    this.mRemote.transact(32, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void stopWifi()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    this.mRemote.transact(33, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void updateWifiLockWorkSource(IBinder paramIBinder, WorkSource paramWorkSource)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.IWifiManager");
                    localParcel1.writeStrongBinder(paramIBinder);
                    if (paramWorkSource != null)
                    {
                        localParcel1.writeInt(1);
                        paramWorkSource.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(22, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.IWifiManager
 * JD-Core Version:        0.6.2
 */