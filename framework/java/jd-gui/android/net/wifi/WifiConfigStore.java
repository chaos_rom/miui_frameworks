package android.net.wifi;

import android.content.Context;
import android.content.Intent;
import android.net.DhcpInfoInternal;
import android.net.LinkAddress;
import android.net.LinkProperties;
import android.net.NetworkInfo.DetailedState;
import android.net.ProxyProperties;
import android.net.RouteInfo;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.TextUtils;
import android.util.Log;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

class WifiConfigStore
{
    private static final boolean DBG = false;
    private static final String DNS_KEY = "dns";
    private static final String EOS = "eos";
    private static final String EXCLUSION_LIST_KEY = "exclusionList";
    private static final String GATEWAY_KEY = "gateway";
    private static final String ID_KEY = "id";
    private static final int IPCONFIG_FILE_VERSION = 2;
    private static final String IP_ASSIGNMENT_KEY = "ipAssignment";
    private static final String LINK_ADDRESS_KEY = "linkAddress";
    private static final String PROXY_HOST_KEY = "proxyHost";
    private static final String PROXY_PORT_KEY = "proxyPort";
    private static final String PROXY_SETTINGS_KEY = "proxySettings";
    private static final String TAG = "WifiConfigStore";
    private static final String ipConfigFile = Environment.getDataDirectory() + "/misc/wifi/ipconfig.txt";
    private HashMap<Integer, WifiConfiguration> mConfiguredNetworks = new HashMap();
    private Context mContext;
    private int mLastPriority = -1;
    private HashMap<Integer, Integer> mNetworkIds = new HashMap();
    private WifiNative mWifiNative;

    WifiConfigStore(Context paramContext, WifiNative paramWifiNative)
    {
        this.mContext = paramContext;
        this.mWifiNative = paramWifiNative;
    }

    private void addIpSettingsFromConfig(LinkProperties paramLinkProperties, WifiConfiguration paramWifiConfiguration)
    {
        Iterator localIterator1 = paramWifiConfiguration.linkProperties.getLinkAddresses().iterator();
        while (localIterator1.hasNext())
            paramLinkProperties.addLinkAddress((LinkAddress)localIterator1.next());
        Iterator localIterator2 = paramWifiConfiguration.linkProperties.getRoutes().iterator();
        while (localIterator2.hasNext())
            paramLinkProperties.addRoute((RouteInfo)localIterator2.next());
        Iterator localIterator3 = paramWifiConfiguration.linkProperties.getDnses().iterator();
        while (localIterator3.hasNext())
            paramLinkProperties.addDns((InetAddress)localIterator3.next());
    }

    private NetworkUpdateResult addOrUpdateNetworkNative(WifiConfiguration paramWifiConfiguration)
    {
        int i = paramWifiConfiguration.networkId;
        boolean bool = false;
        int j;
        label96: NetworkUpdateResult localNetworkUpdateResult;
        if (i == -1)
        {
            Integer localInteger = (Integer)this.mNetworkIds.get(Integer.valueOf(configKey(paramWifiConfiguration)));
            if (localInteger != null)
                i = localInteger.intValue();
        }
        else
        {
            j = 1;
            if ((paramWifiConfiguration.SSID == null) || (this.mWifiNative.setNetworkVariable(i, "ssid", paramWifiConfiguration.SSID)))
                break label185;
            loge("failed to set SSID: " + paramWifiConfiguration.SSID);
            if (j == 0)
                break label1120;
            if (bool)
            {
                this.mWifiNative.removeNetwork(i);
                loge("Failed to set a network variable, removed network: " + i);
            }
            localNetworkUpdateResult = new NetworkUpdateResult(-1);
        }
        while (true)
        {
            return localNetworkUpdateResult;
            bool = true;
            i = this.mWifiNative.addNetwork();
            if (i >= 0)
                break;
            loge("Failed to add a network!");
            localNetworkUpdateResult = new NetworkUpdateResult(-1);
            continue;
            label185: if ((paramWifiConfiguration.BSSID != null) && (!this.mWifiNative.setNetworkVariable(i, "bssid", paramWifiConfiguration.BSSID)))
            {
                loge("failed to set BSSID: " + paramWifiConfiguration.BSSID);
                break label96;
            }
            String str1 = makeString(paramWifiConfiguration.allowedKeyManagement, WifiConfiguration.KeyMgmt.strings);
            if ((paramWifiConfiguration.allowedKeyManagement.cardinality() != 0) && (!this.mWifiNative.setNetworkVariable(i, "key_mgmt", str1)))
            {
                loge("failed to set key_mgmt: " + str1);
                break label96;
            }
            String str2 = makeString(paramWifiConfiguration.allowedProtocols, WifiConfiguration.Protocol.strings);
            if ((paramWifiConfiguration.allowedProtocols.cardinality() != 0) && (!this.mWifiNative.setNetworkVariable(i, "proto", str2)))
            {
                loge("failed to set proto: " + str2);
                break label96;
            }
            String str3 = makeString(paramWifiConfiguration.allowedAuthAlgorithms, WifiConfiguration.AuthAlgorithm.strings);
            if ((paramWifiConfiguration.allowedAuthAlgorithms.cardinality() != 0) && (!this.mWifiNative.setNetworkVariable(i, "auth_alg", str3)))
            {
                loge("failed to set auth_alg: " + str3);
                break label96;
            }
            String str4 = makeString(paramWifiConfiguration.allowedPairwiseCiphers, WifiConfiguration.PairwiseCipher.strings);
            if ((paramWifiConfiguration.allowedPairwiseCiphers.cardinality() != 0) && (!this.mWifiNative.setNetworkVariable(i, "pairwise", str4)))
            {
                loge("failed to set pairwise: " + str4);
                break label96;
            }
            String str5 = makeString(paramWifiConfiguration.allowedGroupCiphers, WifiConfiguration.GroupCipher.strings);
            if ((paramWifiConfiguration.allowedGroupCiphers.cardinality() != 0) && (!this.mWifiNative.setNetworkVariable(i, "group", str5)))
            {
                loge("failed to set group: " + str5);
                break label96;
            }
            if ((paramWifiConfiguration.preSharedKey != null) && (!paramWifiConfiguration.preSharedKey.equals("*")) && (!this.mWifiNative.setNetworkVariable(i, "psk", paramWifiConfiguration.preSharedKey)))
            {
                loge("failed to set psk");
                break label96;
            }
            int k = 0;
            if (paramWifiConfiguration.wepKeys != null)
                for (int i2 = 0; ; i2++)
                {
                    if (i2 >= paramWifiConfiguration.wepKeys.length)
                        break label745;
                    if ((paramWifiConfiguration.wepKeys[i2] != null) && (!paramWifiConfiguration.wepKeys[i2].equals("*")))
                    {
                        if (!this.mWifiNative.setNetworkVariable(i, WifiConfiguration.wepKeyVarNames[i2], paramWifiConfiguration.wepKeys[i2]))
                        {
                            loge("failed to set wep_key" + i2 + ": " + paramWifiConfiguration.wepKeys[i2]);
                            break;
                        }
                        k = 1;
                    }
                }
            label745: if ((k != 0) && (!this.mWifiNative.setNetworkVariable(i, "wep_tx_keyidx", Integer.toString(paramWifiConfiguration.wepTxKeyIndex))))
            {
                loge("failed to set wep_tx_keyidx: " + paramWifiConfiguration.wepTxKeyIndex);
                break label96;
            }
            if (!this.mWifiNative.setNetworkVariable(i, "priority", Integer.toString(paramWifiConfiguration.priority)))
            {
                loge(paramWifiConfiguration.SSID + ": failed to set priority: " + paramWifiConfiguration.priority);
                break label96;
            }
            if (paramWifiConfiguration.hiddenSSID)
            {
                WifiNative localWifiNative = this.mWifiNative;
                if (paramWifiConfiguration.hiddenSSID);
                for (int i1 = 1; ; i1 = 0)
                {
                    if (localWifiNative.setNetworkVariable(i, "scan_ssid", Integer.toString(i1)))
                        break label942;
                    loge(paramWifiConfiguration.SSID + ": failed to set hiddenSSID: " + paramWifiConfiguration.hiddenSSID);
                    break;
                }
            }
            label942: WifiConfiguration.EnterpriseField[] arrayOfEnterpriseField = paramWifiConfiguration.enterpriseFields;
            int m = arrayOfEnterpriseField.length;
            for (int n = 0; ; n++)
            {
                if (n >= m)
                    break label1114;
                WifiConfiguration.EnterpriseField localEnterpriseField = arrayOfEnterpriseField[n];
                String str6 = localEnterpriseField.varName();
                String str7 = localEnterpriseField.value();
                if (str7 != null)
                {
                    if (localEnterpriseField == paramWifiConfiguration.engine)
                        if (str7.length() == 0)
                            str7 = "0";
                    while (true)
                        if (!this.mWifiNative.setNetworkVariable(i, str6, str7))
                        {
                            loge(paramWifiConfiguration.SSID + ": failed to set " + str6 + ": " + str7);
                            break;
                            if (localEnterpriseField != paramWifiConfiguration.eap)
                            {
                                if (str7.length() == 0);
                                for (str7 = "NULL"; ; str7 = convertToQuotedString(str7))
                                    break;
                            }
                        }
                }
            }
            label1114: j = 0;
            break label96;
            label1120: WifiConfiguration localWifiConfiguration = (WifiConfiguration)this.mConfiguredNetworks.get(Integer.valueOf(i));
            if (localWifiConfiguration == null)
            {
                localWifiConfiguration = new WifiConfiguration();
                localWifiConfiguration.networkId = i;
            }
            readNetworkVariables(localWifiConfiguration);
            this.mConfiguredNetworks.put(Integer.valueOf(i), localWifiConfiguration);
            this.mNetworkIds.put(Integer.valueOf(configKey(localWifiConfiguration)), Integer.valueOf(i));
            localNetworkUpdateResult = writeIpAndProxyConfigurationsOnChange(localWifiConfiguration, paramWifiConfiguration);
            localNetworkUpdateResult.setIsNewNetwork(bool);
            localNetworkUpdateResult.setNetworkId(i);
        }
    }

    private static int configKey(WifiConfiguration paramWifiConfiguration)
    {
        String str;
        if (paramWifiConfiguration.allowedKeyManagement.get(1))
            str = paramWifiConfiguration.SSID + WifiConfiguration.KeyMgmt.strings[1];
        while (true)
        {
            return str.hashCode();
            if ((paramWifiConfiguration.allowedKeyManagement.get(2)) || (paramWifiConfiguration.allowedKeyManagement.get(3)))
                str = paramWifiConfiguration.SSID + WifiConfiguration.KeyMgmt.strings[2];
            else if (paramWifiConfiguration.wepKeys[0] != null)
                str = paramWifiConfiguration.SSID + "WEP";
            else
                str = paramWifiConfiguration.SSID + WifiConfiguration.KeyMgmt.strings[0];
        }
    }

    private String convertToQuotedString(String paramString)
    {
        return "\"" + paramString + "\"";
    }

    private void log(String paramString)
    {
        Log.d("WifiConfigStore", paramString);
    }

    private void loge(String paramString)
    {
        Log.e("WifiConfigStore", paramString);
    }

    private int lookupString(String paramString, String[] paramArrayOfString)
    {
        int i = paramArrayOfString.length;
        String str = paramString.replace('-', '_');
        int j = 0;
        if (j < i)
            if (!str.equals(paramArrayOfString[j]));
        while (true)
        {
            return j;
            j++;
            break;
            loge("Failed to look-up a string: " + str);
            j = -1;
        }
    }

    private String makeString(BitSet paramBitSet, String[] paramArrayOfString)
    {
        StringBuffer localStringBuffer = new StringBuffer();
        int i = -1;
        BitSet localBitSet = paramBitSet.get(0, paramArrayOfString.length);
        while (true)
        {
            i = localBitSet.nextSetBit(i + 1);
            if (i == -1)
                break;
            localStringBuffer.append(paramArrayOfString[i].replace('_', '-')).append(' ');
        }
        if (localBitSet.cardinality() > 0)
            localStringBuffer.setLength(-1 + localStringBuffer.length());
        return localStringBuffer.toString();
    }

    private void markAllNetworksDisabled()
    {
        markAllNetworksDisabledExcept(-1);
    }

    private void markAllNetworksDisabledExcept(int paramInt)
    {
        Iterator localIterator = this.mConfiguredNetworks.values().iterator();
        while (localIterator.hasNext())
        {
            WifiConfiguration localWifiConfiguration = (WifiConfiguration)localIterator.next();
            if ((localWifiConfiguration != null) && (localWifiConfiguration.networkId != paramInt) && (localWifiConfiguration.status != 1))
            {
                localWifiConfiguration.status = 1;
                localWifiConfiguration.disableReason = 0;
            }
        }
    }

    private void migrateOldEapTlsIfNecessary(WifiConfiguration paramWifiConfiguration, int paramInt)
    {
        String str1 = this.mWifiNative.getNetworkVariable(paramInt, "private_key");
        if (TextUtils.isEmpty(str1));
        while (true)
        {
            return;
            String str2 = removeDoubleQuotes(str1);
            if (!TextUtils.isEmpty(str2))
            {
                paramWifiConfiguration.engine.setValue("1");
                paramWifiConfiguration.engine_id.setValue(convertToQuotedString("keystore"));
                if (str2.startsWith("keystore://"));
                for (String str3 = new String(str2.substring("keystore://".length())); ; str3 = str2)
                {
                    paramWifiConfiguration.key_id.setValue(convertToQuotedString(str3));
                    WifiConfiguration.EnterpriseField[] arrayOfEnterpriseField = new WifiConfiguration.EnterpriseField[3];
                    arrayOfEnterpriseField[0] = paramWifiConfiguration.engine;
                    arrayOfEnterpriseField[1] = paramWifiConfiguration.engine_id;
                    arrayOfEnterpriseField[2] = paramWifiConfiguration.key_id;
                    int i = arrayOfEnterpriseField.length;
                    for (int j = 0; j < i; j++)
                    {
                        WifiConfiguration.EnterpriseField localEnterpriseField = arrayOfEnterpriseField[j];
                        this.mWifiNative.setNetworkVariable(paramInt, localEnterpriseField.varName(), localEnterpriseField.value());
                    }
                }
                this.mWifiNative.setNetworkVariable(paramInt, "private_key", convertToQuotedString(""));
                saveConfig();
            }
        }
    }

    // ERROR //
    private void readIpAndProxyConfigurations()
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_1
        //     2: new 509	java/io/DataInputStream
        //     5: dup
        //     6: new 511	java/io/BufferedInputStream
        //     9: dup
        //     10: new 513	java/io/FileInputStream
        //     13: dup
        //     14: getstatic 88	android/net/wifi/WifiConfigStore:ipConfigFile	Ljava/lang/String;
        //     17: invokespecial 514	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
        //     20: invokespecial 517	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
        //     23: invokespecial 518	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
        //     26: astore_2
        //     27: aload_2
        //     28: invokevirtual 521	java/io/DataInputStream:readInt	()I
        //     31: istore 8
        //     33: iload 8
        //     35: iconst_2
        //     36: if_icmpeq +82 -> 118
        //     39: iload 8
        //     41: iconst_1
        //     42: if_icmpeq +76 -> 118
        //     45: aload_0
        //     46: ldc_w 523
        //     49: invokespecial 199	android/net/wifi/WifiConfigStore:loge	(Ljava/lang/String;)V
        //     52: aload_2
        //     53: ifnull +7 -> 60
        //     56: aload_2
        //     57: invokevirtual 526	java/io/DataInputStream:close	()V
        //     60: return
        //     61: aload 16
        //     63: ldc 19
        //     65: invokevirtual 299	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     68: istore 18
        //     70: iload 18
        //     72: ifeq +471 -> 543
        //     75: iload 9
        //     77: bipush 255
        //     79: if_icmpeq +39 -> 118
        //     82: aload_0
        //     83: getfield 95	android/net/wifi/WifiConfigStore:mConfiguredNetworks	Ljava/util/HashMap;
        //     86: aload_0
        //     87: getfield 97	android/net/wifi/WifiConfigStore:mNetworkIds	Ljava/util/HashMap;
        //     90: iload 9
        //     92: invokestatic 174	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     95: invokevirtual 178	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     98: invokevirtual 178	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     101: checkcast 113	android/net/wifi/WifiConfiguration
        //     104: astore 19
        //     106: aload 19
        //     108: ifnonnull +469 -> 577
        //     111: aload_0
        //     112: ldc_w 528
        //     115: invokespecial 199	android/net/wifi/WifiConfigStore:loge	(Ljava/lang/String;)V
        //     118: bipush 255
        //     120: istore 9
        //     122: getstatic 534	android/net/wifi/WifiConfiguration$IpAssignment:UNASSIGNED	Landroid/net/wifi/WifiConfiguration$IpAssignment;
        //     125: astore 10
        //     127: getstatic 539	android/net/wifi/WifiConfiguration$ProxySettings:UNASSIGNED	Landroid/net/wifi/WifiConfiguration$ProxySettings;
        //     130: astore 11
        //     132: new 119	android/net/LinkProperties
        //     135: dup
        //     136: invokespecial 540	android/net/LinkProperties:<init>	()V
        //     139: astore 12
        //     141: aconst_null
        //     142: astore 13
        //     144: bipush 255
        //     146: istore 14
        //     148: aconst_null
        //     149: astore 15
        //     151: aload_2
        //     152: invokevirtual 543	java/io/DataInputStream:readUTF	()Ljava/lang/String;
        //     155: astore 16
        //     157: aload 16
        //     159: ldc 28
        //     161: invokevirtual 299	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     164: ifeq +12 -> 176
        //     167: aload_2
        //     168: invokevirtual 521	java/io/DataInputStream:readInt	()I
        //     171: istore 9
        //     173: goto -22 -> 151
        //     176: aload 16
        //     178: ldc 34
        //     180: invokevirtual 299	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     183: ifeq +15 -> 198
        //     186: aload_2
        //     187: invokevirtual 543	java/io/DataInputStream:readUTF	()Ljava/lang/String;
        //     190: invokestatic 546	android/net/wifi/WifiConfiguration$IpAssignment:valueOf	(Ljava/lang/String;)Landroid/net/wifi/WifiConfiguration$IpAssignment;
        //     193: astore 10
        //     195: goto -44 -> 151
        //     198: aload 16
        //     200: ldc 37
        //     202: invokevirtual 299	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     205: ifeq +79 -> 284
        //     208: aload 12
        //     210: new 141	android/net/LinkAddress
        //     213: dup
        //     214: aload_2
        //     215: invokevirtual 543	java/io/DataInputStream:readUTF	()Ljava/lang/String;
        //     218: invokestatic 552	android/net/NetworkUtils:numericToInetAddress	(Ljava/lang/String;)Ljava/net/InetAddress;
        //     221: aload_2
        //     222: invokevirtual 521	java/io/DataInputStream:readInt	()I
        //     225: invokespecial 555	android/net/LinkAddress:<init>	(Ljava/net/InetAddress;I)V
        //     228: invokevirtual 145	android/net/LinkProperties:addLinkAddress	(Landroid/net/LinkAddress;)V
        //     231: goto -80 -> 151
        //     234: astore 17
        //     236: aload_0
        //     237: new 64	java/lang/StringBuilder
        //     240: dup
        //     241: invokespecial 67	java/lang/StringBuilder:<init>	()V
        //     244: ldc_w 557
        //     247: invokevirtual 82	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     250: aload 17
        //     252: invokevirtual 77	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     255: invokevirtual 86	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     258: invokespecial 199	android/net/wifi/WifiConfigStore:loge	(Ljava/lang/String;)V
        //     261: goto -110 -> 151
        //     264: astore 7
        //     266: aload_2
        //     267: astore_1
        //     268: aload_1
        //     269: ifnull -209 -> 60
        //     272: aload_1
        //     273: invokevirtual 526	java/io/DataInputStream:close	()V
        //     276: goto -216 -> 60
        //     279: astore 6
        //     281: goto -221 -> 60
        //     284: aload 16
        //     286: ldc 25
        //     288: invokevirtual 299	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     291: ifeq +135 -> 426
        //     294: aconst_null
        //     295: astore 21
        //     297: aconst_null
        //     298: astore 22
        //     300: iload 8
        //     302: iconst_1
        //     303: if_icmpne +75 -> 378
        //     306: aload_2
        //     307: invokevirtual 543	java/io/DataInputStream:readUTF	()Ljava/lang/String;
        //     310: invokestatic 552	android/net/NetworkUtils:numericToInetAddress	(Ljava/lang/String;)Ljava/net/InetAddress;
        //     313: astore 22
        //     315: new 150	android/net/RouteInfo
        //     318: dup
        //     319: aload 21
        //     321: aload 22
        //     323: invokespecial 560	android/net/RouteInfo:<init>	(Landroid/net/LinkAddress;Ljava/net/InetAddress;)V
        //     326: astore 23
        //     328: aload 12
        //     330: aload 23
        //     332: invokevirtual 154	android/net/LinkProperties:addRoute	(Landroid/net/RouteInfo;)V
        //     335: goto -184 -> 151
        //     338: astore 5
        //     340: aload_2
        //     341: astore_1
        //     342: aload_0
        //     343: new 64	java/lang/StringBuilder
        //     346: dup
        //     347: invokespecial 67	java/lang/StringBuilder:<init>	()V
        //     350: ldc_w 562
        //     353: invokevirtual 82	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     356: aload 5
        //     358: invokevirtual 77	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     361: invokevirtual 86	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     364: invokespecial 199	android/net/wifi/WifiConfigStore:loge	(Ljava/lang/String;)V
        //     367: aload_1
        //     368: ifnull -308 -> 60
        //     371: aload_1
        //     372: invokevirtual 526	java/io/DataInputStream:close	()V
        //     375: goto -315 -> 60
        //     378: aload_2
        //     379: invokevirtual 521	java/io/DataInputStream:readInt	()I
        //     382: iconst_1
        //     383: if_icmpne +23 -> 406
        //     386: new 141	android/net/LinkAddress
        //     389: dup
        //     390: aload_2
        //     391: invokevirtual 543	java/io/DataInputStream:readUTF	()Ljava/lang/String;
        //     394: invokestatic 552	android/net/NetworkUtils:numericToInetAddress	(Ljava/lang/String;)Ljava/net/InetAddress;
        //     397: aload_2
        //     398: invokevirtual 521	java/io/DataInputStream:readInt	()I
        //     401: invokespecial 555	android/net/LinkAddress:<init>	(Ljava/net/InetAddress;I)V
        //     404: astore 21
        //     406: aload_2
        //     407: invokevirtual 521	java/io/DataInputStream:readInt	()I
        //     410: iconst_1
        //     411: if_icmpne -96 -> 315
        //     414: aload_2
        //     415: invokevirtual 543	java/io/DataInputStream:readUTF	()Ljava/lang/String;
        //     418: invokestatic 552	android/net/NetworkUtils:numericToInetAddress	(Ljava/lang/String;)Ljava/net/InetAddress;
        //     421: astore 22
        //     423: goto -108 -> 315
        //     426: aload 16
        //     428: ldc 16
        //     430: invokevirtual 299	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     433: ifeq +31 -> 464
        //     436: aload 12
        //     438: aload_2
        //     439: invokevirtual 543	java/io/DataInputStream:readUTF	()Ljava/lang/String;
        //     442: invokestatic 552	android/net/NetworkUtils:numericToInetAddress	(Ljava/lang/String;)Ljava/net/InetAddress;
        //     445: invokevirtual 163	android/net/LinkProperties:addDns	(Ljava/net/InetAddress;)V
        //     448: goto -297 -> 151
        //     451: astore_3
        //     452: aload_2
        //     453: astore_1
        //     454: aload_1
        //     455: ifnull +7 -> 462
        //     458: aload_1
        //     459: invokevirtual 526	java/io/DataInputStream:close	()V
        //     462: aload_3
        //     463: athrow
        //     464: aload 16
        //     466: ldc 46
        //     468: invokevirtual 299	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     471: ifeq +15 -> 486
        //     474: aload_2
        //     475: invokevirtual 543	java/io/DataInputStream:readUTF	()Ljava/lang/String;
        //     478: invokestatic 565	android/net/wifi/WifiConfiguration$ProxySettings:valueOf	(Ljava/lang/String;)Landroid/net/wifi/WifiConfiguration$ProxySettings;
        //     481: astore 11
        //     483: goto -332 -> 151
        //     486: aload 16
        //     488: ldc 40
        //     490: invokevirtual 299	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     493: ifeq +12 -> 505
        //     496: aload_2
        //     497: invokevirtual 543	java/io/DataInputStream:readUTF	()Ljava/lang/String;
        //     500: astore 13
        //     502: goto -351 -> 151
        //     505: aload 16
        //     507: ldc 43
        //     509: invokevirtual 299	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     512: ifeq +12 -> 524
        //     515: aload_2
        //     516: invokevirtual 521	java/io/DataInputStream:readInt	()I
        //     519: istore 14
        //     521: goto -370 -> 151
        //     524: aload 16
        //     526: ldc 22
        //     528: invokevirtual 299	java/lang/String:equals	(Ljava/lang/Object;)Z
        //     531: ifeq -470 -> 61
        //     534: aload_2
        //     535: invokevirtual 543	java/io/DataInputStream:readUTF	()Ljava/lang/String;
        //     538: astore 15
        //     540: goto -389 -> 151
        //     543: aload_0
        //     544: new 64	java/lang/StringBuilder
        //     547: dup
        //     548: invokespecial 67	java/lang/StringBuilder:<init>	()V
        //     551: ldc_w 567
        //     554: invokevirtual 82	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     557: aload 16
        //     559: invokevirtual 82	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     562: ldc_w 569
        //     565: invokevirtual 82	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     568: invokevirtual 86	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     571: invokespecial 199	android/net/wifi/WifiConfigStore:loge	(Ljava/lang/String;)V
        //     574: goto -423 -> 151
        //     577: aload 19
        //     579: aload 12
        //     581: putfield 117	android/net/wifi/WifiConfiguration:linkProperties	Landroid/net/LinkProperties;
        //     584: getstatic 573	android/net/wifi/WifiConfigStore$1:$SwitchMap$android$net$wifi$WifiConfiguration$IpAssignment	[I
        //     587: aload 10
        //     589: invokevirtual 576	android/net/wifi/WifiConfiguration$IpAssignment:ordinal	()I
        //     592: iaload
        //     593: tableswitch	default:+27 -> 620, 1:+81->674, 2:+81->674, 3:+34->627
        //     621: ldc_w 578
        //     624: invokespecial 199	android/net/wifi/WifiConfigStore:loge	(Ljava/lang/String;)V
        //     627: getstatic 581	android/net/wifi/WifiConfigStore$1:$SwitchMap$android$net$wifi$WifiConfiguration$ProxySettings	[I
        //     630: aload 11
        //     632: invokevirtual 582	android/net/wifi/WifiConfiguration$ProxySettings:ordinal	()I
        //     635: iaload
        //     636: tableswitch	default:+28 -> 664, 1:+48->684, 2:+80->716, 3:+-518->118
        //     665: ldc_w 584
        //     668: invokespecial 199	android/net/wifi/WifiConfigStore:loge	(Ljava/lang/String;)V
        //     671: goto -553 -> 118
        //     674: aload 19
        //     676: aload 10
        //     678: putfield 586	android/net/wifi/WifiConfiguration:ipAssignment	Landroid/net/wifi/WifiConfiguration$IpAssignment;
        //     681: goto -54 -> 627
        //     684: aload 19
        //     686: aload 11
        //     688: putfield 588	android/net/wifi/WifiConfiguration:proxySettings	Landroid/net/wifi/WifiConfiguration$ProxySettings;
        //     691: new 590	android/net/ProxyProperties
        //     694: dup
        //     695: aload 13
        //     697: iload 14
        //     699: aload 15
        //     701: invokespecial 593	android/net/ProxyProperties:<init>	(Ljava/lang/String;ILjava/lang/String;)V
        //     704: astore 20
        //     706: aload 12
        //     708: aload 20
        //     710: invokevirtual 597	android/net/LinkProperties:setHttpProxy	(Landroid/net/ProxyProperties;)V
        //     713: goto -595 -> 118
        //     716: aload 19
        //     718: aload 11
        //     720: putfield 588	android/net/wifi/WifiConfiguration:proxySettings	Landroid/net/wifi/WifiConfiguration$ProxySettings;
        //     723: goto -605 -> 118
        //     726: astore 4
        //     728: goto -266 -> 462
        //     731: astore 24
        //     733: goto -673 -> 60
        //     736: astore_3
        //     737: goto -283 -> 454
        //     740: astore 5
        //     742: goto -400 -> 342
        //     745: astore 25
        //     747: goto -479 -> 268
        //
        // Exception table:
        //     from	to	target	type
        //     61	70	234	java/lang/IllegalArgumentException
        //     157	231	234	java/lang/IllegalArgumentException
        //     284	335	234	java/lang/IllegalArgumentException
        //     378	448	234	java/lang/IllegalArgumentException
        //     464	574	234	java/lang/IllegalArgumentException
        //     27	52	264	java/io/EOFException
        //     61	70	264	java/io/EOFException
        //     82	157	264	java/io/EOFException
        //     157	231	264	java/io/EOFException
        //     236	261	264	java/io/EOFException
        //     284	335	264	java/io/EOFException
        //     378	448	264	java/io/EOFException
        //     464	574	264	java/io/EOFException
        //     577	723	264	java/io/EOFException
        //     272	276	279	java/lang/Exception
        //     371	375	279	java/lang/Exception
        //     27	52	338	java/io/IOException
        //     61	70	338	java/io/IOException
        //     82	157	338	java/io/IOException
        //     157	231	338	java/io/IOException
        //     236	261	338	java/io/IOException
        //     284	335	338	java/io/IOException
        //     378	448	338	java/io/IOException
        //     464	574	338	java/io/IOException
        //     577	723	338	java/io/IOException
        //     27	52	451	finally
        //     61	70	451	finally
        //     82	157	451	finally
        //     157	231	451	finally
        //     236	261	451	finally
        //     284	335	451	finally
        //     378	448	451	finally
        //     464	574	451	finally
        //     577	723	451	finally
        //     458	462	726	java/lang/Exception
        //     56	60	731	java/lang/Exception
        //     2	27	736	finally
        //     342	367	736	finally
        //     2	27	740	java/io/IOException
        //     2	27	745	java/io/EOFException
    }

    private void readNetworkVariables(WifiConfiguration paramWifiConfiguration)
    {
        int i = paramWifiConfiguration.networkId;
        if (i < 0);
        while (true)
        {
            return;
            String str1 = this.mWifiNative.getNetworkVariable(i, "ssid");
            label59: String str3;
            if (!TextUtils.isEmpty(str1))
            {
                paramWifiConfiguration.SSID = str1;
                String str2 = this.mWifiNative.getNetworkVariable(i, "bssid");
                if (TextUtils.isEmpty(str2))
                    break label230;
                paramWifiConfiguration.BSSID = str2;
                str3 = this.mWifiNative.getNetworkVariable(i, "priority");
                paramWifiConfiguration.priority = -1;
                if (TextUtils.isEmpty(str3));
            }
            try
            {
                paramWifiConfiguration.priority = Integer.parseInt(str3);
                label95: String str4 = this.mWifiNative.getNetworkVariable(i, "scan_ssid");
                paramWifiConfiguration.hiddenSSID = false;
                if (!TextUtils.isEmpty(str4));
                try
                {
                    boolean bool;
                    label132: label138: String str5;
                    if (Integer.parseInt(str4) != 0)
                    {
                        bool = true;
                        paramWifiConfiguration.hiddenSSID = bool;
                        str5 = this.mWifiNative.getNetworkVariable(i, "wep_tx_keyidx");
                        paramWifiConfiguration.wepTxKeyIndex = -1;
                        if (TextUtils.isEmpty(str5));
                    }
                    try
                    {
                        paramWifiConfiguration.wepTxKeyIndex = Integer.parseInt(str5);
                        label174: int j = 0;
                        label177: if (j < 4)
                        {
                            String str13 = this.mWifiNative.getNetworkVariable(i, WifiConfiguration.wepKeyVarNames[j]);
                            if (!TextUtils.isEmpty(str13))
                                paramWifiConfiguration.wepKeys[j] = str13;
                            while (true)
                            {
                                j++;
                                break label177;
                                paramWifiConfiguration.SSID = null;
                                break;
                                label230: paramWifiConfiguration.BSSID = null;
                                break label59;
                                bool = false;
                                break label132;
                                paramWifiConfiguration.wepKeys[j] = null;
                            }
                        }
                        String str6 = this.mWifiNative.getNetworkVariable(i, "psk");
                        if (!TextUtils.isEmpty(str6));
                        for (paramWifiConfiguration.preSharedKey = str6; ; paramWifiConfiguration.preSharedKey = null)
                        {
                            String str7 = this.mWifiNative.getNetworkVariable(paramWifiConfiguration.networkId, "proto");
                            if (TextUtils.isEmpty(str7))
                                break;
                            String[] arrayOfString5 = str7.split(" ");
                            int i12 = arrayOfString5.length;
                            for (int i13 = 0; i13 < i12; i13++)
                            {
                                int i14 = lookupString(arrayOfString5[i13], WifiConfiguration.Protocol.strings);
                                if (i14 >= 0)
                                    paramWifiConfiguration.allowedProtocols.set(i14);
                            }
                        }
                        String str8 = this.mWifiNative.getNetworkVariable(paramWifiConfiguration.networkId, "key_mgmt");
                        if (!TextUtils.isEmpty(str8))
                        {
                            String[] arrayOfString4 = str8.split(" ");
                            int i9 = arrayOfString4.length;
                            for (int i10 = 0; i10 < i9; i10++)
                            {
                                int i11 = lookupString(arrayOfString4[i10], WifiConfiguration.KeyMgmt.strings);
                                if (i11 >= 0)
                                    paramWifiConfiguration.allowedKeyManagement.set(i11);
                            }
                        }
                        String str9 = this.mWifiNative.getNetworkVariable(paramWifiConfiguration.networkId, "auth_alg");
                        if (!TextUtils.isEmpty(str9))
                        {
                            String[] arrayOfString3 = str9.split(" ");
                            int i6 = arrayOfString3.length;
                            for (int i7 = 0; i7 < i6; i7++)
                            {
                                int i8 = lookupString(arrayOfString3[i7], WifiConfiguration.AuthAlgorithm.strings);
                                if (i8 >= 0)
                                    paramWifiConfiguration.allowedAuthAlgorithms.set(i8);
                            }
                        }
                        String str10 = this.mWifiNative.getNetworkVariable(paramWifiConfiguration.networkId, "pairwise");
                        if (!TextUtils.isEmpty(str10))
                        {
                            String[] arrayOfString2 = str10.split(" ");
                            int i3 = arrayOfString2.length;
                            for (int i4 = 0; i4 < i3; i4++)
                            {
                                int i5 = lookupString(arrayOfString2[i4], WifiConfiguration.PairwiseCipher.strings);
                                if (i5 >= 0)
                                    paramWifiConfiguration.allowedPairwiseCiphers.set(i5);
                            }
                        }
                        String str11 = this.mWifiNative.getNetworkVariable(paramWifiConfiguration.networkId, "group");
                        if (!TextUtils.isEmpty(str11))
                        {
                            String[] arrayOfString1 = str11.split(" ");
                            int n = arrayOfString1.length;
                            for (int i1 = 0; i1 < n; i1++)
                            {
                                int i2 = lookupString(arrayOfString1[i1], WifiConfiguration.GroupCipher.strings);
                                if (i2 >= 0)
                                    paramWifiConfiguration.allowedGroupCiphers.set(i2);
                            }
                        }
                        for (WifiConfiguration.EnterpriseField localEnterpriseField : paramWifiConfiguration.enterpriseFields)
                        {
                            String str12 = this.mWifiNative.getNetworkVariable(i, localEnterpriseField.varName());
                            if (!TextUtils.isEmpty(str12))
                            {
                                if ((localEnterpriseField != paramWifiConfiguration.eap) && (localEnterpriseField != paramWifiConfiguration.engine))
                                    str12 = removeDoubleQuotes(str12);
                                localEnterpriseField.setValue(str12);
                            }
                        }
                        migrateOldEapTlsIfNecessary(paramWifiConfiguration, i);
                    }
                    catch (NumberFormatException localNumberFormatException1)
                    {
                        break label174;
                    }
                }
                catch (NumberFormatException localNumberFormatException2)
                {
                    break label138;
                }
            }
            catch (NumberFormatException localNumberFormatException3)
            {
                break label95;
            }
        }
    }

    private String removeDoubleQuotes(String paramString)
    {
        if (paramString.length() <= 2);
        for (String str = ""; ; str = paramString.substring(1, -1 + paramString.length()))
            return str;
    }

    private void sendConfiguredNetworksChangedBroadcast()
    {
        Intent localIntent = new Intent("android.net.wifi.CONFIGURED_NETWORKS_CHANGE");
        localIntent.addFlags(134217728);
        localIntent.putExtra("multipleChanges", true);
        this.mContext.sendBroadcast(localIntent);
    }

    private void sendConfiguredNetworksChangedBroadcast(WifiConfiguration paramWifiConfiguration, int paramInt)
    {
        Intent localIntent = new Intent("android.net.wifi.CONFIGURED_NETWORKS_CHANGE");
        localIntent.addFlags(134217728);
        localIntent.putExtra("multipleChanges", false);
        localIntent.putExtra("wifiConfiguration", paramWifiConfiguration);
        localIntent.putExtra("changeReason", paramInt);
        this.mContext.sendBroadcast(localIntent);
    }

    private void writeIpAndProxyConfigurations()
    {
        ArrayList localArrayList = new ArrayList();
        Iterator localIterator = this.mConfiguredNetworks.values().iterator();
        while (localIterator.hasNext())
            localArrayList.add(new WifiConfiguration((WifiConfiguration)localIterator.next()));
        DelayedDiskWrite.write(localArrayList);
    }

    private NetworkUpdateResult writeIpAndProxyConfigurationsOnChange(WifiConfiguration paramWifiConfiguration1, WifiConfiguration paramWifiConfiguration2)
    {
        boolean bool1 = false;
        boolean bool2 = false;
        LinkProperties localLinkProperties = new LinkProperties();
        switch (1.$SwitchMap$android$net$wifi$WifiConfiguration$IpAssignment[paramWifiConfiguration2.ipAssignment.ordinal()])
        {
        default:
            loge("Ignore invalid ip assignment during write");
        case 3:
            switch (1.$SwitchMap$android$net$wifi$WifiConfiguration$ProxySettings[paramWifiConfiguration2.proxySettings.ordinal()])
            {
            default:
                loge("Ignore invalid proxy configuration during write");
            case 3:
                label103: if (!bool1)
                {
                    addIpSettingsFromConfig(localLinkProperties, paramWifiConfiguration1);
                    label114: if (bool2)
                        break label524;
                    localLinkProperties.setHttpProxy(paramWifiConfiguration1.linkProperties.getHttpProxy());
                }
                break;
            case 1:
            case 2:
            }
            break;
        case 1:
        case 2:
        }
        while (true)
        {
            if ((bool1) || (bool2))
            {
                paramWifiConfiguration1.linkProperties = localLinkProperties;
                writeIpAndProxyConfigurations();
                sendConfiguredNetworksChangedBroadcast(paramWifiConfiguration1, 2);
            }
            NetworkUpdateResult localNetworkUpdateResult = new NetworkUpdateResult(bool1, bool2);
            return localNetworkUpdateResult;
            Collection localCollection1 = paramWifiConfiguration1.linkProperties.getLinkAddresses();
            Collection localCollection2 = paramWifiConfiguration2.linkProperties.getLinkAddresses();
            Collection localCollection3 = paramWifiConfiguration1.linkProperties.getDnses();
            Collection localCollection4 = paramWifiConfiguration2.linkProperties.getDnses();
            Collection localCollection5 = paramWifiConfiguration1.linkProperties.getRoutes();
            Collection localCollection6 = paramWifiConfiguration2.linkProperties.getRoutes();
            int i;
            label257: int j;
            if ((localCollection1.size() != localCollection2.size()) || (!localCollection1.containsAll(localCollection2)))
            {
                i = 1;
                if ((localCollection3.size() == localCollection4.size()) && (localCollection3.containsAll(localCollection4)))
                    break label358;
                j = 1;
                label289: if ((localCollection5.size() == localCollection6.size()) && (localCollection5.containsAll(localCollection6)))
                    break label364;
            }
            label358: label364: for (int k = 1; (paramWifiConfiguration1.ipAssignment != paramWifiConfiguration2.ipAssignment) || (i != 0) || (j != 0) || (k != 0); k = 0)
            {
                bool1 = true;
                break;
                i = 0;
                break label257;
                j = 0;
                break label289;
            }
            if (paramWifiConfiguration1.ipAssignment == paramWifiConfiguration2.ipAssignment)
                break;
            bool1 = true;
            break;
            ProxyProperties localProxyProperties1 = paramWifiConfiguration2.linkProperties.getHttpProxy();
            ProxyProperties localProxyProperties2 = paramWifiConfiguration1.linkProperties.getHttpProxy();
            if (localProxyProperties1 != null)
            {
                if (!localProxyProperties1.equals(localProxyProperties2));
                for (bool2 = true; ; bool2 = false)
                    break;
            }
            if (localProxyProperties2 != null);
            for (bool2 = true; ; bool2 = false)
                break;
            if (paramWifiConfiguration1.proxySettings == paramWifiConfiguration2.proxySettings)
                break label103;
            bool2 = true;
            break label103;
            paramWifiConfiguration1.ipAssignment = paramWifiConfiguration2.ipAssignment;
            addIpSettingsFromConfig(localLinkProperties, paramWifiConfiguration2);
            log("IP config changed SSID = " + paramWifiConfiguration1.SSID + " linkProperties: " + localLinkProperties.toString());
            break label114;
            label524: paramWifiConfiguration1.proxySettings = paramWifiConfiguration2.proxySettings;
            localLinkProperties.setHttpProxy(paramWifiConfiguration2.linkProperties.getHttpProxy());
            log("proxy changed SSID = " + paramWifiConfiguration1.SSID);
            if (localLinkProperties.getHttpProxy() != null)
                log(" proxyProperties: " + localLinkProperties.getHttpProxy().toString());
        }
    }

    int addOrUpdateNetwork(WifiConfiguration paramWifiConfiguration)
    {
        NetworkUpdateResult localNetworkUpdateResult = addOrUpdateNetworkNative(paramWifiConfiguration);
        WifiConfiguration localWifiConfiguration;
        if (localNetworkUpdateResult.getNetworkId() != -1)
        {
            localWifiConfiguration = (WifiConfiguration)this.mConfiguredNetworks.get(Integer.valueOf(localNetworkUpdateResult.getNetworkId()));
            if (!localNetworkUpdateResult.isNewNetwork)
                break label55;
        }
        label55: for (int i = 0; ; i = 2)
        {
            sendConfiguredNetworksChangedBroadcast(localWifiConfiguration, i);
            return localNetworkUpdateResult.getNetworkId();
        }
    }

    void clearIpConfiguration(int paramInt)
    {
        WifiConfiguration localWifiConfiguration = (WifiConfiguration)this.mConfiguredNetworks.get(Integer.valueOf(paramInt));
        if ((localWifiConfiguration != null) && (localWifiConfiguration.linkProperties != null))
        {
            ProxyProperties localProxyProperties = localWifiConfiguration.linkProperties.getHttpProxy();
            localWifiConfiguration.linkProperties.clear();
            localWifiConfiguration.linkProperties.setHttpProxy(localProxyProperties);
        }
    }

    boolean disableNetwork(int paramInt)
    {
        return disableNetwork(paramInt, 0);
    }

    boolean disableNetwork(int paramInt1, int paramInt2)
    {
        boolean bool = this.mWifiNative.disableNetwork(paramInt1);
        Object localObject = null;
        WifiConfiguration localWifiConfiguration = (WifiConfiguration)this.mConfiguredNetworks.get(Integer.valueOf(paramInt1));
        if ((localWifiConfiguration != null) && (localWifiConfiguration.status != 1))
        {
            localWifiConfiguration.status = 1;
            localWifiConfiguration.disableReason = paramInt2;
            localObject = localWifiConfiguration;
        }
        if (localObject != null)
            sendConfiguredNetworksChangedBroadcast(localObject, 2);
        return bool;
    }

    String dump()
    {
        StringBuffer localStringBuffer = new StringBuffer();
        String str = System.getProperty("line.separator");
        localStringBuffer.append("mLastPriority ").append(this.mLastPriority).append(str);
        localStringBuffer.append("Configured networks ").append(str);
        Iterator localIterator = getConfiguredNetworks().iterator();
        while (localIterator.hasNext())
            localStringBuffer.append((WifiConfiguration)localIterator.next()).append(str);
        return localStringBuffer.toString();
    }

    void enableAllNetworks()
    {
        int i = 0;
        Iterator localIterator = this.mConfiguredNetworks.values().iterator();
        while (localIterator.hasNext())
        {
            WifiConfiguration localWifiConfiguration = (WifiConfiguration)localIterator.next();
            if ((localWifiConfiguration != null) && (localWifiConfiguration.status == 1))
                if (this.mWifiNative.enableNetwork(localWifiConfiguration.networkId, false))
                {
                    i = 1;
                    localWifiConfiguration.status = 2;
                }
                else
                {
                    loge("Enable network failed on " + localWifiConfiguration.networkId);
                }
        }
        if (i != 0)
        {
            this.mWifiNative.saveConfig();
            sendConfiguredNetworksChangedBroadcast();
        }
    }

    boolean enableNetwork(int paramInt, boolean paramBoolean)
    {
        boolean bool = enableNetworkWithoutBroadcast(paramInt, paramBoolean);
        if (paramBoolean)
            sendConfiguredNetworksChangedBroadcast();
        while (true)
        {
            return bool;
            synchronized (this.mConfiguredNetworks)
            {
                WifiConfiguration localWifiConfiguration = (WifiConfiguration)this.mConfiguredNetworks.get(Integer.valueOf(paramInt));
                if (localWifiConfiguration == null)
                    continue;
                sendConfiguredNetworksChangedBroadcast(localWifiConfiguration, 2);
            }
        }
    }

    boolean enableNetworkWithoutBroadcast(int paramInt, boolean paramBoolean)
    {
        boolean bool = this.mWifiNative.enableNetwork(paramInt, paramBoolean);
        WifiConfiguration localWifiConfiguration = (WifiConfiguration)this.mConfiguredNetworks.get(Integer.valueOf(paramInt));
        if (localWifiConfiguration != null)
            localWifiConfiguration.status = 2;
        if (paramBoolean)
            markAllNetworksDisabledExcept(paramInt);
        return bool;
    }

    boolean forgetNetwork(int paramInt)
    {
        int i = 1;
        if (this.mWifiNative.removeNetwork(paramInt))
        {
            this.mWifiNative.saveConfig();
            WifiConfiguration localWifiConfiguration1 = null;
            WifiConfiguration localWifiConfiguration2 = (WifiConfiguration)this.mConfiguredNetworks.get(Integer.valueOf(paramInt));
            if (localWifiConfiguration2 != null)
            {
                localWifiConfiguration1 = (WifiConfiguration)this.mConfiguredNetworks.remove(Integer.valueOf(paramInt));
                this.mNetworkIds.remove(Integer.valueOf(configKey(localWifiConfiguration2)));
            }
            if (localWifiConfiguration1 != null)
            {
                writeIpAndProxyConfigurations();
                sendConfiguredNetworksChangedBroadcast(localWifiConfiguration1, i);
            }
        }
        while (true)
        {
            return i;
            loge("Failed to remove network " + paramInt);
            int j = 0;
        }
    }

    public String getConfigFile()
    {
        return ipConfigFile;
    }

    List<WifiConfiguration> getConfiguredNetworks()
    {
        ArrayList localArrayList = new ArrayList();
        Iterator localIterator = this.mConfiguredNetworks.values().iterator();
        while (localIterator.hasNext())
            localArrayList.add(new WifiConfiguration((WifiConfiguration)localIterator.next()));
        return localArrayList;
    }

    DhcpInfoInternal getIpConfiguration(int paramInt)
    {
        DhcpInfoInternal localDhcpInfoInternal = new DhcpInfoInternal();
        LinkProperties localLinkProperties = getLinkProperties(paramInt);
        if (localLinkProperties != null)
        {
            Iterator localIterator1 = localLinkProperties.getLinkAddresses().iterator();
            if (localIterator1.hasNext())
            {
                LinkAddress localLinkAddress = (LinkAddress)localIterator1.next();
                localDhcpInfoInternal.ipAddress = localLinkAddress.getAddress().getHostAddress();
                Iterator localIterator2 = localLinkProperties.getRoutes().iterator();
                while (localIterator2.hasNext())
                    localDhcpInfoInternal.addRoute((RouteInfo)localIterator2.next());
                localDhcpInfoInternal.prefixLength = localLinkAddress.getNetworkPrefixLength();
                Iterator localIterator3 = localLinkProperties.getDnses().iterator();
                localDhcpInfoInternal.dns1 = ((InetAddress)localIterator3.next()).getHostAddress();
                if (localIterator3.hasNext())
                    localDhcpInfoInternal.dns2 = ((InetAddress)localIterator3.next()).getHostAddress();
            }
        }
        return localDhcpInfoInternal;
    }

    LinkProperties getLinkProperties(int paramInt)
    {
        WifiConfiguration localWifiConfiguration = (WifiConfiguration)this.mConfiguredNetworks.get(Integer.valueOf(paramInt));
        if (localWifiConfiguration != null);
        for (LinkProperties localLinkProperties = new LinkProperties(localWifiConfiguration.linkProperties); ; localLinkProperties = null)
            return localLinkProperties;
    }

    ProxyProperties getProxyProperties(int paramInt)
    {
        LinkProperties localLinkProperties = getLinkProperties(paramInt);
        if (localLinkProperties != null);
        for (ProxyProperties localProxyProperties = new ProxyProperties(localLinkProperties.getHttpProxy()); ; localProxyProperties = null)
            return localProxyProperties;
    }

    void initialize()
    {
        loadConfiguredNetworks();
        enableAllNetworks();
    }

    boolean isUsingStaticIp(int paramInt)
    {
        WifiConfiguration localWifiConfiguration = (WifiConfiguration)this.mConfiguredNetworks.get(Integer.valueOf(paramInt));
        if ((localWifiConfiguration != null) && (localWifiConfiguration.ipAssignment == WifiConfiguration.IpAssignment.STATIC));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    void loadConfiguredNetworks()
    {
        String str = this.mWifiNative.listNetworks();
        this.mLastPriority = 0;
        this.mConfiguredNetworks.clear();
        this.mNetworkIds.clear();
        if (str == null);
        while (true)
        {
            return;
            String[] arrayOfString1 = str.split("\n");
            int i = 1;
            while (true)
                if (i < arrayOfString1.length)
                {
                    String[] arrayOfString2 = arrayOfString1[i].split("\t");
                    WifiConfiguration localWifiConfiguration = new WifiConfiguration();
                    try
                    {
                        localWifiConfiguration.networkId = Integer.parseInt(arrayOfString2[0]);
                        if (arrayOfString2.length > 3)
                            if (arrayOfString2[3].indexOf("[CURRENT]") != -1)
                            {
                                localWifiConfiguration.status = 0;
                                readNetworkVariables(localWifiConfiguration);
                                if (localWifiConfiguration.priority > this.mLastPriority)
                                    this.mLastPriority = localWifiConfiguration.priority;
                                this.mConfiguredNetworks.put(Integer.valueOf(localWifiConfiguration.networkId), localWifiConfiguration);
                                this.mNetworkIds.put(Integer.valueOf(configKey(localWifiConfiguration)), Integer.valueOf(localWifiConfiguration.networkId));
                                i++;
                            }
                    }
                    catch (NumberFormatException localNumberFormatException)
                    {
                        while (true)
                        {
                            continue;
                            if (arrayOfString2[3].indexOf("[DISABLED]") != -1)
                            {
                                localWifiConfiguration.status = 1;
                            }
                            else
                            {
                                localWifiConfiguration.status = 2;
                                continue;
                                localWifiConfiguration.status = 2;
                            }
                        }
                    }
                }
            readIpAndProxyConfigurations();
            sendConfiguredNetworksChangedBroadcast();
        }
    }

    boolean removeNetwork(int paramInt)
    {
        boolean bool = this.mWifiNative.removeNetwork(paramInt);
        WifiConfiguration localWifiConfiguration = null;
        if (bool)
        {
            localWifiConfiguration = (WifiConfiguration)this.mConfiguredNetworks.get(Integer.valueOf(paramInt));
            if (localWifiConfiguration != null)
            {
                localWifiConfiguration = (WifiConfiguration)this.mConfiguredNetworks.remove(Integer.valueOf(paramInt));
                this.mNetworkIds.remove(Integer.valueOf(configKey(localWifiConfiguration)));
            }
        }
        if (localWifiConfiguration != null)
            sendConfiguredNetworksChangedBroadcast(localWifiConfiguration, 1);
        return bool;
    }

    boolean saveConfig()
    {
        return this.mWifiNative.saveConfig();
    }

    NetworkUpdateResult saveNetwork(WifiConfiguration paramWifiConfiguration)
    {
        int i = 0;
        NetworkUpdateResult localNetworkUpdateResult;
        if ((paramWifiConfiguration == null) || ((paramWifiConfiguration.networkId == -1) && (paramWifiConfiguration.SSID == null)))
        {
            localNetworkUpdateResult = new NetworkUpdateResult(-1);
            return localNetworkUpdateResult;
        }
        int j;
        if (paramWifiConfiguration.networkId == -1)
        {
            j = 1;
            label46: localNetworkUpdateResult = addOrUpdateNetworkNative(paramWifiConfiguration);
            int k = localNetworkUpdateResult.getNetworkId();
            if ((j != 0) && (k != -1))
            {
                this.mWifiNative.enableNetwork(k, false);
                ((WifiConfiguration)this.mConfiguredNetworks.get(Integer.valueOf(k))).status = 2;
            }
            this.mWifiNative.saveConfig();
            if (!localNetworkUpdateResult.isNewNetwork())
                break label130;
        }
        while (true)
        {
            sendConfiguredNetworksChangedBroadcast(paramWifiConfiguration, i);
            break;
            j = 0;
            break label46;
            label130: i = 2;
        }
    }

    boolean selectNetwork(int paramInt)
    {
        boolean bool = false;
        if (paramInt == -1);
        while (true)
        {
            return bool;
            if ((this.mLastPriority == -1) || (this.mLastPriority > 1000000))
            {
                Iterator localIterator = this.mConfiguredNetworks.values().iterator();
                while (localIterator.hasNext())
                {
                    WifiConfiguration localWifiConfiguration2 = (WifiConfiguration)localIterator.next();
                    if (localWifiConfiguration2.networkId != -1)
                    {
                        localWifiConfiguration2.priority = 0;
                        addOrUpdateNetworkNative(localWifiConfiguration2);
                    }
                }
                this.mLastPriority = 0;
            }
            WifiConfiguration localWifiConfiguration1 = new WifiConfiguration();
            localWifiConfiguration1.networkId = paramInt;
            int i = 1 + this.mLastPriority;
            this.mLastPriority = i;
            localWifiConfiguration1.priority = i;
            addOrUpdateNetworkNative(localWifiConfiguration1);
            this.mWifiNative.saveConfig();
            enableNetworkWithoutBroadcast(paramInt, true);
            bool = true;
        }
    }

    void setIpConfiguration(int paramInt, DhcpInfoInternal paramDhcpInfoInternal)
    {
        LinkProperties localLinkProperties = paramDhcpInfoInternal.makeLinkProperties();
        WifiConfiguration localWifiConfiguration = (WifiConfiguration)this.mConfiguredNetworks.get(Integer.valueOf(paramInt));
        if (localWifiConfiguration != null)
        {
            if (localWifiConfiguration.linkProperties != null)
                localLinkProperties.setHttpProxy(localWifiConfiguration.linkProperties.getHttpProxy());
            localWifiConfiguration.linkProperties = localLinkProperties;
        }
    }

    WpsResult startWpsPbc(WpsInfo paramWpsInfo)
    {
        WpsResult localWpsResult = new WpsResult();
        if (this.mWifiNative.startWpsPbc(paramWpsInfo.BSSID))
            markAllNetworksDisabled();
        for (localWpsResult.status = WpsResult.Status.SUCCESS; ; localWpsResult.status = WpsResult.Status.FAILURE)
        {
            return localWpsResult;
            loge("Failed to start WPS push button configuration");
        }
    }

    WpsResult startWpsWithPinFromAccessPoint(WpsInfo paramWpsInfo)
    {
        WpsResult localWpsResult = new WpsResult();
        if (this.mWifiNative.startWpsRegistrar(paramWpsInfo.BSSID, paramWpsInfo.pin))
            markAllNetworksDisabled();
        for (localWpsResult.status = WpsResult.Status.SUCCESS; ; localWpsResult.status = WpsResult.Status.FAILURE)
        {
            return localWpsResult;
            loge("Failed to start WPS pin method configuration");
        }
    }

    WpsResult startWpsWithPinFromDevice(WpsInfo paramWpsInfo)
    {
        WpsResult localWpsResult = new WpsResult();
        localWpsResult.pin = this.mWifiNative.startWpsPinDisplay(paramWpsInfo.BSSID);
        if (!TextUtils.isEmpty(localWpsResult.pin))
            markAllNetworksDisabled();
        for (localWpsResult.status = WpsResult.Status.SUCCESS; ; localWpsResult.status = WpsResult.Status.FAILURE)
        {
            return localWpsResult;
            loge("Failed to start WPS pin method configuration");
        }
    }

    void updateStatus(int paramInt, NetworkInfo.DetailedState paramDetailedState)
    {
        WifiConfiguration localWifiConfiguration;
        if (paramInt != -1)
        {
            localWifiConfiguration = (WifiConfiguration)this.mConfiguredNetworks.get(Integer.valueOf(paramInt));
            if (localWifiConfiguration != null)
                break label26;
        }
        while (true)
        {
            return;
            label26: switch (1.$SwitchMap$android$net$NetworkInfo$DetailedState[paramDetailedState.ordinal()])
            {
            default:
                break;
            case 1:
                localWifiConfiguration.status = 0;
                break;
            case 2:
                if (localWifiConfiguration.status == 0)
                    localWifiConfiguration.status = 2;
                break;
            }
        }
    }

    private static class DelayedDiskWrite
    {
        private static final String TAG = "DelayedDiskWrite";
        private static Handler sDiskWriteHandler;
        private static HandlerThread sDiskWriteHandlerThread;
        private static int sWriteSequence = 0;

        private static void loge(String paramString)
        {
            Log.e("DelayedDiskWrite", paramString);
        }

        // ERROR //
        private static void onWriteCalled(List<WifiConfiguration> paramList)
        {
            // Byte code:
            //     0: aconst_null
            //     1: astore_1
            //     2: new 46	java/io/DataOutputStream
            //     5: dup
            //     6: new 48	java/io/BufferedOutputStream
            //     9: dup
            //     10: new 50	java/io/FileOutputStream
            //     13: dup
            //     14: invokestatic 54	android/net/wifi/WifiConfigStore:access$100	()Ljava/lang/String;
            //     17: invokespecial 56	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
            //     20: invokespecial 59	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;)V
            //     23: invokespecial 60	java/io/DataOutputStream:<init>	(Ljava/io/OutputStream;)V
            //     26: astore_2
            //     27: aload_2
            //     28: iconst_2
            //     29: invokevirtual 64	java/io/DataOutputStream:writeInt	(I)V
            //     32: aload_0
            //     33: invokeinterface 70 1 0
            //     38: astore 11
            //     40: aload 11
            //     42: invokeinterface 76 1 0
            //     47: ifeq +676 -> 723
            //     50: aload 11
            //     52: invokeinterface 80 1 0
            //     57: checkcast 82	android/net/wifi/WifiConfiguration
            //     60: astore 15
            //     62: iconst_0
            //     63: istore 16
            //     65: aload 15
            //     67: getfield 86	android/net/wifi/WifiConfiguration:linkProperties	Landroid/net/LinkProperties;
            //     70: astore 18
            //     72: getstatic 92	android/net/wifi/WifiConfigStore$1:$SwitchMap$android$net$wifi$WifiConfiguration$IpAssignment	[I
            //     75: aload 15
            //     77: getfield 96	android/net/wifi/WifiConfiguration:ipAssignment	Landroid/net/wifi/WifiConfiguration$IpAssignment;
            //     80: invokevirtual 102	android/net/wifi/WifiConfiguration$IpAssignment:ordinal	()I
            //     83: iaload
            //     84: tableswitch	default:+28 -> 112, 1:+165->249, 2:+511->595, 3:+33->117
            //     113: imul
            //     114: invokestatic 106	android/net/wifi/WifiConfigStore$DelayedDiskWrite:loge	(Ljava/lang/String;)V
            //     117: getstatic 109	android/net/wifi/WifiConfigStore$1:$SwitchMap$android$net$wifi$WifiConfiguration$ProxySettings	[I
            //     120: aload 15
            //     122: getfield 113	android/net/wifi/WifiConfiguration:proxySettings	Landroid/net/wifi/WifiConfiguration$ProxySettings;
            //     125: invokevirtual 116	android/net/wifi/WifiConfiguration$ProxySettings:ordinal	()I
            //     128: iaload
            //     129: tableswitch	default:+27 -> 156, 1:+490->619, 2:+570->699, 3:+32->161
            //     157: fneg
            //     158: invokestatic 106	android/net/wifi/WifiConfigStore$DelayedDiskWrite:loge	(Ljava/lang/String;)V
            //     161: iload 16
            //     163: ifeq +18 -> 181
            //     166: aload_2
            //     167: ldc 120
            //     169: invokevirtual 123	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
            //     172: aload_2
            //     173: aload 15
            //     175: invokestatic 127	android/net/wifi/WifiConfigStore:access$200	(Landroid/net/wifi/WifiConfiguration;)I
            //     178: invokevirtual 64	java/io/DataOutputStream:writeInt	(I)V
            //     181: aload_2
            //     182: ldc 129
            //     184: invokevirtual 123	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
            //     187: goto -147 -> 40
            //     190: astore 7
            //     192: aload_2
            //     193: astore_1
            //     194: ldc 131
            //     196: invokestatic 106	android/net/wifi/WifiConfigStore$DelayedDiskWrite:loge	(Ljava/lang/String;)V
            //     199: aload_1
            //     200: ifnull +7 -> 207
            //     203: aload_1
            //     204: invokevirtual 134	java/io/DataOutputStream:close	()V
            //     207: ldc 2
            //     209: monitorenter
            //     210: bipush 255
            //     212: getstatic 22	android/net/wifi/WifiConfigStore$DelayedDiskWrite:sWriteSequence	I
            //     215: iadd
            //     216: istore 9
            //     218: iload 9
            //     220: putstatic 22	android/net/wifi/WifiConfigStore$DelayedDiskWrite:sWriteSequence	I
            //     223: iload 9
            //     225: ifne +20 -> 245
            //     228: getstatic 136	android/net/wifi/WifiConfigStore$DelayedDiskWrite:sDiskWriteHandler	Landroid/os/Handler;
            //     231: invokevirtual 142	android/os/Handler:getLooper	()Landroid/os/Looper;
            //     234: invokevirtual 147	android/os/Looper:quit	()V
            //     237: aconst_null
            //     238: putstatic 136	android/net/wifi/WifiConfigStore$DelayedDiskWrite:sDiskWriteHandler	Landroid/os/Handler;
            //     241: aconst_null
            //     242: putstatic 149	android/net/wifi/WifiConfigStore$DelayedDiskWrite:sDiskWriteHandlerThread	Landroid/os/HandlerThread;
            //     245: ldc 2
            //     247: monitorexit
            //     248: return
            //     249: aload_2
            //     250: ldc 150
            //     252: invokevirtual 123	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
            //     255: aload_2
            //     256: aload 15
            //     258: getfield 96	android/net/wifi/WifiConfiguration:ipAssignment	Landroid/net/wifi/WifiConfiguration$IpAssignment;
            //     261: invokevirtual 153	android/net/wifi/WifiConfiguration$IpAssignment:toString	()Ljava/lang/String;
            //     264: invokevirtual 123	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
            //     267: aload 18
            //     269: invokevirtual 159	android/net/LinkProperties:getLinkAddresses	()Ljava/util/Collection;
            //     272: invokeinterface 162 1 0
            //     277: astore 21
            //     279: aload 21
            //     281: invokeinterface 76 1 0
            //     286: ifeq +135 -> 421
            //     289: aload 21
            //     291: invokeinterface 80 1 0
            //     296: checkcast 164	android/net/LinkAddress
            //     299: astore 27
            //     301: aload_2
            //     302: ldc 166
            //     304: invokevirtual 123	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
            //     307: aload_2
            //     308: aload 27
            //     310: invokevirtual 170	android/net/LinkAddress:getAddress	()Ljava/net/InetAddress;
            //     313: invokevirtual 175	java/net/InetAddress:getHostAddress	()Ljava/lang/String;
            //     316: invokevirtual 123	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
            //     319: aload_2
            //     320: aload 27
            //     322: invokevirtual 178	android/net/LinkAddress:getNetworkPrefixLength	()I
            //     325: invokevirtual 64	java/io/DataOutputStream:writeInt	(I)V
            //     328: goto -49 -> 279
            //     331: astore 17
            //     333: new 180	java/lang/StringBuilder
            //     336: dup
            //     337: invokespecial 181	java/lang/StringBuilder:<init>	()V
            //     340: ldc 183
            //     342: invokevirtual 187	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //     345: aload 15
            //     347: getfield 86	android/net/wifi/WifiConfiguration:linkProperties	Landroid/net/LinkProperties;
            //     350: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     353: aload 17
            //     355: invokevirtual 190	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //     358: invokevirtual 191	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     361: invokestatic 106	android/net/wifi/WifiConfigStore$DelayedDiskWrite:loge	(Ljava/lang/String;)V
            //     364: goto -183 -> 181
            //     367: astore_3
            //     368: aload_2
            //     369: astore_1
            //     370: aload_1
            //     371: ifnull +7 -> 378
            //     374: aload_1
            //     375: invokevirtual 134	java/io/DataOutputStream:close	()V
            //     378: ldc 2
            //     380: monitorenter
            //     381: bipush 255
            //     383: getstatic 22	android/net/wifi/WifiConfigStore$DelayedDiskWrite:sWriteSequence	I
            //     386: iadd
            //     387: istore 5
            //     389: iload 5
            //     391: putstatic 22	android/net/wifi/WifiConfigStore$DelayedDiskWrite:sWriteSequence	I
            //     394: iload 5
            //     396: ifne +20 -> 416
            //     399: getstatic 136	android/net/wifi/WifiConfigStore$DelayedDiskWrite:sDiskWriteHandler	Landroid/os/Handler;
            //     402: invokevirtual 142	android/os/Handler:getLooper	()Landroid/os/Looper;
            //     405: invokevirtual 147	android/os/Looper:quit	()V
            //     408: aconst_null
            //     409: putstatic 136	android/net/wifi/WifiConfigStore$DelayedDiskWrite:sDiskWriteHandler	Landroid/os/Handler;
            //     412: aconst_null
            //     413: putstatic 149	android/net/wifi/WifiConfigStore$DelayedDiskWrite:sDiskWriteHandlerThread	Landroid/os/HandlerThread;
            //     416: ldc 2
            //     418: monitorexit
            //     419: aload_3
            //     420: athrow
            //     421: aload 18
            //     423: invokevirtual 194	android/net/LinkProperties:getRoutes	()Ljava/util/Collection;
            //     426: invokeinterface 162 1 0
            //     431: astore 22
            //     433: aload 22
            //     435: invokeinterface 76 1 0
            //     440: ifeq +103 -> 543
            //     443: aload 22
            //     445: invokeinterface 80 1 0
            //     450: checkcast 196	android/net/RouteInfo
            //     453: astore 25
            //     455: aload_2
            //     456: ldc 198
            //     458: invokevirtual 123	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
            //     461: aload 25
            //     463: invokevirtual 202	android/net/RouteInfo:getDestination	()Landroid/net/LinkAddress;
            //     466: astore 26
            //     468: aload 26
            //     470: ifnull +57 -> 527
            //     473: aload_2
            //     474: iconst_1
            //     475: invokevirtual 64	java/io/DataOutputStream:writeInt	(I)V
            //     478: aload_2
            //     479: aload 26
            //     481: invokevirtual 170	android/net/LinkAddress:getAddress	()Ljava/net/InetAddress;
            //     484: invokevirtual 175	java/net/InetAddress:getHostAddress	()Ljava/lang/String;
            //     487: invokevirtual 123	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
            //     490: aload_2
            //     491: aload 26
            //     493: invokevirtual 178	android/net/LinkAddress:getNetworkPrefixLength	()I
            //     496: invokevirtual 64	java/io/DataOutputStream:writeInt	(I)V
            //     499: aload 25
            //     501: invokevirtual 205	android/net/RouteInfo:getGateway	()Ljava/net/InetAddress;
            //     504: ifnull +31 -> 535
            //     507: aload_2
            //     508: iconst_1
            //     509: invokevirtual 64	java/io/DataOutputStream:writeInt	(I)V
            //     512: aload_2
            //     513: aload 25
            //     515: invokevirtual 205	android/net/RouteInfo:getGateway	()Ljava/net/InetAddress;
            //     518: invokevirtual 175	java/net/InetAddress:getHostAddress	()Ljava/lang/String;
            //     521: invokevirtual 123	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
            //     524: goto -91 -> 433
            //     527: aload_2
            //     528: iconst_0
            //     529: invokevirtual 64	java/io/DataOutputStream:writeInt	(I)V
            //     532: goto -33 -> 499
            //     535: aload_2
            //     536: iconst_0
            //     537: invokevirtual 64	java/io/DataOutputStream:writeInt	(I)V
            //     540: goto -107 -> 433
            //     543: aload 18
            //     545: invokevirtual 208	android/net/LinkProperties:getDnses	()Ljava/util/Collection;
            //     548: invokeinterface 162 1 0
            //     553: astore 23
            //     555: aload 23
            //     557: invokeinterface 76 1 0
            //     562: ifeq +261 -> 823
            //     565: aload 23
            //     567: invokeinterface 80 1 0
            //     572: checkcast 172	java/net/InetAddress
            //     575: astore 24
            //     577: aload_2
            //     578: ldc 210
            //     580: invokevirtual 123	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
            //     583: aload_2
            //     584: aload 24
            //     586: invokevirtual 175	java/net/InetAddress:getHostAddress	()Ljava/lang/String;
            //     589: invokevirtual 123	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
            //     592: goto -37 -> 555
            //     595: aload_2
            //     596: ldc 150
            //     598: invokevirtual 123	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
            //     601: aload_2
            //     602: aload 15
            //     604: getfield 96	android/net/wifi/WifiConfiguration:ipAssignment	Landroid/net/wifi/WifiConfiguration$IpAssignment;
            //     607: invokevirtual 153	android/net/wifi/WifiConfiguration$IpAssignment:toString	()Ljava/lang/String;
            //     610: invokevirtual 123	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
            //     613: iconst_1
            //     614: istore 16
            //     616: goto -499 -> 117
            //     619: aload 18
            //     621: invokevirtual 214	android/net/LinkProperties:getHttpProxy	()Landroid/net/ProxyProperties;
            //     624: astore 19
            //     626: aload 19
            //     628: invokevirtual 219	android/net/ProxyProperties:getExclusionList	()Ljava/lang/String;
            //     631: astore 20
            //     633: aload_2
            //     634: ldc 220
            //     636: invokevirtual 123	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
            //     639: aload_2
            //     640: aload 15
            //     642: getfield 113	android/net/wifi/WifiConfiguration:proxySettings	Landroid/net/wifi/WifiConfiguration$ProxySettings;
            //     645: invokevirtual 221	android/net/wifi/WifiConfiguration$ProxySettings:toString	()Ljava/lang/String;
            //     648: invokevirtual 123	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
            //     651: aload_2
            //     652: ldc 223
            //     654: invokevirtual 123	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
            //     657: aload_2
            //     658: aload 19
            //     660: invokevirtual 226	android/net/ProxyProperties:getHost	()Ljava/lang/String;
            //     663: invokevirtual 123	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
            //     666: aload_2
            //     667: ldc 228
            //     669: invokevirtual 123	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
            //     672: aload_2
            //     673: aload 19
            //     675: invokevirtual 231	android/net/ProxyProperties:getPort	()I
            //     678: invokevirtual 64	java/io/DataOutputStream:writeInt	(I)V
            //     681: aload_2
            //     682: ldc 233
            //     684: invokevirtual 123	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
            //     687: aload_2
            //     688: aload 20
            //     690: invokevirtual 123	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
            //     693: iconst_1
            //     694: istore 16
            //     696: goto -535 -> 161
            //     699: aload_2
            //     700: ldc 220
            //     702: invokevirtual 123	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
            //     705: aload_2
            //     706: aload 15
            //     708: getfield 113	android/net/wifi/WifiConfiguration:proxySettings	Landroid/net/wifi/WifiConfiguration$ProxySettings;
            //     711: invokevirtual 221	android/net/wifi/WifiConfiguration$ProxySettings:toString	()Ljava/lang/String;
            //     714: invokevirtual 123	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
            //     717: iconst_1
            //     718: istore 16
            //     720: goto -559 -> 161
            //     723: aload_2
            //     724: ifnull +7 -> 731
            //     727: aload_2
            //     728: invokevirtual 134	java/io/DataOutputStream:close	()V
            //     731: ldc 2
            //     733: monitorenter
            //     734: bipush 255
            //     736: getstatic 22	android/net/wifi/WifiConfigStore$DelayedDiskWrite:sWriteSequence	I
            //     739: iadd
            //     740: istore 13
            //     742: iload 13
            //     744: putstatic 22	android/net/wifi/WifiConfigStore$DelayedDiskWrite:sWriteSequence	I
            //     747: iload 13
            //     749: ifne +20 -> 769
            //     752: getstatic 136	android/net/wifi/WifiConfigStore$DelayedDiskWrite:sDiskWriteHandler	Landroid/os/Handler;
            //     755: invokevirtual 142	android/os/Handler:getLooper	()Landroid/os/Looper;
            //     758: invokevirtual 147	android/os/Looper:quit	()V
            //     761: aconst_null
            //     762: putstatic 136	android/net/wifi/WifiConfigStore$DelayedDiskWrite:sDiskWriteHandler	Landroid/os/Handler;
            //     765: aconst_null
            //     766: putstatic 149	android/net/wifi/WifiConfigStore$DelayedDiskWrite:sDiskWriteHandlerThread	Landroid/os/HandlerThread;
            //     769: ldc 2
            //     771: monitorexit
            //     772: goto -524 -> 248
            //     775: astore 6
            //     777: goto -399 -> 378
            //     780: astore 4
            //     782: ldc 2
            //     784: monitorexit
            //     785: aload 4
            //     787: athrow
            //     788: astore 10
            //     790: goto -583 -> 207
            //     793: astore 8
            //     795: ldc 2
            //     797: monitorexit
            //     798: aload 8
            //     800: athrow
            //     801: astore 14
            //     803: goto -72 -> 731
            //     806: astore 12
            //     808: ldc 2
            //     810: monitorexit
            //     811: aload 12
            //     813: athrow
            //     814: astore_3
            //     815: goto -445 -> 370
            //     818: astore 28
            //     820: goto -626 -> 194
            //     823: iconst_1
            //     824: istore 16
            //     826: goto -709 -> 117
            //
            // Exception table:
            //     from	to	target	type
            //     27	62	190	java/io/IOException
            //     65	181	190	java/io/IOException
            //     181	187	190	java/io/IOException
            //     249	328	190	java/io/IOException
            //     333	364	190	java/io/IOException
            //     421	717	190	java/io/IOException
            //     65	181	331	java/lang/NullPointerException
            //     249	328	331	java/lang/NullPointerException
            //     421	717	331	java/lang/NullPointerException
            //     27	62	367	finally
            //     65	181	367	finally
            //     181	187	367	finally
            //     249	328	367	finally
            //     333	364	367	finally
            //     421	717	367	finally
            //     374	378	775	java/lang/Exception
            //     381	419	780	finally
            //     782	785	780	finally
            //     203	207	788	java/lang/Exception
            //     210	248	793	finally
            //     795	798	793	finally
            //     727	731	801	java/lang/Exception
            //     734	772	806	finally
            //     808	811	806	finally
            //     2	27	814	finally
            //     194	199	814	finally
            //     2	27	818	java/io/IOException
        }

        static void write(List<WifiConfiguration> paramList)
        {
            try
            {
                int i = 1 + sWriteSequence;
                sWriteSequence = i;
                if (i == 1)
                {
                    sDiskWriteHandlerThread = new HandlerThread("WifiConfigThread");
                    sDiskWriteHandlerThread.start();
                    sDiskWriteHandler = new Handler(sDiskWriteHandlerThread.getLooper());
                }
                sDiskWriteHandler.post(new Runnable()
                {
                    public void run()
                    {
                        WifiConfigStore.DelayedDiskWrite.onWriteCalled(WifiConfigStore.DelayedDiskWrite.this);
                    }
                });
                return;
            }
            finally
            {
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.WifiConfigStore
 * JD-Core Version:        0.6.2
 */