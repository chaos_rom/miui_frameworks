package android.net.wifi;

public class StateChangeResult
{
    String BSSID;
    String SSID;
    int networkId;
    SupplicantState state;

    StateChangeResult(int paramInt, String paramString1, String paramString2, SupplicantState paramSupplicantState)
    {
        this.state = paramSupplicantState;
        this.SSID = paramString1;
        this.BSSID = paramString2;
        this.networkId = paramInt;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.StateChangeResult
 * JD-Core Version:        0.6.2
 */