package android.net.wifi;

import android.net.NetworkInfo.DetailedState;
import android.net.NetworkUtils;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.EnumMap;

public class WifiInfo
    implements Parcelable
{
    public static final Parcelable.Creator<WifiInfo> CREATOR = new Parcelable.Creator()
    {
        public WifiInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            int i = 1;
            WifiInfo localWifiInfo = new WifiInfo();
            localWifiInfo.setNetworkId(paramAnonymousParcel.readInt());
            localWifiInfo.setRssi(paramAnonymousParcel.readInt());
            localWifiInfo.setLinkSpeed(paramAnonymousParcel.readInt());
            if (paramAnonymousParcel.readByte() == i);
            try
            {
                localWifiInfo.setInetAddress(InetAddress.getByAddress(paramAnonymousParcel.createByteArray()));
                label53: localWifiInfo.setSSID(paramAnonymousParcel.readString());
                WifiInfo.access$002(localWifiInfo, paramAnonymousParcel.readString());
                WifiInfo.access$102(localWifiInfo, paramAnonymousParcel.readString());
                if (paramAnonymousParcel.readInt() != 0);
                while (true)
                {
                    WifiInfo.access$202(localWifiInfo, i);
                    WifiInfo.access$302(localWifiInfo, (SupplicantState)SupplicantState.CREATOR.createFromParcel(paramAnonymousParcel));
                    return localWifiInfo;
                    int j = 0;
                }
            }
            catch (UnknownHostException localUnknownHostException)
            {
                break label53;
            }
        }

        public WifiInfo[] newArray(int paramAnonymousInt)
        {
            return new WifiInfo[paramAnonymousInt];
        }
    };
    public static final String LINK_SPEED_UNITS = "Mbps";
    private static final EnumMap<SupplicantState, NetworkInfo.DetailedState> stateMap = new EnumMap(SupplicantState.class);
    private String mBSSID;
    private boolean mHiddenSSID;
    private InetAddress mIpAddress;
    private int mLinkSpeed;
    private String mMacAddress;
    private boolean mMeteredHint;
    private int mNetworkId;
    private int mRssi;
    private String mSSID;
    private SupplicantState mSupplicantState;

    static
    {
        stateMap.put(SupplicantState.DISCONNECTED, NetworkInfo.DetailedState.DISCONNECTED);
        stateMap.put(SupplicantState.INTERFACE_DISABLED, NetworkInfo.DetailedState.DISCONNECTED);
        stateMap.put(SupplicantState.INACTIVE, NetworkInfo.DetailedState.IDLE);
        stateMap.put(SupplicantState.SCANNING, NetworkInfo.DetailedState.SCANNING);
        stateMap.put(SupplicantState.AUTHENTICATING, NetworkInfo.DetailedState.CONNECTING);
        stateMap.put(SupplicantState.ASSOCIATING, NetworkInfo.DetailedState.CONNECTING);
        stateMap.put(SupplicantState.ASSOCIATED, NetworkInfo.DetailedState.CONNECTING);
        stateMap.put(SupplicantState.FOUR_WAY_HANDSHAKE, NetworkInfo.DetailedState.AUTHENTICATING);
        stateMap.put(SupplicantState.GROUP_HANDSHAKE, NetworkInfo.DetailedState.AUTHENTICATING);
        stateMap.put(SupplicantState.COMPLETED, NetworkInfo.DetailedState.OBTAINING_IPADDR);
        stateMap.put(SupplicantState.DORMANT, NetworkInfo.DetailedState.DISCONNECTED);
        stateMap.put(SupplicantState.UNINITIALIZED, NetworkInfo.DetailedState.IDLE);
        stateMap.put(SupplicantState.INVALID, NetworkInfo.DetailedState.FAILED);
    }

    WifiInfo()
    {
        this.mSSID = null;
        this.mBSSID = null;
        this.mNetworkId = -1;
        this.mSupplicantState = SupplicantState.UNINITIALIZED;
        this.mRssi = -9999;
        this.mLinkSpeed = -1;
        this.mHiddenSSID = false;
    }

    public WifiInfo(WifiInfo paramWifiInfo)
    {
        if (paramWifiInfo != null)
        {
            this.mSupplicantState = paramWifiInfo.mSupplicantState;
            this.mBSSID = paramWifiInfo.mBSSID;
            this.mSSID = paramWifiInfo.mSSID;
            this.mNetworkId = paramWifiInfo.mNetworkId;
            this.mHiddenSSID = paramWifiInfo.mHiddenSSID;
            this.mRssi = paramWifiInfo.mRssi;
            this.mLinkSpeed = paramWifiInfo.mLinkSpeed;
            this.mIpAddress = paramWifiInfo.mIpAddress;
            this.mMacAddress = paramWifiInfo.mMacAddress;
            this.mMeteredHint = paramWifiInfo.mMeteredHint;
        }
    }

    public static NetworkInfo.DetailedState getDetailedStateOf(SupplicantState paramSupplicantState)
    {
        return (NetworkInfo.DetailedState)stateMap.get(paramSupplicantState);
    }

    public static String removeDoubleQuotes(String paramString)
    {
        if (paramString == null);
        int i;
        for (paramString = null; ; paramString = paramString.substring(1, i - 1))
            do
            {
                return paramString;
                i = paramString.length();
            }
            while ((i <= 1) || (paramString.charAt(0) != '"') || (paramString.charAt(i - 1) != '"'));
    }

    static SupplicantState valueOf(String paramString)
    {
        Object localObject;
        if ("4WAY_HANDSHAKE".equalsIgnoreCase(paramString))
            localObject = SupplicantState.FOUR_WAY_HANDSHAKE;
        while (true)
        {
            return localObject;
            try
            {
                SupplicantState localSupplicantState = SupplicantState.valueOf(paramString.toUpperCase());
                localObject = localSupplicantState;
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
                localObject = SupplicantState.INVALID;
            }
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public String getBSSID()
    {
        return this.mBSSID;
    }

    public boolean getHiddenSSID()
    {
        return this.mHiddenSSID;
    }

    public int getIpAddress()
    {
        if ((this.mIpAddress == null) || ((this.mIpAddress instanceof Inet6Address)));
        for (int i = 0; ; i = NetworkUtils.inetAddressToInt(this.mIpAddress))
            return i;
    }

    public int getLinkSpeed()
    {
        return this.mLinkSpeed;
    }

    public String getMacAddress()
    {
        return this.mMacAddress;
    }

    public boolean getMeteredHint()
    {
        return this.mMeteredHint;
    }

    public int getNetworkId()
    {
        return this.mNetworkId;
    }

    public int getRssi()
    {
        return this.mRssi;
    }

    public String getSSID()
    {
        return this.mSSID;
    }

    public SupplicantState getSupplicantState()
    {
        return this.mSupplicantState;
    }

    void setBSSID(String paramString)
    {
        this.mBSSID = paramString;
    }

    public void setHiddenSSID(boolean paramBoolean)
    {
        this.mHiddenSSID = paramBoolean;
    }

    void setInetAddress(InetAddress paramInetAddress)
    {
        this.mIpAddress = paramInetAddress;
    }

    void setLinkSpeed(int paramInt)
    {
        this.mLinkSpeed = paramInt;
    }

    void setMacAddress(String paramString)
    {
        this.mMacAddress = paramString;
    }

    public void setMeteredHint(boolean paramBoolean)
    {
        this.mMeteredHint = paramBoolean;
    }

    void setNetworkId(int paramInt)
    {
        this.mNetworkId = paramInt;
    }

    void setRssi(int paramInt)
    {
        this.mRssi = paramInt;
    }

    void setSSID(String paramString)
    {
        this.mSSID = paramString;
        this.mHiddenSSID = false;
    }

    void setSupplicantState(SupplicantState paramSupplicantState)
    {
        this.mSupplicantState = paramSupplicantState;
    }

    void setSupplicantState(String paramString)
    {
        this.mSupplicantState = valueOf(paramString);
    }

    public String toString()
    {
        StringBuffer localStringBuffer1 = new StringBuffer();
        Object localObject1 = "<none>";
        StringBuffer localStringBuffer2 = localStringBuffer1.append("SSID: ");
        Object localObject2;
        Object localObject3;
        label51: Object localObject4;
        label75: StringBuffer localStringBuffer5;
        if (this.mSSID == null)
        {
            localObject2 = localObject1;
            StringBuffer localStringBuffer3 = localStringBuffer2.append((String)localObject2).append(", BSSID: ");
            if (this.mBSSID != null)
                break label165;
            localObject3 = localObject1;
            StringBuffer localStringBuffer4 = localStringBuffer3.append((String)localObject3).append(", MAC: ");
            if (this.mMacAddress != null)
                break label174;
            localObject4 = localObject1;
            localStringBuffer5 = localStringBuffer4.append((String)localObject4).append(", Supplicant state: ");
            if (this.mSupplicantState != null)
                break label183;
        }
        while (true)
        {
            localStringBuffer5.append(localObject1).append(", RSSI: ").append(this.mRssi).append(", Link speed: ").append(this.mLinkSpeed).append(", Net ID: ").append(this.mNetworkId).append(", Metered hint: ").append(this.mMeteredHint);
            return localStringBuffer1.toString();
            localObject2 = this.mSSID;
            break;
            label165: localObject3 = this.mBSSID;
            break label51;
            label174: localObject4 = this.mMacAddress;
            break label75;
            label183: localObject1 = this.mSupplicantState;
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        byte b = 1;
        paramParcel.writeInt(this.mNetworkId);
        paramParcel.writeInt(this.mRssi);
        paramParcel.writeInt(this.mLinkSpeed);
        if (this.mIpAddress != null)
        {
            paramParcel.writeByte(b);
            paramParcel.writeByteArray(this.mIpAddress.getAddress());
            paramParcel.writeString(getSSID());
            paramParcel.writeString(this.mBSSID);
            paramParcel.writeString(this.mMacAddress);
            if (!this.mMeteredHint)
                break label103;
        }
        while (true)
        {
            paramParcel.writeInt(b);
            this.mSupplicantState.writeToParcel(paramParcel, paramInt);
            return;
            paramParcel.writeByte((byte)0);
            break;
            label103: b = 0;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.WifiInfo
 * JD-Core Version:        0.6.2
 */