package android.net.wifi;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.android.internal.util.State;
import com.android.internal.util.StateMachine;

class SupplicantStateTracker extends StateMachine
{
    private static final boolean DBG = false;
    private static final int MAX_RETRIES_ON_AUTHENTICATION_FAILURE = 2;
    private static final String TAG = "SupplicantStateTracker";
    private boolean mAuthFailureInSupplicantBroadcast = false;
    private int mAuthenticationFailuresCount = 0;
    private State mCompletedState = new CompletedState();
    private Context mContext;
    private State mDefaultState = new DefaultState();
    private State mDisconnectState = new DisconnectedState();
    private State mDormantState = new DormantState();
    private State mHandshakeState = new HandshakeState();
    private State mInactiveState = new InactiveState();
    private boolean mNetworksDisabledDuringConnect = false;
    private State mScanState = new ScanState();
    private State mUninitializedState = new UninitializedState();
    private WifiConfigStore mWifiConfigStore;
    private WifiStateMachine mWifiStateMachine;

    public SupplicantStateTracker(Context paramContext, WifiStateMachine paramWifiStateMachine, WifiConfigStore paramWifiConfigStore, Handler paramHandler)
    {
        super("SupplicantStateTracker", paramHandler.getLooper());
        this.mContext = paramContext;
        this.mWifiStateMachine = paramWifiStateMachine;
        this.mWifiConfigStore = paramWifiConfigStore;
        addState(this.mDefaultState);
        addState(this.mUninitializedState, this.mDefaultState);
        addState(this.mInactiveState, this.mDefaultState);
        addState(this.mDisconnectState, this.mDefaultState);
        addState(this.mScanState, this.mDefaultState);
        addState(this.mHandshakeState, this.mDefaultState);
        addState(this.mCompletedState, this.mDefaultState);
        addState(this.mDormantState, this.mDefaultState);
        setInitialState(this.mUninitializedState);
        start();
    }

    private void handleNetworkConnectionFailure(int paramInt)
    {
        if (this.mNetworksDisabledDuringConnect)
        {
            this.mWifiConfigStore.enableAllNetworks();
            this.mNetworksDisabledDuringConnect = false;
        }
        this.mWifiConfigStore.disableNetwork(paramInt, 3);
    }

    private void sendSupplicantStateChangedBroadcast(SupplicantState paramSupplicantState, boolean paramBoolean)
    {
        Intent localIntent = new Intent("android.net.wifi.supplicant.STATE_CHANGE");
        localIntent.addFlags(671088640);
        localIntent.putExtra("newState", paramSupplicantState);
        if (paramBoolean)
            localIntent.putExtra("supplicantError", 1);
        this.mContext.sendStickyBroadcast(localIntent);
    }

    private void transitionOnSupplicantStateChange(StateChangeResult paramStateChangeResult)
    {
        SupplicantState localSupplicantState = paramStateChangeResult.state;
        switch (1.$SwitchMap$android$net$wifi$SupplicantState[localSupplicantState.ordinal()])
        {
        default:
            Log.e("SupplicantStateTracker", "Unknown supplicant state " + localSupplicantState);
        case 2:
        case 1:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
        case 13:
        }
        while (true)
        {
            return;
            transitionTo(this.mDisconnectState);
            continue;
            transitionTo(this.mScanState);
            continue;
            transitionTo(this.mHandshakeState);
            continue;
            transitionTo(this.mCompletedState);
            continue;
            transitionTo(this.mDormantState);
            continue;
            transitionTo(this.mInactiveState);
            continue;
            transitionTo(this.mUninitializedState);
        }
    }

    class DormantState extends State
    {
        DormantState()
        {
        }

        public void enter()
        {
        }
    }

    class CompletedState extends State
    {
        CompletedState()
        {
        }

        public void enter()
        {
            SupplicantStateTracker.access$002(SupplicantStateTracker.this, 0);
            if (SupplicantStateTracker.this.mNetworksDisabledDuringConnect)
            {
                SupplicantStateTracker.this.mWifiConfigStore.enableAllNetworks();
                SupplicantStateTracker.access$602(SupplicantStateTracker.this, false);
            }
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool = false;
            StateChangeResult localStateChangeResult;
            switch (paramMessage.what)
            {
            default:
                return bool;
            case 147462:
                localStateChangeResult = (StateChangeResult)paramMessage.obj;
                SupplicantState localSupplicantState = localStateChangeResult.state;
                SupplicantStateTracker.this.sendSupplicantStateChangedBroadcast(localSupplicantState, SupplicantStateTracker.this.mAuthFailureInSupplicantBroadcast);
                if (!SupplicantState.isConnecting(localSupplicantState))
                    break;
            case 131183:
            }
            while (true)
            {
                bool = true;
                break;
                SupplicantStateTracker.this.transitionOnSupplicantStateChange(localStateChangeResult);
                continue;
                SupplicantStateTracker.this.sendSupplicantStateChangedBroadcast(SupplicantState.DISCONNECTED, false);
                SupplicantStateTracker.this.transitionTo(SupplicantStateTracker.this.mUninitializedState);
            }
        }
    }

    class HandshakeState extends State
    {
        private static final int MAX_SUPPLICANT_LOOP_ITERATIONS = 4;
        private int mLoopDetectCount;
        private int mLoopDetectIndex;

        HandshakeState()
        {
        }

        public void enter()
        {
            this.mLoopDetectIndex = 0;
            this.mLoopDetectCount = 0;
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool = false;
            switch (paramMessage.what)
            {
            default:
            case 147462:
            }
            while (true)
            {
                return bool;
                StateChangeResult localStateChangeResult = (StateChangeResult)paramMessage.obj;
                SupplicantState localSupplicantState = localStateChangeResult.state;
                if (SupplicantState.isHandshakeState(localSupplicantState))
                {
                    if (this.mLoopDetectIndex > localSupplicantState.ordinal())
                        this.mLoopDetectCount = (1 + this.mLoopDetectCount);
                    if (this.mLoopDetectCount > 4)
                    {
                        Log.d("SupplicantStateTracker", "Supplicant loop detected, disabling network " + localStateChangeResult.networkId);
                        SupplicantStateTracker.this.handleNetworkConnectionFailure(localStateChangeResult.networkId);
                    }
                    this.mLoopDetectIndex = localSupplicantState.ordinal();
                    SupplicantStateTracker.this.sendSupplicantStateChangedBroadcast(localSupplicantState, SupplicantStateTracker.this.mAuthFailureInSupplicantBroadcast);
                    bool = true;
                }
            }
        }
    }

    class ScanState extends State
    {
        ScanState()
        {
        }

        public void enter()
        {
        }
    }

    class DisconnectedState extends State
    {
        DisconnectedState()
        {
        }

        public void enter()
        {
            StateChangeResult localStateChangeResult = (StateChangeResult)SupplicantStateTracker.this.getCurrentMessage().obj;
            if (SupplicantStateTracker.this.mAuthenticationFailuresCount >= 2)
            {
                Log.d("SupplicantStateTracker", "Failed to authenticate, disabling network " + localStateChangeResult.networkId);
                SupplicantStateTracker.this.handleNetworkConnectionFailure(localStateChangeResult.networkId);
                SupplicantStateTracker.access$002(SupplicantStateTracker.this, 0);
            }
        }
    }

    class InactiveState extends State
    {
        InactiveState()
        {
        }

        public void enter()
        {
        }
    }

    class UninitializedState extends State
    {
        UninitializedState()
        {
        }

        public void enter()
        {
        }
    }

    class DefaultState extends State
    {
        DefaultState()
        {
        }

        public void enter()
        {
        }

        public boolean processMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
                Log.e("SupplicantStateTracker", "Ignoring " + paramMessage);
            case 147463:
            case 147462:
            case 131183:
            case 151553:
            }
            while (true)
            {
                return true;
                SupplicantStateTracker.access$008(SupplicantStateTracker.this);
                SupplicantStateTracker.access$102(SupplicantStateTracker.this, true);
                continue;
                StateChangeResult localStateChangeResult = (StateChangeResult)paramMessage.obj;
                SupplicantState localSupplicantState = localStateChangeResult.state;
                SupplicantStateTracker.this.sendSupplicantStateChangedBroadcast(localSupplicantState, SupplicantStateTracker.this.mAuthFailureInSupplicantBroadcast);
                SupplicantStateTracker.access$102(SupplicantStateTracker.this, false);
                SupplicantStateTracker.this.transitionOnSupplicantStateChange(localStateChangeResult);
                continue;
                SupplicantStateTracker.this.transitionTo(SupplicantStateTracker.this.mUninitializedState);
                continue;
                SupplicantStateTracker.access$602(SupplicantStateTracker.this, true);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.SupplicantStateTracker
 * JD-Core Version:        0.6.2
 */