package android.net.wifi;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import com.android.internal.util.AsyncChannel;
import com.android.internal.util.State;
import com.android.internal.util.StateMachine;
import java.util.BitSet;
import java.util.UUID;

class WifiApConfigStore extends StateMachine
{
    private static final String AP_CONFIG_FILE = Environment.getDataDirectory() + "/misc/wifi/softap.conf";
    private static final int AP_CONFIG_FILE_VERSION = 1;
    private static final String TAG = "WifiApConfigStore";
    private State mActiveState = new ActiveState();
    private Context mContext;
    private State mDefaultState = new DefaultState();
    private State mInactiveState = new InactiveState();
    private AsyncChannel mReplyChannel = new AsyncChannel();
    private WifiConfiguration mWifiApConfig = null;

    WifiApConfigStore(Context paramContext, Handler paramHandler)
    {
        super("WifiApConfigStore", paramHandler.getLooper());
        this.mContext = paramContext;
        addState(this.mDefaultState);
        addState(this.mInactiveState, this.mDefaultState);
        addState(this.mActiveState, this.mDefaultState);
        setInitialState(this.mInactiveState);
    }

    public static WifiApConfigStore makeWifiApConfigStore(Context paramContext, Handler paramHandler)
    {
        WifiApConfigStore localWifiApConfigStore = new WifiApConfigStore(paramContext, paramHandler);
        localWifiApConfigStore.start();
        return localWifiApConfigStore;
    }

    private void setDefaultApConfiguration()
    {
        WifiConfiguration localWifiConfiguration = new WifiConfiguration();
        localWifiConfiguration.SSID = this.mContext.getString(17040391);
        localWifiConfiguration.allowedKeyManagement.set(4);
        String str = UUID.randomUUID().toString();
        localWifiConfiguration.preSharedKey = (str.substring(0, 8) + str.substring(9, 13));
        sendMessage(131097, localWifiConfiguration);
    }

    // ERROR //
    private void writeApConfiguration(WifiConfiguration paramWifiConfiguration)
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_2
        //     2: new 183	java/io/DataOutputStream
        //     5: dup
        //     6: new 185	java/io/BufferedOutputStream
        //     9: dup
        //     10: new 187	java/io/FileOutputStream
        //     13: dup
        //     14: getstatic 59	android/net/wifi/WifiApConfigStore:AP_CONFIG_FILE	Ljava/lang/String;
        //     17: invokespecial 190	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
        //     20: invokespecial 193	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;)V
        //     23: invokespecial 194	java/io/DataOutputStream:<init>	(Ljava/io/OutputStream;)V
        //     26: astore_3
        //     27: aload_3
        //     28: iconst_1
        //     29: invokevirtual 197	java/io/DataOutputStream:writeInt	(I)V
        //     32: aload_3
        //     33: aload_1
        //     34: getfield 148	android/net/wifi/WifiConfiguration:SSID	Ljava/lang/String;
        //     37: invokevirtual 200	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
        //     40: aload_1
        //     41: invokevirtual 204	android/net/wifi/WifiConfiguration:getAuthType	()I
        //     44: istore 9
        //     46: aload_3
        //     47: iload 9
        //     49: invokevirtual 197	java/io/DataOutputStream:writeInt	(I)V
        //     52: iload 9
        //     54: ifeq +11 -> 65
        //     57: aload_3
        //     58: aload_1
        //     59: getfield 174	android/net/wifi/WifiConfiguration:preSharedKey	Ljava/lang/String;
        //     62: invokevirtual 200	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
        //     65: aload_3
        //     66: ifnull +7 -> 73
        //     69: aload_3
        //     70: invokevirtual 207	java/io/DataOutputStream:close	()V
        //     73: return
        //     74: astore 4
        //     76: ldc 21
        //     78: new 35	java/lang/StringBuilder
        //     81: dup
        //     82: invokespecial 38	java/lang/StringBuilder:<init>	()V
        //     85: ldc 209
        //     87: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     90: aload 4
        //     92: invokevirtual 48	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     95: invokevirtual 57	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     98: invokestatic 215	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     101: pop
        //     102: aload_2
        //     103: ifnull -30 -> 73
        //     106: aload_2
        //     107: invokevirtual 207	java/io/DataOutputStream:close	()V
        //     110: goto -37 -> 73
        //     113: astore 8
        //     115: goto -42 -> 73
        //     118: astore 5
        //     120: aload_2
        //     121: ifnull +7 -> 128
        //     124: aload_2
        //     125: invokevirtual 207	java/io/DataOutputStream:close	()V
        //     128: aload 5
        //     130: athrow
        //     131: astore 6
        //     133: goto -5 -> 128
        //     136: astore 10
        //     138: goto -65 -> 73
        //     141: astore 5
        //     143: aload_3
        //     144: astore_2
        //     145: goto -25 -> 120
        //     148: astore 4
        //     150: aload_3
        //     151: astore_2
        //     152: goto -76 -> 76
        //
        // Exception table:
        //     from	to	target	type
        //     2	27	74	java/io/IOException
        //     106	110	113	java/io/IOException
        //     2	27	118	finally
        //     76	102	118	finally
        //     124	128	131	java/io/IOException
        //     69	73	136	java/io/IOException
        //     27	65	141	finally
        //     27	65	148	java/io/IOException
    }

    Messenger getMessenger()
    {
        return new Messenger(getHandler());
    }

    // ERROR //
    void loadApConfiguration()
    {
        // Byte code:
        //     0: aconst_null
        //     1: astore_1
        //     2: new 137	android/net/wifi/WifiConfiguration
        //     5: dup
        //     6: invokespecial 138	android/net/wifi/WifiConfiguration:<init>	()V
        //     9: astore_2
        //     10: new 229	java/io/DataInputStream
        //     13: dup
        //     14: new 231	java/io/BufferedInputStream
        //     17: dup
        //     18: new 233	java/io/FileInputStream
        //     21: dup
        //     22: getstatic 59	android/net/wifi/WifiApConfigStore:AP_CONFIG_FILE	Ljava/lang/String;
        //     25: invokespecial 234	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
        //     28: invokespecial 237	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
        //     31: invokespecial 238	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
        //     34: astore_3
        //     35: aload_3
        //     36: invokevirtual 241	java/io/DataInputStream:readInt	()I
        //     39: iconst_1
        //     40: if_icmpeq +24 -> 64
        //     43: ldc 21
        //     45: ldc 243
        //     47: invokestatic 215	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
        //     50: pop
        //     51: aload_0
        //     52: invokespecial 245	android/net/wifi/WifiApConfigStore:setDefaultApConfiguration	()V
        //     55: aload_3
        //     56: ifnull +7 -> 63
        //     59: aload_3
        //     60: invokevirtual 246	java/io/DataInputStream:close	()V
        //     63: return
        //     64: aload_2
        //     65: aload_3
        //     66: invokevirtual 249	java/io/DataInputStream:readUTF	()Ljava/lang/String;
        //     69: putfield 148	android/net/wifi/WifiConfiguration:SSID	Ljava/lang/String;
        //     72: aload_3
        //     73: invokevirtual 241	java/io/DataInputStream:readInt	()I
        //     76: istore 8
        //     78: aload_2
        //     79: getfield 152	android/net/wifi/WifiConfiguration:allowedKeyManagement	Ljava/util/BitSet;
        //     82: iload 8
        //     84: invokevirtual 158	java/util/BitSet:set	(I)V
        //     87: iload 8
        //     89: ifeq +11 -> 100
        //     92: aload_2
        //     93: aload_3
        //     94: invokevirtual 249	java/io/DataInputStream:readUTF	()Ljava/lang/String;
        //     97: putfield 174	android/net/wifi/WifiConfiguration:preSharedKey	Ljava/lang/String;
        //     100: aload_0
        //     101: aload_2
        //     102: putfield 82	android/net/wifi/WifiApConfigStore:mWifiApConfig	Landroid/net/wifi/WifiConfiguration;
        //     105: aload_3
        //     106: ifnull +7 -> 113
        //     109: aload_3
        //     110: invokevirtual 246	java/io/DataInputStream:close	()V
        //     113: goto -50 -> 63
        //     116: astore 12
        //     118: aload_0
        //     119: invokespecial 245	android/net/wifi/WifiApConfigStore:setDefaultApConfiguration	()V
        //     122: aload_1
        //     123: ifnull -60 -> 63
        //     126: aload_1
        //     127: invokevirtual 246	java/io/DataInputStream:close	()V
        //     130: goto -67 -> 63
        //     133: astore 7
        //     135: goto -72 -> 63
        //     138: astore 5
        //     140: aload_1
        //     141: ifnull +7 -> 148
        //     144: aload_1
        //     145: invokevirtual 246	java/io/DataInputStream:close	()V
        //     148: aload 5
        //     150: athrow
        //     151: astore 6
        //     153: goto -5 -> 148
        //     156: astore 11
        //     158: goto -95 -> 63
        //     161: astore 9
        //     163: goto -50 -> 113
        //     166: astore 5
        //     168: aload_3
        //     169: astore_1
        //     170: goto -30 -> 140
        //     173: astore 4
        //     175: aload_3
        //     176: astore_1
        //     177: goto -59 -> 118
        //
        // Exception table:
        //     from	to	target	type
        //     2	35	116	java/io/IOException
        //     126	130	133	java/io/IOException
        //     2	35	138	finally
        //     118	122	138	finally
        //     144	148	151	java/io/IOException
        //     59	63	156	java/io/IOException
        //     109	113	161	java/io/IOException
        //     35	55	166	finally
        //     64	105	166	finally
        //     35	55	173	java/io/IOException
        //     64	105	173	java/io/IOException
    }

    class ActiveState extends State
    {
        ActiveState()
        {
        }

        public void enter()
        {
            new Thread(new Runnable()
            {
                public void run()
                {
                    WifiApConfigStore.this.writeApConfiguration(WifiApConfigStore.this.mWifiApConfig);
                    WifiApConfigStore.this.sendMessage(131098);
                }
            }).start();
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool;
            switch (paramMessage.what)
            {
            default:
                bool = false;
                return bool;
            case 131097:
                WifiApConfigStore.this.deferMessage(paramMessage);
            case 131098:
            }
            while (true)
            {
                bool = true;
                break;
                WifiApConfigStore.this.transitionTo(WifiApConfigStore.this.mInactiveState);
            }
        }
    }

    class InactiveState extends State
    {
        InactiveState()
        {
        }

        public boolean processMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
            case 131097:
            }
            for (boolean bool = false; ; bool = true)
            {
                return bool;
                WifiApConfigStore.access$002(WifiApConfigStore.this, (WifiConfiguration)paramMessage.obj);
                WifiApConfigStore.this.transitionTo(WifiApConfigStore.this.mActiveState);
            }
        }
    }

    class DefaultState extends State
    {
        DefaultState()
        {
        }

        public boolean processMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
                Log.e("WifiApConfigStore", "Failed to handle " + paramMessage);
            case 131097:
            case 131098:
            case 131099:
            }
            while (true)
            {
                return true;
                Log.e("WifiApConfigStore", "Unexpected message: " + paramMessage);
                continue;
                WifiApConfigStore.this.mReplyChannel.replyToMessage(paramMessage, 131100, WifiApConfigStore.this.mWifiApConfig);
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.WifiApConfigStore
 * JD-Core Version:        0.6.2
 */