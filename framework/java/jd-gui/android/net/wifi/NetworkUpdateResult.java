package android.net.wifi;

class NetworkUpdateResult
{
    boolean ipChanged;
    boolean isNewNetwork = false;
    int netId;
    boolean proxyChanged;

    public NetworkUpdateResult(int paramInt)
    {
        this.netId = paramInt;
        this.ipChanged = false;
        this.proxyChanged = false;
    }

    public NetworkUpdateResult(boolean paramBoolean1, boolean paramBoolean2)
    {
        this.netId = -1;
        this.ipChanged = paramBoolean1;
        this.proxyChanged = paramBoolean2;
    }

    public int getNetworkId()
    {
        return this.netId;
    }

    public boolean hasIpChanged()
    {
        return this.ipChanged;
    }

    public boolean hasProxyChanged()
    {
        return this.proxyChanged;
    }

    public boolean isNewNetwork()
    {
        return this.isNewNetwork;
    }

    public void setIpChanged(boolean paramBoolean)
    {
        this.ipChanged = paramBoolean;
    }

    public void setIsNewNetwork(boolean paramBoolean)
    {
        this.isNewNetwork = paramBoolean;
    }

    public void setNetworkId(int paramInt)
    {
        this.netId = paramInt;
    }

    public void setProxyChanged(boolean paramBoolean)
    {
        this.proxyChanged = paramBoolean;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.NetworkUpdateResult
 * JD-Core Version:        0.6.2
 */