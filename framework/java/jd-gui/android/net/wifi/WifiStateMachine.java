package android.net.wifi;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.backup.IBackupManager;
import android.app.backup.IBackupManager.Stub;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.DhcpInfoInternal;
import android.net.DhcpStateMachine;
import android.net.InterfaceConfiguration;
import android.net.LinkAddress;
import android.net.LinkProperties;
import android.net.NetworkInfo;
import android.net.NetworkInfo.DetailedState;
import android.net.NetworkInfo.State;
import android.net.NetworkUtils;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Binder;
import android.os.INetworkManagementService;
import android.os.INetworkManagementService.Stub;
import android.os.Message;
import android.os.Messenger;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.WorkSource;
import android.provider.Settings.Secure;
import android.util.EventLog;
import android.util.Log;
import android.util.LruCache;
import com.android.internal.app.IBatteryStats;
import com.android.internal.app.IBatteryStats.Stub;
import com.android.internal.util.AsyncChannel;
import com.android.internal.util.IState;
import com.android.internal.util.State;
import com.android.internal.util.StateMachine;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

public class WifiStateMachine extends StateMachine
{
    private static final String ACTION_DELAYED_DRIVER_STOP = "com.android.server.WifiManager.action.DELAYED_DRIVER_STOP";
    private static final String ACTION_START_SCAN = "com.android.server.WifiManager.action.START_SCAN";
    static final int BASE = 131072;
    static final int CMD_ADD_OR_UPDATE_NETWORK = 131124;
    static final int CMD_BLACKLIST_NETWORK = 131128;
    static final int CMD_BLUETOOTH_ADAPTER_STATE_CHANGE = 131103;
    static final int CMD_CLEAR_BLACKLIST = 131129;
    static final int CMD_CLEAR_SUSPEND_OPTIMIZATIONS = 131159;
    static final int CMD_DELAYED_STOP_DRIVER = 131090;
    public static final int CMD_DISABLE_P2P = 131204;
    static final int CMD_DISCONNECT = 131146;
    static final int CMD_DRIVER_START_TIMED_OUT = 131091;
    static final int CMD_ENABLE_ALL_NETWORKS = 131127;
    static final int CMD_ENABLE_BACKGROUND_SCAN = 131163;
    static final int CMD_ENABLE_NETWORK = 131126;
    public static final int CMD_ENABLE_P2P = 131203;
    static final int CMD_ENABLE_RSSI_POLL = 131154;
    static final int CMD_GET_CONFIGURED_NETWORKS = 131131;
    static final int CMD_LOAD_DRIVER = 131073;
    static final int CMD_LOAD_DRIVER_FAILURE = 131076;
    static final int CMD_LOAD_DRIVER_SUCCESS = 131075;
    static final int CMD_NO_NETWORKS_PERIODIC_SCAN = 131160;
    static final int CMD_PING_SUPPLICANT = 131123;
    static final int CMD_REASSOCIATE = 131148;
    static final int CMD_RECONNECT = 131147;
    static final int CMD_REMOVE_NETWORK = 131125;
    static final int CMD_REQUEST_AP_CONFIG = 131099;
    static final int CMD_RESET_SUPPLICANT_STATE = 131183;
    static final int CMD_RESPONSE_AP_CONFIG = 131100;
    static final int CMD_RSSI_POLL = 131155;
    static final int CMD_SAVE_CONFIG = 131130;
    static final int CMD_SET_AP_CONFIG = 131097;
    static final int CMD_SET_AP_CONFIG_COMPLETED = 131098;
    static final int CMD_SET_COUNTRY_CODE = 131152;
    static final int CMD_SET_FREQUENCY_BAND = 131162;
    static final int CMD_SET_HIGH_PERF_MODE = 131149;
    static final int CMD_SET_SCAN_MODE = 131144;
    static final int CMD_SET_SCAN_TYPE = 131145;
    static final int CMD_SET_SUSPEND_OPTIMIZATIONS = 131158;
    static final int CMD_START_AP = 131093;
    static final int CMD_START_AP_FAILURE = 131095;
    static final int CMD_START_AP_SUCCESS = 131094;
    static final int CMD_START_DRIVER = 131085;
    static final int CMD_START_PACKET_FILTERING = 131156;
    static final int CMD_START_SCAN = 131143;
    static final int CMD_START_SUPPLICANT = 131083;
    static final int CMD_STATIC_IP_FAILURE = 131088;
    static final int CMD_STATIC_IP_SUCCESS = 131087;
    static final int CMD_STOP_AP = 131096;
    static final int CMD_STOP_DRIVER = 131086;
    static final int CMD_STOP_PACKET_FILTERING = 131157;
    static final int CMD_STOP_SUPPLICANT = 131084;
    static final int CMD_STOP_SUPPLICANT_FAILED = 131089;
    static final int CMD_TETHER_NOTIFICATION_TIMED_OUT = 131102;
    static final int CMD_TETHER_STATE_CHANGE = 131101;
    static final int CMD_UNLOAD_DRIVER = 131074;
    static final int CMD_UNLOAD_DRIVER_FAILURE = 131078;
    static final int CMD_UNLOAD_DRIVER_SUCCESS = 131077;
    private static final int CONNECT_MODE = 1;
    private static final boolean DBG = false;
    private static final int DEFAULT_MAX_DHCP_RETRIES = 9;
    private static final String DELAYED_STOP_COUNTER = "DelayedStopCounter";
    private static final int DRIVER_START_TIME_OUT_MSECS = 10000;
    private static final int DRIVER_STOP_REQUEST = 0;
    private static final int EVENTLOG_SUPPLICANT_STATE_CHANGED = 50023;
    private static final int EVENTLOG_WIFI_EVENT_HANDLED = 50022;
    private static final int EVENTLOG_WIFI_STATE_CHANGED = 50021;
    private static final int FAILURE = -1;
    private static final int IN_ECM_STATE = 1;
    private static final int MAX_RSSI = 256;
    private static final int MIN_INTERVAL_ENABLE_ALL_NETWORKS_MS = 600000;
    private static final int MIN_RSSI = -200;
    static final int MULTICAST_V4 = 0;
    static final int MULTICAST_V6 = 1;
    private static final String NETWORKTYPE = "WIFI";
    private static final int NOT_IN_ECM_STATE = 0;
    private static final int POLL_RSSI_INTERVAL_MSECS = 3000;
    private static final int SCAN_ACTIVE = 1;
    private static final int SCAN_ONLY_MODE = 2;
    private static final int SCAN_PASSIVE = 2;
    private static final int SCAN_REQUEST = 0;
    private static final int SCAN_RESULT_CACHE_SIZE = 80;
    private static final String SOFTAP_IFACE = "wl0.1";
    private static final int SUCCESS = 1;
    private static final int SUPPLICANT_RESTART_INTERVAL_MSECS = 5000;
    private static final int SUPPLICANT_RESTART_TRIES = 5;
    private static final String TAG = "WifiStateMachine";
    private static final int TETHER_NOTIFICATION_TIME_OUT_MSECS = 5000;
    private static final Pattern scanResultPattern = Pattern.compile("\t+");
    private AlarmManager mAlarmManager;
    private final boolean mBackgroundScanSupported;
    private final IBatteryStats mBatteryStats;
    private boolean mBluetoothConnectionActive = false;
    private ConnectivityManager mCm;
    private State mConnectModeState = new ConnectModeState();
    private State mConnectedState = new ConnectedState();
    private Context mContext;
    private final int mDefaultFrameworkScanIntervalMs;
    private State mDefaultState = new DefaultState();
    private int mDelayedStopCounter;
    private DhcpInfoInternal mDhcpInfoInternal;
    private DhcpStateMachine mDhcpStateMachine;
    private State mDisconnectedState = new DisconnectedState();
    private State mDisconnectingState = new DisconnectingState();
    private State mDriverFailedState = new DriverFailedState();
    private State mDriverLoadedState = new DriverLoadedState();
    private State mDriverLoadingState = new DriverLoadingState();
    private int mDriverStartToken = 0;
    private State mDriverStartedState = new DriverStartedState();
    private State mDriverStartingState = new DriverStartingState();
    private final int mDriverStopDelayMs;
    private PendingIntent mDriverStopIntent;
    private State mDriverStoppedState = new DriverStoppedState();
    private State mDriverStoppingState = new DriverStoppingState();
    private State mDriverUnloadedState = new DriverUnloadedState();
    private State mDriverUnloadingState = new DriverUnloadingState();
    private boolean mEnableBackgroundScan = false;
    private boolean mEnableRssiPolling = false;
    private AtomicBoolean mFilteringMulticastV4Packets = new AtomicBoolean(true);
    private AtomicInteger mFrequencyBand = new AtomicInteger(0);
    private boolean mHighPerfMode = false;
    private boolean mInDelayedStop = false;
    private State mInitialState = new InitialState();
    private String mInterfaceName;
    private boolean mIsRunning = false;
    private boolean mIsScanMode = false;
    private State mL2ConnectedState = new L2ConnectedState();
    private final AtomicInteger mLastApEnableUid = new AtomicInteger(Process.myUid());
    private String mLastBssid;
    private long mLastEnableAllNetworksTime;
    private final AtomicInteger mLastEnableUid = new AtomicInteger(Process.myUid());
    private int mLastNetworkId;
    private final WorkSource mLastRunningWifiUids = new WorkSource();
    private int mLastSignalLevel = -1;
    private LinkProperties mLinkProperties;
    private NetworkInfo mNetworkInfo;
    private INetworkManagementService mNwService;
    private State mObtainingIpState = new ObtainingIpState();
    private final boolean mP2pSupported;
    private int mPeriodicScanToken = 0;
    private boolean mPowerSaveEnabled = true;
    private final String mPrimaryDeviceType;
    private int mReconnectCount = 0;
    private AsyncChannel mReplyChannel = new AsyncChannel();
    private boolean mReportedRunning = false;
    private int mRssiPollToken = 0;
    private final WorkSource mRunningWifiUids = new WorkSource();
    private PendingIntent mScanIntent;
    private State mScanModeState = new ScanModeState();
    private final LruCache<String, ScanResult> mScanResultCache;
    private boolean mScanResultIsPending = false;
    private List<ScanResult> mScanResults;
    private IntentFilter mScreenFilter;
    private BroadcastReceiver mScreenReceiver;
    private boolean mSetScanActive = false;
    private State mSoftApStartedState = new SoftApStartedState();
    private State mSoftApStartingState = new SoftApStartingState();
    private State mSoftApStoppingState = new SoftApStoppingState();
    private int mSupplicantRestartCount = 0;
    private long mSupplicantScanIntervalMs;
    private State mSupplicantStartedState = new SupplicantStartedState();
    private State mSupplicantStartingState = new SupplicantStartingState();
    private SupplicantStateTracker mSupplicantStateTracker;
    private int mSupplicantStopFailureToken = 0;
    private State mSupplicantStoppingState = new SupplicantStoppingState();
    private PowerManager.WakeLock mSuspendWakeLock;
    private String mTetherInterfaceName;
    private int mTetherToken = 0;
    private State mTetheredState = new TetheredState();
    private State mTetheringState = new TetheringState();
    private State mVerifyingLinkState = new VerifyingLinkState();
    private PowerManager.WakeLock mWakeLock;
    private AsyncChannel mWifiApConfigChannel = new AsyncChannel();
    private final AtomicInteger mWifiApState = new AtomicInteger(11);
    private WifiConfigStore mWifiConfigStore;
    private WifiInfo mWifiInfo;
    private WifiMonitor mWifiMonitor;
    private WifiNative mWifiNative;
    private AsyncChannel mWifiP2pChannel = new AsyncChannel();
    private WifiP2pManager mWifiP2pManager;
    private final AtomicInteger mWifiState = new AtomicInteger(1);
    private State mWpsRunningState = new WpsRunningState();

    public WifiStateMachine(Context paramContext, String paramString)
    {
        super("WifiStateMachine");
        this.mContext = paramContext;
        this.mInterfaceName = paramString;
        this.mNetworkInfo = new NetworkInfo(1, 0, "WIFI", "");
        this.mBatteryStats = IBatteryStats.Stub.asInterface(ServiceManager.getService("batteryinfo"));
        this.mNwService = INetworkManagementService.Stub.asInterface(ServiceManager.getService("network_management"));
        this.mP2pSupported = this.mContext.getPackageManager().hasSystemFeature("android.hardware.wifi.direct");
        this.mWifiNative = new WifiNative(this.mInterfaceName);
        this.mWifiConfigStore = new WifiConfigStore(paramContext, this.mWifiNative);
        this.mWifiMonitor = new WifiMonitor(this, this.mWifiNative);
        this.mDhcpInfoInternal = new DhcpInfoInternal();
        this.mWifiInfo = new WifiInfo();
        this.mSupplicantStateTracker = new SupplicantStateTracker(paramContext, this, this.mWifiConfigStore, getHandler());
        this.mLinkProperties = new LinkProperties();
        WifiApConfigStore localWifiApConfigStore = WifiApConfigStore.makeWifiApConfigStore(paramContext, getHandler());
        localWifiApConfigStore.loadApConfiguration();
        this.mWifiApConfigChannel.connectSync(this.mContext, getHandler(), localWifiApConfigStore.getMessenger());
        this.mNetworkInfo.setIsAvailable(false);
        this.mLinkProperties.clear();
        this.mLastBssid = null;
        this.mLastNetworkId = -1;
        this.mLastSignalLevel = -1;
        this.mAlarmManager = ((AlarmManager)this.mContext.getSystemService("alarm"));
        Intent localIntent = new Intent("com.android.server.WifiManager.action.START_SCAN", null);
        this.mScanIntent = PendingIntent.getBroadcast(this.mContext, 0, localIntent, 0);
        this.mDefaultFrameworkScanIntervalMs = this.mContext.getResources().getInteger(17694732);
        this.mDriverStopDelayMs = this.mContext.getResources().getInteger(17694733);
        this.mBackgroundScanSupported = this.mContext.getResources().getBoolean(17891344);
        this.mPrimaryDeviceType = this.mContext.getResources().getString(17039386);
        this.mContext.registerReceiver(new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                ArrayList localArrayList1 = paramAnonymousIntent.getStringArrayListExtra("availableArray");
                ArrayList localArrayList2 = paramAnonymousIntent.getStringArrayListExtra("activeArray");
                WifiStateMachine.this.sendMessage(131101, new WifiStateMachine.TetherStateChange(WifiStateMachine.this, localArrayList1, localArrayList2));
            }
        }
        , new IntentFilter("android.net.conn.TETHER_STATE_CHANGED"));
        this.mContext.registerReceiver(new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                WifiStateMachine.this.startScan(false);
            }
        }
        , new IntentFilter("com.android.server.WifiManager.action.START_SCAN"));
        this.mScreenFilter = new IntentFilter();
        this.mScreenFilter.addAction("android.intent.action.SCREEN_ON");
        this.mScreenFilter.addAction("android.intent.action.SCREEN_OFF");
        this.mScreenReceiver = new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                String str = paramAnonymousIntent.getAction();
                if (str.equals("android.intent.action.SCREEN_ON"))
                {
                    WifiStateMachine.this.enableRssiPolling(true);
                    if (WifiStateMachine.this.mBackgroundScanSupported)
                        WifiStateMachine.this.enableBackgroundScanCommand(false);
                    WifiStateMachine.this.enableAllNetworks();
                    WifiStateMachine.this.sendMessage(131159);
                }
                while (true)
                {
                    return;
                    if (str.equals("android.intent.action.SCREEN_OFF"))
                    {
                        WifiStateMachine.this.enableRssiPolling(false);
                        if (WifiStateMachine.this.mBackgroundScanSupported)
                            WifiStateMachine.this.enableBackgroundScanCommand(true);
                        WifiStateMachine.this.mSuspendWakeLock.acquire(2000L);
                        WifiStateMachine.this.sendMessage(131158);
                    }
                }
            }
        };
        this.mContext.registerReceiver(new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                int i = paramAnonymousIntent.getIntExtra("DelayedStopCounter", 0);
                WifiStateMachine.this.sendMessage(WifiStateMachine.this.obtainMessage(131090, i, 0));
            }
        }
        , new IntentFilter("com.android.server.WifiManager.action.DELAYED_DRIVER_STOP"));
        this.mScanResultCache = new LruCache(80);
        PowerManager localPowerManager = (PowerManager)this.mContext.getSystemService("power");
        this.mWakeLock = localPowerManager.newWakeLock(1, "WifiStateMachine");
        this.mSuspendWakeLock = localPowerManager.newWakeLock(1, "WifiSuspend");
        this.mSuspendWakeLock.setReferenceCounted(false);
        addState(this.mDefaultState);
        addState(this.mInitialState, this.mDefaultState);
        addState(this.mDriverUnloadingState, this.mDefaultState);
        addState(this.mDriverUnloadedState, this.mDefaultState);
        addState(this.mDriverFailedState, this.mDriverUnloadedState);
        addState(this.mDriverLoadingState, this.mDefaultState);
        addState(this.mDriverLoadedState, this.mDefaultState);
        addState(this.mSupplicantStartingState, this.mDefaultState);
        addState(this.mSupplicantStartedState, this.mDefaultState);
        addState(this.mDriverStartingState, this.mSupplicantStartedState);
        addState(this.mDriverStartedState, this.mSupplicantStartedState);
        addState(this.mScanModeState, this.mDriverStartedState);
        addState(this.mConnectModeState, this.mDriverStartedState);
        addState(this.mL2ConnectedState, this.mConnectModeState);
        addState(this.mObtainingIpState, this.mL2ConnectedState);
        addState(this.mVerifyingLinkState, this.mL2ConnectedState);
        addState(this.mConnectedState, this.mL2ConnectedState);
        addState(this.mDisconnectingState, this.mConnectModeState);
        addState(this.mDisconnectedState, this.mConnectModeState);
        addState(this.mWpsRunningState, this.mConnectModeState);
        addState(this.mDriverStoppingState, this.mSupplicantStartedState);
        addState(this.mDriverStoppedState, this.mSupplicantStartedState);
        addState(this.mSupplicantStoppingState, this.mDefaultState);
        addState(this.mSoftApStartingState, this.mDefaultState);
        addState(this.mSoftApStartedState, this.mDefaultState);
        addState(this.mTetheringState, this.mSoftApStartedState);
        addState(this.mTetheredState, this.mSoftApStartedState);
        addState(this.mSoftApStoppingState, this.mDefaultState);
        setInitialState(this.mInitialState);
        setProcessedMessagesSize(100);
        start();
    }

    private void checkAndSetConnectivityInstance()
    {
        if (this.mCm == null)
            this.mCm = ((ConnectivityManager)this.mContext.getSystemService("connectivity"));
    }

    private void configureLinkProperties()
    {
        if (this.mWifiConfigStore.isUsingStaticIp(this.mLastNetworkId))
            this.mLinkProperties = this.mWifiConfigStore.getLinkProperties(this.mLastNetworkId);
        while (true)
        {
            this.mLinkProperties.setInterfaceName(this.mInterfaceName);
            return;
            synchronized (this.mDhcpInfoInternal)
            {
                this.mLinkProperties = this.mDhcpInfoInternal.makeLinkProperties();
                this.mLinkProperties.setHttpProxy(this.mWifiConfigStore.getProxyProperties(this.mLastNetworkId));
            }
        }
    }

    private void fetchRssiAndLinkSpeedNative()
    {
        int i = -1;
        int j = -1;
        String str = this.mWifiNative.signalPoll();
        int n;
        String[] arrayOfString2;
        if (str != null)
        {
            String[] arrayOfString1 = str.split("\n");
            int m = arrayOfString1.length;
            n = 0;
            if (n < m)
            {
                arrayOfString2 = arrayOfString1[n].split("=");
                if (arrayOfString2.length >= 2);
            }
        }
        while (true)
        {
            n++;
            break;
            try
            {
                if (arrayOfString2[0].equals("RSSI"))
                {
                    i = Integer.parseInt(arrayOfString2[1]);
                }
                else if (arrayOfString2[0].equals("LINKSPEED"))
                {
                    int i1 = Integer.parseInt(arrayOfString2[1]);
                    j = i1;
                    continue;
                    if ((i != -1) && (-200 < i) && (i < 256))
                    {
                        if (i > 0)
                            i -= 256;
                        this.mWifiInfo.setRssi(i);
                        int k = WifiManager.calculateSignalLevel(i, 5);
                        if (k != this.mLastSignalLevel)
                            sendRssiChangeBroadcast(i);
                        this.mLastSignalLevel = k;
                    }
                    while (true)
                    {
                        if (j != -1)
                            this.mWifiInfo.setLinkSpeed(j);
                        return;
                        this.mWifiInfo.setRssi(-200);
                    }
                }
            }
            catch (NumberFormatException localNumberFormatException)
            {
            }
        }
    }

    private int getMaxDhcpRetries()
    {
        return Settings.Secure.getInt(this.mContext.getContentResolver(), "wifi_max_dhcp_retry_count", 9);
    }

    private NetworkInfo.DetailedState getNetworkDetailedState()
    {
        return this.mNetworkInfo.getDetailedState();
    }

    private void handleFailedIpConfiguration()
    {
        loge("IP configuration failed");
        this.mWifiInfo.setInetAddress(null);
        this.mWifiInfo.setMeteredHint(false);
        int i = getMaxDhcpRetries();
        if (i > 0)
        {
            int j = 1 + this.mReconnectCount;
            this.mReconnectCount = j;
            if (j > i)
            {
                loge("Failed " + this.mReconnectCount + " times, Disabling " + this.mLastNetworkId);
                this.mWifiConfigStore.disableNetwork(this.mLastNetworkId, 2);
                this.mReconnectCount = 0;
            }
        }
        this.mWifiNative.disconnect();
        this.mWifiNative.reconnect();
    }

    private void handleNetworkDisconnect()
    {
        if (this.mDhcpStateMachine != null)
        {
            handlePostDhcpSetup();
            this.mDhcpStateMachine.sendMessage(196610);
            this.mDhcpStateMachine.quit();
            this.mDhcpStateMachine = null;
        }
        try
        {
            this.mNwService.clearInterfaceAddresses(this.mInterfaceName);
            this.mNwService.disableIpv6(this.mInterfaceName);
            this.mWifiInfo.setInetAddress(null);
            this.mWifiInfo.setBSSID(null);
            this.mWifiInfo.setSSID(null);
            this.mWifiInfo.setNetworkId(-1);
            this.mWifiInfo.setRssi(-200);
            this.mWifiInfo.setLinkSpeed(-1);
            this.mWifiInfo.setMeteredHint(false);
            setNetworkDetailedState(NetworkInfo.DetailedState.DISCONNECTED);
            this.mWifiConfigStore.updateStatus(this.mLastNetworkId, NetworkInfo.DetailedState.DISCONNECTED);
            sendNetworkStateChangeBroadcast(this.mLastBssid);
            this.mLinkProperties.clear();
            if (!this.mWifiConfigStore.isUsingStaticIp(this.mLastNetworkId))
                this.mWifiConfigStore.clearIpConfiguration(this.mLastNetworkId);
            this.mLastBssid = null;
            this.mLastNetworkId = -1;
            return;
        }
        catch (Exception localException)
        {
            while (true)
                loge("Failed to clear addresses or disable ipv6" + localException);
        }
    }

    private void handleSuccessfulIpConfiguration(DhcpInfoInternal paramDhcpInfoInternal)
    {
        while (true)
        {
            synchronized (this.mDhcpInfoInternal)
            {
                this.mDhcpInfoInternal = paramDhcpInfoInternal;
                this.mLastSignalLevel = -1;
                this.mReconnectCount = 0;
                this.mWifiConfigStore.setIpConfiguration(this.mLastNetworkId, paramDhcpInfoInternal);
                InetAddress localInetAddress = NetworkUtils.numericToInetAddress(paramDhcpInfoInternal.ipAddress);
                this.mWifiInfo.setInetAddress(localInetAddress);
                this.mWifiInfo.setMeteredHint(paramDhcpInfoInternal.hasMeteredHint());
                if (getNetworkDetailedState() == NetworkInfo.DetailedState.CONNECTED)
                {
                    LinkProperties localLinkProperties = paramDhcpInfoInternal.makeLinkProperties();
                    localLinkProperties.setHttpProxy(this.mWifiConfigStore.getProxyProperties(this.mLastNetworkId));
                    localLinkProperties.setInterfaceName(this.mInterfaceName);
                    if (!localLinkProperties.equals(this.mLinkProperties))
                    {
                        this.mLinkProperties = localLinkProperties;
                        sendLinkConfigurationChangedBroadcast();
                    }
                    return;
                }
            }
            configureLinkProperties();
        }
    }

    private SupplicantState handleSupplicantStateChange(Message paramMessage)
    {
        StateChangeResult localStateChangeResult = (StateChangeResult)paramMessage.obj;
        SupplicantState localSupplicantState = localStateChangeResult.state;
        EventLog.writeEvent(50023, localSupplicantState.ordinal());
        this.mWifiInfo.setSupplicantState(localSupplicantState);
        if (SupplicantState.isConnecting(localSupplicantState))
            this.mWifiInfo.setNetworkId(localStateChangeResult.networkId);
        while (true)
        {
            if (localSupplicantState == SupplicantState.ASSOCIATING)
                this.mWifiInfo.setBSSID(localStateChangeResult.BSSID);
            this.mWifiInfo.setSSID(localStateChangeResult.SSID);
            this.mSupplicantStateTracker.sendMessage(Message.obtain(paramMessage));
            return localSupplicantState;
            this.mWifiInfo.setNetworkId(-1);
        }
    }

    private boolean isWifiTethered(ArrayList<String> paramArrayList)
    {
        checkAndSetConnectivityInstance();
        String[] arrayOfString = this.mCm.getTetherableWifiRegexs();
        Iterator localIterator = paramArrayList.iterator();
        int j;
        if (localIterator.hasNext())
        {
            String str = (String)localIterator.next();
            int i = arrayOfString.length;
            j = 0;
            label44: if (j < i)
                if (!str.matches(arrayOfString[j]));
        }
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            j++;
            break label44;
            break;
        }
    }

    private void log(String paramString)
    {
        Log.d("WifiStateMachine", paramString);
    }

    private void loge(String paramString)
    {
        Log.e("WifiStateMachine", paramString);
    }

    private Message obtainMessageWithArg2(Message paramMessage)
    {
        Message localMessage = Message.obtain();
        localMessage.arg2 = paramMessage.arg2;
        return localMessage;
    }

    private ScanResult parseScanResult(String paramString)
    {
        Object localObject1 = null;
        if (paramString != null);
        synchronized (this.mScanResultCache)
        {
            String[] arrayOfString = scanResultPattern.split(paramString);
            String str1;
            if ((3 <= arrayOfString.length) && (arrayOfString.length <= 5))
                str1 = arrayOfString[0];
            try
            {
                i = Integer.parseInt(arrayOfString[1]);
                int k = Integer.parseInt(arrayOfString[2]);
                j = k;
                if (j > 0)
                    j -= 256;
                String str2;
                String str3;
                if (arrayOfString.length == 4)
                    if (arrayOfString[3].charAt(0) == '[')
                    {
                        str2 = arrayOfString[3];
                        str3 = "";
                    }
                while (true)
                {
                    String str4 = str1 + str3;
                    ScanResult localScanResult = (ScanResult)this.mScanResultCache.get(str4);
                    if (localScanResult != null);
                    try
                    {
                        localScanResult.level = j;
                        localScanResult.SSID = str3;
                        localScanResult.capabilities = str2;
                        localScanResult.frequency = i;
                        localObject1 = localScanResult;
                        while (true)
                        {
                            break label330;
                            str2 = "";
                            str3 = arrayOfString[3];
                            break;
                            if (arrayOfString.length == 5)
                            {
                                str2 = arrayOfString[3];
                                str3 = arrayOfString[4];
                                break;
                            }
                            str2 = "";
                            str3 = "";
                            break;
                            if (str3.trim().length() <= 0)
                                break label324;
                            localObject1 = new ScanResult(str3, str1, str2, j, i);
                            this.mScanResultCache.put(str4, localObject1);
                            continue;
                            Object localObject2;
                            throw localObject2;
                            loge("Misformatted scan result text with " + arrayOfString.length + " fields: " + paramString);
                        }
                    }
                    finally
                    {
                        while (true)
                        {
                            continue;
                            label324: localObject1 = localScanResult;
                        }
                    }
                }
                label330: return localObject1;
            }
            catch (NumberFormatException localNumberFormatException)
            {
                while (true)
                {
                    int i = 0;
                    int j = 0;
                }
            }
        }
    }

    private void replyToMessage(Message paramMessage, int paramInt)
    {
        if (paramMessage.replyTo == null);
        while (true)
        {
            return;
            Message localMessage = obtainMessageWithArg2(paramMessage);
            localMessage.what = paramInt;
            this.mReplyChannel.replyToMessage(paramMessage, localMessage);
        }
    }

    private void replyToMessage(Message paramMessage, int paramInt1, int paramInt2)
    {
        if (paramMessage.replyTo == null);
        while (true)
        {
            return;
            Message localMessage = obtainMessageWithArg2(paramMessage);
            localMessage.what = paramInt1;
            localMessage.arg1 = paramInt2;
            this.mReplyChannel.replyToMessage(paramMessage, localMessage);
        }
    }

    private void replyToMessage(Message paramMessage, int paramInt, Object paramObject)
    {
        if (paramMessage.replyTo == null);
        while (true)
        {
            return;
            Message localMessage = obtainMessageWithArg2(paramMessage);
            localMessage.what = paramInt;
            localMessage.obj = paramObject;
            this.mReplyChannel.replyToMessage(paramMessage, localMessage);
        }
    }

    private void sendLinkConfigurationChangedBroadcast()
    {
        Intent localIntent = new Intent("android.net.wifi.LINK_CONFIGURATION_CHANGED");
        localIntent.addFlags(134217728);
        localIntent.putExtra("linkProperties", new LinkProperties(this.mLinkProperties));
        this.mContext.sendBroadcast(localIntent);
    }

    private void sendNetworkStateChangeBroadcast(String paramString)
    {
        Intent localIntent = new Intent("android.net.wifi.STATE_CHANGE");
        localIntent.addFlags(134217728);
        localIntent.putExtra("networkInfo", new NetworkInfo(this.mNetworkInfo));
        localIntent.putExtra("linkProperties", new LinkProperties(this.mLinkProperties));
        if (paramString != null)
            localIntent.putExtra("bssid", paramString);
        if ((this.mNetworkInfo.getDetailedState() == NetworkInfo.DetailedState.VERIFYING_POOR_LINK) || (this.mNetworkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED))
            localIntent.putExtra("wifiInfo", new WifiInfo(this.mWifiInfo));
        this.mContext.sendStickyBroadcast(localIntent);
    }

    private void sendRssiChangeBroadcast(int paramInt)
    {
        Intent localIntent = new Intent("android.net.wifi.RSSI_CHANGED");
        localIntent.addFlags(134217728);
        localIntent.putExtra("newRssi", paramInt);
        this.mContext.sendBroadcast(localIntent);
    }

    private void sendScanResultsAvailableBroadcast()
    {
        Intent localIntent = new Intent("android.net.wifi.SCAN_RESULTS");
        localIntent.addFlags(134217728);
        this.mContext.sendBroadcast(localIntent);
    }

    private void sendSupplicantConnectionChangedBroadcast(boolean paramBoolean)
    {
        Intent localIntent = new Intent("android.net.wifi.supplicant.CONNECTION_CHANGE");
        localIntent.addFlags(134217728);
        localIntent.putExtra("connected", paramBoolean);
        this.mContext.sendBroadcast(localIntent);
    }

    private void setCountryCode()
    {
        String str = Settings.Secure.getString(this.mContext.getContentResolver(), "wifi_country_code");
        if ((str != null) && (!str.isEmpty()))
            setCountryCode(str, false);
    }

    private void setFrequencyBand()
    {
        setFrequencyBand(Settings.Secure.getInt(this.mContext.getContentResolver(), "wifi_frequency_band", 0), false);
    }

    private void setNetworkDetailedState(NetworkInfo.DetailedState paramDetailedState)
    {
        if (paramDetailedState != this.mNetworkInfo.getDetailedState())
            this.mNetworkInfo.setDetailedState(paramDetailedState, null, null);
    }

    private void setScanResults(String paramString)
    {
        if (paramString == null);
        while (true)
        {
            return;
            ArrayList localArrayList = new ArrayList();
            int i = 0;
            int j = paramString.length();
            int k = 0;
            int m = 0;
            if (m <= j)
            {
                if ((m == j) || (paramString.charAt(m) == '\n'))
                {
                    i++;
                    if (i != 1)
                        break label72;
                }
                for (k = m + 1; ; k = m + 1)
                {
                    m++;
                    break;
                    label72: if (m > k)
                    {
                        ScanResult localScanResult = parseScanResult(paramString.substring(k, m));
                        if (localScanResult != null)
                            localArrayList.add(localScanResult);
                    }
                }
            }
            this.mScanResults = localArrayList;
        }
    }

    private void setWifiApState(int paramInt)
    {
        int i = this.mWifiApState.get();
        if (paramInt == 13);
        try
        {
            this.mBatteryStats.noteWifiOn();
            while (true)
            {
                this.mWifiApState.set(paramInt);
                Intent localIntent = new Intent("android.net.wifi.WIFI_AP_STATE_CHANGED");
                localIntent.addFlags(134217728);
                localIntent.putExtra("wifi_state", paramInt);
                localIntent.putExtra("previous_wifi_state", i);
                this.mContext.sendStickyBroadcast(localIntent);
                return;
                if (paramInt == 11)
                    this.mBatteryStats.noteWifiOff();
            }
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                loge("Failed to note battery stats in wifi");
        }
    }

    private void setWifiState(int paramInt)
    {
        int i = this.mWifiState.get();
        if (paramInt == 3);
        try
        {
            this.mBatteryStats.noteWifiOn();
            while (true)
            {
                this.mWifiState.set(paramInt);
                Intent localIntent = new Intent("android.net.wifi.WIFI_STATE_CHANGED");
                localIntent.addFlags(134217728);
                localIntent.putExtra("wifi_state", paramInt);
                localIntent.putExtra("previous_wifi_state", i);
                this.mContext.sendStickyBroadcast(localIntent);
                return;
                if (paramInt == 1)
                    this.mBatteryStats.noteWifiOff();
            }
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                loge("Failed to note battery stats in wifi");
        }
    }

    private void startSoftApWithConfig(final WifiConfiguration paramWifiConfiguration)
    {
        new Thread(new Runnable()
        {
            public void run()
            {
                try
                {
                    WifiStateMachine.this.mNwService.startAccessPoint(paramWifiConfiguration, WifiStateMachine.this.mInterfaceName, "wl0.1");
                    WifiStateMachine.this.sendMessage(131094);
                    return;
                }
                catch (Exception localException1)
                {
                    while (true)
                    {
                        WifiStateMachine.this.loge("Exception in softap start " + localException1);
                        try
                        {
                            WifiStateMachine.this.mNwService.stopAccessPoint(WifiStateMachine.this.mInterfaceName);
                            WifiStateMachine.this.mNwService.startAccessPoint(paramWifiConfiguration, WifiStateMachine.this.mInterfaceName, "wl0.1");
                        }
                        catch (Exception localException2)
                        {
                            WifiStateMachine.this.loge("Exception in softap re-start " + localException2);
                            WifiStateMachine.this.sendMessage(131095);
                        }
                    }
                }
            }
        }).start();
    }

    private boolean startTethering(ArrayList<String> paramArrayList)
    {
        boolean bool = false;
        checkAndSetConnectivityInstance();
        String[] arrayOfString = this.mCm.getTetherableWifiRegexs();
        Iterator localIterator = paramArrayList.iterator();
        while (true)
        {
            String str;
            int i;
            if (localIterator.hasNext())
            {
                str = (String)localIterator.next();
                i = arrayOfString.length;
            }
            for (int j = 0; j < i; j++)
                if (str.matches(arrayOfString[j]))
                    try
                    {
                        InterfaceConfiguration localInterfaceConfiguration = this.mNwService.getInterfaceConfig(str);
                        if (localInterfaceConfiguration != null)
                        {
                            localInterfaceConfiguration.setLinkAddress(new LinkAddress(NetworkUtils.numericToInetAddress("192.168.43.1"), 24));
                            localInterfaceConfiguration.setInterfaceUp();
                            this.mNwService.setInterfaceConfig(str, localInterfaceConfiguration);
                        }
                        if (this.mCm.tether(str) != 0)
                        {
                            loge("Error tethering on " + str);
                            return bool;
                        }
                    }
                    catch (Exception localException)
                    {
                        while (true)
                        {
                            loge("Error configuring interface " + str + ", :" + localException);
                            continue;
                            this.mTetherInterfaceName = str;
                            bool = true;
                        }
                    }
        }
    }

    private void stopTethering()
    {
        checkAndSetConnectivityInstance();
        try
        {
            InterfaceConfiguration localInterfaceConfiguration = this.mNwService.getInterfaceConfig(this.mInterfaceName);
            if (localInterfaceConfiguration != null)
            {
                localInterfaceConfiguration.setLinkAddress(new LinkAddress(NetworkUtils.numericToInetAddress("0.0.0.0"), 0));
                this.mNwService.setInterfaceConfig(this.mInterfaceName, localInterfaceConfiguration);
            }
            if (this.mCm.untether(this.mTetherInterfaceName) != 0)
                loge("Untether initiate failed!");
            return;
        }
        catch (Exception localException)
        {
            while (true)
                loge("Error resetting interface " + this.mInterfaceName + ", :" + localException);
        }
    }

    public void addToBlacklist(String paramString)
    {
        sendMessage(obtainMessage(131128, paramString));
    }

    public void clearBlacklist()
    {
        sendMessage(obtainMessage(131129));
    }

    public void disconnectCommand()
    {
        sendMessage(131146);
    }

    public void enableAllNetworks()
    {
        sendMessage(131127);
    }

    public void enableBackgroundScanCommand(boolean paramBoolean)
    {
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            sendMessage(obtainMessage(131163, i, 0));
            return;
        }
    }

    public void enableRssiPolling(boolean paramBoolean)
    {
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            sendMessage(obtainMessage(131154, i, 0));
            return;
        }
    }

    public String getConfigFile()
    {
        return this.mWifiConfigStore.getConfigFile();
    }

    public int getFrequencyBand()
    {
        return this.mFrequencyBand.get();
    }

    public Messenger getMessenger()
    {
        return new Messenger(getHandler());
    }

    void handlePostDhcpSetup()
    {
        this.mPowerSaveEnabled = true;
        this.mWifiNative.setPowerSave(this.mPowerSaveEnabled);
        WifiNative localWifiNative = this.mWifiNative;
        localWifiNative.setBluetoothCoexistenceMode(2);
    }

    void handlePreDhcpSetup()
    {
        if (!this.mBluetoothConnectionActive)
        {
            WifiNative localWifiNative = this.mWifiNative;
            localWifiNative.setBluetoothCoexistenceMode(1);
        }
        if (this.mPowerSaveEnabled)
        {
            this.mPowerSaveEnabled = false;
            this.mWifiNative.setPowerSave(this.mPowerSaveEnabled);
        }
    }

    public void reassociateCommand()
    {
        sendMessage(131148);
    }

    public void reconnectCommand()
    {
        sendMessage(131147);
    }

    protected boolean recordProcessedMessage(Message paramMessage)
    {
        boolean bool = false;
        if ((getCurrentState() == this.mConnectedState) || (getCurrentState() == this.mDisconnectedState));
        switch (paramMessage.what)
        {
        default:
            switch (paramMessage.what)
            {
            default:
                bool = true;
            case 131090:
            case 131143:
            case 131154:
            case 131155:
            case 135191:
            case 147461:
            }
            break;
        case 131073:
        case 131083:
        case 131085:
        case 131127:
        case 131144:
        case 131149:
        case 131158:
        case 131159:
        case 131163:
        }
        return bool;
    }

    public void sendBluetoothAdapterStateChange(int paramInt)
    {
        sendMessage(obtainMessage(131103, paramInt, 0));
    }

    public void setCountryCode(String paramString, boolean paramBoolean)
    {
        if (paramBoolean)
            Settings.Secure.putString(this.mContext.getContentResolver(), "wifi_country_code", paramString);
        sendMessage(obtainMessage(131152, paramString));
    }

    public void setDriverStart(boolean paramBoolean1, boolean paramBoolean2)
    {
        if (paramBoolean1)
        {
            sendMessage(131085);
            return;
        }
        if (paramBoolean2);
        for (int i = 1; ; i = 0)
        {
            sendMessage(obtainMessage(131086, i, 0));
            break;
        }
    }

    public void setFrequencyBand(int paramInt, boolean paramBoolean)
    {
        if (paramBoolean)
            Settings.Secure.putInt(this.mContext.getContentResolver(), "wifi_frequency_band", paramInt);
        sendMessage(obtainMessage(131162, paramInt, 0));
    }

    public void setHighPerfModeEnabled(boolean paramBoolean)
    {
        if (paramBoolean);
        for (int i = 1; ; i = 0)
        {
            sendMessage(obtainMessage(131149, i, 0));
            return;
        }
    }

    public void setScanOnlyMode(boolean paramBoolean)
    {
        if (paramBoolean)
            sendMessage(obtainMessage(131144, 2, 0));
        while (true)
        {
            return;
            sendMessage(obtainMessage(131144, 1, 0));
        }
    }

    public void setScanType(boolean paramBoolean)
    {
        if (paramBoolean)
            sendMessage(obtainMessage(131145, 1, 0));
        while (true)
        {
            return;
            sendMessage(obtainMessage(131145, 2, 0));
        }
    }

    public void setWifiApConfiguration(WifiConfiguration paramWifiConfiguration)
    {
        this.mWifiApConfigChannel.sendMessage(131097, paramWifiConfiguration);
    }

    public void setWifiApEnabled(WifiConfiguration paramWifiConfiguration, boolean paramBoolean)
    {
        this.mLastApEnableUid.set(Binder.getCallingUid());
        if (paramBoolean)
        {
            sendMessage(obtainMessage(131073, 12, 0));
            sendMessage(obtainMessage(131093, paramWifiConfiguration));
        }
        while (true)
        {
            return;
            sendMessage(131096);
            sendMessage(obtainMessage(131074, 11, 0));
        }
    }

    public void setWifiEnabled(boolean paramBoolean)
    {
        this.mLastEnableUid.set(Binder.getCallingUid());
        if (paramBoolean)
        {
            sendMessage(obtainMessage(131073, 2, 0));
            sendMessage(131083);
        }
        while (true)
        {
            return;
            sendMessage(131084);
            sendMessage(obtainMessage(131074, 1, 0));
        }
    }

    public void startFilteringMulticastV4Packets()
    {
        this.mFilteringMulticastV4Packets.set(true);
        sendMessage(obtainMessage(131156, 0, 0));
    }

    public void startFilteringMulticastV6Packets()
    {
        sendMessage(obtainMessage(131156, 1, 0));
    }

    public void startScan(boolean paramBoolean)
    {
        if (paramBoolean);
        for (int i = 1; ; i = 2)
        {
            sendMessage(obtainMessage(131143, i, 0));
            return;
        }
    }

    public void stopFilteringMulticastV4Packets()
    {
        this.mFilteringMulticastV4Packets.set(false);
        sendMessage(obtainMessage(131157, 0, 0));
    }

    public void stopFilteringMulticastV6Packets()
    {
        sendMessage(obtainMessage(131157, 1, 0));
    }

    public int syncAddOrUpdateNetwork(AsyncChannel paramAsyncChannel, WifiConfiguration paramWifiConfiguration)
    {
        Message localMessage = paramAsyncChannel.sendMessageSynchronously(131124, paramWifiConfiguration);
        int i = localMessage.arg1;
        localMessage.recycle();
        return i;
    }

    public boolean syncDisableNetwork(AsyncChannel paramAsyncChannel, int paramInt)
    {
        Message localMessage = paramAsyncChannel.sendMessageSynchronously(151569, paramInt);
        if (localMessage.arg1 != 151570);
        for (boolean bool = true; ; bool = false)
        {
            localMessage.recycle();
            return bool;
        }
    }

    public boolean syncEnableNetwork(AsyncChannel paramAsyncChannel, int paramInt, boolean paramBoolean)
    {
        int i;
        Message localMessage;
        if (paramBoolean)
        {
            i = 1;
            localMessage = paramAsyncChannel.sendMessageSynchronously(131126, paramInt, i);
            if (localMessage.arg1 == -1)
                break label45;
        }
        label45: for (boolean bool = true; ; bool = false)
        {
            localMessage.recycle();
            return bool;
            i = 0;
            break;
        }
    }

    public List<WifiConfiguration> syncGetConfiguredNetworks(AsyncChannel paramAsyncChannel)
    {
        Message localMessage = paramAsyncChannel.sendMessageSynchronously(131131);
        List localList = (List)localMessage.obj;
        localMessage.recycle();
        return localList;
    }

    public DhcpInfo syncGetDhcpInfo()
    {
        synchronized (this.mDhcpInfoInternal)
        {
            DhcpInfo localDhcpInfo = this.mDhcpInfoInternal.makeDhcpInfo();
            return localDhcpInfo;
        }
    }

    public List<ScanResult> syncGetScanResultsList()
    {
        return this.mScanResults;
    }

    public WifiConfiguration syncGetWifiApConfiguration()
    {
        Message localMessage = this.mWifiApConfigChannel.sendMessageSynchronously(131099);
        WifiConfiguration localWifiConfiguration = (WifiConfiguration)localMessage.obj;
        localMessage.recycle();
        return localWifiConfiguration;
    }

    public int syncGetWifiApState()
    {
        return this.mWifiApState.get();
    }

    public String syncGetWifiApStateByName()
    {
        String str;
        switch (this.mWifiApState.get())
        {
        default:
            str = "[invalid state]";
        case 10:
        case 11:
        case 12:
        case 13:
        case 14:
        }
        while (true)
        {
            return str;
            str = "disabling";
            continue;
            str = "disabled";
            continue;
            str = "enabling";
            continue;
            str = "enabled";
            continue;
            str = "failed";
        }
    }

    public int syncGetWifiState()
    {
        return this.mWifiState.get();
    }

    public String syncGetWifiStateByName()
    {
        String str;
        switch (this.mWifiState.get())
        {
        default:
            str = "[invalid state]";
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        }
        while (true)
        {
            return str;
            str = "disabling";
            continue;
            str = "disabled";
            continue;
            str = "enabling";
            continue;
            str = "enabled";
            continue;
            str = "unknown state";
        }
    }

    public boolean syncPingSupplicant(AsyncChannel paramAsyncChannel)
    {
        Message localMessage = paramAsyncChannel.sendMessageSynchronously(131123);
        if (localMessage.arg1 != -1);
        for (boolean bool = true; ; bool = false)
        {
            localMessage.recycle();
            return bool;
        }
    }

    public boolean syncRemoveNetwork(AsyncChannel paramAsyncChannel, int paramInt)
    {
        Message localMessage = paramAsyncChannel.sendMessageSynchronously(131125, paramInt);
        if (localMessage.arg1 != -1);
        for (boolean bool = true; ; bool = false)
        {
            localMessage.recycle();
            return bool;
        }
    }

    public WifiInfo syncRequestConnectionInfo()
    {
        return this.mWifiInfo;
    }

    public boolean syncSaveConfig(AsyncChannel paramAsyncChannel)
    {
        Message localMessage = paramAsyncChannel.sendMessageSynchronously(131130);
        if (localMessage.arg1 != -1);
        for (boolean bool = true; ; bool = false)
        {
            localMessage.recycle();
            return bool;
        }
    }

    public String toString()
    {
        StringBuffer localStringBuffer = new StringBuffer();
        String str = System.getProperty("line.separator");
        localStringBuffer.append("current HSM state: ").append(getCurrentState().getName()).append(str);
        localStringBuffer.append("mLinkProperties ").append(this.mLinkProperties).append(str);
        localStringBuffer.append("mWifiInfo ").append(this.mWifiInfo).append(str);
        localStringBuffer.append("mDhcpInfoInternal ").append(this.mDhcpInfoInternal).append(str);
        localStringBuffer.append("mNetworkInfo ").append(this.mNetworkInfo).append(str);
        localStringBuffer.append("mLastSignalLevel ").append(this.mLastSignalLevel).append(str);
        localStringBuffer.append("mLastBssid ").append(this.mLastBssid).append(str);
        localStringBuffer.append("mLastNetworkId ").append(this.mLastNetworkId).append(str);
        localStringBuffer.append("mReconnectCount ").append(this.mReconnectCount).append(str);
        localStringBuffer.append("mIsScanMode ").append(this.mIsScanMode).append(str);
        localStringBuffer.append("Supplicant status").append(str).append(this.mWifiNative.status()).append(str).append(str);
        localStringBuffer.append(this.mWifiConfigStore.dump());
        return localStringBuffer.toString();
    }

    // ERROR //
    public void updateBatteryWorkSource(WorkSource paramWorkSource)
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 584	android/net/wifi/WifiStateMachine:mRunningWifiUids	Landroid/os/WorkSource;
        //     4: astore_2
        //     5: aload_2
        //     6: monitorenter
        //     7: aload_1
        //     8: ifnull +11 -> 19
        //     11: aload_0
        //     12: getfield 584	android/net/wifi/WifiStateMachine:mRunningWifiUids	Landroid/os/WorkSource;
        //     15: aload_1
        //     16: invokevirtual 1890	android/os/WorkSource:set	(Landroid/os/WorkSource;)V
        //     19: aload_0
        //     20: getfield 577	android/net/wifi/WifiStateMachine:mIsRunning	Z
        //     23: ifeq +95 -> 118
        //     26: aload_0
        //     27: getfield 579	android/net/wifi/WifiStateMachine:mReportedRunning	Z
        //     30: ifeq +56 -> 86
        //     33: aload_0
        //     34: getfield 586	android/net/wifi/WifiStateMachine:mLastRunningWifiUids	Landroid/os/WorkSource;
        //     37: aload_0
        //     38: getfield 584	android/net/wifi/WifiStateMachine:mRunningWifiUids	Landroid/os/WorkSource;
        //     41: invokevirtual 1894	android/os/WorkSource:diff	(Landroid/os/WorkSource;)Z
        //     44: ifeq +31 -> 75
        //     47: aload_0
        //     48: getfield 615	android/net/wifi/WifiStateMachine:mBatteryStats	Lcom/android/internal/app/IBatteryStats;
        //     51: aload_0
        //     52: getfield 586	android/net/wifi/WifiStateMachine:mLastRunningWifiUids	Landroid/os/WorkSource;
        //     55: aload_0
        //     56: getfield 584	android/net/wifi/WifiStateMachine:mRunningWifiUids	Landroid/os/WorkSource;
        //     59: invokeinterface 1898 3 0
        //     64: aload_0
        //     65: getfield 586	android/net/wifi/WifiStateMachine:mLastRunningWifiUids	Landroid/os/WorkSource;
        //     68: aload_0
        //     69: getfield 584	android/net/wifi/WifiStateMachine:mRunningWifiUids	Landroid/os/WorkSource;
        //     72: invokevirtual 1890	android/os/WorkSource:set	(Landroid/os/WorkSource;)V
        //     75: aload_0
        //     76: getfield 805	android/net/wifi/WifiStateMachine:mWakeLock	Landroid/os/PowerManager$WakeLock;
        //     79: aload_1
        //     80: invokevirtual 1901	android/os/PowerManager$WakeLock:setWorkSource	(Landroid/os/WorkSource;)V
        //     83: aload_2
        //     84: monitorexit
        //     85: return
        //     86: aload_0
        //     87: getfield 615	android/net/wifi/WifiStateMachine:mBatteryStats	Lcom/android/internal/app/IBatteryStats;
        //     90: aload_0
        //     91: getfield 584	android/net/wifi/WifiStateMachine:mRunningWifiUids	Landroid/os/WorkSource;
        //     94: invokeinterface 1904 2 0
        //     99: aload_0
        //     100: getfield 586	android/net/wifi/WifiStateMachine:mLastRunningWifiUids	Landroid/os/WorkSource;
        //     103: aload_0
        //     104: getfield 584	android/net/wifi/WifiStateMachine:mRunningWifiUids	Landroid/os/WorkSource;
        //     107: invokevirtual 1890	android/os/WorkSource:set	(Landroid/os/WorkSource;)V
        //     110: aload_0
        //     111: iconst_1
        //     112: putfield 579	android/net/wifi/WifiStateMachine:mReportedRunning	Z
        //     115: goto -40 -> 75
        //     118: aload_0
        //     119: getfield 579	android/net/wifi/WifiStateMachine:mReportedRunning	Z
        //     122: ifeq -47 -> 75
        //     125: aload_0
        //     126: getfield 615	android/net/wifi/WifiStateMachine:mBatteryStats	Lcom/android/internal/app/IBatteryStats;
        //     129: aload_0
        //     130: getfield 586	android/net/wifi/WifiStateMachine:mLastRunningWifiUids	Landroid/os/WorkSource;
        //     133: invokeinterface 1907 2 0
        //     138: aload_0
        //     139: getfield 586	android/net/wifi/WifiStateMachine:mLastRunningWifiUids	Landroid/os/WorkSource;
        //     142: invokevirtual 1908	android/os/WorkSource:clear	()V
        //     145: aload_0
        //     146: iconst_0
        //     147: putfield 579	android/net/wifi/WifiStateMachine:mReportedRunning	Z
        //     150: goto -75 -> 75
        //     153: astore_3
        //     154: aload_2
        //     155: monitorexit
        //     156: aload_3
        //     157: athrow
        //     158: astore 4
        //     160: goto -77 -> 83
        //
        // Exception table:
        //     from	to	target	type
        //     11	83	153	finally
        //     83	85	153	finally
        //     86	150	153	finally
        //     154	156	153	finally
        //     11	83	158	android/os/RemoteException
        //     86	150	158	android/os/RemoteException
    }

    class SoftApStoppingState extends State
    {
        SoftApStoppingState()
        {
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
            WifiStateMachine.this.sendMessageDelayed(WifiStateMachine.this.obtainMessage(131102, WifiStateMachine.access$16304(WifiStateMachine.this), 0), 5000L);
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool;
            switch (paramMessage.what)
            {
            default:
                bool = false;
                return bool;
            case 131101:
                WifiStateMachine.TetherStateChange localTetherStateChange = (WifiStateMachine.TetherStateChange)paramMessage.obj;
                if (!WifiStateMachine.this.isWifiTethered(localTetherStateChange.active))
                    break;
            case 131102:
            case 131073:
            case 131074:
            case 131083:
            case 131084:
            case 131085:
            case 131086:
            case 131093:
            case 131096:
            case 131144:
            case 131145:
            case 131152:
            case 131156:
            case 131157:
            case 131162:
            }
            while (true)
            {
                bool = true;
                break;
                try
                {
                    WifiStateMachine.this.mNwService.stopAccessPoint(WifiStateMachine.this.mInterfaceName);
                    WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDriverLoadedState);
                }
                catch (Exception localException2)
                {
                    while (true)
                        WifiStateMachine.this.loge("Exception in stopAccessPoint()");
                }
                if (paramMessage.arg1 == WifiStateMachine.this.mTetherToken)
                {
                    WifiStateMachine.this.loge("Failed to get tether update, force stop access point");
                    try
                    {
                        WifiStateMachine.this.mNwService.stopAccessPoint(WifiStateMachine.this.mInterfaceName);
                        WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDriverLoadedState);
                    }
                    catch (Exception localException1)
                    {
                        while (true)
                            WifiStateMachine.this.loge("Exception in stopAccessPoint()");
                    }
                    WifiStateMachine.this.deferMessage(paramMessage);
                }
            }
        }
    }

    class TetheredState extends State
    {
        TetheredState()
        {
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool = true;
            switch (paramMessage.what)
            {
            default:
                bool = false;
            case 131101:
            case 131096:
            }
            while (true)
            {
                return bool;
                WifiStateMachine.TetherStateChange localTetherStateChange = (WifiStateMachine.TetherStateChange)paramMessage.obj;
                if (!WifiStateMachine.this.isWifiTethered(localTetherStateChange.active))
                {
                    WifiStateMachine.this.loge("Tethering reports wifi as untethered!, shut down soft Ap");
                    WifiStateMachine.this.setWifiApEnabled(null, false);
                    continue;
                    WifiStateMachine.this.setWifiApState(10);
                    WifiStateMachine.this.stopTethering();
                    WifiStateMachine.this.transitionTo(WifiStateMachine.this.mSoftApStoppingState);
                }
            }
        }
    }

    class TetheringState extends State
    {
        TetheringState()
        {
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
            WifiStateMachine.this.sendMessageDelayed(WifiStateMachine.this.obtainMessage(131102, WifiStateMachine.access$16304(WifiStateMachine.this), 0), 5000L);
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool = true;
            switch (paramMessage.what)
            {
            default:
                bool = false;
            case 131101:
            case 131102:
            case 131073:
            case 131074:
            case 131083:
            case 131084:
            case 131085:
            case 131086:
            case 131093:
            case 131096:
            case 131144:
            case 131145:
            case 131152:
            case 131156:
            case 131157:
            case 131162:
            }
            while (true)
            {
                return bool;
                WifiStateMachine.TetherStateChange localTetherStateChange = (WifiStateMachine.TetherStateChange)paramMessage.obj;
                if (WifiStateMachine.this.isWifiTethered(localTetherStateChange.active))
                {
                    WifiStateMachine.this.transitionTo(WifiStateMachine.this.mTetheredState);
                    continue;
                    if (paramMessage.arg1 == WifiStateMachine.this.mTetherToken)
                    {
                        WifiStateMachine.this.loge("Failed to get tether update, shutdown soft access point");
                        WifiStateMachine.this.setWifiApEnabled(null, false);
                        continue;
                        WifiStateMachine.this.deferMessage(paramMessage);
                    }
                }
            }
        }
    }

    class SoftApStartedState extends State
    {
        SoftApStartedState()
        {
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool;
            switch (paramMessage.what)
            {
            default:
                bool = false;
            case 131096:
            case 131093:
            case 131083:
            case 131101:
            }
            while (true)
            {
                return bool;
                WifiStateMachine.this.setWifiApState(10);
                try
                {
                    WifiStateMachine.this.mNwService.stopAccessPoint(WifiStateMachine.this.mInterfaceName);
                    WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDriverLoadedState);
                    bool = true;
                }
                catch (Exception localException)
                {
                    while (true)
                    {
                        WifiStateMachine.this.loge("Exception in stopAccessPoint()");
                        continue;
                        WifiStateMachine.this.loge("Cannot start supplicant with a running soft AP");
                        WifiStateMachine.this.setWifiState(4);
                        continue;
                        WifiStateMachine.TetherStateChange localTetherStateChange = (WifiStateMachine.TetherStateChange)paramMessage.obj;
                        if (WifiStateMachine.this.startTethering(localTetherStateChange.available))
                            WifiStateMachine.this.transitionTo(WifiStateMachine.this.mTetheringState);
                    }
                }
            }
        }
    }

    class SoftApStartingState extends State
    {
        SoftApStartingState()
        {
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
            Message localMessage = WifiStateMachine.this.getCurrentMessage();
            if (localMessage.what == 131093)
            {
                WifiConfiguration localWifiConfiguration = (WifiConfiguration)localMessage.obj;
                if (localWifiConfiguration == null)
                    WifiStateMachine.this.mWifiApConfigChannel.sendMessage(131099);
                while (true)
                {
                    return;
                    WifiStateMachine.this.mWifiApConfigChannel.sendMessage(131097, localWifiConfiguration);
                    WifiStateMachine.this.startSoftApWithConfig(localWifiConfiguration);
                }
            }
            throw new RuntimeException("Illegal transition to SoftApStartingState: " + localMessage);
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool = false;
            switch (paramMessage.what)
            {
            default:
                return bool;
            case 131073:
            case 131074:
            case 131083:
            case 131084:
            case 131085:
            case 131086:
            case 131093:
            case 131096:
            case 131101:
            case 131144:
            case 131145:
            case 131152:
            case 131156:
            case 131157:
            case 131162:
                WifiStateMachine.this.deferMessage(paramMessage);
            case 131100:
            case 131094:
            case 131095:
            }
            while (true)
            {
                bool = true;
                break;
                WifiConfiguration localWifiConfiguration = (WifiConfiguration)paramMessage.obj;
                if (localWifiConfiguration != null)
                {
                    WifiStateMachine.this.startSoftApWithConfig(localWifiConfiguration);
                }
                else
                {
                    WifiStateMachine.this.loge("Softap config is null!");
                    WifiStateMachine.this.sendMessage(131095);
                    continue;
                    WifiStateMachine.this.setWifiApState(13);
                    WifiStateMachine.this.transitionTo(WifiStateMachine.this.mSoftApStartedState);
                    continue;
                    WifiStateMachine.this.sendMessage(WifiStateMachine.this.obtainMessage(131074, 14, 0));
                }
            }
        }
    }

    class WpsRunningState extends State
    {
        private Message mSourceMessage;

        WpsRunningState()
        {
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
            this.mSourceMessage = Message.obtain(WifiStateMachine.this.getCurrentMessage());
        }

        public void exit()
        {
            WifiStateMachine.this.mWifiConfigStore.enableAllNetworks();
            WifiStateMachine.this.mWifiConfigStore.loadConfiguredNetworks();
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool = false;
            switch (paramMessage.what)
            {
            default:
                return bool;
            case 147464:
                WifiStateMachine.this.replyToMessage(this.mSourceMessage, 151565);
                this.mSourceMessage.recycle();
                this.mSourceMessage = null;
                WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDisconnectedState);
            case 147462:
            case 147466:
            case 147465:
            case 147467:
            case 151562:
            case 151566:
            case 131086:
            case 131126:
            case 131144:
            case 131147:
            case 131148:
            case 147459:
            case 151553:
            case 147460:
            }
            while (true)
            {
                bool = true;
                break;
                WifiStateMachine.this.replyToMessage(this.mSourceMessage, 151564, 3);
                this.mSourceMessage.recycle();
                this.mSourceMessage = null;
                WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDisconnectedState);
                continue;
                WifiStateMachine.this.replyToMessage(this.mSourceMessage, 151564, paramMessage.arg1);
                this.mSourceMessage.recycle();
                this.mSourceMessage = null;
                WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDisconnectedState);
                continue;
                WifiStateMachine.this.replyToMessage(this.mSourceMessage, 151564, 7);
                this.mSourceMessage.recycle();
                this.mSourceMessage = null;
                WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDisconnectedState);
                continue;
                WifiStateMachine.this.replyToMessage(paramMessage, 151564, 1);
                continue;
                if (WifiStateMachine.this.mWifiNative.cancelWps())
                    WifiStateMachine.this.replyToMessage(paramMessage, 151568);
                while (true)
                {
                    WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDisconnectedState);
                    break;
                    WifiStateMachine.this.replyToMessage(paramMessage, 151567, 0);
                }
                WifiStateMachine.this.deferMessage(paramMessage);
                continue;
                WifiStateMachine.this.handleNetworkDisconnect();
            }
        }
    }

    class DisconnectedState extends State
    {
        private boolean mAlarmEnabled = false;
        private long mFrameworkScanIntervalMs;

        DisconnectedState()
        {
        }

        private void setScanAlarm(boolean paramBoolean)
        {
            if (paramBoolean == this.mAlarmEnabled);
            while (true)
            {
                return;
                if (paramBoolean)
                {
                    if (this.mFrameworkScanIntervalMs > 0L)
                    {
                        WifiStateMachine.this.mAlarmManager.setRepeating(0, System.currentTimeMillis() + this.mFrameworkScanIntervalMs, this.mFrameworkScanIntervalMs, WifiStateMachine.this.mScanIntent);
                        this.mAlarmEnabled = true;
                    }
                }
                else
                {
                    WifiStateMachine.this.mAlarmManager.cancel(WifiStateMachine.this.mScanIntent);
                    this.mAlarmEnabled = false;
                }
            }
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
            this.mFrameworkScanIntervalMs = Settings.Secure.getLong(WifiStateMachine.this.mContext.getContentResolver(), "wifi_framework_scan_interval_ms", WifiStateMachine.this.mDefaultFrameworkScanIntervalMs);
            if (WifiStateMachine.this.mEnableBackgroundScan)
                if (!WifiStateMachine.this.mScanResultIsPending)
                    WifiStateMachine.this.mWifiNative.enableBackgroundScan(true);
            while (true)
            {
                if (WifiStateMachine.this.mWifiConfigStore.getConfiguredNetworks().size() == 0)
                    WifiStateMachine.this.sendMessageDelayed(WifiStateMachine.this.obtainMessage(131160, WifiStateMachine.access$14404(WifiStateMachine.this), 0), WifiStateMachine.this.mSupplicantScanIntervalMs);
                return;
                setScanAlarm(true);
            }
        }

        public void exit()
        {
            if (WifiStateMachine.this.mEnableBackgroundScan)
                WifiStateMachine.this.mWifiNative.enableBackgroundScan(false);
            setScanAlarm(false);
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool1 = true;
            switch (paramMessage.what)
            {
            default:
                bool1 = false;
            case 147460:
            case 131160:
            case 131125:
            case 151556:
            case 131144:
            case 131163:
            case 147462:
            case 131143:
            case 147461:
            }
            while (true)
            {
                return bool1;
                if ((paramMessage.arg1 == WifiStateMachine.this.mPeriodicScanToken) && (WifiStateMachine.this.mWifiConfigStore.getConfiguredNetworks().size() == 0))
                {
                    WifiStateMachine.this.sendMessage(131143);
                    WifiStateMachine.this.sendMessageDelayed(WifiStateMachine.this.obtainMessage(131160, WifiStateMachine.access$14404(WifiStateMachine.this), 0), WifiStateMachine.this.mSupplicantScanIntervalMs);
                    continue;
                    WifiStateMachine.this.sendMessageDelayed(WifiStateMachine.this.obtainMessage(131160, WifiStateMachine.access$14404(WifiStateMachine.this), 0), WifiStateMachine.this.mSupplicantScanIntervalMs);
                    bool1 = false;
                    continue;
                    if (paramMessage.arg1 == 2)
                    {
                        WifiStateMachine.this.mWifiNative.setScanResultHandling(paramMessage.arg1);
                        WifiStateMachine.this.mWifiNative.disconnect();
                        WifiStateMachine.access$5602(WifiStateMachine.this, true);
                        WifiStateMachine.this.transitionTo(WifiStateMachine.this.mScanModeState);
                        continue;
                        WifiStateMachine localWifiStateMachine = WifiStateMachine.this;
                        if (paramMessage.arg1 == 1);
                        for (boolean bool2 = true; ; bool2 = false)
                        {
                            WifiStateMachine.access$1002(localWifiStateMachine, bool2);
                            if (!WifiStateMachine.this.mEnableBackgroundScan)
                                break label323;
                            WifiStateMachine.this.mWifiNative.enableBackgroundScan(true);
                            setScanAlarm(false);
                            break;
                        }
                        label323: WifiStateMachine.this.mWifiNative.enableBackgroundScan(false);
                        setScanAlarm(true);
                        continue;
                        StateChangeResult localStateChangeResult = (StateChangeResult)paramMessage.obj;
                        WifiStateMachine.this.setNetworkDetailedState(WifiInfo.getDetailedStateOf(localStateChangeResult.state));
                        bool1 = false;
                        continue;
                        if (WifiStateMachine.this.mEnableBackgroundScan)
                            WifiStateMachine.this.mWifiNative.enableBackgroundScan(false);
                        bool1 = false;
                        continue;
                        if ((WifiStateMachine.this.mEnableBackgroundScan) && (WifiStateMachine.this.mScanResultIsPending))
                            WifiStateMachine.this.mWifiNative.enableBackgroundScan(true);
                        bool1 = false;
                    }
                }
            }
        }
    }

    class DisconnectingState extends State
    {
        DisconnectingState()
        {
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool;
            switch (paramMessage.what)
            {
            default:
                bool = false;
                return bool;
            case 131144:
                if (paramMessage.arg1 == 2)
                    WifiStateMachine.this.deferMessage(paramMessage);
                break;
            case 147462:
            }
            while (true)
            {
                bool = true;
                break;
                WifiStateMachine.this.deferMessage(paramMessage);
                WifiStateMachine.this.handleNetworkDisconnect();
                WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDisconnectedState);
            }
        }
    }

    class ConnectedState extends State
    {
        ConnectedState()
        {
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
        }

        public void exit()
        {
            WifiStateMachine.this.checkAndSetConnectivityInstance();
            WifiStateMachine.this.mCm.requestNetworkTransitionWakelock("WifiStateMachine");
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool;
            switch (paramMessage.what)
            {
            default:
                bool = false;
            case 135189:
            }
            while (true)
            {
                return bool;
                try
                {
                    WifiStateMachine.this.mNwService.disableIpv6(WifiStateMachine.this.mInterfaceName);
                    WifiStateMachine.this.setNetworkDetailedState(NetworkInfo.DetailedState.DISCONNECTED);
                    WifiStateMachine.this.mWifiConfigStore.updateStatus(WifiStateMachine.this.mLastNetworkId, NetworkInfo.DetailedState.DISCONNECTED);
                    WifiStateMachine.this.sendNetworkStateChangeBroadcast(WifiStateMachine.this.mLastBssid);
                    WifiStateMachine.this.transitionTo(WifiStateMachine.this.mVerifyingLinkState);
                    bool = true;
                }
                catch (RemoteException localRemoteException)
                {
                    while (true)
                        WifiStateMachine.this.loge("Failed to disable IPv6: " + localRemoteException);
                }
                catch (IllegalStateException localIllegalStateException)
                {
                    while (true)
                        WifiStateMachine.this.loge("Failed to disable IPv6: " + localIllegalStateException);
                }
            }
        }
    }

    class VerifyingLinkState extends State
    {
        VerifyingLinkState()
        {
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
            WifiStateMachine.this.setNetworkDetailedState(NetworkInfo.DetailedState.VERIFYING_POOR_LINK);
            WifiStateMachine.this.mWifiConfigStore.updateStatus(WifiStateMachine.this.mLastNetworkId, NetworkInfo.DetailedState.VERIFYING_POOR_LINK);
            WifiStateMachine.this.sendNetworkStateChangeBroadcast(WifiStateMachine.this.mLastBssid);
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool;
            switch (paramMessage.what)
            {
            default:
                bool = false;
            case 135190:
            case 135189:
            }
            while (true)
            {
                return bool;
                try
                {
                    WifiStateMachine.this.mNwService.enableIpv6(WifiStateMachine.this.mInterfaceName);
                    WifiStateMachine.this.setNetworkDetailedState(NetworkInfo.DetailedState.CONNECTED);
                    WifiStateMachine.this.mWifiConfigStore.updateStatus(WifiStateMachine.this.mLastNetworkId, NetworkInfo.DetailedState.CONNECTED);
                    WifiStateMachine.this.sendNetworkStateChangeBroadcast(WifiStateMachine.this.mLastBssid);
                    WifiStateMachine.this.transitionTo(WifiStateMachine.this.mConnectedState);
                    bool = true;
                }
                catch (RemoteException localRemoteException)
                {
                    while (true)
                        WifiStateMachine.this.loge("Failed to enable IPv6: " + localRemoteException);
                }
                catch (IllegalStateException localIllegalStateException)
                {
                    while (true)
                        WifiStateMachine.this.loge("Failed to enable IPv6: " + localIllegalStateException);
                }
            }
        }
    }

    class ObtainingIpState extends State
    {
        ObtainingIpState()
        {
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
            if (!WifiStateMachine.this.mWifiConfigStore.isUsingStaticIp(WifiStateMachine.this.mLastNetworkId))
            {
                WifiStateMachine.access$11802(WifiStateMachine.this, DhcpStateMachine.makeDhcpStateMachine(WifiStateMachine.this.mContext, WifiStateMachine.this, WifiStateMachine.this.mInterfaceName));
                WifiStateMachine.this.mDhcpStateMachine.registerForPreDhcpNotification();
                WifiStateMachine.this.mDhcpStateMachine.sendMessage(196609);
            }
            while (true)
            {
                return;
                DhcpInfoInternal localDhcpInfoInternal = WifiStateMachine.this.mWifiConfigStore.getIpConfiguration(WifiStateMachine.this.mLastNetworkId);
                InterfaceConfiguration localInterfaceConfiguration = new InterfaceConfiguration();
                localInterfaceConfiguration.setLinkAddress(localDhcpInfoInternal.makeLinkAddress());
                localInterfaceConfiguration.setInterfaceUp();
                try
                {
                    WifiStateMachine.this.mNwService.setInterfaceConfig(WifiStateMachine.this.mInterfaceName, localInterfaceConfiguration);
                    WifiStateMachine.this.sendMessage(131087, localDhcpInfoInternal);
                }
                catch (RemoteException localRemoteException)
                {
                    WifiStateMachine.this.loge("Static IP configuration failed: " + localRemoteException);
                    WifiStateMachine.this.sendMessage(131088);
                }
                catch (IllegalStateException localIllegalStateException)
                {
                    WifiStateMachine.this.loge("Static IP configuration failed: " + localIllegalStateException);
                    WifiStateMachine.this.sendMessage(131088);
                }
            }
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool;
            switch (paramMessage.what)
            {
            default:
                bool = false;
                return bool;
            case 131087:
                WifiStateMachine.this.handleSuccessfulIpConfiguration((DhcpInfoInternal)paramMessage.obj);
                WifiStateMachine.this.transitionTo(WifiStateMachine.this.mVerifyingLinkState);
            case 131088:
            case 151559:
            case 131149:
            }
            while (true)
            {
                bool = true;
                break;
                WifiStateMachine.this.handleFailedIpConfiguration();
                WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDisconnectingState);
                continue;
                WifiStateMachine.this.deferMessage(paramMessage);
                continue;
                WifiStateMachine.this.deferMessage(paramMessage);
            }
        }
    }

    class L2ConnectedState extends State
    {
        L2ConnectedState()
        {
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
            WifiStateMachine.access$11708(WifiStateMachine.this);
            if (WifiStateMachine.this.mEnableRssiPolling)
                WifiStateMachine.this.sendMessage(WifiStateMachine.this.obtainMessage(131155, WifiStateMachine.this.mRssiPollToken, 0));
        }

        public void exit()
        {
            if (WifiStateMachine.this.mScanResultIsPending)
                WifiStateMachine.this.mWifiNative.setScanResultHandling(1);
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool1 = false;
            switch (paramMessage.what)
            {
            default:
                return bool1;
            case 196612:
                WifiStateMachine.this.handlePreDhcpSetup();
                WifiStateMachine.this.mDhcpStateMachine.sendMessage(196614);
            case 147459:
            case 196613:
            case 131146:
            case 131144:
            case 131143:
            case 151553:
            case 151559:
            case 131155:
            case 131154:
            case 135191:
            }
            while (true)
            {
                bool1 = true;
                break;
                WifiStateMachine.this.handlePostDhcpSetup();
                if (paramMessage.arg1 == 1)
                {
                    WifiStateMachine.this.handleSuccessfulIpConfiguration((DhcpInfoInternal)paramMessage.obj);
                    WifiStateMachine.this.transitionTo(WifiStateMachine.this.mVerifyingLinkState);
                }
                else if (paramMessage.arg1 == 2)
                {
                    WifiStateMachine.this.handleFailedIpConfiguration();
                    WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDisconnectingState);
                    continue;
                    WifiStateMachine.this.mWifiNative.disconnect();
                    WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDisconnectingState);
                    continue;
                    if (paramMessage.arg1 == 2)
                    {
                        WifiStateMachine.this.sendMessage(131146);
                        WifiStateMachine.this.deferMessage(paramMessage);
                        continue;
                        WifiStateMachine.this.mWifiNative.setScanResultHandling(2);
                        break;
                        int i = paramMessage.arg1;
                        if (WifiStateMachine.this.mWifiInfo.getNetworkId() != i)
                            break;
                        continue;
                        WifiConfiguration localWifiConfiguration = (WifiConfiguration)paramMessage.obj;
                        NetworkUpdateResult localNetworkUpdateResult = WifiStateMachine.this.mWifiConfigStore.saveNetwork(localWifiConfiguration);
                        if (WifiStateMachine.this.mWifiInfo.getNetworkId() == localNetworkUpdateResult.getNetworkId())
                        {
                            if (localNetworkUpdateResult.hasIpChanged())
                            {
                                WifiStateMachine.this.log("Reconfiguring IP on connection");
                                WifiStateMachine.this.transitionTo(WifiStateMachine.this.mObtainingIpState);
                            }
                            if (localNetworkUpdateResult.hasProxyChanged())
                            {
                                WifiStateMachine.this.log("Reconfiguring proxy on connection");
                                WifiStateMachine.this.configureLinkProperties();
                                WifiStateMachine.this.sendLinkConfigurationChangedBroadcast();
                            }
                        }
                        if (localNetworkUpdateResult.getNetworkId() != -1)
                        {
                            WifiStateMachine.this.replyToMessage(paramMessage, 151561);
                        }
                        else
                        {
                            WifiStateMachine.this.loge("Failed to save network");
                            WifiStateMachine.this.replyToMessage(paramMessage, 151560, 0);
                            continue;
                            if (paramMessage.arg1 == WifiStateMachine.this.mRssiPollToken)
                            {
                                WifiStateMachine.this.fetchRssiAndLinkSpeedNative();
                                WifiStateMachine.this.sendMessageDelayed(WifiStateMachine.this.obtainMessage(131155, WifiStateMachine.this.mRssiPollToken, 0), 3000L);
                                continue;
                                WifiStateMachine localWifiStateMachine = WifiStateMachine.this;
                                if (paramMessage.arg1 == 1);
                                for (boolean bool2 = true; ; bool2 = false)
                                {
                                    WifiStateMachine.access$902(localWifiStateMachine, bool2);
                                    WifiStateMachine.access$11708(WifiStateMachine.this);
                                    if (!WifiStateMachine.this.mEnableRssiPolling)
                                        break;
                                    WifiStateMachine.this.fetchRssiAndLinkSpeedNative();
                                    WifiStateMachine.this.sendMessageDelayed(WifiStateMachine.this.obtainMessage(131155, WifiStateMachine.this.mRssiPollToken, 0), 3000L);
                                    break;
                                }
                                WifiStateMachine.this.fetchRssiAndLinkSpeedNative();
                                WifiStateMachine.this.replyToMessage(paramMessage, 135192, WifiStateMachine.this.mWifiInfo.getRssi());
                            }
                        }
                    }
                }
            }
        }
    }

    class ConnectModeState extends State
    {
        ConnectModeState()
        {
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool = false;
            switch (paramMessage.what)
            {
            default:
                return bool;
            case 147463:
                WifiStateMachine.this.mSupplicantStateTracker.sendMessage(147463);
            case 147462:
            case 131146:
            case 131147:
            case 131148:
            case 151553:
            case 151562:
            case 147461:
            case 147459:
            case 147460:
            }
            while (true)
            {
                bool = true;
                break;
                SupplicantState localSupplicantState = WifiStateMachine.this.handleSupplicantStateChange(paramMessage);
                if (!SupplicantState.isDriverActive(localSupplicantState))
                {
                    if (WifiStateMachine.this.mNetworkInfo.getState() != NetworkInfo.State.DISCONNECTED)
                        WifiStateMachine.this.handleNetworkDisconnect();
                    WifiStateMachine.this.log("Detected an interface down, restart driver");
                    WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDriverStoppedState);
                    WifiStateMachine.this.sendMessage(131085);
                }
                else if ((localSupplicantState == SupplicantState.DISCONNECTED) && (WifiStateMachine.this.mNetworkInfo.getState() != NetworkInfo.State.DISCONNECTED))
                {
                    WifiStateMachine.this.handleNetworkDisconnect();
                    WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDisconnectedState);
                    continue;
                    WifiStateMachine.this.mWifiNative.disconnect();
                    continue;
                    WifiStateMachine.this.mWifiNative.reconnect();
                    continue;
                    WifiStateMachine.this.mWifiNative.reassociate();
                    continue;
                    int i = paramMessage.arg1;
                    WifiConfiguration localWifiConfiguration = (WifiConfiguration)paramMessage.obj;
                    if (localWifiConfiguration != null)
                        i = WifiStateMachine.this.mWifiConfigStore.saveNetwork(localWifiConfiguration).getNetworkId();
                    if ((WifiStateMachine.this.mWifiConfigStore.selectNetwork(i)) && (WifiStateMachine.this.mWifiNative.reconnect()))
                    {
                        WifiStateMachine.this.mSupplicantStateTracker.sendMessage(151553);
                        WifiStateMachine.this.replyToMessage(paramMessage, 151555);
                        WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDisconnectingState);
                    }
                    else
                    {
                        WifiStateMachine.this.loge("Failed to connect config: " + localWifiConfiguration + " netId: " + i);
                        WifiStateMachine.this.replyToMessage(paramMessage, 151554, 0);
                        continue;
                        WpsInfo localWpsInfo = (WpsInfo)paramMessage.obj;
                        WpsResult localWpsResult;
                        switch (localWpsInfo.setup)
                        {
                        default:
                            localWpsResult = new WpsResult(WpsResult.Status.FAILURE);
                            Log.e("WifiStateMachine", "Invalid setup for WPS");
                        case 0:
                        case 2:
                        case 1:
                        }
                        while (true)
                        {
                            if (localWpsResult.status != WpsResult.Status.SUCCESS)
                                break label587;
                            WifiStateMachine.this.replyToMessage(paramMessage, 151563, localWpsResult);
                            WifiStateMachine.this.transitionTo(WifiStateMachine.this.mWpsRunningState);
                            break;
                            localWpsResult = WifiStateMachine.this.mWifiConfigStore.startWpsPbc(localWpsInfo);
                            continue;
                            localWpsResult = WifiStateMachine.this.mWifiConfigStore.startWpsWithPinFromAccessPoint(localWpsInfo);
                            continue;
                            localWpsResult = WifiStateMachine.this.mWifiConfigStore.startWpsWithPinFromDevice(localWpsInfo);
                        }
                        label587: Log.e("WifiStateMachine", "Failed to start WPS with config " + localWpsInfo.toString());
                        WifiStateMachine.this.replyToMessage(paramMessage, 151564, 0);
                        continue;
                        WifiStateMachine.this.mWifiNative.setScanResultHandling(1);
                        break;
                        WifiStateMachine.access$4602(WifiStateMachine.this, paramMessage.arg1);
                        WifiStateMachine.access$4502(WifiStateMachine.this, (String)paramMessage.obj);
                        WifiStateMachine.this.mWifiInfo.setBSSID(WifiStateMachine.this.mLastBssid);
                        WifiStateMachine.this.mWifiInfo.setNetworkId(WifiStateMachine.this.mLastNetworkId);
                        WifiStateMachine.this.setNetworkDetailedState(NetworkInfo.DetailedState.OBTAINING_IPADDR);
                        WifiStateMachine.this.sendNetworkStateChangeBroadcast(WifiStateMachine.this.mLastBssid);
                        WifiStateMachine.this.transitionTo(WifiStateMachine.this.mObtainingIpState);
                        continue;
                        WifiStateMachine.this.handleNetworkDisconnect();
                        WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDisconnectedState);
                    }
                }
            }
        }
    }

    class ScanModeState extends State
    {
        ScanModeState()
        {
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool = true;
            switch (paramMessage.what)
            {
            default:
                bool = false;
            case 131146:
            case 131147:
            case 131148:
            case 147459:
            case 147460:
            case 147462:
            case 131144:
            }
            while (true)
            {
                return bool;
                if (paramMessage.arg1 != 2)
                {
                    WifiStateMachine.this.mWifiNative.setScanResultHandling(paramMessage.arg1);
                    WifiStateMachine.this.mWifiNative.reconnect();
                    WifiStateMachine.access$5602(WifiStateMachine.this, false);
                    WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDisconnectedState);
                }
            }
        }
    }

    class DriverStoppedState extends State
    {
        DriverStoppedState()
        {
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool;
            switch (paramMessage.what)
            {
            default:
                bool = false;
                return bool;
            case 147462:
                if (SupplicantState.isDriverActive(((StateChangeResult)paramMessage.obj).state))
                    WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDriverStartedState);
                break;
            case 131085:
            }
            while (true)
            {
                bool = true;
                break;
                WifiStateMachine.this.mWakeLock.acquire();
                WifiStateMachine.this.mWifiNative.startDriver();
                WifiStateMachine.this.mWakeLock.release();
                WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDriverStartingState);
            }
        }
    }

    class DriverStoppingState extends State
    {
        DriverStoppingState()
        {
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool;
            switch (paramMessage.what)
            {
            default:
                bool = false;
                return bool;
            case 147462:
                if (WifiStateMachine.this.handleSupplicantStateChange(paramMessage) == SupplicantState.INTERFACE_DISABLED)
                    WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDriverStoppedState);
                break;
            case 131085:
            case 131086:
            case 131143:
            case 131145:
            case 131146:
            case 131147:
            case 131148:
            case 131152:
            case 131156:
            case 131157:
            case 131162:
            }
            while (true)
            {
                bool = true;
                break;
                WifiStateMachine.this.deferMessage(paramMessage);
            }
        }
    }

    class DriverStartedState extends State
    {
        DriverStartedState()
        {
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
            WifiStateMachine.access$7702(WifiStateMachine.this, true);
            WifiStateMachine.access$7802(WifiStateMachine.this, false);
            WifiStateMachine.this.updateBatteryWorkSource(null);
            WifiStateMachine.this.mWifiNative.setBluetoothCoexistenceScanMode(WifiStateMachine.this.mBluetoothConnectionActive);
            WifiStateMachine.this.setCountryCode();
            WifiStateMachine.this.setFrequencyBand();
            WifiStateMachine.this.setNetworkDetailedState(NetworkInfo.DetailedState.DISCONNECTED);
            WifiStateMachine.this.mWifiNative.stopFilteringMulticastV6Packets();
            if (WifiStateMachine.this.mFilteringMulticastV4Packets.get())
            {
                WifiStateMachine.this.mWifiNative.startFilteringMulticastV4Packets();
                WifiStateMachine.this.mWifiNative.setPowerSave(WifiStateMachine.this.mPowerSaveEnabled);
                if (!WifiStateMachine.this.mIsScanMode)
                    break label239;
                WifiStateMachine.this.mWifiNative.setScanResultHandling(2);
                WifiStateMachine.this.mWifiNative.disconnect();
                WifiStateMachine.this.transitionTo(WifiStateMachine.this.mScanModeState);
            }
            while (true)
            {
                if (WifiStateMachine.this.mP2pSupported)
                    WifiStateMachine.this.mWifiP2pChannel.sendMessage(131203);
                WifiStateMachine.this.mContext.registerReceiver(WifiStateMachine.this.mScreenReceiver, WifiStateMachine.this.mScreenFilter);
                return;
                WifiStateMachine.this.mWifiNative.stopFilteringMulticastV4Packets();
                break;
                label239: WifiStateMachine.this.mWifiNative.setScanResultHandling(1);
                WifiStateMachine.this.mWifiNative.reconnect();
                WifiStateMachine.this.mWifiNative.status();
                WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDisconnectedState);
            }
        }

        public void exit()
        {
            WifiStateMachine.access$7702(WifiStateMachine.this, false);
            WifiStateMachine.this.updateBatteryWorkSource(null);
            WifiStateMachine.access$9902(WifiStateMachine.this, null);
            if (WifiStateMachine.this.mP2pSupported)
                WifiStateMachine.this.mWifiP2pChannel.sendMessage(131204);
            WifiStateMachine.this.mContext.unregisterReceiver(WifiStateMachine.this.mScreenReceiver);
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool1 = false;
            switch (paramMessage.what)
            {
            default:
                return bool1;
            case 131145:
                WifiStateMachine localWifiStateMachine3 = WifiStateMachine.this;
                if (paramMessage.arg1 == 1)
                    bool1 = true;
                WifiStateMachine.access$9002(localWifiStateMachine3, bool1);
                WifiStateMachine.this.mWifiNative.setScanMode(WifiStateMachine.this.mSetScanActive);
            case 131143:
            case 131152:
            case 131162:
            case 131103:
            case 131086:
            case 131085:
            case 131090:
            case 131156:
            case 131157:
            case 131158:
            case 131159:
                while (true)
                {
                    bool1 = true;
                    break;
                    if (paramMessage.arg1 == 1);
                    for (boolean bool3 = true; ; bool3 = false)
                    {
                        if ((bool3) && (!WifiStateMachine.this.mSetScanActive))
                            WifiStateMachine.this.mWifiNative.setScanMode(bool3);
                        WifiStateMachine.this.mWifiNative.scan();
                        if ((bool3) && (!WifiStateMachine.this.mSetScanActive))
                            WifiStateMachine.this.mWifiNative.setScanMode(WifiStateMachine.this.mSetScanActive);
                        WifiStateMachine.access$6502(WifiStateMachine.this, true);
                        break;
                    }
                    String str = (String)paramMessage.obj;
                    if (!WifiStateMachine.this.mWifiNative.setCountryCode(str.toUpperCase()))
                    {
                        WifiStateMachine.this.loge("Failed to set country code " + str);
                        continue;
                        int j = paramMessage.arg1;
                        if (WifiStateMachine.this.mWifiNative.setBand(j))
                        {
                            WifiStateMachine.this.mFrequencyBand.set(j);
                            WifiStateMachine.this.startScan(true);
                        }
                        else
                        {
                            WifiStateMachine.this.loge("Failed to set frequency band " + j);
                            continue;
                            WifiStateMachine localWifiStateMachine2 = WifiStateMachine.this;
                            if (paramMessage.arg1 != 0)
                                bool1 = true;
                            WifiStateMachine.access$602(localWifiStateMachine2, bool1);
                            WifiStateMachine.this.mWifiNative.setBluetoothCoexistenceScanMode(WifiStateMachine.this.mBluetoothConnectionActive);
                            continue;
                            int i = paramMessage.arg1;
                            if ((!WifiStateMachine.this.mInDelayedStop) || (i == 1))
                            {
                                WifiStateMachine.access$7802(WifiStateMachine.this, true);
                                WifiStateMachine.access$9208(WifiStateMachine.this);
                                if (i == 1)
                                {
                                    WifiStateMachine.this.sendMessage(WifiStateMachine.this.obtainMessage(131090, WifiStateMachine.this.mDelayedStopCounter, 0));
                                }
                                else
                                {
                                    Intent localIntent = new Intent("com.android.server.WifiManager.action.DELAYED_DRIVER_STOP", null);
                                    localIntent.putExtra("DelayedStopCounter", WifiStateMachine.this.mDelayedStopCounter);
                                    WifiStateMachine.access$9302(WifiStateMachine.this, PendingIntent.getBroadcast(WifiStateMachine.this.mContext, 0, localIntent, 134217728));
                                    WifiStateMachine.this.mAlarmManager.set(0, System.currentTimeMillis() + WifiStateMachine.this.mDriverStopDelayMs, WifiStateMachine.this.mDriverStopIntent);
                                    continue;
                                    if (WifiStateMachine.this.mInDelayedStop)
                                    {
                                        WifiStateMachine.access$7802(WifiStateMachine.this, false);
                                        WifiStateMachine.access$9208(WifiStateMachine.this);
                                        WifiStateMachine.this.mAlarmManager.cancel(WifiStateMachine.this.mDriverStopIntent);
                                        continue;
                                        if (paramMessage.arg1 == WifiStateMachine.this.mDelayedStopCounter)
                                        {
                                            if (WifiStateMachine.this.getCurrentState() != WifiStateMachine.this.mDisconnectedState)
                                            {
                                                WifiStateMachine.this.mWifiNative.disconnect();
                                                WifiStateMachine.this.handleNetworkDisconnect();
                                            }
                                            WifiStateMachine.this.mWakeLock.acquire();
                                            WifiStateMachine.this.mWifiNative.stopDriver();
                                            WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDriverStoppingState);
                                            WifiStateMachine.this.mWakeLock.release();
                                            continue;
                                            if (paramMessage.arg1 == 1)
                                            {
                                                WifiStateMachine.this.mWifiNative.startFilteringMulticastV6Packets();
                                            }
                                            else if (paramMessage.arg1 == 0)
                                            {
                                                WifiStateMachine.this.mWifiNative.startFilteringMulticastV4Packets();
                                            }
                                            else
                                            {
                                                WifiStateMachine.this.loge("Illegal arugments to CMD_START_PACKET_FILTERING");
                                                continue;
                                                if (paramMessage.arg1 == 1)
                                                {
                                                    WifiStateMachine.this.mWifiNative.stopFilteringMulticastV6Packets();
                                                }
                                                else if (paramMessage.arg1 == 0)
                                                {
                                                    WifiStateMachine.this.mWifiNative.stopFilteringMulticastV4Packets();
                                                }
                                                else
                                                {
                                                    WifiStateMachine.this.loge("Illegal arugments to CMD_STOP_PACKET_FILTERING");
                                                    continue;
                                                    if (!WifiStateMachine.this.mHighPerfMode)
                                                        WifiStateMachine.this.mWifiNative.setSuspendOptimizations(true);
                                                    WifiStateMachine.this.mSuspendWakeLock.release();
                                                    continue;
                                                    WifiStateMachine.this.mWifiNative.setSuspendOptimizations(false);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            case 131149:
            }
            WifiStateMachine localWifiStateMachine1 = WifiStateMachine.this;
            if (paramMessage.arg1 == 1);
            for (boolean bool2 = true; ; bool2 = false)
            {
                WifiStateMachine.access$1102(localWifiStateMachine1, bool2);
                if (!WifiStateMachine.this.mHighPerfMode)
                    break;
                WifiStateMachine.this.mWifiNative.setSuspendOptimizations(false);
                break;
            }
        }
    }

    class DriverStartingState extends State
    {
        private int mTries;

        DriverStartingState()
        {
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
            this.mTries = 1;
            WifiStateMachine.this.sendMessageDelayed(WifiStateMachine.this.obtainMessage(131091, WifiStateMachine.access$7104(WifiStateMachine.this), 0), 10000L);
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool = false;
            switch (paramMessage.what)
            {
            default:
                return bool;
            case 147462:
                if (SupplicantState.isDriverActive(WifiStateMachine.this.handleSupplicantStateChange(paramMessage)))
                    WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDriverStartedState);
                break;
            case 131091:
            case 131085:
            case 131086:
            case 131143:
            case 131145:
            case 131146:
            case 131147:
            case 131148:
            case 131152:
            case 131156:
            case 131157:
            case 131162:
            case 147459:
            case 147460:
            case 147463:
            case 147466:
            }
            while (true)
            {
                bool = true;
                break;
                if (paramMessage.arg1 == WifiStateMachine.this.mDriverStartToken)
                    if (this.mTries >= 2)
                    {
                        WifiStateMachine.this.loge("Failed to start driver after " + this.mTries);
                        WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDriverStoppedState);
                    }
                    else
                    {
                        WifiStateMachine.this.loge("Driver start failed, retrying");
                        WifiStateMachine.this.mWakeLock.acquire();
                        WifiStateMachine.this.mWifiNative.startDriver();
                        WifiStateMachine.this.mWakeLock.release();
                        this.mTries = (1 + this.mTries);
                        WifiStateMachine.this.sendMessageDelayed(WifiStateMachine.this.obtainMessage(131091, WifiStateMachine.access$7104(WifiStateMachine.this), 0), 10000L);
                        continue;
                        WifiStateMachine.this.deferMessage(paramMessage);
                    }
            }
        }
    }

    class SupplicantStoppingState extends State
    {
        SupplicantStoppingState()
        {
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
            WifiStateMachine.this.handleNetworkDisconnect();
            if (!WifiStateMachine.this.mWifiNative.stopSupplicant())
                WifiStateMachine.this.loge("Failed to stop supplicant");
            WifiStateMachine.this.sendMessageDelayed(WifiStateMachine.this.obtainMessage(131089, WifiStateMachine.access$6704(WifiStateMachine.this), 0), 5000L);
            WifiStateMachine.this.mNetworkInfo.setIsAvailable(false);
            WifiStateMachine.this.setWifiState(0);
            WifiStateMachine.this.sendSupplicantConnectionChangedBroadcast(false);
            WifiStateMachine.this.mSupplicantStateTracker.sendMessage(131183);
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool;
            switch (paramMessage.what)
            {
            default:
                bool = false;
                return bool;
            case 147457:
                WifiStateMachine.this.loge("Supplicant connection received while stopping");
            case 147458:
            case 131089:
            case 131073:
            case 131074:
            case 131083:
            case 131084:
            case 131085:
            case 131086:
            case 131093:
            case 131096:
            case 131144:
            case 131145:
            case 131152:
            case 131156:
            case 131157:
            case 131162:
            }
            while (true)
            {
                bool = true;
                break;
                WifiNative.killSupplicant();
                WifiStateMachine.this.mWifiNative.closeSupplicantConnection();
                WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDriverLoadedState);
                continue;
                if (paramMessage.arg1 == WifiStateMachine.this.mSupplicantStopFailureToken)
                {
                    WifiStateMachine.this.loge("Timed out on a supplicant stop, kill and proceed");
                    WifiNative.killSupplicant();
                    WifiStateMachine.this.mWifiNative.closeSupplicantConnection();
                    WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDriverLoadedState);
                    continue;
                    WifiStateMachine.this.deferMessage(paramMessage);
                }
            }
        }
    }

    class SupplicantStartedState extends State
    {
        SupplicantStartedState()
        {
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
            WifiStateMachine.access$5602(WifiStateMachine.this, false);
            WifiStateMachine.this.mNetworkInfo.setIsAvailable(true);
            int i = WifiStateMachine.this.mContext.getResources().getInteger(17694731);
            WifiStateMachine.access$5802(WifiStateMachine.this, Settings.Secure.getLong(WifiStateMachine.this.mContext.getContentResolver(), "wifi_supplicant_scan_interval_ms", i));
            WifiStateMachine.this.mWifiNative.setScanInterval((int)WifiStateMachine.this.mSupplicantScanIntervalMs / 1000);
        }

        public void exit()
        {
            WifiStateMachine.this.mNetworkInfo.setIsAvailable(false);
        }

        public boolean processMessage(Message paramMessage)
        {
            int i = -1;
            boolean bool1 = false;
            switch (paramMessage.what)
            {
            default:
                return bool1;
            case 131084:
                WifiStateMachine.this.transitionTo(WifiStateMachine.this.mSupplicantStoppingState);
            case 147458:
            case 147461:
            case 131123:
            case 131124:
            case 131125:
            case 131126:
            case 131127:
            case 151569:
            case 131128:
            case 131129:
            case 131130:
            case 131131:
            case 131093:
            case 131144:
            case 151559:
            case 151556:
            }
            while (true)
            {
                bool1 = true;
                break;
                WifiStateMachine.this.loge("Connection lost, restart supplicant");
                WifiNative.killSupplicant();
                WifiStateMachine.this.mWifiNative.closeSupplicantConnection();
                WifiStateMachine.this.mNetworkInfo.setIsAvailable(false);
                WifiStateMachine.this.handleNetworkDisconnect();
                WifiStateMachine.this.sendSupplicantConnectionChangedBroadcast(false);
                WifiStateMachine.this.mSupplicantStateTracker.sendMessage(131183);
                WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDriverLoadedState);
                WifiStateMachine.this.sendMessageDelayed(131083, 5000L);
                continue;
                WifiStateMachine.this.setScanResults(WifiStateMachine.this.mWifiNative.scanResults());
                WifiStateMachine.this.sendScanResultsAvailableBroadcast();
                WifiStateMachine.access$6502(WifiStateMachine.this, false);
                continue;
                boolean bool5 = WifiStateMachine.this.mWifiNative.ping();
                WifiStateMachine localWifiStateMachine5 = WifiStateMachine.this;
                int n = paramMessage.what;
                if (bool5)
                    i = 1;
                localWifiStateMachine5.replyToMessage(paramMessage, n, i);
                continue;
                WifiConfiguration localWifiConfiguration2 = (WifiConfiguration)paramMessage.obj;
                WifiStateMachine.this.replyToMessage(paramMessage, 131124, WifiStateMachine.this.mWifiConfigStore.addOrUpdateNetwork(localWifiConfiguration2));
                continue;
                boolean bool4 = WifiStateMachine.this.mWifiConfigStore.removeNetwork(paramMessage.arg1);
                WifiStateMachine localWifiStateMachine4 = WifiStateMachine.this;
                int m = paramMessage.what;
                if (bool4)
                    i = 1;
                localWifiStateMachine4.replyToMessage(paramMessage, m, i);
                continue;
                WifiConfigStore localWifiConfigStore = WifiStateMachine.this.mWifiConfigStore;
                int j = paramMessage.arg1;
                if (paramMessage.arg2 == 1)
                    bool1 = true;
                boolean bool3 = localWifiConfigStore.enableNetwork(j, bool1);
                WifiStateMachine localWifiStateMachine3 = WifiStateMachine.this;
                int k = paramMessage.what;
                if (bool3)
                    i = 1;
                localWifiStateMachine3.replyToMessage(paramMessage, k, i);
                continue;
                long l = SystemClock.elapsedRealtime();
                if (l - WifiStateMachine.this.mLastEnableAllNetworksTime > 600000L)
                {
                    WifiStateMachine.this.mWifiConfigStore.enableAllNetworks();
                    WifiStateMachine.access$6602(WifiStateMachine.this, l);
                    continue;
                    if (WifiStateMachine.this.mWifiConfigStore.disableNetwork(paramMessage.arg1, 0) == true)
                    {
                        WifiStateMachine.this.replyToMessage(paramMessage, 151571);
                    }
                    else
                    {
                        WifiStateMachine.this.replyToMessage(paramMessage, 151570, 0);
                        continue;
                        WifiStateMachine.this.mWifiNative.addToBlacklist((String)paramMessage.obj);
                        continue;
                        WifiStateMachine.this.mWifiNative.clearBlacklist();
                        continue;
                        boolean bool2 = WifiStateMachine.this.mWifiConfigStore.saveConfig();
                        WifiStateMachine localWifiStateMachine2 = WifiStateMachine.this;
                        if (bool2)
                            i = 1;
                        localWifiStateMachine2.replyToMessage(paramMessage, 131130, i);
                        IBackupManager localIBackupManager = IBackupManager.Stub.asInterface(ServiceManager.getService("backup"));
                        if (localIBackupManager != null)
                        {
                            try
                            {
                                localIBackupManager.dataChanged("com.android.providers.settings");
                            }
                            catch (Exception localException)
                            {
                            }
                            continue;
                            WifiStateMachine.this.replyToMessage(paramMessage, paramMessage.what, WifiStateMachine.this.mWifiConfigStore.getConfiguredNetworks());
                            continue;
                            WifiStateMachine.this.loge("Failed to start soft AP with a running supplicant");
                            WifiStateMachine.this.setWifiApState(14);
                            continue;
                            WifiStateMachine localWifiStateMachine1 = WifiStateMachine.this;
                            if (paramMessage.arg1 == 2)
                                bool1 = true;
                            WifiStateMachine.access$5602(localWifiStateMachine1, bool1);
                            continue;
                            WifiConfiguration localWifiConfiguration1 = (WifiConfiguration)paramMessage.obj;
                            if (WifiStateMachine.this.mWifiConfigStore.saveNetwork(localWifiConfiguration1).getNetworkId() != i)
                            {
                                WifiStateMachine.this.replyToMessage(paramMessage, 151561);
                            }
                            else
                            {
                                WifiStateMachine.this.loge("Failed to save network");
                                WifiStateMachine.this.replyToMessage(paramMessage, 151560, 0);
                                continue;
                                if (WifiStateMachine.this.mWifiConfigStore.forgetNetwork(paramMessage.arg1))
                                {
                                    WifiStateMachine.this.replyToMessage(paramMessage, 151558);
                                }
                                else
                                {
                                    WifiStateMachine.this.loge("Failed to forget network");
                                    WifiStateMachine.this.replyToMessage(paramMessage, 151557, 0);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    class SupplicantStartingState extends State
    {
        SupplicantStartingState()
        {
        }

        private void initializeWpsDetails()
        {
            String str1 = SystemProperties.get("ro.product.name", "");
            if (!WifiStateMachine.this.mWifiNative.setDeviceName(str1))
                WifiStateMachine.this.loge("Failed to set device name " + str1);
            String str2 = SystemProperties.get("ro.product.manufacturer", "");
            if (!WifiStateMachine.this.mWifiNative.setManufacturer(str2))
                WifiStateMachine.this.loge("Failed to set manufacturer " + str2);
            String str3 = SystemProperties.get("ro.product.model", "");
            if (!WifiStateMachine.this.mWifiNative.setModelName(str3))
                WifiStateMachine.this.loge("Failed to set model name " + str3);
            String str4 = SystemProperties.get("ro.product.model", "");
            if (!WifiStateMachine.this.mWifiNative.setModelNumber(str4))
                WifiStateMachine.this.loge("Failed to set model number " + str4);
            String str5 = SystemProperties.get("ro.serialno", "");
            if (!WifiStateMachine.this.mWifiNative.setSerialNumber(str5))
                WifiStateMachine.this.loge("Failed to set serial number " + str5);
            if (!WifiStateMachine.this.mWifiNative.setConfigMethods("physical_display virtual_push_button keypad"))
                WifiStateMachine.this.loge("Failed to set WPS config methods");
            if (!WifiStateMachine.this.mWifiNative.setDeviceType(WifiStateMachine.this.mPrimaryDeviceType))
                WifiStateMachine.this.loge("Failed to set primary device type " + WifiStateMachine.this.mPrimaryDeviceType);
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool = false;
            switch (paramMessage.what)
            {
            default:
                return bool;
            case 147457:
                WifiStateMachine.this.setWifiState(3);
                WifiStateMachine.access$4302(WifiStateMachine.this, 0);
                WifiStateMachine.this.mSupplicantStateTracker.sendMessage(131183);
                WifiStateMachine.access$4502(WifiStateMachine.this, null);
                WifiStateMachine.access$4602(WifiStateMachine.this, -1);
                WifiStateMachine.access$4702(WifiStateMachine.this, -1);
                WifiStateMachine.this.mWifiInfo.setMacAddress(WifiStateMachine.this.mWifiNative.getMacAddress());
                WifiStateMachine.this.mWifiConfigStore.initialize();
                initializeWpsDetails();
                WifiStateMachine.this.sendSupplicantConnectionChangedBroadcast(true);
                WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDriverStartedState);
            case 147458:
            case 131073:
            case 131074:
            case 131083:
            case 131084:
            case 131085:
            case 131086:
            case 131093:
            case 131096:
            case 131144:
            case 131145:
            case 131152:
            case 131156:
            case 131157:
            case 131162:
            }
            while (true)
            {
                bool = true;
                break;
                if (WifiStateMachine.access$4304(WifiStateMachine.this) <= 5)
                {
                    WifiStateMachine.this.loge("Failed to setup control channel, restart supplicant");
                    WifiNative.killSupplicant();
                    WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDriverLoadedState);
                    WifiStateMachine.this.sendMessageDelayed(131083, 5000L);
                }
                else
                {
                    WifiStateMachine.this.loge("Failed " + WifiStateMachine.this.mSupplicantRestartCount + " times to start supplicant, unload driver");
                    WifiStateMachine.access$4302(WifiStateMachine.this, 0);
                    WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDriverLoadedState);
                    WifiStateMachine.this.sendMessage(WifiStateMachine.this.obtainMessage(131074, 4, 0));
                    continue;
                    WifiStateMachine.this.deferMessage(paramMessage);
                }
            }
        }
    }

    class DriverFailedState extends State
    {
        DriverFailedState()
        {
        }

        public void enter()
        {
            WifiStateMachine.this.loge(getName() + "\n");
            EventLog.writeEvent(50021, getName());
        }

        public boolean processMessage(Message paramMessage)
        {
            return false;
        }
    }

    class DriverUnloadedState extends State
    {
        DriverUnloadedState()
        {
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
        }

        public boolean processMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
            case 131073:
            }
            for (boolean bool = false; ; bool = true)
            {
                return bool;
                WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDriverLoadingState);
            }
        }
    }

    class DriverUnloadingState extends State
    {
        DriverUnloadingState()
        {
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
            final Message localMessage = new Message();
            localMessage.copyFrom(WifiStateMachine.this.getCurrentMessage());
            new Thread(new Runnable()
            {
                public void run()
                {
                    WifiStateMachine.this.mWakeLock.acquire();
                    if (WifiNative.unloadDriver())
                    {
                        WifiStateMachine.this.sendMessage(131077);
                        switch (localMessage.arg1)
                        {
                        default:
                        case 1:
                        case 4:
                        case 11:
                        case 14:
                        }
                    }
                    while (true)
                    {
                        WifiStateMachine.this.mWakeLock.release();
                        return;
                        WifiStateMachine.this.setWifiState(localMessage.arg1);
                        continue;
                        WifiStateMachine.this.setWifiApState(localMessage.arg1);
                        continue;
                        WifiStateMachine.this.loge("Failed to unload driver!");
                        WifiStateMachine.this.sendMessage(131078);
                        switch (localMessage.arg1)
                        {
                        default:
                            break;
                        case 1:
                        case 4:
                            WifiStateMachine.this.setWifiState(4);
                            break;
                        case 11:
                        case 14:
                            WifiStateMachine.this.setWifiApState(14);
                        }
                    }
                }
            }).start();
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool;
            switch (paramMessage.what)
            {
            default:
                bool = false;
                return bool;
            case 131077:
                WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDriverUnloadedState);
            case 131078:
            case 131073:
            case 131074:
            case 131083:
            case 131084:
            case 131085:
            case 131086:
            case 131093:
            case 131096:
            case 131144:
            case 131145:
            case 131152:
            case 131156:
            case 131157:
            case 131162:
            }
            while (true)
            {
                bool = true;
                break;
                WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDriverFailedState);
                continue;
                WifiStateMachine.this.deferMessage(paramMessage);
            }
        }
    }

    class DriverLoadedState extends State
    {
        DriverLoadedState()
        {
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool = false;
            switch (paramMessage.what)
            {
            default:
                return bool;
            case 131074:
                WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDriverUnloadingState);
            case 131083:
            case 131093:
            }
            while (true)
            {
                bool = true;
                break;
                try
                {
                    WifiStateMachine.this.mNwService.wifiFirmwareReload(WifiStateMachine.this.mInterfaceName, "STA");
                }
                catch (Exception localException)
                {
                    try
                    {
                        while (true)
                        {
                            WifiStateMachine.this.mNwService.setInterfaceDown(WifiStateMachine.this.mInterfaceName);
                            WifiStateMachine.this.mNwService.setInterfaceIpv6PrivacyExtensions(WifiStateMachine.this.mInterfaceName, true);
                            if (!WifiNative.startSupplicant(WifiStateMachine.this.mP2pSupported))
                                break label263;
                            WifiStateMachine.this.mWifiMonitor.startMonitoring();
                            WifiStateMachine.this.transitionTo(WifiStateMachine.this.mSupplicantStartingState);
                            break;
                            localException = localException;
                            WifiStateMachine.this.loge("Failed to reload STA firmware " + localException);
                        }
                    }
                    catch (RemoteException localRemoteException)
                    {
                        while (true)
                            WifiStateMachine.this.loge("Unable to change interface settings: " + localRemoteException);
                    }
                    catch (IllegalStateException localIllegalStateException)
                    {
                        while (true)
                            WifiStateMachine.this.loge("Unable to change interface settings: " + localIllegalStateException);
                        label263: WifiStateMachine.this.loge("Failed to start supplicant!");
                        WifiStateMachine.this.sendMessage(WifiStateMachine.this.obtainMessage(131074, 4, 0));
                    }
                }
                continue;
                WifiStateMachine.this.transitionTo(WifiStateMachine.this.mSoftApStartingState);
            }
        }
    }

    class DriverLoadingState extends State
    {
        DriverLoadingState()
        {
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
            final Message localMessage = new Message();
            localMessage.copyFrom(WifiStateMachine.this.getCurrentMessage());
            new Thread(new Runnable()
            {
                public void run()
                {
                    WifiStateMachine.this.mWakeLock.acquire();
                    switch (localMessage.arg1)
                    {
                    default:
                    case 2:
                    case 12:
                    }
                    while (WifiNative.loadDriver())
                    {
                        WifiStateMachine.this.sendMessage(131075);
                        WifiStateMachine.this.mWakeLock.release();
                        return;
                        WifiStateMachine.this.setWifiState(2);
                        continue;
                        WifiStateMachine.this.setWifiApState(12);
                    }
                    WifiStateMachine.this.loge("Failed to load driver!");
                    switch (localMessage.arg1)
                    {
                    default:
                    case 2:
                    case 12:
                    }
                    while (true)
                    {
                        WifiStateMachine.this.sendMessage(131076);
                        break;
                        WifiStateMachine.this.setWifiState(4);
                        continue;
                        WifiStateMachine.this.setWifiApState(14);
                    }
                }
            }).start();
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool;
            switch (paramMessage.what)
            {
            default:
                bool = false;
                return bool;
            case 131075:
                WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDriverLoadedState);
            case 131076:
            case 131073:
            case 131074:
            case 131083:
            case 131084:
            case 131085:
            case 131086:
            case 131093:
            case 131096:
            case 131144:
            case 131145:
            case 131152:
            case 131156:
            case 131157:
            case 131162:
            }
            while (true)
            {
                bool = true;
                break;
                WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDriverFailedState);
                continue;
                WifiStateMachine.this.deferMessage(paramMessage);
            }
        }
    }

    class InitialState extends State
    {
        InitialState()
        {
        }

        public void enter()
        {
            EventLog.writeEvent(50021, getName());
            if (WifiNative.isDriverLoaded())
                WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDriverLoadedState);
            while (true)
            {
                WifiStateMachine.access$1802(WifiStateMachine.this, (WifiP2pManager)WifiStateMachine.this.mContext.getSystemService("wifip2p"));
                WifiStateMachine.this.mWifiP2pChannel.connect(WifiStateMachine.this.mContext, WifiStateMachine.this.getHandler(), WifiStateMachine.this.mWifiP2pManager.getMessenger());
                try
                {
                    WifiStateMachine.this.mNwService.disableIpv6(WifiStateMachine.this.mInterfaceName);
                    return;
                    WifiStateMachine.this.transitionTo(WifiStateMachine.this.mDriverUnloadedState);
                }
                catch (RemoteException localRemoteException)
                {
                    while (true)
                        WifiStateMachine.this.loge("Failed to disable IPv6: " + localRemoteException);
                }
                catch (IllegalStateException localIllegalStateException)
                {
                    while (true)
                        WifiStateMachine.this.loge("Failed to disable IPv6: " + localIllegalStateException);
                }
            }
        }
    }

    class DefaultState extends State
    {
        DefaultState()
        {
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool = false;
            switch (paramMessage.what)
            {
            default:
                WifiStateMachine.this.loge("Error! unhandled message" + paramMessage);
            case 131073:
            case 131074:
            case 131083:
            case 131084:
            case 131085:
            case 131086:
            case 131089:
            case 131090:
            case 131091:
            case 131093:
            case 131094:
            case 131095:
            case 131096:
            case 131097:
            case 131098:
            case 131099:
            case 131100:
            case 131101:
            case 131102:
            case 131127:
            case 131128:
            case 131129:
            case 131143:
            case 131144:
            case 131145:
            case 131146:
            case 131147:
            case 131148:
            case 131152:
            case 131155:
            case 131159:
            case 131160:
            case 131162:
            case 135189:
            case 135190:
            case 147457:
            case 147458:
            case 147459:
            case 147460:
            case 147461:
            case 147462:
            case 147463:
            case 147466:
            case 196612:
            case 196613:
            case 69632:
            case 69636:
            case 131103:
            case 131123:
            case 131124:
            case 131125:
            case 131126:
            case 131130:
            case 131131:
            case 131154:
            case 131163:
            case 131149:
            case 131158:
            case 147468:
            case 151553:
            case 151556:
            case 151559:
            case 151562:
            case 151566:
            case 151569:
            case 135191:
            }
            while (true)
            {
                return true;
                if (paramMessage.arg1 == 0)
                {
                    WifiStateMachine.this.mWifiP2pChannel.sendMessage(69633);
                }
                else
                {
                    WifiStateMachine.this.loge("WifiP2pService connection failure, error=" + paramMessage.arg1);
                    continue;
                    WifiStateMachine.this.loge("WifiP2pService channel lost, message.arg1 =" + paramMessage.arg1);
                    continue;
                    WifiStateMachine localWifiStateMachine4 = WifiStateMachine.this;
                    if (paramMessage.arg1 != 0)
                        bool = true;
                    WifiStateMachine.access$602(localWifiStateMachine4, bool);
                    continue;
                    WifiStateMachine.this.replyToMessage(paramMessage, paramMessage.what, -1);
                    continue;
                    WifiStateMachine.this.replyToMessage(paramMessage, paramMessage.what, (List)null);
                    continue;
                    WifiStateMachine localWifiStateMachine3 = WifiStateMachine.this;
                    if (paramMessage.arg1 == 1)
                        bool = true;
                    WifiStateMachine.access$902(localWifiStateMachine3, bool);
                    continue;
                    WifiStateMachine localWifiStateMachine2 = WifiStateMachine.this;
                    if (paramMessage.arg1 == 1)
                        bool = true;
                    WifiStateMachine.access$1002(localWifiStateMachine2, bool);
                    continue;
                    WifiStateMachine localWifiStateMachine1 = WifiStateMachine.this;
                    if (paramMessage.arg1 == 1)
                        bool = true;
                    WifiStateMachine.access$1102(localWifiStateMachine1, bool);
                    continue;
                    WifiStateMachine.this.mSuspendWakeLock.release();
                    continue;
                    WifiStateMachine.this.setWifiEnabled(false);
                    WifiStateMachine.this.setWifiEnabled(true);
                    continue;
                    WifiStateMachine.this.replyToMessage(paramMessage, 151554, 2);
                    continue;
                    WifiStateMachine.this.replyToMessage(paramMessage, 151557, 2);
                    continue;
                    WifiStateMachine.this.replyToMessage(paramMessage, 151560, 2);
                    continue;
                    WifiStateMachine.this.replyToMessage(paramMessage, 151564, 2);
                    continue;
                    WifiStateMachine.this.replyToMessage(paramMessage, 151567, 2);
                    continue;
                    WifiStateMachine.this.replyToMessage(paramMessage, 151570, 2);
                    continue;
                    WifiStateMachine.this.replyToMessage(paramMessage, 135193);
                }
            }
        }
    }

    private class TetherStateChange
    {
        ArrayList<String> active;
        ArrayList<String> available;

        TetherStateChange(ArrayList<String> arg2)
        {
            Object localObject1;
            this.available = localObject1;
            Object localObject2;
            this.active = localObject2;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.WifiStateMachine
 * JD-Core Version:        0.6.2
 */