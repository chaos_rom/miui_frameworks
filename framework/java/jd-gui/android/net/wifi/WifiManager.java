package android.net.wifi;

import android.content.Context;
import android.net.DhcpInfo;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.WorkSource;
import android.util.SparseArray;
import com.android.internal.util.AsyncChannel;
import java.util.List;

public class WifiManager
{
    public static final String ACTION_PICK_WIFI_NETWORK = "android.net.wifi.PICK_WIFI_NETWORK";
    private static final int BASE = 151552;
    public static final int BUSY = 2;
    public static final int CANCEL_WPS = 151566;
    public static final int CANCEL_WPS_FAILED = 151567;
    public static final int CANCEL_WPS_SUCCEDED = 151568;
    public static final int CHANGE_REASON_ADDED = 0;
    public static final int CHANGE_REASON_CONFIG_CHANGE = 2;
    public static final int CHANGE_REASON_REMOVED = 1;
    public static final String CONFIGURED_NETWORKS_CHANGED_ACTION = "android.net.wifi.CONFIGURED_NETWORKS_CHANGE";
    public static final int CONNECT_NETWORK = 151553;
    public static final int CONNECT_NETWORK_FAILED = 151554;
    public static final int CONNECT_NETWORK_SUCCEEDED = 151555;
    public static final int DATA_ACTIVITY_IN = 1;
    public static final int DATA_ACTIVITY_INOUT = 3;
    public static final int DATA_ACTIVITY_NONE = 0;
    public static final int DATA_ACTIVITY_NOTIFICATION = 1;
    public static final int DATA_ACTIVITY_OUT = 2;
    public static final int DISABLE_NETWORK = 151569;
    public static final int DISABLE_NETWORK_FAILED = 151570;
    public static final int DISABLE_NETWORK_SUCCEEDED = 151571;
    public static final int ENABLE_TRAFFIC_STATS_POLL = 151573;
    public static final int ERROR = 0;
    public static final int ERROR_AUTHENTICATING = 1;
    public static final String EXTRA_BSSID = "bssid";
    public static final String EXTRA_CHANGE_REASON = "changeReason";
    public static final String EXTRA_LINK_CAPABILITIES = "linkCapabilities";
    public static final String EXTRA_LINK_PROPERTIES = "linkProperties";
    public static final String EXTRA_MULTIPLE_NETWORKS_CHANGED = "multipleChanges";
    public static final String EXTRA_NETWORK_INFO = "networkInfo";
    public static final String EXTRA_NEW_RSSI = "newRssi";
    public static final String EXTRA_NEW_STATE = "newState";
    public static final String EXTRA_PREVIOUS_WIFI_AP_STATE = "previous_wifi_state";
    public static final String EXTRA_PREVIOUS_WIFI_STATE = "previous_wifi_state";
    public static final String EXTRA_SUPPLICANT_CONNECTED = "connected";
    public static final String EXTRA_SUPPLICANT_ERROR = "supplicantError";
    public static final String EXTRA_WIFI_AP_STATE = "wifi_state";
    public static final String EXTRA_WIFI_CONFIGURATION = "wifiConfiguration";
    public static final String EXTRA_WIFI_INFO = "wifiInfo";
    public static final String EXTRA_WIFI_STATE = "wifi_state";
    public static final int FORGET_NETWORK = 151556;
    public static final int FORGET_NETWORK_FAILED = 151557;
    public static final int FORGET_NETWORK_SUCCEEDED = 151558;
    public static final int IN_PROGRESS = 1;
    public static final String LINK_CONFIGURATION_CHANGED_ACTION = "android.net.wifi.LINK_CONFIGURATION_CHANGED";
    private static final int MAX_ACTIVE_LOCKS = 50;
    private static final int MAX_RSSI = -55;
    private static final int MIN_RSSI = -100;
    public static final String NETWORK_IDS_CHANGED_ACTION = "android.net.wifi.NETWORK_IDS_CHANGED";
    public static final String NETWORK_STATE_CHANGED_ACTION = "android.net.wifi.STATE_CHANGE";
    public static final String RSSI_CHANGED_ACTION = "android.net.wifi.RSSI_CHANGED";
    public static final int RSSI_LEVELS = 5;
    public static final int SAVE_NETWORK = 151559;
    public static final int SAVE_NETWORK_FAILED = 151560;
    public static final int SAVE_NETWORK_SUCCEEDED = 151561;
    public static final String SCAN_RESULTS_AVAILABLE_ACTION = "android.net.wifi.SCAN_RESULTS";
    public static final int START_WPS = 151562;
    public static final int START_WPS_SUCCEEDED = 151563;
    public static final String SUPPLICANT_CONNECTION_CHANGE_ACTION = "android.net.wifi.supplicant.CONNECTION_CHANGE";
    public static final String SUPPLICANT_STATE_CHANGED_ACTION = "android.net.wifi.supplicant.STATE_CHANGE";
    public static final int TRAFFIC_STATS_POLL = 151574;
    public static final String WIFI_AP_STATE_CHANGED_ACTION = "android.net.wifi.WIFI_AP_STATE_CHANGED";
    public static final int WIFI_AP_STATE_DISABLED = 11;
    public static final int WIFI_AP_STATE_DISABLING = 10;
    public static final int WIFI_AP_STATE_ENABLED = 13;
    public static final int WIFI_AP_STATE_ENABLING = 12;
    public static final int WIFI_AP_STATE_FAILED = 14;
    public static final int WIFI_FREQUENCY_BAND_2GHZ = 2;
    public static final int WIFI_FREQUENCY_BAND_5GHZ = 1;
    public static final int WIFI_FREQUENCY_BAND_AUTO = 0;
    public static final int WIFI_MODE_FULL = 1;
    public static final int WIFI_MODE_FULL_HIGH_PERF = 3;
    public static final int WIFI_MODE_SCAN_ONLY = 2;
    public static final String WIFI_STATE_CHANGED_ACTION = "android.net.wifi.WIFI_STATE_CHANGED";
    public static final int WIFI_STATE_DISABLED = 1;
    public static final int WIFI_STATE_DISABLING = 0;
    public static final int WIFI_STATE_ENABLED = 3;
    public static final int WIFI_STATE_ENABLING = 2;
    public static final int WIFI_STATE_UNKNOWN = 4;
    public static final int WPS_AUTH_FAILURE = 6;
    public static final int WPS_COMPLETED = 151565;
    public static final int WPS_FAILED = 151564;
    public static final int WPS_OVERLAP_ERROR = 3;
    public static final int WPS_TIMED_OUT = 7;
    public static final int WPS_TKIP_ONLY_PROHIBITED = 5;
    public static final int WPS_WEP_PROHIBITED = 4;
    private int mActiveLockCount;
    Handler mHandler;
    IWifiManager mService;

    public WifiManager(IWifiManager paramIWifiManager, Handler paramHandler)
    {
        this.mService = paramIWifiManager;
        this.mHandler = paramHandler;
    }

    private int addOrUpdateNetwork(WifiConfiguration paramWifiConfiguration)
    {
        try
        {
            int j = this.mService.addOrUpdateNetwork(paramWifiConfiguration);
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                int i = -1;
        }
    }

    public static int calculateSignalLevel(int paramInt1, int paramInt2)
    {
        int i;
        if (paramInt1 <= -100)
            i = 0;
        while (true)
        {
            return i;
            if (paramInt1 >= -55)
                i = paramInt2 - 1;
            else
                i = (int)((paramInt2 - 1) * (paramInt1 + 100) / 45.0F);
        }
    }

    public static int compareSignalLevel(int paramInt1, int paramInt2)
    {
        return paramInt1 - paramInt2;
    }

    public int addNetwork(WifiConfiguration paramWifiConfiguration)
    {
        int i = -1;
        if (paramWifiConfiguration == null);
        while (true)
        {
            return i;
            paramWifiConfiguration.networkId = i;
            i = addOrUpdateNetwork(paramWifiConfiguration);
        }
    }

    public boolean addToBlacklist(String paramString)
    {
        try
        {
            this.mService.addToBlacklist(paramString);
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public void cancelWps(Channel paramChannel, ActionListener paramActionListener)
    {
        if (paramChannel == null)
            throw new IllegalArgumentException("Channel needs to be initialized");
        paramChannel.mAsyncChannel.sendMessage(151566, 0, paramChannel.putListener(paramActionListener));
    }

    public boolean clearBlacklist()
    {
        try
        {
            this.mService.clearBlacklist();
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public void connect(Channel paramChannel, int paramInt, ActionListener paramActionListener)
    {
        if (paramChannel == null)
            throw new IllegalArgumentException("Channel needs to be initialized");
        if (paramInt < 0)
            throw new IllegalArgumentException("Network id cannot be negative");
        paramChannel.mAsyncChannel.sendMessage(151553, paramInt, paramChannel.putListener(paramActionListener));
    }

    public void connect(Channel paramChannel, WifiConfiguration paramWifiConfiguration, ActionListener paramActionListener)
    {
        if (paramChannel == null)
            throw new IllegalArgumentException("Channel needs to be initialized");
        if (paramWifiConfiguration == null)
            throw new IllegalArgumentException("config cannot be null");
        paramChannel.mAsyncChannel.sendMessage(151553, -1, paramChannel.putListener(paramActionListener), paramWifiConfiguration);
    }

    public MulticastLock createMulticastLock(String paramString)
    {
        return new MulticastLock(paramString, null);
    }

    public WifiLock createWifiLock(int paramInt, String paramString)
    {
        return new WifiLock(paramInt, paramString, null);
    }

    public WifiLock createWifiLock(String paramString)
    {
        return new WifiLock(1, paramString, null);
    }

    public void disable(Channel paramChannel, int paramInt, ActionListener paramActionListener)
    {
        if (paramChannel == null)
            throw new IllegalArgumentException("Channel needs to be initialized");
        if (paramInt < 0)
            throw new IllegalArgumentException("Network id cannot be negative");
        paramChannel.mAsyncChannel.sendMessage(151569, paramInt, paramChannel.putListener(paramActionListener));
    }

    public boolean disableNetwork(int paramInt)
    {
        try
        {
            boolean bool2 = this.mService.disableNetwork(paramInt);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public boolean disconnect()
    {
        try
        {
            this.mService.disconnect();
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public boolean enableNetwork(int paramInt, boolean paramBoolean)
    {
        try
        {
            boolean bool2 = this.mService.enableNetwork(paramInt, paramBoolean);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public void forget(Channel paramChannel, int paramInt, ActionListener paramActionListener)
    {
        if (paramChannel == null)
            throw new IllegalArgumentException("Channel needs to be initialized");
        if (paramInt < 0)
            throw new IllegalArgumentException("Network id cannot be negative");
        paramChannel.mAsyncChannel.sendMessage(151556, paramInt, paramChannel.putListener(paramActionListener));
    }

    public String getConfigFile()
    {
        try
        {
            String str2 = this.mService.getConfigFile();
            str1 = str2;
            return str1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                String str1 = null;
        }
    }

    public List<WifiConfiguration> getConfiguredNetworks()
    {
        try
        {
            List localList2 = this.mService.getConfiguredNetworks();
            localList1 = localList2;
            return localList1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                List localList1 = null;
        }
    }

    public WifiInfo getConnectionInfo()
    {
        try
        {
            WifiInfo localWifiInfo2 = this.mService.getConnectionInfo();
            localWifiInfo1 = localWifiInfo2;
            return localWifiInfo1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                WifiInfo localWifiInfo1 = null;
        }
    }

    public DhcpInfo getDhcpInfo()
    {
        try
        {
            DhcpInfo localDhcpInfo2 = this.mService.getDhcpInfo();
            localDhcpInfo1 = localDhcpInfo2;
            return localDhcpInfo1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                DhcpInfo localDhcpInfo1 = null;
        }
    }

    public int getFrequencyBand()
    {
        try
        {
            int j = this.mService.getFrequencyBand();
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                int i = -1;
        }
    }

    public List<ScanResult> getScanResults()
    {
        try
        {
            List localList2 = this.mService.getScanResults();
            localList1 = localList2;
            return localList1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                List localList1 = null;
        }
    }

    public WifiConfiguration getWifiApConfiguration()
    {
        try
        {
            WifiConfiguration localWifiConfiguration2 = this.mService.getWifiApConfiguration();
            localWifiConfiguration1 = localWifiConfiguration2;
            return localWifiConfiguration1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                WifiConfiguration localWifiConfiguration1 = null;
        }
    }

    public int getWifiApState()
    {
        try
        {
            int j = this.mService.getWifiApEnabledState();
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                int i = 14;
        }
    }

    public Messenger getWifiServiceMessenger()
    {
        try
        {
            Messenger localMessenger2 = this.mService.getWifiServiceMessenger();
            localMessenger1 = localMessenger2;
            return localMessenger1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Messenger localMessenger1 = null;
        }
    }

    public int getWifiState()
    {
        try
        {
            int j = this.mService.getWifiEnabledState();
            i = j;
            return i;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                int i = 4;
        }
    }

    public Messenger getWifiStateMachineMessenger()
    {
        try
        {
            Messenger localMessenger2 = this.mService.getWifiStateMachineMessenger();
            localMessenger1 = localMessenger2;
            return localMessenger1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Messenger localMessenger1 = null;
        }
    }

    public Channel initialize(Context paramContext, Looper paramLooper, ChannelListener paramChannelListener)
    {
        Messenger localMessenger = getWifiServiceMessenger();
        Channel localChannel;
        if (localMessenger == null)
            localChannel = null;
        while (true)
        {
            return localChannel;
            localChannel = new Channel(paramLooper, paramChannelListener);
            if (localChannel.mAsyncChannel.connectSync(paramContext, localChannel.mHandler, localMessenger) != 0)
                localChannel = null;
        }
    }

    public boolean initializeMulticastFiltering()
    {
        try
        {
            this.mService.initializeMulticastFiltering();
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public boolean isDualBandSupported()
    {
        try
        {
            boolean bool2 = this.mService.isDualBandSupported();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public boolean isMulticastEnabled()
    {
        try
        {
            boolean bool2 = this.mService.isMulticastEnabled();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public boolean isWifiApEnabled()
    {
        if (getWifiApState() == 13);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isWifiEnabled()
    {
        if (getWifiState() == 3);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean pingSupplicant()
    {
        boolean bool1 = false;
        if (this.mService == null);
        while (true)
        {
            return bool1;
            try
            {
                boolean bool2 = this.mService.pingSupplicant();
                bool1 = bool2;
            }
            catch (RemoteException localRemoteException)
            {
            }
        }
    }

    public boolean reassociate()
    {
        try
        {
            this.mService.reassociate();
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public boolean reconnect()
    {
        try
        {
            this.mService.reconnect();
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public boolean removeNetwork(int paramInt)
    {
        try
        {
            boolean bool2 = this.mService.removeNetwork(paramInt);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public void save(Channel paramChannel, WifiConfiguration paramWifiConfiguration, ActionListener paramActionListener)
    {
        if (paramChannel == null)
            throw new IllegalArgumentException("Channel needs to be initialized");
        if (paramWifiConfiguration == null)
            throw new IllegalArgumentException("config cannot be null");
        paramChannel.mAsyncChannel.sendMessage(151559, 0, paramChannel.putListener(paramActionListener), paramWifiConfiguration);
    }

    public boolean saveConfiguration()
    {
        try
        {
            boolean bool2 = this.mService.saveConfiguration();
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public void setCountryCode(String paramString, boolean paramBoolean)
    {
        try
        {
            this.mService.setCountryCode(paramString, paramBoolean);
            label11: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label11;
        }
    }

    public void setFrequencyBand(int paramInt, boolean paramBoolean)
    {
        try
        {
            this.mService.setFrequencyBand(paramInt, paramBoolean);
            label11: return;
        }
        catch (RemoteException localRemoteException)
        {
            break label11;
        }
    }

    public boolean setWifiApConfiguration(WifiConfiguration paramWifiConfiguration)
    {
        try
        {
            this.mService.setWifiApConfiguration(paramWifiConfiguration);
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public boolean setWifiApEnabled(WifiConfiguration paramWifiConfiguration, boolean paramBoolean)
    {
        try
        {
            this.mService.setWifiApEnabled(paramWifiConfiguration, paramBoolean);
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public boolean setWifiEnabled(boolean paramBoolean)
    {
        try
        {
            boolean bool2 = this.mService.setWifiEnabled(paramBoolean);
            bool1 = bool2;
            return bool1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool1 = false;
        }
    }

    public boolean startScan()
    {
        boolean bool = false;
        try
        {
            this.mService.startScan(false);
            bool = true;
            label14: return bool;
        }
        catch (RemoteException localRemoteException)
        {
            break label14;
        }
    }

    public boolean startScanActive()
    {
        boolean bool = true;
        try
        {
            this.mService.startScan(true);
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                bool = false;
        }
    }

    public boolean startWifi()
    {
        try
        {
            this.mService.startWifi();
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public void startWps(Channel paramChannel, WpsInfo paramWpsInfo, WpsListener paramWpsListener)
    {
        if (paramChannel == null)
            throw new IllegalArgumentException("Channel needs to be initialized");
        if (paramWpsInfo == null)
            throw new IllegalArgumentException("config cannot be null");
        paramChannel.mAsyncChannel.sendMessage(151562, 0, paramChannel.putListener(paramWpsListener), paramWpsInfo);
    }

    public boolean stopWifi()
    {
        try
        {
            this.mService.stopWifi();
            bool = true;
            return bool;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                boolean bool = false;
        }
    }

    public int updateNetwork(WifiConfiguration paramWifiConfiguration)
    {
        if ((paramWifiConfiguration == null) || (paramWifiConfiguration.networkId < 0));
        for (int i = -1; ; i = addOrUpdateNetwork(paramWifiConfiguration))
            return i;
    }

    public class MulticastLock
    {
        private final IBinder mBinder;
        private boolean mHeld;
        private int mRefCount;
        private boolean mRefCounted;
        private String mTag;

        private MulticastLock(String arg2)
        {
            Object localObject;
            this.mTag = localObject;
            this.mBinder = new Binder();
            this.mRefCount = 0;
            this.mRefCounted = true;
            this.mHeld = false;
        }

        // ERROR //
        public void acquire()
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 32	android/net/wifi/WifiManager$MulticastLock:mBinder	Landroid/os/IBinder;
            //     4: astore_1
            //     5: aload_1
            //     6: monitorenter
            //     7: aload_0
            //     8: getfield 36	android/net/wifi/WifiManager$MulticastLock:mRefCounted	Z
            //     11: ifeq +104 -> 115
            //     14: iconst_1
            //     15: aload_0
            //     16: getfield 34	android/net/wifi/WifiManager$MulticastLock:mRefCount	I
            //     19: iadd
            //     20: istore 8
            //     22: aload_0
            //     23: iload 8
            //     25: putfield 34	android/net/wifi/WifiManager$MulticastLock:mRefCount	I
            //     28: iload 8
            //     30: iconst_1
            //     31: if_icmpne +81 -> 112
            //     34: aload_0
            //     35: getfield 22	android/net/wifi/WifiManager$MulticastLock:this$0	Landroid/net/wifi/WifiManager;
            //     38: getfield 48	android/net/wifi/WifiManager:mService	Landroid/net/wifi/IWifiManager;
            //     41: aload_0
            //     42: getfield 32	android/net/wifi/WifiManager$MulticastLock:mBinder	Landroid/os/IBinder;
            //     45: aload_0
            //     46: getfield 27	android/net/wifi/WifiManager$MulticastLock:mTag	Ljava/lang/String;
            //     49: invokeinterface 54 3 0
            //     54: aload_0
            //     55: getfield 22	android/net/wifi/WifiManager$MulticastLock:this$0	Landroid/net/wifi/WifiManager;
            //     58: astore 5
            //     60: aload 5
            //     62: monitorenter
            //     63: aload_0
            //     64: getfield 22	android/net/wifi/WifiManager$MulticastLock:this$0	Landroid/net/wifi/WifiManager;
            //     67: invokestatic 58	android/net/wifi/WifiManager:access$300	(Landroid/net/wifi/WifiManager;)I
            //     70: bipush 50
            //     72: if_icmplt +55 -> 127
            //     75: aload_0
            //     76: getfield 22	android/net/wifi/WifiManager$MulticastLock:this$0	Landroid/net/wifi/WifiManager;
            //     79: getfield 48	android/net/wifi/WifiManager:mService	Landroid/net/wifi/IWifiManager;
            //     82: invokeinterface 61 1 0
            //     87: new 63	java/lang/UnsupportedOperationException
            //     90: dup
            //     91: ldc 65
            //     93: invokespecial 68	java/lang/UnsupportedOperationException:<init>	(Ljava/lang/String;)V
            //     96: athrow
            //     97: astore 6
            //     99: aload 5
            //     101: monitorexit
            //     102: aload 6
            //     104: athrow
            //     105: astore 4
            //     107: aload_0
            //     108: iconst_1
            //     109: putfield 38	android/net/wifi/WifiManager$MulticastLock:mHeld	Z
            //     112: aload_1
            //     113: monitorexit
            //     114: return
            //     115: aload_0
            //     116: getfield 38	android/net/wifi/WifiManager$MulticastLock:mHeld	Z
            //     119: istore_3
            //     120: iload_3
            //     121: ifne -9 -> 112
            //     124: goto -90 -> 34
            //     127: aload_0
            //     128: getfield 22	android/net/wifi/WifiManager$MulticastLock:this$0	Landroid/net/wifi/WifiManager;
            //     131: invokestatic 71	android/net/wifi/WifiManager:access$308	(Landroid/net/wifi/WifiManager;)I
            //     134: pop
            //     135: aload 5
            //     137: monitorexit
            //     138: goto -31 -> 107
            //     141: astore_2
            //     142: aload_1
            //     143: monitorexit
            //     144: aload_2
            //     145: athrow
            //
            // Exception table:
            //     from	to	target	type
            //     63	102	97	finally
            //     127	138	97	finally
            //     34	63	105	android/os/RemoteException
            //     102	105	105	android/os/RemoteException
            //     7	28	141	finally
            //     34	63	141	finally
            //     102	105	141	finally
            //     107	120	141	finally
            //     142	144	141	finally
        }

        protected void finalize()
            throws Throwable
        {
            super.finalize();
            setReferenceCounted(false);
            release();
        }

        public boolean isHeld()
        {
            synchronized (this.mBinder)
            {
                boolean bool = this.mHeld;
                return bool;
            }
        }

        public void release()
        {
            synchronized (this.mBinder)
            {
                if (this.mRefCounted)
                {
                    int i = -1 + this.mRefCount;
                    this.mRefCount = i;
                    if (i != 0);
                }
            }
            try
            {
                while (true)
                {
                    WifiManager.this.mService.releaseMulticastLock();
                    synchronized (WifiManager.this)
                    {
                        WifiManager.access$310(WifiManager.this);
                        label66: this.mHeld = false;
                        boolean bool;
                        do
                        {
                            if (this.mRefCount >= 0)
                                break;
                            throw new RuntimeException("MulticastLock under-locked " + this.mTag);
                            localObject1 = finally;
                            throw localObject1;
                            bool = this.mHeld;
                        }
                        while (!bool);
                    }
                }
            }
            catch (RemoteException localRemoteException)
            {
                break label66;
            }
        }

        public void setReferenceCounted(boolean paramBoolean)
        {
            this.mRefCounted = paramBoolean;
        }

        public String toString()
        {
            while (true)
            {
                synchronized (this.mBinder)
                {
                    String str1 = Integer.toHexString(System.identityHashCode(this));
                    if (this.mHeld)
                    {
                        str2 = "held; ";
                        if (!this.mRefCounted)
                            break label115;
                        str3 = "refcounted: refcount = " + this.mRefCount;
                        String str4 = "MulticastLock{ " + str1 + "; " + str2 + str3 + " }";
                        return str4;
                    }
                }
                String str2 = "";
                continue;
                label115: String str3 = "not refcounted";
            }
        }
    }

    public class WifiLock
    {
        private final IBinder mBinder;
        private boolean mHeld;
        int mLockType;
        private int mRefCount;
        private boolean mRefCounted;
        private String mTag;
        private WorkSource mWorkSource;

        private WifiLock(int paramString, String arg3)
        {
            Object localObject;
            this.mTag = localObject;
            this.mLockType = paramString;
            this.mBinder = new Binder();
            this.mRefCount = 0;
            this.mRefCounted = true;
            this.mHeld = false;
        }

        // ERROR //
        public void acquire()
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 37	android/net/wifi/WifiManager$WifiLock:mBinder	Landroid/os/IBinder;
            //     4: astore_1
            //     5: aload_1
            //     6: monitorenter
            //     7: aload_0
            //     8: getfield 41	android/net/wifi/WifiManager$WifiLock:mRefCounted	Z
            //     11: ifeq +118 -> 129
            //     14: iconst_1
            //     15: aload_0
            //     16: getfield 39	android/net/wifi/WifiManager$WifiLock:mRefCount	I
            //     19: iadd
            //     20: istore 10
            //     22: aload_0
            //     23: iload 10
            //     25: putfield 39	android/net/wifi/WifiManager$WifiLock:mRefCount	I
            //     28: iload 10
            //     30: iconst_1
            //     31: if_icmpne +95 -> 126
            //     34: aload_0
            //     35: getfield 25	android/net/wifi/WifiManager$WifiLock:this$0	Landroid/net/wifi/WifiManager;
            //     38: getfield 53	android/net/wifi/WifiManager:mService	Landroid/net/wifi/IWifiManager;
            //     41: aload_0
            //     42: getfield 37	android/net/wifi/WifiManager$WifiLock:mBinder	Landroid/os/IBinder;
            //     45: aload_0
            //     46: getfield 32	android/net/wifi/WifiManager$WifiLock:mLockType	I
            //     49: aload_0
            //     50: getfield 30	android/net/wifi/WifiManager$WifiLock:mTag	Ljava/lang/String;
            //     53: aload_0
            //     54: getfield 55	android/net/wifi/WifiManager$WifiLock:mWorkSource	Landroid/os/WorkSource;
            //     57: invokeinterface 61 5 0
            //     62: pop
            //     63: aload_0
            //     64: getfield 25	android/net/wifi/WifiManager$WifiLock:this$0	Landroid/net/wifi/WifiManager;
            //     67: astore 6
            //     69: aload 6
            //     71: monitorenter
            //     72: aload_0
            //     73: getfield 25	android/net/wifi/WifiManager$WifiLock:this$0	Landroid/net/wifi/WifiManager;
            //     76: invokestatic 65	android/net/wifi/WifiManager:access$300	(Landroid/net/wifi/WifiManager;)I
            //     79: bipush 50
            //     81: if_icmplt +60 -> 141
            //     84: aload_0
            //     85: getfield 25	android/net/wifi/WifiManager$WifiLock:this$0	Landroid/net/wifi/WifiManager;
            //     88: getfield 53	android/net/wifi/WifiManager:mService	Landroid/net/wifi/IWifiManager;
            //     91: aload_0
            //     92: getfield 37	android/net/wifi/WifiManager$WifiLock:mBinder	Landroid/os/IBinder;
            //     95: invokeinterface 69 2 0
            //     100: pop
            //     101: new 71	java/lang/UnsupportedOperationException
            //     104: dup
            //     105: ldc 73
            //     107: invokespecial 76	java/lang/UnsupportedOperationException:<init>	(Ljava/lang/String;)V
            //     110: athrow
            //     111: astore 7
            //     113: aload 6
            //     115: monitorexit
            //     116: aload 7
            //     118: athrow
            //     119: astore 4
            //     121: aload_0
            //     122: iconst_1
            //     123: putfield 43	android/net/wifi/WifiManager$WifiLock:mHeld	Z
            //     126: aload_1
            //     127: monitorexit
            //     128: return
            //     129: aload_0
            //     130: getfield 43	android/net/wifi/WifiManager$WifiLock:mHeld	Z
            //     133: istore_3
            //     134: iload_3
            //     135: ifne -9 -> 126
            //     138: goto -104 -> 34
            //     141: aload_0
            //     142: getfield 25	android/net/wifi/WifiManager$WifiLock:this$0	Landroid/net/wifi/WifiManager;
            //     145: invokestatic 79	android/net/wifi/WifiManager:access$308	(Landroid/net/wifi/WifiManager;)I
            //     148: pop
            //     149: aload 6
            //     151: monitorexit
            //     152: goto -31 -> 121
            //     155: astore_2
            //     156: aload_1
            //     157: monitorexit
            //     158: aload_2
            //     159: athrow
            //
            // Exception table:
            //     from	to	target	type
            //     72	116	111	finally
            //     141	152	111	finally
            //     34	72	119	android/os/RemoteException
            //     116	119	119	android/os/RemoteException
            //     7	28	155	finally
            //     34	72	155	finally
            //     116	119	155	finally
            //     121	134	155	finally
            //     156	158	155	finally
        }

        protected void finalize()
            throws Throwable
        {
            super.finalize();
            synchronized (this.mBinder)
            {
                boolean bool = this.mHeld;
                if (bool);
                try
                {
                    WifiManager.this.mService.releaseWifiLock(this.mBinder);
                    synchronized (WifiManager.this)
                    {
                        WifiManager.access$310(WifiManager.this);
                        label57: return;
                    }
                }
                catch (RemoteException localRemoteException)
                {
                    break label57;
                }
            }
        }

        public boolean isHeld()
        {
            synchronized (this.mBinder)
            {
                boolean bool = this.mHeld;
                return bool;
            }
        }

        public void release()
        {
            synchronized (this.mBinder)
            {
                if (this.mRefCounted)
                {
                    int i = -1 + this.mRefCount;
                    this.mRefCount = i;
                    if (i != 0);
                }
            }
            try
            {
                while (true)
                {
                    WifiManager.this.mService.releaseWifiLock(this.mBinder);
                    synchronized (WifiManager.this)
                    {
                        WifiManager.access$310(WifiManager.this);
                        label71: this.mHeld = false;
                        boolean bool;
                        do
                        {
                            if (this.mRefCount >= 0)
                                break;
                            throw new RuntimeException("WifiLock under-locked " + this.mTag);
                            localObject1 = finally;
                            throw localObject1;
                            bool = this.mHeld;
                        }
                        while (!bool);
                    }
                }
            }
            catch (RemoteException localRemoteException)
            {
                break label71;
            }
        }

        public void setReferenceCounted(boolean paramBoolean)
        {
            this.mRefCounted = paramBoolean;
        }

        // ERROR //
        public void setWorkSource(WorkSource paramWorkSource)
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 37	android/net/wifi/WifiManager$WifiLock:mBinder	Landroid/os/IBinder;
            //     4: astore_2
            //     5: aload_2
            //     6: monitorenter
            //     7: aload_1
            //     8: ifnull +12 -> 20
            //     11: aload_1
            //     12: invokevirtual 116	android/os/WorkSource:size	()I
            //     15: ifne +5 -> 20
            //     18: aconst_null
            //     19: astore_1
            //     20: iconst_1
            //     21: istore_3
            //     22: aload_1
            //     23: ifnonnull +46 -> 69
            //     26: aload_0
            //     27: aconst_null
            //     28: putfield 55	android/net/wifi/WifiManager$WifiLock:mWorkSource	Landroid/os/WorkSource;
            //     31: iload_3
            //     32: ifeq +34 -> 66
            //     35: aload_0
            //     36: getfield 43	android/net/wifi/WifiManager$WifiLock:mHeld	Z
            //     39: istore 5
            //     41: iload 5
            //     43: ifeq +23 -> 66
            //     46: aload_0
            //     47: getfield 25	android/net/wifi/WifiManager$WifiLock:this$0	Landroid/net/wifi/WifiManager;
            //     50: getfield 53	android/net/wifi/WifiManager:mService	Landroid/net/wifi/IWifiManager;
            //     53: aload_0
            //     54: getfield 37	android/net/wifi/WifiManager$WifiLock:mBinder	Landroid/os/IBinder;
            //     57: aload_0
            //     58: getfield 55	android/net/wifi/WifiManager$WifiLock:mWorkSource	Landroid/os/WorkSource;
            //     61: invokeinterface 120 3 0
            //     66: aload_2
            //     67: monitorexit
            //     68: return
            //     69: aload_0
            //     70: getfield 55	android/net/wifi/WifiManager$WifiLock:mWorkSource	Landroid/os/WorkSource;
            //     73: ifnonnull +39 -> 112
            //     76: aload_0
            //     77: getfield 55	android/net/wifi/WifiManager$WifiLock:mWorkSource	Landroid/os/WorkSource;
            //     80: ifnull +27 -> 107
            //     83: iconst_1
            //     84: istore_3
            //     85: aload_0
            //     86: new 112	android/os/WorkSource
            //     89: dup
            //     90: aload_1
            //     91: invokespecial 122	android/os/WorkSource:<init>	(Landroid/os/WorkSource;)V
            //     94: putfield 55	android/net/wifi/WifiManager$WifiLock:mWorkSource	Landroid/os/WorkSource;
            //     97: goto -66 -> 31
            //     100: astore 4
            //     102: aload_2
            //     103: monitorexit
            //     104: aload 4
            //     106: athrow
            //     107: iconst_0
            //     108: istore_3
            //     109: goto -24 -> 85
            //     112: aload_0
            //     113: getfield 55	android/net/wifi/WifiManager$WifiLock:mWorkSource	Landroid/os/WorkSource;
            //     116: aload_1
            //     117: invokevirtual 126	android/os/WorkSource:diff	(Landroid/os/WorkSource;)Z
            //     120: istore_3
            //     121: iload_3
            //     122: ifeq -91 -> 31
            //     125: aload_0
            //     126: getfield 55	android/net/wifi/WifiManager$WifiLock:mWorkSource	Landroid/os/WorkSource;
            //     129: aload_1
            //     130: invokevirtual 129	android/os/WorkSource:set	(Landroid/os/WorkSource;)V
            //     133: goto -102 -> 31
            //     136: astore 6
            //     138: goto -72 -> 66
            //
            // Exception table:
            //     from	to	target	type
            //     11	41	100	finally
            //     46	66	100	finally
            //     66	104	100	finally
            //     112	133	100	finally
            //     46	66	136	android/os/RemoteException
        }

        public String toString()
        {
            while (true)
            {
                synchronized (this.mBinder)
                {
                    String str1 = Integer.toHexString(System.identityHashCode(this));
                    if (this.mHeld)
                    {
                        str2 = "held; ";
                        if (!this.mRefCounted)
                            break label115;
                        str3 = "refcounted: refcount = " + this.mRefCount;
                        String str4 = "WifiLock{ " + str1 + "; " + str2 + str3 + " }";
                        return str4;
                    }
                }
                String str2 = "";
                continue;
                label115: String str3 = "not refcounted";
            }
        }
    }

    public static class Channel
    {
        private static final int INVALID_KEY = -1;
        AsyncChannel mAsyncChannel = new AsyncChannel();
        private WifiManager.ChannelListener mChannelListener;
        WifiHandler mHandler;
        private int mListenerKey = 0;
        private SparseArray<Object> mListenerMap = new SparseArray();
        private Object mListenerMapLock = new Object();

        Channel(Looper paramLooper, WifiManager.ChannelListener paramChannelListener)
        {
            this.mHandler = new WifiHandler(paramLooper);
            this.mChannelListener = paramChannelListener;
        }

        int putListener(Object paramObject)
        {
            int i;
            if (paramObject == null)
                i = -1;
            while (true)
            {
                return i;
                synchronized (this.mListenerMapLock)
                {
                    do
                    {
                        i = this.mListenerKey;
                        this.mListenerKey = (i + 1);
                    }
                    while (i == -1);
                    this.mListenerMap.put(i, paramObject);
                }
            }
        }

        Object removeListener(int paramInt)
        {
            Object localObject3;
            if (paramInt == -1)
                localObject3 = null;
            while (true)
            {
                return localObject3;
                synchronized (this.mListenerMapLock)
                {
                    localObject3 = this.mListenerMap.get(paramInt);
                    this.mListenerMap.remove(paramInt);
                }
            }
        }

        class WifiHandler extends Handler
        {
            WifiHandler(Looper arg2)
            {
                super();
            }

            public void handleMessage(Message paramMessage)
            {
                Object localObject1 = WifiManager.Channel.this.removeListener(paramMessage.arg2);
                switch (paramMessage.what)
                {
                default:
                case 69636:
                case 151554:
                case 151557:
                case 151560:
                case 151567:
                case 151570:
                case 151555:
                case 151558:
                case 151561:
                case 151568:
                case 151571:
                case 151563:
                case 151565:
                case 151564:
                }
                while (true)
                {
                    return;
                    if (WifiManager.Channel.this.mChannelListener != null)
                    {
                        WifiManager.Channel.this.mChannelListener.onChannelDisconnected();
                        WifiManager.Channel.access$002(WifiManager.Channel.this, null);
                        continue;
                        if (localObject1 != null)
                        {
                            ((WifiManager.ActionListener)localObject1).onFailure(paramMessage.arg1);
                            continue;
                            if (localObject1 != null)
                            {
                                ((WifiManager.ActionListener)localObject1).onSuccess();
                                continue;
                                if (localObject1 != null)
                                {
                                    WpsResult localWpsResult = (WpsResult)paramMessage.obj;
                                    ((WifiManager.WpsListener)localObject1).onStartSuccess(localWpsResult.pin);
                                    synchronized (WifiManager.Channel.this.mListenerMapLock)
                                    {
                                        WifiManager.Channel.this.mListenerMap.put(paramMessage.arg2, localObject1);
                                    }
                                    if (localObject1 != null)
                                    {
                                        ((WifiManager.WpsListener)localObject1).onCompletion();
                                        continue;
                                        if (localObject1 != null)
                                            ((WifiManager.WpsListener)localObject1).onFailure(paramMessage.arg1);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static abstract interface WpsListener
    {
        public abstract void onCompletion();

        public abstract void onFailure(int paramInt);

        public abstract void onStartSuccess(String paramString);
    }

    public static abstract interface ActionListener
    {
        public abstract void onFailure(int paramInt);

        public abstract void onSuccess();
    }

    public static abstract interface ChannelListener
    {
        public abstract void onChannelDisconnected();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.WifiManager
 * JD-Core Version:        0.6.2
 */