package android.net.wifi;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class WpsResult
    implements Parcelable
{
    public static final Parcelable.Creator<WpsResult> CREATOR = new Parcelable.Creator()
    {
        public WpsResult createFromParcel(Parcel paramAnonymousParcel)
        {
            WpsResult localWpsResult = new WpsResult();
            localWpsResult.status = WpsResult.Status.valueOf(paramAnonymousParcel.readString());
            localWpsResult.pin = paramAnonymousParcel.readString();
            return localWpsResult;
        }

        public WpsResult[] newArray(int paramAnonymousInt)
        {
            return new WpsResult[paramAnonymousInt];
        }
    };
    public String pin;
    public Status status;

    public WpsResult()
    {
        this.status = Status.FAILURE;
        this.pin = null;
    }

    public WpsResult(Status paramStatus)
    {
        this.status = paramStatus;
        this.pin = null;
    }

    public WpsResult(WpsResult paramWpsResult)
    {
        if (paramWpsResult != null)
        {
            this.status = paramWpsResult.status;
            this.pin = paramWpsResult.pin;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public String toString()
    {
        StringBuffer localStringBuffer = new StringBuffer();
        localStringBuffer.append(" status: ").append(this.status.toString());
        localStringBuffer.append('\n');
        localStringBuffer.append(" pin: ").append(this.pin);
        localStringBuffer.append("\n");
        return localStringBuffer.toString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.status.name());
        paramParcel.writeString(this.pin);
    }

    public static enum Status
    {
        static
        {
            FAILURE = new Status("FAILURE", 1);
            IN_PROGRESS = new Status("IN_PROGRESS", 2);
            Status[] arrayOfStatus = new Status[3];
            arrayOfStatus[0] = SUCCESS;
            arrayOfStatus[1] = FAILURE;
            arrayOfStatus[2] = IN_PROGRESS;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.WpsResult
 * JD-Core Version:        0.6.2
 */