package android.net.wifi.p2p;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Notification;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.DhcpInfoInternal;
import android.net.DhcpStateMachine;
import android.net.InterfaceConfiguration;
import android.net.LinkAddress;
import android.net.NetworkInfo;
import android.net.NetworkInfo.DetailedState;
import android.net.NetworkUtils;
import android.net.wifi.WifiMonitor;
import android.net.wifi.WifiNative;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.nsd.WifiP2pServiceInfo;
import android.net.wifi.p2p.nsd.WifiP2pServiceRequest;
import android.net.wifi.p2p.nsd.WifiP2pServiceResponse;
import android.os.Binder;
import android.os.Bundle;
import android.os.INetworkManagementService;
import android.os.INetworkManagementService.Stub;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.util.Slog;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import com.android.internal.util.AsyncChannel;
import com.android.internal.util.State;
import com.android.internal.util.StateMachine;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class WifiP2pService extends IWifiP2pManager.Stub
{
    private static final int BASE = 143360;
    private static final boolean DBG = false;
    private static final String[] DHCP_RANGE = arrayOfString;
    private static final int DISCOVER_TIMEOUT_S = 120;
    private static final Boolean FORM_GROUP;
    public static final int GROUP_CREATING_TIMED_OUT = 143361;
    private static final int GROUP_CREATING_WAIT_TIME_MS = 120000;
    private static final int GROUP_IDLE_TIME_S = 2;
    private static final Boolean JOIN_GROUP = Boolean.valueOf(true);
    private static final String NETWORKTYPE = "WIFI_P2P";
    private static final int P2P_RESTART_INTERVAL_MSECS = 5000;
    private static final int P2P_RESTART_TRIES = 5;
    private static final int PEER_CONNECTION_USER_ACCEPT = 143362;
    private static final int PEER_CONNECTION_USER_REJECT = 143363;
    private static final String SERVER_ADDRESS = "192.168.49.1";
    private static final String TAG = "WifiP2pService";
    private static int mGroupCreatingTimeoutIndex;
    private ActivityManager mActivityMgr;
    private boolean mAutonomousGroup;
    private HashMap<Messenger, ClientInfo> mClientInfoList = new HashMap();
    private Context mContext;
    private DhcpStateMachine mDhcpStateMachine;
    private boolean mDiscoveryStarted;
    private Messenger mForegroundAppMessenger;
    private String mForegroundAppPkgName;
    private String mInterface;
    private boolean mJoinExistingGroup;
    private NetworkInfo mNetworkInfo;
    private Notification mNotification;
    INetworkManagementService mNwService;
    private int mP2pRestartCount = 0;
    private P2pStateMachine mP2pStateMachine;
    private final boolean mP2pSupported;
    private AsyncChannel mReplyChannel = new AsyncChannel();
    private String mServiceDiscReqId;
    private byte mServiceTransactionId = 0;
    private WifiP2pDevice mThisDevice = new WifiP2pDevice();
    private AsyncChannel mWifiChannel;

    static
    {
        FORM_GROUP = Boolean.valueOf(false);
        mGroupCreatingTimeoutIndex = 0;
        String[] arrayOfString = new String[2];
        arrayOfString[0] = "192.168.49.2";
        arrayOfString[1] = "192.168.49.254";
    }

    public WifiP2pService(Context paramContext)
    {
        this.mContext = paramContext;
        this.mInterface = "p2p0";
        this.mActivityMgr = ((ActivityManager)paramContext.getSystemService("activity"));
        this.mNetworkInfo = new NetworkInfo(13, 0, "WIFI_P2P", "");
        this.mP2pSupported = this.mContext.getPackageManager().hasSystemFeature("android.hardware.wifi.direct");
        this.mThisDevice.primaryDeviceType = this.mContext.getResources().getString(17039386);
        this.mP2pStateMachine = new P2pStateMachine("WifiP2pService", this.mP2pSupported);
        this.mP2pStateMachine.start();
    }

    private void enforceAccessPermission()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE", "WifiP2pService");
    }

    private void enforceChangePermission()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CHANGE_WIFI_STATE", "WifiP2pService");
    }

    public void connectivityServiceReady()
    {
        this.mNwService = INetworkManagementService.Stub.asInterface(ServiceManager.getService("network_management"));
    }

    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.DUMP") != 0)
            paramPrintWriter.println("Permission Denial: can't dump WifiP2pService from from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid());
    }

    public Messenger getMessenger()
    {
        enforceAccessPermission();
        enforceChangePermission();
        return new Messenger(this.mP2pStateMachine.getHandler());
    }

    private class ClientInfo
    {
        private Messenger mMessenger;
        private SparseArray<WifiP2pServiceRequest> mReqList;
        private List<WifiP2pServiceInfo> mServList;

        private ClientInfo(Messenger arg2)
        {
            Object localObject;
            this.mMessenger = localObject;
            this.mReqList = new SparseArray();
            this.mServList = new ArrayList();
        }
    }

    private class P2pStateMachine extends StateMachine
    {
        private DefaultState mDefaultState = new DefaultState();
        private WifiP2pGroup mGroup;
        private GroupCreatedState mGroupCreatedState = new GroupCreatedState();
        private GroupCreatingState mGroupCreatingState = new GroupCreatingState();
        private GroupNegotiationState mGroupNegotiationState = new GroupNegotiationState();
        private InactiveState mInactiveState = new InactiveState();
        private P2pDisabledState mP2pDisabledState = new P2pDisabledState();
        private P2pDisablingState mP2pDisablingState = new P2pDisablingState();
        private P2pEnabledState mP2pEnabledState = new P2pEnabledState();
        private P2pEnablingState mP2pEnablingState = new P2pEnablingState();
        private P2pNotSupportedState mP2pNotSupportedState = new P2pNotSupportedState();
        private WifiP2pDeviceList mPeers = new WifiP2pDeviceList();
        private ProvisionDiscoveryState mProvisionDiscoveryState = new ProvisionDiscoveryState();
        private WifiP2pGroup mSavedP2pGroup;
        private WifiP2pConfig mSavedPeerConfig;
        private WifiP2pDevice mSavedProvDiscDevice;
        private UserAuthorizingInvitationState mUserAuthorizingInvitationState = new UserAuthorizingInvitationState();
        private UserAuthorizingJoinState mUserAuthorizingJoinState = new UserAuthorizingJoinState();
        private WifiMonitor mWifiMonitor = new WifiMonitor(this, this.mWifiNative);
        private WifiNative mWifiNative = new WifiNative(WifiP2pService.this.mInterface);
        private WifiP2pInfo mWifiP2pInfo = new WifiP2pInfo();

        P2pStateMachine(String paramBoolean, boolean arg3)
        {
            super();
            addState(this.mDefaultState);
            addState(this.mP2pNotSupportedState, this.mDefaultState);
            addState(this.mP2pDisablingState, this.mDefaultState);
            addState(this.mP2pDisabledState, this.mDefaultState);
            addState(this.mP2pEnablingState, this.mDefaultState);
            addState(this.mP2pEnabledState, this.mDefaultState);
            addState(this.mInactiveState, this.mP2pEnabledState);
            addState(this.mGroupCreatingState, this.mP2pEnabledState);
            addState(this.mUserAuthorizingInvitationState, this.mGroupCreatingState);
            addState(this.mProvisionDiscoveryState, this.mGroupCreatingState);
            addState(this.mGroupNegotiationState, this.mGroupCreatingState);
            addState(this.mGroupCreatedState, this.mP2pEnabledState);
            addState(this.mUserAuthorizingJoinState, this.mGroupCreatedState);
            int i;
            if (i != 0)
                setInitialState(this.mP2pDisabledState);
            while (true)
            {
                return;
                setInitialState(this.mP2pNotSupportedState);
            }
        }

        private boolean addLocalService(Messenger paramMessenger, WifiP2pServiceInfo paramWifiP2pServiceInfo)
        {
            boolean bool = false;
            clearClientDeadChannels();
            WifiP2pService.ClientInfo localClientInfo = getClientInfo(paramMessenger, true);
            if (localClientInfo == null);
            while (true)
            {
                return bool;
                if (localClientInfo.mServList.add(paramWifiP2pServiceInfo))
                    if (!this.mWifiNative.p2pServiceAdd(paramWifiP2pServiceInfo))
                        localClientInfo.mServList.remove(paramWifiP2pServiceInfo);
                    else
                        bool = true;
            }
        }

        private void addRowToDialog(ViewGroup paramViewGroup, int paramInt, String paramString)
        {
            Resources localResources = Resources.getSystem();
            View localView = LayoutInflater.from(WifiP2pService.this.mContext).inflate(17367242, paramViewGroup, false);
            ((TextView)localView.findViewById(16909162)).setText(localResources.getString(paramInt));
            ((TextView)localView.findViewById(16908946)).setText(paramString);
            paramViewGroup.addView(localView);
        }

        private boolean addServiceRequest(Messenger paramMessenger, WifiP2pServiceRequest paramWifiP2pServiceRequest)
        {
            boolean bool = true;
            clearClientDeadChannels();
            WifiP2pService.ClientInfo localClientInfo = getClientInfo(paramMessenger, bool);
            if (localClientInfo == null)
                bool = false;
            while (true)
            {
                return bool;
                WifiP2pService.access$9204(WifiP2pService.this);
                if (WifiP2pService.this.mServiceTransactionId == 0)
                    WifiP2pService.access$9204(WifiP2pService.this);
                paramWifiP2pServiceRequest.setTransactionId(WifiP2pService.this.mServiceTransactionId);
                localClientInfo.mReqList.put(WifiP2pService.this.mServiceTransactionId, paramWifiP2pServiceRequest);
                if (WifiP2pService.this.mServiceDiscReqId != null)
                    bool = updateSupplicantServiceRequest();
            }
        }

        private void clearClientDeadChannels()
        {
            ArrayList localArrayList = new ArrayList();
            Iterator localIterator1 = WifiP2pService.this.mClientInfoList.values().iterator();
            while (localIterator1.hasNext())
            {
                WifiP2pService.ClientInfo localClientInfo = (WifiP2pService.ClientInfo)localIterator1.next();
                Message localMessage = Message.obtain();
                localMessage.what = 139313;
                localMessage.arg1 = 0;
                localMessage.arg2 = 0;
                localMessage.obj = null;
                try
                {
                    localClientInfo.mMessenger.send(localMessage);
                }
                catch (RemoteException localRemoteException)
                {
                    localArrayList.add(localClientInfo.mMessenger);
                }
            }
            Iterator localIterator2 = localArrayList.iterator();
            while (localIterator2.hasNext())
                clearClientInfo((Messenger)localIterator2.next());
        }

        private void clearClientInfo(Messenger paramMessenger)
        {
            clearLocalServices(paramMessenger);
            clearServiceRequests(paramMessenger);
        }

        private void clearLocalServices(Messenger paramMessenger)
        {
            WifiP2pService.ClientInfo localClientInfo = getClientInfo(paramMessenger, false);
            if (localClientInfo == null);
            while (true)
            {
                return;
                Iterator localIterator = localClientInfo.mServList.iterator();
                while (localIterator.hasNext())
                {
                    WifiP2pServiceInfo localWifiP2pServiceInfo = (WifiP2pServiceInfo)localIterator.next();
                    this.mWifiNative.p2pServiceDel(localWifiP2pServiceInfo);
                }
                localClientInfo.mServList.clear();
                if (localClientInfo.mReqList.size() == 0)
                    WifiP2pService.this.mClientInfoList.remove(localClientInfo.mMessenger);
            }
        }

        private void clearServiceRequests(Messenger paramMessenger)
        {
            WifiP2pService.ClientInfo localClientInfo = getClientInfo(paramMessenger, false);
            if (localClientInfo == null);
            while (true)
            {
                return;
                if (localClientInfo.mReqList.size() != 0)
                {
                    localClientInfo.mReqList.clear();
                    if (localClientInfo.mServList.size() == 0)
                        WifiP2pService.this.mClientInfoList.remove(localClientInfo.mMessenger);
                    if (WifiP2pService.this.mServiceDiscReqId != null)
                        updateSupplicantServiceRequest();
                }
            }
        }

        private void clearSupplicantServiceRequest()
        {
            if (WifiP2pService.this.mServiceDiscReqId == null);
            while (true)
            {
                return;
                this.mWifiNative.p2pServDiscCancelReq(WifiP2pService.this.mServiceDiscReqId);
                WifiP2pService.access$5702(WifiP2pService.this, null);
            }
        }

        private int configuredNetworkId(String paramString)
        {
            return -1;
        }

        private WifiP2pService.ClientInfo getClientInfo(Messenger paramMessenger, boolean paramBoolean)
        {
            WifiP2pService.ClientInfo localClientInfo = (WifiP2pService.ClientInfo)WifiP2pService.this.mClientInfoList.get(paramMessenger);
            if ((localClientInfo == null) && (paramBoolean))
            {
                localClientInfo = new WifiP2pService.ClientInfo(WifiP2pService.this, paramMessenger, null);
                WifiP2pService.this.mClientInfoList.put(paramMessenger, localClientInfo);
            }
            return localClientInfo;
        }

        private String getDeviceName(String paramString)
        {
            WifiP2pDevice localWifiP2pDevice = this.mPeers.get(paramString);
            if (localWifiP2pDevice != null)
                paramString = localWifiP2pDevice.deviceName;
            return paramString;
        }

        private String getPersistedDeviceName()
        {
            String str1 = Settings.Secure.getString(WifiP2pService.this.mContext.getContentResolver(), "wifi_p2p_device_name");
            if (str1 == null)
            {
                String str2 = Settings.Secure.getString(WifiP2pService.this.mContext.getContentResolver(), "android_id");
                str1 = "Android_" + str2.substring(0, 4);
            }
            return str1;
        }

        private void handleGroupCreationFailure()
        {
            this.mSavedPeerConfig = null;
            this.mWifiNative.p2pFlush();
            WifiP2pService.access$5702(WifiP2pService.this, null);
            sendMessage(139265);
        }

        private void initializeP2pSettings()
        {
            this.mWifiNative.setPersistentReconnect(true);
            WifiP2pService.this.mThisDevice.deviceName = getPersistedDeviceName();
            this.mWifiNative.setDeviceName(WifiP2pService.this.mThisDevice.deviceName);
            this.mWifiNative.setP2pSsidPostfix("-" + WifiP2pService.this.mThisDevice.deviceName);
            this.mWifiNative.setDeviceType(WifiP2pService.this.mThisDevice.primaryDeviceType);
            this.mWifiNative.setConfigMethods("virtual_push_button physical_display keypad");
            this.mWifiNative.setConcurrencyPriority("sta");
            WifiP2pService.this.mThisDevice.deviceAddress = this.mWifiNative.p2pGetDeviceAddress();
            updateThisDevice(3);
            WifiP2pService.this.mClientInfoList.clear();
            this.mWifiNative.p2pFlush();
            this.mWifiNative.p2pServiceFlush();
            WifiP2pService.access$9202(WifiP2pService.this, (byte)0);
            WifiP2pService.access$5702(WifiP2pService.this, null);
        }

        private boolean isForegroundApp(String paramString)
        {
            boolean bool = false;
            if (paramString == null);
            while (true)
            {
                return bool;
                List localList = WifiP2pService.this.mActivityMgr.getRunningTasks(1);
                if (localList.size() != 0)
                    bool = paramString.equals(((ActivityManager.RunningTaskInfo)localList.get(0)).baseActivity.getPackageName());
            }
        }

        private void logd(String paramString)
        {
            Slog.d("WifiP2pService", paramString);
        }

        private void loge(String paramString)
        {
            Slog.e("WifiP2pService", paramString);
        }

        private void notifyInvitationReceived()
        {
            Resources localResources = Resources.getSystem();
            final WpsInfo localWpsInfo = this.mSavedPeerConfig.wps;
            View localView = LayoutInflater.from(WifiP2pService.this.mContext).inflate(17367241, null);
            ViewGroup localViewGroup = (ViewGroup)localView.findViewById(16909033);
            addRowToDialog(localViewGroup, 17040401, getDeviceName(this.mSavedPeerConfig.deviceAddress));
            final EditText localEditText = (EditText)localView.findViewById(16909161);
            AlertDialog localAlertDialog = new AlertDialog.Builder(WifiP2pService.this.mContext).setTitle(localResources.getString(17040400)).setView(localView).setPositiveButton(localResources.getString(17040397), new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
                {
                    if (localWpsInfo.setup == 2)
                        WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.wps.pin = localEditText.getText().toString();
                    WifiP2pService.P2pStateMachine.this.sendMessage(143362);
                }
            }).setNegativeButton(localResources.getString(17040398), new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
                {
                    WifiP2pService.P2pStateMachine.this.sendMessage(143363);
                }
            }).setOnCancelListener(new DialogInterface.OnCancelListener()
            {
                public void onCancel(DialogInterface paramAnonymousDialogInterface)
                {
                    WifiP2pService.P2pStateMachine.this.sendMessage(143363);
                }
            }).create();
            switch (localWpsInfo.setup)
            {
            default:
            case 2:
            case 1:
            }
            while (true)
            {
                localAlertDialog.getWindow().setType(2003);
                localAlertDialog.show();
                return;
                localView.findViewById(16909160).setVisibility(0);
                continue;
                addRowToDialog(localViewGroup, 17040404, localWpsInfo.pin);
            }
        }

        private void notifyInvitationSent(String paramString1, String paramString2)
        {
            Resources localResources = Resources.getSystem();
            View localView = LayoutInflater.from(WifiP2pService.this.mContext).inflate(17367241, null);
            ViewGroup localViewGroup = (ViewGroup)localView.findViewById(16909033);
            addRowToDialog(localViewGroup, 17040402, getDeviceName(paramString2));
            addRowToDialog(localViewGroup, 17040404, paramString1);
            AlertDialog localAlertDialog = new AlertDialog.Builder(WifiP2pService.this.mContext).setTitle(localResources.getString(17040399)).setView(localView).setPositiveButton(localResources.getString(17039370), null).create();
            localAlertDialog.getWindow().setType(2003);
            localAlertDialog.show();
        }

        private void notifyP2pEnableFailure()
        {
            Resources localResources = Resources.getSystem();
            AlertDialog localAlertDialog = new AlertDialog.Builder(WifiP2pService.this.mContext).setTitle(localResources.getString(17040392)).setMessage(localResources.getString(17040394)).setPositiveButton(localResources.getString(17039370), null).create();
            localAlertDialog.getWindow().setType(2003);
            localAlertDialog.show();
        }

        private Message obtainMessage(Message paramMessage)
        {
            Message localMessage = Message.obtain();
            localMessage.arg2 = paramMessage.arg2;
            return localMessage;
        }

        private void p2pConnectWithPinDisplay(WifiP2pConfig paramWifiP2pConfig, boolean paramBoolean)
        {
            String str = this.mWifiNative.p2pConnect(paramWifiP2pConfig, paramBoolean);
            try
            {
                Integer.parseInt(str);
                if (!sendShowPinReqToFrontApp(str))
                    notifyInvitationSent(str, paramWifiP2pConfig.deviceAddress);
                label32: return;
            }
            catch (NumberFormatException localNumberFormatException)
            {
                break label32;
            }
        }

        private void removeLocalService(Messenger paramMessenger, WifiP2pServiceInfo paramWifiP2pServiceInfo)
        {
            WifiP2pService.ClientInfo localClientInfo = getClientInfo(paramMessenger, false);
            if (localClientInfo == null);
            while (true)
            {
                return;
                this.mWifiNative.p2pServiceDel(paramWifiP2pServiceInfo);
                localClientInfo.mServList.remove(paramWifiP2pServiceInfo);
                if ((localClientInfo.mReqList.size() == 0) && (localClientInfo.mServList.size() == 0))
                    WifiP2pService.this.mClientInfoList.remove(localClientInfo.mMessenger);
            }
        }

        private void removeServiceRequest(Messenger paramMessenger, WifiP2pServiceRequest paramWifiP2pServiceRequest)
        {
            WifiP2pService.ClientInfo localClientInfo = getClientInfo(paramMessenger, false);
            if (localClientInfo == null)
                return;
            int i = 0;
            for (int j = 0; ; j++)
                if (j < localClientInfo.mReqList.size())
                {
                    if (paramWifiP2pServiceRequest.equals(localClientInfo.mReqList.valueAt(j)))
                    {
                        i = 1;
                        localClientInfo.mReqList.removeAt(j);
                    }
                }
                else
                {
                    if (i == 0)
                        break;
                    if ((localClientInfo.mReqList.size() == 0) && (localClientInfo.mServList.size() == 0))
                        WifiP2pService.this.mClientInfoList.remove(localClientInfo.mMessenger);
                    if (WifiP2pService.this.mServiceDiscReqId == null)
                        break;
                    updateSupplicantServiceRequest();
                    break;
                }
        }

        private void replyToMessage(Message paramMessage, int paramInt)
        {
            if (paramMessage.replyTo == null);
            while (true)
            {
                return;
                Message localMessage = obtainMessage(paramMessage);
                localMessage.what = paramInt;
                WifiP2pService.this.mReplyChannel.replyToMessage(paramMessage, localMessage);
            }
        }

        private void replyToMessage(Message paramMessage, int paramInt1, int paramInt2)
        {
            if (paramMessage.replyTo == null);
            while (true)
            {
                return;
                Message localMessage = obtainMessage(paramMessage);
                localMessage.what = paramInt1;
                localMessage.arg1 = paramInt2;
                WifiP2pService.this.mReplyChannel.replyToMessage(paramMessage, localMessage);
            }
        }

        private void replyToMessage(Message paramMessage, int paramInt, Object paramObject)
        {
            if (paramMessage.replyTo == null);
            while (true)
            {
                return;
                Message localMessage = obtainMessage(paramMessage);
                localMessage.what = paramInt;
                localMessage.obj = paramObject;
                WifiP2pService.this.mReplyChannel.replyToMessage(paramMessage, localMessage);
            }
        }

        private boolean sendConnectNoticeToApp(WifiP2pDevice paramWifiP2pDevice, WifiP2pConfig paramWifiP2pConfig)
        {
            if (paramWifiP2pDevice == null)
                paramWifiP2pDevice = new WifiP2pDevice(paramWifiP2pConfig.deviceAddress);
            if (!isForegroundApp(WifiP2pService.this.mForegroundAppPkgName))
                sendDetachedMsg(4);
            Message localMessage;
            for (boolean bool = false; ; bool = sendDialogMsgToFrontApp(localMessage))
            {
                return bool;
                localMessage = Message.obtain();
                localMessage.what = 139321;
                Bundle localBundle = new Bundle();
                localBundle.putParcelable("wifiP2pDevice", paramWifiP2pDevice);
                localBundle.putParcelable("wifiP2pConfig", paramWifiP2pConfig);
                localMessage.setData(localBundle);
            }
        }

        private void sendDetachedMsg(int paramInt)
        {
            if (WifiP2pService.this.mForegroundAppMessenger == null);
            while (true)
            {
                return;
                Message localMessage = Message.obtain();
                localMessage.what = 139319;
                localMessage.arg1 = paramInt;
                try
                {
                    WifiP2pService.this.mForegroundAppMessenger.send(localMessage);
                    label38: WifiP2pService.access$9802(WifiP2pService.this, null);
                    WifiP2pService.access$9902(WifiP2pService.this, null);
                }
                catch (RemoteException localRemoteException)
                {
                    break label38;
                }
            }
        }

        private boolean sendDialogMsgToFrontApp(Message paramMessage)
        {
            try
            {
                WifiP2pService.this.mForegroundAppMessenger.send(paramMessage);
                bool = true;
                return bool;
            }
            catch (RemoteException localRemoteException)
            {
                while (true)
                {
                    WifiP2pService.access$9802(WifiP2pService.this, null);
                    WifiP2pService.access$9902(WifiP2pService.this, null);
                    boolean bool = false;
                }
            }
        }

        private void sendP2pConnectionChangedBroadcast()
        {
            Intent localIntent = new Intent("android.net.wifi.p2p.CONNECTION_STATE_CHANGE");
            localIntent.addFlags(671088640);
            localIntent.putExtra("wifiP2pInfo", new WifiP2pInfo(this.mWifiP2pInfo));
            localIntent.putExtra("networkInfo", new NetworkInfo(WifiP2pService.this.mNetworkInfo));
            WifiP2pService.this.mContext.sendStickyBroadcast(localIntent);
        }

        private void sendP2pDiscoveryChangedBroadcast(boolean paramBoolean)
        {
            if (WifiP2pService.this.mDiscoveryStarted == paramBoolean)
                return;
            WifiP2pService.access$8902(WifiP2pService.this, paramBoolean);
            Intent localIntent = new Intent("android.net.wifi.p2p.DISCOVERY_STATE_CHANGE");
            localIntent.addFlags(134217728);
            if (paramBoolean);
            for (int i = 2; ; i = 1)
            {
                localIntent.putExtra("discoveryState", i);
                WifiP2pService.this.mContext.sendStickyBroadcast(localIntent);
                break;
            }
        }

        private void sendP2pPeersChangedBroadcast()
        {
            Intent localIntent = new Intent("android.net.wifi.p2p.PEERS_CHANGED");
            localIntent.addFlags(134217728);
            WifiP2pService.this.mContext.sendBroadcast(localIntent);
        }

        private void sendP2pStateChangedBroadcast(boolean paramBoolean)
        {
            Intent localIntent = new Intent("android.net.wifi.p2p.STATE_CHANGED");
            localIntent.addFlags(134217728);
            if (paramBoolean)
                localIntent.putExtra("wifi_p2p_state", 2);
            while (true)
            {
                WifiP2pService.this.mContext.sendStickyBroadcast(localIntent);
                return;
                localIntent.putExtra("wifi_p2p_state", 1);
            }
        }

        private void sendServiceResponse(WifiP2pServiceResponse paramWifiP2pServiceResponse)
        {
            Iterator localIterator = WifiP2pService.this.mClientInfoList.values().iterator();
            while (true)
                if (localIterator.hasNext())
                {
                    WifiP2pService.ClientInfo localClientInfo = (WifiP2pService.ClientInfo)localIterator.next();
                    if ((WifiP2pServiceRequest)localClientInfo.mReqList.get(paramWifiP2pServiceResponse.getTransactionId()) != null)
                    {
                        Message localMessage = Message.obtain();
                        localMessage.what = 139314;
                        localMessage.arg1 = 0;
                        localMessage.arg2 = 0;
                        localMessage.obj = paramWifiP2pServiceResponse;
                        try
                        {
                            localClientInfo.mMessenger.send(localMessage);
                        }
                        catch (RemoteException localRemoteException)
                        {
                            clearClientInfo(localClientInfo.mMessenger);
                        }
                    }
                }
        }

        private boolean sendShowPinReqToFrontApp(String paramString)
        {
            if (!isForegroundApp(WifiP2pService.this.mForegroundAppPkgName))
                sendDetachedMsg(4);
            Message localMessage;
            for (boolean bool = false; ; bool = sendDialogMsgToFrontApp(localMessage))
            {
                return bool;
                localMessage = Message.obtain();
                localMessage.what = 139322;
                Bundle localBundle = new Bundle();
                localBundle.putString("wpsPin", paramString);
                localMessage.setData(localBundle);
            }
        }

        private void sendThisDeviceChangedBroadcast()
        {
            Intent localIntent = new Intent("android.net.wifi.p2p.THIS_DEVICE_CHANGED");
            localIntent.addFlags(134217728);
            localIntent.putExtra("wifiP2pDevice", new WifiP2pDevice(WifiP2pService.this.mThisDevice));
            WifiP2pService.this.mContext.sendStickyBroadcast(localIntent);
        }

        private boolean setAndPersistDeviceName(String paramString)
        {
            boolean bool = false;
            if (paramString == null);
            while (true)
            {
                return bool;
                if (!this.mWifiNative.setDeviceName(paramString))
                {
                    loge("Failed to set device name " + paramString);
                }
                else
                {
                    WifiP2pService.this.mThisDevice.deviceName = paramString;
                    this.mWifiNative.setP2pSsidPostfix("-" + WifiP2pService.this.mThisDevice.deviceName);
                    Settings.Secure.putString(WifiP2pService.this.mContext.getContentResolver(), "wifi_p2p_device_name", paramString);
                    sendThisDeviceChangedBroadcast();
                    bool = true;
                }
            }
        }

        private boolean setDialogListenerApp(Messenger paramMessenger, String paramString, boolean paramBoolean)
        {
            boolean bool = false;
            if ((WifiP2pService.this.mForegroundAppPkgName != null) && (!WifiP2pService.this.mForegroundAppPkgName.equals(paramString)))
                if (!isForegroundApp(WifiP2pService.this.mForegroundAppPkgName));
            while (true)
            {
                return bool;
                sendDetachedMsg(4);
                if (paramBoolean)
                {
                    WifiP2pService.access$9802(WifiP2pService.this, null);
                    WifiP2pService.access$9902(WifiP2pService.this, null);
                    bool = true;
                }
                else if (isForegroundApp(paramString))
                {
                    WifiP2pService.access$9802(WifiP2pService.this, paramMessenger);
                    WifiP2pService.access$9902(WifiP2pService.this, paramString);
                    bool = true;
                }
            }
        }

        private void setWifiP2pInfoOnGroupFormation(String paramString)
        {
            this.mWifiP2pInfo.groupFormed = true;
            this.mWifiP2pInfo.isGroupOwner = this.mGroup.isGroupOwner();
            this.mWifiP2pInfo.groupOwnerAddress = NetworkUtils.numericToInetAddress(paramString);
        }

        private void setWifiP2pInfoOnGroupTermination()
        {
            this.mWifiP2pInfo.groupFormed = false;
            this.mWifiP2pInfo.isGroupOwner = false;
            this.mWifiP2pInfo.groupOwnerAddress = null;
        }

        private void startDhcpServer(String paramString)
        {
            try
            {
                InterfaceConfiguration localInterfaceConfiguration = WifiP2pService.this.mNwService.getInterfaceConfig(paramString);
                localInterfaceConfiguration.setLinkAddress(new LinkAddress(NetworkUtils.numericToInetAddress("192.168.49.1"), 24));
                localInterfaceConfiguration.setInterfaceUp();
                WifiP2pService.this.mNwService.setInterfaceConfig(paramString, localInterfaceConfiguration);
                WifiP2pService.this.mNwService.startTethering(WifiP2pService.DHCP_RANGE);
                logd("Started Dhcp server on " + paramString);
                return;
            }
            catch (Exception localException)
            {
                while (true)
                    loge("Error configuring interface " + paramString + ", :" + localException);
            }
        }

        private void stopDhcpServer()
        {
            try
            {
                WifiP2pService.this.mNwService.stopTethering();
                logd("Stopped Dhcp server");
                return;
            }
            catch (Exception localException)
            {
                while (true)
                    loge("Error stopping Dhcp server" + localException);
            }
        }

        private boolean updateSupplicantServiceRequest()
        {
            boolean bool = false;
            clearSupplicantServiceRequest();
            StringBuffer localStringBuffer = new StringBuffer();
            Iterator localIterator = WifiP2pService.this.mClientInfoList.values().iterator();
            while (localIterator.hasNext())
            {
                WifiP2pService.ClientInfo localClientInfo = (WifiP2pService.ClientInfo)localIterator.next();
                for (int i = 0; i < localClientInfo.mReqList.size(); i++)
                {
                    WifiP2pServiceRequest localWifiP2pServiceRequest = (WifiP2pServiceRequest)localClientInfo.mReqList.valueAt(i);
                    if (localWifiP2pServiceRequest != null)
                        localStringBuffer.append(localWifiP2pServiceRequest.getSupplicantQuery());
                }
            }
            if (localStringBuffer.length() == 0);
            while (true)
            {
                return bool;
                WifiP2pService.access$5702(WifiP2pService.this, this.mWifiNative.p2pServDiscReq("00:00:00:00:00:00", localStringBuffer.toString()));
                if (WifiP2pService.this.mServiceDiscReqId != null)
                    bool = true;
            }
        }

        private void updateThisDevice(int paramInt)
        {
            WifiP2pService.this.mThisDevice.status = paramInt;
            sendThisDeviceChangedBroadcast();
        }

        class UserAuthorizingJoinState extends State
        {
            UserAuthorizingJoinState()
            {
            }

            public void enter()
            {
                WifiP2pService.P2pStateMachine.this.notifyInvitationReceived();
            }

            public void exit()
            {
            }

            public boolean processMessage(Message paramMessage)
            {
                boolean bool;
                switch (paramMessage.what)
                {
                default:
                    bool = false;
                    return bool;
                case 143362:
                    if (WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.wps.setup == 0)
                    {
                        WifiP2pService.P2pStateMachine.this.mWifiNative.startWpsPbc(WifiP2pService.P2pStateMachine.this.mGroup.getInterface(), null);
                        label98: WifiP2pService.P2pStateMachine.access$4202(WifiP2pService.P2pStateMachine.this, null);
                        WifiP2pService.P2pStateMachine.this.transitionTo(WifiP2pService.P2pStateMachine.this.mGroupCreatedState);
                    }
                    break;
                case 147489:
                case 147491:
                case 147492:
                case 143363:
                }
                while (true)
                {
                    bool = true;
                    break;
                    WifiP2pService.P2pStateMachine.this.mWifiNative.startWpsPinKeypad(WifiP2pService.P2pStateMachine.this.mGroup.getInterface(), WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.wps.pin);
                    break label98;
                    WifiP2pService.P2pStateMachine.access$4202(WifiP2pService.P2pStateMachine.this, null);
                    WifiP2pService.P2pStateMachine.this.transitionTo(WifiP2pService.P2pStateMachine.this.mGroupCreatedState);
                }
            }
        }

        class GroupCreatedState extends State
        {
            GroupCreatedState()
            {
            }

            public void enter()
            {
                WifiP2pService.this.mNetworkInfo.setDetailedState(NetworkInfo.DetailedState.CONNECTED, null, null);
                WifiP2pService.P2pStateMachine.this.updateThisDevice(0);
                if (WifiP2pService.P2pStateMachine.this.mGroup.isGroupOwner())
                {
                    WifiP2pService.P2pStateMachine.this.setWifiP2pInfoOnGroupFormation("192.168.49.1");
                    WifiP2pService.P2pStateMachine.this.sendP2pConnectionChangedBroadcast();
                }
            }

            public void exit()
            {
                WifiP2pService.P2pStateMachine.access$7902(WifiP2pService.P2pStateMachine.this, null);
                WifiP2pService.P2pStateMachine.this.updateThisDevice(3);
                WifiP2pService.P2pStateMachine.this.setWifiP2pInfoOnGroupTermination();
                WifiP2pService.this.mNetworkInfo.setDetailedState(NetworkInfo.DetailedState.DISCONNECTED, null, null);
                WifiP2pService.P2pStateMachine.this.sendP2pConnectionChangedBroadcast();
            }

            public boolean processMessage(Message paramMessage)
            {
                boolean bool;
                WifiP2pConfig localWifiP2pConfig;
                switch (paramMessage.what)
                {
                default:
                    bool = false;
                    return bool;
                case 147498:
                    String str4 = ((WifiP2pDevice)paramMessage.obj).deviceAddress;
                    if (str4 != null)
                    {
                        if ((WifiP2pService.P2pStateMachine.this.mSavedProvDiscDevice != null) && (str4.equals(WifiP2pService.P2pStateMachine.this.mSavedProvDiscDevice.deviceAddress)))
                            WifiP2pService.P2pStateMachine.access$7902(WifiP2pService.P2pStateMachine.this, null);
                        WifiP2pService.P2pStateMachine.this.mGroup.addClient(str4);
                        WifiP2pService.P2pStateMachine.this.mPeers.updateStatus(str4, 0);
                        WifiP2pService.P2pStateMachine.this.sendP2pPeersChangedBroadcast();
                    }
                case 147497:
                case 196613:
                case 139280:
                case 147486:
                case 147478:
                case 131204:
                    while (true)
                    {
                        bool = true;
                        break;
                        WifiP2pService.P2pStateMachine.this.loge("Connect on null device address, ignore");
                        continue;
                        WifiP2pDevice localWifiP2pDevice3 = (WifiP2pDevice)paramMessage.obj;
                        String str3 = localWifiP2pDevice3.deviceAddress;
                        if (str3 != null)
                        {
                            WifiP2pService.P2pStateMachine.this.mPeers.updateStatus(str3, 3);
                            if (WifiP2pService.P2pStateMachine.this.mGroup.removeClient(str3))
                                if ((!WifiP2pService.this.mAutonomousGroup) && (WifiP2pService.P2pStateMachine.this.mGroup.isClientListEmpty()))
                                {
                                    Slog.d("WifiP2pService", "Client list empty, remove non-persistent p2p group");
                                    WifiP2pService.P2pStateMachine.this.mWifiNative.p2pGroupRemove(WifiP2pService.P2pStateMachine.this.mGroup.getInterface());
                                }
                            while (true)
                            {
                                WifiP2pService.P2pStateMachine.this.sendP2pPeersChangedBroadcast();
                                break;
                                Iterator localIterator2 = WifiP2pService.P2pStateMachine.this.mGroup.getClientList().iterator();
                                while (localIterator2.hasNext())
                                    ((WifiP2pDevice)localIterator2.next());
                            }
                        }
                        WifiP2pService.P2pStateMachine.this.loge("Disconnect on unknown device: " + localWifiP2pDevice3);
                        continue;
                        DhcpInfoInternal localDhcpInfoInternal = (DhcpInfoInternal)paramMessage.obj;
                        if ((paramMessage.arg1 == 1) && (localDhcpInfoInternal != null))
                        {
                            WifiP2pService.P2pStateMachine.this.setWifiP2pInfoOnGroupFormation(localDhcpInfoInternal.serverAddress);
                            WifiP2pService.P2pStateMachine.this.sendP2pConnectionChangedBroadcast();
                            WifiP2pService.P2pStateMachine.this.mWifiNative.setP2pPowerSave(WifiP2pService.P2pStateMachine.this.mGroup.getInterface(), true);
                        }
                        else
                        {
                            WifiP2pService.P2pStateMachine.this.loge("DHCP failed");
                            WifiP2pService.P2pStateMachine.this.mWifiNative.p2pGroupRemove(WifiP2pService.P2pStateMachine.this.mGroup.getInterface());
                            continue;
                            if (WifiP2pService.P2pStateMachine.this.mWifiNative.p2pGroupRemove(WifiP2pService.P2pStateMachine.this.mGroup.getInterface()))
                            {
                                WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139282);
                            }
                            else
                            {
                                WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139281, 0);
                                continue;
                                Collection localCollection = WifiP2pService.P2pStateMachine.this.mGroup.getClientList();
                                int i = 0;
                                Iterator localIterator1 = WifiP2pService.P2pStateMachine.this.mPeers.getDeviceList().iterator();
                                while (localIterator1.hasNext())
                                {
                                    WifiP2pDevice localWifiP2pDevice2 = (WifiP2pDevice)localIterator1.next();
                                    if ((localCollection.contains(localWifiP2pDevice2)) || (WifiP2pService.P2pStateMachine.this.mGroup.getOwner().equals(localWifiP2pDevice2)))
                                    {
                                        localWifiP2pDevice2.status = 3;
                                        i = 1;
                                    }
                                }
                                if (WifiP2pService.P2pStateMachine.this.mGroup.isGroupOwner())
                                    WifiP2pService.P2pStateMachine.this.stopDhcpServer();
                                while (true)
                                {
                                    WifiP2pService.P2pStateMachine.access$802(WifiP2pService.P2pStateMachine.this, null);
                                    WifiP2pService.P2pStateMachine.this.mWifiNative.p2pFlush();
                                    WifiP2pService.access$5702(WifiP2pService.this, null);
                                    if (i != 0)
                                        WifiP2pService.P2pStateMachine.this.sendP2pPeersChangedBroadcast();
                                    WifiP2pService.P2pStateMachine.this.transitionTo(WifiP2pService.P2pStateMachine.this.mInactiveState);
                                    break;
                                    WifiP2pService.this.mDhcpStateMachine.sendMessage(196610);
                                    WifiP2pService.this.mDhcpStateMachine.quit();
                                    WifiP2pService.access$7302(WifiP2pService.this, null);
                                }
                                WifiP2pDevice localWifiP2pDevice1 = (WifiP2pDevice)paramMessage.obj;
                                if (WifiP2pService.P2pStateMachine.this.mGroup.contains(localWifiP2pDevice1))
                                {
                                    bool = true;
                                    break;
                                }
                                bool = false;
                                break;
                                WifiP2pService.P2pStateMachine.this.sendMessage(139280);
                                WifiP2pService.P2pStateMachine.this.deferMessage(paramMessage);
                            }
                        }
                    }
                case 139271:
                    localWifiP2pConfig = (WifiP2pConfig)paramMessage.obj;
                    if ((localWifiP2pConfig.deviceAddress == null) || ((WifiP2pService.P2pStateMachine.this.mSavedProvDiscDevice != null) && (WifiP2pService.P2pStateMachine.this.mSavedProvDiscDevice.deviceAddress.equals(localWifiP2pConfig.deviceAddress))))
                        if (localWifiP2pConfig.wps.setup == 0)
                            WifiP2pService.P2pStateMachine.this.mWifiNative.startWpsPbc(WifiP2pService.P2pStateMachine.this.mGroup.getInterface(), null);
                    break;
                case 147489:
                case 147491:
                case 147492:
                case 147485:
                }
                while (true)
                    while (true)
                    {
                        if (localWifiP2pConfig.deviceAddress != null)
                        {
                            WifiP2pService.P2pStateMachine.this.mPeers.updateStatus(localWifiP2pConfig.deviceAddress, 1);
                            WifiP2pService.P2pStateMachine.this.sendP2pPeersChangedBroadcast();
                        }
                        WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139273);
                        break;
                        String str1;
                        if (localWifiP2pConfig.wps.pin == null)
                            str1 = WifiP2pService.P2pStateMachine.this.mWifiNative.startWpsPinDisplay(WifiP2pService.P2pStateMachine.this.mGroup.getInterface());
                        try
                        {
                            Integer.parseInt(str1);
                            if (!WifiP2pService.P2pStateMachine.this.sendShowPinReqToFrontApp(str1))
                            {
                                WifiP2pService.P2pStateMachine localP2pStateMachine = WifiP2pService.P2pStateMachine.this;
                                if (localWifiP2pConfig.deviceAddress != null);
                                for (String str2 = localWifiP2pConfig.deviceAddress; ; str2 = "any")
                                {
                                    localP2pStateMachine.notifyInvitationSent(str1, str2);
                                    break;
                                }
                                WifiP2pService.P2pStateMachine.this.mWifiNative.startWpsPinKeypad(WifiP2pService.P2pStateMachine.this.mGroup.getInterface(), localWifiP2pConfig.wps.pin);
                                continue;
                                WifiP2pService.P2pStateMachine.this.logd("Inviting device : " + localWifiP2pConfig.deviceAddress);
                                if (WifiP2pService.P2pStateMachine.this.mWifiNative.p2pInvite(WifiP2pService.P2pStateMachine.this.mGroup, localWifiP2pConfig.deviceAddress))
                                {
                                    WifiP2pService.P2pStateMachine.this.mPeers.updateStatus(localWifiP2pConfig.deviceAddress, 1);
                                    WifiP2pService.P2pStateMachine.this.sendP2pPeersChangedBroadcast();
                                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139273);
                                    break;
                                }
                                WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139272, 0);
                                break;
                                WifiP2pProvDiscEvent localWifiP2pProvDiscEvent = (WifiP2pProvDiscEvent)paramMessage.obj;
                                WifiP2pService.P2pStateMachine.access$7902(WifiP2pService.P2pStateMachine.this, localWifiP2pProvDiscEvent.device);
                                WifiP2pService.P2pStateMachine.access$4202(WifiP2pService.P2pStateMachine.this, new WifiP2pConfig());
                                WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.deviceAddress = localWifiP2pProvDiscEvent.device.deviceAddress;
                                if (paramMessage.what == 147491)
                                    WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.wps.setup = 2;
                                while (!WifiP2pService.P2pStateMachine.this.sendConnectNoticeToApp(WifiP2pService.P2pStateMachine.this.mSavedProvDiscDevice, WifiP2pService.P2pStateMachine.this.mSavedPeerConfig))
                                {
                                    WifiP2pService.P2pStateMachine.this.transitionTo(WifiP2pService.P2pStateMachine.this.mUserAuthorizingJoinState);
                                    break;
                                    if (paramMessage.what == 147492)
                                    {
                                        WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.wps.setup = 1;
                                        WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.wps.pin = localWifiP2pProvDiscEvent.pin;
                                    }
                                    else
                                    {
                                        WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.wps.setup = 0;
                                    }
                                }
                                Slog.e("WifiP2pService", "Duplicate group creation event notice, ignore");
                            }
                        }
                        catch (NumberFormatException localNumberFormatException)
                        {
                        }
                    }
            }
        }

        class GroupNegotiationState extends State
        {
            GroupNegotiationState()
            {
            }

            public void enter()
            {
            }

            public boolean processMessage(Message paramMessage)
            {
                boolean bool;
                switch (paramMessage.what)
                {
                default:
                    bool = false;
                    return bool;
                case 147485:
                    WifiP2pService.P2pStateMachine.access$802(WifiP2pService.P2pStateMachine.this, (WifiP2pGroup)paramMessage.obj);
                    if (WifiP2pService.P2pStateMachine.this.mGroup.isGroupOwner())
                    {
                        WifiP2pService.P2pStateMachine.this.startDhcpServer(WifiP2pService.P2pStateMachine.this.mGroup.getInterface());
                        label93: WifiP2pService.P2pStateMachine.access$4202(WifiP2pService.P2pStateMachine.this, null);
                        WifiP2pService.P2pStateMachine.this.transitionTo(WifiP2pService.P2pStateMachine.this.mGroupCreatedState);
                    }
                    break;
                case 147481:
                case 147483:
                case 147484:
                case 147482:
                case 147486:
                }
                while (true)
                {
                    bool = true;
                    break;
                    WifiP2pService.P2pStateMachine.this.mWifiNative.setP2pGroupIdle(WifiP2pService.P2pStateMachine.this.mGroup.getInterface(), 2);
                    WifiP2pService.access$7302(WifiP2pService.this, DhcpStateMachine.makeDhcpStateMachine(WifiP2pService.this.mContext, WifiP2pService.P2pStateMachine.this, WifiP2pService.P2pStateMachine.this.mGroup.getInterface()));
                    WifiP2pService.this.mDhcpStateMachine.sendMessage(196609);
                    WifiP2pDevice localWifiP2pDevice = WifiP2pService.P2pStateMachine.this.mGroup.getOwner();
                    WifiP2pService.P2pStateMachine.this.mPeers.updateStatus(localWifiP2pDevice.deviceAddress, 0);
                    WifiP2pService.P2pStateMachine.this.sendP2pPeersChangedBroadcast();
                    break label93;
                    WifiP2pService.P2pStateMachine.this.handleGroupCreationFailure();
                    WifiP2pService.P2pStateMachine.this.transitionTo(WifiP2pService.P2pStateMachine.this.mInactiveState);
                }
            }
        }

        class ProvisionDiscoveryState extends State
        {
            ProvisionDiscoveryState()
            {
            }

            public void enter()
            {
                WifiP2pService.P2pStateMachine.this.mWifiNative.p2pProvisionDiscovery(WifiP2pService.P2pStateMachine.this.mSavedPeerConfig);
            }

            public boolean processMessage(Message paramMessage)
            {
                boolean bool = false;
                switch (paramMessage.what)
                {
                default:
                    return bool;
                case 147490:
                    if (((WifiP2pProvDiscEvent)paramMessage.obj).device.deviceAddress.equals(WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.deviceAddress))
                        break;
                case 147491:
                case 147492:
                }
                while (true)
                {
                    bool = true;
                    break;
                    if (WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.wps.setup == 0)
                    {
                        WifiP2pService.P2pStateMachine.this.mWifiNative.p2pConnect(WifiP2pService.P2pStateMachine.this.mSavedPeerConfig, WifiP2pService.FORM_GROUP.booleanValue());
                        WifiP2pService.P2pStateMachine.this.transitionTo(WifiP2pService.P2pStateMachine.this.mGroupNegotiationState);
                        continue;
                        if ((((WifiP2pProvDiscEvent)paramMessage.obj).device.deviceAddress.equals(WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.deviceAddress)) && (WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.wps.setup == 2))
                            if (!TextUtils.isEmpty(WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.wps.pin))
                            {
                                WifiP2pService.P2pStateMachine.this.mWifiNative.p2pConnect(WifiP2pService.P2pStateMachine.this.mSavedPeerConfig, WifiP2pService.FORM_GROUP.booleanValue());
                                WifiP2pService.P2pStateMachine.this.transitionTo(WifiP2pService.P2pStateMachine.this.mGroupNegotiationState);
                            }
                            else
                            {
                                WifiP2pService.access$5202(WifiP2pService.this, false);
                                WifiP2pService.P2pStateMachine.this.transitionTo(WifiP2pService.P2pStateMachine.this.mUserAuthorizingInvitationState);
                                continue;
                                WifiP2pProvDiscEvent localWifiP2pProvDiscEvent = (WifiP2pProvDiscEvent)paramMessage.obj;
                                WifiP2pDevice localWifiP2pDevice = localWifiP2pProvDiscEvent.device;
                                if ((localWifiP2pDevice.deviceAddress.equals(WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.deviceAddress)) && (WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.wps.setup == 1))
                                {
                                    WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.wps.pin = localWifiP2pProvDiscEvent.pin;
                                    WifiP2pService.P2pStateMachine.this.mWifiNative.p2pConnect(WifiP2pService.P2pStateMachine.this.mSavedPeerConfig, WifiP2pService.FORM_GROUP.booleanValue());
                                    if (!WifiP2pService.P2pStateMachine.this.sendShowPinReqToFrontApp(localWifiP2pProvDiscEvent.pin))
                                        WifiP2pService.P2pStateMachine.this.notifyInvitationSent(localWifiP2pProvDiscEvent.pin, localWifiP2pDevice.deviceAddress);
                                    WifiP2pService.P2pStateMachine.this.transitionTo(WifiP2pService.P2pStateMachine.this.mGroupNegotiationState);
                                }
                            }
                    }
                }
            }
        }

        class UserAuthorizingInvitationState extends State
        {
            UserAuthorizingInvitationState()
            {
            }

            public void enter()
            {
                if (!WifiP2pService.P2pStateMachine.this.sendConnectNoticeToApp(WifiP2pService.P2pStateMachine.this.mPeers.get(WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.deviceAddress), WifiP2pService.P2pStateMachine.this.mSavedPeerConfig))
                    WifiP2pService.P2pStateMachine.this.notifyInvitationReceived();
            }

            public void exit()
            {
            }

            public boolean processMessage(Message paramMessage)
            {
                boolean bool = true;
                switch (paramMessage.what)
                {
                default:
                    bool = false;
                case 143362:
                case 143363:
                }
                while (true)
                {
                    return bool;
                    if (WifiP2pService.this.mJoinExistingGroup)
                        WifiP2pService.P2pStateMachine.this.p2pConnectWithPinDisplay(WifiP2pService.P2pStateMachine.this.mSavedPeerConfig, WifiP2pService.JOIN_GROUP.booleanValue());
                    while (true)
                    {
                        WifiP2pService.P2pStateMachine.this.mPeers.updateStatus(WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.deviceAddress, 1);
                        WifiP2pService.P2pStateMachine.this.sendP2pPeersChangedBroadcast();
                        WifiP2pService.P2pStateMachine.this.transitionTo(WifiP2pService.P2pStateMachine.this.mGroupNegotiationState);
                        break;
                        WifiP2pService.P2pStateMachine.this.p2pConnectWithPinDisplay(WifiP2pService.P2pStateMachine.this.mSavedPeerConfig, WifiP2pService.FORM_GROUP.booleanValue());
                    }
                    WifiP2pService.P2pStateMachine.access$4202(WifiP2pService.P2pStateMachine.this, null);
                    WifiP2pService.P2pStateMachine.this.transitionTo(WifiP2pService.P2pStateMachine.this.mInactiveState);
                }
            }
        }

        class GroupCreatingState extends State
        {
            GroupCreatingState()
            {
            }

            public void enter()
            {
                WifiP2pService.P2pStateMachine.this.sendMessageDelayed(WifiP2pService.P2pStateMachine.this.obtainMessage(143361, WifiP2pService.access$5904(), 0), 120000L);
            }

            public boolean processMessage(Message paramMessage)
            {
                boolean bool = true;
                switch (paramMessage.what)
                {
                default:
                    bool = false;
                case 143361:
                case 147478:
                case 139265:
                case 139274:
                }
                while (true)
                {
                    return bool;
                    if (WifiP2pService.mGroupCreatingTimeoutIndex == paramMessage.arg1)
                    {
                        WifiP2pService.P2pStateMachine.this.handleGroupCreationFailure();
                        WifiP2pService.P2pStateMachine.this.transitionTo(WifiP2pService.P2pStateMachine.this.mInactiveState);
                        continue;
                        WifiP2pDevice localWifiP2pDevice = (WifiP2pDevice)paramMessage.obj;
                        if ((WifiP2pService.P2pStateMachine.this.mSavedPeerConfig != null) && (!WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.deviceAddress.equals(localWifiP2pDevice.deviceAddress)))
                        {
                            bool = false;
                            continue;
                            WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139266, 2);
                            continue;
                            WifiP2pService.P2pStateMachine.this.mWifiNative.p2pCancelConnect();
                            WifiP2pService.P2pStateMachine.this.handleGroupCreationFailure();
                            WifiP2pService.P2pStateMachine.this.transitionTo(WifiP2pService.P2pStateMachine.this.mInactiveState);
                            WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139276);
                        }
                    }
                }
            }
        }

        class InactiveState extends State
        {
            InactiveState()
            {
            }

            public void enter()
            {
            }

            public boolean processMessage(Message paramMessage)
            {
                boolean bool;
                WifiP2pConfig localWifiP2pConfig;
                switch (paramMessage.what)
                {
                default:
                    bool = false;
                    return bool;
                case 139271:
                    localWifiP2pConfig = (WifiP2pConfig)paramMessage.obj;
                    WifiP2pService.access$4102(WifiP2pService.this, false);
                    int i = WifiP2pService.P2pStateMachine.this.mWifiNative.getGroupCapability(localWifiP2pConfig.deviceAddress);
                    WifiP2pService.P2pStateMachine.this.mPeers.updateGroupCapability(localWifiP2pConfig.deviceAddress, i);
                    if ((WifiP2pService.P2pStateMachine.this.mSavedPeerConfig != null) && (localWifiP2pConfig.deviceAddress.equals(WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.deviceAddress)))
                    {
                        WifiP2pService.P2pStateMachine.access$4202(WifiP2pService.P2pStateMachine.this, localWifiP2pConfig);
                        WifiP2pService.P2pStateMachine.this.mWifiNative.p2pStopFind();
                        if (WifiP2pService.P2pStateMachine.this.mPeers.isGroupOwner(WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.deviceAddress))
                        {
                            WifiP2pService.P2pStateMachine.this.p2pConnectWithPinDisplay(WifiP2pService.P2pStateMachine.this.mSavedPeerConfig, WifiP2pService.JOIN_GROUP.booleanValue());
                            WifiP2pService.P2pStateMachine.this.transitionTo(WifiP2pService.P2pStateMachine.this.mGroupNegotiationState);
                            WifiP2pService.P2pStateMachine.this.mPeers.updateStatus(WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.deviceAddress, 1);
                            WifiP2pService.P2pStateMachine.this.sendP2pPeersChangedBroadcast();
                            WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139273);
                        }
                    }
                case 147489:
                case 147491:
                case 147492:
                case 147479:
                case 147487:
                case 147493:
                    while (true)
                    {
                        label234: label248: bool = true;
                        break;
                        WifiP2pService.P2pStateMachine.this.p2pConnectWithPinDisplay(WifiP2pService.P2pStateMachine.this.mSavedPeerConfig, WifiP2pService.FORM_GROUP.booleanValue());
                        break label234;
                        WifiP2pService.P2pStateMachine.access$4202(WifiP2pService.P2pStateMachine.this, localWifiP2pConfig);
                        int j = WifiP2pService.P2pStateMachine.this.configuredNetworkId(WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.deviceAddress);
                        if (j >= 0)
                        {
                            WifiP2pService.P2pStateMachine.this.mWifiNative.p2pReinvoke(j, WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.deviceAddress);
                            break label248;
                        }
                        WifiP2pService.P2pStateMachine.this.mWifiNative.p2pStopFind();
                        if (WifiP2pService.P2pStateMachine.this.mPeers.isGroupOwner(WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.deviceAddress))
                        {
                            WifiP2pService.P2pStateMachine.this.p2pConnectWithPinDisplay(WifiP2pService.P2pStateMachine.this.mSavedPeerConfig, WifiP2pService.JOIN_GROUP.booleanValue());
                            WifiP2pService.P2pStateMachine.this.transitionTo(WifiP2pService.P2pStateMachine.this.mGroupNegotiationState);
                            break label248;
                        }
                        WifiP2pService.P2pStateMachine.this.transitionTo(WifiP2pService.P2pStateMachine.this.mProvisionDiscoveryState);
                        break label248;
                        WifiP2pService.P2pStateMachine.access$4202(WifiP2pService.P2pStateMachine.this, (WifiP2pConfig)paramMessage.obj);
                        WifiP2pService.access$4102(WifiP2pService.this, false);
                        WifiP2pService.access$5202(WifiP2pService.this, false);
                        if (!WifiP2pService.P2pStateMachine.this.sendConnectNoticeToApp(WifiP2pService.P2pStateMachine.this.mPeers.get(WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.deviceAddress), WifiP2pService.P2pStateMachine.this.mSavedPeerConfig))
                        {
                            WifiP2pService.P2pStateMachine.this.transitionTo(WifiP2pService.P2pStateMachine.this.mUserAuthorizingInvitationState);
                            continue;
                            WifiP2pGroup localWifiP2pGroup = (WifiP2pGroup)paramMessage.obj;
                            WifiP2pDevice localWifiP2pDevice1 = localWifiP2pGroup.getOwner();
                            if (localWifiP2pDevice1 != null)
                            {
                                WifiP2pService.P2pStateMachine.access$4202(WifiP2pService.P2pStateMachine.this, new WifiP2pConfig());
                                WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.deviceAddress = localWifiP2pGroup.getOwner().deviceAddress;
                                WifiP2pDevice localWifiP2pDevice2 = WifiP2pService.P2pStateMachine.this.mPeers.get(localWifiP2pDevice1.deviceAddress);
                                if (localWifiP2pDevice2 != null)
                                {
                                    if (!localWifiP2pDevice2.wpsPbcSupported())
                                        break label731;
                                    WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.wps.setup = 0;
                                }
                                while (true)
                                {
                                    WifiP2pService.access$4102(WifiP2pService.this, false);
                                    WifiP2pService.access$5202(WifiP2pService.this, true);
                                    if (WifiP2pService.P2pStateMachine.this.sendConnectNoticeToApp(WifiP2pService.P2pStateMachine.this.mPeers.get(WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.deviceAddress), WifiP2pService.P2pStateMachine.this.mSavedPeerConfig))
                                        break;
                                    WifiP2pService.P2pStateMachine.this.transitionTo(WifiP2pService.P2pStateMachine.this.mUserAuthorizingInvitationState);
                                    break;
                                    label731: if (localWifiP2pDevice2.wpsKeypadSupported())
                                        WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.wps.setup = 2;
                                    else if (localWifiP2pDevice2.wpsDisplaySupported())
                                        WifiP2pService.P2pStateMachine.this.mSavedPeerConfig.wps.setup = 1;
                                }
                                WifiP2pService.P2pStateMachine.this.mWifiNative.p2pFlush();
                                WifiP2pService.access$5702(WifiP2pService.this, null);
                                WifiP2pService.P2pStateMachine.this.sendP2pDiscoveryChangedBroadcast(false);
                            }
                        }
                    }
                case 139277:
                }
                WifiP2pService.access$4102(WifiP2pService.this, true);
                if (WifiP2pService.P2pStateMachine.this.mWifiNative.p2pGroupAdd())
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139279);
                while (true)
                {
                    WifiP2pService.P2pStateMachine.this.transitionTo(WifiP2pService.P2pStateMachine.this.mGroupNegotiationState);
                    break;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139278, 0);
                }
            }
        }

        class P2pEnabledState extends State
        {
            P2pEnabledState()
            {
            }

            public void enter()
            {
                WifiP2pService.P2pStateMachine.this.sendP2pStateChangedBroadcast(true);
                WifiP2pService.this.mNetworkInfo.setIsAvailable(true);
                WifiP2pService.P2pStateMachine.this.sendP2pConnectionChangedBroadcast();
                WifiP2pService.P2pStateMachine.this.initializeP2pSettings();
            }

            public void exit()
            {
                WifiP2pService.P2pStateMachine.this.sendP2pStateChangedBroadcast(false);
                WifiP2pService.this.mNetworkInfo.setIsAvailable(false);
            }

            public boolean processMessage(Message paramMessage)
            {
                boolean bool = false;
                switch (paramMessage.what)
                {
                default:
                    return bool;
                case 131204:
                    if (WifiP2pService.P2pStateMachine.this.mPeers.clear())
                        WifiP2pService.P2pStateMachine.this.sendP2pPeersChangedBroadcast();
                    WifiP2pService.P2pStateMachine.this.mWifiNative.closeSupplicantConnection();
                    WifiP2pService.P2pStateMachine.this.transitionTo(WifiP2pService.P2pStateMachine.this.mP2pDisablingState);
                case 131203:
                case 139315:
                case 139265:
                case 147493:
                case 139268:
                case 139310:
                case 147477:
                case 147478:
                case 139292:
                case 139295:
                case 139298:
                case 139301:
                case 139304:
                case 139307:
                case 147494:
                }
                while (true)
                {
                    bool = true;
                    break;
                    WifiP2pDevice localWifiP2pDevice3 = (WifiP2pDevice)paramMessage.obj;
                    if ((localWifiP2pDevice3 != null) && (WifiP2pService.P2pStateMachine.this.setAndPersistDeviceName(localWifiP2pDevice3.deviceName)))
                    {
                        WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139317);
                    }
                    else
                    {
                        WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139316, 0);
                        continue;
                        WifiP2pService.P2pStateMachine.this.clearSupplicantServiceRequest();
                        if (WifiP2pService.P2pStateMachine.this.mWifiNative.p2pFind(120))
                        {
                            WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139267);
                            WifiP2pService.P2pStateMachine.this.sendP2pDiscoveryChangedBroadcast(true);
                        }
                        else
                        {
                            WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139266, 0);
                            continue;
                            WifiP2pService.P2pStateMachine.this.sendP2pDiscoveryChangedBroadcast(false);
                            continue;
                            if (WifiP2pService.P2pStateMachine.this.mWifiNative.p2pStopFind())
                            {
                                WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139270);
                            }
                            else
                            {
                                WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139269, 0);
                                continue;
                                if (!WifiP2pService.P2pStateMachine.this.updateSupplicantServiceRequest())
                                {
                                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139311, 3);
                                }
                                else if (WifiP2pService.P2pStateMachine.this.mWifiNative.p2pFind(120))
                                {
                                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139312);
                                }
                                else
                                {
                                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139311, 0);
                                    continue;
                                    WifiP2pDevice localWifiP2pDevice2 = (WifiP2pDevice)paramMessage.obj;
                                    if (!WifiP2pService.this.mThisDevice.deviceAddress.equals(localWifiP2pDevice2.deviceAddress))
                                    {
                                        WifiP2pService.P2pStateMachine.this.mPeers.update(localWifiP2pDevice2);
                                        WifiP2pService.P2pStateMachine.this.sendP2pPeersChangedBroadcast();
                                        continue;
                                        WifiP2pDevice localWifiP2pDevice1 = (WifiP2pDevice)paramMessage.obj;
                                        if (WifiP2pService.P2pStateMachine.this.mPeers.remove(localWifiP2pDevice1))
                                        {
                                            WifiP2pService.P2pStateMachine.this.sendP2pPeersChangedBroadcast();
                                            continue;
                                            WifiP2pServiceInfo localWifiP2pServiceInfo2 = (WifiP2pServiceInfo)paramMessage.obj;
                                            if (WifiP2pService.P2pStateMachine.this.addLocalService(paramMessage.replyTo, localWifiP2pServiceInfo2))
                                            {
                                                WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139294);
                                            }
                                            else
                                            {
                                                WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139293);
                                                continue;
                                                WifiP2pServiceInfo localWifiP2pServiceInfo1 = (WifiP2pServiceInfo)paramMessage.obj;
                                                WifiP2pService.P2pStateMachine.this.removeLocalService(paramMessage.replyTo, localWifiP2pServiceInfo1);
                                                WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139297);
                                                continue;
                                                WifiP2pService.P2pStateMachine.this.clearLocalServices(paramMessage.replyTo);
                                                WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139300);
                                                continue;
                                                if (!WifiP2pService.P2pStateMachine.this.addServiceRequest(paramMessage.replyTo, (WifiP2pServiceRequest)paramMessage.obj))
                                                {
                                                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139302);
                                                }
                                                else
                                                {
                                                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139303);
                                                    continue;
                                                    WifiP2pService.P2pStateMachine.this.removeServiceRequest(paramMessage.replyTo, (WifiP2pServiceRequest)paramMessage.obj);
                                                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139306);
                                                    continue;
                                                    WifiP2pService.P2pStateMachine.this.clearServiceRequests(paramMessage.replyTo);
                                                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139309);
                                                    continue;
                                                    Iterator localIterator = ((List)paramMessage.obj).iterator();
                                                    while (localIterator.hasNext())
                                                    {
                                                        WifiP2pServiceResponse localWifiP2pServiceResponse = (WifiP2pServiceResponse)localIterator.next();
                                                        localWifiP2pServiceResponse.setSrcDevice(WifiP2pService.P2pStateMachine.this.mPeers.get(localWifiP2pServiceResponse.getSrcDevice().deviceAddress));
                                                        WifiP2pService.P2pStateMachine.this.sendServiceResponse(localWifiP2pServiceResponse);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        class P2pEnablingState extends State
        {
            P2pEnablingState()
            {
            }

            public void enter()
            {
            }

            public boolean processMessage(Message paramMessage)
            {
                boolean bool;
                switch (paramMessage.what)
                {
                default:
                    bool = false;
                    return bool;
                case 147457:
                    WifiP2pService.P2pStateMachine.this.transitionTo(WifiP2pService.P2pStateMachine.this.mInactiveState);
                case 147458:
                case 131203:
                case 131204:
                }
                while (true)
                {
                    bool = true;
                    break;
                    WifiP2pService.P2pStateMachine.this.loge("P2p socket connection failed");
                    WifiP2pService.P2pStateMachine.this.transitionTo(WifiP2pService.P2pStateMachine.this.mP2pDisabledState);
                    continue;
                    WifiP2pService.P2pStateMachine.this.deferMessage(paramMessage);
                }
            }
        }

        class P2pDisabledState extends State
        {
            P2pDisabledState()
            {
            }

            public void enter()
            {
            }

            public boolean processMessage(Message paramMessage)
            {
                boolean bool;
                switch (paramMessage.what)
                {
                default:
                    bool = false;
                case 131203:
                case 131204:
                }
                while (true)
                {
                    return bool;
                    try
                    {
                        WifiP2pService.this.mNwService.setInterfaceUp(WifiP2pService.this.mInterface);
                        WifiP2pService.P2pStateMachine.this.mWifiMonitor.startMonitoring();
                        WifiP2pService.P2pStateMachine.this.transitionTo(WifiP2pService.P2pStateMachine.this.mP2pEnablingState);
                        bool = true;
                    }
                    catch (RemoteException localRemoteException)
                    {
                        while (true)
                            WifiP2pService.P2pStateMachine.this.loge("Unable to change interface settings: " + localRemoteException);
                    }
                    catch (IllegalStateException localIllegalStateException)
                    {
                        while (true)
                            WifiP2pService.P2pStateMachine.this.loge("Unable to change interface settings: " + localIllegalStateException);
                    }
                }
            }
        }

        class P2pDisablingState extends State
        {
            P2pDisablingState()
            {
            }

            public boolean processMessage(Message paramMessage)
            {
                boolean bool;
                switch (paramMessage.what)
                {
                default:
                    bool = false;
                    return bool;
                case 147458:
                    WifiP2pService.P2pStateMachine.this.transitionTo(WifiP2pService.P2pStateMachine.this.mP2pDisabledState);
                case 131203:
                case 131204:
                }
                while (true)
                {
                    bool = true;
                    break;
                    WifiP2pService.P2pStateMachine.this.deferMessage(paramMessage);
                }
            }
        }

        class P2pNotSupportedState extends State
        {
            P2pNotSupportedState()
            {
            }

            public boolean processMessage(Message paramMessage)
            {
                int i = 1;
                switch (paramMessage.what)
                {
                default:
                    i = 0;
                case 139265:
                case 139268:
                case 139310:
                case 139271:
                case 139274:
                case 139277:
                case 139280:
                case 139292:
                case 139318:
                case 139295:
                case 139298:
                case 139301:
                case 139304:
                case 139307:
                case 139315:
                }
                while (true)
                {
                    return i;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139266, i);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139269, i);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139311, i);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139272, i);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139275, i);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139278, i);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139281, i);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139293, i);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139319, i);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139296, i);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139299, i);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139302, i);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139305, i);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139308, i);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139316, i);
                }
            }
        }

        class DefaultState extends State
        {
            DefaultState()
            {
            }

            public boolean processMessage(Message paramMessage)
            {
                boolean bool1;
                switch (paramMessage.what)
                {
                default:
                    WifiP2pService.P2pStateMachine.this.loge("Unhandled message " + paramMessage);
                    bool1 = false;
                    return bool1;
                case 69632:
                    if (paramMessage.arg1 == 0)
                        WifiP2pService.access$102(WifiP2pService.this, (AsyncChannel)paramMessage.obj);
                    break;
                case 131203:
                case 131204:
                case 143361:
                case 143362:
                case 143363:
                case 147457:
                case 147458:
                case 147459:
                case 147460:
                case 147461:
                case 147462:
                case 147477:
                case 147478:
                case 147484:
                case 147486:
                case 147488:
                case 147493:
                case 147494:
                case 69636:
                case 69633:
                case 139265:
                case 139268:
                case 139310:
                case 139271:
                case 139274:
                case 139277:
                case 139280:
                case 139292:
                case 139295:
                case 139298:
                case 139301:
                case 139304:
                case 139307:
                case 139315:
                case 139283:
                case 139285:
                case 139287:
                case 139318:
                case 147485:
                }
                while (true)
                {
                    bool1 = true;
                    break;
                    WifiP2pService.P2pStateMachine.this.loge("Full connection failure, error = " + paramMessage.arg1);
                    WifiP2pService.access$102(WifiP2pService.this, null);
                    continue;
                    if (paramMessage.arg1 == 2)
                        WifiP2pService.P2pStateMachine.this.loge("Send failed, client connection lost");
                    while (true)
                    {
                        WifiP2pService.access$102(WifiP2pService.this, null);
                        break;
                        WifiP2pService.P2pStateMachine.this.loge("Client connection lost with reason: " + paramMessage.arg1);
                    }
                    new AsyncChannel().connect(WifiP2pService.this.mContext, WifiP2pService.P2pStateMachine.this.getHandler(), paramMessage.replyTo);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139266, 2);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139269, 2);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139311, 2);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139272, 2);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139275, 2);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139278, 2);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139281, 2);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139293, 2);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139296, 2);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139299, 2);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139302, 2);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139305, 2);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139308, 2);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139316, 2);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139284, WifiP2pService.P2pStateMachine.this.mPeers);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139286, WifiP2pService.P2pStateMachine.this.mWifiP2pInfo);
                    continue;
                    WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139288, WifiP2pService.P2pStateMachine.this.mGroup);
                    continue;
                    String str = paramMessage.getData().getString("appPkgName");
                    boolean bool2 = paramMessage.getData().getBoolean("dialogResetFlag");
                    if (WifiP2pService.P2pStateMachine.this.setDialogListenerApp(paramMessage.replyTo, str, bool2))
                    {
                        WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139320);
                    }
                    else
                    {
                        WifiP2pService.P2pStateMachine.this.replyToMessage(paramMessage, 139319, 4);
                        continue;
                        WifiP2pService.P2pStateMachine.access$802(WifiP2pService.P2pStateMachine.this, (WifiP2pGroup)paramMessage.obj);
                        WifiP2pService.P2pStateMachine.this.loge("Unexpected group creation, remove " + WifiP2pService.P2pStateMachine.this.mGroup);
                        WifiP2pService.P2pStateMachine.this.mWifiNative.p2pGroupRemove(WifiP2pService.P2pStateMachine.this.mGroup.getInterface());
                    }
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.p2p.WifiP2pService
 * JD-Core Version:        0.6.2
 */