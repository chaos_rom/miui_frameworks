package android.net.wifi.p2p;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

public class WifiP2pDeviceList
    implements Parcelable
{
    public static final Parcelable.Creator<WifiP2pDeviceList> CREATOR = new Parcelable.Creator()
    {
        public WifiP2pDeviceList createFromParcel(Parcel paramAnonymousParcel)
        {
            WifiP2pDeviceList localWifiP2pDeviceList = new WifiP2pDeviceList();
            int i = paramAnonymousParcel.readInt();
            for (int j = 0; j < i; j++)
                localWifiP2pDeviceList.update((WifiP2pDevice)paramAnonymousParcel.readParcelable(null));
            return localWifiP2pDeviceList;
        }

        public WifiP2pDeviceList[] newArray(int paramAnonymousInt)
        {
            return new WifiP2pDeviceList[paramAnonymousInt];
        }
    };
    private HashMap<String, WifiP2pDevice> mDevices;

    public WifiP2pDeviceList()
    {
        this.mDevices = new HashMap();
    }

    public WifiP2pDeviceList(WifiP2pDeviceList paramWifiP2pDeviceList)
    {
        if (paramWifiP2pDeviceList != null)
        {
            Iterator localIterator = paramWifiP2pDeviceList.getDeviceList().iterator();
            while (localIterator.hasNext())
            {
                WifiP2pDevice localWifiP2pDevice = (WifiP2pDevice)localIterator.next();
                this.mDevices.put(localWifiP2pDevice.deviceAddress, localWifiP2pDevice);
            }
        }
    }

    public WifiP2pDeviceList(ArrayList<WifiP2pDevice> paramArrayList)
    {
        this.mDevices = new HashMap();
        Iterator localIterator = paramArrayList.iterator();
        while (localIterator.hasNext())
        {
            WifiP2pDevice localWifiP2pDevice = (WifiP2pDevice)localIterator.next();
            if (localWifiP2pDevice.deviceAddress != null)
                this.mDevices.put(localWifiP2pDevice.deviceAddress, localWifiP2pDevice);
        }
    }

    public boolean clear()
    {
        if (this.mDevices.isEmpty());
        for (boolean bool = false; ; bool = true)
        {
            return bool;
            this.mDevices.clear();
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public WifiP2pDevice get(String paramString)
    {
        if (paramString == null);
        for (WifiP2pDevice localWifiP2pDevice = null; ; localWifiP2pDevice = (WifiP2pDevice)this.mDevices.get(paramString))
            return localWifiP2pDevice;
    }

    public Collection<WifiP2pDevice> getDeviceList()
    {
        return Collections.unmodifiableCollection(this.mDevices.values());
    }

    public boolean isGroupOwner(String paramString)
    {
        WifiP2pDevice localWifiP2pDevice;
        if (paramString != null)
        {
            localWifiP2pDevice = (WifiP2pDevice)this.mDevices.get(paramString);
            if (localWifiP2pDevice == null);
        }
        for (boolean bool = localWifiP2pDevice.isGroupOwner(); ; bool = false)
            return bool;
    }

    public boolean remove(WifiP2pDevice paramWifiP2pDevice)
    {
        boolean bool = false;
        if ((paramWifiP2pDevice == null) || (paramWifiP2pDevice.deviceAddress == null));
        while (true)
        {
            return bool;
            if (this.mDevices.remove(paramWifiP2pDevice.deviceAddress) != null)
                bool = true;
        }
    }

    public String toString()
    {
        StringBuffer localStringBuffer = new StringBuffer();
        Iterator localIterator = this.mDevices.values().iterator();
        while (localIterator.hasNext())
        {
            WifiP2pDevice localWifiP2pDevice = (WifiP2pDevice)localIterator.next();
            localStringBuffer.append("\n").append(localWifiP2pDevice);
        }
        return localStringBuffer.toString();
    }

    public void update(WifiP2pDevice paramWifiP2pDevice)
    {
        if ((paramWifiP2pDevice == null) || (paramWifiP2pDevice.deviceAddress == null));
        while (true)
        {
            return;
            WifiP2pDevice localWifiP2pDevice = (WifiP2pDevice)this.mDevices.get(paramWifiP2pDevice.deviceAddress);
            if (localWifiP2pDevice != null)
            {
                localWifiP2pDevice.deviceName = paramWifiP2pDevice.deviceName;
                localWifiP2pDevice.primaryDeviceType = paramWifiP2pDevice.primaryDeviceType;
                localWifiP2pDevice.secondaryDeviceType = paramWifiP2pDevice.secondaryDeviceType;
                localWifiP2pDevice.wpsConfigMethodsSupported = paramWifiP2pDevice.wpsConfigMethodsSupported;
                localWifiP2pDevice.deviceCapability = paramWifiP2pDevice.deviceCapability;
                localWifiP2pDevice.groupCapability = paramWifiP2pDevice.groupCapability;
            }
            else
            {
                this.mDevices.put(paramWifiP2pDevice.deviceAddress, paramWifiP2pDevice);
            }
        }
    }

    public void updateGroupCapability(String paramString, int paramInt)
    {
        if (TextUtils.isEmpty(paramString));
        while (true)
        {
            return;
            WifiP2pDevice localWifiP2pDevice = (WifiP2pDevice)this.mDevices.get(paramString);
            if (localWifiP2pDevice != null)
                localWifiP2pDevice.groupCapability = paramInt;
        }
    }

    public void updateStatus(String paramString, int paramInt)
    {
        if (TextUtils.isEmpty(paramString));
        while (true)
        {
            return;
            WifiP2pDevice localWifiP2pDevice = (WifiP2pDevice)this.mDevices.get(paramString);
            if (localWifiP2pDevice != null)
                localWifiP2pDevice.status = paramInt;
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mDevices.size());
        Iterator localIterator = this.mDevices.values().iterator();
        while (localIterator.hasNext())
            paramParcel.writeParcelable((WifiP2pDevice)localIterator.next(), paramInt);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.p2p.WifiP2pDeviceList
 * JD-Core Version:        0.6.2
 */