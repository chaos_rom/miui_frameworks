package android.net.wifi.p2p;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class WifiP2pInfo
    implements Parcelable
{
    public static final Parcelable.Creator<WifiP2pInfo> CREATOR = new Parcelable.Creator()
    {
        public WifiP2pInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            boolean bool1 = false;
            WifiP2pInfo localWifiP2pInfo = new WifiP2pInfo();
            boolean bool2;
            if (paramAnonymousParcel.readByte() == 1)
                bool2 = true;
            while (true)
            {
                localWifiP2pInfo.groupFormed = bool2;
                if (paramAnonymousParcel.readByte() == 1)
                    bool1 = true;
                localWifiP2pInfo.isGroupOwner = bool1;
                if (paramAnonymousParcel.readByte() == 1);
                try
                {
                    localWifiP2pInfo.groupOwnerAddress = InetAddress.getByAddress(paramAnonymousParcel.createByteArray());
                    label61: return localWifiP2pInfo;
                    bool2 = false;
                }
                catch (UnknownHostException localUnknownHostException)
                {
                    break label61;
                }
            }
        }

        public WifiP2pInfo[] newArray(int paramAnonymousInt)
        {
            return new WifiP2pInfo[paramAnonymousInt];
        }
    };
    public boolean groupFormed;
    public InetAddress groupOwnerAddress;
    public boolean isGroupOwner;

    public WifiP2pInfo()
    {
    }

    public WifiP2pInfo(WifiP2pInfo paramWifiP2pInfo)
    {
        if (paramWifiP2pInfo != null)
        {
            this.groupFormed = paramWifiP2pInfo.groupFormed;
            this.isGroupOwner = paramWifiP2pInfo.isGroupOwner;
            this.groupOwnerAddress = paramWifiP2pInfo.groupOwnerAddress;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public String toString()
    {
        StringBuffer localStringBuffer = new StringBuffer();
        localStringBuffer.append("groupFormed: ").append(this.groupFormed).append("isGroupOwner: ").append(this.isGroupOwner).append("groupOwnerAddress: ").append(this.groupOwnerAddress);
        return localStringBuffer.toString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        byte b1;
        byte b2;
        if (this.groupFormed)
        {
            b1 = 1;
            paramParcel.writeByte(b1);
            if (!this.isGroupOwner)
                break label59;
            b2 = 1;
            label24: paramParcel.writeByte(b2);
            if (this.groupOwnerAddress == null)
                break label65;
            paramParcel.writeByte((byte)1);
            paramParcel.writeByteArray(this.groupOwnerAddress.getAddress());
        }
        while (true)
        {
            return;
            b1 = 0;
            break;
            label59: b2 = 0;
            break label24;
            label65: paramParcel.writeByte((byte)0);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.p2p.WifiP2pInfo
 * JD-Core Version:        0.6.2
 */