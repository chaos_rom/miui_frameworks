package android.net.wifi.p2p;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Log;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WifiP2pDevice
    implements Parcelable
{
    public static final int AVAILABLE = 3;
    public static final int CONNECTED = 0;
    public static final Parcelable.Creator<WifiP2pDevice> CREATOR = new Parcelable.Creator()
    {
        public WifiP2pDevice createFromParcel(Parcel paramAnonymousParcel)
        {
            WifiP2pDevice localWifiP2pDevice = new WifiP2pDevice();
            localWifiP2pDevice.deviceName = paramAnonymousParcel.readString();
            localWifiP2pDevice.deviceAddress = paramAnonymousParcel.readString();
            localWifiP2pDevice.primaryDeviceType = paramAnonymousParcel.readString();
            localWifiP2pDevice.secondaryDeviceType = paramAnonymousParcel.readString();
            localWifiP2pDevice.wpsConfigMethodsSupported = paramAnonymousParcel.readInt();
            localWifiP2pDevice.deviceCapability = paramAnonymousParcel.readInt();
            localWifiP2pDevice.groupCapability = paramAnonymousParcel.readInt();
            localWifiP2pDevice.status = paramAnonymousParcel.readInt();
            return localWifiP2pDevice;
        }

        public WifiP2pDevice[] newArray(int paramAnonymousInt)
        {
            return new WifiP2pDevice[paramAnonymousInt];
        }
    };
    private static final int DEVICE_CAPAB_CLIENT_DISCOVERABILITY = 2;
    private static final int DEVICE_CAPAB_CONCURRENT_OPER = 4;
    private static final int DEVICE_CAPAB_DEVICE_LIMIT = 16;
    private static final int DEVICE_CAPAB_INFRA_MANAGED = 8;
    private static final int DEVICE_CAPAB_INVITATION_PROCEDURE = 32;
    private static final int DEVICE_CAPAB_SERVICE_DISCOVERY = 1;
    public static final int FAILED = 2;
    private static final int GROUP_CAPAB_CROSS_CONN = 16;
    private static final int GROUP_CAPAB_GROUP_FORMATION = 64;
    private static final int GROUP_CAPAB_GROUP_LIMIT = 4;
    private static final int GROUP_CAPAB_GROUP_OWNER = 1;
    private static final int GROUP_CAPAB_INTRA_BSS_DIST = 8;
    private static final int GROUP_CAPAB_PERSISTENT_GROUP = 2;
    private static final int GROUP_CAPAB_PERSISTENT_RECONN = 32;
    public static final int INVITED = 1;
    private static final String TAG = "WifiP2pDevice";
    public static final int UNAVAILABLE = 4;
    private static final int WPS_CONFIG_DISPLAY = 8;
    private static final int WPS_CONFIG_KEYPAD = 256;
    private static final int WPS_CONFIG_PUSHBUTTON = 128;
    private static final Pattern detailedDevicePattern = Pattern.compile("((?:[0-9a-f]{2}:){5}[0-9a-f]{2}) (\\d+ )?p2p_dev_addr=((?:[0-9a-f]{2}:){5}[0-9a-f]{2}) pri_dev_type=(\\d+-[0-9a-fA-F]+-\\d+) name='(.*)' config_methods=(0x[0-9a-fA-F]+) dev_capab=(0x[0-9a-fA-F]+) group_capab=(0x[0-9a-fA-F]+)");
    private static final Pattern threeTokenPattern;
    private static final Pattern twoTokenPattern = Pattern.compile("(p2p_dev_addr=)?((?:[0-9a-f]{2}:){5}[0-9a-f]{2})");
    public String deviceAddress = "";
    public int deviceCapability;
    public String deviceName = "";
    public int groupCapability;
    public String primaryDeviceType;
    public String secondaryDeviceType;
    public int status = 4;
    public int wpsConfigMethodsSupported;

    static
    {
        threeTokenPattern = Pattern.compile("(?:[0-9a-f]{2}:){5}[0-9a-f]{2} p2p_dev_addr=((?:[0-9a-f]{2}:){5}[0-9a-f]{2})");
    }

    public WifiP2pDevice()
    {
    }

    public WifiP2pDevice(WifiP2pDevice paramWifiP2pDevice)
    {
        if (paramWifiP2pDevice != null)
        {
            this.deviceName = paramWifiP2pDevice.deviceName;
            this.deviceAddress = paramWifiP2pDevice.deviceAddress;
            this.primaryDeviceType = paramWifiP2pDevice.primaryDeviceType;
            this.secondaryDeviceType = paramWifiP2pDevice.secondaryDeviceType;
            this.wpsConfigMethodsSupported = paramWifiP2pDevice.wpsConfigMethodsSupported;
            this.deviceCapability = paramWifiP2pDevice.deviceCapability;
            this.groupCapability = paramWifiP2pDevice.groupCapability;
            this.status = paramWifiP2pDevice.status;
        }
    }

    public WifiP2pDevice(String paramString)
        throws IllegalArgumentException
    {
        String[] arrayOfString = paramString.split("[ \n]");
        if (arrayOfString.length < 1)
            throw new IllegalArgumentException("Malformed supplicant event");
        Matcher localMatcher3;
        switch (arrayOfString.length)
        {
        default:
            localMatcher3 = detailedDevicePattern.matcher(paramString);
            if (!localMatcher3.find())
                throw new IllegalArgumentException("Malformed supplicant event");
            break;
        case 1:
            this.deviceAddress = paramString;
        case 2:
        case 3:
        }
        while (true)
        {
            return;
            Matcher localMatcher2 = twoTokenPattern.matcher(paramString);
            if (!localMatcher2.find())
                throw new IllegalArgumentException("Malformed supplicant event");
            this.deviceAddress = localMatcher2.group(2);
            continue;
            Matcher localMatcher1 = threeTokenPattern.matcher(paramString);
            if (!localMatcher1.find())
                throw new IllegalArgumentException("Malformed supplicant event");
            this.deviceAddress = localMatcher1.group(1);
            continue;
            this.deviceAddress = localMatcher3.group(3);
            this.primaryDeviceType = localMatcher3.group(4);
            this.deviceName = localMatcher3.group(5);
            this.wpsConfigMethodsSupported = parseHex(localMatcher3.group(6));
            this.deviceCapability = parseHex(localMatcher3.group(7));
            this.groupCapability = parseHex(localMatcher3.group(8));
            if (arrayOfString[0].startsWith("P2P-DEVICE-FOUND"))
                this.status = 3;
        }
    }

    private int parseHex(String paramString)
    {
        int i = 0;
        if ((paramString.startsWith("0x")) || (paramString.startsWith("0X")))
            paramString = paramString.substring(2);
        try
        {
            int j = Integer.parseInt(paramString, 16);
            i = j;
            return i;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            while (true)
                Log.e("WifiP2pDevice", "Failed to parse hex string " + paramString);
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = true;
        if (this == paramObject);
        while (true)
        {
            return bool;
            if (!(paramObject instanceof WifiP2pDevice))
            {
                bool = false;
            }
            else
            {
                WifiP2pDevice localWifiP2pDevice = (WifiP2pDevice)paramObject;
                if ((localWifiP2pDevice == null) || (localWifiP2pDevice.deviceAddress == null))
                {
                    if (this.deviceAddress != null)
                        bool = false;
                }
                else
                    bool = localWifiP2pDevice.deviceAddress.equals(this.deviceAddress);
            }
        }
    }

    public boolean isGroupOwner()
    {
        if ((0x1 & this.groupCapability) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isServiceDiscoveryCapable()
    {
        if ((0x1 & this.deviceCapability) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public String toString()
    {
        StringBuffer localStringBuffer = new StringBuffer();
        localStringBuffer.append("Device: ").append(this.deviceName);
        localStringBuffer.append("\n deviceAddress: ").append(this.deviceAddress);
        localStringBuffer.append("\n primary type: ").append(this.primaryDeviceType);
        localStringBuffer.append("\n secondary type: ").append(this.secondaryDeviceType);
        localStringBuffer.append("\n wps: ").append(this.wpsConfigMethodsSupported);
        localStringBuffer.append("\n grpcapab: ").append(this.groupCapability);
        localStringBuffer.append("\n devcapab: ").append(this.deviceCapability);
        localStringBuffer.append("\n status: ").append(this.status);
        return localStringBuffer.toString();
    }

    public boolean wpsDisplaySupported()
    {
        if ((0x8 & this.wpsConfigMethodsSupported) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean wpsKeypadSupported()
    {
        if ((0x100 & this.wpsConfigMethodsSupported) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean wpsPbcSupported()
    {
        if ((0x80 & this.wpsConfigMethodsSupported) != 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.deviceName);
        paramParcel.writeString(this.deviceAddress);
        paramParcel.writeString(this.primaryDeviceType);
        paramParcel.writeString(this.secondaryDeviceType);
        paramParcel.writeInt(this.wpsConfigMethodsSupported);
        paramParcel.writeInt(this.deviceCapability);
        paramParcel.writeInt(this.groupCapability);
        paramParcel.writeInt(this.status);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.p2p.WifiP2pDevice
 * JD-Core Version:        0.6.2
 */