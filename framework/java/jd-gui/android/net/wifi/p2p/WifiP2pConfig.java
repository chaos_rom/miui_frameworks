package android.net.wifi.p2p;

import android.net.wifi.WpsInfo;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class WifiP2pConfig
    implements Parcelable
{
    public static final Parcelable.Creator<WifiP2pConfig> CREATOR = new Parcelable.Creator()
    {
        public WifiP2pConfig createFromParcel(Parcel paramAnonymousParcel)
        {
            WifiP2pConfig localWifiP2pConfig = new WifiP2pConfig();
            localWifiP2pConfig.deviceAddress = paramAnonymousParcel.readString();
            localWifiP2pConfig.wps = ((WpsInfo)paramAnonymousParcel.readParcelable(null));
            localWifiP2pConfig.groupOwnerIntent = paramAnonymousParcel.readInt();
            localWifiP2pConfig.persist = WifiP2pConfig.Persist.valueOf(paramAnonymousParcel.readString());
            return localWifiP2pConfig;
        }

        public WifiP2pConfig[] newArray(int paramAnonymousInt)
        {
            return new WifiP2pConfig[paramAnonymousInt];
        }
    };
    public String deviceAddress;
    public int groupOwnerIntent = -1;
    public Persist persist = Persist.SYSTEM_DEFAULT;
    public WpsInfo wps;

    public WifiP2pConfig()
    {
        this.wps = new WpsInfo();
        this.wps.setup = 0;
    }

    public WifiP2pConfig(WifiP2pConfig paramWifiP2pConfig)
    {
        if (paramWifiP2pConfig != null)
        {
            this.deviceAddress = paramWifiP2pConfig.deviceAddress;
            this.wps = new WpsInfo(paramWifiP2pConfig.wps);
            this.groupOwnerIntent = paramWifiP2pConfig.groupOwnerIntent;
            this.persist = paramWifiP2pConfig.persist;
        }
    }

    public WifiP2pConfig(String paramString)
        throws IllegalArgumentException
    {
        String[] arrayOfString1 = paramString.split(" ");
        if ((arrayOfString1.length < 2) || (!arrayOfString1[0].equals("P2P-GO-NEG-REQUEST")))
            throw new IllegalArgumentException("Malformed supplicant event");
        this.deviceAddress = arrayOfString1[1];
        this.wps = new WpsInfo();
        String[] arrayOfString2;
        if (arrayOfString1.length > 2)
            arrayOfString2 = arrayOfString1[2].split("=");
        try
        {
            int j = Integer.parseInt(arrayOfString2[1]);
            i = j;
            switch (i)
            {
            case 2:
            case 3:
            default:
                this.wps.setup = 0;
                return;
            case 1:
            case 4:
            case 5:
            }
        }
        catch (NumberFormatException localNumberFormatException)
        {
            while (true)
            {
                int i = 0;
                continue;
                this.wps.setup = 1;
                continue;
                this.wps.setup = 0;
                continue;
                this.wps.setup = 2;
            }
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public String toString()
    {
        StringBuffer localStringBuffer = new StringBuffer();
        localStringBuffer.append("\n address: ").append(this.deviceAddress);
        localStringBuffer.append("\n wps: ").append(this.wps);
        localStringBuffer.append("\n groupOwnerIntent: ").append(this.groupOwnerIntent);
        localStringBuffer.append("\n persist: ").append(this.persist.toString());
        return localStringBuffer.toString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.deviceAddress);
        paramParcel.writeParcelable(this.wps, paramInt);
        paramParcel.writeInt(this.groupOwnerIntent);
        paramParcel.writeString(this.persist.name());
    }

    public static enum Persist
    {
        static
        {
            NO = new Persist("NO", 2);
            Persist[] arrayOfPersist = new Persist[3];
            arrayOfPersist[0] = SYSTEM_DEFAULT;
            arrayOfPersist[1] = YES;
            arrayOfPersist[2] = NO;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.p2p.WifiP2pConfig
 * JD-Core Version:        0.6.2
 */