package android.net.wifi.p2p;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WifiP2pGroup
    implements Parcelable
{
    public static final Parcelable.Creator<WifiP2pGroup> CREATOR = new Parcelable.Creator()
    {
        public WifiP2pGroup createFromParcel(Parcel paramAnonymousParcel)
        {
            WifiP2pGroup localWifiP2pGroup = new WifiP2pGroup();
            localWifiP2pGroup.setNetworkName(paramAnonymousParcel.readString());
            localWifiP2pGroup.setOwner((WifiP2pDevice)paramAnonymousParcel.readParcelable(null));
            if (paramAnonymousParcel.readByte() == 1);
            for (boolean bool = true; ; bool = false)
            {
                localWifiP2pGroup.setIsGroupOwner(bool);
                int i = paramAnonymousParcel.readInt();
                for (int j = 0; j < i; j++)
                    localWifiP2pGroup.addClient((WifiP2pDevice)paramAnonymousParcel.readParcelable(null));
            }
            localWifiP2pGroup.setPassphrase(paramAnonymousParcel.readString());
            localWifiP2pGroup.setInterface(paramAnonymousParcel.readString());
            return localWifiP2pGroup;
        }

        public WifiP2pGroup[] newArray(int paramAnonymousInt)
        {
            return new WifiP2pGroup[paramAnonymousInt];
        }
    };
    private static final Pattern groupStartedPattern = Pattern.compile("ssid=\"(.+)\" freq=(\\d+) (?:psk=)?([0-9a-fA-F]{64})?(?:passphrase=)?(?:\"(.{8,63})\")? go_dev_addr=((?:[0-9a-f]{2}:){5}[0-9a-f]{2})");
    private List<WifiP2pDevice> mClients = new ArrayList();
    private String mInterface;
    private boolean mIsGroupOwner;
    private String mNetworkName;
    private WifiP2pDevice mOwner;
    private String mPassphrase;

    public WifiP2pGroup()
    {
    }

    public WifiP2pGroup(WifiP2pGroup paramWifiP2pGroup)
    {
        if (paramWifiP2pGroup != null)
        {
            this.mNetworkName = paramWifiP2pGroup.getNetworkName();
            this.mOwner = new WifiP2pDevice(paramWifiP2pGroup.getOwner());
            this.mIsGroupOwner = paramWifiP2pGroup.mIsGroupOwner;
            Iterator localIterator = paramWifiP2pGroup.getClientList().iterator();
            while (localIterator.hasNext())
            {
                WifiP2pDevice localWifiP2pDevice = (WifiP2pDevice)localIterator.next();
                this.mClients.add(localWifiP2pDevice);
            }
            this.mPassphrase = paramWifiP2pGroup.getPassphrase();
            this.mInterface = paramWifiP2pGroup.getInterface();
        }
    }

    public WifiP2pGroup(String paramString)
        throws IllegalArgumentException
    {
        String[] arrayOfString1 = paramString.split(" ");
        if (arrayOfString1.length < 3)
            throw new IllegalArgumentException("Malformed supplicant event");
        if (arrayOfString1[0].startsWith("P2P-GROUP"))
        {
            this.mInterface = arrayOfString1[1];
            this.mIsGroupOwner = arrayOfString1[2].equals("GO");
            Matcher localMatcher = groupStartedPattern.matcher(paramString);
            if (!localMatcher.find());
            while (true)
            {
                return;
                this.mNetworkName = localMatcher.group(1);
                this.mPassphrase = localMatcher.group(4);
                this.mOwner = new WifiP2pDevice(localMatcher.group(5));
            }
        }
        if (arrayOfString1[0].equals("P2P-INVITATION-RECEIVED"))
        {
            int i = arrayOfString1.length;
            int j = 0;
            label143: String[] arrayOfString2;
            if (j < i)
            {
                arrayOfString2 = arrayOfString1[j].split("=");
                if (arrayOfString2.length == 2)
                    break label173;
            }
            while (true)
            {
                j++;
                break label143;
                break;
                label173: if (arrayOfString2[0].equals("go_dev_addr"))
                    this.mOwner = new WifiP2pDevice(arrayOfString2[1]);
            }
        }
        throw new IllegalArgumentException("Malformed supplicant event");
    }

    public void addClient(WifiP2pDevice paramWifiP2pDevice)
    {
        Iterator localIterator = this.mClients.iterator();
        do
            if (!localIterator.hasNext())
                break;
        while (!((WifiP2pDevice)localIterator.next()).equals(paramWifiP2pDevice));
        while (true)
        {
            return;
            this.mClients.add(paramWifiP2pDevice);
        }
    }

    public void addClient(String paramString)
    {
        addClient(new WifiP2pDevice(paramString));
    }

    public boolean contains(WifiP2pDevice paramWifiP2pDevice)
    {
        if ((this.mOwner.equals(paramWifiP2pDevice)) || (this.mClients.contains(paramWifiP2pDevice)));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public int describeContents()
    {
        return 0;
    }

    public Collection<WifiP2pDevice> getClientList()
    {
        return Collections.unmodifiableCollection(this.mClients);
    }

    public String getInterface()
    {
        return this.mInterface;
    }

    public String getNetworkName()
    {
        return this.mNetworkName;
    }

    public WifiP2pDevice getOwner()
    {
        return this.mOwner;
    }

    public String getPassphrase()
    {
        return this.mPassphrase;
    }

    public boolean isClientListEmpty()
    {
        if (this.mClients.size() == 0);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isGroupOwner()
    {
        return this.mIsGroupOwner;
    }

    public boolean removeClient(WifiP2pDevice paramWifiP2pDevice)
    {
        return this.mClients.remove(paramWifiP2pDevice);
    }

    public boolean removeClient(String paramString)
    {
        return this.mClients.remove(new WifiP2pDevice(paramString));
    }

    public void setInterface(String paramString)
    {
        this.mInterface = paramString;
    }

    public void setIsGroupOwner(boolean paramBoolean)
    {
        this.mIsGroupOwner = paramBoolean;
    }

    public void setNetworkName(String paramString)
    {
        this.mNetworkName = paramString;
    }

    public void setOwner(WifiP2pDevice paramWifiP2pDevice)
    {
        this.mOwner = paramWifiP2pDevice;
    }

    public void setPassphrase(String paramString)
    {
        this.mPassphrase = paramString;
    }

    public String toString()
    {
        StringBuffer localStringBuffer = new StringBuffer();
        localStringBuffer.append("network: ").append(this.mNetworkName);
        localStringBuffer.append("\n isGO: ").append(this.mIsGroupOwner);
        localStringBuffer.append("\n GO: ").append(this.mOwner);
        Iterator localIterator = this.mClients.iterator();
        while (localIterator.hasNext())
        {
            WifiP2pDevice localWifiP2pDevice = (WifiP2pDevice)localIterator.next();
            localStringBuffer.append("\n Client: ").append(localWifiP2pDevice);
        }
        localStringBuffer.append("\n interface: ").append(this.mInterface);
        return localStringBuffer.toString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.mNetworkName);
        paramParcel.writeParcelable(this.mOwner, paramInt);
        if (this.mIsGroupOwner);
        for (byte b = 1; ; b = 0)
        {
            paramParcel.writeByte(b);
            paramParcel.writeInt(this.mClients.size());
            Iterator localIterator = this.mClients.iterator();
            while (localIterator.hasNext())
                paramParcel.writeParcelable((WifiP2pDevice)localIterator.next(), paramInt);
        }
        paramParcel.writeString(this.mPassphrase);
        paramParcel.writeString(this.mInterface);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.p2p.WifiP2pGroup
 * JD-Core Version:        0.6.2
 */