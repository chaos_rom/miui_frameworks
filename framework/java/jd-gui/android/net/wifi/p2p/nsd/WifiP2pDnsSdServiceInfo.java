package android.net.wifi.p2p.nsd;

import android.net.nsd.DnsSdTxtRecord;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class WifiP2pDnsSdServiceInfo extends WifiP2pServiceInfo
{
    public static final int DNS_TYPE_PTR = 12;
    public static final int DNS_TYPE_TXT = 16;
    public static final int VERSION_1 = 1;
    private static final Map<String, String> sVmPacket = new HashMap();

    static
    {
        sVmPacket.put("_tcp.local.", "c00c");
        sVmPacket.put("local.", "c011");
        sVmPacket.put("_udp.local.", "c01c");
    }

    private WifiP2pDnsSdServiceInfo(List<String> paramList)
    {
        super(paramList);
    }

    private static String compressDnsName(String paramString)
    {
        StringBuffer localStringBuffer = new StringBuffer();
        while (true)
        {
            String str1 = (String)sVmPacket.get(paramString);
            if (str1 != null)
                localStringBuffer.append(str1);
            int i;
            while (true)
            {
                return localStringBuffer.toString();
                i = paramString.indexOf('.');
                if (i != -1)
                    break;
                if (paramString.length() > 0)
                {
                    Object[] arrayOfObject2 = new Object[1];
                    arrayOfObject2[0] = Integer.valueOf(paramString.length());
                    localStringBuffer.append(String.format("%02x", arrayOfObject2));
                    localStringBuffer.append(WifiP2pServiceInfo.bin2HexStr(paramString.getBytes()));
                }
                localStringBuffer.append("00");
            }
            String str2 = paramString.substring(0, i);
            paramString = paramString.substring(i + 1);
            Object[] arrayOfObject1 = new Object[1];
            arrayOfObject1[0] = Integer.valueOf(str2.length());
            localStringBuffer.append(String.format("%02x", arrayOfObject1));
            localStringBuffer.append(WifiP2pServiceInfo.bin2HexStr(str2.getBytes()));
        }
    }

    private static String createPtrServiceQuery(String paramString1, String paramString2)
    {
        StringBuffer localStringBuffer = new StringBuffer();
        localStringBuffer.append("bonjour ");
        localStringBuffer.append(createRequest(paramString2 + ".local.", 12, 1));
        localStringBuffer.append(" ");
        byte[] arrayOfByte = paramString1.getBytes();
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = Integer.valueOf(arrayOfByte.length);
        localStringBuffer.append(String.format("%02x", arrayOfObject));
        localStringBuffer.append(WifiP2pServiceInfo.bin2HexStr(arrayOfByte));
        localStringBuffer.append("c027");
        return localStringBuffer.toString();
    }

    static String createRequest(String paramString, int paramInt1, int paramInt2)
    {
        StringBuffer localStringBuffer = new StringBuffer();
        if (paramInt1 == 16)
            paramString = paramString.toLowerCase();
        localStringBuffer.append(compressDnsName(paramString));
        Object[] arrayOfObject1 = new Object[1];
        arrayOfObject1[0] = Integer.valueOf(paramInt1);
        localStringBuffer.append(String.format("%04x", arrayOfObject1));
        Object[] arrayOfObject2 = new Object[1];
        arrayOfObject2[0] = Integer.valueOf(paramInt2);
        localStringBuffer.append(String.format("%02x", arrayOfObject2));
        return localStringBuffer.toString();
    }

    private static String createTxtServiceQuery(String paramString1, String paramString2, DnsSdTxtRecord paramDnsSdTxtRecord)
    {
        StringBuffer localStringBuffer = new StringBuffer();
        localStringBuffer.append("bonjour ");
        localStringBuffer.append(createRequest(paramString1 + "." + paramString2 + ".local.", 16, 1));
        localStringBuffer.append(" ");
        byte[] arrayOfByte = paramDnsSdTxtRecord.getRawData();
        if (arrayOfByte.length == 0)
            localStringBuffer.append("00");
        while (true)
        {
            return localStringBuffer.toString();
            localStringBuffer.append(bin2HexStr(arrayOfByte));
        }
    }

    public static WifiP2pDnsSdServiceInfo newInstance(String paramString1, String paramString2, Map<String, String> paramMap)
    {
        if ((TextUtils.isEmpty(paramString1)) || (TextUtils.isEmpty(paramString2)))
            throw new IllegalArgumentException("instance name or service type cannot be empty");
        DnsSdTxtRecord localDnsSdTxtRecord = new DnsSdTxtRecord();
        if (paramMap != null)
        {
            Iterator localIterator = paramMap.keySet().iterator();
            while (localIterator.hasNext())
            {
                String str = (String)localIterator.next();
                localDnsSdTxtRecord.set(str, (String)paramMap.get(str));
            }
        }
        ArrayList localArrayList = new ArrayList();
        localArrayList.add(createPtrServiceQuery(paramString1, paramString2));
        localArrayList.add(createTxtServiceQuery(paramString1, paramString2, localDnsSdTxtRecord));
        return new WifiP2pDnsSdServiceInfo(localArrayList);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceInfo
 * JD-Core Version:        0.6.2
 */