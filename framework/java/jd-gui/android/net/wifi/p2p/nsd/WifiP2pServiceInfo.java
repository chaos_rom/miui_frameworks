package android.net.wifi.p2p.nsd;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.List;

public class WifiP2pServiceInfo
    implements Parcelable
{
    public static final Parcelable.Creator<WifiP2pServiceInfo> CREATOR = new Parcelable.Creator()
    {
        public WifiP2pServiceInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            ArrayList localArrayList = new ArrayList();
            paramAnonymousParcel.readStringList(localArrayList);
            return new WifiP2pServiceInfo(localArrayList);
        }

        public WifiP2pServiceInfo[] newArray(int paramAnonymousInt)
        {
            return new WifiP2pServiceInfo[paramAnonymousInt];
        }
    };
    public static final int SERVICE_TYPE_ALL = 0;
    public static final int SERVICE_TYPE_BONJOUR = 1;
    public static final int SERVICE_TYPE_UPNP = 2;
    public static final int SERVICE_TYPE_VENDOR_SPECIFIC = 255;
    public static final int SERVICE_TYPE_WS_DISCOVERY = 3;
    private List<String> mQueryList;

    protected WifiP2pServiceInfo(List<String> paramList)
    {
        if (paramList == null)
            throw new IllegalArgumentException("query list cannot be null");
        this.mQueryList = paramList;
    }

    static String bin2HexStr(byte[] paramArrayOfByte)
    {
        StringBuffer localStringBuffer = new StringBuffer();
        int i = paramArrayOfByte.length;
        int j = 0;
        String str1;
        while (true)
            if (j < i)
            {
                int k = 0xFF & paramArrayOfByte[j];
                try
                {
                    String str2 = Integer.toHexString(k);
                    if (str2.length() == 1)
                        localStringBuffer.append('0');
                    localStringBuffer.append(str2);
                    j++;
                }
                catch (Exception localException)
                {
                    localException.printStackTrace();
                    str1 = null;
                }
            }
        while (true)
        {
            return str1;
            str1 = localStringBuffer.toString();
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool;
        if (paramObject == this)
            bool = true;
        while (true)
        {
            return bool;
            if (!(paramObject instanceof WifiP2pServiceInfo))
            {
                bool = false;
            }
            else
            {
                WifiP2pServiceInfo localWifiP2pServiceInfo = (WifiP2pServiceInfo)paramObject;
                bool = this.mQueryList.equals(localWifiP2pServiceInfo.mQueryList);
            }
        }
    }

    public List<String> getSupplicantQueryList()
    {
        return this.mQueryList;
    }

    public int hashCode()
    {
        if (this.mQueryList == null);
        for (int i = 0; ; i = this.mQueryList.hashCode())
            return i + 527;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeStringList(this.mQueryList);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.p2p.nsd.WifiP2pServiceInfo
 * JD-Core Version:        0.6.2
 */