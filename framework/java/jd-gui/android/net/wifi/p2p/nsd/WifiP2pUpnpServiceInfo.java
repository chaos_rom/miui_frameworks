package android.net.wifi.p2p.nsd;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public class WifiP2pUpnpServiceInfo extends WifiP2pServiceInfo
{
    public static final int VERSION_1_0 = 16;

    private WifiP2pUpnpServiceInfo(List<String> paramList)
    {
        super(paramList);
    }

    private static String createSupplicantQuery(String paramString1, String paramString2)
    {
        StringBuffer localStringBuffer = new StringBuffer();
        localStringBuffer.append("upnp ");
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = Integer.valueOf(16);
        localStringBuffer.append(String.format("%02x ", arrayOfObject));
        localStringBuffer.append("uuid:");
        localStringBuffer.append(paramString1);
        if (paramString2 != null)
        {
            localStringBuffer.append("::");
            localStringBuffer.append(paramString2);
        }
        return localStringBuffer.toString();
    }

    public static WifiP2pUpnpServiceInfo newInstance(String paramString1, String paramString2, List<String> paramList)
    {
        if ((paramString1 == null) || (paramString2 == null))
            throw new IllegalArgumentException("uuid or device cannnot be null");
        UUID.fromString(paramString1);
        ArrayList localArrayList = new ArrayList();
        localArrayList.add(createSupplicantQuery(paramString1, null));
        localArrayList.add(createSupplicantQuery(paramString1, "upnp:rootdevice"));
        localArrayList.add(createSupplicantQuery(paramString1, paramString2));
        if (paramList != null)
        {
            Iterator localIterator = paramList.iterator();
            while (localIterator.hasNext())
                localArrayList.add(createSupplicantQuery(paramString1, (String)localIterator.next()));
        }
        return new WifiP2pUpnpServiceInfo(localArrayList);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.p2p.nsd.WifiP2pUpnpServiceInfo
 * JD-Core Version:        0.6.2
 */