package android.net.wifi.p2p.nsd;

import android.net.wifi.p2p.WifiP2pDevice;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class WifiP2pUpnpServiceResponse extends WifiP2pServiceResponse
{
    private List<String> mUniqueServiceNames;
    private int mVersion;

    protected WifiP2pUpnpServiceResponse(int paramInt1, int paramInt2, WifiP2pDevice paramWifiP2pDevice, byte[] paramArrayOfByte)
    {
        super(2, paramInt1, paramInt2, paramWifiP2pDevice, paramArrayOfByte);
        if (!parse())
            throw new IllegalArgumentException("Malformed upnp service response");
    }

    static WifiP2pUpnpServiceResponse newInstance(int paramInt1, int paramInt2, WifiP2pDevice paramWifiP2pDevice, byte[] paramArrayOfByte)
    {
        WifiP2pUpnpServiceResponse localWifiP2pUpnpServiceResponse;
        if (paramInt1 != 0)
            localWifiP2pUpnpServiceResponse = new WifiP2pUpnpServiceResponse(paramInt1, paramInt2, paramWifiP2pDevice, null);
        while (true)
        {
            return localWifiP2pUpnpServiceResponse;
            try
            {
                localWifiP2pUpnpServiceResponse = new WifiP2pUpnpServiceResponse(paramInt1, paramInt2, paramWifiP2pDevice, paramArrayOfByte);
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
                localIllegalArgumentException.printStackTrace();
                localWifiP2pUpnpServiceResponse = null;
            }
        }
    }

    private boolean parse()
    {
        int i = 1;
        if (this.mData == null);
        while (true)
        {
            return i;
            int j;
            if (this.mData.length < i)
            {
                j = 0;
            }
            else
            {
                this.mVersion = (0xFF & this.mData[0]);
                String[] arrayOfString = new String(this.mData, j, -1 + this.mData.length).split(",");
                this.mUniqueServiceNames = new ArrayList();
                int k = arrayOfString.length;
                for (int m = 0; m < k; m++)
                {
                    String str = arrayOfString[m];
                    this.mUniqueServiceNames.add(str);
                }
            }
        }
    }

    public List<String> getUniqueServiceNames()
    {
        return this.mUniqueServiceNames;
    }

    public int getVersion()
    {
        return this.mVersion;
    }

    public String toString()
    {
        StringBuffer localStringBuffer1 = new StringBuffer();
        localStringBuffer1.append("serviceType:UPnP(").append(this.mServiceType).append(")");
        localStringBuffer1.append(" status:").append(WifiP2pServiceResponse.Status.toString(this.mStatus));
        localStringBuffer1.append(" srcAddr:").append(this.mDevice.deviceAddress);
        StringBuffer localStringBuffer2 = localStringBuffer1.append(" version:");
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = Integer.valueOf(this.mVersion);
        localStringBuffer2.append(String.format("%02x", arrayOfObject));
        if (this.mUniqueServiceNames != null)
        {
            Iterator localIterator = this.mUniqueServiceNames.iterator();
            while (localIterator.hasNext())
            {
                String str = (String)localIterator.next();
                localStringBuffer1.append(" usn:").append(str);
            }
        }
        return localStringBuffer1.toString();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.p2p.nsd.WifiP2pUpnpServiceResponse
 * JD-Core Version:        0.6.2
 */