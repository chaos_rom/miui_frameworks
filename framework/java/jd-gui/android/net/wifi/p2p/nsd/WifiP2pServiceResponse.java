package android.net.wifi.p2p.nsd;

import android.net.wifi.p2p.WifiP2pDevice;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WifiP2pServiceResponse
    implements Parcelable
{
    public static final Parcelable.Creator<WifiP2pServiceResponse> CREATOR = new Parcelable.Creator()
    {
        public WifiP2pServiceResponse createFromParcel(Parcel paramAnonymousParcel)
        {
            int i = paramAnonymousParcel.readInt();
            int j = paramAnonymousParcel.readInt();
            int k = paramAnonymousParcel.readInt();
            WifiP2pDevice localWifiP2pDevice = (WifiP2pDevice)paramAnonymousParcel.readParcelable(null);
            int m = paramAnonymousParcel.readInt();
            byte[] arrayOfByte = null;
            if (m > 0)
            {
                arrayOfByte = new byte[m];
                paramAnonymousParcel.readByteArray(arrayOfByte);
            }
            Object localObject;
            if (i == 1)
                localObject = WifiP2pDnsSdServiceResponse.newInstance(j, k, localWifiP2pDevice, arrayOfByte);
            while (true)
            {
                return localObject;
                if (i == 2)
                    localObject = WifiP2pUpnpServiceResponse.newInstance(j, k, localWifiP2pDevice, arrayOfByte);
                else
                    localObject = new WifiP2pServiceResponse(i, j, k, localWifiP2pDevice, arrayOfByte);
            }
        }

        public WifiP2pServiceResponse[] newArray(int paramAnonymousInt)
        {
            return new WifiP2pServiceResponse[paramAnonymousInt];
        }
    };
    private static int MAX_BUF_SIZE = 1024;
    protected byte[] mData;
    protected WifiP2pDevice mDevice;
    protected int mServiceType;
    protected int mStatus;
    protected int mTransId;

    protected WifiP2pServiceResponse(int paramInt1, int paramInt2, int paramInt3, WifiP2pDevice paramWifiP2pDevice, byte[] paramArrayOfByte)
    {
        this.mServiceType = paramInt1;
        this.mStatus = paramInt2;
        this.mTransId = paramInt3;
        this.mDevice = paramWifiP2pDevice;
        this.mData = paramArrayOfByte;
    }

    private boolean equals(Object paramObject1, Object paramObject2)
    {
        boolean bool;
        if ((paramObject1 == null) && (paramObject2 == null))
            bool = true;
        while (true)
        {
            return bool;
            if (paramObject1 != null)
                bool = paramObject1.equals(paramObject2);
            else
                bool = false;
        }
    }

    private static byte[] hexStr2Bin(String paramString)
    {
        int i = paramString.length() / 2;
        byte[] arrayOfByte = new byte[paramString.length() / 2];
        int j = 0;
        while (j < i)
        {
            int k = j * 2;
            int m = 2 + j * 2;
            try
            {
                arrayOfByte[j] = ((byte)Integer.parseInt(paramString.substring(k, m), 16));
                j++;
            }
            catch (Exception localException)
            {
                localException.printStackTrace();
                arrayOfByte = null;
            }
        }
        return arrayOfByte;
    }

    public static List<WifiP2pServiceResponse> newInstance(String paramString)
    {
        ArrayList localArrayList = new ArrayList();
        String[] arrayOfString = paramString.split(" ");
        if (arrayOfString.length != 4);
        WifiP2pDevice localWifiP2pDevice;
        byte[] arrayOfByte1;
        for (localArrayList = null; ; localArrayList = null)
        {
            return localArrayList;
            localWifiP2pDevice = new WifiP2pDevice();
            localWifiP2pDevice.deviceAddress = arrayOfString[1];
            arrayOfByte1 = hexStr2Bin(arrayOfString[3]);
            if (arrayOfByte1 != null)
                break;
        }
        DataInputStream localDataInputStream = new DataInputStream(new ByteArrayInputStream(arrayOfByte1));
        label314: 
        while (true)
        {
            int i;
            int j;
            int k;
            int m;
            try
            {
                if (localDataInputStream.available() <= 0)
                    break;
                i = -3 + ((0xFF & localDataInputStream.readByte()) + ((0xFF & localDataInputStream.readByte()) << 8));
                j = localDataInputStream.readUnsignedByte();
                k = localDataInputStream.readByte();
                m = localDataInputStream.readUnsignedByte();
                if (i < 0)
                {
                    localArrayList = null;
                    break;
                }
                if (i != 0)
                    break label198;
                if (m != 0)
                    continue;
                localArrayList.add(new WifiP2pServiceResponse(j, m, k, localWifiP2pDevice, null));
                continue;
            }
            catch (IOException localIOException)
            {
                localIOException.printStackTrace();
            }
            if (localArrayList.size() > 0)
                break;
            localArrayList = null;
            break;
            label198: if (i > MAX_BUF_SIZE)
            {
                localDataInputStream.skip(i);
            }
            else
            {
                byte[] arrayOfByte2 = new byte[i];
                localDataInputStream.readFully(arrayOfByte2);
                Object localObject;
                if (j == 1)
                    localObject = WifiP2pDnsSdServiceResponse.newInstance(m, k, localWifiP2pDevice, arrayOfByte2);
                while (true)
                {
                    if ((localObject == null) || (((WifiP2pServiceResponse)localObject).getStatus() != 0))
                        break label314;
                    localArrayList.add(localObject);
                    break;
                    if (j == 2)
                        localObject = WifiP2pUpnpServiceResponse.newInstance(m, k, localWifiP2pDevice, arrayOfByte2);
                    else
                        localObject = new WifiP2pServiceResponse(j, m, k, localWifiP2pDevice, arrayOfByte2);
                }
            }
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = true;
        if (paramObject == this);
        while (true)
        {
            return bool;
            if (!(paramObject instanceof WifiP2pServiceResponse))
            {
                bool = false;
            }
            else
            {
                WifiP2pServiceResponse localWifiP2pServiceResponse = (WifiP2pServiceResponse)paramObject;
                if ((localWifiP2pServiceResponse.mServiceType != this.mServiceType) || (localWifiP2pServiceResponse.mStatus != this.mStatus) || (!equals(localWifiP2pServiceResponse.mDevice.deviceAddress, this.mDevice.deviceAddress)) || (!Arrays.equals(localWifiP2pServiceResponse.mData, this.mData)))
                    bool = false;
            }
        }
    }

    public byte[] getRawData()
    {
        return this.mData;
    }

    public int getServiceType()
    {
        return this.mServiceType;
    }

    public WifiP2pDevice getSrcDevice()
    {
        return this.mDevice;
    }

    public int getStatus()
    {
        return this.mStatus;
    }

    public int getTransactionId()
    {
        return this.mTransId;
    }

    public int hashCode()
    {
        int i = 0;
        int j = 31 * (31 * (31 * (527 + this.mServiceType) + this.mStatus) + this.mTransId);
        int k;
        int m;
        if (this.mDevice.deviceAddress == null)
        {
            k = 0;
            m = 31 * (j + k);
            if (this.mData != null)
                break label76;
        }
        while (true)
        {
            return m + i;
            k = this.mDevice.deviceAddress.hashCode();
            break;
            label76: i = this.mData.hashCode();
        }
    }

    public void setSrcDevice(WifiP2pDevice paramWifiP2pDevice)
    {
        if (paramWifiP2pDevice == null);
        while (true)
        {
            return;
            this.mDevice = paramWifiP2pDevice;
        }
    }

    public String toString()
    {
        StringBuffer localStringBuffer = new StringBuffer();
        localStringBuffer.append("serviceType:").append(this.mServiceType);
        localStringBuffer.append(" status:").append(Status.toString(this.mStatus));
        localStringBuffer.append(" srcAddr:").append(this.mDevice.deviceAddress);
        localStringBuffer.append(" data:").append(this.mData);
        return localStringBuffer.toString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mServiceType);
        paramParcel.writeInt(this.mStatus);
        paramParcel.writeInt(this.mTransId);
        paramParcel.writeParcelable(this.mDevice, paramInt);
        if ((this.mData == null) || (this.mData.length == 0))
            paramParcel.writeInt(0);
        while (true)
        {
            return;
            paramParcel.writeInt(this.mData.length);
            paramParcel.writeByteArray(this.mData);
        }
    }

    public static class Status
    {
        public static final int BAD_REQUEST = 3;
        public static final int REQUESTED_INFORMATION_NOT_AVAILABLE = 2;
        public static final int SERVICE_PROTOCOL_NOT_AVAILABLE = 1;
        public static final int SUCCESS;

        public static String toString(int paramInt)
        {
            String str;
            switch (paramInt)
            {
            default:
                str = "UNKNOWN";
            case 0:
            case 1:
            case 2:
            case 3:
            }
            while (true)
            {
                return str;
                str = "SUCCESS";
                continue;
                str = "SERVICE_PROTOCOL_NOT_AVAILABLE";
                continue;
                str = "REQUESTED_INFORMATION_NOT_AVAILABLE";
                continue;
                str = "BAD_REQUEST";
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.p2p.nsd.WifiP2pServiceResponse
 * JD-Core Version:        0.6.2
 */