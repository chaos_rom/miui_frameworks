package android.net.wifi.p2p.nsd;

import android.net.wifi.p2p.WifiP2pDevice;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class WifiP2pDnsSdServiceResponse extends WifiP2pServiceResponse
{
    private static final Map<Integer, String> sVmpack = new HashMap();
    private String mDnsQueryName;
    private int mDnsType;
    private String mInstanceName;
    private final HashMap<String, String> mTxtRecord = new HashMap();
    private int mVersion;

    static
    {
        sVmpack.put(Integer.valueOf(12), "_tcp.local.");
        sVmpack.put(Integer.valueOf(17), "local.");
        sVmpack.put(Integer.valueOf(28), "_udp.local.");
    }

    protected WifiP2pDnsSdServiceResponse(int paramInt1, int paramInt2, WifiP2pDevice paramWifiP2pDevice, byte[] paramArrayOfByte)
    {
        super(1, paramInt1, paramInt2, paramWifiP2pDevice, paramArrayOfByte);
        if (!parse())
            throw new IllegalArgumentException("Malformed bonjour service response");
    }

    static WifiP2pDnsSdServiceResponse newInstance(int paramInt1, int paramInt2, WifiP2pDevice paramWifiP2pDevice, byte[] paramArrayOfByte)
    {
        WifiP2pDnsSdServiceResponse localWifiP2pDnsSdServiceResponse;
        if (paramInt1 != 0)
            localWifiP2pDnsSdServiceResponse = new WifiP2pDnsSdServiceResponse(paramInt1, paramInt2, paramWifiP2pDevice, null);
        while (true)
        {
            return localWifiP2pDnsSdServiceResponse;
            try
            {
                localWifiP2pDnsSdServiceResponse = new WifiP2pDnsSdServiceResponse(paramInt1, paramInt2, paramWifiP2pDevice, paramArrayOfByte);
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
                localIllegalArgumentException.printStackTrace();
                localWifiP2pDnsSdServiceResponse = null;
            }
        }
    }

    private boolean parse()
    {
        boolean bool = true;
        if (this.mData == null);
        while (true)
        {
            return bool;
            DataInputStream localDataInputStream = new DataInputStream(new ByteArrayInputStream(this.mData));
            this.mDnsQueryName = readDnsName(localDataInputStream);
            label152: if (this.mDnsQueryName == null)
            {
                bool = false;
            }
            else
            {
                String str;
                try
                {
                    this.mDnsType = localDataInputStream.readUnsignedShort();
                    this.mVersion = localDataInputStream.readUnsignedByte();
                    if (this.mDnsType != 12)
                        break label152;
                    str = readDnsName(localDataInputStream);
                    if (str != null)
                        break label103;
                    bool = false;
                }
                catch (IOException localIOException)
                {
                    localIOException.printStackTrace();
                    bool = false;
                }
                continue;
                label103: if (str.length() <= this.mDnsQueryName.length())
                {
                    bool = false;
                }
                else
                {
                    this.mInstanceName = str.substring(0, -1 + (str.length() - this.mDnsQueryName.length()));
                    continue;
                    if (this.mDnsType == 16)
                        bool = readTxtData(localDataInputStream);
                    else
                        bool = false;
                }
            }
        }
    }

    private String readDnsName(DataInputStream paramDataInputStream)
    {
        String str1 = null;
        StringBuffer localStringBuffer = new StringBuffer();
        HashMap localHashMap = new HashMap(sVmpack);
        if (this.mDnsQueryName != null)
            localHashMap.put(Integer.valueOf(39), this.mDnsQueryName);
        try
        {
            while (true)
            {
                int i = paramDataInputStream.readUnsignedByte();
                if (i == 0)
                {
                    str1 = localStringBuffer.toString();
                    break;
                }
                if (i == 192)
                {
                    String str2 = (String)localHashMap.get(Integer.valueOf(paramDataInputStream.readUnsignedByte()));
                    if (str2 == null)
                        break;
                    localStringBuffer.append(str2);
                    str1 = localStringBuffer.toString();
                    break;
                }
                byte[] arrayOfByte = new byte[i];
                paramDataInputStream.readFully(arrayOfByte);
                localStringBuffer.append(new String(arrayOfByte));
                localStringBuffer.append(".");
            }
        }
        catch (IOException localIOException)
        {
            localIOException.printStackTrace();
        }
        return str1;
    }

    private boolean readTxtData(DataInputStream paramDataInputStream)
    {
        boolean bool = false;
        try
        {
            while (paramDataInputStream.available() > 0)
            {
                int i = paramDataInputStream.readUnsignedByte();
                if (i == 0)
                    break;
                byte[] arrayOfByte = new byte[i];
                paramDataInputStream.readFully(arrayOfByte);
                String[] arrayOfString = new String(arrayOfByte).split("=");
                if (arrayOfString.length != 2)
                    break label87;
                this.mTxtRecord.put(arrayOfString[0], arrayOfString[1]);
            }
        }
        catch (IOException localIOException)
        {
            localIOException.printStackTrace();
            break label87;
            bool = true;
        }
        label87: return bool;
    }

    public String getDnsQueryName()
    {
        return this.mDnsQueryName;
    }

    public int getDnsType()
    {
        return this.mDnsType;
    }

    public String getInstanceName()
    {
        return this.mInstanceName;
    }

    public Map<String, String> getTxtRecord()
    {
        return this.mTxtRecord;
    }

    public int getVersion()
    {
        return this.mVersion;
    }

    public String toString()
    {
        StringBuffer localStringBuffer1 = new StringBuffer();
        localStringBuffer1.append("serviceType:DnsSd(").append(this.mServiceType).append(")");
        localStringBuffer1.append(" status:").append(WifiP2pServiceResponse.Status.toString(this.mStatus));
        localStringBuffer1.append(" srcAddr:").append(this.mDevice.deviceAddress);
        StringBuffer localStringBuffer2 = localStringBuffer1.append(" version:");
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = Integer.valueOf(this.mVersion);
        localStringBuffer2.append(String.format("%02x", arrayOfObject));
        localStringBuffer1.append(" dnsName:").append(this.mDnsQueryName);
        localStringBuffer1.append(" TxtRecord:");
        Iterator localIterator = this.mTxtRecord.keySet().iterator();
        while (localIterator.hasNext())
        {
            String str = (String)localIterator.next();
            localStringBuffer1.append(" key:").append(str).append(" value:").append((String)this.mTxtRecord.get(str));
        }
        if (this.mInstanceName != null)
            localStringBuffer1.append(" InsName:").append(this.mInstanceName);
        return localStringBuffer1.toString();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceResponse
 * JD-Core Version:        0.6.2
 */