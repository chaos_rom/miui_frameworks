package android.net.wifi.p2p.nsd;

public class WifiP2pUpnpServiceRequest extends WifiP2pServiceRequest
{
    protected WifiP2pUpnpServiceRequest()
    {
        super(2, null);
    }

    protected WifiP2pUpnpServiceRequest(String paramString)
    {
        super(2, paramString);
    }

    public static WifiP2pUpnpServiceRequest newInstance()
    {
        return new WifiP2pUpnpServiceRequest();
    }

    public static WifiP2pUpnpServiceRequest newInstance(String paramString)
    {
        if (paramString == null)
            throw new IllegalArgumentException("search target cannot be null");
        StringBuffer localStringBuffer = new StringBuffer();
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = Integer.valueOf(16);
        localStringBuffer.append(String.format("%02x", arrayOfObject));
        localStringBuffer.append(WifiP2pServiceInfo.bin2HexStr(paramString.getBytes()));
        return new WifiP2pUpnpServiceRequest(localStringBuffer.toString());
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.p2p.nsd.WifiP2pUpnpServiceRequest
 * JD-Core Version:        0.6.2
 */