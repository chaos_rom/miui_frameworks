package android.net.wifi.p2p.nsd;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class WifiP2pServiceRequest
    implements Parcelable
{
    public static final Parcelable.Creator<WifiP2pServiceRequest> CREATOR = new Parcelable.Creator()
    {
        public WifiP2pServiceRequest createFromParcel(Parcel paramAnonymousParcel)
        {
            return new WifiP2pServiceRequest(paramAnonymousParcel.readInt(), paramAnonymousParcel.readInt(), paramAnonymousParcel.readInt(), paramAnonymousParcel.readString(), null);
        }

        public WifiP2pServiceRequest[] newArray(int paramAnonymousInt)
        {
            return new WifiP2pServiceRequest[paramAnonymousInt];
        }
    };
    private int mLength;
    private int mProtocolType;
    private String mQuery;
    private int mTransId;

    private WifiP2pServiceRequest(int paramInt1, int paramInt2, int paramInt3, String paramString)
    {
        this.mProtocolType = paramInt1;
        this.mLength = paramInt2;
        this.mTransId = paramInt3;
        this.mQuery = paramString;
    }

    protected WifiP2pServiceRequest(int paramInt, String paramString)
    {
        validateQuery(paramString);
        this.mProtocolType = paramInt;
        this.mQuery = paramString;
        if (paramString != null);
        for (this.mLength = (2 + paramString.length() / 2); ; this.mLength = 2)
            return;
    }

    public static WifiP2pServiceRequest newInstance(int paramInt)
    {
        return new WifiP2pServiceRequest(paramInt, null);
    }

    public static WifiP2pServiceRequest newInstance(int paramInt, String paramString)
    {
        return new WifiP2pServiceRequest(paramInt, paramString);
    }

    private void validateQuery(String paramString)
    {
        if (paramString == null);
        while (true)
        {
            return;
            if (paramString.length() % 2 == 1)
                throw new IllegalArgumentException("query size is invalid. query=" + paramString);
            if (paramString.length() / 2 > 65535)
                throw new IllegalArgumentException("query size is too large. len=" + paramString.length());
            String str = paramString.toLowerCase();
            for (int k : str.toCharArray())
                if (((k < 48) || (k > 57)) && ((k < 97) || (k > 102)))
                    throw new IllegalArgumentException("query should be hex string. query=" + str);
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = true;
        if (paramObject == this);
        while (true)
        {
            return bool;
            if (!(paramObject instanceof WifiP2pServiceRequest))
            {
                bool = false;
            }
            else
            {
                WifiP2pServiceRequest localWifiP2pServiceRequest = (WifiP2pServiceRequest)paramObject;
                if ((localWifiP2pServiceRequest.mProtocolType != this.mProtocolType) || (localWifiP2pServiceRequest.mLength != this.mLength))
                    bool = false;
                else if ((localWifiP2pServiceRequest.mQuery != null) || (this.mQuery != null))
                    if (localWifiP2pServiceRequest.mQuery != null)
                        bool = localWifiP2pServiceRequest.mQuery.equals(this.mQuery);
                    else
                        bool = false;
            }
        }
    }

    public String getSupplicantQuery()
    {
        StringBuffer localStringBuffer = new StringBuffer();
        Object[] arrayOfObject1 = new Object[1];
        arrayOfObject1[0] = Integer.valueOf(0xFF & this.mLength);
        localStringBuffer.append(String.format("%02x", arrayOfObject1));
        Object[] arrayOfObject2 = new Object[1];
        arrayOfObject2[0] = Integer.valueOf(0xFF & this.mLength >> 8);
        localStringBuffer.append(String.format("%02x", arrayOfObject2));
        Object[] arrayOfObject3 = new Object[1];
        arrayOfObject3[0] = Integer.valueOf(this.mProtocolType);
        localStringBuffer.append(String.format("%02x", arrayOfObject3));
        Object[] arrayOfObject4 = new Object[1];
        arrayOfObject4[0] = Integer.valueOf(this.mTransId);
        localStringBuffer.append(String.format("%02x", arrayOfObject4));
        if (this.mQuery != null)
            localStringBuffer.append(this.mQuery);
        return localStringBuffer.toString();
    }

    public int getTransactionId()
    {
        return this.mTransId;
    }

    public int hashCode()
    {
        int i = 31 * (31 * (527 + this.mProtocolType) + this.mLength);
        if (this.mQuery == null);
        for (int j = 0; ; j = this.mQuery.hashCode())
            return i + j;
    }

    public void setTransactionId(int paramInt)
    {
        this.mTransId = paramInt;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mProtocolType);
        paramParcel.writeInt(this.mLength);
        paramParcel.writeInt(this.mTransId);
        paramParcel.writeString(this.mQuery);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.p2p.nsd.WifiP2pServiceRequest
 * JD-Core Version:        0.6.2
 */