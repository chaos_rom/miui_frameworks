package android.net.wifi.p2p;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IWifiP2pManager extends IInterface
{
    public abstract Messenger getMessenger()
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IWifiP2pManager
    {
        private static final String DESCRIPTOR = "android.net.wifi.p2p.IWifiP2pManager";
        static final int TRANSACTION_getMessenger = 1;

        public Stub()
        {
            attachInterface(this, "android.net.wifi.p2p.IWifiP2pManager");
        }

        public static IWifiP2pManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.net.wifi.p2p.IWifiP2pManager");
                if ((localIInterface != null) && ((localIInterface instanceof IWifiP2pManager)))
                    localObject = (IWifiP2pManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 1;
            switch (paramInt1)
            {
            default:
                i = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            }
            while (true)
            {
                return i;
                paramParcel2.writeString("android.net.wifi.p2p.IWifiP2pManager");
                continue;
                paramParcel1.enforceInterface("android.net.wifi.p2p.IWifiP2pManager");
                Messenger localMessenger = getMessenger();
                paramParcel2.writeNoException();
                if (localMessenger != null)
                {
                    paramParcel2.writeInt(i);
                    localMessenger.writeToParcel(paramParcel2, i);
                }
                else
                {
                    paramParcel2.writeInt(0);
                }
            }
        }

        private static class Proxy
            implements IWifiP2pManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.net.wifi.p2p.IWifiP2pManager";
            }

            public Messenger getMessenger()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.wifi.p2p.IWifiP2pManager");
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localMessenger = (Messenger)Messenger.CREATOR.createFromParcel(localParcel2);
                        return localMessenger;
                    }
                    Messenger localMessenger = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.p2p.IWifiP2pManager
 * JD-Core Version:        0.6.2
 */