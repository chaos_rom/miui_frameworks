package android.net.wifi.p2p;

import android.content.Context;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceResponse;
import android.net.wifi.p2p.nsd.WifiP2pServiceInfo;
import android.net.wifi.p2p.nsd.WifiP2pServiceRequest;
import android.net.wifi.p2p.nsd.WifiP2pServiceResponse;
import android.net.wifi.p2p.nsd.WifiP2pUpnpServiceResponse;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import com.android.internal.util.AsyncChannel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WifiP2pManager
{
    public static final int ADD_LOCAL_SERVICE = 139292;
    public static final int ADD_LOCAL_SERVICE_FAILED = 139293;
    public static final int ADD_LOCAL_SERVICE_SUCCEEDED = 139294;
    public static final int ADD_SERVICE_REQUEST = 139301;
    public static final int ADD_SERVICE_REQUEST_FAILED = 139302;
    public static final int ADD_SERVICE_REQUEST_SUCCEEDED = 139303;
    public static final String APP_PKG_BUNDLE_KEY = "appPkgName";
    private static final int BASE = 139264;
    public static final int BUSY = 2;
    public static final int CANCEL_CONNECT = 139274;
    public static final int CANCEL_CONNECT_FAILED = 139275;
    public static final int CANCEL_CONNECT_SUCCEEDED = 139276;
    public static final int CLEAR_LOCAL_SERVICES = 139298;
    public static final int CLEAR_LOCAL_SERVICES_FAILED = 139299;
    public static final int CLEAR_LOCAL_SERVICES_SUCCEEDED = 139300;
    public static final int CLEAR_SERVICE_REQUESTS = 139307;
    public static final int CLEAR_SERVICE_REQUESTS_FAILED = 139308;
    public static final int CLEAR_SERVICE_REQUESTS_SUCCEEDED = 139309;
    public static final int CONNECT = 139271;
    public static final int CONNECTION_REQUESTED = 139321;
    public static final int CONNECT_FAILED = 139272;
    public static final int CONNECT_SUCCEEDED = 139273;
    public static final int CREATE_GROUP = 139277;
    public static final int CREATE_GROUP_FAILED = 139278;
    public static final int CREATE_GROUP_SUCCEEDED = 139279;
    public static final int DIALOG_LISTENER_ATTACHED = 139320;
    public static final int DIALOG_LISTENER_DETACHED = 139319;
    public static final int DISCOVER_PEERS = 139265;
    public static final int DISCOVER_PEERS_FAILED = 139266;
    public static final int DISCOVER_PEERS_SUCCEEDED = 139267;
    public static final int DISCOVER_SERVICES = 139310;
    public static final int DISCOVER_SERVICES_FAILED = 139311;
    public static final int DISCOVER_SERVICES_SUCCEEDED = 139312;
    public static final int ERROR = 0;
    public static final String EXTRA_DISCOVERY_STATE = "discoveryState";
    public static final String EXTRA_LINK_CAPABILITIES = "linkCapabilities";
    public static final String EXTRA_LINK_PROPERTIES = "linkProperties";
    public static final String EXTRA_NETWORK_INFO = "networkInfo";
    public static final String EXTRA_WIFI_P2P_DEVICE = "wifiP2pDevice";
    public static final String EXTRA_WIFI_P2P_INFO = "wifiP2pInfo";
    public static final String EXTRA_WIFI_STATE = "wifi_p2p_state";
    public static final int NOT_IN_FOREGROUND = 4;
    public static final int NO_SERVICE_REQUESTS = 3;
    public static final String P2P_CONFIG_BUNDLE_KEY = "wifiP2pConfig";
    public static final String P2P_DEV_BUNDLE_KEY = "wifiP2pDevice";
    public static final int P2P_UNSUPPORTED = 1;
    public static final int PING = 139313;
    public static final int REMOVE_GROUP = 139280;
    public static final int REMOVE_GROUP_FAILED = 139281;
    public static final int REMOVE_GROUP_SUCCEEDED = 139282;
    public static final int REMOVE_LOCAL_SERVICE = 139295;
    public static final int REMOVE_LOCAL_SERVICE_FAILED = 139296;
    public static final int REMOVE_LOCAL_SERVICE_SUCCEEDED = 139297;
    public static final int REMOVE_SERVICE_REQUEST = 139304;
    public static final int REMOVE_SERVICE_REQUEST_FAILED = 139305;
    public static final int REMOVE_SERVICE_REQUEST_SUCCEEDED = 139306;
    public static final int REQUEST_CONNECTION_INFO = 139285;
    public static final int REQUEST_GROUP_INFO = 139287;
    public static final int REQUEST_PEERS = 139283;
    public static final String RESET_DIALOG_LISTENER_BUNDLE_KEY = "dialogResetFlag";
    public static final int RESPONSE_CONNECTION_INFO = 139286;
    public static final int RESPONSE_GROUP_INFO = 139288;
    public static final int RESPONSE_PEERS = 139284;
    public static final int RESPONSE_SERVICE = 139314;
    public static final int SET_DEVICE_NAME = 139315;
    public static final int SET_DEVICE_NAME_FAILED = 139316;
    public static final int SET_DEVICE_NAME_SUCCEEDED = 139317;
    public static final int SET_DIALOG_LISTENER = 139318;
    public static final int SHOW_PIN_REQUESTED = 139322;
    public static final int STOP_DISCOVERY = 139268;
    public static final int STOP_DISCOVERY_FAILED = 139269;
    public static final int STOP_DISCOVERY_SUCCEEDED = 139270;
    private static final String TAG = "WifiP2pManager";
    public static final String WIFI_P2P_CONNECTION_CHANGED_ACTION = "android.net.wifi.p2p.CONNECTION_STATE_CHANGE";
    public static final String WIFI_P2P_DISCOVERY_CHANGED_ACTION = "android.net.wifi.p2p.DISCOVERY_STATE_CHANGE";
    public static final int WIFI_P2P_DISCOVERY_STARTED = 2;
    public static final int WIFI_P2P_DISCOVERY_STOPPED = 1;
    public static final String WIFI_P2P_PEERS_CHANGED_ACTION = "android.net.wifi.p2p.PEERS_CHANGED";
    public static final String WIFI_P2P_STATE_CHANGED_ACTION = "android.net.wifi.p2p.STATE_CHANGED";
    public static final int WIFI_P2P_STATE_DISABLED = 1;
    public static final int WIFI_P2P_STATE_ENABLED = 2;
    public static final String WIFI_P2P_THIS_DEVICE_CHANGED_ACTION = "android.net.wifi.p2p.THIS_DEVICE_CHANGED";
    public static final String WPS_PIN_BUNDLE_KEY = "wpsPin";
    IWifiP2pManager mService;

    public WifiP2pManager(IWifiP2pManager paramIWifiP2pManager)
    {
        this.mService = paramIWifiP2pManager;
    }

    private static void checkChannel(Channel paramChannel)
    {
        if (paramChannel == null)
            throw new IllegalArgumentException("Channel needs to be initialized");
    }

    private static void checkServiceInfo(WifiP2pServiceInfo paramWifiP2pServiceInfo)
    {
        if (paramWifiP2pServiceInfo == null)
            throw new IllegalArgumentException("service info is null");
    }

    private static void checkServiceRequest(WifiP2pServiceRequest paramWifiP2pServiceRequest)
    {
        if (paramWifiP2pServiceRequest == null)
            throw new IllegalArgumentException("service request is null");
    }

    public void addLocalService(Channel paramChannel, WifiP2pServiceInfo paramWifiP2pServiceInfo, ActionListener paramActionListener)
    {
        checkChannel(paramChannel);
        checkServiceInfo(paramWifiP2pServiceInfo);
        paramChannel.mAsyncChannel.sendMessage(139292, 0, paramChannel.putListener(paramActionListener), paramWifiP2pServiceInfo);
    }

    public void addServiceRequest(Channel paramChannel, WifiP2pServiceRequest paramWifiP2pServiceRequest, ActionListener paramActionListener)
    {
        checkChannel(paramChannel);
        checkServiceRequest(paramWifiP2pServiceRequest);
        paramChannel.mAsyncChannel.sendMessage(139301, 0, paramChannel.putListener(paramActionListener), paramWifiP2pServiceRequest);
    }

    public void cancelConnect(Channel paramChannel, ActionListener paramActionListener)
    {
        checkChannel(paramChannel);
        paramChannel.mAsyncChannel.sendMessage(139274, 0, paramChannel.putListener(paramActionListener));
    }

    public void clearLocalServices(Channel paramChannel, ActionListener paramActionListener)
    {
        checkChannel(paramChannel);
        paramChannel.mAsyncChannel.sendMessage(139298, 0, paramChannel.putListener(paramActionListener));
    }

    public void clearServiceRequests(Channel paramChannel, ActionListener paramActionListener)
    {
        checkChannel(paramChannel);
        paramChannel.mAsyncChannel.sendMessage(139307, 0, paramChannel.putListener(paramActionListener));
    }

    public void connect(Channel paramChannel, WifiP2pConfig paramWifiP2pConfig, ActionListener paramActionListener)
    {
        checkChannel(paramChannel);
        paramChannel.mAsyncChannel.sendMessage(139271, 0, paramChannel.putListener(paramActionListener), paramWifiP2pConfig);
    }

    public void createGroup(Channel paramChannel, ActionListener paramActionListener)
    {
        checkChannel(paramChannel);
        paramChannel.mAsyncChannel.sendMessage(139277, 0, paramChannel.putListener(paramActionListener));
    }

    public void discoverPeers(Channel paramChannel, ActionListener paramActionListener)
    {
        checkChannel(paramChannel);
        paramChannel.mAsyncChannel.sendMessage(139265, 0, paramChannel.putListener(paramActionListener));
    }

    public void discoverServices(Channel paramChannel, ActionListener paramActionListener)
    {
        checkChannel(paramChannel);
        paramChannel.mAsyncChannel.sendMessage(139310, 0, paramChannel.putListener(paramActionListener));
    }

    public Messenger getMessenger()
    {
        try
        {
            Messenger localMessenger2 = this.mService.getMessenger();
            localMessenger1 = localMessenger2;
            return localMessenger1;
        }
        catch (RemoteException localRemoteException)
        {
            while (true)
                Messenger localMessenger1 = null;
        }
    }

    public Channel initialize(Context paramContext, Looper paramLooper, ChannelListener paramChannelListener)
    {
        Messenger localMessenger = getMessenger();
        Channel localChannel;
        if (localMessenger == null)
            localChannel = null;
        while (true)
        {
            return localChannel;
            localChannel = new Channel(paramContext, paramLooper, paramChannelListener);
            if (localChannel.mAsyncChannel.connectSync(paramContext, localChannel.mHandler, localMessenger) != 0)
                localChannel = null;
        }
    }

    public void removeGroup(Channel paramChannel, ActionListener paramActionListener)
    {
        checkChannel(paramChannel);
        paramChannel.mAsyncChannel.sendMessage(139280, 0, paramChannel.putListener(paramActionListener));
    }

    public void removeLocalService(Channel paramChannel, WifiP2pServiceInfo paramWifiP2pServiceInfo, ActionListener paramActionListener)
    {
        checkChannel(paramChannel);
        checkServiceInfo(paramWifiP2pServiceInfo);
        paramChannel.mAsyncChannel.sendMessage(139295, 0, paramChannel.putListener(paramActionListener), paramWifiP2pServiceInfo);
    }

    public void removeServiceRequest(Channel paramChannel, WifiP2pServiceRequest paramWifiP2pServiceRequest, ActionListener paramActionListener)
    {
        checkChannel(paramChannel);
        checkServiceRequest(paramWifiP2pServiceRequest);
        paramChannel.mAsyncChannel.sendMessage(139304, 0, paramChannel.putListener(paramActionListener), paramWifiP2pServiceRequest);
    }

    public void requestConnectionInfo(Channel paramChannel, ConnectionInfoListener paramConnectionInfoListener)
    {
        checkChannel(paramChannel);
        paramChannel.mAsyncChannel.sendMessage(139285, 0, paramChannel.putListener(paramConnectionInfoListener));
    }

    public void requestGroupInfo(Channel paramChannel, GroupInfoListener paramGroupInfoListener)
    {
        checkChannel(paramChannel);
        paramChannel.mAsyncChannel.sendMessage(139287, 0, paramChannel.putListener(paramGroupInfoListener));
    }

    public void requestPeers(Channel paramChannel, PeerListListener paramPeerListListener)
    {
        checkChannel(paramChannel);
        paramChannel.mAsyncChannel.sendMessage(139283, 0, paramChannel.putListener(paramPeerListListener));
    }

    public void setDeviceName(Channel paramChannel, String paramString, ActionListener paramActionListener)
    {
        checkChannel(paramChannel);
        WifiP2pDevice localWifiP2pDevice = new WifiP2pDevice();
        localWifiP2pDevice.deviceName = paramString;
        paramChannel.mAsyncChannel.sendMessage(139315, 0, paramChannel.putListener(paramActionListener), localWifiP2pDevice);
    }

    public void setDialogListener(Channel paramChannel, DialogListener paramDialogListener)
    {
        checkChannel(paramChannel);
        paramChannel.setDialogListener(paramDialogListener);
        Message localMessage = Message.obtain();
        Bundle localBundle = new Bundle();
        localBundle.putString("appPkgName", paramChannel.mContext.getPackageName());
        if (paramDialogListener == null);
        for (boolean bool = true; ; bool = false)
        {
            localBundle.putBoolean("dialogResetFlag", bool);
            localMessage.what = 139318;
            localMessage.setData(localBundle);
            paramChannel.mAsyncChannel.sendMessage(localMessage);
            return;
        }
    }

    public void setDnsSdResponseListeners(Channel paramChannel, DnsSdServiceResponseListener paramDnsSdServiceResponseListener, DnsSdTxtRecordListener paramDnsSdTxtRecordListener)
    {
        checkChannel(paramChannel);
        Channel.access$802(paramChannel, paramDnsSdServiceResponseListener);
        Channel.access$902(paramChannel, paramDnsSdTxtRecordListener);
    }

    public void setServiceResponseListener(Channel paramChannel, ServiceResponseListener paramServiceResponseListener)
    {
        checkChannel(paramChannel);
        Channel.access$702(paramChannel, paramServiceResponseListener);
    }

    public void setUpnpServiceResponseListener(Channel paramChannel, UpnpServiceResponseListener paramUpnpServiceResponseListener)
    {
        checkChannel(paramChannel);
        Channel.access$1002(paramChannel, paramUpnpServiceResponseListener);
    }

    public void stopPeerDiscovery(Channel paramChannel, ActionListener paramActionListener)
    {
        checkChannel(paramChannel);
        paramChannel.mAsyncChannel.sendMessage(139268, 0, paramChannel.putListener(paramActionListener));
    }

    public static class Channel
    {
        private static final int INVALID_LISTENER_KEY;
        private AsyncChannel mAsyncChannel = new AsyncChannel();
        private WifiP2pManager.ChannelListener mChannelListener;
        Context mContext;
        private WifiP2pManager.DialogListener mDialogListener;
        private WifiP2pManager.DnsSdServiceResponseListener mDnsSdServRspListener;
        private WifiP2pManager.DnsSdTxtRecordListener mDnsSdTxtListener;
        private P2pHandler mHandler;
        private int mListenerKey = 0;
        private HashMap<Integer, Object> mListenerMap = new HashMap();
        private Object mListenerMapLock = new Object();
        private WifiP2pManager.ServiceResponseListener mServRspListener;
        private WifiP2pManager.UpnpServiceResponseListener mUpnpServRspListener;

        Channel(Context paramContext, Looper paramLooper, WifiP2pManager.ChannelListener paramChannelListener)
        {
            this.mHandler = new P2pHandler(paramLooper);
            this.mChannelListener = paramChannelListener;
            this.mContext = paramContext;
        }

        private Object getListener(int paramInt)
        {
            Object localObject3;
            if (paramInt == 0)
                localObject3 = null;
            while (true)
            {
                return localObject3;
                synchronized (this.mListenerMapLock)
                {
                    localObject3 = this.mListenerMap.remove(Integer.valueOf(paramInt));
                }
            }
        }

        private void handleDnsSdServiceResponse(WifiP2pDnsSdServiceResponse paramWifiP2pDnsSdServiceResponse)
        {
            if (paramWifiP2pDnsSdServiceResponse.getDnsType() == 12)
                if (this.mDnsSdServRspListener != null)
                    this.mDnsSdServRspListener.onDnsSdServiceAvailable(paramWifiP2pDnsSdServiceResponse.getInstanceName(), paramWifiP2pDnsSdServiceResponse.getDnsQueryName(), paramWifiP2pDnsSdServiceResponse.getSrcDevice());
            while (true)
            {
                return;
                if (paramWifiP2pDnsSdServiceResponse.getDnsType() == 16)
                {
                    if (this.mDnsSdTxtListener != null)
                        this.mDnsSdTxtListener.onDnsSdTxtRecordAvailable(paramWifiP2pDnsSdServiceResponse.getDnsQueryName(), paramWifiP2pDnsSdServiceResponse.getTxtRecord(), paramWifiP2pDnsSdServiceResponse.getSrcDevice());
                }
                else
                    Log.e("WifiP2pManager", "Unhandled resp " + paramWifiP2pDnsSdServiceResponse);
            }
        }

        private void handleServiceResponse(WifiP2pServiceResponse paramWifiP2pServiceResponse)
        {
            if ((paramWifiP2pServiceResponse instanceof WifiP2pDnsSdServiceResponse))
                handleDnsSdServiceResponse((WifiP2pDnsSdServiceResponse)paramWifiP2pServiceResponse);
            while (true)
            {
                return;
                if ((paramWifiP2pServiceResponse instanceof WifiP2pUpnpServiceResponse))
                {
                    if (this.mUpnpServRspListener != null)
                        handleUpnpServiceResponse((WifiP2pUpnpServiceResponse)paramWifiP2pServiceResponse);
                }
                else if (this.mServRspListener != null)
                    this.mServRspListener.onServiceAvailable(paramWifiP2pServiceResponse.getServiceType(), paramWifiP2pServiceResponse.getRawData(), paramWifiP2pServiceResponse.getSrcDevice());
            }
        }

        private void handleUpnpServiceResponse(WifiP2pUpnpServiceResponse paramWifiP2pUpnpServiceResponse)
        {
            this.mUpnpServRspListener.onUpnpServiceAvailable(paramWifiP2pUpnpServiceResponse.getUniqueServiceNames(), paramWifiP2pUpnpServiceResponse.getSrcDevice());
        }

        private int putListener(Object paramObject)
        {
            int i;
            if (paramObject == null)
                i = 0;
            while (true)
            {
                return i;
                synchronized (this.mListenerMapLock)
                {
                    do
                    {
                        i = this.mListenerKey;
                        this.mListenerKey = (i + 1);
                    }
                    while (i == 0);
                    this.mListenerMap.put(Integer.valueOf(i), paramObject);
                }
            }
        }

        private void setDialogListener(WifiP2pManager.DialogListener paramDialogListener)
        {
            this.mDialogListener = paramDialogListener;
        }

        class P2pHandler extends Handler
        {
            P2pHandler(Looper arg2)
            {
                super();
            }

            public void handleMessage(Message paramMessage)
            {
                Object localObject = WifiP2pManager.Channel.this.getListener(paramMessage.arg2);
                switch (paramMessage.what)
                {
                default:
                    Log.d("WifiP2pManager", "Ignored " + paramMessage);
                case 69636:
                case 139266:
                case 139269:
                case 139272:
                case 139275:
                case 139278:
                case 139281:
                case 139293:
                case 139296:
                case 139299:
                case 139302:
                case 139305:
                case 139308:
                case 139311:
                case 139316:
                case 139267:
                case 139270:
                case 139273:
                case 139276:
                case 139279:
                case 139282:
                case 139294:
                case 139297:
                case 139300:
                case 139303:
                case 139306:
                case 139309:
                case 139312:
                case 139317:
                case 139284:
                case 139286:
                case 139288:
                case 139314:
                case 139321:
                case 139322:
                case 139320:
                case 139319:
                }
                while (true)
                {
                    return;
                    if (WifiP2pManager.Channel.this.mChannelListener != null)
                    {
                        WifiP2pManager.Channel.this.mChannelListener.onChannelDisconnected();
                        WifiP2pManager.Channel.access$102(WifiP2pManager.Channel.this, null);
                        continue;
                        if (localObject != null)
                        {
                            ((WifiP2pManager.ActionListener)localObject).onFailure(paramMessage.arg1);
                            continue;
                            if (localObject != null)
                            {
                                ((WifiP2pManager.ActionListener)localObject).onSuccess();
                                continue;
                                WifiP2pDeviceList localWifiP2pDeviceList = (WifiP2pDeviceList)paramMessage.obj;
                                if (localObject != null)
                                {
                                    ((WifiP2pManager.PeerListListener)localObject).onPeersAvailable(localWifiP2pDeviceList);
                                    continue;
                                    WifiP2pInfo localWifiP2pInfo = (WifiP2pInfo)paramMessage.obj;
                                    if (localObject != null)
                                    {
                                        ((WifiP2pManager.ConnectionInfoListener)localObject).onConnectionInfoAvailable(localWifiP2pInfo);
                                        continue;
                                        WifiP2pGroup localWifiP2pGroup = (WifiP2pGroup)paramMessage.obj;
                                        if (localObject != null)
                                        {
                                            ((WifiP2pManager.GroupInfoListener)localObject).onGroupInfoAvailable(localWifiP2pGroup);
                                            continue;
                                            WifiP2pServiceResponse localWifiP2pServiceResponse = (WifiP2pServiceResponse)paramMessage.obj;
                                            WifiP2pManager.Channel.this.handleServiceResponse(localWifiP2pServiceResponse);
                                            continue;
                                            if (WifiP2pManager.Channel.this.mDialogListener != null)
                                            {
                                                Bundle localBundle2 = paramMessage.getData();
                                                WifiP2pManager.Channel.this.mDialogListener.onConnectionRequested((WifiP2pDevice)localBundle2.getParcelable("wifiP2pDevice"), (WifiP2pConfig)localBundle2.getParcelable("wifiP2pConfig"));
                                                continue;
                                                if (WifiP2pManager.Channel.this.mDialogListener != null)
                                                {
                                                    Bundle localBundle1 = paramMessage.getData();
                                                    WifiP2pManager.Channel.this.mDialogListener.onShowPinRequested(localBundle1.getString("wpsPin"));
                                                    continue;
                                                    if (WifiP2pManager.Channel.this.mDialogListener != null)
                                                    {
                                                        WifiP2pManager.Channel.this.mDialogListener.onAttached();
                                                        continue;
                                                        if (WifiP2pManager.Channel.this.mDialogListener != null)
                                                        {
                                                            WifiP2pManager.Channel.this.mDialogListener.onDetached(paramMessage.arg1);
                                                            WifiP2pManager.Channel.access$302(WifiP2pManager.Channel.this, null);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static abstract interface DialogListener
    {
        public abstract void onAttached();

        public abstract void onConnectionRequested(WifiP2pDevice paramWifiP2pDevice, WifiP2pConfig paramWifiP2pConfig);

        public abstract void onDetached(int paramInt);

        public abstract void onShowPinRequested(String paramString);
    }

    public static abstract interface UpnpServiceResponseListener
    {
        public abstract void onUpnpServiceAvailable(List<String> paramList, WifiP2pDevice paramWifiP2pDevice);
    }

    public static abstract interface DnsSdTxtRecordListener
    {
        public abstract void onDnsSdTxtRecordAvailable(String paramString, Map<String, String> paramMap, WifiP2pDevice paramWifiP2pDevice);
    }

    public static abstract interface DnsSdServiceResponseListener
    {
        public abstract void onDnsSdServiceAvailable(String paramString1, String paramString2, WifiP2pDevice paramWifiP2pDevice);
    }

    public static abstract interface ServiceResponseListener
    {
        public abstract void onServiceAvailable(int paramInt, byte[] paramArrayOfByte, WifiP2pDevice paramWifiP2pDevice);
    }

    public static abstract interface GroupInfoListener
    {
        public abstract void onGroupInfoAvailable(WifiP2pGroup paramWifiP2pGroup);
    }

    public static abstract interface ConnectionInfoListener
    {
        public abstract void onConnectionInfoAvailable(WifiP2pInfo paramWifiP2pInfo);
    }

    public static abstract interface PeerListListener
    {
        public abstract void onPeersAvailable(WifiP2pDeviceList paramWifiP2pDeviceList);
    }

    public static abstract interface ActionListener
    {
        public abstract void onFailure(int paramInt);

        public abstract void onSuccess();
    }

    public static abstract interface ChannelListener
    {
        public abstract void onChannelDisconnected();
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.p2p.WifiP2pManager
 * JD-Core Version:        0.6.2
 */