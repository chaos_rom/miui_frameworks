package android.net.wifi;

import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.nsd.WifiP2pServiceInfo;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class WifiNative
{
    static final int BLUETOOTH_COEXISTENCE_MODE_DISABLED = 1;
    static final int BLUETOOTH_COEXISTENCE_MODE_ENABLED = 0;
    static final int BLUETOOTH_COEXISTENCE_MODE_SENSE = 2;
    private static final boolean DBG = false;
    private static final int DEFAULT_GROUP_OWNER_INTENT = 7;
    String mInterface = "";
    private final String mTAG;

    public WifiNative(String paramString)
    {
        this.mInterface = paramString;
        this.mTAG = ("WifiNative-" + paramString);
    }

    private native void closeSupplicantConnection(String paramString);

    private native boolean connectToSupplicant(String paramString);

    private boolean doBooleanCommand(String paramString)
    {
        return doBooleanCommand(this.mInterface, paramString);
    }

    private native boolean doBooleanCommand(String paramString1, String paramString2);

    private int doIntCommand(String paramString)
    {
        return doIntCommand(this.mInterface, paramString);
    }

    private native int doIntCommand(String paramString1, String paramString2);

    private String doStringCommand(String paramString)
    {
        return doStringCommand(this.mInterface, paramString);
    }

    private native String doStringCommand(String paramString1, String paramString2);

    public static native boolean isDriverLoaded();

    public static native boolean killSupplicant();

    public static native boolean loadDriver();

    public static native boolean startSupplicant(boolean paramBoolean);

    public static native boolean unloadDriver();

    private native String waitForEvent(String paramString);

    public int addNetwork()
    {
        return doIntCommand("ADD_NETWORK");
    }

    public boolean addToBlacklist(String paramString)
    {
        if (TextUtils.isEmpty(paramString));
        for (boolean bool = false; ; bool = doBooleanCommand("BLACKLIST " + paramString))
            return bool;
    }

    public boolean cancelWps()
    {
        return doBooleanCommand("WPS_CANCEL");
    }

    public boolean clearBlacklist()
    {
        return doBooleanCommand("BLACKLIST clear");
    }

    public void closeSupplicantConnection()
    {
        closeSupplicantConnection(this.mInterface);
    }

    public boolean connectToSupplicant()
    {
        return connectToSupplicant(this.mInterface);
    }

    public boolean disableNetwork(int paramInt)
    {
        return doBooleanCommand("DISABLE_NETWORK " + paramInt);
    }

    public boolean disconnect()
    {
        return doBooleanCommand("DISCONNECT");
    }

    public void enableBackgroundScan(boolean paramBoolean)
    {
        if (paramBoolean)
            doBooleanCommand("SET pno 1");
        while (true)
        {
            return;
            doBooleanCommand("SET pno 0");
        }
    }

    public boolean enableNetwork(int paramInt, boolean paramBoolean)
    {
        if (paramBoolean);
        for (boolean bool = doBooleanCommand("SELECT_NETWORK " + paramInt); ; bool = doBooleanCommand("ENABLE_NETWORK " + paramInt))
            return bool;
    }

    public int getBand()
    {
        int i = -1;
        String str = doStringCommand("DRIVER GETBAND");
        String[] arrayOfString;
        if (!TextUtils.isEmpty(str))
            arrayOfString = str.split(" ");
        try
        {
            if (arrayOfString.length == 2)
            {
                int j = Integer.parseInt(arrayOfString[1]);
                i = j;
            }
            label41: return i;
        }
        catch (NumberFormatException localNumberFormatException)
        {
            break label41;
        }
    }

    public int getGroupCapability(String paramString)
    {
        int i = 0;
        if (TextUtils.isEmpty(paramString));
        label103: label107: 
        while (true)
        {
            return i;
            String str1 = p2pPeer(paramString);
            if (!TextUtils.isEmpty(str1))
            {
                String[] arrayOfString1 = str1.split("\n");
                int j = arrayOfString1.length;
                for (int k = 0; ; k++)
                {
                    while (true)
                    {
                        if (k >= j)
                            break label107;
                        String str2 = arrayOfString1[k];
                        if (!str2.startsWith("group_capab="))
                            break label103;
                        String[] arrayOfString2 = str2.split("=");
                        if (arrayOfString2.length != 2)
                            break;
                        try
                        {
                            int m = Integer.decode(arrayOfString2[1]).intValue();
                            i = m;
                        }
                        catch (NumberFormatException localNumberFormatException)
                        {
                        }
                    }
                    break;
                }
            }
        }
    }

    public String getMacAddress()
    {
        String str1 = doStringCommand("DRIVER MACADDR");
        String[] arrayOfString;
        if (!TextUtils.isEmpty(str1))
        {
            arrayOfString = str1.split(" = ");
            if (arrayOfString.length != 2);
        }
        for (String str2 = arrayOfString[1]; ; str2 = null)
            return str2;
    }

    public String getNetworkVariable(int paramInt, String paramString)
    {
        if (TextUtils.isEmpty(paramString));
        for (String str = null; ; str = doStringCommand("GET_NETWORK " + paramInt + " " + paramString))
            return str;
    }

    public String listNetworks()
    {
        return doStringCommand("LIST_NETWORKS");
    }

    public boolean p2pCancelConnect()
    {
        return doBooleanCommand("P2P_CANCEL");
    }

    public String p2pConnect(WifiP2pConfig paramWifiP2pConfig, boolean paramBoolean)
    {
        if (paramWifiP2pConfig == null);
        String str1;
        for (String str2 = null; ; str2 = doStringCommand(str1))
        {
            return str2;
            ArrayList localArrayList = new ArrayList();
            WpsInfo localWpsInfo = paramWifiP2pConfig.wps;
            localArrayList.add(paramWifiP2pConfig.deviceAddress);
            switch (localWpsInfo.setup)
            {
            default:
                if (paramBoolean)
                    localArrayList.add("join");
                break;
            case 0:
            case 1:
            case 2:
            case 3:
            }
            while (true)
            {
                str1 = "P2P_CONNECT ";
                Iterator localIterator = localArrayList.iterator();
                while (localIterator.hasNext())
                {
                    String str3 = (String)localIterator.next();
                    str1 = str1 + str3 + " ";
                }
                localArrayList.add("pbc");
                break;
                if (TextUtils.isEmpty(localWpsInfo.pin))
                    localArrayList.add("pin");
                while (true)
                {
                    localArrayList.add("display");
                    break;
                    localArrayList.add(localWpsInfo.pin);
                }
                localArrayList.add(localWpsInfo.pin);
                localArrayList.add("keypad");
                break;
                localArrayList.add(localWpsInfo.pin);
                localArrayList.add("label");
                break;
                int i = paramWifiP2pConfig.groupOwnerIntent;
                if ((i < 0) || (i > 15))
                    i = 7;
                localArrayList.add("go_intent=" + i);
            }
        }
    }

    public boolean p2pFind()
    {
        return doBooleanCommand("P2P_FIND");
    }

    public boolean p2pFind(int paramInt)
    {
        if (paramInt <= 0);
        for (boolean bool = p2pFind(); ; bool = doBooleanCommand("P2P_FIND " + paramInt))
            return bool;
    }

    public boolean p2pFlush()
    {
        return doBooleanCommand("P2P_FLUSH");
    }

    public String p2pGetDeviceAddress()
    {
        String str1 = status();
        String str2;
        if (str1 == null)
        {
            str2 = "";
            return str2;
        }
        String[] arrayOfString1 = str1.split("\n");
        int i = arrayOfString1.length;
        for (int j = 0; ; j++)
        {
            String[] arrayOfString2;
            if (j < i)
            {
                String str3 = arrayOfString1[j];
                if (!str3.startsWith("p2p_device_address="))
                    continue;
                arrayOfString2 = str3.split("=");
                if (arrayOfString2.length == 2);
            }
            else
            {
                str2 = "";
                break;
            }
            str2 = arrayOfString2[1];
            break;
        }
    }

    public boolean p2pGroupAdd()
    {
        return doBooleanCommand("P2P_GROUP_ADD");
    }

    public boolean p2pGroupRemove(String paramString)
    {
        if (TextUtils.isEmpty(paramString));
        for (boolean bool = false; ; bool = doBooleanCommand("P2P_GROUP_REMOVE " + paramString))
            return bool;
    }

    public boolean p2pInvite(WifiP2pGroup paramWifiP2pGroup, String paramString)
    {
        boolean bool;
        if (TextUtils.isEmpty(paramString))
            bool = false;
        while (true)
        {
            return bool;
            if (paramWifiP2pGroup == null)
                bool = doBooleanCommand("P2P_INVITE peer=" + paramString);
            else
                bool = doBooleanCommand("P2P_INVITE group=" + paramWifiP2pGroup.getInterface() + " peer=" + paramString + " go_dev_addr=" + paramWifiP2pGroup.getOwner().deviceAddress);
        }
    }

    public boolean p2pListen()
    {
        return doBooleanCommand("P2P_LISTEN");
    }

    public boolean p2pListen(int paramInt)
    {
        if (paramInt <= 0);
        for (boolean bool = p2pListen(); ; bool = doBooleanCommand("P2P_LISTEN " + paramInt))
            return bool;
    }

    public String p2pPeer(String paramString)
    {
        return doStringCommand("P2P_PEER " + paramString);
    }

    public boolean p2pProvisionDiscovery(WifiP2pConfig paramWifiP2pConfig)
    {
        boolean bool = false;
        if (paramWifiP2pConfig == null);
        while (true)
        {
            return bool;
            switch (paramWifiP2pConfig.wps.setup)
            {
            default:
                break;
            case 0:
                bool = doBooleanCommand("P2P_PROV_DISC " + paramWifiP2pConfig.deviceAddress + " pbc");
                break;
            case 1:
                bool = doBooleanCommand("P2P_PROV_DISC " + paramWifiP2pConfig.deviceAddress + " keypad");
                break;
            case 2:
                bool = doBooleanCommand("P2P_PROV_DISC " + paramWifiP2pConfig.deviceAddress + " display");
            }
        }
    }

    public boolean p2pReinvoke(int paramInt, String paramString)
    {
        if ((TextUtils.isEmpty(paramString)) || (paramInt < 0));
        for (boolean bool = false; ; bool = doBooleanCommand("P2P_INVITE persistent=" + paramInt + " peer=" + paramString))
            return bool;
    }

    public boolean p2pReject(String paramString)
    {
        return doBooleanCommand("P2P_REJECT " + paramString);
    }

    public boolean p2pServDiscCancelReq(String paramString)
    {
        return doBooleanCommand("P2P_SERV_DISC_CANCEL_REQ " + paramString);
    }

    public String p2pServDiscReq(String paramString1, String paramString2)
    {
        String str = "P2P_SERV_DISC_REQ" + " " + paramString1;
        return doStringCommand(str + " " + paramString2);
    }

    public boolean p2pServiceAdd(WifiP2pServiceInfo paramWifiP2pServiceInfo)
    {
        Iterator localIterator = paramWifiP2pServiceInfo.getSupplicantQueryList().iterator();
        String str;
        do
        {
            if (!localIterator.hasNext())
                break;
            str = (String)localIterator.next();
        }
        while (doBooleanCommand("P2P_SERVICE_ADD" + " " + str));
        for (boolean bool = false; ; bool = true)
            return bool;
    }

    public boolean p2pServiceDel(WifiP2pServiceInfo paramWifiP2pServiceInfo)
    {
        boolean bool = false;
        Iterator localIterator = paramWifiP2pServiceInfo.getSupplicantQueryList().iterator();
        String str1;
        String[] arrayOfString;
        if (localIterator.hasNext())
        {
            str1 = (String)localIterator.next();
            arrayOfString = str1.split(" ");
            if (arrayOfString.length >= 2);
        }
        while (true)
        {
            return bool;
            if ("upnp".equals(arrayOfString[0]));
            String str2;
            for (String str3 = "P2P_SERVICE_DEL " + str1; ; str3 = str2 + " " + arrayOfString[1])
            {
                if (doBooleanCommand(str3))
                    break label166;
                break;
                if (!"bonjour".equals(arrayOfString[0]))
                    break;
                str2 = "P2P_SERVICE_DEL " + arrayOfString[0];
            }
            label166: break;
            bool = true;
        }
    }

    public boolean p2pServiceFlush()
    {
        return doBooleanCommand("P2P_SERVICE_FLUSH");
    }

    public boolean p2pStopFind()
    {
        return doBooleanCommand("P2P_STOP_FIND");
    }

    public boolean ping()
    {
        String str = doStringCommand("PING");
        if ((str != null) && (str.equals("PONG")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean reassociate()
    {
        return doBooleanCommand("REASSOCIATE");
    }

    public boolean reconnect()
    {
        return doBooleanCommand("RECONNECT");
    }

    public boolean removeNetwork(int paramInt)
    {
        return doBooleanCommand("REMOVE_NETWORK " + paramInt);
    }

    public boolean saveConfig()
    {
        if ((doBooleanCommand("AP_SCAN 1")) && (doBooleanCommand("SAVE_CONFIG")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean scan()
    {
        return doBooleanCommand("SCAN");
    }

    public String scanResults()
    {
        return doStringCommand("SCAN_RESULTS");
    }

    public boolean setBand(int paramInt)
    {
        return doBooleanCommand("DRIVER SETBAND " + paramInt);
    }

    public boolean setBluetoothCoexistenceMode(int paramInt)
    {
        return doBooleanCommand("DRIVER BTCOEXMODE " + paramInt);
    }

    public boolean setBluetoothCoexistenceScanMode(boolean paramBoolean)
    {
        if (paramBoolean);
        for (boolean bool = doBooleanCommand("DRIVER BTCOEXSCAN-START"); ; bool = doBooleanCommand("DRIVER BTCOEXSCAN-STOP"))
            return bool;
    }

    public boolean setConcurrencyPriority(String paramString)
    {
        return doBooleanCommand("P2P_SET conc_pref " + paramString);
    }

    public boolean setConfigMethods(String paramString)
    {
        return doBooleanCommand("SET config_methods " + paramString);
    }

    public boolean setCountryCode(String paramString)
    {
        return doBooleanCommand("DRIVER COUNTRY " + paramString);
    }

    public boolean setDeviceName(String paramString)
    {
        return doBooleanCommand("SET device_name " + paramString);
    }

    public boolean setDeviceType(String paramString)
    {
        return doBooleanCommand("SET device_type " + paramString);
    }

    public boolean setManufacturer(String paramString)
    {
        return doBooleanCommand("SET manufacturer " + paramString);
    }

    public boolean setModelName(String paramString)
    {
        return doBooleanCommand("SET model_name " + paramString);
    }

    public boolean setModelNumber(String paramString)
    {
        return doBooleanCommand("SET model_number " + paramString);
    }

    public boolean setNetworkVariable(int paramInt, String paramString1, String paramString2)
    {
        if ((TextUtils.isEmpty(paramString1)) || (TextUtils.isEmpty(paramString2)));
        for (boolean bool = false; ; bool = doBooleanCommand("SET_NETWORK " + paramInt + " " + paramString1 + " " + paramString2))
            return bool;
    }

    public boolean setP2pGroupIdle(String paramString, int paramInt)
    {
        return doBooleanCommand("SET interface=" + paramString + " p2p_group_idle " + paramInt);
    }

    public boolean setP2pPowerSave(String paramString, boolean paramBoolean)
    {
        if (paramBoolean);
        for (boolean bool = doBooleanCommand("P2P_SET interface=" + paramString + " ps 1"); ; bool = doBooleanCommand("P2P_SET interface=" + paramString + " ps 0"))
            return bool;
    }

    public boolean setP2pSsidPostfix(String paramString)
    {
        return doBooleanCommand("SET p2p_ssid_postfix " + paramString);
    }

    public boolean setPersistentReconnect(boolean paramBoolean)
    {
        boolean bool = true;
        if (paramBoolean == bool);
        while (true)
        {
            return doBooleanCommand("SET persistent_reconnect " + bool);
            bool = false;
        }
    }

    public void setPowerSave(boolean paramBoolean)
    {
        if (paramBoolean)
            doBooleanCommand("SET ps 1");
        while (true)
        {
            return;
            doBooleanCommand("SET ps 0");
        }
    }

    public void setScanInterval(int paramInt)
    {
        doBooleanCommand("SCAN_INTERVAL " + paramInt);
    }

    public boolean setScanMode(boolean paramBoolean)
    {
        if (paramBoolean);
        for (boolean bool = doBooleanCommand("DRIVER SCAN-ACTIVE"); ; bool = doBooleanCommand("DRIVER SCAN-PASSIVE"))
            return bool;
    }

    public boolean setScanResultHandling(int paramInt)
    {
        return doBooleanCommand("AP_SCAN " + paramInt);
    }

    public boolean setSerialNumber(String paramString)
    {
        return doBooleanCommand("SET serial_number " + paramString);
    }

    public boolean setSuspendOptimizations(boolean paramBoolean)
    {
        if (paramBoolean);
        for (boolean bool = doBooleanCommand("DRIVER SETSUSPENDMODE 1"); ; bool = doBooleanCommand("DRIVER SETSUSPENDMODE 0"))
            return bool;
    }

    public String signalPoll()
    {
        return doStringCommand("SIGNAL_POLL");
    }

    public boolean startDriver()
    {
        return doBooleanCommand("DRIVER START");
    }

    public boolean startFilteringMulticastV4Packets()
    {
        if ((doBooleanCommand("DRIVER RXFILTER-STOP")) && (doBooleanCommand("DRIVER RXFILTER-REMOVE 2")) && (doBooleanCommand("DRIVER RXFILTER-START")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean startFilteringMulticastV6Packets()
    {
        if ((doBooleanCommand("DRIVER RXFILTER-STOP")) && (doBooleanCommand("DRIVER RXFILTER-REMOVE 3")) && (doBooleanCommand("DRIVER RXFILTER-START")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean startWpsPbc(String paramString)
    {
        if (TextUtils.isEmpty(paramString));
        for (boolean bool = doBooleanCommand("WPS_PBC"); ; bool = doBooleanCommand("WPS_PBC " + paramString))
            return bool;
    }

    public boolean startWpsPbc(String paramString1, String paramString2)
    {
        if (TextUtils.isEmpty(paramString2));
        for (boolean bool = doBooleanCommand("WPS_PBC interface=" + paramString1); ; bool = doBooleanCommand("WPS_PBC interface=" + paramString1 + " " + paramString2))
            return bool;
    }

    public String startWpsPinDisplay(String paramString)
    {
        if (TextUtils.isEmpty(paramString));
        for (String str = doStringCommand("WPS_PIN any"); ; str = doStringCommand("WPS_PIN " + paramString))
            return str;
    }

    public String startWpsPinDisplay(String paramString1, String paramString2)
    {
        if (TextUtils.isEmpty(paramString2));
        for (String str = doStringCommand("WPS_PIN interface=" + paramString1 + " any"); ; str = doStringCommand("WPS_PIN interface=" + paramString1 + " " + paramString2))
            return str;
    }

    public boolean startWpsPinKeypad(String paramString)
    {
        if (TextUtils.isEmpty(paramString));
        for (boolean bool = false; ; bool = doBooleanCommand("WPS_PIN any " + paramString))
            return bool;
    }

    public boolean startWpsPinKeypad(String paramString1, String paramString2)
    {
        if (TextUtils.isEmpty(paramString2));
        for (boolean bool = false; ; bool = doBooleanCommand("WPS_PIN interface=" + paramString1 + " any " + paramString2))
            return bool;
    }

    public boolean startWpsRegistrar(String paramString1, String paramString2)
    {
        if ((TextUtils.isEmpty(paramString1)) || (TextUtils.isEmpty(paramString2)));
        for (boolean bool = false; ; bool = doBooleanCommand("WPS_REG " + paramString1 + " " + paramString2))
            return bool;
    }

    public String status()
    {
        return doStringCommand("STATUS");
    }

    public boolean stopDriver()
    {
        return doBooleanCommand("DRIVER STOP");
    }

    public boolean stopFilteringMulticastV4Packets()
    {
        if ((doBooleanCommand("DRIVER RXFILTER-STOP")) && (doBooleanCommand("DRIVER RXFILTER-ADD 2")) && (doBooleanCommand("DRIVER RXFILTER-START")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean stopFilteringMulticastV6Packets()
    {
        if ((doBooleanCommand("DRIVER RXFILTER-STOP")) && (doBooleanCommand("DRIVER RXFILTER-ADD 3")) && (doBooleanCommand("DRIVER RXFILTER-START")));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean stopSupplicant()
    {
        return doBooleanCommand("TERMINATE");
    }

    public String waitForEvent()
    {
        return waitForEvent(this.mInterface);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.WifiNative
 * JD-Core Version:        0.6.2
 */