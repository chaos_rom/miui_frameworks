package android.net.wifi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.LinkCapabilities;
import android.net.LinkProperties;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.net.NetworkStateTracker;
import android.os.Handler;
import android.os.Message;
import android.util.Slog;
import java.util.concurrent.atomic.AtomicBoolean;

public class WifiStateTracker
    implements NetworkStateTracker
{
    private static final boolean LOGV = true;
    private static final String NETWORKTYPE = "WIFI";
    private static final String TAG = "WifiStateTracker";
    private Context mContext;
    private Handler mCsHandler;
    private AtomicBoolean mDefaultRouteSet = new AtomicBoolean(false);
    private NetworkInfo.State mLastState = NetworkInfo.State.UNKNOWN;
    private LinkCapabilities mLinkCapabilities;
    private LinkProperties mLinkProperties;
    private NetworkInfo mNetworkInfo;
    private AtomicBoolean mPrivateDnsRouteSet = new AtomicBoolean(false);
    private AtomicBoolean mTeardownRequested = new AtomicBoolean(false);
    private WifiManager mWifiManager;
    private BroadcastReceiver mWifiStateReceiver;

    public WifiStateTracker(int paramInt, String paramString)
    {
        this.mNetworkInfo = new NetworkInfo(paramInt, 0, paramString, "");
        this.mLinkProperties = new LinkProperties();
        this.mLinkCapabilities = new LinkCapabilities();
        this.mNetworkInfo.setIsAvailable(false);
        setTeardownRequested(false);
    }

    public void defaultRouteSet(boolean paramBoolean)
    {
        this.mDefaultRouteSet.set(paramBoolean);
    }

    public LinkCapabilities getLinkCapabilities()
    {
        return new LinkCapabilities(this.mLinkCapabilities);
    }

    public LinkProperties getLinkProperties()
    {
        return new LinkProperties(this.mLinkProperties);
    }

    public NetworkInfo getNetworkInfo()
    {
        return new NetworkInfo(this.mNetworkInfo);
    }

    public String getTcpBufferSizesPropName()
    {
        return "net.tcp.buffersize.wifi";
    }

    public boolean isAvailable()
    {
        return this.mNetworkInfo.isAvailable();
    }

    public boolean isDefaultRouteSet()
    {
        return this.mDefaultRouteSet.get();
    }

    public boolean isPrivateDnsRouteSet()
    {
        return this.mPrivateDnsRouteSet.get();
    }

    public boolean isTeardownRequested()
    {
        return this.mTeardownRequested.get();
    }

    public void privateDnsRouteSet(boolean paramBoolean)
    {
        this.mPrivateDnsRouteSet.set(paramBoolean);
    }

    public boolean reconnect()
    {
        this.mTeardownRequested.set(false);
        this.mWifiManager.startWifi();
        return true;
    }

    public void setDependencyMet(boolean paramBoolean)
    {
    }

    public void setPolicyDataEnable(boolean paramBoolean)
    {
    }

    public boolean setRadio(boolean paramBoolean)
    {
        this.mWifiManager.setWifiEnabled(paramBoolean);
        return true;
    }

    public void setTeardownRequested(boolean paramBoolean)
    {
        this.mTeardownRequested.set(paramBoolean);
    }

    public void setUserDataEnable(boolean paramBoolean)
    {
        Slog.w("WifiStateTracker", "ignoring setUserDataEnable(" + paramBoolean + ")");
    }

    public void startMonitoring(Context paramContext, Handler paramHandler)
    {
        this.mCsHandler = paramHandler;
        this.mContext = paramContext;
        this.mWifiManager = ((WifiManager)this.mContext.getSystemService("wifi"));
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("android.net.wifi.STATE_CHANGE");
        localIntentFilter.addAction("android.net.wifi.LINK_CONFIGURATION_CHANGED");
        localIntentFilter.addAction("android.net.wifi.p2p.CONNECTION_STATE_CHANGE");
        this.mWifiStateReceiver = new WifiStateReceiver(null);
        this.mContext.registerReceiver(this.mWifiStateReceiver, localIntentFilter);
    }

    public boolean teardown()
    {
        this.mTeardownRequested.set(true);
        this.mWifiManager.stopWifi();
        return true;
    }

    private class WifiStateReceiver extends BroadcastReceiver
    {
        private WifiStateReceiver()
        {
        }

        public void onReceive(Context paramContext, Intent paramIntent)
        {
            if (paramIntent.getAction().equals("android.net.wifi.p2p.CONNECTION_STATE_CHANGE"))
            {
                WifiStateTracker.access$102(WifiStateTracker.this, (NetworkInfo)paramIntent.getParcelableExtra("networkInfo"));
                WifiStateTracker.access$202(WifiStateTracker.this, (LinkProperties)paramIntent.getParcelableExtra("linkProperties"));
                if (WifiStateTracker.this.mLinkProperties == null)
                    WifiStateTracker.access$202(WifiStateTracker.this, new LinkProperties());
                WifiStateTracker.access$302(WifiStateTracker.this, (LinkCapabilities)paramIntent.getParcelableExtra("linkCapabilities"));
                if (WifiStateTracker.this.mLinkCapabilities == null)
                    WifiStateTracker.access$302(WifiStateTracker.this, new LinkCapabilities());
            }
            while (true)
            {
                return;
                if (paramIntent.getAction().equals("android.net.wifi.STATE_CHANGE"))
                {
                    WifiStateTracker.access$102(WifiStateTracker.this, (NetworkInfo)paramIntent.getParcelableExtra("networkInfo"));
                    WifiStateTracker.access$202(WifiStateTracker.this, (LinkProperties)paramIntent.getParcelableExtra("linkProperties"));
                    if (WifiStateTracker.this.mLinkProperties == null)
                        WifiStateTracker.access$202(WifiStateTracker.this, new LinkProperties());
                    WifiStateTracker.access$302(WifiStateTracker.this, (LinkCapabilities)paramIntent.getParcelableExtra("linkCapabilities"));
                    if (WifiStateTracker.this.mLinkCapabilities == null)
                        WifiStateTracker.access$302(WifiStateTracker.this, new LinkCapabilities());
                    NetworkInfo.State localState = WifiStateTracker.this.mNetworkInfo.getState();
                    if (WifiStateTracker.this.mLastState != localState)
                    {
                        WifiStateTracker.access$402(WifiStateTracker.this, localState);
                        WifiStateTracker.this.mCsHandler.obtainMessage(1, new NetworkInfo(WifiStateTracker.this.mNetworkInfo)).sendToTarget();
                    }
                }
                else if (paramIntent.getAction().equals("android.net.wifi.LINK_CONFIGURATION_CHANGED"))
                {
                    WifiStateTracker.access$202(WifiStateTracker.this, (LinkProperties)paramIntent.getParcelableExtra("linkProperties"));
                    WifiStateTracker.this.mCsHandler.obtainMessage(3, WifiStateTracker.this.mNetworkInfo).sendToTarget();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.WifiStateTracker
 * JD-Core Version:        0.6.2
 */