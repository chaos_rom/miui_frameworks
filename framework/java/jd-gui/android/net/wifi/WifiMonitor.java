package android.net.wifi;

import android.net.NetworkInfo.DetailedState;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pProvDiscEvent;
import android.net.wifi.p2p.nsd.WifiP2pServiceResponse;
import android.os.Message;
import android.util.Log;
import com.android.internal.util.StateMachine;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WifiMonitor
{
    public static final int AP_STA_CONNECTED_EVENT = 147498;
    private static final String AP_STA_CONNECTED_STR = "AP-STA-CONNECTED";
    public static final int AP_STA_DISCONNECTED_EVENT = 147497;
    private static final String AP_STA_DISCONNECTED_STR = "AP-STA-DISCONNECTED";
    public static final int AUTHENTICATION_FAILURE_EVENT = 147463;
    private static final int BASE = 147456;
    private static final int CONFIG_AUTH_FAILURE = 18;
    private static final int CONFIG_MULTIPLE_PBC_DETECTED = 12;
    private static final int CONNECTED = 1;
    private static final String CONNECTED_STR = "CONNECTED";
    private static final int DISCONNECTED = 2;
    private static final String DISCONNECTED_STR = "DISCONNECTED";
    public static final int DRIVER_HUNG_EVENT = 147468;
    private static final int DRIVER_STATE = 7;
    private static final String DRIVER_STATE_STR = "DRIVER-STATE";
    private static final String EAP_AUTH_FAILURE_STR = "EAP authentication failed";
    private static final int EAP_FAILURE = 8;
    private static final String EAP_FAILURE_STR = "EAP-FAILURE";
    private static final int EVENT_PREFIX_LEN_STR = 0;
    private static final String EVENT_PREFIX_STR = "CTRL-EVENT-";
    private static final String HOST_AP_EVENT_PREFIX_STR = "AP";
    private static final int LINK_SPEED = 5;
    private static final String LINK_SPEED_STR = "LINK-SPEED";
    private static final int MAX_RECV_ERRORS = 10;
    private static final String MONITOR_SOCKET_CLOSED_STR = "connection closed";
    public static final int NETWORK_CONNECTION_EVENT = 147459;
    public static final int NETWORK_DISCONNECTION_EVENT = 147460;
    public static final int P2P_DEVICE_FOUND_EVENT = 147477;
    private static final String P2P_DEVICE_FOUND_STR = "P2P-DEVICE-FOUND";
    public static final int P2P_DEVICE_LOST_EVENT = 147478;
    private static final String P2P_DEVICE_LOST_STR = "P2P-DEVICE-LOST";
    private static final String P2P_EVENT_PREFIX_STR = "P2P";
    public static final int P2P_FIND_STOPPED_EVENT = 147493;
    private static final String P2P_FIND_STOPPED_STR = "P2P-FIND-STOPPED";
    public static final int P2P_GO_NEGOTIATION_FAILURE_EVENT = 147482;
    public static final int P2P_GO_NEGOTIATION_REQUEST_EVENT = 147479;
    public static final int P2P_GO_NEGOTIATION_SUCCESS_EVENT = 147481;
    private static final String P2P_GO_NEG_FAILURE_STR = "P2P-GO-NEG-FAILURE";
    private static final String P2P_GO_NEG_REQUEST_STR = "P2P-GO-NEG-REQUEST";
    private static final String P2P_GO_NEG_SUCCESS_STR = "P2P-GO-NEG-SUCCESS";
    public static final int P2P_GROUP_FORMATION_FAILURE_EVENT = 147484;
    private static final String P2P_GROUP_FORMATION_FAILURE_STR = "P2P-GROUP-FORMATION-FAILURE";
    public static final int P2P_GROUP_FORMATION_SUCCESS_EVENT = 147483;
    private static final String P2P_GROUP_FORMATION_SUCCESS_STR = "P2P-GROUP-FORMATION-SUCCESS";
    public static final int P2P_GROUP_REMOVED_EVENT = 147486;
    private static final String P2P_GROUP_REMOVED_STR = "P2P-GROUP-REMOVED";
    public static final int P2P_GROUP_STARTED_EVENT = 147485;
    private static final String P2P_GROUP_STARTED_STR = "P2P-GROUP-STARTED";
    public static final int P2P_INVITATION_RECEIVED_EVENT = 147487;
    private static final String P2P_INVITATION_RECEIVED_STR = "P2P-INVITATION-RECEIVED";
    public static final int P2P_INVITATION_RESULT_EVENT = 147488;
    private static final String P2P_INVITATION_RESULT_STR = "P2P-INVITATION-RESULT";
    public static final int P2P_PROV_DISC_ENTER_PIN_EVENT = 147491;
    private static final String P2P_PROV_DISC_ENTER_PIN_STR = "P2P-PROV-DISC-ENTER-PIN";
    public static final int P2P_PROV_DISC_PBC_REQ_EVENT = 147489;
    private static final String P2P_PROV_DISC_PBC_REQ_STR = "P2P-PROV-DISC-PBC-REQ";
    public static final int P2P_PROV_DISC_PBC_RSP_EVENT = 147490;
    private static final String P2P_PROV_DISC_PBC_RSP_STR = "P2P-PROV-DISC-PBC-RESP";
    public static final int P2P_PROV_DISC_SHOW_PIN_EVENT = 147492;
    private static final String P2P_PROV_DISC_SHOW_PIN_STR = "P2P-PROV-DISC-SHOW-PIN";
    public static final int P2P_SERV_DISC_RESP_EVENT = 147494;
    private static final String P2P_SERV_DISC_RESP_STR = "P2P-SERV-DISC-RESP";
    private static final String PASSWORD_MAY_BE_INCORRECT_STR = "pre-shared key may be incorrect";
    private static final int REASON_TKIP_ONLY_PROHIBITED = 1;
    private static final int REASON_WEP_PROHIBITED = 2;
    private static final int SCAN_RESULTS = 4;
    public static final int SCAN_RESULTS_EVENT = 147461;
    private static final String SCAN_RESULTS_STR = "SCAN-RESULTS";
    private static final int STATE_CHANGE = 3;
    private static final String STATE_CHANGE_STR = "STATE-CHANGE";
    public static final int SUPPLICANT_STATE_CHANGE_EVENT = 147462;
    public static final int SUP_CONNECTION_EVENT = 147457;
    public static final int SUP_DISCONNECTION_EVENT = 147458;
    private static final String TAG = "WifiMonitor";
    private static final int TERMINATING = 6;
    private static final String TERMINATING_STR = "TERMINATING";
    private static final int UNKNOWN = 9;
    private static final String WPA_EVENT_PREFIX_STR = "WPA:";
    private static final String WPA_RECV_ERROR_STR = "recv error";
    public static final int WPS_FAIL_EVENT = 147465;
    private static final String WPS_FAIL_PATTERN = "WPS-FAIL msg=\\d+(?: config_error=(\\d+))?(?: reason=(\\d+))?";
    private static final String WPS_FAIL_STR = "WPS-FAIL";
    public static final int WPS_OVERLAP_EVENT = 147466;
    private static final String WPS_OVERLAP_STR = "WPS-OVERLAP-DETECTED";
    public static final int WPS_SUCCESS_EVENT = 147464;
    private static final String WPS_SUCCESS_STR = "WPS-SUCCESS";
    public static final int WPS_TIMEOUT_EVENT = 147467;
    private static final String WPS_TIMEOUT_STR = "WPS-TIMEOUT";
    private static Pattern mConnectedEventPattern = Pattern.compile("((?:[0-9a-f]{2}:){5}[0-9a-f]{2}) .* \\[id=([0-9]+) ");
    private int mRecvErrors = 0;
    private final StateMachine mStateMachine;
    private final WifiNative mWifiNative;

    public WifiMonitor(StateMachine paramStateMachine, WifiNative paramWifiNative)
    {
        this.mStateMachine = paramStateMachine;
        this.mWifiNative = paramWifiNative;
    }

    private void handleNetworkStateChange(NetworkInfo.DetailedState paramDetailedState, String paramString)
    {
        String str = null;
        int i = -1;
        Matcher localMatcher;
        if (paramDetailedState == NetworkInfo.DetailedState.CONNECTED)
        {
            localMatcher = mConnectedEventPattern.matcher(paramString);
            if (localMatcher.find())
                break label39;
        }
        while (true)
        {
            notifyNetworkStateChange(paramDetailedState, str, i);
            return;
            label39: str = localMatcher.group(1);
            try
            {
                int j = Integer.parseInt(localMatcher.group(2));
                i = j;
            }
            catch (NumberFormatException localNumberFormatException)
            {
                i = -1;
            }
        }
    }

    private static void nap(int paramInt)
    {
        long l = paramInt * 1000;
        try
        {
            Thread.sleep(l);
            label11: return;
        }
        catch (InterruptedException localInterruptedException)
        {
            break label11;
        }
    }

    void notifyNetworkStateChange(NetworkInfo.DetailedState paramDetailedState, String paramString, int paramInt)
    {
        if (paramDetailedState == NetworkInfo.DetailedState.CONNECTED)
        {
            Message localMessage2 = this.mStateMachine.obtainMessage(147459, paramInt, 0, paramString);
            this.mStateMachine.sendMessage(localMessage2);
        }
        while (true)
        {
            return;
            Message localMessage1 = this.mStateMachine.obtainMessage(147460, paramInt, 0, paramString);
            this.mStateMachine.sendMessage(localMessage1);
        }
    }

    void notifySupplicantStateChange(int paramInt, String paramString1, String paramString2, SupplicantState paramSupplicantState)
    {
        this.mStateMachine.sendMessage(this.mStateMachine.obtainMessage(147462, new StateChangeResult(paramInt, paramString1, paramString2, paramSupplicantState)));
    }

    public void startMonitoring()
    {
        new MonitorThread().start();
    }

    class MonitorThread extends Thread
    {
        public MonitorThread()
        {
            super();
        }

        private boolean connectToSupplicant()
        {
            int i = 1;
            int k = 0;
            if (WifiMonitor.this.mWifiNative.connectToSupplicant());
            while (true)
            {
                return i;
                int m = k + 1;
                if (k < 5)
                {
                    WifiMonitor.nap(i);
                    k = m;
                    break;
                }
                int j = 0;
            }
        }

        private void handleDriverEvent(String paramString)
        {
            if (paramString == null);
            while (true)
            {
                return;
                if (paramString.equals("HANGED"))
                    WifiMonitor.this.mStateMachine.sendMessage(147468);
            }
        }

        private void handleHostApEvents(String paramString)
        {
            String[] arrayOfString = paramString.split(" ");
            if (arrayOfString[0].equals("AP-STA-CONNECTED"))
                WifiMonitor.this.mStateMachine.sendMessage(147498, new WifiP2pDevice(paramString));
            while (true)
            {
                return;
                if (arrayOfString[0].equals("AP-STA-DISCONNECTED"))
                    WifiMonitor.this.mStateMachine.sendMessage(147497, new WifiP2pDevice(paramString));
            }
        }

        private void handleP2pEvents(String paramString)
        {
            if (paramString.startsWith("P2P-DEVICE-FOUND"))
                WifiMonitor.this.mStateMachine.sendMessage(147477, new WifiP2pDevice(paramString));
            while (true)
            {
                return;
                if (paramString.startsWith("P2P-DEVICE-LOST"))
                {
                    WifiMonitor.this.mStateMachine.sendMessage(147478, new WifiP2pDevice(paramString));
                }
                else if (paramString.startsWith("P2P-FIND-STOPPED"))
                {
                    WifiMonitor.this.mStateMachine.sendMessage(147493);
                }
                else if (paramString.startsWith("P2P-GO-NEG-REQUEST"))
                {
                    WifiMonitor.this.mStateMachine.sendMessage(147479, new WifiP2pConfig(paramString));
                }
                else if (paramString.startsWith("P2P-GO-NEG-SUCCESS"))
                {
                    WifiMonitor.this.mStateMachine.sendMessage(147481);
                }
                else if (paramString.startsWith("P2P-GO-NEG-FAILURE"))
                {
                    WifiMonitor.this.mStateMachine.sendMessage(147482);
                }
                else if (paramString.startsWith("P2P-GROUP-FORMATION-SUCCESS"))
                {
                    WifiMonitor.this.mStateMachine.sendMessage(147483);
                }
                else if (paramString.startsWith("P2P-GROUP-FORMATION-FAILURE"))
                {
                    WifiMonitor.this.mStateMachine.sendMessage(147484);
                }
                else if (paramString.startsWith("P2P-GROUP-STARTED"))
                {
                    WifiMonitor.this.mStateMachine.sendMessage(147485, new WifiP2pGroup(paramString));
                }
                else if (paramString.startsWith("P2P-GROUP-REMOVED"))
                {
                    WifiMonitor.this.mStateMachine.sendMessage(147486, new WifiP2pGroup(paramString));
                }
                else if (paramString.startsWith("P2P-INVITATION-RECEIVED"))
                {
                    WifiMonitor.this.mStateMachine.sendMessage(147487, new WifiP2pGroup(paramString));
                }
                else if (paramString.startsWith("P2P-INVITATION-RESULT"))
                {
                    String[] arrayOfString1 = paramString.split(" ");
                    if (arrayOfString1.length == 2)
                    {
                        String[] arrayOfString2 = arrayOfString1[1].split("=");
                        if (arrayOfString2.length == 2)
                            WifiMonitor.this.mStateMachine.sendMessage(147488, arrayOfString2[1]);
                    }
                }
                else if (paramString.startsWith("P2P-PROV-DISC-PBC-REQ"))
                {
                    WifiMonitor.this.mStateMachine.sendMessage(147489, new WifiP2pProvDiscEvent(paramString));
                }
                else if (paramString.startsWith("P2P-PROV-DISC-PBC-RESP"))
                {
                    WifiMonitor.this.mStateMachine.sendMessage(147490, new WifiP2pProvDiscEvent(paramString));
                }
                else if (paramString.startsWith("P2P-PROV-DISC-ENTER-PIN"))
                {
                    WifiMonitor.this.mStateMachine.sendMessage(147491, new WifiP2pProvDiscEvent(paramString));
                }
                else if (paramString.startsWith("P2P-PROV-DISC-SHOW-PIN"))
                {
                    WifiMonitor.this.mStateMachine.sendMessage(147492, new WifiP2pProvDiscEvent(paramString));
                }
                else if (paramString.startsWith("P2P-SERV-DISC-RESP"))
                {
                    List localList = WifiP2pServiceResponse.newInstance(paramString);
                    if (localList != null)
                        WifiMonitor.this.mStateMachine.sendMessage(147494, localList);
                    else
                        Log.e("WifiMonitor", "Null service resp " + paramString);
                }
            }
        }

        private void handleSupplicantStateChange(String paramString)
        {
            String str1 = null;
            int i = paramString.lastIndexOf("SSID=");
            if (i != -1)
                str1 = paramString.substring(i + 5);
            String[] arrayOfString1 = paramString.split(" ");
            String str2 = null;
            int j = -1;
            int k = -1;
            int m = arrayOfString1.length;
            int n = 0;
            if (n < m)
            {
                String[] arrayOfString2 = arrayOfString1[n].split("=");
                if (arrayOfString2.length != 2);
                while (true)
                {
                    n++;
                    break;
                    if (arrayOfString2[0].equals("BSSID"))
                    {
                        str2 = arrayOfString2[1];
                    }
                    else
                    {
                        int i3;
                        try
                        {
                            i3 = Integer.parseInt(arrayOfString2[1]);
                            if (!arrayOfString2[0].equals("id"))
                                break label136;
                            j = i3;
                        }
                        catch (NumberFormatException localNumberFormatException)
                        {
                        }
                        continue;
                        label136: if (arrayOfString2[0].equals("state"))
                            k = i3;
                    }
                }
            }
            if (k == -1)
                return;
            Object localObject = SupplicantState.INVALID;
            SupplicantState[] arrayOfSupplicantState = SupplicantState.values();
            int i1 = arrayOfSupplicantState.length;
            for (int i2 = 0; ; i2++)
                if (i2 < i1)
                {
                    SupplicantState localSupplicantState = arrayOfSupplicantState[i2];
                    if (localSupplicantState.ordinal() == k)
                        localObject = localSupplicantState;
                }
                else
                {
                    if (localObject == SupplicantState.INVALID)
                        Log.w("WifiMonitor", "Invalid supplicant state: " + k);
                    WifiMonitor.this.notifySupplicantStateChange(j, str1, str2, (SupplicantState)localObject);
                    break;
                }
        }

        private void handleWpsFailEvent(String paramString)
        {
            Matcher localMatcher = Pattern.compile("WPS-FAIL msg=\\d+(?: config_error=(\\d+))?(?: reason=(\\d+))?").matcher(paramString);
            String str1;
            String str2;
            if (localMatcher.find())
            {
                str1 = localMatcher.group(1);
                str2 = localMatcher.group(2);
                if (str2 == null);
            }
            switch (Integer.parseInt(str2))
            {
            default:
                if (str1 != null);
                switch (Integer.parseInt(str1))
                {
                default:
                    WifiMonitor.this.mStateMachine.sendMessage(WifiMonitor.this.mStateMachine.obtainMessage(147465, 0, 0));
                case 18:
                case 12:
                }
                break;
            case 1:
            case 2:
            }
            while (true)
            {
                return;
                WifiMonitor.this.mStateMachine.sendMessage(WifiMonitor.this.mStateMachine.obtainMessage(147465, 5, 0));
                continue;
                WifiMonitor.this.mStateMachine.sendMessage(WifiMonitor.this.mStateMachine.obtainMessage(147465, 4, 0));
                continue;
                WifiMonitor.this.mStateMachine.sendMessage(WifiMonitor.this.mStateMachine.obtainMessage(147465, 6, 0));
                continue;
                WifiMonitor.this.mStateMachine.sendMessage(WifiMonitor.this.mStateMachine.obtainMessage(147465, 3, 0));
            }
        }

        void handleEvent(int paramInt, String paramString)
        {
            switch (paramInt)
            {
            case 3:
            default:
            case 2:
            case 1:
            case 4:
            }
            while (true)
            {
                return;
                WifiMonitor.this.handleNetworkStateChange(NetworkInfo.DetailedState.DISCONNECTED, paramString);
                continue;
                WifiMonitor.this.handleNetworkStateChange(NetworkInfo.DetailedState.CONNECTED, paramString);
                continue;
                WifiMonitor.this.mStateMachine.sendMessage(147461);
            }
        }

        public void run()
        {
            if (connectToSupplicant())
                WifiMonitor.this.mStateMachine.sendMessage(147457);
            String str1;
            label90: String str2;
            do
            {
                while (true)
                {
                    str1 = WifiMonitor.this.mWifiNative.waitForEvent();
                    if (str1.startsWith("CTRL-EVENT-"))
                        break;
                    if ((str1.startsWith("WPA:")) && (str1.indexOf("pre-shared key may be incorrect") > 0))
                    {
                        WifiMonitor.this.mStateMachine.sendMessage(147463);
                        continue;
                        WifiMonitor.this.mStateMachine.sendMessage(147458);
                    }
                    else if (str1.startsWith("WPS-SUCCESS"))
                    {
                        WifiMonitor.this.mStateMachine.sendMessage(147464);
                    }
                    else if (str1.startsWith("WPS-FAIL"))
                    {
                        handleWpsFailEvent(str1);
                    }
                    else if (str1.startsWith("WPS-OVERLAP-DETECTED"))
                    {
                        WifiMonitor.this.mStateMachine.sendMessage(147466);
                    }
                    else if (str1.startsWith("WPS-TIMEOUT"))
                    {
                        WifiMonitor.this.mStateMachine.sendMessage(147467);
                    }
                    else if (str1.startsWith("P2P"))
                    {
                        handleP2pEvents(str1);
                    }
                    else if (str1.startsWith("AP"))
                    {
                        handleHostApEvents(str1);
                    }
                }
                str2 = str1.substring(WifiMonitor.EVENT_PREFIX_LEN_STR);
                int i = str2.indexOf(' ');
                if (i != -1)
                    str2 = str2.substring(0, i);
            }
            while (str2.length() == 0);
            int j;
            label271: String str3;
            if (str2.equals("CONNECTED"))
            {
                j = 1;
                str3 = str1;
                if ((j != 7) && (j != 5))
                    break label444;
                str3 = str3.split(" ")[1];
                label298: if (j != 3)
                    break label514;
                handleSupplicantStateChange(str3);
            }
            while (true)
            {
                WifiMonitor.access$302(WifiMonitor.this, 0);
                break;
                if (str2.equals("DISCONNECTED"))
                {
                    j = 2;
                    break label271;
                }
                if (str2.equals("STATE-CHANGE"))
                {
                    j = 3;
                    break label271;
                }
                if (str2.equals("SCAN-RESULTS"))
                {
                    j = 4;
                    break label271;
                }
                if (str2.equals("LINK-SPEED"))
                {
                    j = 5;
                    break label271;
                }
                if (str2.equals("TERMINATING"))
                {
                    j = 6;
                    break label271;
                }
                if (str2.equals("DRIVER-STATE"))
                {
                    j = 7;
                    break label271;
                }
                if (str2.equals("EAP-FAILURE"))
                {
                    j = 8;
                    break label271;
                }
                j = 9;
                break label271;
                label444: if ((j == 3) || (j == 8))
                {
                    int k = str1.indexOf(" ");
                    if (k == -1)
                        break label298;
                    str3 = str1.substring(k + 1);
                    break label298;
                }
                int m = str1.indexOf(" - ");
                if (m == -1)
                    break label298;
                str3 = str1.substring(m + 3);
                break label298;
                label514: if (j == 7)
                {
                    handleDriverEvent(str3);
                }
                else
                {
                    if (j == 6)
                    {
                        if ((str3.startsWith("recv error")) && (WifiMonitor.access$304(WifiMonitor.this) <= 10))
                            break;
                        WifiMonitor.this.mStateMachine.sendMessage(147458);
                        break label90;
                    }
                    if (j == 8)
                    {
                        if (str3.startsWith("EAP authentication failed"))
                            WifiMonitor.this.mStateMachine.sendMessage(147463);
                    }
                    else
                        handleEvent(j, str3);
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.WifiMonitor
 * JD-Core Version:        0.6.2
 */