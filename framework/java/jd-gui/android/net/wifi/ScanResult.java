package android.net.wifi;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class ScanResult
    implements Parcelable
{
    public static final Parcelable.Creator<ScanResult> CREATOR = new Parcelable.Creator()
    {
        public ScanResult createFromParcel(Parcel paramAnonymousParcel)
        {
            return new ScanResult(paramAnonymousParcel.readString(), paramAnonymousParcel.readString(), paramAnonymousParcel.readString(), paramAnonymousParcel.readInt(), paramAnonymousParcel.readInt());
        }

        public ScanResult[] newArray(int paramAnonymousInt)
        {
            return new ScanResult[paramAnonymousInt];
        }
    };
    public String BSSID;
    public String SSID;
    public String capabilities;
    public int frequency;
    public int level;

    public ScanResult(String paramString1, String paramString2, String paramString3, int paramInt1, int paramInt2)
    {
        this.SSID = paramString1;
        this.BSSID = paramString2;
        this.capabilities = paramString3;
        this.level = paramInt1;
        this.frequency = paramInt2;
    }

    public int describeContents()
    {
        return 0;
    }

    public String toString()
    {
        StringBuffer localStringBuffer1 = new StringBuffer();
        String str1 = "<none>";
        StringBuffer localStringBuffer2 = localStringBuffer1.append("SSID: ");
        String str2;
        String str3;
        label51: StringBuffer localStringBuffer4;
        if (this.SSID == null)
        {
            str2 = str1;
            StringBuffer localStringBuffer3 = localStringBuffer2.append(str2).append(", BSSID: ");
            if (this.BSSID != null)
                break label117;
            str3 = str1;
            localStringBuffer4 = localStringBuffer3.append(str3).append(", capabilities: ");
            if (this.capabilities != null)
                break label126;
        }
        while (true)
        {
            localStringBuffer4.append(str1).append(", level: ").append(this.level).append(", frequency: ").append(this.frequency);
            return localStringBuffer1.toString();
            str2 = this.SSID;
            break;
            label117: str3 = this.BSSID;
            break label51;
            label126: str1 = this.capabilities;
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.SSID);
        paramParcel.writeString(this.BSSID);
        paramParcel.writeString(this.capabilities);
        paramParcel.writeInt(this.level);
        paramParcel.writeInt(this.frequency);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.ScanResult
 * JD-Core Version:        0.6.2
 */