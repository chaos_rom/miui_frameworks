package android.net.wifi;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.net.ConnectivityManager;
import android.net.LinkAddress;
import android.net.LinkProperties;
import android.net.NetworkInfo;
import android.net.RouteInfo;
import android.net.Uri;
import android.net.arp.ArpPeer;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.provider.Settings.Secure;
import android.util.Log;
import com.android.internal.util.AsyncChannel;
import com.android.internal.util.IState;
import com.android.internal.util.State;
import com.android.internal.util.StateMachine;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.URL;
import java.util.Collection;
import java.util.Iterator;

public class WifiWatchdogStateMachine extends StateMachine
{
    private static final int BASE = 135168;
    private static final int CMD_ARP_CHECK = 135179;
    private static final int CMD_DELAYED_WALLED_GARDEN_CHECK = 135180;
    private static final int CMD_RSSI_FETCH = 135181;
    private static boolean DBG = false;
    private static final long DEFAULT_ARP_CHECK_INTERVAL_MS = 120000L;
    private static final int DEFAULT_ARP_PING_TIMEOUT_MS = 100;
    private static final int DEFAULT_MIN_ARP_RESPONSES = 1;
    private static final int DEFAULT_NUM_ARP_PINGS = 5;
    private static final long DEFAULT_RSSI_FETCH_INTERVAL_MS = 1000L;
    private static final long DEFAULT_WALLED_GARDEN_INTERVAL_MS = 1800000L;
    private static final String DEFAULT_WALLED_GARDEN_URL = "http://clients3.google.com/generate_204";
    private static final int EVENT_NETWORK_STATE_CHANGE = 135170;
    private static final int EVENT_RSSI_CHANGE = 135171;
    private static final int EVENT_WATCHDOG_SETTINGS_CHANGE = 135174;
    private static final int EVENT_WATCHDOG_TOGGLED = 135169;
    private static final int EVENT_WIFI_RADIO_STATE_CHANGE = 135173;
    private static final int FULL_ARP_CHECK = 1;
    static final int GOOD_LINK_DETECTED = 135190;
    private static final int[] MIN_INTERVAL_AVOID_BSSID_MS = arrayOfInt;
    static final int POOR_LINK_DETECTED = 135189;
    static final int RSSI_FETCH = 135191;
    static final int RSSI_FETCH_FAILED = 135193;
    static final int RSSI_FETCH_SUCCEEDED = 135192;
    private static final int RSSI_LEVEL_MONITOR = 0;
    private static final int RSSI_MONITOR_COUNT = 5;
    private static final int RSSI_MONITOR_THRESHOLD = -88;
    private static final int SINGLE_ARP_CHECK = 0;
    private static final String TAG = "WifiWatchdogStateMachine";
    private static final String WALLED_GARDEN_NOTIFICATION_ID = "WifiWatchdog.walledgarden";
    private static final int WALLED_GARDEN_SOCKET_TIMEOUT_MS = 10000;
    private static final int WALLED_GARDEN_START_DELAY_MS = 3000;
    private static boolean sWifiOnly = false;
    private long mArpCheckIntervalMs;
    private int mArpPingTimeoutMs;
    private int mArpToken = 0;
    private BroadcastReceiver mBroadcastReceiver;
    private ConnectedState mConnectedState = new ConnectedState();
    private ContentResolver mContentResolver;
    private Context mContext;
    private int mCurrentSignalLevel;
    private DefaultState mDefaultState = new DefaultState();
    private IntentFilter mIntentFilter;
    private long mLastBssidAvoidedTime;
    private long mLastWalledGardenCheckTime = 0L;
    private LinkProperties mLinkProperties;
    private int mMinArpResponses;
    private int mMinIntervalArrayIndex = 0;
    private NotConnectedState mNotConnectedState = new NotConnectedState();
    private int mNumArpPings;
    private OnlineState mOnlineState = new OnlineState();
    private OnlineWatchState mOnlineWatchState = new OnlineWatchState();
    private boolean mPoorNetworkDetectionEnabled;
    private long mRssiFetchIntervalMs;
    private int mRssiFetchToken = 0;
    private int mRssiMonitorCount = 0;
    private RssiMonitoringState mRssiMonitoringState = new RssiMonitoringState();
    private VerifyingLinkState mVerifyingLinkState = new VerifyingLinkState();
    private WalledGardenCheckState mWalledGardenCheckState = new WalledGardenCheckState();
    private long mWalledGardenIntervalMs;
    private boolean mWalledGardenNotificationShown;
    private boolean mWalledGardenTestEnabled;
    private String mWalledGardenUrl;
    private WatchdogDisabledState mWatchdogDisabledState = new WatchdogDisabledState();
    private WatchdogEnabledState mWatchdogEnabledState = new WatchdogEnabledState();
    private WifiInfo mWifiInfo;
    private WifiManager mWifiManager;
    private AsyncChannel mWsmChannel = new AsyncChannel();

    static
    {
        int[] arrayOfInt = new int[5];
        arrayOfInt[0] = 0;
        arrayOfInt[1] = 30000;
        arrayOfInt[2] = 60000;
        arrayOfInt[3] = 300000;
        arrayOfInt[4] = 1800000;
    }

    private WifiWatchdogStateMachine(Context paramContext)
    {
        super("WifiWatchdogStateMachine");
        this.mContext = paramContext;
        this.mContentResolver = paramContext.getContentResolver();
        this.mWifiManager = ((WifiManager)paramContext.getSystemService("wifi"));
        this.mWsmChannel.connectSync(this.mContext, getHandler(), this.mWifiManager.getWifiStateMachineMessenger());
        setupNetworkReceiver();
        registerForSettingsChanges();
        registerForWatchdogToggle();
        addState(this.mDefaultState);
        addState(this.mWatchdogDisabledState, this.mDefaultState);
        addState(this.mWatchdogEnabledState, this.mDefaultState);
        addState(this.mNotConnectedState, this.mWatchdogEnabledState);
        addState(this.mVerifyingLinkState, this.mWatchdogEnabledState);
        addState(this.mConnectedState, this.mWatchdogEnabledState);
        addState(this.mWalledGardenCheckState, this.mConnectedState);
        addState(this.mOnlineWatchState, this.mConnectedState);
        addState(this.mRssiMonitoringState, this.mOnlineWatchState);
        addState(this.mOnlineState, this.mConnectedState);
        if (isWatchdogEnabled())
            setInitialState(this.mNotConnectedState);
        while (true)
        {
            updateSettings();
            return;
            setInitialState(this.mWatchdogDisabledState);
        }
    }

    private int calculateSignalLevel(int paramInt)
    {
        int i = WifiManager.calculateSignalLevel(paramInt, 5);
        if (DBG)
            log("RSSI current: " + this.mCurrentSignalLevel + "new: " + paramInt + ", " + i);
        return i;
    }

    private boolean doArpTest(int paramInt)
    {
        String str1 = this.mLinkProperties.getInterfaceName();
        String str2 = this.mWifiInfo.getMacAddress();
        InetAddress localInetAddress1 = null;
        InetAddress localInetAddress2 = null;
        Iterator localIterator1 = this.mLinkProperties.getLinkAddresses().iterator();
        if (localIterator1.hasNext())
            localInetAddress1 = ((LinkAddress)localIterator1.next()).getAddress();
        Iterator localIterator2 = this.mLinkProperties.getRoutes().iterator();
        if (localIterator2.hasNext())
            localInetAddress2 = ((RouteInfo)localIterator2.next()).getGateway();
        if (DBG)
            log("ARP " + str1 + "addr: " + localInetAddress1 + "mac: " + str2 + "gw: " + localInetAddress2);
        while (true)
        {
            try
            {
                ArpPeer localArpPeer = new ArpPeer(str1, localInetAddress1, str2, localInetAddress2);
                if (paramInt != 0)
                    break label374;
                if (localArpPeer.doArp(this.mArpPingTimeoutMs) == null)
                    break label368;
                bool = true;
                if (DBG)
                    log("single ARP test result: " + bool);
                localArpPeer.close();
                break label365;
                if (j < this.mNumArpPings)
                {
                    if (localArpPeer.doArp(this.mArpPingTimeoutMs) == null)
                        break label383;
                    i++;
                    break label383;
                }
                if (DBG)
                    log("full ARP test result: " + i + "/" + this.mNumArpPings);
                int k = this.mMinArpResponses;
                if (i >= k)
                {
                    bool = true;
                    continue;
                }
                bool = false;
                continue;
            }
            catch (SocketException localSocketException)
            {
                loge("ARP test initiation failure: " + localSocketException);
                bool = true;
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
                bool = true;
            }
            label365: return bool;
            label368: boolean bool = false;
            continue;
            label374: int i = 0;
            int j = 0;
            continue;
            label383: j++;
        }
    }

    private static boolean getSettingsBoolean(ContentResolver paramContentResolver, String paramString, boolean paramBoolean)
    {
        int i = 1;
        if (paramBoolean)
        {
            int k = i;
            if (Settings.Secure.getInt(paramContentResolver, paramString, k) != i)
                break label28;
        }
        while (true)
        {
            return i;
            int m = 0;
            break;
            label28: int j = 0;
        }
    }

    private static String getSettingsStr(ContentResolver paramContentResolver, String paramString1, String paramString2)
    {
        String str = Settings.Secure.getString(paramContentResolver, paramString1);
        if (str != null);
        while (true)
        {
            return str;
            str = paramString2;
        }
    }

    private boolean isWalledGardenConnection()
    {
        HttpURLConnection localHttpURLConnection = null;
        try
        {
            localHttpURLConnection = (HttpURLConnection)new URL(this.mWalledGardenUrl).openConnection();
            localHttpURLConnection.setInstanceFollowRedirects(false);
            localHttpURLConnection.setConnectTimeout(10000);
            localHttpURLConnection.setReadTimeout(10000);
            localHttpURLConnection.setUseCaches(false);
            localHttpURLConnection.getInputStream();
            int i = localHttpURLConnection.getResponseCode();
            if (i != 204);
            for (bool = true; ; bool = false)
                return bool;
        }
        catch (IOException localIOException)
        {
            while (true)
            {
                if (DBG)
                    log("Walled garden check - probably not a portal: exception " + localIOException);
                if (localHttpURLConnection != null)
                    localHttpURLConnection.disconnect();
                boolean bool = false;
            }
        }
        finally
        {
            if (localHttpURLConnection != null)
                localHttpURLConnection.disconnect();
        }
    }

    private boolean isWatchdogEnabled()
    {
        boolean bool = getSettingsBoolean(this.mContentResolver, "wifi_watchdog_on", true);
        if (DBG)
            log("watchdog enabled " + bool);
        return bool;
    }

    private static void log(String paramString)
    {
        Log.d("WifiWatchdogStateMachine", paramString);
    }

    private static void loge(String paramString)
    {
        Log.e("WifiWatchdogStateMachine", paramString);
    }

    public static WifiWatchdogStateMachine makeWifiWatchdogStateMachine(Context paramContext)
    {
        ContentResolver localContentResolver = paramContext.getContentResolver();
        if (!((ConnectivityManager)paramContext.getSystemService("connectivity")).isNetworkSupported(0));
        for (boolean bool = true; ; bool = false)
        {
            sWifiOnly = bool;
            putSettingsBoolean(localContentResolver, "wifi_watchdog_on", true);
            if (sWifiOnly)
            {
                log("Disabling poor network avoidance for wi-fi only device");
                putSettingsBoolean(localContentResolver, "wifi_watchdog_poor_network_test_enabled", false);
            }
            WifiWatchdogStateMachine localWifiWatchdogStateMachine = new WifiWatchdogStateMachine(paramContext);
            localWifiWatchdogStateMachine.start();
            return localWifiWatchdogStateMachine;
        }
    }

    private static boolean putSettingsBoolean(ContentResolver paramContentResolver, String paramString, boolean paramBoolean)
    {
        if (paramBoolean);
        for (int i = 1; ; i = 0)
            return Settings.Secure.putInt(paramContentResolver, paramString, i);
    }

    private void registerForSettingsChanges()
    {
        ContentObserver local3 = new ContentObserver(getHandler())
        {
            public void onChange(boolean paramAnonymousBoolean)
            {
                WifiWatchdogStateMachine.this.sendMessage(135174);
            }
        };
        this.mContext.getContentResolver().registerContentObserver(Settings.Secure.getUriFor("wifi_watchdog_arp_interval_ms"), false, local3);
        this.mContext.getContentResolver().registerContentObserver(Settings.Secure.getUriFor("wifi_watchdog_walled_garden_interval_ms"), false, local3);
        this.mContext.getContentResolver().registerContentObserver(Settings.Secure.getUriFor("wifi_watchdog_num_arp_pings"), false, local3);
        this.mContext.getContentResolver().registerContentObserver(Settings.Secure.getUriFor("wifi_watchdog_min_arp_responses"), false, local3);
        this.mContext.getContentResolver().registerContentObserver(Settings.Secure.getUriFor("wifi_watchdog_arp_ping_timeout_ms"), false, local3);
        this.mContext.getContentResolver().registerContentObserver(Settings.Secure.getUriFor("wifi_watchdog_poor_network_test_enabled"), false, local3);
        this.mContext.getContentResolver().registerContentObserver(Settings.Secure.getUriFor("wifi_watchdog_walled_garden_test_enabled"), false, local3);
        this.mContext.getContentResolver().registerContentObserver(Settings.Secure.getUriFor("wifi_watchdog_walled_garden_url"), false, local3);
    }

    private void registerForWatchdogToggle()
    {
        ContentObserver local2 = new ContentObserver(getHandler())
        {
            public void onChange(boolean paramAnonymousBoolean)
            {
                WifiWatchdogStateMachine.this.sendMessage(135169);
            }
        };
        this.mContext.getContentResolver().registerContentObserver(Settings.Secure.getUriFor("wifi_watchdog_on"), false, local2);
    }

    private void sendPoorLinkDetected()
    {
        if (DBG)
            log("send POOR_LINK_DETECTED " + this.mWifiInfo);
        this.mWsmChannel.sendMessage(135189);
        if (SystemClock.elapsedRealtime() - this.mLastBssidAvoidedTime > MIN_INTERVAL_AVOID_BSSID_MS[(-1 + MIN_INTERVAL_AVOID_BSSID_MS.length)])
        {
            this.mMinIntervalArrayIndex = 1;
            if (DBG)
                log("set mMinIntervalArrayIndex to 1");
        }
        while (true)
        {
            this.mLastBssidAvoidedTime = SystemClock.elapsedRealtime();
            return;
            if (this.mMinIntervalArrayIndex < -1 + MIN_INTERVAL_AVOID_BSSID_MS.length)
                this.mMinIntervalArrayIndex = (1 + this.mMinIntervalArrayIndex);
            if (DBG)
                log("mMinIntervalArrayIndex: " + this.mMinIntervalArrayIndex);
        }
    }

    private void setWalledGardenNotificationVisible(boolean paramBoolean)
    {
        if ((!paramBoolean) && (!this.mWalledGardenNotificationShown))
            return;
        Resources localResources = Resources.getSystem();
        NotificationManager localNotificationManager = (NotificationManager)this.mContext.getSystemService("notification");
        if (paramBoolean)
        {
            Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse(this.mWalledGardenUrl));
            localIntent.setFlags(272629760);
            Object[] arrayOfObject1 = new Object[1];
            arrayOfObject1[0] = Integer.valueOf(0);
            String str1 = localResources.getString(17040387, arrayOfObject1);
            Object[] arrayOfObject2 = new Object[1];
            arrayOfObject2[0] = this.mWifiInfo.getSSID();
            String str2 = localResources.getString(17040388, arrayOfObject2);
            Notification localNotification = new Notification();
            localNotification.when = 0L;
            localNotification.icon = 17302807;
            localNotification.flags = 16;
            localNotification.contentIntent = PendingIntent.getActivity(this.mContext, 0, localIntent, 0);
            localNotification.tickerText = str1;
            localNotification.setLatestEventInfo(this.mContext, str1, str2, localNotification.contentIntent);
            localNotificationManager.notify("WifiWatchdog.walledgarden", 1, localNotification);
        }
        while (true)
        {
            this.mWalledGardenNotificationShown = paramBoolean;
            break;
            localNotificationManager.cancel("WifiWatchdog.walledgarden", 1);
        }
    }

    private void setupNetworkReceiver()
    {
        this.mBroadcastReceiver = new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                String str = paramAnonymousIntent.getAction();
                if (str.equals("android.net.wifi.STATE_CHANGE"))
                    WifiWatchdogStateMachine.this.sendMessage(135170, paramAnonymousIntent);
                while (true)
                {
                    return;
                    if (str.equals("android.net.wifi.RSSI_CHANGED"))
                        WifiWatchdogStateMachine.this.obtainMessage(135171, paramAnonymousIntent.getIntExtra("newRssi", -200), 0).sendToTarget();
                    else if (str.equals("android.net.wifi.WIFI_STATE_CHANGED"))
                        WifiWatchdogStateMachine.this.sendMessage(135173, Integer.valueOf(paramAnonymousIntent.getIntExtra("wifi_state", 4)));
                }
            }
        };
        this.mIntentFilter = new IntentFilter();
        this.mIntentFilter.addAction("android.net.wifi.STATE_CHANGE");
        this.mIntentFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        this.mIntentFilter.addAction("android.net.wifi.RSSI_CHANGED");
        this.mContext.registerReceiver(this.mBroadcastReceiver, this.mIntentFilter);
    }

    private boolean shouldCheckWalledGarden()
    {
        boolean bool = false;
        if (!this.mWalledGardenTestEnabled)
            if (DBG)
                log("Skipping walled garden check - disabled");
        while (true)
        {
            return bool;
            long l = this.mWalledGardenIntervalMs + this.mLastWalledGardenCheckTime - SystemClock.elapsedRealtime();
            if ((this.mLastWalledGardenCheckTime != 0L) && (l > 0L))
            {
                if (DBG)
                    log("Skipping walled garden check - wait " + l + " ms.");
            }
            else
                bool = true;
        }
    }

    private void updateSettings()
    {
        if (DBG)
            log("Updating secure settings");
        this.mArpCheckIntervalMs = Settings.Secure.getLong(this.mContentResolver, "wifi_watchdog_arp_interval_ms", 120000L);
        this.mRssiFetchIntervalMs = Settings.Secure.getLong(this.mContentResolver, "wifi_watchdog_rssi_fetch_interval_ms", 1000L);
        this.mNumArpPings = Settings.Secure.getInt(this.mContentResolver, "wifi_watchdog_num_arp_pings", 5);
        this.mMinArpResponses = Settings.Secure.getInt(this.mContentResolver, "wifi_watchdog_min_arp_responses", 1);
        this.mArpPingTimeoutMs = Settings.Secure.getInt(this.mContentResolver, "wifi_watchdog_arp_ping_timeout_ms", 100);
        this.mPoorNetworkDetectionEnabled = getSettingsBoolean(this.mContentResolver, "wifi_watchdog_poor_network_test_enabled", true);
        this.mWalledGardenTestEnabled = getSettingsBoolean(this.mContentResolver, "wifi_watchdog_walled_garden_test_enabled", true);
        this.mWalledGardenUrl = getSettingsStr(this.mContentResolver, "wifi_watchdog_walled_garden_url", "http://clients3.google.com/generate_204");
        this.mWalledGardenIntervalMs = Settings.Secure.getLong(this.mContentResolver, "wifi_watchdog_walled_garden_interval_ms", 1800000L);
    }

    public void dump(PrintWriter paramPrintWriter)
    {
        paramPrintWriter.print("WatchdogStatus: ");
        paramPrintWriter.print("State: " + getCurrentState());
        paramPrintWriter.println("mWifiInfo: [" + this.mWifiInfo + "]");
        paramPrintWriter.println("mLinkProperties: [" + this.mLinkProperties + "]");
        paramPrintWriter.println("mCurrentSignalLevel: [" + this.mCurrentSignalLevel + "]");
        paramPrintWriter.println("mArpCheckIntervalMs: [" + this.mArpCheckIntervalMs + "]");
        paramPrintWriter.println("mRssiFetchIntervalMs: [" + this.mRssiFetchIntervalMs + "]");
        paramPrintWriter.println("mWalledGardenIntervalMs: [" + this.mWalledGardenIntervalMs + "]");
        paramPrintWriter.println("mNumArpPings: [" + this.mNumArpPings + "]");
        paramPrintWriter.println("mMinArpResponses: [" + this.mMinArpResponses + "]");
        paramPrintWriter.println("mArpPingTimeoutMs: [" + this.mArpPingTimeoutMs + "]");
        paramPrintWriter.println("mPoorNetworkDetectionEnabled: [" + this.mPoorNetworkDetectionEnabled + "]");
        paramPrintWriter.println("mWalledGardenTestEnabled: [" + this.mWalledGardenTestEnabled + "]");
        paramPrintWriter.println("mWalledGardenUrl: [" + this.mWalledGardenUrl + "]");
    }

    class OnlineState extends State
    {
        OnlineState()
        {
        }

        public void enter()
        {
            if (WifiWatchdogStateMachine.DBG)
                WifiWatchdogStateMachine.log(getName() + "\n");
        }
    }

    class RssiMonitoringState extends State
    {
        RssiMonitoringState()
        {
        }

        public void enter()
        {
            if (WifiWatchdogStateMachine.DBG)
                WifiWatchdogStateMachine.log(getName() + "\n");
            WifiWatchdogStateMachine.this.sendMessage(WifiWatchdogStateMachine.this.obtainMessage(135181, WifiWatchdogStateMachine.access$4004(WifiWatchdogStateMachine.this), 0));
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool = false;
            switch (paramMessage.what)
            {
            default:
                return bool;
            case 135171:
                WifiWatchdogStateMachine.access$302(WifiWatchdogStateMachine.this, WifiWatchdogStateMachine.this.calculateSignalLevel(paramMessage.arg1));
                if (WifiWatchdogStateMachine.this.mCurrentSignalLevel > 0)
                    break;
            case 135181:
            case 135192:
            case 135193:
            }
            while (true)
            {
                bool = true;
                break;
                WifiWatchdogStateMachine.this.transitionTo(WifiWatchdogStateMachine.this.mOnlineWatchState);
                continue;
                if (paramMessage.arg1 == WifiWatchdogStateMachine.this.mRssiFetchToken)
                {
                    WifiWatchdogStateMachine.this.mWsmChannel.sendMessage(135191);
                    WifiWatchdogStateMachine.this.sendMessageDelayed(WifiWatchdogStateMachine.this.obtainMessage(135181, WifiWatchdogStateMachine.access$4004(WifiWatchdogStateMachine.this), 0), WifiWatchdogStateMachine.this.mRssiFetchIntervalMs);
                    continue;
                    int i = paramMessage.arg1;
                    if (WifiWatchdogStateMachine.DBG)
                        WifiWatchdogStateMachine.log("RSSI_FETCH_SUCCEEDED: " + i);
                    if (paramMessage.arg1 < -88)
                        WifiWatchdogStateMachine.access$4308(WifiWatchdogStateMachine.this);
                    while (WifiWatchdogStateMachine.this.mRssiMonitorCount > 5)
                    {
                        WifiWatchdogStateMachine.this.sendPoorLinkDetected();
                        WifiWatchdogStateMachine.access$4004(WifiWatchdogStateMachine.this);
                        break;
                        WifiWatchdogStateMachine.access$4302(WifiWatchdogStateMachine.this, 0);
                    }
                    if (WifiWatchdogStateMachine.DBG)
                        WifiWatchdogStateMachine.log("RSSI_FETCH_FAILED");
                }
            }
        }
    }

    class OnlineWatchState extends State
    {
        OnlineWatchState()
        {
        }

        private void handleRssiChange()
        {
            if (WifiWatchdogStateMachine.this.mCurrentSignalLevel <= 0)
                WifiWatchdogStateMachine.this.transitionTo(WifiWatchdogStateMachine.this.mRssiMonitoringState);
        }

        public void enter()
        {
            if (WifiWatchdogStateMachine.DBG)
                WifiWatchdogStateMachine.log(getName() + "\n");
            if (WifiWatchdogStateMachine.this.mPoorNetworkDetectionEnabled)
                handleRssiChange();
            while (true)
            {
                return;
                WifiWatchdogStateMachine.this.transitionTo(WifiWatchdogStateMachine.this.mOnlineState);
            }
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool;
            switch (paramMessage.what)
            {
            default:
                bool = false;
                return bool;
            case 135171:
            }
            WifiWatchdogStateMachine.access$302(WifiWatchdogStateMachine.this, WifiWatchdogStateMachine.this.calculateSignalLevel(paramMessage.arg1));
            long l = SystemClock.elapsedRealtime();
            if (l - WifiWatchdogStateMachine.this.mLastBssidAvoidedTime > WifiWatchdogStateMachine.MIN_INTERVAL_AVOID_BSSID_MS[WifiWatchdogStateMachine.this.mMinIntervalArrayIndex])
                handleRssiChange();
            while (true)
            {
                bool = true;
                break;
                if (WifiWatchdogStateMachine.DBG)
                    WifiWatchdogStateMachine.log("Early to avoid " + WifiWatchdogStateMachine.this.mWifiInfo + " time: " + l + " last avoided: " + WifiWatchdogStateMachine.this.mLastBssidAvoidedTime + " mMinIntervalArrayIndex: " + WifiWatchdogStateMachine.this.mMinIntervalArrayIndex);
            }
        }
    }

    class WalledGardenCheckState extends State
    {
        private int mWalledGardenToken = 0;

        WalledGardenCheckState()
        {
        }

        public void enter()
        {
            if (WifiWatchdogStateMachine.DBG)
                WifiWatchdogStateMachine.log(getName() + "\n");
            WifiWatchdogStateMachine localWifiWatchdogStateMachine1 = WifiWatchdogStateMachine.this;
            WifiWatchdogStateMachine localWifiWatchdogStateMachine2 = WifiWatchdogStateMachine.this;
            int i = 1 + this.mWalledGardenToken;
            this.mWalledGardenToken = i;
            localWifiWatchdogStateMachine1.sendMessageDelayed(localWifiWatchdogStateMachine2.obtainMessage(135180, i, 0), 3000L);
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool = true;
            switch (paramMessage.what)
            {
            default:
                bool = false;
            case 135180:
            }
            while (true)
            {
                return bool;
                if (paramMessage.arg1 == this.mWalledGardenToken)
                {
                    WifiWatchdogStateMachine.access$3102(WifiWatchdogStateMachine.this, SystemClock.elapsedRealtime());
                    if (WifiWatchdogStateMachine.this.isWalledGardenConnection())
                    {
                        if (WifiWatchdogStateMachine.DBG)
                            WifiWatchdogStateMachine.log("Walled garden detected");
                        WifiWatchdogStateMachine.this.setWalledGardenNotificationVisible(bool);
                    }
                    WifiWatchdogStateMachine.this.transitionTo(WifiWatchdogStateMachine.this.mOnlineWatchState);
                }
            }
        }
    }

    class ConnectedState extends State
    {
        ConnectedState()
        {
        }

        public void enter()
        {
            if (WifiWatchdogStateMachine.DBG)
                WifiWatchdogStateMachine.log(getName() + "\n");
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool = true;
            switch (paramMessage.what)
            {
            default:
                bool = false;
            case 135174:
            }
            while (true)
            {
                return bool;
                WifiWatchdogStateMachine.this.updateSettings();
                WifiWatchdogStateMachine.access$002(bool);
                if (WifiWatchdogStateMachine.DBG)
                    WifiWatchdogStateMachine.log("Updated secure settings and turned debug on");
                if (WifiWatchdogStateMachine.this.mPoorNetworkDetectionEnabled)
                    WifiWatchdogStateMachine.this.transitionTo(WifiWatchdogStateMachine.this.mOnlineWatchState);
                else
                    WifiWatchdogStateMachine.this.transitionTo(WifiWatchdogStateMachine.this.mOnlineState);
            }
        }
    }

    class VerifyingLinkState extends State
    {
        VerifyingLinkState()
        {
        }

        private void handleRssiChange()
        {
            if (WifiWatchdogStateMachine.this.mCurrentSignalLevel <= 0)
                if (WifiWatchdogStateMachine.DBG)
                    WifiWatchdogStateMachine.log("enter VerifyingLinkState, stay level: " + WifiWatchdogStateMachine.this.mCurrentSignalLevel);
            while (true)
            {
                return;
                if (WifiWatchdogStateMachine.DBG)
                    WifiWatchdogStateMachine.log("enter VerifyingLinkState, arp check level: " + WifiWatchdogStateMachine.this.mCurrentSignalLevel);
                WifiWatchdogStateMachine.this.sendMessage(WifiWatchdogStateMachine.this.obtainMessage(135179, WifiWatchdogStateMachine.access$2504(WifiWatchdogStateMachine.this), 0));
            }
        }

        public void enter()
        {
            if (WifiWatchdogStateMachine.DBG)
                WifiWatchdogStateMachine.log(getName() + "\n");
            handleRssiChange();
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool = false;
            switch (paramMessage.what)
            {
            default:
                return bool;
            case 135174:
                WifiWatchdogStateMachine.this.updateSettings();
                if (!WifiWatchdogStateMachine.this.mPoorNetworkDetectionEnabled)
                    WifiWatchdogStateMachine.this.mWsmChannel.sendMessage(135190);
                break;
            case 135171:
            case 135179:
            }
            while (true)
            {
                bool = true;
                break;
                WifiWatchdogStateMachine.access$302(WifiWatchdogStateMachine.this, WifiWatchdogStateMachine.this.calculateSignalLevel(paramMessage.arg1));
                handleRssiChange();
                continue;
                if (paramMessage.arg1 == WifiWatchdogStateMachine.this.mArpToken)
                    if (WifiWatchdogStateMachine.this.doArpTest(1) == true)
                    {
                        if (WifiWatchdogStateMachine.DBG)
                            WifiWatchdogStateMachine.log("Notify link is good " + WifiWatchdogStateMachine.this.mCurrentSignalLevel);
                        WifiWatchdogStateMachine.this.mWsmChannel.sendMessage(135190);
                    }
                    else
                    {
                        if (WifiWatchdogStateMachine.DBG)
                            WifiWatchdogStateMachine.log("Continue ARP check, rssi level: " + WifiWatchdogStateMachine.this.mCurrentSignalLevel);
                        WifiWatchdogStateMachine.this.sendMessageDelayed(WifiWatchdogStateMachine.this.obtainMessage(135179, WifiWatchdogStateMachine.access$2504(WifiWatchdogStateMachine.this), 0), WifiWatchdogStateMachine.this.mArpCheckIntervalMs);
                    }
            }
        }
    }

    class NotConnectedState extends State
    {
        NotConnectedState()
        {
        }

        public void enter()
        {
            if (WifiWatchdogStateMachine.DBG)
                WifiWatchdogStateMachine.log(getName() + "\n");
        }
    }

    class WatchdogEnabledState extends State
    {
        WatchdogEnabledState()
        {
        }

        public void enter()
        {
            if (WifiWatchdogStateMachine.DBG)
                WifiWatchdogStateMachine.log("WifiWatchdogService enabled");
        }

        public void exit()
        {
            if (WifiWatchdogStateMachine.DBG)
                WifiWatchdogStateMachine.log("WifiWatchdogService disabled");
        }

        public boolean processMessage(Message paramMessage)
        {
            boolean bool;
            switch (paramMessage.what)
            {
            case 135171:
            case 135172:
            default:
                bool = false;
                return bool;
            case 135169:
                if (!WifiWatchdogStateMachine.this.isWatchdogEnabled())
                    WifiWatchdogStateMachine.this.transitionTo(WifiWatchdogStateMachine.this.mWatchdogDisabledState);
                break;
            case 135170:
            case 135173:
            }
            while (true)
            {
                WifiWatchdogStateMachine.this.setWalledGardenNotificationVisible(false);
                bool = true;
                break;
                Intent localIntent = (Intent)paramMessage.obj;
                NetworkInfo localNetworkInfo = (NetworkInfo)localIntent.getParcelableExtra("networkInfo");
                if (WifiWatchdogStateMachine.DBG)
                    WifiWatchdogStateMachine.log("network state change " + localNetworkInfo.getDetailedState());
                switch (WifiWatchdogStateMachine.4.$SwitchMap$android$net$NetworkInfo$DetailedState[localNetworkInfo.getDetailedState().ordinal()])
                {
                default:
                    WifiWatchdogStateMachine.this.transitionTo(WifiWatchdogStateMachine.this.mNotConnectedState);
                    break;
                case 1:
                    WifiWatchdogStateMachine.access$1202(WifiWatchdogStateMachine.this, (LinkProperties)localIntent.getParcelableExtra("linkProperties"));
                    WifiWatchdogStateMachine.access$1302(WifiWatchdogStateMachine.this, (WifiInfo)localIntent.getParcelableExtra("wifiInfo"));
                    if (WifiWatchdogStateMachine.this.mPoorNetworkDetectionEnabled)
                    {
                        if (WifiWatchdogStateMachine.this.mWifiInfo == null)
                        {
                            WifiWatchdogStateMachine.log("Ignoring link verification, mWifiInfo is NULL");
                            WifiWatchdogStateMachine.this.mWsmChannel.sendMessage(135190);
                        }
                        else
                        {
                            WifiWatchdogStateMachine.this.transitionTo(WifiWatchdogStateMachine.this.mVerifyingLinkState);
                        }
                    }
                    else
                        WifiWatchdogStateMachine.this.mWsmChannel.sendMessage(135190);
                    break;
                case 2:
                    if (WifiWatchdogStateMachine.this.shouldCheckWalledGarden())
                    {
                        WifiWatchdogStateMachine.this.transitionTo(WifiWatchdogStateMachine.this.mWalledGardenCheckState);
                    }
                    else
                    {
                        WifiWatchdogStateMachine.this.transitionTo(WifiWatchdogStateMachine.this.mOnlineWatchState);
                        continue;
                        if (((Integer)paramMessage.obj).intValue() == 0)
                        {
                            if (WifiWatchdogStateMachine.DBG)
                                WifiWatchdogStateMachine.log("WifiStateDisabling -- Resetting WatchdogState");
                            WifiWatchdogStateMachine.this.transitionTo(WifiWatchdogStateMachine.this.mNotConnectedState);
                        }
                    }
                    break;
                }
            }
        }
    }

    class WatchdogDisabledState extends State
    {
        WatchdogDisabledState()
        {
        }

        public void enter()
        {
            if (WifiWatchdogStateMachine.DBG)
                WifiWatchdogStateMachine.log(getName() + "\n");
        }

        public boolean processMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
            case 135169:
            case 135170:
            }
            while (true)
            {
                for (boolean bool = false; ; bool = true)
                {
                    return bool;
                    if (WifiWatchdogStateMachine.this.isWatchdogEnabled())
                        WifiWatchdogStateMachine.this.transitionTo(WifiWatchdogStateMachine.this.mNotConnectedState);
                }
                NetworkInfo localNetworkInfo = (NetworkInfo)((Intent)paramMessage.obj).getParcelableExtra("networkInfo");
                switch (WifiWatchdogStateMachine.4.$SwitchMap$android$net$NetworkInfo$DetailedState[localNetworkInfo.getDetailedState().ordinal()])
                {
                default:
                    break;
                case 1:
                    if (WifiWatchdogStateMachine.DBG)
                        WifiWatchdogStateMachine.log("Watchdog disabled, verify link");
                    WifiWatchdogStateMachine.this.mWsmChannel.sendMessage(135190);
                }
            }
        }
    }

    class DefaultState extends State
    {
        DefaultState()
        {
        }

        public void enter()
        {
            if (WifiWatchdogStateMachine.DBG)
                WifiWatchdogStateMachine.log(getName() + "\n");
        }

        public boolean processMessage(Message paramMessage)
        {
            switch (paramMessage.what)
            {
            default:
                WifiWatchdogStateMachine.log("Unhandled message " + paramMessage + " in state " + WifiWatchdogStateMachine.access$500(WifiWatchdogStateMachine.this).getName());
            case 135170:
            case 135173:
            case 135179:
            case 135180:
            case 135181:
            case 135192:
            case 135193:
            case 135174:
            case 135171:
            }
            while (true)
            {
                return true;
                WifiWatchdogStateMachine.this.updateSettings();
                if (WifiWatchdogStateMachine.DBG)
                {
                    WifiWatchdogStateMachine.log("Updating wifi-watchdog secure settings");
                    continue;
                    WifiWatchdogStateMachine.access$302(WifiWatchdogStateMachine.this, WifiWatchdogStateMachine.this.calculateSignalLevel(paramMessage.arg1));
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.WifiWatchdogStateMachine
 * JD-Core Version:        0.6.2
 */