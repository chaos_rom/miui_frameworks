package android.net.wifi;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public enum SupplicantState
    implements Parcelable
{
    public static final Parcelable.Creator<SupplicantState> CREATOR = new Parcelable.Creator()
    {
        public SupplicantState createFromParcel(Parcel paramAnonymousParcel)
        {
            return SupplicantState.valueOf(paramAnonymousParcel.readString());
        }

        public SupplicantState[] newArray(int paramAnonymousInt)
        {
            return new SupplicantState[paramAnonymousInt];
        }
    };

    static
    {
        INACTIVE = new SupplicantState("INACTIVE", 2);
        SCANNING = new SupplicantState("SCANNING", 3);
        AUTHENTICATING = new SupplicantState("AUTHENTICATING", 4);
        ASSOCIATING = new SupplicantState("ASSOCIATING", 5);
        ASSOCIATED = new SupplicantState("ASSOCIATED", 6);
        FOUR_WAY_HANDSHAKE = new SupplicantState("FOUR_WAY_HANDSHAKE", 7);
        GROUP_HANDSHAKE = new SupplicantState("GROUP_HANDSHAKE", 8);
        COMPLETED = new SupplicantState("COMPLETED", 9);
        DORMANT = new SupplicantState("DORMANT", 10);
        UNINITIALIZED = new SupplicantState("UNINITIALIZED", 11);
        INVALID = new SupplicantState("INVALID", 12);
        SupplicantState[] arrayOfSupplicantState = new SupplicantState[13];
        arrayOfSupplicantState[0] = DISCONNECTED;
        arrayOfSupplicantState[1] = INTERFACE_DISABLED;
        arrayOfSupplicantState[2] = INACTIVE;
        arrayOfSupplicantState[3] = SCANNING;
        arrayOfSupplicantState[4] = AUTHENTICATING;
        arrayOfSupplicantState[5] = ASSOCIATING;
        arrayOfSupplicantState[6] = ASSOCIATED;
        arrayOfSupplicantState[7] = FOUR_WAY_HANDSHAKE;
        arrayOfSupplicantState[8] = GROUP_HANDSHAKE;
        arrayOfSupplicantState[9] = COMPLETED;
        arrayOfSupplicantState[10] = DORMANT;
        arrayOfSupplicantState[11] = UNINITIALIZED;
        arrayOfSupplicantState[12] = INVALID;
    }

    static boolean isConnecting(SupplicantState paramSupplicantState)
    {
        switch (2.$SwitchMap$android$net$wifi$SupplicantState[paramSupplicantState.ordinal()])
        {
        default:
            throw new IllegalArgumentException("Unknown supplicant state");
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
        case 13:
        }
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    static boolean isDriverActive(SupplicantState paramSupplicantState)
    {
        switch (2.$SwitchMap$android$net$wifi$SupplicantState[paramSupplicantState.ordinal()])
        {
        default:
            throw new IllegalArgumentException("Unknown supplicant state");
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 9:
        case 10:
        case 11:
        case 8:
        case 12:
        case 13:
        }
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isHandshakeState(SupplicantState paramSupplicantState)
    {
        switch (2.$SwitchMap$android$net$wifi$SupplicantState[paramSupplicantState.ordinal()])
        {
        default:
            throw new IllegalArgumentException("Unknown supplicant state");
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
        case 13:
        }
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public static boolean isValidState(SupplicantState paramSupplicantState)
    {
        if ((paramSupplicantState != UNINITIALIZED) && (paramSupplicantState != INVALID));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(name());
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.wifi.SupplicantState
 * JD-Core Version:        0.6.2
 */