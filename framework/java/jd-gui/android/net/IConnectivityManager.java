package android.net;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import com.android.internal.net.LegacyVpnInfo;
import com.android.internal.net.VpnConfig;

public abstract interface IConnectivityManager extends IInterface
{
    public abstract ParcelFileDescriptor establishVpn(VpnConfig paramVpnConfig)
        throws RemoteException;

    public abstract LinkProperties getActiveLinkProperties()
        throws RemoteException;

    public abstract NetworkInfo getActiveNetworkInfo()
        throws RemoteException;

    public abstract NetworkInfo getActiveNetworkInfoForUid(int paramInt)
        throws RemoteException;

    public abstract NetworkQuotaInfo getActiveNetworkQuotaInfo()
        throws RemoteException;

    public abstract NetworkInfo[] getAllNetworkInfo()
        throws RemoteException;

    public abstract NetworkState[] getAllNetworkState()
        throws RemoteException;

    public abstract ProxyProperties getGlobalProxy()
        throws RemoteException;

    public abstract int getLastTetherError(String paramString)
        throws RemoteException;

    public abstract LegacyVpnInfo getLegacyVpnInfo()
        throws RemoteException;

    public abstract LinkProperties getLinkProperties(int paramInt)
        throws RemoteException;

    public abstract boolean getMobileDataEnabled()
        throws RemoteException;

    public abstract NetworkInfo getNetworkInfo(int paramInt)
        throws RemoteException;

    public abstract int getNetworkPreference()
        throws RemoteException;

    public abstract ProxyProperties getProxy()
        throws RemoteException;

    public abstract String[] getTetherableBluetoothRegexs()
        throws RemoteException;

    public abstract String[] getTetherableIfaces()
        throws RemoteException;

    public abstract String[] getTetherableUsbRegexs()
        throws RemoteException;

    public abstract String[] getTetherableWifiRegexs()
        throws RemoteException;

    public abstract String[] getTetheredIfacePairs()
        throws RemoteException;

    public abstract String[] getTetheredIfaces()
        throws RemoteException;

    public abstract String[] getTetheringErroredIfaces()
        throws RemoteException;

    public abstract boolean isActiveNetworkMetered()
        throws RemoteException;

    public abstract boolean isNetworkSupported(int paramInt)
        throws RemoteException;

    public abstract boolean isTetheringSupported()
        throws RemoteException;

    public abstract boolean prepareVpn(String paramString1, String paramString2)
        throws RemoteException;

    public abstract boolean protectVpn(ParcelFileDescriptor paramParcelFileDescriptor)
        throws RemoteException;

    public abstract void reportInetCondition(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void requestNetworkTransitionWakelock(String paramString)
        throws RemoteException;

    public abstract boolean requestRouteToHost(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract boolean requestRouteToHostAddress(int paramInt, byte[] paramArrayOfByte)
        throws RemoteException;

    public abstract void setDataDependency(int paramInt, boolean paramBoolean)
        throws RemoteException;

    public abstract void setGlobalProxy(ProxyProperties paramProxyProperties)
        throws RemoteException;

    public abstract void setMobileDataEnabled(boolean paramBoolean)
        throws RemoteException;

    public abstract void setNetworkPreference(int paramInt)
        throws RemoteException;

    public abstract void setPolicyDataEnable(int paramInt, boolean paramBoolean)
        throws RemoteException;

    public abstract boolean setRadio(int paramInt, boolean paramBoolean)
        throws RemoteException;

    public abstract boolean setRadios(boolean paramBoolean)
        throws RemoteException;

    public abstract int setUsbTethering(boolean paramBoolean)
        throws RemoteException;

    public abstract void startLegacyVpn(VpnConfig paramVpnConfig, String[] paramArrayOfString1, String[] paramArrayOfString2)
        throws RemoteException;

    public abstract int startUsingNetworkFeature(int paramInt, String paramString, IBinder paramIBinder)
        throws RemoteException;

    public abstract int stopUsingNetworkFeature(int paramInt, String paramString)
        throws RemoteException;

    public abstract int tether(String paramString)
        throws RemoteException;

    public abstract int untether(String paramString)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IConnectivityManager
    {
        private static final String DESCRIPTOR = "android.net.IConnectivityManager";
        static final int TRANSACTION_establishVpn = 42;
        static final int TRANSACTION_getActiveLinkProperties = 8;
        static final int TRANSACTION_getActiveNetworkInfo = 3;
        static final int TRANSACTION_getActiveNetworkInfoForUid = 4;
        static final int TRANSACTION_getActiveNetworkQuotaInfo = 11;
        static final int TRANSACTION_getAllNetworkInfo = 6;
        static final int TRANSACTION_getAllNetworkState = 10;
        static final int TRANSACTION_getGlobalProxy = 36;
        static final int TRANSACTION_getLastTetherError = 24;
        static final int TRANSACTION_getLegacyVpnInfo = 44;
        static final int TRANSACTION_getLinkProperties = 9;
        static final int TRANSACTION_getMobileDataEnabled = 19;
        static final int TRANSACTION_getNetworkInfo = 5;
        static final int TRANSACTION_getNetworkPreference = 2;
        static final int TRANSACTION_getProxy = 38;
        static final int TRANSACTION_getTetherableBluetoothRegexs = 32;
        static final int TRANSACTION_getTetherableIfaces = 26;
        static final int TRANSACTION_getTetherableUsbRegexs = 30;
        static final int TRANSACTION_getTetherableWifiRegexs = 31;
        static final int TRANSACTION_getTetheredIfacePairs = 28;
        static final int TRANSACTION_getTetheredIfaces = 27;
        static final int TRANSACTION_getTetheringErroredIfaces = 29;
        static final int TRANSACTION_isActiveNetworkMetered = 12;
        static final int TRANSACTION_isNetworkSupported = 7;
        static final int TRANSACTION_isTetheringSupported = 25;
        static final int TRANSACTION_prepareVpn = 41;
        static final int TRANSACTION_protectVpn = 40;
        static final int TRANSACTION_reportInetCondition = 35;
        static final int TRANSACTION_requestNetworkTransitionWakelock = 34;
        static final int TRANSACTION_requestRouteToHost = 17;
        static final int TRANSACTION_requestRouteToHostAddress = 18;
        static final int TRANSACTION_setDataDependency = 39;
        static final int TRANSACTION_setGlobalProxy = 37;
        static final int TRANSACTION_setMobileDataEnabled = 20;
        static final int TRANSACTION_setNetworkPreference = 1;
        static final int TRANSACTION_setPolicyDataEnable = 21;
        static final int TRANSACTION_setRadio = 14;
        static final int TRANSACTION_setRadios = 13;
        static final int TRANSACTION_setUsbTethering = 33;
        static final int TRANSACTION_startLegacyVpn = 43;
        static final int TRANSACTION_startUsingNetworkFeature = 15;
        static final int TRANSACTION_stopUsingNetworkFeature = 16;
        static final int TRANSACTION_tether = 22;
        static final int TRANSACTION_untether = 23;

        public Stub()
        {
            attachInterface(this, "android.net.IConnectivityManager");
        }

        public static IConnectivityManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.net.IConnectivityManager");
                if ((localIInterface != null) && ((localIInterface instanceof IConnectivityManager)))
                    localObject = (IConnectivityManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            }
            while (true)
            {
                return j;
                paramParcel2.writeString("android.net.IConnectivityManager");
                continue;
                paramParcel1.enforceInterface("android.net.IConnectivityManager");
                setNetworkPreference(paramParcel1.readInt());
                paramParcel2.writeNoException();
                continue;
                paramParcel1.enforceInterface("android.net.IConnectivityManager");
                int i19 = getNetworkPreference();
                paramParcel2.writeNoException();
                paramParcel2.writeInt(i19);
                continue;
                paramParcel1.enforceInterface("android.net.IConnectivityManager");
                NetworkInfo localNetworkInfo3 = getActiveNetworkInfo();
                paramParcel2.writeNoException();
                if (localNetworkInfo3 != null)
                {
                    paramParcel2.writeInt(j);
                    localNetworkInfo3.writeToParcel(paramParcel2, j);
                }
                else
                {
                    paramParcel2.writeInt(0);
                    continue;
                    paramParcel1.enforceInterface("android.net.IConnectivityManager");
                    NetworkInfo localNetworkInfo2 = getActiveNetworkInfoForUid(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    if (localNetworkInfo2 != null)
                    {
                        paramParcel2.writeInt(j);
                        localNetworkInfo2.writeToParcel(paramParcel2, j);
                    }
                    else
                    {
                        paramParcel2.writeInt(0);
                        continue;
                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                        NetworkInfo localNetworkInfo1 = getNetworkInfo(paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        if (localNetworkInfo1 != null)
                        {
                            paramParcel2.writeInt(j);
                            localNetworkInfo1.writeToParcel(paramParcel2, j);
                        }
                        else
                        {
                            paramParcel2.writeInt(0);
                            continue;
                            paramParcel1.enforceInterface("android.net.IConnectivityManager");
                            NetworkInfo[] arrayOfNetworkInfo = getAllNetworkInfo();
                            paramParcel2.writeNoException();
                            paramParcel2.writeTypedArray(arrayOfNetworkInfo, j);
                            continue;
                            paramParcel1.enforceInterface("android.net.IConnectivityManager");
                            boolean bool10 = isNetworkSupported(paramParcel1.readInt());
                            paramParcel2.writeNoException();
                            if (bool10)
                                i = j;
                            paramParcel2.writeInt(i);
                            continue;
                            paramParcel1.enforceInterface("android.net.IConnectivityManager");
                            LinkProperties localLinkProperties2 = getActiveLinkProperties();
                            paramParcel2.writeNoException();
                            if (localLinkProperties2 != null)
                            {
                                paramParcel2.writeInt(j);
                                localLinkProperties2.writeToParcel(paramParcel2, j);
                            }
                            else
                            {
                                paramParcel2.writeInt(0);
                                continue;
                                paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                LinkProperties localLinkProperties1 = getLinkProperties(paramParcel1.readInt());
                                paramParcel2.writeNoException();
                                if (localLinkProperties1 != null)
                                {
                                    paramParcel2.writeInt(j);
                                    localLinkProperties1.writeToParcel(paramParcel2, j);
                                }
                                else
                                {
                                    paramParcel2.writeInt(0);
                                    continue;
                                    paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                    NetworkState[] arrayOfNetworkState = getAllNetworkState();
                                    paramParcel2.writeNoException();
                                    paramParcel2.writeTypedArray(arrayOfNetworkState, j);
                                    continue;
                                    paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                    NetworkQuotaInfo localNetworkQuotaInfo = getActiveNetworkQuotaInfo();
                                    paramParcel2.writeNoException();
                                    if (localNetworkQuotaInfo != null)
                                    {
                                        paramParcel2.writeInt(j);
                                        localNetworkQuotaInfo.writeToParcel(paramParcel2, j);
                                    }
                                    else
                                    {
                                        paramParcel2.writeInt(0);
                                        continue;
                                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                        boolean bool9 = isActiveNetworkMetered();
                                        paramParcel2.writeNoException();
                                        if (bool9)
                                            i = j;
                                        paramParcel2.writeInt(i);
                                        continue;
                                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                        if (paramParcel1.readInt() != 0);
                                        int i18;
                                        for (int i17 = j; ; i18 = 0)
                                        {
                                            boolean bool8 = setRadios(i17);
                                            paramParcel2.writeNoException();
                                            if (bool8)
                                                i = j;
                                            paramParcel2.writeInt(i);
                                            break;
                                        }
                                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                        int i14 = paramParcel1.readInt();
                                        if (paramParcel1.readInt() != 0);
                                        int i16;
                                        for (int i15 = j; ; i16 = 0)
                                        {
                                            boolean bool7 = setRadio(i14, i15);
                                            paramParcel2.writeNoException();
                                            if (bool7)
                                                i = j;
                                            paramParcel2.writeInt(i);
                                            break;
                                        }
                                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                        int i13 = startUsingNetworkFeature(paramParcel1.readInt(), paramParcel1.readString(), paramParcel1.readStrongBinder());
                                        paramParcel2.writeNoException();
                                        paramParcel2.writeInt(i13);
                                        continue;
                                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                        int i12 = stopUsingNetworkFeature(paramParcel1.readInt(), paramParcel1.readString());
                                        paramParcel2.writeNoException();
                                        paramParcel2.writeInt(i12);
                                        continue;
                                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                        boolean bool6 = requestRouteToHost(paramParcel1.readInt(), paramParcel1.readInt());
                                        paramParcel2.writeNoException();
                                        if (bool6)
                                            i = j;
                                        paramParcel2.writeInt(i);
                                        continue;
                                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                        boolean bool5 = requestRouteToHostAddress(paramParcel1.readInt(), paramParcel1.createByteArray());
                                        paramParcel2.writeNoException();
                                        if (bool5)
                                            i = j;
                                        paramParcel2.writeInt(i);
                                        continue;
                                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                        boolean bool4 = getMobileDataEnabled();
                                        paramParcel2.writeNoException();
                                        if (bool4)
                                            i = j;
                                        paramParcel2.writeInt(i);
                                        continue;
                                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                        if (paramParcel1.readInt() != 0);
                                        int i11;
                                        for (int i10 = j; ; i11 = 0)
                                        {
                                            setMobileDataEnabled(i10);
                                            paramParcel2.writeNoException();
                                            break;
                                        }
                                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                        int i7 = paramParcel1.readInt();
                                        if (paramParcel1.readInt() != 0);
                                        int i9;
                                        for (int i8 = j; ; i9 = 0)
                                        {
                                            setPolicyDataEnable(i7, i8);
                                            paramParcel2.writeNoException();
                                            break;
                                        }
                                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                        int i6 = tether(paramParcel1.readString());
                                        paramParcel2.writeNoException();
                                        paramParcel2.writeInt(i6);
                                        continue;
                                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                        int i5 = untether(paramParcel1.readString());
                                        paramParcel2.writeNoException();
                                        paramParcel2.writeInt(i5);
                                        continue;
                                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                        int i4 = getLastTetherError(paramParcel1.readString());
                                        paramParcel2.writeNoException();
                                        paramParcel2.writeInt(i4);
                                        continue;
                                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                        boolean bool3 = isTetheringSupported();
                                        paramParcel2.writeNoException();
                                        if (bool3)
                                            i = j;
                                        paramParcel2.writeInt(i);
                                        continue;
                                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                        String[] arrayOfString7 = getTetherableIfaces();
                                        paramParcel2.writeNoException();
                                        paramParcel2.writeStringArray(arrayOfString7);
                                        continue;
                                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                        String[] arrayOfString6 = getTetheredIfaces();
                                        paramParcel2.writeNoException();
                                        paramParcel2.writeStringArray(arrayOfString6);
                                        continue;
                                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                        String[] arrayOfString5 = getTetheredIfacePairs();
                                        paramParcel2.writeNoException();
                                        paramParcel2.writeStringArray(arrayOfString5);
                                        continue;
                                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                        String[] arrayOfString4 = getTetheringErroredIfaces();
                                        paramParcel2.writeNoException();
                                        paramParcel2.writeStringArray(arrayOfString4);
                                        continue;
                                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                        String[] arrayOfString3 = getTetherableUsbRegexs();
                                        paramParcel2.writeNoException();
                                        paramParcel2.writeStringArray(arrayOfString3);
                                        continue;
                                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                        String[] arrayOfString2 = getTetherableWifiRegexs();
                                        paramParcel2.writeNoException();
                                        paramParcel2.writeStringArray(arrayOfString2);
                                        continue;
                                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                        String[] arrayOfString1 = getTetherableBluetoothRegexs();
                                        paramParcel2.writeNoException();
                                        paramParcel2.writeStringArray(arrayOfString1);
                                        continue;
                                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                        if (paramParcel1.readInt() != 0);
                                        int i2;
                                        for (int i1 = j; ; i2 = 0)
                                        {
                                            int i3 = setUsbTethering(i1);
                                            paramParcel2.writeNoException();
                                            paramParcel2.writeInt(i3);
                                            break;
                                        }
                                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                        requestNetworkTransitionWakelock(paramParcel1.readString());
                                        paramParcel2.writeNoException();
                                        continue;
                                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                        reportInetCondition(paramParcel1.readInt(), paramParcel1.readInt());
                                        paramParcel2.writeNoException();
                                        continue;
                                        paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                        ProxyProperties localProxyProperties3 = getGlobalProxy();
                                        paramParcel2.writeNoException();
                                        if (localProxyProperties3 != null)
                                        {
                                            paramParcel2.writeInt(j);
                                            localProxyProperties3.writeToParcel(paramParcel2, j);
                                        }
                                        else
                                        {
                                            paramParcel2.writeInt(0);
                                            continue;
                                            paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                            if (paramParcel1.readInt() != 0);
                                            for (ProxyProperties localProxyProperties2 = (ProxyProperties)ProxyProperties.CREATOR.createFromParcel(paramParcel1); ; localProxyProperties2 = null)
                                            {
                                                setGlobalProxy(localProxyProperties2);
                                                paramParcel2.writeNoException();
                                                break;
                                            }
                                            paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                            ProxyProperties localProxyProperties1 = getProxy();
                                            paramParcel2.writeNoException();
                                            if (localProxyProperties1 != null)
                                            {
                                                paramParcel2.writeInt(j);
                                                localProxyProperties1.writeToParcel(paramParcel2, j);
                                            }
                                            else
                                            {
                                                paramParcel2.writeInt(0);
                                                continue;
                                                paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                                int k = paramParcel1.readInt();
                                                if (paramParcel1.readInt() != 0);
                                                int n;
                                                for (int m = j; ; n = 0)
                                                {
                                                    setDataDependency(k, m);
                                                    paramParcel2.writeNoException();
                                                    break;
                                                }
                                                paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                                if (paramParcel1.readInt() != 0);
                                                for (ParcelFileDescriptor localParcelFileDescriptor2 = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(paramParcel1); ; localParcelFileDescriptor2 = null)
                                                {
                                                    boolean bool2 = protectVpn(localParcelFileDescriptor2);
                                                    paramParcel2.writeNoException();
                                                    if (bool2)
                                                        i = j;
                                                    paramParcel2.writeInt(i);
                                                    break;
                                                }
                                                paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                                boolean bool1 = prepareVpn(paramParcel1.readString(), paramParcel1.readString());
                                                paramParcel2.writeNoException();
                                                if (bool1)
                                                    i = j;
                                                paramParcel2.writeInt(i);
                                                continue;
                                                paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                                if (paramParcel1.readInt() != 0);
                                                for (VpnConfig localVpnConfig2 = (VpnConfig)VpnConfig.CREATOR.createFromParcel(paramParcel1); ; localVpnConfig2 = null)
                                                {
                                                    ParcelFileDescriptor localParcelFileDescriptor1 = establishVpn(localVpnConfig2);
                                                    paramParcel2.writeNoException();
                                                    if (localParcelFileDescriptor1 == null)
                                                        break label1981;
                                                    paramParcel2.writeInt(j);
                                                    localParcelFileDescriptor1.writeToParcel(paramParcel2, j);
                                                    break;
                                                }
                                                label1981: paramParcel2.writeInt(0);
                                                continue;
                                                paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                                if (paramParcel1.readInt() != 0);
                                                for (VpnConfig localVpnConfig1 = (VpnConfig)VpnConfig.CREATOR.createFromParcel(paramParcel1); ; localVpnConfig1 = null)
                                                {
                                                    startLegacyVpn(localVpnConfig1, paramParcel1.createStringArray(), paramParcel1.createStringArray());
                                                    paramParcel2.writeNoException();
                                                    break;
                                                }
                                                paramParcel1.enforceInterface("android.net.IConnectivityManager");
                                                LegacyVpnInfo localLegacyVpnInfo = getLegacyVpnInfo();
                                                paramParcel2.writeNoException();
                                                if (localLegacyVpnInfo != null)
                                                {
                                                    paramParcel2.writeInt(j);
                                                    localLegacyVpnInfo.writeToParcel(paramParcel2, j);
                                                }
                                                else
                                                {
                                                    paramParcel2.writeInt(0);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private static class Proxy
            implements IConnectivityManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public ParcelFileDescriptor establishVpn(VpnConfig paramVpnConfig)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                        if (paramVpnConfig != null)
                        {
                            localParcel1.writeInt(1);
                            paramVpnConfig.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(42, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            if (localParcel2.readInt() != 0)
                            {
                                localParcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(localParcel2);
                                return localParcelFileDescriptor;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    ParcelFileDescriptor localParcelFileDescriptor = null;
                }
            }

            public LinkProperties getActiveLinkProperties()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localLinkProperties = (LinkProperties)LinkProperties.CREATOR.createFromParcel(localParcel2);
                        return localLinkProperties;
                    }
                    LinkProperties localLinkProperties = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public NetworkInfo getActiveNetworkInfo()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localNetworkInfo = (NetworkInfo)NetworkInfo.CREATOR.createFromParcel(localParcel2);
                        return localNetworkInfo;
                    }
                    NetworkInfo localNetworkInfo = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public NetworkInfo getActiveNetworkInfoForUid(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localNetworkInfo = (NetworkInfo)NetworkInfo.CREATOR.createFromParcel(localParcel2);
                        return localNetworkInfo;
                    }
                    NetworkInfo localNetworkInfo = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public NetworkQuotaInfo getActiveNetworkQuotaInfo()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    this.mRemote.transact(11, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localNetworkQuotaInfo = (NetworkQuotaInfo)NetworkQuotaInfo.CREATOR.createFromParcel(localParcel2);
                        return localNetworkQuotaInfo;
                    }
                    NetworkQuotaInfo localNetworkQuotaInfo = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public NetworkInfo[] getAllNetworkInfo()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    NetworkInfo[] arrayOfNetworkInfo = (NetworkInfo[])localParcel2.createTypedArray(NetworkInfo.CREATOR);
                    return arrayOfNetworkInfo;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public NetworkState[] getAllNetworkState()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    this.mRemote.transact(10, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    NetworkState[] arrayOfNetworkState = (NetworkState[])localParcel2.createTypedArray(NetworkState.CREATOR);
                    return arrayOfNetworkState;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public ProxyProperties getGlobalProxy()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    this.mRemote.transact(36, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localProxyProperties = (ProxyProperties)ProxyProperties.CREATOR.createFromParcel(localParcel2);
                        return localProxyProperties;
                    }
                    ProxyProperties localProxyProperties = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.net.IConnectivityManager";
            }

            public int getLastTetherError(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(24, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public LegacyVpnInfo getLegacyVpnInfo()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    this.mRemote.transact(44, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localLegacyVpnInfo = (LegacyVpnInfo)LegacyVpnInfo.CREATOR.createFromParcel(localParcel2);
                        return localLegacyVpnInfo;
                    }
                    LegacyVpnInfo localLegacyVpnInfo = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public LinkProperties getLinkProperties(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(9, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localLinkProperties = (LinkProperties)LinkProperties.CREATOR.createFromParcel(localParcel2);
                        return localLinkProperties;
                    }
                    LinkProperties localLinkProperties = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean getMobileDataEnabled()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    this.mRemote.transact(19, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public NetworkInfo getNetworkInfo(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localNetworkInfo = (NetworkInfo)NetworkInfo.CREATOR.createFromParcel(localParcel2);
                        return localNetworkInfo;
                    }
                    NetworkInfo localNetworkInfo = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getNetworkPreference()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public ProxyProperties getProxy()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    this.mRemote.transact(38, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localProxyProperties = (ProxyProperties)ProxyProperties.CREATOR.createFromParcel(localParcel2);
                        return localProxyProperties;
                    }
                    ProxyProperties localProxyProperties = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String[] getTetherableBluetoothRegexs()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    this.mRemote.transact(32, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String[] arrayOfString = localParcel2.createStringArray();
                    return arrayOfString;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String[] getTetherableIfaces()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    this.mRemote.transact(26, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String[] arrayOfString = localParcel2.createStringArray();
                    return arrayOfString;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String[] getTetherableUsbRegexs()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    this.mRemote.transact(30, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String[] arrayOfString = localParcel2.createStringArray();
                    return arrayOfString;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String[] getTetherableWifiRegexs()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    this.mRemote.transact(31, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String[] arrayOfString = localParcel2.createStringArray();
                    return arrayOfString;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String[] getTetheredIfacePairs()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    this.mRemote.transact(28, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String[] arrayOfString = localParcel2.createStringArray();
                    return arrayOfString;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String[] getTetheredIfaces()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    this.mRemote.transact(27, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String[] arrayOfString = localParcel2.createStringArray();
                    return arrayOfString;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String[] getTetheringErroredIfaces()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    this.mRemote.transact(29, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String[] arrayOfString = localParcel2.createStringArray();
                    return arrayOfString;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isActiveNetworkMetered()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    this.mRemote.transact(12, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isNetworkSupported(int paramInt)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isTetheringSupported()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    this.mRemote.transact(25, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean prepareVpn(String paramString1, String paramString2)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    this.mRemote.transact(41, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean protectVpn(ParcelFileDescriptor paramParcelFileDescriptor)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                        if (paramParcelFileDescriptor != null)
                        {
                            localParcel1.writeInt(1);
                            paramParcelFileDescriptor.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(40, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public void reportInetCondition(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(35, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void requestNetworkTransitionWakelock(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(34, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean requestRouteToHost(int paramInt1, int paramInt2)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(17, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean requestRouteToHostAddress(int paramInt, byte[] paramArrayOfByte)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeByteArray(paramArrayOfByte);
                    this.mRemote.transact(18, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setDataDependency(int paramInt, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    localParcel1.writeInt(paramInt);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(39, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setGlobalProxy(ProxyProperties paramProxyProperties)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    if (paramProxyProperties != null)
                    {
                        localParcel1.writeInt(1);
                        paramProxyProperties.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(37, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setMobileDataEnabled(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(20, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setNetworkPreference(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setPolicyDataEnable(int paramInt, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    localParcel1.writeInt(paramInt);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(21, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean setRadio(int paramInt, boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    localParcel1.writeInt(paramInt);
                    if (paramBoolean);
                    int k;
                    for (int j = i; ; k = 0)
                    {
                        localParcel1.writeInt(j);
                        this.mRemote.transact(14, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int m = localParcel2.readInt();
                        if (m == 0)
                            break;
                        return i;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean setRadios(boolean paramBoolean)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    if (paramBoolean);
                    int k;
                    for (int j = i; ; k = 0)
                    {
                        localParcel1.writeInt(j);
                        this.mRemote.transact(13, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int m = localParcel2.readInt();
                        if (m == 0)
                            break;
                        return i;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int setUsbTethering(boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(33, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int j = localParcel2.readInt();
                    return j;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void startLegacyVpn(VpnConfig paramVpnConfig, String[] paramArrayOfString1, String[] paramArrayOfString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    if (paramVpnConfig != null)
                    {
                        localParcel1.writeInt(1);
                        paramVpnConfig.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeStringArray(paramArrayOfString1);
                        localParcel1.writeStringArray(paramArrayOfString2);
                        this.mRemote.transact(43, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int startUsingNetworkFeature(int paramInt, String paramString, IBinder paramIBinder)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeString(paramString);
                    localParcel1.writeStrongBinder(paramIBinder);
                    this.mRemote.transact(15, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int stopUsingNetworkFeature(int paramInt, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(16, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int tether(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(22, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int untether(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.net.IConnectivityManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(23, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.IConnectivityManager
 * JD-Core Version:        0.6.2
 */