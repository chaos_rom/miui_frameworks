package android.net;

import android.os.SystemProperties;
import android.util.Log;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.cert.X509Certificate;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.harmony.xnet.provider.jsse.ClientSessionContext;
import org.apache.harmony.xnet.provider.jsse.OpenSSLContextImpl;
import org.apache.harmony.xnet.provider.jsse.OpenSSLSocketImpl;
import org.apache.harmony.xnet.provider.jsse.SSLClientSessionCache;

public class SSLCertificateSocketFactory extends javax.net.ssl.SSLSocketFactory
{
    private static final HostnameVerifier HOSTNAME_VERIFIER = HttpsURLConnection.getDefaultHostnameVerifier();
    private static final TrustManager[] INSECURE_TRUST_MANAGER;
    private static final String TAG = "SSLCertificateSocketFactory";
    private final int mHandshakeTimeoutMillis;
    private javax.net.ssl.SSLSocketFactory mInsecureFactory = null;
    private KeyManager[] mKeyManagers = null;
    private byte[] mNpnProtocols = null;
    private final boolean mSecure;
    private javax.net.ssl.SSLSocketFactory mSecureFactory = null;
    private final SSLClientSessionCache mSessionCache;
    private TrustManager[] mTrustManagers = null;

    static
    {
        TrustManager[] arrayOfTrustManager = new TrustManager[1];
        arrayOfTrustManager[0] = new X509TrustManager()
        {
            public void checkClientTrusted(X509Certificate[] paramAnonymousArrayOfX509Certificate, String paramAnonymousString)
            {
            }

            public void checkServerTrusted(X509Certificate[] paramAnonymousArrayOfX509Certificate, String paramAnonymousString)
            {
            }

            public X509Certificate[] getAcceptedIssuers()
            {
                return null;
            }
        };
        INSECURE_TRUST_MANAGER = arrayOfTrustManager;
    }

    @Deprecated
    public SSLCertificateSocketFactory(int paramInt)
    {
        this(paramInt, null, true);
    }

    private SSLCertificateSocketFactory(int paramInt, SSLSessionCache paramSSLSessionCache, boolean paramBoolean)
    {
        this.mHandshakeTimeoutMillis = paramInt;
        if (paramSSLSessionCache == null);
        while (true)
        {
            this.mSessionCache = localSSLClientSessionCache;
            this.mSecure = paramBoolean;
            return;
            localSSLClientSessionCache = paramSSLSessionCache.mSessionCache;
        }
    }

    public static SocketFactory getDefault(int paramInt)
    {
        return new SSLCertificateSocketFactory(paramInt, null, true);
    }

    public static javax.net.ssl.SSLSocketFactory getDefault(int paramInt, SSLSessionCache paramSSLSessionCache)
    {
        return new SSLCertificateSocketFactory(paramInt, paramSSLSessionCache, true);
    }

    /** @deprecated */
    private javax.net.ssl.SSLSocketFactory getDelegate()
    {
        while (true)
        {
            try
            {
                if ((!this.mSecure) || (isSslCheckRelaxed()))
                {
                    if (this.mInsecureFactory == null)
                    {
                        if (this.mSecure)
                        {
                            Log.w("SSLCertificateSocketFactory", "*** BYPASSING SSL SECURITY CHECKS (socket.relaxsslcheck=yes) ***");
                            this.mInsecureFactory = makeSocketFactory(this.mKeyManagers, INSECURE_TRUST_MANAGER);
                        }
                    }
                    else
                    {
                        localSSLSocketFactory = this.mInsecureFactory;
                        return localSSLSocketFactory;
                    }
                    Log.w("SSLCertificateSocketFactory", "Bypassing SSL security checks at caller's request");
                    continue;
                }
            }
            finally
            {
            }
            if (this.mSecureFactory == null)
                this.mSecureFactory = makeSocketFactory(this.mKeyManagers, this.mTrustManagers);
            javax.net.ssl.SSLSocketFactory localSSLSocketFactory = this.mSecureFactory;
        }
    }

    public static org.apache.http.conn.ssl.SSLSocketFactory getHttpSocketFactory(int paramInt, SSLSessionCache paramSSLSessionCache)
    {
        return new org.apache.http.conn.ssl.SSLSocketFactory(new SSLCertificateSocketFactory(paramInt, paramSSLSessionCache, true));
    }

    public static javax.net.ssl.SSLSocketFactory getInsecure(int paramInt, SSLSessionCache paramSSLSessionCache)
    {
        return new SSLCertificateSocketFactory(paramInt, paramSSLSessionCache, false);
    }

    private static boolean isSslCheckRelaxed()
    {
        if (("1".equals(SystemProperties.get("ro.debuggable"))) && ("yes".equals(SystemProperties.get("socket.relaxsslcheck"))));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    private javax.net.ssl.SSLSocketFactory makeSocketFactory(KeyManager[] paramArrayOfKeyManager, TrustManager[] paramArrayOfTrustManager)
    {
        try
        {
            OpenSSLContextImpl localOpenSSLContextImpl = new OpenSSLContextImpl();
            localOpenSSLContextImpl.engineInit(paramArrayOfKeyManager, paramArrayOfTrustManager, null);
            localOpenSSLContextImpl.engineGetClientSessionContext().setPersistentCache(this.mSessionCache);
            javax.net.ssl.SSLSocketFactory localSSLSocketFactory2 = localOpenSSLContextImpl.engineGetSocketFactory();
            localSSLSocketFactory1 = localSSLSocketFactory2;
            return localSSLSocketFactory1;
        }
        catch (KeyManagementException localKeyManagementException)
        {
            while (true)
            {
                Log.wtf("SSLCertificateSocketFactory", localKeyManagementException);
                javax.net.ssl.SSLSocketFactory localSSLSocketFactory1 = (javax.net.ssl.SSLSocketFactory)javax.net.ssl.SSLSocketFactory.getDefault();
            }
        }
    }

    static byte[] toNpnProtocolsList(byte[][] paramArrayOfByte)
    {
        if (paramArrayOfByte.length == 0)
            throw new IllegalArgumentException("npnProtocols.length == 0");
        int i = 0;
        int j = paramArrayOfByte.length;
        for (int k = 0; k < j; k++)
        {
            byte[] arrayOfByte3 = paramArrayOfByte[k];
            if ((arrayOfByte3.length == 0) || (arrayOfByte3.length > 255))
                throw new IllegalArgumentException("s.length == 0 || s.length > 255: " + arrayOfByte3.length);
            i += 1 + arrayOfByte3.length;
        }
        byte[] arrayOfByte1 = new byte[i];
        int m = paramArrayOfByte.length;
        int n = 0;
        int i1 = 0;
        while (n < m)
        {
            byte[] arrayOfByte2 = paramArrayOfByte[n];
            int i2 = i1 + 1;
            arrayOfByte1[i1] = ((byte)arrayOfByte2.length);
            int i3 = arrayOfByte2.length;
            int i4 = 0;
            int i6;
            for (i1 = i2; i4 < i3; i1 = i6)
            {
                int i5 = arrayOfByte2[i4];
                i6 = i1 + 1;
                arrayOfByte1[i1] = i5;
                i4++;
            }
            n++;
        }
        return arrayOfByte1;
    }

    public static void verifyHostname(Socket paramSocket, String paramString)
        throws IOException
    {
        if (!(paramSocket instanceof SSLSocket))
            throw new IllegalArgumentException("Attempt to verify non-SSL socket");
        if (!isSslCheckRelaxed())
        {
            SSLSocket localSSLSocket = (SSLSocket)paramSocket;
            localSSLSocket.startHandshake();
            SSLSession localSSLSession = localSSLSocket.getSession();
            if (localSSLSession == null)
                throw new SSLException("Cannot verify SSL socket without session");
            if (!HOSTNAME_VERIFIER.verify(paramString, localSSLSession))
                throw new SSLPeerUnverifiedException("Cannot verify hostname: " + paramString);
        }
    }

    public Socket createSocket()
        throws IOException
    {
        OpenSSLSocketImpl localOpenSSLSocketImpl = (OpenSSLSocketImpl)getDelegate().createSocket();
        localOpenSSLSocketImpl.setNpnProtocols(this.mNpnProtocols);
        localOpenSSLSocketImpl.setHandshakeTimeout(this.mHandshakeTimeoutMillis);
        return localOpenSSLSocketImpl;
    }

    public Socket createSocket(String paramString, int paramInt)
        throws IOException
    {
        OpenSSLSocketImpl localOpenSSLSocketImpl = (OpenSSLSocketImpl)getDelegate().createSocket(paramString, paramInt);
        localOpenSSLSocketImpl.setNpnProtocols(this.mNpnProtocols);
        localOpenSSLSocketImpl.setHandshakeTimeout(this.mHandshakeTimeoutMillis);
        if (this.mSecure)
            verifyHostname(localOpenSSLSocketImpl, paramString);
        return localOpenSSLSocketImpl;
    }

    public Socket createSocket(String paramString, int paramInt1, InetAddress paramInetAddress, int paramInt2)
        throws IOException
    {
        OpenSSLSocketImpl localOpenSSLSocketImpl = (OpenSSLSocketImpl)getDelegate().createSocket(paramString, paramInt1, paramInetAddress, paramInt2);
        localOpenSSLSocketImpl.setNpnProtocols(this.mNpnProtocols);
        localOpenSSLSocketImpl.setHandshakeTimeout(this.mHandshakeTimeoutMillis);
        if (this.mSecure)
            verifyHostname(localOpenSSLSocketImpl, paramString);
        return localOpenSSLSocketImpl;
    }

    public Socket createSocket(InetAddress paramInetAddress, int paramInt)
        throws IOException
    {
        OpenSSLSocketImpl localOpenSSLSocketImpl = (OpenSSLSocketImpl)getDelegate().createSocket(paramInetAddress, paramInt);
        localOpenSSLSocketImpl.setNpnProtocols(this.mNpnProtocols);
        localOpenSSLSocketImpl.setHandshakeTimeout(this.mHandshakeTimeoutMillis);
        return localOpenSSLSocketImpl;
    }

    public Socket createSocket(InetAddress paramInetAddress1, int paramInt1, InetAddress paramInetAddress2, int paramInt2)
        throws IOException
    {
        OpenSSLSocketImpl localOpenSSLSocketImpl = (OpenSSLSocketImpl)getDelegate().createSocket(paramInetAddress1, paramInt1, paramInetAddress2, paramInt2);
        localOpenSSLSocketImpl.setNpnProtocols(this.mNpnProtocols);
        localOpenSSLSocketImpl.setHandshakeTimeout(this.mHandshakeTimeoutMillis);
        return localOpenSSLSocketImpl;
    }

    public Socket createSocket(Socket paramSocket, String paramString, int paramInt, boolean paramBoolean)
        throws IOException
    {
        OpenSSLSocketImpl localOpenSSLSocketImpl = (OpenSSLSocketImpl)getDelegate().createSocket(paramSocket, paramString, paramInt, paramBoolean);
        localOpenSSLSocketImpl.setNpnProtocols(this.mNpnProtocols);
        localOpenSSLSocketImpl.setHandshakeTimeout(this.mHandshakeTimeoutMillis);
        if (this.mSecure)
            verifyHostname(localOpenSSLSocketImpl, paramString);
        return localOpenSSLSocketImpl;
    }

    public String[] getDefaultCipherSuites()
    {
        return getDelegate().getSupportedCipherSuites();
    }

    public byte[] getNpnSelectedProtocol(Socket paramSocket)
    {
        return ((OpenSSLSocketImpl)paramSocket).getNpnSelectedProtocol();
    }

    public String[] getSupportedCipherSuites()
    {
        return getDelegate().getSupportedCipherSuites();
    }

    public void setKeyManagers(KeyManager[] paramArrayOfKeyManager)
    {
        this.mKeyManagers = paramArrayOfKeyManager;
        this.mSecureFactory = null;
        this.mInsecureFactory = null;
    }

    public void setNpnProtocols(byte[][] paramArrayOfByte)
    {
        this.mNpnProtocols = toNpnProtocolsList(paramArrayOfByte);
    }

    public void setTrustManagers(TrustManager[] paramArrayOfTrustManager)
    {
        this.mTrustManagers = paramArrayOfTrustManager;
        this.mSecureFactory = null;
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.SSLCertificateSocketFactory
 * JD-Core Version:        0.6.2
 */