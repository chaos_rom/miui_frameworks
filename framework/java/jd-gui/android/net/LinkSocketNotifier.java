package android.net;

public abstract interface LinkSocketNotifier
{
    public abstract boolean onBetterLinkAvailable(LinkSocket paramLinkSocket1, LinkSocket paramLinkSocket2);

    public abstract void onCapabilitiesChanged(LinkSocket paramLinkSocket, LinkCapabilities paramLinkCapabilities);

    public abstract void onLinkLost(LinkSocket paramLinkSocket);

    public abstract void onNewLinkUnavailable(LinkSocket paramLinkSocket);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.net.LinkSocketNotifier
 * JD-Core Version:        0.6.2
 */