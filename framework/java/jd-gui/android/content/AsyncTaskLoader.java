package android.content;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.OperationCanceledException;
import android.os.SystemClock;
import android.util.TimeUtils;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.concurrent.CountDownLatch;

public abstract class AsyncTaskLoader<D> extends Loader<D>
{
    static final boolean DEBUG = false;
    static final String TAG = "AsyncTaskLoader";
    volatile AsyncTaskLoader<D>.LoadTask mCancellingTask;
    Handler mHandler;
    long mLastLoadCompleteTime = -10000L;
    volatile AsyncTaskLoader<D>.LoadTask mTask;
    long mUpdateThrottle;

    public AsyncTaskLoader(Context paramContext)
    {
        super(paramContext);
    }

    public void cancelLoadInBackground()
    {
    }

    void dispatchOnCancelled(AsyncTaskLoader<D>.LoadTask paramAsyncTaskLoader, D paramD)
    {
        onCanceled(paramD);
        if (this.mCancellingTask == paramAsyncTaskLoader)
        {
            this.mLastLoadCompleteTime = SystemClock.uptimeMillis();
            this.mCancellingTask = null;
            deliverCancellation();
            executePendingTask();
        }
    }

    void dispatchOnLoadComplete(AsyncTaskLoader<D>.LoadTask paramAsyncTaskLoader, D paramD)
    {
        if (this.mTask != paramAsyncTaskLoader)
            dispatchOnCancelled(paramAsyncTaskLoader, paramD);
        while (true)
        {
            return;
            if (isAbandoned())
            {
                onCanceled(paramD);
            }
            else
            {
                this.mLastLoadCompleteTime = SystemClock.uptimeMillis();
                this.mTask = null;
                deliverResult(paramD);
            }
        }
    }

    public void dump(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        super.dump(paramString, paramFileDescriptor, paramPrintWriter, paramArrayOfString);
        if (this.mTask != null)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("mTask=");
            paramPrintWriter.print(this.mTask);
            paramPrintWriter.print(" waiting=");
            paramPrintWriter.println(this.mTask.waiting);
        }
        if (this.mCancellingTask != null)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("mCancellingTask=");
            paramPrintWriter.print(this.mCancellingTask);
            paramPrintWriter.print(" waiting=");
            paramPrintWriter.println(this.mCancellingTask.waiting);
        }
        if (this.mUpdateThrottle != 0L)
        {
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("mUpdateThrottle=");
            TimeUtils.formatDuration(this.mUpdateThrottle, paramPrintWriter);
            paramPrintWriter.print(" mLastLoadCompleteTime=");
            TimeUtils.formatDuration(this.mLastLoadCompleteTime, SystemClock.uptimeMillis(), paramPrintWriter);
            paramPrintWriter.println();
        }
    }

    void executePendingTask()
    {
        if ((this.mCancellingTask == null) && (this.mTask != null))
        {
            if (this.mTask.waiting)
            {
                this.mTask.waiting = false;
                this.mHandler.removeCallbacks(this.mTask);
            }
            if ((this.mUpdateThrottle <= 0L) || (SystemClock.uptimeMillis() >= this.mLastLoadCompleteTime + this.mUpdateThrottle))
                break label98;
            this.mTask.waiting = true;
            this.mHandler.postAtTime(this.mTask, this.mLastLoadCompleteTime + this.mUpdateThrottle);
        }
        while (true)
        {
            return;
            label98: this.mTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void[])null);
        }
    }

    public boolean isLoadInBackgroundCanceled()
    {
        if (this.mCancellingTask != null);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public abstract D loadInBackground();

    protected boolean onCancelLoad()
    {
        boolean bool = false;
        if (this.mTask != null)
        {
            if (this.mCancellingTask == null)
                break label52;
            if (this.mTask.waiting)
            {
                this.mTask.waiting = false;
                this.mHandler.removeCallbacks(this.mTask);
            }
            this.mTask = null;
        }
        while (true)
        {
            return bool;
            label52: if (this.mTask.waiting)
            {
                this.mTask.waiting = false;
                this.mHandler.removeCallbacks(this.mTask);
                this.mTask = null;
            }
            else
            {
                bool = this.mTask.cancel(false);
                if (bool)
                {
                    this.mCancellingTask = this.mTask;
                    cancelLoadInBackground();
                }
                this.mTask = null;
            }
        }
    }

    public void onCanceled(D paramD)
    {
    }

    protected void onForceLoad()
    {
        super.onForceLoad();
        cancelLoad();
        this.mTask = new LoadTask();
        executePendingTask();
    }

    protected D onLoadInBackground()
    {
        return loadInBackground();
    }

    public void setUpdateThrottle(long paramLong)
    {
        this.mUpdateThrottle = paramLong;
        if (paramLong != 0L)
            this.mHandler = new Handler();
    }

    public void waitForLoader()
    {
        LoadTask localLoadTask = this.mTask;
        if (localLoadTask != null)
            localLoadTask.waitForLoader();
    }

    final class LoadTask extends AsyncTask<Void, Void, D>
        implements Runnable
    {
        private final CountDownLatch mDone = new CountDownLatch(1);
        boolean waiting;

        LoadTask()
        {
        }

        protected D doInBackground(Void[] paramArrayOfVoid)
        {
            try
            {
                Object localObject2 = AsyncTaskLoader.this.onLoadInBackground();
                localObject1 = localObject2;
                return localObject1;
            }
            catch (OperationCanceledException localOperationCanceledException)
            {
                while (true)
                {
                    if (!isCancelled())
                        throw localOperationCanceledException;
                    Object localObject1 = null;
                }
            }
        }

        protected void onCancelled(D paramD)
        {
            try
            {
                AsyncTaskLoader.this.dispatchOnCancelled(this, paramD);
                return;
            }
            finally
            {
                this.mDone.countDown();
            }
        }

        protected void onPostExecute(D paramD)
        {
            try
            {
                AsyncTaskLoader.this.dispatchOnLoadComplete(this, paramD);
                return;
            }
            finally
            {
                this.mDone.countDown();
            }
        }

        public void run()
        {
            this.waiting = false;
            AsyncTaskLoader.this.executePendingTask();
        }

        public void waitForLoader()
        {
            try
            {
                this.mDone.await();
                label7: return;
            }
            catch (InterruptedException localInterruptedException)
            {
                break label7;
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.AsyncTaskLoader
 * JD-Core Version:        0.6.2
 */