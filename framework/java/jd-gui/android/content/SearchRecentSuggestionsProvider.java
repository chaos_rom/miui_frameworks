package android.content;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import java.util.List;

public class SearchRecentSuggestionsProvider extends ContentProvider
{
    public static final int DATABASE_MODE_2LINES = 2;
    public static final int DATABASE_MODE_QUERIES = 1;
    private static final int DATABASE_VERSION = 512;
    private static final String NULL_COLUMN = "query";
    private static final String ORDER_BY = "date DESC";
    private static final String TAG = "SuggestionsProvider";
    private static final int URI_MATCH_SUGGEST = 1;
    private static final String sDatabaseName = "suggestions.db";
    private static final String sSuggestions = "suggestions";
    private String mAuthority;
    private int mMode;
    private SQLiteOpenHelper mOpenHelper;
    private String mSuggestSuggestionClause;
    private String[] mSuggestionProjection;
    private Uri mSuggestionsUri;
    private boolean mTwoLineDisplay;
    private UriMatcher mUriMatcher;

    public int delete(Uri paramUri, String paramString, String[] paramArrayOfString)
    {
        SQLiteDatabase localSQLiteDatabase = this.mOpenHelper.getWritableDatabase();
        if (paramUri.getPathSegments().size() != 1)
            throw new IllegalArgumentException("Unknown Uri");
        if (((String)paramUri.getPathSegments().get(0)).equals("suggestions"))
        {
            int i = localSQLiteDatabase.delete("suggestions", paramString, paramArrayOfString);
            getContext().getContentResolver().notifyChange(paramUri, null);
            return i;
        }
        throw new IllegalArgumentException("Unknown Uri");
    }

    public String getType(Uri paramUri)
    {
        String str;
        if (this.mUriMatcher.match(paramUri) == 1)
            str = "vnd.android.cursor.dir/vnd.android.search.suggest";
        while (true)
        {
            return str;
            int i = paramUri.getPathSegments().size();
            if ((i < 1) || (!((String)paramUri.getPathSegments().get(0)).equals("suggestions")))
                break;
            if (i == 1)
            {
                str = "vnd.android.cursor.dir/suggestion";
            }
            else
            {
                if (i != 2)
                    break;
                str = "vnd.android.cursor.item/suggestion";
            }
        }
        throw new IllegalArgumentException("Unknown Uri");
    }

    public Uri insert(Uri paramUri, ContentValues paramContentValues)
    {
        SQLiteDatabase localSQLiteDatabase = this.mOpenHelper.getWritableDatabase();
        int i = paramUri.getPathSegments().size();
        if (i < 1)
            throw new IllegalArgumentException("Unknown Uri");
        long l = -1L;
        String str = (String)paramUri.getPathSegments().get(0);
        Uri localUri = null;
        if ((str.equals("suggestions")) && (i == 1))
        {
            l = localSQLiteDatabase.insert("suggestions", "query", paramContentValues);
            if (l > 0L)
                localUri = Uri.withAppendedPath(this.mSuggestionsUri, String.valueOf(l));
        }
        if (l < 0L)
            throw new IllegalArgumentException("Unknown Uri");
        getContext().getContentResolver().notifyChange(localUri, null);
        return localUri;
    }

    public boolean onCreate()
    {
        if ((this.mAuthority == null) || (this.mMode == 0))
            throw new IllegalArgumentException("Provider not configured");
        int i = 512 + this.mMode;
        this.mOpenHelper = new DatabaseHelper(getContext(), i);
        return true;
    }

    public Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
    {
        SQLiteDatabase localSQLiteDatabase = this.mOpenHelper.getReadableDatabase();
        String str3;
        String[] arrayOfString2;
        Cursor localCursor;
        if (this.mUriMatcher.match(paramUri) == 1)
            if (TextUtils.isEmpty(paramArrayOfString2[0]))
            {
                str3 = null;
                arrayOfString2 = null;
                localCursor = localSQLiteDatabase.query("suggestions", this.mSuggestionProjection, str3, arrayOfString2, null, null, "date DESC", null);
                ContentResolver localContentResolver2 = getContext().getContentResolver();
                localCursor.setNotificationUri(localContentResolver2, paramUri);
            }
        while (true)
        {
            return localCursor;
            String str2 = "%" + paramArrayOfString2[0] + "%";
            if (this.mTwoLineDisplay)
            {
                arrayOfString2 = new String[2];
                arrayOfString2[0] = str2;
                arrayOfString2[1] = str2;
            }
            while (true)
            {
                str3 = this.mSuggestSuggestionClause;
                break;
                arrayOfString2 = new String[1];
                arrayOfString2[0] = str2;
            }
            int i = paramUri.getPathSegments().size();
            if ((i != 1) && (i != 2))
                throw new IllegalArgumentException("Unknown Uri");
            String str1 = (String)paramUri.getPathSegments().get(0);
            if (!str1.equals("suggestions"))
                throw new IllegalArgumentException("Unknown Uri");
            String[] arrayOfString1 = null;
            if ((paramArrayOfString1 != null) && (paramArrayOfString1.length > 0))
            {
                arrayOfString1 = new String[1 + paramArrayOfString1.length];
                System.arraycopy(paramArrayOfString1, 0, arrayOfString1, 0, paramArrayOfString1.length);
                arrayOfString1[paramArrayOfString1.length] = "_id AS _id";
            }
            StringBuilder localStringBuilder = new StringBuilder(256);
            if (i == 2)
                localStringBuilder.append("(_id = ").append((String)paramUri.getPathSegments().get(1)).append(")");
            if ((paramString1 != null) && (paramString1.length() > 0))
            {
                if (localStringBuilder.length() > 0)
                    localStringBuilder.append(" AND ");
                localStringBuilder.append('(');
                localStringBuilder.append(paramString1);
                localStringBuilder.append(')');
            }
            localCursor = localSQLiteDatabase.query(str1, arrayOfString1, localStringBuilder.toString(), paramArrayOfString2, null, null, paramString2, null);
            ContentResolver localContentResolver1 = getContext().getContentResolver();
            localCursor.setNotificationUri(localContentResolver1, paramUri);
        }
    }

    protected void setupSuggestions(String paramString, int paramInt)
    {
        if ((TextUtils.isEmpty(paramString)) || ((paramInt & 0x1) == 0))
            throw new IllegalArgumentException();
        boolean bool;
        String[] arrayOfString2;
        if ((paramInt & 0x2) != 0)
        {
            bool = true;
            this.mTwoLineDisplay = bool;
            this.mAuthority = new String(paramString);
            this.mMode = paramInt;
            this.mSuggestionsUri = Uri.parse("content://" + this.mAuthority + "/suggestions");
            this.mUriMatcher = new UriMatcher(-1);
            this.mUriMatcher.addURI(this.mAuthority, "search_suggest_query", 1);
            if (!this.mTwoLineDisplay)
                break label180;
            this.mSuggestSuggestionClause = "display1 LIKE ? OR display2 LIKE ?";
            arrayOfString2 = new String[6];
            arrayOfString2[0] = "0 AS suggest_format";
            arrayOfString2[1] = "'android.resource://system/17301578' AS suggest_icon_1";
            arrayOfString2[2] = "display1 AS suggest_text_1";
            arrayOfString2[3] = "display2 AS suggest_text_2";
            arrayOfString2[4] = "query AS suggest_intent_query";
            arrayOfString2[5] = "_id";
        }
        label180: String[] arrayOfString1;
        for (this.mSuggestionProjection = arrayOfString2; ; this.mSuggestionProjection = arrayOfString1)
        {
            return;
            bool = false;
            break;
            this.mSuggestSuggestionClause = "display1 LIKE ?";
            arrayOfString1 = new String[5];
            arrayOfString1[0] = "0 AS suggest_format";
            arrayOfString1[1] = "'android.resource://system/17301578' AS suggest_icon_1";
            arrayOfString1[2] = "display1 AS suggest_text_1";
            arrayOfString1[3] = "query AS suggest_intent_query";
            arrayOfString1[4] = "_id";
        }
    }

    public int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    private static class DatabaseHelper extends SQLiteOpenHelper
    {
        private int mNewVersion;

        public DatabaseHelper(Context paramContext, int paramInt)
        {
            super("suggestions.db", null, paramInt);
            this.mNewVersion = paramInt;
        }

        public void onCreate(SQLiteDatabase paramSQLiteDatabase)
        {
            StringBuilder localStringBuilder = new StringBuilder();
            localStringBuilder.append("CREATE TABLE suggestions (_id INTEGER PRIMARY KEY,display1 TEXT UNIQUE ON CONFLICT REPLACE");
            if ((0x2 & this.mNewVersion) != 0)
                localStringBuilder.append(",display2 TEXT");
            localStringBuilder.append(",query TEXT,date LONG);");
            paramSQLiteDatabase.execSQL(localStringBuilder.toString());
        }

        public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
        {
            Log.w("SuggestionsProvider", "Upgrading database from version " + paramInt1 + " to " + paramInt2 + ", which will destroy all old data");
            paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS suggestions");
            onCreate(paramSQLiteDatabase);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.SearchRecentSuggestionsProvider
 * JD-Core Version:        0.6.2
 */