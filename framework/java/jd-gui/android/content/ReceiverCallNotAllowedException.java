package android.content;

import android.util.AndroidRuntimeException;

public class ReceiverCallNotAllowedException extends AndroidRuntimeException
{
    public ReceiverCallNotAllowedException(String paramString)
    {
        super(paramString);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.ReceiverCallNotAllowedException
 * JD-Core Version:        0.6.2
 */