package android.content;

import android.content.res.AssetFileDescriptor;
import android.database.BulkCursorDescriptor;
import android.database.Cursor;
import android.database.CursorToBulkCursorAdaptor;
import android.database.DatabaseUtils;
import android.database.IContentObserver;
import android.database.IContentObserver.Stub;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ICancellationSignal;
import android.os.ICancellationSignal.Stub;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import java.util.ArrayList;

public abstract class ContentProviderNative extends Binder
    implements IContentProvider
{
    public ContentProviderNative()
    {
        attachInterface(this, "android.content.IContentProvider");
    }

    public static IContentProvider asInterface(IBinder paramIBinder)
    {
        Object localObject;
        if (paramIBinder == null)
            localObject = null;
        while (true)
        {
            return localObject;
            localObject = (IContentProvider)paramIBinder.queryLocalInterface("android.content.IContentProvider");
            if (localObject == null)
                localObject = new ContentProviderProxy(paramIBinder);
        }
    }

    public IBinder asBinder()
    {
        return this;
    }

    public abstract String getProviderName();

    public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
        throws RemoteException
    {
        boolean bool;
        switch (paramInt1)
        {
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 11:
        case 12:
        case 16:
        case 17:
        case 18:
        case 19:
        default:
            bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
        case 1:
        case 2:
        case 3:
        case 13:
        case 20:
        case 4:
        case 10:
        case 14:
        case 15:
        case 21:
        case 22:
        case 23:
        case 24:
        }
        while (true)
        {
            return bool;
            try
            {
                paramParcel1.enforceInterface("android.content.IContentProvider");
                Uri localUri2 = (Uri)Uri.CREATOR.createFromParcel(paramParcel1);
                int i1 = paramParcel1.readInt();
                String[] arrayOfString2 = null;
                if (i1 > 0)
                {
                    arrayOfString2 = new String[i1];
                    for (int i4 = 0; i4 < i1; i4++)
                        arrayOfString2[i4] = paramParcel1.readString();
                }
                String str2 = paramParcel1.readString();
                int i2 = paramParcel1.readInt();
                String[] arrayOfString3 = null;
                if (i2 > 0)
                {
                    arrayOfString3 = new String[i2];
                    for (int i3 = 0; i3 < i2; i3++)
                        arrayOfString3[i3] = paramParcel1.readString();
                }
                String str3 = paramParcel1.readString();
                IContentObserver localIContentObserver = IContentObserver.Stub.asInterface(paramParcel1.readStrongBinder());
                Cursor localCursor = query(localUri2, arrayOfString2, str2, arrayOfString3, str3, ICancellationSignal.Stub.asInterface(paramParcel1.readStrongBinder()));
                if (localCursor != null)
                {
                    BulkCursorDescriptor localBulkCursorDescriptor = new CursorToBulkCursorAdaptor(localCursor, localIContentObserver, getProviderName()).getBulkCursorDescriptor();
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(1);
                    localBulkCursorDescriptor.writeToParcel(paramParcel2, 1);
                }
                else
                {
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(0);
                }
            }
            catch (Exception localException)
            {
                DatabaseUtils.writeExceptionToParcel(paramParcel2, localException);
                bool = true;
            }
            continue;
            paramParcel1.enforceInterface("android.content.IContentProvider");
            String str1 = getType((Uri)Uri.CREATOR.createFromParcel(paramParcel1));
            paramParcel2.writeNoException();
            paramParcel2.writeString(str1);
            bool = true;
            continue;
            paramParcel1.enforceInterface("android.content.IContentProvider");
            Uri localUri1 = insert((Uri)Uri.CREATOR.createFromParcel(paramParcel1), (ContentValues)ContentValues.CREATOR.createFromParcel(paramParcel1));
            paramParcel2.writeNoException();
            Uri.writeToParcel(paramParcel2, localUri1);
            bool = true;
            continue;
            paramParcel1.enforceInterface("android.content.IContentProvider");
            int n = bulkInsert((Uri)Uri.CREATOR.createFromParcel(paramParcel1), (ContentValues[])paramParcel1.createTypedArray(ContentValues.CREATOR));
            paramParcel2.writeNoException();
            paramParcel2.writeInt(n);
            bool = true;
            continue;
            paramParcel1.enforceInterface("android.content.IContentProvider");
            int k = paramParcel1.readInt();
            ArrayList localArrayList = new ArrayList(k);
            for (int m = 0; m < k; m++)
            {
                Object localObject = ContentProviderOperation.CREATOR.createFromParcel(paramParcel1);
                localArrayList.add(m, localObject);
            }
            ContentProviderResult[] arrayOfContentProviderResult = applyBatch(localArrayList);
            paramParcel2.writeNoException();
            paramParcel2.writeTypedArray(arrayOfContentProviderResult, 0);
            bool = true;
            continue;
            paramParcel1.enforceInterface("android.content.IContentProvider");
            int j = delete((Uri)Uri.CREATOR.createFromParcel(paramParcel1), paramParcel1.readString(), paramParcel1.readStringArray());
            paramParcel2.writeNoException();
            paramParcel2.writeInt(j);
            bool = true;
            continue;
            paramParcel1.enforceInterface("android.content.IContentProvider");
            int i = update((Uri)Uri.CREATOR.createFromParcel(paramParcel1), (ContentValues)ContentValues.CREATOR.createFromParcel(paramParcel1), paramParcel1.readString(), paramParcel1.readStringArray());
            paramParcel2.writeNoException();
            paramParcel2.writeInt(i);
            bool = true;
            continue;
            paramParcel1.enforceInterface("android.content.IContentProvider");
            ParcelFileDescriptor localParcelFileDescriptor = openFile((Uri)Uri.CREATOR.createFromParcel(paramParcel1), paramParcel1.readString());
            paramParcel2.writeNoException();
            if (localParcelFileDescriptor != null)
            {
                paramParcel2.writeInt(1);
                localParcelFileDescriptor.writeToParcel(paramParcel2, 1);
            }
            else
            {
                paramParcel2.writeInt(0);
                break label993;
                paramParcel1.enforceInterface("android.content.IContentProvider");
                AssetFileDescriptor localAssetFileDescriptor2 = openAssetFile((Uri)Uri.CREATOR.createFromParcel(paramParcel1), paramParcel1.readString());
                paramParcel2.writeNoException();
                if (localAssetFileDescriptor2 != null)
                {
                    paramParcel2.writeInt(1);
                    localAssetFileDescriptor2.writeToParcel(paramParcel2, 1);
                    break label999;
                }
                paramParcel2.writeInt(0);
                break label999;
                paramParcel1.enforceInterface("android.content.IContentProvider");
                Bundle localBundle = call(paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readBundle());
                paramParcel2.writeNoException();
                paramParcel2.writeBundle(localBundle);
                bool = true;
                continue;
                paramParcel1.enforceInterface("android.content.IContentProvider");
                String[] arrayOfString1 = getStreamTypes((Uri)Uri.CREATOR.createFromParcel(paramParcel1), paramParcel1.readString());
                paramParcel2.writeNoException();
                paramParcel2.writeStringArray(arrayOfString1);
                bool = true;
                continue;
                paramParcel1.enforceInterface("android.content.IContentProvider");
                AssetFileDescriptor localAssetFileDescriptor1 = openTypedAssetFile((Uri)Uri.CREATOR.createFromParcel(paramParcel1), paramParcel1.readString(), paramParcel1.readBundle());
                paramParcel2.writeNoException();
                if (localAssetFileDescriptor1 != null)
                {
                    paramParcel2.writeInt(1);
                    localAssetFileDescriptor1.writeToParcel(paramParcel2, 1);
                    break label1005;
                }
                paramParcel2.writeInt(0);
                break label1005;
                paramParcel1.enforceInterface("android.content.IContentProvider");
                ICancellationSignal localICancellationSignal = createCancellationSignal();
                paramParcel2.writeNoException();
                paramParcel2.writeStrongBinder(localICancellationSignal.asBinder());
                bool = true;
                continue;
                bool = true;
                continue;
            }
            label993: bool = true;
            continue;
            label999: bool = true;
            continue;
            label1005: bool = true;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.ContentProviderNative
 * JD-Core Version:        0.6.2
 */