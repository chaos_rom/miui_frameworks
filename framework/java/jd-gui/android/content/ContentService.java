package android.content;

import android.accounts.Account;
import android.database.IContentObserver;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.UserId;
import android.util.Log;
import android.util.SparseIntArray;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class ContentService extends IContentService.Stub
{
    private static final String TAG = "ContentService";
    private Context mContext;
    private boolean mFactoryTest;
    private final ObserverNode mRootNode = new ObserverNode("");
    private SyncManager mSyncManager = null;
    private final Object mSyncManagerLock = new Object();

    ContentService(Context paramContext, boolean paramBoolean)
    {
        this.mContext = paramContext;
        this.mFactoryTest = paramBoolean;
    }

    private SyncManager getSyncManager()
    {
        synchronized (this.mSyncManagerLock)
        {
            try
            {
                if (this.mSyncManager == null)
                    this.mSyncManager = new SyncManager(this.mContext, this.mFactoryTest);
                SyncManager localSyncManager = this.mSyncManager;
                return localSyncManager;
            }
            catch (SQLiteException localSQLiteException)
            {
                while (true)
                    Log.e("ContentService", "Can't create SyncManager", localSQLiteException);
            }
        }
    }

    public static ContentService main(Context paramContext, boolean paramBoolean)
    {
        ContentService localContentService = new ContentService(paramContext, paramBoolean);
        ServiceManager.addService("content", localContentService);
        return localContentService;
    }

    public void addPeriodicSync(Account paramAccount, String paramString, Bundle paramBundle, long paramLong)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.WRITE_SYNC_SETTINGS", "no permission to write the sync settings");
        int i = UserId.getCallingUserId();
        long l = clearCallingIdentity();
        try
        {
            getSyncManager().getSyncStorageEngine().addPeriodicSync(paramAccount, i, paramString, paramBundle, paramLong);
            return;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public void addStatusChangeListener(int paramInt, ISyncStatusObserver paramISyncStatusObserver)
    {
        long l = clearCallingIdentity();
        try
        {
            SyncManager localSyncManager = getSyncManager();
            if ((localSyncManager != null) && (paramISyncStatusObserver != null))
                localSyncManager.getSyncStorageEngine().addStatusChangeListener(paramInt, paramISyncStatusObserver);
            return;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public void cancelSync(Account paramAccount, String paramString)
    {
        int i = UserId.getCallingUserId();
        long l = clearCallingIdentity();
        try
        {
            SyncManager localSyncManager = getSyncManager();
            if (localSyncManager != null)
            {
                localSyncManager.clearScheduledSyncOperations(paramAccount, i, paramString);
                localSyncManager.cancelActiveSync(paramAccount, i, paramString);
            }
            return;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    /** @deprecated */
    // ERROR //
    protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        // Byte code:
        //     0: aload_0
        //     1: monitorenter
        //     2: aload_0
        //     3: getfield 47	android/content/ContentService:mContext	Landroid/content/Context;
        //     6: ldc 130
        //     8: ldc 132
        //     10: invokevirtual 88	android/content/Context:enforceCallingOrSelfPermission	(Ljava/lang/String;Ljava/lang/String;)V
        //     13: invokestatic 98	android/content/ContentService:clearCallingIdentity	()J
        //     16: lstore 5
        //     18: aload_0
        //     19: getfield 40	android/content/ContentService:mSyncManager	Landroid/content/SyncManager;
        //     22: ifnonnull +108 -> 130
        //     25: aload_2
        //     26: ldc 134
        //     28: invokevirtual 139	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     31: aload_2
        //     32: invokevirtual 141	java/io/PrintWriter:println	()V
        //     35: aload_2
        //     36: ldc 143
        //     38: invokevirtual 139	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     41: aload_0
        //     42: getfield 38	android/content/ContentService:mRootNode	Landroid/content/ContentService$ObserverNode;
        //     45: astore 8
        //     47: aload 8
        //     49: monitorenter
        //     50: iconst_2
        //     51: newarray int
        //     53: astore 10
        //     55: new 145	android/util/SparseIntArray
        //     58: dup
        //     59: invokespecial 146	android/util/SparseIntArray:<init>	()V
        //     62: astore 11
        //     64: aload_0
        //     65: getfield 38	android/content/ContentService:mRootNode	Landroid/content/ContentService$ObserverNode;
        //     68: aload_1
        //     69: aload_2
        //     70: aload_3
        //     71: ldc 33
        //     73: ldc 148
        //     75: aload 10
        //     77: aload 11
        //     79: invokevirtual 152	android/content/ContentService$ObserverNode:dumpLocked	(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[ILandroid/util/SparseIntArray;)V
        //     82: aload_2
        //     83: invokevirtual 141	java/io/PrintWriter:println	()V
        //     86: new 154	java/util/ArrayList
        //     89: dup
        //     90: invokespecial 155	java/util/ArrayList:<init>	()V
        //     93: astore 12
        //     95: iconst_0
        //     96: istore 13
        //     98: iload 13
        //     100: aload 11
        //     102: invokevirtual 158	android/util/SparseIntArray:size	()I
        //     105: if_icmpge +54 -> 159
        //     108: aload 12
        //     110: aload 11
        //     112: iload 13
        //     114: invokevirtual 162	android/util/SparseIntArray:keyAt	(I)I
        //     117: invokestatic 168	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     120: invokevirtual 172	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     123: pop
        //     124: iinc 13 1
        //     127: goto -29 -> 98
        //     130: aload_0
        //     131: getfield 40	android/content/ContentService:mSyncManager	Landroid/content/SyncManager;
        //     134: aload_1
        //     135: aload_2
        //     136: invokevirtual 175	android/content/SyncManager:dump	(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V
        //     139: goto -108 -> 31
        //     142: astore 7
        //     144: lload 5
        //     146: invokestatic 113	android/content/ContentService:restoreCallingIdentity	(J)V
        //     149: aload 7
        //     151: athrow
        //     152: astore 4
        //     154: aload_0
        //     155: monitorexit
        //     156: aload 4
        //     158: athrow
        //     159: aload 12
        //     161: new 6	android/content/ContentService$1
        //     164: dup
        //     165: aload_0
        //     166: aload 11
        //     168: invokespecial 178	android/content/ContentService$1:<init>	(Landroid/content/ContentService;Landroid/util/SparseIntArray;)V
        //     171: invokestatic 184	java/util/Collections:sort	(Ljava/util/List;Ljava/util/Comparator;)V
        //     174: iconst_0
        //     175: istore 14
        //     177: iload 14
        //     179: aload 12
        //     181: invokevirtual 185	java/util/ArrayList:size	()I
        //     184: if_icmpge +59 -> 243
        //     187: aload 12
        //     189: iload 14
        //     191: invokevirtual 189	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     194: checkcast 164	java/lang/Integer
        //     197: invokevirtual 192	java/lang/Integer:intValue	()I
        //     200: istore 15
        //     202: aload_2
        //     203: ldc 194
        //     205: invokevirtual 197	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     208: aload_2
        //     209: iload 15
        //     211: invokevirtual 200	java/io/PrintWriter:print	(I)V
        //     214: aload_2
        //     215: ldc 202
        //     217: invokevirtual 197	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     220: aload_2
        //     221: aload 11
        //     223: iload 15
        //     225: invokevirtual 204	android/util/SparseIntArray:get	(I)I
        //     228: invokevirtual 200	java/io/PrintWriter:print	(I)V
        //     231: aload_2
        //     232: ldc 206
        //     234: invokevirtual 139	java/io/PrintWriter:println	(Ljava/lang/String;)V
        //     237: iinc 14 1
        //     240: goto -63 -> 177
        //     243: aload_2
        //     244: invokevirtual 141	java/io/PrintWriter:println	()V
        //     247: aload_2
        //     248: ldc 208
        //     250: invokevirtual 197	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     253: aload_2
        //     254: aload 10
        //     256: iconst_0
        //     257: iaload
        //     258: invokevirtual 210	java/io/PrintWriter:println	(I)V
        //     261: aload_2
        //     262: ldc 212
        //     264: invokevirtual 197	java/io/PrintWriter:print	(Ljava/lang/String;)V
        //     267: aload_2
        //     268: aload 10
        //     270: iconst_1
        //     271: iaload
        //     272: invokevirtual 210	java/io/PrintWriter:println	(I)V
        //     275: aload 8
        //     277: monitorexit
        //     278: lload 5
        //     280: invokestatic 113	android/content/ContentService:restoreCallingIdentity	(J)V
        //     283: aload_0
        //     284: monitorexit
        //     285: return
        //     286: astore 9
        //     288: aload 8
        //     290: monitorexit
        //     291: aload 9
        //     293: athrow
        //
        // Exception table:
        //     from	to	target	type
        //     18	50	142	finally
        //     130	139	142	finally
        //     291	294	142	finally
        //     2	18	152	finally
        //     144	152	152	finally
        //     278	283	152	finally
        //     50	124	286	finally
        //     159	278	286	finally
        //     288	291	286	finally
    }

    public List<SyncInfo> getCurrentSyncs()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.READ_SYNC_STATS", "no permission to read the sync stats");
        int i = UserId.getCallingUserId();
        long l = clearCallingIdentity();
        try
        {
            List localList = getSyncManager().getSyncStorageEngine().getCurrentSyncs(i);
            return localList;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public int getIsSyncable(Account paramAccount, String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.READ_SYNC_SETTINGS", "no permission to read the sync settings");
        int i = UserId.getCallingUserId();
        long l = clearCallingIdentity();
        try
        {
            SyncManager localSyncManager = getSyncManager();
            if (localSyncManager != null)
            {
                int k = localSyncManager.getSyncStorageEngine().getIsSyncable(paramAccount, i, paramString);
                j = k;
                return j;
            }
            restoreCallingIdentity(l);
            int j = -1;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public boolean getMasterSyncAutomatically()
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.READ_SYNC_SETTINGS", "no permission to read the sync settings");
        int i = UserId.getCallingUserId();
        long l = clearCallingIdentity();
        try
        {
            SyncManager localSyncManager = getSyncManager();
            if (localSyncManager != null)
            {
                boolean bool2 = localSyncManager.getSyncStorageEngine().getMasterSyncAutomatically(i);
                bool1 = bool2;
                return bool1;
            }
            restoreCallingIdentity(l);
            boolean bool1 = false;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public List<PeriodicSync> getPeriodicSyncs(Account paramAccount, String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.READ_SYNC_SETTINGS", "no permission to read the sync settings");
        int i = UserId.getCallingUserId();
        long l = clearCallingIdentity();
        try
        {
            List localList = getSyncManager().getSyncStorageEngine().getPeriodicSyncs(paramAccount, i, paramString);
            return localList;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public SyncAdapterType[] getSyncAdapterTypes()
    {
        long l = clearCallingIdentity();
        try
        {
            SyncAdapterType[] arrayOfSyncAdapterType = getSyncManager().getSyncAdapterTypes();
            return arrayOfSyncAdapterType;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public boolean getSyncAutomatically(Account paramAccount, String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.READ_SYNC_SETTINGS", "no permission to read the sync settings");
        int i = UserId.getCallingUserId();
        long l = clearCallingIdentity();
        try
        {
            SyncManager localSyncManager = getSyncManager();
            if (localSyncManager != null)
            {
                boolean bool2 = localSyncManager.getSyncStorageEngine().getSyncAutomatically(paramAccount, i, paramString);
                bool1 = bool2;
                return bool1;
            }
            restoreCallingIdentity(l);
            boolean bool1 = false;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public SyncStatusInfo getSyncStatus(Account paramAccount, String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.READ_SYNC_STATS", "no permission to read the sync stats");
        int i = UserId.getCallingUserId();
        long l = clearCallingIdentity();
        try
        {
            SyncManager localSyncManager = getSyncManager();
            if (localSyncManager != null)
            {
                SyncStatusInfo localSyncStatusInfo2 = localSyncManager.getSyncStorageEngine().getStatusByAccountAndAuthority(paramAccount, i, paramString);
                localSyncStatusInfo1 = localSyncStatusInfo2;
                return localSyncStatusInfo1;
            }
            restoreCallingIdentity(l);
            SyncStatusInfo localSyncStatusInfo1 = null;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public boolean isSyncActive(Account paramAccount, String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.READ_SYNC_STATS", "no permission to read the sync stats");
        int i = UserId.getCallingUserId();
        long l = clearCallingIdentity();
        try
        {
            SyncManager localSyncManager = getSyncManager();
            if (localSyncManager != null)
            {
                boolean bool2 = localSyncManager.getSyncStorageEngine().isSyncActive(paramAccount, i, paramString);
                bool1 = bool2;
                return bool1;
            }
            restoreCallingIdentity(l);
            boolean bool1 = false;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public boolean isSyncPending(Account paramAccount, String paramString)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.READ_SYNC_STATS", "no permission to read the sync stats");
        int i = UserId.getCallingUserId();
        long l = clearCallingIdentity();
        try
        {
            SyncManager localSyncManager = getSyncManager();
            if (localSyncManager != null)
            {
                boolean bool2 = localSyncManager.getSyncStorageEngine().isSyncPending(paramAccount, i, paramString);
                bool1 = bool2;
                return bool1;
            }
            restoreCallingIdentity(l);
            boolean bool1 = false;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    // ERROR //
    public void notifyChange(Uri paramUri, IContentObserver paramIContentObserver, boolean paramBoolean1, boolean paramBoolean2)
    {
        // Byte code:
        //     0: ldc 16
        //     2: iconst_2
        //     3: invokestatic 269	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
        //     6: ifeq +50 -> 56
        //     9: ldc 16
        //     11: new 271	java/lang/StringBuilder
        //     14: dup
        //     15: invokespecial 272	java/lang/StringBuilder:<init>	()V
        //     18: ldc_w 274
        //     21: invokevirtual 278	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     24: aload_1
        //     25: invokevirtual 281	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     28: ldc_w 283
        //     31: invokevirtual 278	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     34: aload_2
        //     35: invokevirtual 281	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     38: ldc_w 285
        //     41: invokevirtual 278	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     44: iload 4
        //     46: invokevirtual 288	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
        //     49: invokevirtual 292	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     52: invokestatic 296	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
        //     55: pop
        //     56: invokestatic 94	android/os/UserId:getCallingUserId	()I
        //     59: istore 5
        //     61: invokestatic 98	android/content/ContentService:clearCallingIdentity	()J
        //     64: lstore 6
        //     66: new 154	java/util/ArrayList
        //     69: dup
        //     70: invokespecial 155	java/util/ArrayList:<init>	()V
        //     73: astore 8
        //     75: aload_0
        //     76: getfield 38	android/content/ContentService:mRootNode	Landroid/content/ContentService$ObserverNode;
        //     79: astore 10
        //     81: aload 10
        //     83: monitorenter
        //     84: aload_0
        //     85: getfield 38	android/content/ContentService:mRootNode	Landroid/content/ContentService$ObserverNode;
        //     88: aload_1
        //     89: iconst_0
        //     90: aload_2
        //     91: iload_3
        //     92: aload 8
        //     94: invokevirtual 300	android/content/ContentService$ObserverNode:collectObserversLocked	(Landroid/net/Uri;ILandroid/database/IContentObserver;ZLjava/util/ArrayList;)V
        //     97: aload 10
        //     99: monitorexit
        //     100: aload 8
        //     102: invokevirtual 185	java/util/ArrayList:size	()I
        //     105: istore 12
        //     107: iconst_0
        //     108: istore 13
        //     110: iload 13
        //     112: iload 12
        //     114: if_icmpge +223 -> 337
        //     117: aload 8
        //     119: iload 13
        //     121: invokevirtual 189	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     124: checkcast 11	android/content/ContentService$ObserverCall
        //     127: astore 15
        //     129: aload 15
        //     131: getfield 304	android/content/ContentService$ObserverCall:mObserver	Landroid/database/IContentObserver;
        //     134: aload 15
        //     136: getfield 307	android/content/ContentService$ObserverCall:mSelfChange	Z
        //     139: aload_1
        //     140: invokeinterface 313 3 0
        //     145: ldc 16
        //     147: iconst_2
        //     148: invokestatic 269	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
        //     151: ifeq +49 -> 200
        //     154: ldc 16
        //     156: new 271	java/lang/StringBuilder
        //     159: dup
        //     160: invokespecial 272	java/lang/StringBuilder:<init>	()V
        //     163: ldc_w 315
        //     166: invokevirtual 278	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     169: aload 15
        //     171: getfield 304	android/content/ContentService$ObserverCall:mObserver	Landroid/database/IContentObserver;
        //     174: invokevirtual 281	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     177: ldc_w 317
        //     180: invokevirtual 278	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     183: ldc_w 319
        //     186: invokevirtual 278	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     189: aload_1
        //     190: invokevirtual 281	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     193: invokevirtual 292	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     196: invokestatic 296	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
        //     199: pop
        //     200: iinc 13 1
        //     203: goto -93 -> 110
        //     206: astore 11
        //     208: aload 10
        //     210: monitorexit
        //     211: aload 11
        //     213: athrow
        //     214: astore 9
        //     216: lload 6
        //     218: invokestatic 113	android/content/ContentService:restoreCallingIdentity	(J)V
        //     221: aload 9
        //     223: athrow
        //     224: astore 16
        //     226: aload_0
        //     227: getfield 38	android/content/ContentService:mRootNode	Landroid/content/ContentService$ObserverNode;
        //     230: astore 17
        //     232: aload 17
        //     234: monitorenter
        //     235: ldc 16
        //     237: ldc_w 321
        //     240: invokestatic 324	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     243: pop
        //     244: aload 15
        //     246: getfield 304	android/content/ContentService$ObserverCall:mObserver	Landroid/database/IContentObserver;
        //     249: invokeinterface 328 1 0
        //     254: astore 20
        //     256: aload 15
        //     258: getfield 331	android/content/ContentService$ObserverCall:mNode	Landroid/content/ContentService$ObserverNode;
        //     261: invokestatic 335	android/content/ContentService$ObserverNode:access$000	(Landroid/content/ContentService$ObserverNode;)Ljava/util/ArrayList;
        //     264: astore 21
        //     266: aload 21
        //     268: invokevirtual 185	java/util/ArrayList:size	()I
        //     271: istore 22
        //     273: iconst_0
        //     274: istore 23
        //     276: iload 23
        //     278: iload 22
        //     280: if_icmpge +43 -> 323
        //     283: aload 21
        //     285: iload 23
        //     287: invokevirtual 189	java/util/ArrayList:get	(I)Ljava/lang/Object;
        //     290: checkcast 337	android/content/ContentService$ObserverNode$ObserverEntry
        //     293: getfield 340	android/content/ContentService$ObserverNode$ObserverEntry:observer	Landroid/database/IContentObserver;
        //     296: invokeinterface 328 1 0
        //     301: aload 20
        //     303: if_acmpne +68 -> 371
        //     306: aload 21
        //     308: iload 23
        //     310: invokevirtual 343	java/util/ArrayList:remove	(I)Ljava/lang/Object;
        //     313: pop
        //     314: iinc 23 255
        //     317: iinc 22 255
        //     320: goto +51 -> 371
        //     323: aload 17
        //     325: monitorexit
        //     326: goto -126 -> 200
        //     329: astore 18
        //     331: aload 17
        //     333: monitorexit
        //     334: aload 18
        //     336: athrow
        //     337: iload 4
        //     339: ifeq +26 -> 365
        //     342: aload_0
        //     343: invokespecial 100	android/content/ContentService:getSyncManager	()Landroid/content/SyncManager;
        //     346: astore 14
        //     348: aload 14
        //     350: ifnull +15 -> 365
        //     353: aload 14
        //     355: aconst_null
        //     356: iload 5
        //     358: aload_1
        //     359: invokevirtual 348	android/net/Uri:getAuthority	()Ljava/lang/String;
        //     362: invokevirtual 351	android/content/SyncManager:scheduleLocalSync	(Landroid/accounts/Account;ILjava/lang/String;)V
        //     365: lload 6
        //     367: invokestatic 113	android/content/ContentService:restoreCallingIdentity	(J)V
        //     370: return
        //     371: iinc 23 1
        //     374: goto -98 -> 276
        //
        // Exception table:
        //     from	to	target	type
        //     84	100	206	finally
        //     208	211	206	finally
        //     66	84	214	finally
        //     100	129	214	finally
        //     129	200	214	finally
        //     211	214	214	finally
        //     226	235	214	finally
        //     334	365	214	finally
        //     129	200	224	android/os/RemoteException
        //     235	334	329	finally
    }

    public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
        throws RemoteException
    {
        try
        {
            boolean bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            return bool;
        }
        catch (RuntimeException localRuntimeException)
        {
            if (!(localRuntimeException instanceof SecurityException))
                Log.e("ContentService", "Content Service Crash", localRuntimeException);
            throw localRuntimeException;
        }
    }

    public void registerContentObserver(Uri paramUri, boolean paramBoolean, IContentObserver paramIContentObserver)
    {
        if ((paramIContentObserver == null) || (paramUri == null))
            throw new IllegalArgumentException("You must pass a valid uri and observer");
        synchronized (this.mRootNode)
        {
            this.mRootNode.addObserverLocked(paramUri, paramIContentObserver, paramBoolean, this.mRootNode, Binder.getCallingUid(), Binder.getCallingPid());
            return;
        }
    }

    public void removePeriodicSync(Account paramAccount, String paramString, Bundle paramBundle)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.WRITE_SYNC_SETTINGS", "no permission to write the sync settings");
        int i = UserId.getCallingUserId();
        long l = clearCallingIdentity();
        try
        {
            getSyncManager().getSyncStorageEngine().removePeriodicSync(paramAccount, i, paramString, paramBundle);
            return;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public void removeStatusChangeListener(ISyncStatusObserver paramISyncStatusObserver)
    {
        long l = clearCallingIdentity();
        try
        {
            SyncManager localSyncManager = getSyncManager();
            if ((localSyncManager != null) && (paramISyncStatusObserver != null))
                localSyncManager.getSyncStorageEngine().removeStatusChangeListener(paramISyncStatusObserver);
            return;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public void requestSync(Account paramAccount, String paramString, Bundle paramBundle)
    {
        ContentResolver.validateSyncExtrasBundle(paramBundle);
        int i = UserId.getCallingUserId();
        long l = clearCallingIdentity();
        try
        {
            SyncManager localSyncManager = getSyncManager();
            if (localSyncManager != null)
                localSyncManager.scheduleSync(paramAccount, i, paramString, paramBundle, 0L, false);
            return;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public void setIsSyncable(Account paramAccount, String paramString, int paramInt)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.WRITE_SYNC_SETTINGS", "no permission to write the sync settings");
        int i = UserId.getCallingUserId();
        long l = clearCallingIdentity();
        try
        {
            SyncManager localSyncManager = getSyncManager();
            if (localSyncManager != null)
                localSyncManager.getSyncStorageEngine().setIsSyncable(paramAccount, i, paramString, paramInt);
            return;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public void setMasterSyncAutomatically(boolean paramBoolean)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.WRITE_SYNC_SETTINGS", "no permission to write the sync settings");
        int i = UserId.getCallingUserId();
        long l = clearCallingIdentity();
        try
        {
            SyncManager localSyncManager = getSyncManager();
            if (localSyncManager != null)
                localSyncManager.getSyncStorageEngine().setMasterSyncAutomatically(paramBoolean, i);
            return;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public void setSyncAutomatically(Account paramAccount, String paramString, boolean paramBoolean)
    {
        this.mContext.enforceCallingOrSelfPermission("android.permission.WRITE_SYNC_SETTINGS", "no permission to write the sync settings");
        int i = UserId.getCallingUserId();
        long l = clearCallingIdentity();
        try
        {
            SyncManager localSyncManager = getSyncManager();
            if (localSyncManager != null)
                localSyncManager.getSyncStorageEngine().setSyncAutomatically(paramAccount, i, paramString, paramBoolean);
            return;
        }
        finally
        {
            restoreCallingIdentity(l);
        }
    }

    public void systemReady()
    {
        getSyncManager();
    }

    public void unregisterContentObserver(IContentObserver paramIContentObserver)
    {
        if (paramIContentObserver == null)
            throw new IllegalArgumentException("You must pass a valid observer");
        synchronized (this.mRootNode)
        {
            this.mRootNode.removeObserverLocked(paramIContentObserver);
            return;
        }
    }

    public static final class ObserverNode
    {
        public static final int DELETE_TYPE = 2;
        public static final int INSERT_TYPE = 0;
        public static final int UPDATE_TYPE = 1;
        private ArrayList<ObserverNode> mChildren = new ArrayList();
        private String mName;
        private ArrayList<ObserverEntry> mObservers = new ArrayList();

        public ObserverNode(String paramString)
        {
            this.mName = paramString;
        }

        private void addObserverLocked(Uri paramUri, int paramInt1, IContentObserver paramIContentObserver, boolean paramBoolean, Object paramObject, int paramInt2, int paramInt3)
        {
            if (paramInt1 == countUriSegments(paramUri))
                this.mObservers.add(new ObserverEntry(paramIContentObserver, paramBoolean, paramObject, paramInt2, paramInt3));
            while (true)
            {
                return;
                String str = getUriSegment(paramUri, paramInt1);
                if (str == null)
                    throw new IllegalArgumentException("Invalid Uri (" + paramUri + ") used for observer");
                int i = this.mChildren.size();
                for (int j = 0; ; j++)
                {
                    if (j >= i)
                        break label153;
                    ObserverNode localObserverNode2 = (ObserverNode)this.mChildren.get(j);
                    if (localObserverNode2.mName.equals(str))
                    {
                        localObserverNode2.addObserverLocked(paramUri, paramInt1 + 1, paramIContentObserver, paramBoolean, paramObject, paramInt2, paramInt3);
                        break;
                    }
                }
                label153: ObserverNode localObserverNode1 = new ObserverNode(str);
                this.mChildren.add(localObserverNode1);
                localObserverNode1.addObserverLocked(paramUri, paramInt1 + 1, paramIContentObserver, paramBoolean, paramObject, paramInt2, paramInt3);
            }
        }

        private void collectMyObserversLocked(boolean paramBoolean1, IContentObserver paramIContentObserver, boolean paramBoolean2, ArrayList<ContentService.ObserverCall> paramArrayList)
        {
            int i = this.mObservers.size();
            IBinder localIBinder;
            int j;
            label19: ObserverEntry localObserverEntry;
            boolean bool;
            if (paramIContentObserver == null)
            {
                localIBinder = null;
                j = 0;
                if (j >= i)
                    return;
                localObserverEntry = (ObserverEntry)this.mObservers.get(j);
                if (localObserverEntry.observer.asBinder() != localIBinder)
                    break label84;
                bool = true;
                label58: if ((!bool) || (paramBoolean2))
                    break label90;
            }
            while (true)
            {
                j++;
                break label19;
                localIBinder = paramIContentObserver.asBinder();
                break;
                label84: bool = false;
                break label58;
                label90: if ((paramBoolean1) || ((!paramBoolean1) && (localObserverEntry.notifyForDescendents)))
                    paramArrayList.add(new ContentService.ObserverCall(this, localObserverEntry.observer, bool));
            }
        }

        private int countUriSegments(Uri paramUri)
        {
            if (paramUri == null);
            for (int i = 0; ; i = 1 + paramUri.getPathSegments().size())
                return i;
        }

        private String getUriSegment(Uri paramUri, int paramInt)
        {
            String str;
            if (paramUri != null)
                if (paramInt == 0)
                    str = paramUri.getAuthority();
            while (true)
            {
                return str;
                str = (String)paramUri.getPathSegments().get(paramInt - 1);
                continue;
                str = null;
            }
        }

        public void addObserverLocked(Uri paramUri, IContentObserver paramIContentObserver, boolean paramBoolean, Object paramObject, int paramInt1, int paramInt2)
        {
            addObserverLocked(paramUri, 0, paramIContentObserver, paramBoolean, paramObject, paramInt1, paramInt2);
        }

        public void collectObserversLocked(Uri paramUri, int paramInt, IContentObserver paramIContentObserver, boolean paramBoolean, ArrayList<ContentService.ObserverCall> paramArrayList)
        {
            Object localObject = null;
            int i = countUriSegments(paramUri);
            int j;
            if (paramInt >= i)
            {
                collectMyObserversLocked(true, paramIContentObserver, paramBoolean, paramArrayList);
                j = this.mChildren.size();
            }
            for (int k = 0; ; k++)
                if (k < j)
                {
                    ObserverNode localObserverNode = (ObserverNode)this.mChildren.get(k);
                    if ((localObject == null) || (localObserverNode.mName.equals(localObject)))
                    {
                        localObserverNode.collectObserversLocked(paramUri, paramInt + 1, paramIContentObserver, paramBoolean, paramArrayList);
                        if (localObject == null);
                    }
                }
                else
                {
                    return;
                    if (paramInt >= i)
                        break;
                    localObject = getUriSegment(paramUri, paramInt);
                    collectMyObserversLocked(false, paramIContentObserver, paramBoolean, paramArrayList);
                    break;
                }
        }

        public void dumpLocked(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString, String paramString1, String paramString2, int[] paramArrayOfInt, SparseIntArray paramSparseIntArray)
        {
            String str = null;
            if (this.mObservers.size() > 0)
            {
                if ("".equals(paramString1));
                for (str = this.mName; ; str = paramString1 + "/" + this.mName)
                    for (int j = 0; j < this.mObservers.size(); j++)
                    {
                        paramArrayOfInt[1] = (1 + paramArrayOfInt[1]);
                        ((ObserverEntry)this.mObservers.get(j)).dumpLocked(paramFileDescriptor, paramPrintWriter, paramArrayOfString, str, paramString2, paramSparseIntArray);
                    }
            }
            if (this.mChildren.size() > 0)
            {
                if (str == null)
                    if (!"".equals(paramString1))
                        break label204;
                label204: for (str = this.mName; ; str = paramString1 + "/" + this.mName)
                    for (int i = 0; i < this.mChildren.size(); i++)
                    {
                        paramArrayOfInt[0] = (1 + paramArrayOfInt[0]);
                        ((ObserverNode)this.mChildren.get(i)).dumpLocked(paramFileDescriptor, paramPrintWriter, paramArrayOfString, str, paramString2, paramArrayOfInt, paramSparseIntArray);
                    }
            }
        }

        public boolean removeObserverLocked(IContentObserver paramIContentObserver)
        {
            int i = this.mChildren.size();
            for (int j = 0; j < i; j++)
                if (((ObserverNode)this.mChildren.get(j)).removeObserverLocked(paramIContentObserver))
                {
                    this.mChildren.remove(j);
                    j--;
                    i--;
                }
            IBinder localIBinder = paramIContentObserver.asBinder();
            int k = this.mObservers.size();
            int m = 0;
            if (m < k)
            {
                ObserverEntry localObserverEntry = (ObserverEntry)this.mObservers.get(m);
                if (localObserverEntry.observer.asBinder() == localIBinder)
                {
                    this.mObservers.remove(m);
                    localIBinder.unlinkToDeath(localObserverEntry, 0);
                }
            }
            else
            {
                if ((this.mChildren.size() != 0) || (this.mObservers.size() != 0))
                    break label163;
            }
            label163: for (boolean bool = true; ; bool = false)
            {
                return bool;
                m++;
                break;
            }
        }

        private class ObserverEntry
            implements IBinder.DeathRecipient
        {
            public final boolean notifyForDescendents;
            public final IContentObserver observer;
            private final Object observersLock;
            public final int pid;
            public final int uid;

            public ObserverEntry(IContentObserver paramBoolean, boolean paramObject, Object paramInt1, int paramInt2, int arg6)
            {
                this.observersLock = paramInt1;
                this.observer = paramBoolean;
                this.uid = paramInt2;
                int i;
                this.pid = i;
                this.notifyForDescendents = paramObject;
                try
                {
                    this.observer.asBinder().linkToDeath(this, 0);
                    return;
                }
                catch (RemoteException localRemoteException)
                {
                    while (true)
                        binderDied();
                }
            }

            public void binderDied()
            {
                synchronized (this.observersLock)
                {
                    ContentService.ObserverNode.this.removeObserverLocked(this.observer);
                    return;
                }
            }

            public void dumpLocked(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString, String paramString1, String paramString2, SparseIntArray paramSparseIntArray)
            {
                paramSparseIntArray.put(this.pid, 1 + paramSparseIntArray.get(this.pid));
                paramPrintWriter.print(paramString2);
                paramPrintWriter.print(paramString1);
                paramPrintWriter.print(": pid=");
                paramPrintWriter.print(this.pid);
                paramPrintWriter.print(" uid=");
                paramPrintWriter.print(this.uid);
                paramPrintWriter.print(" target=");
                if (this.observer != null);
                for (IBinder localIBinder = this.observer.asBinder(); ; localIBinder = null)
                {
                    paramPrintWriter.println(Integer.toHexString(System.identityHashCode(localIBinder)));
                    return;
                }
            }
        }
    }

    public static final class ObserverCall
    {
        final ContentService.ObserverNode mNode;
        final IContentObserver mObserver;
        final boolean mSelfChange;

        ObserverCall(ContentService.ObserverNode paramObserverNode, IContentObserver paramIContentObserver, boolean paramBoolean)
        {
            this.mNode = paramObserverNode;
            this.mObserver = paramIContentObserver;
            this.mSelfChange = paramBoolean;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.ContentService
 * JD-Core Version:        0.6.2
 */