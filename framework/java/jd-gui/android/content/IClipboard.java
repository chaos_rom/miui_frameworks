package android.content;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface IClipboard extends IInterface
{
    public abstract void addPrimaryClipChangedListener(IOnPrimaryClipChangedListener paramIOnPrimaryClipChangedListener)
        throws RemoteException;

    public abstract ClipData getPrimaryClip(String paramString)
        throws RemoteException;

    public abstract ClipDescription getPrimaryClipDescription()
        throws RemoteException;

    public abstract boolean hasClipboardText()
        throws RemoteException;

    public abstract boolean hasPrimaryClip()
        throws RemoteException;

    public abstract void removePrimaryClipChangedListener(IOnPrimaryClipChangedListener paramIOnPrimaryClipChangedListener)
        throws RemoteException;

    public abstract void setPrimaryClip(ClipData paramClipData)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IClipboard
    {
        private static final String DESCRIPTOR = "android.content.IClipboard";
        static final int TRANSACTION_addPrimaryClipChangedListener = 5;
        static final int TRANSACTION_getPrimaryClip = 2;
        static final int TRANSACTION_getPrimaryClipDescription = 3;
        static final int TRANSACTION_hasClipboardText = 7;
        static final int TRANSACTION_hasPrimaryClip = 4;
        static final int TRANSACTION_removePrimaryClipChangedListener = 6;
        static final int TRANSACTION_setPrimaryClip = 1;

        public Stub()
        {
            attachInterface(this, "android.content.IClipboard");
        }

        public static IClipboard asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.content.IClipboard");
                if ((localIInterface != null) && ((localIInterface instanceof IClipboard)))
                    localObject = (IClipboard)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            int i = 0;
            int j = 1;
            switch (paramInt1)
            {
            default:
                j = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            }
            while (true)
            {
                return j;
                paramParcel2.writeString("android.content.IClipboard");
                continue;
                paramParcel1.enforceInterface("android.content.IClipboard");
                if (paramParcel1.readInt() != 0);
                for (ClipData localClipData2 = (ClipData)ClipData.CREATOR.createFromParcel(paramParcel1); ; localClipData2 = null)
                {
                    setPrimaryClip(localClipData2);
                    paramParcel2.writeNoException();
                    break;
                }
                paramParcel1.enforceInterface("android.content.IClipboard");
                ClipData localClipData1 = getPrimaryClip(paramParcel1.readString());
                paramParcel2.writeNoException();
                if (localClipData1 != null)
                {
                    paramParcel2.writeInt(j);
                    localClipData1.writeToParcel(paramParcel2, j);
                }
                else
                {
                    paramParcel2.writeInt(0);
                    continue;
                    paramParcel1.enforceInterface("android.content.IClipboard");
                    ClipDescription localClipDescription = getPrimaryClipDescription();
                    paramParcel2.writeNoException();
                    if (localClipDescription != null)
                    {
                        paramParcel2.writeInt(j);
                        localClipDescription.writeToParcel(paramParcel2, j);
                    }
                    else
                    {
                        paramParcel2.writeInt(0);
                        continue;
                        paramParcel1.enforceInterface("android.content.IClipboard");
                        boolean bool2 = hasPrimaryClip();
                        paramParcel2.writeNoException();
                        if (bool2)
                            i = j;
                        paramParcel2.writeInt(i);
                        continue;
                        paramParcel1.enforceInterface("android.content.IClipboard");
                        addPrimaryClipChangedListener(IOnPrimaryClipChangedListener.Stub.asInterface(paramParcel1.readStrongBinder()));
                        paramParcel2.writeNoException();
                        continue;
                        paramParcel1.enforceInterface("android.content.IClipboard");
                        removePrimaryClipChangedListener(IOnPrimaryClipChangedListener.Stub.asInterface(paramParcel1.readStrongBinder()));
                        paramParcel2.writeNoException();
                        continue;
                        paramParcel1.enforceInterface("android.content.IClipboard");
                        boolean bool1 = hasClipboardText();
                        paramParcel2.writeNoException();
                        if (bool1)
                            i = j;
                        paramParcel2.writeInt(i);
                    }
                }
            }
        }

        private static class Proxy
            implements IClipboard
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public void addPrimaryClipChangedListener(IOnPrimaryClipChangedListener paramIOnPrimaryClipChangedListener)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.IClipboard");
                    if (paramIOnPrimaryClipChangedListener != null)
                    {
                        localIBinder = paramIOnPrimaryClipChangedListener.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(5, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.content.IClipboard";
            }

            public ClipData getPrimaryClip(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.IClipboard");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localClipData = (ClipData)ClipData.CREATOR.createFromParcel(localParcel2);
                        return localClipData;
                    }
                    ClipData localClipData = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public ClipDescription getPrimaryClipDescription()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.IClipboard");
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localClipDescription = (ClipDescription)ClipDescription.CREATOR.createFromParcel(localParcel2);
                        return localClipDescription;
                    }
                    ClipDescription localClipDescription = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean hasClipboardText()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.IClipboard");
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean hasPrimaryClip()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.IClipboard");
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void removePrimaryClipChangedListener(IOnPrimaryClipChangedListener paramIOnPrimaryClipChangedListener)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.IClipboard");
                    if (paramIOnPrimaryClipChangedListener != null)
                    {
                        localIBinder = paramIOnPrimaryClipChangedListener.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(6, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setPrimaryClip(ClipData paramClipData)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.IClipboard");
                    if (paramClipData != null)
                    {
                        localParcel1.writeInt(1);
                        paramClipData.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(1, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.IClipboard
 * JD-Core Version:        0.6.2
 */