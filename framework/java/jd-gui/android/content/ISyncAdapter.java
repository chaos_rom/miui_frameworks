package android.content;

import android.accounts.Account;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;

public abstract interface ISyncAdapter extends IInterface
{
    public abstract void cancelSync(ISyncContext paramISyncContext)
        throws RemoteException;

    public abstract void initialize(Account paramAccount, String paramString)
        throws RemoteException;

    public abstract void startSync(ISyncContext paramISyncContext, String paramString, Account paramAccount, Bundle paramBundle)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements ISyncAdapter
    {
        private static final String DESCRIPTOR = "android.content.ISyncAdapter";
        static final int TRANSACTION_cancelSync = 2;
        static final int TRANSACTION_initialize = 3;
        static final int TRANSACTION_startSync = 1;

        public Stub()
        {
            attachInterface(this, "android.content.ISyncAdapter");
        }

        public static ISyncAdapter asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.content.ISyncAdapter");
                if ((localIInterface != null) && ((localIInterface instanceof ISyncAdapter)))
                    localObject = (ISyncAdapter)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
                while (true)
                {
                    return bool;
                    paramParcel2.writeString("android.content.ISyncAdapter");
                    continue;
                    paramParcel1.enforceInterface("android.content.ISyncAdapter");
                    ISyncContext localISyncContext = ISyncContext.Stub.asInterface(paramParcel1.readStrongBinder());
                    String str = paramParcel1.readString();
                    Account localAccount2;
                    if (paramParcel1.readInt() != 0)
                    {
                        localAccount2 = (Account)Account.CREATOR.createFromParcel(paramParcel1);
                        label113: if (paramParcel1.readInt() == 0)
                            break label155;
                    }
                    label155: for (Bundle localBundle = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1); ; localBundle = null)
                    {
                        startSync(localISyncContext, str, localAccount2, localBundle);
                        break;
                        localAccount2 = null;
                        break label113;
                    }
                    paramParcel1.enforceInterface("android.content.ISyncAdapter");
                    cancelSync(ISyncContext.Stub.asInterface(paramParcel1.readStrongBinder()));
                }
            case 3:
            }
            paramParcel1.enforceInterface("android.content.ISyncAdapter");
            if (paramParcel1.readInt() != 0);
            for (Account localAccount1 = (Account)Account.CREATOR.createFromParcel(paramParcel1); ; localAccount1 = null)
            {
                initialize(localAccount1, paramParcel1.readString());
                break;
            }
        }

        private static class Proxy
            implements ISyncAdapter
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public void cancelSync(ISyncContext paramISyncContext)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.content.ISyncAdapter");
                    if (paramISyncContext != null)
                        localIBinder = paramISyncContext.asBinder();
                    localParcel.writeStrongBinder(localIBinder);
                    this.mRemote.transact(2, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.content.ISyncAdapter";
            }

            public void initialize(Account paramAccount, String paramString)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.content.ISyncAdapter");
                    if (paramAccount != null)
                    {
                        localParcel.writeInt(1);
                        paramAccount.writeToParcel(localParcel, 0);
                    }
                    while (true)
                    {
                        localParcel.writeString(paramString);
                        this.mRemote.transact(3, localParcel, null, 1);
                        return;
                        localParcel.writeInt(0);
                    }
                }
                finally
                {
                    localParcel.recycle();
                }
            }

            public void startSync(ISyncContext paramISyncContext, String paramString, Account paramAccount, Bundle paramBundle)
                throws RemoteException
            {
                IBinder localIBinder = null;
                Parcel localParcel = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel.writeInterfaceToken("android.content.ISyncAdapter");
                        if (paramISyncContext != null)
                            localIBinder = paramISyncContext.asBinder();
                        localParcel.writeStrongBinder(localIBinder);
                        localParcel.writeString(paramString);
                        if (paramAccount != null)
                        {
                            localParcel.writeInt(1);
                            paramAccount.writeToParcel(localParcel, 0);
                            if (paramBundle != null)
                            {
                                localParcel.writeInt(1);
                                paramBundle.writeToParcel(localParcel, 0);
                                this.mRemote.transact(1, localParcel, null, 1);
                            }
                        }
                        else
                        {
                            localParcel.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel.recycle();
                    }
                    localParcel.writeInt(0);
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.ISyncAdapter
 * JD-Core Version:        0.6.2
 */