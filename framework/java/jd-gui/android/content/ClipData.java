package android.content;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.URLSpan;
import java.util.ArrayList;

public class ClipData
    implements Parcelable
{
    public static final Parcelable.Creator<ClipData> CREATOR = new Parcelable.Creator()
    {
        public ClipData createFromParcel(Parcel paramAnonymousParcel)
        {
            return new ClipData(paramAnonymousParcel);
        }

        public ClipData[] newArray(int paramAnonymousInt)
        {
            return new ClipData[paramAnonymousInt];
        }
    };
    static final String[] MIMETYPES_TEXT_HTML;
    static final String[] MIMETYPES_TEXT_INTENT;
    static final String[] MIMETYPES_TEXT_PLAIN;
    static final String[] MIMETYPES_TEXT_URILIST;
    final ClipDescription mClipDescription;
    final Bitmap mIcon;
    final ArrayList<Item> mItems;

    static
    {
        String[] arrayOfString1 = new String[1];
        arrayOfString1[0] = "text/plain";
        MIMETYPES_TEXT_PLAIN = arrayOfString1;
        String[] arrayOfString2 = new String[1];
        arrayOfString2[0] = "text/html";
        MIMETYPES_TEXT_HTML = arrayOfString2;
        String[] arrayOfString3 = new String[1];
        arrayOfString3[0] = "text/uri-list";
        MIMETYPES_TEXT_URILIST = arrayOfString3;
        String[] arrayOfString4 = new String[1];
        arrayOfString4[0] = "text/vnd.android.intent";
        MIMETYPES_TEXT_INTENT = arrayOfString4;
    }

    public ClipData(ClipData paramClipData)
    {
        this.mClipDescription = paramClipData.mClipDescription;
        this.mIcon = paramClipData.mIcon;
        this.mItems = new ArrayList(paramClipData.mItems);
    }

    public ClipData(ClipDescription paramClipDescription, Item paramItem)
    {
        this.mClipDescription = paramClipDescription;
        if (paramItem == null)
            throw new NullPointerException("item is null");
        this.mIcon = null;
        this.mItems = new ArrayList();
        this.mItems.add(paramItem);
    }

    ClipData(Parcel paramParcel)
    {
        this.mClipDescription = new ClipDescription(paramParcel);
        int j;
        label57: CharSequence localCharSequence;
        String str;
        Intent localIntent;
        if (paramParcel.readInt() != 0)
        {
            this.mIcon = ((Bitmap)Bitmap.CREATOR.createFromParcel(paramParcel));
            this.mItems = new ArrayList();
            int i = paramParcel.readInt();
            j = 0;
            if (j >= i)
                return;
            localCharSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
            str = paramParcel.readString();
            if (paramParcel.readInt() == 0)
                break label161;
            localIntent = (Intent)Intent.CREATOR.createFromParcel(paramParcel);
            label103: if (paramParcel.readInt() == 0)
                break label167;
        }
        label161: label167: for (Uri localUri = (Uri)Uri.CREATOR.createFromParcel(paramParcel); ; localUri = null)
        {
            this.mItems.add(new Item(localCharSequence, str, localIntent, localUri));
            j++;
            break label57;
            this.mIcon = null;
            break;
            localIntent = null;
            break label103;
        }
    }

    public ClipData(CharSequence paramCharSequence, String[] paramArrayOfString, Item paramItem)
    {
        this.mClipDescription = new ClipDescription(paramCharSequence, paramArrayOfString);
        if (paramItem == null)
            throw new NullPointerException("item is null");
        this.mIcon = null;
        this.mItems = new ArrayList();
        this.mItems.add(paramItem);
    }

    public static ClipData newHtmlText(CharSequence paramCharSequence1, CharSequence paramCharSequence2, String paramString)
    {
        Item localItem = new Item(paramCharSequence2, paramString);
        return new ClipData(paramCharSequence1, MIMETYPES_TEXT_HTML, localItem);
    }

    public static ClipData newIntent(CharSequence paramCharSequence, Intent paramIntent)
    {
        Item localItem = new Item(paramIntent);
        return new ClipData(paramCharSequence, MIMETYPES_TEXT_INTENT, localItem);
    }

    public static ClipData newPlainText(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
    {
        Item localItem = new Item(paramCharSequence2);
        return new ClipData(paramCharSequence1, MIMETYPES_TEXT_PLAIN, localItem);
    }

    public static ClipData newRawUri(CharSequence paramCharSequence, Uri paramUri)
    {
        Item localItem = new Item(paramUri);
        return new ClipData(paramCharSequence, MIMETYPES_TEXT_URILIST, localItem);
    }

    public static ClipData newUri(ContentResolver paramContentResolver, CharSequence paramCharSequence, Uri paramUri)
    {
        int i = 2;
        Item localItem = new Item(paramUri);
        Object localObject = null;
        String str;
        if ("content".equals(paramUri.getScheme()))
        {
            str = paramContentResolver.getType(paramUri);
            localObject = paramContentResolver.getStreamTypes(paramUri, "*/*");
            if (localObject != null)
                break label94;
            if (str != null)
            {
                localObject = new String[i];
                localObject[0] = str;
                localObject[1] = "text/uri-list";
            }
        }
        if (localObject == null)
            localObject = MIMETYPES_TEXT_URILIST;
        return new ClipData(paramCharSequence, (String[])localObject, localItem);
        label94: int j = localObject.length;
        if (str != null);
        while (true)
        {
            String[] arrayOfString = new String[i + j];
            int k = 0;
            if (str != null)
            {
                arrayOfString[0] = str;
                k = 0 + 1;
            }
            System.arraycopy(localObject, 0, arrayOfString, k, localObject.length);
            arrayOfString[(k + localObject.length)] = "text/uri-list";
            localObject = arrayOfString;
            break;
            i = 1;
        }
    }

    public void addItem(Item paramItem)
    {
        if (paramItem == null)
            throw new NullPointerException("item is null");
        this.mItems.add(paramItem);
    }

    public int describeContents()
    {
        return 0;
    }

    public ClipDescription getDescription()
    {
        return this.mClipDescription;
    }

    public Bitmap getIcon()
    {
        return this.mIcon;
    }

    public Item getItemAt(int paramInt)
    {
        return (Item)this.mItems.get(paramInt);
    }

    public int getItemCount()
    {
        return this.mItems.size();
    }

    public void toShortString(StringBuilder paramStringBuilder)
    {
        int i;
        if (this.mClipDescription != null)
            if (!this.mClipDescription.toShortString(paramStringBuilder))
                i = 1;
        while (true)
        {
            if (this.mIcon != null)
            {
                if (i == 0)
                    paramStringBuilder.append(' ');
                i = 0;
                paramStringBuilder.append("I:");
                paramStringBuilder.append(this.mIcon.getWidth());
                paramStringBuilder.append('x');
                paramStringBuilder.append(this.mIcon.getHeight());
            }
            for (int j = 0; j < this.mItems.size(); j++)
            {
                if (i == 0)
                    paramStringBuilder.append(' ');
                i = 0;
                paramStringBuilder.append('{');
                ((Item)this.mItems.get(j)).toShortString(paramStringBuilder);
                paramStringBuilder.append('}');
            }
            i = 0;
            continue;
            i = 1;
        }
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder(128);
        localStringBuilder.append("ClipData { ");
        toShortString(localStringBuilder);
        localStringBuilder.append(" }");
        return localStringBuilder.toString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        this.mClipDescription.writeToParcel(paramParcel, paramInt);
        int j;
        if (this.mIcon != null)
        {
            paramParcel.writeInt(1);
            this.mIcon.writeToParcel(paramParcel, paramInt);
            int i = this.mItems.size();
            paramParcel.writeInt(i);
            j = 0;
            label46: if (j >= i)
                return;
            Item localItem = (Item)this.mItems.get(j);
            TextUtils.writeToParcel(localItem.mText, paramParcel, paramInt);
            paramParcel.writeString(localItem.mHtmlText);
            if (localItem.mIntent == null)
                break label145;
            paramParcel.writeInt(1);
            localItem.mIntent.writeToParcel(paramParcel, paramInt);
            label108: if (localItem.mUri == null)
                break label153;
            paramParcel.writeInt(1);
            localItem.mUri.writeToParcel(paramParcel, paramInt);
        }
        while (true)
        {
            j++;
            break label46;
            paramParcel.writeInt(0);
            break;
            label145: paramParcel.writeInt(0);
            break label108;
            label153: paramParcel.writeInt(0);
        }
    }

    public static class Item
    {
        final String mHtmlText;
        final Intent mIntent;
        final CharSequence mText;
        final Uri mUri;

        public Item(Intent paramIntent)
        {
            this.mText = null;
            this.mHtmlText = null;
            this.mIntent = paramIntent;
            this.mUri = null;
        }

        public Item(Uri paramUri)
        {
            this.mText = null;
            this.mHtmlText = null;
            this.mIntent = null;
            this.mUri = paramUri;
        }

        public Item(CharSequence paramCharSequence)
        {
            this.mText = paramCharSequence;
            this.mHtmlText = null;
            this.mIntent = null;
            this.mUri = null;
        }

        public Item(CharSequence paramCharSequence, Intent paramIntent, Uri paramUri)
        {
            this.mText = paramCharSequence;
            this.mHtmlText = null;
            this.mIntent = paramIntent;
            this.mUri = paramUri;
        }

        public Item(CharSequence paramCharSequence, String paramString)
        {
            this.mText = paramCharSequence;
            this.mHtmlText = paramString;
            this.mIntent = null;
            this.mUri = null;
        }

        public Item(CharSequence paramCharSequence, String paramString, Intent paramIntent, Uri paramUri)
        {
            if ((paramString != null) && (paramCharSequence == null))
                throw new IllegalArgumentException("Plain text must be supplied if HTML text is supplied");
            this.mText = paramCharSequence;
            this.mHtmlText = paramString;
            this.mIntent = paramIntent;
            this.mUri = paramUri;
        }

        // ERROR //
        private CharSequence coerceToHtmlOrStyledText(Context paramContext, boolean paramBoolean)
        {
            // Byte code:
            //     0: aload_0
            //     1: getfield 28	android/content/ClipData$Item:mUri	Landroid/net/Uri;
            //     4: ifnull +457 -> 461
            //     7: aload_1
            //     8: invokevirtual 54	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
            //     11: aload_0
            //     12: getfield 28	android/content/ClipData$Item:mUri	Landroid/net/Uri;
            //     15: ldc 56
            //     17: invokevirtual 62	android/content/ContentResolver:getStreamTypes	(Landroid/net/Uri;Ljava/lang/String;)[Ljava/lang/String;
            //     20: astore 4
            //     22: iconst_0
            //     23: istore 5
            //     25: iconst_0
            //     26: istore 6
            //     28: aload 4
            //     30: ifnull +60 -> 90
            //     33: aload 4
            //     35: arraylength
            //     36: istore 35
            //     38: iconst_0
            //     39: istore 36
            //     41: iload 36
            //     43: iload 35
            //     45: if_icmpge +45 -> 90
            //     48: aload 4
            //     50: iload 36
            //     52: aaload
            //     53: astore 37
            //     55: ldc 64
            //     57: aload 37
            //     59: invokevirtual 70	java/lang/String:equals	(Ljava/lang/Object;)Z
            //     62: ifeq +12 -> 74
            //     65: iconst_1
            //     66: istore 5
            //     68: iinc 36 1
            //     71: goto -30 -> 41
            //     74: aload 37
            //     76: ldc 72
            //     78: invokevirtual 76	java/lang/String:startsWith	(Ljava/lang/String;)Z
            //     81: ifeq -13 -> 68
            //     84: iconst_1
            //     85: istore 6
            //     87: goto -19 -> 68
            //     90: iload 5
            //     92: ifne +8 -> 100
            //     95: iload 6
            //     97: ifeq +114 -> 211
            //     100: aconst_null
            //     101: astore 7
            //     103: aload_1
            //     104: invokevirtual 54	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
            //     107: astore 16
            //     109: aload_0
            //     110: getfield 28	android/content/ClipData$Item:mUri	Landroid/net/Uri;
            //     113: astore 17
            //     115: iload 5
            //     117: ifeq +112 -> 229
            //     120: ldc 64
            //     122: astore 18
            //     124: aload 16
            //     126: aload 17
            //     128: aload 18
            //     130: aconst_null
            //     131: invokevirtual 80	android/content/ContentResolver:openTypedAssetFileDescriptor	(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/res/AssetFileDescriptor;
            //     134: invokevirtual 86	android/content/res/AssetFileDescriptor:createInputStream	()Ljava/io/FileInputStream;
            //     137: astore 7
            //     139: new 88	java/io/InputStreamReader
            //     142: dup
            //     143: aload 7
            //     145: ldc 90
            //     147: invokespecial 93	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;Ljava/lang/String;)V
            //     150: astore 19
            //     152: new 95	java/lang/StringBuilder
            //     155: dup
            //     156: sipush 128
            //     159: invokespecial 98	java/lang/StringBuilder:<init>	(I)V
            //     162: astore 20
            //     164: sipush 8192
            //     167: newarray char
            //     169: astore 21
            //     171: aload 19
            //     173: aload 21
            //     175: invokevirtual 102	java/io/InputStreamReader:read	([C)I
            //     178: istore 22
            //     180: iload 22
            //     182: ifle +54 -> 236
            //     185: aload 20
            //     187: aload 21
            //     189: iconst_0
            //     190: iload 22
            //     192: invokevirtual 106	java/lang/StringBuilder:append	([CII)Ljava/lang/StringBuilder;
            //     195: pop
            //     196: goto -25 -> 171
            //     199: astore 14
            //     201: aload 7
            //     203: ifnull +8 -> 211
            //     206: aload 7
            //     208: invokevirtual 111	java/io/FileInputStream:close	()V
            //     211: iload_2
            //     212: ifeq +234 -> 446
            //     215: aload_0
            //     216: aload_0
            //     217: getfield 28	android/content/ClipData$Item:mUri	Landroid/net/Uri;
            //     220: invokevirtual 117	android/net/Uri:toString	()Ljava/lang/String;
            //     223: invokespecial 121	android/content/ClipData$Item:uriToStyledText	(Ljava/lang/String;)Ljava/lang/CharSequence;
            //     226: astore_3
            //     227: aload_3
            //     228: areturn
            //     229: ldc 123
            //     231: astore 18
            //     233: goto -109 -> 124
            //     236: aload 20
            //     238: invokevirtual 124	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     241: astore 23
            //     243: aload 23
            //     245: astore_3
            //     246: iload 5
            //     248: ifeq +91 -> 339
            //     251: iload_2
            //     252: ifeq +60 -> 312
            //     255: aload_3
            //     256: invokestatic 130	android/text/Html:fromHtml	(Ljava/lang/String;)Landroid/text/Spanned;
            //     259: astore 31
            //     261: aload 31
            //     263: astore 32
            //     265: aload 32
            //     267: ifnull +19 -> 286
            //     270: aload 7
            //     272: ifnull +8 -> 280
            //     275: aload 7
            //     277: invokevirtual 111	java/io/FileInputStream:close	()V
            //     280: aload 32
            //     282: astore_3
            //     283: goto -56 -> 227
            //     286: aload_3
            //     287: astore 32
            //     289: goto -19 -> 270
            //     292: astore 29
            //     294: aload 7
            //     296: ifnull -69 -> 227
            //     299: aload 7
            //     301: invokevirtual 111	java/io/FileInputStream:close	()V
            //     304: goto -77 -> 227
            //     307: astore 30
            //     309: goto -82 -> 227
            //     312: aload_3
            //     313: invokevirtual 131	java/lang/String:toString	()Ljava/lang/String;
            //     316: astore 27
            //     318: aload 27
            //     320: astore_3
            //     321: aload 7
            //     323: ifnull -96 -> 227
            //     326: aload 7
            //     328: invokevirtual 111	java/io/FileInputStream:close	()V
            //     331: goto -104 -> 227
            //     334: astore 28
            //     336: goto -109 -> 227
            //     339: iload_2
            //     340: ifeq +21 -> 361
            //     343: aload 7
            //     345: ifnull -118 -> 227
            //     348: aload 7
            //     350: invokevirtual 111	java/io/FileInputStream:close	()V
            //     353: goto -126 -> 227
            //     356: astore 26
            //     358: goto -131 -> 227
            //     361: aload_3
            //     362: invokestatic 135	android/text/Html:escapeHtml	(Ljava/lang/CharSequence;)Ljava/lang/String;
            //     365: astore 24
            //     367: aload 24
            //     369: astore_3
            //     370: aload 7
            //     372: ifnull -145 -> 227
            //     375: aload 7
            //     377: invokevirtual 111	java/io/FileInputStream:close	()V
            //     380: goto -153 -> 227
            //     383: astore 25
            //     385: goto -158 -> 227
            //     388: astore 10
            //     390: ldc 137
            //     392: ldc 139
            //     394: aload 10
            //     396: invokestatic 145	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     399: pop
            //     400: aload 10
            //     402: invokevirtual 146	java/io/IOException:toString	()Ljava/lang/String;
            //     405: invokestatic 135	android/text/Html:escapeHtml	(Ljava/lang/CharSequence;)Ljava/lang/String;
            //     408: astore 12
            //     410: aload 12
            //     412: astore_3
            //     413: aload 7
            //     415: ifnull -188 -> 227
            //     418: aload 7
            //     420: invokevirtual 111	java/io/FileInputStream:close	()V
            //     423: goto -196 -> 227
            //     426: astore 13
            //     428: goto -201 -> 227
            //     431: astore 8
            //     433: aload 7
            //     435: ifnull +8 -> 443
            //     438: aload 7
            //     440: invokevirtual 111	java/io/FileInputStream:close	()V
            //     443: aload 8
            //     445: athrow
            //     446: aload_0
            //     447: aload_0
            //     448: getfield 28	android/content/ClipData$Item:mUri	Landroid/net/Uri;
            //     451: invokevirtual 117	android/net/Uri:toString	()Ljava/lang/String;
            //     454: invokespecial 150	android/content/ClipData$Item:uriToHtml	(Ljava/lang/String;)Ljava/lang/String;
            //     457: astore_3
            //     458: goto -231 -> 227
            //     461: aload_0
            //     462: getfield 26	android/content/ClipData$Item:mIntent	Landroid/content/Intent;
            //     465: ifnull +39 -> 504
            //     468: iload_2
            //     469: ifeq +19 -> 488
            //     472: aload_0
            //     473: aload_0
            //     474: getfield 26	android/content/ClipData$Item:mIntent	Landroid/content/Intent;
            //     477: iconst_1
            //     478: invokevirtual 156	android/content/Intent:toUri	(I)Ljava/lang/String;
            //     481: invokespecial 121	android/content/ClipData$Item:uriToStyledText	(Ljava/lang/String;)Ljava/lang/CharSequence;
            //     484: astore_3
            //     485: goto -258 -> 227
            //     488: aload_0
            //     489: aload_0
            //     490: getfield 26	android/content/ClipData$Item:mIntent	Landroid/content/Intent;
            //     493: iconst_1
            //     494: invokevirtual 156	android/content/Intent:toUri	(I)Ljava/lang/String;
            //     497: invokespecial 150	android/content/ClipData$Item:uriToHtml	(Ljava/lang/String;)Ljava/lang/String;
            //     500: astore_3
            //     501: goto -274 -> 227
            //     504: ldc 158
            //     506: astore_3
            //     507: goto -280 -> 227
            //     510: astore 33
            //     512: goto -232 -> 280
            //     515: astore 15
            //     517: goto -306 -> 211
            //     520: astore 9
            //     522: goto -79 -> 443
            //
            // Exception table:
            //     from	to	target	type
            //     103	196	199	java/io/FileNotFoundException
            //     229	243	199	java/io/FileNotFoundException
            //     255	261	199	java/io/FileNotFoundException
            //     312	318	199	java/io/FileNotFoundException
            //     361	367	199	java/io/FileNotFoundException
            //     255	261	292	java/lang/RuntimeException
            //     299	304	307	java/io/IOException
            //     326	331	334	java/io/IOException
            //     348	353	356	java/io/IOException
            //     375	380	383	java/io/IOException
            //     103	196	388	java/io/IOException
            //     229	243	388	java/io/IOException
            //     255	261	388	java/io/IOException
            //     312	318	388	java/io/IOException
            //     361	367	388	java/io/IOException
            //     418	423	426	java/io/IOException
            //     103	196	431	finally
            //     229	243	431	finally
            //     255	261	431	finally
            //     312	318	431	finally
            //     361	367	431	finally
            //     390	410	431	finally
            //     275	280	510	java/io/IOException
            //     206	211	515	java/io/IOException
            //     438	443	520	java/io/IOException
        }

        private String uriToHtml(String paramString)
        {
            StringBuilder localStringBuilder = new StringBuilder(256);
            localStringBuilder.append("<a href=\"");
            localStringBuilder.append(paramString);
            localStringBuilder.append("\">");
            localStringBuilder.append(Html.escapeHtml(paramString));
            localStringBuilder.append("</a>");
            return localStringBuilder.toString();
        }

        private CharSequence uriToStyledText(String paramString)
        {
            SpannableStringBuilder localSpannableStringBuilder = new SpannableStringBuilder();
            localSpannableStringBuilder.append(paramString);
            localSpannableStringBuilder.setSpan(new URLSpan(paramString), 0, localSpannableStringBuilder.length(), 33);
            return localSpannableStringBuilder;
        }

        public String coerceToHtmlText(Context paramContext)
        {
            Object localObject = getHtmlText();
            if (localObject != null);
            while (true)
            {
                return localObject;
                CharSequence localCharSequence1 = getText();
                if (localCharSequence1 == null)
                    break;
                if ((localCharSequence1 instanceof Spanned))
                    localObject = Html.toHtml((Spanned)localCharSequence1);
                else
                    localObject = Html.escapeHtml(localCharSequence1);
            }
            CharSequence localCharSequence2 = coerceToHtmlOrStyledText(paramContext, false);
            if (localCharSequence2 != null);
            for (String str = localCharSequence2.toString(); ; str = null)
            {
                localObject = str;
                break;
            }
        }

        public CharSequence coerceToStyledText(Context paramContext)
        {
            Object localObject = getText();
            if ((localObject instanceof Spanned));
            while (true)
            {
                return localObject;
                String str = getHtmlText();
                if (str != null)
                    try
                    {
                        Spanned localSpanned = Html.fromHtml(str);
                        if (localSpanned != null)
                            localObject = localSpanned;
                    }
                    catch (RuntimeException localRuntimeException)
                    {
                    }
                else if (localObject == null)
                    localObject = coerceToHtmlOrStyledText(paramContext, true);
            }
        }

        // ERROR //
        public CharSequence coerceToText(Context paramContext)
        {
            // Byte code:
            //     0: aload_0
            //     1: invokevirtual 193	android/content/ClipData$Item:getText	()Ljava/lang/CharSequence;
            //     4: astore_2
            //     5: aload_2
            //     6: ifnull +5 -> 11
            //     9: aload_2
            //     10: areturn
            //     11: aload_0
            //     12: invokevirtual 209	android/content/ClipData$Item:getUri	()Landroid/net/Uri;
            //     15: astore_3
            //     16: aload_3
            //     17: ifnull +185 -> 202
            //     20: aconst_null
            //     21: astore 5
            //     23: aload_1
            //     24: invokevirtual 54	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
            //     27: aload_3
            //     28: ldc 56
            //     30: aconst_null
            //     31: invokevirtual 80	android/content/ContentResolver:openTypedAssetFileDescriptor	(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/res/AssetFileDescriptor;
            //     34: invokevirtual 86	android/content/res/AssetFileDescriptor:createInputStream	()Ljava/io/FileInputStream;
            //     37: astore 5
            //     39: new 88	java/io/InputStreamReader
            //     42: dup
            //     43: aload 5
            //     45: ldc 90
            //     47: invokespecial 93	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;Ljava/lang/String;)V
            //     50: astore 14
            //     52: new 95	java/lang/StringBuilder
            //     55: dup
            //     56: sipush 128
            //     59: invokespecial 98	java/lang/StringBuilder:<init>	(I)V
            //     62: astore 15
            //     64: sipush 8192
            //     67: newarray char
            //     69: astore 16
            //     71: aload 14
            //     73: aload 16
            //     75: invokevirtual 102	java/io/InputStreamReader:read	([C)I
            //     78: istore 17
            //     80: iload 17
            //     82: ifle +37 -> 119
            //     85: aload 15
            //     87: aload 16
            //     89: iconst_0
            //     90: iload 17
            //     92: invokevirtual 106	java/lang/StringBuilder:append	([CII)Ljava/lang/StringBuilder;
            //     95: pop
            //     96: goto -25 -> 71
            //     99: astore 12
            //     101: aload 5
            //     103: ifnull +8 -> 111
            //     106: aload 5
            //     108: invokevirtual 111	java/io/FileInputStream:close	()V
            //     111: aload_3
            //     112: invokevirtual 117	android/net/Uri:toString	()Ljava/lang/String;
            //     115: astore_2
            //     116: goto -107 -> 9
            //     119: aload 15
            //     121: invokevirtual 124	java/lang/StringBuilder:toString	()Ljava/lang/String;
            //     124: astore 18
            //     126: aload 18
            //     128: astore_2
            //     129: aload 5
            //     131: ifnull -122 -> 9
            //     134: aload 5
            //     136: invokevirtual 111	java/io/FileInputStream:close	()V
            //     139: goto -130 -> 9
            //     142: astore 19
            //     144: goto -135 -> 9
            //     147: astore 8
            //     149: ldc 137
            //     151: ldc 139
            //     153: aload 8
            //     155: invokestatic 145	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //     158: pop
            //     159: aload 8
            //     161: invokevirtual 146	java/io/IOException:toString	()Ljava/lang/String;
            //     164: astore 10
            //     166: aload 10
            //     168: astore_2
            //     169: aload 5
            //     171: ifnull -162 -> 9
            //     174: aload 5
            //     176: invokevirtual 111	java/io/FileInputStream:close	()V
            //     179: goto -170 -> 9
            //     182: astore 11
            //     184: goto -175 -> 9
            //     187: astore 6
            //     189: aload 5
            //     191: ifnull +8 -> 199
            //     194: aload 5
            //     196: invokevirtual 111	java/io/FileInputStream:close	()V
            //     199: aload 6
            //     201: athrow
            //     202: aload_0
            //     203: invokevirtual 213	android/content/ClipData$Item:getIntent	()Landroid/content/Intent;
            //     206: astore 4
            //     208: aload 4
            //     210: ifnull +13 -> 223
            //     213: aload 4
            //     215: iconst_1
            //     216: invokevirtual 156	android/content/Intent:toUri	(I)Ljava/lang/String;
            //     219: astore_2
            //     220: goto -211 -> 9
            //     223: ldc 158
            //     225: astore_2
            //     226: goto -217 -> 9
            //     229: astore 13
            //     231: goto -120 -> 111
            //     234: astore 7
            //     236: goto -37 -> 199
            //
            // Exception table:
            //     from	to	target	type
            //     23	96	99	java/io/FileNotFoundException
            //     119	126	99	java/io/FileNotFoundException
            //     134	139	142	java/io/IOException
            //     23	96	147	java/io/IOException
            //     119	126	147	java/io/IOException
            //     174	179	182	java/io/IOException
            //     23	96	187	finally
            //     119	126	187	finally
            //     149	166	187	finally
            //     106	111	229	java/io/IOException
            //     194	199	234	java/io/IOException
        }

        public String getHtmlText()
        {
            return this.mHtmlText;
        }

        public Intent getIntent()
        {
            return this.mIntent;
        }

        public CharSequence getText()
        {
            return this.mText;
        }

        public Uri getUri()
        {
            return this.mUri;
        }

        public void toShortString(StringBuilder paramStringBuilder)
        {
            if (this.mHtmlText != null)
            {
                paramStringBuilder.append("H:");
                paramStringBuilder.append(this.mHtmlText);
            }
            while (true)
            {
                return;
                if (this.mText != null)
                {
                    paramStringBuilder.append("T:");
                    paramStringBuilder.append(this.mText);
                }
                else if (this.mUri != null)
                {
                    paramStringBuilder.append("U:");
                    paramStringBuilder.append(this.mUri);
                }
                else if (this.mIntent != null)
                {
                    paramStringBuilder.append("I:");
                    this.mIntent.toShortString(paramStringBuilder, true, true, true, true);
                }
                else
                {
                    paramStringBuilder.append("NULL");
                }
            }
        }

        public String toString()
        {
            StringBuilder localStringBuilder = new StringBuilder(128);
            localStringBuilder.append("ClipData.Item { ");
            toShortString(localStringBuilder);
            localStringBuilder.append(" }");
            return localStringBuilder.toString();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.ClipData
 * JD-Core Version:        0.6.2
 */