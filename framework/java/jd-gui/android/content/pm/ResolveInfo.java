package android.content.pm;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.MiuiThemeHelper;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.Printer;
import java.text.Collator;
import java.util.Comparator;

public class ResolveInfo
    implements Parcelable
{
    public static final Parcelable.Creator<ResolveInfo> CREATOR = new Parcelable.Creator()
    {
        public ResolveInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            return new ResolveInfo(paramAnonymousParcel, null);
        }

        public ResolveInfo[] newArray(int paramAnonymousInt)
        {
            return new ResolveInfo[paramAnonymousInt];
        }
    };
    public ActivityInfo activityInfo;
    public IntentFilter filter;
    public int icon;
    public boolean isDefault;
    public int labelRes;
    public int match;
    public CharSequence nonLocalizedLabel;
    public int preferredOrder;
    public int priority;
    public String resolvePackageName;
    public ServiceInfo serviceInfo;
    public int specificIndex = -1;
    public boolean system;

    public ResolveInfo()
    {
    }

    private ResolveInfo(Parcel paramParcel)
    {
        switch (paramParcel.readInt())
        {
        default:
            this.activityInfo = null;
            this.serviceInfo = null;
            if (paramParcel.readInt() != 0)
                this.filter = ((IntentFilter)IntentFilter.CREATOR.createFromParcel(paramParcel));
            this.priority = paramParcel.readInt();
            this.preferredOrder = paramParcel.readInt();
            this.match = paramParcel.readInt();
            this.specificIndex = paramParcel.readInt();
            this.labelRes = paramParcel.readInt();
            this.nonLocalizedLabel = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
            this.icon = paramParcel.readInt();
            this.resolvePackageName = paramParcel.readString();
            if (paramParcel.readInt() == 0)
                break;
        case 1:
        case 2:
        }
        for (boolean bool = true; ; bool = false)
        {
            this.system = bool;
            return;
            this.activityInfo = ((ActivityInfo)ActivityInfo.CREATOR.createFromParcel(paramParcel));
            this.serviceInfo = null;
            break;
            this.serviceInfo = ((ServiceInfo)ServiceInfo.CREATOR.createFromParcel(paramParcel));
            this.activityInfo = null;
            break;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public void dump(Printer paramPrinter, String paramString)
    {
        if (this.filter != null)
        {
            paramPrinter.println(paramString + "Filter:");
            this.filter.dump(paramPrinter, paramString + "    ");
        }
        paramPrinter.println(paramString + "priority=" + this.priority + " preferredOrder=" + this.preferredOrder + " match=0x" + Integer.toHexString(this.match) + " specificIndex=" + this.specificIndex + " isDefault=" + this.isDefault);
        if (this.resolvePackageName != null)
            paramPrinter.println(paramString + "resolvePackageName=" + this.resolvePackageName);
        if ((this.labelRes != 0) || (this.nonLocalizedLabel != null) || (this.icon != 0))
            paramPrinter.println(paramString + "labelRes=0x" + Integer.toHexString(this.labelRes) + " nonLocalizedLabel=" + this.nonLocalizedLabel + " icon=0x" + Integer.toHexString(this.icon));
        if (this.activityInfo != null)
        {
            paramPrinter.println(paramString + "ActivityInfo:");
            this.activityInfo.dump(paramPrinter, paramString + "    ");
        }
        while (true)
        {
            return;
            if (this.serviceInfo != null)
            {
                paramPrinter.println(paramString + "ServiceInfo:");
                this.serviceInfo.dump(paramPrinter, paramString + "    ");
            }
        }
    }

    public final int getIconResource()
    {
        int i;
        if (this.icon != 0)
            i = this.icon;
        while (true)
        {
            return i;
            if (this.activityInfo != null)
                i = this.activityInfo.getIconResource();
            else if (this.serviceInfo != null)
                i = this.serviceInfo.getIconResource();
            else
                i = 0;
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public Drawable loadIcon(PackageManager paramPackageManager)
    {
        Drawable localDrawable;
        if ((this.resolvePackageName != null) && (this.icon != 0))
        {
            localDrawable = Injector.getDrawable(this, paramPackageManager, this.resolvePackageName, this.icon, null);
            if (localDrawable != null)
                return localDrawable;
        }
        if (this.activityInfo != null);
        for (Object localObject = this.activityInfo; ; localObject = this.serviceInfo)
        {
            ApplicationInfo localApplicationInfo = ((ComponentInfo)localObject).applicationInfo;
            if (this.icon != 0)
            {
                localDrawable = Injector.getDrawable(this, paramPackageManager, ((PackageItemInfo)localObject).packageName, this.icon, localApplicationInfo);
                if (localDrawable != null)
                    break;
            }
            localDrawable = ((ComponentInfo)localObject).loadIcon(paramPackageManager);
            break;
        }
    }

    public CharSequence loadLabel(PackageManager paramPackageManager)
    {
        Object localObject2;
        if (this.nonLocalizedLabel != null)
            localObject2 = this.nonLocalizedLabel;
        while (true)
        {
            return localObject2;
            if ((this.resolvePackageName != null) && (this.labelRes != 0))
            {
                CharSequence localCharSequence2 = paramPackageManager.getText(this.resolvePackageName, this.labelRes, null);
                if (localCharSequence2 != null)
                    localObject2 = localCharSequence2.toString().trim();
            }
            else
            {
                if (this.activityInfo != null);
                for (Object localObject1 = this.activityInfo; ; localObject1 = this.serviceInfo)
                {
                    ApplicationInfo localApplicationInfo = ((ComponentInfo)localObject1).applicationInfo;
                    if (this.labelRes == 0)
                        break label128;
                    CharSequence localCharSequence1 = paramPackageManager.getText(((PackageItemInfo)localObject1).packageName, this.labelRes, localApplicationInfo);
                    if (localCharSequence1 == null)
                        break label128;
                    localObject2 = localCharSequence1.toString().trim();
                    break;
                }
                label128: localObject2 = ((ComponentInfo)localObject1).loadLabel(paramPackageManager);
                if (localObject2 != null)
                    localObject2 = localObject2.toString().trim();
            }
        }
    }

    public String toString()
    {
        if (this.activityInfo != null);
        for (Object localObject = this.activityInfo; ; localObject = this.serviceInfo)
            return "ResolveInfo{" + Integer.toHexString(System.identityHashCode(this)) + " " + ((PackageItemInfo)localObject).name + " p=" + this.priority + " o=" + this.preferredOrder + " m=0x" + Integer.toHexString(this.match) + "}";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        int i = 1;
        if (this.activityInfo != null)
        {
            paramParcel.writeInt(i);
            this.activityInfo.writeToParcel(paramParcel, paramInt);
            if (this.filter == null)
                break label154;
            paramParcel.writeInt(i);
            this.filter.writeToParcel(paramParcel, paramInt);
            label44: paramParcel.writeInt(this.priority);
            paramParcel.writeInt(this.preferredOrder);
            paramParcel.writeInt(this.match);
            paramParcel.writeInt(this.specificIndex);
            paramParcel.writeInt(this.labelRes);
            TextUtils.writeToParcel(this.nonLocalizedLabel, paramParcel, paramInt);
            paramParcel.writeInt(this.icon);
            paramParcel.writeString(this.resolvePackageName);
            if (!this.system)
                break label162;
        }
        while (true)
        {
            paramParcel.writeInt(i);
            return;
            if (this.serviceInfo != null)
            {
                paramParcel.writeInt(2);
                this.serviceInfo.writeToParcel(paramParcel, paramInt);
                break;
            }
            paramParcel.writeInt(0);
            break;
            label154: paramParcel.writeInt(0);
            break label44;
            label162: i = 0;
        }
    }

    public static class DisplayNameComparator
        implements Comparator<ResolveInfo>
    {
        private PackageManager mPM;
        private final Collator sCollator = Collator.getInstance();

        public DisplayNameComparator(PackageManager paramPackageManager)
        {
            this.mPM = paramPackageManager;
        }

        public final int compare(ResolveInfo paramResolveInfo1, ResolveInfo paramResolveInfo2)
        {
            Object localObject1 = paramResolveInfo1.loadLabel(this.mPM);
            if (localObject1 == null)
                localObject1 = paramResolveInfo1.activityInfo.name;
            Object localObject2 = paramResolveInfo2.loadLabel(this.mPM);
            if (localObject2 == null)
                localObject2 = paramResolveInfo2.activityInfo.name;
            return this.sCollator.compare(localObject1.toString(), localObject2.toString());
        }
    }

    @MiuiHook(MiuiHook.MiuiHookType.NEW_CLASS)
    static class Injector
    {
        static Drawable getDrawable(ResolveInfo paramResolveInfo, PackageManager paramPackageManager, String paramString, int paramInt, ApplicationInfo paramApplicationInfo)
        {
            if (paramResolveInfo.activityInfo != null);
            for (Object localObject = paramResolveInfo.activityInfo; ; localObject = paramResolveInfo.serviceInfo)
                return MiuiThemeHelper.getDrawable(paramPackageManager, paramString, paramInt, paramApplicationInfo, (PackageItemInfo)localObject, MiuiThemeHelper.isCustomizedIcon(paramResolveInfo.filter));
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.ResolveInfo
 * JD-Core Version:        0.6.2
 */