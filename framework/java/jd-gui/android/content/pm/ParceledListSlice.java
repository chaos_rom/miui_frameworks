package android.content.pm;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.List;

public class ParceledListSlice<T extends Parcelable>
    implements Parcelable
{

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public static final Parcelable.Creator<ParceledListSlice> CREATOR = new Parcelable.Creator()
    {
        public ParceledListSlice createFromParcel(Parcel paramAnonymousParcel)
        {
            int i = 1;
            int j = paramAnonymousParcel.readInt();
            ParceledListSlice localParceledListSlice;
            if (paramAnonymousParcel.readInt() == i)
            {
                if (j <= 0)
                    break label89;
                int k = paramAnonymousParcel.readInt();
                int m = paramAnonymousParcel.dataPosition();
                paramAnonymousParcel.setDataPosition(m + k);
                Parcel localParcel = Parcel.obtain();
                localParcel.setDataPosition(0);
                localParcel.appendFrom(paramAnonymousParcel, m, k);
                localParcel.setDataPosition(0);
                localParceledListSlice = new ParceledListSlice(localParcel, j, i, null);
            }
            while (true)
            {
                return localParceledListSlice;
                boolean bool = false;
                break;
                label89: localParceledListSlice = new ParceledListSlice();
                localParceledListSlice.setLastSlice(bool);
            }
        }

        public ParceledListSlice[] newArray(int paramAnonymousInt)
        {
            return new ParceledListSlice[paramAnonymousInt];
        }
    };
    private static final int MAX_IPC_SIZE = 262144;
    private boolean mIsLastSlice;
    private int mNumItems;
    private Parcel mParcel;

    public ParceledListSlice()
    {
        this.mParcel = Parcel.obtain();
    }

    private ParceledListSlice(Parcel paramParcel, int paramInt, boolean paramBoolean)
    {
        this.mParcel = paramParcel;
        this.mNumItems = paramInt;
        this.mIsLastSlice = paramBoolean;
    }

    public boolean append(T paramT)
    {
        int i = 1;
        if (this.mParcel == null)
            throw new IllegalStateException("ParceledListSlice has already been recycled");
        paramT.writeToParcel(this.mParcel, i);
        this.mNumItems = (1 + this.mNumItems);
        if (this.mParcel.dataSize() > 262144);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean isLastSlice()
    {
        return this.mIsLastSlice;
    }

    public T populateList(List<T> paramList, Parcelable.Creator<T> paramCreator)
    {
        this.mParcel.setDataPosition(0);
        Parcelable localParcelable = null;
        for (int i = 0; i < this.mNumItems; i++)
        {
            localParcelable = (Parcelable)paramCreator.createFromParcel(this.mParcel);
            paramList.add(localParcelable);
        }
        this.mParcel.recycle();
        this.mParcel = null;
        return localParcelable;
    }

    public void setLastSlice(boolean paramBoolean)
    {
        this.mIsLastSlice = paramBoolean;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.mNumItems);
        if (this.mIsLastSlice);
        for (int i = 1; ; i = 0)
        {
            paramParcel.writeInt(i);
            if (this.mNumItems > 0)
            {
                int j = this.mParcel.dataSize();
                paramParcel.writeInt(j);
                paramParcel.appendFrom(this.mParcel, 0, j);
            }
            this.mNumItems = 0;
            this.mParcel.recycle();
            this.mParcel = null;
            return;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.ParceledListSlice
 * JD-Core Version:        0.6.2
 */