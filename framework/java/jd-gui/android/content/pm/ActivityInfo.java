package android.content.pm;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Printer;

public class ActivityInfo extends ComponentInfo
    implements Parcelable
{
    public static final int CONFIG_FONT_SCALE = 1073741824;
    public static final int CONFIG_KEYBOARD = 16;
    public static final int CONFIG_KEYBOARD_HIDDEN = 32;
    public static final int CONFIG_LOCALE = 4;
    public static final int CONFIG_MCC = 1;
    public static final int CONFIG_MNC = 2;
    public static int[] CONFIG_NATIVE_BITS = arrayOfInt;
    public static final int CONFIG_NAVIGATION = 64;
    public static final int CONFIG_ORIENTATION = 128;
    public static final int CONFIG_SCREEN_LAYOUT = 256;
    public static final int CONFIG_SCREEN_SIZE = 1024;
    public static final int CONFIG_SMALLEST_SCREEN_SIZE = 2048;
    public static final int CONFIG_TOUCHSCREEN = 8;
    public static final int CONFIG_UI_MODE = 512;
    public static final Parcelable.Creator<ActivityInfo> CREATOR = new Parcelable.Creator()
    {
        public ActivityInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            return new ActivityInfo(paramAnonymousParcel, null);
        }

        public ActivityInfo[] newArray(int paramAnonymousInt)
        {
            return new ActivityInfo[paramAnonymousInt];
        }
    };
    public static final int FLAG_ALLOW_TASK_REPARENTING = 64;
    public static final int FLAG_ALWAYS_RETAIN_TASK_STATE = 8;
    public static final int FLAG_CLEAR_TASK_ON_LAUNCH = 4;
    public static final int FLAG_EXCLUDE_FROM_RECENTS = 32;
    public static final int FLAG_FINISH_ON_CLOSE_SYSTEM_DIALOGS = 256;
    public static final int FLAG_FINISH_ON_TASK_LAUNCH = 2;
    public static final int FLAG_HARDWARE_ACCELERATED = 512;
    public static final int FLAG_IMMERSIVE = 1024;
    public static final int FLAG_MULTIPROCESS = 1;
    public static final int FLAG_NO_HISTORY = 128;
    public static final int FLAG_STATE_NOT_NEEDED = 16;
    public static final int LAUNCH_MULTIPLE = 0;
    public static final int LAUNCH_SINGLE_INSTANCE = 3;
    public static final int LAUNCH_SINGLE_TASK = 2;
    public static final int LAUNCH_SINGLE_TOP = 1;
    public static final int SCREEN_ORIENTATION_BEHIND = 3;
    public static final int SCREEN_ORIENTATION_FULL_SENSOR = 10;
    public static final int SCREEN_ORIENTATION_LANDSCAPE = 0;
    public static final int SCREEN_ORIENTATION_NOSENSOR = 5;
    public static final int SCREEN_ORIENTATION_PORTRAIT = 1;
    public static final int SCREEN_ORIENTATION_REVERSE_LANDSCAPE = 8;
    public static final int SCREEN_ORIENTATION_REVERSE_PORTRAIT = 9;
    public static final int SCREEN_ORIENTATION_SENSOR = 4;
    public static final int SCREEN_ORIENTATION_SENSOR_LANDSCAPE = 6;
    public static final int SCREEN_ORIENTATION_SENSOR_PORTRAIT = 7;
    public static final int SCREEN_ORIENTATION_UNSPECIFIED = -1;
    public static final int SCREEN_ORIENTATION_USER = 2;
    public static final int UIOPTION_SPLIT_ACTION_BAR_WHEN_NARROW = 1;
    public int configChanges;
    public int flags;
    public int launchMode;
    public String parentActivityName;
    public String permission;
    public int screenOrientation = -1;
    public int softInputMode;
    public String targetActivity;
    public String taskAffinity;
    public int theme;
    public int uiOptions = 0;

    static
    {
        int[] arrayOfInt = new int[12];
        arrayOfInt[0] = 1;
        arrayOfInt[1] = 2;
        arrayOfInt[2] = 4;
        arrayOfInt[3] = 8;
        arrayOfInt[4] = 16;
        arrayOfInt[5] = 32;
        arrayOfInt[6] = 64;
        arrayOfInt[7] = 128;
        arrayOfInt[8] = 2048;
        arrayOfInt[9] = 4096;
        arrayOfInt[10] = 512;
        arrayOfInt[11] = 8192;
    }

    public ActivityInfo()
    {
    }

    public ActivityInfo(ActivityInfo paramActivityInfo)
    {
        super(paramActivityInfo);
        this.theme = paramActivityInfo.theme;
        this.launchMode = paramActivityInfo.launchMode;
        this.permission = paramActivityInfo.permission;
        this.taskAffinity = paramActivityInfo.taskAffinity;
        this.targetActivity = paramActivityInfo.targetActivity;
        this.flags = paramActivityInfo.flags;
        this.screenOrientation = paramActivityInfo.screenOrientation;
        this.configChanges = paramActivityInfo.configChanges;
        this.softInputMode = paramActivityInfo.softInputMode;
        this.uiOptions = paramActivityInfo.uiOptions;
        this.parentActivityName = paramActivityInfo.parentActivityName;
    }

    private ActivityInfo(Parcel paramParcel)
    {
        super(paramParcel);
        this.theme = paramParcel.readInt();
        this.launchMode = paramParcel.readInt();
        this.permission = paramParcel.readString();
        this.taskAffinity = paramParcel.readString();
        this.targetActivity = paramParcel.readString();
        this.flags = paramParcel.readInt();
        this.screenOrientation = paramParcel.readInt();
        this.configChanges = paramParcel.readInt();
        this.softInputMode = paramParcel.readInt();
        this.uiOptions = paramParcel.readInt();
        this.parentActivityName = paramParcel.readString();
    }

    public static int activityInfoConfigToNative(int paramInt)
    {
        int i = 0;
        for (int j = 0; j < CONFIG_NATIVE_BITS.length; j++)
            if ((paramInt & 1 << j) != 0)
                i |= CONFIG_NATIVE_BITS[j];
        return i;
    }

    public int describeContents()
    {
        return 0;
    }

    public void dump(Printer paramPrinter, String paramString)
    {
        super.dumpFront(paramPrinter, paramString);
        if (this.permission != null)
            paramPrinter.println(paramString + "permission=" + this.permission);
        paramPrinter.println(paramString + "taskAffinity=" + this.taskAffinity + " targetActivity=" + this.targetActivity);
        if ((this.launchMode != 0) || (this.flags != 0) || (this.theme != 0))
            paramPrinter.println(paramString + "launchMode=" + this.launchMode + " flags=0x" + Integer.toHexString(this.flags) + " theme=0x" + Integer.toHexString(this.theme));
        if ((this.screenOrientation != -1) || (this.configChanges != 0) || (this.softInputMode != 0))
            paramPrinter.println(paramString + "screenOrientation=" + this.screenOrientation + " configChanges=0x" + Integer.toHexString(this.configChanges) + " softInputMode=0x" + Integer.toHexString(this.softInputMode));
        if (this.uiOptions != 0)
            paramPrinter.println(paramString + " uiOptions=0x" + Integer.toHexString(this.uiOptions));
        super.dumpBack(paramPrinter, paramString);
    }

    public int getRealConfigChanged()
    {
        if (this.applicationInfo.targetSdkVersion < 13);
        for (int i = 0x800 | (0x400 | this.configChanges); ; i = this.configChanges)
            return i;
    }

    public final int getThemeResource()
    {
        if (this.theme != 0);
        for (int i = this.theme; ; i = this.applicationInfo.theme)
            return i;
    }

    public String toString()
    {
        return "ActivityInfo{" + Integer.toHexString(System.identityHashCode(this)) + " " + this.name + "}";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        super.writeToParcel(paramParcel, paramInt);
        paramParcel.writeInt(this.theme);
        paramParcel.writeInt(this.launchMode);
        paramParcel.writeString(this.permission);
        paramParcel.writeString(this.taskAffinity);
        paramParcel.writeString(this.targetActivity);
        paramParcel.writeInt(this.flags);
        paramParcel.writeInt(this.screenOrientation);
        paramParcel.writeInt(this.configChanges);
        paramParcel.writeInt(this.softInputMode);
        paramParcel.writeInt(this.uiOptions);
        paramParcel.writeString(this.parentActivityName);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.ActivityInfo
 * JD-Core Version:        0.6.2
 */