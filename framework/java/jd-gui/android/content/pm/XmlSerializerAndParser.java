package android.content.pm;

import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public abstract interface XmlSerializerAndParser<T>
{
    public abstract T createFromXml(XmlPullParser paramXmlPullParser)
        throws IOException, XmlPullParserException;

    public abstract void writeAsXml(T paramT, XmlSerializer paramXmlSerializer)
        throws IOException;
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.XmlSerializerAndParser
 * JD-Core Version:        0.6.2
 */