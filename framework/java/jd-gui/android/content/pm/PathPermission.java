package android.content.pm;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.PatternMatcher;

public class PathPermission extends PatternMatcher
{
    public static final Parcelable.Creator<PathPermission> CREATOR = new Parcelable.Creator()
    {
        public PathPermission createFromParcel(Parcel paramAnonymousParcel)
        {
            return new PathPermission(paramAnonymousParcel);
        }

        public PathPermission[] newArray(int paramAnonymousInt)
        {
            return new PathPermission[paramAnonymousInt];
        }
    };
    private final String mReadPermission;
    private final String mWritePermission;

    public PathPermission(Parcel paramParcel)
    {
        super(paramParcel);
        this.mReadPermission = paramParcel.readString();
        this.mWritePermission = paramParcel.readString();
    }

    public PathPermission(String paramString1, int paramInt, String paramString2, String paramString3)
    {
        super(paramString1, paramInt);
        this.mReadPermission = paramString2;
        this.mWritePermission = paramString3;
    }

    public String getReadPermission()
    {
        return this.mReadPermission;
    }

    public String getWritePermission()
    {
        return this.mWritePermission;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        super.writeToParcel(paramParcel, paramInt);
        paramParcel.writeString(this.mReadPermission);
        paramParcel.writeString(this.mWritePermission);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.PathPermission
 * JD-Core Version:        0.6.2
 */