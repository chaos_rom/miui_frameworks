package android.content.pm;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class FeatureInfo
    implements Parcelable
{
    public static final Parcelable.Creator<FeatureInfo> CREATOR = new Parcelable.Creator()
    {
        public FeatureInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            return new FeatureInfo(paramAnonymousParcel, null);
        }

        public FeatureInfo[] newArray(int paramAnonymousInt)
        {
            return new FeatureInfo[paramAnonymousInt];
        }
    };
    public static final int FLAG_REQUIRED = 1;
    public static final int GL_ES_VERSION_UNDEFINED;
    public int flags;
    public String name;
    public int reqGlEsVersion;

    public FeatureInfo()
    {
    }

    public FeatureInfo(FeatureInfo paramFeatureInfo)
    {
        this.name = paramFeatureInfo.name;
        this.reqGlEsVersion = paramFeatureInfo.reqGlEsVersion;
        this.flags = paramFeatureInfo.flags;
    }

    private FeatureInfo(Parcel paramParcel)
    {
        this.name = paramParcel.readString();
        this.reqGlEsVersion = paramParcel.readInt();
        this.flags = paramParcel.readInt();
    }

    public int describeContents()
    {
        return 0;
    }

    public String getGlEsVersion()
    {
        int i = (0xFFFF0000 & this.reqGlEsVersion) >> 16;
        int j = 0xFFFF & this.reqGlEsVersion;
        return String.valueOf(i) + "." + String.valueOf(j);
    }

    public String toString()
    {
        if (this.name != null);
        for (String str = "FeatureInfo{" + Integer.toHexString(System.identityHashCode(this)) + " " + this.name + " fl=0x" + Integer.toHexString(this.flags) + "}"; ; str = "FeatureInfo{" + Integer.toHexString(System.identityHashCode(this)) + " glEsVers=" + getGlEsVersion() + " fl=0x" + Integer.toHexString(this.flags) + "}")
            return str;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.name);
        paramParcel.writeInt(this.reqGlEsVersion);
        paramParcel.writeInt(this.flags);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.FeatureInfo
 * JD-Core Version:        0.6.2
 */