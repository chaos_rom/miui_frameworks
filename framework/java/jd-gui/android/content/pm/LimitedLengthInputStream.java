package android.content.pm;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

public class LimitedLengthInputStream extends FilterInputStream
{
    private final long mEnd;
    private long mOffset;

    public LimitedLengthInputStream(InputStream paramInputStream, long paramLong1, long paramLong2)
        throws IOException
    {
        super(paramInputStream);
        if (paramInputStream == null)
            throw new IOException("in == null");
        if (paramLong1 < 0L)
            throw new IOException("offset < 0");
        if (paramLong2 < 0L)
            throw new IOException("length < 0");
        if (paramLong2 > 9223372036854775807L - paramLong1)
            throw new IOException("offset + length > Long.MAX_VALUE");
        this.mEnd = (paramLong1 + paramLong2);
        skip(paramLong1);
        this.mOffset = paramLong1;
    }

    /** @deprecated */
    public int read()
        throws IOException
    {
        try
        {
            long l1 = this.mOffset;
            long l2 = this.mEnd;
            if (l1 >= l2);
            int i;
            for (int j = -1; ; j = i)
            {
                return j;
                this.mOffset = (1L + this.mOffset);
                i = super.read();
            }
        }
        finally
        {
        }
    }

    public int read(byte[] paramArrayOfByte)
        throws IOException
    {
        return read(paramArrayOfByte, 0, paramArrayOfByte.length);
    }

    public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
        throws IOException
    {
        int i;
        if (this.mOffset >= this.mEnd)
            i = -1;
        while (true)
        {
            return i;
            Arrays.checkOffsetAndCount(paramArrayOfByte.length, paramInt1, paramInt2);
            if (this.mOffset > 9223372036854775807L - paramInt2)
                throw new IOException("offset out of bounds: " + this.mOffset + " + " + paramInt2);
            if (this.mOffset + paramInt2 > this.mEnd)
                paramInt2 = (int)(this.mEnd - this.mOffset);
            i = super.read(paramArrayOfByte, paramInt1, paramInt2);
            this.mOffset += i;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.LimitedLengthInputStream
 * JD-Core Version:        0.6.2
 */