package android.content.pm;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.util.AndroidException;
import android.util.DisplayMetrics;
import java.io.File;
import java.util.List;

public abstract class PackageManager
{
    public static final String ACTION_CLEAN_EXTERNAL_STORAGE = "android.content.pm.CLEAN_EXTERNAL_STORAGE";

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    public static final int COMPONENT_ENABLED_STATE_ACCESS_CONTROL = -2147483648;
    public static final int COMPONENT_ENABLED_STATE_DEFAULT = 0;
    public static final int COMPONENT_ENABLED_STATE_DISABLED = 2;
    public static final int COMPONENT_ENABLED_STATE_DISABLED_USER = 3;
    public static final int COMPONENT_ENABLED_STATE_ENABLED = 1;
    public static final int DELETE_FAILED_DEVICE_POLICY_MANAGER = -2;
    public static final int DELETE_FAILED_INTERNAL_ERROR = -1;
    public static final int DELETE_SUCCEEDED = 1;
    public static final int DONT_DELETE_DATA = 1;
    public static final int DONT_KILL_APP = 1;
    public static final String EXTRA_VERIFICATION_ID = "android.content.pm.extra.VERIFICATION_ID";
    public static final String EXTRA_VERIFICATION_INSTALLER_PACKAGE = "android.content.pm.extra.VERIFICATION_INSTALLER_PACKAGE";
    public static final String EXTRA_VERIFICATION_INSTALL_FLAGS = "android.content.pm.extra.VERIFICATION_INSTALL_FLAGS";
    public static final String EXTRA_VERIFICATION_URI = "android.content.pm.extra.VERIFICATION_URI";
    public static final String FEATURE_AUDIO_LOW_LATENCY = "android.hardware.audio.low_latency";
    public static final String FEATURE_BLUETOOTH = "android.hardware.bluetooth";
    public static final String FEATURE_CAMERA = "android.hardware.camera";
    public static final String FEATURE_CAMERA_AUTOFOCUS = "android.hardware.camera.autofocus";
    public static final String FEATURE_CAMERA_FLASH = "android.hardware.camera.flash";
    public static final String FEATURE_CAMERA_FRONT = "android.hardware.camera.front";
    public static final String FEATURE_FAKETOUCH = "android.hardware.faketouch";
    public static final String FEATURE_FAKETOUCH_MULTITOUCH_DISTINCT = "android.hardware.faketouch.multitouch.distinct";
    public static final String FEATURE_FAKETOUCH_MULTITOUCH_JAZZHAND = "android.hardware.faketouch.multitouch.jazzhand";
    public static final String FEATURE_LIVE_WALLPAPER = "android.software.live_wallpaper";
    public static final String FEATURE_LOCATION = "android.hardware.location";
    public static final String FEATURE_LOCATION_GPS = "android.hardware.location.gps";
    public static final String FEATURE_LOCATION_NETWORK = "android.hardware.location.network";
    public static final String FEATURE_MICROPHONE = "android.hardware.microphone";
    public static final String FEATURE_NFC = "android.hardware.nfc";
    public static final String FEATURE_SCREEN_LANDSCAPE = "android.hardware.screen.landscape";
    public static final String FEATURE_SCREEN_PORTRAIT = "android.hardware.screen.portrait";
    public static final String FEATURE_SENSOR_ACCELEROMETER = "android.hardware.sensor.accelerometer";
    public static final String FEATURE_SENSOR_BAROMETER = "android.hardware.sensor.barometer";
    public static final String FEATURE_SENSOR_COMPASS = "android.hardware.sensor.compass";
    public static final String FEATURE_SENSOR_GYROSCOPE = "android.hardware.sensor.gyroscope";
    public static final String FEATURE_SENSOR_LIGHT = "android.hardware.sensor.light";
    public static final String FEATURE_SENSOR_PROXIMITY = "android.hardware.sensor.proximity";
    public static final String FEATURE_SIP = "android.software.sip";
    public static final String FEATURE_SIP_VOIP = "android.software.sip.voip";
    public static final String FEATURE_TELEPHONY = "android.hardware.telephony";
    public static final String FEATURE_TELEPHONY_CDMA = "android.hardware.telephony.cdma";
    public static final String FEATURE_TELEPHONY_GSM = "android.hardware.telephony.gsm";
    public static final String FEATURE_TELEVISION = "android.hardware.type.television";
    public static final String FEATURE_TOUCHSCREEN = "android.hardware.touchscreen";
    public static final String FEATURE_TOUCHSCREEN_MULTITOUCH = "android.hardware.touchscreen.multitouch";
    public static final String FEATURE_TOUCHSCREEN_MULTITOUCH_DISTINCT = "android.hardware.touchscreen.multitouch.distinct";
    public static final String FEATURE_TOUCHSCREEN_MULTITOUCH_JAZZHAND = "android.hardware.touchscreen.multitouch.jazzhand";
    public static final String FEATURE_USB_ACCESSORY = "android.hardware.usb.accessory";
    public static final String FEATURE_USB_HOST = "android.hardware.usb.host";
    public static final String FEATURE_WIFI = "android.hardware.wifi";
    public static final String FEATURE_WIFI_DIRECT = "android.hardware.wifi.direct";
    public static final int GET_ACTIVITIES = 1;
    public static final int GET_CONFIGURATIONS = 16384;
    public static final int GET_DISABLED_COMPONENTS = 512;
    public static final int GET_GIDS = 256;
    public static final int GET_INSTRUMENTATION = 16;
    public static final int GET_INTENT_FILTERS = 32;
    public static final int GET_META_DATA = 128;
    public static final int GET_PERMISSIONS = 4096;
    public static final int GET_PROVIDERS = 8;
    public static final int GET_RECEIVERS = 2;
    public static final int GET_RESOLVED_FILTER = 64;
    public static final int GET_SERVICES = 4;
    public static final int GET_SHARED_LIBRARY_FILES = 1024;
    public static final int GET_SIGNATURES = 64;
    public static final int GET_UNINSTALLED_PACKAGES = 8192;
    public static final int GET_URI_PERMISSION_PATTERNS = 2048;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    public static final int HAS_ACTIVITY = 131072;

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    public static final int HAS_ACTIVITY_OR_SERVICES = 262144;
    public static final int INSTALL_ALLOW_TEST = 4;
    public static final int INSTALL_EXTERNAL = 8;
    public static final int INSTALL_FAILED_ALREADY_EXISTS = -1;
    public static final int INSTALL_FAILED_CONFLICTING_PROVIDER = -13;
    public static final int INSTALL_FAILED_CONTAINER_ERROR = -18;
    public static final int INSTALL_FAILED_CPU_ABI_INCOMPATIBLE = -16;
    public static final int INSTALL_FAILED_DEXOPT = -11;
    public static final int INSTALL_FAILED_DUPLICATE_PACKAGE = -5;
    public static final int INSTALL_FAILED_INSUFFICIENT_STORAGE = -4;
    public static final int INSTALL_FAILED_INTERNAL_ERROR = -110;
    public static final int INSTALL_FAILED_INVALID_APK = -2;
    public static final int INSTALL_FAILED_INVALID_INSTALL_LOCATION = -19;
    public static final int INSTALL_FAILED_INVALID_URI = -3;
    public static final int INSTALL_FAILED_MEDIA_UNAVAILABLE = -20;
    public static final int INSTALL_FAILED_MISSING_FEATURE = -17;
    public static final int INSTALL_FAILED_MISSING_SHARED_LIBRARY = -9;
    public static final int INSTALL_FAILED_NEWER_SDK = -14;
    public static final int INSTALL_FAILED_NO_SHARED_USER = -6;
    public static final int INSTALL_FAILED_OLDER_SDK = -12;
    public static final int INSTALL_FAILED_PACKAGE_CHANGED = -23;
    public static final int INSTALL_FAILED_REPLACE_COULDNT_DELETE = -10;
    public static final int INSTALL_FAILED_SHARED_USER_INCOMPATIBLE = -8;
    public static final int INSTALL_FAILED_TEST_ONLY = -15;
    public static final int INSTALL_FAILED_UID_CHANGED = -24;
    public static final int INSTALL_FAILED_UPDATE_INCOMPATIBLE = -7;
    public static final int INSTALL_FAILED_VERIFICATION_FAILURE = -22;
    public static final int INSTALL_FAILED_VERIFICATION_TIMEOUT = -21;
    public static final int INSTALL_FORWARD_LOCK = 1;
    public static final int INSTALL_FROM_ADB = 32;
    public static final int INSTALL_INTERNAL = 16;
    public static final int INSTALL_PARSE_FAILED_BAD_MANIFEST = -101;
    public static final int INSTALL_PARSE_FAILED_BAD_PACKAGE_NAME = -106;
    public static final int INSTALL_PARSE_FAILED_BAD_SHARED_USER_ID = -107;
    public static final int INSTALL_PARSE_FAILED_CERTIFICATE_ENCODING = -105;
    public static final int INSTALL_PARSE_FAILED_INCONSISTENT_CERTIFICATES = -104;
    public static final int INSTALL_PARSE_FAILED_MANIFEST_EMPTY = -109;
    public static final int INSTALL_PARSE_FAILED_MANIFEST_MALFORMED = -108;
    public static final int INSTALL_PARSE_FAILED_NOT_APK = -100;
    public static final int INSTALL_PARSE_FAILED_NO_CERTIFICATES = -103;
    public static final int INSTALL_PARSE_FAILED_UNEXPECTED_EXCEPTION = -102;
    public static final int INSTALL_REPLACE_EXISTING = 2;
    public static final int INSTALL_SUCCEEDED = 1;
    public static final int MATCH_DEFAULT_ONLY = 65536;
    public static final int MOVE_EXTERNAL_MEDIA = 2;
    public static final int MOVE_FAILED_DOESNT_EXIST = -2;
    public static final int MOVE_FAILED_FORWARD_LOCKED = -4;
    public static final int MOVE_FAILED_INSUFFICIENT_STORAGE = -1;
    public static final int MOVE_FAILED_INTERNAL_ERROR = -6;
    public static final int MOVE_FAILED_INVALID_LOCATION = -5;
    public static final int MOVE_FAILED_OPERATION_PENDING = -7;
    public static final int MOVE_FAILED_SYSTEM_PACKAGE = -3;
    public static final int MOVE_INTERNAL = 1;
    public static final int MOVE_SUCCEEDED = 1;
    public static final int PERMISSION_DENIED = -1;
    public static final int PERMISSION_GRANTED = 0;
    public static final int SIGNATURE_FIRST_NOT_SIGNED = -1;
    public static final int SIGNATURE_MATCH = 0;
    public static final int SIGNATURE_NEITHER_SIGNED = 1;
    public static final int SIGNATURE_NO_MATCH = -3;
    public static final int SIGNATURE_SECOND_NOT_SIGNED = -2;
    public static final int SIGNATURE_UNKNOWN_PACKAGE = -4;
    public static final int VERIFICATION_ALLOW = 1;
    public static final int VERIFICATION_ALLOW_WITHOUT_SUFFICIENT = 2;
    public static final int VERIFICATION_REJECT = -1;

    public static String getDataDirForUser(int paramInt, String paramString)
    {
        return Environment.getDataDirectory().toString() + "/user/" + paramInt + "/" + paramString;
    }

    @Deprecated
    public abstract void addPackageToPreferred(String paramString);

    public abstract boolean addPermission(PermissionInfo paramPermissionInfo);

    public abstract boolean addPermissionAsync(PermissionInfo paramPermissionInfo);

    @Deprecated
    public abstract void addPreferredActivity(IntentFilter paramIntentFilter, int paramInt, ComponentName[] paramArrayOfComponentName, ComponentName paramComponentName);

    public abstract String[] canonicalToCurrentPackageNames(String[] paramArrayOfString);

    public abstract int checkPermission(String paramString1, String paramString2);

    public abstract int checkSignatures(int paramInt1, int paramInt2);

    public abstract int checkSignatures(String paramString1, String paramString2);

    public abstract void clearApplicationUserData(String paramString, IPackageDataObserver paramIPackageDataObserver);

    public abstract void clearPackagePreferredActivities(String paramString);

    public abstract UserInfo createUser(String paramString, int paramInt);

    public abstract String[] currentToCanonicalPackageNames(String[] paramArrayOfString);

    public abstract void deleteApplicationCacheFiles(String paramString, IPackageDataObserver paramIPackageDataObserver);

    public abstract void deletePackage(String paramString, IPackageDeleteObserver paramIPackageDeleteObserver, int paramInt);

    public abstract void freeStorage(long paramLong, IntentSender paramIntentSender);

    public abstract void freeStorageAndNotify(long paramLong, IPackageDataObserver paramIPackageDataObserver);

    public abstract Drawable getActivityIcon(ComponentName paramComponentName)
        throws PackageManager.NameNotFoundException;

    public abstract Drawable getActivityIcon(Intent paramIntent)
        throws PackageManager.NameNotFoundException;

    public abstract ActivityInfo getActivityInfo(ComponentName paramComponentName, int paramInt)
        throws PackageManager.NameNotFoundException;

    public abstract Drawable getActivityLogo(ComponentName paramComponentName)
        throws PackageManager.NameNotFoundException;

    public abstract Drawable getActivityLogo(Intent paramIntent)
        throws PackageManager.NameNotFoundException;

    public abstract List<PermissionGroupInfo> getAllPermissionGroups(int paramInt);

    public abstract int getApplicationEnabledSetting(String paramString);

    public abstract Drawable getApplicationIcon(ApplicationInfo paramApplicationInfo);

    public abstract Drawable getApplicationIcon(String paramString)
        throws PackageManager.NameNotFoundException;

    public abstract ApplicationInfo getApplicationInfo(String paramString, int paramInt)
        throws PackageManager.NameNotFoundException;

    public abstract CharSequence getApplicationLabel(ApplicationInfo paramApplicationInfo);

    public abstract Drawable getApplicationLogo(ApplicationInfo paramApplicationInfo);

    public abstract Drawable getApplicationLogo(String paramString)
        throws PackageManager.NameNotFoundException;

    public abstract int getComponentEnabledSetting(ComponentName paramComponentName);

    public abstract Drawable getDefaultActivityIcon();

    public abstract Drawable getDrawable(String paramString, int paramInt, ApplicationInfo paramApplicationInfo);

    public abstract List<ApplicationInfo> getInstalledApplications(int paramInt);

    public abstract List<PackageInfo> getInstalledPackages(int paramInt);

    public abstract String getInstallerPackageName(String paramString);

    public abstract InstrumentationInfo getInstrumentationInfo(ComponentName paramComponentName, int paramInt)
        throws PackageManager.NameNotFoundException;

    public abstract Intent getLaunchIntentForPackage(String paramString);

    public abstract String getNameForUid(int paramInt);

    public PackageInfo getPackageArchiveInfo(String paramString, int paramInt)
    {
        PackageInfo localPackageInfo = null;
        PackageParser localPackageParser = new PackageParser(paramString);
        DisplayMetrics localDisplayMetrics = new DisplayMetrics();
        localDisplayMetrics.setToDefaults();
        PackageParser.Package localPackage = localPackageParser.parsePackage(new File(paramString), paramString, localDisplayMetrics, 0);
        if (localPackage == null);
        while (true)
        {
            return localPackageInfo;
            if ((paramInt & 0x40) != 0)
                localPackageParser.collectCertificates(localPackage, 0);
            localPackageInfo = PackageParser.generatePackageInfo(localPackage, null, paramInt, 0L, 0L, null, false, 0);
        }
    }

    public abstract int[] getPackageGids(String paramString)
        throws PackageManager.NameNotFoundException;

    public abstract PackageInfo getPackageInfo(String paramString, int paramInt)
        throws PackageManager.NameNotFoundException;

    public abstract void getPackageSizeInfo(String paramString, IPackageStatsObserver paramIPackageStatsObserver);

    public abstract String[] getPackagesForUid(int paramInt);

    public abstract PermissionGroupInfo getPermissionGroupInfo(String paramString, int paramInt)
        throws PackageManager.NameNotFoundException;

    public abstract PermissionInfo getPermissionInfo(String paramString, int paramInt)
        throws PackageManager.NameNotFoundException;

    public abstract int getPreferredActivities(List<IntentFilter> paramList, List<ComponentName> paramList1, String paramString);

    public abstract List<PackageInfo> getPreferredPackages(int paramInt);

    public abstract ProviderInfo getProviderInfo(ComponentName paramComponentName, int paramInt)
        throws PackageManager.NameNotFoundException;

    public abstract ActivityInfo getReceiverInfo(ComponentName paramComponentName, int paramInt)
        throws PackageManager.NameNotFoundException;

    public abstract Resources getResourcesForActivity(ComponentName paramComponentName)
        throws PackageManager.NameNotFoundException;

    public abstract Resources getResourcesForApplication(ApplicationInfo paramApplicationInfo)
        throws PackageManager.NameNotFoundException;

    public abstract Resources getResourcesForApplication(String paramString)
        throws PackageManager.NameNotFoundException;

    public abstract ServiceInfo getServiceInfo(ComponentName paramComponentName, int paramInt)
        throws PackageManager.NameNotFoundException;

    public abstract FeatureInfo[] getSystemAvailableFeatures();

    public abstract String[] getSystemSharedLibraryNames();

    public abstract CharSequence getText(String paramString, int paramInt, ApplicationInfo paramApplicationInfo);

    public abstract int getUidForSharedUser(String paramString)
        throws PackageManager.NameNotFoundException;

    public abstract UserInfo getUser(int paramInt);

    public abstract List<UserInfo> getUsers();

    public abstract VerifierDeviceIdentity getVerifierDeviceIdentity();

    public abstract XmlResourceParser getXml(String paramString, int paramInt, ApplicationInfo paramApplicationInfo);

    public abstract void grantPermission(String paramString1, String paramString2);

    public abstract boolean hasSystemFeature(String paramString);

    public abstract void installPackage(Uri paramUri, IPackageInstallObserver paramIPackageInstallObserver, int paramInt, String paramString);

    public abstract void installPackageWithVerification(Uri paramUri1, IPackageInstallObserver paramIPackageInstallObserver, int paramInt, String paramString, Uri paramUri2, ManifestDigest paramManifestDigest, ContainerEncryptionParams paramContainerEncryptionParams);

    public abstract boolean isSafeMode();

    public abstract void movePackage(String paramString, IPackageMoveObserver paramIPackageMoveObserver, int paramInt);

    public abstract List<ResolveInfo> queryBroadcastReceivers(Intent paramIntent, int paramInt);

    public abstract List<ProviderInfo> queryContentProviders(String paramString, int paramInt1, int paramInt2);

    public abstract List<InstrumentationInfo> queryInstrumentation(String paramString, int paramInt);

    public abstract List<ResolveInfo> queryIntentActivities(Intent paramIntent, int paramInt);

    public abstract List<ResolveInfo> queryIntentActivityOptions(ComponentName paramComponentName, Intent[] paramArrayOfIntent, Intent paramIntent, int paramInt);

    public abstract List<ResolveInfo> queryIntentServices(Intent paramIntent, int paramInt);

    public abstract List<PermissionInfo> queryPermissionsByGroup(String paramString, int paramInt)
        throws PackageManager.NameNotFoundException;

    @Deprecated
    public abstract void removePackageFromPreferred(String paramString);

    public abstract void removePermission(String paramString);

    public abstract boolean removeUser(int paramInt);

    @Deprecated
    public abstract void replacePreferredActivity(IntentFilter paramIntentFilter, int paramInt, ComponentName[] paramArrayOfComponentName, ComponentName paramComponentName);

    public abstract ResolveInfo resolveActivity(Intent paramIntent, int paramInt);

    public abstract ProviderInfo resolveContentProvider(String paramString, int paramInt);

    public abstract ResolveInfo resolveService(Intent paramIntent, int paramInt);

    public abstract void revokePermission(String paramString1, String paramString2);

    public abstract void setApplicationEnabledSetting(String paramString, int paramInt1, int paramInt2);

    public abstract void setComponentEnabledSetting(ComponentName paramComponentName, int paramInt1, int paramInt2);

    public abstract void setInstallerPackageName(String paramString1, String paramString2);

    public abstract void updateUserFlags(int paramInt1, int paramInt2);

    public abstract void updateUserName(int paramInt, String paramString);

    public abstract void verifyPendingInstall(int paramInt1, int paramInt2);

    public static class NameNotFoundException extends AndroidException
    {
        public NameNotFoundException()
        {
        }

        public NameNotFoundException(String paramString)
        {
            super();
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.PackageManager
 * JD-Core Version:        0.6.2
 */