package android.content.pm;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Printer;
import java.text.Collator;
import java.util.Comparator;

public class ApplicationInfo extends PackageItemInfo
    implements Parcelable
{
    public static final Parcelable.Creator<ApplicationInfo> CREATOR = new Parcelable.Creator()
    {
        public ApplicationInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            return new ApplicationInfo(paramAnonymousParcel, null);
        }

        public ApplicationInfo[] newArray(int paramAnonymousInt)
        {
            return new ApplicationInfo[paramAnonymousInt];
        }
    };

    @MiuiHook(MiuiHook.MiuiHookType.NEW_FIELD)
    public static final int FLAG_ACCESS_CONTROL_PASSWORD = -2147483648;
    public static final int FLAG_ALLOW_BACKUP = 32768;
    public static final int FLAG_ALLOW_CLEAR_USER_DATA = 64;
    public static final int FLAG_ALLOW_TASK_REPARENTING = 32;
    public static final int FLAG_CANT_SAVE_STATE = 268435456;
    public static final int FLAG_DEBUGGABLE = 2;
    public static final int FLAG_EXTERNAL_STORAGE = 262144;
    public static final int FLAG_FACTORY_TEST = 16;
    public static final int FLAG_FORWARD_LOCK = 536870912;
    public static final int FLAG_HAS_CODE = 4;
    public static final int FLAG_KILL_AFTER_RESTORE = 65536;
    public static final int FLAG_LARGE_HEAP = 1048576;
    public static final int FLAG_PERSISTENT = 8;
    public static final int FLAG_RESIZEABLE_FOR_SCREENS = 4096;
    public static final int FLAG_RESTORE_ANY_VERSION = 131072;
    public static final int FLAG_STOPPED = 2097152;
    public static final int FLAG_SUPPORTS_LARGE_SCREENS = 2048;
    public static final int FLAG_SUPPORTS_NORMAL_SCREENS = 1024;
    public static final int FLAG_SUPPORTS_RTL = 4194304;
    public static final int FLAG_SUPPORTS_SCREEN_DENSITIES = 8192;
    public static final int FLAG_SUPPORTS_SMALL_SCREENS = 512;
    public static final int FLAG_SUPPORTS_XLARGE_SCREENS = 524288;
    public static final int FLAG_SYSTEM = 1;
    public static final int FLAG_TEST_ONLY = 256;
    public static final int FLAG_UPDATED_SYSTEM_APP = 128;
    public static final int FLAG_VM_SAFE_MODE = 16384;
    public String backupAgentName;
    public String className;
    public int compatibleWidthLimitDp = 0;
    public String dataDir;
    public int descriptionRes;
    public boolean enabled;
    public int enabledSetting;
    public int flags = 0;
    public int installLocation;
    public int largestWidthLimitDp = 0;
    public String manageSpaceActivityName;
    public String nativeLibraryDir;
    public String permission;
    public String processName;
    public String publicSourceDir;
    public int requiresSmallestWidthDp = 0;
    public String[] resourceDirs;
    public String[] sharedLibraryFiles;
    public String sourceDir;
    public int targetSdkVersion;
    public String taskAffinity;
    public int theme;
    public int uiOptions = 0;
    public int uid;

    public ApplicationInfo()
    {
        this.enabled = true;
        this.enabledSetting = 0;
        this.installLocation = -1;
    }

    public ApplicationInfo(ApplicationInfo paramApplicationInfo)
    {
        super(paramApplicationInfo);
        this.enabled = true;
        this.enabledSetting = 0;
        this.installLocation = -1;
        this.taskAffinity = paramApplicationInfo.taskAffinity;
        this.permission = paramApplicationInfo.permission;
        this.processName = paramApplicationInfo.processName;
        this.className = paramApplicationInfo.className;
        this.theme = paramApplicationInfo.theme;
        this.flags = paramApplicationInfo.flags;
        this.requiresSmallestWidthDp = paramApplicationInfo.requiresSmallestWidthDp;
        this.compatibleWidthLimitDp = paramApplicationInfo.compatibleWidthLimitDp;
        this.largestWidthLimitDp = paramApplicationInfo.largestWidthLimitDp;
        this.sourceDir = paramApplicationInfo.sourceDir;
        this.publicSourceDir = paramApplicationInfo.publicSourceDir;
        this.nativeLibraryDir = paramApplicationInfo.nativeLibraryDir;
        this.resourceDirs = paramApplicationInfo.resourceDirs;
        this.sharedLibraryFiles = paramApplicationInfo.sharedLibraryFiles;
        this.dataDir = paramApplicationInfo.dataDir;
        this.uid = paramApplicationInfo.uid;
        this.targetSdkVersion = paramApplicationInfo.targetSdkVersion;
        this.enabled = paramApplicationInfo.enabled;
        this.enabledSetting = paramApplicationInfo.enabledSetting;
        this.installLocation = paramApplicationInfo.installLocation;
        this.manageSpaceActivityName = paramApplicationInfo.manageSpaceActivityName;
        this.descriptionRes = paramApplicationInfo.descriptionRes;
        this.uiOptions = paramApplicationInfo.uiOptions;
    }

    private ApplicationInfo(Parcel paramParcel)
    {
        super(paramParcel);
        this.enabled = bool;
        this.enabledSetting = 0;
        this.installLocation = -1;
        this.taskAffinity = paramParcel.readString();
        this.permission = paramParcel.readString();
        this.processName = paramParcel.readString();
        this.className = paramParcel.readString();
        this.theme = paramParcel.readInt();
        this.flags = paramParcel.readInt();
        this.requiresSmallestWidthDp = paramParcel.readInt();
        this.compatibleWidthLimitDp = paramParcel.readInt();
        this.largestWidthLimitDp = paramParcel.readInt();
        this.sourceDir = paramParcel.readString();
        this.publicSourceDir = paramParcel.readString();
        this.nativeLibraryDir = paramParcel.readString();
        this.resourceDirs = paramParcel.readStringArray();
        this.sharedLibraryFiles = paramParcel.readStringArray();
        this.dataDir = paramParcel.readString();
        this.uid = paramParcel.readInt();
        this.targetSdkVersion = paramParcel.readInt();
        if (paramParcel.readInt() != 0);
        while (true)
        {
            this.enabled = bool;
            this.enabledSetting = paramParcel.readInt();
            this.installLocation = paramParcel.readInt();
            this.manageSpaceActivityName = paramParcel.readString();
            this.backupAgentName = paramParcel.readString();
            this.descriptionRes = paramParcel.readInt();
            this.uiOptions = paramParcel.readInt();
            return;
            bool = false;
        }
    }

    private boolean isPackageUnavailable(PackageManager paramPackageManager)
    {
        boolean bool = true;
        try
        {
            PackageInfo localPackageInfo = paramPackageManager.getPackageInfo(this.packageName, 0);
            if (localPackageInfo == null);
            while (true)
            {
                label18: return bool;
                bool = false;
            }
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            break label18;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public void disableCompatibilityMode()
    {
        this.flags = (0x83E00 | this.flags);
    }

    public void dump(Printer paramPrinter, String paramString)
    {
        super.dumpFront(paramPrinter, paramString);
        if (this.className != null)
            paramPrinter.println(paramString + "className=" + this.className);
        if (this.permission != null)
            paramPrinter.println(paramString + "permission=" + this.permission);
        paramPrinter.println(paramString + "processName=" + this.processName);
        paramPrinter.println(paramString + "taskAffinity=" + this.taskAffinity);
        paramPrinter.println(paramString + "uid=" + this.uid + " flags=0x" + Integer.toHexString(this.flags) + " theme=0x" + Integer.toHexString(this.theme));
        paramPrinter.println(paramString + "requiresSmallestWidthDp=" + this.requiresSmallestWidthDp + " compatibleWidthLimitDp=" + this.compatibleWidthLimitDp + " largestWidthLimitDp=" + this.largestWidthLimitDp);
        paramPrinter.println(paramString + "sourceDir=" + this.sourceDir);
        StringBuilder localStringBuilder;
        if (this.sourceDir == null)
        {
            if (this.publicSourceDir != null)
                paramPrinter.println(paramString + "publicSourceDir=" + this.publicSourceDir);
            if (this.resourceDirs != null)
                paramPrinter.println(paramString + "resourceDirs=" + this.resourceDirs);
            paramPrinter.println(paramString + "dataDir=" + this.dataDir);
            if (this.sharedLibraryFiles != null)
                paramPrinter.println(paramString + "sharedLibraryFiles=" + this.sharedLibraryFiles);
            paramPrinter.println(paramString + "enabled=" + this.enabled + " targetSdkVersion=" + this.targetSdkVersion);
            if (this.manageSpaceActivityName != null)
                paramPrinter.println(paramString + "manageSpaceActivityName=" + this.manageSpaceActivityName);
            if (this.descriptionRes != 0)
                paramPrinter.println(paramString + "description=0x" + Integer.toHexString(this.descriptionRes));
            if (this.uiOptions != 0)
                paramPrinter.println(paramString + "uiOptions=0x" + Integer.toHexString(this.uiOptions));
            localStringBuilder = new StringBuilder().append(paramString).append("supportsRtl=");
            if (!hasRtlSupport())
                break label728;
        }
        label728: for (String str = "true"; ; str = "false")
        {
            paramPrinter.println(str);
            super.dumpBack(paramPrinter, paramString);
            return;
            if (this.sourceDir.equals(this.publicSourceDir))
                break;
            paramPrinter.println(paramString + "publicSourceDir=" + this.publicSourceDir);
            break;
        }
    }

    protected ApplicationInfo getApplicationInfo()
    {
        return this;
    }

    public boolean hasRtlSupport()
    {
        if ((0x400000 & this.flags) == 4194304);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected Drawable loadDefaultIcon(PackageManager paramPackageManager)
    {
        if (((0x40000 & this.flags) != 0) && (isPackageUnavailable(paramPackageManager)));
        for (Drawable localDrawable = Resources.getSystem().getDrawable(17302909); ; localDrawable = paramPackageManager.getDefaultActivityIcon())
            return localDrawable;
    }

    public CharSequence loadDescription(PackageManager paramPackageManager)
    {
        CharSequence localCharSequence;
        if (this.descriptionRes != 0)
        {
            localCharSequence = paramPackageManager.getText(this.packageName, this.descriptionRes, this);
            if (localCharSequence == null);
        }
        while (true)
        {
            return localCharSequence;
            localCharSequence = null;
        }
    }

    public String toString()
    {
        return "ApplicationInfo{" + Integer.toHexString(System.identityHashCode(this)) + " " + this.packageName + "}";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        super.writeToParcel(paramParcel, paramInt);
        paramParcel.writeString(this.taskAffinity);
        paramParcel.writeString(this.permission);
        paramParcel.writeString(this.processName);
        paramParcel.writeString(this.className);
        paramParcel.writeInt(this.theme);
        paramParcel.writeInt(this.flags);
        paramParcel.writeInt(this.requiresSmallestWidthDp);
        paramParcel.writeInt(this.compatibleWidthLimitDp);
        paramParcel.writeInt(this.largestWidthLimitDp);
        paramParcel.writeString(this.sourceDir);
        paramParcel.writeString(this.publicSourceDir);
        paramParcel.writeString(this.nativeLibraryDir);
        paramParcel.writeStringArray(this.resourceDirs);
        paramParcel.writeStringArray(this.sharedLibraryFiles);
        paramParcel.writeString(this.dataDir);
        paramParcel.writeInt(this.uid);
        paramParcel.writeInt(this.targetSdkVersion);
        if (this.enabled);
        for (int i = 1; ; i = 0)
        {
            paramParcel.writeInt(i);
            paramParcel.writeInt(this.enabledSetting);
            paramParcel.writeInt(this.installLocation);
            paramParcel.writeString(this.manageSpaceActivityName);
            paramParcel.writeString(this.backupAgentName);
            paramParcel.writeInt(this.descriptionRes);
            paramParcel.writeInt(this.uiOptions);
            return;
        }
    }

    public static class DisplayNameComparator
        implements Comparator<ApplicationInfo>
    {
        private PackageManager mPM;
        private final Collator sCollator = Collator.getInstance();

        public DisplayNameComparator(PackageManager paramPackageManager)
        {
            this.mPM = paramPackageManager;
        }

        public final int compare(ApplicationInfo paramApplicationInfo1, ApplicationInfo paramApplicationInfo2)
        {
            Object localObject1 = this.mPM.getApplicationLabel(paramApplicationInfo1);
            if (localObject1 == null)
                localObject1 = paramApplicationInfo1.packageName;
            Object localObject2 = this.mPM.getApplicationLabel(paramApplicationInfo2);
            if (localObject2 == null)
                localObject2 = paramApplicationInfo2.packageName;
            return this.sCollator.compare(localObject1.toString(), localObject2.toString());
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.ApplicationInfo
 * JD-Core Version:        0.6.2
 */