package android.content.pm;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface IPackageMoveObserver extends IInterface
{
    public abstract void packageMoved(String paramString, int paramInt)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IPackageMoveObserver
    {
        private static final String DESCRIPTOR = "android.content.pm.IPackageMoveObserver";
        static final int TRANSACTION_packageMoved = 1;

        public Stub()
        {
            attachInterface(this, "android.content.pm.IPackageMoveObserver");
        }

        public static IPackageMoveObserver asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.content.pm.IPackageMoveObserver");
                if ((localIInterface != null) && ((localIInterface instanceof IPackageMoveObserver)))
                    localObject = (IPackageMoveObserver)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool = true;
            switch (paramInt1)
            {
            default:
                bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            }
            while (true)
            {
                return bool;
                paramParcel2.writeString("android.content.pm.IPackageMoveObserver");
                continue;
                paramParcel1.enforceInterface("android.content.pm.IPackageMoveObserver");
                packageMoved(paramParcel1.readString(), paramParcel1.readInt());
            }
        }

        private static class Proxy
            implements IPackageMoveObserver
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String getInterfaceDescriptor()
            {
                return "android.content.pm.IPackageMoveObserver";
            }

            public void packageMoved(String paramString, int paramInt)
                throws RemoteException
            {
                Parcel localParcel = Parcel.obtain();
                try
                {
                    localParcel.writeInterfaceToken("android.content.pm.IPackageMoveObserver");
                    localParcel.writeString(paramString);
                    localParcel.writeInt(paramInt);
                    this.mRemote.transact(1, localParcel, null, 1);
                    return;
                }
                finally
                {
                    localParcel.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.IPackageMoveObserver
 * JD-Core Version:        0.6.2
 */