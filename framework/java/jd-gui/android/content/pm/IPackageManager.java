package android.content.pm;

import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public abstract interface IPackageManager extends IInterface
{
    public abstract void addPackageToPreferred(String paramString)
        throws RemoteException;

    public abstract boolean addPermission(PermissionInfo paramPermissionInfo)
        throws RemoteException;

    public abstract boolean addPermissionAsync(PermissionInfo paramPermissionInfo)
        throws RemoteException;

    public abstract void addPreferredActivity(IntentFilter paramIntentFilter, int paramInt, ComponentName[] paramArrayOfComponentName, ComponentName paramComponentName)
        throws RemoteException;

    public abstract String[] canonicalToCurrentPackageNames(String[] paramArrayOfString)
        throws RemoteException;

    public abstract int checkPermission(String paramString1, String paramString2)
        throws RemoteException;

    public abstract int checkSignatures(String paramString1, String paramString2)
        throws RemoteException;

    public abstract int checkUidPermission(String paramString, int paramInt)
        throws RemoteException;

    public abstract int checkUidSignatures(int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void clearApplicationUserData(String paramString, IPackageDataObserver paramIPackageDataObserver, int paramInt)
        throws RemoteException;

    public abstract void clearPackagePreferredActivities(String paramString)
        throws RemoteException;

    public abstract UserInfo createUser(String paramString, int paramInt)
        throws RemoteException;

    public abstract String[] currentToCanonicalPackageNames(String[] paramArrayOfString)
        throws RemoteException;

    public abstract void deleteApplicationCacheFiles(String paramString, IPackageDataObserver paramIPackageDataObserver)
        throws RemoteException;

    public abstract void deletePackage(String paramString, IPackageDeleteObserver paramIPackageDeleteObserver, int paramInt)
        throws RemoteException;

    public abstract void enterSafeMode()
        throws RemoteException;

    public abstract void finishPackageInstall(int paramInt)
        throws RemoteException;

    public abstract void freeStorage(long paramLong, IntentSender paramIntentSender)
        throws RemoteException;

    public abstract void freeStorageAndNotify(long paramLong, IPackageDataObserver paramIPackageDataObserver)
        throws RemoteException;

    public abstract ActivityInfo getActivityInfo(ComponentName paramComponentName, int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract List<PermissionGroupInfo> getAllPermissionGroups(int paramInt)
        throws RemoteException;

    public abstract int getApplicationEnabledSetting(String paramString, int paramInt)
        throws RemoteException;

    public abstract ApplicationInfo getApplicationInfo(String paramString, int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract int getComponentEnabledSetting(ComponentName paramComponentName, int paramInt)
        throws RemoteException;

    public abstract int getInstallLocation()
        throws RemoteException;

    public abstract ParceledListSlice getInstalledApplications(int paramInt1, String paramString, int paramInt2)
        throws RemoteException;

    public abstract ParceledListSlice getInstalledPackages(int paramInt, String paramString)
        throws RemoteException;

    public abstract String getInstallerPackageName(String paramString)
        throws RemoteException;

    public abstract InstrumentationInfo getInstrumentationInfo(ComponentName paramComponentName, int paramInt)
        throws RemoteException;

    public abstract String getNameForUid(int paramInt)
        throws RemoteException;

    public abstract int[] getPackageGids(String paramString)
        throws RemoteException;

    public abstract PackageInfo getPackageInfo(String paramString, int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void getPackageSizeInfo(String paramString, IPackageStatsObserver paramIPackageStatsObserver)
        throws RemoteException;

    public abstract int getPackageUid(String paramString, int paramInt)
        throws RemoteException;

    public abstract String[] getPackagesForUid(int paramInt)
        throws RemoteException;

    public abstract PermissionGroupInfo getPermissionGroupInfo(String paramString, int paramInt)
        throws RemoteException;

    public abstract PermissionInfo getPermissionInfo(String paramString, int paramInt)
        throws RemoteException;

    public abstract List<ApplicationInfo> getPersistentApplications(int paramInt)
        throws RemoteException;

    public abstract int getPreferredActivities(List<IntentFilter> paramList, List<ComponentName> paramList1, String paramString)
        throws RemoteException;

    public abstract List<PackageInfo> getPreferredPackages(int paramInt)
        throws RemoteException;

    public abstract ProviderInfo getProviderInfo(ComponentName paramComponentName, int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract ActivityInfo getReceiverInfo(ComponentName paramComponentName, int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract ServiceInfo getServiceInfo(ComponentName paramComponentName, int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract FeatureInfo[] getSystemAvailableFeatures()
        throws RemoteException;

    public abstract String[] getSystemSharedLibraryNames()
        throws RemoteException;

    public abstract int getUidForSharedUser(String paramString)
        throws RemoteException;

    public abstract UserInfo getUser(int paramInt)
        throws RemoteException;

    public abstract List<UserInfo> getUsers()
        throws RemoteException;

    public abstract VerifierDeviceIdentity getVerifierDeviceIdentity()
        throws RemoteException;

    public abstract void grantPermission(String paramString1, String paramString2)
        throws RemoteException;

    public abstract boolean hasSystemFeature(String paramString)
        throws RemoteException;

    public abstract boolean hasSystemUidErrors()
        throws RemoteException;

    public abstract void installPackage(Uri paramUri, IPackageInstallObserver paramIPackageInstallObserver, int paramInt, String paramString)
        throws RemoteException;

    public abstract void installPackageWithVerification(Uri paramUri1, IPackageInstallObserver paramIPackageInstallObserver, int paramInt, String paramString, Uri paramUri2, ManifestDigest paramManifestDigest, ContainerEncryptionParams paramContainerEncryptionParams)
        throws RemoteException;

    public abstract boolean isFirstBoot()
        throws RemoteException;

    public abstract boolean isPermissionEnforced(String paramString)
        throws RemoteException;

    public abstract boolean isProtectedBroadcast(String paramString)
        throws RemoteException;

    public abstract boolean isSafeMode()
        throws RemoteException;

    public abstract boolean isStorageLow()
        throws RemoteException;

    public abstract void movePackage(String paramString, IPackageMoveObserver paramIPackageMoveObserver, int paramInt)
        throws RemoteException;

    public abstract String nextPackageToClean(String paramString)
        throws RemoteException;

    public abstract void performBootDexOpt()
        throws RemoteException;

    public abstract boolean performDexOpt(String paramString)
        throws RemoteException;

    public abstract List<ProviderInfo> queryContentProviders(String paramString, int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract List<InstrumentationInfo> queryInstrumentation(String paramString, int paramInt)
        throws RemoteException;

    public abstract List<ResolveInfo> queryIntentActivities(Intent paramIntent, String paramString, int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract List<ResolveInfo> queryIntentActivityOptions(ComponentName paramComponentName, Intent[] paramArrayOfIntent, String[] paramArrayOfString, Intent paramIntent, String paramString, int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract List<ResolveInfo> queryIntentReceivers(Intent paramIntent, String paramString, int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract List<ResolveInfo> queryIntentServices(Intent paramIntent, String paramString, int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract List<PermissionInfo> queryPermissionsByGroup(String paramString, int paramInt)
        throws RemoteException;

    public abstract void querySyncProviders(List<String> paramList, List<ProviderInfo> paramList1)
        throws RemoteException;

    public abstract void removePackageFromPreferred(String paramString)
        throws RemoteException;

    public abstract void removePermission(String paramString)
        throws RemoteException;

    public abstract boolean removeUser(int paramInt)
        throws RemoteException;

    public abstract void replacePreferredActivity(IntentFilter paramIntentFilter, int paramInt, ComponentName[] paramArrayOfComponentName, ComponentName paramComponentName)
        throws RemoteException;

    public abstract ProviderInfo resolveContentProvider(String paramString, int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract ResolveInfo resolveIntent(Intent paramIntent, String paramString, int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract ResolveInfo resolveService(Intent paramIntent, String paramString, int paramInt1, int paramInt2)
        throws RemoteException;

    public abstract void revokePermission(String paramString1, String paramString2)
        throws RemoteException;

    public abstract void setApplicationEnabledSetting(String paramString, int paramInt1, int paramInt2, int paramInt3)
        throws RemoteException;

    public abstract void setComponentEnabledSetting(ComponentName paramComponentName, int paramInt1, int paramInt2, int paramInt3)
        throws RemoteException;

    public abstract boolean setInstallLocation(int paramInt)
        throws RemoteException;

    public abstract void setInstallerPackageName(String paramString1, String paramString2)
        throws RemoteException;

    public abstract void setPackageStoppedState(String paramString, boolean paramBoolean, int paramInt)
        throws RemoteException;

    public abstract void setPermissionEnforced(String paramString, boolean paramBoolean)
        throws RemoteException;

    public abstract void systemReady()
        throws RemoteException;

    public abstract void updateExternalMediaStatus(boolean paramBoolean1, boolean paramBoolean2)
        throws RemoteException;

    public abstract void updateUserName(int paramInt, String paramString)
        throws RemoteException;

    public abstract void verifyPendingInstall(int paramInt1, int paramInt2)
        throws RemoteException;

    public static abstract class Stub extends Binder
        implements IPackageManager
    {
        private static final String DESCRIPTOR = "android.content.pm.IPackageManager";
        static final int TRANSACTION_addPackageToPreferred = 46;
        static final int TRANSACTION_addPermission = 17;
        static final int TRANSACTION_addPermissionAsync = 75;
        static final int TRANSACTION_addPreferredActivity = 49;
        static final int TRANSACTION_canonicalToCurrentPackageNames = 5;
        static final int TRANSACTION_checkPermission = 15;
        static final int TRANSACTION_checkSignatures = 22;
        static final int TRANSACTION_checkUidPermission = 16;
        static final int TRANSACTION_checkUidSignatures = 23;
        static final int TRANSACTION_clearApplicationUserData = 61;
        static final int TRANSACTION_clearPackagePreferredActivities = 51;
        static final int TRANSACTION_createUser = 78;
        static final int TRANSACTION_currentToCanonicalPackageNames = 4;
        static final int TRANSACTION_deleteApplicationCacheFiles = 60;
        static final int TRANSACTION_deletePackage = 44;
        static final int TRANSACTION_enterSafeMode = 66;
        static final int TRANSACTION_finishPackageInstall = 42;
        static final int TRANSACTION_freeStorage = 59;
        static final int TRANSACTION_freeStorageAndNotify = 58;
        static final int TRANSACTION_getActivityInfo = 11;
        static final int TRANSACTION_getAllPermissionGroups = 9;
        static final int TRANSACTION_getApplicationEnabledSetting = 56;
        static final int TRANSACTION_getApplicationInfo = 10;
        static final int TRANSACTION_getComponentEnabledSetting = 54;
        static final int TRANSACTION_getInstallLocation = 77;
        static final int TRANSACTION_getInstalledApplications = 34;
        static final int TRANSACTION_getInstalledPackages = 33;
        static final int TRANSACTION_getInstallerPackageName = 45;
        static final int TRANSACTION_getInstrumentationInfo = 39;
        static final int TRANSACTION_getNameForUid = 25;
        static final int TRANSACTION_getPackageGids = 3;
        static final int TRANSACTION_getPackageInfo = 1;
        static final int TRANSACTION_getPackageSizeInfo = 62;
        static final int TRANSACTION_getPackageUid = 2;
        static final int TRANSACTION_getPackagesForUid = 24;
        static final int TRANSACTION_getPermissionGroupInfo = 8;
        static final int TRANSACTION_getPermissionInfo = 6;
        static final int TRANSACTION_getPersistentApplications = 35;
        static final int TRANSACTION_getPreferredActivities = 52;
        static final int TRANSACTION_getPreferredPackages = 48;
        static final int TRANSACTION_getProviderInfo = 14;
        static final int TRANSACTION_getReceiverInfo = 12;
        static final int TRANSACTION_getServiceInfo = 13;
        static final int TRANSACTION_getSystemAvailableFeatures = 64;
        static final int TRANSACTION_getSystemSharedLibraryNames = 63;
        static final int TRANSACTION_getUidForSharedUser = 26;
        static final int TRANSACTION_getUser = 86;
        static final int TRANSACTION_getUsers = 85;
        static final int TRANSACTION_getVerifierDeviceIdentity = 83;
        static final int TRANSACTION_grantPermission = 19;
        static final int TRANSACTION_hasSystemFeature = 65;
        static final int TRANSACTION_hasSystemUidErrors = 69;
        static final int TRANSACTION_installPackage = 41;
        static final int TRANSACTION_installPackageWithVerification = 81;
        static final int TRANSACTION_isFirstBoot = 84;
        static final int TRANSACTION_isPermissionEnforced = 88;
        static final int TRANSACTION_isProtectedBroadcast = 21;
        static final int TRANSACTION_isSafeMode = 67;
        static final int TRANSACTION_isStorageLow = 89;
        static final int TRANSACTION_movePackage = 74;
        static final int TRANSACTION_nextPackageToClean = 73;
        static final int TRANSACTION_performBootDexOpt = 70;
        static final int TRANSACTION_performDexOpt = 71;
        static final int TRANSACTION_queryContentProviders = 38;
        static final int TRANSACTION_queryInstrumentation = 40;
        static final int TRANSACTION_queryIntentActivities = 28;
        static final int TRANSACTION_queryIntentActivityOptions = 29;
        static final int TRANSACTION_queryIntentReceivers = 30;
        static final int TRANSACTION_queryIntentServices = 32;
        static final int TRANSACTION_queryPermissionsByGroup = 7;
        static final int TRANSACTION_querySyncProviders = 37;
        static final int TRANSACTION_removePackageFromPreferred = 47;
        static final int TRANSACTION_removePermission = 18;
        static final int TRANSACTION_removeUser = 79;
        static final int TRANSACTION_replacePreferredActivity = 50;
        static final int TRANSACTION_resolveContentProvider = 36;
        static final int TRANSACTION_resolveIntent = 27;
        static final int TRANSACTION_resolveService = 31;
        static final int TRANSACTION_revokePermission = 20;
        static final int TRANSACTION_setApplicationEnabledSetting = 55;
        static final int TRANSACTION_setComponentEnabledSetting = 53;
        static final int TRANSACTION_setInstallLocation = 76;
        static final int TRANSACTION_setInstallerPackageName = 43;
        static final int TRANSACTION_setPackageStoppedState = 57;
        static final int TRANSACTION_setPermissionEnforced = 87;
        static final int TRANSACTION_systemReady = 68;
        static final int TRANSACTION_updateExternalMediaStatus = 72;
        static final int TRANSACTION_updateUserName = 80;
        static final int TRANSACTION_verifyPendingInstall = 82;

        public Stub()
        {
            attachInterface(this, "android.content.pm.IPackageManager");
        }

        public static IPackageManager asInterface(IBinder paramIBinder)
        {
            Object localObject;
            if (paramIBinder == null)
                localObject = null;
            while (true)
            {
                return localObject;
                IInterface localIInterface = paramIBinder.queryLocalInterface("android.content.pm.IPackageManager");
                if ((localIInterface != null) && ((localIInterface instanceof IPackageManager)))
                    localObject = (IPackageManager)localIInterface;
                else
                    localObject = new Proxy(paramIBinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
            throws RemoteException
        {
            boolean bool2;
            switch (paramInt1)
            {
            default:
                bool2 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
            case 1598968902:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            case 65:
            case 66:
            case 67:
            case 68:
            case 69:
            case 70:
            case 71:
            case 72:
            case 73:
            case 74:
            case 75:
            case 76:
            case 77:
            case 78:
            case 79:
            case 80:
            case 81:
            case 82:
            case 83:
            case 84:
            case 85:
                while (true)
                {
                    return bool2;
                    paramParcel2.writeString("android.content.pm.IPackageManager");
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    PackageInfo localPackageInfo = getPackageInfo(paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    if (localPackageInfo != null)
                    {
                        paramParcel2.writeInt(1);
                        localPackageInfo.writeToParcel(paramParcel2, 1);
                    }
                    while (true)
                    {
                        bool2 = true;
                        break;
                        paramParcel2.writeInt(0);
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    int i20 = getPackageUid(paramParcel1.readString(), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i20);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    int[] arrayOfInt = getPackageGids(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    paramParcel2.writeIntArray(arrayOfInt);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    String[] arrayOfString5 = currentToCanonicalPackageNames(paramParcel1.createStringArray());
                    paramParcel2.writeNoException();
                    paramParcel2.writeStringArray(arrayOfString5);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    String[] arrayOfString4 = canonicalToCurrentPackageNames(paramParcel1.createStringArray());
                    paramParcel2.writeNoException();
                    paramParcel2.writeStringArray(arrayOfString4);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    PermissionInfo localPermissionInfo3 = getPermissionInfo(paramParcel1.readString(), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    if (localPermissionInfo3 != null)
                    {
                        paramParcel2.writeInt(1);
                        localPermissionInfo3.writeToParcel(paramParcel2, 1);
                    }
                    while (true)
                    {
                        bool2 = true;
                        break;
                        paramParcel2.writeInt(0);
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    List localList11 = queryPermissionsByGroup(paramParcel1.readString(), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedList(localList11);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    PermissionGroupInfo localPermissionGroupInfo = getPermissionGroupInfo(paramParcel1.readString(), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    if (localPermissionGroupInfo != null)
                    {
                        paramParcel2.writeInt(1);
                        localPermissionGroupInfo.writeToParcel(paramParcel2, 1);
                    }
                    while (true)
                    {
                        bool2 = true;
                        break;
                        paramParcel2.writeInt(0);
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    List localList10 = getAllPermissionGroups(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedList(localList10);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    ApplicationInfo localApplicationInfo = getApplicationInfo(paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    if (localApplicationInfo != null)
                    {
                        paramParcel2.writeInt(1);
                        localApplicationInfo.writeToParcel(paramParcel2, 1);
                    }
                    while (true)
                    {
                        bool2 = true;
                        break;
                        paramParcel2.writeInt(0);
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    ComponentName localComponentName10;
                    if (paramParcel1.readInt() != 0)
                    {
                        localComponentName10 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1);
                        ActivityInfo localActivityInfo2 = getActivityInfo(localComponentName10, paramParcel1.readInt(), paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        if (localActivityInfo2 == null)
                            break label1262;
                        paramParcel2.writeInt(1);
                        localActivityInfo2.writeToParcel(paramParcel2, 1);
                    }
                    while (true)
                    {
                        bool2 = true;
                        break;
                        localComponentName10 = null;
                        break label1213;
                        paramParcel2.writeInt(0);
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    ComponentName localComponentName9;
                    if (paramParcel1.readInt() != 0)
                    {
                        localComponentName9 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1);
                        ActivityInfo localActivityInfo1 = getReceiverInfo(localComponentName9, paramParcel1.readInt(), paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        if (localActivityInfo1 == null)
                            break label1346;
                        paramParcel2.writeInt(1);
                        localActivityInfo1.writeToParcel(paramParcel2, 1);
                    }
                    while (true)
                    {
                        bool2 = true;
                        break;
                        localComponentName9 = null;
                        break label1297;
                        paramParcel2.writeInt(0);
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    ComponentName localComponentName8;
                    if (paramParcel1.readInt() != 0)
                    {
                        localComponentName8 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1);
                        ServiceInfo localServiceInfo = getServiceInfo(localComponentName8, paramParcel1.readInt(), paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        if (localServiceInfo == null)
                            break label1430;
                        paramParcel2.writeInt(1);
                        localServiceInfo.writeToParcel(paramParcel2, 1);
                    }
                    while (true)
                    {
                        bool2 = true;
                        break;
                        localComponentName8 = null;
                        break label1381;
                        paramParcel2.writeInt(0);
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    ComponentName localComponentName7;
                    if (paramParcel1.readInt() != 0)
                    {
                        localComponentName7 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1);
                        ProviderInfo localProviderInfo2 = getProviderInfo(localComponentName7, paramParcel1.readInt(), paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        if (localProviderInfo2 == null)
                            break label1514;
                        paramParcel2.writeInt(1);
                        localProviderInfo2.writeToParcel(paramParcel2, 1);
                    }
                    while (true)
                    {
                        bool2 = true;
                        break;
                        localComponentName7 = null;
                        break label1465;
                        paramParcel2.writeInt(0);
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    int i19 = checkPermission(paramParcel1.readString(), paramParcel1.readString());
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i19);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    int i18 = checkUidPermission(paramParcel1.readString(), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i18);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    PermissionInfo localPermissionInfo2;
                    if (paramParcel1.readInt() != 0)
                    {
                        localPermissionInfo2 = (PermissionInfo)PermissionInfo.CREATOR.createFromParcel(paramParcel1);
                        boolean bool17 = addPermission(localPermissionInfo2);
                        paramParcel2.writeNoException();
                        if (!bool17)
                            break label1659;
                    }
                    for (int i17 = 1; ; i17 = 0)
                    {
                        paramParcel2.writeInt(i17);
                        bool2 = true;
                        break;
                        localPermissionInfo2 = null;
                        break label1621;
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    removePermission(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    grantPermission(paramParcel1.readString(), paramParcel1.readString());
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    revokePermission(paramParcel1.readString(), paramParcel1.readString());
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    boolean bool16 = isProtectedBroadcast(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (bool16);
                    for (int i16 = 1; ; i16 = 0)
                    {
                        paramParcel2.writeInt(i16);
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    int i15 = checkSignatures(paramParcel1.readString(), paramParcel1.readString());
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i15);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    int i14 = checkUidSignatures(paramParcel1.readInt(), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i14);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    String[] arrayOfString3 = getPackagesForUid(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    paramParcel2.writeStringArray(arrayOfString3);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    String str6 = getNameForUid(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    paramParcel2.writeString(str6);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    int i13 = getUidForSharedUser(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i13);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    Intent localIntent6;
                    if (paramParcel1.readInt() != 0)
                    {
                        localIntent6 = (Intent)Intent.CREATOR.createFromParcel(paramParcel1);
                        ResolveInfo localResolveInfo2 = resolveIntent(localIntent6, paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        if (localResolveInfo2 == null)
                            break label2039;
                        paramParcel2.writeInt(1);
                        localResolveInfo2.writeToParcel(paramParcel2, 1);
                    }
                    while (true)
                    {
                        bool2 = true;
                        break;
                        localIntent6 = null;
                        break label1986;
                        paramParcel2.writeInt(0);
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    if (paramParcel1.readInt() != 0);
                    for (Intent localIntent5 = (Intent)Intent.CREATOR.createFromParcel(paramParcel1); ; localIntent5 = null)
                    {
                        List localList9 = queryIntentActivities(localIntent5, paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        paramParcel2.writeTypedList(localList9);
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    ComponentName localComponentName6;
                    Intent[] arrayOfIntent;
                    String[] arrayOfString2;
                    if (paramParcel1.readInt() != 0)
                    {
                        localComponentName6 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1);
                        arrayOfIntent = (Intent[])paramParcel1.createTypedArray(Intent.CREATOR);
                        arrayOfString2 = paramParcel1.createStringArray();
                        if (paramParcel1.readInt() == 0)
                            break label2230;
                    }
                    for (Intent localIntent4 = (Intent)Intent.CREATOR.createFromParcel(paramParcel1); ; localIntent4 = null)
                    {
                        List localList8 = queryIntentActivityOptions(localComponentName6, arrayOfIntent, arrayOfString2, localIntent4, paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        paramParcel2.writeTypedList(localList8);
                        bool2 = true;
                        break;
                        localComponentName6 = null;
                        break label2143;
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    if (paramParcel1.readInt() != 0);
                    for (Intent localIntent3 = (Intent)Intent.CREATOR.createFromParcel(paramParcel1); ; localIntent3 = null)
                    {
                        List localList7 = queryIntentReceivers(localIntent3, paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        paramParcel2.writeTypedList(localList7);
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    Intent localIntent2;
                    if (paramParcel1.readInt() != 0)
                    {
                        localIntent2 = (Intent)Intent.CREATOR.createFromParcel(paramParcel1);
                        ResolveInfo localResolveInfo1 = resolveService(localIntent2, paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        if (localResolveInfo1 == null)
                            break label2385;
                        paramParcel2.writeInt(1);
                        localResolveInfo1.writeToParcel(paramParcel2, 1);
                    }
                    while (true)
                    {
                        bool2 = true;
                        break;
                        localIntent2 = null;
                        break label2332;
                        paramParcel2.writeInt(0);
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    if (paramParcel1.readInt() != 0);
                    for (Intent localIntent1 = (Intent)Intent.CREATOR.createFromParcel(paramParcel1); ; localIntent1 = null)
                    {
                        List localList6 = queryIntentServices(localIntent1, paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        paramParcel2.writeTypedList(localList6);
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    ParceledListSlice localParceledListSlice2 = getInstalledPackages(paramParcel1.readInt(), paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (localParceledListSlice2 != null)
                    {
                        paramParcel2.writeInt(1);
                        localParceledListSlice2.writeToParcel(paramParcel2, 1);
                    }
                    while (true)
                    {
                        bool2 = true;
                        break;
                        paramParcel2.writeInt(0);
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    ParceledListSlice localParceledListSlice1 = getInstalledApplications(paramParcel1.readInt(), paramParcel1.readString(), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    if (localParceledListSlice1 != null)
                    {
                        paramParcel2.writeInt(1);
                        localParceledListSlice1.writeToParcel(paramParcel2, 1);
                    }
                    while (true)
                    {
                        bool2 = true;
                        break;
                        paramParcel2.writeInt(0);
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    List localList5 = getPersistentApplications(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedList(localList5);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    ProviderInfo localProviderInfo1 = resolveContentProvider(paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    if (localProviderInfo1 != null)
                    {
                        paramParcel2.writeInt(1);
                        localProviderInfo1.writeToParcel(paramParcel2, 1);
                    }
                    while (true)
                    {
                        bool2 = true;
                        break;
                        paramParcel2.writeInt(0);
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    ArrayList localArrayList3 = paramParcel1.createStringArrayList();
                    ArrayList localArrayList4 = paramParcel1.createTypedArrayList(ProviderInfo.CREATOR);
                    querySyncProviders(localArrayList3, localArrayList4);
                    paramParcel2.writeNoException();
                    paramParcel2.writeStringList(localArrayList3);
                    paramParcel2.writeTypedList(localArrayList4);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    List localList4 = queryContentProviders(paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedList(localList4);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    ComponentName localComponentName5;
                    if (paramParcel1.readInt() != 0)
                    {
                        localComponentName5 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1);
                        InstrumentationInfo localInstrumentationInfo = getInstrumentationInfo(localComponentName5, paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        if (localInstrumentationInfo == null)
                            break label2830;
                        paramParcel2.writeInt(1);
                        localInstrumentationInfo.writeToParcel(paramParcel2, 1);
                    }
                    while (true)
                    {
                        bool2 = true;
                        break;
                        localComponentName5 = null;
                        break label2785;
                        paramParcel2.writeInt(0);
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    List localList3 = queryInstrumentation(paramParcel1.readString(), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedList(localList3);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    if (paramParcel1.readInt() != 0);
                    for (Uri localUri3 = (Uri)Uri.CREATOR.createFromParcel(paramParcel1); ; localUri3 = null)
                    {
                        installPackage(localUri3, IPackageInstallObserver.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readInt(), paramParcel1.readString());
                        paramParcel2.writeNoException();
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    finishPackageInstall(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    setInstallerPackageName(paramParcel1.readString(), paramParcel1.readString());
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    deletePackage(paramParcel1.readString(), IPackageDeleteObserver.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    String str5 = getInstallerPackageName(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    paramParcel2.writeString(str5);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    addPackageToPreferred(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    removePackageFromPreferred(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    List localList2 = getPreferredPackages(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedList(localList2);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    IntentFilter localIntentFilter2;
                    int i12;
                    ComponentName[] arrayOfComponentName2;
                    if (paramParcel1.readInt() != 0)
                    {
                        localIntentFilter2 = (IntentFilter)IntentFilter.CREATOR.createFromParcel(paramParcel1);
                        i12 = paramParcel1.readInt();
                        arrayOfComponentName2 = (ComponentName[])paramParcel1.createTypedArray(ComponentName.CREATOR);
                        if (paramParcel1.readInt() == 0)
                            break label3231;
                    }
                    for (ComponentName localComponentName4 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName4 = null)
                    {
                        addPreferredActivity(localIntentFilter2, i12, arrayOfComponentName2, localComponentName4);
                        paramParcel2.writeNoException();
                        bool2 = true;
                        break;
                        localIntentFilter2 = null;
                        break label3164;
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    IntentFilter localIntentFilter1;
                    int i11;
                    ComponentName[] arrayOfComponentName1;
                    if (paramParcel1.readInt() != 0)
                    {
                        localIntentFilter1 = (IntentFilter)IntentFilter.CREATOR.createFromParcel(paramParcel1);
                        i11 = paramParcel1.readInt();
                        arrayOfComponentName1 = (ComponentName[])paramParcel1.createTypedArray(ComponentName.CREATOR);
                        if (paramParcel1.readInt() == 0)
                            break label3331;
                    }
                    for (ComponentName localComponentName3 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName3 = null)
                    {
                        replacePreferredActivity(localIntentFilter1, i11, arrayOfComponentName1, localComponentName3);
                        paramParcel2.writeNoException();
                        bool2 = true;
                        break;
                        localIntentFilter1 = null;
                        break label3264;
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    clearPackagePreferredActivities(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    ArrayList localArrayList1 = new ArrayList();
                    ArrayList localArrayList2 = new ArrayList();
                    int i10 = getPreferredActivities(localArrayList1, localArrayList2, paramParcel1.readString());
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i10);
                    paramParcel2.writeTypedList(localArrayList1);
                    paramParcel2.writeTypedList(localArrayList2);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    if (paramParcel1.readInt() != 0);
                    for (ComponentName localComponentName2 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName2 = null)
                    {
                        setComponentEnabledSetting(localComponentName2, paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    if (paramParcel1.readInt() != 0);
                    for (ComponentName localComponentName1 = (ComponentName)ComponentName.CREATOR.createFromParcel(paramParcel1); ; localComponentName1 = null)
                    {
                        int i9 = getComponentEnabledSetting(localComponentName1, paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        paramParcel2.writeInt(i9);
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    setApplicationEnabledSetting(paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.readInt(), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    int i8 = getApplicationEnabledSetting(paramParcel1.readString(), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i8);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    String str4 = paramParcel1.readString();
                    if (paramParcel1.readInt() != 0);
                    for (boolean bool15 = true; ; bool15 = false)
                    {
                        setPackageStoppedState(str4, bool15, paramParcel1.readInt());
                        paramParcel2.writeNoException();
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    freeStorageAndNotify(paramParcel1.readLong(), IPackageDataObserver.Stub.asInterface(paramParcel1.readStrongBinder()));
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    long l = paramParcel1.readLong();
                    if (paramParcel1.readInt() != 0);
                    for (IntentSender localIntentSender = (IntentSender)IntentSender.CREATOR.createFromParcel(paramParcel1); ; localIntentSender = null)
                    {
                        freeStorage(l, localIntentSender);
                        paramParcel2.writeNoException();
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    deleteApplicationCacheFiles(paramParcel1.readString(), IPackageDataObserver.Stub.asInterface(paramParcel1.readStrongBinder()));
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    clearApplicationUserData(paramParcel1.readString(), IPackageDataObserver.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    getPackageSizeInfo(paramParcel1.readString(), IPackageStatsObserver.Stub.asInterface(paramParcel1.readStrongBinder()));
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    String[] arrayOfString1 = getSystemSharedLibraryNames();
                    paramParcel2.writeNoException();
                    paramParcel2.writeStringArray(arrayOfString1);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    FeatureInfo[] arrayOfFeatureInfo = getSystemAvailableFeatures();
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedArray(arrayOfFeatureInfo, 1);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    boolean bool14 = hasSystemFeature(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (bool14);
                    for (int i7 = 1; ; i7 = 0)
                    {
                        paramParcel2.writeInt(i7);
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    enterSafeMode();
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    boolean bool13 = isSafeMode();
                    paramParcel2.writeNoException();
                    if (bool13);
                    for (int i6 = 1; ; i6 = 0)
                    {
                        paramParcel2.writeInt(i6);
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    systemReady();
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    boolean bool12 = hasSystemUidErrors();
                    paramParcel2.writeNoException();
                    if (bool12);
                    for (int i5 = 1; ; i5 = 0)
                    {
                        paramParcel2.writeInt(i5);
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    performBootDexOpt();
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    boolean bool11 = performDexOpt(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    if (bool11);
                    for (int i4 = 1; ; i4 = 0)
                    {
                        paramParcel2.writeInt(i4);
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    boolean bool9;
                    if (paramParcel1.readInt() != 0)
                    {
                        bool9 = true;
                        if (paramParcel1.readInt() == 0)
                            break label4199;
                    }
                    for (boolean bool10 = true; ; bool10 = false)
                    {
                        updateExternalMediaStatus(bool9, bool10);
                        paramParcel2.writeNoException();
                        bool2 = true;
                        break;
                        bool9 = false;
                        break label4165;
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    String str3 = nextPackageToClean(paramParcel1.readString());
                    paramParcel2.writeNoException();
                    paramParcel2.writeString(str3);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    movePackage(paramParcel1.readString(), IPackageMoveObserver.Stub.asInterface(paramParcel1.readStrongBinder()), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    PermissionInfo localPermissionInfo1;
                    if (paramParcel1.readInt() != 0)
                    {
                        localPermissionInfo1 = (PermissionInfo)PermissionInfo.CREATOR.createFromParcel(paramParcel1);
                        boolean bool8 = addPermissionAsync(localPermissionInfo1);
                        paramParcel2.writeNoException();
                        if (!bool8)
                            break label4337;
                    }
                    for (int i3 = 1; ; i3 = 0)
                    {
                        paramParcel2.writeInt(i3);
                        bool2 = true;
                        break;
                        localPermissionInfo1 = null;
                        break label4299;
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    boolean bool7 = setInstallLocation(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    if (bool7);
                    for (int i2 = 1; ; i2 = 0)
                    {
                        paramParcel2.writeInt(i2);
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    int i1 = getInstallLocation();
                    paramParcel2.writeNoException();
                    paramParcel2.writeInt(i1);
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    UserInfo localUserInfo2 = createUser(paramParcel1.readString(), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    if (localUserInfo2 != null)
                    {
                        paramParcel2.writeInt(1);
                        localUserInfo2.writeToParcel(paramParcel2, 1);
                    }
                    while (true)
                    {
                        bool2 = true;
                        break;
                        paramParcel2.writeInt(0);
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    boolean bool6 = removeUser(paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    if (bool6);
                    for (int n = 1; ; n = 0)
                    {
                        paramParcel2.writeInt(n);
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    updateUserName(paramParcel1.readInt(), paramParcel1.readString());
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    Uri localUri1;
                    IPackageInstallObserver localIPackageInstallObserver;
                    int m;
                    String str2;
                    Uri localUri2;
                    ManifestDigest localManifestDigest;
                    if (paramParcel1.readInt() != 0)
                    {
                        localUri1 = (Uri)Uri.CREATOR.createFromParcel(paramParcel1);
                        localIPackageInstallObserver = IPackageInstallObserver.Stub.asInterface(paramParcel1.readStrongBinder());
                        m = paramParcel1.readInt();
                        str2 = paramParcel1.readString();
                        if (paramParcel1.readInt() == 0)
                            break label4691;
                        localUri2 = (Uri)Uri.CREATOR.createFromParcel(paramParcel1);
                        if (paramParcel1.readInt() == 0)
                            break label4697;
                        localManifestDigest = (ManifestDigest)ManifestDigest.CREATOR.createFromParcel(paramParcel1);
                        if (paramParcel1.readInt() == 0)
                            break label4703;
                    }
                    for (ContainerEncryptionParams localContainerEncryptionParams = (ContainerEncryptionParams)ContainerEncryptionParams.CREATOR.createFromParcel(paramParcel1); ; localContainerEncryptionParams = null)
                    {
                        installPackageWithVerification(localUri1, localIPackageInstallObserver, m, str2, localUri2, localManifestDigest, localContainerEncryptionParams);
                        paramParcel2.writeNoException();
                        bool2 = true;
                        break;
                        localUri1 = null;
                        break label4573;
                        localUri2 = null;
                        break label4615;
                        localManifestDigest = null;
                        break label4636;
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    verifyPendingInstall(paramParcel1.readInt(), paramParcel1.readInt());
                    paramParcel2.writeNoException();
                    bool2 = true;
                    continue;
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    VerifierDeviceIdentity localVerifierDeviceIdentity = getVerifierDeviceIdentity();
                    paramParcel2.writeNoException();
                    if (localVerifierDeviceIdentity != null)
                    {
                        paramParcel2.writeInt(1);
                        localVerifierDeviceIdentity.writeToParcel(paramParcel2, 1);
                    }
                    while (true)
                    {
                        bool2 = true;
                        break;
                        paramParcel2.writeInt(0);
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    boolean bool5 = isFirstBoot();
                    paramParcel2.writeNoException();
                    if (bool5);
                    for (int k = 1; ; k = 0)
                    {
                        paramParcel2.writeInt(k);
                        bool2 = true;
                        break;
                    }
                    paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                    List localList1 = getUsers();
                    paramParcel2.writeNoException();
                    paramParcel2.writeTypedList(localList1);
                    bool2 = true;
                }
            case 86:
                paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                UserInfo localUserInfo1 = getUser(paramParcel1.readInt());
                paramParcel2.writeNoException();
                if (localUserInfo1 != null)
                {
                    paramParcel2.writeInt(1);
                    localUserInfo1.writeToParcel(paramParcel2, 1);
                }
                while (true)
                {
                    bool2 = true;
                    break;
                    paramParcel2.writeInt(0);
                }
            case 87:
                label2332: label3231: paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                label2830: label4636: String str1 = paramParcel1.readString();
                label3331: label4615: if (paramParcel1.readInt() != 0);
                for (boolean bool4 = true; ; bool4 = false)
                {
                    setPermissionEnforced(str1, bool4);
                    paramParcel2.writeNoException();
                    bool2 = true;
                    break;
                }
            case 88:
                label1213: label1262: label1659: paramParcel1.enforceInterface("android.content.pm.IPackageManager");
                label1297: label1346: label1381: label1514: label2039: boolean bool3 = isPermissionEnforced(paramParcel1.readString());
                label1430: label1465: label1621: label2143: label2785: label4199: label4337: paramParcel2.writeNoException();
                label1986: label2385: label3164: label4573: label4703: if (bool3);
                label2230: label3264: label4165: label4299: label4691: label4697: for (int j = 1; ; j = 0)
                {
                    paramParcel2.writeInt(j);
                    bool2 = true;
                    break;
                }
            case 89:
            }
            paramParcel1.enforceInterface("android.content.pm.IPackageManager");
            boolean bool1 = isStorageLow();
            paramParcel2.writeNoException();
            if (bool1);
            for (int i = 1; ; i = 0)
            {
                paramParcel2.writeInt(i);
                bool2 = true;
                break;
            }
        }

        private static class Proxy
            implements IPackageManager
        {
            private IBinder mRemote;

            Proxy(IBinder paramIBinder)
            {
                this.mRemote = paramIBinder;
            }

            public void addPackageToPreferred(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(46, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean addPermission(PermissionInfo paramPermissionInfo)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                        if (paramPermissionInfo != null)
                        {
                            localParcel1.writeInt(1);
                            paramPermissionInfo.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(17, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public boolean addPermissionAsync(PermissionInfo paramPermissionInfo)
                throws RemoteException
            {
                boolean bool = true;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                        if (paramPermissionInfo != null)
                        {
                            localParcel1.writeInt(1);
                            paramPermissionInfo.writeToParcel(localParcel1, 0);
                            this.mRemote.transact(75, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            int i = localParcel2.readInt();
                            if (i != 0)
                                return bool;
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    bool = false;
                }
            }

            public void addPreferredActivity(IntentFilter paramIntentFilter, int paramInt, ComponentName[] paramArrayOfComponentName, ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                        if (paramIntentFilter != null)
                        {
                            localParcel1.writeInt(1);
                            paramIntentFilter.writeToParcel(localParcel1, 0);
                            localParcel1.writeInt(paramInt);
                            localParcel1.writeTypedArray(paramArrayOfComponentName, 0);
                            if (paramComponentName != null)
                            {
                                localParcel1.writeInt(1);
                                paramComponentName.writeToParcel(localParcel1, 0);
                                this.mRemote.transact(49, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    localParcel1.writeInt(0);
                }
            }

            public IBinder asBinder()
            {
                return this.mRemote;
            }

            public String[] canonicalToCurrentPackageNames(String[] paramArrayOfString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeStringArray(paramArrayOfString);
                    this.mRemote.transact(5, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String[] arrayOfString = localParcel2.createStringArray();
                    return arrayOfString;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int checkPermission(String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    this.mRemote.transact(15, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int checkSignatures(String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    this.mRemote.transact(22, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int checkUidPermission(String paramString, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(16, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int checkUidSignatures(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(23, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void clearApplicationUserData(String paramString, IPackageDataObserver paramIPackageDataObserver, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    if (paramIPackageDataObserver != null)
                    {
                        localIBinder = paramIPackageDataObserver.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(61, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void clearPackagePreferredActivities(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(51, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public UserInfo createUser(String paramString, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(78, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localUserInfo = (UserInfo)UserInfo.CREATOR.createFromParcel(localParcel2);
                        return localUserInfo;
                    }
                    UserInfo localUserInfo = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String[] currentToCanonicalPackageNames(String[] paramArrayOfString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeStringArray(paramArrayOfString);
                    this.mRemote.transact(4, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String[] arrayOfString = localParcel2.createStringArray();
                    return arrayOfString;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void deleteApplicationCacheFiles(String paramString, IPackageDataObserver paramIPackageDataObserver)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    if (paramIPackageDataObserver != null)
                    {
                        localIBinder = paramIPackageDataObserver.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(60, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void deletePackage(String paramString, IPackageDeleteObserver paramIPackageDeleteObserver, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    if (paramIPackageDeleteObserver != null)
                    {
                        localIBinder = paramIPackageDeleteObserver.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(44, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void enterSafeMode()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    this.mRemote.transact(66, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void finishPackageInstall(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(42, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void freeStorage(long paramLong, IntentSender paramIntentSender)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeLong(paramLong);
                    if (paramIntentSender != null)
                    {
                        localParcel1.writeInt(1);
                        paramIntentSender.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        this.mRemote.transact(59, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void freeStorageAndNotify(long paramLong, IPackageDataObserver paramIPackageDataObserver)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeLong(paramLong);
                    if (paramIPackageDataObserver != null)
                    {
                        localIBinder = paramIPackageDataObserver.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(58, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public ActivityInfo getActivityInfo(ComponentName paramComponentName, int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                        if (paramComponentName != null)
                        {
                            localParcel1.writeInt(1);
                            paramComponentName.writeToParcel(localParcel1, 0);
                            localParcel1.writeInt(paramInt1);
                            localParcel1.writeInt(paramInt2);
                            this.mRemote.transact(11, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            if (localParcel2.readInt() != 0)
                            {
                                localActivityInfo = (ActivityInfo)ActivityInfo.CREATOR.createFromParcel(localParcel2);
                                return localActivityInfo;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    ActivityInfo localActivityInfo = null;
                }
            }

            public List<PermissionGroupInfo> getAllPermissionGroups(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(9, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(PermissionGroupInfo.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getApplicationEnabledSetting(String paramString, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(56, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public ApplicationInfo getApplicationInfo(String paramString, int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(10, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localApplicationInfo = (ApplicationInfo)ApplicationInfo.CREATOR.createFromParcel(localParcel2);
                        return localApplicationInfo;
                    }
                    ApplicationInfo localApplicationInfo = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getComponentEnabledSetting(ComponentName paramComponentName, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(54, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        int i = localParcel2.readInt();
                        return i;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getInstallLocation()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    this.mRemote.transact(77, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public ParceledListSlice getInstalledApplications(int paramInt1, String paramString, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(34, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localParceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(localParcel2);
                        return localParceledListSlice;
                    }
                    ParceledListSlice localParceledListSlice = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public ParceledListSlice getInstalledPackages(int paramInt, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(33, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localParceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(localParcel2);
                        return localParceledListSlice;
                    }
                    ParceledListSlice localParceledListSlice = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String getInstallerPackageName(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(45, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public InstrumentationInfo getInstrumentationInfo(ComponentName paramComponentName, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                        if (paramComponentName != null)
                        {
                            localParcel1.writeInt(1);
                            paramComponentName.writeToParcel(localParcel1, 0);
                            localParcel1.writeInt(paramInt);
                            this.mRemote.transact(39, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            if (localParcel2.readInt() != 0)
                            {
                                localInstrumentationInfo = (InstrumentationInfo)InstrumentationInfo.CREATOR.createFromParcel(localParcel2);
                                return localInstrumentationInfo;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    InstrumentationInfo localInstrumentationInfo = null;
                }
            }

            public String getInterfaceDescriptor()
            {
                return "android.content.pm.IPackageManager";
            }

            public String getNameForUid(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(25, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int[] getPackageGids(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(3, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int[] arrayOfInt = localParcel2.createIntArray();
                    return arrayOfInt;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public PackageInfo getPackageInfo(String paramString, int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(1, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localPackageInfo = (PackageInfo)PackageInfo.CREATOR.createFromParcel(localParcel2);
                        return localPackageInfo;
                    }
                    PackageInfo localPackageInfo = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void getPackageSizeInfo(String paramString, IPackageStatsObserver paramIPackageStatsObserver)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    if (paramIPackageStatsObserver != null)
                    {
                        localIBinder = paramIPackageStatsObserver.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        this.mRemote.transact(62, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getPackageUid(String paramString, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(2, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String[] getPackagesForUid(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(24, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String[] arrayOfString = localParcel2.createStringArray();
                    return arrayOfString;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public PermissionGroupInfo getPermissionGroupInfo(String paramString, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(8, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localPermissionGroupInfo = (PermissionGroupInfo)PermissionGroupInfo.CREATOR.createFromParcel(localParcel2);
                        return localPermissionGroupInfo;
                    }
                    PermissionGroupInfo localPermissionGroupInfo = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public PermissionInfo getPermissionInfo(String paramString, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(6, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localPermissionInfo = (PermissionInfo)PermissionInfo.CREATOR.createFromParcel(localParcel2);
                        return localPermissionInfo;
                    }
                    PermissionInfo localPermissionInfo = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<ApplicationInfo> getPersistentApplications(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(35, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(ApplicationInfo.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getPreferredActivities(List<IntentFilter> paramList, List<ComponentName> paramList1, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(52, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    localParcel2.readTypedList(paramList, IntentFilter.CREATOR);
                    localParcel2.readTypedList(paramList1, ComponentName.CREATOR);
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<PackageInfo> getPreferredPackages(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(48, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(PackageInfo.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public ProviderInfo getProviderInfo(ComponentName paramComponentName, int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                        if (paramComponentName != null)
                        {
                            localParcel1.writeInt(1);
                            paramComponentName.writeToParcel(localParcel1, 0);
                            localParcel1.writeInt(paramInt1);
                            localParcel1.writeInt(paramInt2);
                            this.mRemote.transact(14, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            if (localParcel2.readInt() != 0)
                            {
                                localProviderInfo = (ProviderInfo)ProviderInfo.CREATOR.createFromParcel(localParcel2);
                                return localProviderInfo;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    ProviderInfo localProviderInfo = null;
                }
            }

            public ActivityInfo getReceiverInfo(ComponentName paramComponentName, int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                        if (paramComponentName != null)
                        {
                            localParcel1.writeInt(1);
                            paramComponentName.writeToParcel(localParcel1, 0);
                            localParcel1.writeInt(paramInt1);
                            localParcel1.writeInt(paramInt2);
                            this.mRemote.transact(12, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            if (localParcel2.readInt() != 0)
                            {
                                localActivityInfo = (ActivityInfo)ActivityInfo.CREATOR.createFromParcel(localParcel2);
                                return localActivityInfo;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    ActivityInfo localActivityInfo = null;
                }
            }

            public ServiceInfo getServiceInfo(ComponentName paramComponentName, int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                        if (paramComponentName != null)
                        {
                            localParcel1.writeInt(1);
                            paramComponentName.writeToParcel(localParcel1, 0);
                            localParcel1.writeInt(paramInt1);
                            localParcel1.writeInt(paramInt2);
                            this.mRemote.transact(13, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            if (localParcel2.readInt() != 0)
                            {
                                localServiceInfo = (ServiceInfo)ServiceInfo.CREATOR.createFromParcel(localParcel2);
                                return localServiceInfo;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    ServiceInfo localServiceInfo = null;
                }
            }

            public FeatureInfo[] getSystemAvailableFeatures()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    this.mRemote.transact(64, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    FeatureInfo[] arrayOfFeatureInfo = (FeatureInfo[])localParcel2.createTypedArray(FeatureInfo.CREATOR);
                    return arrayOfFeatureInfo;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String[] getSystemSharedLibraryNames()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    this.mRemote.transact(63, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String[] arrayOfString = localParcel2.createStringArray();
                    return arrayOfString;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public int getUidForSharedUser(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(26, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    return i;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public UserInfo getUser(int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(86, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localUserInfo = (UserInfo)UserInfo.CREATOR.createFromParcel(localParcel2);
                        return localUserInfo;
                    }
                    UserInfo localUserInfo = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<UserInfo> getUsers()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    this.mRemote.transact(85, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(UserInfo.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public VerifierDeviceIdentity getVerifierDeviceIdentity()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    this.mRemote.transact(83, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localVerifierDeviceIdentity = (VerifierDeviceIdentity)VerifierDeviceIdentity.CREATOR.createFromParcel(localParcel2);
                        return localVerifierDeviceIdentity;
                    }
                    VerifierDeviceIdentity localVerifierDeviceIdentity = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void grantPermission(String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    this.mRemote.transact(19, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean hasSystemFeature(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(65, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean hasSystemUidErrors()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    this.mRemote.transact(69, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void installPackage(Uri paramUri, IPackageInstallObserver paramIPackageInstallObserver, int paramInt, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                        if (paramUri != null)
                        {
                            localParcel1.writeInt(1);
                            paramUri.writeToParcel(localParcel1, 0);
                            if (paramIPackageInstallObserver != null)
                            {
                                localIBinder = paramIPackageInstallObserver.asBinder();
                                localParcel1.writeStrongBinder(localIBinder);
                                localParcel1.writeInt(paramInt);
                                localParcel1.writeString(paramString);
                                this.mRemote.transact(41, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    IBinder localIBinder = null;
                }
            }

            public void installPackageWithVerification(Uri paramUri1, IPackageInstallObserver paramIPackageInstallObserver, int paramInt, String paramString, Uri paramUri2, ManifestDigest paramManifestDigest, ContainerEncryptionParams paramContainerEncryptionParams)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                        if (paramUri1 != null)
                        {
                            localParcel1.writeInt(1);
                            paramUri1.writeToParcel(localParcel1, 0);
                            if (paramIPackageInstallObserver != null)
                            {
                                localIBinder = paramIPackageInstallObserver.asBinder();
                                localParcel1.writeStrongBinder(localIBinder);
                                localParcel1.writeInt(paramInt);
                                localParcel1.writeString(paramString);
                                if (paramUri2 == null)
                                    break label186;
                                localParcel1.writeInt(1);
                                paramUri2.writeToParcel(localParcel1, 0);
                                if (paramManifestDigest == null)
                                    break label195;
                                localParcel1.writeInt(1);
                                paramManifestDigest.writeToParcel(localParcel1, 0);
                                if (paramContainerEncryptionParams == null)
                                    break label204;
                                localParcel1.writeInt(1);
                                paramContainerEncryptionParams.writeToParcel(localParcel1, 0);
                                this.mRemote.transact(81, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    IBinder localIBinder = null;
                    continue;
                    label186: localParcel1.writeInt(0);
                    continue;
                    label195: localParcel1.writeInt(0);
                    continue;
                    label204: localParcel1.writeInt(0);
                }
            }

            public boolean isFirstBoot()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    this.mRemote.transact(84, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isPermissionEnforced(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(88, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isProtectedBroadcast(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(21, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isSafeMode()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    this.mRemote.transact(67, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean isStorageLow()
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    this.mRemote.transact(89, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void movePackage(String paramString, IPackageMoveObserver paramIPackageMoveObserver, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    if (paramIPackageMoveObserver != null)
                    {
                        localIBinder = paramIPackageMoveObserver.asBinder();
                        localParcel1.writeStrongBinder(localIBinder);
                        localParcel1.writeInt(paramInt);
                        this.mRemote.transact(74, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    IBinder localIBinder = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public String nextPackageToClean(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(73, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    String str = localParcel2.readString();
                    return str;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void performBootDexOpt()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    this.mRemote.transact(70, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean performDexOpt(String paramString)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(71, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<ProviderInfo> queryContentProviders(String paramString, int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(38, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(ProviderInfo.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<InstrumentationInfo> queryInstrumentation(String paramString, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(40, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(InstrumentationInfo.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<ResolveInfo> queryIntentActivities(Intent paramIntent, String paramString, int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    if (paramIntent != null)
                    {
                        localParcel1.writeInt(1);
                        paramIntent.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeString(paramString);
                        localParcel1.writeInt(paramInt1);
                        localParcel1.writeInt(paramInt2);
                        this.mRemote.transact(28, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        ArrayList localArrayList = localParcel2.createTypedArrayList(ResolveInfo.CREATOR);
                        return localArrayList;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<ResolveInfo> queryIntentActivityOptions(ComponentName paramComponentName, Intent[] paramArrayOfIntent, String[] paramArrayOfString, Intent paramIntent, String paramString, int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                        if (paramComponentName != null)
                        {
                            localParcel1.writeInt(1);
                            paramComponentName.writeToParcel(localParcel1, 0);
                            localParcel1.writeTypedArray(paramArrayOfIntent, 0);
                            localParcel1.writeStringArray(paramArrayOfString);
                            if (paramIntent != null)
                            {
                                localParcel1.writeInt(1);
                                paramIntent.writeToParcel(localParcel1, 0);
                                localParcel1.writeString(paramString);
                                localParcel1.writeInt(paramInt1);
                                localParcel1.writeInt(paramInt2);
                                this.mRemote.transact(29, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                                ArrayList localArrayList = localParcel2.createTypedArrayList(ResolveInfo.CREATOR);
                                return localArrayList;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    localParcel1.writeInt(0);
                }
            }

            public List<ResolveInfo> queryIntentReceivers(Intent paramIntent, String paramString, int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    if (paramIntent != null)
                    {
                        localParcel1.writeInt(1);
                        paramIntent.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeString(paramString);
                        localParcel1.writeInt(paramInt1);
                        localParcel1.writeInt(paramInt2);
                        this.mRemote.transact(30, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        ArrayList localArrayList = localParcel2.createTypedArrayList(ResolveInfo.CREATOR);
                        return localArrayList;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<ResolveInfo> queryIntentServices(Intent paramIntent, String paramString, int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    if (paramIntent != null)
                    {
                        localParcel1.writeInt(1);
                        paramIntent.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeString(paramString);
                        localParcel1.writeInt(paramInt1);
                        localParcel1.writeInt(paramInt2);
                        this.mRemote.transact(32, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        ArrayList localArrayList = localParcel2.createTypedArrayList(ResolveInfo.CREATOR);
                        return localArrayList;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public List<PermissionInfo> queryPermissionsByGroup(String paramString, int paramInt)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(7, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    ArrayList localArrayList = localParcel2.createTypedArrayList(PermissionInfo.CREATOR);
                    return localArrayList;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void querySyncProviders(List<String> paramList, List<ProviderInfo> paramList1)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeStringList(paramList);
                    localParcel1.writeTypedList(paramList1);
                    this.mRemote.transact(37, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    localParcel2.readStringList(paramList);
                    localParcel2.readTypedList(paramList1, ProviderInfo.CREATOR);
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void removePackageFromPreferred(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(47, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void removePermission(String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(18, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean removeUser(int paramInt)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(79, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void replacePreferredActivity(IntentFilter paramIntentFilter, int paramInt, ComponentName[] paramArrayOfComponentName, ComponentName paramComponentName)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                        if (paramIntentFilter != null)
                        {
                            localParcel1.writeInt(1);
                            paramIntentFilter.writeToParcel(localParcel1, 0);
                            localParcel1.writeInt(paramInt);
                            localParcel1.writeTypedArray(paramArrayOfComponentName, 0);
                            if (paramComponentName != null)
                            {
                                localParcel1.writeInt(1);
                                paramComponentName.writeToParcel(localParcel1, 0);
                                this.mRemote.transact(50, localParcel1, localParcel2, 0);
                                localParcel2.readException();
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    localParcel1.writeInt(0);
                }
            }

            public ProviderInfo resolveContentProvider(String paramString, int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(36, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    if (localParcel2.readInt() != 0)
                    {
                        localProviderInfo = (ProviderInfo)ProviderInfo.CREATOR.createFromParcel(localParcel2);
                        return localProviderInfo;
                    }
                    ProviderInfo localProviderInfo = null;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public ResolveInfo resolveIntent(Intent paramIntent, String paramString, int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                        if (paramIntent != null)
                        {
                            localParcel1.writeInt(1);
                            paramIntent.writeToParcel(localParcel1, 0);
                            localParcel1.writeString(paramString);
                            localParcel1.writeInt(paramInt1);
                            localParcel1.writeInt(paramInt2);
                            this.mRemote.transact(27, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            if (localParcel2.readInt() != 0)
                            {
                                localResolveInfo = (ResolveInfo)ResolveInfo.CREATOR.createFromParcel(localParcel2);
                                return localResolveInfo;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    ResolveInfo localResolveInfo = null;
                }
            }

            public ResolveInfo resolveService(Intent paramIntent, String paramString, int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                while (true)
                {
                    try
                    {
                        localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                        if (paramIntent != null)
                        {
                            localParcel1.writeInt(1);
                            paramIntent.writeToParcel(localParcel1, 0);
                            localParcel1.writeString(paramString);
                            localParcel1.writeInt(paramInt1);
                            localParcel1.writeInt(paramInt2);
                            this.mRemote.transact(31, localParcel1, localParcel2, 0);
                            localParcel2.readException();
                            if (localParcel2.readInt() != 0)
                            {
                                localResolveInfo = (ResolveInfo)ResolveInfo.CREATOR.createFromParcel(localParcel2);
                                return localResolveInfo;
                            }
                        }
                        else
                        {
                            localParcel1.writeInt(0);
                            continue;
                        }
                    }
                    finally
                    {
                        localParcel2.recycle();
                        localParcel1.recycle();
                    }
                    ResolveInfo localResolveInfo = null;
                }
            }

            public void revokePermission(String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    this.mRemote.transact(20, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setApplicationEnabledSetting(String paramString, int paramInt1, int paramInt2, int paramInt3)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    localParcel1.writeInt(paramInt3);
                    this.mRemote.transact(55, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setComponentEnabledSetting(ComponentName paramComponentName, int paramInt1, int paramInt2, int paramInt3)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    if (paramComponentName != null)
                    {
                        localParcel1.writeInt(1);
                        paramComponentName.writeToParcel(localParcel1, 0);
                    }
                    while (true)
                    {
                        localParcel1.writeInt(paramInt1);
                        localParcel1.writeInt(paramInt2);
                        localParcel1.writeInt(paramInt3);
                        this.mRemote.transact(53, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                        localParcel1.writeInt(0);
                    }
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public boolean setInstallLocation(int paramInt)
                throws RemoteException
            {
                boolean bool = false;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(76, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    int i = localParcel2.readInt();
                    if (i != 0)
                        bool = true;
                    return bool;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setInstallerPackageName(String paramString1, String paramString2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString1);
                    localParcel1.writeString(paramString2);
                    this.mRemote.transact(43, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setPackageStoppedState(String paramString, boolean paramBoolean, int paramInt)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    localParcel1.writeInt(paramInt);
                    this.mRemote.transact(57, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void setPermissionEnforced(String paramString, boolean paramBoolean)
                throws RemoteException
            {
                int i = 0;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeString(paramString);
                    if (paramBoolean)
                        i = 1;
                    localParcel1.writeInt(i);
                    this.mRemote.transact(87, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void systemReady()
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    this.mRemote.transact(68, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void updateExternalMediaStatus(boolean paramBoolean1, boolean paramBoolean2)
                throws RemoteException
            {
                int i = 1;
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    if (paramBoolean1);
                    for (int j = i; ; j = 0)
                    {
                        localParcel1.writeInt(j);
                        if (!paramBoolean2)
                            break;
                        localParcel1.writeInt(i);
                        this.mRemote.transact(72, localParcel1, localParcel2, 0);
                        localParcel2.readException();
                        return;
                    }
                    i = 0;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void updateUserName(int paramInt, String paramString)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeInt(paramInt);
                    localParcel1.writeString(paramString);
                    this.mRemote.transact(80, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }

            public void verifyPendingInstall(int paramInt1, int paramInt2)
                throws RemoteException
            {
                Parcel localParcel1 = Parcel.obtain();
                Parcel localParcel2 = Parcel.obtain();
                try
                {
                    localParcel1.writeInterfaceToken("android.content.pm.IPackageManager");
                    localParcel1.writeInt(paramInt1);
                    localParcel1.writeInt(paramInt2);
                    this.mRemote.transact(82, localParcel1, localParcel2, 0);
                    localParcel2.readException();
                    return;
                }
                finally
                {
                    localParcel2.recycle();
                    localParcel1.recycle();
                }
            }
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.IPackageManager
 * JD-Core Version:        0.6.2
 */