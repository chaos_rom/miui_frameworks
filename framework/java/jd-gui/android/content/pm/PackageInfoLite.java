package android.content.pm;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class PackageInfoLite
    implements Parcelable
{
    public static final Parcelable.Creator<PackageInfoLite> CREATOR = new Parcelable.Creator()
    {
        public PackageInfoLite createFromParcel(Parcel paramAnonymousParcel)
        {
            return new PackageInfoLite(paramAnonymousParcel, null);
        }

        public PackageInfoLite[] newArray(int paramAnonymousInt)
        {
            return new PackageInfoLite[paramAnonymousInt];
        }
    };
    public int installLocation;
    public String packageName;
    public int recommendedInstallLocation;
    public VerifierInfo[] verifiers;

    public PackageInfoLite()
    {
    }

    private PackageInfoLite(Parcel paramParcel)
    {
        this.packageName = paramParcel.readString();
        this.recommendedInstallLocation = paramParcel.readInt();
        this.installLocation = paramParcel.readInt();
        int i = paramParcel.readInt();
        if (i == 0)
            this.verifiers = new VerifierInfo[0];
        while (true)
        {
            return;
            this.verifiers = new VerifierInfo[i];
            paramParcel.readTypedArray(this.verifiers, VerifierInfo.CREATOR);
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public String toString()
    {
        return "PackageInfoLite{" + Integer.toHexString(System.identityHashCode(this)) + " " + this.packageName + "}";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.packageName);
        paramParcel.writeInt(this.recommendedInstallLocation);
        paramParcel.writeInt(this.installLocation);
        if ((this.verifiers == null) || (this.verifiers.length == 0))
            paramParcel.writeInt(0);
        while (true)
        {
            return;
            paramParcel.writeInt(this.verifiers.length);
            paramParcel.writeTypedArray(this.verifiers, paramInt);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.PackageInfoLite
 * JD-Core Version:        0.6.2
 */