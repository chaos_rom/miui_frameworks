package android.content.pm;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class PackageStats
    implements Parcelable
{
    public static final Parcelable.Creator<PackageStats> CREATOR = new Parcelable.Creator()
    {
        public PackageStats createFromParcel(Parcel paramAnonymousParcel)
        {
            return new PackageStats(paramAnonymousParcel);
        }

        public PackageStats[] newArray(int paramAnonymousInt)
        {
            return new PackageStats[paramAnonymousInt];
        }
    };
    public long cacheSize;
    public long codeSize;
    public long dataSize;
    public long externalCacheSize;
    public long externalCodeSize;
    public long externalDataSize;
    public long externalMediaSize;
    public long externalObbSize;
    public String packageName;

    public PackageStats(PackageStats paramPackageStats)
    {
        this.packageName = paramPackageStats.packageName;
        this.codeSize = paramPackageStats.codeSize;
        this.dataSize = paramPackageStats.dataSize;
        this.cacheSize = paramPackageStats.cacheSize;
        this.externalCodeSize = paramPackageStats.externalCodeSize;
        this.externalDataSize = paramPackageStats.externalDataSize;
        this.externalCacheSize = paramPackageStats.externalCacheSize;
        this.externalMediaSize = paramPackageStats.externalMediaSize;
        this.externalObbSize = paramPackageStats.externalObbSize;
    }

    public PackageStats(Parcel paramParcel)
    {
        this.packageName = paramParcel.readString();
        this.codeSize = paramParcel.readLong();
        this.dataSize = paramParcel.readLong();
        this.cacheSize = paramParcel.readLong();
        this.externalCodeSize = paramParcel.readLong();
        this.externalDataSize = paramParcel.readLong();
        this.externalCacheSize = paramParcel.readLong();
        this.externalMediaSize = paramParcel.readLong();
        this.externalObbSize = paramParcel.readLong();
    }

    public PackageStats(String paramString)
    {
        this.packageName = paramString;
    }

    public int describeContents()
    {
        return 0;
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder("PackageStats{");
        localStringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
        localStringBuilder.append(" packageName=");
        localStringBuilder.append(this.packageName);
        localStringBuilder.append(",codeSize=");
        localStringBuilder.append(this.codeSize);
        localStringBuilder.append(",dataSize=");
        localStringBuilder.append(this.dataSize);
        localStringBuilder.append(",cacheSize=");
        localStringBuilder.append(this.cacheSize);
        localStringBuilder.append(",externalCodeSize=");
        localStringBuilder.append(this.externalCodeSize);
        localStringBuilder.append(",externalDataSize=");
        localStringBuilder.append(this.externalDataSize);
        localStringBuilder.append(",externalCacheSize=");
        localStringBuilder.append(this.externalCacheSize);
        localStringBuilder.append(",externalMediaSize=");
        localStringBuilder.append(this.externalMediaSize);
        localStringBuilder.append(",externalObbSize=");
        localStringBuilder.append(this.externalObbSize);
        return localStringBuilder.toString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.packageName);
        paramParcel.writeLong(this.codeSize);
        paramParcel.writeLong(this.dataSize);
        paramParcel.writeLong(this.cacheSize);
        paramParcel.writeLong(this.externalCodeSize);
        paramParcel.writeLong(this.externalDataSize);
        paramParcel.writeLong(this.externalCacheSize);
        paramParcel.writeLong(this.externalMediaSize);
        paramParcel.writeLong(this.externalObbSize);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.PackageStats
 * JD-Core Version:        0.6.2
 */