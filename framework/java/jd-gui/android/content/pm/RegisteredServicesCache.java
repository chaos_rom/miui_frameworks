package android.content.pm;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.os.Environment;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import com.android.internal.os.AtomicFile;
import com.android.internal.util.FastXmlSerializer;
import com.google.android.collect.Maps;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public abstract class RegisteredServicesCache<V>
{
    private static final String TAG = "PackageManager";
    private final String mAttributesName;
    public final Context mContext;
    private Handler mHandler;
    private final String mInterfaceName;
    private RegisteredServicesCacheListener<V> mListener;
    private final String mMetaDataName;
    private HashMap<V, Integer> mPersistentServices;
    private final AtomicFile mPersistentServicesFile;
    private boolean mPersistentServicesFileDidNotExist;
    private final AtomicReference<BroadcastReceiver> mReceiver;
    private final XmlSerializerAndParser<V> mSerializerAndParser;
    private Map<V, ServiceInfo<V>> mServices;
    private final Object mServicesLock = new Object();

    public RegisteredServicesCache(Context paramContext, String paramString1, String paramString2, String paramString3, XmlSerializerAndParser<V> paramXmlSerializerAndParser)
    {
        this.mContext = paramContext;
        this.mInterfaceName = paramString1;
        this.mMetaDataName = paramString2;
        this.mAttributesName = paramString3;
        this.mSerializerAndParser = paramXmlSerializerAndParser;
        this.mPersistentServicesFile = new AtomicFile(new File(new File(new File(Environment.getDataDirectory(), "system"), "registered_services"), paramString1 + ".xml"));
        generateServicesMap();
        BroadcastReceiver local1 = new BroadcastReceiver()
        {
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {
                RegisteredServicesCache.this.generateServicesMap();
            }
        };
        this.mReceiver = new AtomicReference(local1);
        IntentFilter localIntentFilter1 = new IntentFilter();
        localIntentFilter1.addAction("android.intent.action.PACKAGE_ADDED");
        localIntentFilter1.addAction("android.intent.action.PACKAGE_CHANGED");
        localIntentFilter1.addAction("android.intent.action.PACKAGE_REMOVED");
        localIntentFilter1.addDataScheme("package");
        this.mContext.registerReceiver(local1, localIntentFilter1);
        IntentFilter localIntentFilter2 = new IntentFilter();
        localIntentFilter2.addAction("android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE");
        localIntentFilter2.addAction("android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE");
        this.mContext.registerReceiver(local1, localIntentFilter2);
    }

    private boolean containsType(ArrayList<ServiceInfo<V>> paramArrayList, V paramV)
    {
        int i = 0;
        int j = paramArrayList.size();
        if (i < j)
            if (!((ServiceInfo)paramArrayList.get(i)).type.equals(paramV));
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            i++;
            break;
        }
    }

    private boolean containsTypeAndUid(ArrayList<ServiceInfo<V>> paramArrayList, V paramV, int paramInt)
    {
        int i = 0;
        int j = paramArrayList.size();
        if (i < j)
        {
            ServiceInfo localServiceInfo = (ServiceInfo)paramArrayList.get(i);
            if ((!localServiceInfo.type.equals(paramV)) || (localServiceInfo.uid != paramInt));
        }
        for (boolean bool = true; ; bool = false)
        {
            return bool;
            i++;
            break;
        }
    }

    private boolean inSystemImage(int paramInt)
    {
        boolean bool = false;
        String[] arrayOfString = this.mContext.getPackageManager().getPackagesForUid(paramInt);
        int i = arrayOfString.length;
        int j = 0;
        while (true)
        {
            String str;
            if (j < i)
                str = arrayOfString[j];
            try
            {
                int k = this.mContext.getPackageManager().getPackageInfo(str, 0).applicationInfo.flags;
                if ((k & 0x1) != 0)
                {
                    bool = true;
                    label64: return bool;
                }
            }
            catch (PackageManager.NameNotFoundException localNameNotFoundException)
            {
                break label64;
                j++;
            }
        }
    }

    private void notifyListener(final V paramV, final boolean paramBoolean)
    {
        String str;
        if (Log.isLoggable("PackageManager", 2))
        {
            StringBuilder localStringBuilder = new StringBuilder().append("notifyListener: ").append(paramV).append(" is ");
            if (!paramBoolean)
                break label78;
            str = "removed";
            Log.d("PackageManager", str);
        }
        while (true)
        {
            final RegisteredServicesCacheListener localRegisteredServicesCacheListener;
            Handler localHandler;
            try
            {
                localRegisteredServicesCacheListener = this.mListener;
                localHandler = this.mHandler;
                if (localRegisteredServicesCacheListener == null)
                {
                    return;
                    label78: str = "added";
                    break;
                }
            }
            finally
            {
            }
            localHandler.post(new Runnable()
            {
                public void run()
                {
                    localRegisteredServicesCacheListener.onServiceChanged(paramV, paramBoolean);
                }
            });
        }
    }

    private ServiceInfo<V> parseServiceInfo(ResolveInfo paramResolveInfo)
        throws XmlPullParserException, IOException
    {
        ServiceInfo localServiceInfo = paramResolveInfo.serviceInfo;
        ComponentName localComponentName = new ComponentName(localServiceInfo.packageName, localServiceInfo.name);
        PackageManager localPackageManager = this.mContext.getPackageManager();
        XmlResourceParser localXmlResourceParser = null;
        try
        {
            localXmlResourceParser = localServiceInfo.loadXmlMetaData(localPackageManager, this.mMetaDataName);
            if (localXmlResourceParser == null)
                throw new XmlPullParserException("No " + this.mMetaDataName + " meta-data");
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
            throw new XmlPullParserException("Unable to load resources for pacakge " + localServiceInfo.packageName);
        }
        finally
        {
            if (localXmlResourceParser != null)
                localXmlResourceParser.close();
        }
        AttributeSet localAttributeSet = Xml.asAttributeSet(localXmlResourceParser);
        int i;
        do
            i = localXmlResourceParser.next();
        while ((i != 1) && (i != 2));
        String str = localXmlResourceParser.getName();
        if (!this.mAttributesName.equals(str))
            throw new XmlPullParserException("Meta-data does not start with " + this.mAttributesName + " tag");
        Object localObject2 = parseServiceAttributes(localPackageManager.getResourcesForApplication(localServiceInfo.applicationInfo), localServiceInfo.packageName, localAttributeSet);
        Object localObject3;
        if (localObject2 == null)
        {
            localObject3 = null;
            if (localXmlResourceParser != null)
                localXmlResourceParser.close();
        }
        while (true)
        {
            return localObject3;
            localObject3 = new ServiceInfo(localObject2, localComponentName, paramResolveInfo.serviceInfo.applicationInfo.uid);
            if (localXmlResourceParser != null)
                localXmlResourceParser.close();
        }
    }

    private void readPersistentServicesLocked()
    {
        this.mPersistentServices = Maps.newHashMap();
        if (this.mSerializerAndParser == null);
        FileInputStream localFileInputStream;
        while (true)
        {
            return;
            localFileInputStream = null;
            try
            {
                if (!this.mPersistentServicesFile.getBaseFile().exists());
                for (boolean bool1 = true; ; bool1 = false)
                {
                    this.mPersistentServicesFileDidNotExist = bool1;
                    boolean bool2 = this.mPersistentServicesFileDidNotExist;
                    if (!bool2)
                        break label67;
                    if (0 == 0)
                        break;
                    try
                    {
                        throw null;
                    }
                    catch (IOException localIOException4)
                    {
                    }
                    break;
                }
                label67: localFileInputStream = this.mPersistentServicesFile.openRead();
                XmlPullParser localXmlPullParser = Xml.newPullParser();
                localXmlPullParser.setInput(localFileInputStream, null);
                for (int i = localXmlPullParser.getEventType(); i != 2; i = localXmlPullParser.next());
                int j;
                label141: Object localObject2;
                if ("services".equals(localXmlPullParser.getName()))
                {
                    j = localXmlPullParser.next();
                    if ((j != 2) || (localXmlPullParser.getDepth() != 2) || (!"service".equals(localXmlPullParser.getName())))
                        break label239;
                    localObject2 = this.mSerializerAndParser.createFromXml(localXmlPullParser);
                    if (localObject2 != null)
                        break label208;
                }
                while (localFileInputStream != null)
                {
                    try
                    {
                        localFileInputStream.close();
                    }
                    catch (IOException localIOException3)
                    {
                    }
                    break;
                    label208: int m = Integer.parseInt(localXmlPullParser.getAttributeValue(null, "uid"));
                    this.mPersistentServices.put(localObject2, Integer.valueOf(m));
                    label239: int k = localXmlPullParser.next();
                    j = k;
                    if (j != 1)
                        break label141;
                }
            }
            catch (Exception localException)
            {
                Log.w("PackageManager", "Error reading persistent services, starting from scratch", localException);
                if (localFileInputStream != null)
                    try
                    {
                        localFileInputStream.close();
                    }
                    catch (IOException localIOException2)
                    {
                    }
            }
            finally
            {
                if (localFileInputStream == null);
            }
        }
        try
        {
            localFileInputStream.close();
            label299: throw localObject1;
        }
        catch (IOException localIOException1)
        {
            break label299;
        }
    }

    private void writePersistentServicesLocked()
    {
        if (this.mSerializerAndParser == null);
        while (true)
        {
            return;
            FileOutputStream localFileOutputStream = null;
            FastXmlSerializer localFastXmlSerializer;
            try
            {
                localFileOutputStream = this.mPersistentServicesFile.startWrite();
                localFastXmlSerializer = new FastXmlSerializer();
                localFastXmlSerializer.setOutput(localFileOutputStream, "utf-8");
                localFastXmlSerializer.startDocument(null, Boolean.valueOf(true));
                localFastXmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
                localFastXmlSerializer.startTag(null, "services");
                Iterator localIterator = this.mPersistentServices.entrySet().iterator();
                while (localIterator.hasNext())
                {
                    Map.Entry localEntry = (Map.Entry)localIterator.next();
                    localFastXmlSerializer.startTag(null, "service");
                    localFastXmlSerializer.attribute(null, "uid", Integer.toString(((Integer)localEntry.getValue()).intValue()));
                    this.mSerializerAndParser.writeAsXml(localEntry.getKey(), localFastXmlSerializer);
                    localFastXmlSerializer.endTag(null, "service");
                }
            }
            catch (IOException localIOException)
            {
                Log.w("PackageManager", "Error writing accounts", localIOException);
            }
            if (localFileOutputStream != null)
            {
                this.mPersistentServicesFile.failWrite(localFileOutputStream);
                continue;
                localFastXmlSerializer.endTag(null, "services");
                localFastXmlSerializer.endDocument();
                this.mPersistentServicesFile.finishWrite(localFileOutputStream);
            }
        }
    }

    public void close()
    {
        BroadcastReceiver localBroadcastReceiver = (BroadcastReceiver)this.mReceiver.getAndSet(null);
        if (localBroadcastReceiver != null)
            this.mContext.unregisterReceiver(localBroadcastReceiver);
    }

    public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
        synchronized (this.mServicesLock)
        {
            Map localMap = this.mServices;
            paramPrintWriter.println("RegisteredServicesCache: " + localMap.size() + " services");
            Iterator localIterator = localMap.values().iterator();
            if (localIterator.hasNext())
            {
                ServiceInfo localServiceInfo = (ServiceInfo)localIterator.next();
                paramPrintWriter.println("    " + localServiceInfo);
            }
        }
    }

    protected void finalize()
        throws Throwable
    {
        if (this.mReceiver.get() != null)
            Log.e("PackageManager", "RegisteredServicesCache finalized without being closed");
        close();
        super.finalize();
    }

    // ERROR //
    public void generateServicesMap()
    {
        // Byte code:
        //     0: aload_0
        //     1: getfield 53	android/content/pm/RegisteredServicesCache:mContext	Landroid/content/Context;
        //     4: invokevirtual 170	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
        //     7: astore_1
        //     8: new 141	java/util/ArrayList
        //     11: dup
        //     12: invokespecial 529	java/util/ArrayList:<init>	()V
        //     15: astore_2
        //     16: aload_1
        //     17: new 531	android/content/Intent
        //     20: dup
        //     21: aload_0
        //     22: getfield 55	android/content/pm/RegisteredServicesCache:mInterfaceName	Ljava/lang/String;
        //     25: invokespecial 532	android/content/Intent:<init>	(Ljava/lang/String;)V
        //     28: sipush 128
        //     31: invokevirtual 536	android/content/pm/PackageManager:queryIntentServices	(Landroid/content/Intent;I)Ljava/util/List;
        //     34: invokeinterface 539 1 0
        //     39: astore_3
        //     40: aload_3
        //     41: invokeinterface 437 1 0
        //     46: ifeq +144 -> 190
        //     49: aload_3
        //     50: invokeinterface 440 1 0
        //     55: checkcast 235	android/content/pm/ResolveInfo
        //     58: astore 30
        //     60: aload_0
        //     61: aload 30
        //     63: invokespecial 541	android/content/pm/RegisteredServicesCache:parseServiceInfo	(Landroid/content/pm/ResolveInfo;)Landroid/content/pm/RegisteredServicesCache$ServiceInfo;
        //     66: astore 35
        //     68: aload 35
        //     70: ifnonnull +73 -> 143
        //     73: ldc 16
        //     75: new 80	java/lang/StringBuilder
        //     78: dup
        //     79: invokespecial 81	java/lang/StringBuilder:<init>	()V
        //     82: ldc_w 543
        //     85: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     88: aload 30
        //     90: invokevirtual 544	android/content/pm/ResolveInfo:toString	()Ljava/lang/String;
        //     93: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     96: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     99: invokestatic 546	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
        //     102: pop
        //     103: goto -63 -> 40
        //     106: astore 33
        //     108: ldc 16
        //     110: new 80	java/lang/StringBuilder
        //     113: dup
        //     114: invokespecial 81	java/lang/StringBuilder:<init>	()V
        //     117: ldc_w 543
        //     120: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     123: aload 30
        //     125: invokevirtual 544	android/content/pm/ResolveInfo:toString	()Ljava/lang/String;
        //     128: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     131: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     134: aload 33
        //     136: invokestatic 387	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     139: pop
        //     140: goto -100 -> 40
        //     143: aload_2
        //     144: aload 35
        //     146: invokevirtual 549	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     149: pop
        //     150: goto -110 -> 40
        //     153: astore 31
        //     155: ldc 16
        //     157: new 80	java/lang/StringBuilder
        //     160: dup
        //     161: invokespecial 81	java/lang/StringBuilder:<init>	()V
        //     164: ldc_w 543
        //     167: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     170: aload 30
        //     172: invokevirtual 544	android/content/pm/ResolveInfo:toString	()Ljava/lang/String;
        //     175: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     178: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     181: aload 31
        //     183: invokestatic 387	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //     186: pop
        //     187: goto -147 -> 40
        //     190: aload_0
        //     191: getfield 51	android/content/pm/RegisteredServicesCache:mServicesLock	Ljava/lang/Object;
        //     194: astore 4
        //     196: aload 4
        //     198: monitorenter
        //     199: aload_0
        //     200: getfield 316	android/content/pm/RegisteredServicesCache:mPersistentServices	Ljava/util/HashMap;
        //     203: ifnonnull +7 -> 210
        //     206: aload_0
        //     207: invokespecial 551	android/content/pm/RegisteredServicesCache:readPersistentServicesLocked	()V
        //     210: aload_0
        //     211: invokestatic 314	com/google/android/collect/Maps:newHashMap	()Ljava/util/HashMap;
        //     214: putfield 491	android/content/pm/RegisteredServicesCache:mServices	Ljava/util/Map;
        //     217: new 80	java/lang/StringBuilder
        //     220: dup
        //     221: invokespecial 81	java/lang/StringBuilder:<init>	()V
        //     224: astore 6
        //     226: aload_2
        //     227: invokevirtual 552	java/util/ArrayList:iterator	()Ljava/util/Iterator;
        //     230: astore 7
        //     232: aload 7
        //     234: invokeinterface 437 1 0
        //     239: ifeq +344 -> 583
        //     242: aload 7
        //     244: invokeinterface 440 1 0
        //     249: checkcast 11	android/content/pm/RegisteredServicesCache$ServiceInfo
        //     252: astore 18
        //     254: aload_0
        //     255: getfield 316	android/content/pm/RegisteredServicesCache:mPersistentServices	Ljava/util/HashMap;
        //     258: aload 18
        //     260: getfield 152	android/content/pm/RegisteredServicesCache$ServiceInfo:type	Ljava/lang/Object;
        //     263: invokevirtual 554	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
        //     266: checkcast 367	java/lang/Integer
        //     269: astore 19
        //     271: aload 19
        //     273: ifnonnull +89 -> 362
        //     276: aload 6
        //     278: ldc_w 556
        //     281: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     284: aload 18
        //     286: invokevirtual 204	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     289: ldc_w 558
        //     292: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     295: pop
        //     296: aload_0
        //     297: getfield 491	android/content/pm/RegisteredServicesCache:mServices	Ljava/util/Map;
        //     300: aload 18
        //     302: getfield 152	android/content/pm/RegisteredServicesCache$ServiceInfo:type	Ljava/lang/Object;
        //     305: aload 18
        //     307: invokeinterface 559 3 0
        //     312: pop
        //     313: aload_0
        //     314: getfield 316	android/content/pm/RegisteredServicesCache:mPersistentServices	Ljava/util/HashMap;
        //     317: aload 18
        //     319: getfield 152	android/content/pm/RegisteredServicesCache$ServiceInfo:type	Ljava/lang/Object;
        //     322: aload 18
        //     324: getfield 162	android/content/pm/RegisteredServicesCache$ServiceInfo:uid	I
        //     327: invokestatic 375	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     330: invokevirtual 381	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     333: pop
        //     334: aload_0
        //     335: getfield 325	android/content/pm/RegisteredServicesCache:mPersistentServicesFileDidNotExist	Z
        //     338: ifne -106 -> 232
        //     341: aload_0
        //     342: aload 18
        //     344: getfield 152	android/content/pm/RegisteredServicesCache$ServiceInfo:type	Ljava/lang/Object;
        //     347: iconst_0
        //     348: invokespecial 561	android/content/pm/RegisteredServicesCache:notifyListener	(Ljava/lang/Object;Z)V
        //     351: goto -119 -> 232
        //     354: astore 5
        //     356: aload 4
        //     358: monitorexit
        //     359: aload 5
        //     361: athrow
        //     362: aload 19
        //     364: invokevirtual 448	java/lang/Integer:intValue	()I
        //     367: aload 18
        //     369: getfield 162	android/content/pm/RegisteredServicesCache$ServiceInfo:uid	I
        //     372: if_icmpne +52 -> 424
        //     375: ldc 16
        //     377: iconst_2
        //     378: invokestatic 199	android/util/Log:isLoggable	(Ljava/lang/String;I)Z
        //     381: ifeq +23 -> 404
        //     384: aload 6
        //     386: ldc_w 563
        //     389: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     392: aload 18
        //     394: invokevirtual 204	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     397: ldc_w 558
        //     400: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     403: pop
        //     404: aload_0
        //     405: getfield 491	android/content/pm/RegisteredServicesCache:mServices	Ljava/util/Map;
        //     408: aload 18
        //     410: getfield 152	android/content/pm/RegisteredServicesCache$ServiceInfo:type	Ljava/lang/Object;
        //     413: aload 18
        //     415: invokeinterface 559 3 0
        //     420: pop
        //     421: goto -189 -> 232
        //     424: aload_0
        //     425: aload 18
        //     427: getfield 162	android/content/pm/RegisteredServicesCache$ServiceInfo:uid	I
        //     430: invokespecial 565	android/content/pm/RegisteredServicesCache:inSystemImage	(I)Z
        //     433: ifne +21 -> 454
        //     436: aload_0
        //     437: aload_2
        //     438: aload 18
        //     440: getfield 152	android/content/pm/RegisteredServicesCache$ServiceInfo:type	Ljava/lang/Object;
        //     443: aload 19
        //     445: invokevirtual 448	java/lang/Integer:intValue	()I
        //     448: invokespecial 567	android/content/pm/RegisteredServicesCache:containsTypeAndUid	(Ljava/util/ArrayList;Ljava/lang/Object;I)Z
        //     451: ifne +109 -> 560
        //     454: aload_0
        //     455: aload 18
        //     457: getfield 162	android/content/pm/RegisteredServicesCache$ServiceInfo:uid	I
        //     460: invokespecial 565	android/content/pm/RegisteredServicesCache:inSystemImage	(I)Z
        //     463: ifeq +74 -> 537
        //     466: aload 6
        //     468: ldc_w 569
        //     471: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     474: aload 18
        //     476: invokevirtual 204	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     479: ldc_w 558
        //     482: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     485: pop
        //     486: aload_0
        //     487: getfield 491	android/content/pm/RegisteredServicesCache:mServices	Ljava/util/Map;
        //     490: aload 18
        //     492: getfield 152	android/content/pm/RegisteredServicesCache$ServiceInfo:type	Ljava/lang/Object;
        //     495: aload 18
        //     497: invokeinterface 559 3 0
        //     502: pop
        //     503: aload_0
        //     504: getfield 316	android/content/pm/RegisteredServicesCache:mPersistentServices	Ljava/util/HashMap;
        //     507: aload 18
        //     509: getfield 152	android/content/pm/RegisteredServicesCache$ServiceInfo:type	Ljava/lang/Object;
        //     512: aload 18
        //     514: getfield 162	android/content/pm/RegisteredServicesCache$ServiceInfo:uid	I
        //     517: invokestatic 375	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
        //     520: invokevirtual 381	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //     523: pop
        //     524: aload_0
        //     525: aload 18
        //     527: getfield 152	android/content/pm/RegisteredServicesCache$ServiceInfo:type	Ljava/lang/Object;
        //     530: iconst_0
        //     531: invokespecial 561	android/content/pm/RegisteredServicesCache:notifyListener	(Ljava/lang/Object;Z)V
        //     534: goto -302 -> 232
        //     537: aload 6
        //     539: ldc_w 571
        //     542: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     545: aload 18
        //     547: invokevirtual 204	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     550: ldc_w 558
        //     553: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     556: pop
        //     557: goto -71 -> 486
        //     560: aload 6
        //     562: ldc_w 573
        //     565: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     568: aload 18
        //     570: invokevirtual 204	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     573: ldc_w 558
        //     576: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     579: pop
        //     580: goto -348 -> 232
        //     583: invokestatic 579	com/google/android/collect/Lists:newArrayList	()Ljava/util/ArrayList;
        //     586: astore 8
        //     588: aload_0
        //     589: getfield 316	android/content/pm/RegisteredServicesCache:mPersistentServices	Ljava/util/HashMap;
        //     592: invokevirtual 582	java/util/HashMap:keySet	()Ljava/util/Set;
        //     595: invokeinterface 432 1 0
        //     600: astore 9
        //     602: aload 9
        //     604: invokeinterface 437 1 0
        //     609: ifeq +33 -> 642
        //     612: aload 9
        //     614: invokeinterface 440 1 0
        //     619: astore 16
        //     621: aload_0
        //     622: aload_2
        //     623: aload 16
        //     625: invokespecial 584	android/content/pm/RegisteredServicesCache:containsType	(Ljava/util/ArrayList;Ljava/lang/Object;)Z
        //     628: ifne -26 -> 602
        //     631: aload 8
        //     633: aload 16
        //     635: invokevirtual 549	java/util/ArrayList:add	(Ljava/lang/Object;)Z
        //     638: pop
        //     639: goto -37 -> 602
        //     642: aload 8
        //     644: invokevirtual 552	java/util/ArrayList:iterator	()Ljava/util/Iterator;
        //     647: astore 10
        //     649: aload 10
        //     651: invokeinterface 437 1 0
        //     656: ifeq +52 -> 708
        //     659: aload 10
        //     661: invokeinterface 440 1 0
        //     666: astore 13
        //     668: aload_0
        //     669: getfield 316	android/content/pm/RegisteredServicesCache:mPersistentServices	Ljava/util/HashMap;
        //     672: aload 13
        //     674: invokevirtual 587	java/util/HashMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
        //     677: pop
        //     678: aload 6
        //     680: ldc_w 589
        //     683: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     686: aload 13
        //     688: invokevirtual 204	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     691: ldc_w 558
        //     694: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     697: pop
        //     698: aload_0
        //     699: aload 13
        //     701: iconst_1
        //     702: invokespecial 561	android/content/pm/RegisteredServicesCache:notifyListener	(Ljava/lang/Object;Z)V
        //     705: goto -56 -> 649
        //     708: aload 6
        //     710: invokevirtual 592	java/lang/StringBuilder:length	()I
        //     713: ifle +69 -> 782
        //     716: ldc 16
        //     718: new 80	java/lang/StringBuilder
        //     721: dup
        //     722: invokespecial 81	java/lang/StringBuilder:<init>	()V
        //     725: ldc_w 594
        //     728: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     731: aload_0
        //     732: getfield 55	android/content/pm/RegisteredServicesCache:mInterfaceName	Ljava/lang/String;
        //     735: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     738: ldc_w 596
        //     741: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     744: aload_2
        //     745: invokevirtual 145	java/util/ArrayList:size	()I
        //     748: invokevirtual 499	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     751: ldc_w 598
        //     754: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     757: aload 6
        //     759: invokevirtual 204	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //     762: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     765: invokestatic 212	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     768: pop
        //     769: aload_0
        //     770: invokespecial 600	android/content/pm/RegisteredServicesCache:writePersistentServicesLocked	()V
        //     773: aload_0
        //     774: iconst_0
        //     775: putfield 325	android/content/pm/RegisteredServicesCache:mPersistentServicesFileDidNotExist	Z
        //     778: aload 4
        //     780: monitorexit
        //     781: return
        //     782: ldc 16
        //     784: new 80	java/lang/StringBuilder
        //     787: dup
        //     788: invokespecial 81	java/lang/StringBuilder:<init>	()V
        //     791: ldc_w 594
        //     794: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     797: aload_0
        //     798: getfield 55	android/content/pm/RegisteredServicesCache:mInterfaceName	Ljava/lang/String;
        //     801: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     804: ldc_w 596
        //     807: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     810: aload_2
        //     811: invokevirtual 145	java/util/ArrayList:size	()I
        //     814: invokevirtual 499	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
        //     817: ldc_w 602
        //     820: invokevirtual 85	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //     823: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //     826: invokestatic 212	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
        //     829: pop
        //     830: goto -57 -> 773
        //
        // Exception table:
        //     from	to	target	type
        //     60	103	106	org/xmlpull/v1/XmlPullParserException
        //     143	150	106	org/xmlpull/v1/XmlPullParserException
        //     60	103	153	java/io/IOException
        //     143	150	153	java/io/IOException
        //     199	359	354	finally
        //     362	830	354	finally
    }

    public Collection<ServiceInfo<V>> getAllServices()
    {
        synchronized (this.mServicesLock)
        {
            Collection localCollection = Collections.unmodifiableCollection(this.mServices.values());
            return localCollection;
        }
    }

    public RegisteredServicesCacheListener<V> getListener()
    {
        try
        {
            RegisteredServicesCacheListener localRegisteredServicesCacheListener = this.mListener;
            return localRegisteredServicesCacheListener;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public ServiceInfo<V> getServiceInfo(V paramV)
    {
        synchronized (this.mServicesLock)
        {
            ServiceInfo localServiceInfo = (ServiceInfo)this.mServices.get(paramV);
            return localServiceInfo;
        }
    }

    public abstract V parseServiceAttributes(Resources paramResources, String paramString, AttributeSet paramAttributeSet);

    public void setListener(RegisteredServicesCacheListener<V> paramRegisteredServicesCacheListener, Handler paramHandler)
    {
        if (paramHandler == null)
            paramHandler = new Handler(this.mContext.getMainLooper());
        try
        {
            this.mHandler = paramHandler;
            this.mListener = paramRegisteredServicesCacheListener;
            return;
        }
        finally
        {
            localObject = finally;
            throw localObject;
        }
    }

    public static class ServiceInfo<V>
    {
        public final ComponentName componentName;
        public final V type;
        public final int uid;

        public ServiceInfo(V paramV, ComponentName paramComponentName, int paramInt)
        {
            this.type = paramV;
            this.componentName = paramComponentName;
            this.uid = paramInt;
        }

        public String toString()
        {
            return "ServiceInfo: " + this.type + ", " + this.componentName + ", uid " + this.uid;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.RegisteredServicesCache
 * JD-Core Version:        0.6.2
 */