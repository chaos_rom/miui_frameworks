package android.content.pm;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class PackageInfo
    implements Parcelable
{
    public static final Parcelable.Creator<PackageInfo> CREATOR = new Parcelable.Creator()
    {
        public PackageInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            return new PackageInfo(paramAnonymousParcel, null);
        }

        public PackageInfo[] newArray(int paramAnonymousInt)
        {
            return new PackageInfo[paramAnonymousInt];
        }
    };
    public static final int INSTALL_LOCATION_AUTO = 0;
    public static final int INSTALL_LOCATION_INTERNAL_ONLY = 1;
    public static final int INSTALL_LOCATION_PREFER_EXTERNAL = 2;
    public static final int INSTALL_LOCATION_UNSPECIFIED = -1;
    public static final int REQUESTED_PERMISSION_GRANTED = 2;
    public static final int REQUESTED_PERMISSION_REQUIRED = 1;
    public ActivityInfo[] activities;
    public ApplicationInfo applicationInfo;
    public ConfigurationInfo[] configPreferences;
    public long firstInstallTime;
    public int[] gids;
    public int installLocation = 1;
    public InstrumentationInfo[] instrumentation;
    public long lastUpdateTime;
    public String packageName;
    public PermissionInfo[] permissions;
    public ProviderInfo[] providers;
    public ActivityInfo[] receivers;
    public FeatureInfo[] reqFeatures;
    public String[] requestedPermissions;
    public int[] requestedPermissionsFlags;
    public ServiceInfo[] services;
    public String sharedUserId;
    public int sharedUserLabel;
    public Signature[] signatures;
    public int versionCode;
    public String versionName;

    public PackageInfo()
    {
    }

    private PackageInfo(Parcel paramParcel)
    {
        this.packageName = paramParcel.readString();
        this.versionCode = paramParcel.readInt();
        this.versionName = paramParcel.readString();
        this.sharedUserId = paramParcel.readString();
        this.sharedUserLabel = paramParcel.readInt();
        if (paramParcel.readInt() != 0)
            this.applicationInfo = ((ApplicationInfo)ApplicationInfo.CREATOR.createFromParcel(paramParcel));
        this.firstInstallTime = paramParcel.readLong();
        this.lastUpdateTime = paramParcel.readLong();
        this.gids = paramParcel.createIntArray();
        this.activities = ((ActivityInfo[])paramParcel.createTypedArray(ActivityInfo.CREATOR));
        this.receivers = ((ActivityInfo[])paramParcel.createTypedArray(ActivityInfo.CREATOR));
        this.services = ((ServiceInfo[])paramParcel.createTypedArray(ServiceInfo.CREATOR));
        this.providers = ((ProviderInfo[])paramParcel.createTypedArray(ProviderInfo.CREATOR));
        this.instrumentation = ((InstrumentationInfo[])paramParcel.createTypedArray(InstrumentationInfo.CREATOR));
        this.permissions = ((PermissionInfo[])paramParcel.createTypedArray(PermissionInfo.CREATOR));
        this.requestedPermissions = paramParcel.createStringArray();
        this.requestedPermissionsFlags = paramParcel.createIntArray();
        this.signatures = ((Signature[])paramParcel.createTypedArray(Signature.CREATOR));
        this.configPreferences = ((ConfigurationInfo[])paramParcel.createTypedArray(ConfigurationInfo.CREATOR));
        this.reqFeatures = ((FeatureInfo[])paramParcel.createTypedArray(FeatureInfo.CREATOR));
        this.installLocation = paramParcel.readInt();
    }

    public int describeContents()
    {
        return 0;
    }

    public String toString()
    {
        return "PackageInfo{" + Integer.toHexString(System.identityHashCode(this)) + " " + this.packageName + "}";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.packageName);
        paramParcel.writeInt(this.versionCode);
        paramParcel.writeString(this.versionName);
        paramParcel.writeString(this.sharedUserId);
        paramParcel.writeInt(this.sharedUserLabel);
        if (this.applicationInfo != null)
        {
            paramParcel.writeInt(1);
            this.applicationInfo.writeToParcel(paramParcel, paramInt);
        }
        while (true)
        {
            paramParcel.writeLong(this.firstInstallTime);
            paramParcel.writeLong(this.lastUpdateTime);
            paramParcel.writeIntArray(this.gids);
            paramParcel.writeTypedArray(this.activities, paramInt);
            paramParcel.writeTypedArray(this.receivers, paramInt);
            paramParcel.writeTypedArray(this.services, paramInt);
            paramParcel.writeTypedArray(this.providers, paramInt);
            paramParcel.writeTypedArray(this.instrumentation, paramInt);
            paramParcel.writeTypedArray(this.permissions, paramInt);
            paramParcel.writeStringArray(this.requestedPermissions);
            paramParcel.writeIntArray(this.requestedPermissionsFlags);
            paramParcel.writeTypedArray(this.signatures, paramInt);
            paramParcel.writeTypedArray(this.configPreferences, paramInt);
            paramParcel.writeTypedArray(this.reqFeatures, paramInt);
            paramParcel.writeInt(this.installLocation);
            return;
            paramParcel.writeInt(0);
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.PackageInfo
 * JD-Core Version:        0.6.2
 */