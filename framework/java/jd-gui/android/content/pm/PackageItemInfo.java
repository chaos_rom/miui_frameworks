package android.content.pm;

import android.annotation.MiuiHook;
import android.annotation.MiuiHook.MiuiHookType;
import android.app.MiuiThemeHelper;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.Printer;
import java.text.Collator;
import java.util.Comparator;

public class PackageItemInfo
{
    public int icon;
    public int labelRes;
    public int logo;
    public Bundle metaData;
    public String name;
    public CharSequence nonLocalizedLabel;
    public String packageName;

    public PackageItemInfo()
    {
    }

    public PackageItemInfo(PackageItemInfo paramPackageItemInfo)
    {
        this.name = paramPackageItemInfo.name;
        if (this.name != null)
            this.name = this.name.trim();
        this.packageName = paramPackageItemInfo.packageName;
        this.labelRes = paramPackageItemInfo.labelRes;
        this.nonLocalizedLabel = paramPackageItemInfo.nonLocalizedLabel;
        if (this.nonLocalizedLabel != null)
            this.nonLocalizedLabel = this.nonLocalizedLabel.toString().trim();
        this.icon = paramPackageItemInfo.icon;
        this.logo = paramPackageItemInfo.logo;
        this.metaData = paramPackageItemInfo.metaData;
    }

    protected PackageItemInfo(Parcel paramParcel)
    {
        this.name = paramParcel.readString();
        this.packageName = paramParcel.readString();
        this.labelRes = paramParcel.readInt();
        this.nonLocalizedLabel = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
        this.icon = paramParcel.readInt();
        this.logo = paramParcel.readInt();
        this.metaData = paramParcel.readBundle();
    }

    protected void dumpBack(Printer paramPrinter, String paramString)
    {
    }

    protected void dumpFront(Printer paramPrinter, String paramString)
    {
        if (this.name != null)
            paramPrinter.println(paramString + "name=" + this.name);
        paramPrinter.println(paramString + "packageName=" + this.packageName);
        if ((this.labelRes != 0) || (this.nonLocalizedLabel != null) || (this.icon != 0))
            paramPrinter.println(paramString + "labelRes=0x" + Integer.toHexString(this.labelRes) + " nonLocalizedLabel=" + this.nonLocalizedLabel + " icon=0x" + Integer.toHexString(this.icon));
    }

    protected ApplicationInfo getApplicationInfo()
    {
        return null;
    }

    protected Drawable loadDefaultIcon(PackageManager paramPackageManager)
    {
        return paramPackageManager.getDefaultActivityIcon();
    }

    protected Drawable loadDefaultLogo(PackageManager paramPackageManager)
    {
        return null;
    }

    @MiuiHook(MiuiHook.MiuiHookType.CHANGE_CODE)
    public Drawable loadIcon(PackageManager paramPackageManager)
    {
        Drawable localDrawable;
        if (this.icon != 0)
        {
            localDrawable = MiuiThemeHelper.getDrawable(paramPackageManager, this.packageName, this.icon, getApplicationInfo(), this.name);
            if (localDrawable == null);
        }
        while (true)
        {
            return localDrawable;
            localDrawable = loadDefaultIcon(paramPackageManager);
        }
    }

    public CharSequence loadLabel(PackageManager paramPackageManager)
    {
        Object localObject;
        if (this.nonLocalizedLabel != null)
            localObject = this.nonLocalizedLabel;
        while (true)
        {
            return localObject;
            if (this.labelRes != 0)
            {
                CharSequence localCharSequence = paramPackageManager.getText(this.packageName, this.labelRes, getApplicationInfo());
                if (localCharSequence != null)
                    localObject = localCharSequence.toString().trim();
            }
            else if (this.name != null)
            {
                localObject = this.name;
            }
            else
            {
                localObject = this.packageName;
            }
        }
    }

    public Drawable loadLogo(PackageManager paramPackageManager)
    {
        Drawable localDrawable;
        if (this.logo != 0)
        {
            localDrawable = paramPackageManager.getDrawable(this.packageName, this.logo, getApplicationInfo());
            if (localDrawable == null);
        }
        while (true)
        {
            return localDrawable;
            localDrawable = loadDefaultLogo(paramPackageManager);
        }
    }

    public XmlResourceParser loadXmlMetaData(PackageManager paramPackageManager, String paramString)
    {
        int i;
        if (this.metaData != null)
        {
            i = this.metaData.getInt(paramString);
            if (i == 0);
        }
        for (XmlResourceParser localXmlResourceParser = paramPackageManager.getXml(this.packageName, i, getApplicationInfo()); ; localXmlResourceParser = null)
            return localXmlResourceParser;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeString(this.name);
        paramParcel.writeString(this.packageName);
        paramParcel.writeInt(this.labelRes);
        TextUtils.writeToParcel(this.nonLocalizedLabel, paramParcel, paramInt);
        paramParcel.writeInt(this.icon);
        paramParcel.writeInt(this.logo);
        paramParcel.writeBundle(this.metaData);
    }

    public static class DisplayNameComparator
        implements Comparator<PackageItemInfo>
    {
        private PackageManager mPM;
        private final Collator sCollator = Collator.getInstance();

        public DisplayNameComparator(PackageManager paramPackageManager)
        {
            this.mPM = paramPackageManager;
        }

        public final int compare(PackageItemInfo paramPackageItemInfo1, PackageItemInfo paramPackageItemInfo2)
        {
            Object localObject1 = paramPackageItemInfo1.loadLabel(this.mPM);
            if (localObject1 == null)
                localObject1 = paramPackageItemInfo1.name;
            Object localObject2 = paramPackageItemInfo2.loadLabel(this.mPM);
            if (localObject2 == null)
                localObject2 = paramPackageItemInfo2.name;
            return this.sCollator.compare(localObject1.toString(), localObject2.toString());
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.PackageItemInfo
 * JD-Core Version:        0.6.2
 */