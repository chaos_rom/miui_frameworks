package android.content.pm;

public abstract interface RegisteredServicesCacheListener<V>
{
    public abstract void onServiceChanged(V paramV, boolean paramBoolean);
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.RegisteredServicesCacheListener
 * JD-Core Version:        0.6.2
 */