package android.content.pm;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class UserInfo
    implements Parcelable
{
    public static final Parcelable.Creator<UserInfo> CREATOR = new Parcelable.Creator()
    {
        public UserInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            return new UserInfo(paramAnonymousParcel, null);
        }

        public UserInfo[] newArray(int paramAnonymousInt)
        {
            return new UserInfo[paramAnonymousInt];
        }
    };
    public static final int FLAG_ADMIN = 2;
    public static final int FLAG_GUEST = 4;
    public static final int FLAG_PRIMARY = 1;
    public int flags;
    public int id;
    public String name;

    public UserInfo()
    {
    }

    public UserInfo(int paramInt1, String paramString, int paramInt2)
    {
        this.id = paramInt1;
        this.name = paramString;
        this.flags = paramInt2;
    }

    public UserInfo(UserInfo paramUserInfo)
    {
        this.name = paramUserInfo.name;
        this.id = paramUserInfo.id;
        this.flags = paramUserInfo.flags;
    }

    private UserInfo(Parcel paramParcel)
    {
        this.id = paramParcel.readInt();
        this.name = paramParcel.readString();
        this.flags = paramParcel.readInt();
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean isAdmin()
    {
        if ((0x2 & this.flags) == 2);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isGuest()
    {
        if ((0x4 & this.flags) == 4);
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    public boolean isPrimary()
    {
        int i = 1;
        if ((0x1 & this.flags) == i);
        while (true)
        {
            return i;
            int j = 0;
        }
    }

    public String toString()
    {
        return "UserInfo{" + this.id + ":" + this.name + ":" + Integer.toHexString(this.flags) + "}";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeInt(this.id);
        paramParcel.writeString(this.name);
        paramParcel.writeInt(this.flags);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.UserInfo
 * JD-Core Version:        0.6.2
 */