package android.content.pm;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.PatternMatcher;

public final class ProviderInfo extends ComponentInfo
    implements Parcelable
{
    public static final Parcelable.Creator<ProviderInfo> CREATOR = new Parcelable.Creator()
    {
        public ProviderInfo createFromParcel(Parcel paramAnonymousParcel)
        {
            return new ProviderInfo(paramAnonymousParcel, null);
        }

        public ProviderInfo[] newArray(int paramAnonymousInt)
        {
            return new ProviderInfo[paramAnonymousInt];
        }
    };
    public String authority = null;
    public boolean grantUriPermissions = false;
    public int initOrder = 0;

    @Deprecated
    public boolean isSyncable = false;
    public boolean multiprocess = false;
    public PathPermission[] pathPermissions = null;
    public String readPermission = null;
    public PatternMatcher[] uriPermissionPatterns = null;
    public String writePermission = null;

    public ProviderInfo()
    {
    }

    public ProviderInfo(ProviderInfo paramProviderInfo)
    {
        super(paramProviderInfo);
        this.authority = paramProviderInfo.authority;
        this.readPermission = paramProviderInfo.readPermission;
        this.writePermission = paramProviderInfo.writePermission;
        this.grantUriPermissions = paramProviderInfo.grantUriPermissions;
        this.uriPermissionPatterns = paramProviderInfo.uriPermissionPatterns;
        this.pathPermissions = paramProviderInfo.pathPermissions;
        this.multiprocess = paramProviderInfo.multiprocess;
        this.initOrder = paramProviderInfo.initOrder;
        this.isSyncable = paramProviderInfo.isSyncable;
    }

    private ProviderInfo(Parcel paramParcel)
    {
        super(paramParcel);
        this.authority = paramParcel.readString();
        this.readPermission = paramParcel.readString();
        this.writePermission = paramParcel.readString();
        boolean bool2;
        boolean bool3;
        if (paramParcel.readInt() != 0)
        {
            bool2 = bool1;
            this.grantUriPermissions = bool2;
            this.uriPermissionPatterns = ((PatternMatcher[])paramParcel.createTypedArray(PatternMatcher.CREATOR));
            this.pathPermissions = ((PathPermission[])paramParcel.createTypedArray(PathPermission.CREATOR));
            if (paramParcel.readInt() == 0)
                break label160;
            bool3 = bool1;
            label128: this.multiprocess = bool3;
            this.initOrder = paramParcel.readInt();
            if (paramParcel.readInt() == 0)
                break label166;
        }
        while (true)
        {
            this.isSyncable = bool1;
            return;
            bool2 = false;
            break;
            label160: bool3 = false;
            break label128;
            label166: bool1 = false;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder().append("ContentProviderInfo{name=").append(this.authority).append(" className=").append(this.name).append(" isSyncable=");
        if (this.isSyncable);
        for (String str = "true"; ; str = "false")
            return str + "}";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        int i = 1;
        super.writeToParcel(paramParcel, paramInt);
        paramParcel.writeString(this.authority);
        paramParcel.writeString(this.readPermission);
        paramParcel.writeString(this.writePermission);
        int j;
        int k;
        if (this.grantUriPermissions)
        {
            j = i;
            paramParcel.writeInt(j);
            paramParcel.writeTypedArray(this.uriPermissionPatterns, paramInt);
            paramParcel.writeTypedArray(this.pathPermissions, paramInt);
            if (!this.multiprocess)
                break label109;
            k = i;
            label76: paramParcel.writeInt(k);
            paramParcel.writeInt(this.initOrder);
            if (!this.isSyncable)
                break label115;
        }
        while (true)
        {
            paramParcel.writeInt(i);
            return;
            j = 0;
            break;
            label109: k = 0;
            break label76;
            label115: i = 0;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.ProviderInfo
 * JD-Core Version:        0.6.2
 */