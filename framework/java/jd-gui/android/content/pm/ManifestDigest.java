package android.content.pm;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Base64;
import java.util.Arrays;
import java.util.jar.Attributes;

public class ManifestDigest
    implements Parcelable
{
    public static final Parcelable.Creator<ManifestDigest> CREATOR = new Parcelable.Creator()
    {
        public ManifestDigest createFromParcel(Parcel paramAnonymousParcel)
        {
            return new ManifestDigest(paramAnonymousParcel, null);
        }

        public ManifestDigest[] newArray(int paramAnonymousInt)
        {
            return new ManifestDigest[paramAnonymousInt];
        }
    };
    private static final String[] DIGEST_TYPES;
    private static final String TO_STRING_PREFIX = "ManifestDigest {mDigest=";
    private final byte[] mDigest;

    static
    {
        String[] arrayOfString = new String[3];
        arrayOfString[0] = "SHA1-Digest";
        arrayOfString[1] = "SHA-Digest";
        arrayOfString[2] = "MD5-Digest";
        DIGEST_TYPES = arrayOfString;
    }

    private ManifestDigest(Parcel paramParcel)
    {
        this.mDigest = paramParcel.createByteArray();
    }

    ManifestDigest(byte[] paramArrayOfByte)
    {
        this.mDigest = paramArrayOfByte;
    }

    static ManifestDigest fromAttributes(Attributes paramAttributes)
    {
        ManifestDigest localManifestDigest = null;
        if (paramAttributes == null)
            return localManifestDigest;
        Object localObject = null;
        for (int i = 0; ; i++)
            if (i < DIGEST_TYPES.length)
            {
                String str = paramAttributes.getValue(DIGEST_TYPES[i]);
                if (str != null)
                    localObject = str;
            }
            else
            {
                if (localObject == null)
                    break;
                localManifestDigest = new ManifestDigest(Base64.decode(localObject, 0));
                break;
            }
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = false;
        if (!(paramObject instanceof ManifestDigest));
        while (true)
        {
            return bool;
            ManifestDigest localManifestDigest = (ManifestDigest)paramObject;
            if ((this == localManifestDigest) || (Arrays.equals(this.mDigest, localManifestDigest.mDigest)))
                bool = true;
        }
    }

    public int hashCode()
    {
        return Arrays.hashCode(this.mDigest);
    }

    public String toString()
    {
        StringBuilder localStringBuilder = new StringBuilder(1 + ("ManifestDigest {mDigest=".length() + 3 * this.mDigest.length));
        localStringBuilder.append("ManifestDigest {mDigest=");
        int i = this.mDigest.length;
        for (int j = 0; j < i; j++)
        {
            IntegralToString.appendByteAsHex(localStringBuilder, this.mDigest[j], false);
            localStringBuilder.append(',');
        }
        localStringBuilder.append('}');
        return localStringBuilder.toString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeByteArray(this.mDigest);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.ManifestDigest
 * JD-Core Version:        0.6.2
 */