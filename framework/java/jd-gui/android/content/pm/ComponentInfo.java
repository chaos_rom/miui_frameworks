package android.content.pm;

import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.util.Printer;

public class ComponentInfo extends PackageItemInfo
{
    public ApplicationInfo applicationInfo;
    public int descriptionRes;
    public boolean enabled;
    public boolean exported;
    public String processName;

    public ComponentInfo()
    {
        this.enabled = true;
        this.exported = false;
    }

    public ComponentInfo(ComponentInfo paramComponentInfo)
    {
        super(paramComponentInfo);
        this.enabled = true;
        this.exported = false;
        this.applicationInfo = paramComponentInfo.applicationInfo;
        this.processName = paramComponentInfo.processName;
        this.descriptionRes = paramComponentInfo.descriptionRes;
        this.enabled = paramComponentInfo.enabled;
        this.exported = paramComponentInfo.exported;
    }

    protected ComponentInfo(Parcel paramParcel)
    {
        super(paramParcel);
        this.enabled = bool1;
        this.exported = false;
        this.applicationInfo = ((ApplicationInfo)ApplicationInfo.CREATOR.createFromParcel(paramParcel));
        this.processName = paramParcel.readString();
        this.descriptionRes = paramParcel.readInt();
        boolean bool2;
        if (paramParcel.readInt() != 0)
        {
            bool2 = bool1;
            this.enabled = bool2;
            if (paramParcel.readInt() == 0)
                break label81;
        }
        while (true)
        {
            this.exported = bool1;
            return;
            bool2 = false;
            break;
            label81: bool1 = false;
        }
    }

    protected void dumpBack(Printer paramPrinter, String paramString)
    {
        if (this.applicationInfo != null)
        {
            paramPrinter.println(paramString + "ApplicationInfo:");
            this.applicationInfo.dump(paramPrinter, paramString + "    ");
        }
        while (true)
        {
            super.dumpBack(paramPrinter, paramString);
            return;
            paramPrinter.println(paramString + "ApplicationInfo: null");
        }
    }

    protected void dumpFront(Printer paramPrinter, String paramString)
    {
        super.dumpFront(paramPrinter, paramString);
        paramPrinter.println(paramString + "enabled=" + this.enabled + " exported=" + this.exported + " processName=" + this.processName);
        if (this.descriptionRes != 0)
            paramPrinter.println(paramString + "description=" + this.descriptionRes);
    }

    protected ApplicationInfo getApplicationInfo()
    {
        return this.applicationInfo;
    }

    public final int getIconResource()
    {
        if (this.icon != 0);
        for (int i = this.icon; ; i = this.applicationInfo.icon)
            return i;
    }

    public boolean isEnabled()
    {
        if ((this.enabled) && (this.applicationInfo.enabled));
        for (boolean bool = true; ; bool = false)
            return bool;
    }

    protected Drawable loadDefaultIcon(PackageManager paramPackageManager)
    {
        return this.applicationInfo.loadIcon(paramPackageManager);
    }

    protected Drawable loadDefaultLogo(PackageManager paramPackageManager)
    {
        return this.applicationInfo.loadLogo(paramPackageManager);
    }

    public CharSequence loadLabel(PackageManager paramPackageManager)
    {
        Object localObject;
        if (this.nonLocalizedLabel != null)
            localObject = this.nonLocalizedLabel;
        while (true)
        {
            return localObject;
            ApplicationInfo localApplicationInfo = this.applicationInfo;
            if (this.labelRes != 0)
            {
                localObject = paramPackageManager.getText(this.packageName, this.labelRes, localApplicationInfo);
                if (localObject != null);
            }
            else if (localApplicationInfo.nonLocalizedLabel != null)
            {
                localObject = localApplicationInfo.nonLocalizedLabel;
            }
            else if (localApplicationInfo.labelRes != 0)
            {
                localObject = paramPackageManager.getText(this.packageName, localApplicationInfo.labelRes, localApplicationInfo);
                if (localObject != null);
            }
            else
            {
                localObject = this.name;
            }
        }
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        int i = 1;
        super.writeToParcel(paramParcel, paramInt);
        this.applicationInfo.writeToParcel(paramParcel, paramInt);
        paramParcel.writeString(this.processName);
        paramParcel.writeInt(this.descriptionRes);
        int j;
        if (this.enabled)
        {
            j = i;
            paramParcel.writeInt(j);
            if (!this.exported)
                break label68;
        }
        while (true)
        {
            paramParcel.writeInt(i);
            return;
            j = 0;
            break;
            label68: i = 0;
        }
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.ComponentInfo
 * JD-Core Version:        0.6.2
 */