package android.content.pm;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.util.Random;

public class VerifierDeviceIdentity
    implements Parcelable
{
    public static final Parcelable.Creator<VerifierDeviceIdentity> CREATOR = new Parcelable.Creator()
    {
        public VerifierDeviceIdentity createFromParcel(Parcel paramAnonymousParcel)
        {
            return new VerifierDeviceIdentity(paramAnonymousParcel, null);
        }

        public VerifierDeviceIdentity[] newArray(int paramAnonymousInt)
        {
            return new VerifierDeviceIdentity[paramAnonymousInt];
        }
    };
    private static final char[] ENCODE;
    private static final int GROUP_SIZE = 4;
    private static final int LONG_SIZE = 13;
    private static final char SEPARATOR = '-';
    private final long mIdentity;
    private final String mIdentityString;

    static
    {
        char[] arrayOfChar = new char[32];
        arrayOfChar[0] = 65;
        arrayOfChar[1] = 66;
        arrayOfChar[2] = 67;
        arrayOfChar[3] = 68;
        arrayOfChar[4] = 69;
        arrayOfChar[5] = 70;
        arrayOfChar[6] = 71;
        arrayOfChar[7] = 72;
        arrayOfChar[8] = 73;
        arrayOfChar[9] = 74;
        arrayOfChar[10] = 75;
        arrayOfChar[11] = 76;
        arrayOfChar[12] = 77;
        arrayOfChar[13] = 78;
        arrayOfChar[14] = 79;
        arrayOfChar[15] = 80;
        arrayOfChar[16] = 81;
        arrayOfChar[17] = 82;
        arrayOfChar[18] = 83;
        arrayOfChar[19] = 84;
        arrayOfChar[20] = 85;
        arrayOfChar[21] = 86;
        arrayOfChar[22] = 87;
        arrayOfChar[23] = 88;
        arrayOfChar[24] = 89;
        arrayOfChar[25] = 90;
        arrayOfChar[26] = 50;
        arrayOfChar[27] = 51;
        arrayOfChar[28] = 52;
        arrayOfChar[29] = 53;
        arrayOfChar[30] = 54;
        arrayOfChar[31] = 55;
        ENCODE = arrayOfChar;
    }

    public VerifierDeviceIdentity(long paramLong)
    {
        this.mIdentity = paramLong;
        this.mIdentityString = encodeBase32(paramLong);
    }

    private VerifierDeviceIdentity(Parcel paramParcel)
    {
        long l = paramParcel.readLong();
        this.mIdentity = l;
        this.mIdentityString = encodeBase32(l);
    }

    private static final long decodeBase32(byte[] paramArrayOfByte)
        throws IllegalArgumentException
    {
        long l = 0L;
        int i = 0;
        int j = paramArrayOfByte.length;
        int k = 0;
        if (k < j)
        {
            int m = paramArrayOfByte[k];
            if ((65 <= m) && (m <= 90));
            for (int n = m - 65; ; n = m - 24)
            {
                label45: l = l << 5 | n;
                i++;
                if (i != 1)
                    break label198;
                if ((n & 0xF) == n)
                    break label112;
                throw new IllegalArgumentException("illegal start character; will overflow");
                if ((50 > m) || (m > 55))
                    break;
            }
            if (m == 45);
            label112: 
            while (i <= 13)
            {
                k++;
                break;
                if ((97 <= m) && (m <= 122))
                {
                    n = m - 97;
                    break label45;
                }
                if (m == 48)
                {
                    n = 14;
                    break label45;
                }
                if (m == 49)
                {
                    n = 8;
                    break label45;
                }
                throw new IllegalArgumentException("base base-32 character: " + m);
            }
            label198: throw new IllegalArgumentException("too long; should have 13 characters");
        }
        if (i != 13)
            throw new IllegalArgumentException("too short; should have 13 characters");
        return l;
    }

    private static final String encodeBase32(long paramLong)
    {
        char[] arrayOfChar1 = ENCODE;
        char[] arrayOfChar2 = new char[16];
        int i = arrayOfChar2.length;
        for (int j = 0; j < 13; j++)
        {
            if ((j > 0) && (j % 4 == 1))
            {
                i--;
                arrayOfChar2[i] = '-';
            }
            int k = (int)(0x1F & paramLong);
            paramLong >>>= 5;
            i--;
            arrayOfChar2[i] = arrayOfChar1[k];
        }
        return String.valueOf(arrayOfChar2);
    }

    public static VerifierDeviceIdentity generate()
    {
        return generate(new SecureRandom());
    }

    static VerifierDeviceIdentity generate(Random paramRandom)
    {
        return new VerifierDeviceIdentity(paramRandom.nextLong());
    }

    public static VerifierDeviceIdentity parse(String paramString)
        throws IllegalArgumentException
    {
        try
        {
            byte[] arrayOfByte = paramString.getBytes("US-ASCII");
            return new VerifierDeviceIdentity(decodeBase32(arrayOfByte));
        }
        catch (UnsupportedEncodingException localUnsupportedEncodingException)
        {
        }
        throw new IllegalArgumentException("bad base-32 characters in input");
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object paramObject)
    {
        boolean bool = false;
        if (!(paramObject instanceof VerifierDeviceIdentity));
        while (true)
        {
            return bool;
            VerifierDeviceIdentity localVerifierDeviceIdentity = (VerifierDeviceIdentity)paramObject;
            if (this.mIdentity == localVerifierDeviceIdentity.mIdentity)
                bool = true;
        }
    }

    public int hashCode()
    {
        return (int)this.mIdentity;
    }

    public String toString()
    {
        return this.mIdentityString;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
        paramParcel.writeLong(this.mIdentity);
    }
}

/* Location:                     /home/lithium/miui/chameleon/2.11.16/framework_dex2jar.jar
 * Qualified Name:         android.content.pm.VerifierDeviceIdentity
 * JD-Core Version:        0.6.2
 */